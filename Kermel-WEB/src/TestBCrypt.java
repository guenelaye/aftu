

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

import org.mindrot.jbcrypt.BCrypt;

public class TestBCrypt {

	public static void main(String[] args) {
		
		try {
			String motdepass = BCrypt.hashpw("admin", BCrypt.gensalt(12,SecureRandom.getInstance("SHA1PRNG","SUN")));
			System.out.println("######## " +motdepass+ " ##########");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
