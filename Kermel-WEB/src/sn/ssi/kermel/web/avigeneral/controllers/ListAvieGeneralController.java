package sn.ssi.kermel.web.avigeneral.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.avisgeneral.ejb.AvisGeneralSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygAvisGeneral;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;
import sn.ssi.kermel.web.referentiel.controllers.DownloaDocsController;



@SuppressWarnings("serial")
public class ListAvieGeneralController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox list;
	private Paging pgAvi;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private String sygmNumero;
	private Listheader libelledossiers,Typedossier;
	private Textbox txtnumero,txtTypedossier;
	Long code;
	public String extension,images;
	private Menuitem ADD_AVIGENERAL, MOD_AVIGENERAL, SUPP_AVIGENERAL,WPUBLIER_AVIGENERAL,WDEPUBLIER_AVIGENERAL;
	 private KermelSousMenu monSousMenu;
	 private Integer Etat;
	 private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private final String cheminDossier = UIConstants.PATH_AVISGENERAUX;
	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.AVIGENERAL);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		Components.wireFellows(this, this);
		if (ADD_AVIGENERAL != null)
			ADD_AVIGENERAL.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
		if (MOD_AVIGENERAL != null)
			MOD_AVIGENERAL.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
		if (SUPP_AVIGENERAL != null)
			SUPP_AVIGENERAL.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
		if (WPUBLIER_AVIGENERAL != null)
			WPUBLIER_AVIGENERAL.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_PUBLIER);
		
		if (WDEPUBLIER_AVIGENERAL != null){
			WDEPUBLIER_AVIGENERAL.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DEPUBLIER);
			WDEPUBLIER_AVIGENERAL.setVisible(false);
		}
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_PUBLIER, this);
		addEventListener(ApplicationEvents.ON_DEPUBLIER, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
 
		list.setItemRenderer(this);

		pgAvi.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pgAvi.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		
	}

	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 List<SygAvisGeneral> avis = BeanLocator.defaultLookup(AvisGeneralSession.class).find(pgAvi.getActivePage()*byPage,byPage,autorite,null,sygmNumero);
			 SimpleListModel listModel = new SimpleListModel(avis);
			list.setModel(listModel);
			pgAvi.setTotalSize(BeanLocator.defaultLookup(AvisGeneralSession.class).count(autorite,null,sygmNumero));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/avisgeneral/formavigeneral.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.publication.avis.general"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(AvieGeneralFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/avisgeneral/formavigeneral.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.publication.avis.general"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(AvieGeneralFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(AvieGeneralFormController.PARAM_WIDOW_CODE, list
					.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				// de
				// fixer
				// les
				// dimenisons
				// du
				// popup
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// de
				// passer
				// des
				// parametres
				// au
				// popup
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < list.getSelectedCount(); i++) {
					Long codes = (Long)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					System.out.println(codes);
				BeanLocator.defaultLookup(AvisGeneralSession.class).delete(codes);
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_PUBLIER)){
				if (list.getSelectedItem()==null) 
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				 for (int i = 0; i < list.getSelectedCount(); i++) {
					 Long code=(Long) ((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					 SygAvisGeneral	avi= BeanLocator.defaultLookup(AvisGeneralSession.class).findById(code);
					 avi.setEtat(1);
					 avi.setDatepublication(new Date());
			      	   BeanLocator.defaultLookup(AvisGeneralSession.class).update(avi);
			      	   
				 }
				 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DEPUBLIER)){
				if (list.getSelectedItem()==null) 
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				 for (int i = 0; i < list.getSelectedCount(); i++) {
					 Long code=(Long) ((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					 SygAvisGeneral	avi= BeanLocator.defaultLookup(AvisGeneralSession.class).findById(code);
					 avi.setEtat(0);
					 avi.setDatepublication(null);
			      	   BeanLocator.defaultLookup(AvisGeneralSession.class).update(avi);
			      	   
				 }
				 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
			else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
				if(event.getTarget()!=null)
				{
					Listcell button = (Listcell) event.getTarget();
				       String uri = null;
						
						HashMap<String, String> display = null;
						HashMap<String, Object> data = null;

						uri = "/formateur/doc.zul" ;

						data = new HashMap<String, Object>();
						display = new HashMap<String, String>();

					
						data.put(DownloaDocsController.NOM_FICHIER, (String) button.getAttribute(UIConstants.TODO));
						data.put(DownloaDocsController.NOM_URL, cheminDossier);

						display.put(DSP_TITLE, "Visualisation du fichier");
						display.put(DSP_WIDTH, "90%");
						display.put(DSP_HEIGHT, "600px");

						showPopupWindow(uri, data, display);
				}
			
			}
			
	}
	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render( Listitem item,  Object data, int index) throws Exception {
		SygAvisGeneral avi = (SygAvisGeneral) data;
		item.setValue(avi.getId());
		item.setAttribute("Etat", avi.getEtat());
		Listcell cellnumero = new Listcell(avi.getNumero());
		cellnumero.setParent(item);
		Listcell cellannee = new Listcell(avi.getAnnee());
		cellannee.setParent(item);
		Listcell celldatepublication = new Listcell("");
		if(avi.getDatepublication()!=null)
		celldatepublication.setLabel(UtilVue.getInstance().formateLaDate2(avi.getDatepublication()));
		celldatepublication.setParent(item);
		
		
		
		  Listcell cellImage = new Listcell();
			
			if (avi.getFichier_Avis()!=null)  
			extension=avi.getFichier_Avis().substring(avi.getFichier_Avis().length()-3,  avi.getFichier_Avis().length());
			else extension="";
			 if(extension.equalsIgnoreCase("pdf"))
				 images="/images/icone_pdf.png";
			 else  
				 images="/images/word.jpg";

			 cellImage.setImage(images);
			 cellImage.setAttribute(UIConstants.TODO, avi.getFichier_Avis());
			 cellImage.setTooltiptext("Editer Avis");
			 cellImage.addEventListener(Events.ON_CLICK, ListAvieGeneralController.this);
			 cellImage.setParent(item);

			
		 
	}
	
	public void onOK$txtnumero() {
		onClick$btnRechercher();
	}
	public void onClick$btnRechercher() {
		
		
		if (txtnumero.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.Numero")) || txtnumero.getValue().equals("")) {
			sygmNumero = null;
		} else {
			sygmNumero  = txtnumero.getValue();
		}	

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	public void onFocus$txtnumero() {
				if (txtnumero.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.Numero"))) {
					txtnumero.setValue("");

				}
		}
				public void onBlur$txtnumero() {
					if (txtnumero.getValue().equalsIgnoreCase("")) {
						txtnumero.setValue(Labels.getLabel("kermel.common.form.Numero"));
					}	
			}
				public void onSelect$list(){
					if(list.getSelectedItem()!=null)
					{
						Etat=Integer.parseInt(list.getSelectedItem().getAttribute("Etat").toString());
						if(Etat==0)
							
						{
							SUPP_AVIGENERAL.setVisible(true);
							MOD_AVIGENERAL.setVisible(true);
							WPUBLIER_AVIGENERAL.setVisible(true);
							WDEPUBLIER_AVIGENERAL.setVisible(false);
						}
						else 
						{
							SUPP_AVIGENERAL.setVisible(false);
							WPUBLIER_AVIGENERAL.setVisible(false);
							WDEPUBLIER_AVIGENERAL.setVisible(true);
							MOD_AVIGENERAL.setVisible(false);
						}
					}
					
					}
					
			  
}
