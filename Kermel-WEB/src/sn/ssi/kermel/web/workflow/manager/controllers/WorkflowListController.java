package sn.ssi.kermel.web.workflow.manager.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Window;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.security.ProfilsSession;
import sn.ssi.kermel.be.workflow.entity.SysRole;
import sn.ssi.kermel.be.workflow.manager.ejb.WorkflowManagerSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

import com.mxgraph.util.mxUtils;

@SuppressWarnings("serial")
public class WorkflowListController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstRoles;
	private Paging pgRoles;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_ROLE = "CURRENT_ROLE";

	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);

		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener("onView", this);
		addEventListener("onReload", this);

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lstRoles.setItemRenderer(this);

		pgRoles.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pgRoles.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			final List<SysRole> roles = BeanLocator.defaultLookup(
					WorkflowManagerSession.class).findRoles(
					pgRoles.getActivePage() * byPage, byPage);
			final SimpleListModel listModel = new SimpleListModel(roles);
			lstRoles.setModel(listModel);
			pgRoles.setTotalSize(BeanLocator
					.defaultLookup(ProfilsSession.class).countAllProfils(null, null,null,null));
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/workflow/manager/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_WIDTH, "500px");
			display.put(DSP_HEIGHT, "250px");
			display.put(DSP_TITLE, "Nouveau R�le");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(WorkflowFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstRoles.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/workflow/manager/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_WIDTH, "500px");
			display.put(DSP_HEIGHT, "250px");
			display.put(DSP_TITLE, "Editer Profil");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(WorkflowFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(WorkflowFormController.WINDOW_PARAM_ROLE_CODE, lstRoles
					.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} else if (event.getName().equalsIgnoreCase(Events.ON_DOUBLE_CLICK)) {
			final Listitem listitem = (Listitem) event.getTarget();
			final String uri = "/workflow/manager/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_WIDTH, "500px");
			display.put(DSP_HEIGHT, "250px");
			display.put(DSP_TITLE, "Editer R�le");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(WorkflowFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(WorkflowFormController.WINDOW_PARAM_ROLE_CODE, listitem
					.getValue());

			showPopupWindow(uri, data, display);
		} else if (event.getName()
				.equalsIgnoreCase(ApplicationEvents.ON_DELETE)) {
			if (lstRoles.getSelectedItem() == null) {
				alert("Aucune ligne n'est s�lectionn�e");
				return;
			}
			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(MessageBoxController.DSP_MESSAGE,
					"Supprimer ce processus?");
			display.put(MessageBoxController.DSP_TITLE, "Supprimer?");
			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(CURRENT_ROLE, lstRoles.getSelectedItem().getValue());
			showMessageBox(display, data);
		}

		else if (event.getName().equalsIgnoreCase(
				ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {
			if (((HashMap<String, Object>) event.getData())
					.get("reload_first_confirm") != null) {
				final HashMap<String, String> display = new HashMap<String, String>();
				display
						.put(MessageBoxController.DSP_MESSAGE,
								"La base d'�tats et de transitions va �tre recharg�e, voulez-vous continuez?");
				display.put(MessageBoxController.DSP_TITLE, "Recharger?");
				final HashMap<String, Object> data = new HashMap<String, Object>();
				data.put("reload_second_confirm", 2);
				showMessageBox(display, data);

			} else if (((HashMap<String, Object>) event.getData())
					.get("reload_second_confirm") != null) {
				BeanLocator.defaultLookup(WorkflowManagerSession.class)
						.loadArrowsAndStates();
			} else {
				final String roleCode = (String) ((HashMap<String, Object>) event
						.getData()).get(CURRENT_ROLE);
				BeanLocator.defaultLookup(WorkflowManagerSession.class)
						.deleteRole(roleCode);
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
		}

		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DETAILS)) {
			if (lstRoles.getSelectedItem() == null) {
				alert("");
				return;
			}
			loadApplicationState("profil_actions&"
					+ lstRoles.getSelectedItem().getValue());
		}

		else if (event.getName().equalsIgnoreCase("onView")) {
			if (lstRoles.getSelectedItem() == null) {
				alert("");
				return;
			}

			final SysRole role = BeanLocator.defaultLookup(
					WorkflowManagerSession.class).findByCode(
					lstRoles.getSelectedItem().getValue().toString());

			final Window win = (Window) Executions.createComponents(
					"/workflow/manager/view.zul", null, null);
			win.setWidth("500px");
			win.setHeight("500px");

			if (role.getSvg() != null) {
				mxUtils.writeFile(role.getSvg(), Sessions.getCurrent()
						.getWebApp().getRealPath("/img")
						+ "/preview.svg");
			}

			win.setMaximizable(true);
			win.doModal();
		} else if (event.getName().equalsIgnoreCase("onReload")) {
			final HashMap<String, String> display = new HashMap<String, String>();
			display
					.put(MessageBoxController.DSP_MESSAGE,
							"Voulez-vous r�ellement recharger les configurations de base?");
			display.put(MessageBoxController.DSP_TITLE, "Recharger?");
			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put("reload_first_confirm", 1);
			showMessageBox(display, data);
		}

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		final SysRole role = (SysRole) data;
		item.setValue(role.getCode());
		item.addEventListener(Events.ON_DOUBLE_CLICK, this);

		final Listcell cellCode = new Listcell(role.getCode());
		cellCode.setParent(item);

		final Listcell cellDescription = new Listcell(role.getDesc());
		cellDescription.setParent(item);
	}

}
