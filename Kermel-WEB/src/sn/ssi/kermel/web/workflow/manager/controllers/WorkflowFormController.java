package sn.ssi.kermel.web.workflow.manager.controllers;

import java.io.FileOutputStream;
import java.io.StringBufferInputStream;
import java.util.Map;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Fileupload;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.workflow.entity.SysRole;
import sn.ssi.kermel.be.workflow.manager.ejb.WorkflowManagerSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings( { "serial", "deprecation" })
public class WorkflowFormController extends AbstractWindow implements
		AfterCompose, EventListener {

	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_ROLE_CODE = "ROLE_CODE";

	String mode;
	String roleCode;
	String xmlFile;
	String xml;
	SysRole role;

	private Textbox txtDescription, txtCode, txtFichier;

	private void initUI() {
		if (mode.equalsIgnoreCase("EDIT")) {
			txtCode.setValue(role.getCode());
			txtCode.setDisabled(true);
			txtDescription.setValue(role.getDesc());
		}
	}

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(final CreateEvent createEvent) {
		final Map windowParams = createEvent.getArg();
		mode = (String) windowParams.get(WINDOW_PARAM_MODE);
		if (mode.equalsIgnoreCase("EDIT")) {
			roleCode = (String) windowParams.get(WINDOW_PARAM_ROLE_CODE);
			role = BeanLocator.defaultLookup(WorkflowManagerSession.class)
					.findByCode(roleCode);
		}
		initUI();
	}

	@Override
	public void onEvent(final Event event) throws Exception {

	}

	/**
	 * upload role file
	 * 
	 * @param a_strServerPath
	 * @return path of file
	 */
	public String upload(final String a_strServerPath) {
		String m_Path = null;

		try {

			// open select file dialog
			final Object media = Fileupload.get();
			if (media instanceof org.zkoss.util.media.Media) {
				final org.zkoss.util.media.Media m = (org.zkoss.util.media.Media) media;

				// get file name
				final String m_strFullName = a_strServerPath + m.getName();

				// upload file
				final boolean m_blUP = uploadFile(m, m_strFullName);
				if (m_blUP) {
					// remember file name
					m_Path = m_strFullName;
				} else {
					System.out.print("Chargement impossible");
				}
			}

		} catch (final Exception e) {
			// handle ERROR
			e.printStackTrace();
		}
		return m_Path;
	}

	/**
	 * upload a file to server
	 * 
	 * @param a_mdFile
	 * @param a_strServerPath
	 * @param a_strFileName
	 * @return
	 */
	public boolean uploadFile(final org.zkoss.util.media.Media a_mdFile,
			final String a_strFullName) {
		boolean m_blUF = true;
		try {
			// buffer size in memory
			final byte[] buffer = new byte[1024];
			// java.io.FileInputStream
			xml = a_mdFile.getStringData();
			final StringBufferInputStream sbf = new StringBufferInputStream(xml);
			// FileInputStream m_fisFile = (FileInputStream) StringBuffer1;

			// java.io.FileOutputStream
			final FileOutputStream m_fosFile = new FileOutputStream(
					a_strFullName);
			while (sbf.read(buffer) != -1) {
				// write to server restore
				m_fosFile.write(buffer);
			}
			// finish writing
			m_fosFile.flush();
			m_fosFile.close();
		} catch (final Exception e) {
			// handle ERROR
			e.printStackTrace();
			m_blUF = false;
		}
		return m_blUF;
	}

	public void onLoad() {
		xmlFile = upload(Sessions.getCurrent().getWebApp().getRealPath(
				"/workflow/"));
		txtFichier.setText(xmlFile);

	}

	public void onOK() {
		final SysRole role = new SysRole();
		role.setCode(txtCode.getValue());
		role.setDesc(txtDescription.getValue());
		BeanLocator.defaultLookup(WorkflowManagerSession.class).loadRole(role,xml);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();
	}

}