package sn.ssi.kermel.web.workflow.test.controllers;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import sn.ssi.kermel.be.workflow.entity.SysArrow;

public class ActionsRenderer implements ListitemRenderer, EventListener {

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		final SysArrow arrow = (SysArrow) data;
		item.setValue(arrow.getCode());

		final Listcell cellCode = new Listcell(arrow.getCode());
		cellCode.setParent(item);

	}

	@Override
	public void onEvent(final Event event) throws Exception {
		// TODO Auto-generated method stub

	}

}
