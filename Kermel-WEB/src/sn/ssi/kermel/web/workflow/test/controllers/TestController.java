package sn.ssi.kermel.web.workflow.test.controllers;

import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.workflow.ejb.WorkflowSession;
import sn.ssi.kermel.be.workflow.entity.SysArrow;
import sn.ssi.kermel.be.workflow.entity.SysState;
import sn.ssi.kermel.be.workflow.manager.ejb.WorkflowManagerSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class TestController extends AbstractWindow implements EventListener,
		AfterCompose, ListitemRenderer {

	private String stateCode;
	private String arrowCode;
	private Listbox lstStates;
	private Listbox lstArrows;
	public static final String CURRENT_STATE = "CURRENT_STATE";

	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);

		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lstStates.setItemRenderer(this);
		lstArrows.setItemRenderer(new ActionsRenderer());
		// pgStates.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		// pgStates.addForward("onPaging", this,
		// ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			// List<PadState> states =
			// BeanLocator.defaultLookup(WorkflowManagerSession.class).findStates();
			final List<SysState> states = BeanLocator.defaultLookup(
					WorkflowSession.class).findCanViewStates("AGREMENT", 1);

			final SimpleListModel listModelStates = new SimpleListModel(states);
			lstStates.setModel(listModelStates);

			final List<SysArrow> arrows = BeanLocator.defaultLookup(
					WorkflowManagerSession.class).findArrows();
			final SimpleListModel listModelArrows = new SimpleListModel(arrows);
			lstArrows.setModel(listModelArrows);
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			stateCode = lstStates.getSelectedItem().getValue().toString();
			arrowCode = lstArrows.getSelectedItem().getValue().toString();

			final SysState state = BeanLocator.defaultLookup(
					WorkflowSession.class).findToState(arrowCode);
			for (int i = 0; i < lstStates.getItemCount(); i++)
				if (lstStates.getItemAtIndex(i).getValue().toString()
						.equalsIgnoreCase(state.getCode())) {
					lstStates.setSelectedItem(lstStates.getItemAtIndex(i));
				}

			final List<SysArrow> arrows = BeanLocator.defaultLookup(
					WorkflowSession.class).findStateArrows(1, state.getCode());
			final SimpleListModel listModelArrows = new SimpleListModel(arrows);
			lstArrows.setModel(listModelArrows);

		} else if (event.getName().equalsIgnoreCase(Events.ON_DOUBLE_CLICK)) {
			stateCode = lstStates.getSelectedItem().getValue().toString();
			final List<SysArrow> arrows = BeanLocator.defaultLookup(
					WorkflowSession.class).findStateArrows(1, stateCode);
			final SimpleListModel listModelArrows = new SimpleListModel(arrows);
			lstArrows.setModel(listModelArrows);

		} else if (event.getName().equalsIgnoreCase(
				ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {
			/*
			 * String roleCode = (String) ((HashMap<String,
			 * Object>)event.getData()).get(CURRENT_STATE);
			 * Events.postEvent(ApplicationEvents.ON_MODEL_CHANGESTATE, this,
			 * null);
			 */
		}/*
		 * elseif(event.getName().equalsIgnoreCase(ApplicationEvents.
		 * ON_MODEL_CHANGESTATE)) { List<PadArrow> arrows =
		 * BeanLocator.defaultLookup(WorkflowManagerSession.class).findArrows();
		 * SimpleListModel listModel = new SimpleListModel(arrows);
		 * lstArrows.setModel(listModel); }
		 */
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {

		final SysState state = (SysState) data;
		item.setValue(state.getCode());
		item.addEventListener(Events.ON_DOUBLE_CLICK, this);

		final Listcell cellCode = new Listcell(state.getCode());
		cellCode.setParent(item);

	}

}
