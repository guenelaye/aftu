package sn.ssi.kermel.web.alertes.controllers;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import sn.ssi.kermel.be.entity.Utilisateur;

public class UserConcerneRenderer implements ListitemRenderer, EventListener {

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {

		final Utilisateur user = (Utilisateur) data;
		item.setValue(user.getId());

		final Listcell cellCode = new Listcell(user.getLogin());
		cellCode.setParent(item);

	}

	@Override
	public void onEvent(final Event event) throws Exception {
		// TODO Auto-generated method stub

	}

}
