package sn.ssi.kermel.web.alertes.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.alertes.AlertesSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SysAlerte;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class AlertesListController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstAlertes;
	private Paging pgAlertes;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_ALERTE = "CURRENT_ALERTE";

	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);

		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lstAlertes.setItemRenderer(this);

		pgAlertes.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pgAlertes.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			final List<SysAlerte> alertes = BeanLocator.defaultLookup(
					AlertesSession.class).findAlertes(
					pgAlertes.getActivePage() * byPage, byPage);
			final SimpleListModel listModel = new SimpleListModel(alertes);
			lstAlertes.setModel(listModel);
			pgAlertes.setTotalSize(BeanLocator.defaultLookup(
					AlertesSession.class).countAllAlertes());
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/alertes/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_HEIGHT, "600px");
			display.put(DSP_TITLE, "Nouvelle alerte");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(AlertesFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstAlertes.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/alertes/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_HEIGHT, "600px");
			display.put(DSP_TITLE, "Editer alerte");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(AlertesFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(AlertesFormController.WINDOW_PARAM_ALERTE_ID, lstAlertes
					.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} else if (event.getName().equalsIgnoreCase(Events.ON_DOUBLE_CLICK)) {
			final Listitem listitem = (Listitem) event.getTarget();
			final String uri = "/alertes/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_HEIGHT, "600px");
			display.put(DSP_TITLE, "Editer Alerte");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(AlertesFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(AlertesFormController.WINDOW_PARAM_ALERTE_ID, listitem
					.getValue());

			showPopupWindow(uri, data, display);
		} else if (event.getName()
				.equalsIgnoreCase(ApplicationEvents.ON_DELETE)) {
			if (lstAlertes.getSelectedItem() == null) {
				alert("");
				return;
			}
			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(MessageBoxController.DSP_MESSAGE, "Supprimer?");
			display.put(MessageBoxController.DSP_TITLE, "Supprimer?");
			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(CURRENT_ALERTE, lstAlertes.getSelectedItem().getValue());
			showMessageBox(display, data);
		}

		else if (event.getName().equalsIgnoreCase(
				ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {
			final int alerteId = Integer
					.parseInt((String) ((HashMap<String, Object>) event
							.getData()).get(CURRENT_ALERTE));
			BeanLocator.defaultLookup(AlertesSession.class).deleteAlerte(
					alerteId);
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}

		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DETAILS)) {
			if (lstAlertes.getSelectedItem() == null) {
				alert("");
				return;
			}

			final String uri = "/alertes/detailform.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_HEIGHT, "600px");
			display.put(DSP_TITLE, "D�tails de l'alerte");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(DetailAlertesFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(DetailAlertesFormController.WINDOW_PARAM_ALERTE_ID,
					lstAlertes.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		}

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		final SysAlerte alerte = (SysAlerte) data;
		item.setValue(alerte.getId());
		item.addEventListener(Events.ON_DOUBLE_CLICK, this);

		final Listcell cellNom = new Listcell(alerte.getNom());
		cellNom.setParent(item);

		final Listcell cellDescription = new Listcell(alerte.getDesc());
		cellDescription.setParent(item);

		final Listcell cellState = new Listcell(alerte.getSysState()
				.getLibelle());
		cellState.setParent(item);

		final Listcell cellDelai = new Listcell(String.valueOf(alerte
				.getDelai()));
		cellDelai.setParent(item);

	}
}