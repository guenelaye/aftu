package sn.ssi.kermel.web.alertes.controllers;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLSocketFactory;

import sun.misc.BASE64Encoder;

public class SmsSender {

public static void main(String[] args) {

	
		
		
		
		try {
			List<String> numeros = new ArrayList<String>();
			numeros.add("221773076090");
			send(numeros);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	public static void  send(List<String> numeros) throws Exception {
		String nums = "";
		int id = 1;
		for(String numero:numeros) {
			nums += "{\"id\":"+id+",\"value\":\""+numero+"\"}";
			if(id == 1) 
				nums+= ","; 
			id++;
		}
		String documentJSON="{\"messages\":["
			 + "{"
				+ "\"signature\":\"KERMEL\",\"subject\":\"NOTIF - DRP\",\"content\":\"Vous avez recu une invitation pour une nouvelle drp. Consultez votre messagerie ou rendez -vous sur www.pkermel.sn\","
				+ "\"recipients\":["
								+nums+
								 "]"
			+ "}"
		 + "]"
	  + "}"; 
 	 
		String privateKey="a1f4c3fcdc53be18a4c2bad6d1de3b05";
    	String token="962b8a087e9985c1bd56f84118698cb8";
    	String login = "2si";
		String authString = login + ":" + token;
		String authStringEnc = new BASE64Encoder().encode(authString.getBytes());
		long timestamp = System.currentTimeMillis()/1000;
		String msgToEncrypt=token+documentJSON+timestamp;
		String publicKey=hmacSha(privateKey,msgToEncrypt);	
		
		System.setProperty("javax.net.ssl.trustStore", "clienttrust"); 
		
		SSLSocketFactory ssf = (SSLSocketFactory) SSLSocketFactory.getDefault();
	    Socket socket = ssf.createSocket("api.orangesmspro.sn", 8443);
		String path = "/api/json?token="+token+"&key="+publicKey+"&timestamp="+timestamp;
		String host = "api.orangesmspro.sn";
		
		BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF8"));
		wr.write("POST " + path + " HTTP/1.1\r\n");
		wr.write("Authorization: Basic " + authStringEnc + "\r\n");
		wr.write("HOST: " + host + "\r\n");
		wr.write("Content-Length: " + documentJSON.length() + "\r\n");
		wr.write("Content-Type: application/json\r\n");
		wr.write("\r\n");
		wr.write(documentJSON);
		wr.flush();
		BufferedReader rd = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		String line;
		while ((line = rd.readLine()) != null) {
			System.out.println(line);
		}
		wr.close();
		rd.close();

    }
    
    public static String hmacSha(String SECRETKEY, String VALUE) {
        try {
            SecretKeySpec signingKey = new SecretKeySpec(SECRETKEY.getBytes("UTF-8"), "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(VALUE.getBytes("UTF-8"));

            byte[] hexArray = {
                    (byte)'0', (byte)'1', (byte)'2', (byte)'3',
                    (byte)'4', (byte)'5', (byte)'6', (byte)'7',
                    (byte)'8', (byte)'9', (byte)'a', (byte)'b',
                    (byte)'c', (byte)'d', (byte)'e', (byte)'f'
            };
            byte[] hexChars = new byte[rawHmac.length * 2];
            for ( int j = 0; j < rawHmac.length; j++ ) {
                int v = rawHmac[j] & 0xFF;
                hexChars[j * 2] = hexArray[v >>> 4];
                hexChars[j * 2 + 1] = hexArray[v & 0x0F];
            }
            return new String(hexChars);
        }
        catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

	public static void sendSms(String telephone, String message)
			throws IOException { // testapi

		String link = "http://sms2.orange.sn:8080/2smobile/api?";
		String key_private = "97a0c854df9f5163cf1b64c3dc6f0041";
		String token = "0c5779230d6e663d09755c784daecad3";
		String subject = "NOTIF-DRP";
		String signature = "2SI";

		try {
			message = URLEncoder.encode(message, "UTF8");
			subject = URLEncoder.encode(subject, "UTF8");
			signature = URLEncoder.encode(signature, "UTF8");
			long timestamp = System.currentTimeMillis() / 1000;
			String msgToEncrypt = token + subject + signature + telephone
					+ message + timestamp;
			String key = hmacDigest(msgToEncrypt, key_private, "HmacSHA1");
			String url = link + "token=" + token + "&subject=" + subject
					+ "&signature=" + signature + "&recipient=" + telephone
					+ "&content=" + message + "&timestamp=" + timestamp
					+ "&key=" + key;
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			int responseCode = con.getResponseCode();
			System.out.println("\nSending  request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			System.out.println(response.toString());
		}

		catch (MalformedURLException mue) {
			mue.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void sendSms(String telephone, String subject,
			String message) throws IOException { // testapi
		String xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"?>"
				+ "<message>"
				+ "<login>testapi</login>"
				+ "<password>passer</password>"
				+ "<content>"
				+ message
				+ "</content>"
				+ "<recipients>"
				+ telephone
				+ "</recipients>"
				+ "<signature>KERMEL</signature>"
				+ "<subject>"
				+ subject
				+ "</subject>" + "</message>";
		String host = "http://sms2.orange.sn";
		Socket socket = new Socket(host, 8080);
		String path = "/2smobile/api/xml";
		
		BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(
				socket.getOutputStream(), "UTF8"));
		wr.write("POST " + path + " HTTP/1.1\r\n");
		wr.write("HOST: " + host + "\r\n");
		wr.write("Content-Length: " + xml.length() + "\r\n");
		wr.write("Content-Type: application/xml\r\n");
		wr.write("\r\n");
		wr.write(xml);
		wr.flush();
		BufferedReader rd = new BufferedReader(new InputStreamReader(
				socket.getInputStream()));
		String line;
		while ((line = rd.readLine()) != null) {
			System.out.println(line);
		}
		wr.close();
		rd.close();
	}

	public static String hmacDigest(String msg, String keyString, String algo) {
		String digest = null;
		try {
			SecretKeySpec key = new SecretKeySpec(
					(keyString).getBytes("UTF-8"), algo);
			Mac mac = Mac.getInstance(algo);
			mac.init(key);

			byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

			StringBuffer hash = new StringBuffer();
			for (int i = 0; i < bytes.length; i++) {
				String hex = Integer.toHexString(0xFF & bytes[i]);
				if (hex.length() == 1) {
					hash.append('0');
				}
				hash.append(hex);
			}
			digest = hash.toString();
		} catch (UnsupportedEncodingException e) {
		} catch (InvalidKeyException e) {
		} catch (NoSuchAlgorithmException e) {
		}
		return digest;
	}
}
