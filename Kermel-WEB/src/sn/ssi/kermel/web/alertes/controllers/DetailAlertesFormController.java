package sn.ssi.kermel.web.alertes.controllers;

import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Radio;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.alertes.AlertesSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.StringOperation;
import sn.ssi.kermel.be.entity.SysAlerte;
import sn.ssi.kermel.be.entity.SysAlerteprofil;
import sn.ssi.kermel.be.entity.SysProfil;
import sn.ssi.kermel.be.entity.SysUtilisateuralerte;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.security.ProfilsSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class DetailAlertesFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_ALERTE_ID = "ALERTE_ID";

	private Listbox lstProfil;
	private Listbox lstUser;
	private Listbox lstUserConcerne;
	private Radio rdSemaine;
	private Radio rdJour;
	private Radio rdAutres;
	private Radio rdNotification;
	private Radio rdAlerte;
	private Bandbox bd;

	String mode;
	long alerteId;
	SysAlerte alerte;

	private Textbox txtPeriode;

	private void initUI() {
		if (mode.equalsIgnoreCase("EDIT")) {
			txtPeriode.setReadonly(true);
			if (alerte.getPeriode() == 7) {
				rdSemaine.setSelected(true);
			} else if (alerte.getPeriode() == 1) {
				rdJour.setSelected(true);
			} else {
				rdAutres.setSelected(true);
				txtPeriode.setValue(String.valueOf(alerte.getPeriode()));
				txtPeriode.setReadonly(false);
			}

			if (alerte.getNotification() == 1) {
				rdNotification.setSelected(true);
			} else {
				rdAlerte.setSelected(true);
			}

			final List<Utilisateur> usersConcerne = BeanLocator.defaultLookup(
					AlertesSession.class).findAlerteUsers(alerteId);
			for (int i = 0; i < usersConcerne.size(); i++) {
				lstUserConcerne.appendItem(usersConcerne.get(i).getLogin(),
						usersConcerne.get(i).getId().toString());
			}
		}
	}

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		lstProfil.setItemRenderer(this);
		lstUser.setItemRenderer(new UserRenderer());
		lstUserConcerne.setItemRenderer(new UserConcerneRenderer());
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

	}

	@SuppressWarnings("unchecked")
	public void onCreate(final CreateEvent createEvent) {
		final Map windowParams = createEvent.getArg();
		mode = (String) windowParams.get(WINDOW_PARAM_MODE);
		if (mode.equalsIgnoreCase("EDIT")) {
			alerteId = (Long) windowParams.get(WINDOW_PARAM_ALERTE_ID);
			alerte = BeanLocator.defaultLookup(AlertesSession.class).findById(
					alerteId);
		}
		initUI();
	}

	@Override
	public void onEvent(final Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			final List<SysProfil> profils = BeanLocator.defaultLookup(
					ProfilsSession.class).findProfils(0, 1000, null, null,null,null);
			final SimpleListModel listModel = new SimpleListModel(profils);
			lstProfil.setModel(listModel);

			final List<Utilisateur> users = BeanLocator.defaultLookup(
					AlertesSession.class).findUsers(0, 1000);
			final SimpleListModel listModelUsers = new SimpleListModel(users);
			lstUser.setModel(listModelUsers);

		}
	}

	public void onOK() {
		final SysAlerte alt = BeanLocator.defaultLookup(AlertesSession.class)
				.findById(alerteId);

		if (rdSemaine.isChecked()) {
			alt.setPeriode(7);
		} else if (rdJour.isChecked()) {
			alt.setPeriode(1);
		} else if (StringOperation.isInt(txtPeriode.getValue())) {
			alt.setPeriode(Integer.parseInt(txtPeriode.getValue()));
		} else {
			alt.setPeriode(0);
		}

		if (rdNotification.isChecked()) {
			alt.setNotification((short) 1);
		}

		BeanLocator.defaultLookup(AlertesSession.class).createAlerte(alt);
		BeanLocator.defaultLookup(AlertesSession.class).deleteAlerteprofil(
				alt.getId());
		BeanLocator.defaultLookup(AlertesSession.class).deleteAlerteuser(
				alt.getId());

		for (int i = 0; i < lstProfil.getSelectedCount(); i++) {
			final SysAlerteprofil apf = new SysAlerteprofil();
			apf.setSysAlerte(alt);
			final SysProfil pf = BeanLocator
					.defaultLookup(ProfilsSession.class)
					.findByCode(
							((Listitem) lstProfil.getSelectedItems().toArray()[i])
									.getValue().toString());
			apf.setSysProfil(pf);
			BeanLocator.defaultLookup(AlertesSession.class).createAlerteprofil(
					apf);
		}

		for (int i = 0; i < lstUserConcerne.getItemCount(); i++) {
			SysUtilisateuralerte usa = BeanLocator.defaultLookup(
					AlertesSession.class).findUtilisateurAlerte(
					Long.parseLong(lstUserConcerne.getItemAtIndex(i).getValue()
							.toString()), alerte.getId());
			if (usa == null) {
				usa = new SysUtilisateuralerte();
			}
			usa.setSysAlerte(alt);
			final Utilisateur usr = BeanLocator.defaultLookup(
					AlertesSession.class).findUserById(
					Long.parseLong(lstUserConcerne.getItemAtIndex(i).getValue()
							.toString()));
			usa.setUtilisateur(usr);
			BeanLocator.defaultLookup(AlertesSession.class)
					.createAlerteutilisateur(usa);
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();
	}

	public boolean inUsrList(final Utilisateur user) {
		boolean bool = false;
		for (int i = 0; i < lstUserConcerne.getItemCount(); i++)
			if (lstUserConcerne.getItemAtIndex(i).getValue().toString().equals(
					user.getId().toString())) {
				bool = true;
			}
		return bool;
	}

	public void onUserAdd() {
		final Utilisateur user = BeanLocator
				.defaultLookup(AlertesSession.class).findUserByLogin(
						bd.getValue().toString());
		if ((user != null) && !inUsrList(user)) {
			lstUserConcerne
					.appendItem(user.getLogin(), user.getId().toString());
		}
	}

	public void onUserDelete() {
		if (lstUserConcerne.getSelectedCount() > 0) {
			lstUserConcerne.removeItemAt(lstUserConcerne.getSelectedIndex());
		}

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		final SysProfil profil = (SysProfil) data;
		item.setValue(profil.getPfCode());

		if (alerte != null)
			if (BeanLocator.defaultLookup(AlertesSession.class)
					.isProfilAlerted(profil.getPfCode(), alerte.getId())) {
				item.setSelected(true);
			}

		final Listcell cellLibelle = new Listcell(profil.getPfLibelle());
		cellLibelle.setParent(item);

	}

}