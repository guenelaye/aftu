package sn.ssi.kermel.web.fournituretravaux.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygMembresCommissionsMarches;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersSouscriteresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class VercriteresqualificationController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String nom=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
  	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	SygMembresCommissionsMarches membre=new SygMembresCommissionsMarches();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuValider;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygDossierssouscriteres dossiercritere=new SygDossierssouscriteres();
	List<SygDossierssouscriteres> criteres = new ArrayList<SygDossierssouscriteres>();
	SygPlisouvertures soumissionnaire=new SygPlisouvertures();
	SygPlisouvertures soumissionnaires=new SygPlisouvertures();
	private Label lbltitre;
	private String libelle;
	  
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
    
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier==null)
		{
			menuValider.setDisabled(true);
			
		}
		else
		{
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			Events.postEvent(ApplicationEvents.ON_MEMBRES, this, null);
			soumissionnaires=BeanLocator.defaultLookup(RegistrededepotSession.class).findSoumissionnaire(dossier,-1,  UIConstants.MOINSDISANTQUALIFIE);
				
			
		}
		
	}
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 criteres = BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(activePage, byPage, dossier, null, null);
			 lstListe.setModel(new SimpleListModel(criteres));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(DossiersSouscriteresSession.class).count(dossier, null, null));
			 
			 soumissionnaires=BeanLocator.defaultLookup(RegistrededepotSession.class).findSoumissionnaire(dossier,-1,  UIConstants.MOINSDISANTQUALIFIE);
			 soumissionnaire=BeanLocator.defaultLookup(RegistrededepotSession.class).findSoumissionnaire(dossier, UIConstants.PARENT,  UIConstants.MOINSDISANTQUALIFIE);
				if(soumissionnaire!=null)
					libelle=Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.evaluation.vercriteresqualification.verification")+" "+soumissionnaire.getRetrait().getNomSoumissionnaire();
				else
					libelle=Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.evaluation.vercriteresqualification.aucuncandidat");
				lbltitre.setValue(libelle);
		} 
	
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygDossierssouscriteres criteres = (SygDossierssouscriteres) data;
		item.setValue(criteres);

		 Listcell cellNom= new Listcell(criteres.getCritere().getLibelle());
		 cellNom.setParent(item);
		 
		 Listcell cellConforme= new Listcell("");
		 if(soumissionnaires.getEtatExamenPreliminaire()==UIConstants.PARENT)
		 {
			 cellConforme.setImage("/images/ok.png");
			 cellConforme.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.evaluation.vercriteresqualification.conforme"));
	
		 }
		  cellConforme.setParent(item);
		 
		
		 
		  Listcell cellNonConforme= new Listcell("");
			 if(soumissionnaires.getEtatExamenPreliminaire()!=UIConstants.PARENT)
			 {
				 cellNonConforme.setImage("/images/delete.png");
				 cellNonConforme.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.evaluation.vercriteresqualification.nonconforme"));
		
			 }
			 cellNonConforme.setParent(item);
			 
		  if(soumissionnaires.getCritereQualification()==UIConstants.PARENT)
			 item.setSelected(true);
	}

	
	
	
	public void onClick$menuValider()
	{
     
       
       for (int k = 0; k < criteres.size(); k++) {
    	//   soumissionnaires=plis.get(k);
    	   soumissionnaires.setEtatExamenPreliminaire(UIConstants.NPARENT);
    	   soumissionnaires.setCritereQualification(UIConstants.MOINSDISANTQUALIFIE);
      	   BeanLocator.defaultLookup(RegistrededepotSession.class).update(soumissionnaires);
       }
       if (criteres.size()==lstListe.getSelectedCount()) {
    	 
    	 //  soumissionnaires=(SygPlisouvertures) ((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
    	   soumissionnaires.setEtatExamenPreliminaire(UIConstants.PARENT);
    	   soumissionnaires.setCritereQualification(UIConstants.MOINSDISANTQUALIFIE);
    	   BeanLocator.defaultLookup(RegistrededepotSession.class).update(soumissionnaires);
       }
       session.setAttribute("libelle", "vercriteresqualification");
		loadApplicationState("suivi_fourniture_trav"); 
		
		
	}
}