package sn.ssi.kermel.web.fournituretravaux.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class OuverturesPlisRenderer implements RowRenderer, RowRendererExt {

	private	SygPlisouvertures pli;
	private List<SygPlisouvertures> plisdepot = new ArrayList<SygPlisouvertures>();
	private List<SygPlisouvertures> existenumero = new ArrayList<SygPlisouvertures>();
	private String pourcantage;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygDossiers dossier=new SygDossiers();
	Session session = getHttpSession();
	public static Long CURRENT_CODE;
	public static int CURRENT_NOMBRE;
	private String dtdate,heure="",receptionmode,observations,observationscandidat;
	private int etatvalide,numeroplis;
	
	public void onCreate(CreateEvent createEvent) {
		
		
	
	}
	
	private Session getHttpSession() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Row newRow(Grid grid) {
		// Create EditableRow instead of Row(default)
		Row row = new EditableRow();
		row.applyProperties();
		return row;
	}

	@Override
	public Component newCell(Row row) {
		return null;// Default Cell
	}

	@Override
	public int getControls() {
		return RowRendererExt.DETACH_ON_RENDER; // Default Value
	}

	@Override
	public void render(Row row, Object data, int index) throws Exception {
		final SygRetraitregistredao plis = (SygRetraitregistredao) data;
		
		final EditableRow editRow = (EditableRow) row;
		
		
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).findById(CURRENT_CODE);
		
			
		plisdepot= BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,plis,null,null,-1,-1,1, -1, -1,-1, null,-1, null, null);
		if(plisdepot.size()>0)
		{
		    pli=plisdepot.get(0);
		    dtdate=UtilVue.getInstance().formateLaDate2(pli.getDateDepot());
		    numeroplis=pli.getNumero();
		    if(pli.getHeuredepot()!=null)
		    heure=UtilVue.getInstance().formateLHeure(pli.getHeuredepot());
		    receptionmode=pli.getModereception();
		    observations=pli.getObservationsoffres();
		    observationscandidat=pli.getObservationscandidats();
		    etatvalide=pli.getValide();
		}
		else
		{
			dtdate="";
			//numeroplis="";
			heure="";
		}
		
		final EditableDiv numero = new EditableDiv("", false);
		numero.setParent(editRow);
		
		final EditableDiv raisonsocial = new EditableDiv(plis.getNomSoumissionnaire(), false);
		raisonsocial.txb.setReadonly(true);
		raisonsocial.setParent(editRow);
		
		final EditableDiv modereception = new EditableDiv(receptionmode, false);
		modereception.setParent(editRow);
		
	
		final EditableDiv datedepot = new EditableDiv(dtdate, false);
		datedepot.setParent(editRow);
		
		final EditableDiv heuredepot = new EditableDiv(heure, false);
		heuredepot.setParent(editRow);
		
		final EditableDiv observationsoffres = new EditableDiv(observations, false);
		observationsoffres.setParent(editRow);
		
		
		final EditableDiv candidatobservations = new EditableDiv(observationscandidat, false);
		candidatobservations.setParent(editRow);
		
//		String etat="NON";
//		if(etatvalide==UIConstants.PARENT)
//			etat="OUI";
//		else
//			etat="NON";
//		final EditableCheckDiv etatzonearisque = new EditableCheckDiv(etat,false);
//		etatzonearisque.setAlign("center");
//		etatzonearisque.setStyle("font-weight:bold;color:green");
//		etatzonearisque.setParent(editRow);	
		
		final Div ctrlDiv = new Div();
		ctrlDiv.setParent(editRow);
		final Button editBtn = new Button(null, "/images/disk.png");
		editBtn.setMold("os");
		editBtn.setHeight("20px");
		editBtn.setWidth("30px");
		editBtn.setParent(ctrlDiv);
		
		
		// Button listener - control the editable of row
		editBtn.addEventListener(Events.ON_CLICK, new EventListener() {
			public void onEvent(Event event) throws Exception {
				final Button submitBtn = (Button) new Button(null, "/images/ok.png");
				final Button cancelBtn = (Button) new Button(null, "/images/cancel.png");
				submitBtn.setMold("os");
				submitBtn.setHeight("20px");
				submitBtn.setWidth("30px");
				submitBtn.setTooltiptext("Valider la saisie");
				submitBtn.addEventListener(Events.ON_CLICK, new EventListener() {
					public void onEvent(Event event) throws Exception {

						editRow.toggleEditable(true);
						Date depotdate = ToolKermel.stringToDate(datedepot.txb.getValue());
						Date datedujour	= new Date();
						if(modereception.txb.getValue().equals("")){
							editRow.toggleEditable(true);
							throw new WrongValueException(modereception, "Attention!!Le mode de reception est obligatoire!!");

						}
						if(datedepot.txb.getValue().equals("")){
							editRow.toggleEditable(true);
							throw new WrongValueException(datedepot, "Attention!!Date est obligatoire!!");

						}
						if(!ToolKermel.ControleValiditeDate(datedepot.txb.getValue())){
							editRow.toggleEditable(true);
							throw new WrongValueException(datedepot, "Erreur!!Le format de la date incorrect!!!(jj/mm/aaaa)!!");

						}
						if(depotdate.after(datedujour)){
							editRow.toggleEditable(true);
							throw new WrongValueException(datedepot, "Attention!!La date de d�but doit �tre ant�rieur � la date de fin!!");

						}
						if(heuredepot.txb.getValue().equals("")){
							editRow.toggleEditable(true);
							throw new WrongValueException(heuredepot, "Attention!!Heure est obligatoire!!");

						}
						if(!ToolKermel.ControleValiditeHeure(heuredepot.txb.getValue())){
							editRow.toggleEditable(true);
							throw new WrongValueException(heuredepot, "Erreur!!Le format de l'heure incorrect!!!(mm:ss)!!");

						}
						if(numero.txb.getValue().equals("")){
							editRow.toggleEditable(true);
							throw new WrongValueException(numero, "Attention!!Le num�ro est obligatoire!!");

						}
						if(!ToolKermel.estLong(numero.txb.getValue())){
							editRow.toggleEditable(true);
							throw new WrongValueException(numero, "Attention!!La valeurde l'index doit �tre num�rique!!");

						}
						if(Integer.parseInt(numero.txb.getValue())>CURRENT_NOMBRE)
						{
							editRow.toggleEditable(true);
							throw new WrongValueException(numero, "Attention!!Le num�ro doit �tre inf�rieur ou �gal!!");	
						}
						plisdepot= BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,plis,null,null,-1,-1,1, -1, -1,-1, null,-1, null, null);
//						existenumero= BeanLocator.defaultLookup(RegistrededepotSession.class).find(dossier,plis,numero.txb.getValue());
//						if(plisdepot.size()==0)
//						{
//							 if(existenumero.size()>0)
//						     {
//							 editRow.toggleEditable(true);
//							 throw new WrongValueException(numero, "Attention!!Le num�ro saisi existe d�ja!!"); 
//						     }
//						}
//						else
//						{
//							 if(!plisdepot.get(0).getNumero().equalsIgnoreCase(numero.txb.getValue()))
//							 {
//								 if(existenumero.size()>0)
//							     {
//								 editRow.toggleEditable(true);
//								 throw new WrongValueException(numero, "Attention!!Le num�ro saisi existe d�ja!!"); 
//							     }
//							 }
//						}
						if(plisdepot.size()==0)
						{
						    pli= new SygPlisouvertures();
							pli.setDossier(dossier);
							pli.setRetrait(plis);
							pli.setRaisonsociale(plis.getNomSoumissionnaire());
						//	pli.setNumero(numero.txb.getValue());
							pli.setDateDepot(ToolKermel.stringToDate(datedepot.txb.getValue()));
							pli.setModereception(modereception.txb.getValue());
							pli.setHeuredepot(ToolKermel.stringToHeure(heuredepot.txb.getValue()));
							pli.setObservationsoffres(observationsoffres.txb.getValue());
							pli.setObservationscandidats(candidatobservations.txb.getValue());
							BeanLocator.defaultLookup(RegistrededepotSession.class).save(pli);
						}
						else
						{
							pli=plisdepot.get(0);
						    pli.setDateDepot(ToolKermel.stringToDate(datedepot.txb.getValue()));
							//pli.setNumero(numero.txb.getValue());
							pli.setModereception(modereception.txb.getValue());
							pli.setHeuredepot(ToolKermel.stringToHeure(heuredepot.txb.getValue()));
							pli.setObservationsoffres(observationsoffres.txb.getValue());
							pli.setObservationscandidats(candidatobservations.txb.getValue());
							BeanLocator.defaultLookup(RegistrededepotSession.class).update(pli);
						}
						
						submitBtn.detach();
						cancelBtn.detach();
						editBtn.setParent(ctrlDiv);
					}
				});
			
				cancelBtn.setMold("os");
				cancelBtn.setHeight("20px");
				cancelBtn.setWidth("30px");
				cancelBtn.setTooltiptext("Annuler la saisie");
				cancelBtn.addEventListener(Events.ON_CLICK, new EventListener() {
					public void onEvent(Event event) throws Exception {
						editRow.toggleEditable(false);
						submitBtn.detach();
						cancelBtn.detach();
						editBtn.setParent(ctrlDiv);
					}
				});
				submitBtn.setParent(ctrlDiv);
				cancelBtn.setParent(ctrlDiv);
				editRow.toggleEditable(true);
				editBtn.detach();
			}
		});

	}
	
}