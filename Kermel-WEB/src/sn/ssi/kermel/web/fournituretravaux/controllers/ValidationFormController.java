package sn.ssi.kermel.web.fournituretravaux.controllers;


import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygHistoriqueappeloffresAC;
import sn.ssi.kermel.be.passationsmarches.ejb.DocumentsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.HistoriqueappeloffresACSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;


@SuppressWarnings("serial")
public class ValidationFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	public static final String PARAM_WINDOW_ATTRIBUTION = "ATTRIBUTION";
	private String login,nomFichier,nature;
	private Long code;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtCommentaires,txtVersionElectronique;
	private Label lbStatusBar,lbltitre;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
    private Label lblDateCreation,lblDateMiseenvalidation;
    Session session = getHttpSession();
    private Datebox dtdate;
    private final String cheminDossier = UIConstants.PATH_PJ;
    UtilVue utilVue = UtilVue.getInstance();
    private Radio rdValider,rdRejeter;
    private	SygDossiers dossier=new SygDossiers();
    SygHistoriqueappeloffresAC historique=new SygHistoriqueappeloffresAC();
    private int attribution;
    SygDocuments document=new SygDocuments();
    
	@Override
	public void onEvent(Event event) throws Exception {


	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		code = (Long) map.get(PARAM_WINDOW_CODE);
		nature=(String) map.get(PARAM_WINDOW_MODE);
		attribution=(Integer) map.get(PARAM_WINDOW_ATTRIBUTION);
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).findById(code);
		lblDateCreation.setValue(utilVue.formateLaDate(dossier.getDosDateCreation()));
		lblDateMiseenvalidation.setValue(utilVue.formateLaDate(dossier.getDosDateMiseValidation()));
		historique=BeanLocator.defaultLookup(HistoriqueappeloffresACSession.class).getHistorique(dossier, attribution);
				
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
		  	
			
			dossier.setDosCommentaireValidation(txtCommentaires.getValue());
			dossier.setDosFichierValidation(txtVersionElectronique.getValue());
			
			if(rdValider.isSelected()==true)
			  {
				if(nature.equals("validationdossier"))
				{
					dossier.setDosDateValidation(dtdate.getValue());
				}
				else
				{
					if(nature.equals("validationexamenjuridique"))
					{
						dossier.setDosDateValidationSignature(dtdate.getValue());
					}
					else
					{
						dossier.setDosDateValidationPrequalif(dtdate.getValue());
					}
				}
				 dossier.setDosEtatValidation(UIConstants.DOSSIERVALIDEE);
				 historique.setHisto_validation("1");
				 document.setLibelle(Labels.getLabel("kermel.plansdepassation.proceduresmarches.documents.validationdao"));
			  }
			else
			 {
				if(rdRejeter.isSelected()==true)
				  {
					dossier.setDosDateRejet(dtdate.getValue());
					 dossier.setDosEtatValidation(UIConstants.DOSSIERAVALIDER);
					 if(nature.equals("validationdossier"))
						{
							dossier.setDosDateMiseValidation(null);
						}
						else
						{
							 if(nature.equals("validationexamenjuridique"))
								{
									dossier.setDosDateMiseValidationSignature(null);
								}
								else
								{
									dossier.setDosDateMiseValidationattribution(null);
								}
						}
					
					 historique.setHisto_validation("2");
					 document.setLibelle(Labels.getLabel("kermel.plansdepassation.proceduresmarches.documents.rejetdao"));
				  }
				else
				{
					
					 if(nature.equals("validationdossier"))
						{
							dossier.setDosDateValidation(dtdate.getValue());
						}
						else
						{
							 if(nature.equals("validationexamenjuridique"))
								{
									dossier.setDosDateValidationSignature(dtdate.getValue());
								}
								else
								{
									dossier.setDosDateValidationPrequalif(dtdate.getValue());
								}
						}
					dossier.setDosEtatValidation(UIConstants.DOSSIERSOUSRESERVE);
					historique.setHisto_validation("3");
					document.setLibelle(Labels.getLabel("kermel.plansdepassation.proceduresmarches.documents.validationdaosr"));
				}
				
			 }
			BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).update(dossier);
				
			historique.setHisto_fichiervalidation(txtVersionElectronique.getValue());
			historique.setHisto_commentaire(txtCommentaires.getValue());
			historique.setHisto_datevalidation(dtdate.getValue());
		
			BeanLocator.defaultLookup(HistoriqueappeloffresACSession.class).update(historique);
		
			document.setAppel(dossier.getAppel());
			if(!txtVersionElectronique.getValue().equals(""))
			document.setNomFichier(txtVersionElectronique.getValue());
			document.setTypeDocument(UIConstants.PARAM_TYPEDOCUMENTS_VALIDATIONDAO);
			document.setDossier(dossier);
			document.setDate(new Date());
			document.setHeure(Calendar.getInstance().getTime());
			BeanLocator.defaultLookup(DocumentsSession.class).save(document);
			
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			if(dtdate.getValue()==null)
		     {
              errorComponent = dtdate;
              errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.dossiers.datevalidation")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((dtdate.getValue()).after(new Date() ))
			 {
				errorComponent = dtdate;
				errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.dossiers.datevalidation")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.referentiel.date.dujour")+": "+UtilVue.getInstance().formateLaDate(new Date());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			if(rdRejeter.isSelected()==true)
			{
				if(txtCommentaires.getValue().equals(""))
			     {
	              errorComponent = txtCommentaires;
	              errorMsg = Labels.getLabel("kermel.common.commentaires")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
					lbStatusBar.setStyle(ERROR_MSG_STYLE);
					lbStatusBar.setValue(errorMsg);
					throw new WrongValueException (errorComponent, errorMsg);
			     }
			}
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
 
public void onCheck$validation()
{
	if(rdRejeter.isSelected()==true)
	{
		lbltitre.setVisible(true);
		
	}
	else 
	{
		lbltitre.setVisible(false);
	}
}
	
	public void onClick$btnChoixFichier() {
		//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
			if (ToolKermel.isWindows())
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectronique.setValue(nomFichier);
		}
}
