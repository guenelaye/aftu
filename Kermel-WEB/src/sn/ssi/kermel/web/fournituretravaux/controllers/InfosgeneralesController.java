package sn.ssi.kermel.web.fournituretravaux.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timebox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygNatureprix;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DocumentsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.LotsSession;
import sn.ssi.kermel.be.referentiel.ejb.NatureprixSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class InfosgeneralesController extends AbstractWindow implements
		EventListener, AfterCompose {

	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle,txtVersionElectronique;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
    private String nomFichier="";
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherNature,txtLieu,txtLieudepot;
    SygBailleurs bailleur=null;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygDossiers dossier=new SygDossiers();
	SygDossiers dossiers=null;
	SygRealisations realisation=new SygRealisations();
	private Textbox txtnumero,txtCodeNature;
	private Intbox intallotssement;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	private Combobox cblot;
	SygNatureprix natureprix=null;
	private Paging pgNature;
	private Listbox lstNature;
	private String libellenature=null;
	private Bandbox bdNature;
	private Decimalbox dcmontantdao;//intmontant,
	private Datebox dtouverture,dtdepot;
	private Timebox heureouverture,heuredepot;
	private Combobox cbcible;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private Image image;
	private Div step0,step1;
	private Iframe idIframe;
	private String extension,images;
	private Menuitem menuValider;
	SygDocuments document=new SygDocuments();
	List<SygDocuments> documents = new ArrayList<SygDocuments>();
	List<SygLots> lots = new ArrayList<SygLots>();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_NATURES, this);
		pgNature.setPageSize(byPage);
		pgNature.addForward("onPaging", this, ApplicationEvents.ON_NATURES);
		lstNature.setItemRenderer(new NaturesRenderer());
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		txtnumero.setValue(appel.getAporeference());
		realisation=appel.getRealisation();
		intallotssement.setValue(1);
		dossiers=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		InfosDossiers(dossiers);
		Events.postEvent(ApplicationEvents.ON_NATURES, this, null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_NATURES)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgNature.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgNature.getActivePage() * byPageBandbox;
				pgNature.setPageSize(byPageBandbox);
			}
			List<SygNatureprix> natures = BeanLocator.defaultLookup(NatureprixSession.class).find(activePage, byPageBandbox,null,libellenature);
			lstNature.setModel(new SimpleListModel(natures));
			pgNature.setTotalSize(BeanLocator.defaultLookup(NatureprixSession.class).count(null,libellenature));
		}

	}

	
	
	
	private boolean checkFieldConstraints() {
		
		try {
		
			if(txtnumero.getValue().equals(""))
		     {
               errorComponent = txtnumero;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.avisnumero")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(intallotssement.getValue()==null)
		     {
              errorComponent = intallotssement;
              errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.allotissement")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(intallotssement.getValue().intValue()==0)
		     {
               errorComponent = intallotssement;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.allotissement.nombre");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			   throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dossiers!=null)
			{
				if(intallotssement.getValue().intValue()<lots.size())
				{
				   errorComponent = intallotssement;
		           errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.allotissement.diminuer");
				   lbStatusBar.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar.setValue(errorMsg);
				   throw new WrongValueException (errorComponent, errorMsg);
				}
			}
			if(cblot.getValue().equals(""))
		     {
             errorComponent = cblot;
             errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.lotsstilsdivisibles")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		
			if(bdNature.getValue().equals(""))
		     {
            errorComponent = bdNature;
            errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.natureprix")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(cbcible.getValue().equals(""))
		     {
           errorComponent = cbcible;
           errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.cible")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(nomFichier.equals(""))
		     {
          errorComponent = txtVersionElectronique;
          errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.dossierappeloffres")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
//			if(intmontant.getValue()==null)
//		     {
//         errorComponent = intmontant;
//         errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.montantgarantie")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
//			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
//			   lbStatusBar.setValue(errorMsg);
//			  throw new WrongValueException (errorComponent, errorMsg);
//		     }
		
			if(dcmontantdao.getValue()==null)
		     {
       errorComponent = dcmontantdao;
       errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.prixdao")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtLieudepot.getValue().equals(""))
		     {
      errorComponent = txtLieudepot;
      errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.infosgenerales.saisietermereference.lieudepot")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dtdepot.getValue()==null)
		     {
      errorComponent = dtdepot;
      errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.infosgenerales.saisietermereference.datelimitedepot")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
//			if((dossiersmi.getDosDateLimiteDepot()).after(dtdepot.getValue()))
//			 {
//				errorComponent = dtdepot;
//				errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.infosgenerales.saisietermereference.datelimitedepot")+" "+Labels.getLabel("kermel.referentiel.date.posterieure")+": "+UtilVue.getInstance().formateLaDate(dossiersmi.getDosDateLimiteDepot());
//				lbStatusBar.setStyle(ERROR_MSG_STYLE);
//				lbStatusBar.setValue(errorMsg);
//				throw new WrongValueException (errorComponent, errorMsg);
//			  }
			if(heuredepot.getValue()==null)
		     {
      errorComponent = heuredepot;
      errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.infosgenerales.saisietermereference.heurelimitedepot")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtLieu.getValue().equals(""))
		     {
       errorComponent = txtLieu;
       errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.lieuouvertureplis")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dtouverture.getValue()==null)
		     {
       errorComponent = dtouverture;
       errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.dateouvertureplis")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((dtdepot.getValue()).after(dtouverture.getValue()))
			 {
				errorComponent = dtdepot;
				errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.dateouvertureplis")+" "+Labels.getLabel("kermel.referentiel.date.posterieure")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.infosgenerales.saisietermereference.datelimitedepot")+": "+UtilVue.getInstance().formateLaDate(dtdepot.getValue());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			if(heureouverture.getValue()==null)
		     {
       errorComponent = heureouverture;
       errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.heureouvertureplis")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	
	public void onClick$btnChoixFichier() {
		//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
			if (ToolKermel.isWindows())
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectronique.setValue(nomFichier);
		}
	
		///////////Nature///////// 
		public void onSelect$lstNature(){
			natureprix= (SygNatureprix) lstNature.getSelectedItem().getValue();
			bdNature.setValue(natureprix.getNatLibelle());
			txtCodeNature.setValue(natureprix.getNatCode());
			bdNature.close();
		
		}
		
		public class NaturesRenderer implements ListitemRenderer{
		
		
		
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygNatureprix nature = (SygNatureprix) data;
			item.setValue(nature);
			
			
			Listcell cellCode = new Listcell(nature.getNatCode());
			cellCode.setParent(item);
			
			Listcell cellLibelle = new Listcell(nature.getNatLibelle());
			cellLibelle.setParent(item);
		
		}
		}
		public void onFocus$txtRechercherNature(){
		if(txtRechercherNature.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.natureprix"))){
			txtRechercherNature.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherNature(){
		if(txtRechercherNature.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.natureprix")) || txtRechercherNature.getValue().equals("")){
			libellenature = null;
			page=null;
		}else{
			libellenature = txtRechercherNature.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_NATURES, this, page);
		}

		public void onOK$txtCodeNature() {
			 List<SygNatureprix> natures = BeanLocator.defaultLookup(NatureprixSession.class).find(0, -1,txtCodeNature.getValue(),null);
					
				if (natures.size() > 0) {
					natureprix=natures.get(0);
					txtCodeNature.setValue(natureprix.getNatCode());
					bdNature.setValue(natureprix.getNatLibelle());
				}
				else {
					bdNature.setValue(null);
					bdNature.open();
					txtCodeNature.setFocus(true);
				}
				
			}
		
		public void onClick$menuValider() {
			if(checkFieldConstraints())
			{
				dossier.setDosReference(txtnumero.getValue());
				dossier.setDosNombreLots(intallotssement.getValue());
				dossier.setDosLotDivisible(cblot.getValue());
				dossier.setNatureprix(natureprix);
				dossier.setDosmontantgarantie(new BigDecimal(0));
				dossier.setDosmontantdao(dcmontantdao.getValue());
				dossier.setDosmontant(appel.getApomontantestime());
				dossier.setDosDateCreation(new Date());
				dossier.setDosLieuOuvertureDesPlis(txtLieu.getValue());
				dossier.setDosDateOuvertueDesplis(dtouverture.getValue());
				dossier.setDosHeureOuvertureDesPlis(heureouverture.getValue());
				dossier.setDosLieuDepotDossier(txtLieudepot.getValue());
				dossier.setDosDateLimiteDepot(dtdepot.getValue());
				dossier.setDosHeurelimitedepot(heuredepot.getValue());
				if(!txtVersionElectronique.getValue().equals(""))
				dossier.setDosFichier(nomFichier);
				dossier.setAppel(appel);
				dossier.setRealisation(realisation);
				dossier.setAutorite(autorite);
				
				document.setAppel(appel);
				if(!txtVersionElectronique.getValue().equals(""))
				document.setNomFichier(txtVersionElectronique.getValue());
				document.setTypeDocument(UIConstants.PARAM_TYPEDOCUMENTS_AJOUTDOSSIERS);
				document.setLibelle(Labels.getLabel("kermel.plansdepassation.proceduresmarches.documents.ajoudossier"));
				document.setDate(new Date());
				document.setHeure(Calendar.getInstance().getTime());
				if(dossiers==null)
				{
					dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).save(dossier);
					document.setDossier(dossier);
					BeanLocator.defaultLookup(DocumentsSession.class).save(document);
				}
				else
				{
					BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).update(dossier);
					BeanLocator.defaultLookup(DocumentsSession.class).update(document);
				}
					
				
				
				session.setAttribute("libelle", "infogenerales");
				loadApplicationState("suivi_fourniture_trav");
				
				
				
			}
		}
		public void InfosDossiers(SygDossiers dossiers) {
			if(dossiers!=null)
			{
				if(dossiers.getDosDateMiseValidation()!=null||dossiers.getDosDatePublication()!=null)
				menuValider.setDisabled(true);
				dossier=dossiers;
				txtnumero.setValue(dossier.getDosReference());
				intallotssement.setValue(dossier.getDosNombreLots());
				cblot.setValue(dossier.getDosLotDivisible());
				natureprix=dossier.getNatureprix();
				txtCodeNature.setValue(natureprix.getNatCode());
				bdNature.setValue(natureprix.getNatLibelle());
			    dcmontantdao.setValue(dossier.getDosmontantdao());
					
				txtLieu.setValue(dossier.getDosLieuOuvertureDesPlis());
				dtouverture.setValue(dossier.getDosDateOuvertueDesplis());
				heureouverture.setValue(dossier.getDosHeureOuvertureDesPlis());
				extension=dossier.getDosFichier().substring(dossier.getDosFichier().length()-3,  dossier.getDosFichier().length());
				 if(extension.equalsIgnoreCase("pdf"))
					 images="/images/icone_pdf.png";
				 else  
					 images="/images/word.jpg";
				 
				image.setVisible(true);
				image.setSrc(images);
				nomFichier=dossier.getDosFichier();
				
				documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossier, appel,UIConstants.PARAM_TYPEDOCUMENTS_AJOUTDOSSIERS,null, null);
				if(documents.size()>0)
				document=documents.get(0);
				
				lots=BeanLocator.defaultLookup(LotsSession.class).find(0,-1,dossier,null);
				
				txtLieudepot.setValue(dossiers.getDosLieuDepotDossier());
				if(dossiers.getDosDateLimiteDepot()!=null)
					dtdepot.setValue(dossiers.getDosDateLimiteDepot());
				if(dossiers.getDosHeurelimitedepot()!=null)
					heuredepot.setValue(dossiers.getDosHeurelimitedepot());
			}
		
		}
		public void onClick$image() {
			step0.setVisible(false);
			step1.setVisible(true);
			String filepath = cheminDossier +  dossier.getDosFichier();
			File f = new File(filepath.replaceAll("\\\\", "/"));

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(f, null, null);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (mymedia != null)
				idIframe.setContent(mymedia);
			else
				idIframe.setSrc("");

			idIframe.setHeight("600px");
			idIframe.setWidth("100%");
		}
		
		public org.zkoss.util.media.AMedia fetchFile(File file) {

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(file, null, null);
				return mymedia;
			} catch (Exception e) {

				e.printStackTrace();
				return null;
			}

		}
		public void onClick$menuFermer() {
			step0.setVisible(true);
			step1.setVisible(false);
		}
}