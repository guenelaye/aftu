package sn.ssi.kermel.web.fournituretravaux.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygMonnaieoffre;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygRecours;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RecoursSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class RecoursController extends AbstractWindow implements
EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	public Textbox txtLibelle;
	String libellemonnaie=null,page=null,login,codesuppression,libellesuppression;
	Session session = getHttpSession();
	List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
	SygSecteursactivites categorie=null;
	private final int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private Textbox txtRechercherMonnaie;
	SygBailleurs bailleur=null;
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idrecours=null;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	SygRecours recours=new SygRecours();
	SygMonnaieoffre monnaie=new SygMonnaieoffre();
	private Textbox txtRefrecours,txtRefappel,txtNom,txtObjet;
	private Div step0,step1;
	private Menuitem menuAjouter,menuModifier,menuSupprimer;
	private Datebox dtexpiration,dtreception;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);



	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null) {
			autorite=infoscompte.getAutorite();
		} else {
			autorite=null;
		}
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		txtRefappel.setValue(appel.getAporeference());
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier==null)
		{
			//menuAjouter.setDisabled(true);
			//menuModifier.setDisabled(true);
			//menuSupprimer.setDisabled(true);
		}
		else
		{
			if(dossier.getDosDatePublicationDefinitive()==null)
			{
				menuAjouter.setDisabled(true);
				menuModifier.setDisabled(true);
				menuSupprimer.setDisabled(true);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			Events.postEvent(ApplicationEvents.ON_MONNAIES, this, null);
		}


	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			List<SygRecours> recours = BeanLocator.defaultLookup(RecoursSession.class).find(activePage,byPageBandbox,dossier,null,null,null,null,null,null);
			lstListe.setModel(new SimpleListModel(recours));
			pgPagination.setTotalSize(BeanLocator.defaultLookup(RecoursSession.class).count(dossier,null,null,null));
		} 

		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = ((SygRecours)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue()).getId();
				BeanLocator.defaultLookup(RecoursSession.class).delete(codes);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}

	}


	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {

		SygRecours recours = (SygRecours) data;
		item.setValue(recours);

		Listcell cellRefrecours = new Listcell(recours.getRefrecours());
		cellRefrecours.setParent(item);

		Listcell cellRefappel = new Listcell(recours.getRefAoffre());
		cellRefappel.setParent(item);

		Listcell cellNom = new Listcell(recours.getNomSoumissionnaire());
		cellNom.setParent(item);

		Listcell cellObjet = new Listcell(recours.getObjetRecours());
		cellObjet.setParent(item);

		Listcell cellDateexpiration= new Listcell(UtilVue.getInstance().formateLaDate(recours.getDateexpiration()));
		cellDateexpiration.setParent(item);

		Listcell cellDatereception = new Listcell(UtilVue.getInstance().formateLaDate(recours.getDatereception()));
		cellDatereception.setParent(item);
	}

	public void onClick$menuSupprimer()
	{
		if (lstListe.getSelectedItem() == null) {
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		}


		HashMap<String, String> display = new HashMap<String, String>(); // permet
		display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
		display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
		display.put(MessageBoxController.DSP_HEIGHT, "250px");
		display.put(MessageBoxController.DSP_WIDTH, "47%");

		HashMap<String, Object> map = new HashMap<String, Object>(); // permet
		map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
		showMessageBox(display, map);

	}
	public void onClick$menuModifier()
	{
		if (lstListe.getSelectedItem() == null) {
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		}
		recours=(SygRecours) lstListe.getSelectedItem().getValue();
		idrecours=recours.getId();
		txtRefrecours.setValue(recours.getRefrecours());
		txtNom.setValue(recours.getNomSoumissionnaire());
		txtObjet.setValue(recours.getObjetRecours());
		dtexpiration.setValue(recours.getDateexpiration());
		dtreception.setValue(recours.getDatereception());
		step0.setVisible(false);
		step1.setVisible(true);
	}

	///////////Monnaies///////// 


	private boolean checkFieldConstraints() {

		try {


			if(txtRefrecours.getValue().equals(""))
			{
				errorComponent = txtRefrecours;
				errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.recours.refrecours")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			}
			if(txtRefappel.getValue().equals(""))
			{
				errorComponent = txtRefappel;
				errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.recours.refappel")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			}
			if(txtNom.getValue().equals(""))
			{
				errorComponent = txtNom;
				errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.recours.nom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			}
			if(txtObjet.getValue().equals(""))
			{
				errorComponent = txtObjet;
				errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.recours.objet")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			}
			if(dtexpiration.getValue()==null)
			{
				errorComponent = dtexpiration;
				errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.recours.dateexpedition")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			}
			if(dtreception.getValue()==null)
			{
				errorComponent = dtreception;
				errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.recours.datereception")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			}
			return true;

		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
					+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;


		}

	}
	public void  onClick$menuFermer(){
		step0.setVisible(true);
		step1.setVisible(false);
	}
	public void  onClick$menuAjouter(){
		step0.setVisible(false);
		step1.setVisible(true);
		idrecours=null;

		txtRefrecours.setValue("");
		txtNom.setValue("");
		txtObjet.setValue("");
		dtexpiration.setValue(null);
		dtreception.setValue(null);
	}
	public void  onClick$menuValider(){
		if(checkFieldConstraints())
		{
			recours.setRefrecours(txtRefrecours.getValue());
			recours.setRefAoffre(txtRefappel.getValue());
			recours.setNomSoumissionnaire(txtNom.getValue());
			recours.setObjetRecours(txtObjet.getValue());
			recours.setDateexpiration(dtexpiration.getValue());
			recours.setDatereception(dtreception.getValue());
			recours.setDossier(dossier);
			recours.setAppel(appel);
			recours.setAutorite(autorite);
			if(idrecours==null) {
				BeanLocator.defaultLookup(RecoursSession.class).save(recours);
			} else {
				BeanLocator.defaultLookup(RecoursSession.class).update(recours);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			step0.setVisible(true);
			step1.setVisible(false);

		}
	}
}