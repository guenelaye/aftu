package sn.ssi.kermel.web.fournituretravaux.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Fieldset;
import org.zkoss.zhtml.Legend;
import org.zkoss.zhtml.Table;
import org.zkoss.zhtml.Td;
import org.zkoss.zhtml.Tr;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygHistoriqueappeloffresAC;
import sn.ssi.kermel.be.entity.SygHistoriqueplan;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.plansdepassation.ejb.PlansdepassationSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class HistoriquesValidationPPMController extends AbstractWindow implements
		EventListener, AfterCompose, RowRenderer {


	private Long idbailleur;
	List<SygHistoriqueplan> historiques = new ArrayList<SygHistoriqueplan>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	private String nomFichier;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuValider,menuEditer,menuClose;
	private Datebox dtmiseenvalidation;
	private Grid GridListe;
	private Label label,legend;
	private Fieldset fielsets;
	private Label lblDatecreation;
	private Div step0,step1;
	private Iframe idIframe;
	private Label label2,label3,label4,label5,labeldatemiseenvaleur,labelbordereau,labelcommentaires,Fichier,lbl1,lbl2,lbl3;
	private Button btnChoixFichier;
	private String extension,images;
	SygHistoriqueappeloffresAC historique=new SygHistoriqueappeloffresAC();
	private Long nombrejour;
	SygDocuments document=new SygDocuments();
	private Long idplan;
	private Label lblInfos;
	SygPlansdepassation plan=new SygPlansdepassation();
	public static final String PARAM_WINDOW_CODE = "CODE";
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		GridListe.setRowRenderer(this);
		
		
    	
    
	}


	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		
		idplan=(Long) map.get(PARAM_WINDOW_CODE);
		plan=BeanLocator.defaultLookup(PlansdepassationSession.class).findById(idplan);
		lblInfos.setValue(plan.getNumplan()+" "+Labels.getLabel("kermel.plansdepassation.infos.gestion")+" "+plan.getAnnee());
		lblDatecreation.setValue(utilVue.formateLaDate(plan.getDatecreation()));
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
		 historiques   = BeanLocator.defaultLookup(PlansdepassationSession.class).Historiques(0,-1,plan);
		 GridListe.setModel(new SimpleListModel(historiques));
		
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) {
			Image button = (Image) event.getTarget();
			String nomfichier=(String) button.getAttribute("fichier");
			step0.setVisible(false);
			step1.setVisible(true);
			String filepath = cheminDossier +  nomfichier;
			File f = new File(filepath.replaceAll("\\\\", "/"));

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(f, null, null);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (mymedia != null)
				idIframe.setContent(mymedia);
			else
				idIframe.setSrc("");

			idIframe.setHeight("600px");
			idIframe.setWidth("100%");
		}
	}

	 @Override
		public void render(Row row, Object data, int index) throws Exception {
		 SygHistoriqueplan historiques = (SygHistoriqueplan) data;
		 row.setValue(historiques);
		 
		 Fieldset fieldset=new Fieldset();
		 fieldset.setStyle("width:96%;");
	
		 Legend legend = new Legend();
		 Label label = new Label("Mise en validation");
		 label.setParent(legend);
		 legend.setParent(fieldset);
		 
		 Table table0=new Table();
		 Tr tr0=new Tr();
		 Td tdac=new Td();
		 Td tddt=new Td();
		 Td tddcmp=new Td();
		 
			 /////:::AC
			 Fieldset fieldsetac=new Fieldset();
			 fieldsetac.setStyle("width:400px;");
		
			 Legend legendac = new Legend();
			 Label labelac = new Label("");
			 if(historiques.getUser()!=null)
				 labelac.setValue(historiques.getUser().getPrenom()+" "+historiques.getUser().getNom());
			 else
				 labelac.setValue("(non encore renseign�)");
			 labelac.setParent(legendac);
			 legendac.setParent(fieldsetac);
			 
			 fieldsetac.setParent(tdac);
			 
				 Table tableac=new Table();
				 
				 Tr trac0=new Tr();
				 Td tdac00=new Td();
				 Td tdac01=new Td();
				 Td tdac02=new Td();
				 Label labelac00 = new Label("Propos� pour validation");
				 labelac00.setParent(tdac00);
				 Label labelac01 = new Label(":");
				 labelac01.setParent(tdac01);
				 Label labelac02 = new Label(UtilVue.getInstance().formateLaDate(historiques.getDateMiseEnValidation()));
				 labelac02.setParent(tdac02);
				 tdac00.setParent(trac0);
				 tdac01.setParent(trac0);
				 tdac02.setParent(trac0);
				 trac0.setParent(tableac);
									 
				 Tr trac2=new Tr();
				 Td tdac20=new Td();
				 Td tdac21=new Td();
				 Td tdac22=new Td();
				 Label labelac20 = new Label("Commentaires");
				 labelac20.setParent(tdac20);
				 Label labelac21 = new Label(":");
				 labelac21.setParent(tdac21);
				 Label labelac22 = new Label(historiques.getCommentaireMiseValidation());
				 labelac22.setParent(tdac22);
				 tdac20.setParent(trac2);
				 tdac21.setParent(trac2);
				 tdac22.setParent(trac2);
				 trac2.setParent(tableac);
				 
				 Tr trac3=new Tr();
				 Td tdac30=new Td();
				 Td tdac31=new Td();
				 Td tdac32=new Td();
				 Label labelac30 = new Label("Fichier joint ");
				 labelac30.setParent(tdac30);
				 Label labelac31 = new Label(":");
				 labelac31.setParent(tdac31);
				 Label labelac32 = new Label("");
				 if(historiques.getFichierMiseValidation().equals(""))
				 {
					 labelac32.setValue("(non encore renseign�)");
					 labelac32.setWidth("80%");
					 labelac32.setParent(tdac32);
				 }
				 else
				 {
					 extension=historiques.getFichierMiseValidation().substring(historiques.getFichierMiseValidation().length()-3,  historiques.getFichierMiseValidation().length());
					 if(extension.equalsIgnoreCase("pdf"))
						 images="/images/icone_pdf.png";
					 else  
						 images="/images/word.jpg";
						
					 Image image=new Image();
					 image.setSrc(images);
					 image.setAttribute("fichier", historiques.getFichierMiseValidation());
					 image.setTooltiptext("Visualiser fichier");
					 image.addEventListener(Events.ON_CLICK, HistoriquesValidationPPMController.this);
					 image.setParent(tdac32); 
				 }
				
				 tdac30.setParent(trac3);
				 tdac31.setParent(trac3);
				 tdac32.setParent(trac3);
				 trac3.setParent(tableac);
				 
				 tableac.setParent(fieldsetac);
				 
			 tdac.setParent(tr0);
		 
			 /////////DT////////
			 Fieldset fieldsetdelai=new Fieldset();
			 fieldsetdelai.setStyle("width:20%;align:center;");
			 
			 Legend legenddelai = new Legend();
			 Label labeldelai = new Label("D�lai de traitement ");
			 labeldelai.setParent(legenddelai);
			 legenddelai.setParent(fieldsetdelai);            
			 
			 fieldsetdelai.setParent(tddt);      
			 tddt.setStyle("align:center;");
			 
			 Label duree = new Label(" ");
			 if(historiques.getDateRejet()!=null)
			 {
				 nombrejour=ToolKermel.DifferenceDate(historiques.getDateMiseEnValidation(), historiques.getDateRejet(), "jj");
				 duree.setValue(nombrejour+1+" Jour(s)");
			 }
			 else
				 duree.setValue("");
			 
			 duree.setParent(fieldsetdelai);
			 tddt.setParent(tr0);
		 
		 
	          /////////DCMP////////
			 Fieldset fieldsetdcmp=new Fieldset();
			 fieldsetdcmp.setStyle("width:400px;");
			 
			 Legend legenddcmp = new Legend();
			 Label labeldcmp = new Label("DCMP");
			 labeldcmp.setParent(legenddcmp);
			 legenddcmp.setParent(fieldsetdcmp);
			 
			 
				 Table tabledcmp=new Table();
				 
		           
				 Tr trdcmp2=new Tr();
				 Td tddcmp20=new Td();
				 Td tddcmp21=new Td();
				 Td tddcmp22=new Td();
				 Label labeldcmp20 = new Label("Validation");
				 labeldcmp20.setParent(tddcmp20);
				 Label labeldcmp21 = new Label(":");
				 labeldcmp21.setParent(tddcmp21);
				 Label labeldcmp22 = new Label("");
				 if(historiques.getEtat().equals("0"))
				   {
					 labeldcmp22.setValue(Labels.getLabel("kermel.plansdepassation.dossier.nonrenseigne")); 
				  }
				 else
					 if(historiques.getEtat().equals("OUI"))
					   {
						  labeldcmp22.setValue("Valid� le "+UtilVue.getInstance().formateLaDate(historiques.getDateRejet())); 
					   }
					 else
					  {
						 labeldcmp22.setValue("Rejet� le "+UtilVue.getInstance().formateLaDate(historiques.getDateRejet()));  
					  }
						
				 labeldcmp22.setParent(tddcmp22);
				 tddcmp20.setParent(trdcmp2);
				 tddcmp21.setParent(trdcmp2);
				 tddcmp22.setParent(trdcmp2);
				 trdcmp2.setParent(tabledcmp);
				 
				 
				
				
				 /////////:Commentaires
				 Tr trdcmp4=new Tr();
				 Td tddcmp40=new Td();
				 Td tddcmp41=new Td();
				 Td tddcmp42=new Td();
				 Label labeldcmp40 = new Label("Commentaires");
				 Label labeldcmp41 = new Label(":");
				 Label labeldcmp42 = new Label(historiques.getCommentaireRejet());
				 labeldcmp40.setParent(tddcmp40);
				 labeldcmp41.setParent(tddcmp41);
				 labeldcmp42.setParent(tddcmp42);
				 
				 tddcmp40.setParent(trdcmp4);
				 tddcmp41.setParent(trdcmp4);
				 tddcmp42.setParent(trdcmp4);
				 
				 trdcmp4.setParent(tabledcmp);
				 
				 ////////Fichier Joint
				 Tr trdcmp5=new Tr();
				 Td tddcmp50=new Td();
				 Td tddcmp51=new Td();
				 Td tddcmp52=new Td();
				 Label labeldcmp50 = new Label("Fichier joint");
				 Label labeldcmp51 = new Label(":");
				 Label labeldcmp52 = new Label("");
				 labeldcmp52.setParent(tddcmp52);
				 
				 if(historiques.getFichierRejet()==null)
				 {
					 labeldcmp52.setValue("(non encore renseign�)");
					 labeldcmp52.setWidth("80%");
					 labeldcmp52.setParent(tddcmp52);
				 }
				 else
				 {
					 extension=historiques.getFichierRejet().substring(historiques.getFichierRejet().length()-3,  historiques.getFichierRejet().length());
					 if(extension.equalsIgnoreCase("pdf"))
						 images="/images/icone_pdf.png";
					 else  
						 images="/images/word.jpg";
						
					 Image image=new Image();
					 image.setSrc(images);
					 image.setAttribute("fichier", historiques.getFichierRejet());
					 image.setTooltiptext("Visualiser fichier");
					 image.addEventListener(Events.ON_CLICK, HistoriquesValidationPPMController.this);
					 image.setParent(tddcmp52); 
				 }
				 labeldcmp50.setParent(tddcmp50);
				 labeldcmp51.setParent(tddcmp51);
				
				 
				 tddcmp50.setParent(trdcmp5);
				 tddcmp51.setParent(trdcmp5);
				 tddcmp52.setParent(trdcmp5);
				 
				 trdcmp5.setParent(tabledcmp);
				 
				 tabledcmp.setParent(fieldsetdcmp);

			 fieldsetdcmp.setParent(tddcmp);
			 tddcmp.setParent(tr0);
		 ////////////
		 tr0.setParent(table0);
		 table0.setParent(fieldset);
		 fieldset.setParent(row);

	   }
	

		
		public org.zkoss.util.media.AMedia fetchFile(File file) {

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(file, null, null);
				return mymedia;
			} catch (Exception e) {

				e.printStackTrace();
				return null;
			}

		}
		
		public void onClick$menuFermer() {
			step0.setVisible(true);
			step1.setVisible(false);
		}
		
		
		
		public void onClick$menuCancel() {
		   detach();
		}
		
}