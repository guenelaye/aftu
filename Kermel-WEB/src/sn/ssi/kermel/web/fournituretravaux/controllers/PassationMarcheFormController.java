package sn.ssi.kermel.web.fournituretravaux.controllers;


import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;

import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Adama samb
 * @since mardi 11 Janvier 2011, 12:34
 
 */
public class PassationMarcheFormController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private Include pgActes;

	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		


	}

	public void onCreate(CreateEvent createEvent) {
		pgActes.setSrc("/passationsmarches/fournituretravaux/dossierspassation.zul");
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	
}