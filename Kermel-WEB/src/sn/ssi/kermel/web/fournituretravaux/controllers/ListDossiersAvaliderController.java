package sn.ssi.kermel.web.fournituretravaux.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class ListDossiersAvaliderController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CONFIRMVALIDER="CONFIRMVALIDER";
     String page=null;
     Session session = getHttpSession();
    SygDossiers dossier=new SygDossiers();
     String login,naturevalidation;
    private Listbox lstListe;
     private Combobox cbselect;
 	private int etat=0,gestion=-1;
	private Intbox intgestion;
	 
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
		
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		
		lstListe.setItemRenderer(this);
		
			
	}


	public void onCreate(CreateEvent event) {
		
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygDossiers> dossiers = BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).find(activePage,byPage, null, null, null, etat, gestion);
			 SimpleListModel listModel = new SimpleListModel(dossiers);
			 lstListe.setModel(listModel);
			 pgPagination.setTotalSize(BeanLocator.defaultLookup( DossiersAppelsOffresSession.class).count(null, null, null, etat, gestion));
		} 
		else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
			Listcell button = (Listcell) event.getTarget();
			String toDo = (String) button.getAttribute(UIConstants.TODO);
			String nature = (String) button.getAttribute("naturevalidation");
			dossier=(SygDossiers) button.getAttribute("dossiers");
			if (toDo.equalsIgnoreCase("valider"))
			{
			final String uri = "/passationsmarches/fournituretravaux/formvalidation.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"650px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.soumettre.validation"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ValidationFormController.PARAM_WINDOW_CODE, dossier.getDosID());
			data.put(ValidationFormController.PARAM_WINDOW_MODE, nature);

			showPopupWindow(uri, data, display);
			}
			
		}
//		else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
//			Listcell button = (Listcell) event.getTarget();
//			String toDo = (String) button.getAttribute(UIConstants.TODO);
//			dossier=(SygDossiers) button.getAttribute("dossiers");
//			if (toDo.equalsIgnoreCase("valider"))
//			{
//				HashMap<String, String> display = new HashMap<String, String>(); // permet
//				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.valider"));
//				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.valider"));
//				display.put(MessageBoxController.DSP_HEIGHT, "250px");
//				display.put(MessageBoxController.DSP_WIDTH, "47%");
//
//				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
//				map.put(CONFIRMVALIDER, "Valider_Dossier");
//				showMessageBox(display, map);
//			}
//			if (toDo.equalsIgnoreCase("rejeter"))
//			{
//				HashMap<String, String> display = new HashMap<String, String>(); // permet
//				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.rejeter"));
//				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.rejeter"));
//				display.put(MessageBoxController.DSP_HEIGHT, "250px");
//				display.put(MessageBoxController.DSP_WIDTH, "47%");
//
//				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
//				map.put(CONFIRMVALIDER, "Rejeter_Dossier");
//				showMessageBox(display, map);
//			}
//		}
//		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
//			 String confirmer = (String) ((HashMap<String, Object>) event.getData()).get(CONFIRMVALIDER);
//			 if (confirmer != null && confirmer.equalsIgnoreCase("Valider_Dossier")) 
//			 {
//				 dossier.setDosEtatValidation(UIConstants.DOSSIERVALIDEE);
//				 dossier.setDosDateValidation(new Date());
//			 }
//			 if (confirmer != null && confirmer.equalsIgnoreCase("Rejeter_Dossier")) 
//			 {
//				 dossier.setDosEtatValidation(UIConstants.DOSSIERREJETEE);
//				 dossier.setDosDateValidation(new Date());
//			 }
//			 BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).update(dossier);
//			 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
//		}
//		
	}

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygDossiers dossiers = (SygDossiers) data;
		item.setValue(dossiers.getDosID());
		
		Listcell cellAutorite = new Listcell(dossiers.getAutorite().getDenomination());
		cellAutorite.setParent(item);
		 
		Listcell cellReference = new Listcell(dossiers.getAppel().getAporeference());
		cellReference.setParent(item);
		 
		Listcell cellObjet = new Listcell(dossiers.getAppel().getApoobjet());
		cellObjet.setParent(item);
		 
		Listcell cellModepassation = new Listcell(dossiers.getAppel().getModepassation().getLibelle());
		cellModepassation.setParent(item);
		 
		
		Listcell cellMontant = new Listcell(ToolKermel.format2Decimal(dossiers.getAppel().getApomontantestime()));
		cellMontant.setParent(item);

	
//		Listcell cellImageValider = new Listcell();
//		if((dossiers.getDosDateMiseValidation()!=null&&dossiers.getDosDateValidation()==null)
//				||(dossiers.getDosDateMiseValidationattribution()!=null&&dossiers.getDosDateValidationPrequalif()==null))
//		{
//			if(dossiers.getDosDateValidation()==null)
//				naturevalidation="validationdossier";
//			else if(dossiers.getDosDateValidationPrequalif()==null)
//				naturevalidation="validationattribution";
//			cellImageValider.setImage("/images/ok.png");
//			cellImageValider.setAttribute(UIConstants.TODO, "valider");
//			cellImageValider.setAttribute("naturevalidation", naturevalidation);
//			cellImageValider.setAttribute("dossiers", dossiers);
//			cellImageValider.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validerdossier"));
//			cellImageValider.addEventListener(Events.ON_CLICK, ListDossiersAvaliderController.this);
//    	}
//		cellImageValider.setParent(item);
//		Listcell cellImageRejeter = new Listcell();
//		if(dossiers.getDosEtatValidation()==UIConstants.DOSSIERVALIDEE||dossiers.getDosEtatValidation()==UIConstants.DOSSIERAVALIDER)
//		{
//			cellImageRejeter.setImage("/images/delete.png");
//			cellImageRejeter.setAttribute(UIConstants.TODO, "rejeter");
//			cellImageRejeter.setAttribute("dossiers", dossiers);
//			cellImageRejeter.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.rejeterdossier"));
//			cellImageRejeter.addEventListener(Events.ON_CLICK, ListDossiersAvaliderController.this);
//		}	
//		cellImageRejeter.setParent(item);
	}
	

	public void onSelect$cbselect(){
		
		 if(cbselect.getSelectedItem().getId().equals("dossiersavalides"))
		 {
			 etat=UIConstants.DOSSIERAVALIDER;
		 }
		 else
		 {
			
				 if(cbselect.getSelectedItem().getId().equals("dossiersvalides"))
				 {
					 etat=UIConstants.DOSSIERVALIDEE;
				 }
				 else
				 {
					 if(cbselect.getSelectedItem().getId().equals("dossiersrejetes"))
					 {
						 etat=UIConstants.DOSSIERREJETEE;
					 }
					 else
					 {
						 etat=-1;
						
					 } 
					
				 } 
			 
		 }
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	public void  onClick$btnchercher(){
		
		if(intgestion.getValue()==null)
		 {
			gestion=-1;
			
		 }
		else
		 {
			gestion=intgestion.getValue();
			page="0";
		 }
		
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
	

		public void onOK$txtReference(){
			onClick$btnchercher();	
		}
		public void onOK$bchercher(){
			onClick$btnchercher();	
		}
}