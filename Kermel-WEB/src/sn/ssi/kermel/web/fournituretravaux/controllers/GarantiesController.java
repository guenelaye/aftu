package sn.ssi.kermel.web.fournituretravaux.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygGarantiesDossiers;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.GarantiesSession;
import sn.ssi.kermel.be.referentiel.ejb.PiecesrecusSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class GarantiesController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstBailleur;
	private Paging pgPagination,pgPieces;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference,libellebailleur=null;
    private int parent;
    private Label lbltitre;
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherBailleur,txtChapitre;
    SygBailleurs bailleur=null;
    private Decimalbox dcmontantprevu;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idbailleur;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuAjouter,menuModifier,menuSupprimer;
	private Div step0,step1;
    private Grid GridGaranties;
    private BigDecimal montant=new BigDecimal(0);
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
		addEventListener(ApplicationEvents.ON_PIECES, this);
		pgPieces.setPageSize(byPage);
		pgPieces.addForward("onPaging", this, ApplicationEvents.ON_PIECES);
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier==null)
		{
			menuAjouter.setDisabled(true);
			menuSupprimer.setDisabled(true);
		}
		else
		{
			if(dossier.getDosDateMiseValidation()!=null||dossier.getDosDatePublication()!=null)
			{
				menuAjouter.setDisabled(true);
				menuSupprimer.setDisabled(true);
			}
			
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			
		}
		
	
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			montant=new BigDecimal(0);
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygGarantiesDossiers> garanties = BeanLocator.defaultLookup(GarantiesSession.class).find(activePage,byPageBandbox,dossier,null,null);
			 lstListe.setModel(new SimpleListModel(garanties));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(GarantiesSession.class).count(dossier,null,null));
			 for(int i=0;i<garanties.size();i++)
			 {
				 montant=montant.add(garanties.get(i).getMontant());
			 }
			 dossier.setDosmontantgarantie(montant);
			 BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).update(dossier);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_PIECES)) {
			EditsRowBtnRenderer.CURRENT_CODE=dossier.getDosID();
			GridGaranties.setRowRenderer(new EditsRowBtnRenderer());
			GridGaranties.setModel(new ListModelList(BeanLocator.defaultLookup(PiecesrecusSession.class).find(pgPagination.getActivePage()*byPage, byPage, null, null,UIConstants.GARANTIE)));
			pgPieces.setTotalSize(BeanLocator.defaultLookup(PiecesrecusSession.class).count(null, null,UIConstants.GARANTIE));
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = ((SygGarantiesDossiers)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue()).getId();
				BeanLocator.defaultLookup(GarantiesSession.class).delete(codes);
			}
			session.setAttribute("libelle", "garanties");
			loadApplicationState("suivi_fourniture_trav");
		}
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygGarantiesDossiers pieces = (SygGarantiesDossiers) data;
		item.setValue(pieces);

		 Listcell cellLibelle = new Listcell(pieces.getPiece().getLibelle());
		 cellLibelle.setParent(item);
		 
		 
		 Listcell cellTaux = new Listcell(pieces.getPourcentage().toString());
		 cellTaux.setParent(item);
		 
		 
		 Listcell cellMontant = new Listcell(ToolKermel.format2Decimal(pieces.getMontant()));
		 cellMontant.setParent(item);
	}

	public void onClick$menuAjouter()
	{
		Events.postEvent(ApplicationEvents.ON_PIECES, this, null);
		step0.setVisible(false);
		step1.setVisible(true);
		
	}
	
	public void onClick$menuFermer()
	{
		session.setAttribute("libelle", "garanties");
		loadApplicationState("suivi_fourniture_trav");
	}
	
	public void onClick$menuSupprimer()
	{
		if (lstListe.getSelectedItem() == null)
			
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		

		HashMap<String, String> display = new HashMap<String, String>(); // permet
		display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
		display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
		display.put(MessageBoxController.DSP_HEIGHT, "250px");
		display.put(MessageBoxController.DSP_WIDTH, "47%");

		HashMap<String, Object> map = new HashMap<String, Object>(); // permet
		map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
		showMessageBox(display, map);
		
	}
}