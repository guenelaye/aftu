package sn.ssi.kermel.web.passationsmarches.controllers;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class DossiersApprobationController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code;
	private Tab TAB_infosgenerales,TAB_garanties,TAB_piecesadministratives,TAB_criteresqualifications,TAB_devise,TAB_financement;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblapprobation,souscriptiondumarche,bonsengagement,approbationmarche,notificationmarche,publicationattributiondefinitive;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygDossiers dossier=new SygDossiers();
	private int  nombre=0;
	
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		dossier=(SygDossiers) Executions.getCurrent().getAttribute("dossier");
		if(dossier!=null)
		{   
			
			if(dossier.getDosDateSignature()!=null)
			{
				nombre=nombre+ 1;
				souscriptiondumarche.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				souscriptiondumarche.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.souscritle")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateSignature()));
				
			}
			if(dossier.getDosDateMiseValidationSignature()!=null)
			{
				nombre=nombre+ 1;
				bonsengagement.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				if(dossier.getDosDateValidationSignature()==null)
					bonsengagement.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.examenjuririqueencoursvalidation"));
				else
					bonsengagement.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.examenjuririquevalidation")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateValidationSignature()));

			}
			if(dossier.getDosDateApprobation()!=null)
			{
				nombre=nombre+ 1;
				approbationmarche.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				approbationmarche.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.approbationmarche.approuvele")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateApprobation()));

			}
			if(dossier.getDosDateNotification()!=null)
			{
				nombre=nombre+ 1;
				notificationmarche.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				notificationmarche.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.notificationmarche.notifiele")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateNotification()));

			}
			if(dossier.getDosDatePublicationDefinitive()!=null)
			{
				nombre=nombre+ 1;
				publicationattributiondefinitive.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				publicationattributiondefinitive.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.pubattribdefinitive.publiele")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDatePublicationDefinitive()));
				publicationattributiondefinitive.setTooltip(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.pubattribdefinitive.publieles")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDatePublicationDefinitive()));
			}
			if(nombre>0)
				lblapprobation.setStyle("color:#0066FF;font-size:20px; ");
			if(nombre==5)
			{
				lblapprobation.setStyle("color:#00FF00;font-size:20px; ");
			}
		}
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	public void onClick$souscriptiondumarche() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				
		}
		else
		{
			if(dossier.getDosDatePublicationProvisoire()!=null)
			 {
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.publierdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			 }
			 else
			 {
				 session.setAttribute("libelle", "souscriptiondumarche");
					loadApplicationState("suivi_procedure_passation");
			 }
		}
		
	}
	
	public void onClick$bonsengagement() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				
		}
		else
		{
			if(dossier.getDosDateSignature()==null)
			 {
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.souscriredossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			 }
			 else
			 {
				 session.setAttribute("libelle", "bonsengagement");
					loadApplicationState("suivi_procedure_passation");
			 }
		}
		
		
	}
	
	public void onClick$approbationmarche() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				
		}
		else
		{
			 if(dossier.getDosDateValidationSignature()==null)
			 {
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validerdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			 }
			 else
			 {
		        session.setAttribute("libelle", "approbationmarche");
		        loadApplicationState("suivi_procedure_passation");
			 }
		}
	}
	
	public void onClick$notificationmarche() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				
		}
		else
		{
			 if(dossier.getDosDateApprobation()==null)
			 {
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.approuverdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			 }
			 else
			 {
					session.setAttribute("libelle", "notificationmarche");
					loadApplicationState("suivi_procedure_passation");
			 }
		}
	
	}
	public void onClick$publicationattributiondefinitive() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				
		}
		else
		{
			 if(dossier.getDosDateNotification()==null)
			 {
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.notifierdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			 }
			 else
			 {
				    session.setAttribute("libelle", "publicationattributiondefinitive");
					loadApplicationState("suivi_procedure_passation");
			 }
		}
		
	}
}