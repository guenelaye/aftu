package sn.ssi.kermel.web.passationsmarches.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.ContratsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.be.referentiel.ejb.ContratSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class ReceptionDefinitiveController extends AbstractWindow implements
		 AfterCompose {

	public static final String PARAM_WINDOW_CODE = "CODE";
	private Listbox lstListe,lstBailleur;
	private Paging pgPagination,pgBailleur;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference,libellebailleur=null;
    private int parent;
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtVersionElectronique,txtCommentaires,txtVersionElectroniquerapport;
    SygBailleurs bailleur=null;
    private Decimalbox dcmontantprevu;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idcontrat;
	SygRealisations realisation=new SygRealisations();
	private Long idbailleur;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private String nomFichier,nomFichierrapport;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	private Datebox dtdate;
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuValider;
	private Image image,imagerapport;
	private Label lbltitre,lbldeuxpoints,lbltitrerapport,lbldeuxpointsrapport;
	private Div step0,step1;
	private Iframe idIframe;
	private String extension,images,imagesrapport,extensionrapport;
	private SygContrats contrat=new SygContrats();
	private SygContrats contrats=new SygContrats();
	SygPlisouvertures plis=new SygPlisouvertures();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		

	}


	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		  idcontrat=(Long) session.getAttribute("contrat");
		   contrat=BeanLocator.defaultLookup(ContratSession.class).findById(idcontrat);
		dossier=contrat.getDossier();
		appel=dossier.getAppel();
		realisation=appel.getRealisation();
			
		if(dossier.getDosDateRecepDefinitive()==null)
		{
			
			image.setVisible(false);
		}
		else
		{
			infos(dossier,appel);
			menuValider.setDisabled(true);
		}
		
	}

	public void infos(SygDossiers dossier,SygAppelsOffres appel) {
		
		if(dossier.getDosDateRecepDefinitive()!=null)
		{
			extension=dossier.getDosFichierRecepDefinitive().substring(dossier.getDosFichierRecepDefinitive().length()-3,  dossier.getDosFichierRecepDefinitive().length());
			 if(extension.equalsIgnoreCase("pdf"))
				 images="/images/icone_pdf.png";
			 else  
				 images="/images/word.jpg";
			 
			 extensionrapport=dossier.getDosAchevement().substring(dossier.getDosAchevement().length()-3,  dossier.getDosAchevement().length());
			 if(extensionrapport.equals("pdf"))
				 imagesrapport="/images/icone_pdf.png";
			 else  
				 imagesrapport="/images/word.jpg";
			 
			image.setVisible(true);
			image.setSrc(images);
			lbldeuxpoints.setValue(":");
			lbltitre.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.procesverbalouverture.versionfichier"));
			
			imagerapport.setVisible(true);
			imagerapport.setSrc(imagesrapport);
			lbldeuxpointsrapport.setValue(":");
			lbltitrerapport.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.execution.receptiondefinitive.rapport"));
		
			dtdate.setValue(dossier.getDosDateRecepDefinitive());
			txtCommentaires.setValue(dossier.getDosCommentRecepDefinitive());
			
			contrats=BeanLocator.defaultLookup(ContratsSession.class).getContrat(dossier,autorite, null,null,null);
			if(contrats!=null)
				contrat=contrats;
		
			 plis=BeanLocator.defaultLookup(RegistrededepotSession.class).findSoumissionnaire(dossier,UIConstants.PARENT,  UIConstants.MOINSDISANTQUALIFIE);
				
		}
	}
	public void onClick$btnChoixFichier() {
		//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
			if (ToolKermel.isWindows())
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectronique.setValue(nomFichier);
		}
	
	public void onClick$btnChoixFichierrapport() {
		//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
			if (ToolKermel.isWindows())
				nomFichierrapport = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichierrapport = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectroniquerapport.setValue(nomFichierrapport);
		}
	
private boolean checkFieldConstraints() {
		
		try {
		
			if(dtdate.getValue()==null)
		     {
               errorComponent = dtdate;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.execution.receptionprovisoire.date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		
			if(txtVersionElectronique.getValue().equals(""))
		     {
          errorComponent = txtVersionElectronique;
          errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.execution.ordreserdemarrage.fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtVersionElectroniquerapport.getValue().equals(""))
		     {
         errorComponent = txtVersionElectroniquerapport;
         errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.execution.receptiondefinitive.rapport")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	
		public void onClick$menuValider() {
			if(checkFieldConstraints())
			{
			
				dossier.setDosCommentRecepDefinitive(txtCommentaires.getValue());
				if(!txtVersionElectronique.getValue().equals(""))
				  dossier.setDosFichierRecepDefinitive(txtVersionElectronique.getValue());
				dossier.setDosDateRecepDefinitive(dtdate.getValue());
				if(!txtVersionElectroniquerapport.getValue().equals(""))
		        dossier.setDosAchevement(txtVersionElectroniquerapport.getValue());
				BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).update(dossier);
				
				contrat.setConCommentRecepDefinitive(txtCommentaires.getValue());
				contrat.setConFichierRecepDefinitive(txtVersionElectronique.getValue());
				contrat.setConDateRecepDefinitive(dtdate.getValue());
				contrat.setConDateSignature(dtdate.getValue());
				
//				contrat.setDossier(dossier);
//				contrat.setAutorite(autorite);
				contrat.setConstatus(UIConstants.CONTRAT_NPAYE);
//				if(plis!=null)
//				{
//					contrat.setPlis(plis);
//					contrat.setMontant(plis.getMontantdefinitif());
//				}
				BeanLocator.defaultLookup(ContratsSession.class).update(contrat);
				infos(dossier,appel);
			}
		}
		
		public void onClick$image() {
			step0.setVisible(false);
			step1.setVisible(true);
			String filepath = cheminDossier +  dossier.getDosFichierRecepDefinitive();
			File f = new File(filepath.replaceAll("\\\\", "/"));

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(f, null, null);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (mymedia != null)
				idIframe.setContent(mymedia);
			else
				idIframe.setSrc("");

			idIframe.setHeight("600px");
			idIframe.setWidth("100%");
		}
		
		public org.zkoss.util.media.AMedia fetchFile(File file) {

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(file, null, null);
				return mymedia;
			} catch (Exception e) {

				e.printStackTrace();
				return null;
			}

		}
		public void onClick$menuFermer() {
			step0.setVisible(true);
			step1.setVisible(false);
		}
		
		public void onClick$menuCancel() {
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
}