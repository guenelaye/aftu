package sn.ssi.kermel.web.passationsmarches.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygMonnaieoffre;
import sn.ssi.kermel.be.entity.SygNatureprix;
import sn.ssi.kermel.be.entity.SygPays;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.be.referentiel.ejb.MonnaieoffreSession;
import sn.ssi.kermel.be.referentiel.ejb.NatureprixSession;
import sn.ssi.kermel.be.referentiel.ejb.PaysSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class LectureOffreController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstMonnaie,lstNature,lstPays;
	private Paging pgPagination,pgMonnaie,pgNature,pgPays;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle,txtRechercherPays;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference,libellebailleur=null;
    private int parent;
    private Label lbltitre;
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherBailleur,txtChapitre,txtRechercherMonnaie,txtRaisonsocial,txtRechercherNature;
    private Intbox txtnumplis;
    SygBailleurs bailleur=null;
    private Decimalbox dcMontant;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg,libellemonnaie=null,libellenature=null,libellepays=null;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idbailleur;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuAjouter,menuModifier,menuSupprimer;
	private Div step0,step1;
	private SygPlisouvertures plis=null;
	SygMonnaieoffre monnaie=new SygMonnaieoffre();
	SygNatureprix natureprix=new SygNatureprix();
	SygPays pays=new SygPays();
	private Bandbox bdMonnaie,bdNature,bdPays;
	private Radio rdsoumoui,rdsoumnon,rdoftoui,rdoftnon,rdoffoui,rdoffnon;
	private Radiogroup rdpodepaiement;
	private Intbox rabais;
	List<SygPlisouvertures> registredepot = new ArrayList<SygPlisouvertures>();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
		
		addEventListener(ApplicationEvents.ON_MONNAIES, this);
		pgMonnaie.setPageSize(byPage);
		pgMonnaie.addForward("onPaging", this, ApplicationEvents.ON_MONNAIES);
		lstMonnaie.setItemRenderer(new MonnaiesRenderer());
		Events.postEvent(ApplicationEvents.ON_MONNAIES, this, null);
		
		addEventListener(ApplicationEvents.ON_NATURES, this);
		pgNature.setPageSize(byPage);
		pgNature.addForward("onPaging", this, ApplicationEvents.ON_NATURES);
		lstNature.setItemRenderer(new NaturesPrixRenderer());
		Events.postEvent(ApplicationEvents.ON_NATURES, this, null);
		
		
		addEventListener(ApplicationEvents.ON_PAYS, this);
		pgPays.setPageSize(byPage);
		pgPays.addForward("onPaging", this, ApplicationEvents.ON_PAYS);
		lstPays.setItemRenderer(new PaysRenderer());
		Events.postEvent(ApplicationEvents.ON_PAYS, this, null);
		
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier!=null)
		{
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
		
	
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygPlisouvertures> plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(activePage,byPageBandbox,dossier,null,null,null,-1,-1,1, -1, -1, -1, null, -1, null, null);
			 lstListe.setModel(new SimpleListModel(plis));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(RegistrededepotSession.class).count(dossier,null,null,null,-1,-1,1, -1, -1, -1, null, -1, null, null));
		} 
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MONNAIES)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgMonnaie.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgMonnaie.getActivePage() * byPageBandbox;
				pgMonnaie.setPageSize(byPageBandbox);
			}
			List<SygMonnaieoffre> monnaies = BeanLocator.defaultLookup(MonnaieoffreSession.class).ListeMonnaies(activePage, byPageBandbox,libellemonnaie,dossier.getDosID());
			lstMonnaie.setModel(new SimpleListModel(monnaies));
			pgMonnaie.setTotalSize(BeanLocator.defaultLookup(MonnaieoffreSession.class).ListeMonnaies(activePage, byPageBandbox,libellemonnaie,dossier.getDosID()).size());
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_NATURES)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgNature.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgNature.getActivePage() * byPageBandbox;
				pgNature.setPageSize(byPageBandbox);
			}
			List<SygNatureprix> natures = BeanLocator.defaultLookup(NatureprixSession.class).find(activePage, byPageBandbox,null,libellenature);
			lstNature.setModel(new SimpleListModel(natures));
			pgNature.setTotalSize(BeanLocator.defaultLookup(NatureprixSession.class).count(null,libellenature));
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_PAYS)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgPays.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPays.getActivePage() * byPageBandbox;
				pgPays.setPageSize(byPageBandbox);
			}
			List<SygPays> pays = BeanLocator.defaultLookup(PaysSession.class).find(activePage, byPageBandbox,libellepays,null);
			lstPays.setModel(new SimpleListModel(pays));
			pgPays.setTotalSize(BeanLocator.defaultLookup(PaysSession.class).count(libellepays,null));
		}
		else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
			Listcell button = (Listcell) event.getTarget();
			String toDo = (String) button.getAttribute(UIConstants.TODO);
			plis = (SygPlisouvertures) button.getAttribute("plis");
			if (toDo.equalsIgnoreCase("modifier"))
			{
				
				txtnumplis.setValue(plis.getNumero());
				bdMonnaie.setValue(plis.getMonCode());
				bdNature.setValue(plis.getNatCode());
				txtRaisonsocial.setValue(plis.getRetrait().getNomSoumissionnaire());
				dcMontant.setValue(plis.getMontantoffert());
				bdPays.setValue(plis.getPays());
				natureprix=dossier.getNatureprix();
				bdNature.setValue(natureprix.getNatLibelle());
				bdNature.setButtonVisible(false);
				if(plis.getRabais()>-1)
					rabais.setValue(plis.getRabais());
				if(plis.getLettreSoumission()==1)
					rdsoumoui.setChecked(true);
				else
					rdsoumnon.setChecked(true);
				if(plis.getOffreFinanciere()==1)
					rdoffoui.setChecked(true);
				else
					rdoffnon.setChecked(true);
				if(plis.getOffreTechnique()==1)
					rdoftoui.setChecked(true);
				else
					rdoftnon.setChecked(true);
				step0.setVisible(false);
				step1.setVisible(true);
				Events.postEvent(ApplicationEvents.ON_NATURES, this, null);
				Events.postEvent(ApplicationEvents.ON_MONNAIES, this, null);
				Events.postEvent(ApplicationEvents.ON_PAYS, this, page);
			}
		}
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygPlisouvertures plis = (SygPlisouvertures) data;
		item.setValue(plis);

		 Listcell cellNumero = new Listcell(Integer.toString(plis.getNumero()));
		 cellNumero.setParent(item);
		 
		 Listcell cellRaison = new Listcell(plis.getRetrait().getNomSoumissionnaire());
		 cellRaison.setParent(item);
		 
		 Listcell cellMontantcfa = new Listcell("");
		 if(plis.getMontantoffert()!=null)
			 cellMontantcfa.setLabel(ToolKermel.format2Decimal(plis.getMontantoffert()));
		 cellMontantcfa.setParent(item);
		 
		 Listcell cellMontantoffert = new Listcell("");
		 if(plis.getMontantoffert()!=null)
		 cellMontantoffert.setLabel(ToolKermel.format2Decimal(plis.getMontantoffert()));
		 cellMontantoffert.setParent(item);
		 
		 Listcell cellMonnaie= new Listcell(plis.getMonCode());
		 cellMonnaie.setParent(item);
		 
		 Listcell cellNature= new Listcell(plis.getNatCode());
		 cellNature.setParent(item);
		 
		 Listcell cellRabaiscfa= new Listcell("");
		 cellRabaiscfa.setParent(item);
		 
		 Listcell cellRabaisdevise= new Listcell("");
		 cellRabaisdevise.setParent(item);
		 
		 Listcell cellImageModif = new Listcell();
		 if(dossier!=null)
		 {
			 if(dossier.getDateRemiseDossierTechnique()==null)
			 {
				 cellImageModif.setImage("/images/calculator_add.png");
				 cellImageModif.setAttribute(UIConstants.TODO, "modifier");
				 cellImageModif.setAttribute("plis", plis);
				 cellImageModif.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.lectureoffres.saisiroffre"));
				 cellImageModif.addEventListener(Events.ON_CLICK, LectureOffreController.this);
			 }
			
		 }
		
		 cellImageModif.setParent(item);
		 
		 Listcell cellImageTraite = new Listcell();
		 
		 if(plis.getValide()==UIConstants.NPARENT)
		 {
		
		 cellImageTraite.setImage("/images/deletemoins.png");
		 cellImageTraite.setAttribute(UIConstants.TODO, "traiter");
		 cellImageTraite.setAttribute("plis", plis);
		 cellImageTraite.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.lectureoffres.saisiroffre"));
		
		 }
		 cellImageTraite.setParent(item);
	}

	public void onClick$menuAjouter()
	{
		Events.postEvent(ApplicationEvents.ON_PLIS, this, null);
		step0.setVisible(false);
		step1.setVisible(true);
		
	}
	
	public void onClick$menuFermer()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	
	///////////Monnaies///////// 
	public void onSelect$lstMonnaie(){
		monnaie= (SygMonnaieoffre) lstMonnaie.getSelectedItem().getValue();
		bdMonnaie.setValue(monnaie.getMonLibelle());
		bdMonnaie.close();
	
	}
	
	public class MonnaiesRenderer implements ListitemRenderer{
	
	
	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygMonnaieoffre monnaie = (SygMonnaieoffre) data;
		item.setValue(monnaie);
		
		
		Listcell cellCode = new Listcell(monnaie.getMonCode());
		cellCode.setParent(item);
		
		Listcell cellLibelle = new Listcell(monnaie.getMonLibelle());
		cellLibelle.setParent(item);
	
		if(plis!=null)
		{
			if(plis.getMonCode()!=null)
			{
				if(plis.getMonCode().equals(monnaie.getMonCode()))
					item.setSelected(true);
			}
		}
	}
	}
	public void onFocus$txtRechercherMonnaie(){
	if(txtRechercherMonnaie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.devise.monnaie"))){
		txtRechercherMonnaie.setValue("");
	}		 
	}
	
	public void  onClick$btnRechercherMonnaie(){
	if(txtRechercherMonnaie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.devise.monnaie")) || txtRechercherMonnaie.getValue().equals("")){
		libellemonnaie = null;
		page=null;
	}else{
		libellemonnaie = txtRechercherMonnaie.getValue();
		page="0";
	}
	Events.postEvent(ApplicationEvents.ON_MONNAIES, this, page);
	}

	public void  onClick$menuValider(){
		if(checkFieldConstraints())
		{
			plis.setNumero(txtnumplis.getValue());
			plis.setMonCode(monnaie.getMonCode());
			plis.setNatCode(natureprix.getNatCode());
			plis.setValide(UIConstants.PARENT);
			plis.setMontantoffert(dcMontant.getValue());
			plis.setMontantdefinitif(dcMontant.getValue());
			plis.setPrixevalue(dcMontant.getValue());
			plis.setPays(pays.getLibelle());
			if(rabais.getValue()!=null)
				plis.setRabais(rabais.getValue());
			
			if(rdsoumoui.isChecked()==true)
				plis.setLettreSoumission(UIConstants.PARENT);
			else
				plis.setLettreSoumission(UIConstants.NPARENT);
			
			if(rdoffoui.isChecked()==true)
				plis.setOffreFinanciere(UIConstants.PARENT);
			else
				plis.setOffreFinanciere(UIConstants.NPARENT);
			
			if(rdoftoui.isChecked()==true)
				plis.setOffreTechnique(UIConstants.PARENT);
			else
				plis.setOffreTechnique(UIConstants.NPARENT);
			
			
			BeanLocator.defaultLookup(RegistrededepotSession.class).update(plis);
			
			session.setAttribute("libelle", "lecturesoffres");
			loadApplicationState("suivi_procedure_passation");
		}
	}
private boolean checkFieldConstraints() {
		
		try {
		
			if(bdNature.getValue().equals(""))
		     {
               errorComponent = bdNature;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.devise.natureprix")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(bdMonnaie.getValue().equals(""))
		     {
              errorComponent = bdMonnaie;
              errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.devise.monnaie")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(bdPays.getValue().equals(""))
		     {
             errorComponent = bdPays;
             errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.devise.Pays")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dcMontant.getValue()==null)
		     {
             errorComponent = dcMontant;
             errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.devise.montantcfa")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			return true;
			
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

///////////Natures Prix///////// 
		public void onSelect$lstNature(){
			natureprix= (SygNatureprix) lstNature.getSelectedItem().getValue();
		bdNature.setValue(natureprix.getNatLibelle());
		bdNature.close();
		
		}
		
		public class NaturesPrixRenderer implements ListitemRenderer{
		
		
		
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygNatureprix nature = (SygNatureprix) data;
			item.setValue(nature);
			
			
			Listcell cellCode = new Listcell(nature.getNatCode());
			cellCode.setParent(item);
			
			Listcell cellLibelle = new Listcell(nature.getNatLibelle());
			cellLibelle.setParent(item);
			
				if(plis!=null)
				{
					if(plis.getNatCode()!=null)
					{
						if(plis.getNatCode().equals(nature.getNatCode()))
							item.setSelected(true);
					}
				}
			}
		}
		public void onFocus$txtRechercherNature(){
		if(txtRechercherNature.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.lectureoffres.natureprix"))){
			txtRechercherNature.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherNature(){
		if(txtRechercherNature.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.lectureoffres.natureprix")) || txtRechercherNature.getValue().equals("")){
		libellenature = null;
		page=null;
		}else{
			libellenature = txtRechercherNature.getValue();
		page="0";
		}
		Events.postEvent(ApplicationEvents.ON_NATURES, this, page);
		}
		
///////////Pays///////// 
		public void onSelect$lstPays(){
			pays= (SygPays) lstPays.getSelectedItem().getValue();
		bdPays.setValue(pays.getLibelle());
		bdPays.close();
		
		}
		
		public class PaysRenderer implements ListitemRenderer{
		
		
		
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygPays pays = (SygPays) data;
			item.setValue(pays);
			
			Listcell cellLibelle = new Listcell(pays.getLibelle());
			cellLibelle.setParent(item);
			
			
			}
		}
		public void onFocus$txtRechercherPays(){
		if(txtRechercherPays.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.lectureoffres.pays"))){
			txtRechercherPays.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherPays(){
		if(txtRechercherPays.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.lectureoffres.pays")) || txtRechercherPays.getValue().equals("")){
		libellepays = null;
		page=null;
		}else{
			libellepays = txtRechercherPays.getValue();
		page="0";
		}
		Events.postEvent(ApplicationEvents.ON_PAYS, this, page);
		}

}