package sn.ssi.kermel.web.passationsmarches.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygMonnaieoffre;
import sn.ssi.kermel.be.entity.SygNatureprix;
import sn.ssi.kermel.be.entity.SygPays;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class VerificationconformiteController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstMonnaie,lstNature,lstPays;
	private Paging pgPagination,pgMonnaie,pgNature,pgPays;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle,txtRechercherPays;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference,libellebailleur=null;
    private int parent;
    private Label lbltitre;
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherBailleur,txtChapitre,txtRechercherMonnaie,txtnumplis,txtRaisonsocial,txtRechercherNature;
    SygBailleurs bailleur=null;
    private Decimalbox dcMontant;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg,libellemonnaie=null,libellenature=null,libellepays=null;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idbailleur;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuAjouter,menuModifier,menuSupprimer;
	private Div step0,step1;
	private SygPlisouvertures plis=null;
	SygMonnaieoffre monnaie=new SygMonnaieoffre();
	SygNatureprix natureprix=new SygNatureprix();
	SygPays pays=new SygPays();
	private Bandbox bdMonnaie,bdNature,bdPays;
	private Radio rdsoumoui,rdsoumnon,rdoftoui,rdoftnon,rdoffoui,rdoffnon;
	private Radiogroup rdpodepaiement;
	private Intbox rabais;
	List<SygPlisouvertures> listesplis = new ArrayList<SygPlisouvertures>();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
		
		
		
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier!=null)
		{
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
		
	
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			listesplis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(activePage,byPageBandbox,dossier,null,null,null,-1,-1,1, -1, -1, -1, null, -1, null, null);
			 lstListe.setModel(new SimpleListModel(listesplis));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(RegistrededepotSession.class).count(dossier,null,null,null,-1,-1,1, -1, -1, -1, null, -1, null, null));
		} 
	
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygPlisouvertures plis = (SygPlisouvertures) data;
		item.setValue(plis);
		
		 Listcell cellRaison = new Listcell(plis.getRetrait().getNomSoumissionnaire());
		 cellRaison.setParent(item);
		 
		 Listcell cellMontantcfa = new Listcell("");
		 if(plis.getMontantoffert()!=null)
			 cellMontantcfa.setLabel(ToolKermel.format2Decimal(plis.getMontantoffert()));
		 cellMontantcfa.setParent(item);
		 
		 if(plis.getEtatExamenPreliminaire()==UIConstants.PARENT)
		 item.setSelected(true);
		 
		
		
	}


	public void  onClick$menuValider(){
		
	       
	       for (int k = 0; k < listesplis.size(); k++) {
	    	   plis=listesplis.get(k);
	    	   plis.setEtatExamenPreliminaire(UIConstants.NPARENT);
	      	   BeanLocator.defaultLookup(RegistrededepotSession.class).update(plis);
	       }
	       for (int i = 0; i < lstListe.getSelectedCount(); i++) {
	    	 
	    	   plis=(SygPlisouvertures) ((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
	    	   plis.setEtatExamenPreliminaire(UIConstants.PARENT);
	      	   BeanLocator.defaultLookup(RegistrededepotSession.class).update(plis);
	       }
	       session.setAttribute("libelle", "formverificationconformite");
			loadApplicationState("suivi_procedure_passation");
		
	}


}