package sn.ssi.kermel.web.passationsmarches.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class DossiersDAOController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code;
	private Tab TAB_infosgenerales,TAB_garanties,TAB_piecesadministratives,TAB_criteresqualifications,TAB_devise,TAB_financement;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblNumero,lblMontant,lblLieu,lblDate;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygAppelsOffres appel=new SygAppelsOffres();
	SygDossiers dossier=new SygDossiers();
	private Long iddossier;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		iddossier=(Long) session.getAttribute("iddossier");
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).findById(iddossier);
		appel=dossier.getAppel();
		lblNumero.setValue(appel.getAporeference());
		lblMontant.setValue(ToolKermel.format2Decimal(dossier.getDosmontantdao()));
		lblLieu.setValue(dossier.getDosLieuOuvertureDesPlis());
		lblDate.setValue(UtilVue.getInstance().formateLaDate2(dossier.getDosDateOuvertueDesplis())+" "+UtilVue.getInstance().formateLHeure(dossier.getDosHeureOuvertureDesPlis()));
		 Include inc = (Include) this.getFellowIfAny("incordredeservicededemarrage");
         inc.setSrc("/passationsmarches/procedurespassations/formordredeservicededemarrage.zul");
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	
}