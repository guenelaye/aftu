package sn.ssi.kermel.web.passationsmarches.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygMontantsSeuils;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.referentiel.ejb.MontantsSeuilsSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class TraitementsDossiersController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code,avalider="oui";
	private Tab TAB_infosgenerales,TAB_garanties,TAB_piecesadministratives,TAB_criteresqualifications,TAB_devise,TAB_financement;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblInfos;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygDossiers dossier=new SygDossiers();
	List<SygMontantsSeuils> seuils = new ArrayList<SygMontantsSeuils>();
	private Image imgmiseenvalidation;
	private Label miseenvalidation,dossiersap,lblpreparation,miseenpublication,demandepublication,lblouvertureplis,lblregistreretrait,
	lblregistredepot,ouvertureplis,evaluationsoffres,soumissionpourvalidationattributionprovisoire,publicationattributionprovisoire,attributionprovisoire,
	lblevaattrprovisoire,lblapprobation,souscriptiondumarche,bonsengagement,approbationmarche,notificationmarche,publicationattributiondefinitive,
	lblimmatriculation,lblimmatriculations;
	private int garanties=0,piecesadministratives=0,criteresqualifications=0,devises=0,bailleurs=0,lots=0,registreretrait=0,registredepot
			,membrescommissions,representantsoummissionnaires,observateursindependants,piecessoumissionnaires,compositioncommissiontechnique,
			lecturesoffres,garantiessoumissions,criteresqualificationssoum,representantservicestechniques,registredepotverifies,
			documents,attributaires,nombre=0,nombreapprobation=0;
	 SygPlisouvertures soumissionnaires=new SygPlisouvertures();
	 SygContrats contrat=new SygContrats();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		dossier=(SygDossiers) Executions.getCurrent().getAttribute("dossier");
		if(dossier!=null)
		{
			lblpreparation.setStyle("color:#0066FF;font-size:20px; ");
			dossiersap.setStyle("color:#0066FF;cursor:pointer");
			garanties = (Integer) Executions.getCurrent().getAttribute("garanties");
			piecesadministratives = (Integer) Executions.getCurrent().getAttribute("piecesadministratives");
			criteresqualifications = (Integer) Executions.getCurrent().getAttribute("criteresqualifications");
			devises=(Integer) Executions.getCurrent().getAttribute("devises");
			bailleurs = (Integer) Executions.getCurrent().getAttribute("bailleurs");
			lots= (Integer) Executions.getCurrent().getAttribute("lots");
			membrescommissions= (Integer) Executions.getCurrent().getAttribute("membrescommissions");
			representantsoummissionnaires= (Integer) Executions.getCurrent().getAttribute("representantsoummissionnaires");
			representantservicestechniques= (Integer) Executions.getCurrent().getAttribute("representantservicestechniques");
			observateursindependants= (Integer) Executions.getCurrent().getAttribute("observateursindependants");
			piecessoumissionnaires= (Integer) Executions.getCurrent().getAttribute("piecessoumissionnaires");
			compositioncommissiontechnique= (Integer) Executions.getCurrent().getAttribute("compositioncommissiontechnique");
			lecturesoffres= (Integer) Executions.getCurrent().getAttribute("lecturesoffres");
			garantiessoumissions= (Integer) Executions.getCurrent().getAttribute("garantiessoumissions");
			
			registredepotverifies= (Integer) Executions.getCurrent().getAttribute("registredepotverifies");
			criteresqualificationssoum= (Integer) Executions.getCurrent().getAttribute("criteresqualificationssoum");
			documents= (Integer) Executions.getCurrent().getAttribute("documents");
			soumissionnaires= (SygPlisouvertures) Executions.getCurrent().getAttribute("soumissionnaires");
			attributaires= (Integer) Executions.getCurrent().getAttribute("attributaires");
			contrat=(SygContrats) Executions.getCurrent().getAttribute("contrat");
			
			if(dossier.getDosDateMiseValidation()!=null)
			  {
				if(dossier.getDosDateValidation()==null)
					miseenvalidation.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.enattentevalidation"));
					else
					miseenvalidation.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validationdossier")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateValidation()));
				    miseenvalidation.setStyle("color:#00FF00;");
				
			  }
			if(dossier.getDosdatedemandepublication()!=null)
			{
				demandepublication.setStyle("color:#00FF00;");
				
			}
			if(dossier.getDosDatePublication()!=null)
			{
				miseenpublication.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.publication.publie")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDatePublication()));
				miseenpublication.setStyle("color:#00FF00;");
				
			}
			
			if(dossier.getDosDatePublication()!=null)
			{
				registreretrait= (Integer) Executions.getCurrent().getAttribute("registreretrait");
			}
			if(registreretrait>0)
			{
				lblouvertureplis.setStyle("color:#0066FF; font-size:20px;");
				lblregistreretrait.setStyle("color:#00FF00;");
				registredepot= (Integer) Executions.getCurrent().getAttribute("registredepot");
			}
			if(registredepot>0)
			{
				lblouvertureplis.setStyle("color:#0066FF; font-size:20px;");
				lblregistredepot.setStyle("color:#00FF00;");
			}
			if(membrescommissions>0||representantsoummissionnaires>0||representantservicestechniques>0||observateursindependants>0
					||piecessoumissionnaires>0 ||compositioncommissiontechnique>0||lecturesoffres>0
					||garantiessoumissions>0||criteresqualificationssoum>0)
			{
				ouvertureplis.setStyle("color:#0066FF;");
			}
			if(membrescommissions>0&&representantsoummissionnaires>0&&representantservicestechniques>0&&observateursindependants>0
					||piecessoumissionnaires>0 &&compositioncommissiontechnique>0&&lecturesoffres>0
					||garantiessoumissions>0&&criteresqualificationssoum>0&&appel.getApoDatepvouverturepli()!=null)
			{
				ouvertureplis.setStyle("color:#00FF00;");
				lblouvertureplis.setStyle("color:#00FF00;font-size:20px; ");
				
			}
			if(dossier.getDateRemiseDossierTechnique()!=null&&dossier.getDateLimiteDossierTechnique()!=null&&registredepotverifies>0&&criteresqualificationssoum>0
					&&documents>0&&soumissionnaires!=null)
			{
				evaluationsoffres.setStyle("color:#00FF00;");
				nombre=nombre+1;
			}
			if(attributaires==dossier.getDosNombreLots())
					{
				attributionprovisoire.setStyle("color:#00FF00;");
				nombre=nombre+1;
				lblevaattrprovisoire.setStyle("color:#0066FF;font-size:20px; ");
						}
			 if(dossier.getDosDateMiseValidationattribution()!=null)
				{
				 nombre=nombre+1;
				 soumissionpourvalidationattributionprovisoire.setStyle("color:#00FF00;");
					if(dossier.getDosDateValidationPrequalif()==null)
						soumissionpourvalidationattributionprovisoire.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.enattentevalidation"));
						else
						{
							soumissionpourvalidationattributionprovisoire.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire.validele")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateValidationPrequalif()));
							soumissionpourvalidationattributionprovisoire.setTooltip(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire.valideles")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateValidationPrequalif()));
							
						}
				}
			 if(dossier.getDosDatePublicationProvisoire()!=null)
				{
				 nombre=nombre+1;
				 publicationattributionprovisoire.setStyle("color:#00FF00;");
				 publicationattributionprovisoire.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire.publiele")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDatePublicationProvisoire()));
					publicationattributionprovisoire.setTooltip(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire.publieles")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDatePublicationProvisoire()));
				
				}
			 if(nombre==4)
			 {
				 lblevaattrprovisoire.setStyle("color:#00FF00;font-size:20px; ");
			 }
			 if(dossier.getDosDateSignature()!=null)
				{
				    nombreapprobation=nombreapprobation+ 1;
					souscriptiondumarche.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					souscriptiondumarche.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.souscritle")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateSignature()));
					
				}
				if(dossier.getDosDateMiseValidationSignature()!=null)
				{
					 nombreapprobation=nombreapprobation+ 1;
					bonsengagement.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					if(dossier.getDosDateValidationSignature()==null)
						bonsengagement.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.examenjuririqueencoursvalidation"));
					else
						bonsengagement.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.examenjuririquevalidation")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateValidationSignature()));

				}
				if(dossier.getDosDateApprobation()!=null)
				{
					 nombreapprobation=nombreapprobation+ 1;
					approbationmarche.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					approbationmarche.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.approbationmarche.approuvele")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateApprobation()));

				}
				if(dossier.getDosDateNotification()!=null)
				{
					 nombreapprobation=nombreapprobation+ 1;
					notificationmarche.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					notificationmarche.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.notificationmarche.notifiele")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateNotification()));

				}
				if(dossier.getDosDatePublicationDefinitive()!=null)
				{
					 nombreapprobation=nombreapprobation+ 1;
					publicationattributiondefinitive.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					publicationattributiondefinitive.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.pubattribdefinitive.publiele")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDatePublicationDefinitive()));
					publicationattributiondefinitive.setTooltip(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.pubattribdefinitive.publieles")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDatePublicationDefinitive()));
				}
				if(nombreapprobation>0)
					lblapprobation.setStyle("color:#0066FF;font-size:20px; ");
				if(nombreapprobation==5)
				{
					lblapprobation.setStyle("color:#00FF00;font-size:20px; ");
				}
				if(contrat!=null)
				{
					if(contrat.getDatedemandeimmatriculation()!=null)
					{
						lblimmatriculation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
						lblimmatriculation.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.demande").substring(0, 15)+"...");
						lblimmatriculation.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.demande")+" "+UtilVue.getInstance().formateLaDate2(contrat.getDatedemandeimmatriculation()));
						lblimmatriculations.setStyle("color:#0066FF;font-size:20px; ");
					}
					if(contrat.getDateimmatriculation()!=null)
					{
						lblimmatriculation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
						lblimmatriculation.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.marche").substring(0, 15)+"...");
						lblimmatriculation.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.marche")+" "+UtilVue.getInstance().formateLaDate2(contrat.getDateimmatriculation()));
						lblimmatriculations.setStyle("color:#00FF00;font-size:20px; ");
					}
				}
		}
		seuils = BeanLocator.defaultLookup(MontantsSeuilsSession.class).find(0,-1, autorite.getType(), appel.getTypemarche(), appel.getModepassation(), null, null,UIConstants.TYPE_SEUILSRAPRIORI);

			if(seuils.size()>0)
			{
				if(seuils.get(0).getMontantinferieur()!=null)
				{
					if((appel.getApomontantestime().compareTo(seuils.get(0).getMontantinferieur())==1)||(appel.getApomontantestime().equals(seuils.get(0).getMontantinferieur())))
			
					{
						avalider="oui";
						miseenvalidation.setVisible(true);
						demandepublication.setVisible(true);
						
					}
					else
					{
						avalider="non";
						miseenvalidation.setVisible(false);
						demandepublication.setVisible(false);
						
					}
						
				}
				
			}
			if(lots>0&&garanties>0&&piecesadministratives>0&&criteresqualifications>0&&devises>0&&bailleurs>0)
			{
				dossiersap.setStyle("color:#00FF00;cursor:pointer");
				if(avalider.equals("oui"))
				{
					if(dossier.getDosDateMiseValidation()!=null&&dossier.getDosDatePublication()!=null)
						lblpreparation.setStyle("color:#00FF00; font-size:20px;");
				}
				else
				{
					if(dossier.getDosDatePublication()!=null)
						lblpreparation.setStyle("color:#00FF00; font-size:20px;");
				}
				
			}
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	public void onClick$dossiersap() {
		session.setAttribute("libelle", "infogenerales");
		loadApplicationState("suivi_procedure_passation");
	}
	
	public void onClick$miseenvalidation() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		}
		else
		{
			if(garanties==0)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirgaranties"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				if(piecesadministratives==0)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirpieces"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				}
				else
				{
					if(criteresqualifications==0)
					{
						 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisircriteres"), "Erreur", Messagebox.OK, Messagebox.ERROR);
							
					}
					else
					{
						if(devises==0)
						{
							 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirdevises"), "Erreur", Messagebox.OK, Messagebox.ERROR);
								
						}
						else
						{
							if(bailleurs==0)
							{
								 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirbailleurs"), "Erreur", Messagebox.OK, Messagebox.ERROR);
									
							}
							else
							{
							  if(dossier.getDosNombreLots()==1)
								{
									session.setAttribute("libelle", "validationdossier");
									loadApplicationState("suivi_procedure_passation");
								}
								else
								{
									if(lots==dossier.getDosNombreLots())
									{
										session.setAttribute("libelle", "validationdossier");
										loadApplicationState("suivi_procedure_passation");
									}
									else
									{
										 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.allotissement.nombres")+" "+dossier.getDosNombreLots(), "Erreur", Messagebox.OK, Messagebox.ERROR);
											
									}
								}
								
							}
						}
					}
				}
			}
			
		}
	}
	
	public void onClick$miseenpublication() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		}
		else
		{
			if(avalider.equals("oui"))
			{
				if(dossier.getDosDateValidation()==null)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validerdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);

				}
				else
				{
					session.setAttribute("libelle", "publicationdossier");
					loadApplicationState("suivi_procedure_passation");
				}
			}
			else
			{
				session.setAttribute("libelle", "publicationdossier");
				loadApplicationState("suivi_procedure_passation");
			}
			
			
		}
	}
	
	public void onClick$lblregistreretrait() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		}
		else
		{
			if(dossier.getDosDatePublication()==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.publieravis"), "Erreur", Messagebox.OK, Messagebox.ERROR);

			}
			else
			{
				session.setAttribute("libelle", "registreretraitdao");
				loadApplicationState("suivi_procedure_passation");
			}
			
		}
	}
	
	public void onClick$lblregistredepot() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		}
		else
		{
			if(registreretrait==0)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.saisirregistreretrait"), "Erreur", Messagebox.OK, Messagebox.ERROR);

			}
			else
			{
				session.setAttribute("libelle", "plisouvertures");
				loadApplicationState("suivi_procedure_passation");
			}
			
		}
	}
	
	public void onClick$ouvertureplis() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		}
		else
		{
			if(lecturesoffres==0)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.saisirregistredepot"), "Erreur", Messagebox.OK, Messagebox.ERROR);
	
			}
			else
			{
				session.setAttribute("libelle", "ouverturesplis");
				loadApplicationState("suivi_procedure_passation");
			}
		}
	}
	public void onClick$evaluationsoffres() throws InterruptedException {
		if(appel.getApoDatepvouverturepli()==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.saisirproces"), "Erreur", Messagebox.OK, Messagebox.ERROR);

		}
		else
		{
			session.setAttribute("libelle", "evaluationsoffres");
			loadApplicationState("suivi_procedure_passation");
		}
	}
	
	public void onClick$attributionprovisoire() throws InterruptedException {
		session.setAttribute("libelle", "attributionprovisoire");
		loadApplicationState("suivi_procedure_passation");
	}
	
	public void onClick$soumissionpourvalidationattributionprovisoire() throws InterruptedException {
		session.setAttribute("libelle", "soumissionpourvalidationattributionprovisoire");
		loadApplicationState("suivi_procedure_passation");
	}
	
	public void onClick$publicationattributionprovisoire() throws InterruptedException {
		session.setAttribute("libelle", "publicationattributionprovisoire");
		loadApplicationState("suivi_procedure_passation");
	}
	
	public void onClick$souscriptiondumarche() throws InterruptedException {
		session.setAttribute("libelle", "souscriptiondumarche");
		loadApplicationState("suivi_procedure_passation");
	}
	
	public void onClick$bonsengagement() throws InterruptedException {
		session.setAttribute("libelle", "bonsengagement");
		loadApplicationState("suivi_procedure_passation");
	}
	
	public void onClick$approbationmarche() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				
		}
		else
		{
			 if(dossier.getDosDateValidationSignature()==null)
			 {
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validerdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			 }
			 else
			 {
				 session.setAttribute("libelle", "approbationmarche");
				loadApplicationState("suivi_procedure_passation");
			 }
		}
		
	}
	
	public void onClick$notificationmarche() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				
		}
		else
		{
			 if(dossier.getDosDateApprobation()==null)
			 {
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.approuverdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			 }
			 else
			 {
					session.setAttribute("libelle", "notificationmarche");
					loadApplicationState("suivi_procedure_passation");
			 }
		}
	
	}
	public void onClick$publicationattributiondefinitive() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				
		}
		else
		{
			 if(dossier.getDosDateNotification()==null)
			 {
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.notifierdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			 }
			 else
			 {
				    session.setAttribute("libelle", "publicationattributiondefinitive");
					loadApplicationState("suivi_procedure_passation");
			 }
		}
		
	}
	
	public void onClick$demandepublication() throws InterruptedException {
		session.setAttribute("libelle", "demandepublication");
		loadApplicationState("suivi_procedure_passation");
	}
}