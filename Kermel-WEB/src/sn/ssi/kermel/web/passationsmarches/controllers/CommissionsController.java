package sn.ssi.kermel.web.passationsmarches.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class CommissionsController extends AbstractWindow implements
		AfterCompose {

	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    SygBailleurs bailleur=null;
 	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
    
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		Include inc = (Include) this.getFellowIfAny("incCOMMISSIONSMARCHES");
	    inc.setSrc("/passationsmarches/procedurespassations/commissionsmarches.zul");	
	}

	
}