package sn.ssi.kermel.web.passationsmarches.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygLotsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygMonnaieoffre;
import sn.ssi.kermel.be.entity.SygNatureprix;
import sn.ssi.kermel.be.entity.SygPays;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.LotsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.LotsSoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.be.referentiel.ejb.MonnaieoffreSession;
import sn.ssi.kermel.be.referentiel.ejb.NatureprixSession;
import sn.ssi.kermel.be.referentiel.ejb.PaysSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class LecturesOffresControllers extends AbstractWindow
		implements AfterCompose, EventListener,RowRenderer, RowRendererExt {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String mode, libDist, libEnqueteur, libMarche;
	int idProjet;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private final int byPage1 = 5;
	private Object selobj;
	// private Session session = getHttpSession();
	Grid GridLots;
	Paging pgPagination, pgDist, pgEnqueteur, pgMarche;
	Listbox lstDist, listEnqueteur, listMarche;
	Integer idFiche, distID, enqueteurID, marcheID;
	Textbox txtnumero, txtstatu, txtDrs, txtDist, anneemoi, txtRechercherDist,
			txtRechercherEnqueteur, txtObservation,txtRechercherMonnaie,txtRechercherPays,txtRechercherNature;
	Label nbre, lblMarche, lblEnqueteur, lblDate, lblRegion, lblDepartement,
			lblCampagne, lblSup1, lblSup2;
	Datebox datecree;
	Intbox nbretotalfs, nbretotalconsmois;
	Bandbox bdservice, bdDist, bdFmnsist, bdEnqueteur, bdMarche;
	Button btnGenerer, btnRechercherDist, btnRechercherMarcher,
			btnRechercherEnqueteur;
	Menuitem menuPreviousStep1;
	// Prodistrict district;
	// Proregionmedical regMed;
	private Iframe iframePrint;
	private boolean estGenere = false;
	private String datejour = UtilVue.getInstance().formateLaDate(new Date());
	private int annee = Integer.parseInt(ToolKermel.getAnneeCourante());
	private String mois = datejour.substring(3, 5);
	Session session = getHttpSession();
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	SygDossiers dossier=new SygDossiers();
    SygAutoriteContractante autorite;
	List<SygLotsSoumissionnaires> lotssoum = new ArrayList<SygLotsSoumissionnaires>();
	SygLotsSoumissionnaires lotsoumis=new SygLotsSoumissionnaires();
	private Listbox lstMonnaie,lstNature,lstPays;
	private Paging pgMonnaie,pgNature,pgPays;
	SygMonnaieoffre monnaie=new SygMonnaieoffre();
	SygNatureprix natureprix=new SygNatureprix();
	SygPays pays=new SygPays();
	private Bandbox bdMonnaie,bdNature,bdPays;
	private Radio rdsoumoui,rdsoumnon,rdoftoui,rdoftnon,rdoffoui,rdoffnon;
	private  int activePage;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg,libellemonnaie=null,libellenature=null,libellepays=null,page=null;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Div step0,step1;
	private Intbox txtnumplis,rabais;
	private Textbox txtRaisonsocial;
	private Decimalbox dcMontant;
	List<SygPlisouvertures> registredepot = new ArrayList<SygPlisouvertures>();

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
	

		addEventListener(ApplicationEvents.ON_MONNAIES, this);
		pgMonnaie.setPageSize(byPage);
		pgMonnaie.addForward("onPaging", this, ApplicationEvents.ON_MONNAIES);
		lstMonnaie.setItemRenderer(new MonnaiesRenderer());
		Events.postEvent(ApplicationEvents.ON_MONNAIES, this, null);
		
		addEventListener(ApplicationEvents.ON_NATURES, this);
		pgNature.setPageSize(byPage);
		pgNature.addForward("onPaging", this, ApplicationEvents.ON_NATURES);
		lstNature.setItemRenderer(new NaturesPrixRenderer());
		Events.postEvent(ApplicationEvents.ON_NATURES, this, null);
		
		
		addEventListener(ApplicationEvents.ON_PAYS, this);
		pgPays.setPageSize(byPage);
		pgPays.addForward("onPaging", this, ApplicationEvents.ON_PAYS);
		lstPays.setItemRenderer(new PaysRenderer());
		Events.postEvent(ApplicationEvents.ON_PAYS, this, null);
	}

	public void onCreate(final CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);

		if(dossier!=null)
		{
			
			
			registredepot= BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,-1,-1,-1, -1, -1, -1, null, -1, null, null);
			if(registredepot.size()>0)
			{
				if(dossier.getDosLotDivisible().equals("NON")||dossier.getDosNombreLots()==1)
				  createLigneLots(dossier);
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
		}
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {


			GridLots.setRowRenderer(this);

			    List<SygLotsSoumissionnaires> valLignes = new ArrayList<SygLotsSoumissionnaires>();
			    List<SygPlisouvertures> plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,-1,-1,1, -1, -1, -1, null, -1, null, null);
							
			  for (SygPlisouvertures pli : plis) {
				  SygLotsSoumissionnaires categ=new SygLotsSoumissionnaires();
			      
			      categ.setPlilibelle(pli.getRetrait().getNomSoumissionnaire());
			       List<SygLotsSoumissionnaires> vals = BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).find(0, -1, dossier, pli, null,-1,"oui",-1,-1, -1, null);
			   	   if(vals.size()!=0)   
				     valLignes.add(categ);	
		 		       for (SygLotsSoumissionnaires valLigne : vals) {
			 		       valLignes.add(valLigne);
			 		      
				        }
		 		      GridLots.setModel(new ListModelList(valLignes));	
			  		
	
			  }
		     	
		} 
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MONNAIES)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgMonnaie.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgMonnaie.getActivePage() * byPageBandbox;
				pgMonnaie.setPageSize(byPageBandbox);
			}
			List<SygMonnaieoffre> monnaies = BeanLocator.defaultLookup(MonnaieoffreSession.class).ListeMonnaies(activePage, byPageBandbox,libellemonnaie,dossier.getDosID());
			lstMonnaie.setModel(new SimpleListModel(monnaies));
			pgMonnaie.setTotalSize(BeanLocator.defaultLookup(MonnaieoffreSession.class).ListeMonnaies(activePage, byPageBandbox,libellemonnaie,dossier.getDosID()).size());
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_NATURES)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgNature.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgNature.getActivePage() * byPageBandbox;
				pgNature.setPageSize(byPageBandbox);
			}
			List<SygNatureprix> natures = BeanLocator.defaultLookup(NatureprixSession.class).find(activePage, byPageBandbox,null,libellenature);
			lstNature.setModel(new SimpleListModel(natures));
			pgNature.setTotalSize(BeanLocator.defaultLookup(NatureprixSession.class).count(null,libellenature));
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_PAYS)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgPays.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgPays.getActivePage() * byPageBandbox;
				pgPays.setPageSize(byPageBandbox);
			}
			List<SygPays> pays = BeanLocator.defaultLookup(PaysSession.class).find(activePage, byPageBandbox,libellepays,null);
			lstPays.setModel(new SimpleListModel(pays));
			pgPays.setTotalSize(BeanLocator.defaultLookup(PaysSession.class).count(libellepays,null));
		}
		else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
			Button button = (Button) event.getTarget();
			String toDo = (String) button.getAttribute(UIConstants.TODO);
			lotsoumis = (SygLotsSoumissionnaires) button.getAttribute("lotsoumis");
			if (toDo.equalsIgnoreCase("modifier"))
			{
				step0.setVisible(false);
				step1.setVisible(true);
				txtnumplis.setValue(lotsoumis.getPlis().getNumero());
				txtRaisonsocial.setValue(lotsoumis.getPlilraisonsociale());
				monnaie=lotsoumis.getMonnaie();
				natureprix=dossier.getNatureprix();
				bdNature.setValue(natureprix.getNatLibelle());
				pays=lotsoumis.getPlis().getRetrait().getPays();
				if(pays!=null)
					bdPays.setValue(pays.getLibelle());
				if(monnaie!=null)
				 bdMonnaie.setValue(monnaie.getMonLibelle());
				if(natureprix!=null)
					bdNature.setValue(natureprix.getNatLibelle()); 
				//bdPays.setValue(value)
				dcMontant.setValue(lotsoumis.getPlilmontantoffert());
				rabais.setValue(lotsoumis.getPlilrabais());
				if(lotsoumis.getPlillettreSoumission()==1)
					rdsoumoui.setChecked(true);
				else
					rdsoumnon.setChecked(true);
				if(lotsoumis.getPliloffreFinanciere()==1)
					rdoffoui.setChecked(true);
				else
					rdoffnon.setChecked(true);
				if(lotsoumis.getPliloffreTechnique()==1)
					rdoftoui.setChecked(true);
				else
					rdoftnon.setChecked(true);
			}
		}

	}


	

	 
	   @Override
		public Row newRow(Grid grid) {
			// Create EditableRow instead of Row(default)
			Row row = new EditableRow();
			row.applyProperties();
			return row;
		}

		@Override
		public Component newCell(Row row) {
			return null;// Default Cell
		}

		@Override
		public int getControls() {
			return RowRendererExt.DETACH_ON_RENDER; // Default Value
		}

		@Override
		public void render(Row row, Object data, int index) throws Exception {
//			final SiapzoneaRisque zoneaRisque = (SiapzoneaRisque) data;
			final SygLotsSoumissionnaires lots = (SygLotsSoumissionnaires) data;
			
			final EditableRow editRow = (EditableRow) row;
			
			if (lots.getPlilibelle() != null) {
				final EditableDiv libelle =	new EditableDiv(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial")+":  " +lots.getPlilibelle(),false);
				
				libelle.setHeight("30px");
	 			libelle.setStyle("color:#000;font-size:14px;");
				libelle.txb.setReadonly(true);
				libelle.setParent(editRow);	
				
				final EditableDiv montant =	new EditableDiv(" ",false);
				montant.setAlign("center");
				montant.txb.setReadonly(true);
				montant.setParent(editRow);	
				
				final EditableDiv montantlu =	new EditableDiv(" ",false);
				montantlu.setParent(editRow);	
				
				final EditableDiv monnaie =	new EditableDiv(" ",false);
				monnaie.setParent(editRow);	
				
				final EditableDiv natureprix =	new EditableDiv(" ",false);
				natureprix.setParent(editRow);	
				
				final EditableDiv rabais =	new EditableDiv(" ",false);
				rabais.setParent(editRow);	
				
				final EditableDiv action =	new EditableDiv(" ",false);
				action.setParent(editRow);	
			}
			else
			{
			
			
			final EditableDiv libelle =	new EditableDiv(lots.getLot().getLibelle(),false);
			libelle.setAlign("left");
			libelle.setStyle("margin-left:10px;color:#000;");
			libelle.txb.setReadonly(true);
			libelle.setParent(editRow);	
			
			final EditableDiv garantie =	new EditableDiv(ToolKermel.format2Decimal(lots.getPlilmontantoffert()),false);
			garantie.setAlign("center");
			garantie.setStyle("font-weight:bold;color:green");
			garantie.txb.setReadonly(true);
			garantie.setParent(editRow);	
			
			final EditableDiv montant =	new EditableDiv(ToolKermel.format2Decimal(lots.getPlilmontantoffert()),false);
			montant.setAlign("center");
			montant.setStyle("font-weight:bold;color:green");
			montant.setParent(editRow);	
			
			String codemonnaie="",codenature="";
			if(lots.getMonnaie()!=null)
				codemonnaie=lots.getMonnaie().getMonCode();
			if(lots.getNatureprix()!=null)
				codenature=lots.getNatureprix().getNatCode();
			final EditableDiv monnaie =	new EditableDiv(codemonnaie,false);
			monnaie.setAlign("center");
			monnaie.setStyle("font-weight:bold;color:green");
			monnaie.setParent(editRow);	
			
			final EditableDiv natureprix =	new EditableDiv(codenature,false);
			natureprix.setAlign("center");
			natureprix.setStyle("font-weight:bold;color:green");
			natureprix.setParent(editRow);	
			
			final EditableDiv rabais =	new EditableDiv(Integer.toString(lots.getPlilrabais()),false);
			rabais.setAlign("center");
			rabais.setStyle("font-weight:bold;color:green");
			rabais.setParent(editRow);	
			
			final Div ctrlDiv = new Div();
			ctrlDiv.setParent(editRow);
			final Button editBtn = new Button(null, "/images/pencil-small.png");
			editBtn.setMold("os");
			editBtn.setHeight("20px");
			editBtn.setWidth("30px");
			editBtn.setAttribute("lotsoumis", lots);
			editBtn.setAttribute(UIConstants.TODO, "modifier");
			editBtn.addEventListener(Events.ON_CLICK, LecturesOffresControllers.this);
			if(appel.getApoDatepvouverturepli()!=null)
				editBtn.setDisabled(true);
			editBtn.setParent(ctrlDiv);
			editBtn.addEventListener(Events.ON_CLICK, new EventListener() {
				public void onEvent(Event event) throws Exception {
				
					
				}
			});
			}
		}
	

///////////Monnaies///////// 
	public void onSelect$lstMonnaie(){
		monnaie= (SygMonnaieoffre) lstMonnaie.getSelectedItem().getValue();
		bdMonnaie.setValue(monnaie.getMonLibelle());
		bdMonnaie.close();
	
	}
	
	public class MonnaiesRenderer implements ListitemRenderer{
	
	
	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygMonnaieoffre monnaie = (SygMonnaieoffre) data;
		item.setValue(monnaie);
		
		
		Listcell cellCode = new Listcell(monnaie.getMonCode());
		cellCode.setParent(item);
		
		Listcell cellLibelle = new Listcell(monnaie.getMonLibelle());
		cellLibelle.setParent(item);
	
		
	}
	}
	public void onFocus$txtRechercherMonnaie(){
	if(txtRechercherMonnaie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.devise.monnaie"))){
		txtRechercherMonnaie.setValue("");
	}		 
	}
	
	public void  onClick$btnRechercherMonnaie(){
	if(txtRechercherMonnaie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.devise.monnaie")) || txtRechercherMonnaie.getValue().equals("")){
		libellemonnaie = null;
		page=null;
	}else{
		libellemonnaie = txtRechercherMonnaie.getValue();
		page="0";
	}
	Events.postEvent(ApplicationEvents.ON_MONNAIES, this, page);
	}

///////////Natures Prix///////// 
	public void onSelect$lstNature(){
		natureprix= (SygNatureprix) lstNature.getSelectedItem().getValue();
	bdNature.setValue(natureprix.getNatLibelle());
	bdNature.close();
	
	}
	
	public class NaturesPrixRenderer implements ListitemRenderer{
	
	
	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygNatureprix nature = (SygNatureprix) data;
		item.setValue(nature);
		
		
		Listcell cellCode = new Listcell(nature.getNatCode());
		cellCode.setParent(item);
		
		Listcell cellLibelle = new Listcell(nature.getNatLibelle());
		cellLibelle.setParent(item);
		
		
		}
	}
	public void onFocus$txtRechercherNature(){
	if(txtRechercherNature.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.lectureoffres.natureprix"))){
		txtRechercherNature.setValue("");
	}		 
	}
	
	public void  onClick$btnRechercherNature(){
	if(txtRechercherNature.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.lectureoffres.natureprix")) || txtRechercherNature.getValue().equals("")){
	libellenature = null;
	page=null;
	}else{
		libellenature = txtRechercherNature.getValue();
	page="0";
	}
	Events.postEvent(ApplicationEvents.ON_NATURES, this, page);
	}
	
///////////Pays///////// 
	public void onSelect$lstPays(){
		pays= (SygPays) lstPays.getSelectedItem().getValue();
	bdPays.setValue(pays.getLibelle());
	bdPays.close();
	
	}
	
	public class PaysRenderer implements ListitemRenderer{
	
	
	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygPays pays = (SygPays) data;
		item.setValue(pays);
		
		Listcell cellLibelle = new Listcell(pays.getLibelle());
		cellLibelle.setParent(item);
		
		
		}
	}
	public void onFocus$txtRechercherPays(){
	if(txtRechercherPays.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.lectureoffres.pays"))){
		txtRechercherPays.setValue("");
	}		 
	}
	
	public void  onClick$btnRechercherPays(){
	if(txtRechercherPays.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.lectureoffres.pays")) || txtRechercherPays.getValue().equals("")){
	libellepays = null;
	page=null;
	}else{
		libellepays = txtRechercherPays.getValue();
	page="0";
	}
	Events.postEvent(ApplicationEvents.ON_PAYS, this, page);
	}
  
	public void  onClick$menuFermer(){
		step0.setVisible(true);
		step1.setVisible(false);
	}
	
	public void  onClick$menuValider(){
		if(checkFieldConstraints())
		{
			lotsoumis.setPlinumero(txtnumplis.getValue());
			lotsoumis.setMonnaie(monnaie);
			lotsoumis.setNatureprix(natureprix);
			lotsoumis.setPlilValide(UIConstants.PARENT);
			lotsoumis.setPlilmontantoffert(dcMontant.getValue());
			lotsoumis.setPlilmontantdefinitif(dcMontant.getValue());
			lotsoumis.setPlilsrixevalue(dcMontant.getValue());
			lotsoumis.setPlilEtatExamenPreliminaire(1);
			lotsoumis.setPlilEtatPreselection(1);
			lotsoumis.setPays(pays);
			if(rabais.getValue()!=null)
				lotsoumis.setPlilrabais(rabais.getValue());
			
			if(rdsoumoui.isChecked()==true)
				lotsoumis.setPlillettreSoumission(UIConstants.PARENT);
			else
				lotsoumis.setPlillettreSoumission(UIConstants.NPARENT);
			
			if(rdoffoui.isChecked()==true)
				lotsoumis.setPliloffreFinanciere(UIConstants.PARENT);
			else
				lotsoumis.setPliloffreFinanciere(UIConstants.NPARENT);
			
			if(rdoftoui.isChecked()==true)
				lotsoumis.setPliloffreTechnique(UIConstants.PARENT);
			else
				lotsoumis.setPliloffreTechnique(UIConstants.NPARENT);
			
			
			BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).update(lotsoumis);
			
			session.setAttribute("libelle", "lecturesoffres");
			loadApplicationState("suivi_procedure_passation");
		}
	}
	
private boolean checkFieldConstraints() {
		
		try {
		
			if(bdNature.getValue().equals(""))
		     {
               errorComponent = bdNature;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.devise.natureprix")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(bdMonnaie.getValue().equals(""))
		     {
              errorComponent = bdMonnaie;
              errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.devise.monnaie")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(bdPays.getValue().equals(""))
		     {
             errorComponent = bdPays;
             errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.devise.Pays")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dcMontant.getValue()==null)
		     {
             errorComponent = dcMontant;
             errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.devise.montantcfa")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			return true;
			
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

private void createLigneLots(SygDossiers dossier) {

    
	List<SygLots> lots = BeanLocator.defaultLookup(LotsSession.class).find(0,-1,dossier, null);
	 List<SygPlisouvertures> plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,-1,-1,-1, -1, -1, -1, null, -1, null, null);
	
	for (SygPlisouvertures pli : plis) {
		
		for (int i = 0; i < lots.size(); i++) {
			lotssoum=BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).find(0, -1, dossier, pli, lots.get(i),-1,null,-1,-1, -1, null);
			
			if(lotssoum.size()==0)
			{
				SygLotsSoumissionnaires lot = new SygLotsSoumissionnaires();
			
			//	lot.setPlilraisonsociale(pli.getRetrait().getNomSoumissionnaire());
				lot.setDossier(dossier);
				lot.setPlis(pli);
				lot.setLot(lots.get(i));
				lot.setPlilraisonsociale(pli.getRetrait().getNomSoumissionnaire());
				lot.setPliladresseMail(pli.getRetrait().getEmail());
				lot.setPlilclassementgeneral(0);
				lot.setPlilclassementechnique(0);
				lot.setPlilEtatPreselection(0);
				lot.setPlilEtatExamenPreliminaire(0);
				lot.setPlilcritereQualification(0);
				lot.setPlilattributaireProvisoire(0);
				lot.setPlilValide(0);
				lot.setPlilsrixevalue(new BigDecimal(0));
				lot.setPlilmontantoffert(new BigDecimal(0));
				lot.setPlilscoretechnique(new BigDecimal(0));
				lot.setPlilscorefinancier(new BigDecimal(0));
				lot.setPlilscoretechniquepondere(new BigDecimal(0));
				lot.setPlilscorefinancierpondere(new BigDecimal(0));
				lot.setPlilscorefinal(new BigDecimal(0));
				lot.setPlilmontantoffert(new BigDecimal(0));
				lot.setLotsoumis("oui");
				lot.setLotrecu("non");
				BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).save(lot);
			}
		}
	
		
	   }
	
	}
}
