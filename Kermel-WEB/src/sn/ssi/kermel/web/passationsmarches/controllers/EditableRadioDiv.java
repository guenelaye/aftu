package sn.ssi.kermel.web.passationsmarches.controllers;

import java.util.Date;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;





public class EditableRadioDiv extends Div {
    public static final String ON_EDITABLE = "onEditable";
    private String text="";
    private Date date = new Date();
    private boolean clickable = true;
    private boolean editable = false;
    private final boolean selected = false;
    public Textbox txb;
    Datebox datebox;
    Label lbl;
    Bandbox bandbox;
    Intbox intbox;
    public Radiogroup radiogroup;
    public Radio radio1;
    public Radio radio2;
    public Radio radioSelected;


    // Empty constructor will do the creation
    public EditableRadioDiv() {
	txb = new Textbox();
	lbl = new Label();
	txb.setWidth("99%");
	lbl.setWidth("99%");
	lbl.setMultiline(true);
	lbl.setParent(this);// Default show a label with text
	radiogroup = new Radiogroup();
	radio1 = new Radio("OUI");
	radio2 = new Radio("NON");
	
	radio1.setParent(radiogroup);
	radio2.setParent(radiogroup);
	
	datebox = new Datebox();

    }

    public EditableRadioDiv(String text, String component) {
	if (component.endsWith("Textbox")) {
	    txb = new Textbox();
	    lbl = new Label();
	    txb.setWidth("99%");
	    lbl.setWidth("99%");
	    lbl.setParent(this);// Default show a label with text
	} else if (component.endsWith("Datebox")) {
	    datebox = new Datebox();
	    lbl = new Label();
	    datebox.setWidth("99%");
	    lbl.setWidth("99%");
	    lbl.setParent(this);// Default show a label with text
	} else if (component.endsWith("BandBox")) {
	    bandbox = new Bandbox();
	    lbl = new Label();
	    txb.setWidth("99%");
	    lbl.setWidth("99%");
	    lbl.setParent(this);// Default show a label with text
	} else if (component.endsWith("IntBox")) {
	    bandbox = new Bandbox();
	    lbl = new Label();
	    txb.setWidth("99%");
	    lbl.setWidth("99%");
	    lbl.setParent(this);// Default show a label with text
	}
    }

    public EditableRadioDiv(String text) {
	this();
	setText(text);
	initEditCtrl();
    }

    public EditableRadioDiv(String text, boolean clickable) {
	this();
	setText(text);
	setClickable(clickable);
	initEditCtrl();
    }
    public EditableRadioDiv(Double text, boolean clickable) {
	this();
	setText(text.toString());
	setClickable(clickable);
	initEditCtrl();
    }
    // Getter and setters
    public void setText(String text) {
	this.text = text;
	txb.setValue(text);
	lbl.setValue(text.toString());
    }

    public String getText() {
	return text;
    }

    public void setClickable(boolean clickable) {
	this.clickable = clickable;
    }

    public boolean isClickable() {
	return this.clickable;
    }

    // Initialize the listener of the whole component and the textbox in it
    private void initEditCtrl() {
	this.addEventListener(ON_EDITABLE, new EventListener() {
	    @Override
	    public void onEvent(Event event) throws Exception {
		toggleEdit((Boolean) event.getData());
	    }
	});

	// This will turns the edit funtion on when click on label
	if (isClickable()) {
	    lbl.addEventListener(Events.ON_CLICK, new EventListener() {
		@Override
		public void onEvent(Event event) throws Exception {
		    txb.setFocus(true);
		    Events.postEvent(new Event(EditableRow.ON_EDIT,
			    EditableRadioDiv.this.getParent(), null));
		}
	    });
	}
    }

    // Replace textbox/label with label/textbox
    private void toggleEdit(boolean applyChange) {
	if (!editable) {
	    lbl.detach();
	    EditableRadioDiv.this.appendChild(radiogroup);
	} else {
	    radiogroup.detach();

	    if (applyChange) {// if apply changes then set the value in
		//			lbl.setValue(text = txb.getValue());
		if(radio1.isSelected()) {
		    radioSelected = radio1;
		    lbl.setValue("OUI");
		}else if(radio2.isSelected()){
		    radioSelected = radio2;
		    lbl.setValue("NON");
		}
		radioSelected.setSelected(true);
	    } else {
		radiogroup.setSelectedItem(radioSelected);

		if(radio1.isSelected()) {
		    radioSelected = radio1;
		    lbl.setValue("OUI");
		}else if(radio2.isSelected()){
		    radioSelected = radio2;
		    lbl.setValue("NON");
		}
		radioSelected.setSelected(true);
	    }
	    EditableRadioDiv.this.appendChild(lbl);
	}
	editable = !editable;
    }

    private void toggleEditDate(boolean applyChange) {
	if (!editable) {
	    lbl.detach();
	    EditableRadioDiv.this.appendChild(datebox);
	} else {
	    datebox.detach();
	    if (applyChange) {// if apply changes then set the value in
		lbl.setValue((date = datebox.getValue()).toString());
	    } else {
		datebox.setValue(date);
		lbl.setValue(date.toString());
	    }
	    EditableRadioDiv.this.appendChild(lbl);
	}
	editable = !editable;
    }

    private void toggleEditBandbox(boolean applyChange) {
	if (!editable) {
	    lbl.detach();
	    EditableRadioDiv.this.appendChild(datebox);
	} else {
	    datebox.detach();
	    if (applyChange) {// if apply changes then set the value in
		lbl.setValue((date = datebox.getValue()).toString());
	    } else {
		datebox.setValue(date);
		lbl.setValue(date.toString());
	    }
	    EditableRadioDiv.this.appendChild(lbl);
	}
	editable = !editable;
    }

    private void toggleEditRadion(boolean applyChange) {
	if (!editable) {
	    lbl.detach();
	    EditableRadioDiv.this.appendChild(radiogroup);
	} else {
	    radiogroup.detach();

	    if (applyChange) {// if apply changes then set the value in
		//			lbl.setValue(text = txb.getValue());
		if(radiogroup.getSelectedItem()!=null&&((String) radiogroup.getSelectedItem().getValue()).equalsIgnoreCase("OUI")) {
		    radioSelected = radio1;
		    lbl.setValue("OUI");
		}else if(radiogroup.getSelectedItem()!=null&&((String) radiogroup.getSelectedItem().getValue()).equalsIgnoreCase("NON")){
		    radioSelected = radio2;
		    lbl.setValue("NON");
		}
	    } else {
		radiogroup.setSelectedItem(radioSelected);

		if(radioSelected.getValue().equals(radio1.getValue()) ) {
		    lbl.setValue("OUI");
		} else {
		    lbl.setValue("NON");
		}
	    }
	    EditableRadioDiv.this.appendChild(lbl);
	}
	editable = !editable;
    }
}