package sn.ssi.kermel.web.passationsmarches.controllers;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class DossiersOuverturePlisController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code;
	private Tab TAB_infosgenerales,TAB_garanties,TAB_piecesadministratives,TAB_criteresqualifications,TAB_devise,TAB_financement;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblInfos;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygDossiers dossier=new SygDossiers();
	private int registreretrait,registredepot,membrescommissions,representantsoummissionnaires,representantservicestechniques,
	observateursindependants,piecessoumissionnaires,compositioncommissiontechnique,lecturesoffres,garantiessoumissions,criteresqualificationssoum,
	nombre=0;
	private Label lblregistreretrait,lblouvertureplis,lblregistredepot,lblouverture,lblpresencemembrescommissions,lblrepresentantssoumissionnaires,
	lblobservateursindependants,lblgarantiesoumission,lblincidents,lblcompositioncommissiontechnique,
	lblprocesverbalouverture,lbllecturesoffres,lblpiecessoumissionnaires,lblrepresentantsservicestechniques;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		dossier=(SygDossiers) Executions.getCurrent().getAttribute("dossier");
		if(dossier!=null)
		{
			registreretrait = (Integer) Executions.getCurrent().getAttribute("registreretrait");
			registredepot = (Integer) Executions.getCurrent().getAttribute("registredepot");
			membrescommissions = (Integer) Executions.getCurrent().getAttribute("membrescommissions");
			representantsoummissionnaires = (Integer) Executions.getCurrent().getAttribute("representantsoummissionnaires");
			representantservicestechniques = (Integer) Executions.getCurrent().getAttribute("representantservicestechniques");
			observateursindependants = (Integer) Executions.getCurrent().getAttribute("observateursindependants");
			piecessoumissionnaires = (Integer) Executions.getCurrent().getAttribute("piecessoumissionnaires");
			compositioncommissiontechnique = (Integer) Executions.getCurrent().getAttribute("compositioncommissiontechnique");
			lecturesoffres = (Integer) Executions.getCurrent().getAttribute("lecturesoffres");
			garantiessoumissions = (Integer) Executions.getCurrent().getAttribute("garantiessoumissions");
			criteresqualificationssoum = (Integer) Executions.getCurrent().getAttribute("criteresqualificationssoum");
			if(registreretrait>0)
			{
				lblouvertureplis.setStyle("color:#0066FF;font-size:20px; ");
				lblregistreretrait.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
			}
			if(registredepot>0)
			{
				lblregistredepot.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
			}
			if(membrescommissions>0)
			{
				lblouverture.setStyle("color:#0066FF;font-size:20px; ");
				lblpresencemembrescommissions.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				nombre=nombre+1;
			}
			if(representantsoummissionnaires>0)
			{
				lblouverture.setStyle("color:#0066FF;font-size:20px; ");
				lblrepresentantssoumissionnaires.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				nombre=nombre+1;
			}	
			if(representantservicestechniques>0)
			{
				lblouverture.setStyle("color:#0066FF;font-size:20px; ");
				lblrepresentantsservicestechniques.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				nombre=nombre+1;
			}	
			if(observateursindependants>0)
			{
				lblouverture.setStyle("color:#0066FF;font-size:20px; ");
				lblobservateursindependants.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				nombre=nombre+1;
			}
			if(piecessoumissionnaires>0)
			{
				lblouverture.setStyle("color:#0066FF;font-size:20px; ");
				lblpiecessoumissionnaires.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				nombre=nombre+1;
			}
			if(compositioncommissiontechnique>0)
			{
				lblouverture.setStyle("color:#0066FF;font-size:20px; ");
				lblcompositioncommissiontechnique.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				nombre=nombre+1;
			}
			if(lecturesoffres>0)
			{
				lblouverture.setStyle("color:#0066FF;font-size:20px; ");
				lbllecturesoffres.setStyle("color:#00FF00;;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				nombre=nombre+1;
			}
			if(garantiessoumissions>0)
			{
				lblouverture.setStyle("color:#0066FF;font-size:20px; ");
				lblgarantiesoumission.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				nombre=nombre+1;
			}
			if(appel.getApoDatepvouverturepli()!=null)
			{
				lblprocesverbalouverture.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				nombre=nombre+1;
			}
			if(dossier.getDosIncidents()!=null)
			{
				lblincidents.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
				nombre=nombre+1;
				 
			}
			if(nombre>=9)
			{
				lblouverture.setStyle("color:#00FF00;font-family: sans-serif; font-size: 20px;");
				lblouvertureplis.setStyle("color:#00FF00;font-family: sans-serif; font-size: 20px;");
			}
		}
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	
	public void onClick$lblregistreretrait() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		}
		else
		{
			if(dossier.getDosDatePublication()==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.publieravis"), "Erreur", Messagebox.OK, Messagebox.ERROR);

			}
			else
			{
				session.setAttribute("libelle", "registreretraitdao");
				loadApplicationState("suivi_procedure_passation");
			}
			
		}
	}
	
	public void onClick$lblregistredepot() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		}
		else
		{
			if(registreretrait==0)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.saisirregistreretrait"), "Erreur", Messagebox.OK, Messagebox.ERROR);

			}
			else
			{
				session.setAttribute("libelle", "plisouvertures");
				loadApplicationState("suivi_procedure_passation");
			}
			
		}
	}
	
	public void onClick$lblpresencemembrescommissions() {
		session.setAttribute("libelle", "listepresencemembrescommissions");
		loadApplicationState("suivi_procedure_passation");
	}
	public void onClick$lblrepresentantssoumissionnaires() {
		session.setAttribute("libelle", "representantssoumissionnaires");
		loadApplicationState("suivi_procedure_passation");
	}
	public void onClick$lblrepresentantsservicestechniques() {
		session.setAttribute("libelle", "representantsservicestechniques");
		loadApplicationState("suivi_procedure_passation");
	}
	public void onClick$lblobservateursindependants() {
		session.setAttribute("libelle", "observateursindependants");
		loadApplicationState("suivi_procedure_passation");
	}
	public void onClick$lblgarantiesoumission() {
		session.setAttribute("libelle", "garantiesoumission");
		loadApplicationState("suivi_procedure_passation");
	}
	public void onClick$lblpiecessoumissionnaires() {
		session.setAttribute("libelle", "piecessoumissionnaires");
		loadApplicationState("suivi_procedure_passation");
	}
	public void onClick$lblcompositioncommissiontechnique() {
		session.setAttribute("libelle", "compositioncommissiontechnique");
		loadApplicationState("suivi_procedure_passation");
	}
	public void onClick$lbllecturesoffres() {
		session.setAttribute("libelle", "lecturesoffres");
		loadApplicationState("suivi_procedure_passation");
	}
	public void onClick$lblprocesverbalouverture() {
		session.setAttribute("libelle", "procesverbalouverture");
		loadApplicationState("suivi_procedure_passation");
	}
	public void onClick$lblincidents() {
		session.setAttribute("libelle", "incidents");
		loadApplicationState("suivi_procedure_passation");
	}
}