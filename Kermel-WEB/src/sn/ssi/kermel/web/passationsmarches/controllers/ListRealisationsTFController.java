package sn.ssi.kermel.web.passationsmarches.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Column;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class ListRealisationsTFController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
     String page=null;
    private Listheader lshReference,lshLibelle,lshModepassation,lshDatelancement,lshDateattribution,lshDatedemarrage,lshDateachevement,lshMontant;
    Session session = getHttpSession();
    SygPlansdepassation plan=new SygPlansdepassation();
    private Long idplan,idrealisation;
    private Label lblInfos;
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_REALSATIONS, MOD_REALSATIONS, SUPP_REALSATIONS, WFER_REALSATIONS, WBAIL_REALSATIONS;
    String login;
    private Listbox lstListe;
    private Column idgride;
    private Combobox cbselect;
    private String filtrepar="service.libelle";
    private String idpar="service.id";
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
      
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.PARAM_REALSATIONS);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		
		if (ADD_REALSATIONS != null) { ADD_REALSATIONS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_REALSATIONS != null) { MOD_REALSATIONS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUPP_REALSATIONS != null) { SUPP_REALSATIONS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		if (WFER_REALSATIONS != null) { WFER_REALSATIONS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_CLOSE); }
		if (WBAIL_REALSATIONS != null) { WBAIL_REALSATIONS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_BAILLEURS); }
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_CLOSE, this);
		addEventListener(ApplicationEvents.ON_BAILLEURS, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	
		lshReference.setSortAscending(new FieldComparator("reference", false));
		lshReference.setSortDescending(new FieldComparator("reference", true));
		lshLibelle.setSortAscending(new FieldComparator("libelle", false));
		lshLibelle.setSortDescending(new FieldComparator("libelle", true));
		lshModepassation.setSortAscending(new FieldComparator("modepassation.libelle", false));
		lshModepassation.setSortDescending(new FieldComparator("modepassation.libelle", true));
		lshDatelancement.setSortAscending(new FieldComparator("datelancement", false));
		lshDatelancement.setSortDescending(new FieldComparator("datelancement", true));
		lshDateattribution.setSortAscending(new FieldComparator("dateattribution", false));
		lshDateattribution.setSortDescending(new FieldComparator("dateattribution", true));
		lshDatedemarrage.setSortAscending(new FieldComparator("datedemarrage", false));
		lshDatedemarrage.setSortDescending(new FieldComparator("datedemarrage", true));
		lshDateachevement.setSortAscending(new FieldComparator("dateachevement", false));
		lshDateachevement.setSortDescending(new FieldComparator("dateachevement", true));
		lshMontant.setSortAscending(new FieldComparator("montant", false));
		lshMontant.setSortDescending(new FieldComparator("montant", true));
		
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		
		lstListe.setItemRenderer(this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		login = ((String) getHttpSession().getAttribute("user"));
		
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygRealisations> realisations = BeanLocator.defaultLookup(RealisationsSession.class).find(activePage,byPage,null,null,null,null,-1, null,autorite, null, null);
			 SimpleListModel listModel = new SimpleListModel(realisations);
			 lstListe.setModel(listModel);
			 pgPagination.setTotalSize(BeanLocator.defaultLookup( RealisationsSession.class).count(null,null,null,null,-1, null,autorite, null, null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/plansdepassation/formrealisation.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"650px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ProcedureFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		

			
		
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygRealisations plans = (SygRealisations) data;
		item.setValue(plans.getIdrealisation());

		 Listcell cellReference = new Listcell(plans.getReference());
		 cellReference.setParent(item);
		 
		 Listcell cellLibelle = new Listcell(plans.getLibelle());
		 cellLibelle.setParent(item);
		 
		 Listcell cellTypemarche = new Listcell(plans.getTypemarche().getLibelle());
		 cellTypemarche.setParent(item);
		 
		 Listcell cellModepassation = new Listcell(plans.getModepassation().getLibelle());
		 cellModepassation.setParent(item);
		 
		 Listcell cellDateLancement = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDatelancement()));
		 cellDateLancement.setParent(item);
		 
		 Listcell cellDateattribution = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDateattribution()));
		 cellDateattribution.setParent(item);
		 
		 Listcell cellDatedemarrage = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDatedemarrage()));
		 cellDatedemarrage.setParent(item);
		 
		 Listcell cellDateachevement = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDateachevement()));
		 cellDateachevement.setParent(item);
		 
		 Listcell cellMontant = new Listcell(ToolKermel.format3Decimal(plans.getMontant()));
		 cellMontant.setParent(item);

		 
	}
	public void onSelect$cbselect(){
		 if(cbselect.getSelectedItem().getId().equals("service"))
		 {
			 filtrepar="service.libelle";
			 idpar="service.id";
			 idgride.setLabel(Labels.getLabel("kermel.plansdepassation.realisations.filtrepar.service"));
		 }
		 else
		 {
			 if(cbselect.getSelectedItem().getId().equals("typesmarches"))
			 {
				 filtrepar="type.libelle";
				 idpar="type.id";
				 idgride.setLabel(Labels.getLabel("kermel.plansdepassation.realisations.filtrepar.typemarche"));
			 }
			 else
			 {
				 if(cbselect.getSelectedItem().getId().equals("modespassations"))
				 {
					 filtrepar="mode.libelle";
					 idpar="mode.id";
					 idgride.setLabel(Labels.getLabel("kermel.plansdepassation.realisations.filtrepar.modepassation"));
				 }
				 else
				 {
					 filtrepar="service.libelle";
					 idpar="service.id";
					 idgride.setLabel(Labels.getLabel("kermel.plansdepassation.realisations.filtrepar.service"));
				 } 
			 } 
		 }
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	public void onClick$bchercher()
	{
		
	
		 if(cbselect.getSelectedItem().getId().equals("service"))
		 {
			 filtrepar="service.libelle";
			 idpar="service.id";
			 idgride.setLabel("Par service ou direction");
		 }
		 else
		 {
			 if(cbselect.getSelectedItem().getId().equals("typesmarches"))
			 {
				 filtrepar="type.libelle";
				 idpar="type.id";
				 idgride.setLabel("Par type de march�");
			 }
			 else
			 {
				 if(cbselect.getSelectedItem().getId().equals("modespassations"))
				 {
					 filtrepar="mode.libelle";
					 idpar="mode.id";
					 idgride.setLabel("Par mode de passation ");
				 }
				 else
				 {
					 filtrepar="service.libelle";
					 idpar="service.id";
					 idgride.setLabel("Par service ou direction");
				 } 
			 } 
		 }
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	

	public void onClick$menuFermer()
	{
		loadApplicationState("plans");
	}
	
	
}