package sn.ssi.kermel.web.passationsmarches.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierscommissionsmarches;
import sn.ssi.kermel.be.entity.SygMembresCommissionsMarches;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossierscommissionsmarchesSession;
import sn.ssi.kermel.be.referentiel.ejb.MembresCommissionsMarchesSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class CommissionsMarchesController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstCommissions;
	private Paging pgPagination,pgCommissions;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String nom=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
  	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	SygMembresCommissionsMarches membre=new SygMembresCommissionsMarches();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuAjouter,menuSupprimer;
	private Div step0,step1;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
    	
		addEventListener(ApplicationEvents.ON_MEMBRES, this);
		lstCommissions.setItemRenderer(new MembresRenderer());
		pgCommissions.setPageSize(byPage);
		pgCommissions.addForward("onPaging", this, ApplicationEvents.ON_MEMBRES);
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier==null)
		{
			menuAjouter.setDisabled(true);
			menuSupprimer.setDisabled(true);
		}
		else
		{
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			Events.postEvent(ApplicationEvents.ON_MEMBRES, this, null);
		}
		
	}
	
	@Override
	public void onEvent(final Event event) throws Exception {
     	if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygDossierscommissionsmarches> membres = BeanLocator.defaultLookup(DossierscommissionsmarchesSession.class).find(activePage,byPage,dossier,-1,-1);
			 lstListe.setModel(new SimpleListModel(membres));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(DossierscommissionsmarchesSession.class).count(dossier,-1,-1));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MEMBRES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgCommissions.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgCommissions.getActivePage() * byPage;
				pgCommissions.setPageSize(byPage);
			}
			 List<SygMembresCommissionsMarches> membres = BeanLocator.defaultLookup(MembresCommissionsMarchesSession.class).ListeMembres(activePage,byPage,nom,dossier.getDosID(),autorite.getId());
			 lstCommissions.setModel(new SimpleListModel(membres));
			 pgCommissions.setTotalSize(BeanLocator.defaultLookup(MembresCommissionsMarchesSession.class).ListeMembres(nom,dossier.getDosID(),autorite.getId()).size());
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = ((SygDossierscommissionsmarches)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue()).getId();
				BeanLocator.defaultLookup(DossierscommissionsmarchesSession.class).delete(codes);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygDossierscommissionsmarches membres = (SygDossierscommissionsmarches) data;
		item.setValue(membres);

		 Listcell cellNom= new Listcell(membres.getMembre().getNom());
		 cellNom.setParent(item);
		 
		 Listcell cellPrenom = new Listcell(membres.getMembre().getPrenom());
		 cellPrenom.setParent(item);
		 
		 Listcell cellTelephone = new Listcell(membres.getMembre().getTel());
		 cellTelephone.setParent(item);
		 
		 Listcell cellFonction = new Listcell(membres.getMembre().getFonction());
		 cellFonction.setParent(item);
		 
		 Listcell cellEmail = new Listcell(membres.getMembre().getEmail());
		 cellEmail.setParent(item);
		 
		
	}

	
	public class MembresRenderer implements ListitemRenderer{
		
		
		
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygMembresCommissionsMarches membres = (SygMembresCommissionsMarches) data;
		item.setValue(membres);

		 Listcell cellNom= new Listcell(membres.getNom());
		 cellNom.setParent(item);
		 
		 Listcell cellPrenom = new Listcell(membres.getPrenom());
		 cellPrenom.setParent(item);
		 
		 Listcell cellTelephone = new Listcell(membres.getTel());
		 cellTelephone.setParent(item);
		 
		 Listcell cellFonction = new Listcell(membres.getFonction());
		 cellFonction.setParent(item);
		 
		 Listcell cellEmail = new Listcell(membres.getEmail());
		 cellEmail.setParent(item);
	
	}
	}
	public void onClick$menuSupprimer()
	{
		if (lstListe.getSelectedItem() == null)
			
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		

		HashMap<String, String> display = new HashMap<String, String>(); // permet
		display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
		display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
		display.put(MessageBoxController.DSP_HEIGHT, "250px");
		display.put(MessageBoxController.DSP_WIDTH, "47%");

		HashMap<String, Object> map = new HashMap<String, Object>(); // permet
		map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
		showMessageBox(display, map);
		
	}
	public void onClick$menuAjouter()
	{
		step0.setVisible(false);
		step1.setVisible(true);
		Events.postEvent(ApplicationEvents.ON_MEMBRES, this, null);
	}
	public void onClick$menuFermer()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		
	}
	public void onClick$menuValider()
	{
       if (lstCommissions.getSelectedItem() == null)
			throw new WrongValueException(lstCommissions, Labels.getLabel("kermel.error.select.item"));
       
       for (int i = 0; i < lstCommissions.getSelectedCount(); i++) {
    	   SygDossierscommissionsmarches dossiermembre=new SygDossierscommissionsmarches();
    	   membre=(SygMembresCommissionsMarches) ((Listitem) lstCommissions.getSelectedItems().toArray()[i]).getValue();
    	   dossiermembre.setMembre(membre);
    	   dossiermembre.setDossier(dossier);
    	   BeanLocator.defaultLookup(DossierscommissionsmarchesSession.class).save(dossiermembre);
       }
       Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		step0.setVisible(true);
		step1.setVisible(false);
		
	}
}