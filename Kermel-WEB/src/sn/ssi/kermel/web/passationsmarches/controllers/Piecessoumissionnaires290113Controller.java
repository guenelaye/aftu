package sn.ssi.kermel.web.passationsmarches.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierscommissionsmarches;
import sn.ssi.kermel.be.entity.SygDossierspieces;
import sn.ssi.kermel.be.entity.SygMembresCommissionsMarches;
import sn.ssi.kermel.be.entity.SygPiecesplisouvertures;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersPiecesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.PiecessoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.traitementdossier.controllers.EditableDiv;
import sn.ssi.kermel.web.traitementdossier.controllers.EditableRow;

@SuppressWarnings("serial")
public class Piecessoumissionnaires290113Controller extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer,RowRenderer,RowRendererExt {

	private Listbox lstListe;
	private Paging pgPagination,pgListe;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String nom=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
  	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	SygMembresCommissionsMarches membre=new SygMembresCommissionsMarches();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuValider;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygDossierscommissionsmarches dossiermembre=new SygDossierscommissionsmarches();
	List<SygDossierscommissionsmarches> membres = new ArrayList<SygDossierscommissionsmarches>();
	private Grid grdListes;
	List<SygPiecesplisouvertures> soummissionnaires = new ArrayList<SygPiecesplisouvertures>();
	List<SygPlisouvertures> plis = new ArrayList<SygPlisouvertures>();
	List<SygDossierspieces> piecesrequises = new ArrayList<SygDossierspieces>();
	private Div step0,step1;
	SygPiecesplisouvertures piece=new SygPiecesplisouvertures();
	SygPlisouvertures pli=new SygPlisouvertures();
	List<SygPlisouvertures> registredepot = new ArrayList<SygPlisouvertures>();
	List<SygPiecesplisouvertures> pieces = new ArrayList<SygPiecesplisouvertures>();
	List<SygDossierspieces> piecesadministratives = new ArrayList<SygDossierspieces>();
	private String raisonsocial=null;
	private Textbox txtSoumissionnaire;
	
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		grdListes.setRowRenderer(this);
		
		
		addEventListener(ApplicationEvents.ON_PIECES, this);
		pgListe.setPageSize(byPage);
		pgListe.addForward("onPaging", this, ApplicationEvents.ON_PIECES);
		lstListe.setItemRenderer(this);
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		soummissionnaires=BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).find(0, -1, dossier,null,null,null);
		if(dossier==null)
		{
			menuValider.setDisabled(true);
			
		}
		else
		{
			piecesadministratives=BeanLocator.defaultLookup(DossiersPiecesSession.class).find(0,-1,dossier);
			registredepot= BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,-1,-1,-1, -1, -1,-1, null, -1, null, null);
			if(registredepot.size()==0||dossier.getDateRemiseDossierTechnique()!=null)
				menuValider.setDisabled(true);
			plis=BeanLocator.defaultLookup(RegistrededepotSession.class).find(0, -1, dossier, null, null, null,-1,-1,-1, -1, -1,-1, null, -1, null, null);
			if(soummissionnaires.size()==0)
			{
				
				piecesrequises=BeanLocator.defaultLookup(DossiersPiecesSession.class).find(0, -1, dossier);
				for (int i = 0; i < plis.size(); i++) {
					for (int k = 0; k < piecesrequises.size(); k++) {
						SygPiecesplisouvertures piecefournie=new SygPiecesplisouvertures();
						piecefournie.setDossier(dossier);
						piecefournie.setPlis(plis.get(i));
						piecefournie.setPiece(piecesrequises.get(k).getPiece());
						piecefournie.setEtat("NF");
						BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).save(piecefournie);
					}
				}
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			Events.postEvent(ApplicationEvents.ON_MEMBRES, this, null);
		}
		
		
	}
	
	@Override
	public void onEvent(final Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {


			grdListes.setRowRenderer(this);

			    List<SygPiecesplisouvertures> valLignes = new ArrayList<SygPiecesplisouvertures>();
			    List<SygPlisouvertures> plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,-1,-1,-1, -1, -1, -1, raisonsocial, -1, null, null);
							
			  for (SygPlisouvertures pli : plis) {
				  SygPiecesplisouvertures categ=new SygPiecesplisouvertures();
			      
			      categ.setLibelle(pli.getRetrait().getNomSoumissionnaire());
			       List<SygPiecesplisouvertures> vals = BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).find(0,-1,dossier,null,pli,null);;
			   	   if(vals.size()!=0)   
				     valLignes.add(categ);	
		 		       for (SygPiecesplisouvertures valLigne : vals) {
			 		       valLignes.add(valLigne);
			 		     
				        }
		 		    
		 		      grdListes.setModel(new ListModelList(valLignes));	
			  		
	
			  }
		     	
		} 
		
//		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
//			if (event.getData() != null) {
//				activePage = Integer.parseInt((String) event.getData());
//				byPage = -1;
//				pgPagination.setPageSize(1000);
//			} else {
//				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
//				activePage = pgPagination.getActivePage() * byPage;
//				pgPagination.setPageSize(byPage);
//			}
//			  List<SygDossierspieces> dossierspieces   = BeanLocator.defaultLookup(DossiersPiecesSession.class).find(activePage,byPage,dossier);
//			  grdListes.setModel(new SimpleListModel(dossierspieces));
//			  pgPagination.setTotalSize(BeanLocator.defaultLookup(DossiersPiecesSession.class).count(dossier));
//			  for (int i = 0; i < plis.size(); i++) {
//				  pli=plis.get(i);
//				  pieces=BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).find(0,-1,dossier,null,plis.get(i),"F");
//				  if(piecesadministratives.size()==pieces.size())
//				    pli.setPiecerequise(1) ;
//				  else
//					pli.setPiecerequise(0) ;
//				  BeanLocator.defaultLookup(RegistrededepotSession.class).update(pli);
//			  }
//		} 
//		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_PIECES)) {
//			if (event.getData() != null) {
//				activePage = Integer.parseInt((String) event.getData());
//				byPage = -1;
//				pgListe.setPageSize(1000);
//			} else {
//				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
//				activePage = pgListe.getActivePage() * byPage;
//				pgListe.setPageSize(byPage);
//			}
//			  List<SygPiecesplisouvertures> pieces   = BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).find(activePage,byPage,dossier,null,null,null);
//			  lstListe.setModel(new SimpleListModel(pieces));
//			  pgListe.setTotalSize(BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).count(dossier,null,null,null));
//			  
//		} 
//	

	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygPiecesplisouvertures soumissionnaires = (SygPiecesplisouvertures) data;
		item.setValue(soumissionnaires);

		 Listcell cellNom= new Listcell(soumissionnaires.getPlis().getRetrait().getNomSoumissionnaire());
		 cellNom.setParent(item);
		 
		 Listcell cellPiece = new Listcell(soumissionnaires.getPiece().getLibelle());
		 cellPiece.setParent(item);
		 
		 if(soumissionnaires.getEtat().equals("F"))
		   item.setSelected(true);
	}

	
	
	
	public void onClick$menuValider()
	{
       
       for (int k = 0; k < soummissionnaires.size(); k++) {
    	   piece=soummissionnaires.get(k);
    	   piece.setEtat("NF");
      	   BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).update(piece);
       }
       for (int i = 0; i < lstListe.getSelectedCount(); i++) {
    	 
    	   piece=(SygPiecesplisouvertures) ((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
    	   piece.setEtat("F");
    	   BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).update(piece);
       }
       session.setAttribute("libelle", "piecessoumissionnaires");
		loadApplicationState("suivi_procedure_passation");
		
	}

	
	 private class PiecesRenderer implements ListitemRenderer {
			
			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
				SygPiecesplisouvertures soumissionnaires = (SygPiecesplisouvertures) data;
				item.setValue(soumissionnaires);

				 Listcell cellNom = new Listcell(soumissionnaires.getPlis().getRetrait().getNomSoumissionnaire());
				 cellNom.setParent(item);
				 
				 Listcell cellEtatFournie = new Listcell("");
				 Listcell cellEtatNonFournie = new Listcell("");
				 if(soumissionnaires.getEtat().equals("F"))
				 {
					 cellEtatFournie.setImage("/images/ok.png");
					 cellEtatFournie.setAttribute(UIConstants.TODO, "details");
					 cellEtatFournie.setAttribute("soumissionnaires", soumissionnaires);
					 cellEtatFournie.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.piecesadministratives.fournie"));
					//	cellImageDetails.addEventListener(Events.ON_CLICK, ListRealisationsPIController.this);
				 }
				 else
				 {
					 cellEtatNonFournie.setImage("/images/delete.png");
					 cellEtatNonFournie.setAttribute(UIConstants.TODO, "details");
					 cellEtatNonFournie.setAttribute("soumissionnaires", soumissionnaires);
					 cellEtatNonFournie.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.piecesadministratives.noufournie"));
		
				 }
				 cellEtatFournie.setParent(item);
				 cellEtatNonFournie.setParent(item);
			
			}
	 }
	
	 
	
	 
	 public void onClick$menuModifier()
		{
		 step0.setVisible(false);
		 step1.setVisible(true);
		 Events.postEvent(ApplicationEvents.ON_PIECES, this, null);
		}
	 
	 public void onClick$menuFermer()
		{
		 step0.setVisible(true);
		 step1.setVisible(false);
		
		}
	 
	 public void  onClick$btnRechercher(){
	 		if((txtSoumissionnaire.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial")))||(txtSoumissionnaire.getValue().equals("")))
	 		 {
	 			raisonsocial=null;
	 		 }
	 		else
	 		 {
	 			raisonsocial=txtSoumissionnaire.getValue();
	 			
	 		 }
	 		
	 		
	 		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	 		}
	 	
	 	
	 	 public void onFocus$txtSoumissionnaire(){
	 			if(txtSoumissionnaire.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial"))){
	 				txtSoumissionnaire.setValue("");
	 			}		 
	 			}
	 		public void onBlur$txtSoumissionnaire(){
	 			if(txtSoumissionnaire.getValue().equalsIgnoreCase("")){
	 				txtSoumissionnaire.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial"));
	 			}		 
	 			}
	 		public void onOK$txtSoumissionnaire(){
	 			onClick$btnRechercher();	
	 		}
	 	
	 		public void onOK$btnRechercher(){
	 			onClick$btnRechercher();	
	 		}
	 		
	 		
	 		  @Override
	 		    public Row newRow(Grid grid) {
	 			// Create EditableRow instead of Row(default)
	 			Row row = new EditableRow();
	 			row.applyProperties();
	 			return row;
	 		    }

	 		    @Override
	 		    public Component newCell(Row row) {
	 			return null;// Default Cell
	 		    }



	 		    @Override
	 		    public int getControls() {
	 			return RowRendererExt.DETACH_ON_RENDER; // Default Value
	 		    }

	 		    @Override
	 		    public void render(Row row, Object data, int index) throws Exception {
	 			//		final SiapzoneaRisque zoneaRisque = (SiapzoneaRisque) data;
	 			final SygPiecesplisouvertures grilleana = (SygPiecesplisouvertures) data;

	 			final EditableRow editRow = (EditableRow) row;
	 			if (grilleana.getLibelle() != null) {
	 				final EditableDiv critere = new EditableDiv(grilleana.getLibelle(), false);
		 			critere.txb.setReadonly(true);
		 			critere.setAlign("left");
		 			critere.setHeight("40px");
		 			critere.setParent(editRow);
	 			}
	 			else
	 			{
	 			final EditableDiv critere = new EditableDiv(grilleana.getPiece().getLibelle(), false);
	 			critere.txb.setReadonly(true);
	 			critere.setStyle("margin-left:10px;color:#000;");
	 			critere.setAlign("left");
	 			critere.setHeight("40px");
	 			critere.setParent(editRow);

	 			String etat="";
	 			if(grilleana.getEtat().equals("F"))
	 			{
	 			    etat = "F";
	 			}
	 			else {
	 			    etat = "NF";
	 			}
	 		
	 			final EditableRadioDiv etatfiche =	new EditableRadioDiv(etat,false);
	 			etatfiche.setAlign("center");
	 			etatfiche.setStyle("font-weight:bold;color:green");
	 			if(etat.equalsIgnoreCase("F")){
	 			    etatfiche.radio1.setSelected(true);  
	 			}else  {
	 				etatfiche.radio2.setSelected(true);
	 			}
	 			etatfiche.setParent(editRow);	



	 			final Div ctrlDiv = new Div();
	 			ctrlDiv.setParent(editRow);
	 			final Button editBtn = new Button(null, "/images/pencil-small.png");
	 			editBtn.setMold("os");
	 			editBtn.setHeight("20px");
	 			editBtn.setWidth("30px");
	 			editBtn.setParent(ctrlDiv);
	 			// Button listener - control the editable of row
	 			editBtn.addEventListener(Events.ON_CLICK, new EventListener() {
	 			    @Override
	 			    public void onEvent(Event event) throws Exception {
	 				final Button submitBtn = (Button) new Button(null, "/images/tick-small.png");
	 				final Button cancelBtn = (Button) new Button(null, "/images/cross-small.png");

	 				submitBtn.setMold("os");
	 				submitBtn.setHeight("20px");
	 				submitBtn.setWidth("30px");
	 				submitBtn.setTooltiptext("Valider la saisie");
	 				submitBtn.addEventListener(Events.ON_CLICK, new EventListener() {
	 				    @Override
	 				    public void onEvent(Event event) throws Exception {

	 					editRow.toggleEditable(true);

	 					if(etatfiche.radio1.isChecked()){
	 					    grilleana.setEtat("F");
	 					}else{
	 					  grilleana.setEtat("NF"); 
	 					}
	 					 BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).update(grilleana);
	 				


	 					submitBtn.detach();
	 					cancelBtn.detach();
	 					editBtn.setParent(ctrlDiv);
	 				    }
	 				});

	 				cancelBtn.setMold("os");
	 				cancelBtn.setHeight("20px");
	 				cancelBtn.setWidth("30px");
	 				cancelBtn.setTooltiptext("Annuler la saisie");
	 				cancelBtn.addEventListener(Events.ON_CLICK, new EventListener() {
	 				    @Override
	 				    public void onEvent(Event event) throws Exception {
	 					editRow.toggleEditable(false);
	 					submitBtn.detach();
	 					cancelBtn.detach();
	 					editBtn.setParent(ctrlDiv);
	 				    }
	 				});
	 				submitBtn.setParent(ctrlDiv);
	 				cancelBtn.setParent(ctrlDiv);
	 				editRow.toggleEditable(true);
	 				editBtn.detach();
	 			    }
	 			});

	 		    }
	 		    }

}