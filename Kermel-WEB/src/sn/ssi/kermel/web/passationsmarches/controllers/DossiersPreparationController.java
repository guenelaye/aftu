package sn.ssi.kermel.web.passationsmarches.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossiersEvaluateurs;
import sn.ssi.kermel.be.entity.SygDossierscommissionsmarches;
import sn.ssi.kermel.be.entity.SygMontantsSeuils;
import sn.ssi.kermel.be.entity.SygObservateursIndependants;
import sn.ssi.kermel.be.entity.SygPiecesplisouvertures;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygPresenceouverture;
import sn.ssi.kermel.be.entity.SygRepresentantsServicesTechniques;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.referentiel.ejb.MontantsSeuilsSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class DossiersPreparationController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code,avalider="oui";
	private Tab TAB_infosgenerales,TAB_garanties,TAB_piecesadministratives,TAB_criteresqualifications,TAB_devise,TAB_financement;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblInfos;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygDossiers dossier=new SygDossiers();
	List<SygRetraitregistredao> registreretrait = new ArrayList<SygRetraitregistredao>();
	List<SygPlisouvertures> registredepot = new ArrayList<SygPlisouvertures>();
	List<SygDossierscommissionsmarches> membrescommissions = new ArrayList<SygDossierscommissionsmarches>();
	List<SygPresenceouverture> representantsoummissionnaires = new ArrayList<SygPresenceouverture>();
	List<SygRepresentantsServicesTechniques> representantservicestechniques = new ArrayList<SygRepresentantsServicesTechniques>();
	List<SygObservateursIndependants> observateursindependants = new ArrayList<SygObservateursIndependants>();
	List<SygPiecesplisouvertures> piecessoumissionnaires = new ArrayList<SygPiecesplisouvertures>();
	List<SygDossiersEvaluateurs> compositioncommissiontechnique = new ArrayList<SygDossiersEvaluateurs>();
	List<SygPlisouvertures> lecturesoffres = new ArrayList<SygPlisouvertures>();
	List<SygMontantsSeuils> seuils = new ArrayList<SygMontantsSeuils>();
	private Image imgmiseenvalidation,imgdemandepublication;
	private Label miseenvalidation,infosgenerales,lblgaranties,lblpiecesadministratives,lblcriteresqualifications,lbllots,lblfinancement,lbldevise,
	lblpreparation,lbldossier,miseenpublication,demandepublication;
	private int garanties,piecesadministratives,criteresqualifications,devises,bailleurs,lots;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		dossier=(SygDossiers) Executions.getCurrent().getAttribute("dossier");
		if(dossier!=null)
		{
			lblpreparation.setStyle("color:#0066FF;font-family: sans-serif; font-size: 16px;cursor:pointer");
			lbldossier.setStyle("color:#0066FF;font-family: sans-serif; font-size: 16px;cursor:pointer");
			infosgenerales.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
			
			lots= (Integer) Executions.getCurrent().getAttribute("lots");
			if(lots>0)
				lbllots.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
			
			garanties = (Integer) Executions.getCurrent().getAttribute("garanties");
			if(garanties>0)
				lblgaranties.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
			
			piecesadministratives = (Integer) Executions.getCurrent().getAttribute("piecesadministratives");
			if(piecesadministratives>0)
				lblpiecesadministratives.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
			
			criteresqualifications = (Integer) Executions.getCurrent().getAttribute("criteresqualifications");
			if(criteresqualifications>0)
				lblcriteresqualifications.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
			
			devises=(Integer) Executions.getCurrent().getAttribute("devises");
			if(devises>0)
				lbldevise.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
			
			bailleurs = (Integer) Executions.getCurrent().getAttribute("bailleurs");
			if(bailleurs>0)
				lblfinancement.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
			
			if(dossier.getDosDateMiseValidation()!=null)
			  {
				if(dossier.getDosDateValidation()==null)
					miseenvalidation.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.enattentevalidation"));
					else
					miseenvalidation.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validationdossier")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateValidation()));
				    miseenvalidation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
				
			  }
				
			if(dossier.getDosdatedemandepublication()!=null)
			{
				demandepublication.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
				
			}
			if(dossier.getDosDatePublication()!=null)
			{
				miseenpublication.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.publication.publie")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDatePublication()));
				miseenpublication.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
				
			}
			
			
				
			
		}
       seuils = BeanLocator.defaultLookup(MontantsSeuilsSession.class).find(0,-1, autorite.getType(), appel.getTypemarche(), appel.getModepassation(), null, null,UIConstants.TYPE_SEUILSRAPRIORI);
		
		if(seuils.size()>0)
		{
			if(seuils.get(0).getMontantinferieur()!=null)
			{
				if((appel.getApomontantestime().compareTo(seuils.get(0).getMontantinferieur())==1)||(appel.getApomontantestime().equals(seuils.get(0).getMontantinferieur())))
		
				{
					imgmiseenvalidation.setVisible(true);
					miseenvalidation.setVisible(true);
					avalider="oui";
					
					imgdemandepublication.setVisible(true);
					demandepublication.setVisible(true);
				}
				else
				{
					imgmiseenvalidation.setVisible(false);
					miseenvalidation.setVisible(false);
					avalider="non";
					
					imgdemandepublication.setVisible(false);
					demandepublication.setVisible(false);
				}
					
			}
			
		}
		if(lots>0&&garanties>0&&piecesadministratives>0&&criteresqualifications>0&&devises>0&&bailleurs>0)
		{
			lbldossier.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
			if(avalider.equals("oui"))
			{
				if(dossier.getDosDateMiseValidation()!=null&&dossier.getDosDatePublication()!=null)
					lblpreparation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
			}
			else
			{
				if(dossier.getDosDatePublication()!=null)
					lblpreparation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
			}
			
		}
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	public void onClick$miseenvalidation() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		}
		else
		{
			if(garanties==0)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirgaranties"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				if(piecesadministratives==0)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirpieces"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				}
				else
				{
					if(criteresqualifications==0)
					{
						 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisircriteres"), "Erreur", Messagebox.OK, Messagebox.ERROR);
							
					}
					else
					{
						if(devises==0)
						{
							 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirdevises"), "Erreur", Messagebox.OK, Messagebox.ERROR);
								
						}
						else
						{
							if(bailleurs==0)
							{
								 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirbailleurs"), "Erreur", Messagebox.OK, Messagebox.ERROR);
									
							}
							else
							{
								session.setAttribute("libelle", "validationdossier");
								loadApplicationState("suivi_procedure_passation");
							}
						}
					}
				}
			}
			
		}
	}
	
	public void onClick$miseenpublication() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		}
		else
		{
			if(avalider.equals("oui"))
			{
				if(dossier.getDosDateValidation()==null)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validerdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);

				}
				else
				{
					session.setAttribute("libelle", "publicationdossier");
					loadApplicationState("suivi_procedure_passation");
				}
			}
			else
			{
				session.setAttribute("libelle", "publicationdossier");
				loadApplicationState("suivi_procedure_passation");
			}
			
			
		}
	}
	
	public void onClick$infosgenerales() {
		session.setAttribute("libelle", "infogenerales");
		loadApplicationState("suivi_procedure_passation");
	}
	
	public void onClick$lblgaranties() {
		session.setAttribute("libelle", "garanties");
		loadApplicationState("suivi_procedure_passation");
	}
	
	public void onClick$lblpiecesadministratives() {
		session.setAttribute("libelle", "piecesadministratives");
		loadApplicationState("suivi_procedure_passation");
	}
	
	public void onClick$lblcriteresqualifications() {
		session.setAttribute("libelle", "criteresqualifications");
		loadApplicationState("suivi_procedure_passation");
	}
	
	public void onClick$lbldevise() {
		session.setAttribute("libelle", "devise");
		loadApplicationState("suivi_procedure_passation");
	}
	
	public void onClick$lblfinancement() {
		session.setAttribute("libelle", "financements");
		loadApplicationState("suivi_procedure_passation");
	}
	
	public void onClick$lbllots() {
		session.setAttribute("libelle", "allotissement");
		loadApplicationState("suivi_procedure_passation");
	}
}