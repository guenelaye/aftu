package sn.ssi.kermel.web.categindicsurvmulti.controllers;


import java.util.Map;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygCatIndSurvMulti;
import sn.ssi.kermel.be.session.CatIndSurvMultiSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class FormController extends AbstractWindow implements EventListener, AfterCompose {
	
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode ;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle, txtCode;
	Integer code;
	private	SygCatIndSurvMulti unite = new SygCatIndSurvMulti();
	
	

	@Override
	public void onEvent(Event event) throws Exception {
		
	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
			
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Integer) map.get(PARAM_WIDOW_CODE);
			unite = BeanLocator.defaultLookup(CatIndSurvMultiSession.class).findByCode(code);
			
			txtCode.setValue(unite.getCode());
			txtLibelle.setValue(unite.getLibelle());
			
		}
	}
	
	public void onOK() {
		
		unite.setCode(txtCode.getValue());
		unite.setLibelle(txtLibelle.getValue());  


		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			BeanLocator.defaultLookup(CatIndSurvMultiSession.class).save(unite);

		} 
		else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			BeanLocator.defaultLookup(CatIndSurvMultiSession.class).update(unite);
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();

	}

	
	


}
