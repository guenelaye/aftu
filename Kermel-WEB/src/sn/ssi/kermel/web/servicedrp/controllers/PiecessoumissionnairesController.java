package sn.ssi.kermel.web.servicedrp.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Detail;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.Separator;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierscommissionsmarches;
import sn.ssi.kermel.be.entity.SygDossierspieces;
import sn.ssi.kermel.be.entity.SygMembresCommissionsMarches;
import sn.ssi.kermel.be.entity.SygPiecesplisouvertures;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersPiecesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.PiecessoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class PiecessoumissionnairesController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer,RowRenderer {

	private Listbox lstListe;
	private Paging pgPagination,pgListe;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String nom=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
  	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	SygMembresCommissionsMarches membre=new SygMembresCommissionsMarches();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuValider;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygDossierscommissionsmarches dossiermembre=new SygDossierscommissionsmarches();
	List<SygDossierscommissionsmarches> membres = new ArrayList<SygDossierscommissionsmarches>();
	private Grid grdListes;
	List<SygPiecesplisouvertures> soummissionnaires = new ArrayList<SygPiecesplisouvertures>();
	List<SygPlisouvertures> plis = new ArrayList<SygPlisouvertures>();
	List<SygDossierspieces> piecesrequises = new ArrayList<SygDossierspieces>();
	private Div step0,step1;
	SygPiecesplisouvertures piece=new SygPiecesplisouvertures();
	SygPlisouvertures pli=new SygPlisouvertures();
	List<SygPiecesplisouvertures> pieces = new ArrayList<SygPiecesplisouvertures>();
	List<SygDossierspieces> piecesadministratives = new ArrayList<SygDossierspieces>();
	
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		grdListes.setRowRenderer(this);
		
		
		addEventListener(ApplicationEvents.ON_PIECES, this);
		pgListe.setPageSize(byPage);
		pgListe.addForward("onPaging", this, ApplicationEvents.ON_PIECES);
		lstListe.setItemRenderer(this);
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		soummissionnaires=BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).find(0, -1, dossier,null,null,null);
		if(dossier==null)
		{
			menuValider.setDisabled(true);
			
		}
		else
		{
			if(dossier.getDateRemiseDossierTechnique()!=null)
				menuValider.setDisabled(true);
			
			if(soummissionnaires.size()==0)
			{
				plis=BeanLocator.defaultLookup(RegistrededepotSession.class).find(0, -1, dossier, null, null, null,-1,-1,-1, -1, -1,-1, null, -1, null, null);
				piecesrequises=BeanLocator.defaultLookup(DossiersPiecesSession.class).find(0, -1, dossier);
				for (int i = 0; i < plis.size(); i++) {
					for (int k = 0; k < piecesrequises.size(); k++) {
						SygPiecesplisouvertures piecefournie=new SygPiecesplisouvertures();
						piecefournie.setDossier(dossier);
						piecefournie.setPlis(plis.get(i));
						piecefournie.setPiece(piecesrequises.get(k).getPiece());
						piecefournie.setEtat("NF");
						BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).save(piecefournie);
					}
				}
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			Events.postEvent(ApplicationEvents.ON_MEMBRES, this, null);
		}
		
		
	}
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			  List<SygDossierspieces> dossierspieces   = BeanLocator.defaultLookup(DossiersPiecesSession.class).find(activePage,byPage,dossier);
			  grdListes.setModel(new SimpleListModel(dossierspieces));
			  pgPagination.setTotalSize(BeanLocator.defaultLookup(DossiersPiecesSession.class).count(dossier));
			  for (int i = 0; i < plis.size(); i++) {
				  pli=plis.get(i);
				  pieces=BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).find(0,-1,dossier,null,plis.get(i),"F");
				  if(piecesadministratives.size()==pieces.size())
				    pli.setPiecerequise(1) ;
				  else
					pli.setPiecerequise(0) ;
				  BeanLocator.defaultLookup(RegistrededepotSession.class).update(pli);
			  }
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_PIECES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgListe.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgListe.getActivePage() * byPage;
				pgListe.setPageSize(byPage);
			}
			  List<SygPiecesplisouvertures> pieces   = BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).find(activePage,byPage,dossier,null,null,null);
			  lstListe.setModel(new SimpleListModel(pieces));
			  pgListe.setTotalSize(BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).count(dossier,null,null,null));
		} 
	
//		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
//			for (int i = 0; i < lstListe.getSelectedCount(); i++) {
//				Long codes = ((SygDossierscommissionsmarches)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue()).getId();
//				BeanLocator.defaultLookup(DossierscommissionsmarchesSession.class).delete(codes);
//			}
//			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
//		}
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygPiecesplisouvertures soumissionnaires = (SygPiecesplisouvertures) data;
		item.setValue(soumissionnaires);

		 Listcell cellNom= new Listcell(soumissionnaires.getPlis().getRetrait().getNomSoumissionnaire());
		 cellNom.setParent(item);
		 
		 Listcell cellPiece = new Listcell(soumissionnaires.getPiece().getLibelle());
		 cellPiece.setParent(item);
		 
		 if(soumissionnaires.getEtat().equals("F"))
		   item.setSelected(true);
	}

	
	
	
	public void onClick$menuValider()
	{
       
       for (int k = 0; k < soummissionnaires.size(); k++) {
    	   piece=soummissionnaires.get(k);
    	   piece.setEtat("NF");
      	   BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).update(piece);
       }
       for (int i = 0; i < lstListe.getSelectedCount(); i++) {
    	 
    	   piece=(SygPiecesplisouvertures) ((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
    	   piece.setEtat("F");
    	   BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).update(piece);
       }
       session.setAttribute("libelle", "piecessoumissionnaires");
		loadApplicationState("suivi_service_drp");
		
	}

	
	 private class PiecesRenderer implements ListitemRenderer {
			
			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
				SygPiecesplisouvertures soumissionnaires = (SygPiecesplisouvertures) data;
				item.setValue(soumissionnaires);

				 Listcell cellNom = new Listcell(soumissionnaires.getPlis().getRetrait().getNomSoumissionnaire());
				 cellNom.setParent(item);
				 
				 Listcell cellEtatFournie = new Listcell("");
				 Listcell cellEtatNonFournie = new Listcell("");
				 if(soumissionnaires.getEtat().equals("F"))
				 {
					 cellEtatFournie.setImage("/images/ok.png");
					 cellEtatFournie.setAttribute(UIConstants.TODO, "details");
					 cellEtatFournie.setAttribute("soumissionnaires", soumissionnaires);
					 cellEtatFournie.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.piecesadministratives.fournie"));
					//	cellImageDetails.addEventListener(Events.ON_CLICK, ListRealisationsPIController.this);
				 }
				 else
				 {
					 cellEtatNonFournie.setImage("/images/delete.png");
					 cellEtatNonFournie.setAttribute(UIConstants.TODO, "details");
					 cellEtatNonFournie.setAttribute("soumissionnaires", soumissionnaires);
					 cellEtatNonFournie.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.piecesadministratives.noufournie"));
		
				 }
				 cellEtatFournie.setParent(item);
				 cellEtatNonFournie.setParent(item);
			
			}
	 }
	 @Override
		public void render(Row row, Object data, int index) throws Exception {
			
		    SygDossierspieces piecesdossiers = (SygDossierspieces) data;
		    row.setValue(piecesdossiers.getId());

			Label cell0 = new Label(piecesdossiers.getPiece().getLibelle());
			cell0.setParent(row);
			
					
			Detail detail = new Detail();	
			List<SygPiecesplisouvertures> pieces = new ArrayList<SygPiecesplisouvertures>();
			
			pieces= BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).find(0, -1, dossier,piecesdossiers.getPiece(),null,null);
			
				
			Vbox vboxConnaissement = new Vbox();
			Separator separator0_1 = new Separator("horizontal");
			separator0_1.setParent(vboxConnaissement);
			vboxConnaissement.setStyle("width:100%;");
			
			//****************************//
			//Marchandises
			//****************************//
			Separator separator1_1 = new Separator("horizontal");
			separator1_1.setParent(vboxConnaissement);
			
			Groupbox groupboxRealisations = new Groupbox();
			Caption captionMarchandises = new Caption("");
			captionMarchandises.setParent(groupboxRealisations);
			groupboxRealisations.setStyle("width:96%; margin-left: 1%;margin-right: 1%;");
			
			Listbox listboxReleves = new Listbox();
			listboxReleves.setItemRenderer(new PiecesRenderer());
			
			Listhead listheadRealisations = new Listhead();
			
		
			
			Listheader headerReference = new Listheader(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial"));
			headerReference.setWidth("94%");
			headerReference.setParent(listheadRealisations);
			
		
			
			Listheader headerImageF = new Listheader("F");
			headerImageF.setWidth("3%");
			headerImageF.setParent(listheadRealisations);
			
			Listheader headerImageNF = new Listheader("NF");
			headerImageNF.setWidth("3%");
			headerImageNF.setParent(listheadRealisations);
			
			listheadRealisations.setParent(listboxReleves);
			
			
			if(pieces!=null)
				listboxReleves.setModel(new SimpleListModel(pieces));
			
			listboxReleves.setParent(groupboxRealisations);
			groupboxRealisations.setParent(vboxConnaissement);
			
			Separator separator2_1 = new Separator("horizontal");
			separator2_1.setParent(vboxConnaissement);
			
			
			vboxConnaissement.setParent(detail);
			detail.setParent(row);
		}
	
	 
	 public void onClick$menuModifier()
		{
		 step0.setVisible(false);
		 step1.setVisible(true);
		 Events.postEvent(ApplicationEvents.ON_PIECES, this, null);
		}
	 
	 public void onClick$menuFermer()
		{
		 step0.setVisible(true);
		 step1.setVisible(false);
		
		}
}