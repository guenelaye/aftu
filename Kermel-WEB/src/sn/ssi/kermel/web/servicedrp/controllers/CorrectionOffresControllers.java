package sn.ssi.kermel.web.servicedrp.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygLotsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.LotsSoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class CorrectionOffresControllers extends AbstractWindow
		implements AfterCompose, EventListener,RowRenderer, RowRendererExt {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String mode, libDist, libEnqueteur, libMarche;
	int idProjet;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private final int byPage1 = 5;
	private Object selobj;
	// private Session session = getHttpSession();
	Grid GridLots;
	Paging pgPagination, pgDist, pgEnqueteur, pgMarche;
	Listbox lstDist, listEnqueteur, listMarche;
	Integer idFiche, distID, enqueteurID, marcheID;
	Textbox txtnumero, txtstatu, txtDrs, txtDist, anneemoi, txtRechercherDist,
			txtRechercherEnqueteur, txtObservation;
	Label nbre, lblMarche, lblEnqueteur, lblDate, lblRegion, lblDepartement,
			lblCampagne, lblSup1, lblSup2;
	Datebox datecree;
	Intbox nbretotalfs, nbretotalconsmois;
	Bandbox bdservice, bdDist, bdFmnsist, bdEnqueteur, bdMarche;
	Button btnGenerer, btnRechercherDist, btnRechercherMarcher,
			btnRechercherEnqueteur;
	Menuitem menuPreviousStep1;
	// Prodistrict district;
	// Proregionmedical regMed;
	private Div step1;
	private Iframe iframePrint;
	private boolean estGenere = false;
	private String datejour = UtilVue.getInstance().formateLaDate(new Date());
	private int annee = Integer.parseInt(ToolKermel.getAnneeCourante()),nombreexamen=0;
	private String mois = datejour.substring(3, 5);
	Session session = getHttpSession();
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	SygDossiers dossier=new SygDossiers();
    SygAutoriteContractante autorite;
	List<SygLotsSoumissionnaires> lotssoum = new ArrayList<SygLotsSoumissionnaires>();

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
	


	}

	public void onCreate(final CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier!=null)
		{
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {


			GridLots.setRowRenderer(this);

			    List<SygLotsSoumissionnaires> valLignes = new ArrayList<SygLotsSoumissionnaires>();
			    List<SygPlisouvertures> plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,1,-1,-1, -1, -1, -1, null, -1, null, null);
							
			  for (SygPlisouvertures pli : plis) {
				  nombreexamen=0;
				  SygLotsSoumissionnaires categ=new SygLotsSoumissionnaires();
			      
			      categ.setPlilibelle(pli.getRetrait().getNomSoumissionnaire());
			       List<SygLotsSoumissionnaires> vals = BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).find(0, -1, dossier, pli, null,1,"oui",-1,-1, -1, null);
			   	   if(vals.size()!=0)   
				     valLignes.add(categ);	
		 		       for (SygLotsSoumissionnaires valLigne : vals) {
			 		       valLignes.add(valLigne);
			 		      if(valLigne.getPlilEtatExamenPreliminaire()==1)
			 		    	 nombreexamen=nombreexamen+1;
				        }
//		 		       if(nombreexamen==vals.size())
//		 		         pli.setEtatExamenPreliminaire(1);
//		 		       else
//		 		    	 pli.setEtatExamenPreliminaire(0);
//		 		       BeanLocator.defaultLookup(RegistrededepotSession.class).update(pli);
		 		      GridLots.setModel(new ListModelList(valLignes));	
			  		
	
			  }
		     	
		} 
		
								

	

		

	}


	

	 
	   @Override
		public Row newRow(Grid grid) {
			// Create EditableRow instead of Row(default)
			Row row = new EditableRow();
			row.applyProperties();
			return row;
		}

		@Override
		public Component newCell(Row row) {
			return null;// Default Cell
		}

		@Override
		public int getControls() {
			return RowRendererExt.DETACH_ON_RENDER; // Default Value
		}

		@Override
		public void render(Row row, Object data, int index) throws Exception {
//			final SiapzoneaRisque zoneaRisque = (SiapzoneaRisque) data;
			final SygLotsSoumissionnaires lots = (SygLotsSoumissionnaires) data;
			
			final EditableRow editRow = (EditableRow) row;
			
			if (lots.getPlilibelle() != null) {
				final EditableDiv libelle =	new EditableDiv(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial")+":  " +lots.getPlilibelle(),false);
				
				libelle.setHeight("30px");
	 			libelle.setStyle("color:#000;");
				libelle.txb.setReadonly(true);
				libelle.setParent(editRow);	
				
				final EditableDiv garantie =	new EditableDiv(" ",false);
				garantie.setAlign("center");
				garantie.txb.setReadonly(true);
				garantie.setParent(editRow);	
				
				final EditableDiv etat =	new EditableDiv(" ",false);
				etat.setAlign("center");
				etat.txb.setReadonly(true);
				etat.setParent(editRow);	
				
				final EditableDiv action =	new EditableDiv(" ",false);
				action.setAlign("center");
				action.txb.setReadonly(true);
				action.setParent(editRow);	
			}
			else
			{
			
			
			final EditableDiv libelle =	new EditableDiv(lots.getLot().getLibelle(),false);
			libelle.setAlign("left");
			libelle.setStyle("margin-left:10px;color:#000;");
			libelle.txb.setReadonly(true);
			libelle.setParent(editRow);	
			
			final EditableDiv garantie =	new EditableDiv(ToolKermel.format2Decimal(lots.getLot().getMontant()),false);
			garantie.setAlign("center");
			garantie.setStyle("font-weight:bold;color:green");
			garantie.txb.setReadonly(true);
			garantie.setParent(editRow);
			
			final EditableDiv montant =	new EditableDiv(ToolKermel.format2Decimal(lots.getPlilsrixevalue()),false);
			montant.setAlign("center");
			montant.setStyle("font-weight:bold;color:green");
			montant.setParent(editRow);	
			
		
		   	
	   	
			final Div ctrlDiv = new Div();
			ctrlDiv.setParent(editRow);
			final Button editBtn = new Button(null, "/images/pencil-small.png");
			editBtn.setMold("os");
			editBtn.setHeight("20px");
			editBtn.setWidth("30px");
//			if(dossier.getDosLotDivisible().equals("NON"))
//				editBtn.setDisabled(true);
	
			editBtn.setParent(ctrlDiv);
			// Button listener - control the editable of row
			editBtn.addEventListener(Events.ON_CLICK, new EventListener() {
				public void onEvent(Event event) throws Exception {
					final Button submitBtn = (Button) new Button(null, "/images/tick-small.png");
					final Button cancelBtn = (Button) new Button(null, "/images/cross-small.png");

					submitBtn.setMold("os");
					submitBtn.setHeight("20px");
					submitBtn.setWidth("30px");
					submitBtn.setTooltiptext("Valider la saisie");
					submitBtn.addEventListener(Events.ON_CLICK, new EventListener() {
						public void onEvent(Event event) throws Exception {

							editRow.toggleEditable(true);


							lots.setPlilsrixevalue(new BigDecimal(Long.parseLong(montant.txb.getValue().replace(" ", ""))));
							BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).update(lots);
							submitBtn.detach();
							cancelBtn.detach();
							editBtn.setParent(ctrlDiv);
							session.setAttribute("libelle", "correctionsoffres");
	 						loadApplicationState("suivi_service_drp");
						}
					});
					montant.addEventListener(Events.ON_OK, new EventListener() {
	 					public void onEvent(Event event) throws Exception {

	 						editRow.toggleEditable(true);
	 						
	 						lots.setPlilsrixevalue(new BigDecimal(Long.parseLong(montant.txb.getValue().replace(" ", ""))));
							BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).update(lots);
	 						submitBtn.detach();
	 						cancelBtn.detach();
	 						editBtn.setParent(ctrlDiv);
	 						session.setAttribute("libelle", "correctionsoffres");
	 						loadApplicationState("suivi_service_drp");
	 					}
	 				});
					cancelBtn.setMold("os");
					cancelBtn.setHeight("20px");
					cancelBtn.setWidth("30px");
					cancelBtn.setTooltiptext("Annuler la saisie");
					cancelBtn.addEventListener(Events.ON_CLICK, new EventListener() {
						public void onEvent(Event event) throws Exception {
							editRow.toggleEditable(false);
							submitBtn.detach();
							cancelBtn.detach();
							editBtn.setParent(ctrlDiv);
						}
					});
					submitBtn.setParent(ctrlDiv);
					cancelBtn.setParent(ctrlDiv);
					editRow.toggleEditable(true);
					editBtn.detach();
				}
			});
			}
		}
	

		
	   
}
