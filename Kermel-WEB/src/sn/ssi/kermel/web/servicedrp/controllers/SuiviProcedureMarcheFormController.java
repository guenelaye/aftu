package sn.ssi.kermel.web.servicedrp.controllers;


import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAttributions;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygGarantiesDossiers;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygMontantsSeuils;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;
import sn.ssi.kermel.be.entity.SygTachesEffectues;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.ContratsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.LotsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.be.referentiel.ejb.MontantsSeuilsSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



/**

 * @author Adama samb
 * @since mardi 11 Janvier 2011, 12:34
 
 */
public class SuiviProcedureMarcheFormController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private Include pgActes;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	Session session = getHttpSession();
	private Label lblObjets,lblModePassation,lblMontant,lblModeSelection,lblType,lgtitre,lblDatecreation;
	private String LibelleTab,libelle,avalider="oui";
	private Tab TAB_TDOSSIERS;
	private Tree tree;
	private Treecell celltdossiers,cellconsultationfournisseur,cellpreparation,cellselectionfournisseur,celllettreinvitation,cellenrgistrementouvertureplis,
	cellregistredepot,celllotssoummissionnaires,celllistepresencemembrescommissions,cellrepresentantssoumissionnaires,cellrepresentantsservicestechniques,
	cellobservateursindependants,cellpiecessoumissionnaires,celllecturesoffres,cellprocesverbalouverture,cellcompositioncommissiontechnique,cellincidents,
	cellouvertureplis,cellattributionprovisoire,cellnotification,cellevaluationsoffre,cellverificationconformite,cellvercriteresqualification,cellcorrectionoffre,
	cellevaluationattribution,publicationattributionprovisoire,cellimmatriculation;
	private Include idinclude;
	private Treeitem treetdossiers,treeconsultationfournisseur,treeevaluationattribution,treeenrgistrementouvertureplis,treepreparation,treeselectionfournisseur,
	treelettreinvitation,treeregistredepot,treeouvertureplis,treelistepresencemembrescommissions,treerepresentantssoumissionnaires,treerepresentantsservicestechniques,
	treeobservateursindependants,treepiecessoumissionnaires,treelecturesoffres,treeprocesverbalouverture,treecompositioncommissiontechnique,treeincidents,
	treelotssoummissionnaires,treeevaluationsoffres,treeattributionprovisoire,treenotification,treeverificationconformite,treecorrectionoffre,vercriteresqualification,
	treepublicationattributionprovisoire,itemimmatriculation,treerecours,treedocuments,treecourriers,treeevenements;
	private Treerow rowinfosgenerales;
	private Treeitem treeitem;
	List<SygGarantiesDossiers> garanties = new ArrayList<SygGarantiesDossiers>();
	
	
	
	List<SygRealisationsBailleurs> bailleurs = new ArrayList<SygRealisationsBailleurs>();
	List<SygRetraitregistredao> registreretrait = new ArrayList<SygRetraitregistredao>();
	
	
	

	List<SygMontantsSeuils> seuils = new ArrayList<SygMontantsSeuils>();
    SygAutoriteContractante autorite=new SygAutoriteContractante();
    SygPlisouvertures soumissionnaires=new SygPlisouvertures();
    List<SygDocuments> documents = new ArrayList<SygDocuments>();
	SygDossiers dossier=new SygDossiers();
	private int nbredossierap=0,nbrefournselectionnes=0,nbrelettreinvitation=0,nbredepot=0,nbreop=0,nbreeoff=0,nbreeap=0,nbreautres=0,nbretotal=0
			,nbreevaluationpliss=0,nombreimmatriculation=0;
	SygAttributions attributaire=new SygAttributions();
	List<SygLots> lots = new ArrayList<SygLots>();
	
	List<SygPlisouvertures> plis = new ArrayList<SygPlisouvertures>();
	
	SygContrats contrat=new SygContrats();
	SygTachesEffectues tache=null;
	 
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		


	}

	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		LibelleTab=(String) session.getAttribute("libelle");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		
		lblObjets.setValue(appel.getApoobjet());
		lblModePassation.setValue(appel.getModepassation().getLibelle());
		lblMontant.setValue(ToolKermel.format2Decimal(appel.getApomontantestime()));
		lblModeSelection.setValue(appel.getModeselection().getLibelle());
		lblType.setValue(appel.getTypemarche().getLibelle());
		
		lblDatecreation.setValue(UtilVue.getInstance().formateLaDate2(appel.getApodatecreation()));
		
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		
		seuils = BeanLocator.defaultLookup(MontantsSeuilsSession.class).find(0,-1, autorite.getType(), appel.getTypemarche(), appel.getModepassation(), null, null,UIConstants.TYPE_SEUILSRAPRIORI);
		
		if(seuils.size()>0)
		{
			if(seuils.get(0).getMontantinferieur()!=null)
			{
				if((appel.getApomontantestime().compareTo(seuils.get(0).getMontantinferieur())==1)||(appel.getApomontantestime().equals(seuils.get(0).getMontantinferieur())))
		
				{
					avalider="oui";
				//	itemvalidationdossier.setVisible(false);
				}
				else
				{
					avalider="non";
				}
					
			}
			
		}
		if(dossier!=null)
		{
			tache=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Taches(dossier.getDosID());
			session.setAttribute("tache",tache);
			celltdossiers.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			cellpreparation.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			cellconsultationfournisseur.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			celltdossiers.setImage("/images/puce.png");
			cellpreparation.setImage("/images/puce.png");
			cellconsultationfournisseur.setImage("/images/puce.png");
			
			
			
			lots = BeanLocator.defaultLookup(LotsSession.class).find(0,-1,dossier,null);
			
			
			
			plis=BeanLocator.defaultLookup(RegistrededepotSession.class).find(0, -1, dossier, null, null, null, 1, -1,-1, -1, -1, -1, null, -1, null, null);
			if(tache.getPieceadministrative()==1)
				nbredossierap=nbredossierap+ 1;
			if(tache.getCriterequalification()==1)
				nbredossierap=nbredossierap+ 1;
			if(tache.getDevise()==1)
				nbredossierap=nbredossierap+ 1;
			if(tache.getAllotissement()==1)
				nbredossierap=nbredossierap+ 1;
			if(tache.getPreselection()==1)
			{
				nbrefournselectionnes=nbrefournselectionnes+1;
				cellselectionfournisseur.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellselectionfournisseur.setImage("/images/rdo_on.png");

			}
			if((dossier.getDosNombreLots()>0)&&dossier.getDosLotDivisible().equals("OUI"))
				treelotssoummissionnaires.setVisible(true);
			else
				treelotssoummissionnaires.setVisible(false);
			
			
		
		 if(nbredossierap==4)
				{
					cellconsultationfournisseur.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellconsultationfournisseur.setImage("/images/rdo_on.png");
				}
			
			if(dossier.getDosdateinvitation()!=null)
			{
				celllettreinvitation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				nbrelettreinvitation=nbrelettreinvitation+1;
				celllettreinvitation.setImage("/images/rdo_on.png");
			}
			
			if(nbredossierap+nbrefournselectionnes+nbrelettreinvitation==6)
			{
				cellpreparation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellpreparation.setImage("/images/rdo_on.png");
				nbretotal=nbretotal+6;
			}
			if(tache.getRegistredepot()==1)
			{
				cellregistredepot.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellregistredepot.setImage("/images/rdo_on.png");
				cellenrgistrementouvertureplis.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellenrgistrementouvertureplis.setImage("/images/puce.png");
				nbredepot=nbredepot+1;
			}
			if(tache.getLotssoumis()==1)
			{	
				celllotssoummissionnaires.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellouvertureplis.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
					cellouvertureplis.setImage("/images/puce.png");
					celllotssoummissionnaires.setImage("/images/rdo_on.png");
					nbreop=nbreop+1;
				
			}
			if(tache.getCommissionspassation()==1)
			{
				celllistepresencemembrescommissions.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureplis.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				celllistepresencemembrescommissions.setImage("/images/rdo_on.png");
				cellouvertureplis.setImage("/images/puce.png");
				nbreop=nbreop+1;
			}
			if(tache.getRepresentantssoumis()==1)
			{
				cellrepresentantssoumissionnaires.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureplis.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellrepresentantssoumissionnaires.setImage("/images/rdo_on.png");
				cellouvertureplis.setImage("/images/puce.png");
				nbreop=nbreop+1;
			}
			if(tache.getServicestechniques()==1)
			{
				cellrepresentantsservicestechniques.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureplis.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellrepresentantsservicestechniques.setImage("/images/rdo_on.png");
				cellouvertureplis.setImage("/images/puce.png");
				nbreop=nbreop+1;
			}
			if(tache.getObservateurs()==1)
			{
				cellobservateursindependants.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureplis.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellobservateursindependants.setImage("/images/rdo_on.png");
				cellouvertureplis.setImage("/images/puce.png");
				nbreop=nbreop+1;
			}
			if(tache.getPiecessoumis()==1)
			{
				nbreop=nbreop+1;
				cellpiecessoumissionnaires.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureplis.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellpiecessoumissionnaires.setImage("/images/rdo_on.png");
				cellouvertureplis.setImage("/images/puce.png");
			}
			if(tache.getLectureoffres()==1)
			{
				nbreop=nbreop+1;
				celllecturesoffres.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureplis.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				celllecturesoffres.setImage("/images/rdo_on.png");
				cellouvertureplis.setImage("/images/puce.png");
			}
			if(appel.getApoDatepvouverturepli()!=null)
			{
				nbreop=nbreop+1;
				cellprocesverbalouverture.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureplis.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellprocesverbalouverture.setImage("/images/rdo_on.png");
				cellouvertureplis.setImage("/images/puce.png");
			}
			if(tache.getCommissiontecniques()==1)
			{
				nbreop=nbreop+1;
				cellcompositioncommissiontechnique.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureplis.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellcompositioncommissiontechnique.setImage("/images/rdo_on.png");
				cellouvertureplis.setImage("/images/puce.png");
			}
			
			if((dossier.getDosNombreLots()>0)&&dossier.getDosLotDivisible().equals("OUI"))
			{
				if(nbreop==9)
				{
					cellincidents.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellouvertureplis.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellincidents.setImage("/images/rdo_on.png");
					cellouvertureplis.setImage("/images/rdo_on.png");
				}
				if(nbreop+nbredepot==10)
				{
					cellenrgistrementouvertureplis.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellenrgistrementouvertureplis.setImage("/images/rdo_on.png");
					nbretotal=nbretotal+10;
				}
					
			}
			else
			{
				if(nbreop==8)
				{
					cellincidents.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellouvertureplis.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellincidents.setImage("/images/rdo_on.png");
					cellouvertureplis.setImage("/images/rdo_on.png");
				}
				if(nbreop+nbredepot==9)
				{
					cellenrgistrementouvertureplis.setImage("/images/rdo_on.png");
					cellenrgistrementouvertureplis.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					nbretotal=nbretotal+9;
				}
					
			}
			
			if(plis.size()>0)
			{
				nbreeoff=nbreeoff+1;
				cellverificationconformite.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellevaluationsoffre.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellverificationconformite.setImage("/images/rdo_on.png");
				cellevaluationsoffre.setImage("/images/puce.png");
				cellevaluationattribution.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellevaluationattribution.setImage("/images/puce.png");
			}
			if(tache.getVerificationcritere()==1)
			{
				nbreeoff=nbreeoff+ 1;
				cellvercriteresqualification.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellvercriteresqualification.setImage("/images/rdo_on.png");
				cellevaluationattribution.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellevaluationattribution.setImage("/images/puce.png");
			}
			if(nbreeoff>=2)
			{
				cellevaluationsoffre.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellevaluationsoffre.setImage("/images/rdo_on.png");
				cellcorrectionoffre.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellcorrectionoffre.setImage("/images/rdo_on.png");
			}
			if(tache.getAttributionprovisoire()==1)
			{
				nbreeap=nbreeap+ 1;
				cellattributionprovisoire.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellattributionprovisoire.setImage("/images/rdo_on.png");
				
			}
			if(dossier.getDosDatePublicationProvisoire()!=null)
			{
				nbreeap=nbreeap+ 1;
				publicationattributionprovisoire.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				publicationattributionprovisoire.setImage("/images/rdo_on.png");
				publicationattributionprovisoire.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire.publiele")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDatePublicationProvisoire()));
				publicationattributionprovisoire.setTooltip(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire.publieles")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDatePublicationProvisoire()));
				
			}
			if(dossier.getDosDateNotification()!=null)
			{
				nbreeap=nbreeap+ 1;
				cellnotification.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellnotification.setImage("/images/rdo_on.png");
				contrat=BeanLocator.defaultLookup(ContratsSession.class).getContrat(dossier,autorite, null,null,null);
				
			}
			if(nbreeoff+nbreeap>=5)
			{
				cellevaluationattribution.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellevaluationattribution.setImage("/images/rdo_on.png");
				nbretotal=nbretotal+4;
			}
			
			if(contrat!=null)
			{
				if(contrat.getDatedemandeimmatriculation()!=null)
				{
					cellimmatriculation.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
					cellimmatriculation.setImage("/images/puce.png");
					cellimmatriculation.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.demande").substring(0, 15)+"...");
					cellimmatriculation.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.demande")+" "+UtilVue.getInstance().formateLaDate2(contrat.getDatedemandeimmatriculation()));

				}
				if(contrat.getDateimmatriculation()!=null)
				{
					cellimmatriculation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					nombreimmatriculation=nombreimmatriculation+1;
					cellimmatriculation.setImage("/images/rdo_on.png");
					cellimmatriculation.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.marche").substring(0, 15)+"...");
					cellimmatriculation.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.marche")+" "+UtilVue.getInstance().formateLaDate2(contrat.getDateimmatriculation()));
					nbretotal=nbretotal+1;
				}
			}
			if((dossier.getDosNombreLots()>0)&&dossier.getDosLotDivisible().equals("OUI"))
			{
                 if(nbretotal>=21)
                 {
                	 celltdossiers.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
                	 celltdossiers.setImage("/images/rdo_on.png");
                 }
			}
			else
			{
				 if(nbretotal>=20)
                 {
                	 celltdossiers.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
                	 celltdossiers.setImage("/images/rdo_on.png");
                 }
			}
			
		}
		if(LibelleTab!=null)
		{
			if(LibelleTab.equals("traitementsdossiers"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/traitementsdossiers.zul");	
				 tree.setSelectedItem(treetdossiers);    
			}
			if((LibelleTab.equals("infogenerales"))||(LibelleTab.equals("devise"))||(LibelleTab.equals("piecesadministratives"))
					||(LibelleTab.equals("criteresqualifications"))||(LibelleTab.equals("allotissement"))||(LibelleTab.equals("presentationsoffres")))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/dossierconsultation.zul");	
				 tree.setSelectedItem(treeconsultationfournisseur);  
				 TreeTreepreparation();
			}
			if(LibelleTab.equals("fournisseursselectionnes"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/fournisseurs.zul");	
				 tree.setSelectedItem(treeselectionfournisseur); 
				 TreeTreepreparation();
			}
			if(LibelleTab.equals("lettreinvitation"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/formlettreinvitation.zul");	
				 tree.setSelectedItem(treelettreinvitation); 
				 TreeTreepreparation();
			}
			if(LibelleTab.equals("plisouvertures"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/registredepot.zul")	;
				 tree.setSelectedItem(treeregistredepot); 
				 TreeTreeenregistrementplis();
			}
			if(LibelleTab.equals("listepresencemembrescommissions"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/listepresencemembrescommissions.zul");
				tree.setSelectedItem(treelistepresencemembrescommissions);
				TreeTreeenregistrementplis();
				
			}
			if(LibelleTab.equals("representantssoumissionnaires"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/representantssoumissionnaires.zul");
				tree.setSelectedItem(treerepresentantssoumissionnaires);
				TreeTreeenregistrementplis();
				
			}
			if(LibelleTab.equals("procesverbalouverture"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/procesverbalouverture.zul");
				tree.setSelectedItem(treeprocesverbalouverture);
				TreeTreeenregistrementplis();
				treeouvertureplis.setOpen(true);
			}
			if(LibelleTab.equals("incidents"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/incidents.zul");
				tree.setSelectedItem(treeincidents);
				TreeTreeenregistrementplis();
			}
			if(LibelleTab.equals("compositioncommissiontechnique"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/compositioncommissiontechnique.zul");
				tree.setSelectedItem(treecompositioncommissiontechnique);
				TreeTreeenregistrementplis();
				
			}
			if(LibelleTab.equals("lecturesoffres"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/formlecturesoffres.zul");
				tree.setSelectedItem(treelecturesoffres);
				TreeTreeenregistrementplis();
				
			}
			if(LibelleTab.equals("observateursindependants"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/observateursindependants.zul");
				tree.setSelectedItem(treeobservateursindependants);
				TreeTreeenregistrementplis();
				
			}
			if(LibelleTab.equals("piecessoumissionnaires"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/piecessoumissionnaires.zul");
				tree.setSelectedItem(treepiecessoumissionnaires);
				TreeTreeenregistrementplis();
				
			}
			if(LibelleTab.equals("representantsservicestechniques"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/representantsservicestechniques.zul");
				tree.setSelectedItem(treerepresentantsservicestechniques);
				TreeTreeenregistrementplis();
				
			}
			if(LibelleTab.equals("incidents"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/incidents.zul");
				tree.setSelectedItem(treeincidents);
				TreeTreeenregistrementplis();
				
			}
			if(LibelleTab.equals("lotssoumissionnaires"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/lotssoumissionnaires.zul");
				tree.setSelectedItem(treelotssoummissionnaires);
				TreeTreeenregistrementplis();
				
			}
			if(LibelleTab.equals("evaluationattribution"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/evaluationoffres.zul");
				tree.setSelectedItem(treeevaluationsoffres);
				TreeEvaluationattribution();
				
			}
			if(LibelleTab.equals("attributionprovisoire"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/attributionprovisoire.zul");	
				tree.setSelectedItem(treeattributionprovisoire);
				TreeEvaluationattribution();
				treeevaluationsoffres.setOpen(false);
			}
			if(LibelleTab.equals("notificationmarche"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/formnotificationmarche.zul");	
				tree.setSelectedItem(treenotification);
				TreeEvaluationattribution();
				treeevaluationsoffres.setOpen(false);
			}
			if(LibelleTab.equals("verificationconformite"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/verificationsconformites.zul");
				tree.setSelectedItem(treeverificationconformite);
				TreeEvaluationattribution();
				treeevaluationsoffres.setOpen(true);
			}
			if(LibelleTab.equals("correctionsoffres"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/correctionoffres.zul");
				tree.setSelectedItem(treecorrectionoffre);
				TreeEvaluationattribution();
				treeevaluationsoffres.setOpen(true);
			}
			if(LibelleTab.equals("vercriteresqualification"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/vercriteresqualification.zul");
				tree.setSelectedItem(vercriteresqualification);
				TreeEvaluationattribution();
				treeevaluationsoffres.setOpen(true);
			}
			if(LibelleTab.equals("publicationattributionprovisoire"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/formpublicationattributionprovisoire.zul");	
				tree.setSelectedItem(treepublicationattributionprovisoire);
				TreeEvaluationattribution();
				treeevaluationsoffres.setOpen(false);
				
			}
			if(LibelleTab.equals("immatriculation"))
			{
				idinclude.setSrc("/passationsmarches/servicedrp/formdemandeimmatriculation.zul");
				tree.setSelectedItem(itemimmatriculation);
				TreeImmatriculation();
			}
		}
		else
		{
			idinclude.setSrc("/passationsmarches/servicedrp/dossierappeloffre.zul");	
			 tree.setSelectedItem(treetdossiers);     
		}
		  
		lgtitre.setValue(Labels.getLabel("kermel.plansdepassation.infos.generales.referencedossier")+": "+appel.getAporeference());
	
//		itemimmatriculation.setDisabled(true);
//		treeevaluationattribution.setDisabled(true);
//		treeevaluationsoffres.setDisabled(true);
//		treeverificationconformite.setDisabled(true);
//		treecorrectionoffre.setDisabled(true);
//		vercriteresqualification.setDisabled(true);
//		treeattributionprovisoire.setDisabled(true);
//		treepublicationattributionprovisoire.setDisabled(true);
//		treenotification.setDisabled(true);
//		treerecours.setDisabled(true);
//		treedocuments.setDisabled(true);
//		treecourriers.setDisabled(true);
//		treeevenements.setDisabled(true);
		
		
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}


	public void onSelect$tree() throws Exception
	{
		InfosTree();
	}
	
	public void TreeTreepreparation()
	{
		
		treepreparation.setOpen(true);
		treeenrgistrementouvertureplis.setOpen(false);
		treeevaluationattribution.setOpen(false);
		
	}
	
	public void TreeTreedossiers()
	{
		
		treepreparation.setOpen(false);
		treeenrgistrementouvertureplis.setOpen(false);
		treeevaluationattribution.setOpen(false);
		
	}
	public void TreeTreeenregistrementplis()
	{
		
		treepreparation.setOpen(false);
		treeenrgistrementouvertureplis.setOpen(true);
		treeevaluationattribution.setOpen(false);
		
	}
	public void TreeAutres()
	{
		
		treepreparation.setOpen(false);
		treeenrgistrementouvertureplis.setOpen(false);
		treeevaluationattribution.setOpen(false);
		
	}

	public void TreeEvaluationattribution()
	{
		
		treepreparation.setOpen(false);
		treeenrgistrementouvertureplis.setOpen(false);
		treeevaluationattribution.setOpen(true);
		
	}
	public void InfosTree() throws Exception{
		
		/////////////Preparation du dossier//////////
		if(tree.getSelectedItem().getId().equals("treepreparation"))
		{
			TreeTreepreparation();
			idinclude.setSrc("/passationsmarches/servicedrp/dossierspreparation.zul");
			TreeTreepreparation();
		}
		if(tree.getSelectedItem().getId().equals("treeconsultationfournisseur"))
		{
			TreeTreepreparation();
			idinclude.setSrc("/passationsmarches/servicedrp/dossierconsultation.zul");
		}
		if(tree.getSelectedItem().getId().equals("treetdossiers"))
		{
			TreeTreedossiers();
			idinclude.setSrc("/passationsmarches/servicedrp/traitementsdossiers.zul");
		}
		if(tree.getSelectedItem().getId().equals("treeselectionfournisseur"))
		{
			TreeTreepreparation();
			idinclude.setSrc("/passationsmarches/servicedrp/fournisseurs.zul");
		}
		
		if(tree.getSelectedItem().getId().equals("treelettreinvitation"))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				if(tache.getPreselection()==1)
				{
					 TreeTreepreparation();
					 idinclude.setSrc("/passationsmarches/servicedrp/formlettreinvitation.zul");
				}
				else
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.travauxdrp.candidats.ainvites")+" "+dossier.getDosSoumission(), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				}
					
			
			}
			
		}
		if(tree.getSelectedItem().getId().equals("treeregistredepot"))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
			}
			else
			{
				if(tache.getPreselection()==1)
				{
					TreeTreeenregistrementplis();
					idinclude.setSrc("/passationsmarches/servicedrp/registredepot.zul");
				}
				else
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.saisirregistreretrait"), "Erreur", Messagebox.OK, Messagebox.ERROR);

				}
			}
			
		}
		if((tree.getSelectedItem().getId().equals("treeenrgistrementouvertureplis")))
		{
			TreeTreeenregistrementplis();
			 idinclude.setSrc("/passationsmarches/servicedrp/dossiersouverturesplis.zul");
		}
		if(tree.getSelectedItem().getId().equals("treelistepresencemembrescommissions"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/servicedrp/listepresencemembrescommissions.zul");
			treeouvertureplis.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treerepresentantssoumissionnaires"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/servicedrp/representantssoumissionnaires.zul");
			treeouvertureplis.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treerepresentantsservicestechniques"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/servicedrp/representantsservicestechniques.zul");
			treeouvertureplis.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treeobservateursindependants"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/servicedrp/observateursindependants.zul");
			treeouvertureplis.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treepiecessoumissionnaires"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/servicedrp/piecessoumissionnaires.zul");
			treeouvertureplis.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treelecturesoffres"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/servicedrp/formlecturesoffres.zul");
			treeouvertureplis.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treeprocesverbalouverture"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/servicedrp/procesverbalouverture.zul");
			treeouvertureplis.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treecompositioncommissiontechnique"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/servicedrp/compositioncommissiontechnique.zul");
			treeouvertureplis.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treeincidents"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/servicedrp/incidents.zul");
			treeouvertureplis.setOpen(true);
		}	

		if(tree.getSelectedItem().getId().equals("treeouvertureplis"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/servicedrp/ouvertureplis.zul");
			treeouvertureplis.setOpen(true);
			
		}
		if(tree.getSelectedItem().getId().equals("treelotssoummissionnaires"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/servicedrp/lotssoumissionnaires.zul");
		}
		if(tree.getSelectedItem().getId().equals("treeevaluationattribution"))
		{
			TreeEvaluationattribution();
			idinclude.setSrc("/passationsmarches/servicedrp/dossiersevaluationsattributions.zul");
		}
		if(tree.getSelectedItem().getId().equals("treeevaluationsoffres"))
		{
			TreeEvaluationattribution();
			idinclude.setSrc("/passationsmarches/servicedrp/evaluationoffres.zul");
			treeevaluationsoffres.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treeattributionprovisoire"))
		{
			TreeEvaluationattribution();
			idinclude.setSrc("/passationsmarches/servicedrp/attributionprovisoire.zul");
		}
		if(tree.getSelectedItem().getId().equals("treeverificationconformite"))
		{
			TreeEvaluationattribution();
			idinclude.setSrc("/passationsmarches/servicedrp/verificationsconformites.zul");
		}
		if(tree.getSelectedItem().getId().equals("treecorrectionoffre"))
		{
			TreeEvaluationattribution();
			idinclude.setSrc("/passationsmarches/servicedrp/correctionoffres.zul");
		}
		if((tree.getSelectedItem().getId().equals("treenotification")))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				 if(dossier.getDosDatePublicationProvisoire()==null)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.publieravis"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
					 TreeEvaluationattribution();
					 idinclude.setSrc("/passationsmarches/servicedrp/formnotificationmarche.zul");
					 treeevaluationsoffres.setOpen(false);
				 }
			}
			
		}
		
		if(tree.getSelectedItem().getId().equals("treerecours"))
		{
			idinclude.setSrc("/passationsmarches/servicedrp/recours.zul");
			TreeAutres();
		}
		if(tree.getSelectedItem().getId().equals("treerecours"))
		{
			idinclude.setSrc("/passationsmarches/servicedrp/documents.zul");
			TreeAutres();
		}
		if((tree.getSelectedItem().getId().equals("vercriteresqualification")))
		{
			TreeEvaluationattribution();
			idinclude.setSrc("/passationsmarches/servicedrp/vercriteresqualification.zul");
			
		}	
		if((tree.getSelectedItem().getId().equals("treepublicationattributionprovisoire")))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				if(tache.getAttributionprovisoire()==0)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.saisirattributionprovisoire"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
				    TreeEvaluationattribution();
					treeevaluationsoffres.setOpen(false);
				    idinclude.setSrc("/passationsmarches/servicedrp/formpublicationattributionprovisoire.zul");
				 }
			}
			
		}	
		if(tree.getSelectedItem().getId().equals("itemimmatriculation"))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				if(dossier.getDosDateNotification()==null)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.notifierdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
					 TreeImmatriculation();
			       idinclude.setSrc("/passationsmarches/servicedrp/formdemandeimmatriculation.zul");
				 }
			}
		}	
		
		
		
	}
	
	public void TreeImmatriculation()
	{
		treepreparation.setOpen(false);
		treeenrgistrementouvertureplis.setOpen(false);
		treeevaluationattribution.setOpen(false);
		
		
	}

}