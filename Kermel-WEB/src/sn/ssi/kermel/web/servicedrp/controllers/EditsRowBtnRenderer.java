package sn.ssi.kermel.web.servicedrp.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygGarantiesDossiers;
import sn.ssi.kermel.be.entity.SygPiecesrecus;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.GarantiesSession;

public class EditsRowBtnRenderer implements RowRenderer, RowRendererExt {

	private	SygGarantiesDossiers garantie;
	private List<SygGarantiesDossiers> garanties = new ArrayList<SygGarantiesDossiers>();
	private String pourcantage;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygDossiers dossier=new SygDossiers();
	Session session = getHttpSession();
	public static Long CURRENT_CODE;
	
	public void onCreate(CreateEvent createEvent) {
		
		
	
	}
	
	private Session getHttpSession() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Row newRow(Grid grid) {
		// Create EditableRow instead of Row(default)
		Row row = new EditableRow();
		row.applyProperties();
		return row;
	}

	@Override
	public Component newCell(Row row) {
		return null;// Default Cell
	}

	@Override
	public int getControls() {
		return RowRendererExt.DETACH_ON_RENDER; // Default Value
	}

	@Override
	public void render(Row row, Object data, int index) throws Exception {
		final SygPiecesrecus pieces = (SygPiecesrecus) data;
		
		final EditableRow editRow = (EditableRow) row;
		
		final EditableDiv numero = new EditableDiv(pieces.getLibelle(), false);
		numero.txb.setReadonly(true);
		numero.setParent(editRow);
		
		
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).findById(CURRENT_CODE);
		
		
		final EditableDiv client = new EditableDiv(pieces.getDescription(), false);
		client.txb.setReadonly(true);
		client.setParent(editRow);
		
		garanties= BeanLocator.defaultLookup(GarantiesSession.class).find(0,-1,dossier,pieces,null);
		if(garanties.size()>0)
		{
		    garantie=garanties.get(0);
		    pourcantage=garantie.getPourcentage().toString();
		}
		else
		{
			pourcantage="";
		}
		final EditableDiv indexNew = new EditableDiv(pourcantage, false);
		indexNew.setParent(editRow);
		
		
		final Div ctrlDiv = new Div();
		ctrlDiv.setParent(editRow);
		final Button editBtn = new Button(null, "/images/disk.png");
		editBtn.setMold("os");
		editBtn.setHeight("20px");
		editBtn.setWidth("30px");
		editBtn.setParent(ctrlDiv);
		
		
		// Button listener - control the editable of row
		editBtn.addEventListener(Events.ON_CLICK, new EventListener() {
			public void onEvent(Event event) throws Exception {
				final Button submitBtn = (Button) new Button(null, "/images/ok.png");
				final Button cancelBtn = (Button) new Button(null, "/images/cancel.png");
				submitBtn.setMold("os");
				submitBtn.setHeight("20px");
				submitBtn.setWidth("30px");
				submitBtn.setTooltiptext("Valider la saisie");
				submitBtn.addEventListener(Events.ON_CLICK, new EventListener() {
					public void onEvent(Event event) throws Exception {

						editRow.toggleEditable(true);
						if(!ToolKermel.estLong(indexNew.txb.getValue())){
							editRow.toggleEditable(true);
							throw new WrongValueException(indexNew, "Attention!!La valeurde du pourcentage  doit �tre num�rique!!");

						}
						garanties= BeanLocator.defaultLookup(GarantiesSession.class).find(0,-1,dossier,pieces,null);
						if(garanties.size()==0)
						{
							garantie= new SygGarantiesDossiers();
							garantie.setDossier(dossier);
							garantie.setPiece(pieces);
							garantie.setPourcentage(new BigDecimal(Integer.parseInt(indexNew.txb.getValue())));
							BeanLocator.defaultLookup(GarantiesSession.class).save(garantie);
						}
						else
						{
							garantie.setPourcentage(new BigDecimal(Integer.parseInt(indexNew.txb.getValue())));
							BeanLocator.defaultLookup(GarantiesSession.class).update(garantie);
						}
						
						submitBtn.detach();
						cancelBtn.detach();
						editBtn.setParent(ctrlDiv);
					}
				});
			
				cancelBtn.setMold("os");
				cancelBtn.setHeight("20px");
				cancelBtn.setWidth("30px");
				cancelBtn.setTooltiptext("Annuler la saisie");
				cancelBtn.addEventListener(Events.ON_CLICK, new EventListener() {
					public void onEvent(Event event) throws Exception {
						editRow.toggleEditable(false);
						submitBtn.detach();
						cancelBtn.detach();
						editBtn.setParent(ctrlDiv);
					}
				});
				submitBtn.setParent(ctrlDiv);
				cancelBtn.setParent(ctrlDiv);
				editRow.toggleEditable(true);
				editBtn.detach();
			}
		});

	}
	
}