package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Button;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygMonnaieoffre;
import sn.ssi.kermel.be.entity.SygNatureprix;
import sn.ssi.kermel.be.entity.SygPays;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygResultatNegociation;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.be.passationsmarches.ejb.ResultatNegociationSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class CorrectionOffreController extends AbstractWindow implements
		EventListener, AfterCompose, RowRenderer, RowRendererExt {

	private Listbox lstListe,lstMonnaie,lstNature,lstPays;
	private Paging pgPagination,pgMonnaie,pgNature,pgPays;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle,txtRechercherPays;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference,libellebailleur=null;
    private int parent;
    private Label lbltitre;
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherBailleur,txtChapitre,txtRechercherMonnaie,txtnumplis,txtRaisonsocial,txtRechercherNature;
    SygBailleurs bailleur=null;
    private Decimalbox dcMontant;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg,libellemonnaie=null,libellenature=null,libellepays=null;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idbailleur;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuAjouter,menuModifier,menuSupprimer;
	private Div step0,step1;
	private SygPlisouvertures plis=null;
	SygMonnaieoffre monnaie=new SygMonnaieoffre();
	SygNatureprix natureprix=new SygNatureprix();
	SygPays pays=new SygPays();
	private Bandbox bdMonnaie,bdNature,bdPays;
	private Radio rdsoumoui,rdsoumnon,rdoftoui,rdoftnon,rdoffoui,rdoffnon;
	private Radiogroup rdpodepaiement;
	private Intbox rabais;
	List<SygPlisouvertures> listesplis = new ArrayList<SygPlisouvertures>();
	private Grid GridListe;
	SygResultatNegociation resultat=new SygResultatNegociation();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
		
		
		
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,2);
		if(dossier!=null)
		{
			resultat=BeanLocator.defaultLookup(ResultatNegociationSession.class).getResultat(dossier, appel.getAutorite());
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
		
	
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			
			GridListe.setRowRenderer(this);
			GridListe.setModel(new ListModelList(BeanLocator.defaultLookup(RegistrededepotSession.class).find(pgPagination.getActivePage()*byPage, byPage, null,dossier, 1)));
			pgPagination.setTotalSize(BeanLocator.defaultLookup(RegistrededepotSession.class).count(null,dossier, 1));
			
		
		} 
	
	}

	
	


	public void  onClick$menuValider(){
		
	       
	       for (int k = 0; k < listesplis.size(); k++) {
	    	   plis=listesplis.get(k);
	    	   plis.setEtatExamenPreliminaire(UIConstants.NPARENT);
	      	   BeanLocator.defaultLookup(RegistrededepotSession.class).update(plis);
	       }
	       for (int i = 0; i < lstListe.getSelectedCount(); i++) {
	    	 
	    	   plis=(SygPlisouvertures) ((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
	    	   plis.setEtatExamenPreliminaire(UIConstants.PARENT);
	      	   BeanLocator.defaultLookup(RegistrededepotSession.class).update(plis);
	       }
	       Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
	}

	
	 
	@Override
	public Row newRow(Grid grid) {
		// Create EditableRow instead of Row(default)
		Row row = new EditableRow();
		row.applyProperties();
		return row;
	}

	@Override
	public Component newCell(Row row) {
		return null;// Default Cell
	}

	@Override
	public int getControls() {
		return RowRendererExt.DETACH_ON_RENDER; // Default Value
	}
 @Override
	public void render(Row row, Object data, int index) throws Exception {
		final SygPlisouvertures plis = (SygPlisouvertures) data;
		
		final EditableRow editRow = (EditableRow) row;
		
		final EditableDiv soumissionnaire = new EditableDiv(plis.getRetrait().getNomSoumissionnaire(), false);
		soumissionnaire.txb.setReadonly(true);
		soumissionnaire.setParent(editRow);
		
		
		final EditableDiv montantlu = new EditableDiv(ToolKermel.format2Decimal(plis.getMontantoffert()), false);
		montantlu.txb.setReadonly(true);
		montantlu.setParent(editRow);
		
		final EditableDiv montantdevise = new EditableDiv(ToolKermel.format2Decimal(plis.getMontantoffert()), false);
		montantdevise.txb.setReadonly(true);
		montantdevise.setParent(editRow);
		
		final EditableDiv montantcorriges = new EditableDiv(ToolKermel.format2Decimal(plis.getMontantdefinitif()), false);
		montantcorriges.setParent(editRow);
		
		final EditableDiv monnaie = new EditableDiv(plis.getMonCode(), false);
		monnaie.txb.setReadonly(true);
		monnaie.setParent(editRow);
		
		final EditableDiv nature = new EditableDiv(plis.getNatCode(), false);
		nature.txb.setReadonly(true);
		nature.setParent(editRow);
		
		final Div ctrlDiv = new Div();
		ctrlDiv.setParent(editRow);
		final Button editBtn = new Button(null, "/images/disk.png");
		editBtn.setMold("os");
		editBtn.setHeight("20px");
		editBtn.setWidth("30px");
		editBtn.setParent(ctrlDiv);
		
		if(resultat!=null)
		{
			editBtn.setDisabled(true);
			
		}
//		if(criteres.getDossier().getDosDateMiseValidation()!=null||criteres.getDossier().getDosDatePublication()!=null)
//		{
//			editBtn.setDisabled(true);
//			deleteBtn.setDisabled(true);
//		}

		// Button listener - control the editable of row
		editBtn.addEventListener(Events.ON_CLICK, new EventListener() {
			public void onEvent(Event event) throws Exception {
				final Button submitBtn = (Button) new Button(null, "/images/ok.png");
				final Button cancelBtn = (Button) new Button(null, "/images/cancel.png");
				submitBtn.setMold("os");
				submitBtn.setHeight("20px");
				submitBtn.setWidth("30px");
				submitBtn.setTooltiptext("Valider la saisie");
				submitBtn.addEventListener(Events.ON_CLICK, new EventListener() {
					public void onEvent(Event event) throws Exception {

						editRow.toggleEditable(true);
						if(!ToolKermel.estLong(montantcorriges.txb.getValue())){
							editRow.toggleEditable(true);
							throw new WrongValueException(montantcorriges, "Attention!! Le Montant  doit �tre num�rique!!");

						}
					
						plis.setMontantdefinitif(new BigDecimal(Long.parseLong(montantcorriges.txb.getValue())));
						BeanLocator.defaultLookup(RegistrededepotSession.class).update(plis);
					
						submitBtn.detach();
						cancelBtn.detach();
						editBtn.setParent(ctrlDiv);
						session.setAttribute("libelle", "saisiprixevalues");
						loadApplicationState("procedure_pi");
					}
				});
				montantcorriges.addEventListener(Events.ON_OK, new EventListener() {
					public void onEvent(Event event) throws Exception {

						editRow.toggleEditable(true);
						if(!ToolKermel.estLong(montantcorriges.txb.getValue())){
							editRow.toggleEditable(true);
							throw new WrongValueException(montantcorriges, "Attention!! Le Montant  doit �tre num�rique!!");

						}
					
						plis.setMontantdefinitif(new BigDecimal(Long.parseLong(montantcorriges.txb.getValue())));
						BeanLocator.defaultLookup(RegistrededepotSession.class).update(plis);
					
						submitBtn.detach();
						cancelBtn.detach();
						editBtn.setParent(ctrlDiv);
						session.setAttribute("libelle", "saisiprixevalues");
						loadApplicationState("procedure_pi");
					}
				});
			
				cancelBtn.setMold("os");
				cancelBtn.setHeight("20px");
				cancelBtn.setWidth("30px");
				cancelBtn.setTooltiptext("Annuler la saisie");
				cancelBtn.addEventListener(Events.ON_CLICK, new EventListener() {
					public void onEvent(Event event) throws Exception {
						editRow.toggleEditable(false);
						submitBtn.detach();
						cancelBtn.detach();
						editBtn.setParent(ctrlDiv);
						
					}
				});
				submitBtn.setParent(ctrlDiv);
				cancelBtn.setParent(ctrlDiv);
				editRow.toggleEditable(true);
				editBtn.detach();
				
			}
		});
		
	
	}

}