package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import java.util.Date;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

public class EditableCheckDiv extends Div {
	public static final String ON_EDITABLE = "onEditable";
	private String text = "";
	private Date date = new Date();
	private boolean clickable = true;
	private boolean checked = false;
	private boolean editable = false;
	Textbox txb;
	Datebox datebox;
	Label lbl;
	Bandbox bandbox;
	Intbox intbox;
	Checkbox checkbox;

	// Empty constructor will do the creation
	public EditableCheckDiv() {
		txb = new Textbox();
		lbl = new Label();
		txb.setWidth("99%");
		lbl.setWidth("99%");
		lbl.setMultiline(true);
		lbl.setParent(this);// Default show a label with text
		checkbox = new Checkbox();
		
	}

	public EditableCheckDiv(String text, String component) {
		if (component.endsWith("Textbox")) {
			txb = new Textbox();
			lbl = new Label();
			txb.setWidth("99%");
			lbl.setWidth("99%");
			lbl.setParent(this);// Default show a label with text
		} else if (component.endsWith("Datebox")) {
			datebox = new Datebox();
			lbl = new Label();
			datebox.setWidth("99%");
			lbl.setWidth("99%");
			lbl.setParent(this);// Default show a label with text
		} else if (component.endsWith("BandBox")) {
			bandbox = new Bandbox();
			lbl = new Label();
			txb.setWidth("99%");
			lbl.setWidth("99%");
			lbl.setParent(this);// Default show a label with text
		} else if (component.endsWith("IntBox")) {
			bandbox = new Bandbox();
			lbl = new Label();
			txb.setWidth("99%");
			lbl.setWidth("99%");
			lbl.setParent(this);// Default show a label with text
		}else if (component.endsWith("CheckBox")) {
			checkbox = new Checkbox();
			lbl = new Label();
			txb.setWidth("99%");
			lbl.setWidth("99%");
			checkbox.setWidth("99%");
			checkbox.setParent(this);// Default show a label with text
		}
	}

	public EditableCheckDiv(String text) {
		this();
		setText(text);
		initEditCtrl();
	}

	public EditableCheckDiv(String text, boolean clickable) {
		this();
		setText(text);
		setClickable(clickable);
		initEditCtrl();
	}
	// checkbox
	public EditableCheckDiv(Boolean booleen, boolean clickable) {
		this();
		setChecked(booleen);
		setClickable(clickable);
		initEditCtrl();
	}
	
	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	

	// Getter and setters
	public void setText(String text) {
		this.text = text;
		txb.setValue(text);
		lbl.setValue(text);
	}
	
	

	public String getText() {
		return text;
	}

	public void setClickable(boolean clickable) {
		this.clickable = clickable;
	}

	public boolean isClickable() {
		return this.clickable;
	}

	// Initialize the listener of the whole component and the textbox in it
	private void initEditCtrl() {
		this.addEventListener(ON_EDITABLE, new EventListener() {
			public void onEvent(Event event) throws Exception {
				toggleEdit((Boolean) event.getData());
			}
		});

		// This will turns the edit funtion on when click on label
		if (isClickable()) {
			lbl.addEventListener(Events.ON_CLICK, new EventListener() {
				public void onEvent(Event event) throws Exception {
					txb.setFocus(true);
					checkbox.setFocus(true);
					Events.postEvent(new Event(EditableRow.ON_EDIT,
							EditableCheckDiv.this.getParent(), null));
				}
			});
		}
	}

	// Replace textbox/label with label/textbox
	private void toggleEdit(boolean applyChange) {
		if (!editable) {
			lbl.detach();
			EditableCheckDiv.this.appendChild(checkbox);
		} else {
			checked = checkbox.isChecked();
			checkbox.detach();
			if (applyChange) {// if apply changes then set the value in
//				lbl.setValue(text = txb.getValue());
				if(checked)
					lbl.setValue("OUI");
				else
					lbl.setValue("NON");
			} else {
				checkbox.setChecked(checked);
//				txb.setValue(text);
//				lbl.setValue(text);
				if(checkbox.isChecked())
					lbl.setValue("OUI");
				else
					lbl.setValue("NON");
			}
			EditableCheckDiv.this.appendChild(lbl);
		}
		editable = !editable;
	}

	private void toggleEditDate(boolean applyChange) {
		if (!editable) {
			lbl.detach();
			EditableCheckDiv.this.appendChild(datebox);
		} else {
			datebox.detach();
			if (applyChange) {// if apply changes then set the value in
				lbl.setValue((date = datebox.getValue()).toString());
			} else {
				datebox.setValue(date);
				lbl.setValue(date.toString());
			}
			EditableCheckDiv.this.appendChild(lbl);
		}
		editable = !editable;
	}

	private void toggleEditBandbox(boolean applyChange) {
		if (!editable) {
			lbl.detach();
			EditableCheckDiv.this.appendChild(datebox);
		} else {
			datebox.detach();
			if (applyChange) {// if apply changes then set the value in
				lbl.setValue((date = datebox.getValue()).toString());
			} else {
				datebox.setValue(date);
				lbl.setValue(date.toString());
			}
			EditableCheckDiv.this.appendChild(lbl);
		}
		editable = !editable;
	}
	
	@SuppressWarnings("unused")
	private void toggleEditCheckbox(boolean applyChange) {
		if (!editable) {
			lbl.detach();
			EditableCheckDiv.this.appendChild(checkbox);
		} else {
			checkbox.detach();
			if (applyChange) {// if apply changes then set the value in
				checked = checkbox.isChecked();
				if(checked)
					lbl.setValue("OUI");
				else
					lbl.setValue("NON");
			} else {
				checkbox.setChecked(checked);
				
				if(checked)
					lbl.setValue("OUI");
				else
					lbl.setValue("NON");
			}
			EditableCheckDiv.this.appendChild(lbl);
		}
		editable = !editable;
	}


}