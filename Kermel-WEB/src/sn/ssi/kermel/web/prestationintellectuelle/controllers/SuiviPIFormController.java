package sn.ssi.kermel.web.prestationintellectuelle.controllers;


import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Adama samb
 * @since mardi 11 Janvier 2011, 12:34
 
 */
public class SuiviPIFormController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private Include pgActes;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	Session session = getHttpSession();
	private String LibelleTab;
	private String origine;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		


	}

	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		origine=(String) Executions.getCurrent().getAttribute("origine");
		
		pgActes.setSrc("/passationsmarches/prestationintellectuelle/dossiersprocedures.zul");
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

public void  onClick$menuFermer(){
		if(origine!=null)
		{
			 if(origine.equals("procedure"))
					loadApplicationState("procedures_passations");
			 else
			 {
				 if(origine.equals("validationattribution"))
				 {
					 loadApplicationState("validation_attributionprovisoire"); 
				 }
				 else
				 {
					 if(origine.equals("examenjuridique"))
							loadApplicationState("examen_juridique");
					 else
					 {
						 if(origine.equals("demandeimmatriculation"))
								loadApplicationState("immatriculation_contrats");
						 else
							 loadApplicationState("revue_dao"); 
					 }
							  
							  
				 }
			 }
					  
					  if(origine.equals("validationattribution"))
							loadApplicationState("validation_attributionprovisoire");
						  else
							  
							  loadApplicationState("revue_dao"); 
			 
		}
		else
		{
			loadApplicationState("procedures_passations");
		}
		 
   }
}