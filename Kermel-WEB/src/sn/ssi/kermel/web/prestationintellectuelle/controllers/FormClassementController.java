package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPieces;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DocumentsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class FormClassementController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstPieces;
	private Paging pgPagination,pgPieces;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
  	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	SygPieces piece=new SygPieces();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuAjouter,menuSupprimer;
	private Div step0,step1;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private int nombre=0;
	SygDocuments document=new SygDocuments();
	private String nomFichier;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	private Menuitem menuValider;
	private Image image;
	private Label lbltitre,lbldeuxpoints;
	private Iframe idIframe;
	private String extension,images;
	List<SygDocuments> documents = new ArrayList<SygDocuments>();
	private Textbox txtVersionElectronique;
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
    
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,2);
		if(dossier==null)
		{
			
			menuValider.setDisabled(true);
			image.setVisible(false);
		}
		else
		{
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			infos(dossier,appel);
		}
	}
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygPlisouvertures> candidatsselectionnes = BeanLocator.defaultLookup(RegistrededepotSession.class).find(pgPagination.getActivePage() * byPage,byPage,null,dossier, 0);
			 lstListe.setModel(new SimpleListModel(candidatsselectionnes));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(RegistrededepotSession.class).count(null,dossier, 0));
			 
			
		} 

	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygPlisouvertures plis = (SygPlisouvertures) data;
		item.setValue(plis);

		Listcell cellRaisonsocial = new Listcell(plis.getRetrait().getNomSoumissionnaire());
		 cellRaisonsocial.setParent(item);
		 
		 Listcell cellPays = new Listcell(plis.getPays());
		 cellPays.setParent(item);
		 
			
		Listcell cellNote = new Listcell("");
		   if(plis.getNotepreselectionne()!=null)
			   cellNote.setLabel(ToolKermel.format3Decimal(plis.getNotepreselectionne()));
		cellNote.setParent(item);
		
		nombre=nombre+1;
		Listcell cellRang = new Listcell(nombre+"");
		cellRang.setParent(item);
	
	}

	public void infos(SygDossiers dossier,SygAppelsOffres appel) {
		documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossier, appel,UIConstants.PARAM_TYPEDOCUMENTS_EVALUATION,null, null);
		
		if(documents.size()>0)
		{
			extension=documents.get(0).getNomFichier().substring(documents.get(0).getNomFichier().length()-3,  documents.get(0).getNomFichier().length());
			 if(extension.equalsIgnoreCase("pdf"))
				 images="/images/icone_pdf.png";
			 else  
				 images="/images/word.jpg";
			 
			image.setVisible(true);
			image.setSrc(images);
			lbldeuxpoints.setValue(":");
			lbltitre.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.procesverbalouverture.versionfichier"));
			menuValider.setDisabled(true);	
		
		}
	}
	public void onClick$btnChoixFichier() {
		//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
			if (ToolKermel.isWindows())
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectronique.setValue(nomFichier);
		}
	
private boolean checkFieldConstraints() {
		
		try {
		
		
		
			if(txtVersionElectronique.getValue().equals(""))
		     {
          errorComponent = txtVersionElectronique;
          errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.evaluation.rapportevaluation.fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

public void onClick$menuValider() {
	if(checkFieldConstraints())
	{
		documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossier, appel,UIConstants.PARAM_TYPEDOCUMENTS_EVALUATION,null, null);
		if(documents.size()>0)
		document=documents.get(0);
		document.setAppel(appel);
		document.setDossier(dossier);
		document.setNomFichier(txtVersionElectronique.getValue());
		document.setTypeDocument(UIConstants.PARAM_TYPEDOCUMENTS_EVALUATION);
		document.setLibelle(Labels.getLabel("kermel.plansdepassation.proceduresmarches.documents.pvevaluation"));
		if(documents.size()==0)
		{
			BeanLocator.defaultLookup(DocumentsSession.class).save(document);
		}
		else
		{
			BeanLocator.defaultLookup(DocumentsSession.class).update(document);
		}
	
		BeanLocator.defaultLookup(AppelsOffresSession.class).update(appel);
		
		session.setAttribute("libelle", "rapportevaluation");
		loadApplicationState("procedure_pi");
	}
}

public void onClick$image() {
	step0.setVisible(false);
	step1.setVisible(true);
	String filepath = cheminDossier +  documents.get(0).getNomFichier();
	File f = new File(filepath.replaceAll("\\\\", "/"));

	org.zkoss.util.media.AMedia mymedia = null;
	try {
		mymedia = new AMedia(f, null, null);
	} catch (FileNotFoundException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}

	if (mymedia != null)
		idIframe.setContent(mymedia);
	else
		idIframe.setSrc("");

	idIframe.setHeight("600px");
	idIframe.setWidth("100%");
}

public org.zkoss.util.media.AMedia fetchFile(File file) {

	org.zkoss.util.media.AMedia mymedia = null;
	try {
		mymedia = new AMedia(file, null, null);
		return mymedia;
	} catch (Exception e) {

		e.printStackTrace();
		return null;
	}

}
public void onClick$menuFermer() {
	step0.setVisible(true);
	step1.setVisible(false);
}
	
	
}