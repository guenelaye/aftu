package sn.ssi.kermel.web.prestationintellectuelle.controllers;


import java.util.Date;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.passationsmarches.ejb.ContratsSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;


@SuppressWarnings("serial")
public class ImmatriculationFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtCommentaires,nummatricule;
	Long code;
	private	SygContrats contrat=new SygContrats();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
    private Long idcontrat;
    private Label lblNumcontrat,lblAutorite,lblReference,lblObjets,lblModepassation,lblMontant;
    Session session = getHttpSession();
    private Datebox dtdate;
    UtilVue utilVue = UtilVue.getInstance();
      
	@Override
	public void onEvent(Event event) throws Exception {


	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		login = ((String) getHttpSession().getAttribute("user"));
		idcontrat=(Long) map.get(PARAM_WINDOW_CODE);;
		contrat=BeanLocator.defaultLookup(ContratsSession.class).getContrat(null, null, null, idcontrat,null);
		lblNumcontrat.setValue(Long.toString(contrat.getConID()));
		nummatricule.setValue(BeanLocator.defaultLookup(ContratsSession.class).getGeneratedCode(UIConstants.PARAM_NUMIMMATRICULATION));
		lblAutorite.setValue(contrat.getDossier().getAutorite().getDenomination());
		lblReference.setValue(contrat.getDossier().getAppel().getAporeference());
		lblObjets.setValue(contrat.getDossier().getAppel().getApoobjet());
		lblModepassation.setValue(contrat.getDossier().getAppel().getModepassation().getLibelle());
		lblMontant.setValue(ToolKermel.format2Decimal(contrat.getDossier().getAppel().getApomontantestime()));
	}

	public void onClick$menuValider() {
		if(checkFieldConstraints())
		{
		  	
			
			contrat.setDateimmatriculation(dtdate.getValue());
			contrat.setConcommentairesmatriculation(txtCommentaires.getValue());
			contrat.setNummatriculation(nummatricule.getValue());
			contrat.setImmatriculation(1);
			BeanLocator.defaultLookup(ContratsSession.class).updateContrat(contrat);
				
			
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			if(dtdate.getValue()==null)
		     {
              errorComponent = dtdate;
              errorMsg = Labels.getLabel("kermel.journal.date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((dtdate.getValue()).after(new Date() ))
			 {
				errorComponent = dtdate;
				errorMsg =Labels.getLabel("kermel.journal.date")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.referentiel.date.dujour")+": "+UtilVue.getInstance().formateLaDate(new Date());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
 
public void  onClick$menuFermer(){
	detach();
}
}
