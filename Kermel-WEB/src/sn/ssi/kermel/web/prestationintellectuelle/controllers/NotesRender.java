package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygDossierssouscriteresevaluateurs;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersSouscriteresSession;


public class NotesRender implements RowRenderer, RowRendererExt {


	
	Session session = getHttpSession();
	public static Long CURRENT_CODE;
	private Double noteretenue=0.0;
	List<SygDossierssouscriteres> souscriteres = new ArrayList<SygDossierssouscriteres>();
	public void onCreate(CreateEvent createEvent) {
		
		
	
	}
	
	private Session getHttpSession() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Row newRow(Grid grid) {
		// Create EditableRow instead of Row(default)
		Row row = new EditableRow();
		row.applyProperties();
		return row;
	}

	@Override
	public Component newCell(Row row) {
		return null;// Default Cell
	}

	@Override
	public int getControls() {
		return RowRendererExt.DETACH_ON_RENDER; // Default Value
	}

	@Override
	public void render(Row row, Object data, int index) throws Exception {
		final SygDossierssouscriteresevaluateurs note = (SygDossierssouscriteresevaluateurs) data;
		
		final EditableRow editRow = (EditableRow) row;
		
		if (note.getLibelle() != null) {
			final EditableDiv libelle = new EditableDiv(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial")+":  " +note.getLibelle(), false);
			libelle.txb.setReadonly(true);
			libelle.setAlign("center");
			libelle.setHeight("30px");
			libelle.setStyle("color:#000;");
			libelle.setParent(editRow);
			
			
			
			final EditableDiv notes = new EditableDiv("", false);
			notes.setParent(editRow);
			
			final EditableDiv noteponderee = new EditableDiv("", false);
			noteponderee.setParent(editRow);
			
		}
     else {
		   	final EditableDiv libelle = new EditableDiv( note.getCritere().getLibelle(), false);
	 	    libelle.txb.setReadonly(true);
		    libelle.setParent(editRow);
		
		    souscriteres=BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(0, -1, note.getDossier(), note.getCritere().getId(), null);
	 		final EditableDiv notes= new EditableDiv(ToolKermel.format2Decimal(note.getNote()), false);
	 		notes.setParent(editRow);
		
	 		final EditableDiv noteponderee= new EditableDiv(" / "+ToolKermel.format2Decimal(souscriteres.get(0).getNote()), false);
	 		noteponderee.txb.setReadonly(true);
	 		noteponderee.setParent(editRow);
   	
		final Div ctrlDiv = new Div();
		ctrlDiv.setParent(editRow);
		final Button editBtn = new Button(null, "/images/pencil-small.png");
		editBtn.setMold("os");
		editBtn.setHeight("20px");
		editBtn.setWidth("30px");
		editBtn.setParent(ctrlDiv);
		// Button listener - control the editable of row
		editBtn.addEventListener(Events.ON_CLICK, new EventListener() {
			public void onEvent(Event event) throws Exception {
				final Button submitBtn = (Button) new Button(null, "/images/tick-small.png");
				final Button cancelBtn = (Button) new Button(null, "/images/cross-small.png");
				submitBtn.setMold("os");
				submitBtn.setHeight("20px");
				submitBtn.setWidth("30px");
				submitBtn.setTooltiptext("Valider la saisie");
				submitBtn.addEventListener(Events.ON_CLICK, new EventListener() {
					public void onEvent(Event event) throws Exception {

						editRow.toggleEditable(true);
						if (!ToolKermel.estReel(notes.txb.getValue())) {
							editRow.toggleEditable(true);
							throw new WrongValueException(notes, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.notepondere.numerique"));

						}
						noteretenue=Double.valueOf(Integer.parseInt(notes.txb.getValue()));
						souscriteres=BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(0, -1, note.getDossier(), note.getCritere().getId(), null);
						if(noteretenue.intValue()>+souscriteres.get(0).getNote().intValue())
						{
							editRow.toggleEditable(true);
							throw new WrongValueException(notes, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.notepondere.inferieur")+" :"+souscriteres.get(0).getNote().intValue());

						}
						
						note.setNote(new BigDecimal(noteretenue));   
								
						BeanLocator.defaultLookup(DossiersSouscriteresSession.class).updateNote(note);
						submitBtn.detach();
						cancelBtn.detach();
						editBtn.setParent(ctrlDiv);
					}
				});
				notes.addEventListener(Events.ON_OK, new EventListener() {
					public void onEvent(Event event) throws Exception {

						editRow.toggleEditable(true);
						if (!ToolKermel.estReel(notes.txb.getValue())) {
							editRow.toggleEditable(true);
							throw new WrongValueException(notes, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.notepondere.numerique"));

						}
						noteretenue=Double.valueOf(Integer.parseInt(notes.txb.getValue()));
						souscriteres=BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(0, -1, note.getDossier(), note.getCritere().getId(), null);
						if(noteretenue.intValue()>+souscriteres.get(0).getNote().intValue())
						{
							editRow.toggleEditable(true);
							throw new WrongValueException(notes, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.notepondere.inferieur")+" :"+souscriteres.get(0).getNote().intValue());

						}
						
						note.setNote(new BigDecimal(noteretenue));   
								
						BeanLocator.defaultLookup(DossiersSouscriteresSession.class).updateNote(note);
						submitBtn.detach();
						cancelBtn.detach();
						editBtn.setParent(ctrlDiv);
						
					}
				});
			
				cancelBtn.setMold("os");
				cancelBtn.setHeight("20px");
				cancelBtn.setWidth("30px");
				cancelBtn.setTooltiptext("Annuler la saisie");
				cancelBtn.addEventListener(Events.ON_CLICK, new EventListener() {
					public void onEvent(Event event) throws Exception {
						editRow.toggleEditable(false);
						submitBtn.detach();
						cancelBtn.detach();
						editBtn.setParent(ctrlDiv);
					}
				});
				submitBtn.setParent(ctrlDiv);
				cancelBtn.setParent(ctrlDiv);
				editRow.toggleEditable(true);
				editBtn.detach();
			}
		});

	}
	}
}