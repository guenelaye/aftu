package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAttributions;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygAvisAttribution;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDevise;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.AttributionsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.AvisAttributionSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DeviseSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DocumentsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class AttributionprovisoireController extends AbstractWindow implements
		 AfterCompose {

	private Listbox lstListe,lstBailleur;
	private Paging pgPagination,pgBailleur;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference,libellebailleur=null;
    private int parent;
    List<SygDocuments> documents = new ArrayList<SygDocuments>();
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtVersionElectronique;
    SygBailleurs bailleur=null;
    private Decimalbox dcmontantprevu;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idbailleur;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygDocuments document=new SygDocuments();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private String nomFichier;
	private final String cheminDossier = UIConstants.PATH_PJ_PORTAIL;
	UtilVue utilVue = UtilVue.getInstance();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuValider;
	private Image image;
	private Label lbltitre,lbldeuxpoints;
	private Div step0,step1;
	private Iframe idIframe;
	private String extension,images;
	SygPlisouvertures plis=new SygPlisouvertures();
	private Textbox txtAttributaire,txtReference,txtMonnaie,txtCommentaires;
	private Intbox indelaimois,indelaisemaine,indelaijours;
	private Datebox dtdateattribution;
	private Decimalbox dcmontantlu,dcmontantcorrige,dcmontantmarche,dcmontantdefinitif;
	private BigDecimal  montantcorrige,taux,montantmarche,montantdefinitif,montantlu;
	SygDevise devise=new SygDevise();
	SygAttributions attributaire=new SygAttributions();
	SygAttributions attributaires=new SygAttributions();
	public static final String EDIT_URL = "http://"+UIConstants.IP_SSI+":"+UIConstants.PORT_SSI+"/EtatsKermel/OuverturePlisPDFServlet";
	SygAvisAttribution avisattribution=new SygAvisAttribution(); 
	List<SygPlisouvertures> soumissionnaires = new ArrayList<SygPlisouvertures>();
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,2);
		if(dossier==null)
		{
			menuValider.setDisabled(true);
			image.setVisible(false);
		}
		else
		{
			//plis=BeanLocator.defaultLookup(RegistrededepotSession.class).findSoumissionnaire(dossier,UIConstants.PARENT,  UIConstants.MOINSDISANTQUALIFIE);
			soumissionnaires=BeanLocator.defaultLookup(RegistrededepotSession.class).ClassementFinanciere(dossier, 1);
			plis=soumissionnaires.get(0);
			devise=BeanLocator.defaultLookup(DeviseSession.class).getMonnaieCFA(dossier, UIConstants.PARAM_CFA);
			if(devise!=null)
				taux=devise.getDevTauxConversion();
			else
				taux=new BigDecimal(0);
			if(plis!=null)
			{
				attributaires=BeanLocator.defaultLookup(AttributionsSession.class).findAttributaire(dossier, plis,null);
				
				txtAttributaire.setValue(plis.getRetrait().getNomSoumissionnaire());
				txtReference.setValue(realisation.getReference());
				if(dossier.getDosDateAttributionProvisoire()!=null)
				  dtdateattribution.setValue(dossier.getDosDateAttributionProvisoire());
				txtMonnaie.setValue(plis.getMonCode());
				
				if (plis.getMonCode().equals(UIConstants.PARAM_CFA))
				{
					montantlu=plis.getMontantoffert();
					montantcorrige=plis.getPrixevalue();
					montantmarche=plis.getPrixevalue();
				}
				else
				{
					if(taux.intValue()>0)
					{
						montantlu=plis.getMontantoffert().multiply(taux);
						montantcorrige=plis.getPrixevalue().multiply(taux);
						montantmarche=plis.getPrixevalue().multiply(taux);
					}
					else
					{
						montantlu=plis.getMontantoffert();
						montantcorrige=plis.getPrixevalue();
						montantmarche=plis.getPrixevalue();
					}
						
				}
				montantdefinitif=plis.getMontantdefinitif();
				dcmontantlu.setValue(montantlu);
				dcmontantcorrige.setValue(montantcorrige);
				dcmontantmarche.setValue(montantmarche);
				dcmontantdefinitif.setValue(montantdefinitif);
				if(attributaires!=null)
				{
					attributaire=attributaires;
					indelaimois.setValue(attributaire.getMoisExecution());
					indelaisemaine.setValue(attributaire.getSemaineExecution());
					indelaijours.setValue(attributaire.getJoursExecution());
					dtdateattribution.setValue(attributaire.getDateattribution());
					txtCommentaires.setValue(attributaire.getCommentaire());
					menuValider.setDisabled(true);                                    
				}
				
			}
			infos(dossier,appel);
		}
		
	}

	public void infos(SygDossiers dossier,SygAppelsOffres appel) {
		documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossier, appel,UIConstants.PARAM_TYPEDOCUMENTS_ATTIBUTIONS,null, null);
		attributaires=BeanLocator.defaultLookup(AttributionsSession.class).findAttributaire(dossier, plis,null);
		
		if(documents.size()>0)
		{
			extension=documents.get(0).getNomFichier().substring(documents.get(0).getNomFichier().length()-3,  documents.get(0).getNomFichier().length());
			 if(extension.equalsIgnoreCase("pdf"))
				 images="/images/icone_pdf.png";
			 else  
				 images="/images/word.jpg";
			 
			image.setVisible(true);
			image.setSrc(images);
			lbldeuxpoints.setValue(":");
			lbltitre.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.procesverbalouverture.versionfichier"));
				
		
		}
	}
	public void onClick$btnChoixFichier() {
		//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
			if (ToolKermel.isWindows())
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectronique.setValue(nomFichier);
		}
	
private boolean checkFieldConstraints() {
		
		try {
		
			if(indelaimois.getValue()==null)
		     {
         errorComponent = indelaimois;
         errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.mois")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(indelaimois.getValue()>48)
		     {
        errorComponent = indelaimois;
        errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai")+" "+ Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.mois")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.valeur")+":48 ";
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(indelaisemaine.getValue()==null)
		     {
        errorComponent = indelaisemaine;
        errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.semaines")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(indelaisemaine.getValue()>3)
		     {
       errorComponent = indelaisemaine;
       errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.semaines")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.valeur")+":3 ";
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(indelaijours.getValue()==null)
		     {
        errorComponent = indelaijours;
        errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.jours")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(indelaijours.getValue()>30)
		     {
      errorComponent = indelaijours;
      errorMsg =  Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.jours")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.valeur")+":30 ";
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dtdateattribution.getValue()==null)
		     {
          errorComponent = dtdateattribution;
          errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.dateattribution")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtVersionElectronique.getValue().equals(""))
		     {
         errorComponent = txtVersionElectronique;
         errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.procesverbal")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	
		public void onClick$menuValider() {
			if(checkFieldConstraints())
			{
				attributaires=BeanLocator.defaultLookup(AttributionsSession.class).findAttributaire(dossier, plis,null);
				documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossier, appel,UIConstants.PARAM_TYPEDOCUMENTS_ATTIBUTIONS,null, null);
				if(attributaires!=null)
					attributaire=attributaires;
				
				attributaire.setDossier(dossier);
				attributaire.setPlis(plis);
				attributaire.setCommentaire(txtCommentaires.getValue());
				attributaire.setDateattribution(dtdateattribution.getValue());
				attributaire.setMontantdefinitif(dcmontantdefinitif.getValue());
				attributaire.setMontantMarche(dcmontantmarche.getValue());
				attributaire.setMoisExecution(indelaimois.getValue());
				attributaire.setSemaineExecution(indelaisemaine.getValue());
				attributaire.setJoursExecution(indelaijours.getValue());
			
				avisattribution.setAutorite(autorite);
				avisattribution.setDossier(dossier);
				avisattribution.setAttriRef(appel.getAporeference());
				avisattribution.setAttriType("provisoire");
				avisattribution.setAttriDate(dtdateattribution.getValue());
				avisattribution.setAttriObjet(appel.getApoobjet());
				avisattribution.setAttriPub(UIConstants.NPARENT);
				avisattribution.setAttriRaisonsocial(plis.getRaisonsociale());
				avisattribution.setAttrifichier(txtVersionElectronique.getValue());
				if(attributaires!=null)
				{
					BeanLocator.defaultLookup(AttributionsSession.class).save(attributaire);
					BeanLocator.defaultLookup(AvisAttributionSession.class).save(avisattribution);
				}
				else
				{
					BeanLocator.defaultLookup(AttributionsSession.class).update(attributaire);
					BeanLocator.defaultLookup(AvisAttributionSession.class).update(avisattribution);
				}
				
				if(documents.size()>0)
				document=documents.get(0);
				document.setAppel(appel);
				document.setDossier(dossier);
				if(!txtVersionElectronique.getValue().equals(""))
				document.setNomFichier(txtVersionElectronique.getValue());
				document.setTypeDocument(UIConstants.PARAM_TYPEDOCUMENTS_ATTIBUTIONS);
				document.setLibelle(Labels.getLabel("kermel.plansdepassation.proceduresmarches.documents.pvattribution"));
				document.setDate(new Date());
				document.setHeure(Calendar.getInstance().getTime());
				if(documents.size()==0)
				{
					BeanLocator.defaultLookup(DocumentsSession.class).save(document);
				}
				else
				{
					BeanLocator.defaultLookup(DocumentsSession.class).update(document);
				}
			
				 dossier.setDosDateAttributionProvisoire(dtdateattribution.getValue());
				 BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).update(dossier);
				
				session.setAttribute("libelle", "attributionprovisoire");
				loadApplicationState("procedure_pi");
			}
		}
		
		public void onClick$image() {
			step0.setVisible(false);
			step1.setVisible(true);
			String filepath = cheminDossier +  documents.get(0).getNomFichier();
			File f = new File(filepath.replaceAll("\\\\", "/"));

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(f, null, null);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (mymedia != null)
				idIframe.setContent(mymedia);
			else
				idIframe.setSrc("");

			idIframe.setHeight("600px");
			idIframe.setWidth("100%");
		}
		
		public org.zkoss.util.media.AMedia fetchFile(File file) {

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(file, null, null);
				return mymedia;
			} catch (Exception e) {

				e.printStackTrace();
				return null;
			}

		}
		public void onClick$menuFermer() {
			step0.setVisible(true);
			step1.setVisible(false);
		}
		
		 public void onClick$menuEditer(){
				
			 idIframe.setSrc(EDIT_URL + "?code="+dossier.getDosID()+ "&libelle=pvattributionprovisoire");
					step0.setVisible(false);
				    step1.setVisible(true);
					
				
				
			}
}