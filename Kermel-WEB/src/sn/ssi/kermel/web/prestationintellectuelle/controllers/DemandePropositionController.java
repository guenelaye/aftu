package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class DemandePropositionController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code;
	private Tab TAB_infosgeneralesdp,TAB_criteresqualificationsdp,TAB_devise,TAB_piecesadministratives,TAB_presentationoffres;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblInfos;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		
		LibelleTab=(String) session.getAttribute("libelle");
		if(LibelleTab!=null)
		{
			if(LibelleTab.equals("infogeneralesdp"))
			  {
				TAB_infosgeneralesdp.setSelected(true);
				Include inc = (Include) this.getFellowIfAny("incinfosgeneralesdp");
				inc.setSrc("/passationsmarches/prestationintellectuelle/infogeneralesdp.zul");	
			  }
			else
			{
				if(LibelleTab.equals("criteresqualificationsdp"))
				  {
					TAB_criteresqualificationsdp.setSelected(true);
					Include inc = (Include) this.getFellowIfAny("inccriteresqualificationsdp");
					inc.setSrc("/passationsmarches/prestationintellectuelle/criteresqualificationsdp.zul");	
				  }
				else
				{
					if(LibelleTab.equals("devise"))
					  {
						TAB_devise.setSelected(true);
						Include inc = (Include) this.getFellowIfAny("incdevise");
						inc.setSrc("/passationsmarches/prestationintellectuelle/devise.zul");	
					  }
					else
					{
						if(LibelleTab.equals("piecesadministratives"))
						  {
							TAB_piecesadministratives.setSelected(true);
							Include inc = (Include) this.getFellowIfAny("incpiecesadministratives");
							inc.setSrc("/passationsmarches/prestationintellectuelle/piecesadministratives.zul");	
						  }
						else
						{
							if(LibelleTab.equals("presentationsoffres"))
							  {
								TAB_presentationoffres.setSelected(true);
								Include inc = (Include) this.getFellowIfAny("incpresentationoffres");
								inc.setSrc("/passationsmarches/prestationintellectuelle/presentationsoffres.zul");	
							  }
							else
							{
								TAB_infosgeneralesdp.setSelected(true);
								Include inc = (Include) this.getFellowIfAny("incinfosgeneralesdp");
								inc.setSrc("/passationsmarches/prestationintellectuelle/infogeneralesdp.zul");
							}
						}
					}
				}
				
			}
		
		}
		else
		{
			 Include inc = (Include) this.getFellowIfAny("incinfosgeneralesdp");
		    inc.setSrc("/passationsmarches/prestationintellectuelle/infogeneralesdp.zul");
		}
		    		
		
		   
	
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	
}