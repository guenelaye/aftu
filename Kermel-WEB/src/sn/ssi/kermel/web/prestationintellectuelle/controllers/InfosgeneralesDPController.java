package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timebox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DocumentsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class InfosgeneralesDPController extends AbstractWindow implements
		EventListener, AfterCompose {

	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle,txtVersionElectronique;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
    private String nomFichier="";
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
  //  private Textbox txtLieu;//txtLieudepot,
    SygBailleurs bailleur=null;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygDossiers dossier=new SygDossiers();
	SygDossiers dossiers=null;
	SygRealisations realisation=new SygRealisations();
	private Textbox txtnumero,txtCodeNature;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	//private Intbox intmontant,intmontantdao;
	private Datebox dtouverture,dtdepot;
	private Timebox heureouverture,heuredepot;
	private Combobox cbcible;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private Image image;
	private Div step0,step1;
	private Iframe idIframe;
	private String extension,images;
	private Menuitem menuValider;
	private Intbox intnotetechnique,intponderationtechnique,intponderationfinanciere;
	SygDossiers dossiersmi=null;
	SygDocuments document=new SygDocuments();
	List<SygDocuments> documents = new ArrayList<SygDocuments>();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
	
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		txtnumero.setValue(appel.getAporeference());
		realisation=appel.getRealisation();
		dossiersmi=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,1);
		if(dossiersmi!=null)
		{
//			txtLieudepot.setValue(dossiersmi.getDosLieuDepotDossier());
//			if(dossiersmi.getDosDateLimiteDepot()!=null)
//				dtdepot.setValue(dossiersmi.getDosDateLimiteDepot());
//			if(dossiersmi.getDosHeurelimitedepot()!=null)
//				heuredepot.setValue(dossiersmi.getDosHeurelimitedepot());
			menuValider.setDisabled(false);
		}
		dossiers=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,2);
		InfosDossiers(dossiers);
		Events.postEvent(ApplicationEvents.ON_NATURES, this, null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {


	}

	
	
	private boolean checkFieldConstraints() {
		
		try {
		
			if(txtnumero.getValue().equals(""))
		     {
               errorComponent = txtnumero;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.infosgenerales.saisietermereference.numeromi")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			
			if(cbcible.getValue().equals(""))
		     {
              errorComponent = cbcible;
              errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.cible")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(intnotetechnique.getValue()==null)
		     {
              errorComponent = intnotetechnique;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.evaluationoffres.classement.notetechniqueminimale")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(intnotetechnique.getValue()>=100)
		     {
             errorComponent = intnotetechnique;
             errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.evaluationoffres.classement.notetechniqueminimale.inferieur")+" 100. ";
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(intponderationtechnique.getValue()==null)
		     {
     errorComponent = intponderationtechnique;
     errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.evaluationoffres.classement.ponderationtechnique")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(intponderationtechnique.getValue()>=100)
		     {
            errorComponent = intponderationtechnique;
            errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.evaluationoffres.classement.ponderationtechnique.inferieur")+" 100. ";
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(intponderationtechnique.getValue()+intponderationfinanciere.getValue()>100)
		     {
    errorComponent = intponderationfinanciere;
    errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.evaluationoffres.classement.ponderationfinanciere")+"+"+Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.evaluationoffres.classement.ponderationfinanciere")
    		+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.valeur")+" 100";
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(nomFichier.equals(""))
		     {
               errorComponent = txtVersionElectronique;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.infosgenerales.saisietermereference.fichierelectroniquetdr")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
//			if(intmontant.getValue()==null)
//		     {
//         errorComponent = intmontant;
//         errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.montantgarantie")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
//			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
//			   lbStatusBar.setValue(errorMsg);
//			  throw new WrongValueException (errorComponent, errorMsg);
//		     }
//			if(intmontantdao.getValue()==null)
//		     {
//        errorComponent = intmontantdao;
//        errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.montantdao")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
//			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
//			   lbStatusBar.setValue(errorMsg);
//			  throw new WrongValueException (errorComponent, errorMsg);
//		     }
		
//			if(txtLieudepot.getValue().equals(""))
//		     {
//       errorComponent = txtLieudepot;
//       errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.infosgenerales.saisietermereference.lieudepot")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
//			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
//			   lbStatusBar.setValue(errorMsg);
//			  throw new WrongValueException (errorComponent, errorMsg);
//		     }
			if(dtdepot.getValue()==null)
		     {
       errorComponent = dtdepot;
       errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.infosgenerales.saisietermereference.datelimitedepot")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((dossiersmi.getDosDateLimiteDepot()).after(dtdepot.getValue()))
			 {
				errorComponent = dtdepot;
				errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.infosgenerales.saisietermereference.datelimitedepot")+" "+Labels.getLabel("kermel.referentiel.date.posterieure")+": "+UtilVue.getInstance().formateLaDate(dossiersmi.getDosDateLimiteDepot());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			if(heuredepot.getValue()==null)
		     {
       errorComponent = heuredepot;
       errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.infosgenerales.saisietermereference.heurelimitedepot")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
//			if(txtLieu.getValue().equals(""))
//		     {
//      errorComponent = txtLieu;
//      errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.infosgenerales.demandeproposition.lieuouvertureplis")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
//			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
//			   lbStatusBar.setValue(errorMsg);
//			  throw new WrongValueException (errorComponent, errorMsg);
//		     }
			if(dtdepot.getValue()==null)
		     {
      errorComponent = dtdepot;
      errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.infosgenerales.demandeproposition.dateouvertureplis")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((dtdepot.getValue()).after(dtouverture.getValue()))
			 {
				errorComponent = dtouverture;
				errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.infosgenerales.demandeproposition.dateouvertureplis")+" "+Labels.getLabel("kermel.referentiel.date.posterieure")+": "+UtilVue.getInstance().formateLaDate(dtdepot.getValue());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			if(heuredepot.getValue()==null)
		     {
      errorComponent = heuredepot;
      errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.infosgenerales.demandeproposition.heureouvertureplis")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	
	public void onBlur$intponderationtechnique() {
		if(intponderationtechnique.getValue()!=null)
		{
			if(intponderationtechnique.getValue()<100)
				intponderationfinanciere.setValue(100-intponderationtechnique.getValue());
		}
			
	}
	public void onClick$btnChoixFichier() {
		//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
			if (ToolKermel.isWindows())
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectronique.setValue(nomFichier);
		}
	
	
	
	
		
		public void onClick$menuValider() {
			if(checkFieldConstraints())
			{
				dossier.setDosReference(txtnumero.getValue());
//				dossier.setDosSoumission(intmontant.getValue());
//				dossier.setDosmontantdao(intmontantdao.getValue());
				dossier.setDosDateCreation(new Date());
				//dossier.setDosLieuDepotDossier(txtLieudepot.getValue());
				dossier.setDosDateLimiteDepot(dtdepot.getValue());
				dossier.setDosHeurelimitedepot(heuredepot.getValue());
				//dossier.setDosLieuOuvertureDesPlis(txtLieu.getValue());
				dossier.setDosDateOuvertueDesplis(dtouverture.getValue());
				dossier.setDosHeureOuvertureDesPlis(heureouverture.getValue());
				dossier.setDosNoteEliminatoireAmi(intnotetechnique.getValue());
				dossier.setDosponderationtechnique(intponderationtechnique.getValue());
				dossier.setDosponderationfinanciere(intponderationfinanciere.getValue());
				if(!txtVersionElectronique.getValue().equals(""))
				dossier.setDosFichier(nomFichier);
				dossier.setAppel(appel);
				dossier.setRealisation(realisation);
				dossier.setAutorite(autorite);
				dossier.setEtatPrequalif(2);
				
				document.setAppel(appel);
				if(!txtVersionElectronique.getValue().equals(""))
				document.setNomFichier(txtVersionElectronique.getValue());
				document.setTypeDocument(UIConstants.PARAM_TYPEDOCUMENTS_AJOUTDOSSIERS);
				document.setLibelle(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.infosgenerales.saisietermereference.fichierelectroniquetdr"));
				document.setDate(new Date());
				document.setHeure(Calendar.getInstance().getTime());
				if(dossiers==null)
				{
					dossiers=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).save(dossier);
					document.setDossier(dossier);
					//BeanLocator.defaultLookup(DocumentsSession.class).save(document);
				}
				else
				{
					BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).update(dossier);
					//BeanLocator.defaultLookup(DocumentsSession.class).update(document);
				}
					
				
				session.setAttribute("libelle", "infogeneralesdp");
				loadApplicationState("procedure_pi");
				
				
				
			}
		}
		public void InfosDossiers(SygDossiers dossiers) {
			if(dossiers!=null)
			{
				
				if(dossiers.getDosDateMiseValidation()!=null||dossiers.getDosDatePublication()!=null)
					menuValider.setDisabled(true);
				dossier=dossiers;
				txtnumero.setValue(dossier.getDosReference());
				intnotetechnique.setValue(dossier.getDosNoteEliminatoireAmi());
//				intmontant.setValue(dossier.getDosSoumission());
//				intmontantdao.setValue(dossier.getDosmontantdao());
			
				//txtLieu.setValue(dossier.getDosLieuOuvertureDesPlis());
				dtouverture.setValue(dossier.getDosDateOuvertueDesplis());
				heureouverture.setValue(dossier.getDosHeureOuvertureDesPlis());
				
			//	txtLieudepot.setValue(dossier.getDosLieuDepotDossier());
				dtdepot.setValue(dossier.getDosDateLimiteDepot());
				heuredepot.setValue(dossier.getDosHeurelimitedepot());
				intponderationtechnique.setValue(dossier.getDosponderationtechnique());
				intponderationfinanciere.setValue(dossier.getDosponderationfinanciere());
				extension=dossier.getDosFichier().substring(dossier.getDosFichier().length()-3,  dossier.getDosFichier().length());
				 if(extension.equalsIgnoreCase("pdf"))
					 images="/images/icone_pdf.png";
				 else  
					 images="/images/word.jpg";
				 
				image.setVisible(true);
				image.setSrc(images);
				nomFichier=dossier.getDosFichier();
				
				documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossier, appel,UIConstants.PARAM_TYPEDOCUMENTS_AJOUTDOSSIERS,null, null);
				if(documents.size()>0)
				document=documents.get(0);
			}
		
		}
		public void onClick$image() {
			step0.setVisible(false);
			step1.setVisible(true);
			String filepath = cheminDossier +  dossier.getDosFichier();
			File f = new File(filepath.replaceAll("\\\\", "/"));

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(f, null, null);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (mymedia != null)
				idIframe.setContent(mymedia);
			else
				idIframe.setSrc("");

			idIframe.setHeight("600px");
			idIframe.setWidth("100%");
		}
		
		public org.zkoss.util.media.AMedia fetchFile(File file) {

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(file, null, null);
				return mymedia;
			} catch (Exception e) {

				e.printStackTrace();
				return null;
			}

		}
		public void onClick$menuFermer() {
			step0.setVisible(true);
			step1.setVisible(false);
		}
}