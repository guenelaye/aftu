package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCritere;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersSouscriteresSession;
import sn.ssi.kermel.be.referentiel.ejb.CritereSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class CriteresqualificationsController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer, RowRenderer, RowRendererExt{

	private Listbox lstCriteres;
	private Paging pgPagination,pgCriteres;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
  	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	SygCritere critere=new SygCritere();
	SygCritere newcritere=new SygCritere();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuAjouter,menuNouveauCritere;
	private Div step0,step1,step2;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Textbox txtlibelle;
	private Grid GridListe;
	List<SygDossierssouscriteres> listescriteres = new ArrayList<SygDossierssouscriteres>();
	SygDossierssouscriteres criteredossier=new SygDossierssouscriteres();
	private BigDecimal totalnote=new BigDecimal(0);
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		//lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
    	
		addEventListener(ApplicationEvents.ON_CRITERES, this);
		lstCriteres.setItemRenderer(new PiecesRenderer());
		pgCriteres.setPageSize(byPage);
		pgCriteres.addForward("onPaging", this, ApplicationEvents.ON_CRITERES);
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,1);
		if(dossier==null)
		{
			menuAjouter.setDisabled(true);
    		menuNouveauCritere.setDisabled(true);
		}
		else
		{
			if(dossier.getDosDateMiseValidation()!=null||dossier.getDosDatePublication()!=null)
			{
				menuAjouter.setDisabled(true);
				menuNouveauCritere.setDisabled(true);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			Events.postEvent(ApplicationEvents.ON_PIECES, this, null);
		}
		
	}
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			  GridListe.setRowRenderer(this);
			  listescriteres= BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(activePage,byPage,dossier, null, null);
			 GridListe.setModel(new SimpleListModel(listescriteres));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(DossiersSouscriteresSession.class).count(dossier, null, null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CRITERES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgCriteres.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgCriteres.getActivePage() * byPage;
				pgCriteres.setPageSize(byPage);
			}
			 List<SygCritere> criteres = BeanLocator.defaultLookup(CritereSession.class).ListeCriteres(activePage,byPage,libelle,dossier.getDosID(),autorite.getId(), null);
			 lstCriteres.setModel(new SimpleListModel(criteres));
			 pgCriteres.setTotalSize(BeanLocator.defaultLookup(CritereSession.class).ListeCriteres(libelle,dossier.getDosID(),autorite.getId(), null).size());
		}
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			 Long id = (Long) ((HashMap<String, Object>) event.getData()).get(CURRENT_MODULE);
			 BeanLocator.defaultLookup(DossiersSouscriteresSession.class).delete(id);
			// if (confirmer != null && confirmer.equalsIgnoreCase("Cession_Confirmer")) 
		
			session.setAttribute("libelle", "criteresqualifications");
			loadApplicationState("procedure_pi");
		}
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygDossierssouscriteres dossiers = (SygDossierssouscriteres) data;
		item.setValue(dossiers);

		 Listcell cellLibelle = new Listcell(dossiers.getCritere().getLibelle());
		 cellLibelle.setParent(item);
		 
		
	}

	
	public class PiecesRenderer implements ListitemRenderer{
		
		
		
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygCritere criteres = (SygCritere) data;
		item.setValue(criteres);

		 Listcell cellLibelle = new Listcell(criteres.getLibelle());
		 cellLibelle.setParent(item);
		 
		
	
	}
	}
	public void onClick$menuSupprimer()
	{
//		if (lstListe.getSelectedItem() == null)
//			
//			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
//		
//
//		HashMap<String, String> display = new HashMap<String, String>(); // permet
//		display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
//		display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
//		display.put(MessageBoxController.DSP_HEIGHT, "250px");
//		display.put(MessageBoxController.DSP_WIDTH, "47%");
//
//		HashMap<String, Object> map = new HashMap<String, Object>(); // permet
//		map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
//		showMessageBox(display, map);
		
	}
	public void onClick$menuAjouter()
	{
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
		Events.postEvent(ApplicationEvents.ON_CRITERES, this, null);
	}
	public void onClick$menuFermer()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
		
	}
	public void onClick$menuValider()
	{
       if (lstCriteres.getSelectedItem() == null)
			throw new WrongValueException(lstCriteres, Labels.getLabel("kermel.error.select.item"));
       
       for (int i = 0; i < lstCriteres.getSelectedCount(); i++) {
    	   SygDossierssouscriteres dossiercritere=new SygDossierssouscriteres();
    	   critere=(SygCritere) ((Listitem) lstCriteres.getSelectedItems().toArray()[i]).getValue();
    	   dossiercritere.setCritere(critere);
    	   dossiercritere.setDossier(dossier);
    	   dossiercritere.setNote(new BigDecimal(0));
    	   BeanLocator.defaultLookup(DossiersSouscriteresSession.class).save(dossiercritere);
       }
       session.setAttribute("libelle", "criteresqualifications");
		loadApplicationState("procedure_pi");
	}
	
	public void onClick$menuNouveauCritere()
	{
		step0.setVisible(false);
		step1.setVisible(false);
		step2.setVisible(true);
		
	}
	
	 public void onClick$menuValiderNouveau()  {
		 if(checkFieldConstraints())
		 {
		 newcritere.setLibelle(txtlibelle.getValue());
		 newcritere.setAutorite(autorite);
		 newcritere= BeanLocator.defaultLookup(CritereSession.class).save(newcritere); 
		 
		 SygDossierssouscriteres dossiercritere=new SygDossierssouscriteres();
  	     dossiercritere.setCritere(newcritere);
  	     dossiercritere.setDossier(dossier);
  	     dossiercritere.setNote(new BigDecimal(0));
  	     BeanLocator.defaultLookup(DossiersSouscriteresSession.class).save(dossiercritere);
		 session.setAttribute("libelle", "criteresqualifications");
		loadApplicationState("procedure_pi");
		 }

	 }
	 
	 private boolean checkFieldConstraints() {
			try {
				
				 if(txtlibelle.getValue().equals(""))
			     {
				 errorComponent = txtlibelle;
				 errorMsg = Labels.getLabel("kermel.referentiel.common.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				 lbStatusBar.setStyle(ERROR_MSG_STYLE);
				 lbStatusBar.setValue(errorMsg);
				 throw new WrongValueException (errorComponent, errorMsg);
			    }
				
				
				return true;
					
			}
			catch (Exception e) {
				errorMsg = Labels.getLabel("kermel.erreur.champobligatoire") + ": " + e.toString()
				+ " [checkFieldConstraints]";
				errorComponent = null;
				return false;

				
			}
			
		}
	 
	 public void onClick$menuFermerNouveau()
		{
			step0.setVisible(true);
			step1.setVisible(false);
			step2.setVisible(false);
			
		}
	 
		@Override
		public Row newRow(Grid grid) {
			// Create EditableRow instead of Row(default)
			Row row = new EditableRow();
			row.applyProperties();
			return row;
		}

		@Override
		public Component newCell(Row row) {
			return null;// Default Cell
		}

		@Override
		public int getControls() {
			return RowRendererExt.DETACH_ON_RENDER; // Default Value
		}
	 @Override
		public void render(Row row, Object data, int index) throws Exception {
			final SygDossierssouscriteres criteres = (SygDossierssouscriteres) data;
			
			final EditableRow editRow = (EditableRow) row;
			
			final EditableDiv critere = new EditableDiv(criteres.getCritere().getLibelle(), false);
			critere.txb.setReadonly(true);
			critere.setParent(editRow);
			
			
			final EditableDiv note = new EditableDiv(ToolKermel.format2Decimal(criteres.getNote()), false);
			note.setParent(editRow);
			
			final Div ctrlDiv = new Div();
			ctrlDiv.setParent(editRow);
			final Button editBtn = new Button(null, "/images/disk.png");
			editBtn.setMold("os");
			editBtn.setHeight("20px");
			editBtn.setWidth("30px");
			editBtn.setParent(ctrlDiv);
			
			final Button deleteBtn = new Button(null, "/images/delete.png");
			deleteBtn.setMold("os");
			deleteBtn.setHeight("20px");
			deleteBtn.setWidth("30px");
			deleteBtn.setParent(ctrlDiv);
			if(criteres.getDossier().getDosDateMiseValidation()!=null||criteres.getDossier().getDosDatePublication()!=null)
			{
				editBtn.setDisabled(true);
				deleteBtn.setDisabled(true);
			}

			// Button listener - control the editable of row
			editBtn.addEventListener(Events.ON_CLICK, new EventListener() {
				public void onEvent(Event event) throws Exception {
					final Button submitBtn = (Button) new Button(null, "/images/ok.png");
					final Button cancelBtn = (Button) new Button(null, "/images/cancel.png");
					submitBtn.setMold("os");
					submitBtn.setHeight("20px");
					submitBtn.setWidth("30px");
					submitBtn.setTooltiptext("Valider la saisie");
					submitBtn.addEventListener(Events.ON_CLICK, new EventListener() {
						public void onEvent(Event event) throws Exception {

							editRow.toggleEditable(true);
							if(!ToolKermel.estLong(note.txb.getValue())){
								editRow.toggleEditable(true);
								throw new WrongValueException(note, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.notepondere.numerique"));
								
	
							}
						
							totalnote=new BigDecimal(0);
							if(listescriteres.size()==2)
							{
								criteres.setNote(new BigDecimal(Long.parseLong(note.txb.getValue())));
								BeanLocator.defaultLookup(DossiersSouscriteresSession.class).update(criteres);
							
								for(int i=0;i<listescriteres.size();i++)
								{
									if(criteres.getId()!=listescriteres.get(i).getId())
									{
										criteredossier=listescriteres.get(i);
										criteredossier.setNote(new BigDecimal(100-Long.parseLong(note.txb.getValue())));
										BeanLocator.defaultLookup(DossiersSouscriteresSession.class).update(criteredossier);
									}
								}
								
							}
							else
							{
								for(int i=0;i<listescriteres.size();i++)
								{
									if(criteres.getId()!=listescriteres.get(i).getId())
									{
										totalnote=totalnote.add(listescriteres.get(i).getNote());
									}
								}
								totalnote=totalnote.add(new BigDecimal(Long.parseLong(note.txb.getValue())));
								if(totalnote.intValue()>100)
		 						{
		 							throw new WrongValueException(note, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.notepondere.sommes.inferieur")+" :100");

		 						}
								criteres.setNote(new BigDecimal(Long.parseLong(note.txb.getValue())));
								BeanLocator.defaultLookup(DossiersSouscriteresSession.class).update(criteres);
							}
						
							submitBtn.detach();
							cancelBtn.detach();
							editBtn.setParent(ctrlDiv);
							deleteBtn.setParent(ctrlDiv);
							session.setAttribute("libelle", "criteresqualifications");
							loadApplicationState("procedure_pi");
						}
					});
					note.addEventListener(Events.ON_OK, new EventListener() {
						public void onEvent(Event event) throws Exception {

							editRow.toggleEditable(true);
							if(!ToolKermel.estLong(note.txb.getValue())){
								editRow.toggleEditable(true);
								throw new WrongValueException(note, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.notepondere.numerique"));
	
							}
						
							totalnote=new BigDecimal(0);
							if(listescriteres.size()==2)
							{
								criteres.setNote(new BigDecimal(Long.parseLong(note.txb.getValue())));
								BeanLocator.defaultLookup(DossiersSouscriteresSession.class).update(criteres);
							
								for(int i=0;i<listescriteres.size();i++)
								{
									if(criteres.getId()!=listescriteres.get(i).getId())
									{
										criteredossier=listescriteres.get(i);
										criteredossier.setNote(new BigDecimal(100-Long.parseLong(note.txb.getValue())));
										BeanLocator.defaultLookup(DossiersSouscriteresSession.class).update(criteredossier);
									}
								}
								
							}
							else
							{
								for(int i=0;i<listescriteres.size();i++)
								{
									if(criteres.getId()!=listescriteres.get(i).getId())
									{
										totalnote=totalnote.add(listescriteres.get(i).getNote());
									}
								}
								totalnote=totalnote.add(new BigDecimal(Long.parseLong(note.txb.getValue())));
								if(totalnote.intValue()>100)
		 						{
		 							throw new WrongValueException(note, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.notepondere.sommes.inferieur")+" :100");

		 						}
								criteres.setNote(new BigDecimal(Long.parseLong(note.txb.getValue())));
								BeanLocator.defaultLookup(DossiersSouscriteresSession.class).update(criteres);
							}
						
							submitBtn.detach();
							cancelBtn.detach();
							editBtn.setParent(ctrlDiv);
							deleteBtn.setParent(ctrlDiv);
							session.setAttribute("libelle", "criteresqualifications");
							loadApplicationState("procedure_pi");
						}
					});
					note.addEventListener(Events.ON_CLICK, new EventListener() {
						public void onEvent(Event event) throws Exception {
							if(!ToolKermel.estLong(note.txb.getValue())){
								editRow.toggleEditable(true);
								throw new WrongValueException(note, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.notepondere.numerique"));

							}
							totalnote=new BigDecimal(0);
							if(listescriteres.size()==2)
							{
								criteres.setNote(new BigDecimal(Long.parseLong(note.txb.getValue())));
								BeanLocator.defaultLookup(DossiersSouscriteresSession.class).update(criteres);
							
								for(int i=0;i<listescriteres.size();i++)
								{
									if(criteres.getId()!=listescriteres.get(i).getId())
									{
										criteredossier=listescriteres.get(i);
										criteredossier.setNote(new BigDecimal(100-Long.parseLong(note.txb.getValue())));
										BeanLocator.defaultLookup(DossiersSouscriteresSession.class).update(criteredossier);
									}
								}
								
							}
							else
							{
								for(int i=0;i<listescriteres.size();i++)
								{
									if(criteres.getId()!=listescriteres.get(i).getId())
									{
										totalnote=totalnote.add(listescriteres.get(i).getNote());
									}
								}
								totalnote=totalnote.add(new BigDecimal(Long.parseLong(note.txb.getValue())));
								if(totalnote.intValue()>100)
		 						{
		 							throw new WrongValueException(note, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.notepondere.sommes.inferieur")+" :100");

		 						}
								criteres.setNote(new BigDecimal(Long.parseLong(note.txb.getValue())));
								BeanLocator.defaultLookup(DossiersSouscriteresSession.class).update(criteres);
							}
							session.setAttribute("libelle", "criteresqualifications");
							loadApplicationState("procedure_pi");
							
						}
					});
					cancelBtn.setMold("os");
					cancelBtn.setHeight("20px");
					cancelBtn.setWidth("30px");
					cancelBtn.setTooltiptext("Annuler la saisie");
					cancelBtn.addEventListener(Events.ON_CLICK, new EventListener() {
						public void onEvent(Event event) throws Exception {
							editRow.toggleEditable(false);
							submitBtn.detach();
							cancelBtn.detach();
							editBtn.setParent(ctrlDiv);
							deleteBtn.setParent(ctrlDiv);
						}
					});
					submitBtn.setParent(ctrlDiv);
					cancelBtn.setParent(ctrlDiv);
					editRow.toggleEditable(true);
					editBtn.detach();
					deleteBtn.detach();
				}
			});
			
			deleteBtn.addEventListener(Events.ON_CLICK, new EventListener() {
				public void onEvent(Event event) throws Exception {
				
					HashMap<String, String> display = new HashMap<String, String>(); // permet
					display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
					display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
					display.put(MessageBoxController.DSP_HEIGHT, "250px");
					display.put(MessageBoxController.DSP_WIDTH, "47%");

					HashMap<String, Object> map = new HashMap<String, Object>(); // permet
					map.put(CURRENT_MODULE, criteres.getId());
					showMessageBox(display, map);
				}
			});
			
		
		}
		
}