package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDevise;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygMonnaieoffre;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DeviseSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.referentiel.ejb.MonnaieoffreSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class DevisController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstMonnaie;
	private Paging pgPagination,pgMonnaie;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libellemonnaie=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherMonnaie;
    SygBailleurs bailleur=null;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long iddevise=null;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	SygDevise devise=new SygDevise();
	SygMonnaieoffre monnaie=new SygMonnaieoffre();
	private Bandbox bdMonnaie;
	private Decimalbox dcTaux;
	private Div step0,step1;
	private Menuitem menuAjouter,menuModifier,menuSupprimer;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
    	lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
		addEventListener(ApplicationEvents.ON_MONNAIES, this);
		pgMonnaie.setPageSize(byPage);
		pgMonnaie.addForward("onPaging", this, ApplicationEvents.ON_MONNAIES);
		lstMonnaie.setItemRenderer(new MonnaiesRenderer());
    
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,2);
		if(dossier==null)
		{
			menuAjouter.setDisabled(true);
			menuModifier.setDisabled(true);
			menuSupprimer.setDisabled(true);
		}
		else
		{
			if(dossier.getDosDateMiseValidation()!=null||dossier.getDosDatePublication()!=null)
			{
				menuAjouter.setDisabled(true);
				menuModifier.setDisabled(true);
				menuSupprimer.setDisabled(true);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			Events.postEvent(ApplicationEvents.ON_MONNAIES, this, null);
		}
		
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygDevise> devises = BeanLocator.defaultLookup(DeviseSession.class).find(activePage,byPageBandbox,dossier);
			 lstListe.setModel(new SimpleListModel(devises));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(DeviseSession.class).count(dossier));
		} 
	
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = ((SygDevise)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue()).getDevId();
				BeanLocator.defaultLookup(DeviseSession.class).delete(codes);
			}
			session.setAttribute("libelle", "devise");
			loadApplicationState("procedure_pi");
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MONNAIES)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgMonnaie.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgMonnaie.getActivePage() * byPageBandbox;
				pgMonnaie.setPageSize(byPageBandbox);
			}
			List<SygMonnaieoffre> monnaies = BeanLocator.defaultLookup(MonnaieoffreSession.class).ListeMonnaies(activePage, byPageBandbox,libellemonnaie,dossier.getDosID());
			lstMonnaie.setModel(new SimpleListModel(monnaies));
			pgMonnaie.setTotalSize(BeanLocator.defaultLookup(MonnaieoffreSession.class).ListeMonnaies(activePage, byPageBandbox,libellemonnaie,dossier.getDosID()).size());
		}
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygDevise devises = (SygDevise) data;
		item.setValue(devises);

		 Listcell cellMonnaie = new Listcell(devises.getMonnaie().getMonCode());
		 cellMonnaie.setParent(item);
		 
		 Listcell cellTaux = new Listcell(ToolKermel.format2Decimal(devises.getDevTauxConversion()));
		 cellTaux.setParent(item);
	}

	public void onClick$menuSupprimer()
	{
		if (lstListe.getSelectedItem() == null)
			
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		

		HashMap<String, String> display = new HashMap<String, String>(); // permet
		display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
		display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
		display.put(MessageBoxController.DSP_HEIGHT, "250px");
		display.put(MessageBoxController.DSP_WIDTH, "47%");

		HashMap<String, Object> map = new HashMap<String, Object>(); // permet
		map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
		showMessageBox(display, map);
		
	}
	public void onClick$menuModifier()
	{
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		devise=(SygDevise) lstListe.getSelectedItem().getValue();
		iddevise=devise.getDevId();
		monnaie=devise.getMonnaie();
		bdMonnaie.setValue(monnaie.getMonLibelle());
		dcTaux.setValue(devise.getDevTauxConversion());
		step0.setVisible(false);
		step1.setVisible(true);
	}
	
	///////////Monnaies///////// 
	public void onSelect$lstMonnaie(){
		monnaie= (SygMonnaieoffre) lstMonnaie.getSelectedItem().getValue();
		bdMonnaie.setValue(monnaie.getMonLibelle());
		bdMonnaie.close();
	
	}
	
	public class MonnaiesRenderer implements ListitemRenderer{
	
	
	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygMonnaieoffre monnaie = (SygMonnaieoffre) data;
		item.setValue(monnaie);
		
		
		Listcell cellCode = new Listcell(monnaie.getMonCode());
		cellCode.setParent(item);
		
		Listcell cellLibelle = new Listcell(monnaie.getMonLibelle());
		cellLibelle.setParent(item);
	
	}
	}
	public void onFocus$txtRechercherMonnaie(){
	if(txtRechercherMonnaie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.devise.monnaie"))){
		txtRechercherMonnaie.setValue("");
	}		 
	}
	
	public void  onClick$btnRechercherMonnaie(){
	if(txtRechercherMonnaie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.devise.monnaie")) || txtRechercherMonnaie.getValue().equals("")){
		libellemonnaie = null;
		page=null;
	}else{
		libellemonnaie = txtRechercherMonnaie.getValue();
		page="0";
	}
	Events.postEvent(ApplicationEvents.ON_MONNAIES, this, page);
	}

	

	private boolean checkFieldConstraints() {
		
		try {
		
			if(bdMonnaie.getValue().equals(""))
		     {
               errorComponent = bdMonnaie;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.devise.monnaie")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dcTaux.getValue()==null)
		     {
              errorComponent = dcTaux;
              errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.devise.dtauxconversion")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			return true;
			
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	public void  onClick$menuFermer(){
		step0.setVisible(true);
		step1.setVisible(false);
	}
	public void  onClick$menuAjouter(){
		step0.setVisible(false);
		step1.setVisible(true);
		iddevise=null;
		bdMonnaie.setValue("");
		dcTaux.setValue(new BigDecimal("0"));
	}
	public void  onClick$menuValider(){
		if(checkFieldConstraints())
		{
			devise.setDevTauxConversion(dcTaux.getValue());
			devise.setDossier(dossier);
			devise.setMonnaie(monnaie);
			if(iddevise==null)
				BeanLocator.defaultLookup(DeviseSession.class).save(devise);
			else
				BeanLocator.defaultLookup(DeviseSession.class).update(devise);
			session.setAttribute("libelle", "devise");
			loadApplicationState("procedure_pi");
			
		}
	}
}