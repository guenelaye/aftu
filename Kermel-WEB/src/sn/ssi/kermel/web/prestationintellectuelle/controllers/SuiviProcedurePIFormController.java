package sn.ssi.kermel.web.prestationintellectuelle.controllers;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAttributions;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDevise;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossiersEvaluateurs;
import sn.ssi.kermel.be.entity.SygDossierscommissionsmarches;
import sn.ssi.kermel.be.entity.SygDossierspieces;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygDossierssouscriteresevaluateurs;
import sn.ssi.kermel.be.entity.SygMontantsSeuils;
import sn.ssi.kermel.be.entity.SygObservateursIndependants;
import sn.ssi.kermel.be.entity.SygPiecesplisouvertures;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygPresenceouverture;
import sn.ssi.kermel.be.entity.SygRepresentantsServicesTechniques;
import sn.ssi.kermel.be.entity.SygResultatNegociation;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.ContratsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DeviseSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DocumentsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersEvaluateursSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersPiecesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersSouscriteresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossierscommissionsmarchesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.ObservateursIndependantsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.PiecessoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.Presence0uvertureSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrederetraitdaoSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RepresentantsServicesTechniquesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.ResultatNegociationSession;
import sn.ssi.kermel.be.referentiel.ejb.MontantsSeuilsSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



/**

 * @author Adama samb
 * @since mardi 11 Janvier 2011, 12:34
 
 */
public class SuiviProcedurePIFormController extends AbstractWindow implements AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private Include pgActes;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	Session session = getHttpSession();
	private Tree tree;
	private Include idinclude;
	List<SygMontantsSeuils> seuils = new ArrayList<SygMontantsSeuils>();
	private SygAutoriteContractante autorite;
	private Label lblObjets,lblModePassation,lblMontant,lblModeSelection,lblType,lgtitre,lblDatecreation;
	private String LibelleTab,libelle,avalider="oui";
	SygDossiers dossiermi=new SygDossiers();
	SygDossiers dossierdp=new SygDossiers();
	SygAttributions attributaire=new SygAttributions();
	private Treeitem treemanifestationdinteret,treetraitementdossier,treepreparation,treetermereference,
	treemiseenvalidation,treepublication,treedepotcandidatures,treeevaluation,treepreselection,treenotificationauxcandidatsretenus,treerecours,
	treedocuments,treejournalevenements,treedemandeproposition,itemapprobation,itemevaluationattributionprovisoire,treeouvertureplis,treepreparationdp,
	treetermereferencedp,treemiseenvalidationdp,treeregistredepot,treeouvertureplisdp,treelistepresencemembrescommissions,treeincidents,treecompositioncommissiontechnique,
	treeprocesverbalouverture,treelecturesoffres,treepiecessoumissionnaires,treeobservateursindependants,treerepresentantsservicestechniques,
	treerepresentantssoumissionnaires,treeevaluationoffre,treetransmissiondossier,treeverificationconformite,treerapportevaluation,treenotestechniques,treeouvertureoffrefinancieres,
	treelistepresencemembrescommissionsdp,treerepresentantssoumissionnairesdp,treesaisiprixevalues,treeoffresfinancieres,treeresultatnegociation
	,treepubattriprovisoire,treeattributionprovisoire,treesoumissionpourvalidationattributionprovisoire,treepubattribdefinitive,treenotificationmarche,
	treeapprobationprocedure,treesignaturemarche,itemimmatriculation;
	private Treecell cellmanifestationdinteret,celltraitementdossier,cellpreparation,celltermereference,cellmiseenvalidation,cellpublication,celldepotcandidatures,
	cellevaluation,cellpreselection,cellnotificationauxcandidatsretenus,celldemandeproposition,cellpreparationdp,celltermereferencedp,cellmiseenvalidationdp,
	cellouverturesplis,cellregistredepot,cellouvertureplisdp,celllistepresencemembrescommissions,cellrepresentantssoumissionnaires,cellrepresentantsservicestechniques
	,cellobservateursindependants,cellpiecessoumissionnaires,celllecturesoffres,cellcompositioncommissiontechnique,cellincidents,
	cellouvertureevaluationoffresfin,cellevaluationoffre,celltransmissiondossier,cellverificationconformite,cellnotestechniques,cellrapportevaluation,
	cellouvertureoffrefinancieres,celllistepresencemembrescommissionsdp,cellrepresentantssoumissionnairesdp,celloffresfinancieres,
	cellsaisiprixevalues,cellresultatnegociation,cellattributionprovisoire,cellsoumissionpourvalidationattributionprovisoire,cellpubattriprovisoire,
	cellapprobation,cellsignaturemarche,cellapprobationprocedure,cellnotificationmarche,cellpubattribdefinitive,cellprocesverbalouverture,cellclassementfinal,
	cellclassementfinanciere,cellimmatriculation,celldao;
	List<SygDossierssouscriteres> criteres = new ArrayList<SygDossierssouscriteres>();
	List<SygDossierssouscriteres> criteresdp = new ArrayList<SygDossierssouscriteres>();
	private int nombreprep=0,nombreeval=0,nombreprepdp=0,nombreapprobation=0,nombreeop=0,nombreeap=0,nombreoof=0,nombreev=0,nombreimmatriculation=0,saisioffrefinanciere=0;
	List<SygRetraitregistredao> retraits = new ArrayList<SygRetraitregistredao>();
	List<SygPlisouvertures> plis = new ArrayList<SygPlisouvertures>();
	List<SygDevise> devises = new ArrayList<SygDevise>();
	List<SygDossierspieces> piecesadministratives = new ArrayList<SygDossierspieces>();
	List<SygDossierspieces> piecesadministrativesdp = new ArrayList<SygDossierspieces>();
	List<SygDossierscommissionsmarches> membrescommissions = new ArrayList<SygDossierscommissionsmarches>();
	List<SygPresenceouverture> representantsoummissionnaires = new ArrayList<SygPresenceouverture>();
	List<SygRepresentantsServicesTechniques> representantservicestechniques = new ArrayList<SygRepresentantsServicesTechniques>();
	List<SygObservateursIndependants> observateursindependants = new ArrayList<SygObservateursIndependants>();
	List<SygPiecesplisouvertures> piecessoumissionnaires = new ArrayList<SygPiecesplisouvertures>();
	List<SygDossiersEvaluateurs> compositioncommissiontechnique = new ArrayList<SygDossiersEvaluateurs>();
	List<SygPlisouvertures> lecturesoffres = new ArrayList<SygPlisouvertures>();
	SygResultatNegociation resultats=new SygResultatNegociation();
	List<SygDossierssouscriteresevaluateurs> notes = new ArrayList<SygDossierssouscriteresevaluateurs>();
	List<SygDocuments> documents = new ArrayList<SygDocuments>();
	List<SygDocuments> documentsnotifies = new ArrayList<SygDocuments>();
	SygContrats contrat=new SygContrats();
	 
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		


	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}
	
	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		LibelleTab=(String) session.getAttribute("libelle");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		
		lblObjets.setValue(appel.getApoobjet());
		lblModePassation.setValue(appel.getModepassation().getLibelle());
		lblMontant.setValue(ToolKermel.format2Decimal(appel.getApomontantestime()));
		lblModeSelection.setValue(appel.getModeselection().getLibelle());
		lblType.setValue(appel.getTypemarche().getLibelle());
		
		lblDatecreation.setValue(UtilVue.getInstance().formateLaDate2(appel.getApodatecreation()));
		
		
		dossiermi=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,1);
		dossierdp=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,2);
		seuils = BeanLocator.defaultLookup(MontantsSeuilsSession.class).find(0,-1, autorite.getType(), appel.getTypemarche(), appel.getModepassation(), null, null,UIConstants.TYPE_SEUILSRAPRIORI);
		
		if(seuils.size()>0)
		{
			if(seuils.get(0).getMontantinferieur()!=null)
			{
				if((appel.getApomontantestime().compareTo(seuils.get(0).getMontantinferieur())==1)||(appel.getApomontantestime().equals(seuils.get(0).getMontantinferieur())))
				
				{
					avalider="oui";
					treemiseenvalidation.setVisible(true);
				}
				else
				{
					avalider="non";
					treemiseenvalidation.setVisible(false);
					
				}
					
			}
			
		}
		//////////Libelle//////// tooltiptext
		if(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis").length()>20)
		  cellouverturesplis.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis").substring(0, 20)+"...");
		else
		  cellouverturesplis.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis"));
		
		if(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.representantservicetechnique").length()>20)
		  cellrepresentantsservicestechniques.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.representantservicetechnique").substring(0, 20)+"...");
		else
		 cellrepresentantsservicestechniques.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.representantservicetechnique"));
	   
		if(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.evaluationoffres.lecturesoffresfinancieres").length()>20)
			celloffresfinancieres.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.evaluationoffres.lecturesoffresfinancieres").substring(0, 20)+"...");
		else
			celloffresfinancieres.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.evaluationoffres.lecturesoffresfinancieres"));
		
		if(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.ouvertureoffrefinancieres").length()>20)
			cellouvertureoffrefinancieres.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.ouvertureoffrefinancieres").substring(0, 20)+"...");
		else
			cellouvertureoffrefinancieres.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.ouvertureoffrefinancieres"));
	
		if(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire").length()>20)
			cellouvertureevaluationoffresfin.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire").substring(0, 20)+"...");
		else
			cellouvertureevaluationoffresfin.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire"));
		
		cellobservateursindependants.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.obserindependant").substring(0, 15)+"...");
		cellverificationconformite.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.evaluation.verificationconformite").substring(0, 15)+"...");
		cellpubattriprovisoire.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire").substring(0, 20)+"...");
		
		celllistepresencemembrescommissions.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.presencecommissionsmarche").substring(0, 20)+"...");
		celllistepresencemembrescommissionsdp.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.presencecommissionsmarche").substring(0, 15)+"...");
		
		//////////////////////////
		if(dossiermi!=null)
		{
			cellmanifestationdinteret.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			celltraitementdossier.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			cellpreparation.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			cellmanifestationdinteret.setImage("/images/puce.png");
			celltraitementdossier.setImage("/images/puce.png");
			cellpreparation.setImage("/images/puce.png");
			criteres = BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(0,-1,dossiermi, null, null);
			retraits = BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).find(0,-1,dossiermi);
			plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,autorite,dossiermi, 1);
			documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossiermi, appel,UIConstants.PARAM_TYPEDOCUMENTS_NOTIFICATIONCANDIDAT,null, null);
			
			if(criteres.size()>0)
			{
				nombreprep=nombreprep+1;
				celltermereference.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				celltermereference.setImage("/images/rdo_on.png");
			}
			if(dossiermi.getDosDateMiseValidation()!=null)
			{
				nombreprep=nombreprep+1;
				cellmiseenvalidation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				if(dossiermi.getDosDateValidation()==null)
				{
                  if(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.enattentevalidation").length()>20)
                     cellmiseenvalidation.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.enattentevalidation").substring(0, 20)+"...");
             	  else
					 cellmiseenvalidation.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.enattentevalidation"));
                     cellmiseenvalidation.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.enattentevalidation"));
				}
				else
				{
					if(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validationdossier").length()>20)
	                     cellmiseenvalidation.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validationdossier").substring(0, 20)+"...");
	             	  else
						 cellmiseenvalidation.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validationdossier"));
	                     cellmiseenvalidation.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validationdossier")+": "+UtilVue.getInstance().formateLaDate(dossiermi.getDosDateValidation()));
				}
					
				cellmiseenvalidation.setImage("/images/rdo_on.png");
			}
			if(dossiermi.getDosDatePublication()!=null)
			{
				nombreprep=nombreprep+1;
				cellpublication.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");	
				cellpublication.setImage("/images/rdo_on.png");
				cellpublication.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.publication.publie")+" "+UtilVue.getInstance().formateLaDate2(dossiermi.getDosDatePublication()));

			}
			if(retraits.size()>0)
			{
				nombreprep=nombreprep+1;
				celldepotcandidatures.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				celldepotcandidatures.setImage("/images/rdo_on.png");
			}
			if(nombreprep==4)
			{
				cellpreparation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellpreparation.setImage("/images/rdo_on.png");
			}
				
			if(plis.size()>0)
			{
				nombreeval=nombreeval+1;
				cellpreselection.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellpreselection.setImage("/images/rdo_on.png");
				cellevaluation.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellevaluation.setImage("/images/puce.png");
			}
			if(plis.size()>0&&documents.size()>0)
			{
				if(plis.size()==documents.size())
				{
					nombreeval=nombreeval+1;
					cellnotificationauxcandidatsretenus.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellevaluation.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
					cellnotificationauxcandidatsretenus.setImage("/images/rdo_on.png");
					cellevaluation.setImage("/images/puce.png");
					cellnotificationauxcandidatsretenus.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.notificationmarche.notifiele")+" "+UtilVue.getInstance().formateLaDate2(dossiermi.getDosDateNotification()));
					//cellnotificationauxcandidatsretenus.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.notificationmarche.notifiele"));

				}
			}
			
			if(nombreprep+nombreeval==6)
			{
				cellevaluation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				celltraitementdossier.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellmanifestationdinteret.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellevaluation.setImage("/images/rdo_on.png");
				celltraitementdossier.setImage("/images/rdo_on.png");
				cellmanifestationdinteret.setImage("/images/rdo_on.png");
			}
		
		}
		if(dossierdp!=null)
		{
			celldemandeproposition.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			celldao.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			celldao.setImage("/images/puce.png");
			cellpreparationdp.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			celltermereferencedp.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			celldemandeproposition.setImage("/images/puce.png");
			cellpreparationdp.setImage("/images/puce.png");
			celltermereferencedp.setImage("/images/puce.png");
			devises = BeanLocator.defaultLookup(DeviseSession.class).find(0,-1,dossierdp);
			piecesadministrativesdp= BeanLocator.defaultLookup(DossiersPiecesSession.class).find(0,-1,dossierdp);
			criteresdp = BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(0,-1,dossierdp, null, null);
			plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossierdp,null,null,null,-1,-1,-1, -1, -1,-1, null, -1, null, null);
			membrescommissions = BeanLocator.defaultLookup(DossierscommissionsmarchesSession.class).find(0,-1,dossierdp,UIConstants.PARENT,0);	
			representantsoummissionnaires = BeanLocator.defaultLookup(Presence0uvertureSession.class).find(0,-1,dossierdp,null,null,0);
			representantservicestechniques = BeanLocator.defaultLookup(RepresentantsServicesTechniquesSession.class).find(0,-1,dossierdp,-1);
			observateursindependants = BeanLocator.defaultLookup(ObservateursIndependantsSession.class).find(0,-1,dossierdp,-1);
			piecessoumissionnaires   = BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).find(0,-1,dossierdp,null,null,UIConstants.PIECEFOURNIE);
			compositioncommissiontechnique = BeanLocator.defaultLookup(DossiersEvaluateursSession.class).find(0,-1,dossierdp);
			lecturesoffres = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossierdp,null,null,null,-1,UIConstants.PARENT,-1, -1, -1,-1, null, -1, null, null);
			resultats=BeanLocator.defaultLookup(ResultatNegociationSession.class).getResultat(dossierdp, autorite);
			contrat=BeanLocator.defaultLookup(ContratsSession.class).getContrat(dossierdp,autorite, null,null,null);
			if(devises.size()>0)
				nombreprepdp=nombreprepdp+1;
			if(piecesadministrativesdp.size()>0)
				nombreprepdp=nombreprepdp+1;
			if(criteresdp.size()>0)
				nombreprepdp=nombreprepdp+1;
			if(nombreprepdp==3)
			{
				celltermereferencedp.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				celltermereferencedp.setImage("/images/rdo_on.png");
			}
				
			if(dossierdp.getDosDateMiseValidation()!=null)
				{
				  nombreprepdp=nombreprepdp+1;
				  cellmiseenvalidationdp.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");	
				  cellmiseenvalidationdp.setImage("/images/rdo_on.png");
					if(dossierdp.getDosDateValidation()==null)
					{
						cellmiseenvalidationdp.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.enattentevalidation").substring(0, 20));
						cellmiseenvalidationdp.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.enattentevalidation")+" � "
								+UtilVue.getInstance().formateLaDate2(dossierdp.getDosDateMiseValidation()));
					}
					else
						cellmiseenvalidationdp.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validationdossier")+" "+UtilVue.getInstance().formateLaDate2(dossierdp.getDosDateValidation()));
			

				}
			if(nombreprepdp==4)
			{
				cellpreparationdp.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellpreparationdp.setImage("/images/rdo_on.png");
			}
				
			
			if(plis.size()>0)
			{
				nombreeop=nombreeop+1;
				cellregistredepot.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouverturesplis.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellregistredepot.setImage("/images/rdo_on.png");
				cellouverturesplis.setImage("/images/puce.png");
			}
			if(membrescommissions.size()>0)
			{
				nombreeop=nombreeop+1;
				celllistepresencemembrescommissions.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureplisdp.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				celllistepresencemembrescommissions.setImage("/images/rdo_on.png");
				cellouvertureplisdp.setImage("/images/puce.png");
			}
			if(representantsoummissionnaires.size()>0)
			{
				nombreeop=nombreeop+1;
				cellrepresentantssoumissionnaires.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureplisdp.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellrepresentantssoumissionnaires.setImage("/images/rdo_on.png");
				cellouvertureplisdp.setImage("/images/puce.png");
			}
			if(representantservicestechniques.size()>0)
			{
				nombreeop=nombreeop+1;
				cellrepresentantsservicestechniques.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureplisdp.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellrepresentantsservicestechniques.setImage("/images/rdo_on.png");
				cellouvertureplisdp.setImage("/images/puce.png");
			}
			if(observateursindependants.size()>0)
			{
				nombreeop=nombreeop+1;
				cellobservateursindependants.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureplisdp.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellobservateursindependants.setImage("/images/rdo_on.png");
				cellouvertureplisdp.setImage("/images/puce.png");
			}
			if(piecessoumissionnaires.size()>0)
			{
				nombreeop=nombreeop+1;
				cellpiecessoumissionnaires.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureplisdp.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellpiecessoumissionnaires.setImage("/images/rdo_on.png");
				cellouvertureplisdp.setImage("/images/puce.png");
			}
			if(compositioncommissiontechnique.size()>0)
			{
				nombreeop=nombreeop+1;
				cellcompositioncommissiontechnique.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureplisdp.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellcompositioncommissiontechnique.setImage("/images/rdo_on.png");
				cellouvertureplisdp.setImage("/images/puce.png");
			}
			if(lecturesoffres.size()>0)
			{
				nombreeop=nombreeop+1;
				celllecturesoffres.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureplisdp.setStyle("color#0066FF;font-family: sans-serif; font-size: 14px");
				celllecturesoffres.setImage("/images/rdo_on.png");
				cellouvertureplisdp.setImage("/images/puce.png");
			}
			if(dossierdp.getDosIncidents()!=null)
			{
				cellincidents.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellincidents.setImage("/images/rdo_on.png");
			}
			if(appel.getApoDatepvouverturepli()!=null)
			{
				nombreeop=nombreeop+1;
				cellprocesverbalouverture.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureplisdp.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellprocesverbalouverture.setImage("/images/rdo_on.png");
				cellouvertureplisdp.setImage("/images/puce.png");
			}
			if(nombreeop==9)
			{
				cellincidents.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureplisdp.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouverturesplis.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellincidents.setImage("/images/rdo_on.png");
				cellouvertureplisdp.setImage("/images/rdo_on.png");
				cellouverturesplis.setImage("/images/rdo_on.png");
			}
			if(nombreeop+nombreprepdp==13)
			{
				celldao.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				celldao.setImage("/images/rdo_on.png");

			}
			if(resultats!=null)
			{
				nombreeap=nombreeap+1;
				cellresultatnegociation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellresultatnegociation.setImage("/images/rdo_on.png");
			}
			if(dossierdp.getDosDateAttributionProvisoire()!=null)
			{
				nombreeap=nombreeap+1;
				cellattributionprovisoire.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellattributionprovisoire.setImage("/images/rdo_on.png");
			}
			if(dossierdp.getDosDateMiseValidationattribution()!=null)
			{
				nombreeap=nombreeap+1;
				cellsoumissionpourvalidationattributionprovisoire.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellsoumissionpourvalidationattributionprovisoire.setImage("/images/rdo_on.png");
				if(dossierdp.getDosDateValidationPrequalif()==null)
					cellsoumissionpourvalidationattributionprovisoire.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.enattentevalidation"));
				else
					cellsoumissionpourvalidationattributionprovisoire.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire.validele")+" "+UtilVue.getInstance().formateLaDate2(dossierdp.getDosDateMiseValidationattribution()));
		
			}
			if(dossierdp.getDosDatePublicationProvisoire()!=null)
			{
				nombreeap=nombreeap+1;
				cellpubattriprovisoire.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellpubattriprovisoire.setImage("/images/rdo_on.png");
				cellpubattriprovisoire.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire.publiele")+" "+UtilVue.getInstance().formateLaDate2(dossierdp.getDosDatePublicationProvisoire()));
				
			}
			plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0, -1, dossierdp, null,null,null,-1,-1,-1, -1, -1,-1, null, -1, null, null);
			if(plis.size()>0)
			{
				for(int i=0;i<plis.size();i++)
				{
					if(plis.get(i).getMontantoffert()!=null)
					{
						if((plis.get(i).getMontantoffert().compareTo(new BigDecimal(0))==1))
							saisioffrefinanciere=saisioffrefinanciere+1;
					}
					
				}
				if(saisioffrefinanciere>0)
				{
					nombreoof=nombreoof+1;
					celloffresfinancieres.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellouvertureoffrefinancieres.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
					celloffresfinancieres.setImage("/images/rdo_on.png");
					cellouvertureoffrefinancieres.setImage("/images/puce.png");
				}
				
			}
			plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0, -1, dossierdp, null,null,null,UIConstants.PARENT,-1,-1, -1, -1,-1, null, -1, null, null);
			if(plis.size()>0)
			{
				saisioffrefinanciere=0;
				for(int i=0;i<plis.size();i++)
				{
					if(plis.get(i).getMontantoffert()!=null)
					{
						if((plis.get(i).getMontantoffert().compareTo(new BigDecimal(0))==1))
							saisioffrefinanciere=saisioffrefinanciere+1;
					}
					
				}
				if(saisioffrefinanciere>0)
				{
					nombreoof=nombreoof+1;
					cellsaisiprixevalues.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellouvertureoffrefinancieres.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
					cellsaisiprixevalues.setImage("/images/rdo_on.png");
					cellouvertureoffrefinancieres.setImage("/images/puce.png");
				}
				
			}
			representantsoummissionnaires = BeanLocator.defaultLookup(Presence0uvertureSession.class).find(0,-1,dossierdp,null,null,1);
			if(representantsoummissionnaires.size()>0)
			{
				nombreoof=nombreoof+1;
				cellrepresentantssoumissionnairesdp.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureoffrefinancieres.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellrepresentantssoumissionnairesdp.setImage("/images/rdo_on.png");
				cellouvertureoffrefinancieres.setImage("/images/puce.png");
			}
			membrescommissions = BeanLocator.defaultLookup(DossierscommissionsmarchesSession.class).find(0,-1,dossierdp,UIConstants.PARENT,1);
			if(membrescommissions.size()>0)
			{
				nombreoof=nombreoof+1;
				celllistepresencemembrescommissionsdp.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureoffrefinancieres.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellouvertureoffrefinancieres.setImage("/images/puce.png");
				celllistepresencemembrescommissionsdp.setImage("/images/rdo_on.png");
			}
			if(nombreoof==4)
			{
				cellouvertureoffrefinancieres.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellclassementfinanciere.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureoffrefinancieres.setImage("/images/rdo_on.png");
				cellclassementfinanciere.setImage("/images/rdo_on.png");
			}
			if(dossierdp.getDateRemiseDossierTechnique()!=null||dossierdp.getDateLimiteDossierTechnique()!=null)
			{
				nombreev=nombreev+1;
				celltransmissiondossier.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellevaluationoffre.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				celltransmissiondossier.setImage("/images/rdo_on.png");
				cellevaluationoffre.setImage("/images/puce.png");
			}
			if(plis.size()>0)
			{
				nombreev=nombreev+1;
				cellverificationconformite.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellevaluationoffre.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellevaluationoffre.setImage("/images/puce.png");
				cellverificationconformite.setImage("/images/rdo_on.png");
			}
			notes=BeanLocator.defaultLookup(DossiersSouscriteresSession.class).ListesNotes(0, -1, dossierdp,null, null, null);
			if(notes.size()>0)
			{
				nombreev=nombreev+1;
				cellnotestechniques.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellevaluationoffre.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellnotestechniques.setImage("/images/rdo_on.png");
				cellevaluationoffre.setImage("/images/puce.png");
			}
			
			documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossierdp, appel,UIConstants.PARAM_TYPEDOCUMENTS_EVALUATION,null, null);
			if(documents.size()>0)
			{
				nombreev=nombreev+1;
				cellrapportevaluation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellevaluationoffre.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellevaluationoffre.setImage("/images/puce.png");
				cellrapportevaluation.setImage("/images/rdo_on.png");
			}
			if(nombreev==4)
			{
				cellevaluationoffre.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				//cellclassement.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellevaluationoffre.setImage("/images/rdo_on.png");
				//cellclassement.setImage("/images/rdo_on.png");
			}
			if(nombreev>0||nombreoof>0||nombreeap>0)
			{
				cellouvertureevaluationoffresfin.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellouvertureevaluationoffresfin.setImage("/images/puce.png");
			}
			if(nombreev+nombreoof+nombreeap==12)
			{
				cellouvertureevaluationoffresfin.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellclassementfinal.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouvertureevaluationoffresfin.setImage("/images/rdo_on.png");
				cellclassementfinal.setImage("/images/rdo_on.png");
			}
				
			/////////////Approbation/////////:::
			if(dossierdp.getDosDateSignature()!=null)
			{
				nombreapprobation=nombreapprobation+1;
				cellsignaturemarche.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellapprobation.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellsignaturemarche.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.souscritle")+" "+UtilVue.getInstance().formateLaDate2(dossierdp.getDosDateSignature()));
				cellsignaturemarche.setImage("/images/rdo_on.png");
				cellapprobation.setImage("/images/puce.png");
			}	
			if(dossierdp.getDosDateApprobation()!=null)
			{
				nombreapprobation=nombreapprobation+1;
				cellapprobationprocedure.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellapprobation.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellapprobationprocedure.setImage("/images/rdo_on.png");
				cellapprobation.setImage("/images/puce.png");
				cellapprobationprocedure.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.approbationmarche.approuvele")+" "+UtilVue.getInstance().formateLaDate2(dossierdp.getDosDateApprobation()));
				
			}	
			if(dossierdp.getDosDateNotification()!=null)
			{
				nombreapprobation=nombreapprobation+1;
				cellnotificationmarche.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellapprobation.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellnotificationmarche.setImage("/images/rdo_on.png");
				cellapprobation.setImage("/images/puce.png");
				cellnotificationmarche.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.notificationmarche.notifiele")+" "+UtilVue.getInstance().formateLaDate2(dossierdp.getDosDateNotification()));
				
			}	
			if(dossierdp.getDosDatePublicationDefinitive()!=null)
			{
				nombreapprobation=nombreapprobation+1;
				cellpubattribdefinitive.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellapprobation.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellpubattribdefinitive.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.pubattribdefinitive.publiele")+" "+UtilVue.getInstance().formateLaDate2(dossierdp.getDosDatePublicationDefinitive()));
				cellpubattribdefinitive.setImage("/images/rdo_on.png");
				cellapprobation.setImage("/images/puce.png");
			}
			if(nombreapprobation==4)
			{
				cellapprobation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellapprobation.setImage("/images/rdo_on.png");
			}
				
			if(contrat!=null)
			{
				
				
				if(contrat.getDatedemandeimmatriculation()!=null)
				{
					cellimmatriculation.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
					cellimmatriculation.setImage("/images/puce.png");
					cellimmatriculation.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.demande").substring(0, 15)+"...");
					cellimmatriculation.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.demande")+" "+UtilVue.getInstance().formateLaDate2(contrat.getDatedemandeimmatriculation()));

				}
				if(contrat.getDateimmatriculation()!=null)
				{
					cellimmatriculation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					nombreimmatriculation=nombreimmatriculation+1;
					cellimmatriculation.setImage("/images/rdo_on.png");
					cellimmatriculation.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.marche").substring(0, 15)+"...");
					cellimmatriculation.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.marche")+" "+UtilVue.getInstance().formateLaDate2(contrat.getDateimmatriculation()));

				}
			}
			
			if(nombreapprobation+nombreev+nombreoof+nombreeap+nombreeop+nombreprepdp+nombreimmatriculation==30)
			{
				celldemandeproposition.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				celldemandeproposition.setImage("/images/rdo_on.png");
			}
		}
		if(LibelleTab!=null)
		{
			if(LibelleTab.equals("traitementsdossiers"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/traitementsdossiers.zul");	
				//tree.setSelectedItem(treemanifestationdinteret);
				treemanifestationdinteret.setOpen(false);
				treedemandeproposition.setOpen(false);
			}

			if(LibelleTab.equals("manifestationdinteret"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/traitementsdossiersmi.zul");	
				tree.setSelectedItem(treetraitementdossier);
				TreeManifestationdinteret();
			}
			if((LibelleTab.equals("termereference"))||(LibelleTab.equals("infogenerales"))||(LibelleTab.equals("criteresqualifications")))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/dossierstermesreference.zul");	
				tree.setSelectedItem(treetermereference);
				TreeManifestationdinteret();
			}
		
			if(LibelleTab.equals("validationdossier"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/formsoumissionvalidationmi.zul");	
				tree.setSelectedItem(treemiseenvalidation);
				TreePreparation();
			}
			if(LibelleTab.equals("publicationdossiermi"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/publicationdossiermi.zul");	
				tree.setSelectedItem(treepublication);
				TreePreparation();
			}
			if(LibelleTab.equals("depotcandidature"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/registredepot.zul");	
				tree.setSelectedItem(treedepotcandidatures);
				TreePreparation();
			}
			if(LibelleTab.equals("preselection"))
			{
				Executions.getCurrent().setAttribute("div", Executions.getCurrent().getAttribute("div"));
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/formpreselection.zul");	
				tree.setSelectedItem(treepreselection);
				TreeEvaluation();
			}
			if(LibelleTab.equals("notificationauxcandidatsretenus"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/formnotificationmi.zul");	
				tree.setSelectedItem(treenotificationauxcandidatsretenus);
				TreeEvaluation();
			}
			if(LibelleTab.equals("recours"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/recours.zul");	
				 tree.setSelectedItem(treerecours); 
				 treemanifestationdinteret.setOpen(false);
				treedemandeproposition.setOpen(false);
			}
			if(LibelleTab.equals("documents"))
			{
				 idinclude.setSrc("/passationsmarches/prestationintellectuelle/documents.zul");	
				 tree.setSelectedItem(treedocuments); 
				 treemanifestationdinteret.setOpen(false);
				 treedemandeproposition.setOpen(false);
			}
			if(LibelleTab.equals("demandeproposition"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/traitementsdossiersdp.zul");
				tree.setSelectedItem(treedemandeproposition); 
				TreeDemandeproposition();
			}
			if((LibelleTab.equals("infogeneralesdp"))||(LibelleTab.equals("criteresqualificationsdp"))||(LibelleTab.equals("devise"))||(LibelleTab.equals("piecesadministratives"))
					||(LibelleTab.equals("presentationsoffres")))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/demandeproposition.zul");
				tree.setSelectedItem(treetermereferencedp); 
				TreePreparationdp();
			}
			if(LibelleTab.equals("validationdossierdp"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/formsoumissionvalidationdp.zul");	
				tree.setSelectedItem(treemiseenvalidationdp);
				TreePreparationdp();
			}
			if(LibelleTab.equals("plisouvertures"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/registredepotdp.zul");	
				tree.setSelectedItem(treeregistredepot);
				TreeOuvertureplis();
			}
			if(LibelleTab.equals("listepresencemembrescommissions"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/listepresencemembrescommissions.zul");	
				tree.setSelectedItem(treelistepresencemembrescommissions);
				TreeOuvertureplis();
			}
			if(LibelleTab.equals("representantssoumissionnaires"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/representantssoumissionnaires.zul");
				tree.setSelectedItem(treerepresentantssoumissionnaires);
				TreeOuvertureplis();
				
			}
			if(LibelleTab.equals("representantsservicestechniques"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/representantsservicestechniques.zul");
				tree.setSelectedItem(treerepresentantsservicestechniques);
				TreeOuvertureplis();
				
			}
			if(LibelleTab.equals("observateursindependants"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/observateursindependants.zul");
				tree.setSelectedItem(treeobservateursindependants);
				TreeOuvertureplis();
			}
		
			if(LibelleTab.equals("piecessoumissionnaires"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/piecessoumissionnaires.zul");
				tree.setSelectedItem(treepiecessoumissionnaires);
				TreeOuvertureplis();
			}
			if(LibelleTab.equals("compositioncommissiontechnique"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/compositioncommissiontechnique.zul");
				tree.setSelectedItem(treecompositioncommissiontechnique);
				TreeOuvertureplis();
			}
			if(LibelleTab.equals("lecturesoffres"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/formlecturesoffres.zul");
				tree.setSelectedItem(treelecturesoffres);
				TreeOuvertureplis();
			}
			if(LibelleTab.equals("procesverbalouverture"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/procesverbalouverture.zul");
				tree.setSelectedItem(treeprocesverbalouverture);
				TreeOuvertureplis();
			}
			if(LibelleTab.equals("incidents"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/incidents.zul");
				tree.setSelectedItem(treeincidents);
				TreeOuvertureplis();
			}
			if(LibelleTab.equals("ouvertureplis"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/ouvertureplis.zul");
				tree.setSelectedItem(treeouvertureplisdp);
				TreeOuvertureplis();
			}
			if(LibelleTab.equals("formtransmissiondossier"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/formtransmissiondossier.zul");
				tree.setSelectedItem(treetransmissiondossier);
				TreeEvaluationDP();
				treeevaluationoffre.setOpen(true);
			}
			if(LibelleTab.equals("formverificationconformite"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/formverificationconformite.zul");
				tree.setSelectedItem(treeverificationconformite);
				TreeEvaluationDP();
				treeevaluationoffre.setOpen(true);
			}
			if(LibelleTab.equals("rapportevaluation"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/rapportevaluation.zul");
				tree.setSelectedItem(treerapportevaluation);
				TreeEvaluationDP();
				treeevaluationoffre.setOpen(true);
				treeouvertureoffrefinancieres.setOpen(false);
			}
			if(LibelleTab.equals("saisienotetechnique"))
			{
				Executions.getCurrent().setAttribute("div", Executions.getCurrent().getAttribute("div"));
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/formsaisienotetechnique.zul");
				tree.setSelectedItem(treenotestechniques);
				TreeEvaluationDP();
				treeevaluationoffre.setOpen(true);
				treeouvertureoffrefinancieres.setOpen(false);
			}
//			if(LibelleTab.equals("classement"))
//			{
//				idinclude.setSrc("/passationsmarches/prestationintellectuelle/formclassement.zul");
//				tree.setSelectedItem(treeclassement);
//				TreeEvaluationDP();
//				treeevaluationoffre.setOpen(true);
//			}
			if(LibelleTab.equals("listepresencemembrescommissionsdp"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/listepresencemembrescommissionsdp.zul");	
				tree.setSelectedItem(treelistepresencemembrescommissionsdp);
				TreeEvaluationDP();
			}
			if(LibelleTab.equals("representantssoumissionnairesdp"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/representantssoumissionnairesdp.zul");	
				tree.setSelectedItem(treerepresentantssoumissionnairesdp);
				TreeEvaluationDP();
			}
			if(LibelleTab.equals("offresfinancieres"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/offresfinancieres.zul");	
				tree.setSelectedItem(treeoffresfinancieres);
				TreeEvaluationDP();
			}
			if(LibelleTab.equals("saisiprixevalues"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/formcorrectionoffre.zul");	
				tree.setSelectedItem(treesaisiprixevalues);
				TreeEvaluationDP();
			}
			if(LibelleTab.equals("resultatnegociation"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/resultatnegociation.zul");	
				tree.setSelectedItem(treeresultatnegociation);
				TreeEvaluationDP();
				 treeouvertureoffrefinancieres.setOpen(false);
			}
			if(LibelleTab.equals("publicationattributionprovisoire"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/formpublicationattributionprovisoire.zul");	
				tree.setSelectedItem(treepubattriprovisoire);
				TreeEvaluationDP();
				 treeouvertureoffrefinancieres.setOpen(false);
			}
			if(LibelleTab.equals("attributionprovisoire"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/attributionprovisoire.zul");	
				tree.setSelectedItem(treeattributionprovisoire);
				TreeEvaluationDP();
				 treeouvertureoffrefinancieres.setOpen(false);
			}
			if(LibelleTab.equals("soumissionpourvalidationattributionprovisoire"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/soumissionpourvalidationattributionprovisoire.zul");	
				tree.setSelectedItem(treesoumissionpourvalidationattributionprovisoire);
				TreeEvaluationDP();
				 treeouvertureoffrefinancieres.setOpen(false);
			}
			if(LibelleTab.equals("souscriptiondumarche"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/souscriptiondumarche.zul");	
				tree.setSelectedItem(treesignaturemarche);
				TreeApprobationDP();
			}
			if(LibelleTab.equals("approbationmarche"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/formapprobationmarche.zul");	
				tree.setSelectedItem(treeapprobationprocedure);
				TreeApprobationDP();
			}
			if(LibelleTab.equals("notificationmarche"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/formnotificationmarche.zul");	
				tree.setSelectedItem(treenotificationmarche);
				TreeApprobationDP();
			}
			if(LibelleTab.equals("publicationattributiondefinitive"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/formpublicationattributiondefinitive.zul");	
				tree.setSelectedItem(treepubattribdefinitive);
				TreeApprobationDP();
			}
			if(LibelleTab.equals("evaluationattribution"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/evaluationoffres.zul");	
				tree.setSelectedItem(treeevaluationoffre);
				TreeEvaluationDP();
				treeevaluationoffre.setOpen(true);
				treeouvertureoffrefinancieres.setOpen(false);
			}
			if(LibelleTab.equals("ouvertureoffrefinancieres"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/ouverturesoffresfinancieres.zul");
				tree.setSelectedItem(treeouvertureoffrefinancieres);
				TreeEvaluationDP();
				treeevaluationoffre.setOpen(false);
				treeouvertureoffrefinancieres.setOpen(true);
			}
			if(LibelleTab.equals("immatriculation"))
			{
				idinclude.setSrc("/passationsmarches/prestationintellectuelle/formdemandeimmatriculation.zul");
				tree.setSelectedItem(itemimmatriculation);
				TreeImmatriculation();
			}
		
		}
		else
		{
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/traitementsdossiers.zul");	
			 tree.setSelectedItem(treemanifestationdinteret); 
			 
		}
		
		
	//	tree.setSelectedItem(itemdossiers);     
		
	
	}


	
	

	public void onSelect$tree() throws Exception
	{
		InfosTree();
	}
	
	public void TreeManifestationdinteret()
	{
		treemanifestationdinteret.setOpen(true);
		treedemandeproposition.setOpen(false);
	}
	public void TreeDemandeproposition()
	{
		treemanifestationdinteret.setOpen(false);
		treedemandeproposition.setOpen(true);
		treepreparationdp.setOpen(false);
		treeouvertureplis.setOpen(false);
		itemevaluationattributionprovisoire.setOpen(false);
		itemapprobation.setOpen(false);
	}

	public void TreePreparation()
	{
		treepreparation.setOpen(true);
		treetermereference.setOpen(false);
		treeevaluation.setOpen(false);
		treedemandeproposition.setOpen(false);
		
	}
	public void TreeTermereference()
	{
		treedemandeproposition.setOpen(false);
		treetermereference.setOpen(true);
		treeevaluation.setOpen(false);
		
	}
	public void TreeEvaluation()
	{
		treepreparation.setOpen(false);
		treetermereference.setOpen(false);
		treeevaluation.setOpen(true);
		treedemandeproposition.setOpen(false);
		
	}
	public void TreePreparationdp()
	{
		treemanifestationdinteret.setOpen(false);
		treepreparationdp.setOpen(true);
		treeouvertureplis.setOpen(false);
		itemevaluationattributionprovisoire.setOpen(false);
		itemapprobation.setOpen(false);
		
	}
	public void TreeOuvertureplis()
	{
		treemanifestationdinteret.setOpen(false);
		treepreparationdp.setOpen(false);
		treeouvertureplis.setOpen(true);
		itemevaluationattributionprovisoire.setOpen(false);
		itemapprobation.setOpen(false);
		
	}
	
	public void TreeEvaluationDP()
	{
		treemanifestationdinteret.setOpen(false);
		treepreparationdp.setOpen(false);
		treeouvertureplis.setOpen(false);
		itemevaluationattributionprovisoire.setOpen(true);
		itemapprobation.setOpen(false);
		
	}
	public void TreeApprobationDP()
	{
		treemanifestationdinteret.setOpen(false);
		treepreparationdp.setOpen(false);
		treeouvertureplis.setOpen(false);
		itemevaluationattributionprovisoire.setOpen(false);
		itemapprobation.setOpen(true);
		
	}
	public void InfosTree() throws Exception{
		
		/////////////Preparation du dossier//////////
		if(tree.getSelectedItem().getId().equals("treemanifestationdinteret"))
		{
			TreeManifestationdinteret();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/traitementsdossiersmi.zul");
		}
		if(tree.getSelectedItem().getId().equals("treetraitementdossier"))
		{
			TreeManifestationdinteret();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/traitementsdossiersmi.zul");	
		}
		if(tree.getSelectedItem().getId().equals("treepreparation"))
		{
			TreePreparation();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/dossierspreparation.zul");	
	    }
		if(tree.getSelectedItem().getId().equals("treetermereference"))
		{
			TreePreparation();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/dossierstermesreference.zul");	
	    }
		if(tree.getSelectedItem().getId().equals("treeinfosgenerales"))
		{
			TreeTermereference();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/dossierstermesreference.zul");	
	    }
		if(tree.getSelectedItem().getId().equals("treecriteresqualifications"))
		{
			TreeTermereference();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/criteresqualifications.zul");	
	    }
		if(tree.getSelectedItem().getId().equals("treemiseenvalidation"))
		{
			TreePreparation();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/formsoumissionvalidationmi.zul");	
	    }
		if(tree.getSelectedItem().getId().equals("treepublication"))
		{
			
			if(dossiermi==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
			}
			else
			{
				if(avalider.equals("oui"))
				{
					if(dossiermi.getDosDateValidation()==null)
						 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validerdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						else
							idinclude.setSrc("/passationsmarches/prestationintellectuelle/publicationdossiermi.zul");	
				}
				else
				{
					idinclude.setSrc("/passationsmarches/prestationintellectuelle/publicationdossiermi.zul");	
					TreePreparation();
				}
			
			}
			
	    }
		
		if(tree.getSelectedItem().getId().equals("treedepotcandidatures"))
		{
			if(dossiermi==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
			}
			else
			{
				if(dossiermi.getDosDatePublication()==null)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.publierdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				}
				else
				{
					TreePreparation();
					idinclude.setSrc("/passationsmarches/prestationintellectuelle/registredepot.zul");	
				}
			}
			
			
	    }
		if(tree.getSelectedItem().getId().equals("treepreselection"))
		{
			if(dossiermi==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
			}
			else
			{
				plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,autorite,dossiermi, 0);
				if(plis.size()==0)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.saisirregistredepot"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				}
				else
				{
					TreeEvaluation();
					idinclude.setSrc("/passationsmarches/prestationintellectuelle/formpreselection.zul");	
				}
					
				
			}
			
	    }
		
		
		if(tree.getSelectedItem().getId().equals("treenotificationauxcandidatsretenus"))
		{
			TreeEvaluation();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/formnotificationmi.zul");	
	    }
		if(tree.getSelectedItem().getId().equals("treerecours"))
		{
			treemanifestationdinteret.setOpen(false);
			treedemandeproposition.setOpen(false);
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/recours.zul");	
	    }
		if(tree.getSelectedItem().getId().equals("treedocuments"))
		{
			treemanifestationdinteret.setOpen(false);
			treedemandeproposition.setOpen(false);
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/documents.zul");	
	    }
		if(tree.getSelectedItem().getId().equals("treedemandeproposition"))
		{
			TreeDemandeproposition();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/traitementsdossiersdp.zul");
		}
		if(tree.getSelectedItem().getId().equals("treetermereferencedp"))
		{
			TreePreparationdp();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/demandeproposition.zul");
		}
		if(tree.getSelectedItem().getId().equals("treemiseenvalidationdp"))
		{
			if(dossierdp==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
			}
			else
			{
				if(devises.size()==0)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirdevises"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				}
				else
				{
					if(piecesadministrativesdp.size()==0)
					{
						 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirpieces"), "Erreur", Messagebox.OK, Messagebox.ERROR);
							
					}
					else
					{
						if(criteresdp.size()==0)
						{
							 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisircriteres"), "Erreur", Messagebox.OK, Messagebox.ERROR);
								
						}
						else
						{
							TreePreparationdp();
							idinclude.setSrc("/passationsmarches/prestationintellectuelle/formsoumissionvalidationdp.zul");
						}
					}
				}
				
			}
		}
		if(tree.getSelectedItem().getId().equals("treeregistredepot"))
		{
			if(dossierdp==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
			}
			else
			{
				if(dossierdp.getDosDateValidation()==null)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validerdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				}
				else
				{
					TreeOuvertureplis();
					idinclude.setSrc("/passationsmarches/prestationintellectuelle/registredepotdp.zul");
				}
			}
			
		}
		if(tree.getSelectedItem().getId().equals("treepreparationdp"))
		{
			TreePreparationdp();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/dossierspreparationdp.zul");
		}
		if(tree.getSelectedItem().getId().equals("treeouvertureplis"))
		{
			TreeOuvertureplis();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/dossiersouvertureplisdp.zul");
			treeouvertureplisdp.setOpen(false);
		}
		if(tree.getSelectedItem().getId().equals("itemevaluationattributionprovisoire"))
		{
			TreeEvaluationDP();
			treeevaluationoffre.setOpen(false);
			treeouvertureoffrefinancieres.setOpen(false);
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/dossiersevaluationsdp.zul");
		}
		if(tree.getSelectedItem().getId().equals("itemapprobation"))
		{
			TreeApprobationDP();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/dossiersapprobationdp.zul");
		}
		if(tree.getSelectedItem().getId().equals("treeevaluation"))
		{
			TreeEvaluation();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/dossiersevaluation.zul");
		}
		if(tree.getSelectedItem().getId().equals("treeouvertureplisdp"))
		{
			TreeOuvertureplis();
			treeouvertureplisdp.setOpen(true);
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/ouvertureplis.zul");
		}
		if(tree.getSelectedItem().getId().equals("treelistepresencemembrescommissions"))
		{
			TreeOuvertureplis();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/listepresencemembrescommissions.zul");
		}
		if(tree.getSelectedItem().getId().equals("treerepresentantssoumissionnaires"))
		{
			TreeOuvertureplis();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/representantssoumissionnaires.zul");
		}
		if(tree.getSelectedItem().getId().equals("treerepresentantsservicestechniques"))
		{
			TreeOuvertureplis();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/representantsservicestechniques.zul");
		}
		if(tree.getSelectedItem().getId().equals("treeobservateursindependants"))
		{
			TreeOuvertureplis();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/observateursindependants.zul");
		}
		if(tree.getSelectedItem().getId().equals("treegarantiesoumission"))
		{
			TreeOuvertureplis();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/garantiesoumission.zul");
		}
		if(tree.getSelectedItem().getId().equals("treepiecessoumissionnaires"))
		{
			TreeOuvertureplis();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/piecessoumissionnaires.zul");
		}
		if(tree.getSelectedItem().getId().equals("treeincidents"))
		{
			TreeOuvertureplis();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/incidents.zul");
		}
		if(tree.getSelectedItem().getId().equals("treecompositioncommissiontechnique"))
		{
			TreeOuvertureplis();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/compositioncommissiontechnique.zul");
		}
		if(tree.getSelectedItem().getId().equals("treelecturesoffres"))
		{
			TreeOuvertureplis();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/formlecturesoffres.zul");
		}
		if(tree.getSelectedItem().getId().equals("treeprocesverbalouverture"))
		{
			TreeOuvertureplis();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/procesverbalouverture.zul");
		}
		if(tree.getSelectedItem().getId().equals("treeevaluationoffre"))
		{
			TreeEvaluationDP();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/evaluationoffres.zul");
			treeevaluationoffre.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treetransmissiondossier"))
		{
			TreeEvaluationDP();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/formtransmissiondossier.zul");
		}
		if(tree.getSelectedItem().getId().equals("treeverificationconformite"))
		{
			TreeEvaluationDP();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/formverificationconformite.zul");
		}
		if(tree.getSelectedItem().getId().equals("treerapportevaluation"))
		{
			TreeEvaluationDP();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/rapportevaluation.zul");
		}
		if(tree.getSelectedItem().getId().equals("treenotestechniques"))
		{
			TreeEvaluationDP();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/formsaisienotetechnique.zul");
		}
		if(tree.getSelectedItem().getId().equals("treeclassement"))
		{
			TreeEvaluationDP();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/formclassement.zul");
		}
		if(tree.getSelectedItem().getId().equals("treeouvertureoffrefinancieres"))
		{
			TreeEvaluationDP();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/ouverturesoffresfinancieres.zul");
			 treeouvertureoffrefinancieres.setOpen(true);
			 treeevaluationoffre.setOpen(false);
		}
		if(tree.getSelectedItem().getId().equals("treelistepresencemembrescommissionsdp"))
		{
			TreeEvaluationDP();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/listepresencemembrescommissionsdp.zul");
			 treeouvertureoffrefinancieres.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treerepresentantssoumissionnairesdp"))
		{
			TreeEvaluationDP();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/representantssoumissionnairesdp.zul");
			 treeouvertureoffrefinancieres.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treeoffresfinancieres"))
		{
			TreeEvaluationDP();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/offresfinancieres.zul");
			 treeouvertureoffrefinancieres.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treesaisiprixevalues"))
		{
			TreeEvaluationDP();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/formcorrectionoffre.zul");
			treeouvertureoffrefinancieres.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treeclassementfinanciere"))
		{
			TreeEvaluationDP();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/formclassementfinanciere.zul");
			treeouvertureoffrefinancieres.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treeresultatnegociation"))
		{
			TreeEvaluationDP();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/resultatnegociation.zul");
			 treeouvertureoffrefinancieres.setOpen(false);
		}
		if(tree.getSelectedItem().getId().equals("treeclassementfinal"))
		{
			TreeEvaluationDP();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/formclassementfinal.zul");
			 treeouvertureoffrefinancieres.setOpen(false);
		}
		if(tree.getSelectedItem().getId().equals("treeattributionprovisoire"))
		{
			TreeEvaluationDP();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/attributionprovisoire.zul");
			 treeouvertureoffrefinancieres.setOpen(false);
		}
		if(tree.getSelectedItem().getId().equals("treesoumissionpourvalidationattributionprovisoire"))
		{
			TreeEvaluationDP();
			idinclude.setSrc("/passationsmarches/prestationintellectuelle/soumissionpourvalidationattributionprovisoire.zul");
			 treeouvertureoffrefinancieres.setOpen(false);
		}
		if(tree.getSelectedItem().getId().equals("treepubattriprovisoire"))
		{
			if(dossierdp==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
			}
			else
			{
				if(dossierdp.getDosDateValidationPrequalif()==null)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validerdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				}
				else
				{
					TreeEvaluationDP();
					idinclude.setSrc("/passationsmarches/prestationintellectuelle/formpublicationattributionprovisoire.zul");
					 treeouvertureoffrefinancieres.setOpen(false);
				}
			}
			
		}
		if(tree.getSelectedItem().getId().equals("treesignaturemarche"))
		{
			if(dossierdp==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				 if(dossierdp.getDosDatePublicationProvisoire()==null)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.publierattrribution"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
			        TreeApprobationDP();
			        idinclude.setSrc("/passationsmarches/prestationintellectuelle/souscriptiondumarche.zul");
				}
			}
		}
		if(tree.getSelectedItem().getId().equals("treeapprobationprocedure"))
		{
			if(dossierdp==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				 if(dossierdp.getDosDateSignature()==null)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.souscriredossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
					 TreeApprobationDP();
					 idinclude.setSrc("/passationsmarches/prestationintellectuelle/formapprobationmarche.zul");
				 }
			}
			
		}
		if(tree.getSelectedItem().getId().equals("treenotificationmarche"))
		{
			if(dossierdp==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				 if(dossierdp.getDosDateApprobation()==null)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.approuverdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
			       TreeApprobationDP();
			       idinclude.setSrc("/passationsmarches/prestationintellectuelle/formnotificationmarche.zul");
				 }
			}
		}
		if(tree.getSelectedItem().getId().equals("treepubattribdefinitive"))
		{
			if(dossierdp==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				 if(dossierdp.getDosDateNotification()==null)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.notifierdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
			       TreeApprobationDP();
			        idinclude.setSrc("/passationsmarches/prestationintellectuelle/formpublicationattributiondefinitive.zul");
				 }
			}
		}
		if(tree.getSelectedItem().getId().equals("itemimmatriculation"))
		{
			if(dossierdp==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				 if(dossierdp.getDosDatePublicationDefinitive()==null)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.publierattrribution"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
			        TreeImmatriculation();
			       idinclude.setSrc("/passationsmarches/prestationintellectuelle/formdemandeimmatriculation.zul");
				 }
			}
		}
   }
	public void TreeImmatriculation()
	{
		treemanifestationdinteret.setOpen(false);
		treepreparationdp.setOpen(false);
		treeouvertureplis.setOpen(false);
		itemevaluationattributionprovisoire.setOpen(false);
		itemapprobation.setOpen(false);
		treedemandeproposition.setOpen(true);
		
	}
	public void onOpen$treemanifestationdinteret___() {
		tree.setSelectedItem(treemanifestationdinteret);  
		treedemandeproposition.setOpen(false);
	}
	
	public void onOpen$treedemandeproposition__() {
		tree.setSelectedItem(treedemandeproposition);  
		treemanifestationdinteret.setOpen(false);
		treepreparationdp.setOpen(false);
		treeouvertureplis.setOpen(false);
		itemevaluationattributionprovisoire.setOpen(false);
		treeouvertureoffrefinancieres.setOpen(false);
	}
}