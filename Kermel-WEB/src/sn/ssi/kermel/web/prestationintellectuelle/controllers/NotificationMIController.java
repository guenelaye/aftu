package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DocumentsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class NotificationMIController extends AbstractWindow implements
		 AfterCompose,EventListener,ListitemRenderer {

	private Listbox lstListe,lstBailleur;
	private Paging pgPagination,pgBailleur;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtobjet,txtVersionElectronique,txtReference;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference,libellebailleur=null;
    private int parent;
     List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherBailleur,txtChapitre;
    SygBailleurs bailleur=null;
    private Decimalbox dcmontantprevu;
    private Label lbStatusBar,lblCandidat;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idbailleur;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	SygDossiers dossierdp=new SygDossiers();
	private Menuitem menuValider;
	private Datebox dtdatenotification;
	private String nomFichier;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	private Image image;
	private Div step0,step1,step3;
	private Iframe idIframe;
	private String extension,images;
	List<SygPlisouvertures> plis = new ArrayList<SygPlisouvertures>();
	private SygAutoriteContractante autorite;
	SygPlisouvertures pli=new SygPlisouvertures();
	SygDocuments document=new SygDocuments();
	List<SygDocuments> documents = new ArrayList<SygDocuments>();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	   
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		autorite=appel.getAutorite();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,1);
		dossierdp=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,2);
		if((dossier==null))
		{
			menuValider.setDisabled(true);
		  
		}
		else
		{
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			
		}
	}

	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			
			 plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(pgPagination.getActivePage() * byPage,byPageBandbox,null,dossier,1);
			 lstListe.setModel(new SimpleListModel(plis));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(RegistrededepotSession.class).count(null,dossier,1));
			
		} 
	}
private boolean checkFieldConstraints() {
		
		try {
			if(txtReference.getValue().equals(""))
		     {
               errorComponent = txtReference;
                errorMsg = Labels.getLabel("kermel.referentiel.courrier.reference")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dtdatenotification.getValue()==null)
		     {
               errorComponent = dtdatenotification;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.publication.date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((dtdatenotification.getValue()).after(new Date()))
			 {
				errorComponent = dtdatenotification;
				errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.publication.dater")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.referentiel.date.dujour")+": "+UtilVue.getInstance().formateLaDate(new Date());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			if(txtobjet.getValue().equals(""))
		     {
               errorComponent = txtobjet;
               errorMsg = Labels.getLabel("kermel.referentiel.courrier.objet")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtVersionElectronique.getValue().equals(""))
		     {
              errorComponent = txtVersionElectronique;
              errorMsg = Labels.getLabel("kermel.common.fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			return true;
			
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

		public void onClick$menuValider() {
			if(checkFieldConstraints())
			{
				dossier.setDosDateNotification(dtdatenotification.getValue());
				dossier.setDosCommentNotification(txtobjet.getValue());
				BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).update(dossier);
//				  for (int i = 0; i < plis.size(); i++) {
//					  pli=plis.get(i);
//				  }
				document.setAppel(appel);
				if(!txtVersionElectronique.getValue().equals(""))
				document.setNomFichier(txtVersionElectronique.getValue());
				document.setTypeDocument(UIConstants.PARAM_TYPEDOCUMENTS_NOTIFICATIONCANDIDAT);
				document.setLibelle(Labels.getLabel("kermel.plansdepassation.proceduresmarches.documents.notificationcandidat"+": "+pli.getRetrait().getNomSoumissionnaire()+" � "+UtilVue.getInstance().formateLaDate2(dtdatenotification.getValue())));
				document.setDossier(dossier);
				document.setPlis(pli);
				document.setDate(dtdatenotification.getValue());
				document.setReference(txtReference.getValue());
				document.setObjet(txtobjet.getValue());
				document.setDate(new Date());
				document.setHeure(Calendar.getInstance().getTime());
				if(documents.size()==0)
				{
					BeanLocator.defaultLookup(DocumentsSession.class).save(document);
				}
				else
				{
					BeanLocator.defaultLookup(DocumentsSession.class).update(document);
				}
				
				pli.setNotifie("oui");
				BeanLocator.defaultLookup(RegistrededepotSession.class).update(pli);
				
				session.setAttribute("libelle", "notificationauxcandidatsretenus");
				loadApplicationState("procedure_pi");
				
			}
		}
		
	
		public void onClick$menuFermer() {
			step0.setVisible(true);
			step1.setVisible(false);
			step3.setVisible(false);
		}
		public void onClick$menuCancel() {
			step3.setVisible(true);
			step0.setVisible(false);
			step1.setVisible(false);
			dtdatenotification.setValue(null);
			txtReference.setValue("");
			txtobjet.setValue("");
			image.setVisible(false);
		}
		public void onClick$menuNotifier() {
			if (lstListe.getSelectedItem() == null)
				  throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			pli=(SygPlisouvertures) lstListe.getSelectedItem().getValue();
			lblCandidat.setValue(pli.getRetrait().getNomSoumissionnaire());
			step3.setVisible(false);
			step0.setVisible(true);
			step1.setVisible(false);
		
			documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossier, appel,UIConstants.PARAM_TYPEDOCUMENTS_NOTIFICATIONCANDIDAT,null, pli);
			if(documents.size()>0)
			{
				document=documents.get(0);
				dtdatenotification.setValue(document.getDate());
				txtReference.setValue(document.getReference());
				txtobjet.setValue(document.getObjet());
				
				extension=document.getNomFichier().substring(document.getNomFichier().length()-3,  document.getNomFichier().length());
				 if(extension.equalsIgnoreCase("pdf"))
					 images="/images/icone_pdf.png";
				 else  
					 images="/images/word.jpg";
				 
				image.setVisible(true);
				image.setSrc(images);
				nomFichier=dossier.getDosFichier();
			}
			
			
			if(dossierdp!=null)
				menuValider.setDisabled(true);
			
		}
		
				
			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
				SygPlisouvertures plis = (SygPlisouvertures) data;
				item.setValue(plis);

				Listcell cellRaisonsocial = new Listcell(plis.getRetrait().getNomSoumissionnaire());
				 cellRaisonsocial.setParent(item);
				 
					
				Listcell cellImage = new Listcell();
				if(plis.getNotifie().equals("oui"))
				{
					 cellImage.setImage("/images/rdo_on.png");
					 cellImage.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.candidatnotifie"));
				}
				else
				{
					cellImage.setImage("/images/rdo_off.png");
					cellImage.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.candidatnonnotifie"));
				}
				cellImage.setParent(item);
			}
			
			public void onClick$btnChoixFichier() {
				//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
					if (ToolKermel.isWindows())
						nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
					else
						nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

					txtVersionElectronique.setValue(nomFichier);
				}
			
}