package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygMonnaieoffre;
import sn.ssi.kermel.be.entity.SygPays;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrederetraitdaoSession;
import sn.ssi.kermel.be.referentiel.ejb.FournisseurSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;
import sn.ssi.kermel.web.referentiel.controllers.FournisseurFormController;

@SuppressWarnings("serial")
public class RegistreDepotCandidatsController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstFournisseur;
	private Paging pgPagination,pgFournisseur;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle,txtPays;
    String libellemonnaie=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
    List<SygPlisouvertures> depots = new ArrayList<SygPlisouvertures>();
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherFournisseur;
    SygBailleurs bailleur=null;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg,nomfournisseur=null;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idretrait=null;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	SygRetraitregistredao retrait=new SygRetraitregistredao();
	SygMonnaieoffre monnaie=new SygMonnaieoffre();
	private Bandbox bdFournisseur;
	private Decimalbox dcTaux;
	private Div step0,step1;
	private Menuitem menuAjouter,menuModifier,menuSupprimer;
	private Datebox dtdateretrait;
	private Textbox txtTelephone,txtFax,txtEmail,txtRechercherPays;
	private Label lblNbrplis,lblMontant,lblPartARMP;
	private BigDecimal montant=new BigDecimal(0);
	private BigDecimal montantarmp=new BigDecimal(0);
	private SygAutoriteContractante autorite;
	SygPays pays=new SygPays();
	SygFournisseur fournisseur=new SygFournisseur();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
    	lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
		addEventListener(ApplicationEvents.ON_FOURNISSEUR, this);
		pgFournisseur.setPageSize(byPageBandbox);
		pgFournisseur.addForward("onPaging", this, ApplicationEvents.ON_FOURNISSEUR);
		lstFournisseur.setItemRenderer(new FournisseursRenderer());
		Events.postEvent(ApplicationEvents.ON_FOURNISSEUR, this, null);
    
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		autorite=appel.getAutorite();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,1);
		if(dossier==null)
		{
			menuAjouter.setDisabled(true);
			menuModifier.setDisabled(true);
			menuSupprimer.setDisabled(true);
		}
		else
		{
			if(appel.getApoDatepvouverturepli()!=null)
			 {
				menuAjouter.setDisabled(true);
				menuModifier.setDisabled(true);
				menuSupprimer.setDisabled(true);
			 }
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			Events.postEvent(ApplicationEvents.ON_MONNAIES, this, null);
			
		}
		
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygRetraitregistredao> retraits = BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).find(activePage,byPageBandbox,dossier);
			 lstListe.setModel(new SimpleListModel(retraits));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).count(dossier));
			 lblNbrplis.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.totalpliretire")+": "+retraits.size());
			} 
	
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				retrait = (SygRetraitregistredao)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				depots=BeanLocator.defaultLookup(RegistrededepotSession.class).find(dossier, retrait, -1, null);
				BeanLocator.defaultLookup(RegistrededepotSession.class).delete(depots.get(0).getId());
				BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).delete(retrait.getId());
			}
			session.setAttribute("libelle", "depotcandidature");
			loadApplicationState("procedure_pi");
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_FOURNISSEUR)){
			if (event.getData() != null) {
				if ((Integer) event.getData()==-1000) {
					fournisseur=(SygFournisseur) Executions.getCurrent().getAttribute("fournisseur");
					pays=fournisseur.getPays();
					bdFournisseur.setValue(fournisseur.getNom());
					txtTelephone.setValue(fournisseur.getTel());
					txtFax.setValue(fournisseur.getFax());
					txtEmail.setValue(fournisseur.getEmail());
					txtPays.setValue(pays.getLibelle());
					byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
					activePage = pgFournisseur.getActivePage() * byPageBandbox;
					pgFournisseur.setPageSize(byPageBandbox);
				}
				else
				{
					activePage = Integer.parseInt((String) event.getData());
					byPageBandbox = -1;
					pgFournisseur.setPageSize(1000);
				}
			}
			else
			{
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgFournisseur.getActivePage() * byPageBandbox;
				pgFournisseur.setPageSize(byPageBandbox);
			}
		
			List<SygFournisseur> fournisseurs = BeanLocator.defaultLookup(FournisseurSession.class).find(activePage, byPageBandbox,nomfournisseur,null,null);
			lstFournisseur.setModel(new SimpleListModel(fournisseurs));
			pgFournisseur.setTotalSize(BeanLocator.defaultLookup(FournisseurSession.class).count(nomfournisseur,null,null));
			
		}
		
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygRetraitregistredao retraits = (SygRetraitregistredao) data;
		item.setValue(retraits);

		Listcell cellRaisonsocial = new Listcell(retraits.getNomSoumissionnaire());
		 cellRaisonsocial.setParent(item);
		 
		 Listcell cellPays = new Listcell(retraits.getPays().getLibelle());
		 cellPays.setParent(item);
		 
		 Listcell cellTelephone = new Listcell(retraits.getTelephone());
		 cellTelephone.setParent(item);
		 
		 Listcell cellFax = new Listcell(retraits.getFax());
		 cellFax.setParent(item);
		 
		 Listcell cellEmail = new Listcell(retraits.getEmail());
		 cellEmail.setParent(item);
		 
		 Listcell cellDate = new Listcell(UtilVue.getInstance().formateLaDate2(retraits.getDateRetrait()));
		 cellDate.setParent(item);
	
	}

	public void onClick$menuSupprimer()
	{
		if (lstListe.getSelectedItem() == null)
			
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		

		HashMap<String, String> display = new HashMap<String, String>(); // permet
		display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
		display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
		display.put(MessageBoxController.DSP_HEIGHT, "250px");
		display.put(MessageBoxController.DSP_WIDTH, "47%");

		HashMap<String, Object> map = new HashMap<String, Object>(); // permet
		map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
		showMessageBox(display, map);
		
	}
	public void onClick$menuModifier()
	{
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		retrait=(SygRetraitregistredao) lstListe.getSelectedItem().getValue();
		fournisseur=retrait.getFournisseur();
		pays=retrait.getPays();
		bdFournisseur.setValue(fournisseur.getNom());
		idretrait=retrait.getId();
		dtdateretrait.setValue(retrait.getDateRetrait());
	
		txtTelephone.setValue(retrait.getTelephone());
		txtFax.setValue(retrait.getFax());
		txtEmail.setValue(retrait.getEmail());
		step0.setVisible(false);
		step1.setVisible(true);
	}
	

	

	private boolean checkFieldConstraints() {
		
		try {
		
			if(dtdateretrait.getValue()==null)
		     {
               errorComponent = dtdateretrait;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registredepot.datedepot")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((dossier.getDosDateLimiteDepot()).after(dtdateretrait.getValue()))
			 {
				errorComponent = dtdateretrait;
				errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registredepot.datedepot")+" "+Labels.getLabel("kermel.referentiel.date.posterieure")+": "+UtilVue.getInstance().formateLaDate(dossier.getDosDateLimiteDepot());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			if((dtdateretrait.getValue()).after(new Date() ))
			 {
				errorComponent = dtdateretrait;
				errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registredepot.datedepot")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.referentiel.date.dujour")+": "+UtilVue.getInstance().formateLaDate(new Date());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			
			if(bdFournisseur.getValue().equals(""))
		     {
             errorComponent = bdFournisseur;
             errorMsg = Labels.getLabel("kermel.paiement.consultation.fournisseur")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtTelephone.getValue().equals(""))
		     {
             errorComponent = txtTelephone;
             errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.telephone")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtEmail.getValue().equals(""))
		     {
             errorComponent = txtEmail;
             errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.email")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if (ToolKermel.ControlValidateEmail(txtEmail.getValue())) {
				
			} else {
				errorComponent = txtEmail;
                errorMsg = Labels.getLabel("kermel.referentiel.personne.email.incorrect");
 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
 				lbStatusBar.setValue(errorMsg);
 				throw new WrongValueException(errorComponent,errorMsg);
			}
			return true;
			
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	public void  onClick$menuFermer(){
		effacerform();
		step0.setVisible(true);
		step1.setVisible(false);
	}
	public void  onClick$menuAjouter(){
		step0.setVisible(false);
		step1.setVisible(true);
		
		
	}
	public void  effacerform(){
		dtdateretrait.setValue(null);
	
		txtTelephone.setValue("");
		txtFax.setValue("");
		txtEmail.setValue("");
		idretrait=null;
		
	}
	
	public void  onClick$menuValider(){
		if(checkFieldConstraints())
		{
			retrait.setDateRetrait(dtdateretrait.getValue());
			retrait.setNomSoumissionnaire(fournisseur.getNom());
			retrait.setTelephone(txtTelephone.getValue());
			retrait.setFax(txtFax.getValue());
			retrait.setEmail(txtEmail.getValue());
			retrait.setDossier(dossier);
			retrait.setAutorite(autorite);
			retrait.setPays(pays);
			retrait.setFournisseur(fournisseur);
			if(idretrait==null)
			{
				retrait=BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).save(retrait);
				SygPlisouvertures depot=new SygPlisouvertures();
				depot.setDossier(dossier);
				depot.setRetrait(retrait);
				depot.setRaisonsociale(retrait.getNomSoumissionnaire());
				depot.setDateDepot(dtdateretrait.getValue());
				depot.setAutorite(autorite);
				depot.setNotepreselectionne(new BigDecimal(0));
				depot.setNotifie("non");
				depot.setPays(pays.getLibelle());
				depot.setFournisseur(fournisseur);
				depot.setPrixevalue(new BigDecimal(0));
				depot.setMontantoffert(new BigDecimal(0));
				depot.setMontantdefinitif(new BigDecimal(0));
				depot.setGarantiesoumission("non");
				depot.setRabais(0);
				depot.setScoretechnique(0);
				depot.setSeuilatteint(0);
				depot.setClassementechnique(0);
				depot.setClassementgeneral(0);
				depot.setCandidatrestreint_ID(0);
				depot.setEtatPreselection(0);
				depot.setEtatExamenPreliminaire(0);
				depot.setCritereQualification(0);
				depot.setAttributaireProvisoire(0);
				depot.setOffreTechnique(0);
				depot.setOffreFinanciere(0);
				depot.setLettreSoumission(0);
				depot.setValide(0);
				depot.setGarantie(0);
				depot.setPiecerequise(0);
				BeanLocator.defaultLookup(RegistrededepotSession.class).save(depot);
			}
			else
				BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).update(retrait);
			session.setAttribute("libelle", "depotcandidature");
			loadApplicationState("procedure_pi");
			
		}
	}
	
///////////Pays///////// 
		public void onSelect$lstFournisseur(){
			fournisseur= (SygFournisseur) lstFournisseur.getSelectedItem().getValue();
			pays=fournisseur.getPays();
			txtPays.setValue(pays.getLibelle());
		bdFournisseur.setValue(fournisseur.getNom());
		txtTelephone.setValue(fournisseur.getTel());
		txtFax.setValue(fournisseur.getFax());
		txtEmail.setValue(fournisseur.getEmail());
		bdFournisseur.close();
		
		}
		
		public class FournisseursRenderer implements ListitemRenderer{
		
		
		
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygFournisseur fournisseur = (SygFournisseur) data;
			item.setValue(fournisseur);
			
			Listcell cellNom = new Listcell(fournisseur.getNom());
			cellNom.setParent(item);
			
			
			}
		}
		public void onFocus$txtRechercherFournisseur(){
		if(txtRechercherFournisseur.getValue().equalsIgnoreCase(Labels.getLabel("kermel.paiement.consultation.fournisseur"))){
			txtRechercherFournisseur.setValue("");
		}		 
		}
		
		public void  onOK$btnRechercherFournisseurs(){
			onClick$btnRechercherFournisseurs();
		}
		public void  onOK$txtRechercherFournisseur(){
			onClick$btnRechercherFournisseurs();
		}
		public void  onClick$btnRechercherFournisseurs(){
		if(txtRechercherFournisseur.getValue().equalsIgnoreCase(Labels.getLabel("kermel.paiement.consultation.fournisseur")) || txtRechercherFournisseur.getValue().equals("")){
			nomfournisseur = null;
		page=null;
		}else{
			nomfournisseur = txtRechercherFournisseur.getValue();
		page="0";
		}
		Events.postEvent(ApplicationEvents.ON_FOURNISSEUR, this, page);
		}
		
		 public void onClick$btnNouveau(){
				final String uri = "/referentiel/fournisseur/form.zul";

				final HashMap<String, String> display = new HashMap<String, String>();
				display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
				display.put(DSP_HEIGHT,"500px");
				display.put(DSP_WIDTH, "80%");

				final HashMap<String, Object> data = new HashMap<String, Object>();
				data.put(FournisseurFormController.WINDOW_PARAM_FICHIER, "RETRAIT");
				data.put(FournisseurFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
	    		showPopupWindow(uri, data, display);
		 }

}