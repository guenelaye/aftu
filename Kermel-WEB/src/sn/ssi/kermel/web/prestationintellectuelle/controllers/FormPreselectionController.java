package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCritere;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygDossierssouscriteresevaluateurs;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersSouscriteresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.be.referentiel.ejb.CritereSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class FormPreselectionController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer, RowRenderer, RowRendererExt{

	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
  	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	SygCritere critere=new SygCritere();
	SygCritere newcritere=new SygCritere();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuAjouter,menuNouveauCritere;
	private Div step0,step1,step2;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Textbox txtlibelle;
	private Grid GridListe,GridListeCriteres;
	List<SygDossierssouscriteresevaluateurs> notes = new ArrayList<SygDossierssouscriteresevaluateurs>();
	private int nombre=0;
	private Double noteretenue=0.0;
	List<SygDossierssouscriteres> souscriteres = new ArrayList<SygDossierssouscriteres>();
	private BigDecimal noteobtenue=new BigDecimal(0);
	private String raisonsocial=null,modele;
	private Textbox txtSoumissionnaire;
	private Tab TAB_LOTS,TAB_CRITERTES;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		addEventListener(ApplicationEvents.ON_CRITERES, this);
		
    
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,1);
		if(dossier!=null)
		{
			createLigneNotes(dossier);
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			Events.postEvent(ApplicationEvents.ON_CRITERES, this, null);
			
		}
		 modele= (String) Executions.getCurrent().getAttribute("div");
		 if(modele!=null)
		 {
			 if(modele.equals("criteres"))
				 TAB_CRITERTES.setSelected(true);
			 else
				 TAB_LOTS.setSelected(true);
		 }
		 else
			 TAB_LOTS.setSelected(true);
		
	}
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			
			 
			 GridListe.setRowRenderer(this);
			
			    List<SygDossierssouscriteresevaluateurs> valLignes = new ArrayList<SygDossierssouscriteresevaluateurs>();
			    List<SygPlisouvertures> plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(dossier, null, -1, raisonsocial);
					
			  for (SygPlisouvertures pli : plis) {
				  nombre=0;
				  noteobtenue=new BigDecimal(0);
				  SygDossierssouscriteresevaluateurs categ=new SygDossierssouscriteresevaluateurs();
			      
			      categ.setLibelle(pli.getRetrait().getNomSoumissionnaire());
			       List<SygDossierssouscriteresevaluateurs> vals = BeanLocator.defaultLookup(DossiersSouscriteresSession.class).ListesNotes(0, -1, dossier,null, pli, null);;
			   	   if(vals.size()!=0)   
				     valLignes.add(categ);	
		 		       for (SygDossierssouscriteresevaluateurs valLigne : vals) {
			 		       valLignes.add(valLigne);
			 		       if(valLigne.getNote().intValue()!=0)
			 		    	   nombre=nombre+1;
			 		           noteobtenue=noteobtenue.add(valLigne.getNote());
				        }
		 		    pli.setNotepreselectionne(noteobtenue);
					if((noteobtenue.intValue())>=dossier.getDosNoteEliminatoireAmi())
					  pli.setEtatPreselection(1);
					else
					pli.setEtatPreselection(-1);
			  		BeanLocator.defaultLookup(RegistrededepotSession.class).update(pli);
			
			  		
			//  pgPagination.setTotalSize(BeanLocator.defaultLookup( DossiersSouscriteresSession.class).count(filtrepar,idpar,plan, UIConstants.PARAM_TMFOURNITURES));

			  }
			  GridListe.setModel(new ListModelList(valLignes));	
		     	
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CRITERES)) {
			
			 GridListeCriteres.setRowRenderer(new CriteresRenderer());
			
			  List<SygDossierssouscriteresevaluateurs> valLignes = new ArrayList<SygDossierssouscriteresevaluateurs>();
			    List<SygPlisouvertures> plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(dossier, null, -1, raisonsocial);
			    List<SygDossierssouscriteres> criteres =  BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(0,-1,dossier, null, null);;
				   
			    for (SygDossierssouscriteres critere : criteres) {
			    	
			    	 SygDossierssouscriteresevaluateurs categ=new SygDossierssouscriteresevaluateurs();
				      
				      categ.setLibelle(critere.getCritere().getLibelle());
				      List<SygDossierssouscriteresevaluateurs> vals = BeanLocator.defaultLookup(DossiersSouscriteresSession.class).ListesNotes(0, -1, dossier,critere.getCritere(), null, null);;
				   	   if(vals.size()!=0)   
					     valLignes.add(categ);	
			 		       for (SygDossierssouscriteresevaluateurs valLigne : vals) {
				 		       valLignes.add(valLigne);
				 		   
					        }
			    }
	
		    GridListeCriteres.setModel(new ListModelList(valLignes));	
		     	
		} 
		
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygDossierssouscriteres dossiers = (SygDossierssouscriteres) data;
		item.setValue(dossiers);

		 Listcell cellLibelle = new Listcell(dossiers.getCritere().getLibelle());
		 cellLibelle.setParent(item);
		 
		
	}

	
	public class PiecesRenderer implements ListitemRenderer{
		
		
		
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygCritere criteres = (SygCritere) data;
		item.setValue(criteres);

		 Listcell cellLibelle = new Listcell(criteres.getLibelle());
		 cellLibelle.setParent(item);
		 
		
	
	}
	}

	public void onClick$menuAjouter()
	{
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
		Events.postEvent(ApplicationEvents.ON_CRITERES, this, null);
	}
	public void onClick$menuFermer()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
		
	}
	

	
	 public void onClick$menuValiderNouveau()  {
		 if(checkFieldConstraints())
		 {
		 newcritere.setLibelle(txtlibelle.getValue());
		 newcritere.setAutorite(autorite);
		 newcritere= BeanLocator.defaultLookup(CritereSession.class).save(newcritere); 
		 
		 SygDossierssouscriteres dossiercritere=new SygDossierssouscriteres();
  	     dossiercritere.setCritere(newcritere);
  	     dossiercritere.setDossier(dossier);
  	     dossiercritere.setNote(new BigDecimal(0));
  	     BeanLocator.defaultLookup(DossiersSouscriteresSession.class).save(dossiercritere);
		 session.setAttribute("libelle", "criteresqualifications");
		loadApplicationState("procedure_pi");
		 }

	 }
	 
	 private boolean checkFieldConstraints() {
			try {
				
				 if(txtlibelle.getValue().equals(""))
			     {
				 errorComponent = txtlibelle;
				 errorMsg = Labels.getLabel("kermel.referentiel.common.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				 lbStatusBar.setStyle(ERROR_MSG_STYLE);
				 lbStatusBar.setValue(errorMsg);
				 throw new WrongValueException (errorComponent, errorMsg);
			    }
				
				
				return true;
					
			}
			catch (Exception e) {
				errorMsg = Labels.getLabel("kermel.erreur.champobligatoire") + ": " + e.toString()
				+ " [checkFieldConstraints]";
				errorComponent = null;
				return false;

				
			}
			
		}
	 
	 public void onClick$menuFermerNouveau()
		{
			step0.setVisible(true);
			step1.setVisible(false);
			step2.setVisible(false);
			
		}
	 
		
	 
	     private void createLigneNotes(SygDossiers dossier) {

		    
			List<SygDossierssouscriteres> criteres = BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(0,-1,dossier, null, null);
			List<SygPlisouvertures> plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(dossier, null, -1, null);
			
			for (SygPlisouvertures pli : plis) {
				
				for (int i = 0; i < criteres.size(); i++) {
					notes=BeanLocator.defaultLookup(DossiersSouscriteresSession.class).ListesNotes(0, -1, dossier, criteres.get(i).getCritere(), pli, null);
					
					if(notes.size()==0)
					{
						SygDossierssouscriteresevaluateurs note = new SygDossierssouscriteresevaluateurs();
					
						note.setCritere(criteres.get(i).getCritere());
						note.setDossier(dossier);
						note.setPlis(pli);
						note.setNote(new BigDecimal(0));
						BeanLocator.defaultLookup(DossiersSouscriteresSession.class).saveNote(note);
					}
				}
			
				
			   }
			
			}
	     
	     
	     @Override
	 	public Row newRow(Grid grid) {
	 		// Create EditableRow instead of Row(default)
	 		Row row = new EditableRow();
	 		row.applyProperties();
	 		return row;
	 	}

	 	@Override
	 	public Component newCell(Row row) {
	 		return null;// Default Cell
	 	}

	 	@Override
	 	public int getControls() {
	 		return RowRendererExt.DETACH_ON_RENDER; // Default Value
	 	}

	 	@Override
	 	public void render(Row row, Object data, int index) throws Exception {
	 		
	 		final SygDossierssouscriteresevaluateurs note = (SygDossierssouscriteresevaluateurs) data;
	 		
	 		final EditableRow editRow = (EditableRow) row;
	 		
	 		if (note.getLibelle() != null) {
	 			final EditableDiv libelle = new EditableDiv(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial")+":  " +note.getLibelle(), false);
	 			libelle.txb.setReadonly(true);
	 		
	 			libelle.setHeight("30px");
	 			libelle.setStyle("color:#000;");
	 			libelle.setParent(editRow);
	 			
	 			
	 			
	 			final EditableDiv notes = new EditableDiv("", false);
	 			notes.setParent(editRow);
	 			
	 			final EditableDiv noteponderee = new EditableDiv("", false);
	 			noteponderee.setParent(editRow);
	 			
	 		}
	      else {
	 		   	final EditableDiv libelle = new EditableDiv( note.getCritere().getLibelle(), false);
	 		    libelle.setStyle("margin-left:10px;color:#000;");
	 	 	    libelle.txb.setReadonly(true);
	 		    libelle.setParent(editRow);
	 		
	 		    souscriteres=BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(0, -1, note.getDossier(), note.getCritere().getId(), null);
	 	 		final EditableDiv notes= new EditableDiv(ToolKermel.format2Decimal(note.getNote()), false);
	 	 		notes.setParent(editRow);
	 		
	 	 		final EditableDiv noteponderee= new EditableDiv(" / "+ToolKermel.format2Decimal(souscriteres.get(0).getNote()), false);
	 	 		noteponderee.txb.setReadonly(true);
	 	 		noteponderee.setParent(editRow);
	    	
	 		final Div ctrlDiv = new Div();
	 		ctrlDiv.setParent(editRow);
	 		final Button editBtn = new Button(null, "/images/pencil-small.png");
	 		editBtn.setMold("os");
	 		editBtn.setHeight("20px");
	 		editBtn.setWidth("30px");
	 		if(dossier!=null)
			{
	 			if(dossier.getDosDateNotification()!=null)
	 				editBtn.setDisabled(true);
			}
	 		editBtn.setParent(ctrlDiv);
	 		// Button listener - control the editable of row
	 		editBtn.addEventListener(Events.ON_CLICK, new EventListener() {
	 			public void onEvent(Event event) throws Exception {
	 				final Button submitBtn = (Button) new Button(null, "/images/tick-small.png");
	 				final Button cancelBtn = (Button) new Button(null, "/images/cross-small.png");
	 				submitBtn.setMold("os");
	 				submitBtn.setHeight("20px");
	 				submitBtn.setWidth("30px");
	 				submitBtn.setTooltiptext("Valider la saisie");
	 				submitBtn.addEventListener(Events.ON_CLICK, new EventListener() {
	 					public void onEvent(Event event) throws Exception {

	 						editRow.toggleEditable(true);
	 						if (!ToolKermel.estReel(notes.txb.getValue())) {
	 							editRow.toggleEditable(true);
	 							throw new WrongValueException(notes, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.notepondere.numerique"));

	 						}
	 						noteretenue=Double.valueOf(notes.txb.getValue());
	 						souscriteres=BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(0, -1, note.getDossier(), note.getCritere().getId(), null);
	 						if(noteretenue.intValue()>+souscriteres.get(0).getNote().intValue())
	 						{
	 							editRow.toggleEditable(true);
	 							throw new WrongValueException(notes, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.notepondere.inferieur")+" :"+souscriteres.get(0).getNote().intValue());

	 						}
	 						
	 						note.setNote(new BigDecimal(noteretenue));   
	 								
	 						BeanLocator.defaultLookup(DossiersSouscriteresSession.class).updateNote(note);
	 						submitBtn.detach();
	 						cancelBtn.detach();
	 						editBtn.setParent(ctrlDiv);
	 						Executions.getCurrent().setAttribute("div", "fournisseur");
	 						session.setAttribute("libelle", "preselection");
	 						loadApplicationState("procedure_pi");
	 					}
	 				});
	 				notes.addEventListener(Events.ON_OK, new EventListener() {
	 					public void onEvent(Event event) throws Exception {

	 						editRow.toggleEditable(true);
	 						if (!ToolKermel.estReel(notes.txb.getValue())) {
	 							editRow.toggleEditable(true);
	 							throw new WrongValueException(notes, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.notepondere.numerique"));

	 						}
	 						noteretenue=Double.valueOf(Integer.parseInt(notes.txb.getValue()));
	 						souscriteres=BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(0, -1, note.getDossier(), note.getCritere().getId(), null);
	 						if(noteretenue.intValue()>+souscriteres.get(0).getNote().intValue())
	 						{
	 							editRow.toggleEditable(true);
	 							throw new WrongValueException(notes, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.notepondere.inferieur")+" :"+souscriteres.get(0).getNote().intValue());

	 						}
	 						
	 						note.setNote(new BigDecimal(noteretenue));   
	 								
	 						BeanLocator.defaultLookup(DossiersSouscriteresSession.class).updateNote(note);
	 						submitBtn.detach();
	 						cancelBtn.detach();
	 						editBtn.setParent(ctrlDiv);
	 						Executions.getCurrent().setAttribute("div", "fournisseur");
	 						session.setAttribute("libelle", "preselection");
	 						loadApplicationState("procedure_pi");
	 					}
	 				});
	 			
	 				cancelBtn.setMold("os");
	 				cancelBtn.setHeight("20px");
	 				cancelBtn.setWidth("30px");
	 				cancelBtn.setTooltiptext("Annuler la saisie");
	 				cancelBtn.addEventListener(Events.ON_CLICK, new EventListener() {
	 					public void onEvent(Event event) throws Exception {
	 						editRow.toggleEditable(false);
	 						submitBtn.detach();
	 						cancelBtn.detach();
	 						editBtn.setParent(ctrlDiv);
	 					}
	 				});
	 				submitBtn.setParent(ctrlDiv);
	 				cancelBtn.setParent(ctrlDiv);
	 				editRow.toggleEditable(true);
	 				editBtn.detach();
	 			}
	 		});

	 	}
	 			
	 		
	 	}
	 	  public void  onClick$btnRechercher(){
		 		if((txtSoumissionnaire.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial")))||(txtSoumissionnaire.getValue().equals("")))
		 		 {
		 			raisonsocial=null;
		 		 }
		 		else
		 		 {
		 			raisonsocial=txtSoumissionnaire.getValue();
		 			
		 		 }
		 	
		 		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		 		}
	  	 public void onFocus$txtSoumissionnaire(){
	 			if(txtSoumissionnaire.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial"))){
	 				txtSoumissionnaire.setValue("");
	 			}		 
	 			}
	 		public void onBlur$txtSoumissionnaire(){
	 			if(txtSoumissionnaire.getValue().equalsIgnoreCase("")){
	 				txtSoumissionnaire.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial"));
	 			}		 
	 			}
	 		public void onOK$txtSoumissionnaire(){
	 			onClick$btnRechercher();	
	 		}
	 		public void onOK$btnRechercher(){
	 			onClick$btnRechercher();	
	 		}
	 		
	public class CriteresRenderer implements RowRenderer, RowRendererExt{
		   @Override
		 	public Row newRow(Grid grid) {
		 		// Create EditableRow instead of Row(default)
		 		Row row = new EditableRow();
		 		row.applyProperties();
		 		return row;
		 	}

		 	@Override
		 	public Component newCell(Row row) {
		 		return null;// Default Cell
		 	}

		 	@Override
		 	public int getControls() {
		 		return RowRendererExt.DETACH_ON_RENDER; // Default Value
		 	}

	 	@Override
	 	public void render(Row row, Object data, int index) throws Exception {
	 		
	 		final SygDossierssouscriteresevaluateurs note = (SygDossierssouscriteresevaluateurs) data;
	 		
	 		final EditableRow editRow = (EditableRow) row;
		 		
		 		if (note.getLibelle() != null) {
		 			final EditableDiv libelle = new EditableDiv(Labels.getLabel("kermel.appelsoffre.critere.titre")+":  " +note.getLibelle(), false);
		 			libelle.txb.setReadonly(true);
		 		
		 			libelle.setHeight("30px");
		 			libelle.setStyle("color:#000;");
		 			libelle.setParent(editRow);
		 			
		 			
		 			
		 			final EditableDiv notes = new EditableDiv("", false);
		 			notes.setParent(editRow);
		 			
		 			final EditableDiv noteponderee = new EditableDiv("", false);
		 			noteponderee.setParent(editRow);
		 			
		 		}
		 		else
		 		{
		 		   	final EditableDiv libelle = new EditableDiv( note.getPlis().getRaisonsociale(), false);
		 		    libelle.setStyle("margin-left:10px;color:#000;");
		 	 	    libelle.txb.setReadonly(true);
		 		    libelle.setParent(editRow);
		 		
		 		    souscriteres=BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(0, -1, note.getDossier(), note.getCritere().getId(), null);
		 	 		final EditableDiv notes= new EditableDiv(ToolKermel.format2Decimal(note.getNote()), false);
		 	 		notes.setParent(editRow);
		 		
		 	 		final EditableDiv noteponderee= new EditableDiv(" / "+ToolKermel.format2Decimal(souscriteres.get(0).getNote()), false);
		 	 		noteponderee.txb.setReadonly(true);
		 	 		noteponderee.setParent(editRow);
		    	
		 		final Div ctrlDiv = new Div();
		 		ctrlDiv.setParent(editRow);
		 		final Button editBtn = new Button(null, "/images/pencil-small.png");
		 		editBtn.setMold("os");
		 		editBtn.setHeight("20px");
		 		editBtn.setWidth("30px");
		 		if(dossier!=null)
				{
		 			if(dossier.getDosDateNotification()!=null)
		 				editBtn.setDisabled(true);
				}
		 		editBtn.setParent(ctrlDiv);
		 		// Button listener - control the editable of row
		 		editBtn.addEventListener(Events.ON_CLICK, new EventListener() {
		 			public void onEvent(Event event) throws Exception {
		 				final Button submitBtn = (Button) new Button(null, "/images/tick-small.png");
		 				final Button cancelBtn = (Button) new Button(null, "/images/cross-small.png");
		 				submitBtn.setMold("os");
		 				submitBtn.setHeight("20px");
		 				submitBtn.setWidth("30px");
		 				submitBtn.setTooltiptext("Valider la saisie");
		 				submitBtn.addEventListener(Events.ON_CLICK, new EventListener() {
		 					public void onEvent(Event event) throws Exception {

		 						editRow.toggleEditable(true);
		 						if (!ToolKermel.estReel(notes.txb.getValue())) {
		 							editRow.toggleEditable(true);
		 							throw new WrongValueException(notes, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.notepondere.numerique"));

		 						}
		 						noteretenue=Double.valueOf(notes.txb.getValue());
		 						souscriteres=BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(0, -1, note.getDossier(), note.getCritere().getId(), null);
		 						if(noteretenue.intValue()>+souscriteres.get(0).getNote().intValue())
		 						{
		 							editRow.toggleEditable(true);
		 							throw new WrongValueException(notes, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.notepondere.inferieur")+" :"+souscriteres.get(0).getNote().intValue());

		 						}
		 						
		 						note.setNote(new BigDecimal(noteretenue));   
		 								
		 						BeanLocator.defaultLookup(DossiersSouscriteresSession.class).updateNote(note);
		 						submitBtn.detach();
		 						cancelBtn.detach();
		 						editBtn.setParent(ctrlDiv);
		 						Executions.getCurrent().setAttribute("div", "criteres");
		 						session.setAttribute("libelle", "preselection");
		 						loadApplicationState("procedure_pi");
		 					}
		 				});
		 				notes.addEventListener(Events.ON_OK, new EventListener() {
		 					public void onEvent(Event event) throws Exception {

		 						editRow.toggleEditable(true);
		 						if (!ToolKermel.estReel(notes.txb.getValue())) {
		 							editRow.toggleEditable(true);
		 							throw new WrongValueException(notes, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.notepondere.numerique"));

		 						}
		 						noteretenue=Double.valueOf(Integer.parseInt(notes.txb.getValue()));
		 						souscriteres=BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(0, -1, note.getDossier(), note.getCritere().getId(), null);
		 						if(noteretenue.intValue()>+souscriteres.get(0).getNote().intValue())
		 						{
		 							editRow.toggleEditable(true);
		 							throw new WrongValueException(notes, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.depotscandidatures.notepondere.inferieur")+" :"+souscriteres.get(0).getNote().intValue());

		 						}
		 						
		 						note.setNote(new BigDecimal(noteretenue));   
		 								
		 						BeanLocator.defaultLookup(DossiersSouscriteresSession.class).updateNote(note);
		 						submitBtn.detach();
		 						cancelBtn.detach();
		 						editBtn.setParent(ctrlDiv);
		 						Executions.getCurrent().setAttribute("div", "criteres");
		 						session.setAttribute("libelle", "preselection");
		 						loadApplicationState("procedure_pi");
		 					}
		 				});
		 			
		 				cancelBtn.setMold("os");
		 				cancelBtn.setHeight("20px");
		 				cancelBtn.setWidth("30px");
		 				cancelBtn.setTooltiptext("Annuler la saisie");
		 				cancelBtn.addEventListener(Events.ON_CLICK, new EventListener() {
		 					public void onEvent(Event event) throws Exception {
		 						editRow.toggleEditable(false);
		 						submitBtn.detach();
		 						cancelBtn.detach();
		 						editBtn.setParent(ctrlDiv);
		 					}
		 				});
		 				submitBtn.setParent(ctrlDiv);
		 				cancelBtn.setParent(ctrlDiv);
		 				editRow.toggleEditable(true);
		 				editBtn.detach();
		 			}
		 		});
		 		}
				}
	 			}
}