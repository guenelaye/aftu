package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class DossiersTermeareferenceController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code;
	private Tab TAB_infosgenerales,TAB_garanties,TAB_piecesadministratives,TAB_criteresqualifications,TAB_devise,TAB_financement;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblInfos;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		
		LibelleTab=(String) session.getAttribute("libelle");
		if(LibelleTab!=null)
		{
			if((LibelleTab.equals("termereference"))||(LibelleTab.equals("infogenerales")))
			  {
				TAB_infosgenerales.setSelected(true);
				Include inc = (Include) this.getFellowIfAny("incinfosgenerales");
				inc.setSrc("/passationsmarches/prestationintellectuelle/infogenerales.zul");	
			  }
			else
			{
				if(LibelleTab.equals("criteresqualifications"))
				  {
					TAB_criteresqualifications.setSelected(true);
					Include inc = (Include) this.getFellowIfAny("inccriteresqualifications");
					inc.setSrc("/passationsmarches/prestationintellectuelle/criteresqualifications.zul");	
				  }
				else
				{
					TAB_infosgenerales.setSelected(true);
					Include inc = (Include) this.getFellowIfAny("incinfosgenerales");
					inc.setSrc("/passationsmarches/prestationintellectuelle/infogenerales.zul");
				}
				
			}
		
		}
		else
		{
			 Include inc = (Include) this.getFellowIfAny("incinfosgenerales");
		    inc.setSrc("/passationsmarches/prestationintellectuelle/infogenerales.zul");
		}
		    		
		
		   
	
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	
}