package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygGarantiesDossiers;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersSouscriteresSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;



public class CriteresQualificationsRenderer extends AbstractWindow implements RowRenderer, RowRendererExt,AfterCompose,EventListener {

	private	SygGarantiesDossiers garantie;
	private List<SygGarantiesDossiers> listesplis = new ArrayList<SygGarantiesDossiers>();
	private String pourcantage;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygDossiers dossier=new SygDossiers();
	Session session = getHttpSession();
	public static Long CURRENT_CODE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	}
	
	
	@Override
	public void onEvent(Event event) throws Exception {
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			 Long id = (Long) ((HashMap<String, Object>) event.getData()).get(CURRENT_MODULE);
			 BeanLocator.defaultLookup(DossiersSouscriteresSession.class).delete(id);
			// if (confirmer != null && confirmer.equalsIgnoreCase("Cession_Confirmer")) 
		
			session.setAttribute("libelle", "criteresqualifications");
			loadApplicationState("procedure_pi");
		}
		
	}
	
	
	public void onCreate(CreateEvent createEvent) {
		
		
	
	}


	@Override
	public Row newRow(Grid grid) {
		// Create EditableRow instead of Row(default)
		Row row = new EditableRow();
		row.applyProperties();
		return row;
	}

	@Override
	public Component newCell(Row row) {
		return null;// Default Cell
	}

	@Override
	public int getControls() {
		return RowRendererExt.DETACH_ON_RENDER; // Default Value
	}

	@Override
	public void render(Row row, Object data, int index) throws Exception {
		final SygDossierssouscriteres criteres = (SygDossierssouscriteres) data;
		
		final EditableRow editRow = (EditableRow) row;
		
		final EditableDiv critere = new EditableDiv(criteres.getCritere().getLibelle(), false);
		critere.txb.setReadonly(true);
		critere.setParent(editRow);
		
		
		final EditableDiv note = new EditableDiv(ToolKermel.format2Decimal(criteres.getNote()), false);
		note.setParent(editRow);
		
		final Div ctrlDiv = new Div();
		ctrlDiv.setParent(editRow);
		final Button editBtn = new Button(null, "/images/disk.png");
		editBtn.setMold("os");
		editBtn.setHeight("20px");
		editBtn.setWidth("30px");
		editBtn.setParent(ctrlDiv);
		
		final Button deleteBtn = new Button(null, "/images/delete.png");
		deleteBtn.setMold("os");
		deleteBtn.setHeight("20px");
		deleteBtn.setWidth("30px");
		deleteBtn.setParent(ctrlDiv);
		

		// Button listener - control the editable of row
		editBtn.addEventListener(Events.ON_CLICK, new EventListener() {
			public void onEvent(Event event) throws Exception {
				final Button submitBtn = (Button) new Button(null, "/images/ok.png");
				final Button cancelBtn = (Button) new Button(null, "/images/cancel.png");
				submitBtn.setMold("os");
				submitBtn.setHeight("20px");
				submitBtn.setWidth("30px");
				submitBtn.setTooltiptext("Valider la saisie");
				submitBtn.addEventListener(Events.ON_CLICK, new EventListener() {
					public void onEvent(Event event) throws Exception {

						editRow.toggleEditable(true);
//						if(!ToolKermel.estLong(montantcorriges.txb.getValue())){
//							editRow.toggleEditable(true);
//							throw new WrongValueException(montantcorriges, "Attention!! Le Montant  doit �tre num�rique!!");
//
//						}
//					
//						plis.setMontantdefinitif(new BigDecimal(Long.parseLong(montantcorriges.txb.getValue())));
					//	BeanLocator.defaultLookup(RegistrededepotSession.class).update(plis);
					
						submitBtn.detach();
						cancelBtn.detach();
						editBtn.setParent(ctrlDiv);
						deleteBtn.setParent(ctrlDiv);
						//session.setAttribute("libelle", "formcorrectionoffre");
						//loadApplicationState("procedure_pi");
					}
				});
			
				cancelBtn.setMold("os");
				cancelBtn.setHeight("20px");
				cancelBtn.setWidth("30px");
				cancelBtn.setTooltiptext("Annuler la saisie");
				cancelBtn.addEventListener(Events.ON_CLICK, new EventListener() {
					public void onEvent(Event event) throws Exception {
						editRow.toggleEditable(false);
						submitBtn.detach();
						cancelBtn.detach();
						editBtn.setParent(ctrlDiv);
						deleteBtn.setParent(ctrlDiv);
					}
				});
				submitBtn.setParent(ctrlDiv);
				cancelBtn.setParent(ctrlDiv);
				editRow.toggleEditable(true);
				editBtn.detach();
				deleteBtn.detach();
			}
		});

		deleteBtn.addEventListener(Events.ON_CLICK, new EventListener() {
			public void onEvent(Event event) throws Exception {
				final Button submitBtn = (Button) new Button(null, "/images/ok.png");
				final Button cancelBtn = (Button) new Button(null, "/images/cancel.png");
				submitBtn.setMold("os");
				submitBtn.setHeight("20px");
				submitBtn.setWidth("30px");
				submitBtn.setTooltiptext("Valider la saisie");
				submitBtn.addEventListener(Events.ON_CLICK, new EventListener() {
					public void onEvent(Event event) throws Exception {

						editRow.toggleEditable(true);
//						if(!ToolKermel.estLong(montantcorriges.txb.getValue())){
//							editRow.toggleEditable(true);
//							throw new WrongValueException(montantcorriges, "Attention!! Le Montant  doit �tre num�rique!!");
//
//						}
//					
//						plis.setMontantdefinitif(new BigDecimal(Long.parseLong(montantcorriges.txb.getValue())));
					//	BeanLocator.defaultLookup(RegistrededepotSession.class).update(plis);
					
						submitBtn.detach();
						cancelBtn.detach();
						editBtn.setParent(ctrlDiv);
						deleteBtn.setParent(ctrlDiv);
						//session.setAttribute("libelle", "formcorrectionoffre");
						//loadApplicationState("procedure_pi");
					}
				});
			
				cancelBtn.setMold("os");
				cancelBtn.setHeight("20px");
				cancelBtn.setWidth("30px");
				cancelBtn.setTooltiptext("Annuler la saisie");
				cancelBtn.addEventListener(Events.ON_CLICK, new EventListener() {
					public void onEvent(Event event) throws Exception {
						editRow.toggleEditable(false);
						submitBtn.detach();
						cancelBtn.detach();
						editBtn.setParent(ctrlDiv);
						deleteBtn.setParent(ctrlDiv);
					}
				});
				submitBtn.setParent(ctrlDiv);
				cancelBtn.setParent(ctrlDiv);
				editRow.toggleEditable(true);
				editBtn.detach();
				deleteBtn.detach();
				
				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, criteres.getId());
				showMessageBox(display, map);
			}
		});

	
	}
	
	
}