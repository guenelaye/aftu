package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class TraitementsDossiersMIController extends AbstractWindow implements
		AfterCompose, EventListener,ListitemRenderer {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code,avalider="oui";
	private Tab TAB_infosgenerales,TAB_garanties,TAB_piecesadministratives,TAB_criteresqualifications,TAB_devise,TAB_financement;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblInfos;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygDossiers dossier=new SygDossiers();
	private Listbox lstCandidats,lstCandidatsns;
	private Paging pgCandidats,pgCandidatsns;
	private  int byPage = UIConstants.DSP_BANDBOX_BY_PAGE;
	private Label candidatspreselectionnes,candidatsnonpreselectionnes,preselection;
	List<SygPlisouvertures> plis = new ArrayList<SygPlisouvertures>();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
		lstCandidats.setItemRenderer(this);
		pgCandidats.setPageSize(byPage);
		pgCandidats.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		
		lstCandidatsns.setItemRenderer(new CandidatsRenderer());
		pgCandidatsns.setPageSize(byPage);
		pgCandidatsns.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);

	}

	
	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,1);
		if(dossier!=null)
		{
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			if(dossier.getDosDateNotification()!=null)
			{
				preselection.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.preselection.notifie"));
			}
		}
			
		
	
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			/////////Candidats Pr�selectionnes
			 List<SygPlisouvertures> candidatsselectionnes = BeanLocator.defaultLookup(RegistrededepotSession.class).find(pgCandidats.getActivePage() * byPage,byPage,autorite,dossier, 1);
			 lstCandidats.setModel(new SimpleListModel(candidatsselectionnes));
			 pgCandidats.setTotalSize(BeanLocator.defaultLookup(RegistrededepotSession.class).count(autorite,dossier, 1));
			 if(candidatsselectionnes.size()>0)
			 {
				 candidatspreselectionnes.setVisible(true);
				 lstCandidats.setVisible(true);
				 pgCandidats.setVisible(true);
			 }
			 
		     /////////Candidats non Pr�selectionnes
			 List<SygPlisouvertures> candidatsnonselectionnes = BeanLocator.defaultLookup(RegistrededepotSession.class).find(pgCandidatsns.getActivePage() * byPage,byPage,autorite,dossier, -1);
			 lstCandidatsns.setModel(new SimpleListModel(candidatsnonselectionnes));
			 pgCandidatsns.setTotalSize(BeanLocator.defaultLookup(RegistrededepotSession.class).count(autorite,dossier, -1));
			 if(candidatsnonselectionnes.size()>0)
			 {
				 candidatsnonpreselectionnes.setVisible(true);
				 lstCandidatsns.setVisible(true);
				 pgCandidatsns.setVisible(true);
			 }
		} 
	}

	public class CandidatsRenderer implements ListitemRenderer{
		
		
		
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygPlisouvertures plis = (SygPlisouvertures) data;
			item.setValue(plis);

			Listcell cellRaisonsocial = new Listcell(plis.getRetrait().getNomSoumissionnaire());
			 cellRaisonsocial.setParent(item);
			 
			Listcell cellNote = new Listcell("");
			   if(plis.getNotepreselectionne()!=null)
				   cellNote.setLabel(ToolKermel.format3Decimal(plis.getNotepreselectionne()));
			cellNote.setParent(item);
			
		
		}
		}
	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygPlisouvertures plis = (SygPlisouvertures) data;
		item.setValue(plis);

		Listcell cellRaisonsocial = new Listcell(plis.getRetrait().getNomSoumissionnaire());
		 cellRaisonsocial.setParent(item);
		 
		Listcell cellNote = new Listcell("");
		   if(plis.getNotepreselectionne()!=null)
			   cellNote.setLabel(ToolKermel.format3Decimal(plis.getNotepreselectionne()));
		cellNote.setParent(item);
	
	}
	
	public void onClick$termedereference() {
		session.setAttribute("libelle", "termereference");
		loadApplicationState("procedure_pi");
	}
	
	public void onClick$miseenvalidation() {
		session.setAttribute("libelle", "validationdossier");
		loadApplicationState("procedure_pi");
	}
	
	public void onClick$publication() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		}
		else
		{
			if(avalider.equals("oui"))
			{
				if(dossier.getDosDateValidation()==null)
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validerdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					else
					{
						session.setAttribute("libelle", "publicationdossiermi");
						loadApplicationState("procedure_pi");
					}
						
			}
			else
			{
				session.setAttribute("libelle", "publicationdossiermi");
				loadApplicationState("procedure_pi");
			}
		
		}
		
	}
	
	public void onClick$depotcandidature() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		}
		else
		{
			if(dossier.getDosDatePublication()==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.publierdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
			}
			else
			{
				session.setAttribute("libelle", "depotcandidature");
				loadApplicationState("procedure_pi");
			}
		}
		
	}
	
	public void onClick$preselection() throws InterruptedException {
		plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,autorite,dossier, 1);
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		}
		else
		{
			if(plis.size()==0)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.saisirregistredepot"), "Erreur", Messagebox.OK, Messagebox.ERROR);
			}
			else
			{
				session.setAttribute("libelle", "preselection");
				loadApplicationState("procedure_pi");
			}
		}
		
	}
	
	public void onClick$notificationcandidatsretenus() {
		session.setAttribute("libelle", "notificationauxcandidatsretenus");
		loadApplicationState("procedure_pi");
	}
}