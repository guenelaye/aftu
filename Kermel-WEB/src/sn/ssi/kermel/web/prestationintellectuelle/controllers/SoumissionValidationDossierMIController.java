package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Fieldset;
import org.zkoss.zhtml.Legend;
import org.zkoss.zhtml.Table;
import org.zkoss.zhtml.Td;
import org.zkoss.zhtml.Tr;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygHistoriqueappeloffresAC;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DocumentsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.HistoriqueappeloffresACSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class SoumissionValidationDossierMIController extends AbstractWindow implements
		EventListener, AfterCompose, RowRenderer {

    public Textbox txtVersionElectronique,txtcommentaires;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
     Session session = getHttpSession();
    private Label lbltitre;
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherBailleur,txtChapitre;
    SygBailleurs bailleur=null;
    private Decimalbox dcmontantprevu;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idbailleur;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	private String nomFichier;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuValider;
	private Datebox dtmiseenvalidation;
	private Grid GridListe;
	private Label label,legend;
	private Fieldset fielsets;
	private Label datecreation;
	private Div step0,step1;
	private Iframe idIframe;
	private Label label2,label4,label5,labeldatemiseenvaleur,labelcommentaires,Fichier,lbl1,lbl3;
	private Button btnChoixFichier;
	private String extension,images;
	SygHistoriqueappeloffresAC historique=new SygHistoriqueappeloffresAC();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private Long nombrejour;
	private Menuitem menuEditer,menuClose;
	SygDocuments document=new SygDocuments();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		GridListe.setRowRenderer(this);
		
		
    	
    
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,1);
		if(dossier!=null)
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		if(dossier.getDosDateMiseValidation()==null)
		{
			labeldatemiseenvaleur.setValue(Labels.getLabel("kermel.plansdepassation.datemiseenvaleur"));
		
			labelcommentaires.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.mevalidation.commentaires"));
			Fichier.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.miseenvalidation.lettretransmission"));
			label2.setValue(":");
			
			label4.setValue(":");
			label5.setValue(":");
			lbltitre.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.mevalidation.soumvalidossier")+" "+appel.getAporeference());
			menuEditer.setVisible(true);
		}
		else
		{
			
			InfosHistorique();
//			lbltitre.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.mevalidation.soumvalidossier")+" "+appel.getAporeference());
//			
//			dtmiseenvalidation.setValue(dossier.getDosDateMiseValidation());
//		
//			txtcommentaires.setValue(dossier.getDosCommentaireMiseValidation());
//			menuValider.setDisabled(true);
//			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
//			
//		
//			labelcommentaires.setVisible(false);
//			labeldatemiseenvaleur.setVisible(false);
//			Fichier.setVisible(false);
//			label2.setVisible(false);
//			
//			label4.setVisible(false);
//			label5.setVisible(false);
//			dtmiseenvalidation.setVisible(false);
//		
//			txtcommentaires.setVisible(false);
//			txtVersionElectronique.setVisible(false);
//			btnChoixFichier.setVisible(false);
//			lbl1.setVisible(false);
//			lbl3.setVisible(false);
		}
		
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
		 List<SygHistoriqueappeloffresAC> historiques   = BeanLocator.defaultLookup(HistoriqueappeloffresACSession.class).find(0,-1,dossier,null,0);
		 GridListe.setModel(new SimpleListModel(historiques));
		 if(historiques.size()>0)
		 {
			 if(dossier.getDosDateMiseValidation()==null)
			 {
				 menuEditer.setVisible(true);
			 }
			  
		 }
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) {
			Image button = (Image) event.getTarget();
			String nomfichier=(String) button.getAttribute("fichier");
			step0.setVisible(false);
			step1.setVisible(true);
			String filepath = cheminDossier +  nomfichier;
			File f = new File(filepath.replaceAll("\\\\", "/"));

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(f, null, null);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (mymedia != null)
				idIframe.setContent(mymedia);
			else
				idIframe.setSrc("");

			idIframe.setHeight("600px");
			idIframe.setWidth("100%");
		}
	}

	 @Override
		public void render(Row row, Object data, int index) throws Exception {
		 SygHistoriqueappeloffresAC historiques = (SygHistoriqueappeloffresAC) data;
		 row.setValue(historiques);
		 
		 Fieldset fieldset=new Fieldset();
		 fieldset.setStyle("width:96%;");
	
		 Legend legend = new Legend();
		 Label label = new Label("Mise en validation");
		 label.setParent(legend);
		 legend.setParent(fieldset);
		 
		 Table table0=new Table();
		 Tr tr0=new Tr();
		 Td tdac=new Td();
		 Td tddt=new Td();
		 Td tddcmp=new Td();
		 
			 /////:::AC
			 Fieldset fieldsetac=new Fieldset();
			 fieldsetac.setStyle("width:350px;");
		
			 Legend legendac = new Legend();
			 Label labelac = new Label("");
			 if(historiques.getUser()!=null)
				 labelac.setValue(historiques.getUser().getPrenom()+" "+historiques.getUser().getNom());
			 else
				 labelac.setValue("(non encore renseign�)");
			 labelac.setParent(legendac);
			 legendac.setParent(fieldsetac);
			 
			 fieldsetac.setParent(tdac);
			 
				 Table tableac=new Table();
				 
				 Tr trac0=new Tr();
				 Td tdac00=new Td();
				 Td tdac01=new Td();
				 Td tdac02=new Td();
				 Label labelac00 = new Label("Propos� pour validation");
				 labelac00.setParent(tdac00);
				 Label labelac01 = new Label(":");
				 labelac01.setParent(tdac01);
				 Label labelac02 = new Label(UtilVue.getInstance().formateLaDate(historiques.getHistoac_datevalidation()));
				 labelac02.setParent(tdac02);
				 tdac00.setParent(trac0);
				 tdac01.setParent(trac0);
				 tdac02.setParent(trac0);
				 trac0.setParent(tableac);
				 
					 
				 Tr trac2=new Tr();
				 Td tdac20=new Td();
				 Td tdac21=new Td();
				 Td tdac22=new Td();
				 Label labelac20 = new Label("Commentaires");
				 labelac20.setParent(tdac20);
				 Label labelac21 = new Label(":");
				 labelac21.setParent(tdac21);
				 Label labelac22 = new Label(historiques.getHistoac_commentaire());
				 labelac22.setParent(tdac22);
				 tdac20.setParent(trac2);
				 tdac21.setParent(trac2);
				 tdac22.setParent(trac2);
				 trac2.setParent(tableac);
				 
				 Tr trac3=new Tr();
				 Td tdac30=new Td();
				 Td tdac31=new Td();
				 Td tdac32=new Td();
				 Label labelac30 = new Label("Fichier joint ");
				 labelac30.setParent(tdac30);
				 Label labelac31 = new Label(":");
				 labelac31.setParent(tdac31);
				 Label labelac32 = new Label(historiques.getHistoac_commentaire());
				 if(historiques.getDaofichier().equals(""))
				 {
					 labelac32.setValue("(non encore renseign�)");
					 labelac32.setWidth("80%");
					 labelac32.setParent(tdac32);
				 }
				 else
				 {
					 extension=historiques.getDaofichier().substring(historiques.getDaofichier().length()-3,  historiques.getDaofichier().length());
					 if(extension.equalsIgnoreCase("pdf"))
						 images="/images/icone_pdf.png";
					 else  
						 images="/images/word.jpg";
						
					 Image image=new Image();
					 image.setSrc(images);
					 image.setAttribute("fichier", historiques.getDaofichier());
					 image.setTooltiptext("Visualiser fichier");
					 image.addEventListener(Events.ON_CLICK, SoumissionValidationDossierMIController.this);
					 image.setParent(tdac32); 
				 }
				
				 tdac30.setParent(trac3);
				 tdac31.setParent(trac3);
				 tdac32.setParent(trac3);
				 trac3.setParent(tableac);
				 
				 tableac.setParent(fieldsetac);
				 
			 tdac.setParent(tr0);
		 
			 /////////DT////////
			 Fieldset fieldsetdelai=new Fieldset();
			 fieldsetdelai.setStyle("width:20%;align:center;");
			 
			 Legend legenddelai = new Legend();
			 Label labeldelai = new Label("D�lai de traitement ");
			 labeldelai.setParent(legenddelai);
			 legenddelai.setParent(fieldsetdelai);            
			 
			 fieldsetdelai.setParent(tddt);      
			 tddt.setStyle("align:center;");
			 
			 Label duree = new Label(" ");
			 if(historiques.getHisto_datevalidation()!=null)
			 {
				 nombrejour=ToolKermel.DifferenceDate(historiques.getHistoac_datevalidation(), historiques.getHisto_datevalidation(), "jj");
				 duree.setValue(nombrejour+1+" Jour(s)");
			 }
			 else
				 duree.setValue("");
			 
			 duree.setParent(fieldsetdelai);
			 tddt.setParent(tr0);
		 
		 
	          /////////DCMP////////
			 Fieldset fieldsetdcmp=new Fieldset();
			 fieldsetdcmp.setStyle("width:350px;");
			 
			 Legend legenddcmp = new Legend();
			 Label labeldcmp = new Label("DCMP");
			 labeldcmp.setParent(legenddcmp);
			 legenddcmp.setParent(fieldsetdcmp);
			 
			 
				 Table tabledcmp=new Table();
				 
				 Tr trdcmp2=new Tr();
				 Td tddcmp20=new Td();
				 Td tddcmp21=new Td();
				 Td tddcmp22=new Td();
				 Label labeldcmp20 = new Label("Validation");
				 labeldcmp20.setParent(tddcmp20);
				 Label labeldcmp21 = new Label(":");
				 labeldcmp21.setParent(tddcmp21);
				 Label labeldcmp22 = new Label("");
				 if(historiques.getHisto_validation().equals("0"))
				   {
					 labeldcmp22.setValue(Labels.getLabel("kermel.plansdepassation.dossier.nonrenseigne")); 
				  }
				 else
					 if(historiques.getHisto_validation().equals("1"))
					   {
						  labeldcmp22.setValue("Valid� le "+UtilVue.getInstance().formateLaDate(historiques.getHisto_datevalidation()));
					   }
					 else
					  {
							 labeldcmp22.setValue("Rejet� le "+UtilVue.getInstance().formateLaDate(historiques.getHisto_datevalidation()));
					  }
						 
				
				 labeldcmp22.setParent(tddcmp22);
				 tddcmp20.setParent(trdcmp2);
				 tddcmp21.setParent(trdcmp2);
				 tddcmp22.setParent(trdcmp2);
				 trdcmp2.setParent(tabledcmp);
	 
				
				 /////////:Commentaires
				 Tr trdcmp4=new Tr();
				 Td tddcmp40=new Td();
				 Td tddcmp41=new Td();
				 Td tddcmp42=new Td();
				 Label labeldcmp40 = new Label("Commentaires");
				 Label labeldcmp41 = new Label(":");
				 Label labeldcmp42 = new Label(historiques.getHisto_commentaire());
				 labeldcmp40.setParent(tddcmp40);
				 labeldcmp41.setParent(tddcmp41);
				 labeldcmp42.setParent(tddcmp42);
				 
				 tddcmp40.setParent(trdcmp4);
				 tddcmp41.setParent(trdcmp4);
				 tddcmp42.setParent(trdcmp4);
				 
				 trdcmp4.setParent(tabledcmp);
				 
				 ////////Fichier Joint
				 Tr trdcmp5=new Tr();
				 Td tddcmp50=new Td();
				 Td tddcmp51=new Td();
				 Td tddcmp52=new Td();
				 Label labeldcmp50 = new Label("Fichier joint");
				 Label labeldcmp51 = new Label(":");
				 Label labeldcmp52 = new Label("");
				 labeldcmp52.setParent(tddcmp52);
				 
				 if(historiques.getHisto_fichiervalidation()==null)
				 {
					 labeldcmp52.setValue("(non encore renseign�)");
					 labeldcmp52.setWidth("80%");
					 labeldcmp52.setParent(tddcmp52);
				 }
				 else
				 {
					 extension=historiques.getHisto_fichiervalidation().substring(historiques.getHisto_fichiervalidation().length()-3,  historiques.getHisto_fichiervalidation().length());
					 if(extension.equalsIgnoreCase("pdf"))
						 images="/images/icone_pdf.png";
					 else  
						 images="/images/word.jpg";
						
					 Image image=new Image();
					 image.setSrc(images);
					 image.setAttribute("fichier", historiques.getDaofichier());
					 image.setTooltiptext("Visualiser fichier");
					 image.addEventListener(Events.ON_CLICK, SoumissionValidationDossierMIController.this);
					 image.setParent(tddcmp52); 
				 }
				 labeldcmp50.setParent(tddcmp50);
				 labeldcmp51.setParent(tddcmp51);
				
				 
				 tddcmp50.setParent(trdcmp5);
				 tddcmp51.setParent(trdcmp5);
				 tddcmp52.setParent(trdcmp5);
				 
				 trdcmp5.setParent(tabledcmp);
				 
				 tabledcmp.setParent(fieldsetdcmp);

			 fieldsetdcmp.setParent(tddcmp);
			 tddcmp.setParent(tr0);
		 ////////////
		 tr0.setParent(table0);
		 table0.setParent(fieldset);
		 fieldset.setParent(row);

	   }
	public void onClick$btnChoixFichier() {
		//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
			if (ToolKermel.isWindows())
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectronique.setValue(nomFichier);
		}
	
private boolean checkFieldConstraints() {
		
		try {
		
			if(dtmiseenvalidation.getValue()==null)
		     {
               errorComponent = dtmiseenvalidation;
               errorMsg = Labels.getLabel("kermel.plansdepassation.datemiseenvaleur")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((dtmiseenvalidation.getValue()).after(new Date()))
			 {
				errorComponent = dtmiseenvalidation;
				errorMsg =Labels.getLabel("kermel.plansdepassation.datemiseenvaleur")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.referentiel.date.dujour")+": "+UtilVue.getInstance().formateLaDate(new Date());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			if(txtVersionElectronique.getValue().equals(""))
		     {
              errorComponent = txtVersionElectronique;
              errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.miseenvalidation.lettretransmission")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			return true;
			
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

		public void onClick$menuValider() {
			if(checkFieldConstraints())
			{
				dossier.setDosDateMiseValidation(dtmiseenvalidation.getValue());
				
				dossier.setDosFichierMiseValidationPrequalif(nomFichier);
				dossier.setDosCommentaireMiseValidation(txtcommentaires.getValue());
				BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).update(dossier);
				lbStatusBar.setValue("");
				
				historique.setUser(infoscompte);
				historique.setAutorite(autorite);
				historique.setDaofichier(txtVersionElectronique.getValue());
				historique.setHistoac_commentaire(txtcommentaires.getValue());
				historique.setHistoac_datecreationdossier(dossier.getDosDateCreation());
				historique.setHistoac_datevalidation(dtmiseenvalidation.getValue());
				historique.setHistoac_attribution(0);
				historique.setHisto_validation("0");
				historique.setDossier(dossier);
				BeanLocator.defaultLookup(HistoriqueappeloffresACSession.class).save(historique);
				
				
				document.setAppel(appel);
				if(!txtVersionElectronique.getValue().equals(""))
				document.setNomFichier(txtVersionElectronique.getValue());
				document.setTypeDocument(UIConstants.PARAM_TYPEDOCUMENTS_MEVDPPM);
				document.setLibelle(Labels.getLabel("kermel.plansdepassation.proceduresmarches.documents.mavdao"));
				document.setDossier(dossier);
				document.setDate(new Date());
				document.setHeure(Calendar.getInstance().getTime());
				BeanLocator.defaultLookup(DocumentsSession.class).save(document);
				
				session.setAttribute("libelle", "validationdossier");
				loadApplicationState("procedure_pi");
			}
		}
		
		public org.zkoss.util.media.AMedia fetchFile(File file) {

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(file, null, null);
				return mymedia;
			} catch (Exception e) {

				e.printStackTrace();
				return null;
			}

		}
		
		public void onClick$menuFermer() {
			step0.setVisible(true);
			step1.setVisible(false);
		}
		
		public void InfosHistorique() {
			if(dossier.getDosDateValidation()!=null)
				lbltitre.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.mevalidation.validossier")+" "+appel.getAporeference());
				
				dtmiseenvalidation.setValue(dossier.getDosDateMiseValidation());
				txtcommentaires.setValue(dossier.getDosCommentaireMiseValidation());
				menuValider.setDisabled(true);
				
				
				labelcommentaires.setVisible(false);
				labeldatemiseenvaleur.setVisible(false);
				Fichier.setVisible(false);
				label2.setVisible(false);
				label4.setVisible(false);
				label5.setVisible(false);
				dtmiseenvalidation.setVisible(false);
				txtcommentaires.setVisible(false);
				txtVersionElectronique.setVisible(false);
				btnChoixFichier.setVisible(false);
				lbl1.setVisible(false);
			
				lbl3.setVisible(false);
				GridListe.setVisible(true);
		}
		public void onClick$menuEditer() {
			InfosHistorique();
			GridListe.setVisible(true);
			menuEditer.setVisible(false);
			menuClose.setVisible(true);
		}
		public void onClick$menuClose() {
			InfosDossier();
			menuEditer.setVisible(true);
			menuClose.setVisible(false);
			menuValider.setDisabled(false);
		}
		public void InfosDossier() {
			labelcommentaires.setVisible(true);
			labeldatemiseenvaleur.setVisible(true);
			Fichier.setVisible(true);
			label2.setVisible(true);
			label4.setVisible(true);
			label5.setVisible(true);
			dtmiseenvalidation.setVisible(true);
			txtcommentaires.setVisible(true);
			txtVersionElectronique.setVisible(true);
			btnChoixFichier.setVisible(true);
			lbl1.setVisible(true);
			lbl3.setVisible(true);
			GridListe.setVisible(false);
		}
}