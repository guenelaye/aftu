package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class DossiersMIController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code;
	private Tab TAB_traitdossiers,TAB_recours,TAB_journeven,TAB_documents;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblInfos;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		
		LibelleTab=(String) session.getAttribute("libelle");
		if(LibelleTab!=null)
		{
			if(LibelleTab.equals("manifestationdinteret"))
			  {
				TAB_traitdossiers.setSelected(true);
				Include inc = (Include) this.getFellowIfAny("inctraitdossiers");
				inc.setSrc("/passationsmarches/prestationintellectuelle/traitementsdossiersmi.zul");	
			  }
			else
			{
				if(LibelleTab.equals("recours"))
				  {
					TAB_recours.setSelected(true);
					Include inc = (Include) this.getFellowIfAny("increcours");
					inc.setSrc("/passationsmarches/prestationintellectuelle/recours.zul");	
				  }
				else
				{
					if(LibelleTab.equals("documents"))
					  {
						TAB_documents.setSelected(true);
						Include inc = (Include) this.getFellowIfAny("incdocuments");
						inc.setSrc("/passationsmarches/prestationintellectuelle/documents.zul");	
					  }
					else
					{
						Include inc = (Include) this.getFellowIfAny("inctraitdossiers");
					    inc.setSrc("/passationsmarches/prestationintellectuelle/traitementsdossiersmi.zul");
					}
				}
			}
		
		}
		else
		{
			 Include inc = (Include) this.getFellowIfAny("inctraitdossiers");
		      inc.setSrc("/passationsmarches/prestationintellectuelle/traitementsdossiersmi.zul");
		}
		    		
		
		   
	
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	
}