package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class LectureOffreController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstMonnaie,lstNature;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle,txtRechercherPays;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference,libellebailleur=null;
    private int parent;
    private Label lbltitre;
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtPays,txtRaisonsocial;
    private Intbox txtnumplis;
    SygBailleurs bailleur=null;
     private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg,libellemonnaie=null,libellenature=null,libellepays=null;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idbailleur;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuAjouter,menuModifier,menuSupprimer;
	private Div step0,step1;
	private SygPlisouvertures plis=null;
	private Radio rdsoumoui,rdsoumnon,rdoftoui,rdoftnon,rdoffoui,rdoffnon;
	private Radiogroup rdpodepaiement;
	
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
		
    
		
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,2);
		if(dossier!=null)
		{
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
		
	
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygPlisouvertures> plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(activePage,byPageBandbox,dossier,null,null,null,-1,-1,-1, -1, -1, -1, null, -1, null, null);
			 lstListe.setModel(new SimpleListModel(plis));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(RegistrededepotSession.class).count(dossier,null,null,null,-1,-1,1, -1, -1, -1, null, -1, null, null));
		} 
	
	
		else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
			Listcell button = (Listcell) event.getTarget();
			String toDo = (String) button.getAttribute(UIConstants.TODO);
			plis = (SygPlisouvertures) button.getAttribute("plis");
			if (toDo.equalsIgnoreCase("modifier"))
			{
				
				txtnumplis.setValue(plis.getNumero());
			
				txtRaisonsocial.setValue(plis.getRetrait().getNomSoumissionnaire());
				txtPays.setValue(plis.getPays());
				
				
				if(plis.getLettreSoumission()==1)
					rdsoumoui.setChecked(true);
				else
					rdsoumnon.setChecked(true);
				if(plis.getOffreFinanciere()==1)
					rdoffoui.setChecked(true);
				else
					rdoffnon.setChecked(true);
				if(plis.getOffreTechnique()==1)
					rdoftoui.setChecked(true);
				else
					rdoftnon.setChecked(true);
				step0.setVisible(false);
				step1.setVisible(true);
				Events.postEvent(ApplicationEvents.ON_NATURES, this, null);
				Events.postEvent(ApplicationEvents.ON_MONNAIES, this, null);
				Events.postEvent(ApplicationEvents.ON_PAYS, this, page);
			}
		}
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygPlisouvertures plis = (SygPlisouvertures) data;
		item.setValue(plis);

		 Listcell cellNumero = new Listcell(Integer.toString(plis.getNumero()));
		 cellNumero.setParent(item);
		 
		 Listcell cellRaison = new Listcell(plis.getRetrait().getNomSoumissionnaire());
		 cellRaison.setParent(item);
		 
		
		 Listcell cellPays= new Listcell(plis.getPays());
		 cellPays.setParent(item);
		 
		 Listcell cellImageModif = new Listcell();
		 if(dossier!=null&&plis.getValide()==UIConstants.NPARENT)
		 {
			 cellImageModif.setImage("/images/calculator_add.png");
			 cellImageModif.setAttribute(UIConstants.TODO, "modifier");
			 cellImageModif.setAttribute("plis", plis);
			 cellImageModif.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.lectureoffres.saisiroffre"));
			 cellImageModif.addEventListener(Events.ON_CLICK, LectureOffreController.this);
	     }
		
		 cellImageModif.setParent(item);
		 
		 Listcell cellImageTraite = new Listcell();
		 
		 if(plis.getValide()==UIConstants.NPARENT)
		 {
		
		 cellImageTraite.setImage("/images/deletemoins.png");
		 cellImageTraite.setAttribute(UIConstants.TODO, "traiter");
		 cellImageTraite.setAttribute("plis", plis);
		 cellImageTraite.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.lectureoffres.saisiroffre"));
		
		 }
		 cellImageTraite.setParent(item);
	}

	public void onClick$menuAjouter()
	{
		Events.postEvent(ApplicationEvents.ON_PLIS, this, null);
		step0.setVisible(false);
		step1.setVisible(true);
		
	}
	
	public void onClick$menuFermer()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	


	public void  onClick$menuValider(){
		if(checkFieldConstraints())
		{
			plis.setNumero(txtnumplis.getValue());
			
			plis.setValide(UIConstants.PARENT);
			
			
			
			if(rdsoumoui.isChecked()==true)
				plis.setLettreSoumission(UIConstants.PARENT);
			else
				plis.setLettreSoumission(UIConstants.NPARENT);
			
			if(rdoffoui.isChecked()==true)
				plis.setOffreFinanciere(UIConstants.PARENT);
			else
				plis.setOffreFinanciere(UIConstants.NPARENT);
			
			if(rdoftoui.isChecked()==true)
				plis.setOffreTechnique(UIConstants.PARENT);
			else
				plis.setOffreTechnique(UIConstants.NPARENT);
			
			
			BeanLocator.defaultLookup(RegistrededepotSession.class).update(plis);
			
			session.setAttribute("libelle", "lecturesoffres");
			loadApplicationState("procedure_pi");
		}
	}
private boolean checkFieldConstraints() {
		
		try {


			if(txtPays.getValue().equals(""))
		     {
             errorComponent = txtPays;
             errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.devise.Pays")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		
			return true;
			
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}


		

}