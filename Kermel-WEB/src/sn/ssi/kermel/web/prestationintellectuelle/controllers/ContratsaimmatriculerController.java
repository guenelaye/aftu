package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.passationsmarches.ejb.ContratsSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class ContratsaimmatriculerController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CONFIRMVALIDER="CONFIRMVALIDER";
     String page=null;
     Session session = getHttpSession();
    SygContrats contrat=new SygContrats();
     String login;
    private Listbox lstListe;
     private Combobox cbselect;
 	private int etat=-1,gestion=-1;
	private Intbox intgestion;
	private SygAppelsOffres appel;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
		
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		
		lstListe.setItemRenderer(this);
		
			
	}


	public void onCreate(CreateEvent event) {
		
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygContrats> contrats = BeanLocator.defaultLookup(ContratsSession.class).find(activePage,byPage, null, null, null, null, null, 0);
			 SimpleListModel listModel = new SimpleListModel(contrats);
			 lstListe.setModel(listModel);
			 pgPagination.setTotalSize(BeanLocator.defaultLookup( ContratsSession.class).count(null, null, null, null, null, 0));
		} 
		else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
			Listcell button = (Listcell) event.getTarget();
			String toDo = (String) button.getAttribute(UIConstants.TODO);
			contrat=(SygContrats) button.getAttribute("contrats");
			if (toDo.equalsIgnoreCase("valider"))
			{
			final String uri = "/passationsmarches/prestationintellectuelle/formimmatriculation.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.marche"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ImmatriculationFormController.PARAM_WINDOW_CODE, contrat.getConID());

			showPopupWindow(uri, data, display);
			}
			else
			{
				if (toDo.equalsIgnoreCase("historique"))
				{
					appel=contrat.getDossier().getAppel();
					session.setAttribute("idappel", appel.getApoid());
					Executions.getCurrent().setAttribute("origine", "demandeimmatriculation");
					if(appel.getTypemarche().getId().intValue()==UIConstants.PARAM_TMPI)
					{
						session.setAttribute("libelle", "traitementsdossiers");
						session.setAttribute("LibelleTab", "REALPI");
						if(appel.getModepassation().getId().intValue()==42)
						loadApplicationState("procedure_pi");
						if(appel.getModepassation().getId().intValue()==34)
							loadApplicationState("suivi_pi_drp");
						if(appel.getModepassation().getId().intValue()==5)
							loadApplicationState("entente_directe");
					}
				}
			}
		}

	}

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygContrats contrats = (SygContrats) data;
		item.setValue(contrats.getConID());
		
		Listcell cellNumero = new Listcell(Long.toString(contrats.getConID()));
		cellNumero.setParent(item);
		
		Listcell cellAutorite = new Listcell(contrats.getDossier().getAutorite().getDenomination());
		cellAutorite.setParent(item);
		 
		Listcell cellReference = new Listcell(contrats.getDossier().getAppel().getAporeference());
		cellReference.setParent(item);
		 
		Listcell cellObjet = new Listcell(contrats.getDossier().getAppel().getApoobjet());
		cellObjet.setParent(item);
		 
		Listcell cellModepassation = new Listcell(contrats.getDossier().getAppel().getModepassation().getLibelle());
		cellModepassation.setParent(item);
		 
		
		Listcell cellMontant = new Listcell(ToolKermel.format2Decimal(contrats.getDossier().getAppel().getApomontantestime()));
		cellMontant.setParent(item);

	
		Listcell cellImage = new Listcell();
		
		cellImage.setImage("/images/ok.png");
		cellImage.setAttribute(UIConstants.TODO, "valider");
		cellImage.setAttribute("contrats", contrats);
		cellImage.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validerdossier"));
		cellImage.addEventListener(Events.ON_CLICK, ContratsaimmatriculerController.this);
		cellImage.setParent(item);
		
		Listcell cellImageHistorique = new Listcell();
		cellImageHistorique.setImage("/images/zoom.png");
		cellImageHistorique.setAttribute(UIConstants.TODO, "historique");
		cellImageHistorique.setAttribute("dossiers", contrats.getDossier());
		cellImageHistorique.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.vuedossier"));
		cellImageHistorique.addEventListener(Events.ON_CLICK, ContratsaimmatriculerController.this);
		cellImageHistorique.setParent(item);

	}
	

	
	public void  onClick$btnchercher(){
		
		
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
	

		public void onOK$txtReference(){
			onClick$btnchercher();	
		}
		public void onOK$bchercher(){
			onClick$btnchercher();	
		}
}