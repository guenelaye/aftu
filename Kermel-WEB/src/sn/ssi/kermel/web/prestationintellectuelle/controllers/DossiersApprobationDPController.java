package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class DossiersApprobationDPController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code,avalider="oui";
	private Tab TAB_infosgenerales,TAB_garanties,TAB_piecesadministratives,TAB_criteresqualifications,TAB_devise,TAB_financement;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblInfos;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygDossiers dossier=new SygDossiers();

	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,2);
		if(dossier!=null)
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	
	}

	@Override
	public void onEvent(Event event) throws Exception {
	
	}

	public void onClick$signaturemarche() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				
		}
		else
		{
			 if(dossier.getDosDatePublicationProvisoire()==null)
			 {
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.publierattrribution"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			 }
			 else
			 {
		       session.setAttribute("libelle", "souscriptiondumarche");
		       loadApplicationState("procedure_pi");
			 }
		}
	}
	
	public void onClick$approbationmarche() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				
		}
		else
		{
			 if(dossier.getDosDateSignature()==null)
			 {
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.souscriredossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			 }
			 else
			 {
		       session.setAttribute("libelle", "approbationmarche");
		       loadApplicationState("procedure_pi");
			 }
		}
	}
	public void onClick$notificationmarche() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				
		}
		else
		{
			 if(dossier.getDosDateApprobation()==null)
			 {
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.approuverdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			 }
			 else
			 {
		       session.setAttribute("libelle", "notificationmarche");
		       loadApplicationState("procedure_pi");
			 }
		}
		
	}
	public void onClick$pubattribdefinitive() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				
		}
		else
		{
			 if(dossier.getDosDateNotification()==null)
			 {
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.notifierdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			 }
			 else
			 {
		        session.setAttribute("libelle", "publicationattributiondefinitive");
		        loadApplicationState("procedure_pi");
			 }
		}
	}

}