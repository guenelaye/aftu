package sn.ssi.kermel.web.paiement.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygPaiement;
import sn.ssi.kermel.be.passationsmarches.ejb.ContratsSession;
import sn.ssi.kermel.be.referentiel.ejb.FournisseurSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;

//@SuppressWarnings("serial")
public class EditContratFormController extends AbstractWindow implements AfterCompose, EventListener, ListitemRenderer {

	private static final long serialVersionUID = 3449680167389488155L;
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String WINDOW_PARAM_MODE = "MODE";

	private String mode;
	private Long code;
	
	List<SygPaiement> paiements = new ArrayList<SygPaiement>();
   

	
	
    //Fournisseur
	private SygFournisseur fournisseur = new SygFournisseur();
	
	//Contrat
	//sprivate SygContrats contrat= new SygContrats();
	private Listbox lstContrat;
	private Paging pgContrat;
	
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	

	
	
	private Div step1;
	
	
	
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE, activePage;

	
	
	//Information generale

	private Label lblnom;

   
	
	

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		
		Components.addForwards(this, this);

		//Contrat
		addEventListener(ApplicationEvents.ON_CONTRAT, this);
		lstContrat.setItemRenderer(new ContratRenderer());
		pgContrat.setPageSize(byPageBandbox);
		pgContrat.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_CONTRAT);
		

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	}

	@Override
	public void onEvent(Event event) throws Exception {

		//Contrat
		 if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CONTRAT)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgContrat.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_LIST_BY_PAGES;
				activePage = pgContrat.getActivePage() * byPage;
				pgContrat.setPageSize(byPage);
			}
			List<SygContrats> contrat = BeanLocator.defaultLookup(ContratsSession.class).find(activePage, byPage, null,null,null,UIConstants.CONTRAT_NPAYE,fournisseur);
			lstContrat.setModel(new SimpleListModel(contrat));
			pgContrat.setTotalSize(BeanLocator.defaultLookup(ContratsSession.class).count(null,null,null,UIConstants.CONTRAT_NPAYE,fournisseur));
		}


		else if (event.getName().equalsIgnoreCase(Events.ON_CLICK)) {
			String libelleimage = (String) (((Listcell) event.getTarget()).getAttribute("libelleimage"));
			if (libelleimage == "supconteneur") {
				HashMap<String, String> display = new HashMap<String, String>();
				display.put(MessageBoxController.DSP_MESSAGE, " Etes vous sur de vouloir supprimer?");
				display.put(MessageBoxController.DSP_TITLE, "Supprimer le dossier");
				display.put(MessageBoxController.DSP_HEIGHT, "150px");
				display.put(MessageBoxController.DSP_WIDTH, "100px");
				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				showMessageBox(display, map);
			}
		}
	}

	

	// /////////////////
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent createEvent) {
		Map<String, Object> windowParams = (Map<String, Object>) createEvent.getArg();
		mode = (String) windowParams.get(WINDOW_PARAM_MODE);
		if (mode == null) {
			
		} else {
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				code = (Long) windowParams.get(PARAM_WINDOW_CODE);
				fournisseur = BeanLocator.defaultLookup(FournisseurSession.class).findById(code);
				lblnom.setValue(fournisseur.getNom());
				step1.setVisible(true);
				//step2.setVisible(false);
				
				Events.postEvent(ApplicationEvents.ON_CONTRAT, this, null);
			}
		}

	}


	
	//Step1**********************************************************************************
	
	public void onClick$menuCancelStep1() {
		loadApplicationState("suivi_paiement");
		detach();
	}
	

	//Contrat

	public class ContratRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygContrats contrat = (SygContrats) data;
			item.setValue(contrat);

			Listcell cellReference = new Listcell(contrat.getDossier().getDosReference());
			cellReference.setParent(item);
			
			Listcell cellDate = new Listcell("");
			if(contrat.getConDateRecepDefinitive()!=null)
				cellDate.setLabel(UtilVue.getInstance().formateLaDate(contrat.getConDateRecepDefinitive()));
			cellDate.setParent(item);

		
			Listcell cellAC = new Listcell(contrat.getAutorite().getDenomination());
			cellAC.setParent(item);
			
			Listcell cellMontant = new Listcell(ToolKermel.format2Decimal(contrat.getMontant()));
			cellMontant.setParent(item);
			
			Listcell cellMontantVersement = new Listcell("");
			if(contrat.getMontantverse()!=null)
				cellMontantVersement.setLabel(ToolKermel.format2Decimal(contrat.getMontantverse()));
			cellMontantVersement.setParent(item);
			


		}
	}




	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		

	}
	
	

}