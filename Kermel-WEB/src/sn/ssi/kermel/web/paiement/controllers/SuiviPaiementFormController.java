package sn.ssi.kermel.web.paiement.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygBanque;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygOperationPaiement;
import sn.ssi.kermel.be.entity.SygPaiement;
import sn.ssi.kermel.be.paiement.ejb.OperationPaiementSession;
import sn.ssi.kermel.be.paiement.ejb.PaiementSession;
import sn.ssi.kermel.be.passationsmarches.ejb.ContratsSession;
import sn.ssi.kermel.be.referentiel.ejb.BanqueSession;
import sn.ssi.kermel.be.referentiel.ejb.FournisseurSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;

//@SuppressWarnings("serial")
public class SuiviPaiementFormController extends AbstractWindow implements AfterCompose, EventListener, ListitemRenderer {

	private static final long serialVersionUID = 3449680167389488155L;
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String WINDOW_PARAM_MODE = "MODE";

	private String mode;
	private Long code;
	private SygPaiement paiement = new SygPaiement();
	List<SygPaiement> paiements = new ArrayList<SygPaiement>();
    private int nbre;
	private Label lbStatusBar;

	private Radiogroup radioMpaiement;
	private Radio rdcheque,rdautre;
	
    //Fournisseur
	private SygFournisseur fournisseur = new SygFournisseur();
	
	//Contrat
	private SygContrats contrat= new SygContrats();
	private Listbox lstContrat;
	private Paging pgContrat;
	
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	
	private String montantenlettre;
	
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Div step1, step2,step3;
	
	
	private Textbox txtNumCheque,txtAutres,txtPayerpar;
	private Date datejour;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE, activePage;

	
	
	//Information generale
	private BigDecimal montanttotal=null, montantversetotal=null;
	private Label lblNum, lblNbrContrat, lblMontant,lblMtAverse;

    //Editer cheque
	protected Iframe iframeEs2Detached;
	protected static final String EDIT_URL = "http://" + BeConstants.IP_SYGMAP + ":" + BeConstants.PORT_SYGMAP + "/birt/frameset?__report=/etataxeparafixcale/";
	private SygOperationPaiement operationpaiement = new SygOperationPaiement();
	private String numrecu;
	 private String format;
	 private String nomFichier;
	
	// /Banque 
	private SygBanque banque = new SygBanque();
	private Textbox txtRechercherBanque;
	private Bandbox bdBanque;
	private Paging pgBanque;
	private Listbox lstBanque;
	private String LibelleBanque = null;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		
		Components.addForwards(this, this);

		// champ Banque  du formulaire
		addEventListener(ApplicationEvents.ON_BANQUE, this);
		lstBanque.setItemRenderer(new banqueRenderer());
		pgBanque.setPageSize(byPageBandbox);
		pgBanque.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_BANQUE);
		Events.postEvent(ApplicationEvents.ON_BANQUE, this, null);

		//Contrat
		addEventListener(ApplicationEvents.ON_CONTRAT, this);
		lstContrat.setItemRenderer(new ContratRenderer());
		pgContrat.setPageSize(byPageBandbox);
		pgContrat.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_CONTRAT);
		

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	}

	@Override
	public void onEvent(Event event) throws Exception {

		//Contrat
		 if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CONTRAT)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgContrat.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_LIST_BY_PAGES;
				activePage = pgContrat.getActivePage() * byPage;
				pgContrat.setPageSize(byPage);
			}
			List<SygContrats> contrat = BeanLocator.defaultLookup(ContratsSession.class).find(activePage, byPage, null,null,null,UIConstants.CONTRAT_NPAYE,fournisseur);
			lstContrat.setModel(new SimpleListModel(contrat));
			pgContrat.setTotalSize(BeanLocator.defaultLookup(ContratsSession.class).count(null,null,null,UIConstants.CONTRAT_NPAYE,fournisseur));
		}
		 else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_BANQUE)) {
				List<SygBanque> banques = BeanLocator.defaultLookup(BanqueSession.class).find(pgBanque.getActivePage() * byPageBandbox, byPageBandbox, null, LibelleBanque);
				SimpleListModel listModel = new SimpleListModel(banques);
				lstBanque.setModel(listModel);
				pgBanque.setTotalSize(BeanLocator.defaultLookup(BanqueSession.class).count(null, LibelleBanque));
			}

		else if (event.getName().equalsIgnoreCase(Events.ON_CLICK)) {
			String libelleimage = (String) (((Listcell) event.getTarget()).getAttribute("libelleimage"));
			if (libelleimage == "supconteneur") {
				HashMap<String, String> display = new HashMap<String, String>();
				display.put(MessageBoxController.DSP_MESSAGE, " Etes vous sur de vouloir supprimer?");
				display.put(MessageBoxController.DSP_TITLE, "Supprimer le dossier");
				display.put(MessageBoxController.DSP_HEIGHT, "150px");
				display.put(MessageBoxController.DSP_WIDTH, "100px");
				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				showMessageBox(display, map);
			}
		}
	}

	

	// /////////////////
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent createEvent) {
		Map<String, Object> windowParams = (Map<String, Object>) createEvent.getArg();
		mode = (String) windowParams.get(WINDOW_PARAM_MODE);
		if (mode == null) {
			
		} else {
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				code = (Long) windowParams.get(PARAM_WINDOW_CODE);
				fournisseur = BeanLocator.defaultLookup(FournisseurSession.class).findById(code);
				
				step1.setVisible(true);
				step2.setVisible(false);
				
				Events.postEvent(ApplicationEvents.ON_CONTRAT, this, null);
			}
		}

	}

	private boolean checkFieldConstraints() {

		try {
			
			 if(txtPayerpar.getValue().equals(""))
		       {
                errorComponent = txtPayerpar;
                errorMsg = Labels.getLabel("kermel.paiement.payepar")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		        }
			if(rdcheque.isChecked()==true)
			{
			  if(txtNumCheque.getValue().equals(""))
		       {
                errorComponent = txtNumCheque;
                errorMsg = Labels.getLabel("kermel.paiement.numcheque")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		        }
			  
			  if(bdBanque.getValue().equals(""))
		       {
             errorComponent = bdBanque;
             errorMsg = Labels.getLabel("kermel.paiement.banque")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		        }
			 }
			
			if(rdautre.isChecked()==true)
			{
			   if(txtAutres.getValue().equals(""))
		       {
                errorComponent = txtAutres;
                errorMsg = Labels.getLabel("kermel.paiement.commentaireautre")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		      }
			}
			
			return true;

		} catch (Exception e) {
			errorMsg = Labels.getLabel("cciad.erreur.erreurinconnue") + ": " + e.toString() + " [checkFieldConstraints]";
			errorComponent = null;
			return false;

		}

	}

	
	//Step1**********************************************************************************
	
	public void onClick$menuCancelStep1() {
		loadApplicationState("suivi_paiement");
		detach();
	}
	
	public void onClick$menuNextStep1() {
		 if(lstContrat.getSelectedItem() == null){
			throw new WrongValueException(lstContrat, Labels.getLabel("cciad.error.select.item"));
		} else {
		
			contrat=(SygContrats) lstContrat.getSelectedItem().getValue();
			
			lblNum.setValue(contrat.getDossier().getDosReference());
			nbre=lstContrat.getSelectedCount();
			lblNbrContrat.setValue(Integer.toString(nbre));
			 
			 for (int i = 0; i < lstContrat.getSelectedCount(); i++) {
				 SygContrats contrats = (SygContrats) ((Listitem) lstContrat.getSelectedItems().toArray()[i]).getValue();
				
				 if((montanttotal!=null)&&(montantversetotal!=null)){
				         montanttotal=montanttotal.add(contrats.getMontant());
				         montantversetotal=montantversetotal.add(contrats.getMontantverse());
			        }
				   else{
					   montanttotal =contrats.getMontant();
					   montantversetotal=contrats.getMontantverse();
				   }
				 }
			lblMontant.setValue(ToolKermel.format3Decimal(montanttotal));
			lblMtAverse.setValue(ToolKermel.format3Decimal(montantversetotal));
			txtPayerpar.setValue(fournisseur.getNom());
			
			step1.setVisible(false);
			step2.setVisible(true);
			
			
			
		}
	}
	
	
	
//Step2*****************************************************************
	public void onClick$menuNextStep2() {
		if (checkFieldConstraints()) {
			  // numrecu=BeanLocator.defaultLookup(OperationPaiementSession.class).getGeneratedCode(BeConstants.PARAM_NUMRECU);
			 numrecu="F"+fournisseur.getId()+BeanLocator.defaultLookup(OperationPaiementSession.class).getGeneratedCode(BeConstants.PARAM_NUMRECU);
			   for (int i = 0; i < lstContrat.getSelectedCount(); i++) {
					SygContrats contrats = (SygContrats)((Listitem) lstContrat.getSelectedItems().toArray()[i]).getValue();
					 paiement.setContrats(contrats);
					 
					 contrats.setConstatus(UIConstants.CONTRAT_PAYE);
					 datejour = new Date();
					 contrats.setCondatepaiement(datejour);
					 
					 if (((String) radioMpaiement.getSelectedItem().getValue()).equalsIgnoreCase(Labels.getLabel("kermel.paiement.modepaiement.cheque"))) {
							paiement.setModepaiement(Labels.getLabel("kermel.paiement.modepaiement.cheque"));
							
						} else if (((String) radioMpaiement.getSelectedItem().getValue()).equalsIgnoreCase(Labels.getLabel("kermel.paiement.modepaiement.espece"))) {

							paiement.setModepaiement(Labels.getLabel("kermel.paiement.modepaiement.espece"));
							
						}else {

							paiement.setModepaiement(Labels.getLabel("kermel.paiement.modepaiement.autre"));
							
						}
					 
							banque = (SygBanque) lstBanque.getSelectedItem().getValue();
							
						    paiement.setNumCheque(txtNumCheque.getValue());
						    paiement.setBanque(banque);
						    paiement.setFournisseur(fournisseur);
						    paiement.setCommentaire(txtAutres.getValue());
						    paiement.setOperation(numrecu); 
					 BeanLocator.defaultLookup(ContratsSession.class).update(contrats);
					 BeanLocator.defaultLookup(PaiementSession.class).save(paiement);
					 
					}
			  
			 //Operation
			    operationpaiement.setOpemontanttotal(montanttotal);
			    operationpaiement.setOpmontantTotalVerse(montantversetotal);
			       montantenlettre=ToolKermel.conversionChiffreLettre(montantversetotal);
			      operationpaiement.setOpmontantTotalVerseEnLettre(montantenlettre);
			    operationpaiement.setOpedaterecu(new Date());
			     operationpaiement.setOpecode(numrecu);
			    operationpaiement.setOperecude(txtPayerpar.getValue());
			    operationpaiement.setOpecommentaires(txtAutres.getValue());
			    operationpaiement.setFournisseur(fournisseur);
			    BeanLocator.defaultLookup(OperationPaiementSession.class).save(operationpaiement);
			   
			    nomFichier = "recupaiement.rptdesign";
                format = UIConstants.FORMAT_PDF;
                System.out.println(EDIT_URL + nomFichier + "&numrecu=" + numrecu + "&__format=" + format );
                iframeEs2Detached.setSrc(EDIT_URL + nomFichier + "&numrecu=" + numrecu + "&__format=" + format );
			//loadApplicationState("suivi_paiement");
			//detach();
			    step1.setVisible(false);
				step2.setVisible(false);
				step3.setVisible(true);
			
		}
	}

	
	public void onClick$menuCancelImprimerEs(){
		loadApplicationState("suivi_paiement");
		detach();
      }
	// ///////////////////////////////////////////////////////////////////

	
	
	//Contrat

	public class ContratRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygContrats contrat = (SygContrats) data;
			item.setValue(contrat);

			Listcell cellReference = new Listcell(contrat.getDossier().getDosReference());
			cellReference.setParent(item);
			
			Listcell cellDate = new Listcell("");
			if(contrat.getConDateRecepDefinitive()!=null)
				cellDate.setLabel(UtilVue.getInstance().formateLaDate(contrat.getConDateRecepDefinitive()));
			cellDate.setParent(item);

			Listcell cellObjet = new Listcell(contrat.getDossier().getDosObjetTextAvisAttributiondefinitif());
			cellObjet.setParent(item);

			Listcell cellAC = new Listcell(contrat.getAutorite().getDenomination());
			cellAC.setParent(item);
			
			Listcell cellMontant = new Listcell(contrat.getMontant().toString());
			cellMontant.setParent(item);
			


		}
	}


	public void onClick$menuCancelStep2() {
		
		loadApplicationState("suivi_paiement");
		detach();
	}


	
	public void onClick$menuPreviousStep2() {
		montanttotal=null; montantversetotal=null;
		step1.setVisible(true);
		step2.setVisible(false);
		
		
	}

	// //////////////////////////////////////////////////

	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		

	}
	
	
	public void onCheck$radioMpaiement(){
		
		if(radioMpaiement.getSelectedItem().getValue().equals(Labels.getLabel("kermel.paiement.modepaiement.cheque"))){
			
			txtNumCheque.setDisabled(false);
			bdBanque.setDisabled(false);
			txtAutres.setDisabled(true);
			
		}else if(radioMpaiement.getSelectedItem().getValue().equals(Labels.getLabel("kermel.paiement.modepaiement.espece"))){
			
			txtNumCheque.setDisabled(true);
			bdBanque.setDisabled(true);
			txtAutres.setDisabled(true);
			
		}if(radioMpaiement.getSelectedItem().getValue().equals(Labels.getLabel("kermel.paiement.modepaiement.autre"))){
			
			txtNumCheque.setDisabled(true);
			bdBanque.setDisabled(true);
			txtAutres.setDisabled(false);
		}
	}

	
	// //////////Banque********************
	public void onSelect$lstBanque() {
		bdBanque.setValue((String) lstBanque.getSelectedItem().getAttribute(UIConstants.ATTRIBUTE_LIBELLE));
		bdBanque.close();
	}

	public class banqueRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygBanque banque = (SygBanque) data;
			item.setValue(banque);
			item.setAttribute(UIConstants.ATTRIBUTE_LIBELLE, banque.getLibelle());

			Listcell cellLibelle = new Listcell("");
			if (banque.getLibelle() != null) {
				cellLibelle.setLabel(banque.getLibelle());
			}
			cellLibelle.setParent(item);
			//				

		}
	}

	public void onFocus$txtRechercherBanque() {
		if (txtRechercherBanque.getValue().equalsIgnoreCase(Labels.getLabel("kermel.paiement.banque"))) {
			txtRechercherBanque.setValue("");
		}
	}

	public void onBlur$txtRechercherDecision() {
		if (txtRechercherBanque.getValue().equalsIgnoreCase("")) {
			txtRechercherBanque.setValue(Labels.getLabel("kermel.paiement.banque"));
		}
	}

	public void onClick$btnRechercherDecision() {
		if (txtRechercherBanque.getValue().equalsIgnoreCase(Labels.getLabel("kermel.paiement.banque")) || txtRechercherBanque.getValue().equals("")) {
			LibelleBanque = null;
		} else {
			LibelleBanque = txtRechercherBanque.getValue();
		}
		Events.postEvent(ApplicationEvents.ON_BANQUE, this, null);
	}

	// ////////////////fin ////////////////////////////////
}