package sn.ssi.kermel.web.paiement.controllers;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.ContratsSession;
import sn.ssi.kermel.be.referentiel.ejb.FournisseurSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class ListFournisseurPaiementController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtNom;
    String nom=null, page=null;
    private Listheader lshNom;
    String login;
    private SygAutoriteContractante autorite = null;
    
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem WADD_PAIEMENT,EDIT_CONTRAT;
   
    
//    private SygCatFournisseur categorie = null;
//    private Bandbox bdCategorie;
//	private Paging pgCategorie;
//	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
//	private Listbox lstCategorie;
//	private Textbox txtRechercherCategorie;
//	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
	//private String categorielibelle = null;
	private Utilisateur infoscompte;
	
	private BigDecimal montanttotalfour=null;
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_SUIVIPAIEMENT);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (WADD_PAIEMENT != null) { WADD_PAIEMENT.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (EDIT_CONTRAT != null) { EDIT_CONTRAT.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		
		
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		
		
		//recherche categorie
//		lstCategorie.setItemRenderer(new CategoriesRenderer());
//   		pgCategorie.setPageSize(byPageBandbox);
//   		pgCategorie.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTRE_ACTION);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lshNom.setSortAscending(new FieldComparator("nom", false));
		lshNom.setSortDescending(new FieldComparator("nom", true));
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
    	
    	login = ((String) getHttpSession().getAttribute("user"));
	}

	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);
	}
	
	
	@Override
	public void onEvent(final Event event) throws Exception {
//		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)){
//			if (event.getData() != null) {
//				activePage = Integer.parseInt((String) event.getData());
//				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
//				
//				pgCategorie.setPageSize(byPageBandBox);
//			}
//			else {
//				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
//				
//				activePage = pgCategorie.getActivePage() * byPageBandBox;
//				pgCategorie.setPageSize(byPageBandBox);
//			}
//			List<SygCatFournisseur> categorie = BeanLocator.defaultLookup(CatFourSession.class).find(activePage, byPageBandBox, null);
//			lstCategorie.setModel(new SimpleListModel(categorie));
//			pgCategorie.setTotalSize(BeanLocator.defaultLookup(CatFourSession.class).count(null));
//		}
		 if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygFournisseur> fournisseurs = BeanLocator.defaultLookup(FournisseurSession.class).findPaiement(activePage,byPage,UIConstants.CONTRAT_NPAYE,nom);
			 SimpleListModel listModel = new SimpleListModel(fournisseurs);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup( FournisseurSession.class).countPaiement(UIConstants.CONTRAT_NPAYE,nom));
		} 

		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			if (lstListe.getSelectedItem() == null)
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			
			final String uri = "/paiement/formsuivipaiement.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.paiement"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(SuiviPaiementFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(SuiviPaiementFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				
				HashMap<String, String> display = new HashMap<String, String>();
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); 
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(FournisseurSession.class).delete(codes);
				BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_FOURNISSEUR", Labels.getLabel("kermel.referentiel.common.fournisseur.suppression")+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
		 
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
				if (lstListe.getSelectedItem() == null)
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				
				final String uri = "/paiement/editcontrat.zul";

				final HashMap<String, String> display = new HashMap<String, String>();
				display.put(DSP_HEIGHT,"500px");
				display.put(DSP_WIDTH, "80%");
				display.put(DSP_TITLE, Labels.getLabel("kermel.paiement.visualiser.contrat.titre"));

				final HashMap<String, Object> data = new HashMap<String, Object>();
				data.put(EditContratFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
				data.put(EditContratFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

				showPopupWindow(uri, data, display);
			} 
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygFournisseur fourn = (SygFournisseur) data;
		item.setValue(fourn.getId());

		Listcell cellNom = new Listcell(fourn.getNom());
		cellNom.setParent(item);
		
		Listcell cellAdr = new Listcell(fourn.getAdresse());
		cellAdr.setParent(item);
		
		Listcell cellTel = new Listcell(fourn.getTel());
		cellTel.setParent(item);
		
		List<SygContrats> contratfour = BeanLocator.defaultLookup(ContratsSession.class).find(0, -1, null,null,null,UIConstants.CONTRAT_NPAYE,fourn);
		montanttotalfour=new BigDecimal(0);
		 for (int i = 0; i < contratfour.size(); i++) {
			 montanttotalfour=montanttotalfour.add(contratfour.get(i).getMontant());
			
		 }
		Listcell cellMontant = new Listcell(ToolKermel.format2Decimal(montanttotalfour));
		cellMontant.setParent(item);
	}
	
	///////////Categorie///////// 
//	public void onSelect$lstCategorie(){
//		categorie= (SygCatFournisseur) lstCategorie.getSelectedItem().getValue();
//		bdCategorie.setValue(categorie.getLibelle());
//		bdCategorie.close();
//	}
//	
//	public class CategoriesRenderer implements ListitemRenderer{
//		@Override
//		public void render(Listitem item, Object data, int index)  throws Exception {
//			SygCatFournisseur categorie = (SygCatFournisseur) data;
//			item.setValue(categorie);
//			
//			Listcell cellLibelle = new Listcell("");
//			if (categorie.getLibelle()!=null){
//				cellLibelle.setLabel(categorie.getLibelle());
//			}
//			cellLibelle.setParent(item);
//	
//		}
//	}
	
//	public void onFocus$txtRechercherCategorie(){
//		if(txtRechercherCategorie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.code.categorie"))){
//			txtRechercherCategorie.setValue("");
//		}		 
//	}
	
//	public void  onClick$btnRechercherCategorie(){
//		if(txtRechercherCategorie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.code.categorie")) || txtRechercherCategorie.getValue().equals("")){
//			categorielibelle = null;
//			page=null;
//		}
//		else{
//			categorielibelle = txtRechercherCategorie.getValue();
//			page="0";
//		}
//		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
//	}
	
	public void onClick$bchercher()
	{
		//categorielibelle = bdCategorie.getValue();
		nom = txtNom.getValue();
//		if(categorielibelle.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.code.categorie")) || "".equals(categorielibelle))
//			categorie = null;
		
		if(nom.equalsIgnoreCase(Labels.getLabel("kermel.paiement.form.nomraisonsocial")) || "".equals(nom))
			nom = null;
		
		//bdCategorie.setValue(Labels.getLabel("kermel.referentiel.common.code.categorie"));
		txtNom.setValue(Labels.getLabel("kermel.paiement.form.nomraisonsocial"));
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	
	public void onBlur$txtNom()
	{
		if(txtNom.getValue().equals(""))
			txtNom.setValue(Labels.getLabel("kermel.paiement.form.nomraisonsocial"));
	}
	public void onFocus$txtNom()
	{
		if(txtNom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.paiement.form.nomraisonsocial")))
			txtNom.setValue("");
	
	}
	public void onOK$txtNom()
	{
		onClick$bchercher();
	}
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
	
}
