package sn.ssi.kermel.web.paiement.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygBanque;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygOperationPaiement;
import sn.ssi.kermel.be.entity.SygPaiement;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.paiement.ejb.OperationPaiementSession;
import sn.ssi.kermel.be.paiement.ejb.PaiementSession;
import sn.ssi.kermel.be.passationsmarches.ejb.ContratsSession;
import sn.ssi.kermel.be.referentiel.ejb.BanqueSession;
import sn.ssi.kermel.be.referentiel.ejb.FournisseurSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;

//@SuppressWarnings("serial")
public class PaiementFormController extends AbstractWindow implements AfterCompose, EventListener, ListitemRenderer {

	private static final long serialVersionUID = 3449680167389488155L;
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String WINDOW_PARAM_MODE = "MODE";

	private String mode;
	private Long code;
	
	List<SygPaiement> paiements = new ArrayList<SygPaiement>();
    private int nbre;
	private Label lbStatusBar;
	//private Textbox txtNumero;
	private Radiogroup radioMpaiement;
	private Radio rdcheque,rdespece,rdautre;
	  private Listheader lshNom;
	
    //Fournisseur
	private SygFournisseur fournisseur = new SygFournisseur();
	private Listbox lstFour;
	private Paging pgFour;
	//private Bandbox bdFour;
	
	//Recher Ac
	public Textbox txtDenomination;
	
	//Recher fournisseur
	public Textbox txtLibelle;
	private String nom=null;
	
	
	//Contrat
	private SygContrats contrat= new SygContrats();
	private Listbox lstContrat;
	private Paging pgContrat;
	
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Div step0, step1, step2,step3;
	
	
	private Textbox txtNumCheque,txtAutres,txtPayerpar;
	
	
	private Date datejour;

	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE, activePage;

	
	
	//Information generale
	private BigDecimal montanttotal=new BigDecimal(0), montantversetotal=new BigDecimal(0),montanttotalfour=new BigDecimal(0);
	private Label lblNum, lblNbrContrat, lblMontant,lblMtAverse,lblnom;



	 //Editer cheque
	protected Iframe iframeEs2Detached;
	protected static final String EDIT_URL = "http://" + BeConstants.IP_SYGMAP + ":" + BeConstants.PORT_SYGMAP + "/birt/frameset?__report=/etataxeparafixcale/";
	private SygOperationPaiement operationpaiement = new SygOperationPaiement();
	private String numrecu;
	 private String format;
	 private String nomFichier;
	
	// /Banque 
	private SygBanque banque = new SygBanque();
	private Textbox txtRechercherBanque;
	private Bandbox bdBanque;
	private Paging pgBanque;
	private Listbox lstBanque;
	private String LibelleBanque = null;
	private SygTypeAutoriteContractante type;
	
    private String montantenlettre;
    private SygPaiement paiement = new SygPaiement();
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		
		Components.addForwards(this, this);
		
		lshNom.setSortAscending(new FieldComparator("nom", false));
		lshNom.setSortDescending(new FieldComparator("nom", true));
		
		// champ Banque  du formulaire
		addEventListener(ApplicationEvents.ON_BANQUE, this);
		lstBanque.setItemRenderer(new banqueRenderer());
		pgBanque.setPageSize(byPageBandbox);
		pgBanque.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_BANQUE);
		Events.postEvent(ApplicationEvents.ON_BANQUE, this, null);
   
		//AC
		addEventListener(ApplicationEvents.ON_FOURNISSEUR, this);
		lstFour.setItemRenderer(new FournisseurRenderer());
		pgFour.setPageSize(byPageBandbox);
		pgFour.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_FOURNISSEUR);
		Events.postEvent(ApplicationEvents.ON_FOURNISSEUR, this, null);
		
		//Contrat
		addEventListener(ApplicationEvents.ON_CONTRAT, this);
		lstContrat.setItemRenderer(new ContratRenderer());
		pgContrat.setPageSize(byPageBandbox);
		pgContrat.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_CONTRAT);
		

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_FOURNISSEUR)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgFour.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_LIST_BY_PAGES;
				activePage = pgFour.getActivePage() * byPage;
				pgFour.setPageSize(byPage);
			}
			List<SygFournisseur> fournisseur = BeanLocator.defaultLookup(FournisseurSession.class).findPaiement(activePage,byPage,UIConstants.CONTRAT_NPAYE,nom);
			lstFour.setModel(new SimpleListModel(fournisseur));
			pgFour.setTotalSize(BeanLocator.defaultLookup(FournisseurSession.class).countPaiement(UIConstants.CONTRAT_NPAYE,nom));
		}
		//Contrat
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CONTRAT)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgContrat.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_LIST_BY_PAGES;
				activePage = pgContrat.getActivePage() * byPage;
				pgContrat.setPageSize(byPage);
			}
			List<SygContrats> contrat = BeanLocator.defaultLookup(ContratsSession.class).find(activePage, byPage, null,null,null,UIConstants.CONTRAT_NPAYE,fournisseur);
			lstContrat.setModel(new SimpleListModel(contrat));
			pgContrat.setTotalSize(BeanLocator.defaultLookup(ContratsSession.class).count(null,null,null,UIConstants.CONTRAT_NPAYE,fournisseur));
		}
		
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_BANQUE)) {
			List<SygBanque> banques = BeanLocator.defaultLookup(BanqueSession.class).find(pgBanque.getActivePage() * byPageBandbox, byPageBandbox, null, LibelleBanque);
			SimpleListModel listModel = new SimpleListModel(banques);
			lstBanque.setModel(listModel);
			pgBanque.setTotalSize(BeanLocator.defaultLookup(BanqueSession.class).count(null, LibelleBanque));
		}

		else if (event.getName().equalsIgnoreCase(Events.ON_CLICK)) {
			String libelleimage = (String) (((Listcell) event.getTarget()).getAttribute("libelleimage"));
			if (libelleimage == "supconteneur") {
				HashMap<String, String> display = new HashMap<String, String>();
				display.put(MessageBoxController.DSP_MESSAGE, " Etes vous sur de vouloir supprimer?");
				display.put(MessageBoxController.DSP_TITLE, "Supprimer le dossier");
				display.put(MessageBoxController.DSP_HEIGHT, "150px");
				display.put(MessageBoxController.DSP_WIDTH, "100px");
				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				showMessageBox(display, map);
			}
		}
	}



	// /////////////////
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent createEvent) {
		Map<String, Object> windowParams = (Map<String, Object>) createEvent.getArg();
		mode = (String) windowParams.get(WINDOW_PARAM_MODE);
		if (mode == null) {
			
		} else {
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				code = (Long) windowParams.get(PARAM_WINDOW_CODE);
				paiement = BeanLocator.defaultLookup(PaiementSession.class).findById(code);
				paiements = BeanLocator.defaultLookup(PaiementSession.class).find(0, -1, null, null);
				
				txtNumCheque.setValue(paiement.getNumCheque());
				//txtBanque.setValue(paiement.getBanque());
				bdBanque.setValue(paiement.getBanque().getLibelle());

				fournisseur = paiements.get(0).getFournisseur();
				contrat = paiements.get(0).getContrats();
				
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

			}
		}

	}

	private boolean checkFieldConstraints() {

		try {
			 if(txtPayerpar.getValue().equals(""))
		       {
              errorComponent = txtPayerpar;
              errorMsg = Labels.getLabel("kermel.paiement.payepar")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		        }
			if(rdcheque.isChecked()==true)
			{
			  if(txtNumCheque.getValue().equals(""))
		       {
              errorComponent = txtNumCheque;
              errorMsg = Labels.getLabel("kermel.paiement.numcheque")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		        }
			  
			  if(bdBanque.getValue().equals(""))
		       {
              errorComponent = bdBanque;
              errorMsg = Labels.getLabel("kermel.paiement.banque")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		        }
			 }
			
			if(rdautre.isChecked()==true)
			{
			   if(txtAutres.getValue().equals(""))
		       {
              errorComponent = txtAutres;
              errorMsg = Labels.getLabel("kermel.paiement.commentaireautre")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		      }
			}
			
			return true;

		} catch (Exception e) {
			errorMsg = Labels.getLabel("cciad.erreur.erreurinconnue") + ": " + e.toString() + " [checkFieldConstraints]";
			errorComponent = null;
			return false;

		}

	}

	//Step0
	public void onClick$menuCancelStep0() {
		loadApplicationState("suivi_paiement");
		detach();
	}

	public void onClick$menuNextStep0() {
		if (lstFour.getSelectedItem() == null) {
			throw new WrongValueException(lstFour, Labels.getLabel("cciad.error.select.item"));
		} else {
			fournisseur = (SygFournisseur) lstFour.getSelectedItem().getValue();
			lblnom.setValue(fournisseur.getNom());

			step0.setVisible(false);
			step1.setVisible(true);
			step2.setVisible(false);
			
			Events.postEvent(ApplicationEvents.ON_CONTRAT, this, null);
			
		}
	}

	
	
	//Step1**********************************************************************************
	
	public void onClick$menuCancelStep1() {
		loadApplicationState("suivi_paiement");
		detach();
	}
	
	public void onClick$menuNextStep1() {
		if (lstFour.getSelectedItem() == null){
			throw new WrongValueException(lstFour, Labels.getLabel("cciad.error.select.item"));
		}else if(lstContrat.getSelectedItem() == null){
			throw new WrongValueException(lstContrat, Labels.getLabel("cciad.error.select.item"));
		} else {
			fournisseur = (SygFournisseur) lstFour.getSelectedItem().getValue();
			contrat=(SygContrats) lstContrat.getSelectedItem().getValue();
			
			lblNum.setValue(contrat.getDossier().getDosReference());
			nbre=lstContrat.getSelectedCount();
			lblNbrContrat.setValue(Integer.toString(nbre));
			
			 for (int i = 0; i < lstContrat.getSelectedCount(); i++) {
				 
				 SygContrats contrats = (SygContrats) ((Listitem) lstContrat.getSelectedItems().toArray()[i]).getValue();
				 
				 if((montanttotal!=null)&&(montantversetotal!=null)){
					 if(contrats.getMontant()!=null)
			         montanttotal=montanttotal.add(contrats.getMontant());
			         if(contrats.getMontantverse()!=null)
			         montantversetotal=montantversetotal.add(contrats.getMontantverse());
		           }
			    else{
			    	 if(contrats.getMontant()!=null)
				         montanttotal =contrats.getMontant();
			    	 if(contrats.getMontantverse()!=null)
				         montantversetotal=contrats.getMontantverse();
			       }
			 }
			 
				lblMontant.setValue(ToolKermel.format2Decimal(montanttotal));
				lblMtAverse.setValue(ToolKermel.format2Decimal(montantversetotal));
			     txtPayerpar.setValue(fournisseur.getNom());
			
			step0.setVisible(false);
			step1.setVisible(false);
			step2.setVisible(true);
			
			
			
		}
	}
	
	public void onClick$menuPreviousStep1() {
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
		
	}
	
	
//Step2*****************************************************************
	public void onClick$menuNextStep2() {
		if (checkFieldConstraints()) {

			 numrecu="F_"+fournisseur.getId()+BeanLocator.defaultLookup(OperationPaiementSession.class).getGeneratedCode(BeConstants.PARAM_NUMRECU);
			  //numrecu=BeanLocator.defaultLookup(OperationPaiementSession.class).getGeneratedCode(BeConstants.PARAM_NUMRECU);
			   for (int i = 0; i < lstContrat.getSelectedCount(); i++) {
				    SygPaiement paiement = new SygPaiement();
					SygContrats contrats = (SygContrats)((Listitem) lstContrat.getSelectedItems().toArray()[i]).getValue();
					 paiement.setContrats(contrats);
					 
					 contrats.setConstatus(UIConstants.CONTRAT_PAYE);
					 datejour = new Date();
					 contrats.setCondatepaiement(datejour);
					 
					 if (((String) radioMpaiement.getSelectedItem().getValue()).equalsIgnoreCase(Labels.getLabel("kermel.paiement.modepaiement.cheque"))) {
							paiement.setModepaiement(Labels.getLabel("kermel.paiement.modepaiement.cheque"));
							
						} else if (((String) radioMpaiement.getSelectedItem().getValue()).equalsIgnoreCase(Labels.getLabel("kermel.paiement.modepaiement.espece"))) {

							paiement.setModepaiement(Labels.getLabel("kermel.paiement.modepaiement.espece"));
							
						}else {

							paiement.setModepaiement(Labels.getLabel("kermel.paiement.modepaiement.autre"));
							
						}
					 
				              if(lstBanque.getSelectedItem()!=null){
							   banque = (SygBanque) lstBanque.getSelectedItem().getValue();
							   paiement.setBanque(banque);
				              }

						    paiement.setNumCheque(txtNumCheque.getValue());
							//paiement.setBanque(txtBanque.getValue());
						  
						    paiement.setFournisseur(fournisseur);
						    paiement.setCommentaire(txtAutres.getValue());
						    paiement.setOperation(numrecu); 
						  
					 BeanLocator.defaultLookup(ContratsSession.class).update(contrats);
					 BeanLocator.defaultLookup(PaiementSession.class).save(paiement);
					
					}
			  
			   //Operation
			    operationpaiement.setOpemontanttotal(montanttotal);
			    operationpaiement.setOpmontantTotalVerse(montantversetotal);
			    montantenlettre=ToolKermel.conversionChiffreLettre(montantversetotal);
			      operationpaiement.setOpmontantTotalVerseEnLettre(montantenlettre);
			    operationpaiement.setOpedaterecu(new Date());
			     operationpaiement.setOpecode(numrecu);
			    operationpaiement.setOperecude(txtPayerpar.getValue());
			    operationpaiement.setOpecommentaires(txtAutres.getValue());
			    operationpaiement.setFournisseur(fournisseur);
			    BeanLocator.defaultLookup(OperationPaiementSession.class).save(operationpaiement);
			    nomFichier = "recupaiement.rptdesign";
                format = UIConstants.FORMAT_PDF;
                System.out.println(EDIT_URL + nomFichier + "&numrecu=" + numrecu + "&__format=" + format );
                iframeEs2Detached.setSrc(EDIT_URL + nomFichier + "&numrecu=" + numrecu + "&__format=" + format );
			    
			    step1.setVisible(false);
				step2.setVisible(false);
				step3.setVisible(true);
			
//			loadApplicationState("suivi_paiement");
//			detach();
			
		}
	}

	public void onClick$menuCancelImprimerEs(){
		loadApplicationState("suivi_paiement");
		detach();
      }
	// ///////////////////////////////////////////////////////////////////

	public class FournisseurRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygFournisseur fournisseur = (SygFournisseur) data;
			item.setValue(fournisseur);

			Listcell cellNom = new Listcell(fournisseur.getNom());
			cellNom.setParent(item);

			Listcell cellAdresse = new Listcell(fournisseur.getAdresse());
			cellAdresse.setParent(item);

			Listcell cellTel = new Listcell(fournisseur.getTel());
			cellTel.setParent(item);
			
			
			
			List<SygContrats> contratfour = BeanLocator.defaultLookup(ContratsSession.class).find(0, -1, null,null,null,UIConstants.CONTRAT_NPAYE,fournisseur);
			montanttotalfour=new BigDecimal(0);
			 for (int i = 0; i < contratfour.size(); i++) {
				 montanttotalfour=montanttotalfour.add(contratfour.get(i).getMontant());
//				 if(montanttotalfour!=null){
//					 montanttotalfour=montanttotalfour.add(contratfour.get(i).getMontant());
//			        
//		           }
//			    else{
//			    	montanttotalfour =contratfour.get(i).getMontant();
//				  
//			       }
			 }
			Listcell cellMontant = new Listcell(ToolKermel.format2Decimal(montanttotalfour));
			cellMontant.setParent(item);

			if (mode != null) {
				if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
					if (fournisseur.getId().equals(paiements.get(0).getFournisseur().getId())) {
						item.setSelected(true);
					}

				}
			}

		}
	}
	
	//Contrat

	public class ContratRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygContrats contrat = (SygContrats) data;
			item.setValue(contrat);

			Listcell cellReference = new Listcell(contrat.getDossier().getDosReference());
			cellReference.setParent(item);
			
			Listcell cellDate = new Listcell("");
			if(contrat.getConDateRecepDefinitive()!=null)
				cellDate.setLabel(UtilVue.getInstance().formateLaDate(contrat.getConDateRecepDefinitive()));
			cellDate.setParent(item);

		
			Listcell cellAC = new Listcell(contrat.getAutorite().getDenomination());
			cellAC.setParent(item);
			
			Listcell cellMontant = new Listcell(ToolKermel.format2Decimal(contrat.getMontant()));
			cellMontant.setParent(item);
			
			Listcell cellMontantVersement = new Listcell("");
			if(contrat.getMontantverse()!=null)
				cellMontantVersement.setLabel(ToolKermel.format2Decimal(contrat.getMontantverse()));
			cellMontantVersement.setParent(item);
			

			if (mode != null) {
				if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
					if (contrat.getConID().equals(paiements.get(0).getContrats().getConID())) {
						item.setSelected(true);
					}

				}
			}

		}
	}


	
	
	
	public void onClick$menuCancelStep2() {
	
		loadApplicationState("suivi_paiement");
		detach();
	}


	
	public void onClick$menuPreviousStep2() {
		montanttotal=new BigDecimal(0); montantversetotal=new BigDecimal(0);
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
		
	}

	// //////////////////////////////////////////////////

	// /liste fournisseur
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygFournisseur fournisseur = (SygFournisseur) data;
		item.setValue(fournisseur.getId());

	}
	
	
	public void onCheck$radioMpaiement(){
		if(radioMpaiement.getSelectedItem().getValue().equals(Labels.getLabel("kermel.paiement.modepaiement.cheque"))){
			
			txtNumCheque.setDisabled(false);
			bdBanque.setDisabled(false);
			txtAutres.setDisabled(true);
			
		}else if(radioMpaiement.getSelectedItem().getValue().equals(Labels.getLabel("kermel.paiement.modepaiement.espece"))){
			
			txtNumCheque.setDisabled(true);
			bdBanque.setDisabled(true);
			txtAutres.setDisabled(true);
			
		}if(radioMpaiement.getSelectedItem().getValue().equals(Labels.getLabel("kermel.paiement.modepaiement.autre"))){
			
			txtNumCheque.setDisabled(true);
			bdBanque.setDisabled(true);
			txtAutres.setDisabled(false);
		}
	}
	
	
	// //////////Banque********************
	public void onSelect$lstBanque() {
		bdBanque.setValue((String) lstBanque.getSelectedItem().getAttribute(UIConstants.ATTRIBUTE_LIBELLE));
		bdBanque.close();
	}

	public class banqueRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygBanque banque = (SygBanque) data;
			item.setValue(banque);
			item.setAttribute(UIConstants.ATTRIBUTE_LIBELLE, banque.getLibelle());

			Listcell cellLibelle = new Listcell("");
			if (banque.getLibelle() != null) {
				cellLibelle.setLabel(banque.getLibelle());
			}
			cellLibelle.setParent(item);
			//				

		}
	}

	public void onFocus$txtRechercherBanque() {
		if (txtRechercherBanque.getValue().equalsIgnoreCase(Labels.getLabel("kermel.paiement.banque"))) {
			txtRechercherBanque.setValue("");
		}
	}

	public void onBlur$txtRechercherDecision() {
		if (txtRechercherBanque.getValue().equalsIgnoreCase("")) {
			txtRechercherBanque.setValue(Labels.getLabel("kermel.paiement.banque"));
		}
	}

	public void onClick$btnRechercherDecision() {
		if (txtRechercherBanque.getValue().equalsIgnoreCase(Labels.getLabel("kermel.paiement.banque")) || txtRechercherBanque.getValue().equals("")) {
			LibelleBanque = null;
		} else {
			LibelleBanque = txtRechercherBanque.getValue();
		}
		Events.postEvent(ApplicationEvents.ON_BANQUE, this, null);
	}

	// ////////////////fin ////////////////////////////////
	
	
////////////*******************Raison sociale*******//////////////////////

	// function du bouton recherche par Raison sociale
	public void onFocus$txtLibelle() {
		if (txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.paiement.form.listefournisseur.raisoncial")))
			txtLibelle.setValue("");

	}

	public void onBlur$txtLibelle() {
		if (txtLibelle.getValue().equals(""))
			txtLibelle.setValue(Labels.getLabel("kermel.paiement.form.listefournisseur.raisoncial"));
	}

	public void onOK$txtLibelle() {
		onClick$bchercher();
	}

	
	public void onClick$bchercher() {

		if (txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.paiement.form.listefournisseur.raisoncial")) || txtLibelle.getValue().equals("")) {
			nom = null;
		} else {
			nom = txtLibelle.getValue();
		}

		Events.postEvent(ApplicationEvents.ON_FOURNISSEUR, this, null);

	}

	public void onOK$bchercher() {
		onClick$bchercher();
	}

	//////////////*******************Denomination*******//////////////////////
	

}