package sn.ssi.kermel.web.travauxdrp.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDevise;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossiersEvaluateurs;
import sn.ssi.kermel.be.entity.SygDossiersFournisseurs;
import sn.ssi.kermel.be.entity.SygDossierscommissionsmarches;
import sn.ssi.kermel.be.entity.SygDossierspieces;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygGarantiesDossiers;
import sn.ssi.kermel.be.entity.SygMontantsSeuils;
import sn.ssi.kermel.be.entity.SygObservateursIndependants;
import sn.ssi.kermel.be.entity.SygPiecesplisouvertures;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygPresenceouverture;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygRepresentantsServicesTechniques;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DeviseSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersPiecesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersSouscriteresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.GarantiesSession;
import sn.ssi.kermel.be.plansdepassation.ejb.DossiersFournisseursSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsBailleursSession;
import sn.ssi.kermel.be.referentiel.ejb.MontantsSeuilsSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class DossiersPreparationController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code,avalider="oui";
	private Tab TAB_infosgenerales,TAB_garanties,TAB_piecesadministratives,TAB_criteresqualifications,TAB_devise,TAB_financement;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblInfos;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygDossiers dossier=new SygDossiers();
	List<SygGarantiesDossiers> garanties = new ArrayList<SygGarantiesDossiers>();
	List<SygDossierspieces> piecesadministratives = new ArrayList<SygDossierspieces>();
	List<SygDossierssouscriteres> criteresqualifications = new ArrayList<SygDossierssouscriteres>();
	List<SygDevise> devises = new ArrayList<SygDevise>();
	List<SygRealisationsBailleurs> bailleurs = new ArrayList<SygRealisationsBailleurs>();
	List<SygRetraitregistredao> registreretrait = new ArrayList<SygRetraitregistredao>();
	List<SygPlisouvertures> registredepot = new ArrayList<SygPlisouvertures>();
	List<SygDossierscommissionsmarches> membrescommissions = new ArrayList<SygDossierscommissionsmarches>();
	List<SygPresenceouverture> representantsoummissionnaires = new ArrayList<SygPresenceouverture>();
	List<SygRepresentantsServicesTechniques> representantservicestechniques = new ArrayList<SygRepresentantsServicesTechniques>();
	List<SygObservateursIndependants> observateursindependants = new ArrayList<SygObservateursIndependants>();
	List<SygPiecesplisouvertures> piecessoumissionnaires = new ArrayList<SygPiecesplisouvertures>();
	List<SygDossiersEvaluateurs> compositioncommissiontechnique = new ArrayList<SygDossiersEvaluateurs>();
	List<SygPlisouvertures> lecturesoffres = new ArrayList<SygPlisouvertures>();
	List<SygMontantsSeuils> seuils = new ArrayList<SygMontantsSeuils>();
	private Image imgmiseenvalidation;
	private Label miseenvalidation;
	List<SygDossiersFournisseurs> fournisseursselectionnes = new ArrayList<SygDossiersFournisseurs>();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier!=null)
		{
			garanties = BeanLocator.defaultLookup(GarantiesSession.class).find(0,-1,dossier,null,null);
			piecesadministratives = BeanLocator.defaultLookup(DossiersPiecesSession.class).find(0,-1,dossier);
			criteresqualifications = BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(0,-1,dossier, null, null);
			devises= BeanLocator.defaultLookup(DeviseSession.class).find(0,-1,dossier);
			bailleurs = BeanLocator.defaultLookup(RealisationsBailleursSession.class).find(0,-1,null,appel.getRealisation(),null);
		}
       seuils = BeanLocator.defaultLookup(MontantsSeuilsSession.class).find(0,-1, autorite.getType(), appel.getTypemarche(), appel.getModepassation(), null, null,UIConstants.TYPE_SEUILSRAPRIORI);
		
		if(seuils.size()>0)
		{
			if(seuils.get(0).getMontantinferieur()!=null)
			{
				if((appel.getApomontantestime().compareTo(seuils.get(0).getMontantinferieur())==1)||(appel.getApomontantestime().equals(seuils.get(0).getMontantinferieur())))
		
				{
					imgmiseenvalidation.setVisible(true);
					miseenvalidation.setVisible(true);
					avalider="oui";
				}
				else
				{
					imgmiseenvalidation.setVisible(false);
					miseenvalidation.setVisible(false);
					avalider="non";
				}
					
			}
			
		}
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	public void onClick$miseenvalidation() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		}
		else
		{
			if(garanties.size()==0)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirgaranties"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				if(piecesadministratives.size()==0)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirpieces"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				}
				else
				{
					if(criteresqualifications.size()==0)
					{
						 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisircriteres"), "Erreur", Messagebox.OK, Messagebox.ERROR);
							
					}
					else
					{
						if(devises.size()==0)
						{
							 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirdevises"), "Erreur", Messagebox.OK, Messagebox.ERROR);
								
						}
						else
						{
							if(bailleurs.size()==0)
							{
								 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirbailleurs"), "Erreur", Messagebox.OK, Messagebox.ERROR);
									
							}
							else
							{
								session.setAttribute("libelle", "validationdossier");
								loadApplicationState("suivi_travaux_drp");
							}
						}
					}
				}
			}
			
		}
	}
	
	public void onClick$miseenpublication() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		}
		else
		{
			if(avalider.equals("oui"))
			{
				if(dossier.getDosDateValidation()==null)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validerdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);

				}
				else
				{
					session.setAttribute("libelle", "publicationdossier");
					loadApplicationState("suivi_travaux_drp");
				}
			}
			else
			{
				session.setAttribute("libelle", "publicationdossier");
				loadApplicationState("suivi_travaux_drp");
			}
			
			
		}
	}
	
	public void onClick$infosgenerales() {
		session.setAttribute("libelle", "infogenerales");
		loadApplicationState("suivi_travaux_drp");
	}

	public void onClick$devise() {
		session.setAttribute("libelle", "devise");
		loadApplicationState("suivi_travaux_drp");
	}
	
	public void onClick$piecesgaranties() {
		session.setAttribute("libelle", "piecesadministratives");
		loadApplicationState("suivi_travaux_drp");
	}
	
	public void onClick$criteresqualifications() {
		session.setAttribute("libelle", "criteresqualifications");
		loadApplicationState("suivi_travaux_drp");
	}
	public void onClick$fournisseursselectionnes() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				
		}
		else
		{
			session.setAttribute("libelle", "fournisseursselectionnes");
			loadApplicationState("suivi_travaux_drp");
		}
		
	}
	
	public void onClick$lettreinvitation() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				
		}
		else
		{
			fournisseursselectionnes = BeanLocator.defaultLookup(DossiersFournisseursSession.class).find(0,-1,null,appel,null);
			if(fournisseursselectionnes.size()==dossier.getDosSoumission())
			{
				session.setAttribute("libelle", "lettreinvitation");
				loadApplicationState("suivi_travaux_drp");
			}
			else
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.travauxdrp.candidats.ainvites")+" "+dossier.getDosSoumission(), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
				
		
		}
		
	}
}