package sn.ssi.kermel.web.travauxdrp.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.MessageManager;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossiersFournisseurs;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygTachesEffectues;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.LotsSession;
import sn.ssi.kermel.be.plansdepassation.ejb.DossiersFournisseursSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;



@SuppressWarnings("serial")
public class FournisseursController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstPieces;
	private Paging pgPieces;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
  	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	SygFournisseur fournisseur=new SygFournisseur();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuAjouter,menuSupprimer;
	private Div step0,step1,step2,step3,step4;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygDossiersFournisseurs fournisseur_invite;
	private Textbox txtVersionElectronique,txtCommentaires;
	private String nomFichier,fichiernom="",listefichier,destination,imagebis;
	private  String cheminDossier = UIConstants.PATH_LETTREINVITATION;
	private final String cheminDossier2 = UIConstants.PATH_PJ;
	private String termeref,nomtermeref,lettreinvitation,nomlettreinvitation;
	private Image image,image2;
	private Iframe idIframe;
	private String extension,images,extension2,images2;
	private Datebox dtdate;
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	UtilVue utilVue = UtilVue.getInstance();
	List<SygDossiersFournisseurs> fournisseurs;
	 private String nomfichier;
	 private static Font textSouligne = new Font(Font.FontFamily.TIMES_ROMAN, 12,  Font.UNDERLINE,BaseColor.BLUE);
	
	private static BaseColor bleuClair = new BaseColor(175, 185, 212);
	private static BaseColor gris = new BaseColor(220, 220, 220);
	private static Font textBleu = new Font(Font.FontFamily.TIMES_ROMAN, 12,
		      Font.NORMAL,bleuClair);
	public  String RESULT = "/home/moussa/pdf/bordereau.pdf";
	
	/* styles personnalisé */
	private static Font textNormal = new Font(Font.FontFamily.COURIER, 10,  Font.NORMAL,BaseColor.BLACK);
	private static Font textNormalSouligne = new Font(Font.FontFamily.COURIER, 10,  Font.UNDERLINE,BaseColor.BLACK);
	private static Font textGras = new Font(Font.FontFamily.COURIER, 10, Font.BOLD,BaseColor.BLACK);
	private String repertoire = "";
	List<SygLots> lots;
	SygTachesEffectues tache=new SygTachesEffectues();
		
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		lstListe.setItemRenderer(this);
	
    	
		addEventListener(ApplicationEvents.ON_PIECES, this);
		lstPieces.setItemRenderer(new PiecesRenderer());
		pgPieces.setPageSize(byPage);
		pgPieces.addForward("onPaging", this, ApplicationEvents.ON_PIECES);
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier==null)
		{
			menuAjouter.setDisabled(true);
			menuSupprimer.setDisabled(true);
		}
		else
		{
			//if(dossier.getDosDateMiseValidation()!=null||dossier.getDosDatePublication()!=null)
			tache= (SygTachesEffectues) session.getAttribute("tache");
			if(dossier.getDosdateinvitation()!=null)
			{
				//menuAjouter.setDisabled(true);
				menuSupprimer.setDisabled(true);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			Events.postEvent(ApplicationEvents.ON_PIECES, this, null);
			
//			fichiernom=dossier.getAutorite().getSigle()+"_"  +dossier.getRealisation().getReference()+UtilVue.getInstance().formateLaDate3(Calendar.getInstance().getTime())+".pdf";
//			nomFichier = cheminDossier  + fichiernom;
//			if (ToolKermel.isWindows()) {
//				nomFichier = nomFichier.replaceAll("/", "\\\\");
//			} else {
//				nomFichier = nomFichier.replaceAll("\\\\", "/");
//				
//			}
			lots= BeanLocator.defaultLookup(LotsSession.class).find(0,-1,dossier,null);
		}
		
		
		//nomfichier= UIConstants.PATH_LETTREINVITATION+;
		
		
	}
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
		
			 fournisseurs = BeanLocator.defaultLookup(DossiersFournisseursSession.class).find(0,-1,null,appel,null);
			 lstListe.setModel(new SimpleListModel(fournisseurs));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_PIECES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPieces.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPieces.getActivePage() * byPage;
				pgPieces.setPageSize(byPage);
			}
			 List<SygFournisseur> fournisseurs = BeanLocator.defaultLookup(DossiersFournisseursSession.class).ListesFournisseurs(activePage,byPage,libelle,appel.getApoid(),null);
			 lstPieces.setModel(new SimpleListModel(fournisseurs));
			 pgPieces.setTotalSize(BeanLocator.defaultLookup(DossiersFournisseursSession.class).ListesFournisseurs(libelle,appel.getApoid(),null).size());
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = ((SygDossiersFournisseurs)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue()).getId();
				BeanLocator.defaultLookup(DossiersFournisseursSession.class).delete(codes);
			}
			 Preselections();
			 session.setAttribute("libelle", "fournisseursselectionnes");
			   loadApplicationState("suivi_travaux_drp");
		}
		else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
			Listcell listcell = (Listcell) event.getTarget();
			String toDo = (String) listcell.getAttribute(UIConstants.TODO);
			if(toDo.equals("modifier"))
			{
				step0.setVisible(false);
				step1.setVisible(false);
				step2.setVisible(true);
				step3.setVisible(false);
				step4.setVisible(false);
				
				fournisseur_invite=(SygDossiersFournisseurs) listcell.getAttribute("fournisseurs");
				if(fournisseur_invite.getDateinvitation()!=null)
					dtdate.setValue(fournisseur_invite.getDateinvitation());
				txtCommentaires.setValue(fournisseur_invite.getCommentaire());
				
				if(fournisseur_invite.getLettreInvitation()!=null)
				{
					nomFichier=fournisseur_invite.getLettreInvitation();
					extension=nomFichier.substring(nomFichier.length()-3,  nomFichier.length());
					 if(extension.equalsIgnoreCase("pdf"))
						 images="/images/icone_pdf.png";
					 else  
						 images="/images/word.jpg";
					 
					image.setVisible(true);
					image.setSrc(images);
				}
							}
		}
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygDossiersFournisseurs fournisseurs = (SygDossiersFournisseurs) data;
		item.setValue(fournisseurs);

		 Listcell cellLibelle = new Listcell(fournisseurs.getFournisseur().getNom());
		 cellLibelle.setParent(item);
		 
		 Listcell cellPays = new Listcell(fournisseurs.getFournisseur().getPays().getLibelle());
		 cellPays.setParent(item);
		 
		 Listcell cellEmail = new Listcell(fournisseurs.getFournisseur().getEmail());
		 cellEmail.setParent(item);
		 
		 Listcell cellImage = new Listcell();
		 if(fournisseurs.getLettreInvitation()!=null&&!fournisseurs.getLettreInvitation().equals(""))
		   {
			 cellImage.setImage("/images/rdo_on.png");
			 cellImage.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.candidatnotifie"));
		   }
		 else
		  {
			 cellImage.setImage("/images/rdo_off.png");
			 cellImage.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.candidatnonnotifie"));
		  }
		 cellImage.setAttribute("fournisseurs", fournisseurs);
		 cellImage.setAttribute(UIConstants.TODO, "modifier");
		 cellImage.addEventListener(Events.ON_CLICK, FournisseursController.this);
		cellImage.setParent(item);
			
	}

	
	public class PiecesRenderer implements ListitemRenderer{
		
		
		
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygFournisseur fournisseurs = (SygFournisseur) data;
		item.setValue(fournisseurs);

		 Listcell cellLibelle = new Listcell(fournisseurs.getNom());
		 cellLibelle.setParent(item);
		 
		 Listcell cellPays = new Listcell(fournisseurs.getPays().getLibelle());
		 cellPays.setParent(item);
		 
		 Listcell cellAdresse = new Listcell(fournisseurs.getAdresse());
		 cellAdresse.setParent(item);
		 
		 Listcell cellTel = new Listcell(fournisseurs.getTel());
		 cellTel.setParent(item);
		 
		 Listcell cellFax = new Listcell(fournisseurs.getFax());
		 cellFax.setParent(item);
		 
		 Listcell cellEmail = new Listcell(fournisseurs.getEmail());
		 cellEmail.setParent(item);
	
	}
	}
	public void onClick$menuSupprimer()
	{
		if (lstListe.getSelectedItem() == null)
			
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		

		HashMap<String, String> display = new HashMap<String, String>(); // permet
		display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
		display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
		display.put(MessageBoxController.DSP_HEIGHT, "250px");
		display.put(MessageBoxController.DSP_WIDTH, "47%");

		HashMap<String, Object> map = new HashMap<String, Object>(); // permet
		map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
		showMessageBox(display, map);
		
	}
	public void onClick$menuAjouter()
	{
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
		step3.setVisible(false);
		step4.setVisible(false);
		Events.postEvent(ApplicationEvents.ON_PIECES, this, null);
	}
	public void onClick$menuFermer()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
		step3.setVisible(false);
		step4.setVisible(false);
		
	}
	public void onClick$menuValider()
	{
       if (lstPieces.getSelectedItem() == null)
			throw new WrongValueException(lstPieces, Labels.getLabel("kermel.error.select.item"));
       
       for (int i = 0; i < lstPieces.getSelectedCount(); i++) {
    	   SygDossiersFournisseurs dossierfournisseur=new SygDossiersFournisseurs();
    	   fournisseur=(SygFournisseur) ((Listitem) lstPieces.getSelectedItems().toArray()[i]).getValue();
    	   dossierfournisseur.setFournisseur(fournisseur);
    	   dossierfournisseur.setAppel(appel);
    	   dossierfournisseur.setDossier(dossier);
    	   dossierfournisseur.setTermeRef(dossier.getDosFichier());
    	   BeanLocator.defaultLookup(DossiersFournisseursSession.class).save(dossierfournisseur);
       }
       Preselections();
       session.setAttribute("libelle", "fournisseursselectionnes");
	   loadApplicationState("suivi_travaux_drp");
		
	}
	
	/////26/01/2016
	public void onClick$menuFermerstep2()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
		step3.setVisible(false);
		step4.setVisible(false);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
	}
	public void onClick$menuValiderstep2()
	{
		if(checkFieldConstraints())
		{
			
			if(nomFichier!=null&&!nomFichier.equals(""))
				fournisseur_invite.setLettreInvitation(nomFichier);
			
			
			BeanLocator.defaultLookup(DossiersFournisseursSession.class).update(fournisseur_invite);
			
			
			
			 session.setAttribute("libelle", "fournisseursselectionnes");
			   loadApplicationState("suivi_travaux_drp");
			
		}
	}
	
private boolean checkFieldConstraints() {
		
		try {
		
			
			if(txtVersionElectronique.getValue().equals(""))
		     {
          errorComponent = txtVersionElectronique;
          errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.travauxdrp.lettreinvitation")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}


	public void onClick$image() {
		step0.setVisible(false);
		step1.setVisible(false);
		step2.setVisible(false);
		step3.setVisible(true);
		step4.setVisible(false);
		idIframe.isInvalidated();
		idIframe.setSrc("");
		
		String filepath = cheminDossier +  nomFichier;
		File f = new File(filepath.replaceAll("\\\\", "/"));
	
		org.zkoss.util.media.AMedia mymedia = null;
		try {
			mymedia = new AMedia(f, null, null);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		if (mymedia != null)
			idIframe.setContent(mymedia);
		else
			idIframe.setSrc("");
	
		idIframe.setHeight("600px");
		idIframe.setWidth("100%");
	}
	
	
	
	
	
	public void onClick$btnChoixFichier() {
		//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
			if (ToolKermel.isWindows())
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectronique.setValue(nomFichier);
		}
	
	
	
	public void onClick$menuFermerstep3()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
		step3.setVisible(false);
		step4.setVisible(false);
		
	}
	
	public void onClick$menuFermerstep4()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
		step3.setVisible(false);
		step4.setVisible(false);
		
	}
	
private boolean checkFieldConstraintsstep4() {
		
		try {
		
			if(dtdate.getValue()==null)
		     {
       errorComponent = dtdate;
       errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.resultatnegociation.date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

public void onClick$menuEnvoyer() throws FileNotFoundException, DocumentException
{
	int nombre=0;
	for(int i=0;i<fournisseurs.size();i++)
	{
		if(fournisseurs.get(i).getLettreInvitation()!=null&&!fournisseurs.get(i).getLettreInvitation().equals(""))
			nombre=nombre+1;
		
	}
	if(nombre!=fournisseurs.size())
		throw new WrongValueException(lstListe, Labels.getLabel("kermel.plansdepassation.proceduresmarches.tous.lettreinvitation.envoyer"));
	
	step0.setVisible(false);
	step1.setVisible(false);
	step2.setVisible(false);
	step3.setVisible(false);
	step4.setVisible(true);
}
public void onClick$menuEnvoyerold() throws FileNotFoundException, DocumentException
{
	int nombre=0;
	for(int i=0;i<fournisseurs.size();i++)
	{
		if(fournisseurs.get(i).getLettreInvitation()!=null&&!fournisseurs.get(i).getLettreInvitation().equals(""))
			nombre=nombre+1;
		
	}
	if(nombre!=fournisseurs.size())
		throw new WrongValueException(lstListe, Labels.getLabel("kermel.plansdepassation.proceduresmarches.tous.lettreinvitation.envoyer"));
	
	try {
		
		Generer(fournisseurs);
		
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	step0.setVisible(false);
	step1.setVisible(false);
	step2.setVisible(false);
	step3.setVisible(false);
	step4.setVisible(true);
	idIframe.setSrc("");
	VisualiserFichier();
	
}
public void onClick$menuValiderstep4()
{
	if(checkFieldConstraintsstep4())
	{
		
		//fichiernom=dossier.getAutorite().getSigle()+"_"  +dossier.getRealisation().getReference()+UtilVue.getInstance().formateLaDate3(Calendar.getInstance().getTime())+".pdf";
		nomtermeref="Termes de r�f�rence.pdf";
		nomlettreinvitation="Lettre d'invitation.pdf";
		
		
		for(int i=0;i<fournisseurs.size();i++)
		{
			fournisseur_invite=fournisseurs.get(i);
		    fournisseur_invite.setDateinvitation(dtdate.getValue());
			fournisseur_invite.setCommentaire(txtCommentaires.getValue());
			BeanLocator.defaultLookup(DossiersFournisseursSession.class).update(fournisseur_invite);
			
			termeref = cheminDossier2  + fournisseur_invite.getTermeRef();
			lettreinvitation = cheminDossier  + fournisseur_invite.getLettreInvitation();
		  	if (ToolKermel.isWindows()) {
				termeref = termeref.replaceAll("/", "\\\\");
				lettreinvitation = lettreinvitation.replaceAll("/", "\\\\");
			} else {
				termeref = termeref.replaceAll("\\\\", "/");
				lettreinvitation = lettreinvitation.replaceAll("\\\\", "/");
			}
		  	if(fournisseur_invite.getFournisseur().getEmail()!=null&&!fournisseur_invite.getFournisseur().getEmail().equals(""))
			envoiMail(fournisseur_invite,infoscompte,dossier,dtdate.getValue(),lots,termeref,nomtermeref,lettreinvitation,nomlettreinvitation);

		}
		
		  dossier.setDosdateinvitation(dtdate.getValue());
			BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).update(dossier);
		
		 session.setAttribute("libelle", "fournisseursselectionnes");
		   loadApplicationState("suivi_travaux_drp");
		
	}
}



///05/02/2016
//Function mise enforme envoi de mail
public void envoiMail(SygDossiersFournisseurs fournisseur,Utilisateur users,SygDossiers dossier,Date dateenvoi,List<SygLots> lots,String termeref,String nomtermeref,String lettreinvitation,String nomlettreinvitation) {
    
  SygAutoriteContractante autorite=dossier.getRealisation().getPlan().getAutorite();
	
	// String urlprojet=BeanLocator.defaultLookup(ParametresGenerauxSession.class).findById("PATH_PROJET").getLibelle();
		
	  String mail=fournisseur.getFournisseur().getEmail();//"asamb@ssi.sn";//
		
	 
	 //Sujet
		String subject = UIConstants.OBJET_MESSAGE+" DRP N� "+dossier.getRealisation().getReference();
		//contenu


		//////////////////////////////////////
		 // String message = "Imputation courrier";
		String message = "";
		message += "<html>";
		message += "<style type='text/css'>";
		message += "<!--";
		message += ".Style1 {color: #0000FF}";
		message += "-->";
		message += "</style>";
		message += "<body>";
      //  message += dossier.getRealisation().getLibelle();
       // message +=" <strong>Bonjour "+courimp.getAgent().getPrenom()+" "+courimp.getAgent().getNom()+",</strong><br /> <br />";
	    message +=" <table cellspacing='2' cellpadding='2' border='0' width='100%' >";
		
	  
	    message += "<tr>";
	    message += "<td height='20px'>";
	    message += "</td>";
	    message += "</tr>";
	    message += "<tr>";
	    message += "<td  >";
	    message += "1.	Autorit� contractante: ";
	    message += "</td>";
	    message += "<td align='left'>";
	    message += "<b>"+autorite.getDenomination()+"</b>";
	    message += "</td>";
	    message += "</tr>";
	    message += "<tr>";
	    message += "<td  >";
	    message += "2.	Date transmission: ";
	    message += "</td>";
	    message += "<td align='left'>";
	    message += "<b>"+utilVue.formateLaDate2(dateenvoi)+"</b>";
	    message += "</td>";
	    message += "</tr>";
	    message += "<td>";
	    message += "<tr>";
	    message += "<td >";
	    message += "3.	Date limite de d�p�t des offres  ";
	    message += "</td>";
	    message += "<td align='left'>";
	    message += "<b>"+utilVue.formateLaDate2(dossier.getDosDateOuvertueDesplis())+" � "+utilVue.formateLHeure(dossier.getDosHeureOuvertureDesPlis())+"</b>";
	    message += "</td>";
	    message += "</tr>";
	    message += "<tr>";
	    message += "<td colspan='2'>";
	    message += "4.	Les soumissionnaires invit�s � soumettre des offres sont les suivants :  ";
	    message += "</td>";
	    message += "</tr>";
	    message += "<tr>";
	    message += "<td colspan='2'>";
	    message += "<table width='100%' border='1' cellspacing='0' cellpadding='0'>";
	    message += "<tr>";
	    message += "<td width='5%'>N�</td>";
	    message += "<td width='45%'>Soumissionnaire</td>";
	    message += "<td width='25%'>Adresse</td>";
	    message += "<td width='25%'>Contact</td>";
	    message += "</tr>";
	    int nombre=0;
	    for(int i=0;i<fournisseurs.size();i++)
	    {
	    	 nombre=i+1;
	    	 message += "<tr>";
	    	 message += "<td >";
	    	 message += nombre;
	    	 message += "</td>";
	    	 message += "<td ><b>";
	    	 message += fournisseurs.get(i).getFournisseur().getNom();
	    	 message += "</b></td>";
	    	 message += "<td >";
	    	 message += fournisseurs.get(i).getFournisseur().getAdresse();
	    	 message += "<br/ >";
	    	 message += "BP: "+fournisseurs.get(i).getFournisseur().getAdresse();
	    	 message += "</td>";
	    	 message += "<td >";
	    	 message += "Tel : "+fournisseurs.get(i).getFournisseur().getTel();
	    	 message += "<br/ >";
	    	 message += "Fax : "+fournisseurs.get(i).getFournisseur().getFax();
	    	 message += "<br/ >";
	    	 message += "Email : "+fournisseurs.get(i).getFournisseur().getEmail();
	    	 message += "<br/ >";
	    	 message += "Site : ";
	    	 message += "<br/ >";
	    	 message += "</td>";
	    	 message += "</tr>";
	    }
	    message += "</table>";
	    message += "</td>";
	    message += "</tr>";
	  
        message += "<tr>";
	    message += "<td  >";
	    message += "5.	Lieu de d�p�t des offres : ";
	    message += "</td>";
	    message += "<td align='left'>";
	    message += "<b>"+autorite.getAdresse()+"</b>";
	    message += "</td>";
	    message += "</tr>";
	    message += "</table>";
	    message += "<br/>";
	    message += "<table>";
	    message += "<tr>";
	    message += "<td>";
	    message += "Ci-joint le fichier des termes de r�f�rence et la lettre d'invitation.";
	    message += "</td>";
	    message += "</tr>";
	    message += "</table>";

		message += "</body>";
		message += "</html>";

		  
		
		MessageManager.sendMail(subject, message, mail,termeref,nomtermeref,lettreinvitation,nomlettreinvitation);
 }

/////////////////09/02/2016
public void Generer(List<SygDossiersFournisseurs> fournisseurs) throws FileNotFoundException, DocumentException{
	
	// Format du fichier	
	
	
	listefichier="";

	cheminDossier= UIConstants.path+ "" + "/alldocs/kermel/lettreinvitation/";
	imagebis=UIConstants.path + "" + "/alldocs/daida/photos/agents.jpg";
	destination = UIConstants.path  + dossier.getAutorite().getSigle()+"_"  +dossier.getRealisation().getReference()+".pdf";
	
	
		fichiernom=dossier.getAutorite().getSigle()+"_"  +dossier.getRealisation().getReference()+".pdf";
		
		nomFichier = cheminDossier  + fichiernom;
	
		if (ToolKermel.isWindows()) {
			nomFichier = nomFichier.replaceAll("/", "\\\\");
		} else {
			nomFichier = nomFichier.replaceAll("\\\\", "/");
			
		}
		 genererfichiers(nomFichier,fournisseurs,dossier,dtdate.getValue());
		
  
	//
    // 
}

public  void genererfichiers(String nomfichier,List<SygDossiersFournisseurs> fournisseurs,SygDossiers dossier,Date datedetransmission) throws FileNotFoundException, DocumentException {
	Document document = new Document();
	  
    PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(nomfichier));
   
    document.open();
    
    document.setMargins(0f, 0f, 0f, 0f);
    ///2OP
 
    dtdate.setValue(new Date());
    //1 �tat des sommes dues
    genererLettre(document,fournisseurs,dossier,dtdate.getValue());
 
    
   
    document.close();
}





public static void genererLettre(Document document,List<SygDossiersFournisseurs> fournisseurs,SygDossiers dossier,Date datedetransmission){
    try {
    	SygAutoriteContractante ac=dossier.getRealisation().getPlan().getAutorite();
    	document.newPage();
    	
    	PdfPTable tableEntete = new PdfPTable(1);
		tableEntete.setWidthPercentage(100);
		
		
	
		PdfPTable tableObjet = new PdfPTable(3);
		tableObjet.setWidthPercentage(100);
		tableObjet.setWidths(new float[]{0.8f,1f,3f});
		 
		PdfPCell tableObjet_c1 = new PdfPCell();
		tableObjet_c1.addElement(new Phrase("",textNormal));
		enteteCellStyle(tableObjet_c1);
		tableObjet.addCell(tableObjet_c1);
	
		PdfPCell tableObjet_c2 = new PdfPCell();
		tableObjet_c2.addElement(new Phrase("Objet du mail :",textNormalSouligne));
		tableObjet_c2.setBorder(0);
		enteteCellStyle(tableObjet_c2);
		tableObjet.addCell(tableObjet_c2);
		
		PdfPCell tableObjet_c3 = new PdfPCell();	
		tableObjet_c3.addElement(new Phrase("TRANSMISSION LETTRE D�INVITATION ET TERMES DE REFERENCES",textGras));
		enteteCellStyle(tableObjet_c3);
		tableObjet.addCell(tableObjet_c3);
		
		////1.	Autorit� contractante 
			
		PdfPCell autorite = new PdfPCell();
		autorite.addElement(new Phrase("1.	Autorit� contractante :",textNormal));
		autorite.setBorder(0);
		autorite.setColspan(2);
		tableObjet.addCell(autorite);
		
		PdfPCell autoritenom = new PdfPCell();
		autoritenom.setBorder(0);
		autoritenom.addElement(new Phrase(ac.getDenomination(),textGras));
		tableObjet.addCell(autoritenom);
		
		///2- Date Transmission
		
		PdfPCell datetransmission = new PdfPCell();
		datetransmission.addElement(new Phrase("2.	Date transmission :",textNormal));
		datetransmission.setBorder(0);
		datetransmission.setColspan(2);
		tableObjet.addCell(datetransmission);
		
		PdfPCell datetransmission2 = new PdfPCell();
		datetransmission2.setBorder(0);
		datetransmission2.addElement(new Phrase(UtilVue.getInstance().formateLaDate2(datedetransmission),textGras));
		tableObjet.addCell(datetransmission2);
		
		///3- Date Transmission
		
		PdfPCell datelimite = new PdfPCell();
		datelimite.addElement(new Phrase("3.	Date limite de d�p�t des offres :",textNormal));
		datelimite.setBorder(0);
		datelimite.setColspan(2);
		tableObjet.addCell(datelimite);
				
		PdfPCell datelimite2 = new PdfPCell();
		datelimite2.setBorder(0);
		datelimite2.addElement(new Phrase(UtilVue.getInstance().formateLaDate2(dossier.getDosDateLimiteDepot())+" � "+UtilVue.getInstance().formateLHeure(dossier.getDosHeurelimitedepot()),textGras));
		tableObjet.addCell(datelimite2);
		
		PdfPCell table1_cell = new PdfPCell();
		table1_cell.addElement(tableObjet);
		table1_cell.setPadding(2);
		table1_cell.setBorder(0);
		
		
		///4- Soumissionnaires
		
		PdfPCell soumissionnaires = new PdfPCell();
		soumissionnaires.addElement(new Phrase("4.	Les soumissionnaires invit�s � soumettre des offres sont les suivants :",textNormal));
		soumissionnaires.setBorder(0);
		soumissionnaires.setColspan(3);
		tableObjet.addCell(soumissionnaires);
		
		PdfPCell soumissionnaires_liste = new PdfPCell();
		soumissionnaires_liste.setBorder(0);
		soumissionnaires_liste.setColspan(3);
		
		PdfPTable table_soum = new PdfPTable(4);
		table_soum.setWidthPercentage(100);
		table_soum.setWidths(new float[]{0.5f,2f,2f,2f});
		
		PdfPCell cell1 = new PdfPCell();
		cell1.addElement(new Phrase("N�",textNormal));
		table_soum.addCell(cell1);
		
		PdfPCell cell2 = new PdfPCell();
		cell2.addElement(new Phrase("Soumissionnaire",textNormal));
		table_soum.addCell(cell2);
		
		PdfPCell cell3 = new PdfPCell();
		cell3.addElement(new Phrase("Adresse",textNormal));
		table_soum.addCell(cell3);
		
		PdfPCell cell4 = new PdfPCell();
		cell4.addElement(new Phrase("Contact",textNormal));
		table_soum.addCell(cell4);
		
		int numero=0;
		for(int i=0;i<fournisseurs.size();i++)
		{ 
			numero=i+1;
			PdfPCell ligne1 = new PdfPCell();
			ligne1.addElement(new Phrase(numero+"",textNormal));
			table_soum.addCell(ligne1);
			
			PdfPCell ligne2 = new PdfPCell();
			ligne2.addElement(new Phrase(fournisseurs.get(i).getFournisseur().getNom(),textGras));
			table_soum.addCell(ligne2);
			
			PdfPCell ligne3 = new PdfPCell();
			ligne3.addElement(new Phrase(fournisseurs.get(i).getFournisseur().getAdresse(),textNormal));
			ligne3.addElement(new Phrase("BP: "+fournisseurs.get(i).getFournisseur().getFax(),textNormal));
			table_soum.addCell(ligne3);
			
			PdfPCell ligne4 = new PdfPCell();
			ligne4.addElement(new Phrase("Tel : "+fournisseurs.get(i).getFournisseur().getAdresse(),textNormal));
			ligne4.addElement(new Phrase("Fax : "+fournisseurs.get(i).getFournisseur().getFax(),textNormal));
			ligne4.addElement(new Phrase("Email : "+fournisseurs.get(i).getFournisseur().getEmail(),textNormal));
			ligne4.addElement(new Phrase("Site : ",textNormal));
			table_soum.addCell(ligne4);
		}
		
		soumissionnaires_liste.addElement(table_soum);
		tableObjet.addCell(soumissionnaires_liste);
		
	    ///5- Soumissionnaires
		
		PdfPCell lots = new PdfPCell();
		lots.addElement(new Phrase("5.	Les lots � soumissionner :",textNormal));
		lots.setBorder(0);
		lots.setColspan(3);
		tableObjet.addCell(lots);
		
		///5- Lieu de d�p�t
		
		PdfPCell lieudepot = new PdfPCell();
		lieudepot.addElement(new Phrase("5.	Lieu de d�p�t des offres  :",textNormal));
		lieudepot.setBorder(0);
		lieudepot.setColspan(2);
		tableObjet.addCell(lieudepot);
		
		PdfPCell lieudepot2 = new PdfPCell();
		lieudepot2.addElement(new Phrase(ac.getAdresse(),textGras));
		lieudepot2.setBorder(0);
		tableObjet.addCell(lieudepot2);
		
		///5- Lieu ouverture
		
		PdfPCell lieuouverture = new PdfPCell();
		lieuouverture.addElement(new Phrase("6.	Lieu d'ouverture des plis  :",textNormal));
		lieuouverture.setBorder(0);
		lieuouverture.setColspan(2);
		tableObjet.addCell(lieuouverture);
		
		PdfPCell lieuouverture2 = new PdfPCell();
		lieuouverture2.addElement(new Phrase(dossier.getDosLieuOuvertureDesPlis(),textGras));
		lieuouverture2.setBorder(0);
		tableObjet.addCell(lieuouverture2);
		
		///5- Date ouverture
		
		PdfPCell dateouverture = new PdfPCell();
		dateouverture.addElement(new Phrase("6.	Date d'ouverture des plis  :",textNormal));
		dateouverture.setBorder(0);
		dateouverture.setColspan(2);
		tableObjet.addCell(dateouverture);
		
		PdfPCell dateouverture2 = new PdfPCell();
		dateouverture2.addElement(new Phrase(UtilVue.getInstance().formateLaDate2(dossier.getDosDateOuvertueDesplis())+" � "+UtilVue.getInstance().formateLHeure(dossier.getDosHeureOuvertureDesPlis()),textGras));
		dateouverture2.setBorder(0);
		tableObjet.addCell(dateouverture2);
				
		tableEntete.addCell(table1_cell);
		
		
		 document.add(tableEntete);
		 document.add(new Phrase("\n"));
		 
		
		
		 
		 document.add(new Phrase("\n"));
		 

		/*  
		 * fin tab entete avis de credit
		 *  */
		 
		 /* debut détails créance */
		 
		    PdfPTable tableDetailsCreance = new PdfPTable(1);
		    tableDetailsCreance.setWidthPercentage(100);
		    
		    
		
		 					    
		     document.add(tableDetailsCreance);
		     
		     Paragraph labelDateEdition = new Paragraph("Edit� le, 04/12/2013",textNormal);
		     labelDateEdition.setAlignment(Element.ALIGN_CENTER);
		     
		     document.add(labelDateEdition);
	} catch (DocumentException e) {
		
		e.printStackTrace();
	}
    
}

public static void enteteCellStyle(PdfPCell cell){	
		 
    cell.setBorder(0);
    cell.setPadding(2);
    //cell.setHorizontalAlignment(Element.ALIGN_CENTER);

}

public void VisualiserFichier(){
//	idIframe.setHeight("500px");
//	idIframe.setWidth("100%");

	String filepath =  nomFichier;
	File f = null;
	if(ToolKermel.isWindows())
	// windows
	  f = new File(filepath.replaceAll("/", "\\\\"));
	// linux
	else
     f = new File(filepath.replaceAll("\\\\", "/"));

	org.zkoss.util.media.AMedia mymedia = null;
	try {
		mymedia = new AMedia(f, null, null);
	} catch (FileNotFoundException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}

	if (mymedia != null)
		idIframe.setContent(mymedia);
	else
		idIframe.setSrc("");

	step0.setVisible(false);
	step1.setVisible(false);
	step2.setVisible(false);
	step3.setVisible(true);
	 step4.setVisible(false);
	 
	 
}

public void  Preselections(){
	List<SygDossiersFournisseurs> fournisseursselectionnes = BeanLocator.defaultLookup(DossiersFournisseursSession.class).find(0,-1,null,appel,null);
	if(fournisseursselectionnes.size()==dossier.getDosSoumission())
	  tache.setPreselection(1);
	else
	  tache.setPreselection(0);
	
	BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).updateTache(tache);
		
}

}