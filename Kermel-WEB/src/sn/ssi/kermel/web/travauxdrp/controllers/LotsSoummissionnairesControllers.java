package sn.ssi.kermel.web.travauxdrp.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCritere;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygDossierssouscriteresevaluateurs;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygLotsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygTachesEffectues;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.LotsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.LotsSoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


@SuppressWarnings("serial")
public class LotsSoummissionnairesControllers extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer, RowRenderer, RowRendererExt{

	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
  	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	SygCritere critere=new SygCritere();
	SygCritere newcritere=new SygCritere();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuAjouter,menuNouveauCritere;
	private Div step0,step1,step2;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Textbox txtlibelle;
	List<SygDossierssouscriteresevaluateurs> notes = new ArrayList<SygDossierssouscriteresevaluateurs>();
	private int nombre=0;
	private Double noteretenue=0.0;
	List<SygDossierssouscriteres> souscriteres = new ArrayList<SygDossierssouscriteres>();
	private BigDecimal noteobtenue=new BigDecimal(0);
	private String raisonsocial=null,modele;
	private Textbox txtSoumissionnaire;
	private Grid GridLots,GridListeLots;
	private Tab TAB_LOTS,TAB_FOURISSEURS;
	private String libellelot=null;
	List<SygLotsSoumissionnaires> lotssoum = new ArrayList<SygLotsSoumissionnaires>();
	List<SygLotsSoumissionnaires> list = new ArrayList<SygLotsSoumissionnaires>();
	List<SygLotsSoumissionnaires> listlots = new ArrayList<SygLotsSoumissionnaires>();
	SygTachesEffectues tache=new SygTachesEffectues();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		addEventListener(ApplicationEvents.ON_LOTS, this);
		
    
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier!=null)
		{
			tache=(SygTachesEffectues) session.getAttribute("tache");
			createLigneLots(dossier);
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			Events.postEvent(ApplicationEvents.ON_LOTS, this, null);
			
		}
	
		 modele= (String) Executions.getCurrent().getAttribute("div");
		 if(modele!=null)
		 {
			 if(modele.equals("fournisseurs"))
				 TAB_FOURISSEURS.setSelected(true);
			 else
				 TAB_LOTS.setSelected(true);
		 }
		 else
			 TAB_FOURISSEURS.setSelected(true);
	}
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			
			 
			GridLots.setRowRenderer(this);

		    List<SygLotsSoumissionnaires> valLignes = new ArrayList<SygLotsSoumissionnaires>();
		    List<SygPlisouvertures> plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,-1,-1,-1, -1, -1, -1,raisonsocial, -1, null, null, null, null);
						
		  for (SygPlisouvertures pli : plis) {
			  SygLotsSoumissionnaires categ=new SygLotsSoumissionnaires();
		      
		      categ.setPlilibelle(pli.getRetrait().getNomSoumissionnaire());
		       List<SygLotsSoumissionnaires> vals = BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).find(0, -1, dossier, pli, null,-1,null,-1,-1, -1, libellelot);
		   	   if(vals.size()!=0)   
			     valLignes.add(categ);	
	 		       for (SygLotsSoumissionnaires valLigne : vals) {
		 		       valLignes.add(valLigne);
		 		      
			        }
	 		      GridLots.setModel(new ListModelList(valLignes));	
		  		

		  }
		     	
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_LOTS)) {
			 GridListeLots.setRowRenderer(new LotsRenderer());

			  List<SygLotsSoumissionnaires> valLignes = new ArrayList<SygLotsSoumissionnaires>();
			  List<SygLots> lots = BeanLocator.defaultLookup(LotsSession.class).find(0,-1,dossier,null);

			  for (SygLots critere : lots) {
				SygLotsSoumissionnaires categ=new SygLotsSoumissionnaires();
				      
				categ.setPlilibelle(critere.getLibelle());
			    List<SygLotsSoumissionnaires> vals = BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).find(0, -1, dossier, null, critere,-1,null,-1,-1, -1, libellelot);
			  	 
			   	   if(vals.size()!=0)   
				     valLignes.add(categ);	
		 		   for (SygLotsSoumissionnaires valLigne : vals) {
			 		   valLignes.add(valLigne);
			 		   
				    }
			  }
			  GridListeLots.setModel(new ListModelList(valLignes));	
		} 
		
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygDossierssouscriteres dossiers = (SygDossierssouscriteres) data;
		item.setValue(dossiers);

		 Listcell cellLibelle = new Listcell(dossiers.getCritere().getLibelle());
		 cellLibelle.setParent(item);
		 
		
	}

	
	public class PiecesRenderer implements ListitemRenderer{
		
		
		
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygCritere criteres = (SygCritere) data;
		item.setValue(criteres);

		 Listcell cellLibelle = new Listcell(criteres.getLibelle());
		 cellLibelle.setParent(item);
		 
		
	
	}
	}

	public void onClick$menuAjouter()
	{
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
		Events.postEvent(ApplicationEvents.ON_CRITERES, this, null);
	}
	public void onClick$menuFermer()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
		
	}
	

	
	 
	 private boolean checkFieldConstraints() {
			try {
				
				 if(txtlibelle.getValue().equals(""))
			     {
				 errorComponent = txtlibelle;
				 errorMsg = Labels.getLabel("kermel.referentiel.common.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				 lbStatusBar.setStyle(ERROR_MSG_STYLE);
				 lbStatusBar.setValue(errorMsg);
				 throw new WrongValueException (errorComponent, errorMsg);
			    }
				
				
				return true;
					
			}
			catch (Exception e) {
				errorMsg = Labels.getLabel("kermel.erreur.champobligatoire") + ": " + e.toString()
				+ " [checkFieldConstraints]";
				errorComponent = null;
				return false;

				
			}
			
		}
	 
	 public void onClick$menuFermerNouveau()
		{
			step0.setVisible(true);
			step1.setVisible(false);
			step2.setVisible(false);
			
		}
	 
		
	 
	  private void createLigneLots(SygDossiers dossier) {

		    
			List<SygLots> lots = BeanLocator.defaultLookup(LotsSession.class).find(0,-1,dossier, null);
			 List<SygPlisouvertures> plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,-1,-1,-1, -1, -1, -1,null, -1, null, null, null, null);
			
			for (SygPlisouvertures pli : plis) {
				
				for (int i = 0; i < lots.size(); i++) {
					lotssoum=BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).find(0, -1, dossier, pli, lots.get(i),-1,null,-1,-1, -1, null);
					
					if(lotssoum.size()==0)
					{
						SygLotsSoumissionnaires lot = new SygLotsSoumissionnaires();
					
					//	lot.setPlilraisonsociale(pli.getRetrait().getNomSoumissionnaire());
						lot.setDossier(dossier);
						lot.setPlis(pli);
						lot.setLot(lots.get(i));
						lot.setPlilraisonsociale(pli.getRetrait().getNomSoumissionnaire());
						lot.setPliladresseMail(pli.getRetrait().getEmail());
						lot.setPlilclassementgeneral(0);
						lot.setPlilclassementechnique(0);
						lot.setPlilEtatPreselection(0);
						lot.setPlilEtatExamenPreliminaire(0);
						lot.setPlilcritereQualification(0);
						lot.setPlilattributaireProvisoire(0);
						lot.setPlilValide(0);
						lot.setPlilsrixevalue(new BigDecimal(0));
						lot.setPlilmontantoffert(new BigDecimal(0));
						lot.setPlilscoretechnique(new BigDecimal(0));
						lot.setPlilscorefinancier(new BigDecimal(0));
						lot.setPlilscoretechniquepondere(new BigDecimal(0));
						lot.setPlilscorefinancierpondere(new BigDecimal(0));
						lot.setPlilscorefinal(new BigDecimal(0));
						lot.setPlilmontantoffert(new BigDecimal(0));
						if(dossier.getDosLotDivisible().equals("NON"))
						  lot.setLotsoumis("oui");
						else
						  lot.setLotsoumis("non");
						lot.setLotrecu("non");
						
						
						
						BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).save(lot);
					}
				}
			
				
			   }
			
			}
	     
	     
	     @Override
	 	public Row newRow(Grid grid) {
	 		// Create EditableRow instead of Row(default)
	 		Row row = new EditableRow();
	 		row.applyProperties();
	 		return row;
	 	}

	 	@Override
	 	public Component newCell(Row row) {
	 		return null;// Default Cell
	 	}

	 	@Override
	 	public int getControls() {
	 		return RowRendererExt.DETACH_ON_RENDER; // Default Value
	 	}

	 	@Override
	 	public void render(Row row, Object data, int index) throws Exception {
	 		final SygLotsSoumissionnaires lots = (SygLotsSoumissionnaires) data;
	 		
	 		final EditableRow editRow = (EditableRow) row;
	 		
	 		if (lots.getPlilibelle() != null) {
	 			final EditableDiv libelle = new EditableDiv(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial")+":  " +lots.getPlilibelle(), false);
	 			libelle.txb.setReadonly(true);
	 			
	 			libelle.setHeight("30px");
	 			libelle.setStyle("color:#000;");
	 			libelle.setParent(editRow);
	 			
	 			
	 			
	 			final EditableDiv notes = new EditableDiv("", false);
	 			notes.setParent(editRow);
	 			
	 			
	 		}
	      else {
	 		   	final EditableDiv libelle = new EditableDiv( lots.getLot().getLibelle(), false);
	 		   libelle.setStyle("margin-left:10px;color:#000;"); 
	 	 	    libelle.txb.setReadonly(true);
	 		    libelle.setParent(editRow);
	 		
	 		   final EditableDiv deuxP = new EditableDiv("", false);
				deuxP.setAlign("center");
				deuxP.setStyle("font-weight:bold;color:green");
				deuxP.setParent(editRow);
				
				// pr boutton radio
				final Radiogroup groupe = new Radiogroup();
				groupe.setParent(deuxP);
				

				final EditableRadiosDiv deux1 = new EditableRadiosDiv("oui",false);
				deux1.setAlign("right");
				deux1.radio.setLabel(Labels.getLabel("kermel.common.list.oui"));
				deux1.radio.setValue("oui");
				deux1.setStyle("font-weight:bold;color:green");
				deux1.radio.setParent(groupe);
				
				final EditableRadiosDiv deux2 = new EditableRadiosDiv("non",false);
				deux2.setAlign("right");
				deux2.radio.setLabel(Labels.getLabel("kermel.common.list.non"));
				deux2.radio.setValue("non");
				deux2.setStyle("font-weight:bold;color:green");
				deux2.radio.setParent(groupe);
				
				if (lots.getLotsoumis().equalsIgnoreCase("oui")) 
					deux1.radio.setChecked(true);
				else
					deux2.radio.setChecked(true);
				
				groupe.addEventListener(Events.ON_CHECK, new EventListener() {

					@Override
					public void onEvent(Event arg0) throws Exception {
						// TODO Auto-generated method stub
						if (groupe.getSelectedItem()!=null) {
							lots.setLotsoumis((String) groupe.getSelectedItem().getValue());
						}
					}
					
				});

	            addlist(lots);
				
	 	

	 	}
	 	}
	 	
	 	  public void  onClick$btnRechercher(){
		 		if((txtSoumissionnaire.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial")))||(txtSoumissionnaire.getValue().equals("")))
		 		 {
		 			raisonsocial=null;
		 		 }
		 		else
		 		 {
		 			raisonsocial=txtSoumissionnaire.getValue();
		 			
		 		 }
		 	
		 		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		 		}
	  	 public void onFocus$txtSoumissionnaire(){
	 			if(txtSoumissionnaire.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial"))){
	 				txtSoumissionnaire.setValue("");
	 			}		 
	 			}
	 		public void onBlur$txtSoumissionnaire(){
	 			if(txtSoumissionnaire.getValue().equalsIgnoreCase("")){
	 				txtSoumissionnaire.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial"));
	 			}		 
	 			}
	 		public void onOK$txtSoumissionnaire(){
	 			onClick$btnRechercher();	
	 		}
	 		public void onOK$btnRechercher(){
	 			onClick$btnRechercher();	
	 		}
	 		
	 		public class LotsRenderer implements RowRenderer, RowRendererExt{
	 		   @Override
	 		 	public Row newRow(Grid grid) {
	 		 		// Create EditableRow instead of Row(default)
	 		 		Row row = new EditableRow();
	 		 		row.applyProperties();
	 		 		return row;
	 		 	}

	 		 	@Override
	 		 	public Component newCell(Row row) {
	 		 		return null;// Default Cell
	 		 	}

	 		 	@Override
	 		 	public int getControls() {
	 		 		return RowRendererExt.DETACH_ON_RENDER; // Default Value
	 		 	}

	 	 	@Override
	 	 	public void render(Row row, Object data, int index) throws Exception {
	 	 		
	 	 		final SygLotsSoumissionnaires  lots = (SygLotsSoumissionnaires ) data;
	 	 		
	 	 		final EditableRow editRow = (EditableRow) row;
	 		 		
	 		 		if (lots.getPlilibelle() != null) {
	 		 			final EditableDiv libelle = new EditableDiv(Labels.getLabel("kermel.plansdepassation.proceduresmarches.travauxdrp.lotslot")+":  " +lots.getPlilibelle(), false);
	 		 			libelle.txb.setReadonly(true);
	 		 		
	 		 			libelle.setHeight("30px");
	 		 			libelle.setStyle("color:#000;");
	 		 			libelle.setParent(editRow);
	 		 			
	 		 			
	 		 			
	 		 			final EditableDiv notes = new EditableDiv("", false);
	 		 			notes.setParent(editRow);
	 		 			
	 		 		
	 		 			
	 		 		}
	 		 		else
	 		 		{
	 		 		   	final EditableDiv libelle = new EditableDiv( lots.getPlis().getRaisonsociale(), false);
	 		 		    libelle.setStyle("margin-left:10px;color:#000;");
	 		 	 	    libelle.txb.setReadonly(true);
	 		 		    libelle.setParent(editRow);
	 		 		
	 		 		  final EditableDiv deuxP = new EditableDiv("", false);
	 					deuxP.setAlign("center");
	 					deuxP.setStyle("font-weight:bold;color:green");
	 					deuxP.setParent(editRow);
	 					
	 					// pr boutton radio
	 					final Radiogroup groupe = new Radiogroup();
	 					groupe.setParent(deuxP);
	 					

	 					final EditableRadiosDiv deux1 = new EditableRadiosDiv("oui",false);
	 					deux1.setAlign("right");
	 					deux1.radio.setLabel(Labels.getLabel("kermel.common.list.oui"));
	 					deux1.radio.setValue("oui");
	 					deux1.setStyle("font-weight:bold;color:green");
	 					deux1.radio.setParent(groupe);
	 					
	 					final EditableRadiosDiv deux2 = new EditableRadiosDiv("non",false);
	 					deux2.setAlign("right");
	 					deux2.radio.setLabel(Labels.getLabel("kermel.common.list.non"));
	 					deux2.radio.setValue("non");
	 					deux2.setStyle("font-weight:bold;color:green");
	 					deux2.radio.setParent(groupe);
	 					
	 					if (lots.getLotsoumis().equalsIgnoreCase("oui")) 
	 						deux1.radio.setChecked(true);
	 					else
	 						deux2.radio.setChecked(true);
	 					
	 					groupe.addEventListener(Events.ON_CHECK, new EventListener() {

	 						@Override
	 						public void onEvent(Event arg0) throws Exception {
	 							// TODO Auto-generated method stub
	 							if (groupe.getSelectedItem()!=null) {
	 								lots.setLotsoumis((String) groupe.getSelectedItem().getValue());
	 							}
	 						}
	 						
	 					});

	 					addlistLots(lots);
	 		    	
	 		 		
	 		 
	 		 	}
	 	      }
	 	 }
		
	 		
	 		public void onClick$menuValider()
	 		{
	 			for (SygLotsSoumissionnaires part : list) {
	 				BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).update(part);
	 				
	 			}
	 			LotsSoumis();
	 			Executions.getCurrent().setAttribute("div", "fournisseurs");
	 			session.setAttribute("libelle", "lotssoumissionnaires");
				loadApplicationState("suivi_travaux_drp");
	 			
	 		}
	 		
	 		 public void addlist(SygLotsSoumissionnaires ps) {

	 			list.add(ps);

	 		}
	 		 
	 		 public void addlistLots(SygLotsSoumissionnaires ps) {


		 	 		listlots.add(ps);

			 		  }
		 		 
	 		public void onClick$menuValiderlot()
	 		{
	 			for (SygLotsSoumissionnaires part : listlots) {
	 				BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).update(part);
	 				
	 			}
	 			LotsSoumis();
	 			Executions.getCurrent().setAttribute("div", "lots");
	 			  session.setAttribute("libelle", "lotssoumissionnaires");
					loadApplicationState("suivi_travaux_drp");
	 			
	 		}
	 		
	 		public void  LotsSoumis(){
	 			List<SygLotsSoumissionnaires>	lotssoumissionnaires = BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).find(0, -1, dossier, null, null,-1,"oui",-1,-1, -1, null);
				
	 			 if(lotssoumissionnaires.size()==0)
	 			   tache.setLotssoumis(0);
	 			 else
	 			   tache.setLotssoumis(1);
	 					    
	 			BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).updateTache(tache);
	 		 }
}