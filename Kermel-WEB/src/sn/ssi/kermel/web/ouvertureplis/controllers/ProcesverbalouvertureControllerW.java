package sn.ssi.kermel.web.ouvertureplis.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTShd;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STMerge;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STShd;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STVerticalJc;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossiersEvaluateurs;
import sn.ssi.kermel.be.entity.SygLotsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygMembresCommissionsMarches;
import sn.ssi.kermel.be.entity.SygObservateursIndependants;
import sn.ssi.kermel.be.entity.SygPresenceouverture;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygRepresentantsServicesTechniques;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.entity.SygTachesEffectues;
import sn.ssi.kermel.be.entity.SysParametresGeneraux;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DocumentsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersEvaluateursSession;
import sn.ssi.kermel.be.passationsmarches.ejb.LotsSoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.ObservateursIndependantsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.Presence0uvertureSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RepresentantsServicesTechniquesSession;
import sn.ssi.kermel.be.referentiel.ejb.MembresCommissionsMarchesSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class ProcesverbalouvertureControllerW  extends AbstractWindow implements EventListener, AfterCompose   {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Listbox lstListe,lstBailleur;
	private Paging pgPagination,pgBailleur;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference,libellebailleur=null;
    private int parent;
    List<SygDocuments> documents = new ArrayList<SygDocuments>();
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtVersionElectronique,txtVersionElectronique2,txtMotif;
    SygBailleurs bailleur=null;
    private Decimalbox dcmontantprevu;
    private Label lbStatusBar,lbStatusBar2;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idbailleur;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygDocuments document=new SygDocuments();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private String nomFichier,nomFichier2;
	private  String cheminDossier;
	UtilVue utilVue = UtilVue.getInstance();
	private Datebox dtouverture,dtstopprocedure;
	SygDossiers dossier=new SygDossiers();
	SygDossiers lastdossier=new SygDossiers();
	private Menuitem menuValider,menuValider2,menuFructueuse;
	private Image image,image2;
	private Label lbltitre,lbldeuxpoints,lbltitre2,lbldeuxpoints2;
	private Div step0,step1,step2;
	private Iframe idIframe;
	private String extension,images,fichier;
	public static final String EDIT_URL = "http://"+UIConstants.IP_SSI+":"+UIConstants.PORT_SSI+"/EtatsSygmap/OuverturePlisPDFServlet";
	private Menuitem menuEditer;
//	List<SygPlisouvertures> registredepot = new ArrayList<SygPlisouvertures>();
	protected static final String EDIT_URL_Birt = "http://" + BeConstants.IP_SYGMAP + ":" + BeConstants.PORT_SYGMAP + "/birt/frameset?__report=benin/";
	private String format,gestion;
	SysParametresGeneraux param;
	SygTachesEffectues tache=new SygTachesEffectues();
	List<SygMembresCommissionsMarches> membrescommissions = new ArrayList<SygMembresCommissionsMarches>();
	List<SygRepresentantsServicesTechniques> representantsservicestechniques = new ArrayList<SygRepresentantsServicesTechniques>();
    List<SygObservateursIndependants> observateursindependants = new ArrayList<SygObservateursIndependants>();
    List<SygPresenceouverture> representantssoumissionnaires = new ArrayList<SygPresenceouverture>();	
    List<SygRetraitregistredao> retraits = new ArrayList<SygRetraitregistredao>();
    List<SygDossiersEvaluateurs> commissionstechniques =new ArrayList<SygDossiersEvaluateurs>();	
    List<SygLotsSoumissionnaires> lotssoummissionnaires= new ArrayList<SygLotsSoumissionnaires>();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier==null)
		{
			menuValider.setDisabled(true);
			menuFructueuse.setDisabled(true);
			menuEditer.setDisabled(true);
			image.setVisible(false);
		}
		else
		{
			
			tache=(SygTachesEffectues) session.getAttribute("tache");
			//registredepot= BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,-1,-1,-1, -1, -1,-1, null, -1,null,null,null, null);
			if(tache.getRegistredepot()==0||appel.getApoDatepvouverturepli()!=null)
			   menuValider.setDisabled(true);
			
			if(tache.getRegistredepot()==0||appel.getApoDatepvouverturepli()!=null||appel.getDatestopprocedure()!=null)
			{
				  // menuValider2.setDisabled(true);
				   menuFructueuse.setDisabled(true);
			}
			
			if(appel.getDatestopprocedure()!=null)	
			{
				step0.setVisible(false);
				step1.setVisible(false);
				step2.setVisible(true);
				menuValider2.setVisible(false);
				infos2(appel);
			}
			else
			{
				step0.setVisible(true);
				step1.setVisible(false);
				step2.setVisible(false);
				infos(dossier,appel);
			}
			gestion=Integer.toString(dossier.getRealisation().getPlan().getAnnee());
			membrescommissions=BeanLocator.defaultLookup(MembresCommissionsMarchesSession.class).find(0,-1,null,null,dossier.getAutorite(),gestion,-1,-1);
			observateursindependants = BeanLocator.defaultLookup(ObservateursIndependantsSession.class).find(0,-1,dossier,-1);
			representantsservicestechniques = BeanLocator.defaultLookup(RepresentantsServicesTechniquesSession.class).find(0,-1,dossier,-1);
			representantssoumissionnaires = BeanLocator.defaultLookup(Presence0uvertureSession.class).find(0,-1,dossier,null,null,-1);
			commissionstechniques = BeanLocator.defaultLookup(DossiersEvaluateursSession.class).find(0,-1,dossier);
			lotssoummissionnaires = BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).find(0, -1, dossier, null, null,-1,"oui",-1,-1, -1, null);
			
			
		}
//		param=BeanLocator.defaultLookup(ParametresGenerauxSession.class).findById(UIConstants.);
//		cheminDossier=param.getLibelle();
		cheminDossier = UIConstants.DOCSPATH;
		
	}

	public void infos(SygDossiers dossier,SygAppelsOffres appel) {
		documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossier, appel,UIConstants.PARAM_TYPEDOCUMENTS_PVOUVERTURE,null, null);
		
		if(documents.size()>0)
		{
			extension=documents.get(0).getNomFichier().substring(documents.get(0).getNomFichier().length()-3,  documents.get(0).getNomFichier().length());
			 if(extension.equalsIgnoreCase("pdf"))
				 images="/images/icone_pdf.png";
			 else  
				 images="/images/word.jpg";
			 
			image.setVisible(true);
			image.setSrc(images);
			lbldeuxpoints.setValue(":");
			lbltitre.setValue(Labels.getLabel("sygmap.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.procesverbalouverture.versionfichier"));
			dtouverture.setValue(documents.get(0).getAppel().getApoDatepvouverturepli());
			fichier=documents.get(0).getNomFichier();
		
		}
	}
	public void onClick$btnChoixFichier() {
		//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
			if (ToolKermel.isWindows())
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectronique.setValue(nomFichier);
		}
	
private boolean checkFieldConstraints() {
		
		try {
		
			if(dtouverture.getValue()==null)
		     {
               errorComponent = dtouverture;
               errorMsg = Labels.getLabel("sygmap.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.procesverbalouverture.date")+": "+Labels.getLabel("sygmap.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		
			if(txtVersionElectronique.getValue().equals(""))
		     {
          errorComponent = txtVersionElectronique;
          errorMsg = Labels.getLabel("sygmap.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.procesverbalouverture.ficher")+": "+Labels.getLabel("sygmap.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("sygmap.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	
		public void onClick$menuValider() {
			if(checkFieldConstraints())
			{
				documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossier, appel,UIConstants.PARAM_TYPEDOCUMENTS_PVOUVERTURE,null, null);
				if(documents.size()>0)
				  document=documents.get(0);
				document.setAppel(appel);
				document.setDossier(dossier);
				document.setNomFichier(txtVersionElectronique.getValue());
				document.setTypeDocument(UIConstants.PARAM_TYPEDOCUMENTS_PVOUVERTURE);
				document.setLibelle(Labels.getLabel("sygmap.plansdepassation.proceduresmarches.documents.pvdouverture"));
				document.setDate(new Date());
				document.setHeure(Calendar.getInstance().getTime());
				//document.setOrigine(UIConstants.);
				if(documents.size()==0)
				{
					BeanLocator.defaultLookup(DocumentsSession.class).save(document);
				}
				else
				{
					BeanLocator.defaultLookup(DocumentsSession.class).update(document);
				}
				appel.setApoDatepvouverturepli(dtouverture.getValue());
				appel.setApofichierpv(txtVersionElectronique2.getValue());
				BeanLocator.defaultLookup(AppelsOffresSession.class).update(appel);
				
				
				
				
				session.setAttribute("libelle", "procesverbalouverture");
				loadApplicationState("suivi_procedure_passation");
			}
		}
		
		public void onClick$image() {
			onInfosImage(fichier);
		}
		
		public org.zkoss.util.media.AMedia fetchFile(File file) {

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(file, null, null);
				return mymedia;
			} catch (Exception e) {

				e.printStackTrace();
				return null;
			}

		}
		public void onClick$menuFermer() {
			if(appel.getDatestopprocedure()!=null)	
			{
				step0.setVisible(false);
				step1.setVisible(false);
				step2.setVisible(true);
			}
			else
			{
				step0.setVisible(true);
				step1.setVisible(false);
				step2.setVisible(false);
			}
		}
		
		 public void onClick$menuEditer() throws IOException{
			
			String  chemin= UIConstants.PATH_PJ;
			String fichiernom="procesverbal"+realisation.getReference()+".docx";
			nomFichier = chemin  + fichiernom;
			 if (ToolKermel.isWindows()) {
					nomFichier = nomFichier.replaceAll("/", "\\\\");
				} else {
					nomFichier = nomFichier.replaceAll("\\\\", "/");
					
				}
			 File file = null; 
	           FileOutputStream fos = null; 
	           XWPFDocument document = null; 
	           XWPFParagraph para = null; 
	           XWPFRun run = null; 
	           try { 
	               // Create the first paragraph and set it's text. 
	               document = new XWPFDocument(); 
	               para = document.createParagraph(); 

	               para.setAlignment(ParagraphAlignment.CENTER); 
	               para.setStyle("width:200px");
	               para.setSpacingAfter(10); 

	               para.setSpacingAfterLines(10);
	               run = para.createRun(); 
	               
	             
	               XWPFParagraph   para_entete = document.createParagraph(); 
	               XWPFRun run_entete = para_entete.createRun(); 
	               run_entete.setText("      REPUBLIQUE DU BENIN  "); 
	               run_entete.addBreak();
	               run_entete.setText("                ---------  ");
	               run_entete.addBreak();
	               run_entete.setText("   FRATERNITE-JUSTICE-TRAVAIL");
	               run_entete.addBreak();
	               run_entete.addBreak();
	               run_entete.setText("                             PROCES-VERBAL DE LA SEANCE D�OUVERTURE DES OFFRES");
	 	          
	               run_entete.addBreak();
	               run_entete.setBold(true);
	               
	               XWPFParagraph   para1 = document.createParagraph(); 
	               XWPFRun run1 = para1.createRun(); 
	               run1.setText(" A. Identification de l�Autorit� Contractante "); 
	               run1.setBold(true);
	               
	               XWPFParagraph   para2 = document.createParagraph(); 
	               XWPFRun run2_1 = para2.createRun(); 
	               run2_1.setText("Minist�re, collectivit� territoriale, entreprise publique  ou �tablissement concern�: "); 
	               XWPFRun run2_2 = para2.createRun(); 
	               run2_2.setText(realisation.getPlan().getAutorite().getDenomination()+"."); 
	               run2_2.setBold(true);
	               
	               XWPFParagraph   para3 = document.createParagraph(); 
	               XWPFRun run3 = para3.createRun(); 
	               run3.setText("(Nom, Adresse, Direction, Sous-direction, Bureau, T�l�copie, T�l�phone, E-mail)"); 
	               
	               XWPFParagraph   para4 = document.createParagraph(); 
	               XWPFRun run4_1 = para4.createRun(); 
	               run4_1.setText("Objet de l�appel d�offres ou de la consultation: "); 
	               XWPFRun run4_2 = para4.createRun(); 
	               run4_2.setText(realisation.getLibelle()+"."); 
	               run4_2.setBold(true);
	               
	               XWPFParagraph   para5 = document.createParagraph(); 
	               XWPFRun run5_1 = para5.createRun(); 
	               run5_1.setText("Avis d�appel public � la concurrence n�: "); 
	               XWPFRun run5_2 = para5.createRun(); 
	               run5_2.setText(realisation.getReference()+" du "+utilVue.getInstance().formateLaDate2(appel.getApodatecreation())+"."); 
	               run5_2.setBold(true);
	               
	               XWPFParagraph   para6 = document.createParagraph(); 
	               XWPFRun run6 = para6.createRun(); 
	               run6.setText("Organe(s) et date(s) de parution de l�avis :"); 
	             
	               XWPFParagraph   para7 = document.createParagraph(); 
	               XWPFRun run7 = para7.createRun(); 
	               run7.setText(" B. Composition et fonctionnement de la commission de passation des march�s publics "); 
	               run7.setBold(true);
	               
	               XWPFParagraph   para8 = document.createParagraph(); 
	               XWPFRun run8 = para8.createRun(); 
	               run8.setText("   � La composition de la commission de passation des march�s publics a �t� fix�e par d�cision du"); 
	               run8.addBreak();
	               run8.setText("   � Date(s) de la (des) r�union(s):"); 
	               
	               XWPFTable table1 = document.createTable();
	               table1.setWidth(800);
	               XWPFTableRow tableRowOne = table1.getRow(0);
	               tableRowOne.getCell(0).setText("       ");
	               tableRowOne.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf((long)26000)); 
	               
	               XWPFParagraph   para9 = document.createParagraph(); 
	               XWPFRun run9 = para9.createRun(); 
	               run9.addBreak();
	               run9.setText("   � Membres de la Commission de passation des march�s publics : (placer le nom du Pr�sident en premier)"); 
	               
	               //create table
	               XWPFTable table2 = document.createTable(1, 4);
	               table2.setWidth(100000000);
	               XWPFTableRow row1_table2 = table2.getRow(0);
//	               row1_table2.getCell(0).setText("Nom, pr�noms          ");
//	               row1_table2.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf((long)9000)); 
	               setCellText(row1_table2.getCell(0), "Nom, pr�noms", 9000, true, 6, "CCCCCC");
	               setCellText(row1_table2.getCell(1), "Qualit�", 5000, true, 6, "CCCCCC");
	               setCellText(row1_table2.getCell(2), "Signature", 5000, true, 6, "CCCCCC");
	               setCellText(row1_table2.getCell(3), "Absent mais convoqu� le", 7000, true, 6, "CCCCCC");
	               
	               for(int i=0;i<membrescommissions.size();i++)
	               {
	            	   XWPFTableRow tableRowTwo = table2.createRow();
	            	   tableRowTwo.getCell(0).setText(membrescommissions.get(i).getPrenom()+"  "+membrescommissions.get(i).getNom());
	            	   tableRowTwo.getCell(1).setText("");
	            	   tableRowTwo.getCell(2).setText("");
	            	   tableRowTwo.getCell(3).setText("");

	               }
	            
	               XWPFParagraph   para10 = document.createParagraph(); 
	               XWPFRun run10 = para10.createRun(); 
	               run10.addBreak();
	               run10.setText("   � Le quorum est atteint :"); 
	               run10.addBreak();
	               run10.setText("     (Le quorum doit �tre atteint non seulement � l'ouverture de la s�ance mais encore lors des d�bats et du vote de la commission)"); 
	               run10.addBreak();
	               run10.setText("     La commission peut, ne peut pas, (rayer la  mention inutile) valablement d�lib�rer."); 
	              
	               XWPFParagraph   para11 = document.createParagraph(); 
	               XWPFRun run11 = para11.createRun(); 
	               run11.setText(" C. Soumissionnaires ou leurs repr�sentants / et autres  (sur pr�sentation d�un titre justificatif) "); 
	               run11.setBold(true);
	               
	               XWPFTable table3 = document.createTable(1, 4);
	               XWPFTableRow table3_row1 = table3.getRow(0);
	               setCellText(table3_row1.getCell(0), "Nom, pr�noms", 9000, true, 6, "CCCCCC");
	               setCellText(table3_row1.getCell(1), "Qualit�", 5000, true, 6, "CCCCCC");
	               setCellText(table3_row1.getCell(2), "Entreprise ou soci�t� repr�sent�e", 12000, true, 6, "CCCCCC");
	               setCellText(table3_row1.getCell(3), "Contact", 9000, true, 6, "CCCCCC");
	               
	                for(int i=0;i<representantssoumissionnaires.size();i++)
	               {
	            	   XWPFTableRow tableRowTwo = table3.createRow();
	            	   tableRowTwo.getCell(0).setText(representantssoumissionnaires.get(i).getPrenomrepresentant());
	            	   tableRowTwo.getCell(1).setText("");
	            	   tableRowTwo.getCell(2).setText(representantssoumissionnaires.get(i).getPlis().getRetrait().getNomSoumissionnaire());
	            	   tableRowTwo.getCell(3).setText(representantssoumissionnaires.get(i).getPrenomrepresentant());

	               }
	            
	               XWPFParagraph   para12 = document.createParagraph(); 
	               XWPFRun run12 = para12.createRun(); 
	               run12.addBreak();
	               run12.setText(" D. Repr�sentants du service technique b�n�ficiaire de l�autorit� contractante "); 
	               run12.setBold(true);
	               
	               XWPFTable table4 = document.createTable(1,4);
	               table4.setWidth(100000000);
	               XWPFTableRow row1_table4 = table4.getRow(0);
	               setCellText(row1_table4.getCell(0), "Nom, pr�noms", 9000, true, 6, "CCCCCC");
	               setCellText(row1_table4.getCell(1), "Qualit�", 5000, true, 6, "CCCCCC");
	               setCellText(row1_table4.getCell(2), "Signature", 5000, true, 6, "CCCCCC");
	               setCellText(row1_table4.getCell(3), "Absent mais convoqu� le", 7000, true, 6, "CCCCCC");
	               
//	               row1_table4.getCell(0).setText("Nom, pr�noms          ");
//	               row1_table4.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf((long)9000)); 
//	               row1_table4.addNewTableCell().setText("Qualit�     ");
//	               row1_table4.getCell(1).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf((long)5000)); 
//	               row1_table4.addNewTableCell().setText("Signature");
//	               row1_table4.getCell(2).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf((long)5000)); 
//	               row1_table4.addNewTableCell().setText("Absent mais convoqu� le");
//	               row1_table4.getCell(3).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf((long)7000)); 
	              
	               for(int i=0;i<representantsservicestechniques.size();i++)
	               {
	            	   XWPFTableRow tableRowTwo = table4.createRow();
	            	   tableRowTwo.getCell(0).setText(representantsservicestechniques.get(i).getRepresentant());
	            	   tableRowTwo.getCell(1).setText(representantsservicestechniques.get(i).getQualite());
	            	   tableRowTwo.getCell(2).setText("");
	            	   if(representantsservicestechniques.get(i).getDateconvocation()!=null)
	            	       tableRowTwo.getCell(3).setText(UtilVue.getInstance().formateLaDate2(representantsservicestechniques.get(i).getDateconvocation()));
	            	   else
	            		   tableRowTwo.getCell(3).setText("");
	               }
	            
	               XWPFParagraph   para13 = document.createParagraph(); 
	               XWPFRun run13 = para13.createRun(); 
	               run13.addBreak();
	               run13.setText(" E. Observateur Ind�pendant/ personnes ressources "); 
	               run13.setBold(true);
	               
	               XWPFTable table5 = document.createTable(1,4);
	               table5.setWidth(100000000);
	               XWPFTableRow row1_table5 = table5.getRow(0);
	               setCellText(row1_table5.getCell(0), "Nom, pr�noms", 9000, true, 6, "CCCCCC");
	               setCellText(row1_table5.getCell(1), "Qualit�", 5000, true, 6, "CCCCCC");
	               setCellText(row1_table5.getCell(2), "Signature", 5000, true, 6, "CCCCCC");
	               setCellText(row1_table5.getCell(3), "Absent mais convoqu� le", 7000, true, 6, "CCCCCC");
	                
	               for(int i=0;i<observateursindependants.size();i++)
	               {
	            	   XWPFTableRow tableRowTwo = table5.createRow();
	            	   tableRowTwo.getCell(0).setText(observateursindependants.get(i).getRepresentant());
	            	   tableRowTwo.getCell(1).setText(observateursindependants.get(i).getQualite());
	            	   tableRowTwo.getCell(2).setText("");
	            	   if(observateursindependants.get(i).getDateconvocation()!=null)
	            	    tableRowTwo.getCell(3).setText(UtilVue.getInstance().formateLaDate2(observateursindependants.get(i).getDateconvocation()));
	            	   else
	            		   tableRowTwo.getCell(3).setText("");
	               }
	               
	               XWPFParagraph   para14 = document.createParagraph(); 
	               XWPFRun run14 = para14.createRun(); 
	               run14.addBreak();
	               run14.setText(" F. Commission technique "); 
	               run14.setBold(true);
	               
	               XWPFTable table6 = document.createTable(1,5);
	               table5.setWidth(100000000);
	               XWPFTableRow row1_table6 = table6.getRow(0);
	               setCellText(row1_table6.getCell(0), "Pr�nom", 5000, true, 6, "CCCCCC");
	               setCellText(row1_table6.getCell(1), "Nom", 2000, true, 6, "CCCCCC");
	               setCellText(row1_table6.getCell(2), "Fonction", 6000, true, 6, "CCCCCC");
	               setCellText(row1_table6.getCell(3), "T�l�phone", 2000, true, 6, "CCCCCC");
	               setCellText(row1_table6.getCell(4), "Email", 2000, true, 6, "CCCCCC");
	               
	                         
	               for(int i=0;i<commissionstechniques.size();i++)
	               {
	            	   XWPFTableRow tableRowTwo = table6.createRow();
	            	   tableRowTwo.getCell(0).setText(commissionstechniques.get(i).getEvaluateur().getPrenom());
	            	   tableRowTwo.getCell(1).setText(commissionstechniques.get(i).getEvaluateur().getNom());
	            	   tableRowTwo.getCell(2).setText(commissionstechniques.get(i).getEvaluateur().getFonction());
	            	   tableRowTwo.getCell(3).setText(commissionstechniques.get(i).getEvaluateur().getTelephone());
	            	   tableRowTwo.getCell(4).setText(commissionstechniques.get(i).getEvaluateur().getEmail());

	               }
	            
	               XWPFParagraph   para15 = document.createParagraph(); 
	               XWPFRun run15 = para15.createRun(); 
	               run15.addBreak();
	               run15.setText(" F. D�cision de la Commission de passation des march�s publics "); 
	               run15.setBold(true);
	               
	               XWPFTable table7 = document.createTable(3,9);
	              
	               setCellText(table7.getRow(0).getCell(0), "N� d�ordre au registre sp�cial des d�p�ts", 2000, true, 6, "CCCCCC");
	               mergeCellsVertically(table7, 0, 0, 2);
	               
	              // row1_table7.getCell(1).setText("Nom du candidat ou des candidats group�s. Souligner le nom du mandataire");
	               setCellText(table7.getRow(0).getCell(1), "Nom du candidat ou des candidats group�s. Souligner le nom du mandataire", 2000, true, 6, "CCCCCC");
	               mergeCellsVertically(table7, 1, 0, 2);
	               
	             
	               setCellText(table7.getRow(0).getCell(2), "D�cisions d�examen", 6000, true, 6, "CCCCCC");
		           mergeCellsHorizontal(table7, 0, 2,7); 
	               
	               setCellText(table7.getRow(0).getCell(8), "Motifs (avec renvoi � l'article du code des march�s publics ad�quat)", 2000, true, 6, "CCCCCC");
	                mergeCellsVertically(table7, 8, 0, 2);

	              
	                setCellText(table7.getRow(1).getCell(2), "Pli non ouvert)", 2000, true, 6, "CCCCCC");
		            mergeCellsVertically(table7, 2, 1, 2);
	                        
		            setCellText(table7.getRow(1).getCell(3), "Contenu des Plis ouverts (mentionner P : document pr�sent ; M : document manquant)", 7000, true, 6, "CCCCCC");
		            mergeCellsHorizontal(table7, 1, 3,7);      
	                 
		            setCellText(table7.getRow(2).getCell(3), "", 2000, true, 6, "CCCCCC");
		            setCellText(table7.getRow(2).getCell(4), "", 2000, true, 6, "CCCCCC");
		            setCellText(table7.getRow(2).getCell(5), "", 2000, true, 6, "CCCCCC");
		            setCellText(table7.getRow(2).getCell(6), "", 2000, true, 6, "CCCCCC");
		            setCellText(table7.getRow(2).getCell(7), "", 2000, true, 6, "CCCCCC");
		            
		            XWPFParagraph   para16 = document.createParagraph(); 
		            XWPFRun run16 = para16.createRun(); 
		            run16.addBreak();
		            run16.setText(" H. Lecture du montant des offres "); 
		            run16.setBold(true);
		            

		            XWPFTable table8 = document.createTable(1,5);
		            XWPFTableRow row1_table8 = table8.getRow(0);
		            setCellText(row1_table8.getCell(0), "N�", 200, true, 6, "CCCCCC");
		            setCellText(row1_table8.getCell(1), "Nom ou Raison sociale", 9000, true, 6, "CCCCCC");
		            setCellText(row1_table8.getCell(2), "Lot", 6000, true, 6, "CCCCCC");
		            setCellText(row1_table8.getCell(3), "Montant Lot", 6000, true, 6, "CCCCCC");
		            setCellText(row1_table8.getCell(4), "Montant lu(C.F.A", 6000, true, 6, "CCCCCC");
		                
		                         
		               for(int i=0;i<lotssoummissionnaires.size();i++)
		               {
		            	   XWPFTableRow tableRowTwo = table8.createRow();
		            	   tableRowTwo.getCell(0).setText(lotssoummissionnaires.get(i).getPlis().getNumero()+"");
		            	   tableRowTwo.getCell(1).setText(lotssoummissionnaires.get(i).getPlis().getRaisonsociale());
		            	   tableRowTwo.getCell(2).setText(lotssoummissionnaires.get(i).getLot().getLibelle());
		            	   setCellTextLigne(tableRowTwo.getCell(3), ToolKermel.format3Decimal(lotssoummissionnaires.get(i).getLot().getMontant()),  true, 6, "CCCCCC");
		            	   setCellTextLigne(tableRowTwo.getCell(4), ToolKermel.format3Decimal(lotssoummissionnaires.get(i).getPlilmontantoffert()),  true, 6, "CCCCCC");
		            	 
		            
		               }
			        
		             XWPFParagraph   para17 = document.createParagraph(); 
			         XWPFRun run17 = para17.createRun(); 
			         run17.addBreak();
			         run17.setText(" I. Incidents �ventuels survenus au cours de la s�ance "); 
			         run17.setBold(true);
			         
			         XWPFParagraph   para18= document.createParagraph(); 
			         XWPFRun run18 = para18.createRun(); 
			         run18.addBreak();
			         if(dossier!=null)
			         run18.setText(dossier.getDosIncidents()); 
			         
			         XWPFParagraph   para19= document.createParagraph();
			         para19.setAlignment(ParagraphAlignment.CENTER); 
			         XWPFRun run19 = para19.createRun(); 
			         run19.addBreak();
			         run19.addBreak();
			         run19.addBreak();
			         run19.setText("Fait � Cotonou le "+UtilVue.getInstance().formateLaDate2(new Date())); 
			         run17.setBold(true);
			            
	               file = new File(nomFichier); 
	               if(file.exists())
	                   file.delete();


	               FileOutputStream out = new FileOutputStream(file);
	               document.write(out);
	               out.close();

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				VisualiserFichier();
			   
//			 if(appel.getApoDatepvouverturepli()!=null)
//			 {
//				idIframe.setSrc(EDIT_URL + "?code="+dossier.getDosID()+ "&libelle=ouvertureplis");
//				step0.setVisible(false);
//				step1.setVisible(true);
				
//				 nomFichier = "proces_verbal.rptdesign";
//                 format = UIConstants.FORMAT_WORD_BIRT;
//                 System.out.println(EDIT_URL_Birt + nomFichier + "&code=" + dossier.getDosID() + "&__format=" + format );
//                 idIframe.isInvalidated();
//                 idIframe.setSrc(EDIT_URL_Birt + nomFichier + "&dossier=" + dossier.getDosID()+ "&autorite=" + autorite.getId()+ "&gestion=" + Integer.toString(dossier.getRealisation().getPlan().getAnnee()) + "&__format=" + format );

//			 }
//			 else
//			 {
//				 throw new WrongValueException(menuEditer, Labels.getLabel("sygmap.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.saisirproces")); 
//			 }
//			 
				
				
			}
		 
	
		 public void setCellText(XWPFTableCell cell, String text, int width,
					boolean isShd, int shdValue, String shdColor) {
				CTTc cttc = cell.getCTTc();
				CTTcPr ctPr = cttc.isSetTcPr() ? cttc.getTcPr() : cttc.addNewTcPr();
				CTShd ctshd = ctPr.isSetShd() ? ctPr.getShd() : ctPr.addNewShd();
				ctPr.addNewTcW().setW(BigInteger.valueOf(width));
				if (isShd) {
					if (shdValue > 0 && shdValue <= 38) {
						ctshd.setVal(STShd.Enum.forInt(shdValue));
					}
					if (shdColor != null) {
						// ctshd.setFill(shdColor);
						// ctshd.setColor("auto");
						ctshd.setColor(shdColor);
					}
				}

				ctPr.addNewVAlign().setVal(STVerticalJc.CENTER);
				cttc.getPList().get(0).addNewPPr().addNewJc().setVal(STJc.CENTER);
				cell.setText(text);
			}
		
		 public void setCellTextLigne(XWPFTableCell cell, String text,  boolean isShd, int shdValue, String shdColor) {
				CTTc cttc = cell.getCTTc();
				CTTcPr ctPr = cttc.isSetTcPr() ? cttc.getTcPr() : cttc.addNewTcPr();
				CTShd ctshd = ctPr.isSetShd() ? ctPr.getShd() : ctPr.addNewShd();
			
				if (isShd) {
					if (shdValue > 0 && shdValue <= 38) {
						ctshd.setVal(STShd.Enum.forInt(shdValue));
					}
					if (shdColor != null) {
						// ctshd.setFill(shdColor);
						// ctshd.setColor("auto");
						ctshd.setColor(shdColor);
					}
				}

				ctPr.addNewVAlign().setVal(STVerticalJc.CENTER);
				cttc.getPList().get(0).addNewPPr().addNewJc().setVal(STJc.RIGHT);
				cell.setText(text);
			}
		 /**
			 * @Les colonnes de fusion
			 */
			public void mergeCellsHorizontal(XWPFTable table, int row, int fromCell,
					int toCell) {
				for (int cellIndex = fromCell; cellIndex <= toCell; cellIndex++) {
					XWPFTableCell cell = table.getRow(row).getCell(cellIndex);
					if (cellIndex == fromCell) {
						cell.getCTTc().addNewTcPr().addNewHMerge()
								.setVal(STMerge.RESTART);
					} else {
						cell.getCTTc().addNewTcPr().addNewHMerge()
								.setVal(STMerge.CONTINUE);
					}
				}
			}

			/**
			 * @La ligne de fusion
			 */
			public void mergeCellsVertically(XWPFTable table, int col, int fromRow,
					int toRow) {
				for (int rowIndex = fromRow; rowIndex <= toRow; rowIndex++) {
					XWPFTableCell cell = table.getRow(rowIndex).getCell(col);
					if (rowIndex == fromRow) {
						cell.getCTTc().addNewTcPr().addNewVMerge()
								.setVal(STMerge.RESTART);
					} else {
						cell.getCTTc().addNewTcPr().addNewVMerge()
								.setVal(STMerge.CONTINUE);
					}
				}
			}
		 //////::01/10/2015
		 public void onClick$menuFructueuse(){
		    HashMap<String, String> display = new HashMap<String, String>(); // permet
			display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("sygmap.common.form.question.procedure.fructueuse"));
			display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("sygmap.common.form.procedure.fructueuse"));
			display.put(MessageBoxController.DSP_HEIGHT, "250px");
			display.put(MessageBoxController.DSP_WIDTH, "47%");

			HashMap<String, Object> map = new HashMap<String, Object>(); // permet
			showMessageBox(display, map);
		 }


		@Override
		public void onEvent(Event event) throws Exception {
			// TODO Auto-generated method stub
			if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				step0.setVisible(false);
				step1.setVisible(false);
				step2.setVisible(true);
			}
		}
		
		private boolean checkFieldConstraints2() {
			
			try {
			
				if(dtstopprocedure.getValue()==null)
			     {
	               errorComponent = dtstopprocedure;
	               errorMsg = Labels.getLabel("sygmap.journal.date")+": "+Labels.getLabel("sygmap.erreur.champobligatoire");
	               lbStatusBar2.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar2.setValue(errorMsg);
				  throw new WrongValueException (errorComponent, errorMsg);
			     }
			
				if(txtMotif.getValue().equals(""))
			     {
	          errorComponent = txtMotif;
	          errorMsg = Labels.getLabel("sygmap.plansdepassation.miseajour.motif")+": "+Labels.getLabel("sygmap.erreur.champobligatoire");
	          lbStatusBar2.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar2.setValue(errorMsg);
				  throw new WrongValueException (errorComponent, errorMsg);
			     }
				if(txtVersionElectronique2.getValue().equals(""))
			     {
	          errorComponent = txtVersionElectronique2;
	          errorMsg = Labels.getLabel("sygmap.passation.procedure.fructueuse.fichier")+": "+Labels.getLabel("sygmap.erreur.champobligatoire");
	          lbStatusBar2.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar2.setValue(errorMsg);
				  throw new WrongValueException (errorComponent, errorMsg);
			     }
				return true;
					
			}
			catch (Exception e) {
				errorMsg = Labels.getLabel("sygmap.erreur.erreurinconnue") + ": " + e.toString()
				+ " [checkFieldConstraints]";
				errorComponent = null;
				return false;

				
			}
			
		}
		
		public void onClick$btnChoixFichier2() {
			//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
				if (ToolKermel.isWindows())
					nomFichier2 = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
				else
					nomFichier2 = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

				txtVersionElectronique2.setValue(nomFichier2);
			}
		
		public void onClick$menuValider2() {
			if(checkFieldConstraints2())
			{
				int nbre=0;
				documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossier, appel,UIConstants.PARAM_TYPEDOCUMENTS_PVOUVERTURE,null, null);
				if(documents.size()>0)
				{
					for(int i=0;i<documents.size();i++)
					{
						if(documents.get(i).getLibelle().equalsIgnoreCase(Labels.getLabel("sygmap.passation.procedure.fructueuse.fichier")))
						{
							 document=documents.get(i);
							 nbre=nbre+1;
							 break;
						}
					}
				}
				 
				document.setAppel(appel);
				document.setDossier(dossier);
				document.setNomFichier(txtVersionElectronique2.getValue());
				document.setTypeDocument(UIConstants.PARAM_TYPEDOCUMENTS_PVOUVERTURE);
				document.setLibelle(Labels.getLabel("sygmap.passation.procedure.fructueuse.fichier"));
				document.setDate(new Date());
				document.setHeure(Calendar.getInstance().getTime());
				//document.setOrigine(UIConstants.PARAM_DAO);
				if(nbre==0)
				{
					BeanLocator.defaultLookup(DocumentsSession.class).save(document);
				}
				else
				{
					BeanLocator.defaultLookup(DocumentsSession.class).update(document);
				}
				appel.setDatestopprocedure(dtstopprocedure.getValue());
				appel.setMotifstopprocedure(txtMotif.getValue());
				appel.setApofichierpv(txtVersionElectronique2.getValue());
				BeanLocator.defaultLookup(AppelsOffresSession.class).update(appel);
				
				
				
				session.setAttribute("libelle", "procesverbalouverture");
				loadApplicationState("suivi_procedure_passation");
			}
		}
		
		public void infos2(SygAppelsOffres appel) {
				
			if(appel.getApofichierpv()!=null||!appel.getApofichierpv().equals(""))
			{
				extension=appel.getApofichierpv().substring(appel.getApofichierpv().length()-3,  appel.getApofichierpv().length());
				 if(extension.equalsIgnoreCase("pdf"))
					 images="/images/icone_pdf.png";
				 else  
					 images="/images/word.jpg";
				 
				image2.setVisible(true);
				image2.setSrc(images);
				lbldeuxpoints2.setValue(":");
				lbltitre2.setValue(Labels.getLabel("sygmap.passation.procedure.fructueuse.fichier"));
				dtstopprocedure.setValue(appel.getDatestopprocedure());
				fichier=appel.getApofichierpv(); 
				
				
			}
		}
		
		public void onClick$image2() {
			onInfosImage(fichier);
		}
		
		public void onInfosImage(String fichier ) {
			step0.setVisible(false);
			step1.setVisible(true);
			step2.setVisible(false);
			String filepath = cheminDossier +  fichier;
			File f = new File(filepath.replaceAll("\\\\", "/"));

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(f, null, null);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (mymedia != null)
				idIframe.setContent(mymedia);
			else
				idIframe.setSrc("");

			idIframe.setHeight("600px");
			idIframe.setWidth("100%");
		}
		
		  public void VisualiserFichier(){
//				idIframe.setHeight("500px");
//				idIframe.setWidth("100%");

				String filepath =  nomFichier;
				File f = null;
				if(ToolKermel.isWindows())
				// windows
				  f = new File(filepath.replaceAll("/", "\\\\"));
				// linux
				else
		         f = new File(filepath.replaceAll("\\\\", "/"));

				org.zkoss.util.media.AMedia mymedia = null;
				try {
					mymedia = new AMedia(f, null, null);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				if (mymedia != null)
					idIframe.setContent(mymedia);
				else
					idIframe.setSrc("");

				
			}
		   
}