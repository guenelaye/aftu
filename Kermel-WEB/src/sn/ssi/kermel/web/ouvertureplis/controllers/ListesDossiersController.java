package sn.ssi.kermel.web.ouvertureplis.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygPiecesplisouvertures;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.PiecessoumissionnairesSession;
import sn.ssi.kermel.be.referentiel.ejb.ModepassationSession;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

import com.ibm.icu.util.Calendar;

@SuppressWarnings("serial")
public class ListesDossiersController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Paging pgPagination, pgMode;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE, activePage;
	public static final String CONFIRMVALIDER = "CONFIRMVALIDER";
	String page = null;
	Session session = getHttpSession();
	SygDossiers dossier = new SygDossiers();
	String login, objet = null, typemarche = null, modepassation = null,
			libellemode = null;
	private Listbox lstListe, lstType, lstMode;
	public Textbox txtObjets, txtRechercherMode;
	private SygAppelsOffres appel;
	private Label lblNombreDossiersEnAttente, lblDateCourante, lblMoisCourant, lblHeureCourante, lblNombreDossiersEnCours;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite;
	private KermelSousMenu monSousMenu;
	private Menuitem ADD_OUVERTUREPLIS;
	SygTypesmarches type_marche = null;
	SygModepassation mode_passation = null;
	private Bandbox bdType, bdMode;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE, count;
	Calendar c = Calendar.getInstance();

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.OUVERTUREPLIS);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_OUVERTUREPLIS != null) {
			ADD_OUVERTUREPLIS.addForward(ApplicationEvents.ON_CLICK, this,
					ApplicationEvents.ON_SUIVI);
		}

		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_SUIVI, this);

		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		lstListe.setItemRenderer(this);

		addEventListener(ApplicationEvents.ON_TYPES, this);
		lstType.setItemRenderer(new TypesRenderer());
		Events.postEvent(ApplicationEvents.ON_TYPES, this, null);

		addEventListener(ApplicationEvents.ON_MODES, this);
		lstMode.setItemRenderer(new ModesRenderer());
		pgMode.setPageSize(byPageBandbox);
		pgMode.addForward(ApplicationEvents.ON_PAGING, this,
				ApplicationEvents.ON_MODES);
		Events.postEvent(ApplicationEvents.ON_MODES, this, null);
	}

	public void onCreate(CreateEvent event) {
		infoscompte = (Utilisateur) getHttpSession()
				.getAttribute("infoscompte");
		if ((infoscompte.getType().equals(UIConstants.USERS_TYPES_AC))) {
			autorite = infoscompte.getAutorite();

		} else {
			autorite = null;
		}

		c.add(Calendar.DATE, -10);
		count = BeanLocator.defaultLookup(DossiersAppelsOffresSession.class)
				.countDossiersEnAttenteOP(autorite, null, objet, modepassation,
						typemarche, c.getTime());
		int countEnCours = BeanLocator.defaultLookup(DossiersAppelsOffresSession.class)
				.countDossiersEnCoursOP(autorite, null, objet, modepassation,
						typemarche, c.getTime());
		lblNombreDossiersEnAttente.setValue(count + "");
		lblNombreDossiersEnCours.setValue(countEnCours+"");
		java.util.Calendar calendar = new GregorianCalendar();
		calendar.setTime(new Date());
		lblDateCourante.setValue(calendar.get(java.util.Calendar.DAY_OF_MONTH)+"");
		lblMoisCourant.setValue(new SimpleDateFormat("MMM").format(calendar.getTime()).toUpperCase());
		
		
		
		
		
		
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

	}

	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}

			List<SygDossiers> dossiers = BeanLocator.defaultLookup(
					DossiersAppelsOffresSession.class).DossiersEnAttenteOP(
					activePage, byPage, autorite, null, objet, modepassation,
					typemarche, c.getTime());
			SimpleListModel listModel = new SimpleListModel(dossiers);
			lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup(
					DossiersAppelsOffresSession.class)
					.countDossiersEnAttenteOP(autorite, null, objet,
							modepassation, typemarche, c.getTime()));
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_SUIVI)) {
			if (lstListe.getSelectedItem() == null)

				throw new WrongValueException(lstListe,
						Labels.getLabel("kermel.error.select.item"));

			session.setAttribute("idappel", lstListe.getSelectedItem()
					.getValue());
			session.setAttribute("libelle", "traitementsdossiers");
			loadApplicationState("ouverture_plis");

		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_TYPES)) {

			List<SygTypesmarches> types = BeanLocator.defaultLookup(
					TypesmarchesSession.class).find(0, -1, null, null, null);
			lstType.setModel(new SimpleListModel(types));
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgMode.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgMode.getActivePage() * byPageBandbox;
				pgMode.setPageSize(byPageBandbox);
			}
			List<SygModepassation> modes = BeanLocator.defaultLookup(
					ModepassationSession.class).find(activePage, byPageBandbox,
					null, libellemode);
			lstMode.setModel(new SimpleListModel(modes));
			pgMode.setTotalSize(BeanLocator.defaultLookup(
					ModepassationSession.class).count(null, libellemode));
		}

	}

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygDossiers dossier = (SygDossiers) data;
		item.setValue(dossier.getAppel().getApoid());

		Listcell cellReference = new Listcell();
		if(dossier.getAppel()
				.getAporeference() != null) {
			Label lblApoRef = new Label(dossier.getAppel()
				.getAporeference()) ;
			lblApoRef.setSclass("text-strong");
			lblApoRef.setParent(cellReference);
			
		}
		cellReference.setParent(item);

		Listcell cellObjet = new Listcell(dossier.getAppel().getApoobjet());
		cellObjet.setParent(item);

		Listcell cellTypeMarche = new Listcell(dossier.getAppel()
				.getTypemarche().getLibelle());
		cellTypeMarche.setParent(item);

		Listcell cellModepassation = new Listcell(dossier.getAppel()
				.getModepassation().getLibelle());
		cellModepassation.setParent(item);

		Listcell cellDateDepot = new Listcell("");
		Label lblDateDepot = new Label();

		if (dossier.getDosDateLimiteDepot() != null
				&& dossier.getDosHeurelimitedepot() != null) {
			lblDateDepot.setValue(ToolKermel.dateToString(dossier
					.getDosDateLimiteDepot())
					+ " à "
					+ UtilVue.getInstance().formateLHeure(
							dossier.getDosHeurelimitedepot()));
			if (dossier.getDosDateLimiteDepot().equals(new Date())
					|| dossier.getDosDateLimiteDepot().after(new Date())) {
				lblDateDepot.setSclass("tag alert");
			} else
				lblDateDepot.setSclass("tag info");
		}

		lblDateDepot.setParent(cellDateDepot);
		cellDateDepot.setParent(item);

		Listcell cellDateOuverture = new Listcell("");
		Label lblDateOuverture = new Label();
		String textDate = "";
		if (dossier.getDosDateOuvertueDesplis() != null)
			textDate += ToolKermel.dateToString(dossier
					.getDosDateOuvertueDesplis());
		if (dossier.getDosHeureOuvertureDesPlis() != null)
			textDate += " à "
					+ UtilVue.getInstance().formateLHeure(
							dossier.getDosHeureOuvertureDesPlis());
		lblDateOuverture.setValue(textDate);
		if (dossier.getDosDateOuvertueDesplis().equals(new Date()) || dossier.getDosDateOuvertueDesplis().before(new Date())) {
			lblDateOuverture.setSclass("tag success");
		} else
			lblDateOuverture.setSclass("tag alert");

		lblDateOuverture.setParent(cellDateOuverture);
		cellDateOuverture.setParent(item); 
		
		item.addEventListener(Events.ON_DOUBLE_CLICK, new EventListener() {

			@Override
			public void onEvent(Event event) throws Exception {
				
				lstListe.setSelectedItem(item);
				Events.postEvent(ApplicationEvents.ON_SUIVI, ListesDossiersController.this, null);
				
						 
			}

		});

	}

	public void onClick$btnchercher() {

		if ((txtObjets.getValue().equalsIgnoreCase(Labels
				.getLabel("kermel.plansdepassation.passationsmarches.objets")))
				|| (txtObjets.getValue().equals(""))) {
			objet = null;

		} else {
			objet = txtObjets.getValue();
			page = "0";
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}

	public void onOK$btnchercher() {
		onClick$btnchercher();
	}

	public void onOK$txtObjets() {
		onClick$btnchercher();
	}

	public void onFocus$txtObjets() {
		if (txtObjets
				.getValue()
				.equalsIgnoreCase(
						Labels.getLabel("kermel.plansdepassation.passationsmarches.objets")))
			txtObjets.setValue("");

	}

	public void onBlur$txtObjets() {
		if (txtObjets.getValue().equals(""))
			txtObjets
					.setValue(Labels
							.getLabel("kermel.plansdepassation.passationsmarches.objets"));
	}

	public class TypesRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypesmarches types = (SygTypesmarches) data;
			item.setValue(types);

			Listcell cellLibelle = new Listcell("");
			if (types.getLibelle() != null) {
				cellLibelle.setLabel(types.getLibelle());
			}
			cellLibelle.setParent(item);

		}
	}

	public void onSelect$lstType() {
		type_marche = (SygTypesmarches) lstType.getSelectedItem().getValue();
		typemarche = type_marche.getLibelle();
		bdType.setValue(typemarche);
		bdType.close();
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);

	}

	public void onSelect$lstMode() {
		mode_passation = (SygModepassation) lstMode.getSelectedItem()
				.getValue();
		modepassation = mode_passation.getLibelle();
		bdMode.setValue(modepassation);
		bdMode.close();
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);

	}

	public class ModesRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygModepassation modes = (SygModepassation) data;
			item.setValue(modes);

			Listcell cellLibelle = new Listcell("");
			if (modes.getLibelle() != null) {
				cellLibelle.setLabel(modes.getLibelle());
			}
			cellLibelle.setParent(item);

		}
	}

	public void onOK$txtRechercherMode() {
		onClick$btnRechercherMode();
	}

	public void onFocus$txtRechercherMode() {
		if (txtRechercherMode.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.modepassation"))) {
			txtRechercherMode.setValue("");
		}
	}

	public void onClick$btnRechercherMode() {
		if (txtRechercherMode.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.modepassation"))
				|| txtRechercherMode.getValue().equals("")) {
			libellemode = null;
			page = null;
		} else {
			libellemode = txtRechercherMode.getValue();
			page = "0";
		}
		Events.postEvent(ApplicationEvents.ON_MODES, this, page);
	}
	
	public void onTimer$timer() {
		
		Date ddj = new Date();
		lblHeureCourante.setValue(new SimpleDateFormat("HH:mm:ss").format(ddj));
		
		
	}

}