package sn.ssi.kermel.web.ouvertureplis.controllers;

import java.util.Date;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Textbox;

public class EditableRadiosDiv extends Div {
	public static final String ON_EDITABLE = "onEditable";
	private String text = "";
	private Date date = new Date();
	private boolean clickable = true;
	private boolean checked = false;
	private boolean editable = false;
	Textbox txb;
	Datebox datebox;
	Label lbl;
	Bandbox bandbox;
	Intbox intbox;
	Radio radio;

	// Empty constructor will do the creation
	public EditableRadiosDiv() {
		txb = new Textbox();
		lbl = new Label();
		txb.setWidth("99%");
		lbl.setWidth("99%");
		lbl.setMultiline(true);
		lbl.setParent(this);// Default show a label with text

		radio = new Radio();
	}

	public EditableRadiosDiv(String text, String component) {
		if (component.endsWith("Textbox")) {
			txb = new Textbox();
			lbl = new Label();
			txb.setWidth("99%");
			lbl.setWidth("99%");
			lbl.setParent(this);// Default show a label with text
		} else if (component.endsWith("Datebox")) {
			datebox = new Datebox();
			lbl = new Label();
			datebox.setWidth("99%");
			lbl.setWidth("99%");
			lbl.setParent(this);// Default show a label with text
		} else if (component.endsWith("BandBox")) {
			bandbox = new Bandbox();
			lbl = new Label();
			txb.setWidth("99%");
			lbl.setWidth("99%");
			lbl.setParent(this);// Default show a label with text
		} else if (component.endsWith("IntBox")) {
			bandbox = new Bandbox();
			lbl = new Label();
			txb.setWidth("99%");
			lbl.setWidth("99%");
			lbl.setParent(this);// Default show a label with text
		}else if (component.endsWith("Radio")) {
			radio = new Radio();
			lbl = new Label();
			txb.setWidth("99%");
			lbl.setWidth("99%");
			radio.setWidth("99%");
			radio.setParent(this);// Default show a label with text
		}
	}

	public EditableRadiosDiv(String text) {
		this();
		setText(text);
		initEditCtrl();
	}

	public EditableRadiosDiv(String text, boolean clickable) {
		this();
		setText(text);
		setClickable(clickable);
		initEditCtrl();
	}
	// radiogroupe
	public EditableRadiosDiv(Boolean booleen, boolean clickable) {
		this();
		setRadio(radio);
		setClickable(clickable);
		initEditCtrl();
	}
	public Radio getRadio() {
		return radio;
	}

	public void setRadio(Radio radio) {
		this.radio = radio;
	}

	// Getter and setters
	public void setText(String text) {
		this.text = text;
		txb.setValue(text);
		lbl.setValue(text);
	}
	
	

	public String getText() {
		return text;
	}

	public void setClickable(boolean clickable) {
		this.clickable = clickable;
	}

	public boolean isClickable() {
		return this.clickable;
	}

	// Initialize the listener of the whole component and the textbox in it
	private void initEditCtrl() {
		this.addEventListener(ON_EDITABLE, new EventListener() {
			public void onEvent(Event event) throws Exception {
				toggleEdit((Boolean) event.getData());
			}
		});

		// This will turns the edit funtion on when click on label
		if (isClickable()) {
			lbl.addEventListener(Events.ON_CLICK, new EventListener() {
				public void onEvent(Event event) throws Exception {
					txb.setFocus(true);
					radio.setFocus(true);
					Events.postEvent(new Event(EditableRow.ON_EDIT,
							EditableRadiosDiv.this.getParent(), null));
				}
			});
		}
	}

	// Replace textbox/label with label/textbox
	private void toggleEdit(boolean applyChange) {
		if (!editable) {
			lbl.detach();
			EditableRadiosDiv.this.appendChild(radio);
		} else {
			checked = radio.isChecked();
			radio.detach();
			if (applyChange) {// if apply changes then set the value in
////				lbl.setValue(text = txb.getValue());
//				if(checked)
//					lbl.setValue("OUI");
//				else
//					lbl.setValue("NON");
//			} else {
//				radio.setChecked(checked);
////				txb.setValue(text);
////				lbl.setValue(text);
//				if(checkbox.isChecked())
//					lbl.setValue("OUI");
//				else
//					lbl.setValue("NON");
			}
			EditableRadiosDiv.this.appendChild(lbl);
		}
		editable = !editable;
	}

	private void toggleEditDate(boolean applyChange) {
		if (!editable) {
			lbl.detach();
			EditableRadiosDiv.this.appendChild(datebox);
		} else {
			datebox.detach();
			if (applyChange) {// if apply changes then set the value in
				lbl.setValue((date = datebox.getValue()).toString());
			} else {
				datebox.setValue(date);
				lbl.setValue(date.toString());
			}
			EditableRadiosDiv.this.appendChild(lbl);
		}
		editable = !editable;
	}

	private void toggleEditBandbox(boolean applyChange) {
		if (!editable) {
			lbl.detach();
			EditableRadiosDiv.this.appendChild(datebox);
		} else {
			datebox.detach();
			if (applyChange) {// if apply changes then set the value in
				lbl.setValue((date = datebox.getValue()).toString());
			} else {
				datebox.setValue(date);
				lbl.setValue(date.toString());
			}
			EditableRadiosDiv.this.appendChild(lbl);
		}
		editable = !editable;
	}
	
	@SuppressWarnings("unused")
	private void toggleEditCheckbox(boolean applyChange) {
		if (!editable) {
			lbl.detach();
			EditableRadiosDiv.this.appendChild(radio);
		} else {
			radio.detach();
			if (applyChange) {// if apply changes then set the value in
				checked = radio.isChecked();
//				if(checked)
//					lbl.setValue("OUI");
//				else
//					lbl.setValue("NON");
//			} else {
//				checkbox.setChecked(checked);
//				
//				if(checked)
//					lbl.setValue("OUI");
//				else
//					lbl.setValue("NON");
			}
			EditableRadiosDiv.this.appendChild(lbl);
		}
		editable = !editable;
	}


}