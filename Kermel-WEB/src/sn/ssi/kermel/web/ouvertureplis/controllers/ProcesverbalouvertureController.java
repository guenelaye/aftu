package sn.ssi.kermel.web.ouvertureplis.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xwpf.usermodel.Borders;
import org.apache.poi.xwpf.usermodel.BreakType;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.TextAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableCell.XWPFVertAlign;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTShd;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STMerge;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STShd;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STVerticalJc;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Fileupload;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.MessageManager;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossiersEvaluateurs;
import sn.ssi.kermel.be.entity.SygDossierspieces;
import sn.ssi.kermel.be.entity.SygLotsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygMembresCommissionsMarches;
import sn.ssi.kermel.be.entity.SygObservateursIndependants;
import sn.ssi.kermel.be.entity.SygPiecesplisouvertures;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygPresenceouverture;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygRepresentantsServicesTechniques;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DocumentsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersEvaluateursSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersPiecesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.LotsSoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.ObservateursIndependantsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.PiecessoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.Presence0uvertureSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RepresentantsServicesTechniquesSession;
import sn.ssi.kermel.be.referentiel.ejb.MembresCommissionsMarchesSession;
import sn.ssi.kermel.be.security.ProfilsSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class ProcesverbalouvertureController extends AbstractWindow implements
		AfterCompose {

	private Listbox lstListe, lstBailleur;
	private Paging pgPagination, pgBailleur;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE, activePage;
	public static final String CURRENT_MODULE = "CURRENT_MODULE";
	public Textbox txtLibelle;
	String libelle = null, page = null, login, codesuppression,
			libellesuppression;
	private Listheader lshLibelle, lshDivision;
	Session session = getHttpSession();
	private String reference, libellebailleur = null;
	private int parent;
	List<SygDocuments> documents = new ArrayList<SygDocuments>();
	SygSecteursactivites categorie = null;
	private Bandbox bdBailleur;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private Textbox txtVersionElectronique;
	SygBailleurs bailleur = null;
	private Decimalbox dcmontantprevu;
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel = new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation = new SygRealisations();
	private Long idbailleur;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygDocuments document = new SygDocuments();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite = null;
	private String nomFichier;
	private final String cheminDossier = UIConstants.DOCSPATH;
	UtilVue utilVue = UtilVue.getInstance();
	private Datebox dtouverture;
	SygDossiers dossier = new SygDossiers();
	private Menuitem menuValider, menuSigner, menuEnvoyer, menuFermer,
			menuJoindre;
	private Image image;
	private Label lbltitre, lbldeuxpoints;
	private Div step0, step1;
	private Iframe idIframe;
	private String extension, images;
	public static final String EDIT_URL = "http://" + UIConstants.IP_SSI + ":"
			+ UIConstants.PORT_SSI + "/EtatsKermel/OuverturePlisPDFServlet";
	private Menuitem menuEditer;
	private Iframe signFrame;
	private Div divPv;
	private Button btnSigned, btnSignedSec, btnSignByPresident,
			btnSignBySecretaire;
	private boolean signedPr = false, signedSec = false, appletLaunched = false;
	private Label lblStatutSignPr, lblStatutSignSec;
	List<SygMembresCommissionsMarches> membrescommissions = new ArrayList<SygMembresCommissionsMarches>();
	List<SygRepresentantsServicesTechniques> representantsservicestechniques = new ArrayList<SygRepresentantsServicesTechniques>();
	List<SygObservateursIndependants> observateursindependants = new ArrayList<SygObservateursIndependants>();
	List<SygPresenceouverture> representantssoumissionnaires = new ArrayList<SygPresenceouverture>();
	List<SygRetraitregistredao> retraits = new ArrayList<SygRetraitregistredao>();
	List<SygDossiersEvaluateurs> commissionstechniques = new ArrayList<SygDossiersEvaluateurs>();
	List<SygLotsSoumissionnaires> lotssoummissionnaires = new ArrayList<SygLotsSoumissionnaires>();
	List<SygPlisouvertures> plis = new ArrayList<SygPlisouvertures>();
	List<SygDossierspieces> piecesadministratives = new ArrayList<SygDossierspieces>();
	Utilisateur rapporteur = null;

	private String gestion;

	List<SygPlisouvertures> registredepot = new ArrayList<SygPlisouvertures>();

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

	}

	public void onCreate(CreateEvent createEvent) {
		infoscompte = (Utilisateur) getHttpSession()
				.getAttribute("infoscompte");

		if (infoscompte.getAutorite() != null)
			autorite = infoscompte.getAutorite();
		else
			autorite = null;
		idappel = (Long) session.getAttribute("idappel");
		appel = BeanLocator.defaultLookup(AppelsOffresSession.class).findById(
				idappel);
		realisation = appel.getRealisation();
		dossier = BeanLocator.defaultLookup(DossiersAppelsOffresSession.class)
				.Dossier(null, appel, -1);
		if (dossier == null) {
			menuValider.setDisabled(true);
			menuEditer.setDisabled(true);
			image.setVisible(false);
		} else {
			registredepot = BeanLocator.defaultLookup(
					RegistrededepotSession.class).find(0, -1, dossier, null,
					null, null, -1, -1, -1, -1, -1, -1, null, -1, null, null);
			if (registredepot.size() == 0
					|| appel.getApoDatepvouverturepli() != null) {
				menuValider.setDisabled(true);
			}

			infos(dossier, appel);
			gestion = Integer.toString(dossier.getRealisation().getPlan()
					.getAnnee());
			membrescommissions = BeanLocator.defaultLookup(
					MembresCommissionsMarchesSession.class).find(0, -1, null,
					null, dossier.getAutorite(), gestion, -1, -1);
			observateursindependants = BeanLocator.defaultLookup(
					ObservateursIndependantsSession.class).find(0, -1, dossier,
					-1);
			representantsservicestechniques = BeanLocator.defaultLookup(
					RepresentantsServicesTechniquesSession.class).find(0, -1,
					dossier, -1);
			representantssoumissionnaires = BeanLocator.defaultLookup(
					Presence0uvertureSession.class).find(0, -1, dossier, null,
					null, -1);
			commissionstechniques = BeanLocator.defaultLookup(
					DossiersEvaluateursSession.class).find(0, -1, dossier);
			lotssoummissionnaires = BeanLocator.defaultLookup(
					LotsSoumissionnairesSession.class).find(0, -1, dossier,
					null, null, -1, "oui", -1, -1, -1, null);

			plis = BeanLocator.defaultLookup(RegistrededepotSession.class)
					.find2(0, -1, dossier, null, null, null, -1, -1, -1, -1, -1,
							-1, null, -1, null, null);

			piecesadministratives = BeanLocator.defaultLookup(
					DossiersPiecesSession.class).find(0, -1, dossier);

			rapporteur = BeanLocator.defaultLookup(ProfilsSession.class)
					.findSecretaire(dossier.getAutorite());
		}
		checkPv();
	}

	public void infos(SygDossiers dossier, SygAppelsOffres appel) {
		documents = BeanLocator.defaultLookup(DocumentsSession.class).find(0,
				-1, dossier, appel,
				UIConstants.PARAM_TYPEDOCUMENTS_PVOUVERTURE, null, null);
		
		

		if (documents.size() > 0) {
			nomFichier = documents.get(0).getNomFichier();
			signedPr = documents.get(0).isSignedByPresident();
			signedSec = documents.get(0).isSignedBySecretaire();
			txtVersionElectronique.setValue(document.getNomFichier());
			extension = "pdf"; // documents
//					.get(0)
//					.getNomFichier()
//					.substring(documents.get(0).getNomFichier().length() - 3,
//							documents.get(0).getNomFichier().length());
			if (extension.equalsIgnoreCase("pdf"))
				images = "/images/icone_pdf.png";
			else
				images = "/images/word.jpg";

			image.setVisible(true);
			image.setSrc(images);
			lbldeuxpoints.setValue(":");
			lbltitre.setValue(Labels
					.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.procesverbalouverture.versionfichier"));
			dtouverture.setValue(documents.get(0).getAppel()
					.getApoDatepvouverturepli());

		}
	}

	public void onClick$btnChoixFichier() {
		// String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
		
		String chemin = UIConstants.PATH_PJ;
		chemin=ToolKermel.formatPath(chemin);
		File dossierPj = new File(chemin);
		if (!dossierPj.exists()) {
			dossierPj.mkdirs();
		}
	
		
//		if (ToolKermel.isWindows())
//			nomFichier = FileLoader.uploadPieceDossier(
//					utilVue.formateLaDate3(Calendar.getInstance().getTime()),
//					"Pj", chemin.replaceAll("/", "\\\\"));
//		else
//			nomFichier = FileLoader.uploadPieceDossier(
//					utilVue.formateLaDate3(Calendar.getInstance().getTime()),
//					"Pj", chemin);
//			
			try {
				// open select file dialog
				Object media = Fileupload.get();
				if (media instanceof org.zkoss.util.media.Media) {
					org.zkoss.util.media.Media medium = (org.zkoss.util.media.Media) media;

					String nomPiece = null;
					// get file name
					// String nomPiece = cheminDossierPieces + "/" + dgrCode + "_" +
					// pijcode + "_" + medium.getName();

//					if (ToolKermel.isWindows())
//						nomPiece = cheminDossierPieces + dgrCode + "_" + pijcode + "_" + medium.getName();
//					else
					String dgrCode=utilVue.formateLaDate3(Calendar.getInstance().getTime());
						nomPiece = chemin+ dgrCode + "_" + medium.getName();

					// upload file
					boolean boolUpload = FileLoader.uploadFile(medium, nomPiece);
					if (boolUpload) {
						// remember file name
						nomFichier = dgrCode + "_" + medium.getName();
						txtVersionElectronique.setValue(nomFichier);
						
						onClick$menuValider();

						checkPv();
					} else {
						System.out.print(Labels.getLabel("atlantis.clientele.receptiondossier.joindrepieceerror"));
					}
				}

			} catch (Exception e) {
				// handle ERROR
				e.printStackTrace();
			}

		
	}

	private  void checkFieldConstraints() {

		

//			if (txtVersionElectronique.getValue().equals("")) {
//				errorComponent = txtVersionElectronique;
//				errorMsg = Labels
//						.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.procesverbalouverture.ficher")
//						+ ": "
//						+ Labels.getLabel("kermel.erreur.champobligatoire");
//				lbStatusBar.setStyle(ERROR_MSG_STYLE);
//				lbStatusBar.setValue(errorMsg);
//				throw new WrongValueException(errorComponent, errorMsg);
//			}
				
				
			

			

	}

	public void onClick$menuValider() {
		checkFieldConstraints(); 
		
			documents = BeanLocator.defaultLookup(DocumentsSession.class).find(
					0, -1, dossier, appel,
					UIConstants.PARAM_TYPEDOCUMENTS_PVOUVERTURE, null, null);
			if (documents.size() > 0)
				document = documents.get(0);

			document.setSignedByPresident(signedPr);
			document.setSignedBySecretaire(signedSec);
			document.setAppel(appel);
			document.setDossier(dossier);
			document.setNomFichier(nomFichier);
			document.setTypeDocument(UIConstants.PARAM_TYPEDOCUMENTS_PVOUVERTURE);
			document.setLibelle(Labels
					.getLabel("kermel.plansdepassation.proceduresmarches.documents.pvdouverture"));
			document.setDate(new Date());
			document.setHeure(Calendar.getInstance().getTime());
			if (documents.size() == 0) {
				BeanLocator.defaultLookup(DocumentsSession.class)
						.save(document);
			} else {
				BeanLocator.defaultLookup(DocumentsSession.class).update(
						document);
			}
			appel.setApoDatepvouverturepli(dtouverture.getValue());
			BeanLocator.defaultLookup(AppelsOffresSession.class).update(appel);

			BeanLocator.defaultLookup(DossiersAppelsOffresSession.class)
					.update(dossier);
			session.setAttribute("libelle", "procesverbalouverture");
			loadApplicationState("ouverture_plis");
		
	}

	public void onClick$image() {
		// step0.setVisible(false);
		// step1.setVisible(true);
		String filepath = cheminDossier + documents.get(0).getNomFichier();
		File f = new File(filepath.replaceAll("\\\\", "/"));

		org.zkoss.util.media.AMedia mymedia = null;
		try {
			mymedia = new AMedia(f, null, null);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (mymedia != null)
			idIframe.setContent(mymedia);
		else
			idIframe.setSrc("");

		idIframe.setHeight("600px");
		idIframe.setWidth("100%");
	}

	public org.zkoss.util.media.AMedia fetchFile(File file) {

		org.zkoss.util.media.AMedia mymedia = null;
		try {
			mymedia = new AMedia(file, null, null);
			return mymedia;
		} catch (Exception e) {

			e.printStackTrace();
			return null;
		}

	}

	public void onClick$menuFermer() {
		menuFermer.setVisible(false);
		checkPv();

	}

	public void onClick$menuEditer() {
		// if(appel.getApoDatepvouverturepli()!=null)
		// {

		// idIframe.setSrc(EDIT_URL + "?code=" + dossier.getDosID()
		// + "&libelle=ouvertureplis");
		// idIframe.setVisible(true);
		// menuFermer.setVisible(true);

		try {
			genererProcesVerbalWord();
		} catch (IOException e) {

			e.printStackTrace();
		}

		// step0.setVisible(false);
		// step1.setVisible(true);
		// }
		// else
		// {
		// throw new WrongValueException(menuEditer,
		// Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.saisirproces"));
		// }
		//

	}

	public String getCheminPv() {
		File f=new File(UIConstants.PATH_PJ );
		if(!f.exists())
			f.mkdirs();
		String path = UIConstants.PATH_PJ + nomFichier;

		path = ToolKermel.formatPath(path);

		return path;
	}

	public void checkPv() {

		File pv = new File(getCheminPv());
		if (pv.exists()) {
			showPV();

		}

		checkSigned(pv);

	}

	public void checkSigned(File pv) {

		if (!signedPr || !signedSec) {
			if (pv.exists() && !appletLaunched ) {
				divPv.setVisible(true);		
				String urlPv = UIConstants.PJ_URL + pv.getName();
				Clients.evalJavaScript("startAppletLoader('" + urlPv + "','"
						+ pv.getName() + "');");
				
			}
		} else if (signedPr && signedSec) {
			menuEnvoyer.setDisabled(false);
		}

		if (!signedPr) {
			btnSignByPresident.setDisabled(false);
			if (nomFichier == null) {
				lblStatutSignPr.setValue("En attente de la jointure du pv");
				lblStatutSignPr.setSclass("danger");
			} else {
				lblStatutSignPr.setValue("En attente de signature");
				lblStatutSignPr.setSclass("warning");
			}
		} else {
			btnSignByPresident.setDisabled(true);
			lblStatutSignPr.setValue("Signature effectuée");
			lblStatutSignPr.setSclass("success");
		}

		if (!signedSec) {
			btnSignBySecretaire.setDisabled(false);
			if (nomFichier == null) {
				lblStatutSignSec.setValue("En attente de la jointure du pv");
				lblStatutSignSec.setSclass("danger");
			} else {
				lblStatutSignSec.setValue("En attente de signature");
				lblStatutSignSec.setSclass("warning");
			}
		} else {
			btnSignBySecretaire.setDisabled(true);
			lblStatutSignSec.setValue("Signature effectuée");
			lblStatutSignSec.setSclass("success");
		}

	}

	public void onClick$btnSignByPresident() {
		loadApplicationState("signature_pli");
		
//		Clients.evalJavaScript("sign('PR');");
	}

	public void onClick$btnSignBySecretaire() {
		loadApplicationState("signature_pli");
//		Clients.evalJavaScript("sign('SEC');");
	}

	public void onClick$btnSigned() {
//		signedPr = true;
//		onClick$menuValider();
//		checkPv();
//		File pv = new File(getCheminPv());
//		String urlPv = UIConstants.PJ_URL + pv.getName();
//		Clients.evalJavaScript("startAppletLoader('" + urlPv + "','"
//				+ pv.getName() + "');");
//		appletLaunched = true;
		
	}

	public void onClick$btnSignedSec() {
		signedSec = true;
		onClick$menuValider();
		checkPv();
	}

	public void onClick$menuSigner() {
		loadApplicationState("signature_pli");
//		Clients.evalJavaScript("sign();");

	}

	public void onClick$menuEnvoyer() {
		List<String> recipients = getRecipients();
		String messageContent = "Merci de recevoir en pièce jointe le procès verbal d' ouverture des plis <br/>";
		messageContent+= "<br/> <b>Autorité contractante </b> : "+ dossier.getAutorite().getDenomination();
		messageContent+= "<br/> <b> Marché : </b>"+ dossier.getRealisation().getLibelle();
		messageContent+= "<br/> <b> Référence : </b>"+ dossier.getDosReference();
		MessageManager.sendMail("PV ouverture des plis  ("+dossier.getRealisation().getLibelle()+" - "+ dossier.getDosReference(),
				messageContent, recipients, getCheminPv(), "Procesverbal.pdf",
				null, null);

	}

	public void onClick$menuJoindre() {

		onClick$btnChoixFichier();

	}

	public List<String> getRecipients() {
		List<String> emails = new ArrayList<String>();

		for (SygMembresCommissionsMarches membrecommission : membrescommissions) {
			if (membrecommission.getEmail() != null) {
				emails.add(membrecommission.getEmail());
			}
		}

		for (SygPresenceouverture representant : representantssoumissionnaires) {
			if (representant.getEmail() != null) {
				emails.add(representant.getEmail());
			}
		}

		// emails.add("moussa.ba.bam@gmail.com");
		// emails.add("mba@ssi.sn");
		// emails.add("mfaye@ssi.sn");

		return emails;

	}

	public void showPV() {

		idIframe.invalidate();
		File pv = new File(getCheminPv());
	

		if (pv.exists()) {

			AMedia media = fetchFile(pv);
			if (media != null) {
				idIframe.setContent(media);
				idIframe.setVisible(true);
			}
		}

	}

	public void genererProcesVerbalWord() throws IOException {

		String chemin = UIConstants.PATH_PJ;
		File dossierPj = new File(ToolKermel.formatPath(chemin));
		if (!dossierPj.exists()) {
			dossierPj.mkdirs();
		}
		String fichiernom = "procesverbal" + realisation.getReference()
				+ ".docx";
		nomFichier = fichiernom;
		String cheminFichier = chemin + fichiernom;
		if (ToolKermel.isWindows()) {
			cheminFichier = cheminFichier.replaceAll("/", "\\\\");
		} else {
			cheminFichier = cheminFichier.replaceAll("\\\\", "/");

		}
		File file = null;
		FileOutputStream fos = null;
		XWPFDocument document = null;
		XWPFParagraph para = null;
		XWPFRun run = null;
		try {
			// Create the first paragraph and set it's text.
			document = new XWPFDocument();
			para = document.createParagraph();

			para.setAlignment(ParagraphAlignment.CENTER);
			para.setStyle("width:200px");
			para.setSpacingAfter(10);

			para.setSpacingAfterLines(10);
			run = para.createRun();

			XWPFParagraph para_entete = document.createParagraph();
			para_entete.setAlignment(ParagraphAlignment.CENTER);
			XWPFRun run_entete = para_entete.createRun();
			run_entete.setText("REPUBLIQUE DU SENEGAL ");
			run_entete.setFontSize(16);

			run_entete.addBreak();
			run_entete.setText("---------  ");
			run_entete.addBreak();

			run_entete.addBreak();
			run_entete.addBreak();
			run_entete.addBreak();
			run_entete.setText("PROCES-VERBAL D' OUVERTURE DES PLIS");

			run_entete.addBreak();
			run_entete.addBreak();
			run_entete.addBreak();
			run_entete.addBreak();
			run_entete.addBreak();

			XWPFParagraph paraEntete2 = document.createParagraph();
			paraEntete2.setSpacingBeforeLines(10);
			paraEntete2.setBorderTop(Borders.DOUBLE);
			paraEntete2.setBorderBottom(Borders.DOUBLE);
			paraEntete2.setBorderRight(Borders.DOUBLE);
			paraEntete2.setBorderLeft(Borders.DOUBLE);
			paraEntete2.setAlignment(ParagraphAlignment.CENTER);

			XWPFRun runEntete2 = paraEntete2.createRun();
			runEntete2.addBreak();
			runEntete2.setText("Autorité contractante : ");
			runEntete2.setText(realisation.getPlan().getAutorite()
					.getDenomination());
			runEntete2.setBold(true);
			runEntete2.setFontSize(14);

			runEntete2.addBreak();
			runEntete2.addBreak();

			XWPFRun runEntete_date = paraEntete2.createRun();

			runEntete_date.setFontSize(14);
			runEntete_date.setText("\t Date : "
					+ ToolKermel.dateToString(dossier
							.getDosDateOuvertueDesplis()));
			runEntete_date.addBreak();

			XWPFRun runEntete_lieu = paraEntete2.createRun();

			runEntete_lieu.setFontSize(14);
			runEntete_lieu.setText("\t Lieu : "
					+ dossier.getDosLieuOuvertureDesPlis());
			runEntete_lieu.addBreak();

			XWPFParagraph para1 = document.createParagraph();
			XWPFRun run1 = para1.createRun();
			run1.addBreak();
			run1.addBreak();
			run1.setText("Marché ");
			run1.setFontSize(14);
			run1.setBold(true);
			run1.addBreak();
			run1.addBreak();

			XWPFParagraph para2 = document.createParagraph();
			para2.setSpacingBefore(50);
			XWPFRun run2_1 = para2.createRun();
			run2_1.setBold(true);
			run2_1.setFontSize(14);
			run2_1.setText("\t Référence : ");
			run2_1.setText("\t" + dossier.getDosReference());
			run2_1.addBreak();
			run2_1.addBreak();

			XWPFRun run2_2 = para2.createRun();
			run2_2.setBold(true);
			run2_2.setFontSize(14);
			run2_2.setText("\t Intitulé : ");
			run2_2.setText("\t" + realisation.getLibelle());
			run2_2.addBreak();
			run2_2.addBreak();

			run2_2.addBreak(BreakType.PAGE);

			XWPFParagraph paraText_1_1 = document.createParagraph();
			paraText_1_1.setSpacingAfterLines(10);
			XWPFRun runText_1_1 = paraText_1_1.createRun();
			XWPFRun runText_1_1_mode = paraText_1_1.createRun();
			XWPFRun runText_1_1_ref = paraText_1_1.createRun();
			XWPFRun runText_1_1_portant = paraText_1_1.createRun();
			XWPFRun runText_1_1_real = paraText_1_1.createRun();

			runText_1_1
					.setText("Nous, membres de la commission des marchés, instituée à cette occasion reconnaissons par le présent procès-verbal avoir procédé à l'ouverture des offres relatives à ");
			runText_1_1_mode.setBold(true);
			runText_1_1_mode.setText(appel.getModepassation().getLibelle()
					+ " ");
			runText_1_1_ref.setBold(true);
			runText_1_1_ref.setText(dossier.getDosReference() + " ");
			runText_1_1_portant.setText(" portant ");
			runText_1_1_real.setBold(true);
			runText_1_1_real.setText(realisation.getLibelle());
			// runText_1_1.addBreak();

			XWPFParagraph paraText_1_2 = document.createParagraph();
			XWPFRun runText_1_2 = paraText_1_2.createRun();
			paraText_1_2.setAlignment(ParagraphAlignment.CENTER);
			runText_1_2.addBreak();
			runText_1_2.setText("La séance a eu lieu  "
					+ dossier.getDosLieuOuvertureDesPlis() + " à "
					+ dossier.getDosHeureOuvertureDesPlis());
			runText_1_2.addBreak();
			runText_1_2.addBreak();

			XWPFParagraph paraTitrePresence = document.createParagraph();
			XWPFRun runTitrePresence = paraTitrePresence.createRun();
			runTitrePresence.setText("1. LISTE DE PRESENCE");
			runTitrePresence.setFontSize(16);
			runTitrePresence.addBreak();
			runTitrePresence.setBold(true);

			XWPFParagraph paraTitreAutorite = document.createParagraph();
			paraTitreAutorite.setSpacingBefore(5);
			XWPFRun runTitreAutorite = paraTitreAutorite.createRun();
			runTitreAutorite.setText("\t Pour l'Autorité contractante : ");
			runTitreAutorite.setFontSize(14);
			runTitreAutorite.setBold(true);
			runTitreAutorite.addBreak();

			XWPFParagraph paraTitreMembresCommission = document
					.createParagraph();

			XWPFRun runMembresCommission = paraTitreMembresCommission
					.createRun();

			paraTitreMembresCommission.setSpacingBefore(300);
			paraTitreMembresCommission.setSpacingAfter(300);
			runMembresCommission
					.setText("\t \t Membres de la Comission des Marchés : ");
			runMembresCommission.setFontSize(14);
			runMembresCommission.setBold(true);

			XWPFTable tablePresenceCommission = document.createTable(1, 3);
			tablePresenceCommission.setWidth(100000000);

			XWPFTableRow row1_tableCPresenceCommisssion = tablePresenceCommission
					.getRow(0);
			setCellText(row1_tableCPresenceCommisssion.getCell(0),
					"Prénoms et nom", 9000, true, 6, "CCCCCC");
			setCellText(row1_tableCPresenceCommisssion.getCell(1), "Qualité",
					5000, true, 6, "CCCCCC");
			setCellText(row1_tableCPresenceCommisssion.getCell(2), "Fonction",
					5000, true, 6, "CCCCCC");

			for (int i = 0; i < membrescommissions.size(); i++) {
				XWPFTableRow tableRowTwo = tablePresenceCommission.createRow();
				tableRowTwo.getCell(0).setText(
						membrescommissions.get(i).getPrenom() + "  "
								+ membrescommissions.get(i).getNom());
				tableRowTwo.getCell(1).setText(membrescommissions.get(i).getFonction());
				tableRowTwo.getCell(2).setText("");

			}

			XWPFParagraph paraTitreRapporteur = document.createParagraph();

			paraTitreRapporteur.setSpacingBefore(500);
			paraTitreRapporteur.setSpacingAfter(300);

			XWPFRun runRapporteur = paraTitreRapporteur.createRun();

			runRapporteur.setText("\t \t Rapporteur de la comission : ");
			runRapporteur.setFontSize(14);
			runRapporteur.setBold(true);

			XWPFTable tablePresenceRapporteur = document.createTable(2, 3);
			tablePresenceCommission.setWidth(100000000);

			XWPFTableRow row1_tablePresenceRapporteur = tablePresenceRapporteur
					.getRow(0);

			setCellText(row1_tablePresenceRapporteur.getCell(0),
					"Prénoms et nom", 9000, true, 6, "CCCCCC");
			setCellText(row1_tablePresenceRapporteur.getCell(1), "Qualité",
					5000, true, 6, "CCCCCC");
			setCellText(row1_tablePresenceRapporteur.getCell(2), "Fonction",
					5000, true, 6, "CCCCCC");

			if (rapporteur != null) {

				XWPFTableRow row2_tablePresenceRapporteur = tablePresenceRapporteur
						.getRow(1);

				row2_tablePresenceRapporteur.getCell(0).setText(
						rapporteur.getPrenom() + " " + rapporteur.getNom());
				row2_tablePresenceRapporteur.getCell(1).setText("Secrétaire de la cellule de passation");
				row2_tablePresenceRapporteur.getCell(2).setText(
						"");

			}

			XWPFParagraph paraTitreCandidat = document.createParagraph();

			paraTitreCandidat.setSpacingBefore(500);
			paraTitreCandidat.setSpacingAfter(300);

			XWPFRun runPresenceCandidat = paraTitreCandidat.createRun();

			runPresenceCandidat.setText("\t \t Pour les candidats : ");
			runPresenceCandidat.setFontSize(14);
			runPresenceCandidat.setBold(true);

			XWPFTable tablePresenceCandidats = document.createTable(1, 3);
			XWPFTableRow tablePresenceCandidats_row1 = tablePresenceCandidats
					.getRow(0);
			setCellText(tablePresenceCandidats_row1.getCell(0),
					"Prénoms et nom", 9000, true, 6, "CCCCCC");
			setCellText(tablePresenceCandidats_row1.getCell(1),
					"Entreprise ou société représentée", 12000, true, 6,
					"CCCCCC");
			setCellText(tablePresenceCandidats_row1.getCell(2), "Contact",
					9000, true, 6, "CCCCCC");

			for (int i = 0; i < representantssoumissionnaires.size(); i++) {
				XWPFTableRow tableRowTwo = tablePresenceCandidats.createRow();
				tableRowTwo.getCell(0).setText(
						representantssoumissionnaires.get(i)
								.getPrenomrepresentant()
								+ " "
								+ representantssoumissionnaires.get(i)
										.getNomrepresentant());
				tableRowTwo.getCell(1).setText(
						representantssoumissionnaires.get(i).getPlis()
								.getFournisseur().getNom());
				// Plis()
				// .getRetrait().getNomSoumissionnaire());
				tableRowTwo.getCell(2).setText(
						representantssoumissionnaires.get(i).getTelephone()
								+ " / "
								+ representantssoumissionnaires.get(i)
										.getEmail());

			}

			XWPFParagraph paraTitreReception = document.createParagraph();
			paraTitreReception.setSpacingBefore(500);
			XWPFRun runTitreReception = paraTitreReception.createRun();

			runTitreReception.setText("2. RECEPTION DES OFFRES");
			runTitreReception.setFontSize(16);
			runTitreReception.setBold(true);
			runTitreReception.addBreak();

			XWPFTable tableReception = document.createTable(1, 3);
			XWPFTableRow row1_tableReception = tableReception.getRow(0);
			setCellText(row1_tableReception.getCell(0), "N° ordre", 200, true,
					6, "CCCCCC");
			setCellText(row1_tableReception.getCell(1), "Soumissionnaires",
					9000, true, 6, "CCCCCC");
			setCellText(row1_tableReception.getCell(2), "Date dépôt", 6000,
					true, 6, "CCCCCC");

			for (int i = 0; i < plis.size(); i++) {
				XWPFTableRow tableRowTwo = tableReception.createRow();
				tableRowTwo.getCell(0).setText(plis.get(i).getNumero() + "");
				tableRowTwo.getCell(1).setText(
						plis.get(i).getFournisseur().getNom());
				tableRowTwo.getCell(2).setText(
						ToolKermel.dateToString(plis.get(i).getDateDepot()) + " à "+ plis.get(i).getHeuredepot());

			}

			XWPFParagraph paraTitrePieces = document.createParagraph();
			paraTitrePieces.setSpacingBefore(500);
			XWPFRun runTitrePieces = paraTitrePieces.createRun();

			runTitrePieces.setText("3. PIECES ADMINISTRATIVES");
			runTitrePieces.setFontSize(16);
			runTitrePieces.setBold(true);
			runTitrePieces.addBreak();

			int nbrCells = 1 + piecesadministratives.size();
			XWPFTable tablePieces = document.createTable(1, nbrCells);
			XWPFTableRow row1_tablePieces = tablePieces.getRow(0);
			setCellText(row1_tablePieces.getCell(0), "Soumissionnaires", 200,
					true, 6, "CCCCCC");
			setCellText(row1_tablePieces.getCell(1), "Pièces administratives",
					9000, true, 6, "CCCCCC");

			XWPFTableRow tablePieceRowTwo = tablePieces.createRow();
			for (int i = 0; i < piecesadministratives.size(); i++) {

				ParagraphAlignment alignement = ParagraphAlignment.CENTER;
				XWPFParagraph paragraph = tablePieceRowTwo.getCell(i + 1)
						.getParagraphs().get(0);
				paragraph.setAlignment(alignement);

				XWPFRun run_piece = paragraph.createRun();
				run_piece.setText(piecesadministratives.get(i).getPiece()
						.getLibelle());

			}

			for (int i = 0; i < plis.size(); i++) {
				XWPFTableRow tablePieceRowPliPiece = tablePieces.createRow();

				tablePieceRowPliPiece.getCell(0).setText(
						plis.get(i).getFournisseur().getNom());

				for (int j = 0; j < piecesadministratives.size(); j++) {

					List<SygPiecesplisouvertures> pieces = BeanLocator
							.defaultLookup(PiecessoumissionnairesSession.class)
							.find(0, -1, dossier,
									piecesadministratives.get(j).getPiece(),
									plis.get(i), null);

					tablePieceRowPliPiece.getCell(j + 1).setVerticalAlignment(
							XWPFVertAlign.CENTER);

					ParagraphAlignment alignement = ParagraphAlignment.CENTER;
					XWPFParagraph paragraph = tablePieceRowPliPiece
							.getCell(j + 1).getParagraphs().get(0);
					paragraph.setAlignment(alignement);
					paragraph.setVerticalAlignment(TextAlignment.CENTER);
					XWPFRun run_p = paragraph.createRun();

					if (!pieces.isEmpty())
						run_p.setText(pieces.get(0).getEtat());

					else
						run_p.setText("NF");

				}

			}

			mergeCellsVertically(tablePieces, 0, 0, 1);
			mergeCellsHorizontal(tablePieces, 0, 1, nbrCells - 1);
			
			XWPFParagraph paraLegendePieces = document.createParagraph();
			paraLegendePieces.setSpacingBefore(300);
			XWPFRun runLegendePieces = paraLegendePieces.createRun();
			runLegendePieces.setFontSize(14);
			runLegendePieces.setBold(true);
			runLegendePieces.setText("F = FOURNIE");			
			runLegendePieces.addBreak();
			runLegendePieces.setText("NF = NON FOURNIE");
			runLegendePieces.addBreak();


			// setCellText(row1_tablePieces.getCell(2), "Date dépôt", 6000,
			// true, 6, "CCCCCC");

			// for (int i = 0; i < plis.size(); i++) {
			// XWPFTableRow tableRowTwo = tablePieces.createRow();
			// tableRowTwo.getCell(0)
			// .setText(
			// plis.get(i).getNumero()
			// + "");
			// tableRowTwo.getCell(1).setText(
			// plis.get(i).getFournisseur().getNom());
			// tableRowTwo.getCell(2).setText(
			// ToolKermel.dateToString(plis.get(i).getDateDepot()));
			//
			//
			//
			// }

			XWPFParagraph paraPropositionOffres = document.createParagraph();
			paraPropositionOffres.setSpacingBefore(300);
			XWPFRun runTitrePorpositionOffres = paraPropositionOffres
					.createRun();

			runTitrePorpositionOffres.addBreak();
			runTitrePorpositionOffres.setText("4. LECTURE DES OFFRES");
			runTitrePorpositionOffres.setFontSize(16);
			runTitrePorpositionOffres.setBold(true);
			runTitrePorpositionOffres.addBreak();

			XWPFTable table8 = document.createTable(1, 5);
			XWPFTableRow row1_table8 = table8.getRow(0);
			setCellText(row1_table8.getCell(0), "N°", 200, true, 6, "CCCCCC");
			setCellText(row1_table8.getCell(1), "Nom ou Raison sociale", 9000,
					true, 6, "CCCCCC");
			setCellText(row1_table8.getCell(2), "Lot", 6000, true, 6, "CCCCCC");

			setCellText(row1_table8.getCell(3), "Montant lu ( en F. C.F.A)", 6000, true,
					6, "CCCCCC");

			for (int i = 0; i < lotssoummissionnaires.size(); i++) {
				XWPFTableRow tableRowTwo = table8.createRow();
				tableRowTwo.getCell(0)
						.setText(
								lotssoummissionnaires.get(i).getPlis()
										.getNumero()
										+ "");
				tableRowTwo.getCell(1).setText(
						lotssoummissionnaires.get(i).getPlis().getFournisseur()
								.getNom());
				tableRowTwo.getCell(2).setText(
						lotssoummissionnaires.get(i).getLot().getLibelle());

				setCellTextLigne(tableRowTwo.getCell(3),
						ToolKermel.format3Decimal(lotssoummissionnaires.get(i)
								.getPlilmontantoffert()), true, 6, "CCCCCC");

			}

			// XWPFParagraph para3 = document.createParagraph();
			// XWPFRun run3 = para3.createRun();
			// run3.setText("(Nom, Adresse, Direction, Sous-direction, Bureau, T�l�copie, T�l�phone, E-mail)");
			//
			// XWPFParagraph para4 = document.createParagraph();
			// XWPFRun run4_1 = para4.createRun();
			// run4_1.setText("Objet de l�appel d�offres ou de la consultation: ");
			// XWPFRun run4_2 = para4.createRun();
			// run4_2.setText(realisation.getLibelle() + ".");
			// run4_2.setBold(true);
			//
			// XWPFParagraph para5 = document.createParagraph();
			// XWPFRun run5_1 = para5.createRun();
			// run5_1.setText("Avis d�appel public � la concurrence n�: ");
			// XWPFRun run5_2 = para5.createRun();
			// run5_2.setText(realisation.getReference()
			// + " du "
			// + utilVue.getInstance().formateLaDate2(
			// appel.getApodatecreation()) + ".");
			// run5_2.setBold(true);
			//
			// XWPFParagraph para6 = document.createParagraph();
			// XWPFRun run6 = para6.createRun();
			// run6.setText("Organe(s) et date(s) de parution de l�avis :");
			//
			// XWPFParagraph para7 = document.createParagraph();
			// XWPFRun run7 = para7.createRun();
			// run7.setText(" B. Composition et fonctionnement de la commission de passation des march�s publics ");
			// run7.setBold(true);
			//
			// XWPFParagraph para8 = document.createParagraph();
			// XWPFRun run8 = para8.createRun();
			// run8.setText("   � La composition de la commission de passation des march�s publics a �t� fix�e par d�cision du");
			// run8.addBreak();
			// run8.setText("   � Date(s) de la (des) r�union(s):");
			//
			// XWPFTable table1 = document.createTable();
			// table1.setWidth(800);
			// XWPFTableRow tableRowOne = table1.getRow(0);
			// tableRowOne.getCell(0).setText("       ");
			// tableRowOne.getCell(0).getCTTc().addNewTcPr().addNewTcW()
			// .setW(BigInteger.valueOf((long) 26000));
			//
			// XWPFParagraph para9 = document.createParagraph();
			// XWPFRun run9 = para9.createRun();
			// run9.addBreak();
			// run9.setText("   � Membres de la Commission de passation des march�s publics : (placer le nom du Pr�sident en premier)");
			//
			// // create table
			//
			// XWPFParagraph para10 = document.createParagraph();
			// XWPFRun run10 = para10.createRun();
			// run10.addBreak();
			// run10.setText("   � Le quorum est atteint :");
			// run10.addBreak();
			// run10.setText("     (Le quorum doit �tre atteint non seulement � l'ouverture de la s�ance mais encore lors des d�bats et du vote de la commission)");
			// run10.addBreak();
			// run10.setText("     La commission peut, ne peut pas, (rayer la  mention inutile) valablement d�lib�rer.");
			//
			// XWPFParagraph para11 = document.createParagraph();
			// XWPFRun run11 = para11.createRun();
			// run11.setText(" C. Soumissionnaires ou leurs repr�sentants / et autres  (sur pr�sentation d�un titre justificatif) ");
			// run11.setBold(true);
			//
			//
			// XWPFParagraph para12 = document.createParagraph();
			// XWPFRun run12 = para12.createRun();
			// run12.addBreak();
			// run12.setText(" D. Repr�sentants du service technique b�n�ficiaire de l�autorit� contractante ");
			// run12.setBold(true);
			//
			// XWPFTable table4 = document.createTable(1, 4);
			// table4.setWidth(100000000);
			// XWPFTableRow row1_table4 = table4.getRow(0);
			// setCellText(row1_table4.getCell(0), "Nom, pr�noms", 9000, true,
			// 6,
			// "CCCCCC");
			// setCellText(row1_table4.getCell(1), "Qualit�", 5000, true, 6,
			// "CCCCCC");
			// setCellText(row1_table4.getCell(2), "Signature", 5000, true, 6,
			// "CCCCCC");
			// setCellText(row1_table4.getCell(3), "Absent mais convoqu� le",
			// 7000, true, 6, "CCCCCC");
			//
			// // row1_table4.getCell(0).setText("Nom, pr�noms          ");
			// //
			// row1_table4.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf((long)9000));
			// // row1_table4.addNewTableCell().setText("Qualit�     ");
			// //
			// row1_table4.getCell(1).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf((long)5000));
			// // row1_table4.addNewTableCell().setText("Signature");
			// //
			// row1_table4.getCell(2).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf((long)5000));
			// //
			// row1_table4.addNewTableCell().setText("Absent mais convoqu� le");
			// //
			// row1_table4.getCell(3).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf((long)7000));
			//
			// for (int i = 0; i < representantsservicestechniques.size(); i++)
			// {
			// XWPFTableRow tableRowTwo = table4.createRow();
			// tableRowTwo.getCell(0).setText(
			// representantsservicestechniques.get(i)
			// .getRepresentant());
			// tableRowTwo.getCell(1).setText(
			// representantsservicestechniques.get(i).getQualite());
			// tableRowTwo.getCell(2).setText("");
			// if (representantsservicestechniques.get(i).getDateconvocation()
			// != null)
			// tableRowTwo.getCell(3).setText(
			// UtilVue.getInstance().formateLaDate2(
			// representantsservicestechniques.get(i)
			// .getDateconvocation()));
			// else
			// tableRowTwo.getCell(3).setText("");
			// }
			//
			// XWPFParagraph para13 = document.createParagraph();
			// XWPFRun run13 = para13.createRun();
			// run13.addBreak();
			// run13.setText(" E. Observateur Ind�pendant/ personnes ressources ");
			// run13.setBold(true);
			//
			// XWPFTable table5 = document.createTable(1, 4);
			// table5.setWidth(100000000);
			// XWPFTableRow row1_table5 = table5.getRow(0);
			// setCellText(row1_table5.getCell(0), "Nom, pr�noms", 9000, true,
			// 6,
			// "CCCCCC");
			// setCellText(row1_table5.getCell(1), "Qualit�", 5000, true, 6,
			// "CCCCCC");
			// setCellText(row1_table5.getCell(2), "Signature", 5000, true, 6,
			// "CCCCCC");
			// setCellText(row1_table5.getCell(3), "Absent mais convoqu� le",
			// 7000, true, 6, "CCCCCC");
			//
			// for (int i = 0; i < observateursindependants.size(); i++) {
			// XWPFTableRow tableRowTwo = table5.createRow();
			// tableRowTwo.getCell(0).setText(
			// observateursindependants.get(i).getRepresentant());
			// tableRowTwo.getCell(1).setText(
			// observateursindependants.get(i).getQualite());
			// tableRowTwo.getCell(2).setText("");
			// if (observateursindependants.get(i).getDateconvocation() != null)
			// tableRowTwo.getCell(3).setText(
			// UtilVue.getInstance().formateLaDate2(
			// observateursindependants.get(i)
			// .getDateconvocation()));
			// else
			// tableRowTwo.getCell(3).setText("");
			// }
			//
			// XWPFParagraph para14 = document.createParagraph();
			// XWPFRun run14 = para14.createRun();
			// run14.addBreak();
			// run14.setText(" F. Commission technique ");
			// run14.setBold(true);
			//
			// XWPFTable table6 = document.createTable(1, 5);
			// table5.setWidth(100000000);
			// XWPFTableRow row1_table6 = table6.getRow(0);
			// setCellText(row1_table6.getCell(0), "Pr�nom", 5000, true, 6,
			// "CCCCCC");
			// setCellText(row1_table6.getCell(1), "Nom", 2000, true, 6,
			// "CCCCCC");
			// setCellText(row1_table6.getCell(2), "Fonction", 6000, true, 6,
			// "CCCCCC");
			// setCellText(row1_table6.getCell(3), "T�l�phone", 2000, true, 6,
			// "CCCCCC");
			// setCellText(row1_table6.getCell(4), "Email", 2000, true, 6,
			// "CCCCCC");
			//
			// for (int i = 0; i < commissionstechniques.size(); i++) {
			// XWPFTableRow tableRowTwo = table6.createRow();
			// tableRowTwo.getCell(0).setText(
			// commissionstechniques.get(i).getEvaluateur()
			// .getPrenom());
			// tableRowTwo.getCell(1).setText(
			// commissionstechniques.get(i).getEvaluateur().getNom());
			// tableRowTwo.getCell(2).setText(
			// commissionstechniques.get(i).getEvaluateur()
			// .getFonction());
			// tableRowTwo.getCell(3).setText(
			// commissionstechniques.get(i).getEvaluateur()
			// .getTelephone());
			// tableRowTwo.getCell(4)
			// .setText(
			// commissionstechniques.get(i).getEvaluateur()
			// .getEmail());
			//
			// }
			//
			// XWPFParagraph para15 = document.createParagraph();
			// XWPFRun run15 = para15.createRun();
			// run15.addBreak();
			// run15.setText(" F. D�cision de la Commission de passation des march�s publics ");
			// run15.setBold(true);
			//
			// XWPFTable table7 = document.createTable(3, 9);
			//
			// setCellText(table7.getRow(0).getCell(0),
			// "N� d�ordre au registre sp�cial des d�p�ts", 2000, true, 6,
			// "CCCCCC");
			// mergeCellsVertically(table7, 0, 0, 2);
			//
			// //
			// row1_table7.getCell(1).setText("Nom du candidat ou des candidats group�s. Souligner le nom du mandataire");
			// setCellText(
			// table7.getRow(0).getCell(1),
			// "Nom du candidat ou des candidats group�s. Souligner le nom du mandataire",
			// 2000, true, 6, "CCCCCC");
			// mergeCellsVertically(table7, 1, 0, 2);
			//
			// setCellText(table7.getRow(0).getCell(2), "D�cisions d�examen",
			// 6000, true, 6, "CCCCCC");
			// mergeCellsHorizontal(table7, 0, 2, 7);
			//
			// setCellText(
			// table7.getRow(0).getCell(8),
			// "Motifs (avec renvoi � l'article du code des march�s publics ad�quat)",
			// 2000, true, 6, "CCCCCC");
			// mergeCellsVertically(table7, 8, 0, 2);
			//
			// setCellText(table7.getRow(1).getCell(2), "Pli non ouvert)", 2000,
			// true, 6, "CCCCCC");
			// mergeCellsVertically(table7, 2, 1, 2);
			//
			// setCellText(
			// table7.getRow(1).getCell(3),
			// "Contenu des Plis ouverts (mentionner P : document pr�sent ; M : document manquant)",
			// 7000, true, 6, "CCCCCC");
			// mergeCellsHorizontal(table7, 1, 3, 7);
			//
			// setCellText(table7.getRow(2).getCell(3), "", 2000, true, 6,
			// "CCCCCC");
			// setCellText(table7.getRow(2).getCell(4), "", 2000, true, 6,
			// "CCCCCC");
			// setCellText(table7.getRow(2).getCell(5), "", 2000, true, 6,
			// "CCCCCC");
			// setCellText(table7.getRow(2).getCell(6), "", 2000, true, 6,
			// "CCCCCC");
			// setCellText(table7.getRow(2).getCell(7), "", 2000, true, 6,
			// "CCCCCC");
			//
			// XWPFParagraph para16 = document.createParagraph();
			// XWPFRun run16 = para16.createRun();
			// run16.addBreak();
			// run16.setText(" H. Lecture du montant des offres ");
			// run16.setBold(true);
			//
			//
			// XWPFParagraph para17 = document.createParagraph();
			// XWPFRun run17 = para17.createRun();
			// run17.addBreak();
			// run17.setText(" I. Incidents �ventuels survenus au cours de la s�ance ");
			// run17.setBold(true);
			//
			// XWPFParagraph para18 = document.createParagraph();
			// XWPFRun run18 = para18.createRun();
			// run18.addBreak();
			// if (dossier != null)
			// run18.setText(dossier.getDosIncidents());
			
			
			
			XWPFParagraph paraTitreObservation = document.createParagraph();
			paraTitreObservation.setSpacingBefore(300);
			paraTitreObservation.setSpacingAfter(500);
			XWPFRun runTitreObservations = paraTitreObservation.createRun();

			runTitreObservations.setText("5. OBSERVATIONS");
			runTitreObservations.setFontSize(16);
			runTitreObservations.setBold(true);
			runTitreObservations.addBreak();
			

			XWPFParagraph para19 = document.createParagraph();
			para19.setAlignment(ParagraphAlignment.RIGHT);
			
			XWPFRun run19 = para19.createRun();
			run19.addBreak();
			run19.addBreak();
			run19.addBreak();
			run19.setFontSize(14);
			run19.setText("Fait à Dakar le "
					+ UtilVue.getInstance().formateLaDate2(new Date()));
			run19.setBold(true);
			
			run19.addBreak();
			run19.addBreak();

			run19.setText("Le rapporteur");
			
			run19.addBreak();
			
			run19.setText(rapporteur.getPrenom()+ " "+ rapporteur.getNom());
			
			file = new File(cheminFichier);
			if (file.exists())
				FileUtils.forceDelete(file);

			FileOutputStream out = new FileOutputStream(file);
			document.write(out);
			out.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		showPV();

		// if(appel.getApoDatepvouverturepli()!=null)
		// {
		// idIframe.setSrc(EDIT_URL + "?code="+dossier.getDosID()+
		// "&libelle=ouvertureplis");
		// step0.setVisible(false);
		// step1.setVisible(true);

		// nomFichier = "proces_verbal.rptdesign";
		// format = UIConstants.FORMAT_WORD_BIRT;
		// System.out.println(EDIT_URL_Birt + nomFichier + "&code=" +
		// dossier.getDosID() + "&__format=" + format );
		// idIframe.isInvalidated();
		// idIframe.setSrc(EDIT_URL_Birt + nomFichier + "&dossier=" +
		// dossier.getDosID()+ "&autorite=" + autorite.getId()+ "&gestion=" +
		// Integer.toString(dossier.getRealisation().getPlan().getAnnee()) +
		// "&__format=" + format );

		// }
		// else
		// {
		// throw new WrongValueException(menuEditer,
		// Labels.getLabel("sygmap.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.saisirproces"));
		// }
		//

	}

	public void setCellText(XWPFTableCell cell, String text, int width,
			boolean isShd, int shdValue, String shdColor) {
		CTTc cttc = cell.getCTTc();
		CTTcPr ctPr = cttc.isSetTcPr() ? cttc.getTcPr() : cttc.addNewTcPr();
		CTShd ctshd = ctPr.isSetShd() ? ctPr.getShd() : ctPr.addNewShd();
		ctPr.addNewTcW().setW(BigInteger.valueOf(width));
		if (isShd) {
			if (shdValue > 0 && shdValue <= 38) {
				ctshd.setVal(STShd.Enum.forInt(shdValue));
			}
			if (shdColor != null) {
				// ctshd.setFill(shdColor);
				// ctshd.setColor("auto");
				ctshd.setColor(shdColor);
			}
		}

		ctPr.addNewVAlign().setVal(STVerticalJc.CENTER);
		cttc.getPList().get(0).addNewPPr().addNewJc().setVal(STJc.LEFT);
		cell.setText(text);
	}

	public void setCellTextLigne(XWPFTableCell cell, String text,
			boolean isShd, int shdValue, String shdColor) {
		CTTc cttc = cell.getCTTc();
		CTTcPr ctPr = cttc.isSetTcPr() ? cttc.getTcPr() : cttc.addNewTcPr();
		CTShd ctshd = ctPr.isSetShd() ? ctPr.getShd() : ctPr.addNewShd();

		if (isShd) {
			if (shdValue > 0 && shdValue <= 38) {
				ctshd.setVal(STShd.Enum.forInt(shdValue));
			}
			if (shdColor != null) {
				// ctshd.setFill(shdColor);
				// ctshd.setColor("auto");
				ctshd.setColor(shdColor);
			}
		}

		ctPr.addNewVAlign().setVal(STVerticalJc.CENTER);
		cttc.getPList().get(0).addNewPPr().addNewJc().setVal(STJc.RIGHT);
		cell.setText(text);
	}

	/**
	 * @Les colonnes de fusion
	 */
	public void mergeCellsHorizontal(XWPFTable table, int row, int fromCell,
			int toCell) {
		for (int cellIndex = fromCell; cellIndex <= toCell; cellIndex++) {
			XWPFTableCell cell = table.getRow(row).getCell(cellIndex);
			if (cellIndex == fromCell) {
				cell.getCTTc().addNewTcPr().addNewHMerge()
						.setVal(STMerge.RESTART);
			} else {
				cell.getCTTc().addNewTcPr().addNewHMerge()
						.setVal(STMerge.CONTINUE);
			}
		}
	}

	/**
	 * @La ligne de fusion
	 */
	public void mergeCellsVertically(XWPFTable table, int col, int fromRow,
			int toRow) {
		for (int rowIndex = fromRow; rowIndex <= toRow; rowIndex++) {
			XWPFTableCell cell = table.getRow(rowIndex).getCell(col);
			if (rowIndex == fromRow) {
				cell.getCTTc().addNewTcPr().addNewVMerge()
						.setVal(STMerge.RESTART);
			} else {
				cell.getCTTc().addNewTcPr().addNewVMerge()
						.setVal(STMerge.CONTINUE);
			}
		}
	}

}