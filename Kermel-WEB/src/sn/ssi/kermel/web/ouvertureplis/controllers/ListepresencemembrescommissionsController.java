package sn.ssi.kermel.web.ouvertureplis.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierscommissionsmarches;
import sn.ssi.kermel.be.entity.SygMembresCommissionsMarches;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossierscommissionsmarchesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.be.referentiel.ejb.MembresCommissionsMarchesSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class ListepresencemembrescommissionsController extends AbstractWindow
		implements EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe, lstListe2;
	private Paging pgPagination, pgPagination2;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE, activePage;
	public static final String CURRENT_MODULE = "CURRENT_MODULE";
	public Textbox txtLibelle;
	String nom = null, page = null, login, codesuppression, libellesuppression,
			gestion;
	Session session = getHttpSession();
	SygAppelsOffres appel = new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation = new SygRealisations();
	SygMembresCommissionsMarches membre = new SygMembresCommissionsMarches();
	SygDossiers dossier = new SygDossiers();
	private Menuitem menuValider;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite = null;
	SygDossierscommissionsmarches dossiermembre = new SygDossierscommissionsmarches();
	List<SygDossierscommissionsmarches> membres = new ArrayList<SygDossierscommissionsmarches>();
	List<SygDossierscommissionsmarches> membres_presents = new ArrayList<SygDossierscommissionsmarches>();
	private Div step0, step1, step2;

	private Iframe iframe;
	List<SygMembresCommissionsMarches> membrescommissions = new ArrayList<SygMembresCommissionsMarches>();
	List<SygPlisouvertures> registredepot = new ArrayList<SygPlisouvertures>();

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		lstListe2.setItemRenderer(this);
		pgPagination2.setPageSize(byPage);
		pgPagination2.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);
	}

	public void onCreate(CreateEvent createEvent) {
		infoscompte = (Utilisateur) getHttpSession()
				.getAttribute("infoscompte");
		if (infoscompte.getAutorite() != null)
			autorite = infoscompte.getAutorite();
		else
			autorite = null;
		idappel = (Long) session.getAttribute("idappel");
		appel = BeanLocator.defaultLookup(AppelsOffresSession.class).findById(
				idappel);
		realisation = appel.getRealisation();
		gestion = Integer.toString(realisation.getPlan().getAnnee());
		dossier = BeanLocator.defaultLookup(DossiersAppelsOffresSession.class)
				.Dossier(null, appel, -1);
		if (dossier == null) {
			menuValider.setDisabled(true);

		} else {
			registredepot = BeanLocator.defaultLookup(
					RegistrededepotSession.class).find(0, -1, dossier, null,
					null, null, -1, -1, -1, -1, -1, -1, null, -1, null, null);
			// if(registredepot.size()==0||dossier.getDateRemiseDossierTechnique()!=null)
			// menuValider.setDisabled(true);
			membres = BeanLocator.defaultLookup(
					DossierscommissionsmarchesSession.class).find(0, -1,
					dossier, -1, -1);
			// membrescommissions=BeanLocator.defaultLookup(MembresCommissionsMarchesSession.class).find(0,-1,null,null,autorite,gestion,-1,0);
			membrescommissions = BeanLocator.defaultLookup(
					MembresCommissionsMarchesSession.class).find(0, -1, null,
					null, autorite, null, -1, 0);
			if (membres.size() == 0) {
				for (int i = 0; i < membrescommissions.size(); i++) {
					SygDossierscommissionsmarches dossiermembre = new SygDossierscommissionsmarches();
					membre = membrescommissions.get(i);
					dossiermembre.setMembre(membre);
					dossiermembre.setDossier(dossier);
					dossiermembre.setEtapePI(0);
					BeanLocator.defaultLookup(
							DossierscommissionsmarchesSession.class).save(
							dossiermembre);
				}
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			Events.postEvent(ApplicationEvents.ON_MEMBRES, this, null);
		}

	}

	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			membres = BeanLocator.defaultLookup(
					DossierscommissionsmarchesSession.class).find(activePage,
					byPage, dossier, 0, -1);
			lstListe.setModel(new SimpleListModel(membres));
			pgPagination.setTotalSize(BeanLocator.defaultLookup(
					DossierscommissionsmarchesSession.class).count(dossier, 0,
					-1));

			membres_presents = BeanLocator.defaultLookup(
					DossierscommissionsmarchesSession.class).find(activePage,
					byPage, dossier, 1, -1);
			lstListe2.setModel(new SimpleListModel(membres_presents));
			pgPagination2.setTotalSize(BeanLocator.defaultLookup(
					DossierscommissionsmarchesSession.class).count(dossier, 1,
					-1));
		}

		else if (event.getName().equalsIgnoreCase(
				ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {
			for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = ((SygDossierscommissionsmarches) ((Listitem) lstListe
						.getSelectedItems().toArray()[i]).getValue()).getId();
				BeanLocator.defaultLookup(
						DossierscommissionsmarchesSession.class).delete(codes);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
	}

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygDossierscommissionsmarches membre = (SygDossierscommissionsmarches) data;
		item.setValue(membre);
		
		Listcell lstCell = new Listcell();
		
		Div divImage = new Div();
		divImage.setSclass("col-md-2");
		Image membreImage = new Image();
		membreImage.setSrc("/images/avatar.png");
		membreImage.setParent(divImage);
		divImage.setParent(lstCell);
		lstCell.setParent(item);
		
		Div divDetails = new Div();
		divDetails.setSclass("col-md-10 padding10");
		
		Div divDetails1 = new Div();
		divDetails1.setSclass("col-md-12 nopadding");
		Label lblPrenomNom = new Label();
		lblPrenomNom.setSclass("h5 text-strong");
		if(membre.getMembre().getPrenom() != null &&  membre.getMembre().getNom() != null ) {
			lblPrenomNom.setValue(membre.getMembre().getPrenom() + " "+ membre.getMembre().getNom()+ " ");
		}		
		lblPrenomNom.setParent(divDetails1);
		
		Label lblFonction = new Label();
		lblFonction.setSclass("");
		if(membre.getMembre().getFonction() != null ) {
			lblFonction.setValue(membre.getMembre().getFonction());
		}
		lblFonction.setParent(divDetails1);
		
		divDetails1.setParent(divDetails);
		
		divDetails.setParent(lstCell);
		
		
		Label lblMail = new Label();
		lblMail.setSclass("text-info col-md-10");
		if(membre.getMembre().getEmail() != null) {
			lblMail.setValue(membre.getMembre().getEmail());
		}
		lblMail.setParent(lstCell);
		
		lstCell.setParent(item);
		
		
		 
		
	}

	public void onClick$menuValider() {
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(lstListe,
					Labels.getLabel("kermel.error.select.item"));

		for (int k = 0; k < membres.size(); k++) {
			dossiermembre = membres.get(k);
			dossiermembre.setFlagpresenceevaluation(UIConstants.NPARENT);
			BeanLocator.defaultLookup(DossierscommissionsmarchesSession.class)
					.update(dossiermembre);
		}
		for (int i = 0; i < lstListe.getSelectedCount(); i++) {

			dossiermembre = (SygDossierscommissionsmarches) ((Listitem) lstListe
					.getSelectedItems().toArray()[i]).getValue();
			dossiermembre.setFlagpresenceevaluation(UIConstants.PARENT);
			BeanLocator.defaultLookup(DossierscommissionsmarchesSession.class)
					.update(dossiermembre);
		}
		session.setAttribute("libelle", "listepresencemembrescommissions");
		loadApplicationState("ouverture_plis");

	}

	public void onClick$menuCancel() {
		step0.setVisible(true);
		step1.setVisible(false);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	public void onClick$btAjouter() {
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(lstListe,
					Labels.getLabel("kermel.error.select.item"));

		for (int i = 0; i < lstListe.getSelectedCount(); i++) {

			dossiermembre = (SygDossierscommissionsmarches) ((Listitem) lstListe
					.getSelectedItems().toArray()[i]).getValue();
			dossiermembre.setFlagpresenceevaluation(UIConstants.PARENT);
			BeanLocator.defaultLookup(DossierscommissionsmarchesSession.class)
					.update(dossiermembre);
		}
		session.setAttribute("libelle", "listepresencemembrescommissions");
		loadApplicationState("ouverture_plis");

	}

	public void onClick$btEnlever() {
		if (lstListe2.getSelectedItem() == null)
			throw new WrongValueException(lstListe2,
					Labels.getLabel("kermel.error.select.item"));

		for (int i = 0; i < lstListe2.getSelectedCount(); i++) {

			dossiermembre = (SygDossierscommissionsmarches) ((Listitem) lstListe2
					.getSelectedItems().toArray()[i]).getValue();
			dossiermembre.setFlagpresenceevaluation(UIConstants.NPARENT);
			BeanLocator.defaultLookup(DossierscommissionsmarchesSession.class)
					.update(dossiermembre);
		}
		session.setAttribute("libelle", "listepresencemembrescommissions");
		loadApplicationState("ouverture_plis");

	}
}