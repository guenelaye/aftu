package sn.ssi.kermel.web.ouvertureplis.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.PklEnveloppesFichiers;
import sn.ssi.kermel.be.entity.PklPlis;
import sn.ssi.kermel.be.entity.PklPlisEnveloppes;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBanque;
import sn.ssi.kermel.be.entity.SygContenusEnveloppesOffres;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygPresentationsOffres;
import sn.ssi.kermel.be.passationsmarches.ejb.PresentationsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.SoumissionElectroniqueSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.security.PdfPKCS7;

@SuppressWarnings("serial")
public class OuvertureFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode, login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle, txtSigle;
	Long code;
	private SygBanque banque = new SygBanque();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private SygPlisouvertures plis = new SygPlisouvertures();
	private Label lblNumeroOrdre, lblRaisonSocial, lblEnveloppe,
			lblNumeroOrdre1, lblRaisonSocial1, lblNumeroOrdre2,
			lblRaisonSocial2;
	private Listbox lstListe, lstListesContenus;
	// private Paging pgPagination,pgPaginationDetails;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE, activePage;
	SygDossiers dossier = new SygDossiers();
	SygPresentationsOffres enveloppe = new SygPresentationsOffres();
	private Div step0, step1, step2, step3, step4, step5;
	private Iframe frame, framefichier;
	private Tree tree;
	private Treechildren child = new Treechildren();
	List<PklPlis> plisfournisseurs = new ArrayList<PklPlis>();
	SygFournisseur fournisseur = new SygFournisseur();
	private String nomfichier;
	private final String cheminDossier = UIConstants.PATH_PLIS;
	private Menuitem menuFermer, menuPrevStep, menuNextStep;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		lstListe.setItemRenderer(this);
		// pgPagination.setPageSize(byPage);
		// pgPagination.addForward("onPaging", this,
		// ApplicationEvents.ON_MODEL_CHANGE);

		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_NEXT, this);
		addEventListener(ApplicationEvents.ON_PREV, this);
		lstListesContenus.setItemRenderer(new DetailsRenderer());
		// pgPaginationDetails.setPageSize(byPage);
		// /pgPaginationDetails.addForward("onPaging", this,
		// ApplicationEvents.ON_DETAILS);

	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		plis = (SygPlisouvertures) map.get(PARAM_WINDOW_CODE);
		// getHttpSession().setAttribute("pli", plis);
		lblNumeroOrdre.setValue(Integer.toString(plis.getNumero()));
		lblRaisonSocial.setValue(plis.getFournisseur().getNom());
		lblNumeroOrdre1.setValue(Integer.toString(plis.getNumero()));
		lblRaisonSocial1.setValue(plis.getFournisseur().getNom());
		lblNumeroOrdre2.setValue(Integer.toString(plis.getNumero()));
		lblRaisonSocial2.setValue(plis.getFournisseur().getNom());
		dossier = plis.getDossier();
		fournisseur = plis.getFournisseur();
		if (plis.getOuvert() != null
				&& plis.getOuvert().equalsIgnoreCase("non")) {
			SygAutoriteContractante autorite = dossier.getAutorite();
			Long autoid = null;
			autoid = autorite.getId();
			// frame.setSrc("https://localhost:8443/Kermel-WEB/DecryptServlet?id="
			// + plis.getId() + "&aut=" + autoid);
			frame.setSrc("http://localhost:8080/Kermel-WEB/DecryptServlet?id="
					+ plis.getId() + "&aut=" + autoid);
			// frame.setSrc(BeConstants.urlKermel+"/indexd.jsp?id=" +
			// plis.getId()+"&aut="+autoid);

		} else if (plis.getOuvert() != null
				&& plis.getOuvert().equalsIgnoreCase("oui")) {
			step1.setVisible(false);
			step2.setVisible(true);
			showFichiers();
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			// if (event.getData() != null) {
			// activePage = Integer.parseInt((String) event.getData());
			// byPage = -1;
			// pgPagination.setPageSize(1000);
			// } else {
			// byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
			// activePage = pgPagination.getActivePage() * byPage;
			// pgPagination.setPageSize(byPage);
			// }
			List<SygPresentationsOffres> enveloppes = BeanLocator
					.defaultLookup(PresentationsOffresSession.class).find(0,
							-1, dossier, null, null);
			lstListe.setModel(new SimpleListModel(enveloppes));
			// pgPagination.setTotalSize(BeanLocator.defaultLookup(PresentationsOffresSession.class).count(dossier,null));
		} else if (event.getName().equalsIgnoreCase(
				ApplicationEvents.ON_DETAILS)) {
			// if (event.getData() != null) {
			// activePage = Integer.parseInt((String) event.getData());
			// byPage = -1;
			// pgPaginationDetails.setPageSize(1000);
			// } else {
			// byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
			// activePage = pgPaginationDetails.getActivePage() * byPage;
			// pgPaginationDetails.setPageSize(byPage);
			// }
			List<SygContenusEnveloppesOffres> details = BeanLocator
					.defaultLookup(PresentationsOffresSession.class).Contenus(
							0, -1, enveloppe);
			lstListesContenus.setModel(new SimpleListModel(details));
			// pgPaginationDetails.setTotalSize(BeanLocator.defaultLookup(PresentationsOffresSession.class).countContenus(enveloppe));
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_NEXT)) {
			if (step0.isVisible()) {
				toogle(step0);
				toogle(step1);
				menuPrevStep.setDisabled(false);
			} else if (step1.isVisible()) {
				toogle(step1);
				toogle(step2);
				showFichiers();
				menuPrevStep.setDisabled(false);
			} else if (step2.isVisible()) {
				toogle(step2);
				toogle(step3);
				menuPrevStep.setDisabled(false);
			} else if (step3.isVisible()) {
				toogle(step3);
				toogle(step4);
			} else if (step4.isVisible()) {
				toogle(step4);
				toogle(step5);
				menuNextStep.setDisabled(true);
			}

		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_PREV)) {

			// if (step0.isVisible()) {
			// toogle(step0);
			// toogle(step1);
			// menuPrevStep.setDisabled(false);
			// } else
			if (step2.isVisible()) {
				toogle(step2);
				toogle(step1);
				menuPrevStep.setDisabled(false);
			} else if (step3.isVisible()) {
				toogle(step3);
				toogle(step2);
				showFichiers();
				if (pliIsOpen())
					menuPrevStep.setDisabled(true);
			} else if (step4.isVisible()) {
				toogle(step4);
				toogle(step3);
			} else if (step5.isVisible()) {
				toogle(step5);
				toogle(step4);
				menuNextStep.setDisabled(false);
			}

		}
	}

	public void onClick$menuFermer() {

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();

	}

	@Override
	public void render(final Listitem item, final Object data, int index)
			throws Exception {

		SygPresentationsOffres enveloppes = (SygPresentationsOffres) data;
		item.setValue(enveloppes);

		Listcell cellIntitule = new Listcell(enveloppes.getIntituleenveloppe());
		cellIntitule.setParent(item);

		Listcell cellEtat = new Listcell("");
		if (enveloppes.getChiffre() == 1)
			cellEtat.setLabel(Labels.getLabel("kermel.common.list.oui"));
		else
			cellEtat.setLabel(Labels.getLabel("kermel.common.list.non"));
		cellEtat.setParent(item);
	}

	public void UpdatePlis() {
		/*
		 * plis.setOuvert("oui"); plis =
		 * BeanLocator.defaultLookup(RegistrededepotSession.class).update(
		 * plis);
		 */
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
	}

	public void onSelect$lstListe() {
		enveloppe = (SygPresentationsOffres) lstListe.getSelectedItem()
				.getValue();
		lblEnveloppe.setValue(enveloppe.getIntituleenveloppe());
		Events.postEvent(ApplicationEvents.ON_DETAILS, this, null);
	}

	public class DetailsRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)
				throws Exception {
			SygContenusEnveloppesOffres contenus = (SygContenusEnveloppesOffres) data;
			item.setValue(contenus);

			Listcell cellLibelle = new Listcell(contenus.getLibelle());
			cellLibelle.setParent(item);

			Listcell cellEtat = new Listcell("");
			if (contenus.getSignature() == 1)
				cellEtat.setLabel(Labels.getLabel("kermel.common.list.oui"));
			else
				cellEtat.setLabel(Labels.getLabel("kermel.common.list.non"));
			cellEtat.setParent(item);
		}
	}

	public void onClick$menuNextStep() {
		Events.postEvent(ApplicationEvents.ON_NEXT, this, null);
		/*
		 * step0.setVisible(false);
		 * 
		 * if (plis.getOuvert() != null &&
		 * plis.getOuvert().equalsIgnoreCase("non")) { step1.setVisible(true);
		 * step2.setVisible(false); } else { removeTreeChildren();
		 * plisfournisseurs = BeanLocator.defaultLookup(
		 * SoumissionElectroniqueSession.class).Plis(0, -1, dossier,
		 * fournisseur); initTree(); step1.setVisible(false);
		 * step2.setVisible(true); }
		 */

	}

	public void onClick$menuPrevStep() {
		Events.postEvent(ApplicationEvents.ON_PREV, this, null);
		/*
		 * step0.setVisible(true); step1.setVisible(false);
		 * step2.setVisible(false);
		 */
	}

	public void hideAllStep() {

	}

	public void toogle(Div div) {
		if (div.isVisible())
			div.setVisible(false);
		else
			div.setVisible(true);
	}

	public boolean pliIsOpen() {
		if (plis.getOuvert() != null
				&& plis.getOuvert().equalsIgnoreCase("oui"))
			return true;
		else
			return false;

	}

	public void onClick$menuNextStep1() {
		UpdatePlis();
		step0.setVisible(false);
		step1.setVisible(false);
		step2.setVisible(true);
		// fournisseur=null;

	}

	public void onClick$menuPrevious2() {
		if (plis.getOuvert() != null
				&& plis.getOuvert().equalsIgnoreCase("non")) {
			step0.setVisible(false);
			step1.setVisible(true);
		} else {
			step0.setVisible(true);
			step1.setVisible(false);
		}

		step2.setVisible(false);
	}

	public void onClick$menuFermer1() {
		UpdatePlis();
		detach();
		loadApplicationState("ouverture_plis");
	}

	public void onClick$menuFermer2() {
		onClick$menuFermer1();
		loadApplicationState("ouverture_plis");
	}

	private void initTree() {
		child.setParent(tree);
		treeBuilder();

	}

	public void onSelect$tree() {
		tree.isInvalidated();
		Object selobj = tree.getSelectedItem().getValue();
		tree.getSelectedItem().setOpen(true);

		if (selobj instanceof PklEnveloppesFichiers) {

			nomfichier = ((PklEnveloppesFichiers) tree.getSelectedItem()
					.getValue()).getChemin();

			String filepath = nomfichier;
			File f = new File(ToolKermel.formatPath(filepath));

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(f, null, null);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (mymedia != null)
				framefichier.setContent(mymedia);
			else
				framefichier.setSrc("");

			framefichier.setHeight("600px");
			framefichier.setWidth("100%");

		}
	}

	public void treeBuilder() {

		String key = null;
		try {
			System.out.println("==============================ok");

			for (PklPlis plisfournisseur : plisfournisseurs) {

				Treeitem plisItem = new Treeitem();

				plisItem.setLabel(plisfournisseur.getLibelle());
				plisItem.setTooltiptext(plisfournisseur.getLibelle());

				// procedureItem.setImage("g");
				plisItem.setValue(plisfournisseur);
				plisItem.setOpen(true);

				plisItem.setParent(child);

				// Niveau 2 :enveloppes
				List<PklPlisEnveloppes> enveloppes = BeanLocator.defaultLookup(
						SoumissionElectroniqueSession.class).Enveloppes(0, -1,
						plisfournisseur);

				if (enveloppes.size() > 0) {
					Treechildren enveloppesTreechildren = new Treechildren();
					for (PklPlisEnveloppes enveloppe : enveloppes) {
						Treeitem enveloppeItem = new Treeitem();

						enveloppeItem.setLabel(enveloppe.getLibelle());
						enveloppeItem.setTooltiptext(enveloppe.getLibelle());
						enveloppeItem.setSelected(false);
						enveloppeItem.setValue(enveloppe);
						enveloppeItem.setOpen(true);
						enveloppeItem.setParent(enveloppesTreechildren);

						// Niveau 2 :Fichiers
						List<PklEnveloppesFichiers> fichiers = BeanLocator
								.defaultLookup(
										SoumissionElectroniqueSession.class)
								.Fichiers(0, -1, enveloppe);
						if (fichiers.size() > 0) {
							Treechildren fichierTreechildren = new Treechildren();
							for (PklEnveloppesFichiers fichier : fichiers) {

								Treeitem fichierItem = new Treeitem();
								// Image img = new Image("/images/cancel.png");
								// fichierItem.setSrc("/images/cancel.png");
								fichierItem
										.setImage("/images/icone_pdf_no.png");
								fichierItem.setLabel(fichier.getLibelle()
										+ "(ouvrir)");
								fichierItem.setTooltiptext(fichier.getLibelle()
										+ " (non signé)");
								if (verifySignatures(fichier.getChemin())) {
									// img.setSrc("/images/ok.png");
									fichierItem.setLabel(fichier.getLibelle()
											+ "(ouvrir)");
									fichierItem
											.setImage("/images/icone_pdf_yes.png");
									fichierItem.setTooltiptext(fichier
											.getLibelle() + " (signé)");
									// fichierItem.setSrc("/images/ok.png");
								}
								// img.setParent(fichierItem);
								// fichierItem.setTooltiptext(fichier.getLibelle()+fichier.getChemin());
								// fichierItem.setImage("/images/icone_pdf.png");
								fichierItem.setValue(fichier);
								fichierItem.setParent(fichierTreechildren);
							}

							fichierTreechildren.setParent(enveloppeItem);
						}

						//
					}
					enveloppesTreechildren.setParent(plisItem);
				}// Fin niveau 2 direction
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void removeTreeChildren() {
		while (child.getItemCount() > 0) {
			child.removeChild(child.getFirstChild());
		}
	}

	public void showFichiers() {
		removeTreeChildren();
		plisfournisseurs = BeanLocator.defaultLookup(
				SoumissionElectroniqueSession.class).Plis(0, -1, dossier,
				fournisseur);
		initTree();
	}

	public static boolean verifySignatures(String path) throws IOException,
			GeneralSecurityException {
		boolean signed = false;
		System.out.println(path);
		PdfReader reader = new PdfReader(path);
		AcroFields fields = reader.getAcroFields();
		ArrayList<String> names = fields.getSignatureNames();
		for (String name : names) {
			System.out.println("===== " + name + " =====");
			verifySignature(fields, name);
			signed = true;
			break;
		}
		return signed;
	}

	public static PdfPKCS7 verifySignature(AcroFields fields, String name)
			throws GeneralSecurityException, IOException {
		System.out.println("Signature covers whole document: "
				+ fields.signatureCoversWholeDocument(name));
		System.out.println("Document revision: " + fields.getRevision(name)
				+ " of " + fields.getTotalRevisions());
		PdfPKCS7 pkcs7 = fields.verifySignature(name);
		System.out.println("Integrity check OK? " + pkcs7.verify());
		return pkcs7;
	}

}
