package sn.ssi.kermel.web.ouvertureplis.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossiersEvaluateurs;
import sn.ssi.kermel.be.entity.SygEvaluateur;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersEvaluateursSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.be.referentiel.ejb.EvaluateurSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class CompositioncommissiontechniqueController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstMembres;
	private Paging pgPagination,pgMembres;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
  	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	SygEvaluateur evaluateur=new SygEvaluateur();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuAjouter,menuSupprimer;
	private Div step0,step1;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	  List<SygPlisouvertures> registredepot = new ArrayList<SygPlisouvertures>();
	  
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
    	
		addEventListener(ApplicationEvents.ON_MEMBRES, this);
		lstMembres.setItemRenderer(new MembresRenderer());
		pgMembres.setPageSize(byPage);
		pgMembres.addForward("onPaging", this, ApplicationEvents.ON_MEMBRES);
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier==null)
		{
			menuAjouter.setDisabled(true);
			menuSupprimer.setDisabled(true);
		}
		else
		{
			registredepot= BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,-1,-1,-1, -1, -1,-1, null,-1, null, null);
			if(registredepot.size()==0||dossier.getDateRemiseDossierTechnique()!=null)
			{
				menuAjouter.setDisabled(true);
				menuSupprimer.setDisabled(true);
			}
				
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			Events.postEvent(ApplicationEvents.ON_MEMBRES, this, null);
		}
		
	}
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygDossiersEvaluateurs> evaluateurs = BeanLocator.defaultLookup(DossiersEvaluateursSession.class).find(activePage,byPage,dossier);
			 lstListe.setModel(new SimpleListModel(evaluateurs));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(DossiersEvaluateursSession.class).count(dossier));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MEMBRES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgMembres.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgMembres.getActivePage() * byPage;
				pgMembres.setPageSize(byPage);
			}
			 List<SygEvaluateur> evaluateurs = BeanLocator.defaultLookup(EvaluateurSession.class).ListeEvaluateurs(activePage,byPage,libelle,dossier.getDosID(),autorite.getId());
			 lstMembres.setModel(new SimpleListModel(evaluateurs));
			 pgMembres.setTotalSize(BeanLocator.defaultLookup(EvaluateurSession.class).ListeEvaluateurs(libelle,dossier.getDosID(),autorite.getId()).size());
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = ((SygDossiersEvaluateurs)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue()).getId();
				BeanLocator.defaultLookup(DossiersEvaluateursSession.class).delete(codes);
			}
			session.setAttribute("libelle", "compositioncommissiontechnique");
			loadApplicationState("ouverture_plis");
		}
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygDossiersEvaluateurs evaluateurs = (SygDossiersEvaluateurs) data;
		item.setValue(evaluateurs);

		 Listcell cellPrenom = new Listcell(evaluateurs.getEvaluateur().getPrenom());
		 cellPrenom.setParent(item);
		 
		 Listcell cellNom = new Listcell(evaluateurs.getEvaluateur().getNom());
		 cellNom.setParent(item);
		 
		 Listcell cellFonction= new Listcell(evaluateurs.getEvaluateur().getFonction());
		 cellFonction.setParent(item);
		 
		 Listcell cellTelelephone = new Listcell(evaluateurs.getEvaluateur().getTelephone());
		 cellTelelephone.setParent(item);
		 
		 Listcell cellEMail = new Listcell(evaluateurs.getEvaluateur().getEmail());
		 cellEMail.setParent(item);
		 
		
	}

	
	public class MembresRenderer implements ListitemRenderer{
		
		
		
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygEvaluateur evaluateurs = (SygEvaluateur) data;
		item.setValue(evaluateurs);

		 Listcell cellPrenom = new Listcell(evaluateurs.getPrenom());
		 cellPrenom.setParent(item);
		 
		 Listcell cellNom = new Listcell(evaluateurs.getNom());
		 cellNom.setParent(item);
		 
		 Listcell cellFonction= new Listcell(evaluateurs.getFonction());
		 cellFonction.setParent(item);
		 
		 Listcell cellTelelephone = new Listcell(evaluateurs.getTelephone());
		 cellTelelephone.setParent(item);
		 
		 Listcell cellEMail = new Listcell(evaluateurs.getEmail());
		 cellEMail.setParent(item);
	
	}
	}
	public void onClick$menuSupprimer()
	{
		if (lstListe.getSelectedItem() == null)
			
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		

		HashMap<String, String> display = new HashMap<String, String>(); // permet
		display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
		display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
		display.put(MessageBoxController.DSP_HEIGHT, "250px");
		display.put(MessageBoxController.DSP_WIDTH, "47%");

		HashMap<String, Object> map = new HashMap<String, Object>(); // permet
		map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
		showMessageBox(display, map);
		
	}
	public void onClick$menuAjouter()
	{
		step0.setVisible(false);
		step1.setVisible(true);
		Events.postEvent(ApplicationEvents.ON_MEMBRES, this, null);
	}
	public void onClick$menuFermer()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		
	}
	public void onClick$menuValider()
	{
       if (lstMembres.getSelectedItem() == null)
			throw new WrongValueException(lstMembres, Labels.getLabel("kermel.error.select.item"));
       
       for (int i = 0; i < lstMembres.getSelectedCount(); i++) {
    	   SygDossiersEvaluateurs dossierevaluateur=new SygDossiersEvaluateurs();
    	   evaluateur=(SygEvaluateur) ((Listitem) lstMembres.getSelectedItems().toArray()[i]).getValue();
    	   dossierevaluateur.setEvaluateur(evaluateur);
    	   dossierevaluateur.setDossier(dossier);
    	   BeanLocator.defaultLookup(DossiersEvaluateursSession.class).save(dossierevaluateur);
       }
       session.setAttribute("libelle", "compositioncommissiontechnique");
		loadApplicationState("ouverture_plis");
		
	}
}