package sn.ssi.kermel.web.ouvertureplis.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Li;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.A;
import org.zkoss.zul.Div;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAttributions;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygCriteresQualificationsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygDevise;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossiersEvaluateurs;
import sn.ssi.kermel.be.entity.SygDossierscommissionsmarches;
import sn.ssi.kermel.be.entity.SygDossierspieces;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygGarantiesDossiers;
import sn.ssi.kermel.be.entity.SygGarantiesSoumissionnaires;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygLotsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygMontantsSeuils;
import sn.ssi.kermel.be.entity.SygObservateursIndependants;
import sn.ssi.kermel.be.entity.SygPiecesplisouvertures;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygPresenceouverture;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygRepresentantsServicesTechniques;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DocumentsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersEvaluateursSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossierscommissionsmarchesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.LotsSoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.PiecessoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.Presence0uvertureSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

/**
 * 
 * @author Adama samb
 * @since mardi 11 Janvier 2011, 12:34
 */
public class SuiviOuvertureFormController extends AbstractWindow implements
		AfterCompose, EventListener<Event> {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private Include pgActes;
	SygAppelsOffres appel = new SygAppelsOffres();
	private Long idappel;
	Session session = getHttpSession();
	private Label lblModeSoumission, lblObjets, lblModePassation,
			lblModeSelection, lblType, lgtitre, lblDatecreation, lblDateDepot,
			lblDateOuverture;
	private String LibelleTab, libelle, avalider = "oui";
	private Tab TAB_TDOSSIERS;
	private Tree tree;
	private Treecell cellcommissionspassations, cellrepresentantsfournisseur,
			cellpiecesadministratives, cellprocesverbal,
			cellcommissionevaluations, cellgaranties, celllecturespffres,
			cellouverturesdesoffres;
	private Include idinclude;
	private Treeitem treecommissionspassations, treerepresentantsfournisseur,
			treepiecesadministratives, treeprocesverbal,
			treecommissionevaluations, treegaranties;
	List<SygGarantiesDossiers> garanties = new ArrayList<SygGarantiesDossiers>();
	List<SygDossierspieces> piecesadministratives = new ArrayList<SygDossierspieces>();
	List<SygDossierssouscriteres> criteresqualifications = new ArrayList<SygDossierssouscriteres>();
	List<SygDevise> devises = new ArrayList<SygDevise>();
	List<SygRealisationsBailleurs> bailleurs = new ArrayList<SygRealisationsBailleurs>();
	List<SygRetraitregistredao> registreretrait = new ArrayList<SygRetraitregistredao>();
	List<SygPlisouvertures> registredepot = new ArrayList<SygPlisouvertures>();
	List<SygDossierscommissionsmarches> membrescommissions = new ArrayList<SygDossierscommissionsmarches>();
	List<SygPresenceouverture> representantsoummissionnaires = new ArrayList<SygPresenceouverture>();
	List<SygRepresentantsServicesTechniques> representantservicestechniques = new ArrayList<SygRepresentantsServicesTechniques>();
	List<SygObservateursIndependants> observateursindependants = new ArrayList<SygObservateursIndependants>();
	List<SygPiecesplisouvertures> piecessoumissionnaires = new ArrayList<SygPiecesplisouvertures>();
	List<SygDossiersEvaluateurs> compositioncommissiontechnique = new ArrayList<SygDossiersEvaluateurs>();
	List<SygLotsSoumissionnaires> lecturesoffres = new ArrayList<SygLotsSoumissionnaires>();
	List<SygMontantsSeuils> seuils = new ArrayList<SygMontantsSeuils>();
	SygAutoriteContractante autorite = new SygAutoriteContractante();
	SygPlisouvertures soumissionnaires = null;
	List<SygDocuments> documents = new ArrayList<SygDocuments>();
	SygDossiers dossier = new SygDossiers();
	private int nombre = 0, garantiesoumission = 0;
	List<SygLots> lots = new ArrayList<SygLots>();
	List<SygAttributions> attributaires = new ArrayList<SygAttributions>();
	List<SygLotsSoumissionnaires> lotssoumissionnaires = new ArrayList<SygLotsSoumissionnaires>();
	List<SygGarantiesSoumissionnaires> garantiessoumissions = new ArrayList<SygGarantiesSoumissionnaires>();
	List<SygCriteresQualificationsSoumissionnaires> criteresqualificationssoum = new ArrayList<SygCriteresQualificationsSoumissionnaires>();
	private BigDecimal montantbailleurs = new BigDecimal(0);
	List<SygMontantsSeuils> seuilscommunautaires = new ArrayList<SygMontantsSeuils>();
	SygContrats contrat = new SygContrats();
	List<SygPlisouvertures> registredepotverifies = new ArrayList<SygPlisouvertures>();
	List<SygPlisouvertures> examenspreleminaires = new ArrayList<SygPlisouvertures>();
	List<SygPlisouvertures> plis = new ArrayList<SygPlisouvertures>();
	List<SygPlisouvertures> garanties_plies = new ArrayList<SygPlisouvertures>();
	private Div divListPresence, divRepresentants, divOuverture, divLecture,
			divPieces, divProces;
	private A linkListPresence, linkRepresentants, linkOuverture, linkLecture,
			linkPieces, linkProces;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		addEventListener(ApplicationEvents.ON_CLICK_LISTPRESENCE, this);
		addEventListener(ApplicationEvents.ON_CLICK_LISTREPRESENTANTS, this);
		addEventListener(ApplicationEvents.ON_CLICK_OUVERTURE, this);
		addEventListener(ApplicationEvents.ON_CLICK_LECTEURE, this);
		addEventListener(ApplicationEvents.ON_CLICK_PIECES, this);
		addEventListener(ApplicationEvents.ON_CLICK_PV, this);

		linkListPresence.addForward(Events.ON_CLICK, this,
				ApplicationEvents.ON_CLICK_LISTPRESENCE);
		linkRepresentants.addForward(Events.ON_CLICK, this,
				ApplicationEvents.ON_CLICK_LISTREPRESENTANTS);
		linkOuverture.addForward(Events.ON_CLICK, this,
				ApplicationEvents.ON_CLICK_OUVERTURE);
		linkLecture.addForward(Events.ON_CLICK, this,
				ApplicationEvents.ON_CLICK_LECTEURE);
		linkPieces.addForward(Events.ON_CLICK, this,
				ApplicationEvents.ON_CLICK_PIECES);
		linkProces.addForward(Events.ON_CLICK, this,
				ApplicationEvents.ON_CLICK_PV);

	}

	public void onCreate(CreateEvent createEvent) {
		idappel = (Long) session.getAttribute("idappel");
		LibelleTab = (String) session.getAttribute("libelle");
		appel = BeanLocator.defaultLookup(AppelsOffresSession.class).findById(
				idappel);
		autorite = appel.getAutorite();

		lblObjets.setValue(appel.getApoobjet());
		lblModePassation.setValue(appel.getModepassation().getLibelle());
		lblModeSelection.setValue(appel.getModeselection().getLibelle());
		lblType.setValue(appel.getTypemarche().getLibelle());

		if ("DRP".equalsIgnoreCase(appel.getModepassation().getCode()))
			treegaranties.setVisible(false);

		if (appel.getModeSoumission() != null) {
			if (appel.getModeSoumission() == 1)
				lblModeSoumission.setValue("Physique");
			if (appel.getModeSoumission() == 2)
				lblModeSoumission.setValue("Electronique");
			if (appel.getModeSoumission() == 3)
				lblModeSoumission.setValue("Physique & Electronique");

		} else
			lblModeSoumission.setValue("Physique & Electronique");

		lblDatecreation.setValue(UtilVue.getInstance().formateLaDate2(
				appel.getApodatecreation()));

		dossier = BeanLocator.defaultLookup(DossiersAppelsOffresSession.class)
				.Dossier(null, appel, -1);

		if (dossier != null) {
			if (dossier.getDosDateLimiteDepot() != null
					&& dossier.getDosHeurelimitedepot() != null)
				lblDateDepot.setValue(UtilVue.getInstance().formateLaDate2(
						dossier.getDosDateLimiteDepot())
						+ " à "
						+ UtilVue.getInstance().formateLHeure(
								dossier.getDosHeurelimitedepot()));
			else if (dossier.getDosDateLimiteDepot() != null)
				lblDateDepot.setValue(UtilVue.getInstance().formateLaDate2(
						dossier.getDosDateLimiteDepot()));

			if (dossier.getDosDateOuvertueDesplis() != null
					&& dossier.getDosHeureOuvertureDesPlis() != null)
				lblDateOuverture.setValue(UtilVue.getInstance().formateLaDate2(
						dossier.getDosDateOuvertueDesplis())
						+ " à "
						+ UtilVue.getInstance().formateLHeure(
								dossier.getDosHeureOuvertureDesPlis()));
			else if (dossier.getDosDateOuvertueDesplis() != null)
				lblDateOuverture.setValue(UtilVue.getInstance().formateLaDate2(
						dossier.getDosDateOuvertueDesplis()));

			membrescommissions = BeanLocator.defaultLookup(
					DossierscommissionsmarchesSession.class).find(0, -1,
					dossier, UIConstants.PARENT, -1);
			representantsoummissionnaires = BeanLocator.defaultLookup(
					Presence0uvertureSession.class).find(0, -1, dossier, null,
					null, -1);
			registredepot = BeanLocator.defaultLookup(
					RegistrededepotSession.class).find(0, -1, dossier, null,
					null, null, -1, -1, -1, -1, -1, -1, null, -1, null, "oui");
			// garanties_plies=BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,-1,-1,-1,
			// -1, -1, -1, null, -1, null, null);

			piecessoumissionnaires = BeanLocator.defaultLookup(
					PiecessoumissionnairesSession.class).find(0, -1, dossier,
					null, null, UIConstants.PIECEFOURNIE);
			compositioncommissiontechnique = BeanLocator.defaultLookup(
					DossiersEvaluateursSession.class).find(0, -1, dossier);
			examenspreleminaires = BeanLocator.defaultLookup(
					RegistrededepotSession.class).find(0, -1, dossier, null,
					null, null, -1, -1, -1, -1, -1, -1, null, -1, null, null);
			lecturesoffres = BeanLocator.defaultLookup(
					LotsSoumissionnairesSession.class).find(0, -1, dossier,
					null, null, -1, "oui", -1, -1, -1, null);

			boolean tousOuverts = true;

			plis = BeanLocator.defaultLookup(RegistrededepotSession.class)
					.find(0, -1, dossier, null, null, null, -1, -1, -1, -1, -1,
							-1, null, -1, null, null);

			if (!plis.isEmpty()) {
				for (int i = 0; i < plis.size(); i++) {

					if ("non".equalsIgnoreCase(plis.get(i).getOuvert()))
						tousOuverts = false;

				}
			} else {
				tousOuverts = false;
			}

			if (tousOuverts) {

				cellouverturesdesoffres
						.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellouverturesdesoffres.setImage("/images/rdo_on.png");
				divOuverture.setSclass("li done");
			}

			for (int i = 0; i < examenspreleminaires.size(); i++) {

				if (examenspreleminaires.get(i).getGarantiesoumission() != null
						&& examenspreleminaires.get(i).getGarantiesoumission()
								.equals("oui"))
					garantiesoumission = garantiesoumission + 1;

			}
			if (membrescommissions.size() > 0) {
				nombre = nombre + 1;
				cellcommissionspassations
						.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellcommissionspassations.setImage("/images/rdo_on.png");
				divListPresence
						.setSclass(divListPresence.getSclass() + " done");
			}
			if (representantsoummissionnaires.size() > 0) {
				nombre = nombre + 1;
				cellrepresentantsfournisseur
						.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellrepresentantsfournisseur.setImage("/images/rdo_on.png");
				divRepresentants.setSclass("li done");
			}
			if (piecessoumissionnaires.size() > 0) {
				nombre = nombre + 1;
				cellpiecesadministratives
						.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellpiecesadministratives.setImage("/images/rdo_on.png");
				divPieces.setSclass("li done");
			}

			documents = BeanLocator.defaultLookup(DocumentsSession.class).find(
					0, -1, dossier, appel,
					UIConstants.PARAM_TYPEDOCUMENTS_PVOUVERTURE, null, null);

			if (documents.size() > 0) {
				// if (appel.getApoDatepvouverturepli() != null) {

				cellprocesverbal
						.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellprocesverbal.setImage("/images/rdo_on.png");
				divProces.setSclass("li done");
			}
			if (compositioncommissiontechnique.size() > 0) {
				nombre = nombre + 1;
				cellcommissionevaluations
						.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellcommissionevaluations.setImage("/images/rdo_on.png");
			}
			if (garantiesoumission > 0) {
				nombre = nombre + 1;
				cellgaranties
						.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellgaranties.setImage("/images/rdo_on.png");
			}

			boolean isOffreSaisie = false;
			if (lecturesoffres.size() > 0) {
				for (SygLotsSoumissionnaires offre : lecturesoffres) {
					if (offre.getPlilmontantoffert().compareTo(new BigDecimal(0)) > 0)
						isOffreSaisie = true;

				}
				nombre = nombre + 1;

			} 
			if (isOffreSaisie) {
				celllecturespffres
						.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				celllecturespffres.setImage("/images/rdo_on.png");
				divLecture.setSclass("li done");
			}

		}

		lgtitre.setValue(Labels
				.getLabel("kermel.plansdepassation.infos.generales.referencedossier")
				+ ": " + appel.getAporeference());
		
		idinclude
		.setSrc("/ouvertureplis/listepresencemembrescommissions.zul");
//		if (LibelleTab != null) {
//			if (LibelleTab.equals("listepresencemembrescommissions")
//					|| LibelleTab.equals("traitementsdossiers")) {
//				idinclude
//						.setSrc("/ouvertureplis/listepresencemembrescommissions.zul");
//				tree.setSelectedItem(treecommissionspassations);
//			}
//			if (LibelleTab.equals("representantssoumissionnaires")) {
//				idinclude
//						.setSrc("/ouvertureplis/representantssoumissionnaires.zul");
//				tree.setSelectedItem(treerepresentantsfournisseur);
//			}
//			if (LibelleTab.equals("piecessoumissionnaires")) {
//				idinclude.setSrc("/ouvertureplis/piecessoumissionnaires.zul");
//				tree.setSelectedItem(treepiecesadministratives);
//			}
//			if (LibelleTab.equals("procesverbalouverture")) {
//				idinclude.setSrc("/ouvertureplis/procesverbalouverture.zul");
//				tree.setSelectedItem(treeprocesverbal);
//			}
//			// if(LibelleTab.equals("compositioncommissiontechnique"))
//			// {
//			// idinclude.setSrc("/ouvertureplis/compositioncommissiontechnique.zul");
//			// tree.setSelectedItem(treecommissionevaluations);
//			//
//			// }
//			if (LibelleTab.equals("garantiesoummission")) {
//				idinclude.setSrc("/ouvertureplis/formgarantiesoummission.zul");
//				tree.setSelectedItem(treegaranties);
//
//			}
//			// if(LibelleTab.equals("lotssoumissionnaires")||LibelleTab.equals("lecturesoffres"))
//			// {
//			// idinclude.setSrc("/ouvertureplis/lotssoumissionnaires.zul");
//			// tree.setSelectedItem(treelecturespffres);
//			//
//			// }
//
//		}

		Clients.evalJavaScript("$('div.ul div.li').click(function(){"
				+ "$('div.ul div.li').removeClass('current'); "
				+ "$(this).addClass('current'); " + "});");
		
		if(getHttpSession().getAttribute("select")!=null){
			Events.postEvent(ApplicationEvents.ON_CLICK_PV,this,null);
		}
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (ApplicationEvents.ON_CLICK_LISTPRESENCE.equalsIgnoreCase(event
				.getName())) {
			idinclude
					.setSrc("/ouvertureplis/listepresencemembrescommissions.zul");

		} else if (ApplicationEvents.ON_CLICK_LISTREPRESENTANTS
				.equalsIgnoreCase(event.getName())) {

			if (membrescommissions.size() == 0)
				Messagebox.show(
						Labels.getLabel("erreur.saisir.listepresenceac"),
						"Erreur", Messagebox.OK, Messagebox.ERROR);
			else
				idinclude
						.setSrc("/ouvertureplis/representantssoumissionnaires.zul");

		} else if (ApplicationEvents.ON_CLICK_OUVERTURE.equalsIgnoreCase(event
				.getName())) {
			if (membrescommissions.size() == 0)
				Messagebox.show(
						Labels.getLabel("erreur.saisir.listepresenceac"),
						"Erreur", Messagebox.OK, Messagebox.ERROR);
			else if (representantsoummissionnaires.size() == 0)
				Messagebox
						.show("Veuillez saissir les représentants des soumissionnaires",
								"Erreur", Messagebox.OK, Messagebox.ERROR);
			else
				idinclude.setSrc("/ouvertureplis/ouverturesdesoffres.zul");

		} else if (ApplicationEvents.ON_CLICK_LECTEURE.equalsIgnoreCase(event
				.getName())) {
			if (membrescommissions.size() == 0)
				Messagebox.show(
						Labels.getLabel("erreur.saisir.listepresenceac"),
						"Erreur", Messagebox.OK, Messagebox.ERROR);
			else if (representantsoummissionnaires.size() == 0)
				Messagebox
						.show(Labels
								.getLabel("erreur.saisir.representantsoummissionnaires"),
								"Erreur", Messagebox.OK, Messagebox.ERROR);
			else if (registredepot.size() == 0)
				Messagebox
						.show(Labels
								.getLabel("erreur.saisir.representantsoummissionnaires"),
								"Erreur", Messagebox.OK, Messagebox.ERROR);
			else
				idinclude.setSrc("/ouvertureplis/lecturesdesoffres.zul");

		} else if (ApplicationEvents.ON_CLICK_PIECES.equalsIgnoreCase(event
				.getName())) {
			idinclude.setSrc("/ouvertureplis/piecessoumissionnaires.zul");

		} else if (ApplicationEvents.ON_CLICK_PV.equalsIgnoreCase(event
				.getName())) {
			idinclude.setSrc("/ouvertureplis/procesverbalouverture.zul");

		}

	}

	public void onSelect$tree() throws Exception {
		getHttpSession().removeAttribute("select");
		InfosTree();
	}

	public void InfosTree() throws Exception {
		if (tree.getSelectedItem().getId().equals("treecommissionspassations")) {

			idinclude
					.setSrc("/ouvertureplis/listepresencemembrescommissions.zul");
		}
		if (tree.getSelectedItem().getId()
				.equals("treerepresentantsfournisseur")) {
			if (membrescommissions.size() == 0)
				Messagebox.show(
						Labels.getLabel("erreur.saisir.listepresenceac"),
						"Erreur", Messagebox.OK, Messagebox.ERROR);
			else
				idinclude
						.setSrc("/ouvertureplis/representantssoumissionnaires.zul");
		}
		if (tree.getSelectedItem().getId().equals("treepiecesadministratives")) {

			idinclude.setSrc("/ouvertureplis/piecessoumissionnaires.zul");
		}
		if (tree.getSelectedItem().getId().equals("treeprocesverbal")) {
			getHttpSession().setAttribute("select", "treeprocesverbal");
			idinclude.setSrc("/ouvertureplis/procesverbalouverture.zul");
		}
		// if(tree.getSelectedItem().getId().equals("treecommissionevaluations"))
		// {
		//
		// idinclude.setSrc("/ouvertureplis/compositioncommissiontechnique.zul");
		// }
		//

		// if(tree.getSelectedItem().getId().equals("treelecturespffres"))
		// {
		//
		// idinclude.setSrc("/ouvertureplis/lotssoumissionnaires.zul");
		//
		//
		// }
		if (tree.getSelectedItem().getId().equals("treeouverturesdesoffres")) {
			if (membrescommissions.size() == 0)
				Messagebox.show(
						Labels.getLabel("erreur.saisir.listepresenceac"),
						"Erreur", Messagebox.OK, Messagebox.ERROR);
			else if (representantsoummissionnaires.size() == 0)
				Messagebox
						.show(Labels
								.getLabel("erreur.saisir.representantsoummissionnaires"),
								"Erreur", Messagebox.OK, Messagebox.ERROR);
			else
				idinclude.setSrc("/ouvertureplis/ouverturesdesoffres.zul");

		}
		if (tree.getSelectedItem().getId().equals("treelecturesdesoffres")) {
			if (membrescommissions.size() == 0)
				Messagebox.show(
						Labels.getLabel("erreur.saisir.listepresenceac"),
						"Erreur", Messagebox.OK, Messagebox.ERROR);
			else if (representantsoummissionnaires.size() == 0)
				Messagebox
						.show(Labels
								.getLabel("erreur.saisir.representantsoummissionnaires"),
								"Erreur", Messagebox.OK, Messagebox.ERROR);
			else if (registredepot.size() == 0)
				Messagebox
						.show(Labels
								.getLabel("erreur.saisir.representantsoummissionnaires"),
								"Erreur", Messagebox.OK, Messagebox.ERROR);
			else
				idinclude.setSrc("/ouvertureplis/lecturesdesoffres.zul");

		}

		if (tree.getSelectedItem().getId().equals("treegaranties")) {

			if (membrescommissions.size() == 0)
				Messagebox.show(
						Labels.getLabel("erreur.saisir.listepresenceac"),
						"Erreur", Messagebox.OK, Messagebox.ERROR);
			else if (representantsoummissionnaires.size() == 0)
				Messagebox
						.show(Labels
								.getLabel("erreur.saisir.representantsoummissionnaires"),
								"Erreur", Messagebox.OK, Messagebox.ERROR);
			else if (registredepot.size() == 0)
				Messagebox
						.show(Labels
								.getLabel("erreur.saisir.representantsoummissionnaires"),
								"Erreur", Messagebox.OK, Messagebox.ERROR);
			else
				idinclude.setSrc("/ouvertureplis/formgarantiesoummission.zul");

		}

	}

	
}