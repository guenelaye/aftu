package sn.ssi.kermel.web.statistiqueperiodique.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import sn.ssi.kermel.web.common.controllers.AbstractWindow;


@SuppressWarnings("serial")
public class StatistiquePeriodiqueController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {


    
    private Iframe stat;
    
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		

		 
		final String uri ="http://localhost/togomp/index2.php?option=com_statperiodique";
    	 stat.setWidth("100%");
    	 stat.setHeight("100%");
    	 stat.setSrc(uri);
	}

	
	
	@Override
	public void onEvent(final Event event) throws Exception {

//		
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
	}

}