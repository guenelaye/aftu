package sn.ssi.kermel.web.procedurederogataire.controllers;


import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.plansdepassation.ejb.PlansdepassationSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class ProceduresDerogatoiresFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtCommentaires,txtNum;
	Long code;
	private	SygPlansdepassation plan=new SygPlansdepassation();
	private	SygPlansdepassation existeplan=null;
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg,annee,numplan,sigle=null;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Combobox cbannee;
	private ArrayList<String> listValeursAnnees;
	private int intannee;
	private SygAutoriteContractante autorite;
	private Utilisateur infoscompte;
	public static final String WINDOW_PARAM_CODE = "CODE";
	public static final String WINDOW_PARAM_TYPE = "TYPE";
	@Override
	public void onEvent(Event event) throws Exception {


	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
	    annee = UtilVue.getInstance().anneecourant(new Date());
	    infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if((infoscompte.getType().equals(UIConstants.USERS_TYPES_AC)))
		{
			autorite=infoscompte.getAutorite();
			sigle=autorite.getSigle();
		}
		else
		{
			autorite=null;
		}
	    listValeursAnnees = new ArrayList<String>();
	    intannee=Integer.parseInt(annee)+1;
	    existeplan=BeanLocator.defaultLookup(PlansdepassationSession.class).Plan(Integer.parseInt(annee),autorite,null);
	    if(existeplan==null)
	    	listValeursAnnees.add(annee);
	    existeplan=BeanLocator.defaultLookup(PlansdepassationSession.class).Plan(intannee,autorite,null);
	    if(existeplan==null)
	    	listValeursAnnees.add(intannee+"");
	    
		
		if(mode!=null)
		{
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				code = (Long) map.get(PARAM_WINDOW_CODE);
				plan = BeanLocator.defaultLookup(PlansdepassationSession.class).findById(code);
				txtNum.setValue(plan.getNumplan());
		    }	
		}
		else
		{
			 txtNum.setValue("P_"+sigle);
		}
		
		
		
		cbannee.setModel(new SimpleListModel(listValeursAnnees));
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			plan.setNumplan(txtNum.getValue());
			plan.setCommentaires(txtCommentaires.getValue());
			plan.setAnnee(Integer.parseInt(cbannee.getSelectedItem().getValue().toString()));
			plan.setDatecreation(new Date());
			plan.setStatus(Labels.getLabel("kermel.plansdepassation.statut.saisie"));
			plan.setAutorite(autorite);
			plan.setVersion(UIConstants.PARENT);
		  	if ((mode==null)||(mode.equalsIgnoreCase(UIConstants.MODE_NEW))) {
				BeanLocator.defaultLookup(PlansdepassationSession.class).save(plan);
				BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_PLANPASSATION", Labels.getLabel("kermel.plansdepassation.ajouter")+" :" + UtilVue.getInstance().formateLaDate(new Date()), login);
				
			} 
		
		  	loadApplicationState("plans");
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			
			

			if(txtNum.getValue().equals(""))
		     {
               errorComponent = txtNum;
               errorMsg = Labels.getLabel("kermel.plansdepassation.numero")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
		
		
			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
   public void onSelect$cbannee() throws InterruptedException{
	   if(sigle!=null)
	   {
		  // numplan=BeanLocator.defaultLookup(PlansdepassationSession.class).getGeneratedCode(UIConstants.PARAM_PLANPASSATION, sigle, cbannee.getSelectedItem().getValue().toString());
		   numplan="P_"+sigle+"_"+cbannee.getSelectedItem().getValue().toString()+"_1";
		   txtNum.setValue(numplan);
	   }
	   else
	   {
		   Messagebox.show(Labels.getLabel("kermel.plansdepassation.realisation.sigle.existe"), "Erreur", Messagebox.OK, Messagebox.ERROR);
	   }
	   
   }
      
}
