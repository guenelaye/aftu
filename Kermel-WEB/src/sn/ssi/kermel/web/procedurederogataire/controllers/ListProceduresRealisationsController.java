package sn.ssi.kermel.web.procedurederogataire.controllers;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabpanel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.plansdepassation.ejb.PlansdepassationSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class ListProceduresRealisationsController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code;
	private Tabpanel tabDetailsfecp;
	private Include incDetailsfecp;
	private Tab TAB_REALTRAV,TAB_REALSPI,TAB_REALSSERV,TAB_REALSFOURN;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblInfos;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private Long type,idmode=null;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		login = ((String) getHttpSession().getAttribute("user"));
		idplan=(Long) session.getAttribute("idplan");
		plan=BeanLocator.defaultLookup(PlansdepassationSession.class).findById(idplan);
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
		{
			autorite=infoscompte.getAutorite();	
		}
		else
		{
			autorite=null;	  
		}
		lblInfos.setValue(plan.getNumplan()+" "+Labels.getLabel("kermel.plansdepassation.infos.gestion")+" "+plan.getAnnee());
		TAB_REALTRAV.setLabel(Labels.getLabel("kermel.plansdepassation.realisation.type.travaux")+" ("+BeanLocator.defaultLookup( RealisationsSession.class).count(null,UIConstants.PARAM_TMTRAVAUX,idmode,null,UIConstants.NPARENT, Labels.getLabel("kermel.plansdepassation.statut.publier"),autorite, null, null)+")");
		TAB_REALSFOURN.setLabel(Labels.getLabel("kermel.plansdepassation.realisation.type.fournitures")+" ("+BeanLocator.defaultLookup( RealisationsSession.class).count(null,UIConstants.PARAM_TMFOURNITURES,idmode,null,UIConstants.NPARENT, Labels.getLabel("kermel.plansdepassation.statut.publier"),autorite, null, null)+")");
		TAB_REALSPI.setLabel(Labels.getLabel("kermel.plansdepassation.realisation.type.pi")+" ("+BeanLocator.defaultLookup( RealisationsSession.class).count(null,UIConstants.PARAM_TMPI,idmode,null,UIConstants.NPARENT, Labels.getLabel("kermel.plansdepassation.statut.publier"),autorite, null, null)+")");
		TAB_REALSSERV.setLabel(Labels.getLabel("kermel.plansdepassation.realisation.type.services")+" ("+BeanLocator.defaultLookup( RealisationsSession.class).count(null,UIConstants.PARAM_TMSERVICES,idmode,null,UIConstants.NPARENT, Labels.getLabel("kermel.plansdepassation.statut.publier"),autorite, null, null)+")");
		    LibelleTab = (String) session.getAttribute("LibelleTab");
		    if(LibelleTab!=null)
		    {
		    	 if(LibelleTab.equals("REALTF"))
				  {
		    		 TAB_REALTRAV.setSelected(true);
					Include inc = (Include) this.getFellowIfAny("incREALTRAV");
					inc.setSrc("/procedurederogatoire/listrealisationstravaux.zul");		
				  }
				else
				{
					 if(LibelleTab.equals("REALPI"))
					  {
						 TAB_REALSPI.setSelected(true);
						Include inc = (Include) this.getFellowIfAny("incREALSPI");
						inc.setSrc("/procedurederogatoire/listrealisationspi.zul");	
					  }
					else
					{
						if(LibelleTab.equals("REALSERVICES"))
						  {
							TAB_REALSSERV.setSelected(true);
							Include inc = (Include) this.getFellowIfAny("incREALSSERV");
							inc.setSrc("/procedurederogatoire/listrealisationsserv.zul");	
						  }
						else
						{
							if(LibelleTab.equals("REALSFOURN"))
							  {
								TAB_REALSFOURN.setSelected(true);
								Include inc = (Include) this.getFellowIfAny("incREALSFOURN");
								inc.setSrc("/procedurederogatoire/listrealisationsfournitures.zul");	
							  }
							else
							{
								Include inc = (Include) this.getFellowIfAny("incREALTRAV");
								inc.setSrc("/procedurederogatoire/listrealisationstravaux.zul");	
							}
						}
					}
				}
		    }
		    else
		    {
		    	 Include inc = (Include) this.getFellowIfAny("incREALTRAV");
		    	inc.setSrc("/procedurederogatoire/listrealisationstravaux.zul");	
		    }
		   
	
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	public void onClick$menuFermer()
	{
		detach();
		loadApplicationState("accueil");
	}
}