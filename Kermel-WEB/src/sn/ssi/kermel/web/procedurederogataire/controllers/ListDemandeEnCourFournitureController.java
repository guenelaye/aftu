package sn.ssi.kermel.web.procedurederogataire.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Fieldset;
import org.zkoss.zhtml.Legend;
import org.zkoss.zhtml.Table;
import org.zkoss.zhtml.Td;
import org.zkoss.zhtml.Tr;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDerogatoire;
import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsSession;
import sn.ssi.kermel.be.proceduresderogatoires.ejb.ProcedurederogatoireSession;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;
import sn.ssi.kermel.web.passationsmarches.controllers.ProcedureFormController;



@SuppressWarnings("serial")
public class ListDemandeEnCourFournitureController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer , RowRenderer{

	private Listbox list,lstListe;
	private Paging pgProcedure;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private Date sygmdateValidation;
	private Datebox dateValidation;
	Long code;
	Session session = getHttpSession();
	private Menuitem WSOUMETTRE_PROCEDUREDEROGATOIRE,ADD_PROCEDUREDEROGATOIRE,MOD_PROCEDUREDEROGATOIRE,SUPP_PROCEDUREDEROGATOIRE,
	WVALIDER_PROCEDUREDEROGATOIRE;
	 private KermelSousMenu monSousMenu;
	 private Integer Etat;
	 public String extension,images;
	 private SygTypesmarches type;
	 private Div step1,step0,step2;
	 private SygDerogatoire demande=new SygDerogatoire();
	 private SygDerogatoire lastdemande;
	 private SygRealisations realisation;
	 private SygModepassation modepassation;
	 List<SygDerogatoire> historiques = new ArrayList<SygDerogatoire>();
	 private Grid GridListe;
	 private Menuitem menuEditer,menuFermer,menuClose,menuValider;
	 private Long nombrejour;
	 private Label lblrequete,label1,lblfichier,label2,lbl1,lblRealisations,lblModepassation;
	 private Textbox txtVersionElectronique,txtrequete;
	 private Button btnChoixFichier;
	 private Image image;
	 private Label lbStatusBar;
	 private Component errorComponent;
	 private String errorMsg,LibelleTab,objet=null,annees,mystate;
	 private static final String ERROR_MSG_STYLE = "color:red";
	 private Radio rdValider,rdRejeter;
	 private final String cheminDossier = UIConstants.PATH_PJ;
	 private String nomFichier="";
	 UtilVue utilVue = UtilVue.getInstance();
	 private Utilisateur infoscompte;
	 private SygAutoriteContractante autorite=null;
	 private Iframe idIframe;
	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		mystate=(String) session.getAttribute("statedemande");
		if(mystate.equals("demandeencours"))
		{
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.PROCEDUREDEROGATOIRE);
		monSousMenu.afterCompose();
//		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		Components.wireFellows(this, this);
	
		if (WSOUMETTRE_PROCEDUREDEROGATOIRE != null)
			WSOUMETTRE_PROCEDUREDEROGATOIRE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_SOUMETTRE);
		if (ADD_PROCEDUREDEROGATOIRE != null)
			ADD_PROCEDUREDEROGATOIRE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
		if (MOD_PROCEDUREDEROGATOIRE != null)
			MOD_PROCEDUREDEROGATOIRE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
		if (SUPP_PROCEDUREDEROGATOIRE != null)
			SUPP_PROCEDUREDEROGATOIRE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
		}
		else
		{
			monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
			monSousMenu.setFea_code(UIConstants.VALDMDDEROG);
			monSousMenu.afterCompose();
			
			Components.wireFellows(this, this); // mais le wireFellows precedent
			Components.wireFellows(this, this);
			
			if (WVALIDER_PROCEDUREDEROGATOIRE != null)
				WVALIDER_PROCEDUREDEROGATOIRE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_SOUMETTRE);
			
		}
			
			addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
			addEventListener(ApplicationEvents.ON_SOUMETTRE, this);
			addEventListener(ApplicationEvents.ON_ADD, this);
			addEventListener(ApplicationEvents.ON_EDIT, this);
			addEventListener(ApplicationEvents.ON_DELETE, this);
			addEventListener(ApplicationEvents.ON_TRAITEMENTS, this);
 
		list.setItemRenderer(this);
		GridListe.setRowRenderer(this);
		pgProcedure.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pgProcedure.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);
	
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		if(mystate.equals("validation_demandederogatoires"))
		{
			lblrequete.setValue(Labels.getLabel("kermel.denonciation.Decision.commentaire"));
			}
		else
		{
			
			lblrequete.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.derogatoires.requete"));
	
		}
		type=BeanLocator.defaultLookup(TypesmarchesSession.class).findById(UIConstants.PARAM_TMFOURNITURES);
	
	    Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 List<SygDerogatoire> derogatoire = BeanLocator.defaultLookup(ProcedurederogatoireSession.class).findDemandes(pgProcedure.getActivePage()*byPage,byPage,type.getId());
			 SimpleListModel listModel = new SimpleListModel(derogatoire);
			 list.setModel(listModel);
			 pgProcedure.setTotalSize(BeanLocator.defaultLookup(ProcedurederogatoireSession.class).countDemandes(type.getId()));
			
			
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/procedurederogatoire/formdemandederogatoire.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.proceduresmarches.derogatoires.demande"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
        	final HashMap<String, Object> data = new HashMap<String, Object>();
        	data.put(ProcedureFormController.WINDOW_PARAM_TYPE,UIConstants.PARAM_TMFOURNITURES);
			data.put(DemandeEnCoursFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
 		    showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/procedurederogatoire/formdemandederogatoire.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(DemandeEnCoursFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(DemandeEnCoursFormController.PARAM_WIDOW_CODE, list
					.getSelectedItem().getValue());
			    showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				// de
				// fixer
				// les
				// dimenisons
				// du
				// popup
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// de
				// passer
				// des
				// parametres
				// au
				// popup
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < list.getSelectedCount(); i++) {
					Long codes = (Long)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					System.out.println(codes);
				BeanLocator.defaultLookup(ProcedurederogatoireSession.class).delete(codes);
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_SOUMETTRE)){
				if (list.getSelectedItem()==null) {
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				}
				demande=(SygDerogatoire) list.getSelectedItem().getValue();
				modepassation=demande.getModepassationCible();
				realisation=demande.getRealisation();
				lblRealisations.setValue(realisation.getLibelle());
				lblModepassation.setValue(demande.getModepassationCible().getLibelle());
				step0.setVisible(false);
				step1.setVisible(true);
				step2.setVisible(false);
				Events.postEvent(ApplicationEvents.ON_TRAITEMENTS, this, null);
			
			}
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_TRAITEMENTS)) {
				 historiques   = BeanLocator.defaultLookup(ProcedurederogatoireSession.class).find(0, -1, null, null, realisation, -1, -1, -1);
				 GridListe.setModel(new SimpleListModel(historiques));
				 if(historiques.size()>0)
				 {
//					 if(historiques.get(0).getValidationED()==1||historiques.get(0).getValidationPU()==1||historiques.get(0).getValidationAOR()==1)
//					 {
//						 menuEditer.setVisible(true);
//					 }
					 lastdemande=historiques.get(0);
					 if(mystate.equals("demandeencours"))
						{
						     if(modepassation.getId().intValue()==5)
						     {
						    	 if(lastdemande.getValidationED()==1)
								 {
									 InfosDossier();
									 GridListe.setVisible(false);
									 menuClose.setVisible(false);
									 menuEditer.setVisible(true);
									 menuFermer.setVisible(false);
									 menuValider.setVisible(true);
								 }
								 else
								 {
									 GridListe.setVisible(true);
									 menuClose.setVisible(false);
									 menuEditer.setVisible(false);
									 menuFermer.setVisible(true);
									 menuValider.setVisible(false);
								 }
						     }
							 else if(modepassation.getId().intValue()==4)
							 {
								 if(lastdemande.getValidationAOR()==1)
								 {
									 InfosDossier();
									 GridListe.setVisible(false);
									 menuClose.setVisible(false);
									 menuEditer.setVisible(true);
									 menuFermer.setVisible(false);
									 menuValider.setVisible(true);
								 }
								 else
								 {
									 GridListe.setVisible(true);
									 menuClose.setVisible(false);
									 menuEditer.setVisible(false);
									 menuFermer.setVisible(true);
									 menuValider.setVisible(false);
								 }
							 }
							 else
							 {
								 if(lastdemande.getValidationPU()==1)
								 {
									 InfosDossier();
									 GridListe.setVisible(false);
									 menuClose.setVisible(false);
									 menuEditer.setVisible(true);
									 menuFermer.setVisible(false);
									 menuValider.setVisible(true);
								 }
								 else
								 {
									 GridListe.setVisible(true);
									 menuClose.setVisible(false);
									 menuEditer.setVisible(false);
									 menuFermer.setVisible(true);
									 menuValider.setVisible(false);
								 }
							 }
						}
					 else
					    {
						 if(modepassation.getId().intValue()==5)
					     {
							 if(lastdemande.getValidationED()==0)
							 {
								 InfosDossier();
								 rdValider.setVisible(true);
								 rdRejeter.setVisible(true);
								 menuEditer.setVisible(true);
								 menuValider.setVisible(true);
								 GridListe.setVisible(false);
							 }
							 else
							 {
								 rdValider.setVisible(false);
								 rdRejeter.setVisible(false);
								 menuEditer.setVisible(false);
								 menuValider.setVisible(false);
								 GridListe.setVisible(true);
							 }
					     }
						 else if(modepassation.getId().intValue()==4)
					     {
							 if(lastdemande.getValidationAOR()==0)
							 {
								 InfosDossier();
								 rdValider.setVisible(true);
								 rdRejeter.setVisible(true);
								 menuEditer.setVisible(true);
								 menuValider.setVisible(true);
								 GridListe.setVisible(false);
							 }
							 else
							 {
								 rdValider.setVisible(false);
								 rdRejeter.setVisible(false);
								 menuEditer.setVisible(false);
								 menuValider.setVisible(false);
								 GridListe.setVisible(true);
							 }
					     }
						 else
						 {
							 if(lastdemande.getValidationPU()==0)
							 {
								 InfosDossier();
								 rdValider.setVisible(true);
								 rdRejeter.setVisible(true);
								 menuEditer.setVisible(true);
								 menuValider.setVisible(true);
								 GridListe.setVisible(false);
							 }
							 else
							 {
								 rdValider.setVisible(false);
								 rdRejeter.setVisible(false);
								 menuEditer.setVisible(false);
								 menuValider.setVisible(false);
								 GridListe.setVisible(true);
							 }
						 }
							
						 
					    }
				 }
				}
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) {
				Image button = (Image) event.getTarget();
				String nomfichier=(String) button.getAttribute("fichier");
				step0.setVisible(false);
				step1.setVisible(false);
				step2.setVisible(true);
				String filepath = cheminDossier +  nomfichier;
				File f = new File(filepath.replaceAll("\\\\", "/"));

				org.zkoss.util.media.AMedia mymedia = null;
				try {
					mymedia = new AMedia(f, null, null);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				if (mymedia != null)
					idIframe.setContent(mymedia);
				else
					idIframe.setSrc("");

				idIframe.setHeight("600px");
				idIframe.setWidth("100%");
			}
	}
	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render( Listitem item,  Object data, int index) throws Exception {
		SygDerogatoire derogatoire = (SygDerogatoire) data;
		item.setValue(derogatoire);
		//item.setAttribute("Etat", derogatoire.getEtat());
		
		Listcell cellreference = new Listcell(derogatoire.getRealisation().getReference());
		cellreference.setParent(item);
		
		Listcell cellrealisation = new Listcell(derogatoire.getRealisation().getLibelle());
		cellrealisation.setParent(item);
		
		Listcell cellmodepassation = new Listcell(derogatoire.getModepassationCible().getLibelle());
		cellmodepassation.setParent(item);

		 
		Listcell cellLancement = new Listcell("");
		if(derogatoire.getRealisation().getDatelancement()!=null)
			cellLancement.setLabel(UtilVue.getInstance().formateLaDate(derogatoire.getRealisation().getDatelancement()));
		cellLancement.setParent(item);
	
		Listcell cellAttribution = new Listcell("");
		if(derogatoire.getRealisation().getDateattribution()!=null)
			cellAttribution.setLabel(UtilVue.getInstance().formateLaDate(derogatoire.getRealisation().getDateattribution()));
		cellAttribution.setParent(item);
		
		Listcell cellMontant = new Listcell(ToolKermel.format2Decimal(derogatoire.getRealisation().getMontant()));   
		cellMontant.setParent(item);
		 
	}
	
	 @Override
		public void render(Row row, Object data, int index) throws Exception {
		 SygDerogatoire historiques = (SygDerogatoire) data;
		 row.setValue(historiques);
		 
		 Fieldset fieldset=new Fieldset();
		 fieldset.setStyle("width:96%;");
	
		 Legend legend = new Legend();
		 Label label = new Label("Mise en validation");
		 label.setParent(legend);
		 legend.setParent(fieldset);
		 
		 Table table0=new Table();
		 Tr tr0=new Tr();
		 Td tdac=new Td();
		 Td tddt=new Td();
		 Td tddcmp=new Td();
		 
			 /////:::AC
			 Fieldset fieldsetac=new Fieldset();
			 fieldsetac.setStyle("width:500px;");
		
			 Legend legendac = new Legend();
			 Label labelac = new Label("");
			 if(historiques.getUser()!=null)
			 {
				 labelac.setValue(historiques.getUser().getPrenom()+" "+historiques.getUser().getNom());
			 }
				
			 else
				 labelac.setValue("(non encore renseign�)");
			 labelac.setParent(legendac);
			 legendac.setParent(fieldsetac);
			 
			 fieldsetac.setParent(tdac);
			 
				 Table tableac=new Table();
				 
				 Tr trac0=new Tr();
				 Td tdac00=new Td();
				 Td tdac01=new Td();
				 Td tdac02=new Td();
				 Label labelac00 = new Label("Date de mise en validation");
				 labelac00.setParent(tdac00);
				 Label labelac01 = new Label(":");
				 labelac01.setParent(tdac01);
				 Label labelac02 = new Label(UtilVue.getInstance().formateLaDate(historiques.getDateMiseValidation()));
				 labelac02.setParent(tdac02);
				 tdac00.setParent(trac0);
				 tdac01.setParent(trac0);
				 tdac02.setParent(trac0);
				 trac0.setParent(tableac);
				 
				 Tr trac1=new Tr();
				 Td tdac10=new Td();
				 Td tdac11=new Td();
				 Td tdac12=new Td();
				 Label labelac10 = new Label("N� AUTORISATION");
				 labelac10.setParent(tdac10);
				 Label labelac11 = new Label(":");
				 labelac11.setParent(tdac11);
				 Label labelac12 = new Label(historiques.getNumAut());
				 labelac12.setParent(tdac12);
				 tdac10.setParent(trac1);
				 tdac11.setParent(trac1);
				 tdac12.setParent(trac1);
				 trac1.setParent(tableac);
				 
				 Tr trac2=new Tr();
				 Td tdac20=new Td();
				 Td tdac21=new Td();
				 Td tdac22=new Td();
				 Label labelac20 = new Label("Requete ");
				 labelac20.setParent(tdac20);
				 Label labelac21 = new Label(":");
				 labelac21.setParent(tdac21);
				 Label labelac22 = new Label(historiques.getRequete());
				 labelac22.setParent(tdac22);
				 tdac20.setParent(trac2);
				 tdac21.setParent(trac2);
				 tdac22.setParent(trac2);
				 trac2.setParent(tableac);
				 
				 Tr trac3=new Tr();
				 Td tdac30=new Td();
				 Td tdac31=new Td();
				 Td tdac32=new Td();
				 Label labelac30 = new Label("Fichier joint ");
				 labelac30.setParent(tdac30);
				 Label labelac31 = new Label(":");
				 labelac31.setParent(tdac31);
				 Label labelac32 = new Label("");
				 if(historiques.getFichier().equals(""))
				 {
					 labelac32.setValue("(non encore renseign�)");
					 labelac32.setWidth("80%");
					 labelac32.setParent(tdac32);
				 }
				 else
				 {
					 extension=historiques.getFichier().substring(historiques.getFichier().length()-3,  historiques.getFichier().length());
					 if(extension.equalsIgnoreCase("pdf"))
						 images="/images/icone_pdf.png";
					 else  
						 images="/images/word.jpg";
						
					 Image image=new Image();
					 image.setSrc(images);
					 image.setAttribute("fichier", historiques.getFichier());
					 image.setTooltiptext("Visualiser fichier");
					 image.addEventListener(Events.ON_CLICK, ListDemandeEnCourFournitureController.this);
					 image.setParent(tdac32); 
				 }
				
				 tdac30.setParent(trac3);
				 tdac31.setParent(trac3);
				 tdac32.setParent(trac3);
				 trac3.setParent(tableac);
				 
				 tableac.setParent(fieldsetac);
				 
			 tdac.setParent(tr0);
		 
			 /////////DT////////
			 Fieldset fieldsetdelai=new Fieldset();
			 fieldsetdelai.setStyle("width:20%;align:center;");
			 
			 Legend legenddelai = new Legend();
			 Label labeldelai = new Label("D�lai de traitement ");
			 labeldelai.setParent(legenddelai);
			 legenddelai.setParent(fieldsetdelai);            
			 
			 fieldsetdelai.setParent(tddt);      
			 tddt.setStyle("align:center;");
			 
			 Label duree = new Label(" ");
			 if(historiques.getDateValidation()!=null)
			 {
				 nombrejour=ToolKermel.DifferenceDate(historiques.getDateMiseValidation(), historiques.getDateValidation(), "jj");
				 duree.setValue(nombrejour+1+" Jour(s)");
			 }
			 else
				 duree.setValue("");
			 
			 duree.setParent(fieldsetdelai);
			 tddt.setParent(tr0);
		 
		 
	          /////////DCMP////////
			 Fieldset fieldsetdcmp=new Fieldset();
			 fieldsetdcmp.setStyle("width:500px;");
			 
			 Legend legenddcmp = new Legend();
			 Label labeldcmp = new Label("DCMP");
			 labeldcmp.setParent(legenddcmp);
			 legenddcmp.setParent(fieldsetdcmp);
			 
			 
				 Table tabledcmp=new Table();
				 
				
				 
				 Tr trdcmp1=new Tr();
				 Td tddcmp10=new Td();
				 Td tddcmp11=new Td();
				 Td tddcmp12=new Td();
				 Label labeldcmp10 = new Label("Date de validation ");
				 labeldcmp10.setParent(tddcmp10);
				 Label labeldcmp11 = new Label(":");
				 labeldcmp11.setParent(tddcmp11);
				 Label labeldcmp12 = new Label("");
				 if(historiques.getDateValidation()!=null)
					 labeldcmp12.setValue(UtilVue.getInstance().formateLaDate(historiques.getDateValidation()));
				 labeldcmp12.setParent(tddcmp12);
				 tddcmp10.setParent(trdcmp1);
				 tddcmp11.setParent(trdcmp1);
				 tddcmp12.setParent(trdcmp1);
				 trdcmp1.setParent(tabledcmp);
	             
				 Tr trdcmp2=new Tr();
				 Td tddcmp20=new Td();
				 Td tddcmp21=new Td();
				 Td tddcmp22=new Td();
				 Label labeldcmp20 = new Label("Statut");
				 labeldcmp20.setParent(tddcmp20);
				 Label labeldcmp21 = new Label(":");
				 labeldcmp21.setParent(tddcmp21);
				 Label labeldcmp22 = new Label("");
				 
				 labeldcmp22.setParent(tddcmp22);
				 tddcmp20.setParent(trdcmp2);
				 tddcmp21.setParent(trdcmp2);
				 tddcmp22.setParent(trdcmp2);
				 trdcmp2.setParent(tabledcmp);
				 
				 if(modepassation.getId().intValue()==5)
			     {
					 if(historiques.getValidationED()==0)
					   {
						 labeldcmp22.setValue(Labels.getLabel("kermel.plansdepassation.dossier.nonrenseigne")); 
						  }
					 else
						 if(historiques.getValidationED()==2)
						   {
							  labeldcmp22.setValue(Labels.getLabel("kermel.plansdepassation.dossier.valide")); 
							   }
						 else
						  {
							 labeldcmp22.setValue(Labels.getLabel("kermel.plansdepassation.dossier.rejete"));  
							  }
							 
			     }
				 else
				 {
					 if(modepassation.getId().intValue()==4)
				     {
						 if(historiques.getValidationAOR()==0)
						   {
							 labeldcmp22.setValue(Labels.getLabel("kermel.plansdepassation.dossier.nonrenseigne")); 
							  }
						 else
							 if(historiques.getValidationAOR()==2)
							   {
								  labeldcmp22.setValue(Labels.getLabel("kermel.plansdepassation.dossier.valide")); 
								   }
							 else
							  {
								 labeldcmp22.setValue(Labels.getLabel("kermel.plansdepassation.dossier.rejete"));  
								  }
				     }
					 else
					 {
						 if(historiques.getValidationPU()==0)
						   {
							 labeldcmp22.setValue(Labels.getLabel("kermel.plansdepassation.dossier.nonrenseigne")); 
							  }
						 else
							 if(historiques.getValidationPU()==2)
							   {
								  labeldcmp22.setValue(Labels.getLabel("kermel.plansdepassation.dossier.valide")); 
								   }
							 else
							  {
								 labeldcmp22.setValue(Labels.getLabel("kermel.plansdepassation.dossier.rejete"));  
								  }
					 }
					
							 
				 }
				
				
				
//				 else
//					 labeldcmp22.setValue("non encore renseign�"); 
				
				 /////////:Commentaires
				 Tr trdcmp4=new Tr();
				 Td tddcmp40=new Td();
				 Td tddcmp41=new Td();
				 Td tddcmp42=new Td();
				 Label labeldcmp40 = new Label("Commentaires");
				 Label labeldcmp41 = new Label(":");
				 Label labeldcmp42 = new Label(historiques.getCommentairedcmp());
				 labeldcmp40.setParent(tddcmp40);
				 labeldcmp41.setParent(tddcmp41);
				 labeldcmp42.setParent(tddcmp42);
				 
				 tddcmp40.setParent(trdcmp4);
				 tddcmp41.setParent(trdcmp4);
				 tddcmp42.setParent(trdcmp4);
				 
				 trdcmp4.setParent(tabledcmp);
				 
				 ////////Fichier Joint
				 Tr trdcmp5=new Tr();
				 Td tddcmp50=new Td();
				 Td tddcmp51=new Td();
				 Td tddcmp52=new Td();
				 Label labeldcmp50 = new Label("Fichier joint");
				 Label labeldcmp51 = new Label(":");
				 Label labeldcmp52 = new Label("");
				 labeldcmp52.setParent(tddcmp52);
				 
				 if(historiques.getFichierdcmp()==null)
				 {
					 labeldcmp52.setValue("(non encore renseign�)");
					 labeldcmp52.setWidth("80%");
					 labeldcmp52.setParent(tddcmp52);
				 }
				 else
				 {
					 extension=historiques.getFichierdcmp().substring(historiques.getFichierdcmp().length()-3,  historiques.getFichierdcmp().length());
					 if(extension.equalsIgnoreCase("pdf"))
						 images="/images/icone_pdf.png";
					 else  
						 images="/images/word.jpg";
						
					 Image image=new Image();
					 image.setSrc(images);
					 image.setAttribute("fichier", historiques.getFichierdcmp());
					 image.setTooltiptext("Visualiser fichier");
					 image.addEventListener(Events.ON_CLICK, ListDemandeEnCourFournitureController.this);
					 image.setParent(tddcmp52); 
				 }
				 labeldcmp50.setParent(tddcmp50);
				 labeldcmp51.setParent(tddcmp51);
				
				 
				 tddcmp50.setParent(trdcmp5);
				 tddcmp51.setParent(trdcmp5);
				 tddcmp52.setParent(trdcmp5);
				 
				 trdcmp5.setParent(tabledcmp);
				 
				 tabledcmp.setParent(fieldsetdcmp);

			 fieldsetdcmp.setParent(tddcmp);
			 tddcmp.setParent(tr0);
		 ////////////
		 tr0.setParent(table0);
		 table0.setParent(fieldset);
		 fieldset.setParent(row);

	   }
	

	 public void onClick$menuFermer() {
		    step0.setVisible(true);
			step1.setVisible(false);
			step2.setVisible(false);
		}
	 public void InfosDossier() {
		 lblrequete.setVisible(true);
		 label1.setVisible(true);
		 txtrequete.setVisible(true);
		 lbl1.setVisible(true);
		 lblfichier.setVisible(true);
		 label2.setVisible(true);
		 txtVersionElectronique.setVisible(true);
		 btnChoixFichier.setVisible(true);
		 image.setVisible(true);
		 if(mystate.equals("validation_demandederogatoires"))
			{
			 rdValider.setVisible(true);
			 rdRejeter.setVisible(true);
			}
		 else
		 {
			 rdValider.setVisible(false);
			 rdRejeter.setVisible(false);
		 }
	 }
	 
	 private boolean checkFieldConstraints() {
			
			try {
				if(txtrequete.getValue().equals(""))
			     {
	              errorComponent = txtrequete;
	              errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.derogatoires.requete")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				   lbStatusBar.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar.setValue(errorMsg);
				  throw new WrongValueException (errorComponent, errorMsg);
			     }
				
				
				return true;
					
			}
			catch (Exception e) {
				errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
				+ " [checkFieldConstraints]";
				errorComponent = null;
				return false;

				
			}
			
		}
	 public void onClick$menuValider() {
		
		 if(mystate.equals("validation_demandederogatoires"))
			{
				 if(rdValider.isSelected()==true)
				  {
					 realisation.setAppel(0);
					 if(modepassation.getId().intValue()==5)
						 lastdemande.setValidationED(2);
					 else if(modepassation.getId().intValue()==4)
						 lastdemande.setValidationAOR(2);
					 else
						 lastdemande.setValidationPU(2);
				  }
				 else
				  {
					 if(modepassation.getId().intValue()==5)
						 lastdemande.setValidationED(1);
						 else if(modepassation.getId().intValue()==4)
							 lastdemande.setValidationAOR(1);
						 else
							 lastdemande.setValidationPU(1);
				  }
				 lastdemande.setFichierdcmp(txtVersionElectronique.getValue());
				 lastdemande.setCommentairedcmp(txtrequete.getValue());
				 lastdemande.setDateValidation(new Date());
				 BeanLocator.defaultLookup(ProcedurederogatoireSession.class).update(lastdemande);
				 realisation.setModepassation(modepassation);
				 BeanLocator.defaultLookup(RealisationsSession.class).update(realisation);
		
			}
		 else
		   {
			 savedemande();
		   }
		    session.setAttribute("LibelleTab","REALSFOURN");
		    loadApplicationState(mystate);
			detach();
		 }
		  
	 public void updateemande() {
		 
	 }
	 public void savedemande() {
		 if(checkFieldConstraints())
		 {
			    demande.setUser(infoscompte);
			    demande.setAutorite(lastdemande.getAutorite());
				demande.setModepassationCible(lastdemande.getModepassationCible());
				demande.setRealisation(realisation);
				demande.setModepassation(realisation.getModepassation().getId());
				demande.setDateMiseValidation(new Date());
				demande.setValidationAOR(0);
				demande.setValidationED(0);
				demande.setValidationPU(0);
				demande.setRequete(txtrequete.getValue());
				demande.setFichier(txtVersionElectronique.getValue());
				BeanLocator.defaultLookup(ProcedurederogatoireSession.class).save(demande);
		 }
	 }
	 public void InfosHistorique() {
		 lblrequete.setVisible(false);
		 label1.setVisible(false);
		 txtrequete.setVisible(false);
		 lbl1.setVisible(false);
		 lblfichier.setVisible(false);
		 label2.setVisible(false);
		 txtVersionElectronique.setVisible(false);
		 btnChoixFichier.setVisible(false);
		 image.setVisible(false);
		 rdValider.setVisible(false);
		 rdRejeter.setVisible(false);
	 }
	 public void onClick$menuEditer() {
			InfosHistorique();
			GridListe.setVisible(true);
			menuEditer.setVisible(false);
			menuClose.setVisible(true);
			menuFermer.setVisible(false);
		}
		public void onClick$menuClose() {
			InfosDossier();
			menuEditer.setVisible(true);
			menuClose.setVisible(false);
			menuValider.setDisabled(false);
			GridListe.setVisible(false);
			menuFermer.setVisible(true);
		}
		
		public void onClick$btnChoixFichier() {
			//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
				if (ToolKermel.isWindows())
					nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
				else
					nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

				txtVersionElectronique.setValue(nomFichier);
			}
		public void onClick$menuFermerstep2() {
			step0.setVisible(false);
			step1.setVisible(true);
			step2.setVisible(false);
		}
		
}
