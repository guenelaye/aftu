package sn.ssi.kermel.web.procedurederogataire.controllers;

import java.util.Date;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Column;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.be.entity.SygTypesmarchesmodespassations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsSession;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesSession;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesmodespassationsSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class ListProceduresRealisationsServController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
     String page=null;
    private Listheader lshReference;
    Session session = getHttpSession();
    SygPlansdepassation plan=new SygPlansdepassation();
    private Long idplan,idrealisation;
    private Label lblInfos;
    private KermelSousMenu monSousMenu;
    private Menuitem WSOUMETTRE_PROCEDUREDEROGATOIRE;
    String login;
    private Listbox list;
    private Listbox lstListe;
    private Column idgride;
    private Combobox cbselect;
    private String filtrepar="service.libelle";
    private String idpar="service.id";
    private Bandbox bdMode;
    private Listbox lstMode;
    private Paging pgMode;
    private String libellemode=null,reference=null;
    private Textbox txtRechercherMode,txtReference;
    SygModepassation modepassation=new SygModepassation();
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Long mode=null;
    private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygTypesmarches typemarche=null;
	private Datebox dtdatedebut,dtdatefin;
	private Date datedebut=null,datefin=null;
	private Long type,code,idmode=null;
	 
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.PROCEDURESDEROGATOIRES);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		
		if (WSOUMETTRE_PROCEDUREDEROGATOIRE != null)
			WSOUMETTRE_PROCEDUREDEROGATOIRE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_SOUMETTRE);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_SOUMETTRE, this);
		
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		
		lstListe.setItemRenderer(this);
		
//		addEventListener(ApplicationEvents.ON_MODES, this);
//		lstMode.setItemRenderer(new ModesRenderer());
//		pgMode.setPageSize(byPageBandbox);
//		pgMode.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODES);
		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		login = ((String) getHttpSession().getAttribute("user"));
		
		typemarche=BeanLocator.defaultLookup(TypesmarchesSession.class).findById(UIConstants.PARAM_TMSERVICES);	
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		Events.postEvent(ApplicationEvents.ON_MODES, this, null);
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			List<SygRealisations> realisations = BeanLocator.defaultLookup(RealisationsSession.class).find(activePage,byPage,reference,UIConstants.PARAM_TMSERVICES,idmode,null,UIConstants.NPARENT, Labels.getLabel("kermel.plansdepassation.statut.publier"),autorite, datedebut, datefin);
			 SimpleListModel listModel = new SimpleListModel(realisations);
			 lstListe.setModel(listModel);
			 pgPagination.setTotalSize(BeanLocator.defaultLookup( RealisationsSession.class).count(reference,UIConstants.PARAM_TMSERVICES,idmode,null,UIConstants.NPARENT, Labels.getLabel("kermel.plansdepassation.statut.publier"),autorite, datedebut, datefin));
		}
     	else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_SOUMETTRE)){
		if (lstListe.getSelectedItem()==null) {
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		}
		session.setAttribute("numprocedure",lstListe.getSelectedItem().getValue());
		loadApplicationState("typedemande");
	}
		else 	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODES)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgMode.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgMode.getActivePage() * byPageBandbox;
				pgMode.setPageSize(byPageBandbox);
			}
			List<SygTypesmarchesmodespassations> modes = BeanLocator.defaultLookup(TypesmarchesmodespassationsSession.class).find(activePage, byPageBandbox,libellemode,typemarche,null);
			lstMode.setModel(new SimpleListModel(modes));
			pgMode.setTotalSize(BeanLocator.defaultLookup(TypesmarchesmodespassationsSession.class).count(libellemode,typemarche,null));
		}
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygRealisations plans = (SygRealisations) data;
		item.setValue(plans.getIdrealisation());
		item.setAttribute("typemarches", plans.getTypemarche().getLibelle());
		item.setAttribute("realisation", plans.getLibelle());
	//	item.setAttribute("", plans.getLibelle());

		 Listcell cellReference = new Listcell(plans.getReference());
		 cellReference.setParent(item);
		 
		 Listcell cellLibelle = new Listcell(plans.getLibelle());
		 cellLibelle.setParent(item);
		 
		 Listcell cellTypemarche = new Listcell(plans.getTypemarche().getLibelle());
		 cellTypemarche.setParent(item);
		 
		 Listcell cellModepassation = new Listcell(plans.getModepassation().getLibelle());
		 cellModepassation.setParent(item);
		 
		 Listcell cellDateLancement = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDatelancement()));
		 cellDateLancement.setParent(item);
		 
		 Listcell cellDateattribution = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDateattribution()));
		 cellDateattribution.setParent(item);
		 
		 Listcell cellDatedemarrage = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDatedemarrage()));
		 cellDatedemarrage.setParent(item);
		 
		 Listcell cellDateachevement = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDateachevement()));
		 cellDateachevement.setParent(item);
		 
		 Listcell cellMontant = new Listcell(ToolKermel.format3Decimal(plans.getMontant()));
		 cellMontant.setParent(item);

		 
	}

	public void onSelect$lstListe(){
		session.setAttribute("typemarches", lstListe.getSelectedItem().getAttribute("typemarches").toString());
		session.setAttribute("typemarchet", "");
		session.setAttribute("typemarchef", "");
		session.setAttribute("typemarchepi", "");
		session.setAttribute("realisation", lstListe.getSelectedItem().getAttribute("realisation").toString());
		session.setAttribute("idrealisation", lstListe.getSelectedItem().getValue());
		
	}
	
	

	public void onClick$menuFermer()
	{    detach();
		loadApplicationState("accueil");
	}
	
	
///////////Mode de passation///////// 
	public void onSelect$lstMode(){
	modepassation= (SygModepassation) lstMode.getSelectedItem().getValue();
	bdMode.setValue(modepassation.getLibelle());
	bdMode.close();
	
	}
	
	public class ModesRenderer implements ListitemRenderer{
	
	
	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygTypesmarchesmodespassations modes = (SygTypesmarchesmodespassations) data;
		item.setValue(modes.getMode());
		
		Listcell cellCode = new Listcell(modes.getMode().getCode());
		cellCode.setParent(item);
		Listcell cellLibelle = new Listcell("");
		if (modes.getMode().getLibelle()!=null){
			cellLibelle.setLabel(modes.getMode().getLibelle());
		}
		cellLibelle.setParent(item);
	
	}
	}
	public void onFocus$txtRechercherMode(){
	if(txtRechercherMode.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.modepassation"))){
		txtRechercherMode.setValue("");
	}		 
	}
	
	public void  onClick$btnRechercherMode(){
	if(txtRechercherMode.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.modepassation")) || txtRechercherMode.getValue().equals("")){
		libellemode= null;
		page=null;
	}else{
		libellemode = txtRechercherMode.getValue();
		page="0";
	}
	Events.postEvent(ApplicationEvents.ON_MODES, this, page);
	}
	
	public void  onClick$bchercher(){
		if((txtReference.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.reference")))||(txtReference.getValue().equals("")))
		 {
			reference=null;
		 }
		else
		 {
			reference=txtReference.getValue();
			page="0";
		 }
		if(dtdatedebut.getValue()==null)
		 {
			datedebut=null;
			
		 }
		else
		 {
			datedebut=dtdatedebut.getValue();
			page="0";
		 }
		if(dtdatefin.getValue()==null)
		 {
			datefin=null;
			
		 }
		else
		 {
			datefin=dtdatefin.getValue();
			page="0";
		 }
		if( bdMode.getValue().equals("")){
			mode= null;
			page=null;
		}else{
			mode = modepassation.getId();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
	
	 public void onOK$txtMode() {
//		 List<SygModepassation> modes = BeanLocator.defaultLookup(ModepassationSession.class).find(0, -1,txtMode.getValue(),null);
//				
//			if (modes.size() > 0) {
//				modepassation=(SygModepassation)modes.get(0);
//				txtMode.setValue(modepassation.getCode());
//				bdMode.setValue(modepassation.getLibelle());
//				mode = modepassation.getId();
//			}
//			else {
//				bdMode.setValue(null);
//				bdMode.open();
//				txtMode.setFocus(true);
//			}
			
		}
	 
	 public void onFocus$txtReference(){
			if(txtReference.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.reference"))){
				txtReference.setValue("");
			}		 
			}
		public void onBlur$txtReference(){
			if(txtReference.getValue().equalsIgnoreCase("")){
				txtReference.setValue(Labels.getLabel("kermel.plansdepassation.reference"));
			}		 
			}
		public void onOK$txtReference(){
			onClick$bchercher();	
		}
		public void onOK$bchercher(){
			onClick$bchercher();	
		}
}