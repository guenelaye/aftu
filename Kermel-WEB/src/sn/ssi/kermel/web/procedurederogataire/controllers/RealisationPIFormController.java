package sn.ssi.kermel.web.procedurederogataire.controllers;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCategori;
import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygModeselection;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.be.entity.SygTypesmarchesmodespassations;
import sn.ssi.kermel.be.entity.SygTypesmarchesmodesselections;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsSession;
import sn.ssi.kermel.be.referentiel.ejb.SecteursactivitesSession;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesSession;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesmodespassationsSession;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesmodesselectionsSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

/**

 * @author Adama samb
 * @since mardi 11 Janvier 2011, 12:34
 
 */
public class RealisationPIFormController extends AbstractWindow implements
		AfterCompose, EventListener ,ListitemRenderer{

	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	public static final String WINDOW_PARAM_TYPE = "TYPE";
	private Paging pgPagination,pgCategorie,pgMode,pgModeSelection;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    private   String page=null,mode;
    private Long type,code,idmode=null;
    private Listheader lshReference,lshLibelle,lshModepassation,lshDatelancement,lshDateattribution,lshDatedemarrage,lshDateachevement,lshMontant;
    Session session = getHttpSession();
    SygRealisations realisation=new SygRealisations();
    SygRealisations realisations=new SygRealisations();
    SygCategori categorie=new SygCategori();
    SygTypesmarches typemarche=new SygTypesmarches();
    SygAppelsOffres appel=new SygAppelsOffres();
     String login,libelle=null,libellemode=null,libellemodeselection=null;
    private Listbox lstListe,lstCategorie,lstMode,lstModeSelection;
    private Div step0,step1,step2;
    private Label lblRealisations,lblRealisationsStep2,lblCategories,lblTypemarche,lblTypemarcheStep2;
    private Bandbox bdMode,bdModeSelection;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherMode,txtRechercherModeSelection,References;
    SygModepassation modepassation=null;
    SygModeselection modeselection=null;
    private Textbox txtObjets,txtReferences,txtReference;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg,LibelleTab,reference=null;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Datebox dtcreation;
	private Decimalbox dcmontant;
	private Utilisateur infoscompte;
    private SygAutoriteContractante autorite=null;
    private Datebox dtdatedebut,dtdatefin;
	private Date datedebut=null,datefin=null;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		lshReference.setSortAscending(new FieldComparator("reference", false));
		lshReference.setSortDescending(new FieldComparator("reference", true));
		lshLibelle.setSortAscending(new FieldComparator("libelle", false));
		lshLibelle.setSortDescending(new FieldComparator("libelle", true));
		lshModepassation.setSortAscending(new FieldComparator("modepassation.libelle", false));
		lshModepassation.setSortDescending(new FieldComparator("modepassation.libelle", true));
		lshDatelancement.setSortAscending(new FieldComparator("datelancement", false));
		lshDatelancement.setSortDescending(new FieldComparator("datelancement", true));
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		lstListe.setItemRenderer(this);
		
		addEventListener(ApplicationEvents.ON_CATEGORIES, this);
		pgCategorie.setPageSize(byPage);
		pgCategorie.addForward("onPaging", this, ApplicationEvents.ON_CATEGORIES);
		lstCategorie.setItemRenderer(new CaracteresRenderer());
		
		addEventListener(ApplicationEvents.ON_MODES, this);
		lstMode.setItemRenderer(new ModesRenderer());
		pgMode.setPageSize(byPageBandbox);
		pgMode.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODES);
		
		
		addEventListener(ApplicationEvents.ON_MODESSELECTIONS, this);
		lstModeSelection.setItemRenderer(new SelectionsRenderer());
		pgModeSelection.setPageSize(byPageBandbox);
		pgModeSelection.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODESSELECTIONS);
		

	}

	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		type = (Long) map.get(WINDOW_PARAM_TYPE);
		mode = (String) map.get(WINDOW_PARAM_MODE);
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		if(type.intValue()==UIConstants.PARAM_TMTRAVAUX)
			LibelleTab="REALTF";
		if(type.intValue()==UIConstants.PARAM_TMPI)
			LibelleTab="REALPI";
		if(type.intValue()==UIConstants.PARAM_TMSERVICES)
			LibelleTab="REALSERVICES";
		session.setAttribute("LibelleTab",LibelleTab);
		typemarche=BeanLocator.defaultLookup(TypesmarchesSession.class).findById(type);
		lblTypemarcheStep2.setValue(typemarche.getLibelle());
		lblTypemarche.setValue(typemarche.getLibelle());
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(WINDOW_PARAM_CODE);
			appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(code);
			modepassation=appel.getModepassation();
			bdMode.setValue(modepassation.getLibelle());
			modeselection=appel.getModeselection();
			bdModeSelection.setValue(modeselection.getLibelle());
			typemarche=appel.getTypemarche();
			realisation=appel.getRealisation();
			dtcreation.setValue(appel.getApodatecreation());
			txtObjets.setValue(appel.getApoobjet());
			dcmontant.setValue(appel.getApomontantestime());
			categorie=appel.getCategorie();
			txtReference.setValue(appel.getAporeference());
			realisation=BeanLocator.defaultLookup(RealisationsSession.class).findRealisation(appel.getAporeference());
			realisations=realisation;
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		Events.postEvent(ApplicationEvents.ON_CATEGORIES, this, null);
		Events.postEvent(ApplicationEvents.ON_MODESSELECTIONS, this, null);
		Events.postEvent(ApplicationEvents.ON_MODES, this, null);
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygRealisations> realisations = BeanLocator.defaultLookup(RealisationsSession.class).find(activePage,byPage,reference,UIConstants.PARAM_TMPI,idmode,null,UIConstants.NPARENT, Labels.getLabel("kermel.plansdepassation.statut.publier"),autorite, datedebut, datefin);
			 SimpleListModel listModel = new SimpleListModel(realisations);
			 lstListe.setModel(listModel);
			 pgPagination.setTotalSize(BeanLocator.defaultLookup( RealisationsSession.class).count(reference,UIConstants.PARAM_TMPI,idmode,null,UIConstants.NPARENT, Labels.getLabel("kermel.plansdepassation.statut.publier"),autorite, datedebut, datefin));
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CATEGORIES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgCategorie.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgCategorie.getActivePage() * byPage;
				pgCategorie.setPageSize(byPage);
			}
			 List<SygSecteursactivites> categories = BeanLocator.defaultLookup(SecteursactivitesSession.class).find(activePage,byPage,null,libelle,null);
			 SimpleListModel listModel = new SimpleListModel(categories);
			 lstCategorie.setModel(listModel);
			 pgCategorie.setTotalSize(BeanLocator.defaultLookup( SecteursactivitesSession.class).count(null,libelle,null));
		}
		else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODES)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgMode.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgMode.getActivePage() * byPageBandbox;
				pgMode.setPageSize(byPageBandbox);
			}
			List<SygTypesmarchesmodespassations> modes = BeanLocator.defaultLookup(TypesmarchesmodespassationsSession.class).find(activePage, byPageBandbox,libellemode,typemarche,null);
			lstMode.setModel(new SimpleListModel(modes));
			pgMode.setTotalSize(BeanLocator.defaultLookup(TypesmarchesmodespassationsSession.class).count(libellemode,typemarche,null));
		}
		
		else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODESSELECTIONS)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgModeSelection.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgModeSelection.getActivePage() * byPageBandbox;
				pgModeSelection.setPageSize(byPageBandbox);
			}
			List<SygTypesmarchesmodesselections> modes = BeanLocator.defaultLookup(TypesmarchesmodesselectionsSession.class).find(activePage, byPageBandbox,libellemodeselection,typemarche,null);
			lstModeSelection.setModel(new SimpleListModel(modes));
			pgModeSelection.setTotalSize(BeanLocator.defaultLookup(TypesmarchesmodesselectionsSession .class).count(libellemodeselection,typemarche,null));
		}
	}

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygRealisations realisations = (SygRealisations) data;
		item.setValue(realisations);

		 Listcell cellReference = new Listcell(realisations.getReference());
		 cellReference.setParent(item);
		 
		 Listcell cellLibelle = new Listcell(realisations.getLibelle());
		 cellLibelle.setParent(item);
		 
		 Listcell cellModepassation = new Listcell(realisations.getModepassation().getLibelle());
		 cellModepassation.setParent(item);
		 
		 Listcell cellDateLancement = new Listcell(UtilVue.getInstance().formateLaDate(realisations.getDatelancement()));
		 cellDateLancement.setParent(item);
		 
		 if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
		if(realisation.getIdrealisation().intValue()==realisations.getIdrealisation().intValue())
			item.setSelected(true);
		 }
		 
	}
	
    public class CaracteresRenderer implements ListitemRenderer{
		
		
		
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygSecteursactivites secteur = (SygSecteursactivites) data;
			item.setValue(secteur);

			 Listcell cellLibelle = new Listcell("");
			 if((secteur.getCode().length()/3)==1)
				 cellLibelle.setLabel("---"+secteur.getLibelle());
			 else  if((secteur.getCode().length()/3)==2)
				 cellLibelle.setLabel("------"+secteur.getLibelle());
			 else  if((secteur.getCode().length()/3)==3)
				 cellLibelle.setLabel("---------"+secteur.getLibelle());
			 else  if((secteur.getCode().length()/3)==4)
				 cellLibelle.setLabel("------------"+secteur.getLibelle());
			 else  if((secteur.getCode().length()/3)==4)
				 cellLibelle.setLabel("---------------"+secteur.getLibelle());
			 else  if((secteur.getCode().length()/3)==6)
				 cellLibelle.setLabel("------------------"+secteur.getLibelle());
			 else  if((secteur.getCode().length()/3)==7)
				 cellLibelle.setLabel("---------------------"+secteur.getLibelle());
			 else  if((secteur.getCode().length()/3)==8)
				 cellLibelle.setLabel("------------------------"+secteur.getLibelle());
			 else  if((secteur.getCode().length()/3)==9)
				 cellLibelle.setLabel("---------------------------"+secteur.getLibelle());
			 else  if((secteur.getCode().length()/3)==10)
				 cellLibelle.setLabel("------------------------------"+secteur.getLibelle());
			 else  cellLibelle.setLabel(secteur.getLibelle());
			 cellLibelle.setParent(item);
			 
//			 if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
//			 if(secteur.getCode().equals(categorie.getCode()))
//					item.setSelected(true);
//			 }
		}
		}

	public void onClick$menuNextStep0()
	{
		if (lstListe.getSelectedItem() == null)
			
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		realisation=(SygRealisations) lstListe.getSelectedItem().getValue();
		lblRealisations.setValue(realisation.getLibelle());
		lblRealisationsStep2.setValue(realisation.getLibelle());
		txtReference.setValue(realisation.getReference());
		txtReferences.setValue(realisation.getReference());
		modepassation=realisation.getModepassation();
		bdMode.setValue(modepassation.getLibelle());
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
	}
	
	public void onClick$menuCancelStep0()
	{
		detach();
	}
	
	public void onClick$menuPreviousStep1()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
	}
	
	public void onClick$menuNextStep1()
	{
      if (lstCategorie.getSelectedItem() == null)
			
			throw new WrongValueException(lstCategorie, Labels.getLabel("kermel.error.select.item"));
           categorie=(SygCategori) lstCategorie.getSelectedItem().getValue();
           lblCategories.setValue(categorie.getLibelle());
           step0.setVisible(false);
   		   step1.setVisible(false);
   		   step2.setVisible(true);
	}
	
	public void onClick$menuCancelStep1()
	{
		detach();
	}
	
	public void onClick$menuPreviousStep2()
	{
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
	}
	
	public void onClick$menuCancelStep2()
	{
		detach();
	}
	

	
///////////Mode de passation///////// 
		public void onSelect$lstMode(){
		modepassation= (SygModepassation) lstMode.getSelectedItem().getValue();
		bdMode.setValue(modepassation.getLibelle());
		bdMode.close();
		
		}
		
		public class ModesRenderer implements ListitemRenderer{
		
		
		
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypesmarchesmodespassations modes = (SygTypesmarchesmodespassations) data;
			item.setValue(modes.getMode());
			
			
			Listcell cellLibelle = new Listcell("");
			if (modes.getMode().getLibelle()!=null){
				cellLibelle.setLabel(modes.getMode().getLibelle());
			}
			cellLibelle.setParent(item);
		
		}
		}
		public void onFocus$txtRechercherMode(){
		if(txtRechercherMode.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.modepassation"))){
			txtRechercherMode.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherMode(){
		if(txtRechercherMode.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.modepassation")) || txtRechercherMode.getValue().equals("")){
			libellemode = null;
			page=null;
		}else{
			libellemode = txtRechercherMode.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_MODES, this, page);
		}
		
		
///////////Mode de passation///////// 
	public void onSelect$lstModeSelection(){
	modeselection= (SygModeselection) lstModeSelection.getSelectedItem().getValue();
	bdModeSelection.setValue(modeselection.getLibelle());
	bdModeSelection.close();
	
	}
	
	public class SelectionsRenderer implements ListitemRenderer{
	
	
	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygTypesmarchesmodesselections modes = (SygTypesmarchesmodesselections) data;
		item.setValue(modes.getMode());
		
		
		Listcell cellLibelle = new Listcell("");
		if (modes.getMode().getLibelle()!=null){
			cellLibelle.setLabel(modes.getMode().getLibelle());
		}
		cellLibelle.setParent(item);
	
	}
	}
	public void onFocus$txtRechercherModeSelection(){
	if(txtRechercherModeSelection.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.modeselection"))){
		txtRechercherModeSelection.setValue("");
	}		 
	}
	
	public void  onClick$btnRechercherModeSelection(){
	if(txtRechercherModeSelection.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.modeselection")) || txtRechercherModeSelection.getValue().equals("")){
		libellemodeselection = null;
		page=null;
	}else{
		libellemodeselection = txtRechercherModeSelection.getValue();
		page="0";
	}
	Events.postEvent(ApplicationEvents.ON_MODESSELECTIONS, this, page);
	}
	
private boolean checkFieldConstraints() {
		
		try {
			if(dtcreation.getValue()==null)
		     {
              errorComponent = dtcreation;
              errorMsg = Labels.getLabel("kermel.plansdepassation.passationsmarches.datecreation")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((dtcreation.getValue()).after(new Date()))
			 {
				errorComponent = dtcreation;
				errorMsg =Labels.getLabel("kermel.plansdepassation.passationsmarches.datecreation")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.referentiel.date.dujour")+": "+UtilVue.getInstance().formateLaDate(new Date());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			if(modeselection==null)
		     {
               errorComponent = bdModeSelection;
               errorMsg = Labels.getLabel("kermel.referentiel.modeselection")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(txtReferences.getValue().equals(""))
		     {
              errorComponent = txtReferences;
              errorMsg = Labels.getLabel("kermel.referentiel.courrier.reference")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(txtObjets.getValue().equals(""))
		     {
             errorComponent = txtObjets;
             errorMsg = Labels.getLabel("kermel.plansdepassation.passationsmarches.objets")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dcmontant.getValue()==null)
		     {
            errorComponent = dcmontant;
            errorMsg = Labels.getLabel("kermel.plansdepassation.passationsmarches.montantestime")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

	public void onClick$menuValider()
		{
			if(checkFieldConstraints())
			{
				appel.setModepassation(modepassation);
				appel.setModeselection(modeselection);
				appel.setTypemarche(typemarche);
				appel.setRealisation(realisation);
				appel.setApodatecreation(dtcreation.getValue());
				appel.setApoobjet(txtObjets.getValue());
				appel.setApomontantestime(dcmontant.getValue());
				appel.setCategorie(categorie);
				appel.setAporeference(txtReference.getValue());
				appel.setAutorite(autorite);
				if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
					
				BeanLocator.defaultLookup(AppelsOffresSession.class).save(appel);
				}
				else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
					BeanLocator.defaultLookup(AppelsOffresSession.class).update(appel);
					if(realisations!=realisation)
					{
						realisation.setAppel(UIConstants.NPARENT);
						BeanLocator.defaultLookup(RealisationsSession.class).update(realisation);
					}
				}
				realisation.setAppel(UIConstants.PARENT);
				BeanLocator.defaultLookup(RealisationsSession.class).update(realisation);
				loadApplicationState("procedures_passations");
				detach();
			}
		}
	
	
	public void  onClick$btnchercher(){
		if((References.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.reference")))||(References.getValue().equals("")))
		 {
			reference=null;
		 }
		else
		 {
			reference=References.getValue();
			page="0";
		 }
		if(dtdatedebut.getValue()==null)
		 {
			datedebut=null;
			
		 }
		else
		 {
			datedebut=dtdatedebut.getValue();
			page="0";
		 }
		if(dtdatefin.getValue()==null)
		 {
			datefin=null;
			
		 }
		else
		 {
			datefin=dtdatefin.getValue();
			page="0";
		 }
//		if( bdMode.getValue().equals("")){
//			mode= null;
//			page=null;
//		}else{
//			mode = modepassation.getId();
//			page="0";
//		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
	
	 public void onFocus$References(){
			if(References.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.reference"))){
				References.setValue("");
			}		 
			}
		public void onBlur$References(){
			if(References.getValue().equalsIgnoreCase("")){
				References.setValue(Labels.getLabel("kermel.referentiel.modepassation"));
			}		 
			}
		public void onOK$References(){
			onClick$btnchercher();	
		}
		public void onOK$btnchercher(){
			onClick$btnchercher();	
		}
	
}