package sn.ssi.kermel.web.procedurederogataire.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygTypeDemande;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.plansdepassation.ejb.PlansdepassationSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeDemandeSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;
import sn.ssi.kermel.web.referentiel.controllers.AutoriteContractanteFormController;

@SuppressWarnings("serial")
public class ListdemandeencoursController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,list;
	private Paging pgProcedure,pg;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtNumero,txtNom,txtFonction,txtService;
    String numero=null,prenom=null,reference,callBack,fonction=null,service=null,page=null,sygmlibelle;
    private Listheader lshNumero,lshAnnee,lshDatecreation,lshDatemev;
    Session session = getHttpSession();
    String login,status;
    private Datebox dtdebut,dtfin;
    private Date datedebut,datefin;
    private KermelSousMenu monSousMenu;
    private Menuitem WSOUMETTRE_PROCEDUREDEROGATOIRE;
    List<SygRealisations> realisations = new ArrayList<SygRealisations>();
    SygPlansdepassation plan=new SygPlansdepassation();
    SygTypeDemande demande=new SygTypeDemande();
    private String type;
    private Utilisateur infoscompte;
    private SygAutoriteContractante autorite;
    private Div step0,step1,step2;
    private Label entete4,lblInfos;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
    	monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.PROCEDUREDEROGATOIRE);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		
		if (WSOUMETTRE_PROCEDUREDEROGATOIRE != null)
			WSOUMETTRE_PROCEDUREDEROGATOIRE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_SOUMETTRE);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_TYPES, this);
		addEventListener(ApplicationEvents.ON_SOUMETTRE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
 
		lstListe.setItemRenderer(this);

		pgProcedure.setPageSize(byPage);

		lshNumero.setSortAscending(new FieldComparator("Numplan", false));
		lshNumero.setSortDescending(new FieldComparator("Numplan", true));
		lshAnnee.setSortAscending(new FieldComparator("annee", false));
		lshAnnee.setSortDescending(new FieldComparator("annee", true));
		lshDatecreation.setSortAscending(new FieldComparator("datecreation", false));
		lshDatecreation.setSortDescending(new FieldComparator("datecreation", true));
		lshDatemev.setSortAscending(new FieldComparator("dateMiseEnValidation", false));
		lshDatemev.setSortDescending(new FieldComparator("dateMiseEnValidation", true));
				
				
		lstListe.setItemRenderer(this);
		pgProcedure.setPageSize(byPage);
		pgProcedure.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
     	login = ((String) getHttpSession().getAttribute("user"));
     	
     	list.setItemRenderer(new TypedemandeRenderer());
     	pg.setPageSize(byPage);
     	pg.addForward("onPaging", this, ApplicationEvents.ON_TYPES);
     	login = ((String) getHttpSession().getAttribute("user"));
	}

	@SuppressWarnings("unchecked")
	public void onCreate(final CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if((infoscompte.getType().equals(UIConstants.USERS_TYPES_AC)))
		{
			autorite=infoscompte.getAutorite();
		}
		else
		{
			autorite=null;
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		Events.postEvent(ApplicationEvents.ON_TYPES, this, null);
	}
		@SuppressWarnings("unchecked")
		@Override
		public void onEvent(final Event event) throws Exception {

			if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_TYPES)) {
				if (event.getData() != null) {
					activePage = Integer.parseInt((String) event.getData());
					byPage = -1;
					pg.setPageSize(1000);
				} else {
					byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
					activePage = pg.getActivePage() * byPage;
					pg.setPageSize(byPage);
				}
				 List<SygTypeDemande> demande = BeanLocator.defaultLookup(TypeDemandeSession.class).find(pg.getActivePage()*byPage,byPage,sygmlibelle,null);
				 SimpleListModel listModel = new SimpleListModel(demande);
				 list.setModel(listModel);
				 pg.setTotalSize(BeanLocator.defaultLookup(TypeDemandeSession.class).count(null));
				 
			}
			if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
				if (event.getData() != null) {
					activePage = Integer.parseInt((String) event.getData());
					byPage = -1;
					pgProcedure.setPageSize(1000);
				} else {
					byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
					activePage = pgProcedure.getActivePage() * byPage;
					pgProcedure.setPageSize(byPage);
				}
			 List<SygPlansdepassation> plans = BeanLocator.defaultLookup(PlansdepassationSession.class).find(activePage,byPage,numero,datedebut,datefin,autorite, -1);
			 SimpleListModel listModel = new SimpleListModel(plans);
			 lstListe.setModel(listModel);
			 pgProcedure.setTotalSize(BeanLocator.defaultLookup(PlansdepassationSession.class).count(numero,datedebut,datefin,autorite, -1));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/plansdepassation/formprocedures.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.nouveau"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ProceduresDerogatoiresFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display); 
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DETAILS)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			Long codes=(Long) lstListe .getSelectedItem().getValue();
			plan=BeanLocator.defaultLookup(PlansdepassationSession.class).findById(codes);
			realisations=BeanLocator.defaultLookup(RealisationsSession.class).find(0,-1,null,null,plan, null);
			if(realisations.size()>0)
			{
				final String uri = "/plansdepassation/formsousmissionvalidation.zul";

				final HashMap<String, String> display = new HashMap<String, String>();
				display.put(DSP_HEIGHT,"650px");
				display.put(DSP_WIDTH, "80%");
				display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.soumettre.validation"));

				final HashMap<String, Object> data = new HashMap<String, Object>();
				data.put(AutoriteContractanteFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
				data.put(AutoriteContractanteFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

				showPopupWindow(uri, data, display);
			}
			else
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.realisation.saisir"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_REALISATIONS)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			final String uri = "/procedurederogatoire/formvalidationprocedures.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"650px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.validation"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ValidationProceduresDerogatoiresFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(ValidationProceduresDerogatoiresFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_PUBLIER)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			final String uri = "/procedurederogatoire/formprocedures.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"650px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.validation"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ProceduresDerogatoiresFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(ProceduresDerogatoiresFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
				
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				
				Date datevalidation=(Date) lstListe.getSelectedItem().getAttribute("datevalidation");
				if(datevalidation==null)
				{
					HashMap<String, String> display = new HashMap<String, String>(); // permet
					display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
					display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
					display.put(MessageBoxController.DSP_HEIGHT, "250px");
					display.put(MessageBoxController.DSP_WIDTH, "47%");
	
					HashMap<String, Object> map = new HashMap<String, Object>(); // permet
					map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
					showMessageBox(display, map);
				}
				else
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.suppressionplan"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				}
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(PlansdepassationSession.class).delete(codes);
				BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_PLANPASSATION", Labels.getLabel("kermel.plansdepassation.suppressionn")+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_SOUMETTRE)) {
				if (lstListe.getSelectedItem() == null)
				  throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				
				session.setAttribute("idplan", lstListe.getSelectedItem().getValue());
				loadApplicationState("realisations");
			}
	
	}
	public void onSelect$lstListe(){
		status=(String) lstListe.getSelectedItem().getAttribute("status");
	
	}
	public void onClick$menuNext(){
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		step0.setVisible(false);
		step2.setVisible(true);
		step1.setVisible(false);
		plan=(SygPlansdepassation) lstListe.getSelectedItem().getValue();
		lblInfos.setValue(plan.getNumplan()+" "+(plan.getAnnee()));
		

		Events.postEvent(ApplicationEvents.ON_REALISATIONS, this, null);
		
		 Include inc = (Include) this.getFellowIfAny("incREALTRAV");
	      inc.setSrc("/procedurederogatoire/listrealisationstravaux.zul");

	}
	public void onClick$menuPrevious(){
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		step2.setVisible(false);
		step0.setVisible(true);
		step1.setVisible(false);
		Events.postEvent(ApplicationEvents.ON_REALISATIONS, this, null);

	}
	public void onClick$menuPrevious1(){
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		step1.setVisible(false);
		step2.setVisible(true);
		step0.setVisible(false);
		Events.postEvent(ApplicationEvents.ON_REALISATIONS, this, null);

	}
	public void onClick$menuPrevious2(){
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		step1.setVisible(false);
		step2.setVisible(false);
		step0.setVisible(true);
		Events.postEvent(ApplicationEvents.ON_REALISATIONS, this, null);

	}
	
	public void onClick$menuNext1(){
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		step0.setVisible(false);
		step2.setVisible(false);
		step1.setVisible(true);
	   demande=(SygTypeDemande) lstListe.getSelectedItem().getValue();
	   entete4.setValue(demande.getLibelle());
	Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);

	}
	public void onClick$menuNext2(){
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		step0.setVisible(false);
		step2.setVisible(false);
		step1.setVisible(true);
		Events.postEvent(ApplicationEvents.ON_REALISATIONS, this, null);

	}
	public void onClick$menuFermer()
	{
		detach();
	}
	
	 public class TypedemandeRenderer implements ListitemRenderer{
			
			
			
			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
				SygTypeDemande demande = (SygTypeDemande) data;
				item.setValue(demande.getId());
				item.setAttribute("demande", demande.getLibelle());
				
				Listcell celllibelle = new Listcell(demande.getLibelle());
				celllibelle.setParent(item);
				Listcell celldescription = new Listcell(demande.getDescription());
				celldescription.setParent(item);
			}
			public void onSelect$lstListe(){
				session.setAttribute("demande", lstListe.getSelectedItem().getAttribute("demande").toString());
			}
		}
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygPlansdepassation plans = (SygPlansdepassation) data;
		item.setValue(plans);
		item.setAttribute("status", plans.getStatus());
		
		 Listcell cellNumero = new Listcell(plans.getNumplan());
		 cellNumero.setParent(item);
		 
		 Listcell cellAnnee = new Listcell(Integer.toString(plans.getAnnee()));
		 cellAnnee.setParent(item);
		 
		 Listcell cellDatecreation = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDatecreation()));
		 cellDatecreation.setParent(item);
		 
		 Listcell cellDatemiseenvalidation = new Listcell("");
		  if(plans.getDateMiseEnValidation()!=null)
		  {
			  cellDatemiseenvalidation.setLabel(UtilVue.getInstance().formateLaDate(plans.getDateMiseEnValidation()));
			  item.setAttribute("datevalidation", plans.getDateMiseEnValidation());
		  }
		  else
		  {
			  item.setAttribute("datevalidation", null);
		  }
		 cellDatemiseenvalidation.setParent(item);
		 
		 Listcell cellStatut = new Listcell(plans.getStatus());
		   if(plans.getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.saisie")))
			   cellStatut.setLabel(Labels.getLabel("kermel.plansdepassation.statut.saisies"));
		   else  if(plans.getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.miseenvalidation")))
			   cellStatut.setLabel(Labels.getLabel("kermel.plansdepassation.statut.miseenvalidations"));
		   else  if(plans.getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.valider")))
			   cellStatut.setLabel(Labels.getLabel("kermel.plansdepassation.statut.validers"));
		   else  if(plans.getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.rejeter")))
			   cellStatut.setLabel(Labels.getLabel("kermel.plansdepassation.statut.rejeters"));
		   else  if(plans.getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.publier")))
			   cellStatut.setLabel(Labels.getLabel("kermel.plansdepassation.statut.publiers"));
	   	 cellStatut.setParent(item);
		
		 Listcell cellCommentaires = new Listcell(plans.getCommentaires());
		 cellCommentaires.setParent(item);

	}
	public void onClick$bchercher()
	{
		if((txtNumero.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.num")))||(txtNumero.getValue().equals("")))
		 {
			numero=null;
			
		 }
		else
		 {
			numero=txtNumero.getValue();
			page="0";
		 }
		if(dtdebut.getValue()==null)
		 {
			datedebut=null;
			
		 }
		else
		 {
			datedebut=dtdebut.getValue();
			page="0";
		 }
		if(dtfin.getValue()==null)
		 {
			datefin=null;
		
		 }
		else
		 {
			datefin=dtfin.getValue();
			page="0";
		 }
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	
	public void onFocus$txtNumero()
	{
		if(txtNumero.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.num")))
			txtNumero.setValue("");
		
	}
	public void onBlur$txtNumero()
	{
		if(txtNumero.getValue().equals(""))
			txtNumero.setValue(Labels.getLabel("kermel.plansdepassation.num"));
	}
	
	public void onOK$txtNumero()
	{
		onClick$bchercher();
	}
	
}