package sn.ssi.kermel.web.procedurederogataire.controllers;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.proceduresderogatoires.ejb.ProcedurederogatoireSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



@SuppressWarnings("serial")
public class ListDemandeEnCourController extends AbstractWindow implements  AfterCompose {

	public static final String CURRENT_MODULE="CURRENT_MODULE";
	Long code;
	Session session = getHttpSession();
	public String extension,images,mystate,LibelleTab;
	private Tab TAB_REALTRAVD,TAB_REALSFOURND,TAB_REALSSERVD,TAB_REALSPID;
	
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		 mystate=(String)Executions.getCurrent().getAttribute("mystate");
		 session.setAttribute("statedemande", mystate);
		 
		 TAB_REALTRAVD.setLabel(Labels.getLabel("kermel.plansdepassation.realisation.type.travaux")+" ("+BeanLocator.defaultLookup( ProcedurederogatoireSession.class).countDemandes(UIConstants.PARAM_TMTRAVAUX)+")");
		 TAB_REALSFOURND.setLabel(Labels.getLabel("kermel.plansdepassation.realisation.type.fournitures")+" ("+BeanLocator.defaultLookup( ProcedurederogatoireSession.class).countDemandes(UIConstants.PARAM_TMFOURNITURES)+")");
		 TAB_REALSSERVD.setLabel(Labels.getLabel("kermel.plansdepassation.realisation.type.pi")+" ("+BeanLocator.defaultLookup( ProcedurederogatoireSession.class).countDemandes(UIConstants.PARAM_TMPI)+")");
		 TAB_REALSPID.setLabel(Labels.getLabel("kermel.plansdepassation.realisation.type.services")+" ("+BeanLocator.defaultLookup( ProcedurederogatoireSession.class).countDemandes(UIConstants.PARAM_TMSERVICES)+")");
		
		 LibelleTab = (String) session.getAttribute("LibelleTab");
		    if(LibelleTab!=null)
		    {
		    	 if(LibelleTab.equals("REALTF"))
				  {
		    		 TAB_REALTRAVD.setSelected(true);
					Include inc = (Include) this.getFellowIfAny("incREALTRAVD");
			        inc.setSrc("/procedurederogatoire/listdemandeencourtravaux.zul");	
				  }
				else
				{
					 if(LibelleTab.equals("REALPI"))
					  {
						 TAB_REALSPID.setSelected(true);
						Include inc = (Include) this.getFellowIfAny("incREALSPID");
				        inc.setSrc("/procedurederogatoire/listrealisationspi.zul");	
					  }
					else
					{
						if(LibelleTab.equals("REALSERVICES"))
						  {
							TAB_REALSSERVD.setSelected(true);
							Include inc = (Include) this.getFellowIfAny("incREALSSERVD");
					        inc.setSrc("/procedurederogatoire/listrealisationsserv.zul");	
						  }
						else
						{
							if(LibelleTab.equals("REALSFOURN"))
							  {
								TAB_REALSFOURND.setSelected(true);
								Include inc = (Include) this.getFellowIfAny("incREALSFOURND");
						        inc.setSrc("/procedurederogatoire/listdemandeencourfourniture.zul");	
							  }
							else
							{
								Include inc = (Include) this.getFellowIfAny("incREALTRAVD");
							    inc.setSrc("/procedurederogatoire/listdemandeencourtravaux.zul");
							}
						}
					}
				}
		    }
		    else
		    {
		    	Include inc = (Include) this.getFellowIfAny("incREALTRAVD");
			    inc.setSrc("/procedurederogatoire/listdemandeencourtravaux.zul");	
		    }
		   
	
	}



}
