package sn.ssi.kermel.web.fichdossiertype.controllers;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.dossiers.ejb.DossierTypeSessions;
import sn.ssi.kermel.be.dossiertype.ejb.FichDossierTypeSession;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiersTypes;
import sn.ssi.kermel.be.entity.SygFichDossierType;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.referentiel.ejb.PaysSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;




@SuppressWarnings("serial")
public class FichDossierTypeFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,annee,gestion,numavi,page=null;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtRechercherDossiertype,txtlibellefichierdossiertype,txtpublier,txtVersionElectronique;
	private SygFichDossierType fichdossiertype =new SygFichDossierType();
	private SygDossiersTypes DossiersTypes;
	private SygAutoriteContractante autorite;
	private SygRealisations realisation;
	private SimpleListModel lst;
	private final String ON_LOCALITE = "onLocalite";
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	Long code;
	private Paging pgdossier,pgfiche;
	private Listbox list;
	private String nomFichier,libelledossiers;
	private final String cheminDossier = UIConstants.PATH_PJ;
	private Bandbox bandDossiertype;
	private Component errorcomponent;
	private String errorMessage;
	private Label lbStatusBar;
	private ArrayList<String> listValeursAnnees;
	UtilVue utilVue = UtilVue.getInstance();
	private int activePage;
	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		
//		annee = UtilVue.getInstance().anneecourant(new Date());
//		listValeursAnnees = new ArrayList<String>();
//		for (int i = 2007; i <= Integer.parseInt(annee) ; i++) {
//			listValeursAnnees.add(i+"");
//		}
//		list.setModel(new SimpleListModel(listValeursAnnees));
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(WINDOW_PARAM_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WIDOW_CODE);
			fichdossiertype = BeanLocator.defaultLookup(FichDossierTypeSession.class).findById(code);
			
			DossiersTypes = BeanLocator.defaultLookup(DossierTypeSessions.class).findById(fichdossiertype.getDossiertype_ID().getIddossiers());
			bandDossiertype.setValue(fichdossiertype.getLibellefichierdossiertype());

			txtlibellefichierdossiertype.setValue(fichdossiertype.getLibellefichierdossiertype());
			txtpublier.setValue(fichdossiertype.getPublier());
			txtVersionElectronique.setValue(fichdossiertype.getNomfichierdossiertype());
			
			/*
			 * recuperation de la date pour lors de la modification
			 * */
//				for (int i = 0; i < list.getModel().getSize(); i++) {
//					String codes = (String) list.getListModel().getElementAt(i);
//					System.out.println(codes);
//					if(gestion.equalsIgnoreCase(codes)){
//					((Listitem)list.getItemAtIndex(i)).setSelected(true);	
//					cbannee.setValue(codes);
//					}
//			}
			
		}
			
//			Utilisateur		utilisateur = (Utilisateur) getHttpSession().getAttribute("utilisateur");
//			autorite = BeanLocator.defaultLookup(AutoriteContractanteSession.class).findById(utilisateur.getAutorite().getId());
//			
	}
	public void onOK() {
		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW))
			fichdossiertype=new SygFichDossierType();
		    fichdossiertype.setLibellefichierdossiertype(txtRechercherDossiertype.getValue());
		
		    fichdossiertype.setPublier("non");
		    fichdossiertype.setNomfichierdossiertype(txtVersionElectronique.getValue());
		   // fichdossiertype.setAutorite(autorite);
			
			if (list.getSelectedItem() == null) {
				errorMessage = "Veuillez selectionner un Pays!";
				errorcomponent = bandDossiertype;
				throw new WrongValueException(errorcomponent, errorMessage);
	
			}
			fichdossiertype.setDossiertype_ID(((SygFichDossierType) list.getSelectedItem().getValue()).getDossiertype_ID());
			
		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			BeanLocator.defaultLookup(FichDossierTypeSession.class).save(fichdossiertype);

		} 
		else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			BeanLocator.defaultLookup(FichDossierTypeSession.class).update(fichdossiertype);
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();

	}
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		list.setItemRenderer(this);
		pgdossier.setPageSize(byPage);
		pgdossier.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	
	}
//	@Override
//	public void render(Listitem arg0, Object arg1) throws Exception {
//		// TODO Auto-generated method stub
//		
//	}
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		// TODO Auto-generated method stub
		SygDossiersTypes DossiersTypes = (SygDossiersTypes) data;
		item.setValue(DossiersTypes);
		item.setAttribute("libelle", DossiersTypes.getLibelledossiers());
		addCell(item, DossiersTypes.getLibelledossiers());
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			if (DossiersTypes.getIddossiers() == this.DossiersTypes.getIddossiers())
				item.setSelected(true);
		}

}
	public void onSelect$list() {
		bandDossiertype.setValue(list.getSelectedItem().getAttribute("libelledossiers").toString());
		bandDossiertype.close();
	}

	public void onClick$btnRechercherDossiertype() {
		if (!txtRechercherDossiertype.getValue().equals("") && !txtRechercherDossiertype.getValue().equals("libelledossiers")) {
			libelledossiers = txtRechercherDossiertype.getValue();
		} else {
			libelledossiers = null;
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	public void onOK$txtRechercherDossiertype() {
		onClick$btnRechercherDossiertype();
	}

	public void onFocus$txtRechercherDossiertype() {
		txtRechercherDossiertype.setValue("");
	}
//	@Override
//	public void onEvent(Event arg0) throws Exception {
//		// TODO Auto-generated method stub
//		
//	}
	@Override
	public void onEvent(Event event) throws Exception {
		// TODO Auto-generated method stub
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgdossier.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgdossier.getActivePage() * byPage;
				pgdossier.setPageSize(byPage);
			}
			List<SygDossiersTypes> DossiersTypes = BeanLocator.defaultLookup(DossierTypeSessions.class).find(activePage, byPage, libelledossiers,null);
			list.setModel(new SimpleListModel(DossiersTypes));
			pgdossier.setTotalSize(BeanLocator.defaultLookup(PaysSession.class).count(libelledossiers,null));
		}

	}
   public void onClick$btnChoixFichier() {
	//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
		if (ToolKermel.isWindows())
			nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
		else
			nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

		txtVersionElectronique.setValue(nomFichier);
	}
   
//   	public void onSelect$list(){
//   		cbannee.setValue(list.getSelectedItem().getValue().toString());
//   		numavi="AG_"+autorite.getSigle()+" _ "+list.getSelectedItem().getValue().toString();
//   		gestion=list.getSelectedItem().getValue().toString();
//   		txtnumero.setValue(numavi);
//   		cbannee.close();
//   	}
}