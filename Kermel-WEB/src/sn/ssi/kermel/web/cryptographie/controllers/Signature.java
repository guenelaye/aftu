package sn.ssi.kermel.web.cryptographie.controllers;


import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.PrivateKey;
import java.security.cert.CertStore;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import org.bouncycastle.cms.CMSProcessable;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.ext.AfterCompose;

import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
public class Signature extends AbstractWindow implements  AfterCompose{
	X509Certificate cert = null;
	PrivateKey privatekey = null;
	private final String cheminDossier = UIConstants.PATH_PJ_CRIP;
	private String nomfichier,filepath;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		nomfichier="test.txt";
		filepath = cheminDossier +  nomfichier;
		ChargerCertificat(filepath);
	}

	public void  ChargerCertificat(String filepath) {
		try {
			// Chargement du fichier qui va �tre sign�

			File file_to_sign = new File(filepath);
			byte[] buffer = new byte[(int)file_to_sign.length()];
			DataInputStream in = new DataInputStream(new FileInputStream(file_to_sign));
			in.readFully(buffer);
			in.close();

			// Chargement des certificats qui seront stock�s dans le fichier .p7
			// Ici, seulement le certificat personnal_nyal.cer sera associ�.
			// Par contre, la cha�ne des certificats non.

			ArrayList certList = new ArrayList();
			certList.add(cert);
			CertStore certs = CertStore.getInstance("Collection", new CollectionCertStoreParameters(certList), "BC");

			CMSSignedDataGenerator signGen = new CMSSignedDataGenerator();

			// privatekey correspond � notre cl� priv�e r�cup�r�e du fichier PKCS#12
			// cert correspond au certificat publique personnal_nyal.cer
			// Le dernier argument est l'algorithme de hachage qui sera utilis�

			signGen.addSigner(privatekey, cert, CMSSignedDataGenerator.DIGEST_SHA1);
			signGen.addCertificatesAndCRLs(certs);
			CMSProcessable content = new CMSProcessableByteArray(buffer);

			// Generation du fichier CMS/PKCS#7
			// L'argument deux permet de signifier si le document doit �tre attach� avec la signature
			//     Valeur true:  le fichier est attach� (c'est le cas ici)
			//     Valeur false: le fichier est d�tach�

			CMSSignedData signedData = signGen.generate(content, true, "BC");
			byte[] signeddata = signedData.getEncoded();

			// Ecriture du buffer dans un fichier.	

			FileOutputStream envfos = new FileOutputStream(file_to_sign.getName() + ".pk7");
			envfos.write(signeddata);
			envfos.close();
		} catch (Exception e) {
			e.printStackTrace();
			return ;
		}
	}



		
}