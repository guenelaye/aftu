package sn.ssi.kermel.web.cryptographie.controllers;


import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.CertSelector;
import java.security.cert.CertStore;
import java.security.cert.CertificateFactory;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.X509Certificate;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.cms.CMSEnvelopedData;
import org.bouncycastle.cms.CMSEnvelopedDataGenerator;
import org.bouncycastle.cms.CMSProcessable;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.KeyTransRecipientInformation;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Hex;
import org.omg.CosCollection.Collection;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.ext.AfterCompose;

import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


public class Cryptographie extends AbstractWindow implements  AfterCompose{
	private final String cheminFichier = UIConstants.PATH_PJ_CRIP;
	X509Certificate cert = null;
	PrivateKey privatekey = null;
	PublicKey publickey = null;
	private String fichiersigne;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Recuperation_Cles();
		ChargerUnCertificat();
		SignatureDocument();
		VerificationSignatureDocument();
		ChiffrementDocument();
		Empreinte ();
	}
	public void  Recuperation_Cles() {
		// CHARGEMENT DU FICHIER PKCS#12

		KeyStore ks = null;
		char[] password = null;

		Security.addProvider(new BouncyCastleProvider());
		try {
			ks = KeyStore.getInstance("PKCS12");
			// Password pour le fichier personnal_nyal.p12
			password = "2[$0wUOS".toCharArray();
			ks.load(new FileInputStream(cheminFichier+"personnal_nyal.p12"), password);
		} catch (Exception e) {
			System.out.println("Erreur: fichier " +
		                       "personnal_nyal.p12" +
		                       " n'est pas un fichier pkcs#12 valide ou passphrase incorrect");
			return ;
		}

		// RECUPERATION DU COUPLE CLE PRIVEE/PUBLIQUE ET DU CERTIFICAT PUBLIQUE

	

		try {
			Enumeration en = ks.aliases();
			String ALIAS = "";
			Vector vectaliases = new Vector();

			while (en.hasMoreElements())
				vectaliases.add(en.nextElement());
			String[] aliases = (String []) (vectaliases.toArray(new String[0]));
			for (int i = 0; i < aliases.length; i++)
				if (ks.isKeyEntry(aliases[i]))
				{
					ALIAS = aliases[i];
					break;
				}
			privatekey = (PrivateKey)ks.getKey(ALIAS, password);
			cert = (X509Certificate)ks.getCertificate(ALIAS);
			publickey = ks.getCertificate(ALIAS).getPublicKey();
		} catch (Exception e) {
			e.printStackTrace();
			return ;
		}
	}

	public void  ChargerUnCertificat() {
		try {
			// Chargement du certificat � partir du fichier

			InputStream inStream = new FileInputStream(cheminFichier+"personnal_nyal.cer");
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			X509Certificate cert = (X509Certificate)cf.generateCertificate(inStream);
			inStream.close();

			// Affiche le contenu du certificat

			System.out.println(cert.toString());
		} catch (Exception e) {
			e.printStackTrace();
			return ;
		}
	}
	
	public void  SignatureDocument() {
		try {
			// Chargement du fichier qui va �tre sign�

			File file_to_sign = new File(cheminFichier+"test.txt");
			byte[] buffer = new byte[(int)file_to_sign.length()];
			DataInputStream in = new DataInputStream(new FileInputStream(file_to_sign));
			in.readFully(buffer);
			in.close();

			// Chargement des certificats qui seront stock�s dans le fichier .p7
			// Ici, seulement le certificat personnal_nyal.cer sera associ�.
			// Par contre, la cha�ne des certificats non.

			ArrayList certList = new ArrayList();
			certList.add(cert);
			CertStore certs = CertStore.getInstance("Collection", new CollectionCertStoreParameters(certList), "BC");

			CMSSignedDataGenerator signGen = new CMSSignedDataGenerator();

			// privatekey correspond � notre cl� priv�e r�cup�r�e du fichier PKCS#12
			// cert correspond au certificat publique personnal_nyal.cer
			// Le dernier argument est l'algorithme de hachage qui sera utilis�

			signGen.addSigner(privatekey, cert, CMSSignedDataGenerator.DIGEST_SHA1);
			signGen.addCertificatesAndCRLs(certs);
			CMSProcessable content = new CMSProcessableByteArray(buffer);

			// Generation du fichier CMS/PKCS#7
			// L'argument deux permet de signifier si le document doit �tre attach� avec la signature
			//     Valeur true:  le fichier est attach� (c'est le cas ici)
			//     Valeur false: le fichier est d�tach�

			CMSSignedData signedData = signGen.generate(content, true, "BC");
			byte[] signeddata = signedData.getEncoded();

			// Ecriture du buffer dans un fichier.	

			FileOutputStream envfos = new FileOutputStream(cheminFichier+file_to_sign.getName() + ".pk7");
			envfos.write(signeddata);
			envfos.close();
			fichiersigne=file_to_sign.getName() + ".pk7";
		} catch (Exception e) {
			e.printStackTrace();
			return ;
		}
	}
	
	public void  VerificationSignatureDocument() {
		try {
			// Chargement du fichier sign�
			File f = new File("fichier_signer.txt.pk7");
			byte[] buffer = new byte[(int)f.length()];
			DataInputStream in = new DataInputStream(new FileInputStream(f));
			in.readFully(buffer);
			in.close();

			CMSSignedData signature = new CMSSignedData(buffer);
			SignerInformation signer = (SignerInformation)signature
		                .getSignerInfos().getSigners().iterator().next();
			CertStore cs = signature
		                .getCertificatesAndCRLs("Collection", "BC");
			Iterator iter = cs.getCertificates((CertSelector) signer.getSID()).iterator();
			X509Certificate certificate = (X509Certificate) iter.next();
			CMSProcessable sc = signature.getSignedContent();
			byte[] data = (byte[]) sc.getContent();

			// Verifie la signature
			System.out.println(signer.verify(certificate, "BC"));

			FileOutputStream envfos = new FileOutputStream("document_non_signer.txt");
			envfos.write(data);
			envfos.close();
		} catch (Exception e) {
			e.printStackTrace();
			return ;
		}
	        
	}
	
	public void  ChiffrementDocument() {
		

		try {
			// Fichier � chiffrer
			File f = new File(cheminFichier+"test.txt");
			byte[] buffer = new byte[(int)f.length()];
			DataInputStream in = new DataInputStream(new FileInputStream(f));
			in.readFully(buffer);
			in.close();
		
			// CHoix de l'iv
			byte[] iv = { (byte) 0xc9, (byte) 0x36, (byte) 0x78, (byte) 0x99,
						  (byte) 0x52, (byte) 0x3e, (byte) 0xea, (byte) 0xf2 };
		
			IvParameterSpec salt = new IvParameterSpec(iv);
			// Cl� secr�te choisie
			byte[] raw = "ma cle secrete".getBytes();
			SecretKeySpec skeySpec = new SecretKeySpec(raw, "Blowfish");
		
			// Chiffrement du fichier
			Cipher c = Cipher.getInstance("Blowfish/CBC/PKCS5Padding", "BC");    
		    c.init(Cipher.ENCRYPT_MODE, skeySpec, salt);
			byte[] buf_crypt = c.doFinal(buffer);
		
			FileOutputStream envfos = new FileOutputStream(cheminFichier+"fichier_chiffre");
			envfos.write(buf_crypt);
			envfos.close();
		
			// D�chiffrement du fichier
			c = Cipher.getInstance("Blowfish/CBC/PKCS5Padding", "BC");    
		    c.init(Cipher.DECRYPT_MODE, skeySpec, salt);
			byte[] buf_decrypt = c.doFinal(buf_crypt);
		
			envfos = new FileOutputStream(cheminFichier+"fichier_dechiffre");
			envfos.write(buf_decrypt);
			envfos.close();
		
		} catch (Exception e) {
			e.printStackTrace();
		}

		
	}
	
public void  ChiffrementDocument____() {
		
	try {
        // Chargement du fichier � chiffrer
        File f = new File("fichier_a_chiffrer");
        byte[] buffer = new byte[(int)f.length()];
        DataInputStream in = new DataInputStream(new FileInputStream(f));
        in.readFully(buffer);
        in.close();

        // Chiffrement du document

        CMSEnvelopedDataGenerator gen = new CMSEnvelopedDataGenerator();
        // La variable cert correspond au certificat du destinataire
        // La cl� publique de ce certificat servira � chiffrer la cl� sym�trique
        gen.addKeyTransRecipient((java.security.cert.X509Certificate)cert);

        // Choix de l'algorithme � cl� sym�trique pour chiffrer le document.
        // AES est un standard. Vous pouvez donc l'utiliser sans crainte.
        // Il faut savoir qu'en france la taille maximum autoris�e est de 128
        // bits pour les cl�s sym�triques (ou cl�s secr�tes)
        String algorithm = CMSEnvelopedDataGenerator.AES128_CBC;
        CMSEnvelopedData envData = gen.generate( new CMSProcessableByteArray(buffer),  algorithm, "BC");

        byte[] pkcs7envelopedData = envData.getEncoded();


        // Ecriture du document chiffr�
        FileOutputStream envfos = new FileOutputStream(cheminFichier+f.getName() + ".pk7");
        envfos.write(pkcs7envelopedData);
        envfos.close();

        // D�chiffrement du fichier

        CMSEnvelopedData ced = new CMSEnvelopedData(pkcs7envelopedData);
        Collection recip = (Collection) ced.getRecipientInfos().getRecipients();

        KeyTransRecipientInformation rinfo = (KeyTransRecipientInformation)
            ((AbstractList) recip).iterator().next();
        // privatekey est la cl� priv�e permettant de d�chiffrer la cl� secr�te
        // (sym�trique)
        byte[] contents = rinfo.getContent(privatekey, "BC");

        envfos = new FileOutputStream("fichier_non_chiffrer");
        envfos.write(contents);
        envfos.close();

			} catch (Exception e) {
			        e.printStackTrace();
			}
		
	}
	public void  Empreinte () {
		try {
	        // Chargement du document
	        File f = new File(cheminFichier+"test.txt");
	        byte[] buffer = new byte[(int)f.length()];
	        DataInputStream in = new DataInputStream(new FileInputStream(f));
	        in.readFully(buffer);
	        in.close();

	        // Utilisation de l'algorithme SHA-1
	        MessageDigest md = MessageDigest.getInstance("SHA-1", "BC");
	        byte[] empreinte = Hex.encode(md.digest(buffer));
	        // Affichage de l'empreinte en hexa
	        System.out.println(new String(empreinte));
	       } catch (Exception e) {
	        e.printStackTrace();
	      }
	}
}