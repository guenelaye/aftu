package sn.ssi.kermel.web.cryptographie.controllers;


import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.cert.CertSelector;
import java.security.cert.CertStore;
import java.security.cert.X509Certificate;
import java.util.Iterator;

import org.bouncycastle.cms.CMSProcessable;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.SignerInformation;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.ext.AfterCompose;

import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class VerificationSignature extends AbstractWindow implements  AfterCompose{
	

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		ChargerCertificat();
	}
	public void  ChargerCertificat() {
		
		try {
			// Chargement du fichier sign�
			File f = new File("/cryptographie/test.txt.pk7");
			byte[] buffer = new byte[(int)f.length()];
			DataInputStream in = new DataInputStream(new FileInputStream(f));
			in.readFully(buffer);
			in.close();

			CMSSignedData signature = new CMSSignedData(buffer);
			SignerInformation signer = (SignerInformation)signature .getSignerInfos().getSigners().iterator().next();
			CertStore cs = signature .getCertificatesAndCRLs("Collection", "BC");
			Iterator iter = cs.getCertificates((CertSelector) signer.getSID()).iterator();
			X509Certificate certificate = (X509Certificate) iter.next();
			CMSProcessable sc = signature.getSignedContent();
			byte[] data = (byte[]) sc.getContent();

			// Verifie la signature
			System.out.println(signer.verify(certificate, "BC"));

			FileOutputStream envfos = new FileOutputStream("document_non_signer.txt");
			envfos.write(data);
			envfos.close();
		} catch (Exception e) {
			e.printStackTrace();
			return ;
		}
	}



		
}