package sn.ssi.kermel.web.fournituredrp.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class DossiersConsultationController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code;
	private Tab TAB_infosgenerales,TAB_garanties,TAB_piecesadministratives,TAB_criteresqualifications,TAB_devise,TAB_allotissement,
	TAB_presentationoffres;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblInfos;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygDossiers dossier=new SygDossiers();
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		LibelleTab=(String) session.getAttribute("libelle");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier!=null)
		{
//			if(dossier.getDosNombreLots()==1)
//				TAB_allotissement.setVisible(false);
//			else
//				TAB_allotissement.setVisible(true);
			TAB_allotissement.setVisible(true);
		}
		if(LibelleTab!=null)
		{
			if(LibelleTab.equals("infogenerales"))
			  {
				TAB_infosgenerales.setSelected(true);
				Include inc = (Include) this.getFellowIfAny("incinfosgenerales");
				inc.setSrc("/passationsmarches/fournituredrp/infogenerales.zul");	
			  }
			else
			{
				if(LibelleTab.equals("piecesadministratives"))
				  {
					TAB_piecesadministratives.setSelected(true);
					Include inc = (Include) this.getFellowIfAny("incpiecesadministratives");
					inc.setSrc("/passationsmarches/fournituredrp/piecesadministratives.zul");	
				  }
				else
				{
					if(LibelleTab.equals("criteresqualifications"))
						  {
							TAB_criteresqualifications.setSelected(true);
							Include inc = (Include) this.getFellowIfAny("inccriteresqualifications");
							inc.setSrc("/passationsmarches/fournituredrp/criteresqualifications.zul");	
						  }
						else
						{
							if(LibelleTab.equals("devise"))
							  {
								TAB_devise.setSelected(true);
								Include inc = (Include) this.getFellowIfAny("incdevise");
								inc.setSrc("/passationsmarches/fournituredrp/devise.zul");	
							  }
							else
							{
							
								if(LibelleTab.equals("allotissement"))
								  {
									TAB_allotissement.setSelected(true);
									Include inc = (Include) this.getFellowIfAny("incallotissement");
									inc.setSrc("/passationsmarches/fournituredrp/allotissement.zul");	
								  }
								else
								{
								
									if(LibelleTab.equals("presentationsoffres"))
									  {
										TAB_presentationoffres.setSelected(true);
										Include inc = (Include) this.getFellowIfAny("incpresentationoffres");
										inc.setSrc("/passationsmarches/fournituredrp/presentationsoffres.zul");	
									  }
									else
									{
									
										 Include inc = (Include) this.getFellowIfAny("incinfosgenerales");
										 inc.setSrc("/passationsmarches/fournituredrp/infogenerales.zul");
										
									}
									
								}
								
							}
						}
					}
				}
			
		}
		else
		{
			 Include inc = (Include) this.getFellowIfAny("incinfosgenerales");
		    inc.setSrc("/passationsmarches/fournituredrp/infogenerales.zul");
		}
		    		
		
		   
	
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	
}