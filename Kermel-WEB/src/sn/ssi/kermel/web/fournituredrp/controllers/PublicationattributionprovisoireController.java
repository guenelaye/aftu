package sn.ssi.kermel.web.fournituredrp.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAttributions;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygAvisAttribution;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.AttributionsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.AvisAttributionSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DocumentsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class PublicationattributionprovisoireController extends AbstractWindow implements
		 AfterCompose {

	private Listbox lstListe,lstBailleur;
	private Paging pgPagination,pgBailleur;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtcommentaires;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference,libellebailleur=null;
    private int parent;
     List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherBailleur,txtChapitre;
    SygBailleurs bailleur=null;
    private Decimalbox dcmontantprevu;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idbailleur;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuValider;
	private Datebox dtdatepublication;
	SygAvisAttribution avisattribution=new SygAvisAttribution(); 
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygAttributions attributaire=new SygAttributions();
	List<SygAttributions> attributaires = new ArrayList<SygAttributions>();
	private Textbox txtVersionElectronique;
    private Image image;
	private Label lbltitre,lbldeuxpoints;
	private Div step0,step1;
	private Iframe idIframe;
	private String extension,images;
	List<SygDocuments> documents = new ArrayList<SygDocuments>();
	SygDocuments document=new SygDocuments();
	private String nomFichier;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	   
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if((dossier==null))
		{
			menuValider.setDisabled(true);
		}
		else
		{
			attributaires=BeanLocator.defaultLookup(AttributionsSession.class).find(0, -1, dossier, null);
			avisattribution=BeanLocator.defaultLookup(AvisAttributionSession.class).getAvis(dossier, null,"provisoire");
	    	dtdatepublication.setValue(dossier.getDosDatePublicationProvisoire());
			txtcommentaires.setValue(dossier.getDosCommentaireAttributionProvisoire());
			if(dossier.getDosDatePublicationProvisoire()!=null)
				menuValider.setDisabled(true);
			infos(dossier,appel);
		}
	}

private boolean checkFieldConstraints() {
		
		try {
		
			if(dtdatepublication.getValue()==null)
		     {
               errorComponent = dtdatepublication;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire.date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((dtdatepublication.getValue()).after(new Date()))
			 {
				errorComponent = dtdatepublication;
				errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire.date")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.referentiel.date.dujour")+": "+UtilVue.getInstance().formateLaDate(new Date());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			if(txtVersionElectronique.getValue().equals(""))
		     {
         errorComponent = txtVersionElectronique;
         errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire.fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			return true;
			
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

		public void onClick$menuValider() {
			if(checkFieldConstraints())
			{
				dossier.setDosDatePublicationProvisoire(dtdatepublication.getValue());
				dossier.setDosCommentaireAttributionProvisoire(txtcommentaires.getValue());
				BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).update(dossier);
				lbStatusBar.setValue("");
				
				avisattribution.setAttriDate(dtdatepublication.getValue());
				avisattribution.setAttriPub(UIConstants.PARENT);
				avisattribution.setAttrifichier(txtVersionElectronique.getValue());
				BeanLocator.defaultLookup(AvisAttributionSession.class).update(avisattribution);
				
				for (int i = 0; i < attributaires.size(); i++) {
					attributaire=attributaires.get(i);
					attributaire.setDatePublicationDefinitive(dtdatepublication.getValue());
					BeanLocator.defaultLookup(AttributionsSession.class).update(attributaire);
				}
				
				documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossier, appel,UIConstants.PARAM_TYPEDOCUMENTS_ATTIBUTIONSPROVISOIRE,null, null);
				if(documents.size()>0)
				document=documents.get(0);
				document.setAppel(appel);
				document.setDossier(dossier);
				document.setNomFichier(txtVersionElectronique.getValue());
				document.setTypeDocument(UIConstants.PARAM_TYPEDOCUMENTS_ATTIBUTIONSPROVISOIRE);
				document.setLibelle(Labels.getLabel("kermel.plansdepassation.proceduresmarches.documents.pvattribution"));
				document.setDate(new Date());
				document.setHeure(Calendar.getInstance().getTime());
				if(documents.size()==0)
				{
					BeanLocator.defaultLookup(DocumentsSession.class).save(document);
				}
				else
				{
					BeanLocator.defaultLookup(DocumentsSession.class).update(document);
				}
				session.setAttribute("libelle", "publicationattributionprovisoire");
				loadApplicationState("suivi_fourniture_drp");
			}
		}
		
		public void infos(SygDossiers dossier,SygAppelsOffres appel) {
			documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossier, appel,UIConstants.PARAM_TYPEDOCUMENTS_ATTIBUTIONSPROVISOIRE,null, null);
			
			if(documents.size()>0)
			{
				extension=documents.get(0).getNomFichier().substring(documents.get(0).getNomFichier().length()-3,  documents.get(0).getNomFichier().length());
				 if(extension.equalsIgnoreCase("pdf"))
					 images="/images/icone_pdf.png";
				 else  
					 images="/images/word.jpg";
				 
				image.setVisible(true);
				image.setSrc(images);
				lbldeuxpoints.setValue(":");
				lbltitre.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire.fichiers"));
				menuValider.setDisabled(true);	
			
			}
		}
	
		public void onClick$btnChoixFichier() {
			//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
				if (ToolKermel.isWindows())
					nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
				else
					nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

				txtVersionElectronique.setValue(nomFichier);
			}
}