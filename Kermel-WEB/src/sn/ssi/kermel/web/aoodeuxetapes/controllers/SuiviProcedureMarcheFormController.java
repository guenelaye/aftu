package sn.ssi.kermel.web.aoodeuxetapes.controllers;


import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAttributions;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygCriteresQualificationsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierspieces;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygMontantsSeuils;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersPiecesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersSouscriteresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrederetraitdaoSession;
import sn.ssi.kermel.be.referentiel.ejb.MontantsSeuilsSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



/**

 * @author Adama samb
 * @since mardi 11 Janvier 2011, 12:34
 
 */
public class SuiviProcedureMarcheFormController extends AbstractWindow implements AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private Include pgActes;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	Session session = getHttpSession();
	private Tree tree;
	private Include idinclude;
	List<SygMontantsSeuils> seuils = new ArrayList<SygMontantsSeuils>();
	private SygAutoriteContractante autorite;
	private Label lblObjets,lblModePassation,lblMontant,lblModeSelection,lblType,lgtitre,lblDatecreation;
	private String LibelleTab,libelle,avalider="oui";
	SygDossiers dossierun=new SygDossiers();
	SygDossiers dossierdeux=new SygDossiers();
	SygAttributions attributaire=new SygAttributions();
	private Treeitem treeaoodeuxetapesun,treemiseenvalidation,treepreparation,treedossierappeloffreun,treepublicationdossier,treeregistreretraitdao,
	treedepotplis,treenotificationauxcandidatsretenus,treeaoodeuxetapesdeux,treetraitementdossier;
	private Treecell cellaoodeuxetapesun,celldossierappeloffreun,cellpreparation,celltraitementdossier,cellmiseenvalidation,cellpublicationdossier,
	cellregistreretraitdao,celldepotdepot,cellenregistrementplis,cellnotificationauxcandidatsretenus,cellevaluation,cellouverturesplis,
	cellrepresentantsservicestechniques,cellobservateursindependants,cellevaluationdeux,cellpublicationattributiondefinitive,cellverificationconformite;
	List<SygDossierssouscriteres> criteres = new ArrayList<SygDossierssouscriteres>();
	private int nombreprep=0,nbredossierap;
	SygContrats contrat=new SygContrats();
	List<SygCriteresQualificationsSoumissionnaires> criteresqualificationssoum = new ArrayList<SygCriteresQualificationsSoumissionnaires>();
	List<SygDossierspieces> piecesadministratives = new ArrayList<SygDossierspieces>();
	List<SygDossierssouscriteres> criteresqualifications = new ArrayList<SygDossierssouscriteres>();
	List<SygLots> lots = new ArrayList<SygLots>();
	List<SygRetraitregistredao> registreretrait = new ArrayList<SygRetraitregistredao>();
	List<SygPlisouvertures> registredepot = new ArrayList<SygPlisouvertures>();
	 
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		


	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}
	
	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		LibelleTab=(String) session.getAttribute("libelle");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		
		lblObjets.setValue(appel.getApoobjet());
		lblModePassation.setValue(appel.getModepassation().getLibelle());
		lblMontant.setValue(ToolKermel.format2Decimal(appel.getApomontantestime()));
		lblModeSelection.setValue(appel.getModeselection().getLibelle());
		lblType.setValue(appel.getTypemarche().getLibelle());
		
		lblDatecreation.setValue(UtilVue.getInstance().formateLaDate2(appel.getApodatecreation()));
		
		
		dossierun=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,1);
		dossierdeux=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,2);
		seuils = BeanLocator.defaultLookup(MontantsSeuilsSession.class).find(0,-1, autorite.getType(), appel.getTypemarche(), appel.getModepassation(), null, null,UIConstants.TYPE_SEUILSRAPRIORI);
		
		if(seuils.size()>0)
		{
			if(seuils.get(0).getMontantinferieur()!=null)
			{
				if((appel.getApomontantestime().compareTo(seuils.get(0).getMontantinferieur())==1)||(appel.getApomontantestime().equals(seuils.get(0).getMontantinferieur())))
				
				{
					avalider="oui";
					treemiseenvalidation.setVisible(true);
				}
				else
				{
					avalider="non";
					treemiseenvalidation.setVisible(false);
					
				}
					
			}
			
		}
		//////////Libelle//////// tooltiptext
		if(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis").length()>20)
		  cellouverturesplis.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis").substring(0, 20)+"...");
		else
		  cellouverturesplis.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis"));
		
		if(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.representantservicetechnique").length()>20)
		  cellrepresentantsservicestechniques.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.representantservicetechnique").substring(0, 20)+"...");
		else
		 cellrepresentantsservicestechniques.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.representantservicetechnique"));
		
		if(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.representantservicetechnique").length()>20)
			cellobservateursindependants.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.obserindependant").substring(0, 15)+"...");
		else
			cellobservateursindependants.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.obserindependant"));

		if(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire").length()>15)
			cellevaluationdeux.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire").substring(0, 15)+"...");
		else
			cellevaluationdeux.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire"));
		
		if(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire").length()>15)
			cellpublicationattributiondefinitive.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.pubattribdefinitive").substring(0, 15)+"...");
		else
			cellpublicationattributiondefinitive.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.pubattribdefinitive"));
		
		if(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire").length()>10)
			cellverificationconformite.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.evaluation.verificationconformite").substring(0, 10)+"...");
		else
			cellverificationconformite.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.evaluation.verificationconformite"));
	
		//////////////////////////
		if(dossierun!=null)
		{
			cellaoodeuxetapesun.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			cellaoodeuxetapesun.setImage("/images/puce.png");
			celltraitementdossier.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			celltraitementdossier.setImage("/images/puce.png");
			cellpreparation.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			cellpreparation.setImage("/images/puce.png");
			celldossierappeloffreun.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			celldossierappeloffreun.setImage("/images/puce.png");
			
			piecesadministratives = BeanLocator.defaultLookup(DossiersPiecesSession.class).find(0,-1,dossierun);
			criteresqualifications = BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(0,-1,dossierun, null, null);
				
			if(piecesadministratives.size()>0)
			{
				nbredossierap=nbredossierap+ 1;
			}
			if(criteresqualifications.size()>0)
			{
				nbredossierap=nbredossierap+ 1;
			}
		
			if(lots.size()>0&&lots.size()==dossierun.getDosNombreLots())
			{
				nbredossierap=nbredossierap+ 1;
			}
			if(lots.size()>0&&lots.size()==dossierun.getDosNombreLots())
			{
				if(nbredossierap==3)
				{
					celldossierappeloffreun.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					celldossierappeloffreun.setImage("/images/rdo_on.png");
				}
			}
			else
			{
				if(nbredossierap==2)
				{
					celldossierappeloffreun.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					celldossierappeloffreun.setImage("/images/rdo_on.png");
				}
			}
			if(dossierun.getDosDateMiseValidation()!=null)
			{
				nbredossierap=nbredossierap+ 1;
				cellmiseenvalidation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellmiseenvalidation.setImage("/images/rdo_on.png");
				if(dossierun.getDosDateValidation()==null)
				{
					cellmiseenvalidation.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.enattentevalidation"));
					cellmiseenvalidation.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.encoursvalidation"));
					
				}
				else
					cellmiseenvalidation.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validationdossier")+" "+UtilVue.getInstance().formateLaDate2(dossierun.getDosDateValidation()));
			}
			if(dossierun.getDosDatePublication()!=null)
			{
				nbredossierap=nbredossierap+ 1;
				cellpublicationdossier.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellpublicationdossier.setImage("/images/rdo_on.png");
				cellpublicationdossier.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.publication.publie")+" "+UtilVue.getInstance().formateLaDate2(dossierun.getDosDatePublication()));
				registreretrait = BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).find(0,-1,dossierun);
			}	
			if(registreretrait.size()>0)
			{
				nbredossierap=nbredossierap+ 1;
				cellregistreretraitdao.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellregistreretraitdao.setImage("/images/rdo_on.png");
				registredepot= BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossierun,null,null,null,-1,-1,-1, -1, -1, -1, null, -1, null, null);
				
			}
			if(lots.size()>0&&lots.size()==dossierun.getDosNombreLots())
			{
				if(nbredossierap==6)
				{
					cellpreparation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellpreparation.setImage("/images/rdo_on.png");
				}
			}
			else
			{
				if(nbredossierap==5)
				{
					cellpreparation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellpreparation.setImage("/images/rdo_on.png");
				}
			}
			if(registredepot.size()>0)
			{
				nbredossierap=nbredossierap+ 1;
				celldepotdepot.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				celldepotdepot.setImage("/images/rdo_on.png");
				cellenregistrementplis.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellenregistrementplis.setImage("/images/rdo_on.png");
			}
			if(dossierun.getDosDateNotification()!=null)
			{
				nbredossierap=nbredossierap+ 1;
				cellnotificationauxcandidatsretenus.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellnotificationauxcandidatsretenus.setImage("/images/rdo_on.png");
				cellnotificationauxcandidatsretenus.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.notificationmarche.notifiele")+" "+UtilVue.getInstance().formateLaDate2(dossierun.getDosDateNotification()));
				cellevaluation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellevaluation.setImage("/images/rdo_on.png");
			}
			if(lots.size()>0&&lots.size()==dossierun.getDosNombreLots())
			{
				if(nbredossierap==8)
				{
					celltraitementdossier.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					celltraitementdossier.setImage("/images/rdo_on.png");
					cellaoodeuxetapesun.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellaoodeuxetapesun.setImage("/images/rdo_on.png");
				}
			}
			else
			{
				if(nbredossierap==7)
				{
					celltraitementdossier.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					celltraitementdossier.setImage("/images/rdo_on.png");
					cellaoodeuxetapesun.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellaoodeuxetapesun.setImage("/images/rdo_on.png");
				}
			}
//			if((dossier.getDosNombreLots()>0)&&dossier.getDosLotDivisible().equals("OUI"))
//				treelotssoummissionnaires.setVisible(true);
//			else
//				treelotssoummissionnaires.setVisible(false);
		}
	
		if(LibelleTab!=null)
		{
			if(LibelleTab.equals("traitementsdossiers"))
			{
				idinclude.setSrc("/passationsmarches/aoodeuxetapes/traitementsdossiers.zul");	
				treeaoodeuxetapesun.setOpen(false);
				tree.setSelectedItem(treeaoodeuxetapesun); 
				
			}
			if(LibelleTab.equals("traitementsetapeun"))
			{
				idinclude.setSrc("/passationsmarches/aoodeuxetapes/traitementsetapeun.zul");	
				treeaoodeuxetapesun.setOpen(false);
				tree.setSelectedItem(treetraitementdossier); 
				
			}
			if((LibelleTab.equals("dossierappeloffreun"))||(LibelleTab.equals("infogenerales"))||(LibelleTab.equals("allotissement"))
					||(LibelleTab.equals("piecesadministratives"))||(LibelleTab.equals("criteresqualifications")))
			  {
				idinclude.setSrc("/passationsmarches/aoodeuxetapes/dossierappeloffreun.zul");	
				treeaoodeuxetapesun.setOpen(true);
				tree.setSelectedItem(treedossierappeloffreun); 
			  }
			if(LibelleTab.equals("validationdossier"))
			{
				idinclude.setSrc("/passationsmarches/aoodeuxetapes/soumissionpourvalidation.zul");	
				tree.setSelectedItem(treemiseenvalidation);
				TreeTreepreparation();
			}
			if(LibelleTab.equals("publicationdossier"))
			{
				idinclude.setSrc("/passationsmarches/aoodeuxetapes/publicationdossier.zul");	
				tree.setSelectedItem(treepublicationdossier);
				TreeTreepreparation();
			}
			if(LibelleTab.equals("registreretraitdao"))
			{
				idinclude.setSrc("/passationsmarches/aoodeuxetapes/registreretraitdao.zul");	
				tree.setSelectedItem(treeregistreretraitdao);
				TreeTreepreparation();
				
			}
			if(LibelleTab.equals("plisouvertures"))
			{
				idinclude.setSrc("/passationsmarches/aoodeuxetapes/registredepotplis.zul");	
				tree.setSelectedItem(treedepotplis);
				TreeTreepreparation();
				
			}
			if(LibelleTab.equals("notificationmarche"))
			{
				idinclude.setSrc("/passationsmarches/aoodeuxetapes/formnotificationmarche.zul");	
				tree.setSelectedItem(treenotificationauxcandidatsretenus);
				TreeTreepreparation();
			}
		
			if(LibelleTab.equals("traitementsdossiersdeux"))
			{
				idinclude.setSrc("/passationsmarches/aoodeuxetapes/traitementsdossiersdeux.zul");	
				treeaoodeuxetapesdeux.setOpen(false);
				tree.setSelectedItem(treetraitementdossier);
				
			}
		}
		else
		{
			idinclude.setSrc("/passationsmarches/aoodeuxetapes/traitementsdossiers.zul");	
			 tree.setSelectedItem(treeaoodeuxetapesun); 
			 
		}
		
		
	//	tree.setSelectedItem(itemdossiers);     
		
	
	}


	
	

	public void onSelect$tree() throws Exception
	{
		InfosTree();
	}
	
	public void TreeManifestationdinteret()
	{
		treeaoodeuxetapesun.setOpen(true);
		
	}
	
	public void TreeTreepreparation()
	{
		treepreparation.setOpen(true);
		
	}
	public void InfosTree() throws Exception{
		
		/////////////Preparation du dossier//////////
		if(tree.getSelectedItem().getId().equals("treeaoodeuxetapesun"))
		{
			TreeManifestationdinteret();
			idinclude.setSrc("/passationsmarches/aoodeuxetapes/traitementsetapeun.zul");
		}
		if(tree.getSelectedItem().getId().equals("treepreparation"))
		{
			TreeTreepreparation();
			idinclude.setSrc("/passationsmarches/aoodeuxetapes/dossierspreparation.zul");
			
		}
		if(tree.getSelectedItem().getId().equals("treedossierappeloffreun"))
		{
			TreeTreepreparation();
			idinclude.setSrc("/passationsmarches/aoodeuxetapes/dossierappeloffreun.zul");
			
		}
		if(tree.getSelectedItem().getId().equals("treeinfosgeneral"))
		{
			session.setAttribute("libelle", "infogenerales");
			loadApplicationState("aoo_deuxetapes");
		}
		  if(tree.getSelectedItem().getId().equals("treemiseenvalidation"))
			{
		     TreeTreepreparation();
				if(dossierun==null)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				}
				else
				{
					
						if(piecesadministratives.size()==0)
						{
							 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirpieces"), "Erreur", Messagebox.OK, Messagebox.ERROR);
								
						}
						else
						{
							if(criteresqualifications.size()==0)
							{
								 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisircriteres"), "Erreur", Messagebox.OK, Messagebox.ERROR);
									
							}
							else
							{
								idinclude.setSrc("/passationsmarches/aoodeuxetapes/soumissionpourvalidation.zul");
							}
						}
					
					
				}
			
			}
		  if(tree.getSelectedItem().getId().equals("treepublicationdossier"))
			{
				TreeTreepreparation();
				if(dossierun==null)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				}
				else
				{
					if(avalider.equals("oui"))
					{
						if(dossierun.getDosDateValidation()==null)
							 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validerdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
							else
							   idinclude.setSrc("/passationsmarches/aoodeuxetapes/publicationdossier.zul");
					}
					else
					{
						 idinclude.setSrc("/passationsmarches/aoodeuxetapes/publicationdossier.zul");
					}
				
				}
				
			}
		  if(tree.getSelectedItem().getId().equals("treeregistreretraitdao"))
			{
				if(dossierun==null)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				}
				else
				{
					if(dossierun.getDosDatePublication()==null)
					 {
						 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.publieravis"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					 }
					 else
					 {
						 idinclude.setSrc("/passationsmarches/aoodeuxetapes/registreretraitdao.zul");
					 }
				}
		   }
		  if(tree.getSelectedItem().getId().equals("treedepotplis"))
			{
				
				if(dossierun==null)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				}
				else
				{
					if(registreretrait.size()==0)
					 {
						 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.saisirregistreretrait"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					 }
					 else
					 {
						 idinclude.setSrc("/passationsmarches/aoodeuxetapes/registredepotplis.zul");
					 }
				}
			}
		  if((tree.getSelectedItem().getId().equals("treenotificationauxcandidatsretenus")))
			{
				if(dossierun==null)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				}
				else
				{
					if(registredepot.size()==0)
					 {
						 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.saisirregistreretrait"), "Erreur", Messagebox.OK, Messagebox.ERROR);
							
					 }
					 else
					 {
						 idinclude.setSrc("/passationsmarches/aoodeuxetapes/formnotificationmarche.zul");
					 }
				}
				
			}
	}
}