package sn.ssi.kermel.web.aoodeuxetapes.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierspieces;
import sn.ssi.kermel.be.entity.SygPieces;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersPiecesSession;
import sn.ssi.kermel.be.referentiel.ejb.PiecesSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class PiecesAdministrativesController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstPieces;
	private Paging pgPagination,pgPieces;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
  	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	SygPieces piece=new SygPieces();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuAjouter,menuSupprimer;
	private Div step0,step1;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
    	
		addEventListener(ApplicationEvents.ON_PIECES, this);
		lstPieces.setItemRenderer(new PiecesRenderer());
		pgPieces.setPageSize(byPage);
		pgPieces.addForward("onPaging", this, ApplicationEvents.ON_PIECES);
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,1);
		if(dossier==null)
		{
			menuAjouter.setDisabled(true);
			menuSupprimer.setDisabled(true);
		}
		else
		{
			if(dossier.getDosDateMiseValidation()!=null||dossier.getDosDatePublication()!=null)
			{
				menuAjouter.setDisabled(true);
				menuSupprimer.setDisabled(true);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			Events.postEvent(ApplicationEvents.ON_PIECES, this, null);
		}
		
	}
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygDossierspieces> pieces = BeanLocator.defaultLookup(DossiersPiecesSession.class).find(activePage,byPage,dossier);
			 lstListe.setModel(new SimpleListModel(pieces));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(DossiersPiecesSession.class).count(dossier));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_PIECES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPieces.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPieces.getActivePage() * byPage;
				pgPieces.setPageSize(byPage);
			}
			 List<SygPieces> pieces = BeanLocator.defaultLookup(PiecesSession.class).ListePieces(activePage,byPage,libelle,dossier.getDosID(),autorite.getId());
			 lstPieces.setModel(new SimpleListModel(pieces));
			 pgPieces.setTotalSize(BeanLocator.defaultLookup(PiecesSession.class).ListePieces(libelle,dossier.getDosID(),autorite.getId()).size());
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = ((SygDossierspieces)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue()).getId();
				BeanLocator.defaultLookup(DossiersPiecesSession.class).delete(codes);
			}
			 session.setAttribute("libelle", "piecesadministratives");
			   loadApplicationState("aoo_deuxetapes");
		}
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygDossierspieces pieces = (SygDossierspieces) data;
		item.setValue(pieces);

		 Listcell cellLibelle = new Listcell(pieces.getPiece().getLibelle());
		 cellLibelle.setParent(item);
		 
		 Listcell cellTaux = new Listcell(pieces.getPiece().getDescription());
		 cellTaux.setParent(item);
		 
		 Listcell cellCible = new Listcell("");
		   if(pieces.getPiece().getLocalisation().equals("n"))
			   cellCible.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.pieces.locale"));
		   else  if(pieces.getPiece().getLocalisation().equals("i"))
			   cellCible.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.pieces.internationnal"));
		   else
			   cellCible.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.pieces.lesdeux"));
		 cellCible.setParent(item);
	}

	
	public class PiecesRenderer implements ListitemRenderer{
		
		
		
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygPieces pieces = (SygPieces) data;
		item.setValue(pieces);

		 Listcell cellLibelle = new Listcell(pieces.getLibelle());
		 cellLibelle.setParent(item);
		 
		 Listcell cellTaux = new Listcell(pieces.getDescription());
		 cellTaux.setParent(item);
		 
		 Listcell cellCible = new Listcell("");
		   if(pieces.getLocalisation().equals("n"))
			   cellCible.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.pieces.locale"));
		   else  if(pieces.getLocalisation().equals("i"))
			   cellCible.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.pieces.internationnal"));
		   else
			   cellCible.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.pieces.lesdeux"));
		 cellCible.setParent(item);
	
	}
	}
	public void onClick$menuSupprimer()
	{
		if (lstListe.getSelectedItem() == null)
			
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		

		HashMap<String, String> display = new HashMap<String, String>(); // permet
		display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
		display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
		display.put(MessageBoxController.DSP_HEIGHT, "250px");
		display.put(MessageBoxController.DSP_WIDTH, "47%");

		HashMap<String, Object> map = new HashMap<String, Object>(); // permet
		map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
		showMessageBox(display, map);
		
	}
	public void onClick$menuAjouter()
	{
		step0.setVisible(false);
		step1.setVisible(true);
		Events.postEvent(ApplicationEvents.ON_PIECES, this, null);
	}
	public void onClick$menuFermer()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		
	}
	public void onClick$menuValider()
	{
       if (lstPieces.getSelectedItem() == null)
			throw new WrongValueException(lstPieces, Labels.getLabel("kermel.error.select.item"));
       
       for (int i = 0; i < lstPieces.getSelectedCount(); i++) {
    	   SygDossierspieces dossierpiece=new SygDossierspieces();
    	   piece=(SygPieces) ((Listitem) lstPieces.getSelectedItems().toArray()[i]).getValue();
    	   dossierpiece.setPiece(piece);
    	   dossierpiece.setDossier(dossier);
    	   BeanLocator.defaultLookup(DossiersPiecesSession.class).save(dossierpiece);
       }
       session.setAttribute("libelle", "piecesadministratives");
	   loadApplicationState("aoo_deuxetapes");
		
	}
}