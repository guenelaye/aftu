package sn.ssi.kermel.web.aoodeuxetapes.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class DemandePublicationController extends AbstractWindow implements
		 AfterCompose {

	private Listbox lstListe,lstBailleur;
	private Paging pgPagination,pgBailleur;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtcommentaires,txtVersionElectronique;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference,libellebailleur=null;
    private int parent;
     List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherBailleur,txtChapitre;
    SygBailleurs bailleur=null;
    private Decimalbox dcmontantprevu;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idbailleur;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuValider;
	private Datebox dtdate;
	private String nomFichier;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	private Image image;
	private Div step0,step1;
	private Iframe idIframe;
	private String extension,images;
	SygDocuments document=new SygDocuments();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	   
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,1);
		if((dossier==null))
		{
			menuValider.setDisabled(true);
		}
		else
		{
			if(dossier.getDosdatedemandepublication()!=null)
			{
				menuValider.setDisabled(true);
				dtdate.setValue(dossier.getDosdatedemandepublication());
				txtcommentaires.setValue(dossier.getDoscommentairesdemandepublication());
				Infosdossiers();
			}
		}
	}

private boolean checkFieldConstraints() {
		
		try {
		
			if(dtdate.getValue()==null)
		     {
               errorComponent = dtdate;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.demande.demandepublication")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(((new Date()).before(dtdate.getValue())))
			 {
				errorComponent = dtdate;
				errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.demande.demandepublication")+" "+Labels.getLabel("kermel.referentiel.date.posterieure")+" "+Labels.getLabel("kermel.referentiel.date.dujour")+": "+UtilVue.getInstance().formateLaDate(new Date());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			if(txtVersionElectronique.getValue().equals(""))
		     {
        errorComponent = txtVersionElectronique;
        errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.avisappelsoffre")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			return true;
			
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

		public void onClick$menuValider() {
			if(checkFieldConstraints())
			{
				dossier.setDosdatedemandepublication(dtdate.getValue());
				dossier.setDoscommentairesdemandepublication(txtcommentaires.getValue());
				dossier.setDosfichierdemandepublication(txtVersionElectronique.getValue());
				BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).update(dossier);
				session.setAttribute("libelle", "demandepublication");
				loadApplicationState("aoo_deuxetapes");
			}
		}
		
		public void onClick$btnChoixFichier() {
			//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
				if (ToolKermel.isWindows())
					nomFichier = FileLoader.uploadPieceDossierAO(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
				else
					nomFichier = FileLoader.uploadPieceDossierAO(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

				txtVersionElectronique.setValue(nomFichier);
			}
		
		public org.zkoss.util.media.AMedia fetchFile(File file) {

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(file, null, null);
				return mymedia;
			} catch (Exception e) {

				e.printStackTrace();
				return null;
			}

		}
		public void onClick$menuFermer() {
			step0.setVisible(true);
			step1.setVisible(false);
		}
		public void onClick$image() {
			step0.setVisible(false);
			step1.setVisible(true);
			String filepath = cheminDossier +  dossier.getDosfichierdemandepublication();
			File f = new File(filepath.replaceAll("\\\\", "/"));

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(f, null, null);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (mymedia != null)
				idIframe.setContent(mymedia);
			else
				idIframe.setSrc("");

			idIframe.setHeight("600px");
			idIframe.setWidth("100%");
		} 
		
		public void Infosdossiers()
		{
			if(dossier.getDosfichierdemandepublication()!=null)
			{
				extension=dossier.getDosfichierdemandepublication().substring(dossier.getDosfichierdemandepublication().length()-3,  dossier.getDosfichierdemandepublication().length());
				 if(extension.equalsIgnoreCase("pdf"))
					 images="/images/icone_pdf.png";
				 else  
					 images="/images/word.jpg";
				 
				image.setVisible(true);
				image.setSrc(images);
				nomFichier=dossier.getDosfichierdemandepublication();
			}
		}
	
}