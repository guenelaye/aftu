package sn.ssi.kermel.web.recouvrement.controllers;


import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Label;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SysParametresGeneraux;
import sn.ssi.kermel.be.referentiel.ejb.ParametresGenerauxSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class ConfigurationTauxFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	
	String code;
	private	SysParametresGeneraux parametre=new SysParametresGeneraux();
	List<SysParametresGeneraux> parametres = new ArrayList<SysParametresGeneraux>();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Doublebox dbTaux;

	@Override
	public void onEvent(Event event) throws Exception {


	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
	}

	
	public void onCreate(CreateEvent event) {
		
		login = ((String) getHttpSession().getAttribute("user"));
		
		
			parametres = BeanLocator.defaultLookup(ParametresGenerauxSession.class).find(0, -1, "TAUXVENTEDAO");
			if(parametres!=null){
				code=parametres.get(0).getCode();
			parametre = BeanLocator.defaultLookup(ParametresGenerauxSession.class).findById(code);
			dbTaux.setValue(parametres.get(0).getTaux());
		
			}
		
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			parametre.setTaux(dbTaux.getValue());  

			BeanLocator.defaultLookup(ParametresGenerauxSession.class).update(parametre);
				
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			if(dbTaux.getValue().equals("")|| (dbTaux.getValue() == null))
		     {
               errorComponent = dbTaux;
               errorMsg = Labels.getLabel("kermel.paiement.taux")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			

			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	

}
