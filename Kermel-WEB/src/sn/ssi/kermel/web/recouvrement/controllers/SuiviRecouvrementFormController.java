package sn.ssi.kermel.web.recouvrement.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBanque;
import sn.ssi.kermel.be.entity.SygOperationRecouvrement;
import sn.ssi.kermel.be.entity.SygRecouvrement;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.recouvrement.ejb.OperationRecouvrementSession;
import sn.ssi.kermel.be.recouvrement.ejb.RecouvrementSession;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteContractanteSession;
import sn.ssi.kermel.be.referentiel.ejb.BanqueSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

//@SuppressWarnings("serial")
public class SuiviRecouvrementFormController extends AbstractWindow implements AfterCompose, EventListener, ListitemRenderer {

	private static final long serialVersionUID = 3449680167389488155L;
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_MONTANT = "MONTANT";
	private String mode;
	private Long code;
	private SygRecouvrement recouvrement = new SygRecouvrement();
	List<SygRecouvrement> recouvrements = new ArrayList<SygRecouvrement>();
    private int nbre;
	private Label lbStatusBar;

	private Radiogroup radioMpaiement;
	private Radio rdcheque,rdautre;
	
    //AC
	private SygAutoriteContractante autorite = new SygAutoriteContractante();
	

	
	//Appel Offre
	private SygAppelsOffres appeloffre = new SygAppelsOffres();
	

	

	
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Div step2,step3;
	
	
	private Textbox txtNumCheque,txtAutres,txtPayerpar;
	private Date datejour;

	 private String montantenlettre;
	
	
	//Information generale
	private BigDecimal montanttotal=null;
	private Label lblAC,  lblMontant;

    //Editer cheque
	protected Iframe iframeEs2Detached;
	protected static final String EDIT_URL = "http://" + BeConstants.IP_SYGMAP + ":" + BeConstants.PORT_SYGMAP + "/birt/frameset?__report=/etatventedao/";
	private SygOperationRecouvrement operationrecouvrement = new SygOperationRecouvrement();
	private String numrecu;
	 private String format;
	 private String nomFichier;
	
	// /Banque 
	private SygBanque banque = new SygBanque();
	private Textbox txtRechercherBanque;
	private Bandbox bdBanque;
	private Paging pgBanque;
	private Listbox lstBanque;
	private String LibelleBanque = null;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		
		Components.addForwards(this, this);

		// champ Banque  du formulaire
		addEventListener(ApplicationEvents.ON_BANQUE, this);
		lstBanque.setItemRenderer(new banqueRenderer());
		pgBanque.setPageSize(byPageBandbox);
		pgBanque.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_BANQUE);
		Events.postEvent(ApplicationEvents.ON_BANQUE, this, null);

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_BANQUE)) {
			List<SygBanque> banques = BeanLocator.defaultLookup(BanqueSession.class).find(pgBanque.getActivePage() * byPageBandbox, byPageBandbox, null, LibelleBanque);
			SimpleListModel listModel = new SimpleListModel(banques);
			lstBanque.setModel(listModel);
			pgBanque.setTotalSize(BeanLocator.defaultLookup(BanqueSession.class).count(null, LibelleBanque));
		}

	}

	

	// /////////////////
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent createEvent) {
		Map<String, Object> windowParams = (Map<String, Object>) createEvent.getArg();
		mode = (String) windowParams.get(WINDOW_PARAM_MODE);
		if (mode == null) {
			
		} else {
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				code = (Long) windowParams.get(PARAM_WINDOW_CODE);
				autorite = BeanLocator.defaultLookup(AutoriteContractanteSession.class).findById(code);
				
				List<SygAppelsOffres> appeloffres = BeanLocator.defaultLookup(AppelsOffresSession.class).findRech(0, -1,UIConstants.CONTRAT_NPAYE,autorite,null);
				lblAC.setValue(autorite.getDenomination());
				nbre=appeloffres.size();
				
//				
//				 for (int i = 0; i < appeloffres.size(); i++) {
//					
//					 if((montanttotal!=null)){
//				         montanttotal=montanttotal.add(appeloffres.get(i).getApomontantestime());
//				       
//			           }
//				    else{
//					   montanttotal =appeloffres.get(i).getApomontantestime();
//					   
//				       }
//				 }
				 montanttotal= (BigDecimal) windowParams.get(WINDOW_PARAM_MONTANT);
				 
				lblMontant.setValue(ToolKermel.format3Decimal(montanttotal));
					
			   txtPayerpar.setValue(autorite.getDenomination());
				step2.setVisible(true);
				
				
			}
		}

	}

	private boolean checkFieldConstraints() {

		try {
			
			 if(txtPayerpar.getValue().equals(""))
		       {
                errorComponent = txtPayerpar;
                errorMsg = Labels.getLabel("kermel.paiement.payepar")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		        }
			if(rdcheque.isChecked()==true)
			{
			  if(txtNumCheque.getValue().equals(""))
		       {
                errorComponent = txtNumCheque;
                errorMsg = Labels.getLabel("kermel.paiement.numcheque")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		        }
			  
			  if(bdBanque.getValue().equals(""))
		       {
            errorComponent = bdBanque;
            errorMsg = Labels.getLabel("kermel.paiement.banque")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		        }
			 }
			
			if(rdautre.isChecked()==true)
			{
			   if(txtAutres.getValue().equals(""))
		       {
                errorComponent = txtAutres;
                errorMsg = Labels.getLabel("kermel.paiement.commentaireautre")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		      }
			}
			
			return true;

		} catch (Exception e) {
			errorMsg = Labels.getLabel("cciad.erreur.erreurinconnue") + ": " + e.toString() + " [checkFieldConstraints]";
			errorComponent = null;
			return false;

		}

	}

	
//Step2*****************************************************************
	public void onClick$menuNextStep2() {
		if (checkFieldConstraints()) {
			  // numrecu=BeanLocator.defaultLookup(OperationPaiementSession.class).getGeneratedCode(BeConstants.PARAM_NUMRECU);
			 numrecu="R"+autorite.getId()+BeanLocator.defaultLookup(OperationRecouvrementSession.class).getGeneratedCode(BeConstants.PARAM_NUMRECUVERSEMENT);

			 List<SygAppelsOffres> appeloffres = BeanLocator.defaultLookup(AppelsOffresSession.class).findRech(0, -1,UIConstants.CONTRAT_NPAYE,autorite,null);
			 for (int i = 0; i < appeloffres.size(); i++) {
					
				   appeloffre=appeloffres.get(i);
				   
					 recouvrement.setAppeloffre(appeloffre);
					 appeloffre.setApoStatut(UIConstants.CONTRAT_PAYE);
					 datejour = new Date();
					 appeloffre.setApodateversement(datejour);
					 
					 if (((String) radioMpaiement.getSelectedItem().getValue()).equalsIgnoreCase(Labels.getLabel("kermel.paiement.modepaiement.cheque"))) {
							recouvrement.setNatureVersement(Labels.getLabel("kermel.paiement.modepaiement.cheque"));
							
						} else if (((String) radioMpaiement.getSelectedItem().getValue()).equalsIgnoreCase(Labels.getLabel("kermel.paiement.modepaiement.espece"))) {

							recouvrement.setNatureVersement(Labels.getLabel("kermel.paiement.modepaiement.espece"));
							
						}else {

							recouvrement.setNatureVersement(Labels.getLabel("kermel.paiement.modepaiement.autre"));
							
						}
					 
					
							banque = (SygBanque) lstBanque.getSelectedItem().getValue();

						    recouvrement.setBanque(banque);
						    recouvrement.setNumCheque(txtNumCheque.getValue());
							//recouvrement.setBanque(txtBanque.getValue());
						    recouvrement.setAutorite(autorite);
						    recouvrement.setCommentaire(txtAutres.getValue());
						    recouvrement.setOperation(numrecu); 
						  
					 BeanLocator.defaultLookup(AppelsOffresSession.class).update(appeloffre);
					 BeanLocator.defaultLookup(RecouvrementSession.class).save(recouvrement);
					 
					}
			  
			 //Operation
			    operationrecouvrement.setOpemontanttotal(montanttotal);
			    montantenlettre=ToolKermel.conversionChiffreLettre(montanttotal);
			      operationrecouvrement.setOpmontantTotalEnLettre(montantenlettre);
			    operationrecouvrement.setOpedaterecu(new Date());
			     operationrecouvrement.setOpecode(numrecu);
			    operationrecouvrement.setOperecude(txtPayerpar.getValue());
			    operationrecouvrement.setOpecommentaires(txtAutres.getValue());
			    operationrecouvrement.setAutorite(autorite);
			    BeanLocator.defaultLookup(OperationRecouvrementSession.class).save(operationrecouvrement);
			   
			    nomFichier = "recuventedao.rptdesign";
                format = UIConstants.FORMAT_PDF;
                System.out.println(EDIT_URL + nomFichier + "&code=" + numrecu + "&__format=" + format );
                iframeEs2Detached.setSrc(EDIT_URL + nomFichier + "&numrecu=" + numrecu + "&__format=" + format );
			    
			//loadApplicationState("suivi_recouvrement");
			//detach();
			    
				step2.setVisible(false);
				step3.setVisible(true);
			
		}
	}

	
	public void onClick$menuCancelImprimerEs(){
		loadApplicationState("suivi_recouvrement");
		detach();
      }
	// ///////////////////////////////////////////////////////////////////

	

	public void onClick$menuCancelStep2() {
		
		loadApplicationState("suivi_recouvrement");
		detach();
	}



	// //////////////////////////////////////////////////

	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		

	}
	
	
	public void onCheck$radioMpaiement(){
		
		if(radioMpaiement.getSelectedItem().getValue().equals(Labels.getLabel("kermel.paiement.modepaiement.cheque"))){
			
			txtNumCheque.setDisabled(false);
			bdBanque.setDisabled(false);
			txtAutres.setDisabled(true);
			
		}else if(radioMpaiement.getSelectedItem().getValue().equals(Labels.getLabel("kermel.paiement.modepaiement.espece"))){
			
			txtNumCheque.setDisabled(true);
			bdBanque.setDisabled(true);
			txtAutres.setDisabled(true);
			
		}if(radioMpaiement.getSelectedItem().getValue().equals(Labels.getLabel("kermel.paiement.modepaiement.autre"))){
			
			txtNumCheque.setDisabled(true);
			bdBanque.setDisabled(true);
			txtAutres.setDisabled(false);
		}
	}

	
	// //////////Banque********************
	public void onSelect$lstBanque() {
		bdBanque.setValue((String) lstBanque.getSelectedItem().getAttribute(UIConstants.ATTRIBUTE_LIBELLE));
		bdBanque.close();
	}

	public class banqueRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygBanque banque = (SygBanque) data;
			item.setValue(banque);
			item.setAttribute(UIConstants.ATTRIBUTE_LIBELLE, banque.getLibelle());

			Listcell cellLibelle = new Listcell("");
			if (banque.getLibelle() != null) {
				cellLibelle.setLabel(banque.getLibelle());
			}
			cellLibelle.setParent(item);
			//				

		}
	}

	public void onFocus$txtRechercherBanque() {
		if (txtRechercherBanque.getValue().equalsIgnoreCase(Labels.getLabel("kermel.paiement.banque"))) {
			txtRechercherBanque.setValue("");
		}
	}

	public void onBlur$txtRechercherDecision() {
		if (txtRechercherBanque.getValue().equalsIgnoreCase("")) {
			txtRechercherBanque.setValue(Labels.getLabel("kermel.paiement.banque"));
		}
	}

	public void onClick$btnRechercherDecision() {
		if (txtRechercherBanque.getValue().equalsIgnoreCase(Labels.getLabel("kermel.paiement.banque")) || txtRechercherBanque.getValue().equals("")) {
			LibelleBanque = null;
		} else {
			LibelleBanque = txtRechercherBanque.getValue();
		}
		Events.postEvent(ApplicationEvents.ON_BANQUE, this, null);
	}

	// ////////////////fin ////////////////////////////////
}