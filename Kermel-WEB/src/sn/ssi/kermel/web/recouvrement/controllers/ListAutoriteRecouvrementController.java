package sn.ssi.kermel.web.recouvrement.controllers;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteContractanteSession;
import sn.ssi.kermel.be.referentiel.ejb.FournisseurSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class ListAutoriteRecouvrementController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtNom;
    String nom=null, page=null;
    private Listheader lshNom;
    String login;
    private SygAutoriteContractante autorite = null;
    
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem WADD_RECOUVREMENT,EDIT_DOSSIER;
    
 
	private Utilisateur infoscompte;
	private BigDecimal montanttotalappel=null;
	
	//Recher Ac
	public Textbox txtDenomination;
	private String denomination=null;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_SUIVIRECOUVREMENT);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (WADD_RECOUVREMENT != null) { WADD_RECOUVREMENT.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (EDIT_DOSSIER != null) { EDIT_DOSSIER.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		
		
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lshNom.setSortAscending(new FieldComparator("nom", false));
		lshNom.setSortDescending(new FieldComparator("nom", true));
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
    	
    	login = ((String) getHttpSession().getAttribute("user"));
	}

	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);
	}

	@Override
	public void onEvent(final Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygAutoriteContractante> autorites = BeanLocator.defaultLookup(AutoriteContractanteSession.class).findRecouvrement(activePage,byPage,UIConstants.CONTRAT_NPAYE,denomination);
			 SimpleListModel listModel = new SimpleListModel(autorites);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup(AutoriteContractanteSession.class).countRecouvrement(UIConstants.CONTRAT_NPAYE,denomination));
		} 

		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			if (lstListe.getSelectedItem() == null)
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			
			final String uri = "/recouvrement/formsuivirecouvrement.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.recouvrement.form.titre"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(SuiviRecouvrementFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(SuiviRecouvrementFormController.WINDOW_PARAM_MONTANT, lstListe .getSelectedItem().getAttribute("montanttotalappel"));
			data.put(SuiviRecouvrementFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				
				HashMap<String, String> display = new HashMap<String, String>();
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); 
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(FournisseurSession.class).delete(codes);
				BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_FOURNISSEUR", Labels.getLabel("kermel.referentiel.common.fournisseur.suppression")+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
				if (lstListe.getSelectedItem() == null)
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				
				final String uri = "/recouvrement/editdossier.zul";

				final HashMap<String, String> display = new HashMap<String, String>();
				display.put(DSP_HEIGHT,"500px");
				display.put(DSP_WIDTH, "80%");
				display.put(DSP_TITLE, Labels.getLabel("kermel.recouvrement.visualiser.contrat.titre"));

				final HashMap<String, Object> data = new HashMap<String, Object>();
				data.put(EditDossierFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
				data.put(EditDossierFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

				showPopupWindow(uri, data, display);
			} 
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygAutoriteContractante autorite = (SygAutoriteContractante) data;
		item.setValue(autorite.getId());

		Listcell cellDenomination = new Listcell(autorite.getDenomination());
		cellDenomination.setParent(item);

		List<SygAppelsOffres> appeloffre = BeanLocator.defaultLookup(AppelsOffresSession.class).findRech(0, -1,UIConstants.CONTRAT_NPAYE,autorite,null);
		montanttotalappel=new BigDecimal(0);
		 for (int i = 0; i < appeloffre.size(); i++) {
			 //montanttotalappel=montanttotalappel.add(appeloffre.get(i).getApomontantestime());
			 montanttotalappel=montanttotalappel.add(appeloffre.get(i).getApomontantversement());
			 
		 }
		 Listcell cellMontant = new Listcell(ToolKermel.format2Decimal(montanttotalappel));
		 cellMontant.setParent(item);
		 item.setAttribute("montanttotalappel",montanttotalappel);

	   }
	
	
//////////////*******************Denomination*******//////////////////////

	// function du bouton recherche par Denomination
	public void onFocus$txtDenomination() {
		if (txtDenomination.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.denomination")))
			txtDenomination.setValue("");

	}

	public void onBlur$txtDenomination() {
		if (txtDenomination.getValue().equals(""))
			txtDenomination.setValue(Labels.getLabel("kermel.referentiel.common.denomination"));
	}

	public void onOK$txtDenomination() {
		onClick$bchercherAC();
	}

	
	public void onClick$bchercherAC() {

		if (txtDenomination.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.denomination")) || txtDenomination.getValue().equals("")) {
			denomination = null;
		} else {
			denomination = txtDenomination.getValue();
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

	}

	public void onOK$bchercherAC() {
		onClick$bchercherAC();
	}

	//////////////*******************Denomination*******//////////////////////
	
	
}
