package sn.ssi.kermel.web.recouvrement.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBanque;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygOperationRecouvrement;
import sn.ssi.kermel.be.entity.SygRecouvrement;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.recouvrement.ejb.OperationRecouvrementSession;
import sn.ssi.kermel.be.recouvrement.ejb.RecouvrementSession;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteContractanteSession;
import sn.ssi.kermel.be.referentiel.ejb.BanqueSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

//@SuppressWarnings("serial")
public class RecouvrementFormController extends AbstractWindow implements AfterCompose, EventListener, ListitemRenderer {

	private static final long serialVersionUID = 3449680167389488155L;
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String WINDOW_PARAM_MODE = "MODE";

	private String mode;
	private Long code;
	private SygRecouvrement recouvrement = new SygRecouvrement();
	List<SygRecouvrement> recouvrements = new ArrayList<SygRecouvrement>();
    private int nbre;
	private Label lbStatusBar;
	//private Textbox txtNumero;
	private Radiogroup radioMpaiement;
	private Radio rdcheque,rdautre;
	private BigDecimal montanttotalappel=null;
   
	
	 //Autorite Contractante
	private SygAutoriteContractante autorite = new SygAutoriteContractante();
	private Listbox lstAutorite;
	private Paging pgAutorite;

	
	//Recher Ac
	public Textbox txtDenomination;
	private String denomination=null;
	
	//Appel Offre
	private SygAppelsOffres appeloffre = new SygAppelsOffres();
	private Listbox lstAppeloffre;
	private Paging pgAppeloffre;
	
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Div  step1, step2,step3,stepAP;
	
	
	private Textbox txtNumCheque,txtAutres,txtPayerpar;
	
	
	private Date datejour;

	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE, activePage;

	
	
	//Information generale
	private BigDecimal montanttotal=null;
	private Label lblAC, lblMontant,lblRefAppelOffre;



	 //Editer cheque
	protected Iframe iframeEs2Detached;
	protected static final String EDIT_URL = "http://" + BeConstants.IP_SYGMAP + ":" + BeConstants.PORT_SYGMAP + "/birt/frameset?__report=/etatventedao/";
	private String format;
    private String nomFichier;
	private SygOperationRecouvrement operationrecouvrement = new SygOperationRecouvrement();
	private String numrecu;
	
	// /Banque 
	private SygBanque banque = new SygBanque();
	private Textbox txtRechercherBanque;
	private Bandbox bdBanque;
	private Paging pgBanque;
	private Listbox lstBanque;
	private String LibelleBanque = null;
	
	
	   private String montantenlettre;
	   
	   List<Long> appels = new ArrayList<Long>();
	

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		
		Components.addForwards(this, this);
		
		// champ Banque  du formulaire
		addEventListener(ApplicationEvents.ON_BANQUE, this);
		lstBanque.setItemRenderer(new banqueRenderer());
		pgBanque.setPageSize(byPageBandbox);
		pgBanque.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_BANQUE);
		Events.postEvent(ApplicationEvents.ON_BANQUE, this, null);
   
		//AC
		addEventListener(ApplicationEvents.ON_AUTORITES, this);
		lstAutorite.setItemRenderer(new AutoriteRenderer());
		pgAutorite.setPageSize(byPageBandbox);
		pgAutorite.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTORITES);
		Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);
		
		
		//Appel Offre
		addEventListener(ApplicationEvents.ON_APPELOFFRE, this);
		lstAppeloffre.setItemRenderer(new AppelOffreRenderer());
		pgAppeloffre.setPageSize(byPageBandbox);
		pgAppeloffre.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_APPELOFFRE);
		
		appels=BeanLocator.defaultLookup(AppelsOffresSession.class).findRech();
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	}

	@Override
	public void onEvent(Event event) throws Exception {
		
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTORITES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgAutorite.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_LIST_BY_PAGES;
				activePage = pgAutorite.getActivePage() * byPage;
				pgAutorite.setPageSize(byPage);
			}
			List<SygAutoriteContractante> autorite = BeanLocator.defaultLookup(AutoriteContractanteSession.class).findRecouvrement(activePage, byPage, UIConstants.CONTRAT_NPAYE,denomination);
			lstAutorite.setModel(new SimpleListModel(autorite));
			pgAutorite.setTotalSize(BeanLocator.defaultLookup(AutoriteContractanteSession.class).countRecouvrement(UIConstants.CONTRAT_NPAYE,denomination));
		}
		
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_BANQUE)) {
			List<SygBanque> banques = BeanLocator.defaultLookup(BanqueSession.class).find(pgBanque.getActivePage() * byPageBandbox, byPageBandbox, null, LibelleBanque);
			SimpleListModel listModel = new SimpleListModel(banques);
			lstBanque.setModel(listModel);
			pgBanque.setTotalSize(BeanLocator.defaultLookup(BanqueSession.class).count(null, LibelleBanque));
		}

		
		else if (event.getName().equalsIgnoreCase(Events.ON_CLICK)) {
			String libelleimage = (String) (((Listcell) event.getTarget()).getAttribute("libelleimage"));
			if (libelleimage == "supconteneur") {
				HashMap<String, String> display = new HashMap<String, String>();
				display.put(MessageBoxController.DSP_MESSAGE, " Etes vous sur de vouloir supprimer?");
				display.put(MessageBoxController.DSP_TITLE, "Supprimer le dossier");
				display.put(MessageBoxController.DSP_HEIGHT, "150px");
				display.put(MessageBoxController.DSP_WIDTH, "100px");
				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				showMessageBox(display, map);
			}
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_APPELOFFRE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgAppeloffre.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_LIST_BY_PAGES;
				activePage = pgAppeloffre.getActivePage() * byPage;
				pgAppeloffre.setPageSize(byPage);
			}
			List<SygAppelsOffres> appeloffres = BeanLocator.defaultLookup(AppelsOffresSession.class).findRech(activePage, byPage,UIConstants.CONTRAT_NPAYE,autorite,null);
			lstAppeloffre.setModel(new SimpleListModel(appeloffres));
			pgAppeloffre.setTotalSize(BeanLocator.defaultLookup(AppelsOffresSession.class).countRech(UIConstants.CONTRAT_NPAYE,autorite,null));
		
		}
	}



	// /////////////////
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent createEvent) {
		Map<String, Object> windowParams = (Map<String, Object>) createEvent.getArg();
		mode = (String) windowParams.get(WINDOW_PARAM_MODE);
		if (mode == null) {
			
		} else {
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				code = (Long) windowParams.get(PARAM_WINDOW_CODE);
				recouvrement = BeanLocator.defaultLookup(RecouvrementSession.class).findById(code);
				recouvrements = BeanLocator.defaultLookup(RecouvrementSession.class).find(0, -1, null, null);
				
				txtNumCheque.setValue(recouvrement.getNumCheque());
				//txtBanque.setValue(recouvrement.getBanque());
				bdBanque.setValue(recouvrement.getBanque().getLibelle());

				autorite = recouvrements.get(0).getAutorite();
				
				
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

			}
		}

	}

	private boolean checkFieldConstraints() {

		try {
			 if(txtPayerpar.getValue().equals(""))
		       {
              errorComponent = txtPayerpar;
              errorMsg = Labels.getLabel("kermel.paiement.payepar")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		        }
			if(rdcheque.isChecked()==true)
			{
			  if(txtNumCheque.getValue().equals(""))
		       {
              errorComponent = txtNumCheque;
              errorMsg = Labels.getLabel("kermel.paiement.numcheque")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		        }
			  
			  if(bdBanque.getValue().equals(""))
		       {
             errorComponent = bdBanque;
             errorMsg = Labels.getLabel("kermel.paiement.banque")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		        }
			 }
			
			if(rdautre.isChecked()==true)
			{
			   if(txtAutres.getValue().equals(""))
		       {
              errorComponent = txtAutres;
              errorMsg = Labels.getLabel("kermel.paiement.commentaireautre")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		      }
			}
			
			return true;

		} catch (Exception e) {
			errorMsg = Labels.getLabel("cciad.erreur.erreurinconnue") + ": " + e.toString() + " [checkFieldConstraints]";
			errorComponent = null;
			return false;

		}

	}

	

	
	
	//Step1**********************************************************************************
	
	public void onClick$menuCancelStep1() {
		loadApplicationState("suivi_recouvrement");
		detach();
	}
	
	public void onClick$menuNextStep1() {
		if (lstAutorite.getSelectedItem() == null){
			throw new WrongValueException(lstAutorite, Labels.getLabel("kermel.error.select.item"));
		} else {
			autorite = (SygAutoriteContractante) lstAutorite.getSelectedItem().getValue();
			
			// List<SygAppelsOffres> appeloffres = BeanLocator.defaultLookup(AppelsOffresSession.class).findRech(0, -1,UIConstants.CONTRAT_NPAYE,autorite,null);
			 
			//nbre=appeloffres.size();
			
//			 for (int i = 0; i < appeloffres.size(); i++) {
//				
//				 if((montanttotal!=null)){
//			         montanttotal=montanttotal.add(appeloffres.get(i).getApomontantestime());
//			       
//		           }
//			    else{
//				   montanttotal =appeloffres.get(i).getApomontantestime();
//				   
//			       }
//			 }
			// montanttotal=(BigDecimal) lstAutorite.getSelectedItem().getAttribute("montanttotalappel");
			 
//			 if(montanttotal!=null){
//				 lblMontant.setValue(ToolKermel.format3Decimal(montanttotal));
//			 }
				
			   
			
			
			step1.setVisible(false);
			stepAP.setVisible(true);
			step2.setVisible(false);
			
			if(appels.size()>0)
			    Events.postEvent(ApplicationEvents.ON_APPELOFFRE, this, null);
			
		}
	}
	
	
//stepAP*****************************************************************
	
	public void onClick$menuNextStepAP() {
		if (lstAutorite.getSelectedItem() == null){
			throw new WrongValueException(lstAutorite, Labels.getLabel("kermel.error.select.item"));
		}else if(lstAppeloffre.getSelectedItem() == null){
			throw new WrongValueException(lstAppeloffre, Labels.getLabel("kermel.error.select.item"));
		} else {
			autorite = (SygAutoriteContractante) lstAutorite.getSelectedItem().getValue();
			appeloffre=(SygAppelsOffres) lstAppeloffre.getSelectedItem().getValue();
			
//			lblDenomination.setValue(autorite.getDenomination());
			lblAC.setValue(autorite.getDenomination());
			lblRefAppelOffre.setValue(appeloffre.getAporeference());
			 lblMontant.setValue(ToolKermel.format2Decimal(appeloffre.getApomontantversement()));
//			lblTypeAC2.setValue(autorite.getType().getLibelle());
			  txtPayerpar.setValue(autorite.getDenomination());
			  
			step1.setVisible(false);
		    stepAP.setVisible(false);
		    step2.setVisible(true);
		    step3.setVisible(false);
			
		    
			
		}
	}
	
	public void onClick$menuPreviousStepAP() {
		
		    step1.setVisible(true);
		    stepAP.setVisible(false);
			step2.setVisible(false);
		    step3.setVisible(false);
	}
	
	public void onClick$menuCancelStepAP() {
		
		loadApplicationState("suivi_recouvrement");
		detach();
	}
	
	

	public class AppelOffreRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygAppelsOffres appeloffre = (SygAppelsOffres) data;
			item.setValue(appeloffre);

			Listcell cellReference = new Listcell(appeloffre.getAporeference());
			cellReference.setParent(item);
			
			Listcell cellObjet = new Listcell(appeloffre.getApoobjet());
			cellObjet.setParent(item);
			
			
			Listcell cellModepassation = new Listcell(appeloffre.getModepassation().getLibelle());
			cellModepassation.setParent(item);

			 Listcell cellMontant= new Listcell(ToolKermel.format2Decimal(appeloffre.getApomontantversement().multiply(new BigDecimal(2))));
			 cellMontant.setParent(item);
			
			
			//Montant QuotePartARMP
			 Listcell cellMontantDAO = new Listcell(ToolKermel.format2Decimal(appeloffre.getApomontantversement()));
			 cellMontantDAO.setParent(item);
			 
			
			 
			 
			 Listcell cellNbreDAO = new Listcell(Integer.toString(appeloffre.getApoNbreDAO()));
			 cellNbreDAO.setParent(item);


		}
	}
	
	
	//Fin stepAP*****************************************************************
	
	
//Step2*****************************************************************
	public void onClick$menuNextStep2() {
		if (checkFieldConstraints()) {

			 numrecu="R"+autorite.getId()+BeanLocator.defaultLookup(OperationRecouvrementSession.class).getGeneratedCode(BeConstants.PARAM_NUMRECUVERSEMENT);
			  //numrecu=BeanLocator.defaultLookup(OperationPaiementSession.class).getGeneratedCode(BeConstants.PARAM_NUMRECU);
			
			 //List<SygAppelsOffres> appeloffres = BeanLocator.defaultLookup(AppelsOffresSession.class).findRech(0, -1,UIConstants.CONTRAT_NPAYE,autorite,null);
			 
			  // for (int i = 0; i < appeloffres.size(); i++) {
					
				 //  appeloffre=appeloffres.get(i);
				   
					 recouvrement.setAppeloffre(appeloffre);
					 appeloffre.setApoStatut(UIConstants.CONTRAT_PAYE);
					 datejour = new Date();
					 appeloffre.setApodateversement(datejour);
					 
					 if (((String) radioMpaiement.getSelectedItem().getValue()).equalsIgnoreCase(Labels.getLabel("kermel.paiement.modepaiement.cheque"))) {
							recouvrement.setNatureVersement(Labels.getLabel("kermel.paiement.modepaiement.cheque"));
							
						} else if (((String) radioMpaiement.getSelectedItem().getValue()).equalsIgnoreCase(Labels.getLabel("kermel.paiement.modepaiement.espece"))) {

							recouvrement.setNatureVersement(Labels.getLabel("kermel.paiement.modepaiement.espece"));
							
						}else {

							recouvrement.setNatureVersement(Labels.getLabel("kermel.paiement.modepaiement.autre"));
							
						}
					 
					
					   if(lstBanque.getSelectedItem()!=null){
						  banque = (SygBanque) lstBanque.getSelectedItem().getValue();
						  recouvrement.setBanque(banque);
			              }
							

						    recouvrement.setNumCheque(txtNumCheque.getValue());
							//recouvrement.setBanque(txtBanque.getValue());
						  
						    recouvrement.setAutorite(autorite);
						    recouvrement.setCommentaire(txtAutres.getValue());
						    recouvrement.setOperation(numrecu); 
						  
					 BeanLocator.defaultLookup(AppelsOffresSession.class).update(appeloffre);
					 BeanLocator.defaultLookup(RecouvrementSession.class).save(recouvrement);
					
					//}
			  
			   //Operation
			    operationrecouvrement.setOpemontanttotal(appeloffre.getApomontantversement());
			     montantenlettre=ToolKermel.conversionChiffreLettre(appeloffre.getApomontantversement());
			      operationrecouvrement.setOpmontantTotalEnLettre(montantenlettre);
			   // operationrecouvrement.setOpmontantTotalVerse(montantversetotal);
			    operationrecouvrement.setOpedaterecu(new Date());
			     operationrecouvrement.setOpecode(numrecu);
			    operationrecouvrement.setOperecude(txtPayerpar.getValue());
			    operationrecouvrement.setOpecommentaires(txtAutres.getValue());
			    operationrecouvrement.setAutorite(autorite);
			    operationrecouvrement.setNumeromarche(appeloffre.getAporeference());
			    operationrecouvrement.setObjetmarche(appeloffre.getApoobjet());
			    BeanLocator.defaultLookup(OperationRecouvrementSession.class).save(operationrecouvrement);
			   
			    nomFichier = "recuventedao.rptdesign";
                format = UIConstants.FORMAT_PDF;
                System.out.println(EDIT_URL + nomFichier + "&code=" + numrecu + "&__format=" + format );
                iframeEs2Detached.setSrc(EDIT_URL + nomFichier + "&numrecu=" + numrecu + "&__format=" + format );
			    
			    step1.setVisible(false);
				step2.setVisible(false);
				step3.setVisible(true);
			
//			loadApplicationState("suivi_recouvrement");
//			detach();
			
		}
	}

	public void onClick$menuCancelImprimerEs(){
		loadApplicationState("suivi_recouvrement");
		detach();
      }
	
	
	
	// ////////////////AC///////////////////////////////////////////////////

	public class AutoriteRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygAutoriteContractante autorite = (SygAutoriteContractante) data;
			item.setValue(autorite);

			Listcell cellDenomination = new Listcell(autorite.getDenomination());
			cellDenomination.setParent(item);

			List<SygAppelsOffres> appeloffre = BeanLocator.defaultLookup(AppelsOffresSession.class).findRech(0, -1,UIConstants.CONTRAT_NPAYE,autorite,null);
			montanttotalappel=new BigDecimal(0);
			 for (int i = 0; i < appeloffre.size(); i++) {
				 //montanttotalappel=montanttotalappel.add(appeloffre.get(i).getApomontantestime());
				 montanttotalappel=montanttotalappel.add(appeloffre.get(i).getApomontantversement());
				 
			 }
			 item.setAttribute("montanttotalappel",montanttotalappel);
			 Listcell cellMontant = new Listcell(ToolKermel.format2Decimal(montanttotalappel.multiply(new BigDecimal(2))));
			 cellMontant.setParent(item);
			 
			 Listcell cellMontantDAO = new Listcell(ToolKermel.format2Decimal(montanttotalappel));
			 cellMontantDAO.setParent(item);


		}
	}
	
	
	
	public void onClick$menuCancelStep2() {
	
		loadApplicationState("suivi_recouvrement");
		detach();
	}


	
	public void onClick$menuPreviousStep2() {
		montanttotal=null; 
		
		step1.setVisible(true);
		step2.setVisible(false);
		
	}

	// //////////////////////////////////////////////////

	// /liste autorite
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygFournisseur autorite = (SygFournisseur) data;
		item.setValue(autorite.getId());

	}
	
	
	public void onCheck$radioMpaiement(){
		if(radioMpaiement.getSelectedItem().getValue().equals(Labels.getLabel("kermel.paiement.modepaiement.cheque"))){
			
			txtNumCheque.setDisabled(false);
			bdBanque.setDisabled(false);
			txtAutres.setDisabled(true);
			
		}else if(radioMpaiement.getSelectedItem().getValue().equals(Labels.getLabel("kermel.paiement.modepaiement.espece"))){
			
			txtNumCheque.setDisabled(true);
			bdBanque.setDisabled(true);
			txtAutres.setDisabled(true);
			
		}if(radioMpaiement.getSelectedItem().getValue().equals(Labels.getLabel("kermel.paiement.modepaiement.autre"))){
			
			txtNumCheque.setDisabled(true);
			bdBanque.setDisabled(true);
			txtAutres.setDisabled(false);
		}
	}
	
	// //////////Banque********************
	public void onSelect$lstBanque() {
		bdBanque.setValue((String) lstBanque.getSelectedItem().getAttribute(UIConstants.ATTRIBUTE_LIBELLE));
		bdBanque.close();
	}

	public class banqueRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygBanque banque = (SygBanque) data;
			item.setValue(banque);
			item.setAttribute(UIConstants.ATTRIBUTE_LIBELLE, banque.getLibelle());

			Listcell cellLibelle = new Listcell("");
			if (banque.getLibelle() != null) {
				cellLibelle.setLabel(banque.getLibelle());
			}
			cellLibelle.setParent(item);
			//				

		}
	}

	public void onFocus$txtRechercherBanque() {
		if (txtRechercherBanque.getValue().equalsIgnoreCase(Labels.getLabel("kermel.paiement.banque"))) {
			txtRechercherBanque.setValue("");
		}
	}

	public void onBlur$txtRechercherDecision() {
		if (txtRechercherBanque.getValue().equalsIgnoreCase("")) {
			txtRechercherBanque.setValue(Labels.getLabel("kermel.paiement.banque"));
		}
	}

	public void onClick$btnRechercherDecision() {
		if (txtRechercherBanque.getValue().equalsIgnoreCase(Labels.getLabel("kermel.paiement.banque")) || txtRechercherBanque.getValue().equals("")) {
			LibelleBanque = null;
		} else {
			LibelleBanque = txtRechercherBanque.getValue();
		}
		Events.postEvent(ApplicationEvents.ON_BANQUE, this, null);
	}

	// ////////////////fin ////////////////////////////////
	
//////////////*******************Denomination*******//////////////////////

	// function du bouton recherche par Denomination
	public void onFocus$txtDenomination() {
		if (txtDenomination.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.denomination")))
			txtDenomination.setValue("");

	}

	public void onBlur$txtDenomination() {
		if (txtDenomination.getValue().equals(""))
			txtDenomination.setValue(Labels.getLabel("kermel.referentiel.common.denomination"));
	}

	public void onOK$txtDenomination() {
		onClick$bchercherAC();
	}

	
	public void onClick$bchercherAC() {

		if (txtDenomination.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.denomination")) || txtDenomination.getValue().equals("")) {
			denomination = null;
		} else {
			denomination = txtDenomination.getValue();
		}

		Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);

	}

	public void onOK$bchercherAC() {
		onClick$bchercherAC();
	}

	//////////////*******************Denomination*******//////////////////////

}