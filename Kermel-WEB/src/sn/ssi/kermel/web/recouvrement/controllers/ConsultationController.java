package sn.ssi.kermel.web.recouvrement.controllers;

import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygOperationRecouvrement;
import sn.ssi.kermel.be.paiement.ejb.OperationPaiementSession;
import sn.ssi.kermel.be.recouvrement.ejb.OperationRecouvrementSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;


@SuppressWarnings("serial")
public class ConsultationController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null;
    private Listheader lshCode;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem EDITER_OPDAO;
    private String login,numrecu;
    private Div step0, step1;

    private SygOperationRecouvrement operationrecouvrement = new SygOperationRecouvrement();
	 
	 //Editer cheque
		protected Iframe iframeEs2Detached;
		protected static final String EDIT_URL = "http://" + BeConstants.IP_SYGMAP + ":" + BeConstants.PORT_SYGMAP + "/birt/frameset?__report=/etatventedao/";
		private String format;
	    private String nomFichier;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_OPERATIONDAO);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (EDITER_OPDAO != null) { EDITER_OPDAO.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDITER); }
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_EDITER, this);
		
	
		lshCode.setSortAscending(new FieldComparator("opecode", false));
		lshCode.setSortDescending(new FieldComparator("opecode", true));
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	login = ((String) getHttpSession().getAttribute("user"));
    	
    	
    	
    	
	}
	
	
	
	public void onCreate(CreateEvent event) {

	}

	
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygOperationRecouvrement> operationrecouvrements = BeanLocator.defaultLookup(OperationRecouvrementSession.class).find(activePage,byPage,null,null);
			 SimpleListModel listModel = new SimpleListModel(operationrecouvrements);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup(OperationPaiementSession.class).count(null,null));
		} 
		
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDITER)) {

                  if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
                  
                  operationrecouvrement = (SygOperationRecouvrement) lstListe.getSelectedItem().getValue();
                  numrecu=operationrecouvrement.getOpecode();
                  
                  nomFichier = "recuventedao.rptdesign";
                  format = UIConstants.FORMAT_PDF;
                  System.out.println(EDIT_URL + nomFichier + "&code=" + numrecu + "&__format=" + format );
                  iframeEs2Detached.setSrc(EDIT_URL + nomFichier + "&numrecu=" + numrecu + "&__format=" + format );
                  
			      step0.setVisible(false);
			      step1.setVisible(true);
			
			
		}
		
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygOperationRecouvrement operationrecouvrement = (SygOperationRecouvrement) data;
		item.setValue(operationrecouvrement);

			 
		 Listcell cellCode= new Listcell(operationrecouvrement.getOpecode());
		 cellCode.setParent(item);
		 
		 Listcell cellDate = new Listcell(UtilVue.getInstance().formateLaDate(operationrecouvrement.getOpedaterecu()));
		 cellDate.setParent(item);
		 
		 
		
		 
//		 Listcell cellMontantHVerse= new Listcell(operationrecouvrement.getOpmontantTotalVerse().toString());
//		 cellMontantHVerse.setParent(item);
		 
		 Listcell cellAC= new Listcell(operationrecouvrement.getAutorite().getDenomination());
		 cellAC.setParent(item);
		 
		 Listcell cellMontantHT= new Listcell(ToolKermel.format2Decimal(operationrecouvrement.getOpemontanttotal()));
		 cellMontantHT.setParent(item);
		
	}
	

	public void onClick$menuCancelImprimerEs() {
		
		step0.setVisible(true);
		step1.setVisible(false);
		
		
	}
	


}

