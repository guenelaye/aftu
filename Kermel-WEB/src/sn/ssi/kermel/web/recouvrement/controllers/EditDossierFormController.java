package sn.ssi.kermel.web.recouvrement.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteContractanteSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;

//@SuppressWarnings("serial")
public class EditDossierFormController extends AbstractWindow implements AfterCompose, EventListener, ListitemRenderer {

	private static final long serialVersionUID = 3449680167389488155L;
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String WINDOW_PARAM_MODE = "MODE";

	private String mode;
	private Long code;
	
	List<SygAppelsOffres> paiements = new ArrayList<SygAppelsOffres>();
   

	
	
    //Autorite
	private SygAutoriteContractante autorite = new SygAutoriteContractante();
	
	//Contrat
	//sprivate SygContrats contrat= new SygContrats();
	private Listbox lstListe;
	private Paging pgListe;
	
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	
	List<SygAppelsOffres> listesappels = new ArrayList<SygAppelsOffres>();
	
	
	private Div step1;
	
	
	
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE, activePage;

	
	
	//Information generale

	private Label lblnom;

   
	
	

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		
		Components.addForwards(this, this);

		//Appel offre
		addEventListener(ApplicationEvents.ON_APPELOFFRE, this);
		lstListe.setItemRenderer(new AppelOffreRenderer());
		pgListe.setPageSize(byPageBandbox);
		pgListe.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_APPELOFFRE);
		

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	}

	@Override
	public void onEvent(Event event) throws Exception {

		//Appel offre
		 if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_APPELOFFRE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgListe.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_LIST_BY_PAGES;
				activePage = pgListe.getActivePage() * byPage;
				pgListe.setPageSize(byPage);
			}
			List<SygAppelsOffres> appeloffre = BeanLocator.defaultLookup(AppelsOffresSession.class).findRech(activePage, byPage, UIConstants.CONTRAT_NPAYE,autorite,null);
			
			for(int i=0;i<appeloffre.size();i++)
			{
				if(appeloffre.get(i).getApomontantversement().intValue()!=0)
					listesappels.add(appeloffre.get(i));
			}
			lstListe.setModel(new SimpleListModel(listesappels));
			pgListe.setTotalSize(BeanLocator.defaultLookup(AppelsOffresSession.class).countRech(UIConstants.CONTRAT_NPAYE,autorite,null));
		}


		else if (event.getName().equalsIgnoreCase(Events.ON_CLICK)) {
			String libelleimage = (String) (((Listcell) event.getTarget()).getAttribute("libelleimage"));
			if (libelleimage == "supconteneur") {
				HashMap<String, String> display = new HashMap<String, String>();
				display.put(MessageBoxController.DSP_MESSAGE, " Etes vous sur de vouloir supprimer?");
				display.put(MessageBoxController.DSP_TITLE, "Supprimer le dossier");
				display.put(MessageBoxController.DSP_HEIGHT, "150px");
				display.put(MessageBoxController.DSP_WIDTH, "100px");
				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				showMessageBox(display, map);
			}
		}
	}

	

	// /////////////////
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent createEvent) {
		Map<String, Object> windowParams = (Map<String, Object>) createEvent.getArg();
		mode = (String) windowParams.get(WINDOW_PARAM_MODE);
		if (mode == null) {
			
		} else {
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				code = (Long) windowParams.get(PARAM_WINDOW_CODE);
				autorite = BeanLocator.defaultLookup(AutoriteContractanteSession.class).findById(code);
				lblnom.setValue(autorite.getDenomination());
				step1.setVisible(true);
				//step2.setVisible(false);
				
				Events.postEvent(ApplicationEvents.ON_APPELOFFRE, this, null);
			}
		}

	}


	
	//Step1**********************************************************************************
	
	public void onClick$menuCancelStep1() {
		loadApplicationState("suivi_recouvrement");
		detach();
	}
	

	//Appel offre

	public class AppelOffreRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygAppelsOffres appeloffre = (SygAppelsOffres) data;
			item.setValue(appeloffre);

			Listcell cellReference = new Listcell(appeloffre.getAporeference());
			cellReference.setParent(item);
			
			Listcell cellDate = new Listcell("");
			if(appeloffre.getApodatecreation()!=null)
				cellDate.setLabel(UtilVue.getInstance().formateLaDate(appeloffre.getApodatecreation()));
			cellDate.setParent(item);

			Listcell cellObjet = new Listcell(appeloffre.getApoobjet());
			cellObjet.setParent(item);

//			Listcell cellAC = new Listcell(contrat.getAutorite().getDenomination());
//			cellAC.setParent(item);
			
//			Listcell cellMontant = new Listcell(ToolKermel.format2Decimal(contrat.getMontant()));
//			cellMontant.setParent(item);
			
			Listcell cellMontantVersement = new Listcell(ToolKermel.format2Decimal(appeloffre.getApomontantversement()));
			cellMontantVersement.setParent(item);
			


		}
	}




	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		

	}
	
	

}