package sn.ssi.kermel.web.common.controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import sn.ssi.kermel.web.core.templating.controllers.TemplateWindowInterface;
import sn.ssi.kermel.web.core.templating.exceptions.TemplatingException;

/**
 * 
 * @author BA Alassane Cette classe est la classe m�re de toutes les autres
 *         classes servant de controleurs dans l'application. Elle impl�mente
 *         des comportemments communs � tous les controleurs, comme la
 *         possibilit� d'acc�der � une zone de la fen�tre de l'application.
 */
public class AbstractWindows extends Window {

	Logger logger = Logger.getLogger(AbstractWindows.class);

	AbstractWindows opener = null;
	public static final String DSP_WIDTH = "WIDTH";
	public static final String DSP_HEIGHT = "HEIGHT";
	public static final String DSP_TITLE = "TITLE";

	private TemplateWindowInterface<?> getApplicationWindow() throws TemplatingException {
		TemplateWindowInterface<?> window = null;
		try {
			Page page = (Page) getDesktop().getPages().iterator().next();
			window = (TemplateWindowInterface<?>) page.getFellow("applicationWindow");
		} catch (Exception e) {
			e.printStackTrace();
			throw new TemplatingException();
		}
		return window;

	}

	public void loadApplicationState(String state) {
		try {
			// logger.info("Loading "+state+" state.");
			getApplicationWindow().loadApplicationBookmark(state);
		} catch (TemplatingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param uri
	 * @param arg
	 * @param display
	 * @param parent
	 *            <li>Modifié par Babs pour les besoins de Workflow
	 */
	public void showWindow(String uri, Map arg, HashMap<String, String> display) {
		Window window = (Window) Executions.createComponents(uri, null, arg);

		String width = null;
		String height = null;
		String title = null;
		if (display != null) {
			width = display.get(DSP_WIDTH);
			height = display.get(DSP_HEIGHT);
			title = display.get(DSP_TITLE);
		} else {
			width = "80%";
			height = "600px";
		}
		((AbstractWindows) window).setWidth(width);
		((AbstractWindows) window).setHeight(height);
		window.setTitle(title);
		window.doHighlighted();
		((AbstractWindows) window).setOpener(this.getOpener());

	}

	/**
	 * 
	 * @param uri
	 * @param arg
	 * @param display
	 * @param parent
	 *            <li>Modifié par Babs pour les besoins de Workflow
	 */
	public void showEmbeddedWindow(String uri, Map arg, HashMap<String, String> display, Component parent) {
		// if(parent.getFirstChild()!=null)
		// {
		// parent.getFirstChild().detach();
		// }
		Window window = (Window) Executions.createComponents(uri, parent, arg);
		if (display != null) {
			for (Entry<String, String> entry : display.entrySet()) {
				window.setAttribute(entry.getKey(), entry.getValue());
			}
		}
		// ((AbstractWindow)window).setOpener(this);
		((AbstractWindows) window).setOpener(this.getOpener());
	}

	public void showPopupWindow(String uri, Map arg, HashMap<String, String> display) {
		Window window = (Window) Executions.createComponents(uri, this, arg);

		String width = null;
		String height = null;
		String title = null;
		if (display != null) {
			width = display.get(DSP_WIDTH);
			height = display.get(DSP_HEIGHT);
			title = display.get(DSP_TITLE);
		} else {
			width = "80%";
			height = "600px";
		}
		((AbstractWindows) window).setWidth(width);
		((AbstractWindows) window).setHeight(height);
		window.setTitle(title);
		window.doHighlighted();
		/*
		 * window.smartUpdate("width", width); window.smartUpdate("height",
		 * height);
		 */

		((AbstractWindows) window).setOpener(this);

	}

	public void showPopupWindow(String uri, HashMap<String, String> display) {
		showPopupWindow(uri, null, display);
	}

	public void showMessageBox(HashMap<String, String> display, HashMap<String, Object> inData) {
		Map data = new HashMap<String, Object>();
		data.put("DISPLAY", display);
		data.put("IN_PARAMETERS", inData);
		Window window = (Window) Executions.createComponents("/WEB-INF/components/message_box.zul", this, data);
		window.doHighlighted();
	}

	@SuppressWarnings("unchecked")
	public void showMessageBoxOuiNon(HashMap<String, String> display, HashMap<String, Object> inData) {
		Map data = new HashMap<String, Object>();
		data.put("DISPLAY", display);
		data.put("IN_PARAMETERS", inData);
		Window window = (Window) Executions.createComponents("/WEB-INF/components/messagebox_oui_non.zul", this, data);
		window.doHighlighted();
	}

	public AbstractWindows getOpener() {
		return opener;
	}

	public void setOpener(AbstractWindows opener) {
		this.opener = opener;
	}

	public void alert(String message) {
		Messagebox.show(message);
	}

	public Session getHttpSession() {
		return Sessions.getCurrent();
	}

	public Object getSessionAttribute(String attribute) {
		return getHttpSession().getAttribute(attribute);
	}

}
