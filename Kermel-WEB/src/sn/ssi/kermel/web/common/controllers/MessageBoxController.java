package sn.ssi.kermel.web.common.controllers;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;

import sn.ssi.kermel.web.common.constants.ApplicationEvents;

/**
 * 
 * @author xx Composant d�rivant
 * */
public class MessageBoxController extends AbstractWindow implements
		AfterCompose
{

	public static final String  DSP_MESSAGE="MESSAGE";
	public static final String  DSP_TITLE="TITLE";
	
	Label lbMessage;
	HashMap<String, Object> inParameters;

	public void onOK()
	{
		Events.postEvent(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, getParent(), inParameters);
		detach();
	}

	public void onCancel()
	{
		detach();
	}

	public void afterCompose()
	{
		lbMessage = (Label) getFellow("lbMessage");
	}

	public void onCreate(CreateEvent evt)
	{
		Map initData = evt.getArg();
		HashMap<String, String> display = (HashMap<String, String>) initData
				.get("DISPLAY");
		inParameters = (HashMap<String, Object>) initData.get("IN_PARAMETERS");

		setTitle(display.get("TITLE"));
		lbMessage.setValue(display.get("MESSAGE"));
	}

}
