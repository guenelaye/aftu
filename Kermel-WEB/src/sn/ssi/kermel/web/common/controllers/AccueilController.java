package sn.ssi.kermel.web.common.controllers;

import java.util.List;

import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.validation.Assertion;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.ext.AfterCompose;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.security.ProfilsSession;

public class AccueilController extends AbstractWindow implements AfterCompose {

	
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		Assertion assertion1 = (Assertion) getHttpSession().getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION);
		if (assertion1!=null)
		{
			String login = assertion1.getPrincipal().getName();
			List<Utilisateur> users = BeanLocator.defaultLookup(ProfilsSession.class).findUtilisateur(login);
			if (users.size() > 0) {
				Executions.sendRedirect("/interne/interne.zul");	
			}
		}	

			
	}
}