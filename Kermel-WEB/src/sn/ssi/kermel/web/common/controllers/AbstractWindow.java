package sn.ssi.kermel.web.common.controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import sn.ssi.kermel.web.core.templating.controllers.TemplateWindowInterface;
import sn.ssi.kermel.web.core.templating.exceptions.TemplatingException;

/**
 * 
 * @author BA Alassane Cette classe est la classe m�re de toutes les autres
 *         classes servant de controleurs dans l'application. Elle impl�mente
 *         des comportemments communs � tous les controleurs, comme la
 *         possibilit� d'acc�der � une zone de la fen�tre de l'application.
 */
@SuppressWarnings("serial")
public class AbstractWindow extends Window {

	Logger logger = Logger.getLogger(AbstractWindow.class);

	AbstractWindow opener = null;
	public static final String DSP_WIDTH = "WIDTH";
	public static final String DSP_HEIGHT = "HEIGHT";
	public static final String DSP_TITLE = "TITLE";

	private TemplateWindowInterface<?> getApplicationWindow() throws TemplatingException {
		TemplateWindowInterface<?> window = null;
		try {
			final Page page = (Page) getDesktop().getPages().iterator().next();
			window = (TemplateWindowInterface<?>) page.getFellow("applicationWindow");
		} catch (final Exception e) {
			e.printStackTrace();
			throw new TemplatingException();
		}
		return window;

	}

	protected Listcell addCell(final Listitem parent, final String texte) {
		if (texte == null)
			return null;
		Listcell cell = new Listcell(texte);
		cell.setParent(parent);
		return cell;
	}

	protected Listcell addCell(final Listitem parent, final Integer texte) {
		if (texte == null)
			return null;
		Listcell cell = new Listcell(texte.toString());
		cell.setParent(parent);
		return cell;
	}

	protected Listcell addCell(final Listitem parent, final Long texte) {
		if (texte == null)
			return null;
		Listcell cell = new Listcell(texte.toString());
		cell.setParent(parent);
		return cell;
	}

	public void loadApplicationState(final String state) {
		try {
			// logger.info("Loading "+state+" state.");
			getApplicationWindow().loadApplicationBookmark(state);
		} catch (final TemplatingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param uri
	 * @param arg
	 * @param display
	 * @param parent
	 *            <li>Modifié par Babs pour les besoins de Workflow
	 */
	@SuppressWarnings("unchecked")
	public void showWindow(final String uri, final Map arg, final HashMap<String, String> display) {
		final Window window = (Window) Executions.createComponents(uri, null, arg);

		String width = null;
		String height = null;
		String title = null;
		if (display != null) {
			width = display.get(DSP_WIDTH);
			height = display.get(DSP_HEIGHT);
			title = display.get(DSP_TITLE);
		} else {
			width = "80%";
			height = "600px";
		}
		((AbstractWindow) window).setWidth(width);
		((AbstractWindow) window).setHeight(height);
		window.setTitle(title);
		window.doHighlighted();
		((AbstractWindow) window).setOpener(getOpener());

	}

	/**
	 * 
	 * @param uri
	 * @param arg
	 * @param display
	 * @param parent
	 *            <li>Modifié par Babs pour les besoins de Workflow
	 */
	@SuppressWarnings("unchecked")
	public void showEmbeddedWindow(final String uri, final Map arg, final HashMap<String, String> display, final Component parent) {
		// if(parent.getFirstChild()!=null)
		// {
		// parent.getFirstChild().detach();
		// }
		final Window window = (Window) Executions.createComponents(uri, parent, arg);
		if (display != null)
			for (final Entry<String, String> entry : display.entrySet())
				window.setAttribute(entry.getKey(), entry.getValue());
		// ((AbstractWindow)window).setOpener(this);
		((AbstractWindow) window).setOpener(getOpener());
	}

	@SuppressWarnings("unchecked")
	public void showPopupWindow(final String uri, final Map arg, final HashMap<String, String> display) {
		final Window window = (Window) Executions.createComponents(uri, this, arg);

		String width = null;
		String height = null;
		String title = null;
		if (display != null) {
			width = display.get(DSP_WIDTH);
			height = display.get(DSP_HEIGHT);
			title = display.get(DSP_TITLE);
		} else {
			width = "80%";
			height = "600px";
		}
		((AbstractWindow) window).setWidth(width);
		((AbstractWindow) window).setHeight(height);
		window.setTitle(title);
		window.doHighlighted();
		/*
		 * window.smartUpdate("width", width); window.smartUpdate("height",
		 * height);
		 */

		((AbstractWindow) window).setOpener(this);

	}

	public void showPopupWindow(final String uri, final HashMap<String, String> display) {
		showPopupWindow(uri, null, display);
	}

	@SuppressWarnings("unchecked")
	public void showMessageBox(final HashMap<String, String> display, final HashMap<String, Object> inData) {
		final Map data = new HashMap<String, Object>();
		data.put("DISPLAY", display);
		data.put("IN_PARAMETERS", inData);
		final Window window = (Window) Executions.createComponents("/WEB-INF/components/message_box.zul", this, data);
		window.doHighlighted();
	}

	public AbstractWindow getOpener() {
		return opener;
	}

	public void setOpener(final AbstractWindow opener) {
		this.opener = opener;
	}

	public void alert(final String message) {
		Messagebox.show(message);
	}

	public Session getHttpSession() {
		return Sessions.getCurrent();
	}

	public Object getSessionAttribute(final String attribute) {
		return getHttpSession().getAttribute(attribute);
	}

}
