package sn.ssi.kermel.web.common.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.BookmarkEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Window;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SysFeature;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.security.ProfilsSession;
import sn.ssi.kermel.web.core.constants.ApplicationContextAttributes;
import sn.ssi.kermel.web.core.templating.controllers.TemplateWindowInterface;

public class InterneController extends AbstractWindow implements EventListener,AfterCompose,TemplateWindowInterface<Div>  {

	Label lblConnexion,lbleadmin;

	
	Div spGainde, spKermel, spGed, spEnquetes, spOrbus, spCorus;
	String pfCode, usrPrenom,usrNom;

	public InterneController() {
		addEventListener(Events.ON_BOOKMARK_CHANGE, this);
		
		// footer.setSrc("footer.zul");
	}
	
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		//Assertion assertion1 = (Assertion) getHttpSession().getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION);
		String login = (String) getHttpSession().getAttribute("user");
		List<Utilisateur> users = BeanLocator.defaultLookup(ProfilsSession.class).findUtilisateur(login);
		if (users.size() > 0) {
			final Utilisateur user = users.get(0);
			final Session session = getHttpSession();
			session.setAttribute("user", user.getLogin());
			session.setAttribute("utilisateur", user);
			session.setAttribute("PrenomNom", user.getPrenom() + " " + user.getNom());
			session.setAttribute("profil", user.getSysProfil().getPfCode());
			session.setAttribute("profilLib", user.getSysProfil().getPfLibelle());
			pfCode = (String) user.getSysProfil().getPfCode();
			usrPrenom = user.getPrenom();
			usrNom = user.getNom();
			lblConnexion.setValue("Bienvenue "+usrPrenom+" "+usrNom+", vous avez le profil "+user.getSysProfil().getPfLibelle());
			if(pfCode.equals("ADMIN")){
				lbleadmin.setVisible(true);
				lbleadmin.setStyle("font-weight:bold;");
			}else{
				lbleadmin.setVisible(false);
			}
			
		} else {
			Executions.sendRedirect("/index.zul");
		}
		
		List<SysFeature> features = BeanLocator.defaultLookup(
				ProfilsSession.class)
				.findDisplayableFeaturesForProfilAndModule( pfCode,
						"APPLICATIONS", 0, 0);
		features = removeDoublesFeatures(features);
		for (SysFeature feature : features) {
			if (feature.getFeaCode().equals("GAINDE"))
				spGainde.setVisible(true);
			if (feature.getFeaCode().equals("GRED"))
				spKermel.setVisible(true);
			if (feature.getFeaCode().equals("GED"))
				spGed.setVisible(true);
			if (feature.getFeaCode().equals("ENQUETES"))
				spEnquetes.setVisible(true);
			if (feature.getFeaCode().equals("ORBUS"))
				spOrbus.setVisible(true);
			if (feature.getFeaCode().equals("CORUS"))
				spCorus.setVisible(true);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private List<SysFeature> removeDoublesFeatures(
			final List<SysFeature> features) {
		final ArrayList removables = new ArrayList();
		final ArrayList<SysFeature> finalFeatures = new ArrayList();

		for (int i = 0; i < features.size(); i++)
			if (!inList(features.get(i).getFeaCode(), removables)) {
				finalFeatures.add(features.get(i));
				removables.add(features.get(i).getFeaCode());
			}
		return finalFeatures;
	}
	
	@SuppressWarnings("unchecked")
	private boolean inList(final String code, final List removables) {
		for (int i = 0; i < removables.size(); i++)
			if (removables.get(i).toString().equals(code))
				return true;

		return false;
	}

	@Override
	public void onEvent(Event evt) throws Exception {

		if (evt instanceof BookmarkEvent) {

			BookmarkEvent bookmarkEvent = (BookmarkEvent) evt;

			String bookmark = bookmarkEvent.getBookmark();
			loadApplicationBookmark(bookmark);

		}
		// footer.setSrc("/templates/default/footer.zul");
	}

	@Override
	public Div getTemplateZone(String zone) {

		return (Div) getFellow(zone);
	}

	private void freeZone(Div div) {
		System.out.println("===freeZone===========");
		List components = div.getChildren();
		try {
			for (Iterator iterator = components.iterator(); iterator.hasNext();) {
				Component component = (Component) iterator.next();
				component.detach();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("===fin freeZone===========");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void loadApplicationBookmark(String bookmark) {
		logger.info("Loading " + bookmark + " state.");
		String[] regex = bookmark.split("&");
		String state = regex[0];
		HashMap<String, String> params = new HashMap<String, String>();
		if (regex.length == 2) {
			params.put("params", regex[1]);
		}
		HashMap<String, Object> states = (HashMap<String, Object>) getDesktop()
				.getWebApp().getAttribute(
						ApplicationContextAttributes.APPLICATION_STATES);
		HashMap<String, Object> aState = (HashMap<String, Object>) states
				.get(state);
		Set<String> stateZones = aState.keySet();
		for (String stateZone : stateZones) {

			HashMap<String, Object> stateData = (HashMap<String, Object>) aState
					.get(stateZone);
			String content = (String) stateData.get("content");
			Boolean popup = (Boolean) stateData.get("popup");

			System.out.println("content " + content);
			System.out.println("popup " + popup);

			Div zone = getTemplateZone(stateZone);

			if ((popup == null) || (popup == false)) {
				try {
					freeZone(zone);
					Window w = (Window) Executions.createComponents(content,
							zone, params);
					w.setVisible(true);
				} catch (Exception e) {
				} 
			} else {
				Window w = (Window) Executions.createComponents(content, zone,
						params);
				w.setVisible(true);
				w.doHighlighted();
				w.setClosable(true);
			}

			getDesktop().setBookmark(bookmark);
		}
	}

}