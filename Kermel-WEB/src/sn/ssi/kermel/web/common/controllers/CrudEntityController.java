package sn.ssi.kermel.web.common.controllers;

import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.persistence.CrudEntitySession;
import sn.ssi.kermel.be.common.persistence.Entity;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;

@SuppressWarnings("serial")
public abstract class CrudEntityController<E extends Entity, R extends CrudEntitySession<E>> extends AbstractWindow implements AfterCompose, EventListener, ListitemRenderer {

	public static String WINDOW_PARAM_CODE = "code";
	protected static String WINDOW_PARAM_MODE = "MODE";
	protected static String WINDOW_PARAM_OBJET = "OBJET";
	protected static String WINDOW_PARAM_CCR = "CODE";
	public static String WINDOW_PARAM_CODE_AMODOFIER = "idmodif";

	protected Paging pgPagination;
	protected E entity;
	protected List<E> entities;
	protected R entitySessionBean;
	protected Class<R> beanSessionRef;
	protected Listbox lstListe;
	protected final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;

	protected KermelSousMenu monSousMenu;
	protected String feaCode;
	public static String code;

	public void onClick$Close() {
		detach();
	}

	protected void preserveSelectItem() {
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));

	}

	public void afterCompose() {
		Components.wireFellows(this, this);
		if (lstListe != null) {
			lstListe.setItemRenderer(this);
		}

		if (monSousMenu != null) {
			monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
			monSousMenu.setFea_code(feaCode);
			monSousMenu.afterCompose();
		}
		Components.wireFellows(this, this);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		if (pgPagination != null) {
			pgPagination.setPageSize(byPage);
			pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		Components.addForwards(this, this);
	}

	protected E save() {
		BeanLocator.defaultLookup(beanSessionRef).saveOrUpdate(entity);
		return entity;
	}

	protected E save(final String paramGal) {

		R beanSession = BeanLocator.defaultLookup(beanSessionRef);

		beanSession.setParametresGenerauxSession();
		beanSession.saveOrUpdate(entity, paramGal);

		return entity;
	}

	protected E delete() {
		//BeanLocator.defaultLookup(TypeActeSession.class).delete(entity.getCode());
		return entity;
	}

	protected void paginateEntites() {

		if (entities != null) {
			lstListe.setModel(new SimpleListModel(entities));
			pgPagination.setTotalSize(entities.size());
		}
	}

	protected void paginate() {
		entities = BeanLocator.defaultLookup(beanSessionRef).find(pgPagination.getActivePage() * byPage, byPage);
		if (entities != null) {
			lstListe.setModel(new SimpleListModel(entities));
			pgPagination.setTotalSize(BeanLocator.defaultLookup(beanSessionRef).countAll());
		}
	}

	protected R getEntitySessionBean() {
		return BeanLocator.defaultLookup(beanSessionRef);
	}

	protected void findEntitySessionBean() {
		entitySessionBean = BeanLocator.defaultLookup(beanSessionRef);
	}

	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEvent(Event event) throws Exception {
		// TODO Auto-generated method stub

	}

}
