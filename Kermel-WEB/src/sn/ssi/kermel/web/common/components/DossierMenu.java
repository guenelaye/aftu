package sn.ssi.kermel.web.common.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Menu;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;

import sn.ssi.kermel.be.common.utils.BeanLocator;
//import sn.ssi.kermel.be.entity.KermelDemande;
import sn.ssi.kermel.be.entity.SysFeature;
import sn.ssi.kermel.be.entity.SysModule;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.workflow.ejb.WorkflowSession;
import sn.ssi.kermel.be.workflow.entity.SysArrow;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class DossierMenu extends Menu implements AfterCompose, EventListener {
	private static final String DSP_WIDTH = null;
	private static final String DSP_HEIGHT = null;
	private static final String DSP_TITLE = null;
	String profil, state/**, dosCode*/;
	Long dosCode;

	public String getState() {
		return state;
	}

	public Long getDosCode() {
		return dosCode;
	}

	public void setDosCode(Long dosCode) {
		this.dosCode = dosCode;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getProfil() {
		return profil;
	}

	public void setProfil(String profil) {
		this.profil = profil;
	}

	private boolean inList(String code, List removables) {
		for (int i = 0; i < removables.size(); i++)
			if (removables.get(i).toString().equals(code))
				return true;

		return false;
	}

	private List<SysModule> removeDoubles(List<SysModule> modules) {
		ArrayList removables = new ArrayList();
		ArrayList<SysModule> finalModules = new ArrayList();

		for (int i = 0; i < modules.size(); i++) {
			if (!inList(modules.get(i).getModCode(), removables)) {
				finalModules.add(modules.get(i));
				removables.add(modules.get(i).getModCode());
			}
		}
		return finalModules;
	}

	private List<SysFeature> removeDoublesFeatures(List<SysFeature> features) {
		ArrayList removables = new ArrayList();
		ArrayList<SysFeature> finalFeatures = new ArrayList();

		for (int i = 0; i < features.size(); i++) {
			if (!inList(features.get(i).getFeaCode(), removables)) {
				finalFeatures.add(features.get(i));
				removables.add(features.get(i).getFeaCode());
			}
		}
		return finalFeatures;
	}

	public void redraw() {

		// TODO Auto-generated method stub
		if (this.getMenupopup() != null) {
			this.getMenupopup().detach();
		}
		Menupopup menupopup = new Menupopup();
		menupopup.setParent(this);
		/*
		 * On recherche les modules et on les consid�re comme des menus
		 */
		Session session = getHttpSession();
		Utilisateur user = (Utilisateur) session.getAttribute("utilisateur");
		List<SysArrow> arrows = BeanLocator
				.defaultLookup(WorkflowSession.class).findStateArrows(
						user.getId(), state);
		for (SysArrow arrow : arrows) {
			Menuitem menuitem = new Menuitem(arrow.getLibelle());
			menuitem.setAttribute("state", "dossier");
			menuitem.setAttribute("arrowCode", arrow.getCode());
			menuitem.setImage("/images/imputer.png");
			menuitem.setId(arrow.getCode());
			menuitem.addEventListener(Events.ON_CLICK, this);
			menuitem.setParent(menupopup);

		}
	}

	@Override
	public void afterCompose() {
		redraw();
	}

	@Override
	public void onEvent(Event event) throws Exception {
		final String uri = "/traitementdossier/imputation.zul";
		final HashMap<String, String> display = new HashMap<String, String>();
		display.put("WIDTH", "80%");
		display.put("HEIGHT", "600px");
		display.put("TITLE", "Saisir Prestataires");
			
		
		String status = state.substring(state.length()-8);
		System.out.println(status+ "==============##################");
			
		
		final HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("dosCode", dosCode);
		data.put("arrowCode", event.getTarget().getAttribute("arrowCode"));
		data.put("profil", profil);

		((AbstractWindow) getSpaceOwner()).showPopupWindow(uri, data, display);

	}

	public Session getHttpSession() {
		return Sessions.getCurrent();
	}

	public Object getSessionAttribute(final String attribute) {
		return getHttpSession().getAttribute(attribute);
	}
}
