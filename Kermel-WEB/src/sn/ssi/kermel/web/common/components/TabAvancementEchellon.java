package sn.ssi.kermel.web.common.components;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabAvancementEchellon extends org.zkoss.zul.Tabbox {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void onCreate() {
	}

	// Pour le chargement la demande
	public void onSelect() {

		Tab item = getSelectedTab();
		String code = (String) getAttribute("code");
		if ((item != null) && item.getId().equals("TAB_AGENTS")) {
			Include inc = (Include) this.getFellowIfAny("incAgents");
			inc.setSrc("/avancementechellon/listagent.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_PACTES")) {
			Include inc = (Include) this.getFellowIfAny("incProjetacte");
			inc.setSrc("/avancementechellon/projetacte.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_PJ")) {
			Include inc = (Include) this.getFellowIfAny("incPj");
			inc.setSrc("/avancementechellon/pj.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_IMPUTATION")) {
			Include inc = (Include) this.getFellowIfAny("incImputation");
			inc.setSrc("/avancementechellon/imputation.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_COURRIER")) {
			Include inc = (Include) this.getFellowIfAny("incCourrier");
			inc.setSrc("/avancementechellon/courriers.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_COMPLRMRNT")) {
			Include inc = (Include) this.getFellowIfAny("incComplement");
			inc.setSrc("/avancementechellon/compositiondossier.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_AMPLIATION")) {
			Include inc = (Include) this.getFellowIfAny("incAmpliation");
			inc.setSrc("/avancementechellon/ampliation.zul?code=" + code);
		}

	}
}