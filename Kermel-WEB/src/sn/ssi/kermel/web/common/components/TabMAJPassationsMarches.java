package sn.ssi.kermel.web.common.components;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabMAJPassationsMarches extends org.zkoss.zul.Tabbox {
    
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public void onCreate() {
	}
    
	// Pour le chargement  la demande
	public void onSelect() {
        
		Tab item = getSelectedTab();
		
	
		if(item != null && item.getId().equals("TAB_REALTRAV")) {
			Include inc = (Include) this.getFellowIfAny("incREALTRAV");
            inc.setSrc("/plansdepassation/listmajrealisationstravaux.zul");
        }
		if(item != null && item.getId().equals("TAB_REALSFOURN")) {
			Include inc = (Include) this.getFellowIfAny("incREALSFOURN");
            inc.setSrc("/plansdepassation/listmajrealisationsfournitures.zul");
        }
		
		if(item != null && item.getId().equals("TAB_REALSPI")) {
			Include inc = (Include) this.getFellowIfAny("incREALSPI");
            inc.setSrc("/plansdepassation/listmajrealisationspi.zul");
        }
		if(item != null && item.getId().equals("TAB_REALSSERV")) {
			Include inc = (Include) this.getFellowIfAny("incREALSSERV");
            inc.setSrc("/plansdepassation/listmajrealisationsserv.zul");
        }
		if(item != null && item.getId().equals("TAB_REALSDSP")) {
			Include inc = (Include) this.getFellowIfAny("incREALSDSP");
            inc.setSrc("/plansdepassation/listmajrealisationsdsp.zul");
        }
		
    }   
}