package sn.ssi.kermel.web.common.components;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabAOOprequalification extends org.zkoss.zul.Tabbox {
    
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public void onCreate() {
		
	}
    
	// Pour le chargement  la demande
	public void onSelect() {
        
		Tab item = getSelectedTab();
		
		if(item != null && item.getId().equals("TAB_infosgenerales")) {
			Include inc = (Include) this.getFellowIfAny("incinfosgenerales");
            inc.setSrc("/passationsmarches/aooprequalification/infogenerales.zul");
        }
		if(item != null && item.getId().equals("TAB_garanties")) {
			Include inc = (Include) this.getFellowIfAny("incgaranties");
            inc.setSrc("/passationsmarches/aooprequalification/garantis.zul");
        }
		if(item != null && item.getId().equals("TAB_piecesadministratives")) {
			Include inc = (Include) this.getFellowIfAny("incpiecesadministratives");
            inc.setSrc("/passationsmarches/aooprequalification/piecesadministratives.zul");
        }
		if(item != null && item.getId().equals("TAB_criteresqualifications")) {
			Include inc = (Include) this.getFellowIfAny("inccriteresqualifications");
            inc.setSrc("/passationsmarches/aooprequalification/criteresqualifications.zul");
        }
		if(item != null && item.getId().equals("TAB_devise")) {
			Include inc = (Include) this.getFellowIfAny("incdevise");
            inc.setSrc("/passationsmarches/aooprequalification/devise.zul");
        }
		if(item != null && item.getId().equals("TAB_financement")) {
			Include inc = (Include) this.getFellowIfAny("incfinancement");
            inc.setSrc("/passationsmarches/aooprequalification/financements.zul");
        }
		if(item != null && item.getId().equals("TAB_listepresencemembrescommissions")) {
			Include inc = (Include) this.getFellowIfAny("inclistepresencemembrescommissions");
            inc.setSrc("/passationsmarches/aooprequalification/listepresencemembrescommissions.zul");
        }
		if(item != null && item.getId().equals("TAB_representantssoumissionnaires")) {
			Include inc = (Include) this.getFellowIfAny("increpresentantssoumissionnaires");
            inc.setSrc("/passationsmarches/aooprequalification/representantssoumissionnaires.zul");
        }
		if(item != null && item.getId().equals("TAB_representantsservicestechniques")) {
			Include inc = (Include) this.getFellowIfAny("increpresentantsservicestechniques");
            inc.setSrc("/passationsmarches/aooprequalification/representantsservicestechniques.zul");
        }
		if(item != null && item.getId().equals("TAB_observateursindependants")) {
			Include inc = (Include) this.getFellowIfAny("incobservateursindependants");
            inc.setSrc("/passationsmarches/aooprequalification/observateursindependants.zul");
        }
		if(item != null && item.getId().equals("TAB_garantiesoumission")) {
			Include inc = (Include) this.getFellowIfAny("incgarantiesoumission");
            inc.setSrc("/passationsmarches/aooprequalification/garantiesoumission.zul");
        }
		if(item != null && item.getId().equals("TAB_piecessoumissionnaires")) {
			Include inc = (Include) this.getFellowIfAny("incpiecessoumissionnaires");
            inc.setSrc("/passationsmarches/aooprequalification/piecessoumissionnaires.zul");
        }
		if(item != null && item.getId().equals("TAB_compositioncommissiontechnique")) {
			Include inc = (Include) this.getFellowIfAny("inccompositioncommissiontechnique");
            inc.setSrc("/passationsmarches/aooprequalification/compositioncommissiontechnique.zul");
        }
		if(item != null && item.getId().equals("TAB_lecturesoffres")) {
			Include inc = (Include) this.getFellowIfAny("inclecturesoffres");
            inc.setSrc("/passationsmarches/aooprequalification/formlecturesoffres.zul");
        }
		if(item != null && item.getId().equals("TAB_procesverbalouverture")) {
			Include inc = (Include) this.getFellowIfAny("incprocesverbalouverture");
            inc.setSrc("/passationsmarches/aooprequalification/procesverbalouverture.zul");
        }
		if(item != null && item.getId().equals("TAB_formtransmissiondossier")) {
			Include inc = (Include) this.getFellowIfAny("incformtransmissiondossier");
            inc.setSrc("/passationsmarches/aooprequalification/formtransmissiondossier.zul");
        }
		if(item != null && item.getId().equals("TAB_controlegarantie")) {
			Include inc = (Include) this.getFellowIfAny("inccontrolegarantie");
            inc.setSrc("/passationsmarches/aooprequalification/controlegarantie.zul");
        }
		if(item != null && item.getId().equals("TAB_verificationconformite")) {
			Include inc = (Include) this.getFellowIfAny("incverificationconformite");
            inc.setSrc("/passationsmarches/aooprequalification/formverificationconformite.zul");
        }
		if(item != null && item.getId().equals("TAB_correctionoffres")) {
			Include inc = (Include) this.getFellowIfAny("inccorrectionoffres");
            inc.setSrc("/passationsmarches/aooprequalification/formcorrectionoffre.zul");
        }
		if(item != null && item.getId().equals("TAB_vercriteresqualification")) {
			Include inc = (Include) this.getFellowIfAny("incvercriteresqualification");
            inc.setSrc("/passationsmarches/aooprequalification/vercriteresqualification.zul");
        }
		if(item != null && item.getId().equals("TAB_classementfinal")) {
			Include inc = (Include) this.getFellowIfAny("incclassementfinal");
            inc.setSrc("/passationsmarches/aooprequalification/formclassementfinal.zul");
        }
		if(item != null && item.getId().equals("TAB_rapportevaluation")) {
			Include inc = (Include) this.getFellowIfAny("incrapportevaluation");
            inc.setSrc("/passationsmarches/aooprequalification/rapportevaluation.zul");
        }
		if(item != null && item.getId().equals("TAB_ordredeservicededemarrage")) {
			Include inc = (Include) this.getFellowIfAny("incordredeservicededemarrage");
            inc.setSrc("/passationsmarches/aooprequalification/formordredeservicededemarrage.zul");
        }
		if(item != null && item.getId().equals("TAB_receptionprovisoire")) {
			Include inc = (Include) this.getFellowIfAny("increceptionprovisoire");
            inc.setSrc("/passationsmarches/aooprequalification/formreceptionprovisoire.zul");
        }
		if(item != null && item.getId().equals("TAB_receptiondefinitive")) {
			Include inc = (Include) this.getFellowIfAny("increceptiondefinitive");
            inc.setSrc("/passationsmarches/aooprequalification/formreceptiondefinitive.zul");
        }
		if(item != null && item.getId().equals("TAB_allotissement")) {
			Include inc = (Include) this.getFellowIfAny("incallotissement");
            inc.setSrc("/passationsmarches/aooprequalification/allotissement.zul");
        }
		if(item != null && item.getId().equals("TAB_infosgeneralesdeux")) {
			Include inc = (Include) this.getFellowIfAny("incinfosgeneralesdeux");
            inc.setSrc("/passationsmarches/aooprequalification/infogeneralesdeux.zul");
        }
		if(item != null && item.getId().equals("TAB_piecesadministrativesdeux")) {
			Include inc = (Include) this.getFellowIfAny("incpiecesadministrativesdeux");
            inc.setSrc("/passationsmarches/aooprequalification/piecesadministrativesdeux.zul");
        }
		if(item != null && item.getId().equals("TAB_criteresqualificationsdeux")) {
			Include inc = (Include) this.getFellowIfAny("inccriteresqualificationsdeux");
            inc.setSrc("/passationsmarches/aooprequalification/criteresqualificationsdeux.zul");
        }
		if(item != null && item.getId().equals("TAB_allotissementdeux")) {
			Include inc = (Include) this.getFellowIfAny("incallotissementdeux");
            inc.setSrc("/passationsmarches/aooprequalification/allotissementdeux.zul");
        }
    }   
}