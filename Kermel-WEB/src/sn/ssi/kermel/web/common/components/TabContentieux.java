package sn.ssi.kermel.web.common.components;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabContentieux extends org.zkoss.zul.Tabbox {
    
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void onCreate() {
	}
    
	// Pour le chargement  la demande
	public void onSelect() {
        
		Tab item = getSelectedTab();
		
		if(item != null && item.getId().equals("TAB_DETAILS")) {
			Include inc = (Include) this.getFellowIfAny("incDETAILS");
            inc.setSrc("/contentieux/dossiers/details.zul");
        }
		
		if(item != null && item.getId().equals("TAB_RECEVABILITE")) {
			Include inc = (Include) this.getFellowIfAny("incRECEVABILITE");
            inc.setSrc("/contentieux/dossiers/recevabilite.zul");
        }
		
		if(item != null && item.getId().equals("TAB_RAPPORT")) {
			Include inc = (Include) this.getFellowIfAny("incRAPPORT");
            inc.setSrc("/contentieux/dossiers/rapport.zul");
        }
		
		if(item != null && item.getId().equals("TAB_DECISION")) {
			Include inc = (Include) this.getFellowIfAny("incDECISION");
            inc.setSrc("/contentieux/dossiers/decision.zul");
        }
		
		if(item != null && item.getId().equals("TAB_COURRIER")) {
			Include inc = (Include) this.getFellowIfAny("incCOURRIER");
            inc.setSrc("/contentieux/dossiers/courriers.zul");
        }
		
		if(item != null && item.getId().equals("TAB_PJ")) {
			Include inc = (Include) this.getFellowIfAny("incPJ");
            inc.setSrc("/contentieux/dossiers/piecejointe.zul");
        }
		
    }   
}