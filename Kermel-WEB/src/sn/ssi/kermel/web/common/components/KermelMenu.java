package sn.ssi.kermel.web.common.components;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Menu;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Menupopup;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SysFeature;
import sn.ssi.kermel.be.entity.SysModule;
import sn.ssi.kermel.be.security.ProfilsSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class KermelMenu extends Menu implements AfterCompose, EventListener {
	String profil;

	public String getProfil() {
		return profil;
	}

	public void setProfil(final String profil) {
		this.profil = profil;
	}

	@SuppressWarnings("unchecked")
	private boolean inList(final String code, final List removables) {
		for (int i = 0; i < removables.size(); i++)
			if (removables.get(i).toString().equals(code))
				return true;

		return false;
	}

	@SuppressWarnings("unchecked")
	private List<SysModule> removeDoubles(final List<SysModule> modules) {
		final ArrayList removables = new ArrayList();
		final ArrayList<SysModule> finalModules = new ArrayList();

		for (int i = 0; i < modules.size(); i++)
			if (!inList(modules.get(i).getModCode(), removables)) {
				finalModules.add(modules.get(i));
				removables.add(modules.get(i).getModCode());
			}
		return finalModules;
	}

	@SuppressWarnings("unchecked")
	private List<SysFeature> removeDoublesFeatures(
			final List<SysFeature> features) {
		final ArrayList removables = new ArrayList();
		final ArrayList<SysFeature> finalFeatures = new ArrayList();

		for (int i = 0; i < features.size(); i++)
			if (!inList(features.get(i).getFeaCode(), removables)) {
				finalFeatures.add(features.get(i));
				removables.add(features.get(i).getFeaCode());
			}
		return finalFeatures;
	}

	public void redraw() {

		// TODO Auto-generated method stub
		if (getMenupopup() != null) {
			getMenupopup().detach();
		}
		final Menupopup menupopup = new Menupopup();
		menupopup.setParent(this);
		/*
		 * On recherche les modules et on les consid�re comme des menus
		 */
		List<SysModule> modules = BeanLocator.defaultLookup(
				ProfilsSession.class).findDisplayableModulesForProfil(profil,
				0, 0);
		modules = removeDoubles(modules);
		Locale local = (Locale) getHttpSession().getAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE);;
		
		if(local!=null )
			System.out.println(local);
		for (final SysModule module : modules) {
			String libelle = null;
			if(local!=null  && local.getLanguage().equalsIgnoreCase("fr")){
				libelle = module.getModLibelle();
			}else if(local!=null  && local.getLanguage().equalsIgnoreCase("en")){
				libelle = module.getModLibelle_en();
			}else if(local!=null  && local.getLanguage().equalsIgnoreCase("es")){
				libelle = module.getModLibelle_es();
			}
			final Menu menu = new Menu(libelle);
			menu.setParent(menupopup);
			menu.setImage("images/mnu.png");
			/*
			 * On recherche les actions accessibles au profil et on les
			 * consid�re comme des sous menus
			 */
			List<SysFeature> features = BeanLocator.defaultLookup(
					ProfilsSession.class)
					.findDisplayableFeaturesForProfilAndModule(profil,
							module.getModCode(), 0, 0);
			features = removeDoublesFeatures(features);
			final Menupopup _menupopup = new Menupopup();
			_menupopup.setParent(menu);
			for (final SysFeature feature : features) {
				String libellef = null;
				if(local!=null  && local.getLanguage().equalsIgnoreCase("fr")){
					libellef = feature.getFeaLibelle();
				}else if(local!=null  && local.getLanguage().equalsIgnoreCase("en")){
					libellef = feature.getFeaLibelle_en();
				}else if(local!=null  && local.getLanguage().equalsIgnoreCase("es")){
					libellef = feature.getFeaLibelle_es();
				}
				final Menuitem menuitem = new Menuitem(libellef);
				menuitem
						.setAttribute("state", feature.getFeaApplicationstate());
				menuitem.setImage("images/mnu1.png");
				menuitem.addEventListener(Events.ON_CLICK, this);
				menuitem.setParent(_menupopup);
			}
		}
	}

	@Override
	public void afterCompose() {
		redraw();
	}

	@Override
	public void onEvent(final Event event) throws Exception {
		final String state = (String) event.getTarget().getAttribute("state");
		((AbstractWindow) getSpaceOwner()).loadApplicationState(state);
	}
	public Session getHttpSession() {
		return Sessions.getCurrent();
	}
}
