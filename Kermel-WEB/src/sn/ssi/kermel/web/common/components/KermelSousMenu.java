package sn.ssi.kermel.web.common.components;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Menubar;
import org.zkoss.zul.Menuitem;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SysAction;
import sn.ssi.kermel.be.security.ProfilsSession;

public class KermelSousMenu extends Menubar implements AfterCompose, EventListener {

	String profil;
	String fea_code;

	public String getFea_code() {
		return fea_code;
	}

	public void setFea_code(String feaCode) {
		fea_code = feaCode;
	}

	public String getProfil() {
		return profil;
	}

	public void setProfil(String profil) {
		this.profil = profil;
	}

	private boolean inList(String code, List removables) {
		for (int i = 0; i < removables.size(); i++)
			if (removables.get(i).toString().equals(code))
				return true;

		return false;
	}

	private List<SysAction> removeActionDoubles(List<SysAction> actions) {
		ArrayList removables = new ArrayList();
		ArrayList<SysAction> finalActions = new ArrayList();

		for (int i = 0; i < actions.size(); i++) {
			if (!inList(actions.get(i).getActCode(), removables)) {
				finalActions.add(actions.get(i));
				removables.add(actions.get(i).getActCode());
			}
		}
		return finalActions;
	}

	public void redraw() {
		if (fea_code != null) {
			String[] liste = fea_code.split(",");
			List<SysAction> actions = new ArrayList();
			for (String s : liste) {
				actions.addAll(BeanLocator.defaultLookup(ProfilsSession.class)
						.findDisplayableActionsForProfil(profil, s));
			}
			actions = removeActionDoubles(actions);
			for (SysAction action : actions) {
				Menuitem menuitem = new Menuitem(action.getActLibelle());
				menuitem.setId(action.getActCode());
				menuitem.setParent(this);
				menuitem.setImage("/images/" + action.getActImage());
				// menuitem.setAttribute("state",
				// action.getActApplicationstate());
				menuitem.addEventListener(Events.ON_CLICK, this);
			}
		}
	}

	@Override
	public void afterCompose() {
		redraw();
	}

	/*
	 * @Override public void onEvent(Event event) throws Exception { String
	 * state = (String) event.getTarget().getAttribute("state");
	 * ((AbstractWindow)getSpaceOwner()).loadApplicationState(state); }
	 */

	@Override
	public void onEvent(Event event) throws Exception {

	}

}