package sn.ssi.kermel.web.common.components;

import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabMateriel extends org.zkoss.zul.Tabbox {

	private static final long serialVersionUID = 1L;

	public void onCreate() {
	}

	// Pour le chargement la demande
	public void onSelect() {

		Tab item = getSelectedTab();
		String code = (String) getAttribute("code");
		if ((item != null) && item.getId().equals("TAB_PRODUIT")) {
			Include inc = (Include) this.getFellowIfAny("incProduit");
			inc.setSrc("/demande/produit.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_VEHICULE")) {
			Include inc = (Include) this.getFellowIfAny("incVehicule");
			inc.setSrc("/demande/vehicule.zul?code=" + code);
			
		}



	}

	public Session getHttpSession() {
		return Sessions.getCurrent();
	}

	public Object getSessionAttribute(final String attribute) {
		return getHttpSession().getAttribute(attribute);
	}
}