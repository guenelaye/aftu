package sn.ssi.kermel.web.common.components;

import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabSuiviAudits extends org.zkoss.zul.Tabbox {

	private static final long serialVersionUID = 1L;

	
	public void onCreate() {
	}

	// Pour le chargement la demande
	public void onSelect() {

		Tab item = getSelectedTab();
		if ((item != null) && item.getId().equals("TAB_Programmation")) {
			Include inc = (Include) this.getFellowIfAny("incProgrammation");
		    inc.setSrc("/audits/programmation.zul");
		}
		if ((item != null) && item.getId().equals("TAB_VersionProvisoireRapport")) {
			Include inc = (Include) this.getFellowIfAny("incVersionProvisoireRapport");
		    inc.setSrc("/audits/rapportprovisoire.zul");
		}
		if ((item != null) && item.getId().equals("TAB_ObservationAutoriteContractante")) {
			Include inc = (Include) this.getFellowIfAny("incObservationAutoriteContractante");
		    inc.setSrc("/audits/observation.zul");
		}
		if ((item != null) && item.getId().equals("TAB_VersionDefinitif")) {
			Include inc = (Include) this.getFellowIfAny("incVersionDefinitif");
		    inc.setSrc("/audits/rapportdefinitif.zul");
		}
	
	

	

	}

	public Session getHttpSession() {
		return Sessions.getCurrent();
	}

	public Object getSessionAttribute(final String attribute) {
		return getHttpSession().getAttribute(attribute);
	}
}