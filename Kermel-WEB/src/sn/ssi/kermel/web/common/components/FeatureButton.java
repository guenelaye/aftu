package sn.ssi.kermel.web.common.components;

import java.io.IOException;
import java.io.Writer;

import org.zkoss.zul.Toolbarbutton;

@SuppressWarnings("serial")
public class FeatureButton extends Toolbarbutton {

	String feature;

	/*
	 * SecuritySession securitySession = BeanLocator
	 * .defaultLookup(SecuritySession.class);
	 */

	public String getFeature() {
		return feature;
	}

	public void setFeature(final String feature) {
		this.feature = feature;
	}

	@Override
	public void redraw(final Writer out) throws IOException {
		/*
		 * ActorSession actorSession = (ActorSession) Sessions.getCurrent()
		 * .getAttribute(HttpSessionAttributes.CURRENT_ACTOR_SESSION); if
		 * (feature != null) { boolean visible =
		 * securitySession.isFeatureOn(actorSession .getActor().getId(),
		 * feature); if (visible == true) super.redraw(out); } else {
		 * super.redraw(out); }
		 */
		super.redraw(out);
	}
}
