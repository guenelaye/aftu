package sn.ssi.kermel.web.common.components;

import sn.ssi.kermel.web.common.controllers.AbstractWindow;

/**
 * 
 * @author xx
 * Composant d�rivant de Toolbarbutton et qui permet de charger automatiquement un �tat.
 */
public class StateButton extends FeatureButton {
	
	String state;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public void onClick()
	{
		((AbstractWindow)getSpaceOwner()).loadApplicationState(state);
	}
}
