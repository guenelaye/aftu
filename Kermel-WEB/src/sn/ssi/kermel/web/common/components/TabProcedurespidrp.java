package sn.ssi.kermel.web.common.components;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabProcedurespidrp extends org.zkoss.zul.Tabbox {
    
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void onCreate() {
	}
    
	// Pour le chargement  la demande
	public void onSelect() {
        
		Tab item = getSelectedTab();
		
		if(item != null && item.getId().equals("TAB_infosgenerales")) {
			Include inc = (Include) this.getFellowIfAny("incinfosgenerales");
            inc.setSrc("/passationsmarches/pidrp/infogenerales.zul");
        }
	
		if(item != null && item.getId().equals("TAB_piecesadministratives")) {
			Include inc = (Include) this.getFellowIfAny("incpiecesadministratives");
            inc.setSrc("/passationsmarches/pidrp/piecesadministratives.zul");
        }
		if(item != null && item.getId().equals("TAB_criteresqualifications")) {
			Include inc = (Include) this.getFellowIfAny("inccriteresqualifications");
            inc.setSrc("/passationsmarches/pidrp/criteresqualifications.zul");
        }
		if(item != null && item.getId().equals("TAB_devise")) {
			Include inc = (Include) this.getFellowIfAny("incdevise");
            inc.setSrc("/passationsmarches/pidrp/devise.zul");
        }
	
		if(item != null && item.getId().equals("TAB_presentationoffres")) {
			Include inc = (Include) this.getFellowIfAny("incpresentationoffres");
            inc.setSrc("/passationsmarches/pidrp/presentationsoffres.zul");
        }
	


    }   
}