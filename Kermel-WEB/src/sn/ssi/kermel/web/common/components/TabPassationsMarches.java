package sn.ssi.kermel.web.common.components;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabPassationsMarches extends org.zkoss.zul.Tabbox {
    
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void onCreate() {
	}
    
	// Pour le chargement  la demande
	public void onSelect() {
        
		Tab item = getSelectedTab();
		
		if(item != null && item.getId().equals("TAB_REALTRAVAUX")) {
			Include inc = (Include) this.getFellowIfAny("incTRAVAUX");
            inc.setSrc("/passationsmarches/procedurespassations/procedurestravaux.zul");
        }
		if(item != null && item.getId().equals("TAB_REALPI")) {
			Include inc = (Include) this.getFellowIfAny("incREALPI");
            inc.setSrc("/passationsmarches/procedurespassations/procedurespi.zul");
        }
		if(item != null && item.getId().equals("TAB_REALSERV")) {
			Include inc = (Include) this.getFellowIfAny("incREALSERV");
            inc.setSrc("/passationsmarches/procedurespassations/proceduresservices.zul");
        }
		if(item != null && item.getId().equals("TAB_FOURNITURES")) {
			Include inc = (Include) this.getFellowIfAny("incFOURNITURES");
            inc.setSrc("/passationsmarches/procedurespassations/proceduresfournitures.zul");
        }
		if(item != null && item.getId().equals("TAB_FINANCEMANT")) {
			Include inc = (Include) this.getFellowIfAny("incFINANCEMANT");
            inc.setSrc("/passationsmarches/procedurespassations/financements.zul");
        }
		if(item != null && item.getId().equals("TAB_COMMISSIONSMARCHES")) {
			Include inc = (Include) this.getFellowIfAny("incCOMMISSIONSMARCHES");
            inc.setSrc("/passationsmarches/procedurespassations/commissionsmarches.zul");
        }
		if(item != null && item.getId().equals("TAB_COMMISSIONSCELLULES")) {
			Include inc = (Include) this.getFellowIfAny("incCOMMISSIONSCELLULES");
            inc.setSrc("/passationsmarches/procedurespassations/commissionscellules.zul");
        }
		if(item != null && item.getId().equals("TAB_REALTRAV")) {
			Include inc = (Include) this.getFellowIfAny("incREALTRAV");
            inc.setSrc("/plansdepassation/listrealisationstravaux.zul");
        }
		if(item != null && item.getId().equals("TAB_REALSFOURN")) {
			Include inc = (Include) this.getFellowIfAny("incREALSFOURN");
            inc.setSrc("/plansdepassation/listrealisationsfournitures.zul");
        }
		
		if(item != null && item.getId().equals("TAB_REALSPI")) {
			Include inc = (Include) this.getFellowIfAny("incREALSPI");
            inc.setSrc("/plansdepassation/listrealisationspi.zul");
        }
		if(item != null && item.getId().equals("TAB_REALSSERV")) {
			Include inc = (Include) this.getFellowIfAny("incREALSSERV");
            inc.setSrc("/plansdepassation/listrealisationsserv.zul");
        }
		if(item != null && item.getId().equals("TAB_REALSDSP")) {
			Include inc = (Include) this.getFellowIfAny("incREALSDSP");
            inc.setSrc("/plansdepassation/listrealisationsdsp.zul");
        }
		if(item != null && item.getId().equals("TAB_REALDSP")) {
			Include inc = (Include) this.getFellowIfAny("incREALDSP");
            inc.setSrc("/passationsmarches/procedurespassations/proceduresdsp.zul");
        }
    }   
}