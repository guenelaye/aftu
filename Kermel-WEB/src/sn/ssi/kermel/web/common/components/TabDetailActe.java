package sn.ssi.kermel.web.common.components;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabDetailActe extends org.zkoss.zul.Tabbox {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void onCreate() {
	}

	// Pour le chargement la demande
	public void onSelect() {

		Tab item = getSelectedTab();

		String code = (String) getAttribute("code");
		if (item != null && item.getId().equals("TAB_GRADES")) {
			Include inc = (Include) this.getFellowIfAny("incGrades");
			inc.setSrc("/referentiel/categorie/listehistoriquefonction.zul?code=" + code);
		}
		if (item != null && item.getId().equals("TAB_POSITIONS")) {
			Include inc = (Include) this.getFellowIfAny("incPositions");
			inc.setSrc("/referentiel/categorie/listehistoriqueposition.zul?code=" + code);
		}
		if (item != null && item.getId().equals("TAB_SANCTIONS")) {
			Include inc = (Include) this.getFellowIfAny("incSanctions");
			inc.setSrc("/referentiel/categorie/sanctions.zul?code=" + code);
		}
		if (item != null && item.getId().equals("TAB_NOTES")) {
			Include inc = (Include) this.getFellowIfAny("incNotes");
			inc.setSrc("/referentiel/categorie/notes.zul?code=" + code);
		}
		if (item != null && item.getId().equals("TAB_FORMATIONS")) {
			Include inc = (Include) this.getFellowIfAny("incFormations");
			inc.setSrc("/referentiel/categorie/formations.zul?code=" + code);
		}
		if (item != null && item.getId().equals("TAB_AFFECTATIONS")) {
			Include inc = (Include) this.getFellowIfAny("incAffectations");
			inc.setSrc("/referentiel/categorie/listehistoriqueaffectation.zul?code=" + code);
		}
		if (item != null && item.getId().equals("TAB_DIPLOMES")) {
			Include inc = (Include) this.getFellowIfAny("incDiplomes");
			inc.setSrc("/referentiel/categorie/diplomes.zul?code=" + code);
		}
		if (item != null && item.getId().equals("TAB_FAMILLES")) {
			Include inc = (Include) this.getFellowIfAny("incFamilles");
			inc.setSrc("/referentiel/categorie/familles.zul?code=" + code);
		}
		if (item != null && item.getId().equals("TAB_CHANGEMENTS")) {
			Include inc = (Include) this.getFellowIfAny("incChangements");
			inc.setSrc("/referentiel/categorie/listehistoriquenom.zul?code=" + code);
		}
		if (item != null && item.getId().equals("TAB_DIPLOMES")) {
			Include inc = (Include) this.getFellowIfAny("incDiplomes");
			inc.setSrc("/referentiel/categorie/diplomes.zul?code=" + code);
		}
	}
}