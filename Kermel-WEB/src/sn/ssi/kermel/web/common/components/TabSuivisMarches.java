package sn.ssi.kermel.web.common.components;

import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.entity.SygTypeFormateur;

public class TabSuivisMarches extends org.zkoss.zul.Tabbox {
    
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Session session =Sessions.getCurrent();
	private SygTypeFormateur typeformateur;
 
	
	public void onCreate() {
	}
    
	// Pour le chargement  la demande
	public void onSelect() {
        
		Tab item = getSelectedTab();
		
		if(item != null && item.getId().equals("TAB_REALTRAVAUX")) {
			Include inc = (Include) this.getFellowIfAny("incTRAVAUX");
	      session.setAttribute("type",new Long(3));
            inc.setSrc("/suivimarches/contrats.zul");
        }
		if(item != null && item.getId().equals("TAB_REALPI")) {
			Include inc = (Include) this.getFellowIfAny("incREALPI");
			 session.setAttribute("type",new Long(4));
            inc.setSrc("/suivimarches/contrats.zul");
        }
		if(item != null && item.getId().equals("TAB_REALSERV")) {
			Include inc = (Include) this.getFellowIfAny("incREALSERV");
			 session.setAttribute("type",new Long(2));
            inc.setSrc("/suivimarches/contrats.zul");
        }
		if(item != null && item.getId().equals("TAB_FOURNITURES")) {
			Include inc = (Include) this.getFellowIfAny("incFOURNITURES");
			 session.setAttribute("type",new Long(1));
            inc.setSrc("/suivimarches/contrats.zul");
        }
		if(item != null && item.getId().equals("TAB_REALDSP")) {
			Include inc = (Include) this.getFellowIfAny("incREALDSP");
			 session.setAttribute("type",new Long(5));
            inc.setSrc("/suivimarches/contrats.zul");
        }
		if(item != null && item.getId().equals("TAB_COMMISSIONSMARCHES")) {
			Include inc = (Include) this.getFellowIfAny("incCOMMISSIONSMARCHES");
            inc.setSrc("/passationsmarches/procedurespassations/commissionsmarches.zul");
        }
		if(item != null && item.getId().equals("TAB_COMMISSIONSCELLULES")) {
			Include inc = (Include) this.getFellowIfAny("incCOMMISSIONSCELLULES");
            inc.setSrc("/passationsmarches/procedurespassations/commissionscellules.zul");
        }
		if(item != null && item.getId().equals("TAB_REALTRAV")) {
			Include inc = (Include) this.getFellowIfAny("incREALTRAV");
            inc.setSrc("/plansdepassation/listrealisationstravaux.zul");
        }
		if(item != null && item.getId().equals("TAB_REALSFOURN")) {
			Include inc = (Include) this.getFellowIfAny("incREALSFOURN");
            inc.setSrc("/plansdepassation/listrealisationsfournitures.zul");
        }
		
		if(item != null && item.getId().equals("TAB_REALSPI")) {
			Include inc = (Include) this.getFellowIfAny("incREALSPI");
            inc.setSrc("/plansdepassation/listrealisationspi.zul");
        }
		if(item != null && item.getId().equals("TAB_REALSSERV")) {
			Include inc = (Include) this.getFellowIfAny("incREALSSERV");
            inc.setSrc("/plansdepassation/listrealisationsserv.zul");
        }
		
		//pour les formateurs
		
		if(item != null && item.getId().equals("TAB_FORMIND")) {
			Include inc = (Include) this.getFellowIfAny("incFORMIND");
			 session.setAttribute("type",new Long(1));
            inc.setSrc("/formateur/liste.zul");
        }
		if(item != null && item.getId().equals("TAB_FORMCAB")) {
			Include inc = (Include) this.getFellowIfAny("incFORMCAB");
			 session.setAttribute("type",new Long(2));
            inc.setSrc("/formateur/listeformcabinet.zul");
        }
		
    }   
}