package sn.ssi.kermel.web.common.components;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabBonpilotage extends org.zkoss.zul.Tabbox {
    
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void onCreate() {
	}
    
	// Pour le chargement  la demande
	public void onSelect() {
        
		Tab item = getSelectedTab();
		
		String code = (String) getAttribute("code");
		if(item != null && item.getId().equals("SAISIE_BONPILOTAGE_STATE")) {
			Include inc = (Include) this.getFellowIfAny("saisie");
            inc.setSrc("/bonpilotage/details/saisie.zul?code="+code);
        }
		if(item != null && item.getId().equals("PREVALIDATION_BONPILOTAGE_STATE")) {
			Include inc = (Include) this.getFellowIfAny("prevalidation");
            inc.setSrc("/bonpilotage/details/prevalidation.zul?code="+code);
        }
		if(item != null && item.getId().equals("REALISATION_BONPILOTAGE_STATE")) {
			Include inc = (Include) this.getFellowIfAny("realisation");
            inc.setSrc("/bonpilotage/details/realisation.zul?code="+code);
        }
		if(item != null && item.getId().equals("RETOUR_PREVALIDATION_BONPILOTAGE_STATE")) {
			Include inc = (Include) this.getFellowIfAny("retourprevalidation");
            inc.setSrc("/bonpilotage/details/retourprevalidation.zul?code="+code);
        }
		if(item != null && item.getId().equals("EDITION_ETATSYNTHESE_BONPILOTAGE_STATE")) {
			Include inc = (Include) this.getFellowIfAny("editionetatsynthese");
            inc.setSrc("/bonpilotage/details/editionetatsynthese.zul?code="+code);
        }
		if(item != null && item.getId().equals("TRANSMISSION_ETATSYNTHESE_BONPILOTAGE_STATE")) {
			Include inc = (Include) this.getFellowIfAny("transmissionetatsynthese");
            inc.setSrc("/bonpilotage/details/transmissionetatsynthese.zul?code="+code);
        }
		if(item != null && item.getId().equals("VALIDATION_FACTURATION_BONPILOTAGE_STATE")) {
			Include inc = (Include) this.getFellowIfAny("validationfacturation");
            inc.setSrc("/bonpilotage/details/validationfacturation.zul?code="+code);
        }
		if(item != null && item.getId().equals("REJET_FACTURATION_BONPILOTAGE_STATE")) {
			Include inc = (Include) this.getFellowIfAny("rejetfacturation");
            inc.setSrc("/bonpilotage/details/rejetfacturation.zul?code="+code);
        }
		if(item != null && item.getId().equals("FACTURATION_BONPILOTAGE_STATE")) {
			Include inc = (Include) this.getFellowIfAny("facturation");
            inc.setSrc("/bonpilotage/details/facturation.zul?code="+code);
        }
    }   
}