package sn.ssi.kermel.web.common.components;

import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabTexte extends org.zkoss.zul.Tabbox {

	private static final long serialVersionUID = 1L;

	
	public void onCreate() {
	}

	// Pour le chargement la demande
	public void onSelect() {

		Tab item = getSelectedTab();
		String code = (String) getAttribute("code");
		if ((item != null) && item.getId().equals("TAB_REGIME")) {
			Include inc = (Include) this.getFellowIfAny("incRegime");
			inc.setSrc("/demande/regime.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_TEXTEREF")) {
			Include inc = (Include) this.getFellowIfAny("incTexteref");
			inc.setSrc("/demande/texteref.zul?code=" + code);
			
		}



	}

	public Session getHttpSession() {
		return Sessions.getCurrent();
	}

	public Object getSessionAttribute(final String attribute) {
		return getHttpSession().getAttribute(attribute);
	}
}