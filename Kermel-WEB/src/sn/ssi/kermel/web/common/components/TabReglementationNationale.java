package sn.ssi.kermel.web.common.components;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabReglementationNationale extends org.zkoss.zul.Tabbox {
    
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public void onCreate() {
	}
    
	// Pour le chargement  la demande
	public void onSelect() {
        
		Tab item = getSelectedTab();
		
		if(item != null && item.getId().equals("TAB_DIRECTIVES")) {
			Include inc = (Include) this.getFellowIfAny("incDIRECTIVES");
            inc.setSrc("/reglementation/nationale/dossiers/directives.zul");
        }
		
		if(item != null && item.getId().equals("TAB_DECRETS")) {
			Include inc = (Include) this.getFellowIfAny("incDECRETS");
            inc.setSrc("/reglementation/nationale/dossiers/decrets.zul");
        }
		
		if(item != null && item.getId().equals("TAB_DECISIONS")) {
			Include inc = (Include) this.getFellowIfAny("incDECISIONS");
            inc.setSrc("/reglementation/nationale/dossiers/decisions.zul");
        }
		
		if(item != null && item.getId().equals("TAB_ARRETES")) {
			Include inc = (Include) this.getFellowIfAny("incARRETES");
            inc.setSrc("/reglementation/nationale/dossiers/arretes.zul");
        }
		
		
		
    }   
}