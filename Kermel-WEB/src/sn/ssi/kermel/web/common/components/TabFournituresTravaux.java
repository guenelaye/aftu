package sn.ssi.kermel.web.common.components;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabFournituresTravaux extends org.zkoss.zul.Tabbox {
    
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public void onCreate() {
	}
    
	// Pour le chargement  la demande
	public void onSelect() {
        
		Tab item = getSelectedTab();
		
		if(item != null && item.getId().equals("TAB_infosgenerales")) {
			Include inc = (Include) this.getFellowIfAny("incinfosgenerales");
            inc.setSrc("/passationsmarches/fournituretravaux/infogenerales.zul");
        }
		if(item != null && item.getId().equals("TAB_garanties")) {
			Include inc = (Include) this.getFellowIfAny("incgaranties");
            inc.setSrc("/passationsmarches/fournituretravaux/garantis.zul");
        }
		if(item != null && item.getId().equals("TAB_piecesadministratives")) {
			Include inc = (Include) this.getFellowIfAny("incpiecesadministratives");
            inc.setSrc("/passationsmarches/fournituretravaux/piecesadministratives.zul");
        }
		if(item != null && item.getId().equals("TAB_criteresqualifications")) {
			Include inc = (Include) this.getFellowIfAny("inccriteresqualifications");
            inc.setSrc("/passationsmarches/fournituretravaux/criteresqualifications.zul");
        }
		if(item != null && item.getId().equals("TAB_devise")) {
			Include inc = (Include) this.getFellowIfAny("incdevise");
            inc.setSrc("/passationsmarches/fournituretravaux/devise.zul");
        }
		if(item != null && item.getId().equals("TAB_financement")) {
			Include inc = (Include) this.getFellowIfAny("incfinancement");
            inc.setSrc("/passationsmarches/fournituretravaux/financements.zul");
        }
		if(item != null && item.getId().equals("TAB_listepresencemembrescommissions")) {
			Include inc = (Include) this.getFellowIfAny("inclistepresencemembrescommissions");
            inc.setSrc("/passationsmarches/fournituretravaux/listepresencemembrescommissions.zul");
        }
		if(item != null && item.getId().equals("TAB_representantssoumissionnaires")) {
			Include inc = (Include) this.getFellowIfAny("increpresentantssoumissionnaires");
            inc.setSrc("/passationsmarches/fournituretravaux/representantssoumissionnaires.zul");
        }
		if(item != null && item.getId().equals("TAB_representantsservicestechniques")) {
			Include inc = (Include) this.getFellowIfAny("increpresentantsservicestechniques");
            inc.setSrc("/passationsmarches/fournituretravaux/representantsservicestechniques.zul");
        }
		if(item != null && item.getId().equals("TAB_observateursindependants")) {
			Include inc = (Include) this.getFellowIfAny("incobservateursindependants");
            inc.setSrc("/passationsmarches/fournituretravaux/observateursindependants.zul");
        }
		if(item != null && item.getId().equals("TAB_garantiesoumission")) {
			Include inc = (Include) this.getFellowIfAny("incgarantiesoumission");
            inc.setSrc("/passationsmarches/fournituretravaux/garantiesoumission.zul");
        }
		if(item != null && item.getId().equals("TAB_piecessoumissionnaires")) {
			Include inc = (Include) this.getFellowIfAny("incpiecessoumissionnaires");
            inc.setSrc("/passationsmarches/fournituretravaux/piecessoumissionnaires.zul");
        }
		if(item != null && item.getId().equals("TAB_compositioncommissiontechnique")) {
			Include inc = (Include) this.getFellowIfAny("inccompositioncommissiontechnique");
            inc.setSrc("/passationsmarches/fournituretravaux/compositioncommissiontechnique.zul");
        }
		if(item != null && item.getId().equals("TAB_lecturesoffres")) {
			Include inc = (Include) this.getFellowIfAny("inclecturesoffres");
            inc.setSrc("/passationsmarches/fournituretravaux/formlecturesoffres.zul");
        }
		if(item != null && item.getId().equals("TAB_procesverbalouverture")) {
			Include inc = (Include) this.getFellowIfAny("incprocesverbalouverture");
            inc.setSrc("/passationsmarches/fournituretravaux/procesverbalouverture.zul");
        }
		if(item != null && item.getId().equals("TAB_formtransmissiondossier")) {
			Include inc = (Include) this.getFellowIfAny("incformtransmissiondossier");
            inc.setSrc("/passationsmarches/fournituretravaux/formtransmissiondossier.zul");
        }
		if(item != null && item.getId().equals("TAB_controlegarantie")) {
			Include inc = (Include) this.getFellowIfAny("inccontrolegarantie");
            inc.setSrc("/passationsmarches/fournituretravaux/controlegarantie.zul");
        }
		if(item != null && item.getId().equals("TAB_verificationconformite")) {
			Include inc = (Include) this.getFellowIfAny("incverificationconformite");
            inc.setSrc("/passationsmarches/fournituretravaux/formverificationconformite.zul");
        }
		if(item != null && item.getId().equals("TAB_correctionoffres")) {
			Include inc = (Include) this.getFellowIfAny("inccorrectionoffres");
            inc.setSrc("/passationsmarches/fournituretravaux/formcorrectionoffre.zul");
        }
		if(item != null && item.getId().equals("TAB_vercriteresqualification")) {
			Include inc = (Include) this.getFellowIfAny("incvercriteresqualification");
            inc.setSrc("/passationsmarches/fournituretravaux/vercriteresqualification.zul");
        }
		if(item != null && item.getId().equals("TAB_classementfinal")) {
			Include inc = (Include) this.getFellowIfAny("incclassementfinal");
            inc.setSrc("/passationsmarches/fournituretravaux/formclassementfinal.zul");
        }
		if(item != null && item.getId().equals("TAB_rapportevaluation")) {
			Include inc = (Include) this.getFellowIfAny("incrapportevaluation");
            inc.setSrc("/passationsmarches/fournituretravaux/rapportevaluation.zul");
        }
		if(item != null && item.getId().equals("TAB_ordredeservicededemarrage")) {
			Include inc = (Include) this.getFellowIfAny("incordredeservicededemarrage");
            inc.setSrc("/passationsmarches/fournituretravaux/formordredeservicededemarrage.zul");
        }
		if(item != null && item.getId().equals("TAB_receptionprovisoire")) {
			Include inc = (Include) this.getFellowIfAny("increceptionprovisoire");
            inc.setSrc("/passationsmarches/fournituretravaux/formreceptionprovisoire.zul");
        }
		if(item != null && item.getId().equals("TAB_receptiondefinitive")) {
			Include inc = (Include) this.getFellowIfAny("increceptiondefinitive");
            inc.setSrc("/passationsmarches/fournituretravaux/formreceptiondefinitive.zul");
        }
		if(item != null && item.getId().equals("TAB_allotissement")) {
			Include inc = (Include) this.getFellowIfAny("incallotissement");
            inc.setSrc("/passationsmarches/fournituretravaux/allotissement.zul");
        }
		if(item != null && item.getId().equals("TAB_presentationoffres")) {
			Include inc = (Include) this.getFellowIfAny("incpresentationoffres");
            inc.setSrc("/passationsmarches/fournituretravaux/presentationsoffres.zul");
        }
    }   
}