package sn.ssi.kermel.web.common.components;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabNomination extends org.zkoss.zul.Tabbox {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public void onCreate() {
	}

	// Pour le chargement la demande
	public void onSelect() {

		Tab item = getSelectedTab();
		String code = (String) getAttribute("code");
		if ((item != null) && item.getId().equals("TAB_CANDIDAT")) {
			Include inc = (Include) this.getFellowIfAny("incCandidat");
			inc.setSrc("/nomination/candidats.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_PACTES")) {
			Include inc = (Include) this.getFellowIfAny("incProjetacte");
			inc.setSrc("/nomination/projetacte.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_PJ")) {
			Include inc = (Include) this.getFellowIfAny("incPj");
			inc.setSrc("/nomination/pj.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_IMPUTATION")) {
			Include inc = (Include) this.getFellowIfAny("incImputation");
			inc.setSrc("/nomination/imputation.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_COURRIER")) {
			Include inc = (Include) this.getFellowIfAny("incCourrier");
			inc.setSrc("/nomination/courriers.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_COMPLRMRNT")) {
			Include inc = (Include) this.getFellowIfAny("incComplement");
			inc.setSrc("/nomination/compositiondossier.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_AMPLIATION")) {
			Include inc = (Include) this.getFellowIfAny("incAmpliation");
			inc.setSrc("/nomination/ampliation.zul?code=" + code);
		}

	}
}