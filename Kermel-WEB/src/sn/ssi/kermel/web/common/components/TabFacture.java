package sn.ssi.kermel.web.common.components;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabFacture extends org.zkoss.zul.Tabbox {
    
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void onCreate() {
	}
    
	// Pour le chargement  la demande
	public void onSelect() {
        
		Tab item = getSelectedTab();
		
		String code = (String) getAttribute("code");
		if(item != null && item.getId().equals("EDITION_FACTURE_STATE")) {
			Include inc = (Include) this.getFellowIfAny("editionfacture");
            inc.setSrc("/facture/details/editionfacture.zul?code="+code);
        }
		if(item != null && item.getId().equals("DISPATCHING_FACTURE_STATE")) {
			Include inc = (Include) this.getFellowIfAny("dispatchingfacture");
            inc.setSrc("/facture/details/dispatchingfacture.zul?code="+code);
        }
		if(item != null && item.getId().equals("DEPOT_FACTURE_STATE")) {
			Include inc = (Include) this.getFellowIfAny("depotfacture");
            inc.setSrc("/facture/details/depotfacture.zul?code="+code);
        }
	}   
}