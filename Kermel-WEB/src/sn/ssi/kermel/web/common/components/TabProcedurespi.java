package sn.ssi.kermel.web.common.components;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabProcedurespi extends org.zkoss.zul.Tabbox {
    
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public void onCreate() {
	}
    
	// Pour le chargement  la demande
	public void onSelect() {
        
		Tab item = getSelectedTab();
		
		if(item != null && item.getId().equals("TAB_traitdossiers")) {
			Include inc = (Include) this.getFellowIfAny("inctraitdossiers");
			inc.setSrc("/passationsmarches/prestationintellectuelle/traitementsdossiersmi.zul");	
          
        }
		if(item != null && item.getId().equals("TAB_infosgenerales")) {
			Include inc = (Include) this.getFellowIfAny("incinfosgenerales");
			inc.setSrc("/passationsmarches/prestationintellectuelle/infogenerales.zul");	
          
        }
		if(item != null && item.getId().equals("TAB_criteresqualifications")) {
			Include inc = (Include) this.getFellowIfAny("inccriteresqualifications");
			inc.setSrc("/passationsmarches/prestationintellectuelle/criteresqualifications.zul");	
          
        }
		if(item != null && item.getId().equals("TAB_recours")) {
			Include inc = (Include) this.getFellowIfAny("increcours");
			inc.setSrc("/passationsmarches/prestationintellectuelle/recours.zul");	
          
        }
		if(item != null && item.getId().equals("TAB_documents")) {
			Include inc = (Include) this.getFellowIfAny("incdocuments");
			inc.setSrc("/passationsmarches/prestationintellectuelle/documents.zul");	
          
        }
		if(item != null && item.getId().equals("TAB_infosgeneralesdp")) {
			Include inc = (Include) this.getFellowIfAny("incinfosgeneralesdp");
			inc.setSrc("/passationsmarches/prestationintellectuelle/infogeneralesdp.zul");	
          
        }
		if(item != null && item.getId().equals("TAB_criteresqualificationsdp")) {
			Include inc = (Include) this.getFellowIfAny("inccriteresqualificationsdp");
			inc.setSrc("/passationsmarches/prestationintellectuelle/criteresqualificationsdp.zul");	
          
        }
		if(item != null && item.getId().equals("TAB_devise")) {
			Include inc = (Include) this.getFellowIfAny("incdevise");
			inc.setSrc("/passationsmarches/prestationintellectuelle/devise.zul");	
          }
		if(item != null && item.getId().equals("TAB_piecesadministratives")) {
			Include inc = (Include) this.getFellowIfAny("incpiecesadministratives");
			inc.setSrc("/passationsmarches/prestationintellectuelle/piecesadministratives.zul");	
          }
		if(item != null && item.getId().equals("TAB_presentationoffres")) {
			Include inc = (Include) this.getFellowIfAny("incpresentationoffres");
            inc.setSrc("/passationsmarches/prestationintellectuelle/presentationsoffres.zul");
        }
    }   
}