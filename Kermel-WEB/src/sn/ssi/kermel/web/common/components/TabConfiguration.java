package sn.ssi.kermel.web.common.components;

import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabConfiguration extends org.zkoss.zul.Tabbox {

	private static final long serialVersionUID = 1L;

	public void onCreate() {
	}

	// Pour le chargement la demande
	public void onSelect() {

		Tab item = getSelectedTab();
		if ((item != null) && item.getId().equals("TAB_Composition")) {
			Include inc = (Include) this.getFellowIfAny("incComposition");
		    inc.setSrc("/traitementdossier/listpiecerequises.zul");
		}
		if ((item != null) && item.getId().equals("TAB_Grilledanalyse")) {
			Include inc = (Include) this.getFellowIfAny("incGrilledanalyse");
		    inc.setSrc("/traitementdossier/listegrillesanalyses.zul");
		}
		if ((item != null) && item.getId().equals("TAB_Modeledocument")) {
			Include inc = (Include) this.getFellowIfAny("incModeledocument");
		    inc.setSrc("/traitementdossier/listetypemodeledocument.zul");
		}

	}

	public Session getHttpSession() {
		return Sessions.getCurrent();
	}

	public Object getSessionAttribute(final String attribute) {
		return getHttpSession().getAttribute(attribute);
	}
}