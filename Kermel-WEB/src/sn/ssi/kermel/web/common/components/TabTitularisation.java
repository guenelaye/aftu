package sn.ssi.kermel.web.common.components;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabTitularisation extends org.zkoss.zul.Tabbox {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void onCreate() {
	}

	// Pour le chargement la demande
	public void onSelect() {

		Tab item = getSelectedTab();
		String code = (String) getAttribute("code");
		if ((item != null) && item.getId().equals("TAB_AGENTSTITULARISES")) {
			Include inc = (Include) this.getFellowIfAny("incAgentsTitulaires");
			inc.setSrc("/titularisation/agents.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_PACTES")) {
			Include inc = (Include) this.getFellowIfAny("incProjetacte");
			inc.setSrc("/titularisation/projetacte.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_PJ")) {
			Include inc = (Include) this.getFellowIfAny("incPj");
			inc.setSrc("/titularisation/pj.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_IMPUTATION")) {
			Include inc = (Include) this.getFellowIfAny("incImputation");
			inc.setSrc("/titularisation/imputation.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_COURRIER")) {
			Include inc = (Include) this.getFellowIfAny("incCourrier");
			inc.setSrc("/titularisation/courriers.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_COMPLRMRNT")) {
			Include inc = (Include) this.getFellowIfAny("incComplement");
			inc.setSrc("/titularisation/compositiondossier.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_AMPLIATION")) {
			Include inc = (Include) this.getFellowIfAny("incAmpliation");
			inc.setSrc("/titularisation/ampliation.zul?code=" + code);
		}

	}
}