package sn.ssi.kermel.web.common.components;

import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabAudits extends org.zkoss.zul.Tabbox {

	private static final long serialVersionUID = 1L;

	public void onCreate() {
	}

	// Pour le chargement la demande
	public void onSelect() {

		Tab item = getSelectedTab();
		if ((item != null) && item.getId().equals("TAB_Autorites")) {
			Include inc = (Include) this.getFellowIfAny("incAutorites");
		    inc.setSrc("/audits/autoritescontractantes.zul");
		}
		if ((item != null) && item.getId().equals("TAB_Prestataire")) {
			Include inc = (Include) this.getFellowIfAny("incPrestataire");
		    inc.setSrc("/audits/contratsprestataires.zul");
		}
		if ((item != null) && item.getId().equals("TAB_Courriers")) {
			Include inc = (Include) this.getFellowIfAny("incCourriers");
		    inc.setSrc("/audits/listcourrier.zul");
		}
		if ((item != null) && item.getId().equals("TAB_PJ")) {
			Include inc = (Include) this.getFellowIfAny("incPJ");
		    inc.setSrc("/audits/listpiecejointe.zul");
		}
		if ((item != null) && item.getId().equals("TAB_Historiques")) {
			Include inc = (Include) this.getFellowIfAny("incHistoriques");
		    inc.setSrc("/audits/auditsprestataires.zul");
		}
	
		if ((item != null) && item.getId().equals("TAB_TableauBord")) {
			Include inc = (Include) this.getFellowIfAny("incTableauBord");
		    inc.setSrc("/audits/tableaubord.zul");
		}
	
		if ((item != null) && item.getId().equals("TAB_RapportGlobal")) {
			Include inc = (Include) this.getFellowIfAny("incRapportGlobal");
		    inc.setSrc("/audits/rapportglobal.zul");
		}

	}

	public Session getHttpSession() {
		return Sessions.getCurrent();
	}

	public Object getSessionAttribute(final String attribute) {
		return getHttpSession().getAttribute(attribute);
	}
}