package sn.ssi.kermel.web.common.components;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabFactureGC extends org.zkoss.zul.Tabbox {
    
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void onCreate() {
	}
    
	// Pour le chargement  la demande
	public void onSelect() {
        
		Tab item = getSelectedTab();
		
		String code = (String) getAttribute("code");
		if(item != null && item.getId().equals("EDITION_FACTUREGC_STATE")) {
			Include inc = (Include) this.getFellowIfAny("editionfacturegc");
            inc.setSrc("/facture/details/editionfacturegc.zul?code="+code);
        }
		if(item != null && item.getId().equals("TRANSMISSION_FACTUREGC_STATE")) {
			Include inc = (Include) this.getFellowIfAny("transmissionfacturegc");
            inc.setSrc("/facturegc/details/transmissionfacturegc.zul?code="+code);
        }
	}   
}