package sn.ssi.kermel.web.common.components;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabDelaisDates extends org.zkoss.zul.Tabbox {
    
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

 
	public void onCreate() {
	}
    
	
	public void onSelect() {
        
		Tab item = getSelectedTab();
		
		if(item != null && item.getId().equals("TAB_TRAVAUX")) {
			Include inc = (Include) this.getFellowIfAny("incTRAVAUX");
	        inc.setSrc("/referentiel/delaisdatesrealisations/detailstfs.zul");
        }
		if(item != null && item.getId().equals("TAB_PI")) {
			Include inc = (Include) this.getFellowIfAny("incPI");
          inc.setSrc("/referentiel/delaisdatesrealisations/detailspi.zul");
        }
		
		
    }   
}