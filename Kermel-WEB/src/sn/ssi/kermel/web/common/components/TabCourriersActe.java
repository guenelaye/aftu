package sn.ssi.kermel.web.common.components;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

/**
 * @author 2SI Atlantis
 * @since Jeudi 1 Juillet 2010, 16:19
 * @version 1.0
 */
public class TabCourriersActe extends org.zkoss.zul.Tabbox {
	    
	private static final long serialVersionUID = -5707280480196008851L;

	/*
	 * (non-Javadoc)
	 * @see org.zkoss.zul.Tabbox#onCreate()
	 */
	public void onCreate() {
		initUi();
	}

	
	/**
	 * Pour le chargement  la demande
	 * le <code>code</code> envoyé est celui du type de dossier
	 */
	public void initUi() {
        
//		Tab item = getSelectedTab();
		String code = (String) getAttribute("code");
		Tab item = (Tab)getFellowIfAny(code);
		
		if(item != null && item.getId().equals("TYPE_DOSSIER1")) {
			Include inc = (Include) this.getFellowIfAny("typedossier1");
            inc.setSrc("/courriers/typedossiers/petitzul.zul?code="+code);
        }
		if(item != null && item.getId().equals("TYPE_DOSSIER2")) {
        }
		if(item != null && item.getId().equals("TYPE_DOSSIER3")) {
        }
		if(item != null && item.getId().equals("TYPE_DOSSIER4")) {
        }
		if(item != null && item.getId().equals("TYPE_DOSSIER5")) {
        }
		if(item != null && item.getId().equals("TYPE_DOSSIER6")) {
        }
		if(item != null && item.getId().equals("TYPE_DOSSIER7")) {
        }
		if(item != null && item.getId().equals("TYPE_DOSSIER8")) {
        }
		if(item != null && item.getId().equals("TYPE_DOSSIER9")) {
        }
		if(item != null && item.getId().equals("TYPE_DOSSIER10")) {
        }
		if(item != null && item.getId().equals("TYPE_DOSSIER11")) {
        }
    }   
}