package sn.ssi.kermel.web.common.components;

import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabDenonciation extends org.zkoss.zul.Tabbox {

	private static final long serialVersionUID = 1L;

	public void onCreate() {
	}

	// Pour le chargement la demande
	public void onSelect() {

		Tab item = getSelectedTab();
		if ((item != null) && item.getId().equals("TAB_Portail")) {
			Include inc = (Include) this.getFellowIfAny("incPortail");
		    inc.setSrc("/denonciation/listdenonciation.zul");
		}
		if ((item != null) && item.getId().equals("TAB_Portailp")) {
			Include inc = (Include) this.getFellowIfAny("incPortailp");
		    inc.setSrc("/denonciation/listdenonciationpoubelle.zul");
		}
		if ((item != null) && item.getId().equals("TAB_CourriersDenonciation")) {
			Include inc = (Include) this.getFellowIfAny("incCourriersDenonciation");
		    inc.setSrc("/denonciation/listdenonciationcourrier.zul");
		}
		if ((item != null) && item.getId().equals("TAB_Courriersp")) {
			Include inc = (Include) this.getFellowIfAny("incCourriersp");
		    inc.setSrc("/denonciation/listdenonciationpoubellecourrier.zul");
		}
		if ((item != null) && item.getId().equals("TAB_NumVert")) {
			Include inc = (Include) this.getFellowIfAny("incNumVert");
		    inc.setSrc("/denonciation/listdenonciationnumvert.zul");
		}
		if ((item != null) && item.getId().equals("TAB_NumVertp")) {
			Include inc = (Include) this.getFellowIfAny("incNumVertp");
		    inc.setSrc("/denonciation/listdenonciationpoubellenumvert.zul");
		}
		if ((item != null) && item.getId().equals("TAB_Poubelle")) {
			Include inc = (Include) this.getFellowIfAny("incPoubelle");
		    inc.setSrc("/denonciation/dossierpoubelle.zul");
		}
		if ((item != null) && item.getId().equals("TAB_CourriersDenonciationSuivit")) {
			Include inc = (Include) this.getFellowIfAny("incCourriersDenonciationSuivit");
		    inc.setSrc("/denonciation/listdenonciationcourriersuivit.zul");
		}
		if ((item != null) && item.getId().equals("TAB_PJ")) {
			Include inc = (Include) this.getFellowIfAny("incPJ");
		    inc.setSrc("/denonciation/piecejointe.zul");
		}
		if ((item != null) && item.getId().equals("TAB_Decision")) {
			Include inc = (Include) this.getFellowIfAny("incDecision");
		    inc.setSrc("/denonciation/decision.zul");
		}
	}

	public Session getHttpSession() {
		return Sessions.getCurrent();
	}

	public Object getSessionAttribute(final String attribute) {
		return getHttpSession().getAttribute(attribute);
	}
}