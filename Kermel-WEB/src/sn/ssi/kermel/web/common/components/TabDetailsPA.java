package sn.ssi.kermel.web.common.components;

import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.web.common.constants.UIConstants;

public class TabDetailsPA extends org.zkoss.zul.Tabbox {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void onCreate() {
	}

	// Pour le chargement la demande
	public void onSelect() {

		Tab item = getSelectedTab();
		String code = (String) getAttribute("code");
		if ((item != null) && item.getId().equals("TAB_CANDIDAT")) {
			Include inc = (Include) this.getFellowIfAny("incCandidat");
			String type = (String) getHttpSession().getAttribute(UIConstants.TEST_TYPE);

			if (type.equalsIgnoreCase(UIConstants.TA_NOMINATION)) {
				inc.setSrc("/dossier/candidats.zul?code=" + code);
			} else {
				inc.setSrc("/dossier/agents.zul?code=" + code);
			}

		}
		if ((item != null) && item.getId().equals("TAB_IMPUTATION")) {
			Include inc = (Include) this.getFellowIfAny("incImputation");
			inc.setSrc("/dossier/imputation.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_PJ")) {
			Include inc = (Include) this.getFellowIfAny("incPj");
			inc.setSrc("/dossier/pj.zul?code=" + code);
		}
	}

	public Session getHttpSession() {
		return Sessions.getCurrent();
	}

	public Object getSessionAttribute(final String attribute) {
		return getHttpSession().getAttribute(attribute);
	}
}