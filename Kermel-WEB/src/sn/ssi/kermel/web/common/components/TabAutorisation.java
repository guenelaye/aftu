package sn.ssi.kermel.web.common.components;

import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.web.common.constants.UIConstants;

public class TabAutorisation extends org.zkoss.zul.Tabbox {

	private static final long serialVersionUID = 1L;

	public void onCreate() {
	}

	// Pour le chargement la demande
	public void onSelect() {

		Tab item = getSelectedTab();
		String code = (String) getAttribute("code");
//		if ((item != null) && item.getId().equals("TAB_CANDIDAT")) {
//			Include inc = (Include) this.getFellowIfAny("incCandidat");
//			String type = (String) getHttpSession().getAttribute(UIConstants.TEST_TYPE);
//
//			// if (type.equalsIgnoreCase(UIConstants.TA_NOMINATION)) {
//			// inc.setSrc("/dossier/candidats.zul?code=" + code);
//			// } else {
//			inc.setSrc("/dossier/agents.zul?code=" + code);
//			// }
//		}
		if ((item != null) && item.getId().equals("TAB_TEXTE")) {
			Include inc = (Include) this.getFellowIfAny("incTexte");
			String type = (String) getHttpSession().getAttribute(UIConstants.TEST_TYPE);

			// if (type.equalsIgnoreCase(UIConstants.TA_NOMINATION)) {
			// inc.setSrc("/dossier/candidats.zul?code=" + code);
			// } else {
			inc.setSrc("/autorisations/textes/textes.zul?code=" + code);
			// }
		}
//		if ((item != null) && item.getId().equals("TAB_MAT")) {
//			Include inc = (Include) this.getFellowIfAny("incMat");
//			String type = (String) getHttpSession().getAttribute(UIConstants.TEST_TYPE);

			// if (type.equalsIgnoreCase(UIConstants.TA_NOMINATION)) {
			// inc.setSrc("/dossier/candidats.zul?code=" + code);
			// } else {
//			inc.setSrc("/demande/materiel.zul?code=" + code);
			// }
//		}
		if ((item != null) && item.getId().equals("TAB_PJ")) {
			Include inc = (Include) this.getFellowIfAny("incPj");
			inc.setSrc("/autorisations/pieces/pieces.zul?code=" + code);
		}
		
	
//		if ((item != null) && item.getId().equals("TAB_COURRIER")) {
//			Include inc = (Include) this.getFellowIfAny("incCourrier");
//			inc.setSrc("/dossier/courriers.zul?code=" + code);
//		}
//		if ((item != null) && item.getId().equals("TAB_COMPLEMENT")) {
//			Include inc = (Include) this.getFellowIfAny("incComplement");
//			inc.setSrc("/dossier/compositiondossier.zul?code=" + code);
//		}
//		if ((item != null) && item.getId().equals("TAB_AMPLIATION")) {
//			Include inc = (Include) this.getFellowIfAny("incAmpliation");
//			inc.setSrc("/dossier/ampliation.zul?code=" + code);
//		}
		if ((item != null) && item.getId().equals("TAB_FACTURE")) {
			Include inc = (Include) this.getFellowIfAny("incFacture");
			inc.setSrc("/demande/demfacture.zul?code=" + code);
		}
		if ((item != null) && item.getId().equals("TAB_IMPUTATION")) {
			Include inc = (Include) this.getFellowIfAny("incImputation");
			inc.setSrc("/demande/imputation.zul");
		}
		if ((item != null) && item.getId().equals("TAB_PROJET")) {
			Include inc = (Include) this.getFellowIfAny("incProjet");
			inc.setSrc("/demande/projet.zul");
		}

	}

	public Session getHttpSession() {
		return Sessions.getCurrent();
	}

	public Object getSessionAttribute(final String attribute) {
		return getHttpSession().getAttribute(attribute);
	}
}