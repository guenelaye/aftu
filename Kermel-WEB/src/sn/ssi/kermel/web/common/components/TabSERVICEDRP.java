package sn.ssi.kermel.web.common.components;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabSERVICEDRP extends org.zkoss.zul.Tabbox {
    
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public void onCreate() {
	}
    
	// Pour le chargement  la demande
	public void onSelect() {
        
		Tab item = getSelectedTab();
		
		if(item != null && item.getId().equals("TAB_infosgenerales")) {
			Include inc = (Include) this.getFellowIfAny("incinfosgenerales");
            inc.setSrc("/passationsmarches/servicedrp/infogenerales.zul");
        }
	
		if(item != null && item.getId().equals("TAB_piecesadministratives")) {
			Include inc = (Include) this.getFellowIfAny("incpiecesadministratives");
            inc.setSrc("/passationsmarches/servicedrp/piecesadministratives.zul");
        }
		if(item != null && item.getId().equals("TAB_criteresqualifications")) {
			Include inc = (Include) this.getFellowIfAny("inccriteresqualifications");
            inc.setSrc("/passationsmarches/servicedrp/criteresqualifications.zul");
        }
		if(item != null && item.getId().equals("TAB_devise")) {
			Include inc = (Include) this.getFellowIfAny("incdevise");
            inc.setSrc("/passationsmarches/servicedrp/devise.zul");
        }
		if(item != null && item.getId().equals("TAB_allotissement")) {
			Include inc = (Include) this.getFellowIfAny("incallotissement");
            inc.setSrc("/passationsmarches/servicedrp/allotissement.zul");
        }
		if(item != null && item.getId().equals("TAB_presentationoffres")) {
			Include inc = (Include) this.getFellowIfAny("incpresentationoffres");
            inc.setSrc("/passationsmarches/servicedrp/presentationsoffres.zul");
        }
	


    }   
}