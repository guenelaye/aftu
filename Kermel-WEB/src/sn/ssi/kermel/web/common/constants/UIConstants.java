package sn.ssi.kermel.web.common.constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import sn.ssi.kermel.be.common.utils.ToolKermel;

public class UIConstants {
	public static final String ACTION_EDIT = "EDIT";
	public static final String ACTION_DELETE = "DELETE";

	public static final String MODE_NEW = "NEW";
	public static final String URL_BIRT = "UrlBirt";

	public static final String MODE_EDIT = "EDIT";
	public static final String MODE_DETAIL = "DETAIL";
	public static final String MODE_SUIVIE = "SUIVIE";

	public static final String TODO = "todo";

	public static final int DSP_GRID_ROWS_BY_PAGE = 10;
	public static final int DSP_GRID_ROWS_BY_PAGES = 13;
	public static final int DSP_GRID_ROWS_BY_PAGESS = 18;
	public static final int DSP_GRID_ROWS_DOSSIERREC_BY_PAGE = 25;
	public static final int DSP_GRID_TYPE_OPERATION_BY_PAGE = 20;
	public static final int DSP_BANDBOX_BY_PAGE = 5;
	public static final int DSP_BANDBOXS_BY_PAGES = 10;
	public static final int DSP_GRID_ROW_BY_PAGE = 6;
	public static final int DSP_GRID_ROW_BY_PAGE8 = 8;
	public static final String ATTRIBUTE_LIBELLE = "libelle";
	public static final int DSP_LIST_BY_PAGES= 10;

	
	/**
	 * Titre d�xon�ration  (STATES): Dossier cr��
	 */
	public static final String CREATION_TITREEXO_STATE = "CREATION_DEMANDE_TITEXO_STATE";
	/**
	 * Nomination (STATES):
	 */
	/**
	 * Nomination (STATES): Dossier créé
	 */
	public static final String CREATION_DOSSIER_STATE = "CREATION_DOSSIER_STATE";
	/**
	 * Nomination (STATES):
	 */
	public static final String IMPUTATION_CHEFDRRS_STATE = "IMPUTATION_CHEFDRRS_STATE";
	/**
	 * Nomination (STATES):
	 */
	public static final String IMPUTATION_CHEFSECTION_STATE = "IMPUTATION_CHEFSECTION_STATE";
	/**
	 * Nomination (STATES):
	 */
	public static final String IMPUTATION_CHARGEDETUDE_STATE = "IMPUTATION_CHARGEDETUDE_STATE";

	/**
	 * Nomination (ACTIONS): Créer Dossier
	 */
	public static final String CREATION_DOSSIER = "CREATION_DOSSIER";

	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_NOMMINATION_STATE = "CREATION_PA_NOMMINATION_STATE";

	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_DISPONIBILITE_STATE = "CREATION_PA_DISPONIBILITE_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_CONGE_STATE = "CREATION_PA_CONGE_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_MISEPOSTSTAGE_STATE = "CREATION_PA_MISEPOSTSTAGE_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_MOSA_STATE = "CREATION_PA_MOSA_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_MISADISPOSITION_STATE = "CREATION_PA_MISADISPOSITION_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_MISEPOSTDRAPEAU_STATE = "CREATION_PA_MISEPOSTDRAPEAU_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_MISEPERMISSIONAUT_STATE = "CREATION_PA_MISEPERMISSIONAUT_STATE";

	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_MISEPOSTHORCADRE_STATE = "CREATION_PA_MISEPOSTHORCADRE_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_RSTAGE_RSERV_STATE = "CREATION_PA_RSTAGE_RSERV_STATE";

	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_RENOUVELLEMENTDTH_STATE = "CREATION_PA_RENOUVELLEMENTDTH_STATE";

	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_DEMISSION_STATE = "CREATION_PA_DEMISSION_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_DDETACHEMENT_STATE = "CREATION_PA_DDETACHEMENT_STATE";

	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_SANCTION_STATE = "CREATION_PA_SANCTION_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_RETRAITE_ANTICIPE_STATE = "CREATION_PA_RETRAITE_ANTICIPE_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_BONIF_ECHELON_STATE = "CREATION_PA_BONIF_ECHELON_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_BONIF_ANCIEN_STATE = "CREATION_PA_BONIF_ANCIEN_STATE";

	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String TRANSMISSION_PA_NOMMINATION_CHEFSECTION_STATE = "TRANSMISSION_PA_NOMMINATION_CHEFSECTION_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String TRANSMISSION_PA_NOMMINATION_CHEFDRRS_STATE = "TRANSMISSION_PA_NOMMINATION_CHEFDRRS_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String TRANSMISSION_PA_NOMMINATION_DG_STATE = "TRANSMISSION_PA_NOMMINATION_DG_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String TRANSMISSION_PA_NOMMINATION_MINISTERE_STATE = "TRANSMISSION_PA_NOMMINATION_MINISTERE_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String ENREGISTREMENT_ACTE_NOMMINATION_STATE = "ENREGISTREMENT_ACTE_NOMMINATION_STATE";

	/**
	 * PARAMETRE GENERAL: Numéro dossier courrier
	 */
	public static final String PARAM_NUMDOSC = "NUMDOSC";

	/**
	 * TYPE DE DOSSIER: NOMINATION
	 */
	public static final String TYPE_DOSSIER = "NOMINATION";

	public static String STYLE_STATUSBAR_DEFAULT;
	public static int BBOX_MAX_ITEM_NBR = 5;
	public static final String FORMAT_HTML = "HTML";
	public static final String FORMAT_WORD = "WORD";
	public static final String FORMAT_EXCEL = "EXCEL";
	public static final String FORMAT_PDF = "PDF";
	public static final String CANDIDAT_ADMIS = "ADMIS";

	public static final String CANDIDAT_ADMIS_ATTACHE = "ATTACHE";

	public static final String CANDIDAT_ADMIS_NON_ATTACHE = "NON_ATTACHE";

	public static final String CANDIDAT_ADMIS_DOSSIERCREER = "DOSSIERCREER";

	/**
	 * Type dossier et acte
	 * 
	 */
	public static final String TA_NOMINATION = "NOMINATION";

	/**
	 * Type dossier et acte
	 * 
	 */
//	public static final String PATH_PJ_DOSSIER_URL = "http://localhost/DsidDocumentsCreated/";
//	public static final String PATH_PJ_DOSSIER_SIGNE_URL = "http://localhost/DocumentsSigned/";
//	public static final String PATH_PJ_DOSSIER = "C:\\xampp\\htdocs\\DsidDocumentsCreated\\";
//	public static final String PATH_PJ_DOSSIER_SIGNE = "C:\\xampp\\htdocs\\DocumentsSigned\\";

	public static final String PATH_PJ_DOSSIER_URL = "http://ldgdsap.mainframe.douanes.sn/DsidDocumentsCreatedKermel/";
	public static final String PATH_PJ_DOSSIER_SIGNE_URL = "http://ldgdsap.mainframe.douanes.sn/DocumentsSignedKermel/";
	public static final String PATH_PJ_DOSSIER = "/var/www/html/DsidDocumentsCreatedKermel/";
	public static final String PATH_PJ_DOSSIER_SIGNE = "/var/www/html/DocumentsSignedKermel/";
	
	public static final String PATH_PHOTOS_CONCOURS = "/docs/concours/photos/";
	public static final String PATH_PJ_CONCOURS = "/docs/concours/pj/";
	public static final String PATH_PJ_DEMANDEEXO = "/docs/demandeexo/";
	public static final String PHOTO_PERSO_DEFAULT = "pers.png";
	public static final String PHOTO_AGT_DEFAULT = "default.png";

	public static final String TYPACATEGORIEGRADE = "GRADE";
	public static final String TYPACATEGORIEPOSITION = "POSITION";
	public static final String TYPACATEGORIEAFFECTATION = "AFFECTATION";
	public static final String TYPACATEGORIESANCTION = "SANCTION";
	public static final String TYPACATEGORIEFORMATION = "FORMATION";
	public static final String TYPACATEGORIECHANGEMENT = "CHANGEMENT";
	public static final String TA_TITULARISATION = "DPT";
	public static final String TA_IMMATRICULATION = "IMMAT";
	public static final String TA_DISPONIBILITE = "DSP";
	public static final String TA_ENGAGEMENT = "ENGAGEMENT";
	public static final String TA_FIN_DISPONIBILITE = "FDSP";
	public static final String TA_RENOUVELLEMENTDSP = "RDSP";
	public static final String TA_DETACHEMENT = "DTH";
	public static final String TA_RENOUVELLEMENTDTH = "RDTH";
	public static final String TA_FIN_DETACHEMENT = "FDTH";
	public static final String TA_SANCTIONSCD = "SANCSD";
	public static final String TEST_TYPE = "typeDossier";
	public static final String SESSION_NUMDOS = "numDossier";

	// public static final String CURRENT_ACTION = "typeDossier";

	/*
	 * STATE IMMATRICULATIOn
	 */
	public static final String CREATION_DOSSIER_IMMATRICULATION_STATE = "CREATION_DOSSIER_IMMATRICULATION_STATE";

	/*
	 * STATE TITULARISATION
	 */
	public static final String CREATION_DOSSIER_TITULARISATION = "CREATION_DOSSIER_TITULARISATION_STATE";
	public static final String CREATION_DOSSIER_AA_STATE = "CREATION_DOSSIER_AA_STATE";
	public static final String CREATION_DOSSIER_PROMOTION_STATE = "CREATION_DOSSIER_PROMOTION_STATE";
	/*
	 * Test categorie type dossier
	 */
	public static final String STA_TITULARISATION = "TITULARISATION";
	public static final String TYPE_AGENT_CANDIDAT = "CANDIDAT";
	public static final String STA_IMMATRICULATION = "IMMATRICULATION";
	public static final String STA_CATEGORIEPOSITION = "POSITION";
	public static final String STA_CATEGORIESANCTION_PREMIER = "SANCTIONPREMIER";
	public static final String STA_CATEGORIESANCTION_SECOND = "SANCTIONSECOND";
	public static final String STA_CATEGORIESORTIEDEF = "SORTIEDEF";
	public static final String STA_CATEGORIESORTIETEMP = "SORTIETEMP";
	public static final String STA_CATEGORIECHANGEMENT = "CHANGEMENT";
	public static final String STA_FONCTIONNAIRE = "FONCTIONNAIRE";
	public static final String STA_NON_FONCTIONNAIRE = "NON_FONCTIONNAIRE";
	public static final String TYPE_AGT_FONCTIONNAIRE = "FONCTIONNAIRE";
	public static final String TYPE_AGT_NON_FONCTIONNAIRE = "NON_FONCTIONNAIRE";
	public static final String TYPE_AGT_CONTRACTUEL = "CONTRACTUEL";
	public static final String TYPE_AGT_ACONTRACTUEL = "ACONTRACTUEL";
	public static final String TYPE_AVCECHELLE = "AVECHELLE";
	public static final String STA_SAISIDOSC_STATE = "SAISIDOSC_STATE";
	public static final String STA_REJETERDOSC_STATE = "REJETERDOSC_STATE";
	public static final String STA_VALDERDOSC_STATE = "VALDERDOSC_STATE";
	public static final String STA_CANDADMIS_STATE = "CANDADMIS_STATE";
	public static final String STA_CANDRECALLE_STATE = "CANDRECALLE_STATE";
	public static final String STA_TRANSMFPUB_STATE = "TRANSMFPUB_STATE";
	public static final String ARR_SAISIDOSC_STATE = "SAISIDOSC";
	public static final String ARR_REJETERDOSC_STATE = "REJETERDOSC";
	public static final String ARR_VALDERDOSC_STATE = "VALDERDOSC";
	public static final String ARR_CANDADMIS_STATE = "CANDADMIS";
	public static final String ARR_CANDRECALLE_STATE = "CANDRECALLE";
	public static final String ARR_TRANSMFPUB_STATE = "TRANSMFPUB";
	public static final String STYLE_STATUSBAR_ERROR = "STATUSBAR_ERROR";
	public static final Object AGT_MATRICULE = "MATRICULE";
	public static final String PATH_AGT_PHOTOS = "PHOTOS";
	public static final String ATTRIBUTE_CODE = "code";
	public static final String ATTRIBUT_TYPED = "codetdem";
	public static final String PROFIL_DFPE="DFPE";
	public static final String PROFIL_CBFI="CBUREAU";
	public static final String PROFIL_BC="AGBURCOUR";
	public static final String  SIGNATURE_DFPE_STATE="SIGNATURE_DEMANDE";
	public static final String  TPIECE_TIT_EXO="TE";
	public static final String INIT_DEMANDE = "CREATION_DEMANDE";
	
	public static final String ATTRIBUT_TEXTES="codetexte";
	
	public static final String ATTRIBUT_ORGANISATION="txtcode";
	public static final String ATTRIBUT_MARCHE="txtcode";
	
	public static final String PATH_PJ_PROJET = "http://localhost:9617/birt/";
	
	public static final String TEST_CREATED="newDossier";
	public static final String ANNUAIRE_DCMP="DCMP";
	public static final String ANNUAIRE_ARMP="ARMP";
	
	public static final String path = System.getProperty("jboss.home.dir");
	public static final String PATH_INIT2 = path + "\\alldocs";
	public static final String PATH_INIT = PATH_INIT2 + "/kermel/";
	public static final String PATH_PJ = PATH_INIT + "pj/";
	
	//public static final String path = "C:\\wamp\\www\\togomp";
	//public static final String path = "/var/www/html/kermel";
	///public static final String PATH_INIT = path + "\\alldocs";
	//public static final String PATH_PJ = PATH_INIT + "/pj/";
	public static final String PATH_LETTREINVITATION = PATH_INIT + "lettreinvitation/";
//	public static final String path = "C:\\wamp\\www\\nigermp" ;
//	public static final String PATH_INIT = path + "\\fichiers";
//	public static final String PATH_PJ = PATH_INIT + "/";;
	public static final String PATH_PHOTOS = PATH_INIT + "/photos/";
	public static final String PATH_ARRETE = PATH_INIT + "/arrete/";
	
	public static final String PATH_PJCONT = PATH_INIT + "/contentieux/";
	public static final String PATH_RAPARMP = PATH_INIT + "/rapportarmp/";
	public static final String PATH_RAPDCMP = PATH_INIT + "/rapportdcmp/";
	public static final String PATH_ESPACELEGISLATION = PATH_INIT + "/espacelegislation/";
	public static final String PATH_DENOCIATION = PATH_INIT + "/denonciations/";
	public static final String PATH_AVISGENERAUX = PATH_INIT + "/avisgeneraux/";
	
	

	public static final int GARANTIE=1;
	public static final int NGARANTIE=-1;
	public static final String PARAM_SECTEURACTIVITE = "SECTEURACTIVITE";
	public static final int PARENT=1;
	public static final int NPARENT=0;
	public static final int MOINSDISANTQUALIFIE=2;
	public static final String REF_TYPEAC = "TYPEAC";
	public static final String REF_AUTORITEAC = "AUTORITEAC";
	public static final String REF_SERVICEAC = "SERVICEAC";
	public static final String REF_TYPESERVICE = "TYPESERVICE";
	public static final String REF_POLEDCMP = "POLEDCMP";
	public static final String REF_TYPEUNITEORGDCMP = "TYPEUNITEORGDCMP";
	public static final String REF_AUTORITEAUTC = "AUTORITEAUTC";

	public static final String REF_DIVISIONSDCMP = "DIVISIONSDCMP";
	public static final String REF_BUREAUXDCMP = "BUREAUXDCMP";
	public static final String REF_FONCTIONS = "FONCTIONS";
	public static final String REF_JOURSFERIES = "JOURSFERIES";
	public static final String REF_GARANTIS = "GARANTIS";
	public static final String REF_TYPESMARCHES = "TYPESMARCHES";
	public static final String REF_MODEPASSATION = "MODEPASSATION";
	public static final String REF_MODESELECTION = "MODESELECTION";
	public static final String REF_DELAIS = "DELAIS";
	public static final String REF_SECTEURSACTIVITES = "SECTEURSACTIVITES";
	public static final String REF_CATEGORIES = "CATEGORIES";
	public static final String REF_MONNAIEOFFRE = "MONNAIE_OFFRE";
	public static final String REF_NATPRIX = "NAT_PRIX";
	public static final String REF_GESTIONCRITERE = "GESTION_CRITERE";
	public static final String REF_GESTIONCATEGORIE = "GESTION_CATEGORIE";
	public static final String REF_GESTIONEVAL = "GESTION_EVAL";
	public static final String PARAM_PLANPASSATION = "PLANPASSATION";
	public static final String PARAM_REALSATIONS = "REALSATIONS";
	public static final String PARAM_BREALISATIONS = "BREALISATIONS";
	public static final String REF_TYPESMARCHESMS = "TYPESMARCHESMS";
	public static final String REF_TYPESMARCHESMP = "TYPESMARCHESMP";
	public static final String PLANPASSATION = "PLANPASSATION";
	public static final String PLANSPUBLIES = "PLANSPUBLIES";
	public static final String PLANAVALIDES = "PLANAVALIDES";
	public static final String REALSPLANS = "REALSPLANS";
	
	public static final String PARAM_REALISATION = "REALISATION";
	public static final String PARAM_PROCEDURESPASSATIONS = "PROCEDURESPASSATIONS";
	public static final String ANNUAIREARMP = "ANNUAIREARMP";
	public static final String ANNUAIREDCMP = "ANNUAIREDCMP";
	public static final String REF_COMMISSIONSMARCHES = "COMMISSIONSMARCHES";
	
	public static final Long PARAM_TMTRAVAUX = 3L;
	public static final Long PARAM_TMPI = 4L;
	public static final Long PARAM_TMSERVICES = 2L;
	public static final Long PARAM_TMFOURNITURES = 1L;
	public static final Long PARAM_TMDSP = 5L;
	public static final String REF_FOURNISSEURS = "FOURNISSEURS";
	public static final String REF_MEMBRES = "MEMBRES";
	public static final String REF_BAILLEURS = "BAILLEURS";
	public static final String USERS_TYPES_ARMP = "ARMP";
	public static final String USERS_TYPES_DCMP = "DCMP";
	public static final String USERS_TYPES_AC = "AC";
	public static final String USERS_TYPES_AGENTAC = "AGENTAC";
	
	public static final String PARAM_TYPEDOCUMENTS_PVOUVERTURE = "pmb_pvouverture";
	public static final String PARAM_TYPEDOCUMENTS_EVALUATION = "pv_evaluation";
	public static final String PARAM_TYPEDOCUMENTS_ATTIBUTIONS = "pmb_attributions";
	public static final String PARAM_TYPEDOCUMENTS_AJOUTDOSSIERS = "ajoutdossier";
	public static final String GESTION_UTILISATEURARMP = "UTILISATEURARMP";
	public static final String GESTION_UTILISATEURDCMP = "UTILISATEURDCMP";
	public static final String GESTION_UTILISATEURAC = "UTILISATEURAC";
	public static final String PARAM_TYPEDOCUMENTS_RESULTATNEGOCIATION ="pv_resultatnegociation";
	public static final String PARAM_TYPEDOCUMENTS_ATTIBUTIONSPROVISOIRE = "pmb_attributionsp";
	public static final String PARAM_TYPEDOCUMENTS_ATTIBUTIONSDEFINITIVE = "pmb_attributionsd";
	public static final String PARAM_TYPEDOCUMENTS_LETTREINVITATION = "lettreinvitation";
	
	public static final String REF_TYPEDECISION = "TYPEDECISION";
	public static final String PARAM_CFA = "CFA";
	public static final String ARR_INIAUDIT = "INIAUDIT";
	public static final String REF_DECISION = "DECISION";
	public static final String REF_CONTENTIEUX = "CONTENTIEUX";
	public static final String REF_PJCONT = "PJCONT";
	public static final String REF_PAIEMENT = "PAIEMENT";
	
	public static final String CONTRAT_PAYE = "OUI";
	public static final String CONTRAT_NPAYE = "NON";
	public static final String REF_SUIVIPAIEMENT = "SUIVIPAIEMENT";
	public static final String REF_OPERATION = "OPERATION";
	public static final String REF_RECOUVREMENT = "RECOUVREMENT";
	public static final String REF_SUIVIRECOUVREMENT = "SUIVIRECOUVREMENT";
	public static final String REF_OPERATIONDAO = "OPERATIONDAO";
	public static final String REF_BANQUE = "BANQUE";
	public static final String REGLEMENTATION_NATIONALE = "NATIONALE";
	public static final String REGLEMENTATION_COMMUNAUT = "COMMUNAUTAIRE";
	public static final String REG_DIRECTIVE = "DIRECTIVE";
	public static final String REG_DECRET = "DECRET";
	public static final String REG_DECISION = "DECISION";
	public static final String REG_ARRETE = "ARRETE";
	public static final String REF_REGNAT = "REGNATIONALE";
	public static final String REF_REGCOMM = "REGCOMMUNAUTAIRE";
	public static final String REF_RAPARMP = "RAPPORTARMP";
	public static final String REF_RAPFORMATION = "RAPPORTFORMATION";
	public static final String REF_RAPDCMP = "RAPPORTDCMP";
	public static final String DIRECTION_DCMP = "DIRECTIONDCMP";
	public static final String REF_TYPEUNITEORGARMP = "TYPEUNITEORGARMP";
	public static final String REF_UNITEORGARMP = "UNITEORGARMP";
	
	public static final String REF_PRESTATAIRES = "PRESTATAIRES";
	public static final String REF_PAYS = "PAYS";
	public static final String DOSSIERSTYPE = "DOSSIERSTYPE";
	public static final String AVIGENERAL = "AVIGENERAL";
	public static final String REFCATEGORIES = "REFCATEGORIES";
	public static final String FAMILLES = "FAMILLES";
	public static final String PRODUITS = "PRODUITS";
	public static final String PROCEDUREDEROGATOIRE = "PROCEDUREDEROGATOIRE";
	public static final String PROCEDURESDEROGATOIRES = "PROCEDURESDEROGATOIRES";
	public static final String DPROCEDURESDEROGATOIRES = "DPROCEDURESDEROGATOIRES";
	public static final String RAPPORT = "RAPPORT";
	public static final String CATEGORIEDOCUMENT = "CATEGORIEDOCUMENT";
	public static final String TYPECOURRIER = "TYPECOURRIER";
	public static final String TYPESDOSSIERS = "TYPESDOSSIERS";
	public static final String MODERECEPTION = "MODERECEPTION";
	public static final String MODELEDOCUMENT = "MODELEDOCUMENT";
	public static final String PIECESREQUISES = "PIECESREQUISES";
	public static final String MODETRAITEMENT = "MODETRAITEMENT";
	public static final String GRILLESANALYSES = "GRILLESANALYSES";
	public static final String CRITEREDANALYSE = "CRITEREDANALYSE";
	public static final String TYPEMODELEDOCUMENT = "TYPEMODELEDOCUMENT";
	public static final String NATURECOURRIER = "NATURECOURRIER";
	public static final String AUDITS = "AUDITS";
	public static final String REF_PIECES = "PIECES";
	public static final String REF_DENONCIATION = "DENONCIATION";
	public static final String REF_DENONCIATIONP = "DENONCIATIONP";
	public static final String REF_DENONCIATIONPO = "DENONCIATIONPO";
	public static final String REF_DENONCIATIONC = "DENONCIATIONC";
	public static final String REF_DENONCIATIONS = "DENONCIATIONS";
	public static final String REF_DENONCIATIONN = "DENONCIATIONN";
	public static final String SUIVIDENONCIATION = "SUIVIDENONCIATION";
	public static final String CONFTYPESDOSSIERS = "CONFTYPESDOSSIERS";
	public static final String Suivi_DENONCIATIONP = "Suivi_DENONCIATIONP";
	public static final String DENONCIATIONPORTAIL = "PORTAIL";
	public static final String DENONCIATIONCOURRIER = "COURRIER";
	public static final String DENONCIATIONNUMEROVERT = "NUMEROVERT";
	public static final String DENONCIATIONPOUBELLE = "POUBELLE";
	public static final String REF_TYPEDEMANDE = "REF_TYPEDEMANDE";
	
	//public static final String IP_SSI = "localhost";
	public static final String IP_SSI = "localhost";
	public static final String PORT_SSI = "8080";
	
	public static final int DOSSIERAVALIDER=0;
	public static final int DOSSIERVALIDEE=1;
	public static final int DOSSIERREJETEE=2;
	public static final int DOSSIERSOUSRESERVE=3;
	public static final String PARAM_DOSSIERSPP = "DOSSIERSPP";
	public static final String PARAM_EXECUTIONDAO = "EXECUTIONDAO";
	public static final String REF_MONTANTSSEUILS = "MONTANTSSEUILS";
	public static final String REF_SEUILSCOMMUNAUTAIRE = "SEUILSCOMMUNAUTAIRE";
	public static final String REF_SEUILSRAPRIORI = "SEUILSRAPRIORI";
	public static final String TYPE_SEUILSPASSATION = "SEUILSPASSATION";
	public static final String TYPE_SEUILSCOMMUNAUTAIRE = "SEUILSCOMMUNAUTAIRE";
	public static final String TYPE_SEUILSRAPRIORI = "SEUILSRAPRIORI";
	public static final String REF_DIRECTIONSERVICE = "DIRECTIONSERVICE";
	
	public static final String FEA_COURRIERAC = "FEA_COURRIERAC";
	public static final String FEA_COURRIERACDOSS = "FEA_COURRIERACDOS";
	public static final String PIECEFOURNIE = "F";
	public static final String PIECENONFOURNIE = "NF";
	public static final String PLANPUB = "PUB";
	
	public static final String REF_CONT_DEC = "REF_CONT_DECISION";
	public static final String REF_QUITUS = "QUITUS";
	public static final String FEA_SUIVIMARCHE = "FEA_SUIVIMARCHE";
	public static final String PARAM_NUMIMMATRICULATION = "NUMIMMATRICULATION";
	
	public static final String REF_SPECIALITE = "SPECIALITE";
	public static final String REF_MODULE = "MODULE";
	public static final String REF_FORMATEUR = "FORMATEUR";
	public static final String REF_FORMATION = "FEA_FORMATION";
	public static final String REF_FORMATIONSUIVI = "FEA_SUIVIFORMATION";
	public static final String REF_BAILLEUR = "FEA_BAILLEUR";
	public static final String REF_PRIX = "PRIX";
	public static final String REF_VALPUBPRIX = "VALPUBPRIX";
	

	
	

    public static final String DOSSIERAC ="DOSSIERAC";
    public static final String DOSSIERAC_RETARD ="RETARD";
    public static final String DOSSIERAC_ENCOURS ="ENCOURS";
    public static final String REVUE_DAO ="RDAO";
    public static final String VAL_ATTRIB_PROV ="VAP";
    public static final String EXAM_JUR_TEC ="EJT";
    public static final String VAL_DMD_PROC_DEROG ="VDPD";
    public static final String VAL_AVENANT ="VAV";
    public static final String DMD_IMMAT ="DIM";
    public static final String VALP_MPPM ="VPNPM";
    public static final String VAL_NPPM ="VPMPM";

    public static final String TRAITEMENT_DOSSIER="TRAITEMENTDOS";
    public static final String  FEA_COURRIERACD= "FEA_COURRIERACD";
    
    public static final String ARROW_SAISIDOSSIER = "SAISIDOSSIER";
    
    public static final String DOS_JOURNAL ="http://10.0.0.75/togomp/fichiers/";
    public static final String REF_JOURNAL = "REF_JOURNAL";
    public static final String REF_GLOSSAIRE = "REF_GLOSSAIRE";
    public static final String PATH_CODEMARCHE = PATH_INIT + "/codemarche/";
    
    public static final String REF_GESTIONNOEUDARCHIVE = "GESTIONNOEUDARCHIVE";
    public static final String REF_TYPESELEMENT = "REF_TYPESELEMENT";
    
    public static final String PARAM_TYPEDOCUMENTS_MEVDPPM = "mavdossier";
    public static final String PARAM_TYPEDOCUMENTS_MEVATTRP = "mavattprov";
    public static final String PARAM_TYPEDOCUMENTS_PUBDAO = "publicationdao";
    public static final String PARAM_TYPEDOCUMENTS_PUBATTRP = "publicationattprov";
    public static final String PARAM_TYPEDOCUMENTS_NOTIFICATIONCANDIDAT = "notificationcandidat";
    public static final String PARAM_TYPEDOCUMENTS_SIGMARCHE = "signaturemarche";
    public static final String PARAM_TYPEDOCUMENTS_VALIDATIONDAO = "validationdao";
    
    public static final String MAJPLANAVALIDES = "MAJPLANAVALIDES";
    public static final String FEA_PROJETREPONSE = "FEA_PROJETREPONSE";
    
    public static final String PHOTO_LOGO_DEFAULT = "pers.png";
    public static final String FEA_MODELE_ARC = "FEA_MODELE_ARC";
    public static final String PATH_PJCONTDENONCIATION = "PATH_PJCONTDENONCIATION";
    
    public static final String REF_TAUX = "TAUX";
    public static final String REF_TAUX_DAO = "TAUXDAO";
    
	//public static final String path_portail = "C:\\wamp\\www\\togomp" ;
	public static final String path_portail = PATH_INIT;//"/var/www/html/kermel";
	public static final String PATH_INIT_PORTAIL = path_portail + "\\documents";
    public static final String PATH_PJ_PORTAIL = PATH_INIT_PORTAIL + "/";
    
    public static final String VALDMDDEROG = "VALDMDDEROG";
    
    public static final String GROUPES = "GROUPES";
    public static final String GROUPESUTIL = "GROUPESUTIL";
    
    public static final String CONTENT = "CONTENT";
    public static final String FICHEDOSSIER = "FICHEDOSSIER";
    public static final String ARRETES = "ARRETES";
    public static final String FEA_LISTE_ROUGE_FOURNISSEUR = "FEA_LISTE_ROUGE_FOURNISSEUR";
    public static final String GROUPE = "GROUPE";
    
    public static final String REF_TYPERAPPORT = "TYPERAPPORT";
    public static final String REF_LOGISTICIEN = "LOGISTICIEN";
    public static final String REF_COORDONNATEUR = "COORDONNATEUR";
    public static final String REF_GROUPE = "GROUPEFORMATION";
    public static final String REF_RUBRIQUEFORM = "RUBFORMATION";
    public static final String REF_SOUSRUBRIQUEFORM = "SOUSRUBFORMATION";
    public static final String REF_CURSSUS = "CURSSUS"; 
    public static final String REF_PARTFORMATION = "REF_PARTFORMATION";
    
    public static final String REF_PJDENONCIATION = "PJDENONCIATION";
    
    public static final String  NUMENREGISTREMENT = "NUMENREGISTREMENT";
    
    public static final String  COURRIERARRIVE = "ARRIVE";
    public static final String  COURRIERDEPART = "DEPART";   
    public static final String FEA_COURRIERDEPART = "COURRIERDEPART";
    public static final String  NUMENREGISTREMENTCD = "NUMENREGISTREMENTCD";
    
    public static final String OUVERTUREPLIS = "OUVERTUREPLIS";
    
    
	public static final String pathcriptage = System.getProperty("jboss.home.dir");
	public static final String PATH_INIT_CRIP = pathcriptage + "\\alldocs";
	public static final String PATH_PJ_CRIP = PATH_INIT_CRIP + "/kermel/cryptographie/";
	
	public static final String URL = ToolKermel.getParametre("SERVER_IP");
	public static final String SURL = ToolKermel.getParametre("SECURE_SERVER_IP");
	public static final String PORT = ToolKermel.getParametre("PORT");
	public static final String SPORT = ToolKermel.getParametre("SPORT");
	public static final String WEB_URL = ToolKermel.getParametre("URL");
	public static final String SWEB_URL = ToolKermel.getParametre("SURL");
	public static final String pathplis = "C:\\certificats";
	public static final String PATH_INIT_PLIS = pathplis + "\\T_DER_0049";
	public static final String PATH_PLIS = PATH_INIT_PLIS + "/plis/";
	public static final String PATH_CERIFICAT = PATH_INIT + "/certificats/";
	public static final String DOCSURL = URL+"/docs/";
	public static final String PJ_URL =  DOCSURL +"pj/";
	
	public static final String urlServeurPlis = URL+"/kermel/DepotPli/";
	public static final String dezipsRepertoire = "C:\\wamp\\www\\kermel\\DepotDezip\\";
	public static final String zipsRepertoire = "C:\\wamp\\www\\kermel\\DepotZip\\";
	//public static final String CERTSPATH = "C:\\wamp\\www\\certs\\";  
	public static final String CERTSPATH = "C:\\wamp\\www\\kermel\\certs\\";
	//public static final String DOCSPATH = "C:\\wamp\\www\\docs\\";
	public static final String DOCSPATH = "C:\\wamp\\www\\kermel\\docs\\";	
	//public static final String plisRepertoire = "C:\\wamp\\www\\DepotPli\\";
	public static final String plisRepertoire = "C:\\wamp\\www\\kermel\\DepotPli\\";
	public static final String certificatsUrl = URL+"/kermel/certs/";	
	public static final String cheminKeyP12SignatureAccuse = "/CERT/dcmp.p12";
	public static final String urlKermel = ToolKermel.getParametre("PORTAL_URL");
	public static final String SURLKERMEL = ToolKermel.getParametre("SPORTAL_URL");
	public static final String urlLogoKermel = URL+":8080/PkermelV2/assets/img/logo.png";
	public static	final DateFormat   dateFr = DateFormat.getDateInstance(DateFormat.MEDIUM);
	public static	final DateFormat formatHeure = new SimpleDateFormat("HH:mm:ss");
	
	public static final String OBJET_MESSAGE = "TRANSMISSION LETTRE D'INVITATION ET TERMES DE REFERENCES";
}