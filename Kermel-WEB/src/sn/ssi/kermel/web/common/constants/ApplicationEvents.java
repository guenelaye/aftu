package sn.ssi.kermel.web.common.constants;

public class ApplicationEvents {
	public static final String ON_MODEL_CHANGE = "onModelChange";
	public static final String ON_MODEL_CHANGE_CORPS = "onModelCorps";
	public static final String ON_MODEL_CHANGE_CATEGORIE = "onModelCategorieChange";
	public static final String ON_MODEL_CONCOURS_CHANGE = "onModelConcoursChange";
	public static final String ON_MODEL_CANDIDAT_CHANGE = "onModelCandidatChange";
	public static final String ON_MODEL_CHANGE_DIPLOME = "onModelDiplome";

	public static final String ON_VALIDATE = "onValidate";
	public static final String ON_IMPUTER = "onImputer";
	public static final String ON_GENERER = "onGenerer";
	public static final String ON_REJETER = "onRejeter";
	public static final String ON_DISPATCHER = "onDispatcher";
	public static final String ON_VENTILLER = "onVentiller";
	public static final String ON_PRINT = "onPrint";

	public static final String ON_AUTRE_ACTION = "onAutreAction";
	public static final String ON_AUTRE_ACTION1 = "onAutreAction1";
	public static final String ON_MODULEFORM = "onModuleFormation";
	public static final String ON_RECHER = "onrecher";

	public static final String ON_HLP = "onAdd";
	public static final String ON_EDIT = "onEdit";
	public static final String ON_CONF = "onConf";
	public static final String ON_SOUMETTRE = "onSoumettre";
	public static final String ON_ADD = "onAdd";
	public static final String ON_ADDUSERDNCMP = "onAdduserdncmp";
	public static final String ON_ADDUSERAC = "onAdduserac";
	public static final String ON_EDITUSERDNCMP = "onEdituserdncmp";
	public static final String ON_EDITUSERAC = "onEdituserac";
	public static final String ON_DELETE = "onDelete";
	public static final String ON_DETAILS = "onDetails";
	public static final String ON_CLOSE = "onClose";
	public static final String ON_REALISATIONS = "onRealisations";
	public static final String ON_VALIDATIONS = "onValidations";
	public static final String ON_DEPUBLIER = "onDepublier";
	public static final String ON_PUBLIER = "onPublier";
	public static final String ON_BAILLEURS = "onBailleurs";
	public static final String ON_GERER = "onGerer";
	public static final String ON_DOUBLE_CLICK = "onDoubleClick";
	public static final String ON_CLICK = "onClick";
	public static final String ON_PAGING = "onPaging";
	public static final String ON_SELECT = "onSelect";
	public static final String ON_CONFIG = "onConfig";

	public static final String ON_MESSAGE_BOX_CONFIRM = "onMessageBoxConfirm";
	public static final String ON_MODEL_PIECE_CHANGE = "onModelPieceChange";
	public static final String ON_MODEL_AGENT_CHANGE = "onModelAgentChange";
	public static final String ON_ADD_NOTE = "onAddNote";
	public static final String ON_MODEL_CHANGE_CADRE = "onModelCadreChange";
	public static final String ON_NATIONALITE = "onNationalite";
	public static final String ON_PAYS = "onPays";
	public static final String ON_LOCALITE = "onLocalite";
	public static final String ON_CORPS = "onCorps";
	public static final String ON_DIPLOME = "onDiplome";
	public static final String ON_CENTRES = "onCentres";
	public static final String ON_SALLES = "onSalles";
	public static final String ON_EPREUVES = "onEpreuves";
	public static final String ON_DOSSIERS = "onDossiers";
	public static final String ON_NOTES = "onNotes";
	public static final String ON_PJOINTES = "onPieces";
	public static final String ON_CONDIVERS = "onCondivers";
	public static final String ON_CONDOSSIERS = "onCondossiers";
	public static final String ON_SERVICES = "onServices";
	public static final String ON_TYPES = "onTypes";
	public static final String ON_MODES = "onModes";
	public static final String ON_CATEGORIES = "onCategories";
	public static final String ON_MODESSELECTIONS = "onModesselections";
	public static final String ON_NATURES = "onNatures";
	public static final String ON_MONNAIES = "onMonnaies";
	public static final String ON_PIECES = "onPieces";
	public static final String ON_CRITERES = "onCriteres";
	public static final String ON_MEMBRES = "onMembes";
	public static final String ON_PLIS = "onPlis";
	public static final String ON_BUREAUX = "onBureaux";
	public static final String ON_AUTORITES = "onAutorites";
	public static final String ON_AUTORITE = "onAutorite";
	public static final String ON_PRESTATAIRE = "onPrestataire";
	public static final String ON_PLANS = "onPlans";
	public static final String ON_TYPE_AUTORITES = "onTypeAutorites";
	public static final String ON_TYPE_MARCHES = "onTypeMarches";
	public static final String ON_MODE_PASSATIONS = "onModePassation";
	public static final String ON_LOTS = "onLots";
	public static final String ON_AUTORITESPRES = "onAutoritespres";

	public static final String ON_MODEL_REGIME_CHANGE = "onModelRegimeChange";
	public static final String ON_MODEL_TEXTE_CHANGE = "onModelTexteChange";
	public static final String ON_MODEL_CODEBF_CHANGE = "onModelCodeBfChange";
	public static final String ON_MODEL_CODEPPMIMP_CHANGE = "onModelCodePpmimpChange";
	public static final String ON_MODEL_CODEPPMBEN_CHANGE = "onModelCodePpmbenChange";
	public static final String ON_PRODUITS = "onProduits";
	public static final String ON_PERSONNE = "onPersonne";
	public static final String ON_PROGRAMME = "onProgramme";
	public static final String ON_VEHICULES = "onVehicules";
	public static final String ON_PRODUIT_TEXTE = "onProduitTexte";
	
	public static final String ON_NUMSERIE = "onNumserie";
	public static final String ON_MODEL_LIST_PRODUIT_CHANGE = "onProduitList";
	public static final String ON_MODEL_LIST_VEHICULE_CHANGE="onVehiculesChange";
	public static final String ON_MODEL_LIST_FACTURE_CHANGE="onFActure";
	public static final String ON_MODEL_LIST_PJ_CHANGE="onPieceJointe";
	public static final String ON_SEARCH_TPDMD_BANDBOX="onSearchTpdmd";
	public static final String ON_SEARCH_PPMBEN_BANDBOX="onSearchPpmben";
	public static final String ON_SEARCH_PPMIMP_BANDBOX="onSearchPpmimp";
	public static final String ON_SEARCH_BF_BANDBOX="onSearchBf";
	public static final String ON_SEARCH_REGIME_BANDBOX="onSearchRegime";
	public static final String ON_SEARCH_TEXTE_BANDBOX="onSearchTexte";
	public static final String ON_SEARCH_PRODUIT_BANDBOX="onSearchProduit";
	public static final String ON_SEARCH_VEHICULE_BANDBOX="onSearchVehicule";
	public static final String ON_SEARCH_TYPE_PIECE_BANDBOX="onSearchProduit";
	public static final String ON_SEARCH_BANDBOX="onSearch";
	public static final String ON_SEARCH_UNIT_BANDBOX="onSearchUnit";
	public static final String ON_SEARCH_MODEL_BANDBOX="onSearchModel";
	public static final String ON_SEARCH_MARQUE_BANDBOX="onSearchMarque";
	public static final String ON_UNIT="onUnit";
	public static final String ON_MARQUES="onMarques";
	public static final String ON_MODELES="onModeles";
	public static final String ON_SEARCH_PPM_BANDBOX="onSearchpPpm";
	public static final String ON_SEARCH_BANQUE_BANDBOX="onSearchpBanque";
	public static final String ON_SEARCH_ORGANISATION_BANDBOX="onSearchOrganisation";
	public static final String ON_SEARCH_TEXTE_REGLEMENTAIRE_BANDBOX="onSearchTexteReglementaire";
	public static final String ON_SEARCH_TYPEORGANISATION_BANDBOX="onSearchTypeOrganisation";
	
	
	public static final String ON_TYPEDECISION = "onTypeDecision";
	public static final String ON_DECISION = "onDecision";
	public static final String ON_CLASSER = "onClasser";
	
	public static final String ON_PROCEDURE = "onProcedure";
	public static final String ON_SUIVI = "onSuivi";
	public static final String ON_FOURNISSEUR = "onFournisseur";
	public static final String ON_CONTRAT = "onContrat";
	public static final String ON_EDITER = "onEditer";
	public static final String ON_BANQUE = "onBanque";
	public static final String ON_TYPEUNITEORG = "onTypeUniteOrg";
	
	public static final String ON_ORDRESERVICEDEMARRAGEDAO = "onORDRESERVICEDEMARRAGEDAO";
	public static final String ON_RECEPTIONPROVISOIREDAO = "onRECEPTIONPROVISOIREDAO";
	public static final String ON_WRECEPTIONDEFINITIVEDAO = "onWRECEPTIONDEFINITIVEDAO";
	public static final String ON_APPELOFFRE= "onAppeloffre";
	public static final String ON_SAISIPRIX = "onSaisiprix";
	public static final String ON_VALIDER = "onValider";
	
	
	public static final String ON_TRAITEMENTS="onTraitements";
	public static final String ON_RECEPTIONS="onReceptions";
	public static final String ON_AGENTS="onAgents";
	public static final String ON_ORIGINES="onOrigines";
	public static final String ON_PIECESJOINTES="onPiecesjointes";		
	public static final String ON_USER = "onUser";
	public static final String ON_FICHE = "onFiche";
	public static final String ON_AUTORITESAUDITS = "onAutoritesaudits";
	
	public static final String ON_PREV = "onPrev";
	public static final String ON_NEXT = "onNext";
	public static final String ON_CLICK_LISTPRESENCE = "onClickLP";
	public static final String ON_CLICK_LISTREPRESENTANTS = "onClickLR";
	public static final String ON_CLICK_OUVERTURE = "onClickOuv";
	public static final String ON_CLICK_LECTEURE = "onClickLC";
	public static final String ON_CLICK_PV = "onClickPV";
	public static final String ON_CLICK_PIECES = "onClickPA";
	
}
