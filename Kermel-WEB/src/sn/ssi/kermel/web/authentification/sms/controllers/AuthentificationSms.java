package sn.ssi.kermel.web.authentification.sms.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Button;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.security.ProfilsSession;
import sn.ssi.kermel.be.security.UtilisateurSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class AuthentificationSms extends AbstractWindow implements AfterCompose {

	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;
	private Textbox txtloginPR, txtmdpPR;
	private Textbox txtcodePR, txtloginSG;
	private Textbox txtcodeSecretaire, txtmdpSG;
	List<Utilisateur> users = new ArrayList<Utilisateur>();
	Utilisateur user;
	private Button btnloginPR,btnloginSGS;
	private SygAutoriteContractante autorite;
	private String errorMsg, errorComponent, codeSMS;
	private Utilisateur infoscompte;
	private Label lblErrorPresident, lblErrorSG;
	private Div etapeSMSPR, etapeAuthPR, etapesmsSG, btnconnexionPR, btnValidePR, etapeAuthSG, btnValideSG,
			btnconnexionSG,btnloginSG;
	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	private String numeroSMS, numerotelephone, msg = null;
	private Session session=getHttpSession();

	@Override
	public void afterCompose() {
		// TODO Auto-generated method stub
		Components.wireVariables(this, this);
		Components.addForwards(this, this);
		if (session
				.getAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE) == null) {
			session.setAttribute(
					org.zkoss.web.Attributes.PREFERRED_LOCALE,
					new Locale("fr"));
			session.setAttribute("Prose_Locale", "fr");
			session.setAttribute("pageDeconnexion", "ouverture.zul");
		}
	}

	public void onClick$btnloginPR() {
		users = BeanLocator.defaultLookup(ProfilsSession.class).findUtilisateur(txtloginPR.getValue(),
				txtmdpPR.getValue(), "POPLIS", null);
		if (users.size() > 0) {
			user = users.get(0);
			autorite = user.getAutorite();
			txtmdpPR.setValue("");
			txtloginPR.setValue("");
			etapeAuthPR.setVisible(false);
			etapeSMSPR.setVisible(true);
			btnconnexionPR.setVisible(false);
			btnValidePR.setVisible(true);
			envoyerPresidentSMS();
			
		} else {
			lblErrorPresident.setVisible(true);
			lblErrorPresident.setValue(Labels.getLabel("kermel.common.authentification.erreur"));
			lblErrorPresident.setStyle("color:red");
			errorMsg = Labels.getLabel("kermel.common.authentification.mdp.erreur");
			txtloginPR.setValue("");
			txtmdpPR.setValue("");
			throw new WrongValueException(txtloginPR, errorMsg);
		}

	}

	public void onClick$btnloginSGS() {
		users = BeanLocator.defaultLookup(ProfilsSession.class).findUtilisateur(txtloginSG.getValue(),
				txtmdpSG.getValue(), "SOPLIS", autorite);
		if (users.size() > 0) {
			user = users.get(0);
			if ((user.getType().equals(UIConstants.USERS_TYPES_AC))
					|| (user.getType().equals(UIConstants.USERS_TYPES_AGENTAC))) {
				if (user.getActif() == true) {
					final Session session = getHttpSession();
					session.setAttribute("user", user.getLogin());
					session.setAttribute("utilisateur", user);
					session.setAttribute("profil", user.getSysProfil().getPfCode());
					infoscompte = BeanLocator.defaultLookup(UtilisateurSession.class).InfosCompte(user.getLogin());
					session.setAttribute("infoscompte", infoscompte);
					envoyerPresidentSMS();
					// Pour mes TEST
					etapeAuthSG.setVisible(false);
					etapesmsSG.setVisible(true);
					btnloginSG.setVisible(false);
					btnValideSG.setVisible(true);
					// Fin de mes TEST
				} else {
					lblErrorSG.setVisible(true);
					lblErrorSG.setValue(Labels.getLabel("compte.nonactif"));
					lblErrorSG.setStyle("color:red");
					errorMsg = Labels.getLabel("compte.nonactif");
					txtloginSG.setValue("");
					txtmdpSG.setValue("");
					throw new WrongValueException(txtmdpSG, errorMsg);
				}
			}
		}
	}

	public static String randomAlphaNumeric(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	public void envoyerPresidentSMS() {
		numeroSMS = randomAlphaNumeric(5);
		numerotelephone = user.getTelephone();
		msg = "Boujour " + user.getPrenom() + " " + user.getNom() + " votre identifiant SMS est  " + numeroSMS;
		if (numeroSMS != null) {
//			Code SMS
//			user.setCodesms(numeroSMS);
//			BeanLocator.defaultLookup(UtilisateurSession.class).updateUtilisateur(user);
//			Activation du PUSH SMS
			System.out.println("Votre code SMS est la : " +numeroSMS);
//			SendSMS sms = new SendSMS();
//			try {
////				sms.send(numerotelephone, msg);
//				System.out.println("message :"+msg);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			Fin activation PUSH SMS
		}
	}

	public void activateAuthSG() {
		etapeSMSPR.setVisible(false);
		btnValidePR.setVisible(false);
		etapeAuthSG.setVisible(true);
		btnloginSG.setVisible(true);
		etapesmsSG.setVisible(false);
		btnValideSG.setVisible(false);
		
	}

	public void onClick$btnValideCodePR() {
		codeSMS = txtcodePR.getValue();
		validatecodeSMSPR(codeSMS);
	}

	public void onClick$btnValideCodeSG() {
		codeSMS = txtcodeSecretaire.getValue();
		validatecodeSMSSG(codeSMS);
	}

	public void validatecodeSMSPR(String codeSMS) {
		user = BeanLocator.defaultLookup(ProfilsSession.class).findByCodeSMS(codeSMS);
		if (user != null) {
			activateAuthSG();
			
		} else {

		}
	}

	public void validatecodeSMSSG(String codeSMS) {
		user = BeanLocator.defaultLookup(ProfilsSession.class).findByCodeSMS(codeSMS);
		if (user != null) {
			Executions.sendRedirect("/templates/default/main.zul#dossiers_a_ouvrir");
		} else {

		}
	}
}
