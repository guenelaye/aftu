package sn.ssi.kermel.web.authentification.controllers;

import java.util.List;
import java.util.Locale;

import org.mindrot.jbcrypt.BCrypt;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.security.ProfilsSession;
import sn.ssi.kermel.be.security.UtilisateurSession;
import sn.ssi.kermel.be.session.ComptePersoSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class LoginController extends AbstractWindow implements AfterCompose {

	private static final long serialVersionUID = 1L;
	Textbox txtLogin, txtPassword;
	Button authOk;
	private Label lblErreur;
	private String errorMsg;
	private Component errorComponent;
	private static final String READY_MSG_STYLE = "";
	private static final String ERROR_MSG_STYLE = "color:red";
	private Utilisateur infoscompte;

	public void afterCompose() {
		Components.wireVariables(this, this);
		Components.addForwards(this, this);
	}

	public void onOK() {
		
		if(txtLogin.getValue() == null || txtLogin.getValue().isEmpty()) 
			throw new WrongValueException(txtLogin, "Veuilez renseigner ce champ");
		
		if(txtPassword.getValue() == null || txtPassword.getValue().isEmpty()) 
			throw new WrongValueException(txtPassword, "Veuilez renseigner ce champ");

		Utilisateur user  = BeanLocator.defaultLookup(ProfilsSession.class).findByLogin(txtLogin.getValue());

		
		
		if (user != null) {
//			if(!BCrypt.checkpw(txtPassword.getValue(), user.getPassword()))
//					user = null; 

		}
		
		if(user == null)
			throw new WrongValueException(txtLogin, "Connexion impossible. Login ou mot de passe incorrecte");
		
		final Session session = getHttpSession();
		session.setAttribute("user", user.getLogin());
		session.setAttribute("utilisateur", user);
		
		session.setAttribute("profil", user.getSysProfil().getPfCode());
		infoscompte=BeanLocator.defaultLookup(UtilisateurSession.class).InfosCompte(user.getLogin());
		session.setAttribute("infoscompte", infoscompte);
		if(session.getAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE)== null){
			session.setAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE, new Locale("fr"));
			session.setAttribute("Prose_Locale", "fr");
		}
		Executions.sendRedirect("./templates/default/main.zul#accueil");

		
		
			if(user.getType() != null && (user.getType().equals(UIConstants.USERS_TYPES_AC)||(user.getType().equals(UIConstants.USERS_TYPES_AGENTAC))))
			{
				
				session.setAttribute("autorite", user.getAutorite());
				
				
			}
			

			
		
	}

	public void onClick$tbBtnlogin() {
		onOK();
	}
}