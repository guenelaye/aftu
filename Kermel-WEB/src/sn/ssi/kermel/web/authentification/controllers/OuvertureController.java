package sn.ssi.kermel.web.authentification.controllers;

import java.util.List;
import java.util.Locale;

import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.A;
import org.zkoss.zhtml.impl.AbstractTag;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.security.ProfilsSession;
import sn.ssi.kermel.be.security.UtilisateurSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class OuvertureController extends AbstractWindow implements AfterCompose {

	private Div divGo;
	private Utilisateur infoscompte;
	private Label lblErreur;
	private String errorMsg;
	private Textbox txtPassword;
	private Textbox errorComponent;
	private Textbox txtLogin;

	@Override
	public void afterCompose() {

		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		 String login = "SERIALNUMBER=aa5ffacf0528a3190e65a3e31efe2ea023b285cb, CN=ABDOU NDIAYE, OU=SN 21027052Z3, O=STRATEGIE ET SOLUTION INFORMATIQUE, C=SN";
				 List<Utilisateur> users =
				 BeanLocator.defaultLookup(ProfilsSession.class).findUtilisateur(login);
				 Utilisateur user=users.get(0);
				if (user != null) {

					if (user.getType().equals(UIConstants.USERS_TYPES_AC)
							|| user.getType().equals(UIConstants.USERS_TYPES_AGENTAC)) {
						if (user.getActif() == true) {
							final Session session = getHttpSession();
							session.setAttribute("user", user.getLogin());
							session.setAttribute("utilisateur", user);
							session.setAttribute("profil", user.getSysProfil()
									.getPfCode());
							infoscompte = BeanLocator.defaultLookup(
									UtilisateurSession.class).InfosCompte(
									user.getLogin());
							session.setAttribute("infoscompte", infoscompte);

							if (session
									.getAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE) == null) {
								session.setAttribute(
										org.zkoss.web.Attributes.PREFERRED_LOCALE,
										new Locale("fr"));
								session.setAttribute("Prose_Locale", "fr");
							}
							Executions.sendRedirect("/templates/default/main.zul#accueil");
							// Executions.sendRedirect("/temp");
						} else {
							lblErreur.setVisible(true);
							lblErreur.setValue(Labels.getLabel("compte.nonactif"));
							lblErreur.setStyle("color:red");
							errorMsg = Labels.getLabel("compte.nonactif");
							errorComponent = txtPassword;
							txtPassword.setValue("");
							txtLogin.setValue("");

							// Clients.evalJavaScript("logout();");
							throw new WrongValueException(txtPassword, errorMsg);
						}
					} else {

						final Session session = getHttpSession();
						session.setAttribute("user", user.getLogin());
						session.setAttribute("utilisateur", user);
						session.setAttribute("autorite", user.getAutorite());
						session.setAttribute("profil", user.getSysProfil().getPfCode());
						infoscompte = BeanLocator.defaultLookup(
								UtilisateurSession.class).InfosCompte(user.getLogin());
						session.setAttribute("infoscompte", infoscompte);
						// if (session
						// .getAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE) ==
						// null) {
						// session.setAttribute(
						// org.zkoss.web.Attributes.PREFERRED_LOCALE,
						// new Locale("fr"));
						// session.setAttribute("Prose_Locale", "fr");
						// }
						
					}

				} else {
					/*
					 * try { Messagebox.show("Login ou mot de passe incorrect",
					 * "Erreur", Messagebox.OK, Messagebox.ERROR); } catch (final
					 * InterruptedException e) { e.printStackTrace(); }
					 */
//					lblErreur.setVisible(true);
//					lblErreur.setValue(Labels
//							.getLabel("kermel.common.authentification.erreur"));
//					lblErreur.setStyle("color:red");
//					errorMsg = Labels
//							.getLabel("kermel.common.authentification.mdp.erreur");
//					errorComponent = txtPassword;
//					txtPassword.setValue("");
//					txtLogin.setValue("");
//
//					// Clients.evalJavaScript("logout();");
//					throw new WrongValueException(txtPassword, errorMsg);
				}

//		A aSec = new A();
//		Label label = new Label(
//				"Cliquer ici pour entammer la double authentification");
//		aSec.addEventListener(Events.ON_CLICK, new EventListener() {
//
//			@Override
//			public void onEvent(Event event) throws Exception {
//
//				Executions.sendRedirect(UIConstants.SWEB_URL
//						+ "/templates/default/authsect.zul");
//
//			}
//		});
//
//		Image img = new Image("/images/next.png");
//		aSec.appendChild(img);
//		aSec.appendChild(label);
//
//		divGo.appendChild(aSec);

	}

}
