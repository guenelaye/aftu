package sn.ssi.kermel.web.authentification.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.security.ProfilsSession;
import sn.ssi.kermel.be.security.UtilisateurSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class LoginDeuxController extends AbstractWindow implements AfterCompose {

	private static final long serialVersionUID = 1L;
	Textbox txtLogin, txtPassword;
	Button authOk;
	private Label lblErreur;
	private String errorMsg;
	private Component errorComponent;
	private static final String READY_MSG_STYLE = "";
	private static final String ERROR_MSG_STYLE = "color:red";
	private Utilisateur infoscompte;
	private Label lbllibelle;
	List<Utilisateur> users = new ArrayList<Utilisateur>();
	Utilisateur user;
	private int etape;
	private Image imageun,imagedeux;
	private Label lblAutorite,lblNomPresident,lblPrenomPresident,lblPrenomSecrtaire,lblNomSecrtaire;
	private SygAutoriteContractante autorite;
	 
	public void afterCompose() {
		Components.wireVariables(this, this);
		Components.addForwards(this, this);
		
		
	}
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		etape=1;
		lbllibelle.setValue(Labels.getLabel("kermel.username.libelle.president"));
		imageun.setSrc("images/1gris.png");
		imagedeux.setSrc("images/2vert.png");
	}
	public void InfosPresident() {
		users = BeanLocator.defaultLookup(ProfilsSession.class).findUtilisateur(txtLogin.getValue(), txtPassword.getValue(),"POPLIS",null);
		if (users.size() > 0) {
			user=users.get(0);
			autorite=user.getAutorite();
			lblAutorite.setValue(autorite.getDenomination());
			lblNomPresident.setValue(Labels.getLabel("kermel.common.form.nom")+": "+user.getNom());
			lblPrenomPresident.setValue(Labels.getLabel("kermel.common.form.prenom")+": "+user.getPrenom());
			etape=2;
			lbllibelle.setValue(Labels.getLabel("kermel.username.libelle.secretaire"));
			lblErreur.setVisible(false);
			txtPassword.setValue("");
			txtLogin.setValue("");
			imageun.setSrc("images/1vert.png");
			users = BeanLocator.defaultLookup(ProfilsSession.class).findUtilisateur(null,null,"SOPLIS",autorite);
			if (users.size() > 0) {
				user=users.get(0);
				lblPrenomSecrtaire.setValue(Labels.getLabel("kermel.common.form.nom")+": "+user.getNom());
				lblNomSecrtaire.setValue(Labels.getLabel("kermel.common.form.prenom")+": "+user.getPrenom());
			}
		}
		else {
			
			lblErreur.setVisible(true);
			lblErreur.setValue(Labels.getLabel("kermel.common.authentification.erreur"));
			lblErreur.setStyle("color:red");
			errorMsg = Labels.getLabel("kermel.common.authentification.mdp.erreur");
			errorComponent = txtPassword;
			txtPassword.setValue("");
			txtLogin.setValue("");

			throw new WrongValueException(txtPassword, errorMsg);
		}

	}
	public void InfosSecretaire() {
		users = BeanLocator.defaultLookup(ProfilsSession.class).findUtilisateur(txtLogin.getValue(), txtPassword.getValue(),"SOPLIS",autorite);

		if (users.size() > 0) {
			user = users.get(0);
			if((user.getType().equals(UIConstants.USERS_TYPES_AC))||(user.getType().equals(UIConstants.USERS_TYPES_AGENTAC)))
			{
				if(user.getActif()==true)
				{
					final Session session = getHttpSession();
					session.setAttribute("user", user.getLogin());
					session.setAttribute("utilisateur", user);
					session.setAttribute("profil", user.getSysProfil().getPfCode());
					infoscompte=BeanLocator.defaultLookup(UtilisateurSession.class).InfosCompte(user.getLogin());
					session.setAttribute("infoscompte", infoscompte);
					if(session.getAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE)== null){
						session.setAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE, new Locale("fr"));
						session.setAttribute("Prose_Locale", "fr");
					}
					Executions.sendRedirect("/templates/default/main.zul#dossiers_a_ouvrir");
				}
				else
				{
					lblErreur.setVisible(true);
					lblErreur.setValue(Labels.getLabel("compte.nonactif"));
					lblErreur.setStyle("color:red");
					errorMsg = Labels.getLabel("compte.nonactif");
					errorComponent = txtPassword;
					txtPassword.setValue("");
					txtLogin.setValue("");

					throw new WrongValueException(txtPassword, errorMsg);
				}
			}
			else
			{
				
				final Session session = getHttpSession();
				session.setAttribute("user", user.getLogin());
				session.setAttribute("utilisateur", user);
				session.setAttribute("autorite", user.getAutorite());
				session.setAttribute("profil", user.getSysProfil().getPfCode());
				infoscompte=BeanLocator.defaultLookup(UtilisateurSession.class).InfosCompte(user.getLogin());
				session.setAttribute("infoscompte", infoscompte);
				if(session.getAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE)== null){
					session.setAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE, new Locale("fr"));
					session.setAttribute("Prose_Locale", "fr");
				}
				Executions.sendRedirect("/templates/default/main.zul#accueil");
			}

			
		} else {
			/*
			 * try { Messagebox.show("Login ou mot de passe incorrect",
			 * "Erreur", Messagebox.OK, Messagebox.ERROR); } catch (final
			 * InterruptedException e) { e.printStackTrace(); }
			 */
			lblErreur.setVisible(true);
			lblErreur.setValue(Labels.getLabel("kermel.common.authentification.erreur"));
			lblErreur.setStyle("color:red");
			errorMsg = Labels.getLabel("kermel.common.authentification.mdp.erreur");
			errorComponent = txtPassword;
			txtPassword.setValue("");
			txtLogin.setValue("");

			throw new WrongValueException(txtPassword, errorMsg);
		}
	}
	public void onOK() {
      if(etape==1)
    	  InfosPresident();
      else
    	  InfosSecretaire();
	}

	public void onClick$tbBtnlogin() {
		onOK();
	}
}