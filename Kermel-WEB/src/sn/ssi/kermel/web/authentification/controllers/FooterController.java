package sn.ssi.kermel.web.authentification.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;

import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class FooterController extends AbstractWindow implements AfterCompose,
		EventListener {

	private Label sessionUser;
	private Include footer;

	@Override
	public void afterCompose() {
		// TODO Auto-generated method stub
		Components.wireVariables(this, this);
		Components.addForwards(this, this);

	}

	public void onCreate(CreateEvent e) {
		Session session = getHttpSession();
		String login = (String) session.getAttribute("PrenomNom");
		String profil = (String) session.getAttribute("profil");
		String userConnecte = "Session:" + login + " Profil:" + profil;
		Include inc = (Include) this.getFellowIfAny("footer");
		inc.setSrc("/templates/default/footer.zul?code=" + userConnecte);
		// Utilisateur user =
		// BeanLocator.defaultLookup(UtilisateurSession.class).findUtilisateur(login,
		// password)
	}

	@Override
	public void onEvent(Event event) throws Exception {
		// TODO Auto-generated method stub

	}

}
