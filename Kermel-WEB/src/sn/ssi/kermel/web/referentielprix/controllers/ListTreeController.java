package sn.ssi.kermel.web.referentielprix.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.West;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygCategories;
import sn.ssi.kermel.be.entity.SygFamilles;
import sn.ssi.kermel.be.entity.SygProduits;
import sn.ssi.kermel.be.referentielprix.ejb.CategoriesSession;
import sn.ssi.kermel.be.referentielprix.ejb.FamillesSession;
import sn.ssi.kermel.be.referentielprix.ejb.ProduitsSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class ListTreeController extends AbstractWindow implements
		AfterCompose, IdSpace {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private Include content;
	
	private Tree tree;
	private Treechildren child = new Treechildren();
	private Session session = getHttpSession();
	private SygCategories categorie;
	private SygFamilles famille;
	private SygProduits produit;
	
	private String openWest;
	private West wp;
	private String list;

	List<SygCategories> listCategories = new ArrayList<SygCategories>();
	//List<SygFamilles> listFamilles= new ArrayList<SygFamilles>();
	//List<SygProduits> listProduits= new ArrayList<SygProduits>();
	
	

	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		list=(String) session.getAttribute("list");

		listCategories = BeanLocator.defaultLookup(CategoriesSession.class).find(0, -1, null,null);
	
		
		openWest = (String) Executions.getCurrent().getAttribute("openWest");
		wp.setOpen(true);

		
		initTree();


	}

	private void initTree() {
		child.setParent(tree);
	   treeOrganigrammeBuilder();
	  
	   
	   if((list!=null)){
		   
			  if(list.equals("Famille")) {
				  content.setSrc("/referentielprix/listfamilles.zul");
				  
			  }else if(list.equals("Produit")){
				  content.setSrc("/referentielprix/listproduits.zul");
			  }
				  
		 }else{
			 
			 content.setSrc("/referentielprix/listcategories.zul");
		 }
	   
	    	
			//content.setHeight("100%");
	  
		
	}

	public void onSelect$tree() {
		tree.isInvalidated();
	    Object selobj = tree.getSelectedItem().getValue();

		
		content.invalidate();
		
		if (selobj instanceof SygCategories) {

			//List<String> status = new ArrayList<String>();
			//categorie = (SygCategories) selobj;
			
			
			session.setAttribute("selObj", selobj);
			 System.out.println(tree.getSelectedItem().getId());
			 //content.setSrc("/referentielprix/listcategories.zul");
			 content.setSrc("/referentielprix/listfamilles.zul");
		      
		} else if (selobj instanceof SygFamilles) {

			//List<String> status = new ArrayList<String>();
			//famille = (SygFamilles) selobj;
			
			session.setAttribute("selObj", selobj);
			
			 System.out.println(tree.getSelectedItem().getId());
			// content.setSrc("/referentielprix/listfamilles.zul");
			 content.setSrc("/referentielprix/listproduits.zul");
			 
		} 
//		else if (selobj instanceof SygProduits) {
//
//			//List<String> status = new ArrayList<String>();
//			//produit = (SygProduits) selobj;
//			
//			
//			
//			 System.out.println(tree.getSelectedItem().getId());
//			 content.setSrc("/referentielprix/listproduits.zul");
//		      
//		}


	}

	
	public void treeOrganigrammeBuilder() {

		 String key = null;
		
			System.out.println("==============================ok"+listCategories.size());
			
			 for (SygCategories ohCategorie : listCategories) {
		
		 Treeitem categorieItem = new Treeitem();
		
		 categorieItem.setLabel(ohCategorie.getDesignation());
		 categorieItem.setTooltiptext(ohCategorie.getDesignation());
		 categorieItem.setImage("/images/typprod.png");
	   	 //procedureItem.setImage("g");
		 categorieItem.setValue(ohCategorie);
		 categorieItem.setOpen(false);
		
		 categorieItem.setParent(child);
		
			//Niveau 2 :Famille	 
		 List<SygFamilles> listFamilles = BeanLocator.defaultLookup(
				 FamillesSession.class).findRech(0, -1,null,null,ohCategorie.getId());
			
		 if(listFamilles.size() > 0){
		 Treechildren categorieTreechildren = new Treechildren();
		 for (SygFamilles ohfamille : listFamilles) {
		 Treeitem familleItem = new Treeitem();
		
		 familleItem.setLabel(ohfamille.getDesignation());
		 familleItem.setTooltiptext(ohfamille.getDesignation());
		 familleItem.setImage("/images/cart.png");
		
		// etapeItem.setImage("g");
		
		 familleItem.setValue(ohfamille);
		 familleItem.setOpen(false);
		
		 familleItem.setParent(categorieTreechildren);
		
			//Niveau 3:Produits
		 List<SygProduits> listProduits = BeanLocator.defaultLookup(
				 ProduitsSession.class).findRech(0, -1,null,null,ohfamille.getId());
			System.out.println("==============================ok"+listProduits.size());
			
		 if(listProduits.size() > 0){
		 Treechildren familleTreechildren = new Treechildren();
		 for (SygProduits ohproduit : listProduits) {
		 Treeitem produitItem = new Treeitem();
		
		 produitItem.setLabel(ohproduit.getDesignation());
		 produitItem.setTooltiptext(ohproduit.getDesignation());
		 produitItem.setImage("/images/produit.png");
		
		// tacheItem.setImage("g");
		 produitItem.setValue(ohproduit);
		 produitItem.setOpen(false);
		
		 produitItem.setParent(familleTreechildren);
		
		 }
		 familleTreechildren.setParent(familleItem);
		 }//Fin niveau 3 produit
		 }
		 categorieTreechildren.setParent(categorieItem);
		 }//Fin niveau 2 famille
		 }
	}
	///


	public void removeTreeChildren() {
		while (child.getItemCount() > 0) {
			child.removeChild(child.getFirstChild());
		}
	}


	public void onOpen$tree() {

		System.out.println(tree.getSelectedItem().getId());
	}


	
}