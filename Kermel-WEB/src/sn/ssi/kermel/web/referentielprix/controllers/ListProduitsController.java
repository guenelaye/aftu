package sn.ssi.kermel.web.referentielprix.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygFamilles;
import sn.ssi.kermel.be.entity.SygProduits;
import sn.ssi.kermel.be.referentielprix.ejb.ProduitsSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;


@SuppressWarnings("serial")
public class ListProduitsController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null;
    private Listheader lshcode,lshdesignation;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_PRODUITS, MOD_PRODUITS, SUPP_PRODUITS,WSAISI_PRIX;
    private String login;
    //private SygProduits produit;
    private Long familleid;
    private SygFamilles famille;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.PRODUITS);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_PRODUITS != null) { ADD_PRODUITS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_PRODUITS != null) { MOD_PRODUITS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUPP_PRODUITS != null) { SUPP_PRODUITS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		if (WSAISI_PRIX != null) { WSAISI_PRIX.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_SAISIPRIX); }
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_SAISIPRIX, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		lshcode.setSortAscending(new FieldComparator("code", false));
		lshcode.setSortDescending(new FieldComparator("code", true));
		
		lshdesignation.setSortAscending(new FieldComparator("designation", false));
		lshdesignation.setSortDescending(new FieldComparator("designation", true));
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	login = ((String) getHttpSession().getAttribute("user"));
    	
    	
    	
    	Object selobj = session.getAttribute("selObj");
    	famille = (SygFamilles) selobj;
    	if (famille!=null){
    		familleid=famille.getId();
    	
    	}
    	else{
    		familleid=null;
    		
    		}
		
    	session.setAttribute("familleid", familleid);
    	
	}

	
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygProduits> produits = BeanLocator.defaultLookup(ProduitsSession.class).findRech(activePage,byPage,null,libelle,familleid);
			 SimpleListModel listModel = new SimpleListModel(produits);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup(ProduitsSession.class).countRech(null,libelle,familleid));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/referentielprix/formproduits.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.referentielprix.produit.form.titre.new"));
			display.put(DSP_HEIGHT,"400px");
			display.put(DSP_WIDTH, "60%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(CategoriesFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			final String uri = "/referentielprix/formproduits.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"400px");
			display.put(DSP_WIDTH, "60%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.referentielprix.produit.form.titre.edit"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(CategoriesFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(CategoriesFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
				
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(ProduitsSession.class).delete(codes);
				BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_PRODUITS", Labels.getLabel("kermel.referentielprix.produit.supprimer")+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
				 session.setAttribute("list","Produit");    
					loadApplicationState("referentielprix");
			}
		// ///////Bouton saisi prix

			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_SAISIPRIX)) {

				if (lstListe.getSelectedItem() == null) {
					throw new WrongValueException(lstListe, "Veillez selectionnez un element svp!");
				} else {
					if (lstListe.getSelectedCount() > 1) {
						throw new WrongValueException(lstListe, "Veillez selectionnez un seul element svp!");
					} else {
						
						session.setAttribute("numproduit", lstListe.getSelectedItem().getValue());
						//loadApplicationState("saisi_prix");
						final String uri = "/referentielprix/saisiprix.zul";

						final HashMap<String, String> display = new HashMap<String, String>();
						display.put(DSP_TITLE, Labels.getLabel("kermel.referentielprix.saisiprix.form.titre"));
						display.put(DSP_HEIGHT,"400px");
						display.put(DSP_WIDTH, "60%");

						final HashMap<String, Object> data = new HashMap<String, Object>();
						data.put(CategoriesFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
			    		showPopupWindow(uri, data, display);
						
						

					}
				}
			}

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygProduits produit = (SygProduits) data;
		item.setValue(produit.getId());

			 
		 Listcell cellCode = new Listcell(produit.getCode());
		 cellCode.setParent(item);
		 
		 Listcell cellLibelle = new Listcell(produit.getDesignation());
		 cellLibelle.setParent(item);
		 
		 Listcell cellCommentaire = new Listcell(produit.getCommentaire());
		 cellCommentaire.setParent(item);
		 
		

	}
	public void onClick$bchercher()
	{
		
		if((txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentielprix.categorie.designation")))||(txtLibelle.getValue().equals("")))
		 {
			libelle=null;
			page=null;
		 }
		else
		{
			libelle=txtLibelle.getValue();
			page="0";
		}
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	
	public void onFocus$txtLibelle()
	{
		if(txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentielprix.categorie.designation")))
			txtLibelle.setValue("");
		
	}
	
	public void onBlur$txtLibelle()
	{
		if(txtLibelle.getValue().equals(""))
			txtLibelle.setValue(Labels.getLabel("kermel.referentielprix.categorie.designation"));
	}

	public void onOK$txtLibelle()
	{
		onClick$bchercher();
	}
	
	
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
}