package sn.ssi.kermel.web.referentielprix.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Column;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Paging;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygFamilles;
import sn.ssi.kermel.be.entity.SygPrix;
import sn.ssi.kermel.be.entity.SygProduits;
import sn.ssi.kermel.be.referentielprix.ejb.PrixSession;
import sn.ssi.kermel.be.referentielprix.ejb.ProduitsSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


@SuppressWarnings("serial")
public class ListPrixController extends AbstractWindow implements AfterCompose, EventListener{


	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode;
	public static final String WINDOW_PARAM_MODE = "MODE";
	
	private Label lblCode, lblDesignation;
	
	Column idcolumn;

	Grid Grid;
	private Paging pg;
	
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;

             private SygFamilles famille;
             private Long familleid;
             Session session = getHttpSession();
             private SygProduits produit;
             private String annee;

	@Override
	public void afterCompose() {
		// TODO Auto-generated method stub
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);   
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this); 
             pg.setPageSize(byPage);
		
		pg.addForward("onPaging", this,	ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
		
		Object selobj = session.getAttribute("selObj");
		famille = (SygFamilles) selobj;
    	if (famille!=null){
    		familleid=famille.getId();
    		
    		produit = BeanLocator.defaultLookup(ProduitsSession.class).findById(famille.getId());
    	
    	}
    	else{
    		familleid=null;
    		
    		}
		
    	//session.setAttribute("familleid", familleid);

	
		
		lblCode.setValue(famille.getCode());
		lblDesignation.setValue(famille.getDesignation());
		annee=(String) session.getAttribute("annee");


	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {

			Grid.setRowRenderer(new ListPrixRowRender());

		 List<SygPrix> vals = findLignesFiche(famille);
              
				Grid.setModel(new ListModelList(vals));


		}

	}

	
	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {

		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(PARAM_WINDOW_MODE);
		
	}

	
	public void onClick$menufermer() {
		loadApplicationState("list_prix");
	}
	

	public void onClick$menuValider() {

	
		List<SygProduits> produits = BeanLocator.defaultLookup(ProduitsSession.class).findByFamilly(0,-1,familleid);

		for (SygProduits onProduit : produits) {


			Map<String, Integer> values = BeanLocator.defaultLookup(
					PrixSession.class).findf1(null);

			SygPrix valeurProd = setValeur(values);
			valeurProd.setProduitservice(onProduit);
	

			try {
				valeurProd = BeanLocator.defaultLookup(
						PrixSession.class).savePrix(valeurProd);
			} catch (Exception e) {
			}

			
		}

	}


	
	
	
	private List<SygPrix> findLignesFiche(SygFamilles onFamille) {

		
		List<SygPrix> vals = new ArrayList<SygPrix>();
	    List<SygProduits> produits= BeanLocator
		.defaultLookup(ProduitsSession.class).findRech(0,-1,null,null,familleid);
	    
			for (SygProduits onProduit : produits) {

				SygPrix valeurProd = BeanLocator.defaultLookup(PrixSession.class).findByFamillAndProduit(onFamille, onProduit.getId(),Integer.parseInt(annee));			
		
		   if(valeurProd==null){
				
		
				Map<String, Integer> values = BeanLocator.defaultLookup(PrixSession.class).findf1(null);

				valeurProd = setValeur(values);
				valeurProd.setProduitservice(onProduit);
				valeurProd.setAnnee(Integer.parseInt(annee));
				valeurProd.setStatut(Labels.getLabel("kermel.referentielprix.valpubprix.statut.saisie"));
				

				try {
					valeurProd = BeanLocator.defaultLookup(PrixSession.class).savePrix(valeurProd);
				} catch (Exception e) {
				}

				}
		   
			   vals.add(valeurProd);  
		   
				
			}

			return vals;
		}
		
	
	
		private SygPrix setValeur(Map<String, Integer> values) {
			SygPrix valeur = new SygPrix();
			valeur.setPrix(BigDecimal.valueOf(values.get("un").doubleValue()));
			valeur.setPrixMax(BigDecimal.valueOf(values.get("deux").doubleValue()));
			valeur.setPrixMin(BigDecimal.valueOf(values.get("trois").doubleValue()));
			valeur.setVariationVA(values.get("quatre").longValue());
			valeur.setVariationVR(values.get("cinq").doubleValue());
			if(values.get("six")!=null)
			valeur.setObservation(values.get("six").toString());


			return valeur;
		}
		
	


}