package sn.ssi.kermel.web.referentielprix.controllers;

import java.math.BigDecimal;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygPrix;
import sn.ssi.kermel.be.referentielprix.ejb.PrixSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class SaisiPrixRowRender extends AbstractWindow implements
		AfterCompose, RowRenderer, RowRendererExt,EventListener {

	Session session = getHttpSession();
	SygPrix valeurRech ;
	private boolean hasOpener = true;
	//private Long familleid;
	//SygFamilles familles;
	private Long variation;
	private Double variationVR;
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
	}

	@Override
	public Row newRow(Grid grid) {
		// Create EditableRow instead of Row(default)
		Row row = new EditableRow();
		row.applyProperties();
		return row;
	}

	@Override
	public Component newCell(Row row) {
		return null;// Default Cell
	}

	@Override
	public int getControls() {
		return RowRendererExt.DETACH_ON_RENDER; // Default Value
	}

	@Override
	public void render(Row row, Object data, int index) throws Exception {
		final SygPrix valeurProduit = (SygPrix) data;
		final EditableRow editRow = (EditableRow) row;
		final EditableDiv un, deux, trois, quatre, cinq, six, sept, huit;
		final Button editBtn;
		final Button editBtn1;
		final Button submitBtn = new Button(null, "/images/ok.png");
		final Button cancelBtn = new Button(null,
				"/images/cancel.png");

		final Div ctrlDiv;
		//familleid=(Long) session.getAttribute("familleid");
		//familles= BeanLocator.defaultLookup(FamillesSession.class).findById(familleid);
//		if (valeurProduit.getId()!= null) {
//			
//			
//		} else {

		     final EditableDiv code = new EditableDiv(valeurProduit.getProduitservice().getCode(), false);
		     code.txb.setReadonly(true);
		     code.setParent(editRow);
			
			final EditableDiv libelle = new EditableDiv(valeurProduit.getProduitservice().getDesignation(), false);
			libelle.txb.setReadonly(true);
			libelle.setParent(editRow);

			un = new EditableDiv(ToolKermel.format2Decimal(valeurProduit.getPrix()), false);
			un.setAlign("center");
			un.setParent(editRow);
			
			deux = new EditableDiv(ToolKermel.format2Decimal(valeurProduit.getPrixMax()), false);
			deux.setAlign("center");
			deux.setParent(editRow);

			trois = new EditableDiv(ToolKermel.format2Decimal(valeurProduit.getPrixMin()), false);
			trois.setAlign("center");
			trois.setParent(editRow);

			quatre = new EditableDiv(valeurProduit.getVariationVA().toString(), false);
			quatre.setAlign("center");
			quatre.txb.setReadonly(true);
			quatre.setParent(editRow);
			
			cinq = new EditableDiv(valeurProduit.getVariationVR().toString(), false);
			cinq.setAlign("center");
			cinq.txb.setReadonly(true);
			cinq.setParent(editRow);
			
			if(valeurProduit.getObservation()!=null){
			    six = new EditableDiv(valeurProduit.getObservation(), false);
			    six.setAlign("center");
			    six.setParent(editRow);
			    }
			else{
				six = new EditableDiv("", false);
				six.setAlign("center");
				six.setParent(editRow);
				}
			
//			sept = new EditableDiv(ToolKermel.format2Decimal(valeurProduit.getQuantite()), false);
//			sept.setAlign("center");
//			sept.setParent(editRow);
//			huit = new EditableDiv(ToolKermel.format2Decimal(valeurProduit.getValeur()), false);
//			huit.setAlign("center");
//			huit.setParent(editRow);
			
			ctrlDiv = new Div();
			ctrlDiv.setParent(editRow);
			editBtn = new Button(null, "/images/pencil-small.png");
			editBtn.setMold("os");
			editBtn.setHeight("20px");
			editBtn.setWidth("30px");
			editBtn.setParent(ctrlDiv);

			
			editRow.addEventListener(Events.ON_OK, new EventListener() {
				public void onEvent(Event event) throws Exception {
					
					
					submitBtn.detach();
					cancelBtn.detach();
					editBtn.setParent(ctrlDiv);
					
					
					if (!ToolKermel.estReel(String.valueOf(un.txb.getValue().replace(" ","")))) {
						editRow.toggleEditable(true);
						throw new WrongValueException(un,
								"Attention!!La valeur  doit �tre num�rique !!");

					}
					if (!ToolKermel.estReel(deux.txb.getValue().replace(" ",""))) {
						editRow.toggleEditable(true);
						throw new WrongValueException(deux,
								"Attention!!La valeur doit �tre num�rique!!");

					}

					if (!ToolKermel.estReel(trois.txb.getValue().replace(" ",""))) {
						editRow.toggleEditable(true);
						throw new WrongValueException(trois,
								"Attention!!La valeur  doit �tre num�rique!!");

					}
					


					valeurProduit.setPrix(BigDecimal
							.valueOf(Double.parseDouble(un.txb.getValue().replace(" ","")) ));
					valeurProduit.setPrixMax(BigDecimal
							.valueOf(Double.parseDouble(deux.txb.getValue().replace(" ",""))));
					valeurProduit.setPrixMin(BigDecimal
							.valueOf(Double.parseDouble(trois.txb.getValue().replace(" ",""))));
					
					valeurProduit.setVariationVA(Long
							.valueOf(quatre.txb.getValue().replace(" ","")));
					valeurProduit.setVariationVR(Double
							.valueOf(cinq.txb.getValue().replace(" ","")));
					valeurProduit.setObservation(
							(six.txb.getValue().replace(" ","")));
//					valeurProduit.setQuantite(Double
//							.valueOf(sept.txb.getValue().replace(" ","")));
//					
//					valeurProduit.setValeur(Double
//							.valueOf(huit.txb.getValue().replace(" ","")));
					BeanLocator.defaultLookup(PrixSession.class).update(valeurProduit);
				
					//mise � jour
					//un.txb.setValue("3");
					//System.out.println("######################################################"+un.txb.getValue());
					//un.setParent(editRow);
					un.txb.setValue(ToolKermel.format2Decimal(valeurProduit.getPrix()));
					deux.txb.setValue(ToolKermel.format2Decimal(valeurProduit.getPrixMax()));
					trois.txb.setValue(ToolKermel.format2Decimal(valeurProduit.getPrixMin()));
					quatre.txb.setValue(valeurProduit.getVariationVA().toString());
					cinq.txb.setValue(valeurProduit.getVariationVR().toString());
					six.txb.setValue(valeurProduit.getObservation());
//					huit.txb.setValue(ToolKermel.format2Decimal(valeurProduit.getValeur()));
					
				
					// fin mise � jour
			    
					editRow.toggleEditable(false);	
					
				}
			});
			
			
			submitBtn.addEventListener(Events.ON_CLICK,
					new EventListener() {
						public void onEvent(Event event)
								throws Exception {

				valeurRech= BeanLocator.defaultLookup(PrixSession.class).findByFamillAndProduit(valeurProduit.getProduitservice().getFamilles(), valeurProduit.getProduitservice().getId(),(valeurProduit.getAnnee()-1));
							
				if(valeurRech!=null){
					variation=(valeurProduit.getPrix().longValue()-valeurRech.getPrix().longValue());
					
					}else{
						variation=valeurProduit.getPrix().longValue();
						
					}
				
				  if(valeurProduit.getPrix().intValue()!=0)
				       variationVR=((1.0*variation/valeurProduit.getPrix().longValue())*100);
				  else
					  variationVR=0.0;
				  
				  if(variationVR<0)
					  variationVR=(-1*variationVR);
							editRow.toggleEditable(true);
							

							if (!ToolKermel.estReel(un.txb.getValue().replace(" ",""))) {
								editRow.toggleEditable(true);
								throw new WrongValueException(un,
										"Attention!!La valeur  doit �tre num�rique!!");

							}
							if (!ToolKermel.estReel(deux.txb.getValue().replace(" ",""))) {
								editRow.toggleEditable(true);
								throw new WrongValueException(deux,
										"Attention!!La valeur doit �tre num�rique!!");

							}

							if (!ToolKermel.estReel(trois.txb.getValue().replace(" ",""))) {
								editRow.toggleEditable(true);
								throw new WrongValueException(trois,
										"Attention!!La valeur  doit �tre num�rique!!");

							}
							

							valeurProduit.setPrix(BigDecimal
									.valueOf(Double.parseDouble(un.txb.getValue().replace(" ","")) ));
							valeurProduit.setPrixMax(BigDecimal
									.valueOf(Double.parseDouble(deux.txb.getValue().replace(" ",""))));
							valeurProduit.setPrixMin(BigDecimal
									.valueOf(Double.parseDouble(trois.txb.getValue().replace(" ",""))));
							
							valeurProduit.setVariationVA(Long
									.valueOf(variation));
							valeurProduit.setVariationVR(variationVR);
							valeurProduit.setObservation(
									(six.txb.getValue().replace(" ","")));
//							valeurProduit.setQuantite(Double
//									.valueOf(sept.txb.getValue().replace(" ","")));
//							
//							valeurProduit.setValeur(Double
//									.valueOf(huit.txb.getValue().replace(" ","")));
							BeanLocator.defaultLookup(PrixSession.class).update(valeurProduit);
								
							// TODO Auto-generated method stub
							
							
							//editRow.toggleEditable(false);
							
							//mise � jour
							
							un.txb.setValue(ToolKermel.format2Decimal(valeurProduit.getPrix()));
							deux.txb.setValue(ToolKermel.format2Decimal(valeurProduit.getPrixMax()));
							trois.txb.setValue(ToolKermel.format2Decimal(valeurProduit.getPrixMin()));
							quatre.txb.setValue(variation.toString());
							cinq.txb.setValue(ToolKermel.format2Decimal(variationVR));
							six.txb.setValue(valeurProduit.getObservation());
//							sept.txb.setValue(ToolKermel.format2Decimal(valeurProduit.getQuantite()));
//							huit.txb.setValue(ToolKermel.format2Decimal(valeurProduit.getValeur()));
							// fin mise � jour
							
							cancelBtn.detach();
							submitBtn.detach();
							
							editBtn.setParent(ctrlDiv);
						}
					});

			
			cancelBtn.addEventListener(Events.ON_CLICK,
					new EventListener() {
						public void onEvent(Event event)
								throws Exception {
							editRow.toggleEditable(false);
							submitBtn.detach();
							cancelBtn.detach();
							editBtn.setParent(ctrlDiv);
						}
					});

			editBtn.addEventListener(Events.ON_CLICK, new EventListener() {
				public void onEvent(Event event) throws Exception {
					
					
					submitBtn.setMold("os");
					submitBtn.setHeight("20px");
					submitBtn.setWidth("30px");
					
					
					
					
					cancelBtn.setMold("os");
					cancelBtn.setHeight("20px");
					cancelBtn.setWidth("30px");
					
					
					
					
					submitBtn.setParent(ctrlDiv);
					cancelBtn.setParent(ctrlDiv);
					editRow.toggleEditable(true);
					editBtn.detach();
				}
			});
			
			
			
			
			

		//}

	}

	@Override
	public void onEvent(Event arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
