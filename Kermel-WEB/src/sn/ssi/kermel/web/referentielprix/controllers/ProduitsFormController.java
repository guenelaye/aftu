package sn.ssi.kermel.web.referentielprix.controllers;


import java.util.Date;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygFamilles;
import sn.ssi.kermel.be.entity.SygProduits;
import sn.ssi.kermel.be.referentielprix.ejb.FamillesSession;
import sn.ssi.kermel.be.referentielprix.ejb.ProduitsSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class ProduitsFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtcode,txtdesignation,txtcommentaire;
	Long code;
	private	SygProduits produit=new SygProduits();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygFamilles familles;
	Long familleid;
	Session session = getHttpSession();
	
	@Override
	public void onEvent(Event event) throws Exception {


	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		familleid=(Long) session.getAttribute("familleid");
		//uniteorgId=(Long) session.getAttribute("uniteorg");
		//division = BeanLocator.defaultLookup(TypeUniteOrgarmpSession.class).findById(typeunitorgId);
		familles= BeanLocator.defaultLookup(FamillesSession.class).findById(familleid);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			produit = BeanLocator.defaultLookup(ProduitsSession.class).findById(code);
			txtcode.setValue(produit.getCode());
			txtdesignation.setValue(produit.getDesignation());
			txtcommentaire.setValue(produit.getCommentaire());
		
			
		}
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			produit.setCode(txtcode.getValue()); 
			produit.setDesignation(txtdesignation.getValue()); 
			produit.setCommentaire(txtcommentaire.getValue()); 
			produit.setFamilles(familles);
			
		  	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(ProduitsSession.class).save(produit);
				BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_PRODUITS", Labels.getLabel("ygmap.referentielprix.produit.ajouter")+" :" + new Date(), login);
				
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(ProduitsSession.class).update(produit);
				BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_PRODUITS", Labels.getLabel("kermel.referentielprix.produit.modifier")+" :" + new Date(), login);
				
			}
	
			//Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			//detach();
			//loadApplicationState("referentielprix");
		  	
		  	session.setAttribute("list","Produit");    
			loadApplicationState("referentielprix");
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			if(txtcode.getValue().equals(""))
		     {
               errorComponent = txtcode;
               errorMsg = Labels.getLabel("kermel.referentielprix.famille.code")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(txtdesignation.getValue().equals(""))
		     {
              errorComponent = txtdesignation;
              errorMsg = Labels.getLabel("kermel.referentielprix.famille.designation")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	

}
