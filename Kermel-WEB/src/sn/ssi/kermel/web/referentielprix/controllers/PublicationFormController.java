package sn.ssi.kermel.web.referentielprix.controllers;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygPrix;
import sn.ssi.kermel.be.referentielprix.ejb.PrixSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class PublicationFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String login,mystate;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtCommentaires;
	Long code;
	private	SygPrix prix=new SygPrix();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
    private Long idprix;
    private Label lblInfos;
    Session session = getHttpSession();
    private Datebox dtdate;
    private static final String CONFIRMPUBLIER= "CONFIRMPUBLIER";
    
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(Event event) throws Exception {

		  if(event.getName().equals(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			 String confirmer = (String) ((HashMap<String, Object>) event.getData()).get(CONFIRMPUBLIER);
			 if (confirmer != null && confirmer.equalsIgnoreCase("Publication_Confirmer")) 
			 {
				 prix.setCommentairePublication(txtCommentaires.getValue());
				 prix.setDatePublication(dtdate.getValue());
				 prix.setStatut(Labels.getLabel("kermel.referentielprix.valpubprix.statut.publier"));
				 BeanLocator.defaultLookup(PrixSession.class).update(prix);
				 BeanLocator.defaultLookup(JournalSession.class).logAction("WPUB_PRIX", Labels.getLabel("kermel.plansdepassation.infos.numplan")+" "+lblInfos.getValue()+ Labels.getLabel("kermel.referentielprix.valpubprix.statut.publier")+" :" + UtilVue.getInstance().formateLaDate(new Date()), login);
				 //loadApplicationState("plans_avalider");
				 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
				 detach();
			 }
		 }
	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mystate = (String) map.get(PARAM_WINDOW_MODE);
		idprix=(Long) map.get(PARAM_WINDOW_CODE);;
		prix=BeanLocator.defaultLookup(PrixSession.class).findById(idprix);
		lblInfos.setValue(prix.getProduitservice().getDesignation()+" "+Labels.getLabel("kermel.referentielprix.annee")+" "+prix.getAnnee());
		dtdate.setValue(new Date());
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
		  	
			 HashMap<String, String> display = new HashMap<String, String>(); // permet
			 display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.plansdepassation.publier"));
			 display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.plansdepassation.publication.titre"));
			 display.put(MessageBoxController.DSP_HEIGHT, "150px");
			 display.put(MessageBoxController.DSP_WIDTH, "100px");
	         HashMap<String, Object> map = new HashMap<String, Object>(); // permet
	         map.put(CONFIRMPUBLIER, "Publication_Confirmer");
			 showMessageBox(display, map);
			 
			
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			
			if(dtdate.getValue()==null)
		     {
             errorComponent = dtdate;
             errorMsg = Labels.getLabel("kermel.journal.date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((dtdate.getValue()).after(new Date() ))
			 {
				errorComponent = dtdate;
				errorMsg =Labels.getLabel("kermel.journal.date")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.referentiel.date.dujour")+": "+UtilVue.getInstance().formateLaDate(new Date());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
		
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
 
	
	
	
}
