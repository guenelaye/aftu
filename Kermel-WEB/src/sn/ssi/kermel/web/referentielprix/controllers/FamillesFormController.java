package sn.ssi.kermel.web.referentielprix.controllers;


import java.util.Date;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygCategories;
import sn.ssi.kermel.be.entity.SygFamilles;
import sn.ssi.kermel.be.referentielprix.ejb.CategoriesSession;
import sn.ssi.kermel.be.referentielprix.ejb.FamillesSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class FamillesFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtcode,txtdesignation,txtcommentaire;
	Long code;
	private	SygFamilles famille=new SygFamilles();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygCategories categories;
	Long categorieid;
	Session session = getHttpSession();
	
	@Override
	public void onEvent(Event event) throws Exception {


	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		categorieid=(Long) session.getAttribute("categorieid");
		//uniteorgId=(Long) session.getAttribute("uniteorg");
		//division = BeanLocator.defaultLookup(TypeUniteOrgarmpSession.class).findById(typeunitorgId);
		categories= BeanLocator.defaultLookup(CategoriesSession.class).findById(categorieid);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			famille = BeanLocator.defaultLookup(FamillesSession.class).findById(code);
			txtcode.setValue(famille.getCode());
			txtdesignation.setValue(famille.getDesignation());
			txtcommentaire.setValue(famille.getCommentaire());
		
			
		}
		
		 
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			famille.setCode(txtcode.getValue()); 
			famille.setDesignation(txtdesignation.getValue()); 
			famille.setCommentaire(txtcommentaire.getValue()); 
			famille.setCategories(categories);
			
		  	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(FamillesSession.class).save(famille);
				BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_FAMILLES", Labels.getLabel("kermel.referentielprix.famille.ajouter")+" :" + new Date(), login);
				
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(FamillesSession.class).update(famille);
				BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_FAMILLES", Labels.getLabel("kermel.referentielprix.famille.modifier")+" :" + new Date(), login);
				
			}
	
			//Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			//detach();

		  // Executions.getCurrent().setAttribute("selObj",categorieid);
		   session.setAttribute("list","Famille");    
			loadApplicationState("referentielprix");
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			if(txtcode.getValue().equals(""))
		     {
               errorComponent = txtcode;
               errorMsg = Labels.getLabel("kermel.referentielprix.famille.code")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(txtdesignation.getValue().equals(""))
		     {
              errorComponent = txtdesignation;
              errorMsg = Labels.getLabel("kermel.referentielprix.famille.designation")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	

}
