package sn.ssi.kermel.web.archives.controllers;

import java.util.List;

import org.zkoss.zul.AbstractTreeModel;

import sn.ssi.kermel.be.archivage.ejb.SygNoeudClassementSession;
import sn.ssi.kermel.be.archivage.ejb.SygTypeElementArbreSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygNoeudClassement;
import sn.ssi.kermel.be.entity.SygTypeElementArbre;

@SuppressWarnings("serial")

public class ArbreClassementModele extends AbstractTreeModel {
	
	private List<SygTypeElementArbre> lstTE;
	private List<SygNoeudClassement> lst_SygNC;
	private int cpt;
	private int cpt_TE;
	
    public ArbreClassementModele() {
        super("");
   }
    public boolean isLeaf(Object node) {
        return getLevel((String)node) >= 4; //at most 4 levels
    }
    public Object getChild(Object parent, int index) {
    	this.cpt_TE=BeanLocator.defaultLookup(SygTypeElementArbreSession.class).count();
    	for (int i=0; i<this.cpt_TE; i++){
    		lst_SygNC=BeanLocator.defaultLookup(SygNoeudClassementSession.class).findByLevel(this.lstTE.get(0).getId());	
    	}
    	return String.valueOf(lst_SygNC.get(index).getLibelleNoeud());
    }
    
    public int getChildCount(Object parent) {
    	this.lstTE=BeanLocator.defaultLookup(SygTypeElementArbreSession.class).findAll();
    
    	this.cpt = BeanLocator.defaultLookup(SygNoeudClassementSession.class).count(lstTE.get(0).getId());
    	
    	return isLeaf(parent) ? 0: this.cpt; //each node has 5 children
    }
    public int getIndexOfChild(Object parent, Object child) {
        String data = (String)child;
        int i = data.lastIndexOf('.');
        return Integer.parseInt(data.substring(i + 1));
    }
    private int getLevel(String data) {
        for (int i = -1, level = 0;; ++level)
            if ((i = data.indexOf('.', i + 1)) < 0)
                return level;
    }
};