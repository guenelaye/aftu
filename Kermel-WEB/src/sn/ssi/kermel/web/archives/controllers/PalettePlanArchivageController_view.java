package sn.ssi.kermel.web.archives.controllers;

import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;

import sn.ssi.kermel.be.archivage.ejb.SygNoeudClassementSession;
import sn.ssi.kermel.be.archivage.ejb.SygTypeElementArbreSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygNoeudClassement;
import sn.ssi.kermel.be.entity.SygTypeElementArbre;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

/**
 * 
 * @author Omar MAR
 * @since 01 Decembre 2012, 20:14
 */
public class PalettePlanArchivageController_view extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";

	private List<SygTypeElementArbre> lstTE;
	private List<SygNoeudClassement> lst_SygNC;
	private int cpt;
	private int cpt_TE;

	// SygTypeElementArbre typeElementArbre=new SygTypeElementArbre();

	Session session = getHttpSession();
	private Label lblModePlanClassement, lgtitre;
	public static final String CURRENT_MODULE = "CURRENT_MODULE";
	private Tree tree;
	private Treeitem treeitem;
	private Treechildren treechildren;
	private Include idinclude;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
	
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);

		this.cpt_TE = BeanLocator.defaultLookup(
				SygTypeElementArbreSession.class).count();
		this.lstTE = BeanLocator
				.defaultLookup(SygTypeElementArbreSession.class).findAll();
		int cpt_ = BeanLocator.defaultLookup(SygNoeudClassementSession.class)
				.count(this.lstTE.get(0).getId());
		lst_SygNC = BeanLocator.defaultLookup(SygNoeudClassementSession.class)
				.findByLevel(this.lstTE.get(0).getId());
		Treechildren tc = new Treechildren();

		for (int i = 0; i < cpt_; i++) {
			Treeitem ti = new Treeitem();
			ti.setLabel(String.valueOf(lst_SygNC.get(i).getLibelleNoeud()));
			ti.setValue(lst_SygNC.get(i).getId());
			// arborescence(lst_SygNC.get(i).getId()).setParent(ti);
			ti.setParent(tc);
			ti.setOpen(false);
			ti.addEventListener(ApplicationEvents.ON_ADD, this);
			ti.addEventListener("onRightClick", this);

		}
		tc.setParent(tree);

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	public void onCreate(CreateEvent createEvent) {

		List<SygTypeElementArbre> lst = BeanLocator.defaultLookup(
				SygTypeElementArbreSession.class).findAll();
		String value = "";
		for (SygTypeElementArbre typeElment : lst) {
			if (value.equalsIgnoreCase(""))
				value = typeElment.getLibelle();
			else
				value = value + " --> " + typeElment.getLibelle();
		}
		lblModePlanClassement.setValue(value);

		// idinclude.setSrc("/archivage/documentsView.zul");

		// tree.setSelectedItem(iteminfosgenerales);
		lgtitre.setValue(Labels.getLabel("kermel.archivage.infoPlanClassment"));

	}

	@Override
	public void onEvent(Event event) throws Exception {

			//Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			//loadApplicationState("archives_plan_configuration");
		

	}

	// public void onSelect$tree(){
	public Treechildren arborescence(Long idSelected) {
		Treechildren tc = new Treechildren();
		List<SygNoeudClassement> lst_SygNC_ = BeanLocator.defaultLookup(
				SygNoeudClassementSession.class).findByNode(idSelected);

		for (int i = 0; i < lst_SygNC_.size(); i++) {
			Treeitem ti = new Treeitem();
			ti.setLabel(lst_SygNC_.get(i).getLibelleNoeud());
			ti.setValue(lst_SygNC_.get(i).getId());
			ti.setParent(tc);

			ti.addEventListener(ApplicationEvents.ON_ADD, this);
		}
		return tc;

	}

	public void onSelect$tree() {
		Treechildren tc = null;
		Long tt = Long.valueOf(tree.getSelectedItem().getValue().toString());
		session.setAttribute("idNode", tt);
		if (tree.getSelectedItem().getTreechildren() == null) {
			tc = arborescence(tt);
			if (tc != null) {
				tc.setParent(tree.getSelectedItem());
			}
			tree.getSelectedItem().setOpen(true);

		}
		idinclude.setSrc("");
		//idinclude.setSrc("/archivage/listenoeudsfils.zul");

		SygNoeudClassement node_ = BeanLocator.defaultLookup(
				SygNoeudClassementSession.class).findById(tt);

		int level = node_.getSygTypeElement().getNiveau();

		//if (level == cpt_TE) {
			idinclude.setSrc("/archivage/documentsView.zul");
		//}

	}
}