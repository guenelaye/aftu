package sn.ssi.kermel.web.archives.controllers;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.archivage.ejb.SygNoeudClassementSession;
import sn.ssi.kermel.be.archivage.ejb.SygTypeElementArbreSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygNoeudClassement;
import sn.ssi.kermel.be.entity.SygTypeElementArbre;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


@SuppressWarnings("serial")
public class FormAddFirstNoeudController extends AbstractWindow implements
		EventListener, AfterCompose {

	
	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle,txtCode,txtCodeType, txtCodeNode, txtRechercherNode, txtRechercherType;
	private Paging pgType;
	private Paging pgNode;
	private Listbox lstType;
	private Listbox lstNode;
	private String libelleType=null;
	private String libelleNode=null;
	private Bandbox bdType;
	private Bandbox bdNode;
	Long code;
	private	SygNoeudClassement type=new SygNoeudClassement();
	private SygTypeElementArbre STE =null;
	private List<SygTypeElementArbre> types;
	private SygNoeudClassement SNC =null;
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg, page=null;
	private static final String ERROR_MSG_STYLE = "color:red";
	
	//SygNoeudClassement types = new SygNoeudClassement();
	
	@Override
	public void onEvent(final Event event) throws Exception {
    
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_NATURES)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgType.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgType.getActivePage() * byPageBandbox;
				pgType.setPageSize(byPageBandbox);
			}
			List<SygTypeElementArbre> typesElmnt = BeanLocator.defaultLookup(SygTypeElementArbreSession.class).find(activePage, byPageBandbox);
			lstType.setModel(new SimpleListModel(typesElmnt));
			pgType.setTotalSize(BeanLocator.defaultLookup(SygTypeElementArbreSession.class).count());
		}
		
		


	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_NATURES, this);
		pgType.setPageSize(byPage);
		pgType.addForward("onPaging", this, ApplicationEvents.ON_NATURES);
		lstType.setItemRenderer(new TypesRenderer());
		
		addEventListener(ApplicationEvents.ON_CATEGORIES, this);



		
	}

	
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
	    types = BeanLocator.defaultLookup(SygTypeElementArbreSession.class).findAll();
		
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = Long.parseLong(String.valueOf(map.get(PARAM_WINDOW_CODE)));
			type = BeanLocator.defaultLookup(SygNoeudClassementSession.class).findById(code);
			txtCode.setValue(type.getId().toString());
			txtLibelle.setValue(type.getLibelleNoeud());
			txtCodeType.setValue(String.valueOf(type.getSygTypeElement().getId()));
			bdType.setValue(type.getSygTypeElement().getLibelle());
			
			if(type.getParent()!=null)
			{
			txtCodeNode.setValue(String.valueOf(type.getParent().getId()));
			bdNode.setValue(type.getParent().getLibelleNoeud());
			//txtPosition.setValue(type.getPositionNoeud());
			}
		}

		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			
				
			  
				  txtCodeType.setValue(types.get(0).getId().toString());
				  bdType.setValue(types.get(0).getLibelle());
				  bdType.setDisabled(true);
				
	    	
		}
		
		Events.postEvent(ApplicationEvents.ON_NATURES, this, null);
		Events.postEvent(ApplicationEvents.ON_CATEGORIES, this, null);
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			type.setId(Long.parseLong(txtCode.getValue()));
			type.setLibelleNoeud(txtLibelle.getValue()); 
			
			SygTypeElementArbre stea = BeanLocator.defaultLookup(SygTypeElementArbreSession.class).findById(Long.parseLong(txtCodeType.getValue()));
			type.setSygTypeElement(stea);
			
			
			
			if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(SygNoeudClassementSession.class).save(type);
				BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_ARCHIVECONFIG", Labels.getLabel("kermel.referentiel.common.typemarche.ajouter")+" :" + new Date(), login);
				
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(SygNoeudClassementSession.class).update(type);
				BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_ARCHIVECONFIG", Labels.getLabel("kermel.referentiel.common.typemarche.modifier")+" :" + new Date(), login);
				
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			loadApplicationState("archives_plan_configuration");
			
			detach();
		}
	}

	
private boolean checkFieldConstraints() 
{
  try {
	  
	if(txtCode.getValue().equals(""))
	  {
        errorComponent = txtCode;
        errorMsg = Labels.getLabel("kermel.archivage.idTypeElementArbre")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		lbStatusBar.setStyle(ERROR_MSG_STYLE);
		lbStatusBar.setValue(errorMsg);
		throw new WrongValueException (errorComponent, errorMsg);
	  }
	 if(txtLibelle.getValue().equals(""))
	   {
         errorComponent = txtLibelle;
         errorMsg = Labels.getLabel("kermel.archivage.libelleTypeElementArbre")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		 lbStatusBar.setStyle(ERROR_MSG_STYLE);
		 lbStatusBar.setValue(errorMsg);
		 throw new WrongValueException (errorComponent, errorMsg);
	   }
	
	 if(txtCodeType.getValue().equals(""))
     {
       errorComponent = txtCodeType;
       errorMsg = Labels.getLabel("kermel.archivage.typeNoeudClassement")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
	   lbStatusBar.setStyle(ERROR_MSG_STYLE);
	   lbStatusBar.setValue(errorMsg);
	  throw new WrongValueException (errorComponent, errorMsg);
     }
	 return true;
	 
	 
   }
   catch (Exception e) 
   {
     errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString() + " [checkFieldConstraints]";
	 errorComponent = null;
	 return false;
   }
 }

public void onSelect$lstType(){
	STE= (SygTypeElementArbre) lstType.getSelectedItem().getValue();
	bdType.setValue(STE.getLibelle());
	txtCodeType.setValue(String.valueOf(STE.getId()));
	bdType.close();
	txtCodeNode.setDisabled(true);
	bdNode.setDisabled(true);
	int idChoice = 0;
	int cptVal=0;
	for (SygTypeElementArbre tp : types)
	{
		if (tp.getId() == Long.parseLong(txtCodeType.getValue()))
		{
			cptVal=idChoice;
		}			
		idChoice= idChoice++;
	}
   
	int cpt = BeanLocator.defaultLookup(SygNoeudClassementSession.class).count(STE.getId());
	
	if (cptVal>0)
	{
		
		List<SygNoeudClassement> noeuds = BeanLocator.defaultLookup(SygNoeudClassementSession.class).findByLevel(types.get(cptVal-1).getId());
		lstNode.setModel(new SimpleListModel(noeuds));
		pgNode.setTotalSize(cpt);
		txtCodeNode.setDisabled(false);
		bdNode.setDisabled(false);
	}	
}

public void onSelect$lstNode(){
	SNC= (SygNoeudClassement) lstNode.getSelectedItem().getValue();
	bdNode.setValue(SNC.getLibelleNoeud());
	txtCodeNode.setValue(String.valueOf(SNC.getId()));
	bdNode.close();

}

public class TypesRenderer implements ListitemRenderer{
	
	
	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygTypeElementArbre type = (SygTypeElementArbre) data;
		item.setValue(type);
		
		
		Listcell cellCode = new Listcell(String.valueOf(type.getId()));
		cellCode.setParent(item);
		
		Listcell cellLibelle = new Listcell(type.getLibelle());
		cellLibelle.setParent(item);
	
	}
	}
	public void onFocus$txtRechercherType(){
	if(txtRechercherType.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.natureprix"))){
		txtRechercherType.setValue("");
	}		 
	}
	
	public void  onClick$btnRechercherNature(){
	if(txtRechercherType.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.natureprix")) || txtRechercherType.getValue().equals("")){
		libelleType = null;
		page=null;
	}else{
		libelleType = txtRechercherType.getValue();
		page="0";
	}
	Events.postEvent(ApplicationEvents.ON_NATURES, this, page);
	}


	
	
	public class NodesRenderer implements ListitemRenderer{
		
		
		
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygNoeudClassement type = (SygNoeudClassement) data;
			item.setValue(type);
			
			
			Listcell cellCode = new Listcell(String.valueOf(type.getId()));
			cellCode.setParent(item);
			
			Listcell cellLibelle = new Listcell(type.getLibelleNoeud());
			cellLibelle.setParent(item);
		
		}
		}
		public void onFocus$txtRechercherNode(){
		if(txtRechercherNode.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.natureprix"))){
			txtRechercherNode.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherNode(){
		if(txtRechercherNode.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.natureprix")) || txtRechercherType.getValue().equals("")){
			libelleNode = null;
			page=null;
		}else{
			libelleNode = txtRechercherNode.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_CATEGORIES, this, page);
		}

	
	
	
	public void onOK$txtCodeType() {
		 List<SygTypeElementArbre> natures = BeanLocator.defaultLookup(SygTypeElementArbreSession.class).findAll();
				
			if (natures.size() > 0) {
				STE=natures.get(0);
				txtCodeType.setValue(String.valueOf(STE.getId()));
				bdType.setValue(STE.getLibelle());
			}
			else {
				bdType.setValue(null);
				bdType.open();
				txtCodeType.setFocus(true);
			}		
		}
	public void onOK$txtCodeNode() {
		 List<SygNoeudClassement> nodes = BeanLocator.defaultLookup(SygNoeudClassementSession.class).findAll();
				
			if (nodes.size() > 0) {
				SNC=nodes.get(0);
				txtCodeNode.setValue(String.valueOf(SNC.getId()));
				bdNode.setValue(SNC.getLibelleNoeud());
			}
			else {
				bdNode.setValue(null);
				bdNode.open();
				txtCodeNode.setFocus(true);
			}			
		}
}