package sn.ssi.kermel.web.archives.controllers;

//import org.zkoss.zul.DefaultTreeNode;

import java.util.Date;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.archivage.ejb.SygArchivesContenuSession;
import sn.ssi.kermel.be.archivage.ejb.SygNoeudClassementSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygArchivesContenu;
import sn.ssi.kermel.be.entity.SygNoeudClassement;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class ArchivesDragAndDropView extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode, login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle, txtCode, txtDescriptions, txtMotCle;
	private Datebox txtDateArchive;
	private Long code;
	private SygArchivesContenu archContent = new SygArchivesContenu();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	Session session = getHttpSession();
	private static final String ERROR_MSG_STYLE = "color:red";

	// SygArchivesContenu types = new SygArchivesContenu();

	@Override
	public void onEvent(Event event) throws Exception {

	}

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		code = (Long) map.get(PARAM_WINDOW_CODE);
		archContent = BeanLocator
				.defaultLookup(SygArchivesContenuSession.class).findById(code);
		txtCode.setValue(code.toString());
		txtLibelle.setValue(archContent.getLibelle());

		/*
		 * if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) { code = (Long)
		 * map.get(PARAM_WINDOW_CODE); type =
		 * BeanLocator.defaultLookup(SygArchivesContenuSession.class)
		 * .findById(code); txtCode.setValue(type.getId().toString());
		 * txtLibelle.setValue(type.getLibelle());
		 * txtDescriptions.setValue(type.getDescription());
		 * txtDateArchive.setValue(type.getDateArchive()); }
		 */
	}

	public void onOK() {
		if (checkFieldConstraints()) {
			archContent.setId(Long.parseLong(txtCode.getValue()));
			archContent.setLibelle(txtLibelle.getValue());
			archContent.setMotCle(txtMotCle.getValue());
			//archContent.setDateArchive((Date) txtDateArchive.getValue());
			archContent.setDescription(txtDescriptions.getValue());
			Long id = ((Long) getHttpSession().getAttribute("idNode"));
			SygNoeudClassement conteneur = BeanLocator.defaultLookup(SygNoeudClassementSession.class).findById(id);
			archContent.setConteneur(conteneur);

			BeanLocator.defaultLookup(SygArchivesContenuSession.class).update(archContent);
			BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_ARCHIVECONFIG",Labels.getLabel("kermel.referentiel.common.typemarche.modifier")+ " :" + new Date(), login);

			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(),null);
			detach();
		}
	}

	private boolean checkFieldConstraints() {
		try {

			if (txtCode.getValue().equals("")) {
				errorComponent = txtCode;
				errorMsg = Labels
						.getLabel("kermel.archivage.idTypeElementArbre")
						+ ": "
						+ Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);
			}
			if (txtLibelle.getValue().equals("")) {
				errorComponent = txtLibelle;
				errorMsg = Labels
						.getLabel("kermel.archivage.libelleTypeElementArbre")
						+ ": "
						+ Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);
			}

			return true;

		} catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": "
					+ e.toString() + " [checkFieldConstraints]";
			errorComponent = null;
			return false;
		}
	}
}