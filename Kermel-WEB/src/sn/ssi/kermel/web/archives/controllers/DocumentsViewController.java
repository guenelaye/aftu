package sn.ssi.kermel.web.archives.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.DropEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.archivage.ejb.SygArchivesContenuSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygArchivesContenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class DocumentsViewController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstBoxleft, lstBoxright;

	@Override
	public void afterCompose() {

		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		lstBoxright.setItemRenderer(this);
		lstBoxleft.setItemRenderer(this);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		lstBoxright.addEventListener("onDrop", this);
		lstBoxleft.addEventListener("onDrop", this);

	}

	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			List<SygArchivesContenu> types = BeanLocator.defaultLookup(
					SygArchivesContenuSession.class).findAllToArchive();
			SimpleListModel listModel = new SimpleListModel(types);
			lstBoxright.setModel(listModel);
			
			Long id = ((Long) getHttpSession().getAttribute("idNode"));
			//SygNoeudClassement conteneur = BeanLocator.defaultLookup(SygNoeudClassementSession.class).findById(id);
			List<SygArchivesContenu> archContent = BeanLocator.defaultLookup(SygArchivesContenuSession.class).findAllArchived(id);
			SimpleListModel listModel_right = new SimpleListModel(archContent);
			lstBoxleft.setModel(listModel_right);
			
		} else if (event.getName().equalsIgnoreCase("onDrop")) {

			Listitem ll = (Listitem) ((DropEvent) event).getDragged();

			final String uri = "/archivage/addmetadonnearchive.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE,
					Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT, "500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ArchivesDragAndDropView.PARAM_WINDOW_CODE, ll.getValue());
			showPopupWindow(uri, data, display);
		}

	}

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {

		SygArchivesContenu types = (SygArchivesContenu) data;
		item.setDraggable("true");
		item.setDroppable("true");
		item.setValue(types.getId());

		Listcell cellId = new Listcell(types.getId().toString());
		cellId.setVisible(false);
		cellId.setParent(item);

		Listcell cellLibelle = new Listcell(types.getLibelle());
		cellLibelle.setImage("/images/icone_pdf.png");
		cellLibelle.setParent(item);

	}

}