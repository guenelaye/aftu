package sn.ssi.kermel.web.archives.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;

import sn.ssi.kermel.be.archivage.ejb.SygNoeudClassementSession;
import sn.ssi.kermel.be.archivage.ejb.SygTypeElementArbreSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygNoeudClassement;
import sn.ssi.kermel.be.entity.SygTypeElementArbre;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

/**
 * 
 * @author Omar MAR
 * @since 01 Decembre 2012, 20:14
 */
public class PalettePlanArchivageController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";

	private List<SygTypeElementArbre> lstTE;
	private List<SygNoeudClassement> lst_SygNC;
	private int cpt;
	private int cpt_TE;

	// SygTypeElementArbre typeElementArbre=new SygTypeElementArbre();

	Session session = getHttpSession();
	private Label lblModePlanClassement, lgtitre;
	public static final String CURRENT_MODULE = "CURRENT_MODULE";
	private Tree tree;
	private Treeitem treeitem;
	private Treechildren treechildren;
	private Include idinclude;
	private KermelSousMenu monSousMenu;
	private Menuitem ADD_GESTIONNOEUDARCHIVE, MOD_GESTIONNOEUDARCHIVE,
			DEL_GESTIONNOEUDARCHIVE;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_GESTIONNOEUDARCHIVE);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_GESTIONNOEUDARCHIVE != null) {
			ADD_GESTIONNOEUDARCHIVE.addForward(ApplicationEvents.ON_CLICK,
					this, ApplicationEvents.ON_ADD);
		}
		if (MOD_GESTIONNOEUDARCHIVE != null) {
			MOD_GESTIONNOEUDARCHIVE.addForward(ApplicationEvents.ON_CLICK,
					this, ApplicationEvents.ON_EDIT);
		}
		if (DEL_GESTIONNOEUDARCHIVE != null) {
			DEL_GESTIONNOEUDARCHIVE.addForward(ApplicationEvents.ON_CLICK,
					this, ApplicationEvents.ON_DELETE);
		}

		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);

		this.cpt_TE = BeanLocator.defaultLookup(
				SygTypeElementArbreSession.class).count();
		this.lstTE = BeanLocator .defaultLookup(SygTypeElementArbreSession.class).findAll();
		int cpt_ = BeanLocator.defaultLookup(SygNoeudClassementSession.class) .count(this.lstTE.get(0).getId());
		lst_SygNC = BeanLocator.defaultLookup(SygNoeudClassementSession.class)
				.findByLevel(this.lstTE.get(0).getId());
		Treechildren tc = new Treechildren();

		for (int i = 0; i < cpt_; i++) {
			Treeitem ti = new Treeitem();
			ti.setLabel(String.valueOf(lst_SygNC.get(i).getLibelleNoeud()));
			ti.setValue(lst_SygNC.get(i).getId());
			// arborescence(lst_SygNC.get(i).getId()).setParent(ti);
			ti.setParent(tc);
			ti.setOpen(false);
			ti.addEventListener(ApplicationEvents.ON_ADD, this);
			ti.addEventListener("onRightClick", this);

		}
		tc.setParent(tree);

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	public void onCreate(CreateEvent createEvent) {

		List<SygTypeElementArbre> lst = BeanLocator.defaultLookup(
				SygTypeElementArbreSession.class).findAll();
		String value = "";
		for (SygTypeElementArbre typeElment : lst) {
			if (value.equalsIgnoreCase(""))
				value = typeElment.getLibelle();
			else
				value = value + " --> " + typeElment.getLibelle();
		}
		lblModePlanClassement.setValue(value);

		// idinclude.setSrc("/archivage/documentsView.zul");

		// tree.setSelectedItem(iteminfosgenerales);
		lgtitre.setValue(Labels.getLabel("kermel.archivage.infoPlanClassment"));

	}

	@Override
	public void onEvent(Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {

			final String uri = "/archivage/formaddfirstlevelnoeuds.zul";
			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE,
					Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT, "500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormAddFirstNoeudController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
//			if (tree.getSelectedItem() != null) {
//				data.put(FormAddController.PARAM_WINDOW_CODE, tree
//						.getSelectedItem().getValue().toString());
//			}

			showPopupWindow(uri, data, display);
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (tree.getSelectedItem() == null) {
				throw new WrongValueException(tree,
						Labels.getLabel("kermel.error.select.item"));
			} else {

				final String uri = "/archivage/formaddnoeud.zul";

				final HashMap<String, String> display = new HashMap<String, String>();
				display.put(DSP_HEIGHT, "500px");
				display.put(DSP_WIDTH, "80%");
				display.put(DSP_TITLE,
						Labels.getLabel("kermel.common.form.editer"));

				final HashMap<String, Object> data = new HashMap<String, Object>();
				data.put(FormAddFirstNoeudController.WINDOW_PARAM_MODE,
						UIConstants.MODE_EDIT);
				data.put(FormAddFirstNoeudController.PARAM_WINDOW_CODE, tree
						.getSelectedItem().getValue().toString());

				showPopupWindow(uri, data, display);

			}
		}

		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)) {
			if (tree.getSelectedItem() == null)

				throw new WrongValueException(tree,
						Labels.getLabel("kermel.error.select.item"));

			HashMap<String, String> display = new HashMap<String, String>(); // permet
			display.put(MessageBoxController.DSP_MESSAGE,
					Labels.getLabel("kermel.common.form.question.supprimer"));
			display.put(MessageBoxController.DSP_TITLE,
					Labels.getLabel("kermel.common.form.supprimer"));
			display.put(MessageBoxController.DSP_HEIGHT, "250px");
			display.put(MessageBoxController.DSP_WIDTH, "47%");

			HashMap<String, Object> map = new HashMap<String, Object>(); // permet
			map.put(CURRENT_MODULE, tree.getSelectedItem().getValue());
			showMessageBox(display, map);

		} else if (event.getName().equalsIgnoreCase("onRightClick")) {
			Long codes = Long.parseLong(String.valueOf(tree.getSelectedItem()
					.getValue()));
		} else if (event.getName().equalsIgnoreCase(
				ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {

			Long codes = Long.parseLong(String.valueOf(tree.getSelectedItem()
					.getValue()));
			BeanLocator.defaultLookup(SygNoeudClassementSession.class).delete(
					codes);
			// BeanLocator.defaultLookup(JournalSession.class).logAction("DEL_ARCHIVECONFIG",
			// Labels.getLabel("kermel.referentiel.common.typemarche.suppression")+" :"
			// + new Date(), login);

			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			loadApplicationState("archives_plan_configuration");
		}

	}

	// public void onSelect$tree(){
	public Treechildren arborescence(Long idSelected) {
		Treechildren tc = new Treechildren();
		List<SygNoeudClassement> lst_SygNC_ = BeanLocator.defaultLookup(
				SygNoeudClassementSession.class).findByNode(idSelected);

		for (int i = 0; i < lst_SygNC_.size(); i++) {
			Treeitem ti = new Treeitem();
			ti.setLabel(lst_SygNC_.get(i).getLibelleNoeud());
			ti.setValue(lst_SygNC_.get(i).getId());
			ti.setParent(tc);

			ti.addEventListener(ApplicationEvents.ON_ADD, this);
		}
		return tc;

	}

	public void onSelect$tree() {
		Treechildren tc = null;
		Long tt = Long.valueOf(tree.getSelectedItem().getValue().toString());
		session.setAttribute("idNode", tt);
		if (tree.getSelectedItem().getTreechildren() == null) {
			tc = arborescence(tt);
			if (tc != null) {
				tc.setParent(tree.getSelectedItem());
			}
			tree.getSelectedItem().setOpen(true);

		}
		idinclude.setSrc("");
		idinclude.setSrc("/archivage/listenoeudsfils.zul");

		SygNoeudClassement node_ = BeanLocator.defaultLookup(
				SygNoeudClassementSession.class).findById(tt);

		int level = node_.getSygTypeElement().getNiveau();

		if (level == cpt_TE) {
			idinclude.setSrc("");
		}

	}
}