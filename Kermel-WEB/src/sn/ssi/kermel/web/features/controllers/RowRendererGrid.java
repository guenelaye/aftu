package sn.ssi.kermel.web.features.controllers;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SysAction;
import sn.ssi.kermel.be.entity.SysFeature;
import sn.ssi.kermel.be.entity.SysProfilAction;
import sn.ssi.kermel.be.security.ModulesSession;

public class RowRendererGrid implements RowRenderer, EventListener {

	@Override
	public void onEvent(final Event event) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void render(final Row row, final Object data, int index) throws Exception {

		final FeatureRow pr = (FeatureRow) data;
		final String x = pr.getType();
		final String featureCode = pr.getFeatureCode();
		pr.getModuleCode();
		final String profilCode = pr.getProfilCode();
		final String code = pr.getCode();

		final SysFeature feature = BeanLocator.defaultLookup(
				ModulesSession.class).findFeatureByCode(featureCode);
		final Checkbox c = new Checkbox();
		c.setId(featureCode + "_" + code);
		if (x.equals("1")) {
			final SysAction act = BeanLocator.defaultLookup(
					ModulesSession.class).findActionByCode(code);
			if (act.getActLibelle() != null) {
				c.setLabel(act.getActLibelle());
			} else {
				c.setLabel(act.getActCode());
			}
			c.setParent(row);
			boolean allow = false;
			final SysProfilAction pac = BeanLocator.defaultLookup(
					ModulesSession.class).findProfilAction(code, profilCode);
			if (pac != null)
				if (pac.getAllow() == 1) {
					allow = true;
				}
			c.setChecked(allow);
			c.setId(act.getActCode() + "_act");
			row.setValue(act.getActCode());
		} else {
			final Label l = new Label();
			if (feature.getFeaLibelle() != null) {
				l.setValue(feature.getFeaLibelle());
			} else {
				l.setValue(feature.getFeaCode());
			}
			l.setParent(row);
		}

	}
}