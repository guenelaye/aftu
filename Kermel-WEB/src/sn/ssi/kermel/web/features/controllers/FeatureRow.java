package sn.ssi.kermel.web.features.controllers;

public class FeatureRow {
	String type;
	String code;
	String moduleCode;
	String profilCode;
	String featureCode;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getModuleCode() {
		return moduleCode;
	}
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	public String getProfilCode() {
		return profilCode;
	}
	public void setProfilCode(String profilCode) {
		this.profilCode = profilCode;
	}
	public String getFeatureCode() {
		return featureCode;
	}
	public void setFeatureCode(String featureCode) {
		this.featureCode = featureCode;
	}
	public FeatureRow(String type, String code, String moduleCode,
			String profilCode, String featureCode) {
		super();
		this.type = type;
		this.code = code;
		this.moduleCode = moduleCode;
		this.profilCode = profilCode;
		this.featureCode = featureCode;
	}

}
