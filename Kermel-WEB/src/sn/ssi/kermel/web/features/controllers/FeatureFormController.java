package sn.ssi.kermel.web.features.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.SimpleGroupsModel;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SysAction;
import sn.ssi.kermel.be.entity.SysFeature;
import sn.ssi.kermel.be.entity.SysModule;
import sn.ssi.kermel.be.entity.SysProfil;
import sn.ssi.kermel.be.entity.SysProfilAction;
import sn.ssi.kermel.be.security.ModulesSession;
import sn.ssi.kermel.be.security.ProfilsSession;
import sn.ssi.kermel.be.workflow.entity.SysRole;
import sn.ssi.kermel.be.workflow.entity.SysState;
import sn.ssi.kermel.be.workflow.manager.ejb.WorkflowManagerSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings( { "unchecked", "serial" })
public class FeatureFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstModules;
	private Grid grdFeatures;
	private Textbox searchBox;
	private Button btOK;
	public static final String CURRENT_PROCESSUS = "CURRENT_Module";
	public static final String WINDOW_PARAM_PROFIL_CODE = "PROFIL_CODE";
	String profilCode;

	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);

		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lstModules.setItemRenderer(this);
		grdFeatures.setRowRenderer(new RowRendererGrid());
		searchBox.addEventListener(Events.ON_CHANGING, this);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	public void onCreate(final CreateEvent createEvent) {
		final Map windowParams = createEvent.getArg();
		profilCode = (String) windowParams.get(WINDOW_PARAM_PROFIL_CODE);

	}

	private boolean inList(final String stateCode, final List removables) {
		for (int i = 0; i < removables.size(); i++)
			if (removables.get(i).toString().equals(stateCode))
				return true;

		return false;
	}

	@SuppressWarnings("unused")
	private List<SysState> removeDoubles(final List<SysState> states) {
		final ArrayList removables = new ArrayList();
		final ArrayList<SysState> finalStates = new ArrayList();

		for (int i = 0; i < states.size(); i++)
			if (!inList(states.get(i).getCode(), removables)) {
				finalStates.add(states.get(i));
				removables.add(states.get(i).getCode());
			}
		return finalStates;
	}

	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			final List<SysModule> modules = BeanLocator.defaultLookup(
					ModulesSession.class).findModules(0, 1000);
			final SimpleListModel listModel = new SimpleListModel(modules);
			lstModules.setModel(listModel);
		} else if (event.getName().equalsIgnoreCase(Events.ON_CLICK)) {
			final Listitem listitem = (Listitem) event.getTarget();
			listitem.getValue().toString();
			final List<SysFeature> features = BeanLocator.defaultLookup(
					ModulesSession.class).findFeatures(
					listitem.getValue().toString(), 0, 1000);
			// features = removeDoubles(states);
			final Object[][] datas = new Object[features.size()][];
			final Object[] heads = new Object[features.size()];

			for (int i = 0; i < features.size(); i++) {
				final String featureCode = features.get(i).getFeaCode();
				final List<SysAction> act = BeanLocator.defaultLookup(
						ModulesSession.class).findActions(featureCode, 0, 1000);
				datas[i] = new Object[act.size()];
				final FeatureRow pr = new FeatureRow("0", featureCode, listitem
						.getValue().toString(), profilCode, featureCode);
				heads[i] = pr;
				for (int j = 0; j < act.size(); j++) {
					final FeatureRow prs = new FeatureRow("1", String
							.valueOf(act.get(j).getActCode()), listitem
							.getValue().toString(), profilCode, featureCode);
					datas[i][j] = prs;
				}
			}

			final SimpleGroupsModel gridModel = new SimpleGroupsModel(datas,
					heads);
			grdFeatures.setModel(gridModel);
			if (features.size() > 0) {
				btOK.setVisible(true);
			} else {
				btOK.setVisible(false);
			}

		} else if (event.getName().equalsIgnoreCase(Events.ON_CHANGING)) {
			final List<SysRole> processus = BeanLocator.defaultLookup(
					WorkflowManagerSession.class).findRoles(
					searchBox.getValue());
			final SimpleListModel listModel = new SimpleListModel(processus);
			lstModules.setModel(listModel);
		}

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		final SysModule module = (SysModule) data;
		item.setValue(module.getModCode());
		item.addEventListener(Events.ON_CLICK, this);
		final Listcell cellImage = new Listcell();
		cellImage.setImage("/img/configuration.png");
		cellImage.setParent(item);
		final Listcell cellLibelle = new Listcell(module.getModLibelle());
		cellLibelle.setParent(item);
	}

	public void onOK() {

		final SysProfil profil = BeanLocator
				.defaultLookup(ProfilsSession.class).findByCode(profilCode);

		final Listitem listitem = lstModules.getSelectedItem();
		final String moduleCode = listitem.getValue().toString();

		BeanLocator.defaultLookup(ModulesSession.class).findByCode(moduleCode);

		for (int i = 0; i < grdFeatures.getGroupsModel().getGroupCount(); i++) {
			final String featureCode = ((FeatureRow) grdFeatures
					.getGroupsModel().getGroup(i)).getFeatureCode();
			BeanLocator.defaultLookup(ModulesSession.class).findFeatureByCode(
					featureCode);

			final List<SysAction> acts = BeanLocator.defaultLookup(
					ModulesSession.class).findActions(featureCode, 0, 1000);
			for (int j = 0; j < acts.size(); j++) {
				final SysAction act = acts.get(j);
				SysProfilAction pac;
				pac = BeanLocator.defaultLookup(ModulesSession.class)
						.findProfilAction(act.getActCode(), profilCode);
				if (pac == null) {
					pac = new SysProfilAction();
				}
				pac.setSysAction(act);
				if (((Checkbox) grdFeatures
						.getFellow(act.getActCode() + "_act")).isChecked()) {
					pac.setAllow((short) 1);
				} else {
					pac.setAllow((short) 0);
				}

				pac.setSysProfil(profil);
				BeanLocator.defaultLookup(ModulesSession.class)
						.saveProfilAction(pac);
			}
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
	}
}