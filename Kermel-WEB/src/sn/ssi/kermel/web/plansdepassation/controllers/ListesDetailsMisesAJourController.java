package sn.ssi.kermel.web.plansdepassation.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.plansdepassation.ejb.PlansdepassationSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class ListesDetailsMisesAJourController extends AbstractWindow implements
		AfterCompose, EventListener, ListitemRenderer {

	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_USER_ID = "USER_ID";
	public static String WINDOW_PARAM_DIV = "DIV";

	String mode;
	long usrId = -1;
	Utilisateur user;
	private Listbox lstListePlans;
	private Textbox txtNumeros;
	private Paging pgPaginationPlans;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE, activePage,
			numeroplan;
	private SygAutoriteContractante autorite;
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg, login, type;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Radio rdoui, rdnon;
	private Utilisateur infoscompte;
	private Div step0, step1, step2;
	private Label lblPlan, lblPlanstep2;
	private Menuitem menuNext, menuPrevious;
	String numero = null, prenom = null, reference, callBack, fonction = null,
			service = null, page = null, numplan, status;
	// private Datebox dtdebut,dtfin;
	// private Date datedebut,datefin;
	SygPlansdepassation plan = new SygPlansdepassation();
	SygPlansdepassation plangeneree = new SygPlansdepassation();
	List<SygPlansdepassation> listesplans = new ArrayList<SygPlansdepassation>();
	SygPlansdepassation infosplan = new SygPlansdepassation();
	List<SygRealisations> realisations = new ArrayList<SygRealisations>();
	SygRealisations realisation = new SygRealisations();
	public static final String CURRENT_MODULE = "CURRENT_MODULE";
	Session session = getHttpSession();
	private Long idplan;
	private Menuitem WSOUM_PLANPASSATION, menuSoumettreValidation;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);

		addEventListener(ApplicationEvents.ON_PLANS, this);
		lstListePlans.setItemRenderer(this);
		pgPaginationPlans.setPageSize(byPage);
		pgPaginationPlans.addForward(ApplicationEvents.ON_PAGING, this,
				ApplicationEvents.ON_PLANS);

	}

	@SuppressWarnings("unchecked")
	public void onCreate(final CreateEvent createEvent) {
		final Map windowParams = createEvent.getArg();
		mode = (String) windowParams.get(WINDOW_PARAM_MODE);
		infoscompte = (Utilisateur) getHttpSession()
				.getAttribute("infoscompte");
		login = ((String) getHttpSession().getAttribute("user"));
		if ((infoscompte.getType().equals(UIConstants.USERS_TYPES_AC))) {
			autorite = infoscompte.getAutorite();
		} else {
			autorite = null;
		}
		idplan = (Long) session.getAttribute("idplans");
		plan = BeanLocator.defaultLookup(PlansdepassationSession.class)
				.findById(idplan);
		realisations = BeanLocator.defaultLookup(RealisationsSession.class)
				.find(null, null, plan, null, null, null, null, null);
		lblPlan.setValue(plan.getNumplan() + " "
				+ Labels.getLabel("kermel.plansdepassation.infos.gestion")
				+ " " + plan.getAnnee());
		Events.postEvent(ApplicationEvents.ON_PLANS, this, null);
	}

	@Override
	public void onEvent(final Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_PLANS)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPaginationPlans.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPaginationPlans.getActivePage() * byPage;
				pgPaginationPlans.setPageSize(byPage);
			}
			listesplans = BeanLocator.defaultLookup(
					PlansdepassationSession.class).ListesPlans(activePage,
					byPage, null, null, null, autorite, plan);
			SimpleListModel listModel = new SimpleListModel(listesplans);
			lstListePlans.setModel(listModel);
			pgPaginationPlans.setTotalSize(BeanLocator.defaultLookup(
					PlansdepassationSession.class).countListesPlans(null, null,
					null, autorite, plan));
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			if (listesplans.size() == 0) {
				final String uri = "/plansdepassation/formmiseajour.zul";
				final HashMap<String, String> display = new HashMap<String, String>();
				display.put(DSP_TITLE,
						Labels.getLabel("kermel.plansdepassation.miseajours"));
				display.put(DSP_HEIGHT, "500px");
				display.put(DSP_WIDTH, "80%");

				final HashMap<String, Object> data = new HashMap<String, Object>();
				data.put(MiseAJourFormController.WINDOW_PARAM_MODE,
						UIConstants.MODE_NEW);
				showPopupWindow(uri, data, display);
			} else {
				if (listesplans
						.get(0)
						.getStatus()
						.equals(Labels
								.getLabel("kermel.plansdepassation.statut.publier"))) {
					final String uri = "/plansdepassation/formmiseajour.zul";
					final HashMap<String, String> display = new HashMap<String, String>();
					display.put(DSP_TITLE, Labels
							.getLabel("kermel.plansdepassation.miseajours"));
					display.put(DSP_HEIGHT, "500px");
					display.put(DSP_WIDTH, "80%");

					final HashMap<String, Object> data = new HashMap<String, Object>();
					data.put(MiseAJourFormController.WINDOW_PARAM_MODE,
							UIConstants.MODE_NEW);
					showPopupWindow(uri, data, display);
				} else {
					Messagebox
							.show(Labels
									.getLabel("kermel.plansdepassation.validermiseajour"),
									"Erreur", Messagebox.OK, Messagebox.ERROR);
				}

			}

		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListePlans.getSelectedItem() == null)
				throw new WrongValueException(lstListePlans,
						Labels.getLabel("kermel.error.select.item"));
			final String uri = "/plansdepassation/formmiseajour.zul";
			plan = (SygPlansdepassation) lstListePlans.getSelectedItem()
					.getValue();
			if ((plan.getStatus().equalsIgnoreCase(Labels
					.getLabel("kermel.plansdepassation.statut.saisie")))
					|| (plan.getStatus()
							.equalsIgnoreCase(Labels
									.getLabel("kermel.plansdepassation.statut.rejeter")))) {
				final HashMap<String, String> display = new HashMap<String, String>();
				display.put(DSP_HEIGHT, "500px");
				display.put(DSP_WIDTH, "80%");
				display.put(DSP_TITLE,
						Labels.getLabel("kermel.common.form.editer"));

				final HashMap<String, Object> data = new HashMap<String, Object>();
				data.put(MiseAJourFormController.WINDOW_PARAM_MODE,
						UIConstants.MODE_EDIT);
				data.put(MiseAJourFormController.WINDOW_PARAM_CODE,
						plan.getIDinfoplan());

				showPopupWindow(uri, data, display);
			} else {
				Messagebox
						.show(Labels
								.getLabel("kermel.plansdepassation.modifierplan"),
								"Erreur", Messagebox.OK, Messagebox.ERROR);
			}

		} else if (event.getName()
				.equalsIgnoreCase(ApplicationEvents.ON_DELETE)) {
			if (lstListePlans.getSelectedItem() == null)
				throw new WrongValueException(lstListePlans,
						Labels.getLabel("kermel.error.select.item"));
			plan = (SygPlansdepassation) lstListePlans.getSelectedItem()
					.getValue();
			if ((plan.getStatus().equalsIgnoreCase(Labels
					.getLabel("kermel.plansdepassation.statut.saisie")))
					|| (plan.getStatus()
							.equalsIgnoreCase(Labels
									.getLabel("kermel.plansdepassation.statut.rejeter")))) {
				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE, Labels
						.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE,
						Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, plan.getIDinfoplan());
				showMessageBox(display, map);
			} else {
				Messagebox.show(Labels
						.getLabel("kermel.plansdepassation.suppressionplan"),
						"Erreur", Messagebox.OK, Messagebox.ERROR);
			}

		} else if (event.getName().equalsIgnoreCase(
				ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {

		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) {
			Listcell button = (Listcell) event.getTarget();
			String toDo = (String) button.getAttribute(UIConstants.TODO);
			Long idplan = (Long) button.getAttribute("idplan");
			final String uri = "/plansdepassation/historiquesvalidationppm.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT, "700px");
			display.put(DSP_WIDTH, "80%");
			display.put(
					DSP_TITLE,
					Labels.getLabel("kermel.plansdepassation.proceduresmarches.detailsvalidation"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(DetailsRealisationFormController.PARAM_WINDOW_CODE, idplan);

			showPopupWindow(uri, data, display);
		}
	}

	@Override
	public void render(final Listitem item, final Object data, int index)
			throws Exception {
		SygPlansdepassation plans = (SygPlansdepassation) data;
		item.setValue(plans);
		item.setAttribute("status", plans.getStatus());

		Listcell cellNumero = new Listcell(plans.getMotif());
		cellNumero.setParent(item);

		Listcell cellAnnee = new Listcell(Integer.toString(plans.getVersion()));
		cellAnnee.setParent(item);

		Listcell cellDatecreation = new Listcell(UtilVue.getInstance()
				.formateLaDate(plans.getDatecreation()));
		cellDatecreation.setParent(item);

		Listcell cellDatemiseenvalidation = new Listcell("");
		if (plans.getDateMiseEnValidation() != null) {
			cellDatemiseenvalidation.setLabel(UtilVue.getInstance()
					.formateLaDate(plans.getDateMiseEnValidation()));
			item.setAttribute("datevalidation", plans.getDateMiseEnValidation());
		} else {
			item.setAttribute("datevalidation", null);
		}
		cellDatemiseenvalidation.setParent(item);

		Listcell cellStatut = new Listcell(plans.getStatus());
		if (plans.getStatus().equals(
				Labels.getLabel("kermel.plansdepassation.statut.saisie")))
			cellStatut.setLabel(Labels
					.getLabel("kermel.plansdepassation.statut.saisies"));
		else if (plans
				.getStatus()
				.equals(Labels
						.getLabel("kermel.plansdepassation.statut.miseenvalidation")))
			cellStatut
					.setLabel(Labels
							.getLabel("kermel.plansdepassation.statut.miseenvalidations"));
		else if (plans.getStatus().equals(
				Labels.getLabel("kermel.plansdepassation.statut.valider")))
			cellStatut.setLabel(Labels
					.getLabel("kermel.plansdepassation.statut.validers")
					+ " "
					+ UtilVue.getInstance().formateLaDate(
							plans.getDateValidation()));
		else if (plans.getStatus().equals(
				Labels.getLabel("kermel.plansdepassation.statut.rejeter")))
			cellStatut
					.setLabel(Labels
							.getLabel("kermel.plansdepassation.statut.rejeters")
							+ " "
							+ UtilVue.getInstance().formateLaDate(
									plans.getDateRejet()));
		else if (plans.getStatus().equals(
				Labels.getLabel("kermel.plansdepassation.statut.publier")))
			cellStatut.setLabel(Labels
					.getLabel("kermel.plansdepassation.statut.publiers")
					+ " "
					+ UtilVue.getInstance().formateLaDate(
							plans.getDatepublication()));
		cellStatut.setParent(item);

		Listcell cellImage = new Listcell();
		cellImage.setImage("/images/zoom.png");
		cellImage.setAttribute(UIConstants.TODO, "details");
		cellImage.setAttribute("idplan", plans.getIDinfoplan());
		cellImage
				.setTooltiptext(Labels
						.getLabel("kermel.plansdepassation.passationsmarches.historiquevalidation"));
		cellImage.addEventListener(Events.ON_CLICK,
				ListesDetailsMisesAJourController.this);
		cellImage.setParent(item);

	}

	public void onClick$menuPrevious() {
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
	}

	public void onClick$menuFermer() {
		loadApplicationState("maj_plan");

	}

	public void onClick$menuFermerstep1() {
		detach();
	}

	public void onClick$menuFermerstep2() {
		detach();
	}

	public void onClick$menuNextstep1() {
		numeroplan = listesplans.size() + 2;
		numplan = plan.getNumplan()
				.substring(0, plan.getNumplan().length() - 1) + numeroplan;
		txtNumeros.setValue(numplan);
		step0.setVisible(false);
		step1.setVisible(false);
		step2.setVisible(true);
	}

	public void onClick$menuPreviousstep2() {
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
	}

	public void onClick$menuRealisations() {
		if (lstListePlans.getSelectedItem() == null)
			throw new WrongValueException(lstListePlans,
					Labels.getLabel("kermel.error.select.item"));
		plan = (SygPlansdepassation) lstListePlans.getSelectedItem().getValue();
		session.setAttribute("idplan", plan.getIDinfoplan());
		loadApplicationState("realisations_maj");
	}

	public void onClick$menuSoumettreValidation() throws InterruptedException {
		if (lstListePlans.getSelectedItem() == null)
			throw new WrongValueException(lstListePlans,
					Labels.getLabel("kermel.error.select.item"));

		plan = (SygPlansdepassation) lstListePlans.getSelectedItem().getValue();
		realisations = BeanLocator.defaultLookup(RealisationsSession.class)
				.find(0, -1, null, null, plan, null);
		if (realisations.size() > 0) {
			final String uri = "/plansdepassation/formsousmissionvalidationmaj.zul";
			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT, "650px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE,
					Labels.getLabel("kermel.plansdepassation.validation"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ValidationFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(ValidationFormController.PARAM_WINDOW_CODE,
					plan.getIDinfoplan());

			showPopupWindow(uri, data, display);
		} else {
			Messagebox.show(Labels
					.getLabel("kermel.plansdepassation.realisation.saisir"),
					"Erreur", Messagebox.OK, Messagebox.ERROR);

		}

	}

	public void onSelect$lstListePlans() {
		status = (String) lstListePlans.getSelectedItem()
				.getAttribute("status");

		if ((status.equalsIgnoreCase(Labels
				.getLabel("kermel.plansdepassation.statut.saisie")))
				|| (status.equalsIgnoreCase(Labels
						.getLabel("kermel.plansdepassation.statut.rejeter")))) {
			if (menuSoumettreValidation != null)
				menuSoumettreValidation.setDisabled(false);

		} else {

			if (menuSoumettreValidation != null)
				menuSoumettreValidation.setDisabled(true);

		}
	}
}
