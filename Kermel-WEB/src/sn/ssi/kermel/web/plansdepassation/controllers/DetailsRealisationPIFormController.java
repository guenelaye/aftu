package sn.ssi.kermel.web.plansdepassation.controllers;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygService;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.plansdepassation.ejb.PlansdepassationSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class DetailsRealisationPIFormController extends AbstractWindow implements
		 AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String login;
	Long code;
	private	SygRealisations realisation=new SygRealisations();
	SygService service=null;
	SygTypesmarches typemarche=null;
	SygModepassation modepassation=null;
	SygBailleurs bailleur=null;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Long idplan;
    private Label lblInfos,lblReference,lblRealisation,lblTypemarche,lblModepassation,lblDelaiexecution,lblMontant,
   lblDatepreparationtdrami,lblDateavisccmpdncmpami,lblDatelancementmanifestation,lblDateouverturemanifestation,
    lblDatelisterestreintepreparationdp,lblDateavisccmpdncmpdp,lblDatenonobjectionptf,lblDateinvitationsoumission,lblDateouvertureproposition,lblDatefinevaluation,
    lblDateavisccmpdncmpdppt,lblDatenonobjectionptfpt,lblDateouverture,lblDatefinevaluationpf,lblDateavisccmpdncmp,lblDatenegociation,lblDateavisptfservicecontrole,
    lblDateprevisionnellesignaturecontrat;
    Session session = getHttpSession();
	List<SygRealisationsBailleurs> bailleur_realisation = new ArrayList<SygRealisationsBailleurs>();
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	private Utilisateur infoscompte;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		

		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		idplan=(Long) session.getAttribute("idplan");
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		plan=BeanLocator.defaultLookup(PlansdepassationSession.class).findById(idplan);
		lblInfos.setValue(plan.getNumplan()+" "+Labels.getLabel("kermel.plansdepassation.infos.gestion")+" "+plan.getAnnee());
		code = (Long) map.get(PARAM_WINDOW_CODE);
		realisation = BeanLocator.defaultLookup(RealisationsSession.class).findById(code);
		lblReference.setValue(realisation.getReference());
		lblRealisation.setValue(realisation.getLibelle());
		lblTypemarche.setValue(realisation.getTypemarche().getLibelle());
		lblModepassation.setValue(realisation.getModepassation().getLibelle());
		lblDatepreparationtdrami.setValue(UtilVue.getInstance().formateLaDate(realisation.getDatepreparationtdrami()));
		lblDateavisccmpdncmpami.setValue(UtilVue.getInstance().formateLaDate(realisation.getDateavisccmpdncmpami()));
		lblDatelancementmanifestation.setValue(UtilVue.getInstance().formateLaDate(realisation.getDatelancementmanifestation()));
		lblDateouverturemanifestation.setValue(UtilVue.getInstance().formateLaDate(realisation.getDateouverturemanifestation()));
		lblDatelisterestreintepreparationdp.setValue(UtilVue.getInstance().formateLaDate(realisation.getDatepreparationdp()));
		lblDateavisccmpdncmpdp.setValue(UtilVue.getInstance().formateLaDate(realisation.getDateavisccmpdncmpdp()));
		if(realisation.getDatenonobjectionptf()!=null)
		lblDatenonobjectionptf.setValue(UtilVue.getInstance().formateLaDate(realisation.getDatenonobjectionptf()));
		lblDateinvitationsoumission.setValue(UtilVue.getInstance().formateLaDate(realisation.getDateinvitationsoumission()));
		lblDateouvertureproposition.setValue(UtilVue.getInstance().formateLaDate(realisation.getDateouverturedp()));
		lblDatefinevaluation.setValue(UtilVue.getInstance().formateLaDate(realisation.getDatefinevaluation()));
		lblDateavisccmpdncmpdppt.setValue(UtilVue.getInstance().formateLaDate(realisation.getDateavisccmpdncmpdppt()));
		if(realisation.getDatenonobjectionptfpt()!=null)
		lblDatenonobjectionptfpt.setValue(UtilVue.getInstance().formateLaDate(realisation.getDatenonobjectionptfpt()));
		lblDateouverture.setValue(UtilVue.getInstance().formateLaDate(realisation.getDateouverture()));
		lblDatefinevaluationpf.setValue(UtilVue.getInstance().formateLaDate(realisation.getDatefinevaluationpf()));
		lblDateavisccmpdncmp.setValue(UtilVue.getInstance().formateLaDate(realisation.getDateavisccmpdncmp()));
		lblDatenegociation.setValue(UtilVue.getInstance().formateLaDate(realisation.getDatenegociation()));
		if(realisation.getDateavisptfservcontrole()!=null)
		lblDateavisptfservicecontrole.setValue(UtilVue.getInstance().formateLaDate(realisation.getDateavisptfservcontrole()));
		lblDateprevisionnellesignaturecontrat.setValue(UtilVue.getInstance().formateLaDate(realisation.getDateprevisionnellesignaturecontrat()));
		
		
		lblMontant.setValue(ToolKermel.format2Decimal(realisation.getMontant()));
		lblDelaiexecution.setValue(Integer.toString(realisation.getDelaiexecution()));
		
	
	}



	
			
}
