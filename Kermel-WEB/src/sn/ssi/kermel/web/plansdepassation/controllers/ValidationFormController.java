package sn.ssi.kermel.web.plansdepassation.controllers;


import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygHistoriqueplan;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.plansdepassation.ejb.PlansdepassationSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class ValidationFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login,nomFichier;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtCommentaires,txtVersionElectronique,txtReference;
	Long code;
	private	SygPlansdepassation plan=new SygPlansdepassation();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg,numplan,libelle,mystate;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Combobox cbannee;
    private Long idplan;
    private Label lblInfos,lbltitre;
    Session session = getHttpSession();
    private Datebox dtdate;
    private final String cheminDossier = UIConstants.PATH_PJ;
    UtilVue utilVue = UtilVue.getInstance();
    private Radio rdValider,rdRejeter;
    private Utilisateur infoscompte;
    private	SygHistoriqueplan historique=new SygHistoriqueplan();
    
	@Override
	public void onEvent(Event event) throws Exception {


	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		mystate = (String) map.get(PARAM_WINDOW_MODE);
		login = ((String) getHttpSession().getAttribute("user"));
		idplan=(Long) map.get(PARAM_WINDOW_CODE);;
		plan=BeanLocator.defaultLookup(PlansdepassationSession.class).findById(idplan);
		lblInfos.setValue(plan.getNumplan()+" "+Labels.getLabel("kermel.plansdepassation.infos.gestion")+" "+plan.getAnnee());
		historique=BeanLocator.defaultLookup(PlansdepassationSession.class).Historique(plan);
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
		  	
			historique.setFichierRejet(txtVersionElectronique.getValue());
			historique.setDateRejet(dtdate.getValue());
			historique.setCommentaireRejet(txtCommentaires.getValue());
			plan.setCommentaireValidation(txtCommentaires.getValue());
			plan.setReferenceValidation(txtReference.getValue());
			plan.setFichier_validation(txtVersionElectronique.getValue());
			if(rdValider.isSelected()==true)
			  {
				plan.setDateValidation(dtdate.getValue());
				plan.setStatus(Labels.getLabel("kermel.plansdepassation.statut.valider"));
				libelle=Labels.getLabel("kermel.plansdepassation.statut.valider");
				historique.setEtat("OUI");
			  }
			else
			 {
				plan.setDateRejet(dtdate.getValue());
				plan.setStatus(Labels.getLabel("kermel.plansdepassation.statut.rejeter"));
				libelle=Labels.getLabel("kermel.plansdepassation.statut.rejeter");
				historique.setEtat("NON");
			 }
			BeanLocator.defaultLookup(PlansdepassationSession.class).update(plan);
			BeanLocator.defaultLookup(PlansdepassationSession.class).updateHistorique(historique);
			BeanLocator.defaultLookup(JournalSession.class).logAction("WVALID_PLANPASSATION", Labels.getLabel("kermel.plansdepassation.infos.numplan")+" "+lblInfos.getValue()+ libelle+" :" + UtilVue.getInstance().formateLaDate(new Date()), login);
			if(mystate.equals("realisations"))
			{
				if(rdValider.isSelected()==true)
					loadApplicationState("realisations");
				else
					loadApplicationState("plans_avalider");
			}
			else
			{
				if(mystate.equals("realisations_maj"))
				{
					if(rdValider.isSelected()==true)
						loadApplicationState("realisations_maj");
					else
						loadApplicationState("maj_plans_avalider");
				}
				else
				{
					//mystate=(String)Executions.getCurrent().getAttribute("mystate");
					if(mystate.equals("plans_avalider"))
					{
						loadApplicationState("plans_avalider");
					}
					else
					{
						loadApplicationState("maj_plans_avalider");
					}
				}
					
					
			}
			
		
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			if(dtdate.getValue()==null)
		     {
              errorComponent = dtdate;
              errorMsg = Labels.getLabel("kermel.contentieux.pj.Datecourrier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((dtdate.getValue()).after(new Date() ))
			 {
				errorComponent = dtdate;
				errorMsg =Labels.getLabel("kermel.contentieux.pj.Datecourrier")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.referentiel.date.dujour")+": "+UtilVue.getInstance().formateLaDate(new Date());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			if(rdRejeter.isSelected()==true)
			{
				if(txtCommentaires.getValue().equals(""))
			     {
	              errorComponent = txtCommentaires;
	              errorMsg = Labels.getLabel("kermel.common.commentaires")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
					lbStatusBar.setStyle(ERROR_MSG_STYLE);
					lbStatusBar.setValue(errorMsg);
					throw new WrongValueException (errorComponent, errorMsg);
			     }
			}
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
 
	
	
	public void onClick$btnChoixFichier() {
		//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
			if (ToolKermel.isWindows())
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectronique.setValue(nomFichier);
		}
	
	public void onCheck$validation()
	{
		if(rdRejeter.isSelected()==true)
		{
			lbltitre.setVisible(true);
			
		}
		else 
		{
			lbltitre.setVisible(false);
		}
	}
		
}
