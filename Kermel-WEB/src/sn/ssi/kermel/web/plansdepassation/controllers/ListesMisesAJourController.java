package sn.ssi.kermel.web.plansdepassation.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.plansdepassation.ejb.PlansdepassationSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class ListesMisesAJourController extends AbstractWindow implements AfterCompose, EventListener, ListitemRenderer {

	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_USER_ID = "USER_ID";
	public static  String WINDOW_PARAM_DIV = "DIV";

	String mode;
	long usrId = -1;
	Utilisateur user;
	private Listbox lstListe,lstListePlans;
	private Textbox txtNumero,  txtNumeros;
	private Paging pgPagination,pgPaginationPlans;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage,numeroplan;
	private SygAutoriteContractante autorite;
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg,login,type;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Radio rdoui,rdnon;
	private Utilisateur infoscompte;
	private Div step0,step1,step2;
	private Label lblPlan,lblPlanstep2;
	private Menuitem menuNext,menuPrevious;
	String numero=null,prenom=null,reference,callBack,fonction=null,service=null,page=null,numplan;
//	private Datebox dtdebut,dtfin;
//	private Date datedebut,datefin; 
	SygPlansdepassation plans=new SygPlansdepassation(); 
	SygPlansdepassation plan=new SygPlansdepassation(); 
	SygPlansdepassation plangeneree=new SygPlansdepassation(); 
	List<SygPlansdepassation> listesplans = new ArrayList<SygPlansdepassation>();
	SygPlansdepassation infosplan=new SygPlansdepassation(); 
	List<SygRealisations> realisations = new ArrayList<SygRealisations>();
	SygRealisations realisation=new SygRealisations(); 
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	Session session = getHttpSession();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
	
	
		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(final CreateEvent createEvent) {
		final Map windowParams = createEvent.getArg();
		mode = (String) windowParams.get(WINDOW_PARAM_MODE);
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		login = ((String) getHttpSession().getAttribute("user"));
		if((infoscompte.getType().equals(UIConstants.USERS_TYPES_AC)))
		{
			autorite=infoscompte.getAutorite();
		}
		else
		{
			autorite=null;
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
	}

	@Override
	public void onEvent(final Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygPlansdepassation> plans = BeanLocator.defaultLookup(PlansdepassationSession.class).find(activePage,byPage,numero,null,null,autorite, UIConstants.PARENT, Labels.getLabel("kermel.plansdepassation.statut.publier"));
			 SimpleListModel listModel = new SimpleListModel(plans);
			 lstListe.setModel(listModel);
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(PlansdepassationSession.class).count(numero,null,null,autorite, UIConstants.PARENT, Labels.getLabel("kermel.plansdepassation.statut.publier")));	
		}
		
		
	}


	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygPlansdepassation plans = (SygPlansdepassation) data;
		item.setValue(plans);
	
		 Listcell cellNumero = new Listcell(plans.getNumplan());
		 cellNumero.setParent(item);
		 
		 Listcell cellAnnee = new Listcell(Integer.toString(plans.getAnnee()));
		 cellAnnee.setParent(item);
		 
		 Listcell cellDatecreation = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDatecreation()));
		 cellDatecreation.setParent(item);
		 
		 Listcell cellDatemiseenvalidation = new Listcell("");
		  if(plans.getDateMiseEnValidation()!=null)
		  {
			  cellDatemiseenvalidation.setLabel(UtilVue.getInstance().formateLaDate(plans.getDateMiseEnValidation()));
			  item.setAttribute("datevalidation", plans.getDateMiseEnValidation());
		  }
		  else
		  {
			  item.setAttribute("datevalidation", null);
		  }
		 cellDatemiseenvalidation.setParent(item);
		 
		
		
	}

	 
	
	public void onClick$menuMiseajour(){
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		plan=(SygPlansdepassation) lstListe.getSelectedItem().getValue();
		session.setAttribute("idplans", plan.getIDinfoplan());
		loadApplicationState("details_maj_plan");
	}
	
		 public void onClick$bchercher()
			{
				if((txtNumero.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.num")))||(txtNumero.getValue().equals("")))
				 {
					numero=null;
					page=null;
				 }
				else
				 {
					numero=txtNumero.getValue();
					page="0";
				 }
				
				 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
			}
			
			
			
			public void onFocus$txtNumero()
			{
				if(txtNumero.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.num")))
					txtNumero.setValue("");
				
			}
			public void onBlur$txtNumero()
			{
				if(txtNumero.getValue().equals(""))
					txtNumero.setValue(Labels.getLabel("kermel.plansdepassation.num"));
			}
			
			public void onOK$txtNumero()
			{
				onClick$bchercher();
			}
			
		
			
}
