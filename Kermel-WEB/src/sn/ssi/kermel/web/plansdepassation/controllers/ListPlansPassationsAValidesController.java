package sn.ssi.kermel.web.plansdepassation.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.plansdepassation.ejb.PlansdepassationSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class ListPlansPassationsAValidesController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtDenomination;
    String prenom=null,reference,callBack,fonction=null,service=null,page=null;
    private Listheader lshNumero,lshAnnee,lshDatemev,lshAutorite;
    Session session = getHttpSession();
    String login,status;
    private KermelSousMenu monSousMenu;
    private Menuitem  WREAL_PLANPASSATIONS, WSOUM_PLANPASSATION, WVALID_PLANPASSATION, WWPUB_PLANPASSATION;
    List<SygRealisations> realisations = new ArrayList<SygRealisations>();
    SygPlansdepassation plan=new SygPlansdepassation();
    private String denomination=null;
    private Utilisateur infoscompte;
    private SygAutoriteContractante autorite;
    private Intbox annee;
    private int gestion=-1;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
    	monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.PLANAVALIDES);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (WREAL_PLANPASSATIONS != null) { WREAL_PLANPASSATIONS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_REALISATIONS); }
		if (WSOUM_PLANPASSATION != null) { WSOUM_PLANPASSATION.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DETAILS); }
		if (WVALID_PLANPASSATION != null) { WVALID_PLANPASSATION.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_VALIDATIONS); }
		if (WWPUB_PLANPASSATION != null) { WWPUB_PLANPASSATION.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_PUBLIER); }
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_REALISATIONS, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_VALIDATIONS, this);
		addEventListener(ApplicationEvents.ON_PUBLIER, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lshNumero.setSortAscending(new FieldComparator("Numplan", false));
		lshNumero.setSortDescending(new FieldComparator("Numplan", true));
		lshAnnee.setSortAscending(new FieldComparator("annee", false));
		lshAnnee.setSortDescending(new FieldComparator("annee", true));
		lshAutorite.setSortAscending(new FieldComparator("autorite.denomination", false));
		lshAutorite.setSortDescending(new FieldComparator("autorite.denomination", true));
		lshDatemev.setSortAscending(new FieldComparator("dateMiseEnValidation", false));
		lshDatemev.setSortDescending(new FieldComparator("dateMiseEnValidation", true));
				
				
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
     	login = ((String) getHttpSession().getAttribute("user"));
	}

	@SuppressWarnings("unchecked")
	public void onCreate(final CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if((infoscompte.getType().equals(UIConstants.USERS_TYPES_AC)))
		{
			autorite=infoscompte.getAutorite();
		}
		else
		{
			autorite=null;
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygPlansdepassation> plans = BeanLocator.defaultLookup(PlansdepassationSession.class).Plans(activePage,byPage,denomination, 1,gestion);
			 SimpleListModel listModel = new SimpleListModel(plans);
			 lstListe.setModel(listModel);
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(PlansdepassationSession.class).countPlans(denomination, 1,gestion));
		} 
		
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DETAILS)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			Long codes=(Long) lstListe .getSelectedItem().getValue();
			plan=BeanLocator.defaultLookup(PlansdepassationSession.class).findById(codes);
			realisations=BeanLocator.defaultLookup(RealisationsSession.class).find(0,-1,null,null,plan, null);
			if(realisations.size()>0)
			{
				final String uri = "/plansdepassation/formsousmissionvalidation.zul";

				final HashMap<String, String> display = new HashMap<String, String>();
				display.put(DSP_HEIGHT,"650px");
				display.put(DSP_WIDTH, "80%");
				display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.soumettre.validation"));

				final HashMap<String, Object> data = new HashMap<String, Object>();
				data.put(SoumissionValidationFormController.PARAM_WINDOW_MODE, UIConstants.MODE_EDIT);
				data.put(SoumissionValidationFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

				showPopupWindow(uri, data, display);
			}
			else
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.realisation.saisir"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_VALIDATIONS)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			final String uri = "/plansdepassation/formvalidation.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"650px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.validation"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ValidationFormController.PARAM_WINDOW_MODE, "plans_avalider");
			data.put(ValidationFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_PUBLIER)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			final String uri = "/plansdepassation/formpublication.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"650px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.publication"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(PublicationFormController.PARAM_WINDOW_MODE, "plans_avalider");
			data.put(PublicationFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		
	
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_REALISATIONS)) {
				if (lstListe.getSelectedItem() == null)
				  throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				
				session.setAttribute("idplan", lstListe.getSelectedItem().getValue());
				loadApplicationState("realisations");
			} 
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) {
				Listcell button = (Listcell) event.getTarget();
				String toDo = (String) button.getAttribute(UIConstants.TODO);
				Long idplan = (Long) button.getAttribute("idplan");
				final String uri = "/plansdepassation/historiquesvalidationppm.zul";

				final HashMap<String, String> display = new HashMap<String, String>();
				display.put(DSP_HEIGHT,"700px");
				display.put(DSP_WIDTH, "80%");
				display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.proceduresmarches.detailsvalidation"));

				final HashMap<String, Object> data = new HashMap<String, Object>();
				data.put(DetailsRealisationFormController.PARAM_WINDOW_CODE, idplan);

				showPopupWindow(uri, data, display);
			}
	}
	public void onSelect$lstListe(){
		status=(String) lstListe.getSelectedItem().getAttribute("status");
		
		if((status.equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.saisie")))||(status.equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.rejeter"))))
		{
			if(WSOUM_PLANPASSATION!=null)
			   WSOUM_PLANPASSATION.setDisabled(false);
			if(WVALID_PLANPASSATION!=null)
			WVALID_PLANPASSATION.setDisabled(true);
			if(WWPUB_PLANPASSATION!=null)
			WWPUB_PLANPASSATION.setDisabled(true);
		}
		else
		{
			if(status.equals(Labels.getLabel("kermel.plansdepassation.statut.miseenvalidation")))
			{
				if(WVALID_PLANPASSATION!=null)
				WVALID_PLANPASSATION.setDisabled(false);
				if(WWPUB_PLANPASSATION!=null)
				WWPUB_PLANPASSATION.setDisabled(true);
			}
			else
				
			{
				if(status.equals(Labels.getLabel("kermel.plansdepassation.statut.valider")))
				{
					   if(WVALID_PLANPASSATION!=null)
						WVALID_PLANPASSATION.setDisabled(true);
						if(WWPUB_PLANPASSATION!=null)
						WWPUB_PLANPASSATION.setDisabled(false);
				}
				else
				{
					if(status.equals(Labels.getLabel("kermel.plansdepassation.statut.publier")))
					{
						   if(WVALID_PLANPASSATION!=null)
							WVALID_PLANPASSATION.setDisabled(true);
							if(WWPUB_PLANPASSATION!=null)
							WWPUB_PLANPASSATION.setDisabled(true);
					}
				}
				
			}
			if(WSOUM_PLANPASSATION!=null)
			WSOUM_PLANPASSATION.setDisabled(true);
			
		}
	}
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygPlansdepassation plans = (SygPlansdepassation) data;
		item.setValue(plans.getIDinfoplan());
		item.setAttribute("status", plans.getStatus());
		

		 Listcell cellNumero = new Listcell(plans.getNumplan());
		 cellNumero.setParent(item);
		 
		 Listcell cellAutorite = new Listcell(plans.getAutorite().getDenomination());
		 cellAutorite.setParent(item);
		 
		 Listcell cellAnnee = new Listcell(Integer.toString(plans.getAnnee()));
		 cellAnnee.setParent(item);
		 
	
		 
		 Listcell cellDatemiseenvalidation = new Listcell("");
		  if(plans.getDateMiseEnValidation()!=null)
		  {
			  cellDatemiseenvalidation.setLabel(UtilVue.getInstance().formateLaDate(plans.getDateMiseEnValidation()));
			  item.setAttribute("datevalidation", plans.getDateMiseEnValidation());
		  }
		  else
		  {
			  item.setAttribute("datevalidation", null);
		  }
		 cellDatemiseenvalidation.setParent(item);
		 
		 Listcell cellStatut = new Listcell(plans.getStatus());
		  if(plans.getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.miseenvalidation")))
			   cellStatut.setLabel(Labels.getLabel("kermel.plansdepassation.statut.miseenvalidations"));
		   else  if(plans.getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.valider")))
			   cellStatut.setLabel(Labels.getLabel("kermel.plansdepassation.statut.validers")+" "+UtilVue.getInstance().formateLaDate(plans.getDateValidation()));
	   	 cellStatut.setParent(item);
		
		
	   	 Listcell cellImage = new Listcell();
		 cellImage.setImage("/images/zoom.png");
		 cellImage.setAttribute(UIConstants.TODO, "details");
		 cellImage.setAttribute("idplan", plans.getIDinfoplan());
		 cellImage.setTooltiptext(Labels.getLabel("kermel.plansdepassation.passationsmarches.historiquevalidation"));
		 cellImage.addEventListener(Events.ON_CLICK, ListPlansPassationsAValidesController.this);
		 cellImage.setParent(item);
	}
	public void onClick$bchercher()
	{
		
		if((txtDenomination.getValue().equalsIgnoreCase(Labels.getLabel("kermel.autoritecontractante.libelle")))||(txtDenomination.getValue().equals("")))
		 {
			denomination=null;
			
		 }
		else
		{
			denomination=txtDenomination.getValue();
			page="0";
		}
		if(annee.getValue()==null)
		 {
			gestion=-1;
		
		 }
		else
		 {
			gestion=annee.getValue();
			page="0";
		 }
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	public void onOK$txtDenomination()
	{
		onClick$bchercher();
	}

	public void onFocus$txtDenomination()
	{
		if(txtDenomination.getValue().equalsIgnoreCase(Labels.getLabel("kermel.autoritecontractante.libelle")))
			txtDenomination.setValue("");
	
	}
	public void onBlur$txtDenomination()
	{
		if(txtDenomination.getValue().equals(""))
			txtDenomination.setValue(Labels.getLabel("kermel.autoritecontractante.libelle"));
	}
	public void onOK$annee()
	{
		onClick$bchercher();
	}
	
}