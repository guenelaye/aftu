package sn.ssi.kermel.web.plansdepassation.controllers;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygService;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.be.entity.SygTypesmarchesmodespassations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.plansdepassation.ejb.PlansdepassationSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsBailleursSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsSession;
import sn.ssi.kermel.be.referentiel.ejb.BailleursSession;
import sn.ssi.kermel.be.referentiel.ejb.ServiceSession;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesSession;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesmodespassationsSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class RealisationMiseAJourPIFormController270513 extends AbstractWindow implements
		EventListener, AfterCompose,ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	public static final String PARAM_WINDOW_TYPE = "TYPE";
	private String mode,login;
	
	private Textbox txtLibelle;
	Long code;
	private	SygRealisations realisation=new SygRealisations();
	private	SygRealisations realisations=null;
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Bandbox bdService,bdType,bdMode,bdBailleur;
	private Paging pgService,pgType,pgMode,pgBailleur,pgPagination;
	private Listbox lstService,lstType,lstMode,lstBailleur;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE,activePage;
	private String libelleservice = null,page=null,libelletype=null,libellemode=null,libellebailleur;
    private Textbox txtRechercherService,txtRechercherType,txtRechercherMode,txtRechercherBailleur,txtChapitre;
	SygService service=null;
	SygTypesmarches typemarche=null;
	SygModepassation modepassation=null;
	SygBailleurs bailleur=null;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Long idplan,idrealisation=null,idbailleur=null,idservice;
    private Label lblInfos;
    Session session = getHttpSession();
	List<SygRealisationsBailleurs> bailleur_realisation = new ArrayList<SygRealisationsBailleurs>();
	private Decimalbox dcmontant,dcmontantprevu;
	private String codeservice="",codetypemarche="",reference;
	private Listbox lstListe;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	SygBailleurs bailleurs=new SygBailleurs();
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private Listcell cellValiderBailleur;
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private Tab TAB_BAILLEURS;
	private Datebox datepreparationtdrami,dateavisccmpdncmpami,datelancementmanifestation,dateouverturemanifestation,datepreparationdp,dateavisccmpdncmpdp,
	datenonobjectionptf,dateinvitationsoumission,dateouverturedp,datefinevaluation,dateavisccmpdncmpdppt,datenonobjectionptfpt,dateouverture,
	datefinevaluationpf,dateavisccmpdncmp,datenegociation,dateavisptfservcontrole,dateprevsigncontrat,dateprevisionnellesignaturecontrat;
	private Intbox delaiexecution;
	private BigDecimal montantmarche=new BigDecimal(0),montantbailleurs=new BigDecimal(0),montantsaisie=new BigDecimal(0),
			montantdifference=new BigDecimal(0),montantamodifier=new BigDecimal(0),montanttotal=new BigDecimal(0);
	private Menuitem menuValider;
	private Combobox cbexamendncmp,cbexamenccmp;
	
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		

		addEventListener(ApplicationEvents.ON_SERVICES, this);
		lstService.setItemRenderer(new ServiceRenderer());
		pgService.setPageSize(byPageBandbox);
		pgService.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_SERVICES);
		
		
		addEventListener(ApplicationEvents.ON_TYPES, this);
		lstType.setItemRenderer(new TypesRenderer());
		pgType.setPageSize(byPageBandbox);
		pgType.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_TYPES);
		Events.postEvent(ApplicationEvents.ON_TYPES, this, null);
		
		addEventListener(ApplicationEvents.ON_MODES, this);
		lstMode.setItemRenderer(new ModesRenderer());
		pgMode.setPageSize(byPageBandbox);
		pgMode.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODES);
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		pgPagination.setPageSize(byPageBandbox);
		pgPagination.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		lstListe.setItemRenderer(this);
		
		addEventListener(ApplicationEvents.ON_BAILLEURS, this);
		lstBailleur.setItemRenderer(new BailleursRenderer());
		pgBailleur.setPageSize(byPageBandbox);
		pgBailleur.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_BAILLEURS);
	
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		idplan=(Long) session.getAttribute("idplan");
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		plan=BeanLocator.defaultLookup(PlansdepassationSession.class).findById(idplan);
		lblInfos.setValue(plan.getNumplan()+" "+Labels.getLabel("kermel.plansdepassation.infos.gestion")+" "+plan.getAnnee());
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			realisation = BeanLocator.defaultLookup(RealisationsSession.class).findById(code);
			txtLibelle.setValue(realisation.getLibelle());
			service=realisation.getServicemaitreoeuvre();
			bdService.setValue(service.getLibelle());
			codeservice=service.getCodification();
			modepassation=realisation.getModepassation();
			bdMode.setValue(modepassation.getLibelle());
			reference=realisation.getReference();
			txtLibelle.setValue(realisation.getLibelle());
			dcmontant.setValue(realisation.getMontant());
		
			datepreparationtdrami.setValue(realisation.getDatepreparationtdrami());
			dateavisccmpdncmpami.setValue(realisation.getDateavisccmpdncmpami());
			datelancementmanifestation.setValue(realisation.getDatelancementmanifestation());
			dateouverturemanifestation.setValue(realisation.getDateouverturemanifestation());
			datepreparationdp.setValue(realisation.getDatepreparationdp());
			dateavisccmpdncmpdp.setValue(realisation.getDateavisccmpdncmpdp());
			datenonobjectionptf.setValue(realisation.getDatenonobjectionptf());
			dateinvitationsoumission.setValue(realisation.getDateinvitationsoumission());
			dateouverturedp.setValue(realisation.getDateouverturedp());
			datefinevaluation.setValue(realisation.getDatefinevaluation());
			dateavisccmpdncmpdppt.setValue(realisation.getDateavisccmpdncmpdppt());
			datenonobjectionptfpt.setValue(realisation.getDatenonobjectionptfpt());
			dateouverture.setValue(realisation.getDateouverture());
			datefinevaluationpf.setValue(realisation.getDatefinevaluationpf());
			dateavisccmpdncmp.setValue(realisation.getDateavisccmpdncmp());
			datenegociation.setValue(realisation.getDatenegociation());
			dateavisptfservcontrole.setValue(realisation.getDateavisptfservcontrole());
			dateprevisionnellesignaturecontrat.setValue(realisation.getDateprevisionnellesignaturecontrat());
			delaiexecution.setValue(realisation.getDelaiexecution());
			
			idrealisation=code;
			realisations=realisation;
			
			if(realisation.getExamenccmp()==1)
				cbexamenccmp.setValue(Labels.getLabel("kermel.common.list.oui"));
			else
				cbexamenccmp.setValue(Labels.getLabel("kermel.common.list.non"));
			
			if(realisation.getExamendncmp()==1)
				cbexamendncmp.setValue(Labels.getLabel("kermel.common.list.oui"));
			else
				cbexamendncmp.setValue(Labels.getLabel("kermel.common.list.non"));
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			cellValiderBailleur.setVisible(true);
			
		}
		if(infoscompte.getAutorite()!=null)
		{
			autorite=infoscompte.getAutorite();	
		}
		else
		{
			autorite=null;	
		}
		if(infoscompte.getServices()!=null)
		{
			idservice=infoscompte.getServices().getId();	
		}
		else
		{
			idservice=null;
		}
		Events.postEvent(ApplicationEvents.ON_SERVICES, this, null);
		Events.postEvent(ApplicationEvents.ON_BAILLEURS, this, null);
		typemarche=BeanLocator.defaultLookup(TypesmarchesSession.class).findById(UIConstants.PARAM_TMPI);
		bdType.setValue(typemarche.getLibelle());
		codetypemarche=typemarche.getCode();
		bdType.setDisabled(true);
		Events.postEvent(ApplicationEvents.ON_MODES, this, null);
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_SERVICES)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgService.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgService.getActivePage() * byPageBandbox;
				pgService.setPageSize(byPageBandbox);
			}
			List<SygService> services = BeanLocator.defaultLookup(ServiceSession.class).find(activePage, byPageBandbox,null,libelleservice,autorite, idservice);
			lstService.setModel(new SimpleListModel(services));
			pgService.setTotalSize(BeanLocator.defaultLookup(ServiceSession.class).count(null,libelleservice,autorite, idservice));
		}
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_TYPES)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgType.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgType.getActivePage() * byPageBandbox;
				pgType.setPageSize(byPageBandbox);
			}
			List<SygTypesmarches> types = BeanLocator.defaultLookup(TypesmarchesSession.class).find(activePage, byPageBandbox,null,libelletype, UIConstants.PARAM_TMPI);
			lstType.setModel(new SimpleListModel(types));
			pgType.setTotalSize(BeanLocator.defaultLookup(TypesmarchesSession.class).count(null,libelletype, UIConstants.PARAM_TMPI));
		}
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODES)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgMode.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgMode.getActivePage() * byPageBandbox;
				pgMode.setPageSize(byPageBandbox);
			}
			List<SygTypesmarchesmodespassations> modes = BeanLocator.defaultLookup(TypesmarchesmodespassationsSession.class).find(activePage, byPageBandbox,libellemode,typemarche,null);
			lstMode.setModel(new SimpleListModel(modes));
			pgMode.setTotalSize(BeanLocator.defaultLookup(TypesmarchesmodespassationsSession.class).count(libellemode,typemarche,null));
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPageBandbox;
				pgPagination.setPageSize(byPageBandbox);
			}
			 List<SygRealisationsBailleurs> bailleurs = BeanLocator.defaultLookup(RealisationsBailleursSession.class).find(activePage,byPageBandbox,null,realisations,null);
			 lstListe.setModel(new SimpleListModel(bailleurs));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(RealisationsBailleursSession.class).count(null,realisations,null));
			 montantbailleurs=new BigDecimal(0);
			 for(int i=0;i<bailleurs.size();i++)
			 {
				 montantbailleurs=montantbailleurs.add(bailleurs.get(i).getMontant());
			 }
			} 
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_BAILLEURS)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgBailleur.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgBailleur.getActivePage() * byPageBandbox;
				pgBailleur.setPageSize(byPageBandbox);
			}
			List<SygBailleurs> bailleurs = BeanLocator.defaultLookup(BailleursSession.class).find(activePage, byPageBandbox,libellebailleur,autorite);
			lstBailleur.setModel(new SimpleListModel(bailleurs));
			pgBailleur.setTotalSize(BeanLocator.defaultLookup(BailleursSession.class).count(libellebailleur,autorite));
		}
		else 	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
			Listcell button = (Listcell) event.getTarget();
			String toDo = (String) button.getAttribute(UIConstants.TODO);
			sources = (SygRealisationsBailleurs) button.getAttribute("sources");
			if (toDo.equalsIgnoreCase("delete"))
			{
				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, sources);
				showMessageBox(display, map);
			}
			else 
			{
				if (toDo.equalsIgnoreCase("modifier")) {
					idbailleur=sources.getId();
					bailleur=sources.getBailleurs();
					bdBailleur.setValue(bailleur.getLibelle());
		   			dcmontantprevu.setValue(sources.getMontant());
		   			txtChapitre.setValue(sources.getChapitre());
					}
				
			}
			
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			
			BeanLocator.defaultLookup(RealisationsBailleursSession.class).delete(sources.getId());
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			effacerform();
		}

	}
	 public void  modifierrealisation(){
	    	SygRealisations newrealisation=new SygRealisations();
	    	newrealisation.setReference(reference);
	    	newrealisation.setLibelle(txtLibelle.getValue());
	    	newrealisation.setMontant(dcmontant.getValue());
	    	newrealisation.setServicemaitreoeuvre(service);
			newrealisation.setTypemarche(typemarche);
			newrealisation.setModepassation(modepassation);
			newrealisation.setDatepreparationtdrami(datepreparationtdrami.getValue());
			newrealisation.setDateavisccmpdncmpami(dateavisccmpdncmpami.getValue());
			newrealisation.setDatelancementmanifestation(datelancementmanifestation.getValue());
			newrealisation.setDateouverturemanifestation(dateouverturemanifestation.getValue());
			newrealisation.setDatepreparationdp(datepreparationdp.getValue());
			newrealisation.setDateavisccmpdncmpdp(dateavisccmpdncmpdp.getValue());
			newrealisation.setDatenonobjectionptf(datenonobjectionptf.getValue());
			newrealisation.setDateinvitationsoumission(dateinvitationsoumission.getValue());
			newrealisation.setDateouverturedp(dateouverturedp.getValue());
			newrealisation.setDatefinevaluation(datefinevaluation.getValue());
			newrealisation.setDateavisccmpdncmpdppt(dateavisccmpdncmpdppt.getValue());
			newrealisation.setDatenonobjectionptfpt(datenonobjectionptfpt.getValue());
			newrealisation.setDateouverture(dateouverture.getValue());
			newrealisation.setDatefinevaluationpf(datefinevaluationpf.getValue());
			newrealisation.setDateavisccmpdncmp(dateavisccmpdncmp.getValue());
			newrealisation.setDatenegociation(datenegociation.getValue());
			newrealisation.setDateavisptfservcontrole(dateavisptfservcontrole.getValue());
			newrealisation.setDateprevisionnellesignaturecontrat(dateprevisionnellesignaturecontrat.getValue());
			newrealisation.setPlan(plan);
			newrealisation.setAppel(UIConstants.NPARENT);
			newrealisation.setDelaiexecution(delaiexecution.getValue());
			newrealisation.setRealisationid(0L);
			newrealisation.setSupprime(1);
			newrealisation.setEtat("M"+plan.getVersion());
			if(cbexamenccmp.getSelectedItem().getId().equals("cbexamenccmpoui"))
			  newrealisation.setExamenccmp(1);
			else
			  newrealisation.setExamenccmp(0);
					
			if(cbexamendncmp.getSelectedItem().getId().equals("cbexamendncmpoui"))
			  newrealisation.setExamendncmp(1);
			else
			  newrealisation.setExamendncmp(0);
			
			realisations=BeanLocator.defaultLookup(RealisationsSession.class).save(newrealisation);
			idrealisation=realisations.getIdrealisation();
			BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_REALSATIONS", Labels.getLabel("kermel.plansdepassation.realisation.ajouter")+" :" + new Date(), login);
			TAB_BAILLEURS.setSelected(true);
			Events.postEvent(ApplicationEvents.ON_BAILLEURS, this, null);
			realisation.setEtat("U"+plan.getVersion());
			BeanLocator.defaultLookup(RealisationsSession.class).update(realisation);
	 }
	 public void  modifiermajrealisation(){
		    realisation.setReference(reference);
			realisation.setLibelle(txtLibelle.getValue());
			realisation.setMontant(dcmontant.getValue());
			realisation.setServicemaitreoeuvre(service);
			realisation.setTypemarche(typemarche);
			realisation.setModepassation(modepassation);
			realisation.setDatepreparationtdrami(datepreparationtdrami.getValue());
			realisation.setDateavisccmpdncmpami(dateavisccmpdncmpami.getValue());
			realisation.setDatelancementmanifestation(datelancementmanifestation.getValue());
			realisation.setDateouverturemanifestation(dateouverturemanifestation.getValue());
			realisation.setDatepreparationdp(datepreparationdp.getValue());
			realisation.setDateavisccmpdncmpdp(dateavisccmpdncmpdp.getValue());
			realisation.setDatenonobjectionptf(datenonobjectionptf.getValue());
			realisation.setDateinvitationsoumission(dateinvitationsoumission.getValue());
			realisation.setDateouverturedp(dateouverturedp.getValue());
			realisation.setDatefinevaluation(datefinevaluation.getValue());
			realisation.setDateavisccmpdncmpdppt(dateavisccmpdncmpdppt.getValue());
			realisation.setDatenonobjectionptfpt(datenonobjectionptfpt.getValue());
			realisation.setDateouverture(dateouverture.getValue());
			realisation.setDatefinevaluationpf(datefinevaluationpf.getValue());
			realisation.setDateavisccmpdncmp(dateavisccmpdncmp.getValue());
			realisation.setDatenegociation(datenegociation.getValue());
			realisation.setDateavisptfservcontrole(dateavisptfservcontrole.getValue());
			realisation.setDateprevisionnellesignaturecontrat(dateprevisionnellesignaturecontrat.getValue());
			realisation.setPlan(plan);
			realisation.setAppel(UIConstants.NPARENT);
			realisation.setDelaiexecution(delaiexecution.getValue());
			if(cbexamenccmp.getSelectedItem().getId().equals("cbexamenccmpoui"))
			  realisation.setExamenccmp(1);
			else
			 realisation.setExamenccmp(0);
							
			if(cbexamendncmp.getSelectedItem().getId().equals("cbexamendncmpoui"))
			 realisation.setExamendncmp(1);
			else
			realisation.setExamendncmp(0);
			BeanLocator.defaultLookup(RealisationsSession.class).update(realisation);
	 }
	 public void  ajoutermajrealisation(){
		 realisation.setReference(reference);
			realisation.setLibelle(txtLibelle.getValue());
			realisation.setMontant(dcmontant.getValue());
			realisation.setServicemaitreoeuvre(service);
			realisation.setTypemarche(typemarche);
			realisation.setModepassation(modepassation);
			realisation.setDatepreparationtdrami(datepreparationtdrami.getValue());
			realisation.setDateavisccmpdncmpami(dateavisccmpdncmpami.getValue());
			realisation.setDatelancementmanifestation(datelancementmanifestation.getValue());
			realisation.setDateouverturemanifestation(dateouverturemanifestation.getValue());
			realisation.setDatepreparationdp(datepreparationdp.getValue());
			realisation.setDateavisccmpdncmpdp(dateavisccmpdncmpdp.getValue());
			realisation.setDatenonobjectionptf(datenonobjectionptf.getValue());
			realisation.setDateinvitationsoumission(dateinvitationsoumission.getValue());
			realisation.setDateouverturedp(dateouverturedp.getValue());
			realisation.setDatefinevaluation(datefinevaluation.getValue());
			realisation.setDateavisccmpdncmpdppt(dateavisccmpdncmpdppt.getValue());
			realisation.setDatenonobjectionptfpt(datenonobjectionptfpt.getValue());
			realisation.setDateouverture(dateouverture.getValue());
			realisation.setDatefinevaluationpf(datefinevaluationpf.getValue());
			realisation.setDateavisccmpdncmp(dateavisccmpdncmp.getValue());
			realisation.setDatenegociation(datenegociation.getValue());
			realisation.setDateavisptfservcontrole(dateavisptfservcontrole.getValue());
			realisation.setDateprevisionnellesignaturecontrat(dateprevisionnellesignaturecontrat.getValue());
			realisation.setPlan(plan);
			realisation.setAppel(UIConstants.NPARENT);
			realisation.setDelaiexecution(delaiexecution.getValue());
			realisation.setRealisationid(0L);
			realisation.setSupprime(1);
			realisation.setEtat("A"+plan.getVersion());
			if(cbexamenccmp.getSelectedItem().getId().equals("cbexamenccmpoui"))
			  realisation.setExamenccmp(1);
			else
			  realisation.setExamenccmp(0);
						
			if(cbexamendncmp.getSelectedItem().getId().equals("cbexamendncmpoui"))
			  realisation.setExamendncmp(1);
			else
			  realisation.setExamendncmp(0);
			
			realisations=BeanLocator.defaultLookup(RealisationsSession.class).save(realisation);
			idrealisation=realisations.getIdrealisation();
			TAB_BAILLEURS.setSelected(true);
			menuValider.setDisabled(true);
			Events.postEvent(ApplicationEvents.ON_BAILLEURS, this, null);
	 }
	public void onOK() throws InterruptedException {
		if(checkFieldConstraints())
		{
			
			if (realisations==null) {
				ajoutermajrealisation();
				Messagebox.show(Labels.getLabel("kermel.plansdepassation.realisation.enregistre"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			} 
			else  {
				if(realisations.getRealisationid()==0L)
					 modifiermajrealisation();
				else
					modifierrealisation();
					
			}
			cellValiderBailleur.setVisible(true);
	     	lbStatusBar.setValue("");
		}
	}
	
	public void onSelect$lstListe(){
		sources = (SygRealisationsBailleurs) lstListe.getSelectedItem().getValue();
		idbailleur=sources.getId();
		bailleur=sources.getBailleurs();
		bdBailleur.setValue(bailleur.getLibelle());
		dcmontantprevu.setValue(sources.getMontant());
		txtChapitre.setValue(sources.getChapitre());
		montantamodifier=sources.getMontant();
	}
	public void onClick$menuFermer()
	{
		session.setAttribute("LibelleTab","REALPI");
		loadApplicationState("realisations_maj");
		detach();
	}
	
private boolean checkFieldConstraints() {
		
		try {
			if(typemarche==null)
		     {
            errorComponent = bdType;
            errorMsg = Labels.getLabel("kermel.referentiel.typesmarche")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(service==null)
		     {
             errorComponent = bdService;
             errorMsg = Labels.getLabel("kermel.plansdepassation.service.maitredoeuvre")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtLibelle.getValue().equals(""))
		     {
               errorComponent = txtLibelle;
               errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.description.travaux")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }

			if(modepassation==null)
		     {
             errorComponent = bdMode;
             errorMsg = Labels.getLabel("kermel.referentiel.modepassation")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dcmontant.getValue()==null)
		     {
              errorComponent = dcmontant;
              errorMsg = Labels.getLabel("kermel.plansdepassation.realisations.montant")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		
			
			if(datepreparationtdrami.getValue()==null)
		     {
              errorComponent = datepreparationtdrami;
              errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.datepreparationtdrami")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dateavisccmpdncmpami.getValue()==null)
		     {
              errorComponent = dateavisccmpdncmpami;
              errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.dateavisccmpdncmpami")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
//			if((dtlancement.getValue()).after(dtattribution.getValue()))
//			 {
//				errorComponent = dtlancement;
//				errorMsg =Labels.getLabel("kermel.plansdepassation.realisations.lancementdate")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.plansdepassation.realisations.attributiondate")+": "+UtilVue.getInstance().formateLaDate(dtattribution.getValue());
//				lbStatusBar.setStyle(ERROR_MSG_STYLE);
//				lbStatusBar.setValue(errorMsg);
//				throw new WrongValueException (errorComponent, errorMsg);
//			  }
			if(datelancementmanifestation.getValue()==null)
		     {
               errorComponent = datelancementmanifestation;
               errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.datelancementmanifestation")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
//			if((dtlancement.getValue()).after(dtdemarrage.getValue()))
//			 {
//				errorComponent = dtdemarrage;
//				errorMsg =Labels.getLabel("kermel.plansdepassation.realisations.attributiondate")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.plansdepassation.realisations.demarragedate")+": "+UtilVue.getInstance().formateLaDate(dtdemarrage.getValue());
//				lbStatusBar.setStyle(ERROR_MSG_STYLE);
//				lbStatusBar.setValue(errorMsg);
//				throw new WrongValueException (errorComponent, errorMsg);
//			  }
			if(dateouverturemanifestation.getValue()==null)
		     {
               errorComponent = dateouverturemanifestation;
               errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.dateouverturemanifestation")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
//			if((dtdemarrage.getValue()).after(dtachevement.getValue()))
//			 {
//				errorComponent = dtlancement;
//				errorMsg =Labels.getLabel("kermel.plansdepassation.realisations.demarragedate")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.plansdepassation.realisations.achevementdate")+": "+UtilVue.getInstance().formateLaDate(dtachevement.getValue());
//				lbStatusBar.setStyle(ERROR_MSG_STYLE);
//				lbStatusBar.setValue(errorMsg);
//				throw new WrongValueException (errorComponent, errorMsg);
//			  }
			if(datepreparationdp.getValue()==null)
		     {
              errorComponent = datepreparationdp;
              errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.datelisterestreintepreparationdp")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dateavisccmpdncmpdp.getValue()==null)
		     {
             errorComponent = dateavisccmpdncmpdp;
             errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.dateavisccmpdncmpdp")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(datenonobjectionptf.getValue()==null)
		     {
            errorComponent = datenonobjectionptf;
            errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.datenonobjectionptf")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dateinvitationsoumission.getValue()==null)
		     {
           errorComponent = dateinvitationsoumission;
           errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.dateinvitationsoumission")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dateouverturedp.getValue()==null)
		     {
          errorComponent = dateouverturedp;
          errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.dateouvertureproposition")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(datefinevaluation.getValue()==null)
		     {
         errorComponent = datefinevaluation;
         errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.datefinevaluation")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dateavisccmpdncmpdppt.getValue()==null)
		     {
        errorComponent = dateavisccmpdncmpdppt;
        errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.dateavisccmpdncmpdp")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(datenonobjectionptfpt.getValue()==null)
		     {
       errorComponent = datenonobjectionptfpt;
       errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.datenonobjectionptf")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dateouverture.getValue()==null)
		     {
      errorComponent = dateouverture;
      errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.dateouverture")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(datefinevaluationpf.getValue()==null)
		     {
     errorComponent = datefinevaluationpf;
     errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.datefinevaluation")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dateavisccmpdncmp.getValue()==null)
		     {
    errorComponent = dateavisccmpdncmp;
    errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.dateavisccmpdncmp")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(datenegociation.getValue()==null)
		     {
   errorComponent = datenegociation;
   errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.datenegociation")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dateavisptfservcontrole.getValue()==null)
		     {
  errorComponent = dateavisptfservcontrole;
  errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.dateavisptfservicecontrole")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dateprevisionnellesignaturecontrat.getValue()==null)
		     {
 errorComponent = dateprevisionnellesignaturecontrat;
 errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.dateprevisionnellesignaturecontrat")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(delaiexecution.getValue()==null)
		     {
        errorComponent = delaiexecution;
        errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.delaiexecution")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	

	///////////Service///////// 
	public void onSelect$lstService(){
	service= (SygService) lstService.getSelectedItem().getValue();
	codeservice=service.getCodification();
	bdService.setValue(service.getLibelle());
	codeReference(codetypemarche,codeservice);
	bdService.close();
	
	}

public class ServiceRenderer implements ListitemRenderer{
	


	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygService service = (SygService) data;
		item.setValue(service);
		
		
		Listcell cellLibelle = new Listcell("");
		if (service.getLibelle()!=null){
			cellLibelle.setLabel(service.getLibelle());
		}
		cellLibelle.setParent(item);

	}
}
	public void onFocus$txtRechercherService(){
	if(txtRechercherService.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.service.maitredoeuvre"))){
		txtRechercherService.setValue("");
	}		 
	}
	
	public void  onClick$btnRechercherService(){
	if(txtRechercherService.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.service.maitredoeuvre")) || txtRechercherService.getValue().equals("")){
		libelleservice = null;
		page=null;
	}else{
		libelleservice = txtRechercherService.getValue();
		page="0";
	}
	Events.postEvent(ApplicationEvents.ON_SERVICES, this, page);
	}
	

	///////////Type de marche///////// 
	public void onSelect$lstType(){
	typemarche= (SygTypesmarches) lstType.getSelectedItem().getValue();
	codetypemarche=typemarche.getCode();
	bdType.setValue(typemarche.getLibelle());
	codeReference(codetypemarche,codeservice);
	bdType.close();
	
	}

public class TypesRenderer implements ListitemRenderer{
	


	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygTypesmarches types = (SygTypesmarches) data;
		item.setValue(types);
		
		
		Listcell cellLibelle = new Listcell("");
		if (types.getLibelle()!=null){
			cellLibelle.setLabel(types.getLibelle());
		}
		cellLibelle.setParent(item);

	}
}

public void onOK$txtRechercherType(){
	 onClick$btnRechercherType();
}

	public void onFocus$txtRechercherType(){
	if(txtRechercherType.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.typesmarche"))){
		txtRechercherType.setValue("");
	}		 
	}
	
	public void  onClick$btnRechercherType(){
	if(txtRechercherType.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.typesmarche")) || txtRechercherType.getValue().equals("")){
		libelletype = null;
		page=null;
	}else{
		libelletype = txtRechercherType.getValue();
		page="0";
	}
	Events.postEvent(ApplicationEvents.ON_TYPES, this, page);
	}
	
	
		///////////Mode de passation///////// 
		public void onSelect$lstMode(){
		modepassation= (SygModepassation) lstMode.getSelectedItem().getValue();
		bdMode.setValue(modepassation.getLibelle());
		Dates();
		bdMode.close();
		
		}
		
		public class ModesRenderer implements ListitemRenderer{
		
		
		
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypesmarchesmodespassations modes = (SygTypesmarchesmodespassations) data;
			item.setValue(modes.getMode());
			
			
			Listcell cellLibelle = new Listcell("");
			if (modes.getMode().getLibelle()!=null){
				cellLibelle.setLabel(modes.getMode().getLibelle());
			}
			cellLibelle.setParent(item);
		
		}
		}
		public void onFocus$txtRechercherMode(){
		if(txtRechercherMode.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.modepassation"))){
			txtRechercherMode.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherMode(){
		if(txtRechercherMode.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.modepassation")) || txtRechercherMode.getValue().equals("")){
			libellemode = null;
			page=null;
		}else{
			libellemode = txtRechercherMode.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_MODES, this, page);
		}
		
		public void codeReference(String codetypemarche,String codeservice){
			if(!codeservice.equals("")&&!codetypemarche.equals(""))
			{
				reference=BeanLocator.defaultLookup(RealisationsSession.class).getGeneratedCode(UIConstants.PARAM_REALISATION, codetypemarche, codeservice);
			}
		}

		@Override
		public void render(final Listitem item, final Object data, int index) throws Exception {
			SygRealisationsBailleurs bailleurs = (SygRealisationsBailleurs) data;
			item.setValue(bailleurs);

			
			 Listcell cellLibelle = new Listcell(bailleurs.getBailleurs().getLibelle());
			 cellLibelle.setParent(item);
			 
			 Listcell cellMontant = new Listcell("");
			 if(bailleurs.getMontant()!=null)
				 cellMontant.setLabel(ToolKermel.format2Decimal(bailleurs.getMontant()));
			 cellMontant.setParent(item);
			
			 Listcell cellChapitre = new Listcell(bailleurs.getChapitre());
			 cellChapitre.setParent(item);
			 
//			 Listcell cellImageModif = new Listcell();
//			 cellImageModif.setImage("/images/disk.png");
//			 cellImageModif.setAttribute(UIConstants.TODO, "modifier");
//			 cellImageModif.setAttribute("sources", bailleurs);
//			 cellImageModif.setTooltiptext("Modifier Bailleur");
//			 cellImageModif.addEventListener(Events.ON_CLICK, RealisationFormController.this);
//			 cellImageModif.setParent(item);
			 
			 Listcell cellImageSupprime = new Listcell();
			 cellImageSupprime.setImage("/images/delete.png");
			 cellImageSupprime.setAttribute(UIConstants.TODO, "delete");
			 cellImageSupprime.setAttribute("sources",bailleurs);
			 cellImageSupprime.setTooltiptext("Supprimer Bailleur");
			 cellImageSupprime.addEventListener(Events.ON_CLICK, RealisationMiseAJourPIFormController270513.this);
			 cellImageSupprime.setParent(item);
//			 SygBailleurs bailleurs = (SygBailleurs) data;
//				item.setValue(bailleurs);
//
//				
//				 Listcell cellLibelle = new Listcell(bailleurs.getLibelle());
//				 cellLibelle.setParent(item);
//				 if(realisations!=null)
//				 {
//					 bailleur_realisation= BeanLocator.defaultLookup(RealisationsBailleursSession.class).find(0,-1,null,realisations,bailleurs);
//					 if(bailleur_realisation.size()>0)
//						 item.setSelected(true);
//				 }
			 
		}
		
		
///////////Bailleurs///////// 
		public void onSelect$lstBailleur(){
		bailleur= (SygBailleurs) lstBailleur.getSelectedItem().getValue();
		bdBailleur.setValue(bailleur.getLibelle());
		bdBailleur.close();
		
		}
		
		public class BailleursRenderer implements ListitemRenderer{
		
		
		
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygBailleurs bailleurs = (SygBailleurs) data;
			item.setValue(bailleurs);
			
			
			Listcell cellLibelle = new Listcell("");
			if (bailleurs.getLibelle()!=null){
				cellLibelle.setLabel(bailleurs.getLibelle());
			}
			cellLibelle.setParent(item);
		
		}
		}
		public void onFocus$txtRechercherBailleur(){
		if(txtRechercherBailleur.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.realisation.bailleur"))){
			txtRechercherBailleur.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherBailleur(){
		if(txtRechercherBailleur.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.realisation.bailleur")) || txtRechercherBailleur.getValue().equals("")){
			libellebailleur = null;
			page=null;
		}else{
			libellebailleur = txtRechercherBailleur.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_BAILLEURS, this, page);
		}
		
		private boolean checkFieldConstraintsBailleurs() {
			
			try {
			
				if(bdBailleur.getValue().equals(""))
			     {
	               errorComponent = bdBailleur;
	               errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.bailleur")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				   lbStatusBar.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar.setValue(errorMsg);
				  throw new WrongValueException (errorComponent, errorMsg);
			     }
				 bailleur_realisation= BeanLocator.defaultLookup(RealisationsBailleursSession.class).find(0,-1,null,realisations,bailleur);
					if (idbailleur!=null)
					{
						if(sources.getBailleurs().getId().intValue()!=bailleur.getId().intValue())
						{
							 if(bailleur_realisation.size()>0)
						     {
				                errorComponent = bdBailleur;
								errorMsg =Labels.getLabel("kermel.plansdepassation.realisation.bailleur")+" "+ Labels.getLabel("kermel.referentiel.existe")+": "+bdBailleur.getValue();
								lbStatusBar.setStyle(ERROR_MSG_STYLE);
								lbStatusBar.setValue(errorMsg);
								throw new WrongValueException (errorComponent, errorMsg);
						    }
						}
					 
					}
					else
					{
						  if(bailleur_realisation.size()>0)
						  {
							errorComponent = bdBailleur;
							errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.bailleur")+" "+Labels.getLabel("kermel.referentiel.existe")+" :"+bdBailleur.getValue();
							lbStatusBar.setStyle(ERROR_MSG_STYLE);
							lbStatusBar.setValue(errorMsg);
							throw new WrongValueException (errorComponent, errorMsg);
						  }
					}
				if(dcmontantprevu.getValue()==null)
			     {
	              errorComponent = dcmontantprevu;
	              errorMsg = Labels.getLabel("kermel.plansdepassation.realisations.sourcesfinancement.montant")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				   lbStatusBar.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar.setValue(errorMsg);
				  throw new WrongValueException (errorComponent, errorMsg);
			     }
				if(idbailleur==null)
	   			{
					if(montantbailleurs.intValue()==0)
						montantmarche=dcmontant.getValue();
					montanttotal=montantbailleurs.add(dcmontantprevu.getValue());
					if(montanttotal.compareTo(montantmarche)==1)
					{
						 errorComponent = dcmontantprevu;
			              errorMsg = Labels.getLabel("kermel.plansdepassation.realisations.sourcesfinancement.montant.somme")+": "+ToolKermel.format2Decimal(montantmarche);
						   lbStatusBar.setStyle(ERROR_MSG_STYLE);
						   lbStatusBar.setValue(errorMsg);
						  throw new WrongValueException (errorComponent, errorMsg);
					}
	   			}
				else
				{
					montantsaisie=dcmontantprevu.getValue();
					if(montantsaisie.compareTo(montantamodifier)==1)
					{
						montantdifference=montantsaisie.subtract(montantamodifier);
						montanttotal=montantbailleurs.add(montantdifference);
						if(montanttotal.compareTo(montantmarche)==1)
						{
							 errorComponent = dcmontantprevu;
				              errorMsg = Labels.getLabel("kermel.plansdepassation.realisations.sourcesfinancement.montant.somme")+": "+ToolKermel.format2Decimal(montantmarche);
							   lbStatusBar.setStyle(ERROR_MSG_STYLE);
							   lbStatusBar.setValue(errorMsg);
							  throw new WrongValueException (errorComponent, errorMsg);
						}
					}
					
				}
				return true;
					
			}
			catch (Exception e) {
				errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
				+ " [checkFieldConstraints]";
				errorComponent = null;
				return false;

				
			}
			
		}
		
		public void  effacerform(){
			dcmontantprevu.setValue(new BigDecimal("0"));
   			txtChapitre.setValue("");
   			idbailleur=null;
   			bdBailleur.setValue("");
		}
		public void  onClick$cellValiderBailleur(){
			if(checkFieldConstraintsBailleurs())
			{
				 
	   			sources.setBailleurs(bailleur);
	   			sources.setRealisations(realisations);
	   			sources.setMontant(dcmontantprevu.getValue());
	   			sources.setChapitre(txtChapitre.getValue());
	   			if(idbailleur==null)
	   			{
	   			 BeanLocator.defaultLookup(RealisationsBailleursSession.class).save(sources);
	   		
	   			}
	   			else
	   			{
	   			 BeanLocator.defaultLookup(RealisationsBailleursSession.class).update(sources);
	   		
	   			}
	   			effacerform();
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
				Events.postEvent(ApplicationEvents.ON_BAILLEURS, this, null);
				lbStatusBar.setValue("");
			}
			
		}
		
		public void onOK$txtRechercherMode(){
			onClick$btnRechercherMode();
		}
		
		
		public void onOK$txtRechercherService(){
			onClick$btnRechercherService();
		}
		
	
		public void Dates(){
			if(!bdMode.getValue().equals("")&&!cbexamendncmp.getValue().equals("")&&!cbexamenccmp.getValue().equals("")&&datepreparationtdrami.getValue()!=null)
			{
				if(modepassation.getId().intValue()==42&&cbexamendncmp.getSelectedItem().getId().equals("cbexamendncmpnon")
						&&cbexamenccmp.getSelectedItem().getId().equals("cbexamenccmpoui"))
				{
					dateavisccmpdncmpami.setValue(ToolKermel.getCourantEch3(datepreparationtdrami.getValue(), 7));
					datelancementmanifestation.setValue(ToolKermel.getCourantEch3(dateavisccmpdncmpami.getValue(), 7));
					dateouverturemanifestation.setValue(ToolKermel.getCourantEch3(datelancementmanifestation.getValue(), 30));
					datepreparationdp.setValue(ToolKermel.getCourantEch3(dateouverturemanifestation.getValue(), 30));
					dateavisccmpdncmpdp.setValue(ToolKermel.getCourantEch3(datepreparationdp.getValue(), 7));
					dateinvitationsoumission.setValue(ToolKermel.getCourantEch3(dateavisccmpdncmpdp.getValue(), 7));
					dateouverturedp.setValue(ToolKermel.getCourantEch3(dateinvitationsoumission.getValue(), 30));
					datefinevaluation.setValue(ToolKermel.getCourantEch3(dateouverturedp.getValue(), 21));
					dateavisccmpdncmpdppt.setValue(ToolKermel.getCourantEch3(datefinevaluation.getValue(), 7));
					dateouverture.setValue(ToolKermel.getCourantEch3(dateavisccmpdncmpdppt.getValue(), 7));
					datefinevaluationpf.setValue(ToolKermel.getCourantEch3(dateouverture.getValue(),9));
					dateavisccmpdncmp.setValue(ToolKermel.getCourantEch3(datefinevaluationpf.getValue(),7));
					datenegociation.setValue(ToolKermel.getCourantEch3(dateavisccmpdncmp.getValue(),7));
					dateprevisionnellesignaturecontrat.setValue(ToolKermel.getCourantEch3(datenegociation.getValue(),21));
					
					dateavisccmpdncmpami.setReadonly(true);
					datelancementmanifestation.setReadonly(true);
					dateouverturemanifestation.setReadonly(true);
					datepreparationdp.setReadonly(true);
					dateavisccmpdncmpdp.setReadonly(true);
					dateavisccmpdncmpdppt.setReadonly(true);
					dateavisccmpdncmp.setReadonly(true);
					
				}
				else
				{
					if(modepassation.getId().intValue()==42&&cbexamendncmp.getSelectedItem().getId().equals("cbexamendncmpoui")
							&&cbexamenccmp.getSelectedItem().getId().equals("cbexamenccmpnon"))
					{
						dateavisccmpdncmpami.setValue(ToolKermel.getCourantEch3(datepreparationtdrami.getValue(), 15));
						datelancementmanifestation.setValue(ToolKermel.getCourantEch3(dateavisccmpdncmpami.getValue(), 7));
						dateouverturemanifestation.setValue(ToolKermel.getCourantEch3(datelancementmanifestation.getValue(), 30));
						datepreparationdp.setValue(ToolKermel.getCourantEch3(dateouverturemanifestation.getValue(), 30));
						dateavisccmpdncmpdp.setValue(ToolKermel.getCourantEch3(datepreparationdp.getValue(), 15));
						dateinvitationsoumission.setValue(ToolKermel.getCourantEch3(dateavisccmpdncmpdp.getValue(), 7));
						dateouverturedp.setValue(ToolKermel.getCourantEch3(dateinvitationsoumission.getValue(), 30));
						datefinevaluation.setValue(ToolKermel.getCourantEch3(dateouverturedp.getValue(), 21));
						dateavisccmpdncmpdppt.setValue(ToolKermel.getCourantEch3(datefinevaluation.getValue(), 15));
						dateouverture.setValue(ToolKermel.getCourantEch3(dateavisccmpdncmpdppt.getValue(), 7));
						datefinevaluationpf.setValue(ToolKermel.getCourantEch3(dateouverture.getValue(),9));
						dateavisccmpdncmp.setValue(ToolKermel.getCourantEch3(datefinevaluationpf.getValue(),15));
						datenegociation.setValue(ToolKermel.getCourantEch3(dateavisccmpdncmp.getValue(),7));
						dateprevisionnellesignaturecontrat.setValue(ToolKermel.getCourantEch3(datenegociation.getValue(),21));
						
						dateavisccmpdncmpami.setReadonly(true);
						datelancementmanifestation.setReadonly(true);
						dateouverturemanifestation.setReadonly(true);
						datepreparationdp.setReadonly(true);
						dateavisccmpdncmpdp.setReadonly(true);
						dateavisccmpdncmpdppt.setReadonly(true);
						dateavisccmpdncmp.setReadonly(true);
					}
					else
					{
						if(modepassation.getId().intValue()==34&&cbexamendncmp.getSelectedItem().getId().equals("cbexamendncmpnon")
								&&cbexamenccmp.getSelectedItem().getId().equals("cbexamenccmpnon"))
						{
							dateavisccmpdncmpami.setReadonly(false);
							datelancementmanifestation.setReadonly(false);
							dateouverturemanifestation.setReadonly(false);
							datepreparationdp.setReadonly(false);
							dateavisccmpdncmpdp.setReadonly(false);
							
							dateinvitationsoumission.setValue(ToolKermel.getCourantEch3(datepreparationtdrami.getValue(), 3));
							dateouverturedp.setValue(ToolKermel.getCourantEch3(dateinvitationsoumission.getValue(), 7));
							datefinevaluation.setValue(ToolKermel.getCourantEch3(dateouverturedp.getValue(), 4));
							
							dateavisccmpdncmpdppt.setReadonly(false);
							
							dateouverture.setValue(ToolKermel.getCourantEch3(datefinevaluation.getValue(), 5));
							datefinevaluationpf.setValue(ToolKermel.getCourantEch3(dateouverture.getValue(),3));
							
							dateavisccmpdncmp.setReadonly(false);
							
							datenegociation.setValue(ToolKermel.getCourantEch3(datefinevaluationpf.getValue(),7));
							dateprevisionnellesignaturecontrat.setValue(ToolKermel.getCourantEch3(datenegociation.getValue(),7));
						}
						else
						{
							dateavisccmpdncmpami.setValue(null);
							datelancementmanifestation.setValue(null);
							dateouverturemanifestation.setValue(null);
							datepreparationdp.setValue(null);
							dateavisccmpdncmpdp.setValue(null);
							dateinvitationsoumission.setValue(null);
							dateouverturedp.setValue(null);
							datefinevaluation.setValue(null);
							dateavisccmpdncmpdppt.setValue(null);
							dateouverture.setValue(null);
							datefinevaluationpf.setValue(null);
							dateavisccmpdncmp.setValue(null);
							datenegociation.setValue(null);
							dateprevisionnellesignaturecontrat.setValue(null);
							
							dateavisccmpdncmpami.setReadonly(false);
							datelancementmanifestation.setReadonly(false);
							dateouverturemanifestation.setReadonly(false);
							datepreparationdp.setReadonly(false);
							dateavisccmpdncmpdp.setReadonly(false);
							dateinvitationsoumission.setReadonly(false);
							dateouverturedp.setReadonly(false);
							datefinevaluation.setReadonly(false);
							dateavisccmpdncmpdppt.setReadonly(false);
							dateouverture.setReadonly(false);
							datefinevaluationpf.setReadonly(false);
							dateavisccmpdncmp.setReadonly(false);
							datenegociation.setReadonly(false);
							dateprevisionnellesignaturecontrat.setReadonly(false);
						}
					}
				}
			
				
			}
			
		}
		public void  onChange$datepreparationtdrami(){
			Dates();
		}
		
		public void onSelect$cbexamendncmp(){
			Dates();
		}
		
        public void onSelect$cbexamenccmp(){
        	Dates();
		}	
}
