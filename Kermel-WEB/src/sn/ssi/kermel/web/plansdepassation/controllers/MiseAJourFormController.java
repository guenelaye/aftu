package sn.ssi.kermel.web.plansdepassation.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.plansdepassation.ejb.PlansdepassationSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class MiseAJourFormController extends AbstractWindow implements AfterCompose {

	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	public static final String WINDOW_PARAM_USER_ID = "USER_ID";
	
	
	String mode;
	long usrId = -1;
	Utilisateur user;
	private Textbox  txtMotif, txtNumeros;
	private int numeroplan;
	private SygAutoriteContractante autorite;
	private Label lbStatusBar,lblPlans;
	private Component errorComponent;
	private String errorMsg,login;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Utilisateur infoscompte;
	private Label lblPlan;
	String numero=null,prenom=null,reference,callBack,fonction=null,service=null,page=null,numplan;
	SygPlansdepassation plans=new SygPlansdepassation(); 
	SygPlansdepassation plan=new SygPlansdepassation(); 
	SygPlansdepassation planamodifier=new SygPlansdepassation(); 
	SygPlansdepassation plangeneree=new SygPlansdepassation(); 
	List<SygPlansdepassation> listesplans = new ArrayList<SygPlansdepassation>();
	SygPlansdepassation infosplan=new SygPlansdepassation(); 
	List<SygRealisations> realisations = new ArrayList<SygRealisations>();
	SygRealisations realisation=new SygRealisations(); 
	private Long idplan,idplanamodifier;
	Session session = getHttpSession();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	
			
	}

	@SuppressWarnings("unchecked")
	public void onCreate(final CreateEvent createEvent) {
		final Map windowParams = createEvent.getArg();
		mode = (String) windowParams.get(WINDOW_PARAM_MODE);
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		login = ((String) getHttpSession().getAttribute("user"));
		if(infoscompte.getAutorite()!=null)
		{
			autorite=infoscompte.getAutorite();	
		}
		else
		{
			autorite=null;	  
		}
		idplan=(Long) session.getAttribute("idplans");
		planamodifier=BeanLocator.defaultLookup(PlansdepassationSession.class).findById(idplan);
		if(mode.equals(UIConstants.MODE_EDIT))
		{
			idplanamodifier=(Long) windowParams.get(WINDOW_PARAM_CODE);
			plan=BeanLocator.defaultLookup(PlansdepassationSession.class).findById(idplanamodifier);
			numplan=plan.getNumplan();
			txtMotif.setValue(plan.getMotif());
			lblPlan.setValue(plan.getNumplan()+" "+Labels.getLabel("kermel.plansdepassation.infos.gestion")+" "+plan.getAnnee());	
			
		}
		else
		{
			listesplans = BeanLocator.defaultLookup(PlansdepassationSession.class).ListesPlans(0,-1,null,null,null,autorite,planamodifier);
			if(listesplans.size()==0)
				plans=planamodifier;
			else
				plans=listesplans.get(0);
			realisations=BeanLocator.defaultLookup(RealisationsSession.class).RealisationsMAJ(null, null, plans, null, null, null, null, null,"S"+plans.getVersion(),"U"+plans.getVersion());
			numeroplan=listesplans.size()+2;
			numplan=plans.getNumplan().substring(0,  plans.getNumplan().length()-1)+numeroplan;
			lblPlan.setValue(plans.getNumplan()+" "+Labels.getLabel("kermel.plansdepassation.infos.gestion")+" "+plans.getAnnee());	
			
		}
		
		txtNumeros.setValue(numplan);
	}

	

	public void onOK() {
		if(checkFieldConstraints())
		{
			if(mode.equals(UIConstants.MODE_NEW))
			{
				plan.setNumplan(txtNumeros.getValue());
				plan.setAnnee(plans.getAnnee());
				plan.setDatecreation(new Date());
				plan.setStatus(Labels.getLabel("kermel.plansdepassation.statut.saisie"));
				plan.setAutorite(autorite);
				plan.setVersion(numeroplan);
				plan.setPlan(planamodifier);
				plan.setMotif(txtMotif.getValue());
				plan.setStatus(Labels.getLabel("kermel.plansdepassation.statut.saisie"));
				plan.setDateMiseEnValidation(null);
				plan.setDatepublication(null);
				plan.setDateValidation(null);
				plan=BeanLocator.defaultLookup(PlansdepassationSession.class).save(plan);
				BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_PLANPASSATION", Labels.getLabel("kermel.plansdepassation.ajouter")+" :" + UtilVue.getInstance().formateLaDate(new Date()), login);
				plangeneree=BeanLocator.defaultLookup(PlansdepassationSession.class).Plan(-1,autorite,txtNumeros.getValue());
				infosplan=BeanLocator.defaultLookup(PlansdepassationSession.class).Plan(-1, autorite, txtNumeros.getValue());
				for (int i = 0; i < realisations.size(); i++) {
					realisation=realisations.get(i);
					SygRealisations newrealisation=new SygRealisations(); 
					newrealisation.setReference(realisation.getReference());
					newrealisation.setLibelle(realisation.getLibelle());
					newrealisation.setMontant(realisation.getMontant());
					newrealisation.setServicemaitreoeuvre(realisation.getServicemaitreoeuvre());
					newrealisation.setTypemarche(realisation.getTypemarche());
					newrealisation.setModepassation(realisation.getModepassation());
					newrealisation.setNaturemodepassation(realisation.getNaturemodepassation());
					
					newrealisation.setDatelancement(realisation.getDatelancement());
					newrealisation.setDateattribution(realisation.getDateattribution());
					newrealisation.setDatedemarrage(realisation.getDatedemarrage());
					newrealisation.setDateachevement(realisation.getDateachevement());
					newrealisation.setNaturemodepassation(realisation.getNaturemodepassation());
//					if((realisation.getTypemarche().getId().intValue()==UIConstants.PARAM_TMTRAVAUX.intValue())||(realisation.getTypemarche().getId().intValue()==UIConstants.PARAM_TMSERVICES.intValue())
//							||(realisation.getTypemarche().getId().intValue()==UIConstants.PARAM_TMFOURNITURES.intValue())
//							||(realisation.getTypemarche().getId().intValue()==UIConstants.PARAM_TMDSP.intValue()))
//					{
//						newrealisation.setDatepreparationdaodcrbc(realisation.getDatepreparationdaodcrbc());
//						newrealisation.setDatereceptionavisccmpdncmpappel(realisation.getDatereceptionavisccmpdncmpappel());
//						newrealisation.setDatenonobjectionptfappel(realisation.getDatenonobjectionptfappel());
//						newrealisation.setDateinvitationsoumission(realisation.getDateinvitationsoumission());
//						newrealisation.setDateouvertureplis(realisation.getDateouvertureplis());
//						newrealisation.setDatefinevaluation(realisation.getDatefinevaluation());
//						newrealisation.setDatereceptionavisccmpdncmp(realisation.getDatereceptionavisccmpdncmp());
//						newrealisation.setDatenonobjectionptf(realisation.getDatenonobjectionptf());
//						newrealisation.setDateprevisionnellesignaturecontrat(realisation.getDateprevisionnellesignaturecontrat());
//					}
//					else
//					{
//						newrealisation.setDatepreparationtdrami(realisation.getDatepreparationtdrami());
//						newrealisation.setDateavisccmpdncmpami(realisation.getDateavisccmpdncmpami());
//						newrealisation.setDatelancementmanifestation(realisation.getDatelancementmanifestation());
//						newrealisation.setDateouverturemanifestation(realisation.getDateouverturemanifestation());
//						newrealisation.setDatepreparationdp(realisation.getDatepreparationdp());
//						newrealisation.setDateavisccmpdncmpdp(realisation.getDateavisccmpdncmpdp());
//						newrealisation.setDatenonobjectionptf(realisation.getDatenonobjectionptf());
//						newrealisation.setDateinvitationsoumission(realisation.getDateinvitationsoumission());
//						newrealisation.setDateouverturedp(realisation.getDateouverturedp());
//						newrealisation.setDatefinevaluation(realisation.getDatefinevaluation());
//						newrealisation.setDateavisccmpdncmpdppt(realisation.getDateavisccmpdncmpdppt());
//						newrealisation.setDatenonobjectionptfpt(realisation.getDatenonobjectionptfpt());
//						newrealisation.setDateouverture(realisation.getDateouverture());
//						newrealisation.setDatefinevaluationpf(realisation.getDatefinevaluationpf());
//						newrealisation.setDateavisccmpdncmp(realisation.getDateavisccmpdncmp());
//						newrealisation.setDatenegociation(realisation.getDatenegociation());
//						newrealisation.setDateavisptfservcontrole(realisation.getDateavisptfservcontrole());
//						newrealisation.setDateprevisionnellesignaturecontrat(realisation.getDateprevisionnellesignaturecontrat());
//					}
					newrealisation.setRealisationid(realisation.getIdrealisation());
					newrealisation.setSupprime(realisation.getSupprime());
				//	newrealisation.setEtat(realisation.getEtat());
					newrealisation.setDelaiexecution(realisation.getDelaiexecution());
					newrealisation.setSourcefinancement(realisation.getSourcefinancement());
					newrealisation.setPlan(plan);
					newrealisation.setAppel(UIConstants.NPARENT);
					newrealisation.setExamendncmp(realisation.getExamendncmp());
					newrealisation.setExamenccmp(realisation.getExamenccmp());
					BeanLocator.defaultLookup(RealisationsSession.class).save(newrealisation);
			  }
			}
			else
			{
				plan.setMotif(txtMotif.getValue());
				BeanLocator.defaultLookup(PlansdepassationSession.class).update(plan);
			}
			Events.postEvent(ApplicationEvents.ON_PLANS, getOpener(), null);
			detach();
			
		}
		
	}

	 private boolean checkFieldConstraints() {
		 		try {
		 			if(txtMotif.getValue().equals(""))
		 		     {
		                errorComponent = txtMotif;
		                errorMsg = Labels.getLabel("kermel.plansdepassation.miseajour.motif")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
		 				lbStatusBar.setValue(errorMsg);
		 				throw new WrongValueException (errorComponent, errorMsg);
		 		     }
		 			return true;
		 				
		 		}
		 		catch (Exception e) {
		 			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
		 			+ " [checkFieldConstraints]";
		 			errorComponent = null;
		 			return false;

		 			
		 		}
		 		
		 	}
		 
		
		public void onClick$menuFermer(){
			
			detach();
		}
			
}
