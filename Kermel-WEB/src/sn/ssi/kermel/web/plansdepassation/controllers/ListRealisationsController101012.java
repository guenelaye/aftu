package sn.ssi.kermel.web.plansdepassation.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Column;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Detail;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.Separator;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Vbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.plansdepassation.ejb.PlansdepassationSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class ListRealisationsController101012 extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer,RowRenderer {

	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
     String page=null;
    private Listheader lshReference,lshLibelle,lshTypemarche,lshModepassation,lshDatelancement,lshDateattribution,lshDatedemarrage,lshDateachevement,lshMontant;
    Session session = getHttpSession();
    SygPlansdepassation plan=new SygPlansdepassation();
    private Long idplan,idrealisation;
    private Label lblInfos;
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_REALSATIONS, MOD_REALSATIONS, SUPP_REALSATIONS, WFER_REALSATIONS, WBAIL_REALSATIONS;
    String login;
    private Grid grdRealisations;
    private Column idgride;
    private Combobox cbselect;
    private String filtrepar="service.libelle";
    private String idpar="service.id";
      
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.PARAM_REALSATIONS);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		
		if (ADD_REALSATIONS != null) { ADD_REALSATIONS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_REALSATIONS != null) { MOD_REALSATIONS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUPP_REALSATIONS != null) { SUPP_REALSATIONS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		if (WFER_REALSATIONS != null) { WFER_REALSATIONS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_CLOSE); }
		if (WBAIL_REALSATIONS != null) { WBAIL_REALSATIONS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_BAILLEURS); }
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_CLOSE, this);
		addEventListener(ApplicationEvents.ON_BAILLEURS, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	
		
		
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		
		grdRealisations.setRowRenderer(this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		login = ((String) getHttpSession().getAttribute("user"));
		idplan=(Long) session.getAttribute("idplan");
		plan=BeanLocator.defaultLookup(PlansdepassationSession.class).findById(idplan);
		if((plan.getStatus().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.saisie")))||(plan.getStatus().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.rejeter"))))
		{
			if(ADD_REALSATIONS!=null)
			ADD_REALSATIONS.setDisabled(false);
		}
			
		else
		{
			if(ADD_REALSATIONS!=null)
			ADD_REALSATIONS.setDisabled(true);
		}
			
		lblInfos.setValue(plan.getNumplan()+" "+Labels.getLabel("kermel.plansdepassation.infos.gestion")+" "+plan.getAnnee());
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		idgride.setLabel(Labels.getLabel("kermel.plansdepassation.realisations.filtrepar.service"));
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygRealisations> realisations = BeanLocator.defaultLookup(RealisationsSession.class).find(activePage,byPage,filtrepar,idpar,plan, null);
			 SimpleListModel listModel = new SimpleListModel(realisations);
			 grdRealisations.setModel(listModel);
			 pgPagination.setTotalSize(BeanLocator.defaultLookup( RealisationsSession.class).count(filtrepar,idpar,plan, null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/plansdepassation/formrealisation.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"650px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(RealisationFormController.PARAM_WINDOW_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		
		
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				BeanLocator.defaultLookup(RealisationsSession.class).delete(idrealisation);
				BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_REALSATIONS", Labels.getLabel("kermel.plansdepassation.realisation.suppression")+" :" + new Date(), login);
				
//				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
//				Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
//				BeanLocator.defaultLookup(AutoriteContractanteSession.class).delete(codes);
//				BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_REALSATIONS", Labels.getLabel("kermel.plansdepassation.realisation.suppression")+" :" + new Date(), login);
//				
//				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLOSE)) {
				loadApplicationState("plans");
			}
			
			else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
			Listcell button = (Listcell) event.getTarget();
			String toDo = (String) button.getAttribute(UIConstants.TODO);
			idrealisation = (Long) button.getAttribute("idrealisation");
			if (toDo.equalsIgnoreCase("delete"))
			{
				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, idrealisation);
				showMessageBox(display, map);
			}
			else 
			{
				if (toDo.equalsIgnoreCase("modifier")) {
						
					final String uri = "/plansdepassation/formrealisation.zul";

					final HashMap<String, String> display = new HashMap<String, String>();
					display.put(DSP_HEIGHT,"650px");
					display.put(DSP_WIDTH, "80%");
					display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

					final HashMap<String, Object> data = new HashMap<String, Object>();
					data.put(RealisationFormController.PARAM_WINDOW_MODE, UIConstants.MODE_EDIT);
					data.put(RealisationFormController.PARAM_WINDOW_CODE, idrealisation);

					showPopupWindow(uri, data, display);
					}
				else
				{
					if (toDo.equalsIgnoreCase("bailleurs")) {
						
						session.setAttribute("idrealisation", idrealisation);
						loadApplicationState("realisations_bailleurs");
						}
				}
			}
			
		}
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygRealisations plans = (SygRealisations) data;
		item.setValue(plans.getIdrealisation());

		 Listcell cellReference = new Listcell(plans.getReference());
		 cellReference.setParent(item);
		 
		 Listcell cellLibelle = new Listcell(plans.getLibelle());
		 cellLibelle.setParent(item);
		 
		 Listcell cellTypemarche = new Listcell(plans.getTypemarche().getLibelle());
		 cellTypemarche.setParent(item);
		 
		 Listcell cellModepassation = new Listcell(plans.getModepassation().getLibelle());
		 cellModepassation.setParent(item);
		 
		 Listcell cellDateLancement = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDatelancement()));
		 cellDateLancement.setParent(item);
		 
		 Listcell cellDateattribution = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDateattribution()));
		 cellDateattribution.setParent(item);
		 
		 Listcell cellDatedemarrage = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDatedemarrage()));
		 cellDatedemarrage.setParent(item);
		 
		 Listcell cellDateachevement = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDateachevement()));
		 cellDateachevement.setParent(item);
		 
		 Listcell cellMontant = new Listcell(ToolKermel.format3Decimal(plans.getMontant()));
		 cellMontant.setParent(item);

		 
	}
	public void onSelect$cbselect(){
		 if(cbselect.getSelectedItem().getId().equals("service"))
		 {
			 filtrepar="service.libelle";
			 idpar="service.id";
			 idgride.setLabel(Labels.getLabel("kermel.plansdepassation.realisations.filtrepar.service"));
		 }
		 else
		 {
			 if(cbselect.getSelectedItem().getId().equals("typesmarches"))
			 {
				 filtrepar="type.libelle";
				 idpar="type.id";
				 idgride.setLabel(Labels.getLabel("kermel.plansdepassation.realisations.filtrepar.typemarche"));
			 }
			 else
			 {
				 if(cbselect.getSelectedItem().getId().equals("modespassations"))
				 {
					 filtrepar="mode.libelle";
					 idpar="mode.id";
					 idgride.setLabel(Labels.getLabel("kermel.plansdepassation.realisations.filtrepar.modepassation"));
				 }
				 else
				 {
					 filtrepar="service.libelle";
					 idpar="service.id";
					 idgride.setLabel(Labels.getLabel("kermel.plansdepassation.realisations.filtrepar.service"));
				 } 
			 } 
		 }
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	public void onClick$bchercher()
	{
		
	
		 if(cbselect.getSelectedItem().getId().equals("service"))
		 {
			 filtrepar="service.libelle";
			 idpar="service.id";
			 idgride.setLabel("Par service ou direction");
		 }
		 else
		 {
			 if(cbselect.getSelectedItem().getId().equals("typesmarches"))
			 {
				 filtrepar="type.libelle";
				 idpar="type.id";
				 idgride.setLabel("Par type de march�");
			 }
			 else
			 {
				 if(cbselect.getSelectedItem().getId().equals("modespassations"))
				 {
					 filtrepar="mode.libelle";
					 idpar="mode.id";
					 idgride.setLabel("Par mode de passation ");
				 }
				 else
				 {
					 filtrepar="service.libelle";
					 idpar="service.id";
					 idgride.setLabel("Par service ou direction");
				 } 
			 } 
		 }
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	

	public void onClick$menuFermer()
	{
		loadApplicationState("plans");
	}
	
	 private class RealisationsRenderer implements ListitemRenderer {
			
			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
				SygRealisations plans = (SygRealisations) data;
				item.setValue(plans.getIdrealisation());

				 Listcell cellReference = new Listcell(plans.getReference());
				 cellReference.setParent(item);
				 
				 Listcell cellLibelle = new Listcell(plans.getLibelle());
				 cellLibelle.setParent(item);
				 
				 Listcell cellTypemarche = new Listcell(plans.getTypemarche().getLibelle());
				 cellTypemarche.setParent(item);
				 
				 Listcell cellModepassation = new Listcell(plans.getModepassation().getLibelle());
				 cellModepassation.setParent(item);
				 
				 Listcell cellDateLancement = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDatelancement()));
				 cellDateLancement.setParent(item);
				 
				 Listcell cellDateattribution = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDateattribution()));
				 cellDateattribution.setParent(item);
				 
				 Listcell cellDatedemarrage = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDatedemarrage()));
				 cellDatedemarrage.setParent(item);
				 
				 Listcell cellDateachevement = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDateachevement()));
				 cellDateachevement.setParent(item);
				 
				 Listcell cellMontant = new Listcell(ToolKermel.format2Decimal(plans.getMontant()));
				 cellMontant.setParent(item);
				 
				Listcell cellImageModif = new Listcell();
				if((plan.getStatus().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.saisie")))||(plan.getStatus().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.rejeter"))))
					
				{
					cellImageModif.setImage("/images/disk.png");
					cellImageModif.setAttribute(UIConstants.TODO, "modifier");
					cellImageModif.setAttribute("idrealisation", plans.getIdrealisation());
					cellImageModif.setTooltiptext("Modifier r�alisation");
					cellImageModif.addEventListener(Events.ON_CLICK, ListRealisationsController101012.this);
				}
				cellImageModif.setParent(item);
				
				Listcell cellImageSupprime = new Listcell();
				if((plan.getStatus().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.saisie")))||(plan.getStatus().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.rejeter"))))
					
				{
					cellImageSupprime.setImage("/images/delete.png");
					cellImageSupprime.setAttribute(UIConstants.TODO, "delete");
					cellImageSupprime.setAttribute("idrealisation", plans.getIdrealisation());
					cellImageSupprime.setTooltiptext("Supprimer r�alisation");
					cellImageSupprime.addEventListener(Events.ON_CLICK, ListRealisationsController101012.this);
					
				}
				cellImageSupprime.setParent(item);
				//				Listcell cellImageBailleurs = new Listcell();
//				cellImageBailleurs.setImage("/images/delete.png");
//				cellImageBailleurs.setAttribute(UIConstants.TODO, "bailleurs");
//				cellImageBailleurs.setAttribute("idrealisation", plans.getIdrealisation());
//				cellImageBailleurs.setTooltiptext("Sources de financement");
//				cellImageBailleurs.addEventListener(Events.ON_CLICK, ListRealisationsController.this);
//				cellImageBailleurs.setParent(item);
				
				

			}
	 }
	 @Override
		public void render(Row row, Object data, int index) throws Exception {
			
		    Object[] elmtligne = (Object[]) data;
			row.setValue(elmtligne);
			
			Label cell0 = new Label((String)elmtligne[0]);
			cell0.setParent(row);
			
		
			Detail detail = new Detail();	
			List<SygRealisations> realisations = new ArrayList<SygRealisations>();
			
			realisations= BeanLocator.defaultLookup(RealisationsSession.class).find(idpar,(Long) elmtligne[2],plan,null, null, null, null, null);
			
				
			Vbox vboxConnaissement = new Vbox();
			Separator separator0_1 = new Separator("horizontal");
			separator0_1.setParent(vboxConnaissement);
			vboxConnaissement.setStyle("width:100%;");
			
			//****************************//
			//Marchandises
			//****************************//
			Separator separator1_1 = new Separator("horizontal");
			separator1_1.setParent(vboxConnaissement);
//			Separator separator1_2 = new Separator("horizontal");
//			separator1_2.setParent(vboxConnaissement);
//			Separator separator1_3 = new Separator("horizontal");
//			separator1_3.setParent(vboxConnaissement);
			
			Groupbox groupboxRealisations = new Groupbox();
			Caption captionMarchandises = new Caption("R�alisations");
			captionMarchandises.setParent(groupboxRealisations);
			groupboxRealisations.setStyle("width:96%; margin-left: 1%;margin-right: 1%;");
			
			Listbox listboxReleves = new Listbox();
			listboxReleves.setItemRenderer(new RealisationsRenderer());
			
			Listhead listheadRealisations = new Listhead();
			
			Listheader headerReference = new Listheader(Labels.getLabel("kermel.plansdepassation.reference"));
			headerReference.setWidth("12%");
			headerReference.setParent(listheadRealisations);
			
			Listheader headerLibelle = new Listheader(Labels.getLabel("kermel.plansdepassation.realisations.envisagees"));
			headerLibelle.setWidth("23%");
			headerLibelle.setParent(listheadRealisations);
			
			Listheader headerType = new Listheader(Labels.getLabel("kermel.referentiel.typesmarche"));
			headerType.setWidth("15%");
			headerType.setParent(listheadRealisations);
			
			Listheader headerMode = new Listheader(Labels.getLabel("kermel.referentiel.modepassation"));
			headerMode.setWidth("15%");
			headerMode.setParent(listheadRealisations);
			
			Listheader headerDateL = new Listheader(Labels.getLabel("kermel.plansdepassation.realisations.datelancement"));
			headerDateL.setWidth("5%");
			headerDateL.setParent(listheadRealisations);
			
			Listheader headerDateA = new Listheader(Labels.getLabel("kermel.plansdepassation.realisations.dateattribution"));
			headerDateA.setWidth("5%");
			headerDateA.setParent(listheadRealisations);
			
			Listheader headerDateD = new Listheader(Labels.getLabel("kermel.plansdepassation.realisations.datedemarrage"));
			headerDateD.setWidth("5%");
			headerDateD.setParent(listheadRealisations);
			
			Listheader headerDateAv = new Listheader(Labels.getLabel("kermel.plansdepassation.realisations.dateachevement"));
			headerDateAv.setWidth("6%");
			headerDateAv.setParent(listheadRealisations);
			
			Listheader headerMontant = new Listheader(Labels.getLabel("kermel.plansdepassation.realisations.montant"));
			headerMontant.setWidth("10%");
			headerMontant.setParent(listheadRealisations);
			
			Listheader headerImage = new Listheader("");
			headerImage.setWidth("2%");
			headerImage.setParent(listheadRealisations);
			
			Listheader headerImageS = new Listheader("");
			headerImageS.setWidth("2%");
			headerImageS.setParent(listheadRealisations);
			
			listheadRealisations.setParent(listboxReleves);
			
			
			if(realisations!=null)
				listboxReleves.setModel(new SimpleListModel(realisations));
			
			listboxReleves.setParent(groupboxRealisations);
			groupboxRealisations.setParent(vboxConnaissement);
			
			Separator separator2_1 = new Separator("horizontal");
			separator2_1.setParent(vboxConnaissement);
			
			
			vboxConnaissement.setParent(detail);
			detail.setParent(row);
		}
}