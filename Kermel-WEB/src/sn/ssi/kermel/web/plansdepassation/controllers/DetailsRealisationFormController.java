package sn.ssi.kermel.web.plansdepassation.controllers;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygService;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.plansdepassation.ejb.PlansdepassationSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class DetailsRealisationFormController extends AbstractWindow implements
		 AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String login;
	Long code;
	private	SygRealisations realisation=new SygRealisations();
	SygService service=null;
	SygTypesmarches typemarche=null;
	SygModepassation modepassation=null;
	SygBailleurs bailleur=null;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Long idplan;
    private Label lblInfos,lblReference,lblRealisation,lblTypemarche,lblModepassation,lblDatepreparationdaodcrbc,lblDatereceptionavisccmpdncmpappel,lblDatenonobjectionptfappel,lblDateinvitationsoumission,lblMontant,
    lblDateouvertureplis,lblDatereceptionavisccmpdncmp,lblDatenonobjectionptf,lblDateprevisionnellesignaturecontrat,lblDelaiexecution,lblDatefinevaluation,
    lblCbcible,lblDatelancement,lblDateattribution,lblDatedemarrage,lblDateachevement;
    Session session = getHttpSession();
	List<SygRealisationsBailleurs> bailleur_realisation = new ArrayList<SygRealisationsBailleurs>();
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	private Utilisateur infoscompte;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		

		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		idplan=(Long) session.getAttribute("idplan");
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		plan=BeanLocator.defaultLookup(PlansdepassationSession.class).findById(idplan);
		lblInfos.setValue(plan.getNumplan()+" "+Labels.getLabel("kermel.plansdepassation.infos.gestion")+" "+plan.getAnnee());
		code = (Long) map.get(PARAM_WINDOW_CODE);
		realisation = BeanLocator.defaultLookup(RealisationsSession.class).findById(code);
		lblReference.setValue(realisation.getReference());
		lblRealisation.setValue(realisation.getLibelle());
		lblTypemarche.setValue(realisation.getTypemarche().getLibelle());
		lblModepassation.setValue(realisation.getModepassation().getLibelle());
		lblCbcible.setValue(realisation.getNaturemodepassation());
		if(realisation.getDatelancement()!=null)
		lblDatelancement.setValue(UtilVue.getInstance().formateLaDate(realisation.getDatelancement()));
		
		if(realisation.getDateattribution()!=null)
		lblDateattribution.setValue(UtilVue.getInstance().formateLaDate(realisation.getDateattribution()));
		
		if(realisation.getDatedemarrage()!=null)
		lblDatedemarrage.setValue(UtilVue.getInstance().formateLaDate(realisation.getDatedemarrage()));
		
		if(realisation.getDateachevement()!=null)
		lblDateachevement.setValue(UtilVue.getInstance().formateLaDate(realisation.getDateachevement()));
		
		lblMontant.setValue(ToolKermel.format2Decimal(realisation.getMontant()));
		
		
	
	}



	
			
}
