package sn.ssi.kermel.web.plansdepassation.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Column;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Detail;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.Separator;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.be.plansdepassation.ejb.PlansdepassationSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsSession;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class ListMAJRealisationsFournituresController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer,RowRenderer {

	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
     String page=null;
    Session session = getHttpSession();
    SygPlansdepassation plan=new SygPlansdepassation();
    private Long idplan,idrealisation;
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_REALSATIONS, MOD_REALSATIONS, SUPP_REALSATIONS, WFER_REALSATIONS, WBAIL_REALSATIONS;
    String login;
    private Grid grdRealisations;
    private Column idgride;
    private Combobox cbselect;
    private String filtrepar="service.libelle",objet=null;
    private String idpar="service.id",etat,color,toDo;
//    private Bandbox bdMode;
//    private Listbox lstMode;
//    private Paging pgMode;
//    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtObjet;//txtRechercherMode,
    SygModepassation modepassation=null;
     SygTypesmarches typemarche=null;
     SygRealisations realisation=null;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.PARAM_REALSATIONS);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		
		if (ADD_REALSATIONS != null) { ADD_REALSATIONS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_REALSATIONS != null) { MOD_REALSATIONS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUPP_REALSATIONS != null) { SUPP_REALSATIONS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		if (WFER_REALSATIONS != null) { WFER_REALSATIONS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_CLOSE); }
		if (WBAIL_REALSATIONS != null) { WBAIL_REALSATIONS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_BAILLEURS); }
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_CLOSE, this);
		addEventListener(ApplicationEvents.ON_BAILLEURS, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	
		
		
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		
		grdRealisations.setRowRenderer(this);
		
//		addEventListener(ApplicationEvents.ON_MODES, this);
//		lstMode.setItemRenderer(new ModesRenderer());
//		pgMode.setPageSize(byPageBandbox);
//		pgMode.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODES);
		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		login = ((String) getHttpSession().getAttribute("user"));
		idplan=(Long) session.getAttribute("idplan");
		plan=BeanLocator.defaultLookup(PlansdepassationSession.class).findById(idplan);
		if((plan.getStatus().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.saisie")))||(plan.getStatus().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.rejeter"))))
		{
			if(ADD_REALSATIONS!=null)
			ADD_REALSATIONS.setDisabled(false);
		}
			
		else
		{
			if(ADD_REALSATIONS!=null)
			ADD_REALSATIONS.setDisabled(true);
		}
		typemarche=BeanLocator.defaultLookup(TypesmarchesSession.class).findById(UIConstants.PARAM_TMFOURNITURES);	
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		idgride.setLabel(Labels.getLabel("kermel.plansdepassation.realisations.filtrepar.service"));
		Events.postEvent(ApplicationEvents.ON_MODES, this, null);
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygRealisations> realisations = BeanLocator.defaultLookup(RealisationsSession.class).RealisationsMAJ(activePage,byPage,filtrepar,idpar,plan, UIConstants.PARAM_TMFOURNITURES,"U"+plan.getVersion());
			 SimpleListModel listModel = new SimpleListModel(realisations);
			 grdRealisations.setModel(listModel);
			 pgPagination.setTotalSize(BeanLocator.defaultLookup( RealisationsSession.class).countRealisationsMAJ(filtrepar,idpar,plan, UIConstants.PARAM_TMFOURNITURES,"U"+plan.getVersion()));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/plansdepassation/formmiseajourrealisationfourniture.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"600px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(RealisationFournitureMiseAJourFormController.PARAM_WINDOW_TYPE,UIConstants.PARAM_TMFOURNITURES);
			data.put(RealisationFournitureMiseAJourFormController.PARAM_WINDOW_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}

		
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			   if(realisation.getEtat()==null)
				   {
				    realisation.setEtat("S"+plan.getVersion());
				    realisation.setSupprime(0);
					BeanLocator.defaultLookup(RealisationsSession.class).deletemiseajour(realisation);
				   }
			   else
			      {
				   if(realisation.getEtat().substring(0, 1).equals("A"))
				    {
					   BeanLocator.defaultLookup(RealisationsSession.class).delete(idrealisation);
				     }
				   else
				     {
					   if(realisation.getEtat().substring(0, 1).equals("S"))
					    {
						   realisation.setEtat("M"+plan.getVersion());
						   realisation.setSupprime(1);
						   realisation.setMiseajourid(0L); 
					    }
					   else
					   {
						   realisation.setSupprime(0);
						   realisation.setEtat("S"+plan.getVersion());
						   realisation.setMiseajourid(plan.getIDinfoplan());
					   }
					   BeanLocator.defaultLookup(RealisationsSession.class).deletemiseajour(realisation);  
				     }
			      }
			  
		
				session.setAttribute("LibelleTab","REALSFOURN");
				loadApplicationState("realisations_maj");
			}
	
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLOSE)) {
				loadApplicationState("maj_plan");
			}
			
			else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
			Listcell button = (Listcell) event.getTarget();
			toDo = (String) button.getAttribute(UIConstants.TODO);
			realisation=(SygRealisations) button.getAttribute("idrealisation");
			idrealisation = realisation.getIdrealisation();
			if (toDo.equalsIgnoreCase("delete")||toDo.equalsIgnoreCase("restaurer"))
			{
				HashMap<String, String> display = new HashMap<String, String>(); // permet
				if (toDo.equalsIgnoreCase("delete"))
				{
					display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
					display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				}
				else
				{
					display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.restaurer"));
					display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.restaurer"));
				}
				
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, idrealisation);
				showMessageBox(display, map);
			}
			else 
			{
				if (toDo.equalsIgnoreCase("modifier")) {
						
					final String uri = "/plansdepassation/formmiseajourrealisationfourniture.zul";

					final HashMap<String, String> display = new HashMap<String, String>();
					display.put(DSP_HEIGHT,"600px");
					display.put(DSP_WIDTH, "80%");
					display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

					final HashMap<String, Object> data = new HashMap<String, Object>();
					data.put(RealisationFournitureMiseAJourFormController.PARAM_WINDOW_TYPE,UIConstants.PARAM_TMFOURNITURES);
					data.put(RealisationFournitureMiseAJourFormController.PARAM_WINDOW_MODE, UIConstants.MODE_EDIT);
					data.put(RealisationFournitureMiseAJourFormController.PARAM_WINDOW_CODE, idrealisation);

					showPopupWindow(uri, data, display);
					}
				else
				{
					if (toDo.equalsIgnoreCase("bailleurs")) {
						
						session.setAttribute("idrealisation", idrealisation);
						loadApplicationState("realisations_bailleurs");
						}
					else
						{
						final String uri = "/plansdepassation/formdetailsrealisation.zul";

						final HashMap<String, String> display = new HashMap<String, String>();
						display.put(DSP_HEIGHT,"500px");
						display.put(DSP_WIDTH, "80%");
						display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.realisation.details"));

						final HashMap<String, Object> data = new HashMap<String, Object>();
						data.put(DetailsRealisationFormController.PARAM_WINDOW_CODE, idrealisation);

						showPopupWindow(uri, data, display);
						
						}
				}
			}
			
		}
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygRealisations plans = (SygRealisations) data;
		item.setValue(plans);

		 Listcell cellReference = new Listcell(plans.getReference());
		 cellReference.setParent(item);
		 
		 Listcell cellLibelle = new Listcell(plans.getLibelle());
		 cellLibelle.setParent(item);
		 
		 Listcell cellTypemarche = new Listcell(plans.getTypemarche().getLibelle());
		 cellTypemarche.setParent(item);
		 
		 Listcell cellModepassation = new Listcell(plans.getModepassation().getLibelle());
		 cellModepassation.setParent(item);
		 
		 Listcell cellDateLancement = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDatepreparationdaodcrbc()));
		 cellDateLancement.setParent(item);
		 
		 Listcell cellDateattribution = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDatereceptionavisccmpdncmpappel()));
		 cellDateattribution.setParent(item);
		 
		 Listcell cellDatedemarrage = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDateinvitationsoumission()));
		 cellDatedemarrage.setParent(item);
		 
		 Listcell cellDateachevement = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDateouvertureplis()));
		 cellDateachevement.setParent(item);
		 
		 Listcell cellMontant = new Listcell(ToolKermel.format3Decimal(plans.getMontant()));
		 cellMontant.setParent(item);
		

		 
	}
	public void onSelect$cbselect(){
		
		 if(cbselect.getSelectedItem().getId().equals("service"))
		 {
			 filtrepar="service.libelle";
			 idpar="service.id";
			 idgride.setLabel(Labels.getLabel("kermel.plansdepassation.realisations.filtrepar.service"));
		 }
		 else
		 {
			
				 if(cbselect.getSelectedItem().getId().equals("modespassations"))
				 {
					 filtrepar="mode.libelle";
					 idpar="mode.id";
					 idgride.setLabel(Labels.getLabel("kermel.plansdepassation.realisations.filtrepar.modepassation"));
				 }
				 else
				 {
					 filtrepar="service.libelle";
					 idpar="service.id";
					 idgride.setLabel(Labels.getLabel("kermel.plansdepassation.realisations.filtrepar.service"));
				 } 
			 
		 }
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}

	

	public void onClick$menuFermer()
	{
		loadApplicationState("maj_plan");
	}
	
	 private class RealisationsRenderer implements ListitemRenderer {
			
			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
				SygRealisations realisations = (SygRealisations) data;
				item.setValue(realisations);

				 Listcell cellReference = new Listcell(realisations.getReference());
				 cellReference.setParent(item);
				 
				 Listcell cellLibelle = new Listcell(realisations.getLibelle());
				 cellLibelle.setParent(item);
				 
				 
				 Listcell cellModepassation = new Listcell(realisations.getModepassation().getLibelle());
				 cellModepassation.setParent(item);
				 
				 Listcell cellDateLancement = new Listcell("");
				 if(realisations.getDatelancement()!=null)
					 cellDateLancement.setLabel(UtilVue.getInstance().formateLaDate(realisations.getDatelancement()));
				 cellDateLancement.setParent(item);
				 
				
				 
				Listcell cellImageModif = new Listcell();
				if((plan.getStatus().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.saisie")))||(plan.getStatus().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.rejeter"))))
					
				{
					if(realisations.getEtat()==null)
					{
						cellImageModif.setImage("/images/pencil-small.png");
						cellImageModif.setAttribute(UIConstants.TODO, "modifier");
						cellImageModif.setAttribute("idrealisation", realisations);
						cellImageModif.setTooltiptext("Modifier r�alisation");
						cellImageModif.addEventListener(Events.ON_CLICK, ListMAJRealisationsFournituresController.this);
					}
					else
					{
						if(!realisations.getEtat().substring(0, 1).equals("S"))
						{
							cellImageModif.setImage("/images/pencil-small.png");
							cellImageModif.setAttribute(UIConstants.TODO, "modifier");
							cellImageModif.setAttribute("idrealisation", realisations);
							cellImageModif.setTooltiptext("Modifier r�alisation");
							cellImageModif.addEventListener(Events.ON_CLICK, ListMAJRealisationsFournituresController.this);
						}
					}
					
				}
				cellImageModif.setParent(item);
				
				Listcell cellImageSupprime = new Listcell();
				if((plan.getStatus().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.saisie")))||(plan.getStatus().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.rejeter"))))
					
				{
					cellImageSupprime.setAttribute("idrealisation", realisations);
					if(realisations.getEtat()!=null&&realisations.getEtat().substring(0, 1).equals("S"))
					{
						cellImageSupprime.setAttribute(UIConstants.TODO, "restaurer");
						cellImageSupprime.setImage("/images/application_delete.png");
						cellImageSupprime.setTooltiptext("Restaurer cette r�lisation");
					}
					else
					{
						
						cellImageSupprime.setAttribute(UIConstants.TODO, "delete");
						cellImageSupprime.setImage("/images/delete.png");
						cellImageSupprime.setTooltiptext("Supprimer r�alisation");
					}
					cellImageSupprime.addEventListener(Events.ON_CLICK, ListMAJRealisationsFournituresController.this);
					
				}
				cellImageSupprime.setParent(item);
				
				Listcell cellImageDetails = new Listcell();
				cellImageDetails.setImage("/images/zoom.png");
				cellImageDetails.setAttribute(UIConstants.TODO, "details");
				cellImageDetails.setAttribute("idrealisation", realisations);
				cellImageDetails.setTooltiptext("D�tails r�alisation");
				cellImageDetails.addEventListener(Events.ON_CLICK, ListMAJRealisationsFournituresController.this);
				cellImageDetails.setParent(item);
				
				etat=realisations.getEtat();
				if(etat==null)
					color="";
				else
				{
					etat=realisations.getEtat().substring(0, 1);
					if(etat.equals("A"))
					  color="background-color:#BFBFFF";
					if(etat.equals("M"))
						  color="background-color:#EAFFEF";
					if(etat.equals("S"))
						  color="background-color:#F79DB6";
				}
					
			  item.setStyle(color);
			}
	 }
	 @Override
		public void render(Row row, Object data, int index) throws Exception {
			
		    Object[] elmtligne = (Object[]) data;
			row.setValue(elmtligne);
			
			Label cell0 = new Label((String)elmtligne[0]);
			cell0.setParent(row);
			
		
			Detail detail = new Detail();	
			List<SygRealisations> realisations = new ArrayList<SygRealisations>();
			
			realisations= BeanLocator.defaultLookup(RealisationsSession.class).findRealisationsMAJ(idpar,(Long) elmtligne[2],plan,UIConstants.PARAM_TMFOURNITURES, objet, null, null, modepassation,"U"+plan.getVersion());
			
				
			Vbox vboxConnaissement = new Vbox();
			Separator separator0_1 = new Separator("horizontal");
			separator0_1.setParent(vboxConnaissement);
			vboxConnaissement.setStyle("width:100%;");
			
			//****************************//
			//Marchandises
			//****************************//
			Separator separator1_1 = new Separator("horizontal");
			separator1_1.setParent(vboxConnaissement);
//			Separator separator1_2 = new Separator("horizontal");
//			separator1_2.setParent(vboxConnaissement);
//			Separator separator1_3 = new Separator("horizontal");
//			separator1_3.setParent(vboxConnaissement);
			
			Groupbox groupboxRealisations = new Groupbox();
			Caption captionMarchandises = new Caption("");
			captionMarchandises.setParent(groupboxRealisations);
			groupboxRealisations.setStyle("width:96%; margin-left: 1%;margin-right: 1%;border:0;bordercolor=#FFFFFF");
		
			
			Listbox listboxRealisations = new Listbox();
			listboxRealisations.setItemRenderer(new RealisationsRenderer());
			
			Listhead listheadRealisations = new Listhead();
			
			Listheader headerReference = new Listheader(Labels.getLabel("kermel.plansdepassation.reference"));
			headerReference.setWidth("10%");
			headerReference.setParent(listheadRealisations);
			
			Listheader headerLibelle = new Listheader(Labels.getLabel("kermel.plansdepassation.realisations.envisagees"));
			headerLibelle.setWidth("45%");
			headerLibelle.setParent(listheadRealisations);
			
			
			
			Listheader headerMode = new Listheader(Labels.getLabel("kermel.referentiel.modepassation"));
			headerMode.setWidth("25%");
			headerMode.setParent(listheadRealisations);
			
			Listheader headerDateL = new Listheader(Labels.getLabel("kermel.plansdepassation.realisations.lancementdate"));
			headerDateL.setWidth("14%");
			headerDateL.setParent(listheadRealisations);
			
			
			Listheader headerImage = new Listheader("");
			headerImage.setWidth("2%");
			headerImage.setParent(listheadRealisations);
			
			Listheader headerImageS = new Listheader("");
			headerImageS.setWidth("2%");
			headerImageS.setParent(listheadRealisations);
			
			Listheader headerImageD = new Listheader("");
			headerImageD.setWidth("2%");
			headerImageD.setParent(listheadRealisations);
			
			listheadRealisations.setParent(listboxRealisations);
			
			
			if(realisations!=null)
				listboxRealisations.setModel(new SimpleListModel(realisations));
			
			listboxRealisations.setParent(groupboxRealisations);
			groupboxRealisations.setParent(vboxConnaissement);
			
			Separator separator2_1 = new Separator("horizontal");
			separator2_1.setParent(vboxConnaissement);
			separator2_1.setStyle("width:96%; margin-left: 1%;margin-right: 1%;border:0;bordercolor:#FFFFFF");
			
			
			vboxConnaissement.setParent(detail);
			detail.setParent(row);
		}
	 
	 
		///////////Mode de passation///////// 
//		public void onSelect$lstMode(){
//		modepassation= (SygModepassation) lstMode.getSelectedItem().getValue();
//		bdMode.setValue(modepassation.getLibelle());
//		bdMode.close();
//		
//		}
//		
//		public class ModesRenderer implements ListitemRenderer{
//		
//		
//		
//		@Override
//		public void render(Listitem item, Object data, int index)  throws Exception {
//			SygTypesmarchesmodespassations modes = (SygTypesmarchesmodespassations) data;
//			item.setValue(modes.getMode());
//			
//			Listcell cellCode = new Listcell(modes.getMode().getCode());
//			cellCode.setParent(item);
//			Listcell cellLibelle = new Listcell("");
//			if (modes.getMode().getLibelle()!=null){
//				cellLibelle.setLabel(modes.getMode().getLibelle());
//			}
//			cellLibelle.setParent(item);
//		
//		}
//		}
//		public void onFocus$txtRechercherMode(){
//		if(txtRechercherMode.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.modepassation"))){
//			txtRechercherMode.setValue("");
//		}		 
//		}
//		
//		public void  onClick$btnRechercherMode(){
//		if(txtRechercherMode.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.modepassation")) || txtRechercherMode.getValue().equals("")){
//			libellemode= null;
//			page=null;
//		}else{
//			libellemode = txtRechercherMode.getValue();
//			page="0";
//		}
//		Events.postEvent(ApplicationEvents.ON_MODES, this, page);
//		}

		public void onFocus$txtObjet(){
			if(txtObjet.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.recours.objet"))){
				txtObjet.setValue("");
			}		 
			}
		public void onBlur$txtObjet(){
			if(txtObjet.getValue().equalsIgnoreCase("")){
				txtObjet.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.recours.objet"));
			}		 
			}
		public void onOK$txtObjet(){
			onClick$bchercher();	
		}
		public void onOK$bchercher(){
			onClick$bchercher();	
		}
		public void onClick$bchercher()
		{
			
			
			if((txtObjet.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.recours.objet")))||(txtObjet.getValue().equals("")))
			 {
				objet=null;
			 }
			else
			 {
				objet=txtObjet.getValue();
				page="0";
			 }
			
			 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
}