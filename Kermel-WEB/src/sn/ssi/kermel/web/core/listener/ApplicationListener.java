package sn.ssi.kermel.web.core.listener;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.dom4j.DocumentException;

import sn.ssi.kermel.web.core.constants.ApplicationContextAttributes;
import sn.ssi.kermel.web.core.utils.StatesHashMapLoader;

/**
 * Application Lifecycle Listener implementation class
 * MedisenApplicationListener
 * 
 */
public class ApplicationListener implements ServletContextListener {

	/**
	 * Default constructor.
	 */

	Logger logger = Logger.getLogger(ApplicationListener.class);
	public static HashMap<String, Object> states;

	public ApplicationListener() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent evt) {
		ServletContext servletContext = evt.getServletContext();
		String path = servletContext.getInitParameter("application-states");
		try {
			URL url = servletContext.getResource(path);
			logger.info(url);
			states = StatesHashMapLoader.loadApplicationStates(url);
			servletContext.setAttribute(
					ApplicationContextAttributes.APPLICATION_STATES, states);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
	}

}
