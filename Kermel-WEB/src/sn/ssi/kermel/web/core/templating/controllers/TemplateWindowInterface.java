package sn.ssi.kermel.web.core.templating.controllers;

import org.zkoss.zk.ui.Component;

/**
 * 
 * @author xx
 *
 *Cette interface d�finit le comportement d'une application bas�e sur du templating.
 * @param <T> Type de templating utilis�.
 */
public interface TemplateWindowInterface<T extends Component> 
{
	public T getTemplateZone(String zone); 
	public void loadApplicationBookmark(String state);
}
