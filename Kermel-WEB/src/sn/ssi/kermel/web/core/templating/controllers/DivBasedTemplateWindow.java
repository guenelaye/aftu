package sn.ssi.kermel.web.core.templating.controllers;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.BookmarkEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Div;
import org.zkoss.zul.Window;

import sn.ssi.kermel.web.core.constants.ApplicationContextAttributes;

/**
 * 
 * @author Alassane Cette classe repr�sente la fen�tre principale de
 *         l'application. Elle utilise un templating bas� sur des zones de type
 *         "Include". Elle g�re les zones du template ainsi que les changements
 *         d'�tats de l'application.
 */
public class DivBasedTemplateWindow extends Window implements EventListener,
		TemplateWindowInterface<Div> {

	Logger logger = Logger.getLogger(DivBasedTemplateWindow.class);

	public DivBasedTemplateWindow() {
		addEventListener(Events.ON_BOOKMARK_CHANGE, this);
		// footer.setSrc("footer.zul");
	}

	@Override
	public void onEvent(Event evt) throws Exception {

		if (evt instanceof BookmarkEvent) {

			BookmarkEvent bookmarkEvent = (BookmarkEvent) evt;

			String bookmark = bookmarkEvent.getBookmark();
			loadApplicationBookmark(bookmark);

		}
		// footer.setSrc("/templates/default/footer.zul");
	}

	@Override
	public Div getTemplateZone(String zone) {
		return (Div) getFellow(zone);
	}

	private void freeZone(Div div) {
		System.out.println("===freeZone===========");
		List components = div.getChildren();
		try {
			for (Iterator iterator = components.iterator(); iterator.hasNext();) {
				Component component = (Component) iterator.next();
				component.detach();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("===fin freeZone===========");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void loadApplicationBookmark(String bookmark) {
		logger.info("Loading " + bookmark + " state.");
		String[] regex = bookmark.split("&");
		Executions.getCurrent().setAttribute("mystate", bookmark);
		String state = regex[0];
		HashMap<String, String> params = new HashMap<String, String>();
		if (regex.length == 2) {
			params.put("params", regex[1]);
		}
		HashMap<String, Object> states = (HashMap<String, Object>) getDesktop()
				.getWebApp().getAttribute(
						ApplicationContextAttributes.APPLICATION_STATES);
		HashMap<String, Object> aState = (HashMap<String, Object>) states
				.get(state);
		Set<String> stateZones = aState.keySet();
		for (String stateZone : stateZones) {

			HashMap<String, Object> stateData = (HashMap<String, Object>) aState
					.get(stateZone);
			String content = (String) stateData.get("content");
			Boolean popup = (Boolean) stateData.get("popup");

			System.out.println("content " + content);
			System.out.println("popup " + popup);

			Div zone = getTemplateZone(stateZone);

			if ((popup == null) || (popup == false)) {
				try {
					freeZone(zone);
					Window w = (Window) Executions.createComponents(content,
							zone, params);
					w.setVisible(true);
				} catch (Exception e) {
				}
			} else {
				Window w = (Window) Executions.createComponents(content, zone,
						params);
				w.setVisible(true);
				w.doHighlighted();
				w.setClosable(true);
			}

			getDesktop().setBookmark(bookmark);
		}
	}

}
