package sn.ssi.kermel.web.core.templating.controllers;

import java.util.HashMap;
import java.util.Set;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.event.BookmarkEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Include;
import org.zkoss.zul.Window;

import sn.ssi.kermel.web.core.constants.ApplicationContextAttributes;

/**
 * 
 * @author Alassane Cette classe repr�sente la fen�tre principale de
 *         l'application. Elle utilise un templating bas� sur des zones de type
 *         "Include". Elle g�re les zones du template ainsi que les changements
 *         d'�tats de l'application.
 */
public class IncludeBasedTemplateWindow extends Window implements EventListener, TemplateWindowInterface<Include> {

	Logger logger = Logger.getLogger(IncludeBasedTemplateWindow.class);

	public IncludeBasedTemplateWindow() {
		addEventListener(Events.ON_BOOKMARK_CHANGE, this);
	}

	@Override
	public void onEvent(Event evt) throws Exception {

		if (evt instanceof BookmarkEvent) {
			BookmarkEvent bookmarkEvent = (BookmarkEvent) evt;
			String bookmark = bookmarkEvent.getBookmark();
			loadApplicationBookmark(bookmark);
		}
	}

	@Override
	public Include getTemplateZone(String zone) {

		return (Include) getFellow(zone);
	}

	@Override
	public void loadApplicationBookmark(String state) {

		logger.info("Loading " + state + " state.");
		HashMap<String, Object> states = (HashMap<String, Object>) getDesktop().getWebApp().getAttribute(ApplicationContextAttributes.APPLICATION_STATES);
		HashMap<String, Object> aState = (HashMap<String, Object>) states.get(state);
		Set<String> stateZones = aState.keySet();
		for (String stateZone : stateZones) {
			Include zone = getTemplateZone(stateZone);
			String content = (String) aState.get(stateZone);
			zone.invalidate();
			try {
				zone.getFirstChild().detach();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			zone.setSrc(content);
			getDesktop().setBookmark(state);
		}
	}

}
