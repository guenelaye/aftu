package sn.ssi.kermel.web.core.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Abdel Rakib OLA (olarakib@gmail.com), Guillaume GB
 *         (guibenissan@yahoo.fr)
 * @since Mercredi 06 Janvier 2010, 12:15
 * @version 0.0.1
 * 
 *          <ul>
 *          Utilitaire pour:
 *          <li>Formater les dates en Français
 *          </ul>
 */
public class UtilVue {
	private volatile static UtilVue uniqueInstance;

	private UtilVue() {
	}

	public static UtilVue getInstance() {
		if (uniqueInstance == null) {
			synchronized (UtilVue.class) {
				if (uniqueInstance == null) {
					uniqueInstance = new UtilVue();
				}
			}
		}
		return uniqueInstance;
	}

	/**
	 * @author Guillaume GB (guibenissan@yahoo.fr)
	 * @param dateNonFormatee
	 * @return la date formatee en francais
	 */
	public String formateLaDate(Date dateNonFormatee) {
		Calendar calendrier = Calendar.getInstance();

		calendrier.setTime(dateNonFormatee);

		String jour = String.format("%02d", calendrier.get(Calendar.DAY_OF_MONTH));
		String mois = String.format("%02d", calendrier.get(Calendar.MONTH) + 1);
		String annee = String.format("%d", calendrier.get(Calendar.YEAR));

		return jour + "-" + mois + "-" + annee;

	}

	public String formateLaDate2(Date dateNonFormatee) {
		Calendar calendrier = Calendar.getInstance();

		calendrier.setTime(dateNonFormatee);

		String jour = String.format("%02d", calendrier.get(Calendar.DAY_OF_MONTH));
		String mois = String.format("%02d", calendrier.get(Calendar.MONTH) + 1);
		String annee = String.format("%d", calendrier.get(Calendar.YEAR));

		return jour + "/" + mois + "/" + annee;

	}

	public String formateLaDate3(Date dateNonFormatee) {

		if (dateNonFormatee != null) {
			Calendar calendrier = Calendar.getInstance();

			calendrier.setTime(dateNonFormatee);

			String seconde = String.format("%02d", calendrier.get(Calendar.SECOND));
			String minute = String.format("%02d", calendrier.get(Calendar.MINUTE) + 1);
			String heure = String.format("%d", calendrier.get(Calendar.HOUR));
			String jour = String.format("%02d", calendrier.get(Calendar.DAY_OF_MONTH));
			String mois = String.format("%02d", calendrier.get(Calendar.MONTH) + 1);
			String annee = String.format("%d", calendrier.get(Calendar.YEAR));

			return seconde + minute + heure + jour + mois + annee;
		} else {
			return "";
		}

	}

	/**
	 * @return (String): une heure au format hh:mm.
	 * @param uneHeure
	 *            (java.util.Date): l'heure a formater.
	 * @author Guillaume GB (guibenissan@yahoo.fr)
	 */
	public String formateLHeure(Date uneHeure) {

		Calendar calendrier = Calendar.getInstance();

		calendrier.setTime(uneHeure);

		String heure = String.format("%02d", calendrier.get(Calendar.HOUR_OF_DAY));
		String minutes = String.format("%02d", calendrier.get(Calendar.MINUTE));

		return heure + ":" + minutes;

	}

	/**
	 * @return (java.util.Date): un objet Date construit a partir d'une date et
	 *         d'une heure donnees. <br>
	 *         les secondes et millisecondes sont initialisees a 0.
	 * @param uneDate
	 *            (java.util.Date)
	 * @param uneHeure
	 *            (java.util.Date)
	 * @author Guillaume GB (guibenissan@yahoo.fr)
	 */
	public Date construireDate(Date uneDate, Date uneHeure) {

		Calendar calendrier = Calendar.getInstance();

		calendrier.setTime(uneDate);

		int heure = 0, minutes = 0;
		if (uneHeure != null) {
			Calendar calendrierHeure = Calendar.getInstance();
			calendrierHeure.setTime(uneHeure);
			heure = calendrierHeure.get(Calendar.HOUR_OF_DAY);
			minutes = calendrierHeure.get(Calendar.MINUTE);
		}

		calendrier.set(Calendar.HOUR_OF_DAY, heure);
		calendrier.set(Calendar.MINUTE, minutes);
		calendrier.set(Calendar.SECOND, 0);
		calendrier.set(Calendar.MILLISECOND, 0);

		return calendrier.getTime();

	}

	/**
	 * @return (java.util.Date): la date et l'heure actuelles, avec les secondes
	 *         et millisecondes initialisees a 0.
	 * @author Guillaume GB (guibenissan@yahoo.fr)
	 */
	public Date construireDateActuelle() {

		Calendar calendrier = Calendar.getInstance();

		calendrier.set(Calendar.SECOND, 0);
		calendrier.set(Calendar.MILLISECOND, 0);

		return calendrier.getTime();

	}

	/**
	 * @author Guillaume GB (guibenissan@yahoo.fr)
	 * @param dateA
	 *            (java.util.Date)
	 * @param dateB
	 *            (java.util.Date)
	 * @param comparerHeures
	 *            (boolean): si a true, la comparaison inclut les heures.
	 * @return (int): <li>1 si la date contenu dans dateA est ulterieure a celle
	 *         contenue dans dateB; <li>0 si la date contenu dans dateA est
	 *         identique a celle contenue dans dateB; <li>-1 si la date contenu
	 *         dans dateA est anterieure a celle contenue dans dateB.
	 */
	public int comparerDates(Date dateA, Date dateB, boolean comparerHeures) {

		Calendar calendrierA = Calendar.getInstance();
		Calendar calendrierB = Calendar.getInstance();
		calendrierA.setTime(dateA);
		calendrierB.setTime(dateB);

		calendrierA.set(Calendar.SECOND, 0);
		calendrierB.set(Calendar.SECOND, 0);
		calendrierA.set(Calendar.MILLISECOND, 0);
		calendrierB.set(Calendar.MILLISECOND, 0);

		if (!comparerHeures) {
			calendrierA.set(Calendar.HOUR_OF_DAY, 0);
			calendrierB.set(Calendar.HOUR_OF_DAY, 0);
			calendrierA.set(Calendar.MINUTE, 0);
			calendrierB.set(Calendar.MINUTE, 0);
		}

		return calendrierA.getTime().compareTo(calendrierB.getTime());

	}
	
	public String anneecourant(Date dateNonFormatee){
		
		if (dateNonFormatee == null) return "";
		
		Calendar calendrier = Calendar.getInstance();
		
		calendrier.setTime(dateNonFormatee);
		
		String annee = String.format("%d", calendrier.get(Calendar.YEAR));
		
		return annee;
		
	}
	
 
}