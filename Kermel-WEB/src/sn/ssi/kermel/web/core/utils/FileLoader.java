package sn.ssi.kermel.web.core.utils;

import java.io.FileOutputStream;
import java.io.InputStream;

import org.zkoss.util.media.Media;
import org.zkoss.util.resource.Labels;
import org.zkoss.zul.Fileupload;
public class FileLoader {
	/**
	 * upload role file
	 * 
	 * @param cheminDossierPieces
	 * @return path of file
	 */
	public static String upload(String cheminDossierPieces) {
		String cheminPieceJointe = null;

		try {
			// open select file dialog
			Object media = Fileupload.get();
			if (media instanceof org.zkoss.util.media.Media) {
				org.zkoss.util.media.Media medium = (org.zkoss.util.media.Media) media;

				// get file name
				String nomPiece = cheminDossierPieces + medium.getName();

				// upload file
				boolean boolUpload = uploadFile(medium, nomPiece);
				if (boolUpload) {
					// remember file name
					cheminPieceJointe = nomPiece;
				} else {
					System.out
							.print(Labels
									.getLabel("kermel.clientele.receptiondossier.joindrepieceerror"));
				}
			}

		} catch (Exception e) {
			// handle ERROR
			e.printStackTrace();
		}
		return cheminPieceJointe;
	}

	public static String uploadPieceDossier(String dgrCode, String pijcode, String cheminDossierPieces, String format) {
		String fichierPieceJointe = null;

		try {
			// open select file dialog
			Object media = Fileupload.get();
			if (media instanceof org.zkoss.util.media.Media) {
				org.zkoss.util.media.Media medium = (org.zkoss.util.media.Media) media;

				if (!medium.getFormat().equals(format)) {
					return format;
				} else {

					// get file name
					String val = medium.getFormat();
					System.out.println("format: " + val);
					String nomPiece = cheminDossierPieces + "/" + dgrCode + "_" + pijcode + "_" + medium.getName() + "." + medium.getFormat();

					// upload file
					boolean boolUpload = uploadFile(medium, nomPiece);
					if (boolUpload) {
						// remember file name
						fichierPieceJointe = dgrCode + "_" + pijcode + "_" + medium.getName();
					} else {
						System.out.print(Labels.getLabel("atlantis.clientele.receptiondossier.joindrepieceerror"));
					}
				}
			}

		} catch (Exception e) {
			// handle ERROR
			e.printStackTrace();
		}
		return fichierPieceJointe;
	}
	
	public static String uploadPieceDossier(String dgrCode, String pijcode, String cheminDossierPieces) {
		String fichierPieceJointe = null;

		try {
			// open select file dialog
			Object media = Fileupload.get();
			if (media instanceof org.zkoss.util.media.Media) {
				org.zkoss.util.media.Media medium = (org.zkoss.util.media.Media) media;

				String nomPiece = null;
				// get file name
				// String nomPiece = cheminDossierPieces + "/" + dgrCode + "_" +
				// pijcode + "_" + medium.getName();

//				if (ToolKermel.isWindows())
//					nomPiece = cheminDossierPieces + dgrCode + "_" + pijcode + "_" + medium.getName();
//				else
					nomPiece = cheminDossierPieces + dgrCode + "_" + pijcode + "_" + medium.getName();

				// upload file
				boolean boolUpload = uploadFile(medium, nomPiece);
				if (boolUpload) {
					// remember file name
					fichierPieceJointe = dgrCode + "_" + pijcode + "_" + medium.getName();
				} else {
					System.out.print(Labels.getLabel("atlantis.clientele.receptiondossier.joindrepieceerror"));
				}
			}

		} catch (Exception e) {
			// handle ERROR
			e.printStackTrace();
		}
		return fichierPieceJointe;
	}


	public static String uploadPieceDossierAO(String dgrCode, String pijcode,
			String cheminDossierPieces) {
		String fichierPieceJointe = null;

		try {
			// open select file dialog
			Object media = Fileupload.get();
			if (media instanceof org.zkoss.util.media.Media) {
				org.zkoss.util.media.Media medium = (org.zkoss.util.media.Media) media;

				// get file name
				String nomPiece = cheminDossierPieces + "/" + dgrCode + "_"
						+ pijcode + "_" + medium.getName();
				String format=medium.getFormat();
				System.out.println(format);
//              if(format.equals("pdf"))
//              {
				// upload file
				boolean boolUpload = uploadFile(medium, nomPiece);
				if (boolUpload) {
					// remember file name
					fichierPieceJointe = dgrCode + "_" + pijcode + "_"
							+ medium.getName();
				} else {
					System.out
							.print(Labels
									.getLabel("atlantis.clientele.receptiondossier.joindrepieceerror"));
				}
//              }
//              else
//              {
//            		 Messagebox.show("Vous ne pouvez attacher que des fichiers de type PDF. ", "Erreur", Messagebox.OK, Messagebox.ERROR);
// 					
//              }
			}

		} catch (Exception e) {
			// handle ERROR
			e.printStackTrace();
		}
		return fichierPieceJointe;
	}

	/**
	 * upload a file to server
	 * 
	 * @param medium
	 * @param a_strServerPath
	 * @param a_strFileName
	 * @return
	 */
	public static boolean uploadFile(org.zkoss.util.media.Media medium,
			String nomPiece) {
		boolean boolUpload = true;
		try {
			// buffer size in memory
			byte[] buffer = new byte[1024];
			// bytes of reading
			@SuppressWarnings("unused")
			int nread = 0;
			InputStream inputStream = medium.getStreamData();
			// FileInputStream fileInputStream = (FileInputStream) inputStream;
			FileOutputStream fileOutputStream = new FileOutputStream(nomPiece);
			while ((nread = inputStream.read(buffer)) != -1) {
				// write to server restore
				fileOutputStream.write(buffer);
			}
			// finish writing
			fileOutputStream.flush();
			fileOutputStream.close();
		} catch (Exception e) {
			// handle ERROR
			e.printStackTrace();
			boolUpload = false;
		}
		return boolUpload;
	}

	public static org.zkoss.util.media.Media uploadPieceDossier1(String dgrCode, String pijcode,
			String cheminDossierPieces) {
		@SuppressWarnings("unused")
		String fichierPieceJointe = null;
		org.zkoss.util.media.Media medium=null;

		try {
			// open select file dialog
			Object media = Fileupload.get();
			if (media instanceof org.zkoss.util.media.Media) {
				 medium = (org.zkoss.util.media.Media) media;

				// get file name
				String nomPiece = cheminDossierPieces  + dgrCode + "_"
				+ pijcode + "_" + medium.getName();

//				String nomPiece = cheminDossierPieces + "/" + dgrCode + "_"
//						+ pijcode + "_" + medium.getName();

				// upload file
				boolean boolUpload = uploadFile(medium, nomPiece);
				if (boolUpload) {
					// remember file name
					fichierPieceJointe = dgrCode + "_" + pijcode + "_"
							+ medium.getName()+"."+medium.getFormat();
				} else {
					System.out.print(Labels.getLabel("atlantis.clientele.receptiondossier.joindrepieceerror"));
				}
			}

		} catch (Exception e) {
			// handle ERROR
			e.printStackTrace();
		}
		return medium;
	}
	
	public static String uploadPieceJointe(String dgrCode, String pijcode,
			String cheminDossierPieces,Media medium) {
		String fichierPieceJointe = null;

		try {
			

				// get file name
				String nomPiece = cheminDossierPieces  + dgrCode + "_"
				+ pijcode + "_" + medium.getName();

//				String nomPiece = cheminDossierPieces + "/" + dgrCode + "_"
//						+ pijcode + "_" + medium.getName();

				// upload file
				boolean boolUpload = uploadFile(medium, nomPiece);
				if (boolUpload) {
					// remember file name
					fichierPieceJointe = dgrCode + "_" + pijcode + "_"
							+ medium.getName();
				} else {
					System.out
							.print(Labels
									.getLabel("atlantis.clientele.receptiondossier.joindrepieceerror"));
				}
			

		} catch (Exception e) {
			// handle ERROR
			e.printStackTrace();
		}
		return fichierPieceJointe;
	}
	
	
}
