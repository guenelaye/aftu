package sn.ssi.kermel.web.core.utils;

import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;



public class StatesHashMapLoader 
{
	private static HashMap<String, Object> exportState(Element element)
	{
		HashMap<String, Object> data = new HashMap<String, Object>();
		if(element.getName().equals("state"))
		{
			Iterator iterator = element.elementIterator("zone-content");
			while(iterator.hasNext())
			{
				try {
					Element child = (Element) iterator.next();
					
						String zone = child.attributeValue("zone");
						String content = child.attributeValue("content");
						String popup = child.attributeValue("popup");
						HashMap<String, Object> stateData = new HashMap<String, Object>();
						stateData.put("content", content);
						stateData.put("popup", Boolean.parseBoolean(popup));
						data.put(zone, stateData);
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return data;
	}
	
	private static HashMap<String, Object> exportStates(Element element)
	{
		HashMap<String, Object> data = new HashMap<String, Object>();
		if(element.getName().equals("states"))
		{
			Iterator iterator = element.elementIterator("state");
			while(iterator.hasNext())
			{
				try {
						Element child = (Element) iterator.next();					
						String id = child.attributeValue("id");
						data.put(id, exportState(child));
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return data;
	}
	
	public static HashMap<String, Object> loadApplicationStates(URL url) throws DocumentException
	{
		HashMap<String, Object> data = new HashMap<String, Object>();
		
		 SAXReader reader = new SAXReader();
	     Document document = reader.read(url);

	     data = exportStates(document.getRootElement());
				
		return data;
	}
}
