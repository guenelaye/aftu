package sn.ssi.kermel.web.core.utils;

import java.util.Map;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class ErreurController extends AbstractWindow implements AfterCompose,
		EventListener {

	public static String TITRE_PAGE;
	public static String ERREUR;
	public static String ACTION;
	public static String DETAILS;

	public static String TITRE_DETAILS;
	public static String CLASSE_DETAILS;
	public static String MESSAGE;
	public static String CONTACT;

	private Label titrePage, erreur, TitreDetails, classeDetails, lblaAction,
			lblDetailErreur, message, contact;
	private Textbox errorDescription;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

	}

	public void onCreate(CreateEvent e) {
		Map<String, Object> map = (Map<String, Object>) e.getArg();

		titrePage.setValue(TITRE_PAGE);
		erreur.setValue(ERREUR);
		lblaAction.setValue(ACTION);
		TitreDetails.setValue(TITRE_DETAILS);
		classeDetails.setValue(CLASSE_DETAILS);
		lblDetailErreur.setValue(DETAILS);
		// message.setValue(MESSAGE);
		contact.setValue(CONTACT);

		/*
		 * titrePage.setValue(TITRE_PAGE); erreur.setValue(ERREUR);
		 * lblaAction.setValue(ACTION); TitreDetails.setValue(TITRE_DETAILS);
		 * classeDetails.setValue(CLASSE_DETAILS);
		 * lblDetailErreur.setValue(DETAILS); message.setValue(MESSAGE);
		 * contact.setValue(CONTACT);
		 */

	}

	@Override
	public void onEvent(Event event) throws Exception {
		// TODO Auto-generated method stub

	}

}
