package sn.ssi.kermel.web.core.utils;

//Importation des packages dont on va se servir
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.PklEnveloppesFichiers;
import sn.ssi.kermel.be.entity.PklPlis;
import sn.ssi.kermel.be.entity.PklPlisEnveloppes;
import sn.ssi.kermel.be.passationsmarches.ejb.SoumissionElectroniqueSession;

public class MyZip{

    /**
     * d�compresse le fichier zip dans le r�pertoire donn�
     * @param folder le r�pertoire o� les fichiers seront extraits
     * @param zipfile le fichier zip � d�compresser
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void unzip(File zipfile, File folder) throws FileNotFoundException, IOException{

        // cr�ation de la ZipInputStream qui va servir � lire les donn�es du fichier zip
        ZipInputStream zis = new ZipInputStream(
                new BufferedInputStream(
                        new FileInputStream(zipfile.getCanonicalFile())));

        // extractions des entr�es du fichiers zip (i.e. le contenu du zip)
        ZipEntry ze = null;
        try {
            while((ze = zis.getNextEntry()) != null){

                // Pour chaque entr�e, on cr�e un fichier
                // dans le r�pertoire de sortie "folder"
                File f = new File(folder.getCanonicalPath(), ze.getName());
           
                // Si l'entr�e est un r�pertoire,
                // on le cr�e dans le r�pertoire de sortie
                // et on passe � l'entr�e suivante (continue)
                if (ze.isDirectory()) {
                    f.mkdirs();
                    continue;
                }
               
                // L'entr�e est un fichier, on cr�e une OutputStream
                // pour �crire le contenu du nouveau fichier
                f.getParentFile().mkdirs();
                OutputStream fos = new BufferedOutputStream(
                        new FileOutputStream(f));
           
                // On �crit le contenu du nouveau fichier
                // qu'on lit � partir de la ZipInputStream
                // au moyen d'un buffer (byte[])
                try {
                    try {
                        final byte[] buf = new byte[8192];
                        int bytesRead;
                        while (-1 != (bytesRead = zis.read(buf)))
                            fos.write(buf, 0, bytesRead);
                    }
                    finally {
                        fos.close();
                    }
                }
                catch (final IOException ioe) {
                    // en cas d'erreur on efface le fichier
                    f.delete();
                    throw ioe;
                }
            }
        }
        finally {
            // fermeture de la ZipInputStream
            zis.close();
        }
    }
    
    public static void enregistrer(String path, Object parent){	
    	File file = new File(path);
        File[] files = file.listFiles();
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory() == true) {
                    System.out.println("Dossier" + files[i].getAbsolutePath());
                     
                      PklPlisEnveloppes  enveloppe= new PklPlisEnveloppes();
                      enveloppe.setLibelle(files[i].getName());
                      enveloppe.setPlis((PklPlis)parent);
                      enveloppe =BeanLocator.defaultLookup(SoumissionElectroniqueSession.class).saveEnveloppe(enveloppe);
                      
                    //  enregistrement ua niveau de l' entite PKLENvenveloppe 
                    
                    enregistrer(files[i].getAbsolutePath(),enveloppe);  
                    
                       
                   // this.dircount++;
                } else {
                    System.out.println("Fichier" + files[i].getName());
                    // enregistrement du fichier;
                    
                    PklEnveloppesFichiers  fichier= new PklEnveloppesFichiers();
                    fichier.setLibelle(files[i].getName());
                    fichier.setEnveloppe((PklPlisEnveloppes)parent);
                    fichier.setChemin(files[i].getAbsolutePath());
                    BeanLocator.defaultLookup(SoumissionElectroniqueSession.class).saveFichier(fichier);
                
                    
                    //this.filecount++;
                }
               // if (files[i].isDirectory() == true && this.recursivePath == true) {
                //    this.listDirectory(files[i].getAbsolutePath());
               // }
            }
        }
    }
}