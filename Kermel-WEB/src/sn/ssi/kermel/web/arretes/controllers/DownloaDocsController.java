package sn.ssi.kermel.web.arretes.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;

import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Iframe;

import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


public class DownloaDocsController extends AbstractWindow implements AfterCompose, EventListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Iframe idIframe;
	private String url = UIConstants.PATH_PJ_DOSSIER;
	private String nomFichier;
	public static String NOM_FICHIER = "NOMFICHIER";

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

	}

	@Override
	public void onEvent(Event event) throws Exception {

	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent e) {
		Map<String, Object> windowParams = (Map<String, Object>) e.getArg();
		nomFichier = (String) windowParams.get(NOM_FICHIER);
		System.out.println(url + nomFichier);
		
		  
		idIframe.setHeight("500px");
		idIframe.setWidth("100%");

		String filepath = url + nomFichier;
		File f = null;
		if(ToolKermel.isWindows())
		// windows
		  f = new File(filepath.replaceAll("/", "\\\\"));
		// linux
		else
          f = new File(filepath.replaceAll("\\\\", "/"));

		org.zkoss.util.media.AMedia mymedia = null;
		try {
			mymedia = new AMedia(f, null, null);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		if (mymedia != null)
			idIframe.setContent(mymedia);
		else
			idIframe.setSrc("");

	}

	public org.zkoss.util.media.AMedia fetchFile(File file) {

		org.zkoss.util.media.AMedia mymedia = null;
		try {
			mymedia = new AMedia(file, null, null);
			return mymedia;
		} catch (Exception e) {

			e.printStackTrace();
			return null;
		}

	}

}