package sn.ssi.kermel.web.arretes.controllers;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.arrete.ejb.ArretesSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygArretes;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiersTypes;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteContractanteSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;




@SuppressWarnings("serial")
public class ArretesFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,annee,gestion,numavi,page=null;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtarretNum,txtarretText,txtpublier,txtVersionElectronique,txtRechercherAutorite;
	private SygArretes Arretes =new SygArretes();
	private SygDossiersTypes DossiersTypes;
	private SygAutoriteContractante autorite;
	private SygRealisations realisation;
	private SimpleListModel lst;
	private final String ON_LOCALITE = "onLocalite";
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	Long code;
	private Paging pgPagination,pgfiche;
	private Listbox lstListe;
	private String nomFichier,libelledossiers;
	private final String cheminDossier = UIConstants.PATH_ARRETE;
	private Bandbox bandAutorite;
	private Component errorcomponent;
	private String errorMessage;
	private Label lbStatusBar;
	private ArrayList<String> listValeursAnnees;
	UtilVue utilVue = UtilVue.getInstance();
	private int activePage;
	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		
//		annee = UtilVue.getInstance().anneecourant(new Date());
//		listValeursAnnees = new ArrayList<String>();
//		for (int i = 2007; i <= Integer.parseInt(annee) ; i++) {
//			listValeursAnnees.add(i+"");
//		}
//		list.setModel(new SimpleListModel(listValeursAnnees));
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(WINDOW_PARAM_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WIDOW_CODE);
			Arretes = BeanLocator.defaultLookup(ArretesSession.class).findById(code);
			
			autorite = BeanLocator.defaultLookup(AutoriteContractanteSession.class).findById(Arretes.getAutorite().getId());
			bandAutorite.setValue(Arretes.getArretText());

			txtarretNum.setValue(Arretes.getArretNum());
			txtpublier.setValue(String.valueOf(Arretes.getArretPub()));
			txtarretText.setValue(Arretes.getArretText());
			//txtVersionElectronique.setValue(fichdossiertype.getNomfichierdossiertype());
			
			/*
			 * recuperation de la date pour lors de la modification
			 * */
//				for (int i = 0; i < list.getModel().getSize(); i++) {
//					String codes = (String) list.getListModel().getElementAt(i);
//					System.out.println(codes);
//					if(gestion.equalsIgnoreCase(codes)){
//					((Listitem)list.getItemAtIndex(i)).setSelected(true);	
//					cbannee.setValue(codes);
//					}
//			}
			
		}
			
//			Utilisateur		utilisateur = (Utilisateur) getHttpSession().getAttribute("utilisateur");
//			autorite = BeanLocator.defaultLookup(AutoriteContractanteSession.class).findById(utilisateur.getAutorite().getId());
//			
	}
	public void onOK() {
		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW))
			Arretes=new SygArretes();
		    Arretes.setArretText(txtRechercherAutorite.getValue());
	        Arretes.setArretPub(0);
		    Arretes.setArretNum(txtarretNum.getValue());
		    Arretes.setAutorite(autorite);
			
			if (lstListe.getSelectedItem() == null) {
				errorMessage = "Veuillez selectionner un Pays!";
				errorcomponent = bandAutorite;
				throw new WrongValueException(errorcomponent, errorMessage);
	
			}
			Arretes.setAutorite(((SygAutoriteContractante) lstListe.getSelectedItem().getValue()));
			
		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			BeanLocator.defaultLookup(ArretesSession.class).save(Arretes);

		} 
		else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			BeanLocator.defaultLookup(ArretesSession.class).update(Arretes);
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();

	}
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	
	}
//	@Override
//	public void render(Listitem arg0, Object arg1) throws Exception {
//		// TODO Auto-generated method stub
//		
//	}
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		// TODO Auto-generated method stub
		SygAutoriteContractante autorite = (SygAutoriteContractante) data;
		item.setValue(autorite);
		item.setAttribute("libelle", autorite.getDenomination());
		addCell(item, autorite.getDenomination());
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			if (autorite.getId() == this.autorite.getId())
				item.setSelected(true);
		}

}
	public void onSelect$lstListe() {
		bandAutorite.setValue(lstListe.getSelectedItem().getAttribute("libelle").toString());
		bandAutorite.close();
	}

	public void onClick$btnRechercherAutorite() {
		if (!txtRechercherAutorite.getValue().equals("") && !txtRechercherAutorite.getValue().equals("libelle")) {
			libelledossiers = txtRechercherAutorite.getValue();
		} else {
			libelledossiers = null;
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	public void onOK$txtRechercherAutorite() {
		onClick$btnRechercherAutorite();
	}

	public void onFocus$txtRechercherAutorite() {
		txtRechercherAutorite.setValue("");
	}
//	@Override
//	public void onEvent(Event arg0) throws Exception {
//		// TODO Auto-generated method stub
//		
//	}
	@Override
	public void onEvent(Event event) throws Exception {
		// TODO Auto-generated method stub
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			List<SygAutoriteContractante> autorite = BeanLocator.defaultLookup(AutoriteContractanteSession.class).find(activePage, byPage, null,null,null,null, null);
			lstListe.setModel(new SimpleListModel(autorite));
			pgPagination.setTotalSize(BeanLocator.defaultLookup(AutoriteContractanteSession.class).count(null,null,null,null, null));
		}

	}

	
		public void onClick$btnChoixFichier() {
			//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
				if (ToolKermel.isWindows())
					nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
				else
					nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

				txtVersionElectronique.setValue(nomFichier);
			}
}