package sn.ssi.kermel.web.arretes.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.arrete.ejb.ArretesSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.dossiertype.ejb.FichDossierTypeSession;
import sn.ssi.kermel.be.entity.SygArretes;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;



@SuppressWarnings("serial")
public class ListesArretesController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox list;
	private Paging pgarrete;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private String sygmlibelletype,nomFichier;
	private Listheader libelledossiers,Typedossier;
	private Textbox txtlibelletype,txtTypedossier,txtVersionElectronique,txtarretNum;
	Long code;
	public String extension,images;
	private Menuitem ADD_ARRETES, MOD_ARRETES, SUPP_ARRETES,WPUBLIER_ARRETES,WDEPUBLIER_ARRETES;
	 private KermelSousMenu monSousMenu;
	 private Integer Publier;
	 UtilVue utilVue = UtilVue.getInstance();
	 private Utilisateur infoscompte;
	 private final String cheminDossier = UIConstants.PATH_DENOCIATION;
		private SygAutoriteContractante autorite=null;
	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.ARRETES);
		monSousMenu.afterCompose();
		
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		Components.wireFellows(this, this);
		if (ADD_ARRETES != null)
			ADD_ARRETES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
		if (MOD_ARRETES != null)
			MOD_ARRETES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
		if (SUPP_ARRETES != null)
			SUPP_ARRETES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
		if (WPUBLIER_ARRETES != null)
			WPUBLIER_ARRETES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_PUBLIER);
		
		if (WDEPUBLIER_ARRETES != null){
			WDEPUBLIER_ARRETES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DEPUBLIER);
			WDEPUBLIER_ARRETES.setVisible(false);
		}
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_PUBLIER, this);
		addEventListener(ApplicationEvents.ON_DEPUBLIER, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
 
		list.setItemRenderer(this);

		pgarrete.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pgarrete.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		
	}

	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 List<SygArretes> Arretes = BeanLocator.defaultLookup(ArretesSession.class).find(pgarrete.getActivePage()*byPage,byPage,null,null);
			 SimpleListModel listModel = new SimpleListModel(Arretes);
			list.setModel(listModel);
			pgarrete.setTotalSize(BeanLocator.defaultLookup(ArretesSession.class).count(null,null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/arretes/formarret.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.arret"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ArretesFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/arretes/formarret.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ArretesFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(ArretesFormController.PARAM_WIDOW_CODE, list
					.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				// de
				// fixer
				// les
				// dimenisons
				// du
				// popup
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// de
				// passer
				// des
				// parametres
				// au
				// popup
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < list.getSelectedCount(); i++) {
					Integer codes = (Integer)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					System.out.println(codes);
				BeanLocator.defaultLookup(FichDossierTypeSession.class).delete(codes);
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_PUBLIER)){
				if (list.getSelectedItem()==null) 
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				 for (int i = 0; i < list.getSelectedCount(); i++) {
					 Long code=(Long) ((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					 SygArretes	Arretes= BeanLocator.defaultLookup(ArretesSession.class).findById(code);
					 Arretes.setArretPub(0);
					 //fichdossiertype.setDatepublication(new Date());
			      	   BeanLocator.defaultLookup(ArretesSession.class).update(Arretes);
			      	   
				 }
				 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DEPUBLIER)){
				if (list.getSelectedItem()==null) 
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				 for (int i = 0; i < list.getSelectedCount(); i++) {
					 Long code=(Long) ((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					 SygArretes	Arretes= BeanLocator.defaultLookup(ArretesSession.class).findById(code);
					 Arretes.setArretPub(1);
					// fichdossiertype.setDatepublication(null);
			      	   BeanLocator.defaultLookup(ArretesSession.class).update(Arretes);
			      	   
				 }
				 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	
	}
	

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render( Listitem item,  Object data, int index) throws Exception {
		SygArretes Arretes = (SygArretes) data;
		item.setValue(Arretes.getId());
		item.setAttribute("Publier", Arretes.getArretPub());
		
		Listcell cellArretNum = new Listcell(Arretes.getArretNum());
		cellArretNum.setParent(item);
		
		Listcell cellAutorite = new Listcell(Arretes.getAutorite().getDenomination());
		cellAutorite.setParent(item);
		
//		Listcell cellArretText = new Listcell(Arretes.getArretText());
//		cellArretText.setParent(item);
		
//		Listcell cellannee = new Listcell(fichdossiertype.get);
//		cellannee.setParent(item);
//		Listcell celldatepublication = new Listcell("");
//		if(avi.getDatepublication()!=null)
//		celldatepublication.setLabel(UtilVue.getInstance().formateLaDate2(avi.getDatepublication()));
//		celldatepublication.setParent(item);
		
		
		
		  Listcell cellimage = new Listcell();
			
			if (Arretes.getArreteFichier()!=null)  
			extension=Arretes.getArreteFichier().substring(Arretes.getArreteFichier().length()-3,  Arretes.getArreteFichier().length());
			else extension="";
			 if(extension.equalsIgnoreCase("pdf"))
				 images="/images/icone_pdf.png";
			 else  
				 images="/images/word.jpg";

			 cellimage.setImage(images);
			 cellimage.setParent(item);
		Listcell cellArreteFichier= new Listcell(Arretes.getArreteFichier());
		cellArreteFichier.setParent(item);
		 
	}
	
	public void onOK$txtarretNum() {
		onClick$btnRechercher();
	}
	public void onClick$btnRechercher() {
		
		
		if (txtarretNum.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.arretNum")) || txtarretNum.getValue().equals("")) {
			sygmlibelletype = null;
		} else {
			sygmlibelletype  = txtarretNum.getValue();
		}	

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	public void onFocus$txtnumero() {
				if (txtarretNum.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.arretNum"))) {
					txtarretNum.setValue("");

				}
		}
				public void onBlur$txtlibelletype() {
					if (txtarretNum.getValue().equalsIgnoreCase("")) {
						txtarretNum.setValue(Labels.getLabel("kermel.referentiel.common.arretNum"));
					}	
			}
				public void onSelect$list(){
					Publier=Integer.parseInt(list.getSelectedItem().getAttribute("Publier").toString());
					if(Publier.equals ("non"))
						
					{
						SUPP_ARRETES.setVisible(true);
						WPUBLIER_ARRETES.setVisible(true);
						WDEPUBLIER_ARRETES.setVisible(false);
					}
					else 
					{
						SUPP_ARRETES.setVisible(false);
						WPUBLIER_ARRETES.setVisible(false);
						WDEPUBLIER_ARRETES.setVisible(true);
					}
					}
					
				
}
