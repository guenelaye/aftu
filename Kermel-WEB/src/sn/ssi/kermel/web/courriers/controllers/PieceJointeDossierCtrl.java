package sn.ssi.kermel.web.courriers.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygCourrierAc;
import sn.ssi.kermel.be.entity.SygFilesAc;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.traitementdossier.ejb.FilesAcSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class PieceJointeDossierCtrl extends AbstractWindow implements
AfterCompose, ListitemRenderer,org.zkoss.zk.ui.event.EventListener {

    private Listbox lstFiles;
    public static final String TODO = "TODO";
    public static final String LIST_FILES_MODEL_CHANGE="onFilesModelChange";
    public static final String CURRENT = "CURRENT";
    Session session = getHttpSession();
    SygTypeAutoriteContractante type=new SygTypeAutoriteContractante();
    private String mode ;
    private Paging pgPieces;
    private String errorMsg;
    private static final String ERROR_MSG_STYLE = "color:red";
    private static final String READY_MSG_STYLE = "";
    private Component errorComponent;
    private SygFilesAc filesAc;
    private Textbox txtObjetFile;
    private Textbox txtFichiersrc;
     private Label lblDenomination, lblAdresse,lblTel, lblFax, lblMail;
    private Label lblStatusbar2;
    private SygCourrierAc courrier;
    private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	private Iframe idIframe;
	private Div step0,step1;
	private final String cheminDossier = UIConstants.PATH_PJ;
	private String nomFichier;
    
    @Override
    public void render(Listitem arg0, Object arg1, int index) throws Exception {
	// TODO Auto-generated method stub

    }

    @Override
    public void afterCompose() {
	Components.wireFellows(this, this);
	Components.addForwards(this, this);
	addEventListener(LIST_FILES_MODEL_CHANGE, this);
	addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	lstFiles.setItemRenderer(new FilesRefRender());
	pgPieces.setPageSize(byPage);
	pgPieces.addForward("onPaging", this, LIST_FILES_MODEL_CHANGE);
	courrier = (SygCourrierAc) session.getAttribute("courrierAcInssession");
	Events.postEvent(LIST_FILES_MODEL_CHANGE, this, null);
	addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
    }



    @Override
    public void onEvent(Event event) throws Exception {
	if(event.getName().equalsIgnoreCase(LIST_FILES_MODEL_CHANGE)){
	    List<SygFilesAc>  filesAcs = BeanLocator.defaultLookup(FilesAcSession.class).findByCourrier(pgPieces.getActivePage() * byPage, byPage,courrier.getCourrierId());
	    SimpleListModel listModel = new SimpleListModel(filesAcs);
	    lstFiles.setModel(listModel);
	    pgPieces.setTotalSize(BeanLocator.defaultLookup( FilesAcSession.class).count( courrier.getCourrierId()));
	 }
	if (event.getName().equalsIgnoreCase(Events.ON_CLICK)) {
		Listcell cell = (Listcell) event.getTarget();
		// String toDo = (String) cell.getAttribute(UIConstants.TODO);				
		Listcell button = (Listcell) event.getTarget();
		String toDo = (String) button.getAttribute(UIConstants.TODO);
		SygFilesAc fileAc = BeanLocator.defaultLookup(FilesAcSession.class).findById((Long)cell.getAttribute(UIConstants.ATTRIBUTE_CODE));
		if (toDo.equalsIgnoreCase("openFile")){
			String nomfichier=(String) fileAc.getFileSrc();
			step0.setVisible(false);
			step1.setVisible(false);
			
			String filepath = cheminDossier +  nomfichier;
			File f = new File(filepath.replaceAll("\\\\", "/"));

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(f, null, null);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (mymedia != null)
				idIframe.setContent(mymedia);
			else
				idIframe.setSrc("");

			idIframe.setHeight("600px");
			idIframe.setWidth("95%");
		}else if (toDo.equalsIgnoreCase("delLineFiles")){
			HashMap<String, String> display = new HashMap<String, String>();
			display.put(MessageBoxController.DSP_MESSAGE, Labels
					.getLabel("kermel.common.form.question.supprimer")
					+ " ?");
			display.put(MessageBoxController.DSP_TITLE, Labels
					.getLabel("kermel.courrier.file.supprimer.title"));
			display.put(MessageBoxController.DSP_HEIGHT, "150px");
			display.put(MessageBoxController.DSP_WIDTH, "100px");
			HashMap<String, Object> data = new HashMap<String, Object>();
			data
			.put(CURRENT, cell
					.getAttribute(UIConstants.ATTRIBUTE_CODE));
			data.put(TODO, cell.getAttribute(UIConstants.TODO));
			showMessageBox(display, data);
		}				
	}


	else if (event.getName().equals(
		ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {

	    Long code = (Long) ((HashMap<String, Object>) event.getData())
		    .get(CURRENT);
	    String toDo = (String) ((HashMap<String, Object>) event.getData())
		    .get(TODO);

	    if (toDo != null && toDo.equalsIgnoreCase("delLineFiles")) {

		BeanLocator.defaultLookup(FilesAcSession.class)
		.delete(code);
		Events.postEvent(LIST_FILES_MODEL_CHANGE, this, null);
		initFormAll();
	    }
	}
    }

   private class FilesRefRender implements ListitemRenderer, EventListener{

	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
	    SygFilesAc filesAc = (SygFilesAc)data;
	    item.setValue(filesAc);

	   

	  
	    Listcell objectFile = new Listcell("");
	    if(filesAc.getObjectFile()!=null){
		objectFile = new Listcell(filesAc.getObjectFile());
	    }
	    objectFile.setParent(item);

	    Listcell nomFile = new Listcell(filesAc.getFileSrc());
	    nomFile.setParent(item);
	    
	    Listcell cellSupFile = new Listcell("", "/images/delete.png");
	    cellSupFile.addEventListener(Events.ON_CLICK, this);
	    // cellSupLigneDevis.addEventListener(Events.ON_CLICK,
	    // (EventListener) this);
	    cellSupFile.setAttribute(UIConstants.TODO, "delLineFiles");
	    cellSupFile.setAttribute(UIConstants.ATTRIBUTE_CODE,
		    filesAc.getId());
	    cellSupFile.setParent(item);

	    Listcell cellOpenFile = new Listcell("", "/images/PaperClip-16x16.png");
	    cellOpenFile.addEventListener(Events.ON_CLICK, this);
	    // cellSupLigneDevis.addEventListener(Events.ON_CLICK,
	    // (EventListener) this);
	    cellOpenFile.setAttribute(UIConstants.TODO, "openFile");
	    cellOpenFile.setAttribute(UIConstants.ATTRIBUTE_CODE,
		    filesAc.getId());
	    cellOpenFile.setParent(item);


	}

	@Override
	public void onEvent(Event event) throws Exception {
		 if(event.getName().equalsIgnoreCase(LIST_FILES_MODEL_CHANGE)){
			List<SygFilesAc>  filesAcs = BeanLocator.defaultLookup(FilesAcSession.class).findByCourrier(0, -1, courrier.getCourrierId());
			SimpleListModel listModel = new SimpleListModel(filesAcs);
			lstFiles.setModel(listModel);
		}
	 else if (event.getName().equalsIgnoreCase(Events.ON_CLICK)) {
				Listcell cell = (Listcell) event.getTarget();
				// String toDo = (String) cell.getAttribute(UIConstants.TODO);				
				Listcell button = (Listcell) event.getTarget();
				String toDo = (String) button.getAttribute(UIConstants.TODO);
				SygFilesAc fileAc = BeanLocator.defaultLookup(FilesAcSession.class).findById((Long)cell.getAttribute(UIConstants.ATTRIBUTE_CODE));
				if (toDo.equalsIgnoreCase("openFile")){
					String nomfichier=(String) fileAc.getFileSrc();
					step0.setVisible(false);
					step1.setVisible(true);
					String filepath = cheminDossier +  nomfichier;
					File f = new File(filepath.replaceAll("\\\\", "/"));

					org.zkoss.util.media.AMedia mymedia = null;
					try {
						mymedia = new AMedia(f, null, null);
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					if (mymedia != null)
						idIframe.setContent(mymedia);
					else
						idIframe.setSrc("");

					idIframe.setHeight("600px");
					idIframe.setWidth("95%");
				}else if (toDo.equalsIgnoreCase("delLineFiles")){
					HashMap<String, String> display = new HashMap<String, String>();
					display.put(MessageBoxController.DSP_MESSAGE, Labels
							.getLabel("kermel.common.form.question.supprimer")
							+ " ?");
					display.put(MessageBoxController.DSP_TITLE, Labels
							.getLabel("kermel.courrier.file.supprimer.title"));
					display.put(MessageBoxController.DSP_HEIGHT, "150px");
					display.put(MessageBoxController.DSP_WIDTH, "100px");
					HashMap<String, Object> data = new HashMap<String, Object>();
					data
					.put(CURRENT, cell
							.getAttribute(UIConstants.ATTRIBUTE_CODE));
					data.put(TODO, cell.getAttribute(UIConstants.TODO));
					showMessageBox(display, data);
				}				
			}
	   else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			
		}
		}
	
	}
   

    private void setLineFile() {
	filesAc.setObjectFile(txtObjetFile.getValue());
	filesAc.setFileSrc(txtFichiersrc.getValue());
	filesAc.setCourrier(courrier);
    }

    private void initFormFile() {
	txtFichiersrc.setValue("");
	txtObjetFile.setValue("");

    }

    private void chargeFormAcc(SygFilesAc filesAc) {
	txtFichiersrc.setValue(filesAc.getFileSrc());
	txtObjetFile.setValue(filesAc.getObjectFile());
	this.filesAc = filesAc;

    }

    public void onSelect$listeAccompagnant() {
	filesAc = (SygFilesAc) lstFiles.getSelectedItem().getValue();

	if (filesAc != null) {
	    chargeFormAcc(filesAc);
	}
    }



    private boolean isRequiredFieldFile() {
	try {
	    /* Controle champ obligatoire */
	    if (txtObjetFile.getValue() == null || txtObjetFile.getValue().equals("")) {
		errorMsg = Labels
			.getLabel("kermel.file.nouveau.champnomfichierobligatoire");
		errorComponent = txtObjetFile;
		return false;
	    }
	    if (txtFichiersrc.getValue() == null || txtFichiersrc.getValue().equals("")) {
		errorMsg = Labels
			.getLabel("kermel.file.nouveau.champfichiersrcobligatoire");
		errorComponent = txtFichiersrc;
		return false;
	    }
	 
	  

	    return true;
	} catch (Exception e) {

	    return true;
	}

    }

    public void onClick$cellValider3() {

	if (isRequiredFieldFile()) {
	    if (filesAc == null || filesAc.getId() == 0) {
		filesAc = new SygFilesAc();
	    }
	    setFormFile();
	    if (saveFile()) {
		initFormAll();
		Events.postEvent(LIST_FILES_MODEL_CHANGE, this, null);
	    } else {
		lblStatusbar2.setValue(errorMsg);
		lblStatusbar2.setStyle(ERROR_MSG_STYLE);
		throw new WrongValueException(lstFiles, errorMsg);
	    }

	} else {
	    lblStatusbar2.setValue(errorMsg);
	    lblStatusbar2.setStyle(ERROR_MSG_STYLE);
	    throw new WrongValueException(lstFiles, errorMsg);
	}
    }

    private boolean saveFile() {
	try {
	    BeanLocator.defaultLookup(FilesAcSession.class).save(filesAc);
	    return true;
	} catch (RuntimeException runtimeException) {
	    if (runtimeException.getCause().getClass().getPackage().getName()
		    .equals("javax.transaction")) {
		lblStatusbar2.setValue(Labels
			.getLabel("atlantis.common.erreur.commit"));
		lblStatusbar2.setStyle(ERROR_MSG_STYLE);
	    }
	    return false;
	} catch (Exception e) {
	    lblStatusbar2.setValue(e.toString());

	    return false;
	}
    }

    private void setFormFile() {

	filesAc.setCourrier(courrier);
	filesAc.setFileSrc(txtFichiersrc.getValue());
	filesAc.setObjectFile(txtObjetFile.getValue());
    }

    private void chargeFormAll(SygFilesAc file) {
	txtFichiersrc.setValue(filesAc.getFileSrc());
	txtObjetFile.setValue(filesAc.getObjectFile());
	
    }

    private void initFormAll() {

	txtFichiersrc.setValue(null);
	txtObjetFile.setValue(null);
    }

    public void onSelect$lstFiles() {

	filesAc =  (SygFilesAc) lstFiles.getSelectedItem().getValue();
	if (filesAc != null) {
	    chargeFormAll(filesAc);
	}
    }

    public void onClick$bcUploadFile(){
		if (ToolKermel.isWindows()) {
			nomFichier = FileLoader.uploadPieceDossier(UtilVue.getInstance().formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
		} else {
			nomFichier = FileLoader.uploadPieceDossier(UtilVue.getInstance().formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));
		}

		txtFichiersrc.setValue(nomFichier);
	}
	
	public void onClick$menuFermer() {
		
		step0.setVisible(true);
		step1.setVisible(false);
		
	}
	
	
   
}
