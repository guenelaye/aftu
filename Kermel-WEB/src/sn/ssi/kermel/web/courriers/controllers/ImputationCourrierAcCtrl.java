package sn.ssi.kermel.web.courriers.controllers;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.courriers.ejb.InstructionSession;
import sn.ssi.kermel.be.courriers.ejb.TCourriersAcSession;
import sn.ssi.kermel.be.entity.SygBureauxdcmp;
import sn.ssi.kermel.be.entity.SygCourrierAc;
import sn.ssi.kermel.be.entity.SygInstruction;
import sn.ssi.kermel.be.entity.SygTCourrierAC;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.BureauxdcmpSession;
import sn.ssi.kermel.be.security.UtilisateurSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class ImputationCourrierAcCtrl extends AbstractWindow implements AfterCompose, EventListener , ListitemRenderer{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode ;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
	Long code;
	private int activePage;
	private String page = null;
	private SygTCourrierAC tcourrier;
	Date date = new Date();
	private Textbox txtInstruction,txtlibelleInstruction ;
	private Textbox txtRechercherService,txtRechercherAgent;
	private Bandbox bdService;
	private Combobox comboImputationPour;
	private Paging pgService,pgInstruction;
	private Listbox lstService,listInstruction;
	private SygBureauxdcmp bureau;
	private String serviceLibelle = null;
	private Bandbox bdAgent;
	private Paging pgAgent;
	private Listbox lstAgent;
	private Utilisateur agent;
	private String agentPrenom = null,instructionLibelle;
	Session session =getHttpSession();
	private SygCourrierAc courrier;
	private Long idOrigne;
	private int nbrcourrierBureau,nbrcourrierAgent;
	private Datebox dbMaxTraitement, dbImputation;
	private Label referenceid,datecourrier,autorite,datereception;
	private Groupbox gbInstruction;
	private Checkbox chknotifMail,chkaccusRecept;
	//private SygInstruction ;
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION1, this);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
		listInstruction.setItemRenderer(this);
		pgInstruction.setPageSize(byPageBandBox);
		pgInstruction.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		//Service
		lstService.setItemRenderer(new ServiceRenderer());
		pgService.setPageSize(byPageBandBox);
		pgService.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTRE_ACTION);
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);		
		//Agent
		lstAgent.setItemRenderer(new AgentRenderer());
		pgAgent.setPageSize(byPageBandBox);
		pgAgent.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTRE_ACTION1);		
		idOrigne = ((Utilisateur) session.getAttribute("utilisateur")).getId();
		courrier = (SygCourrierAc) session.getAttribute("courrierAcInssession");
		referenceid.setValue(courrier.getCourrierReference());
		datecourrier.setValue(UtilVue.getInstance().formateLaDate(courrier.getCourrierDate()));
		autorite.setValue(courrier.getCourrierAutoriteContractante().getDenomination());
		datereception.setValue(UtilVue.getInstance().formateLaDate(courrier.getCourrierDateReception()));
			
	}
	
	@Override
	public void onEvent(Event event) throws Exception {
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)){
			//Service
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandBox = 3;//UIConstants.DSP_BANDBOX_BY_PAGE;				
				pgInstruction.setPageSize(byPageBandBox);			
			}
			else {
				byPageBandBox = 3;//UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgInstruction.getActivePage() * byPageBandBox;
				pgInstruction.setPageSize(byPageBandBox);
			}			
			List<SygInstruction> instructions = BeanLocator.defaultLookup(InstructionSession.class).find(activePage, byPageBandBox, instructionLibelle);
			listInstruction.setModel(new SimpleListModel(instructions));
			pgInstruction.setTotalSize(BeanLocator.defaultLookup(InstructionSession.class).count(instructionLibelle));
		}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)){
			//Service
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;				
				pgService.setPageSize(byPageBandBox);			
			}
			else {
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgService.getActivePage() * byPageBandBox;
				pgService.setPageSize(byPageBandBox);
			}			
			List<SygBureauxdcmp> services = BeanLocator.defaultLookup(BureauxdcmpSession.class).findRech(activePage, byPageBandBox, serviceLibelle, null, null);
			lstService.setModel(new SimpleListModel(services));
			pgService.setTotalSize(BeanLocator.defaultLookup(BureauxdcmpSession.class).countRech(serviceLibelle, null,null));
		}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION1)){			
			//Agent
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;				
				pgService.setPageSize(byPageBandBox);			
			}
			else {
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgAgent.getActivePage() * byPageBandBox;
				pgAgent.setPageSize(byPageBandBox);
			}
			List<Utilisateur> depart = BeanLocator.defaultLookup(UtilisateurSession.class).findByBureau(activePage, byPageBandBox, null, bureau);
			lstAgent.setModel(new SimpleListModel(depart));
			pgAgent.setTotalSize(BeanLocator.defaultLookup(UtilisateurSession.class).countUtilByBureau(null, bureau));
			
		}
	}
	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		mode = (String) Executions.getCurrent().getAttribute("MODE");
		gbInstruction.setVisible(true);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			tcourrier = (SygTCourrierAC)Executions.getCurrent().getAttribute("TCOURRIER");//map.get(PARAM_WIDOW_CODE);
			bdService.setValue(tcourrier.getBureau().getLibelle()+" ");
			bdAgent.setValue(tcourrier.getAgent().getPrenom());				
			txtInstruction.setValue(tcourrier.getTcourrierInstruction());	
			dbImputation.setValue(tcourrier.getTdateImputation());
			dbMaxTraitement.setValue(tcourrier.getTdateMaxTraitement());
			agent=tcourrier.getAgent();
			bureau	= tcourrier.getBureau();
			if(tcourrier.isAccuseReception())
				chkaccusRecept.setChecked(true);
			else
				chkaccusRecept.setChecked(false);
			
			if(tcourrier.isNotificationParMail())
				chknotifMail.setChecked(true);
			else
				chknotifMail.setChecked(false);
			
			if(tcourrier.getTimputationPour()==null ){
				comboImputationPour.setValue("Traitement");
			}else {
				comboImputationPour.setValue(tcourrier.getTimputationPour());	
			}
			
		}else{
			comboImputationPour.setValue("Traitement");
			dbImputation.setValue(new Date());
			dbMaxTraitement.setValue(ToolKermel.dateDecalageJour(dbImputation.getValue(), courrier.getCourrierType().getDureTraitementJour()));
		}
		
	}

	public void onBlur$dbImputation(){
		 if(dbImputation.getValue()!=null){
			 dbMaxTraitement.setValue(ToolKermel.dateDecalageJour(dbImputation.getValue(), courrier.getCourrierType().getDureTraitementJour()));
		 }
		
	 }
	public boolean verifField(){
		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			SygTCourrierAC tcourr = BeanLocator.defaultLookup(TCourriersAcSession.class).findByCourrierAndAgent(courrier, agent) ;
			if(tcourr!=null){
				throw new WrongValueException(bdAgent, "Ce courrier est d�j� imput� �  "+agent.getPrenom()+" "+agent.getNom());
			}			
		}
		return true;
	}
	public void onOK() {	
		
		if(verifField()){
			if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				tcourrier = new SygTCourrierAC();
				tcourrier.setAgent(agent);
				tcourrier.setBureau(bureau);
				tcourrier.setCourrierac(courrier);
				tcourrier.setIdOrigne(idOrigne);
				tcourrier.setTcourrierDateSaisie(new Date());
				tcourrier.setTdateImputation(dbImputation.getValue());
				tcourrier.setTdateMaxTraitement(dbMaxTraitement.getValue());
				tcourrier.setTcourrierInstruction(txtInstruction.getValue());
				if(chkaccusRecept.isChecked())
					tcourrier.setAccuseReception(true);
				else
					tcourrier.setAccuseReception(false);
				
				if(chknotifMail.isChecked())
					tcourrier.setNotificationParMail(true);
				else
					tcourrier.setNotificationParMail(false);
				
				BeanLocator.defaultLookup(TCourriersAcSession.class).save(tcourrier);
				String subject = "Iputation courrier";
				///////////////////////////////////////////////////
				String content = "Iputation courrier";
				content +="<html>";
				content +="<style type='text/css'>";
				content +="<!--";
				content +=".Style1 {color: #0000FF}";
				content +="-->";
				content +="</style>";
				content +="<body>";
				content +="<table width='788' border='0' align='center' cellpadding='0' cellspacing='0'>"+
				  "<tr>"+
				    "<td height='169'><img src='$url/icone/backA.png' width='788' height='169' /></td>"+
				  "</tr>"+
				  "<tr>"+
				    "<td valign='top' background='$url/icone/BackB.png'><table width='80%' height='194' border='0' align='center' cellpadding='0' cellspacing='0'>"+
				      "<tr>"+
				        "<td width='38%' height='135' valign='top'><img src='$url/icone/env.jpg' width='225' height='138' /></td>"+
				        "<td width='62%' valign='top'>Imputation  $sujet $titre."+
						"<p>Imput� le: $dateimputation</p>"+
						"<p>$veuillez</p>"+
						"<p>"+tcourrier.getTcourrierInstruction()+"</p>"+
						"<p>Cliquer sur le lien ci-dessous pour acceder au courrier</p><p><a href='$url$suiteurl'>Ecourrier</a></p>"+
						"</td>"+
				      "</tr>"+
				      
				    "</table>"+
				    "</td>"+
				  "</tr>"+
				  "<tr>"+
				    "<td height='46' valign='top'><img src='$url/icone/BackC.png' width='788' height='46' /></td>"+
				  "</tr>"+
				"</table>";
				///////////////////////////////////////////////////
				//String content = "Iputation courrier";
				String usermail = agent.getMail();
				//MessageManager.sendMail(subject, content, null, usermail);

			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				tcourrier.setAgent(agent);
				tcourrier.setBureau(bureau);
				tcourrier.setCourrierac(courrier);
				tcourrier.setIdOrigne(idOrigne);
				tcourrier.setTcourrierDateSaisie(courrier.getCourrierDateSaisie());
				tcourrier.setTdateImputation(dbImputation.getValue());
				tcourrier.setTdateMaxTraitement(dbMaxTraitement.getValue());
				tcourrier.setTcourrierInstruction(txtInstruction.getValue());
				if(chkaccusRecept.isChecked())
					tcourrier.setAccuseReception(true);
				else
					tcourrier.setAccuseReception(false);
				
				if(chknotifMail.isChecked())
					tcourrier.setNotificationParMail(true);
				else
					tcourrier.setNotificationParMail(false);
				
				BeanLocator.defaultLookup(TCourriersAcSession.class).update(tcourrier);
			}
			
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}		
	
	}
	public void onSelect$lstService(){
		bureau = (SygBureauxdcmp) lstService.getSelectedItem().getValue();
		bdService.setValue(bureau.getLibelle());		
		bdService.close();
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION1, this, null);

	}
	
	public class ServiceRenderer implements ListitemRenderer {

		public void render(Listitem item, Object data, int index)  throws Exception {
			SygBureauxdcmp bureau = (SygBureauxdcmp) data;
			item.setValue(bureau);
			nbrcourrierBureau=BeanLocator.defaultLookup(TCourriersAcSession.class).countByServiceAndAgent(bureau, null,null);
			Listcell cellLibelle = new Listcell(bureau.getLibelle()+"( nombre de dossier :"+nbrcourrierBureau+" )");
			cellLibelle.setParent(item);

		}
	}
	
	
	public void onFocus$txtRechercherService(){
		 if(txtRechercherService.getValue().equalsIgnoreCase(Labels.getLabel("gred.common.form.rechercher"))) {
			 txtRechercherService.setValue("");
		 }		 
	}
	
	public void  onClick$btnRechercherService(){
		if(txtRechercherService.getValue().equalsIgnoreCase(Labels.getLabel("gred.common.form.rechercher"))) {
			serviceLibelle = null;
			page = null;
		}
		else
		{
			serviceLibelle = txtRechercherService.getValue();
			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}



	
	//Agent
	public void onSelect$lstAgent(){
		 agent = (Utilisateur) lstAgent.getSelectedItem().getValue();
		 bdAgent.setValue(agent.getPrenom()+ " "+agent.getNom());
		 bdAgent.close();
	}
	
	public class AgentRenderer implements ListitemRenderer {
		public void render(Listitem item, Object data, int index)  throws Exception {
			Utilisateur myagent = (Utilisateur) data;
			item.setValue(myagent);
			nbrcourrierAgent=BeanLocator.defaultLookup(TCourriersAcSession.class).countByServiceAndAgent(null, myagent,null);
			Listcell cellLibelle = new Listcell(myagent.getPrenom()+" "+myagent.getNom()+"( nombre de dossier :"+nbrcourrierAgent+" )");
			cellLibelle.setParent(item);

		} 
	}
	
	
	public void onFocus$txtRechercherAgent(){
		 if(txtRechercherAgent.getValue().equalsIgnoreCase(Labels.getLabel("gred.common.form.rechercher"))) {
			 txtRechercherAgent.setValue("");
		 }		 
	}
	
	public void  onClick$btnRechercherAgent(){
		if(txtRechercherAgent.getValue().equalsIgnoreCase(Labels.getLabel("gred.common.form.rechercher"))) {
			agentPrenom = null;
			page = null;
		}
		else
		{
			agentPrenom = txtRechercherAgent.getValue();
			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION1, this, page);
	}

	
	//Instruction
	public void onSelect$listInstruction(){
//		if(txtInstruction.getValue()==null || "".equals(txtInstruction.getValue()))
//			txtInstruction.setValue(((SygInstruction) listInstruction.getSelectedItem().getValue()).getLibelle());
//		else
//			txtInstruction.setValue(";"+((SygInstruction) listInstruction.getSelectedItem().getValue()).getLibelle());
		 
	}
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		// TODO Auto-generated method stub
		SygInstruction instruction = (SygInstruction) data;
		//item.setAttribute(UIConstants.ATTRIBUTE_LIBELLE, instruction.getLibelle());
		item.setValue(instruction.getLibelle());
		Listcell cellLibelle = new Listcell(instruction.getLibelle());
		cellLibelle.setParent(item);
	}
	
	public void onFocus$txtlibelleInstruction(){
		 if(txtRechercherAgent.getValue().equalsIgnoreCase(Labels.getLabel("gred.common.form.rechercher"))) {
			 txtlibelleInstruction.setValue("");
		 }		 
	}
	
	public void  onClick$btnRechercherInst(){
		if(txtlibelleInstruction.getValue().equalsIgnoreCase(Labels.getLabel("gred.common.form.rechercher"))) {
			instructionLibelle = null;
			page = null;
		}
		else
		{
			instructionLibelle = txtlibelleInstruction.getValue();
			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	public void onClick$btnInstruction(){
		if(listInstruction.getSelectedItem()==null) {
			throw new WrongValueException(listInstruction, "Veuillez s�lectionner un �l�ment");
		}else{
			if(txtInstruction.getValue()==null || "".equals(txtInstruction.getValue()))
				txtInstruction.setValue((String) listInstruction.getSelectedItem().getValue());
			else
				txtInstruction.setValue(txtInstruction.getValue()+";"+(String) listInstruction.getSelectedItem().getValue());
		}
		
	}
}
