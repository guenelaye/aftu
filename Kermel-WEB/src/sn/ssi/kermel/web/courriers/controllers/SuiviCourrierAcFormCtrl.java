package sn.ssi.kermel.web.courriers.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;

import sn.ssi.kermel.be.entity.SygCourrierAc;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class SuiviCourrierAcFormCtrl extends AbstractWindow implements AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String PARAM_WINDOW_CODE = "CODE";
	
	private Long code;
	UtilVue utilVue = UtilVue.getInstance();
	private Include pgActes;
	Session session = getHttpSession();
	private SygCourrierAc courrier;
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

	}

	public void onCreate(CreateEvent createEvent) {

		courrier = (SygCourrierAc) session.getAttribute("courrierAcInssession");
		session.setAttribute("courrierAcInssession", courrier);
		pgActes.setSrc("/courriers/dossiersuivi.zul");

	}

	@Override
	public void onEvent(Event event) throws Exception {

	}

	public void onClick$onClose() {
		if(courrier.getNature().equals(UIConstants.COURRIERARRIVE))
		   loadApplicationState("courrier_ac");
		else
		   loadApplicationState("courrier_depart");
	}
}
