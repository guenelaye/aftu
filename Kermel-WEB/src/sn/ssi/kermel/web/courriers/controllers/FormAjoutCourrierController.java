package sn.ssi.kermel.web.courriers.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.courriers.ejb.CourriersAcSession;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCourrierAc;
import sn.ssi.kermel.be.entity.SygFilesAc;
import sn.ssi.kermel.be.entity.SygModeReception;
import sn.ssi.kermel.be.entity.SygModeTraitement;
import sn.ssi.kermel.be.entity.SygNatureCourrier;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.entity.SygTypesCourrier;
import sn.ssi.kermel.be.entity.SygTypesDossiers;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteContractanteSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeAutoriteContractanteSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.be.traitementdossier.ejb.FilesAcSession;
import sn.ssi.kermel.be.traitementdossier.ejb.ModeReceptionSession;
import sn.ssi.kermel.be.traitementdossier.ejb.NatureCourrierSession;
import sn.ssi.kermel.be.traitementdossier.ejb.TypeCourrierSession;
import sn.ssi.kermel.be.traitementdossier.ejb.TypesDossiersSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class FormAjoutCourrierController extends AbstractWindow implements
AfterCompose, ListitemRenderer,org.zkoss.zk.ui.event.EventListener {

	private Div step0;
	private Div step1;
	private Div step2,step3;
	private Listbox lstTypeAc;
	private Listbox lstAutCont;
	private Listbox lstTypeCourrier;
	private Listbox lstTypeNature;
	//private Listbox lstModeTraitement;
	private Listbox lstModereception;
	private Listbox lstFiles;
	private Listbox lstCourrierRef;
	private Bandbox bdTypeAc;
	private Bandbox bdTypeCourrier;
	private Bandbox bdNature;
//	private Bandbox bdModeTraitment;
	private Bandbox bdModereception;
	private Bandbox bdCourrierRef;
	private Paging pgTypeAC; 
	private Paging pgAc,pgPieces; 
	private Paging pgTypeCourrier; 
	private Paging pgNature; 
//	private Paging pgModeTraitment; 
	private Paging pgModeReception; 
	private Paging pgCourrierRef; 
	private String codeTypeAc, libelleTypeAc;
	public static final String TODO = "TODO";
	private String codeAc, libelleAc;
	private String codeNature, libelleNature;
	private String codeType, libelleType;
//	private String codeModeT, libelleModeT;
	private String codeModeR, libelleModeR, denomination;
	private String codeCourrier, LibelleCourrier;
	private Label lblDenomination1,lblreference,lbldatecourrier,lblObjet,lblSigle;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	private final int byPageb = UIConstants.DSP_BANDBOXS_BY_PAGES;
	public static final String LIST_TYPE_AC_MODEL_CHANGE="onTypeACModelChange";
	public static final String LIST_FILES_MODEL_CHANGE="onFilesModelChange";
	public static final String LIST_TYPE_COURRIER_MODEL_CHANGE="onTypeCModelChange";
	public static final String LIST_AC_MODEL_CHANGE="onACModelChange";
	public static final String LIST_NATURE_COURRIER_MODEL_CHANGE="onNatureCModelChange";
	public static final String LIST_MODETRAITEMENT_COURRIER_MODEL_CHANGE="onModeTModelChange";
	public static final String LIST_MODERECPTION_COURRIER_MODEL_CHANGE="onModeRModelChange";
	public static final String LIST_COURRIERREF_COURRIER_MODEL_CHANGE="onCourrierREFModelChange";
	private Listheader lshCode,lshLibelle;
	public static final String CURRENT = "CURRENT";
	Session session = getHttpSession();
	SygTypeAutoriteContractante type=new SygTypeAutoriteContractante();
	private Long idtype;
	private Label lblType;
	private String login;
	private String mode ;
	private Iframe idIframe;
	public static final String LIST_TYPE_DOSSIER_MODEL_CHANGE="onTypeDModelChange";

	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private static final String READY_MSG_STYLE = "";
	private Component errorComponent;
	private SygAutoriteContractante autoriteContractante;
	private SygTypeAutoriteContractante typeAutoriteContractante;
	private SygTypesCourrier typeCourrier;
	private SygNatureCourrier natureCourrier;
	private SygModeReception modeReception;
//	private SygModeTraitement modeTraitement;
	private SygCourrierAc courrier;
	private SygCourrierAc courrierRef;
	private SygFilesAc filesAc;
	private Textbox txtDenomination, txtRechercherTypeAc,txtnumenregostrement;
	private Textbox txtObjetFile, txtRechercherCourrierRef, txtReference, txtObjet, txtAmpliataire;
	private Textbox txtFichiersrc, txtRechercherTypeCourrier, txtRechercherNature, txtRechercherModeReception;//, txtRechercherModeTraitement
	private Datebox dateSaisieFile, dateReception, dateCourrier;
	private Label lblDenomination,lblSigle1;
	private Label lblStatusbar;
	private Label lblStatusbar2;
	private final String cheminDossier = UIConstants.PATH_PJ;
	private String nomFichier;
	private Menuitem menuValiderStep1;
	private Listbox listTypesdossiers, lstPassations;
	private Paging pgTypesdossiers, pgPassation;
	private String sygmlibelle;
	private Bandbox bandTypecourrier, bandTypeDossier;
    private SygTypesDossiers typeDossier;
    Lock lock = new ReentrantLock();
    private Menuitem menuNextStep1;
    private Long codeautorite=null;
	    
	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		addEventListener(LIST_TYPE_AC_MODEL_CHANGE, this);
		addEventListener(LIST_TYPE_COURRIER_MODEL_CHANGE, this);
		addEventListener(LIST_NATURE_COURRIER_MODEL_CHANGE, this);
		addEventListener(LIST_MODETRAITEMENT_COURRIER_MODEL_CHANGE, this);
		addEventListener(LIST_MODERECPTION_COURRIER_MODEL_CHANGE, this);
		addEventListener(LIST_COURRIERREF_COURRIER_MODEL_CHANGE, this);
		addEventListener(LIST_AC_MODEL_CHANGE, this);
		addEventListener(LIST_FILES_MODEL_CHANGE, this);
		addEventListener(Events.ON_UPLOAD, this);
		addEventListener(LIST_TYPE_DOSSIER_MODEL_CHANGE, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		lshCode.setSortAscending(new FieldComparator("code", false));
		lshCode.setSortDescending(new FieldComparator("code", true));
		lshLibelle.setSortAscending(new FieldComparator("libelle", false));
		lshLibelle.setSortDescending(new FieldComparator("libelle", true));

		lstAutCont.setItemRenderer(new AcRender());
		pgAc.setPageSize(byPage);
		pgAc.addForward("onPaging", this, LIST_AC_MODEL_CHANGE);

		lstTypeAc.setItemRenderer(new TypeAcRender());
		pgTypeAC.setPageSize(byPage);
		pgTypeAC.addForward("onPaging", this, LIST_TYPE_AC_MODEL_CHANGE);

		lstTypeCourrier.setItemRenderer(new TypeCourrierRender());
		pgTypeCourrier.setPageSize(byPage);
		pgTypeCourrier.addForward("onPaging", this, LIST_TYPE_COURRIER_MODEL_CHANGE);

		lstTypeNature.setItemRenderer(new NatureCourrierRender());
		pgNature.setPageSize(byPage);
		pgNature.addForward("onPaging", this, LIST_NATURE_COURRIER_MODEL_CHANGE);

//		lstModeTraitement.setItemRenderer(new ModeTraitementRender());
//		pgModeTraitment.setPageSize(byPage);
//		pgModeTraitment.addForward("onPaging", this, LIST_MODETRAITEMENT_COURRIER_MODEL_CHANGE);

		lstModereception.setItemRenderer(new ModeRecptionRender());
		pgModeReception.setPageSize(byPage);
		pgModeReception.addForward("onPaging", this, LIST_MODERECPTION_COURRIER_MODEL_CHANGE);


		lstCourrierRef.setItemRenderer(new CourrierRefRender());
		pgCourrierRef.setPageSize(byPage);
		pgCourrierRef.addForward("onPaging", this, LIST_COURRIERREF_COURRIER_MODEL_CHANGE);

		lstFiles.setItemRenderer(new FilesRefRender());
		pgPieces.setPageSize(byPage);
		pgPieces.addForward("onPaging", this, LIST_FILES_MODEL_CHANGE);
		//pg.setPageSize(byPage);
		//pgCourrierRef.addForward("onPaging", this, LIST_COURRIERREF_COURRIER_MODEL_CHANGE);
		
		listTypesdossiers.setItemRenderer(new TypeDossierRenderer());
		pgTypesdossiers.setPageSize(byPage);
		pgTypesdossiers.addForward("onPaging", this, LIST_TYPE_DOSSIER_MODEL_CHANGE);
		

	}

	public void onCreate(CreateEvent event) {
			login = ((String) getHttpSession().getAttribute("user"));
		//	idtype=(Long) session.getAttribute("idtype");
		//	type=BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).findById(idtype);
		//
		//	if(type!=null){
		//	    lblType.setValue(type.getLibelle());
		//	}
		mode = (String) Executions.getCurrent().getAttribute("MODE");
		if(mode!=null && mode.equalsIgnoreCase(UIConstants.MODE_EDIT)){
			courrier = (SygCourrierAc) Executions.getCurrent().getAttribute("COURRIER");
			if(courrier!=null){
				initFormCourrier(courrier);
			}
		}
		
		Events.postEvent(LIST_AC_MODEL_CHANGE, this, null);
		Events.postEvent(LIST_TYPE_AC_MODEL_CHANGE, this, null);
		Events.postEvent(LIST_TYPE_COURRIER_MODEL_CHANGE, this, null);
		Events.postEvent(LIST_NATURE_COURRIER_MODEL_CHANGE, this, null);
		Events.postEvent(LIST_MODETRAITEMENT_COURRIER_MODEL_CHANGE, this, null);
		Events.postEvent(LIST_MODERECPTION_COURRIER_MODEL_CHANGE, this, null);
		Events.postEvent(LIST_COURRIERREF_COURRIER_MODEL_CHANGE, this, null);
		Events.postEvent(LIST_TYPE_DOSSIER_MODEL_CHANGE, this, null);

	}

	@Override
	public void onEvent(Event event) throws Exception {
		EventQueues.lookup("MyFile", true).subscribe(
				new EventListener() {
					@Override
					public void onEvent(Event evt) {
						if(evt.getName().equalsIgnoreCase("onFilesModelChange")){
							alert(evt.getData().toString());
						}
					}
				});
		if (event.getName().equalsIgnoreCase(LIST_TYPE_AC_MODEL_CHANGE)) {

			List<SygTypeAutoriteContractante> types = BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).find(activePage,byPage,codeTypeAc,libelleTypeAc);
			SimpleListModel listModel = new SimpleListModel(types);
			lstTypeAc.setModel(listModel);
			pgTypeAC.setTotalSize(BeanLocator.defaultLookup( TypeAutoriteContractanteSession.class).count(codeTypeAc,libelleTypeAc));
		}else if(event.getName().equalsIgnoreCase(LIST_TYPE_COURRIER_MODEL_CHANGE)){

			List<SygTypesCourrier>  courrier = BeanLocator.defaultLookup(TypeCourrierSession.class).find(pgTypeCourrier.getActivePage()*byPage,byPage,libelleType,null,null);
			SimpleListModel listModel = new SimpleListModel(courrier);
			lstTypeCourrier.setModel(listModel);
			pgTypeCourrier.setTotalSize(BeanLocator.defaultLookup(TypeCourrierSession.class).count(libelleType));
			
		}else if(event.getName().equalsIgnoreCase(LIST_NATURE_COURRIER_MODEL_CHANGE)){
			List<SygNatureCourrier>  courrier = BeanLocator.defaultLookup(NatureCourrierSession.class).find(pgNature.getActivePage()*byPage,byPage,libelleNature,null,null);
			SimpleListModel listModel = new SimpleListModel(courrier);
			lstTypeNature.setModel(listModel);
			pgNature.setTotalSize(BeanLocator.defaultLookup(NatureCourrierSession.class).count(libelleNature));

		}
//		else if(event.getName().equalsIgnoreCase(LIST_MODETRAITEMENT_COURRIER_MODEL_CHANGE)){
//			List<SygModeTraitement>  traitement = BeanLocator.defaultLookup(ModeTraitementSession.class).find(pgModeTraitment.getActivePage()*byPage,byPage,libelleModeT,null,null);
//			SimpleListModel listModel = new SimpleListModel(traitement);
//			lstModeTraitement.setModel(listModel);
//			pgModeTraitment.setTotalSize(BeanLocator.defaultLookup(ModeTraitementSession.class).count(libelleModeT));
//
//		}
		else if(event.getName().equalsIgnoreCase(LIST_TYPE_DOSSIER_MODEL_CHANGE)){

		    List<SygTypesDossiers>  dossiers = BeanLocator.defaultLookup(TypesDossiersSession.class).find(pgTypesdossiers.getActivePage()*byPage,byPage,null,sygmlibelle,null,null);
		    SimpleListModel listModel = new SimpleListModel(dossiers);
		    listTypesdossiers.setModel(listModel);
		    pgTypesdossiers.setTotalSize(BeanLocator.defaultLookup(TypesDossiersSession.class).count(null));
		}
		else if(event.getName().equalsIgnoreCase(LIST_COURRIERREF_COURRIER_MODEL_CHANGE)){

		}else if(event.getName().equalsIgnoreCase(LIST_AC_MODEL_CHANGE)){

			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgAc.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgAc.getActivePage() * byPage;
				pgAc.setPageSize(byPage);
			}
			List<SygAutoriteContractante> autorites = BeanLocator.defaultLookup(AutoriteContractanteSession.class).find(activePage,byPage,libelleAc,null,null, null, codeautorite);
			SimpleListModel listModel = new SimpleListModel(autorites);
			lstAutCont.setModel(listModel);
			pgAc.setTotalSize(BeanLocator.defaultLookup( AutoriteContractanteSession.class).count(libelleAc,null,null, null, codeautorite));
		}else if(event.getName().equalsIgnoreCase(LIST_FILES_MODEL_CHANGE)){
			List<SygFilesAc>  filesAcs = BeanLocator.defaultLookup(FilesAcSession.class).findByCourrier(0, -1, courrier.getCourrierId());
			SimpleListModel listModel = new SimpleListModel(filesAcs);
			lstFiles.setModel(listModel);
			pgPieces.setTotalSize(BeanLocator.defaultLookup( FilesAcSession.class).count( codeautorite));
		}else if (event.getName().equals(
				ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {

			Long code = (Long) ((HashMap<String, Object>) event.getData())
					.get(CURRENT);
			String toDo = (String) ((HashMap<String, Object>) event.getData())
					.get(TODO);

			if (toDo != null && toDo.equalsIgnoreCase("delLineFiles")) {

				BeanLocator.defaultLookup(FilesAcSession.class)
				.delete(code);
				filesAc = null;
				Events.postEvent(LIST_FILES_MODEL_CHANGE, this, null);
				initFormAll();
			} 

		}else if(event.getName().equalsIgnoreCase(LIST_MODERECPTION_COURRIER_MODEL_CHANGE)){
			List<SygModeReception>  reception = BeanLocator.defaultLookup(ModeReceptionSession.class).find(pgModeReception.getActivePage()*byPage,byPage,null,libelleModeR,null);
			SimpleListModel listModel = new SimpleListModel(reception);
			lstModereception.setModel(listModel);
			pgModeReception.setTotalSize(BeanLocator.defaultLookup(ModeReceptionSession.class).count(libelleModeR));
		}
//		else if(event.getName().equalsIgnoreCase(Events.ON_UPLOAD)){
//
//		}
//		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
//			
//			for (int i = 0; i < lstFiles.getSelectedCount(); i++) {
//			Long codes = ((SygFilesAc)((Listitem) lstFiles.getSelectedItems().toArray()[i]).getValue()).getId();
//			BeanLocator.defaultLookup(FilesAcSession.class).delete(codes);
//				
//			}
//			
//			Events.postEvent(LIST_FILES_MODEL_CHANGE, this, null);
//		}

	}

	private class TypeAcRender implements ListitemRenderer{



		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypeAutoriteContractante typeAutoriteContractante = (SygTypeAutoriteContractante)data;
			item.setValue(typeAutoriteContractante);

			Listcell code = new Listcell(typeAutoriteContractante.getCode());
			code.setParent(item);

			Listcell libelle = new Listcell(typeAutoriteContractante.getLibelle());
			libelle.setParent(item);

			if(codeTypeAc!=null && codeTypeAc.equals(typeAutoriteContractante.getCode())){
				item.setSelected(true);
			}


		}

	}
	private class AcRender implements ListitemRenderer{

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygAutoriteContractante autorite = (SygAutoriteContractante) data;
			item.setValue(autorite);

			Listcell cellSigle = new Listcell(autorite.getSigle());
			cellSigle.setParent(item);
			
			Listcell cellLibelle = new Listcell(autorite.getDenomination());
			cellLibelle.setParent(item);

		
			if(codeautorite!=null && codeautorite.intValue()==autorite.getId().intValue()){
				item.setSelected(true);

			}

		}

	}
	private class TypeCourrierRender implements ListitemRenderer{

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypesCourrier courrier = (SygTypesCourrier) data;
			item.setValue(courrier);

			Listcell cellcodetypecourrier = new Listcell(courrier.getCodetypecourrier());
			cellcodetypecourrier.setParent(item);
			Listcell celllibelletypecourrier = new Listcell(courrier.getLibelletypecourrier());
			celllibelletypecourrier.setParent(item);
			codeType="LS";
			if(codeType!=null && codeType.equalsIgnoreCase(courrier.getCodetypecourrier())){
				item.setSelected(true);
				typeCourrier = courrier;
				if(courrier!=null){
					bdTypeCourrier.setValue(courrier.getLibelletypecourrier());
				}
				bdTypeCourrier.close();
				bdTypeCourrier.setDisabled(true);
			}
			

		}

	}
	private class NatureCourrierRender implements ListitemRenderer{

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygNatureCourrier courrier= (SygNatureCourrier) data;
			item.setValue(courrier);

			Listcell cellcodenaturecourrier = new Listcell(courrier.getCodenaturecourrier());
			cellcodenaturecourrier.setParent(item);
			Listcell celllibellenaturecourrier = new Listcell(courrier.getLibellenaturecourrier());
			celllibellenaturecourrier.setParent(item);
			if(codeNature!=null && codeNature.equalsIgnoreCase(courrier.getCodenaturecourrier())){
				item.setSelected(true);

			}

		}

	}
	private class ModeTraitementRender implements ListitemRenderer{

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygModeTraitement rapport = (SygModeTraitement) data;
			item.setValue(rapport);

			Listcell cellcodemodetraitement = new Listcell(rapport.getCodemodetraitement());
			cellcodemodetraitement.setParent(item);
			Listcell celllibellemodetraitement = new Listcell(rapport.getLibellemodetraitement());
			celllibellemodetraitement.setParent(item);
//			if(codeModeT!=null && codeModeT.equalsIgnoreCase(rapport.getCodemodetraitement())){
//				item.setSelected(true);
//
//			}

		}

	}
	private class ModeRecptionRender implements ListitemRenderer{

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygModeReception reception = (SygModeReception) data;
			item.setValue(reception);

			Listcell celllibellemodereception = new Listcell(reception.getLibellemodereception());
			celllibellemodereception.setParent(item);
			Listcell cellcodemodereception = new Listcell(reception.getCodemodereception());
			cellcodemodereception.setParent(item);
			if(codeModeR!=null && codeModeR.equalsIgnoreCase(reception.getCodemodereception())){
				item.setSelected(true);

			}

		}

	}
	private class CourrierRefRender implements ListitemRenderer{

		@Override
		public void render(Listitem arg0, Object arg1, int index) throws Exception {
			// TODO Auto-generated method stub

		}

	}

	private class FilesRefRender implements ListitemRenderer, EventListener{

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygFilesAc filesAc = (SygFilesAc)data;
			item.setValue(filesAc);

			//	    Listcell nomFile = new Listcell(filesAc.getNameFile());
			//	    nomFile.setParent(item);

			Listcell objectFile = new Listcell("");
			if(filesAc.getObjectFile()!=null){
				objectFile = new Listcell(filesAc.getObjectFile());
			}
			objectFile.setParent(item);

			Listcell objectFichier = new Listcell("");
			if(filesAc.getFileSrc()!=null){
				objectFichier = new Listcell(filesAc.getFileSrc());
			}
			objectFichier.setParent(item);

			Listcell cellSupFile = new Listcell("", "/images/delete.png");
			cellSupFile.addEventListener(Events.ON_CLICK, this);
			// cellSupLigneDevis.addEventListener(Events.ON_CLICK,
			// (EventListener) this);
			cellSupFile.setAttribute(UIConstants.TODO, "delLineFiles");
			cellSupFile.setAttribute(UIConstants.ATTRIBUTE_CODE,
					filesAc.getId());
			cellSupFile.setParent(item);

			Listcell cellOpenFile = new Listcell("", "/images/PaperClip-16x16.png");
			cellOpenFile.addEventListener(Events.ON_CLICK, this);
			// cellSupLigneDevis.addEventListener(Events.ON_CLICK,
			// (EventListener) this);
			cellOpenFile.setAttribute(UIConstants.TODO, "openFile");
			cellOpenFile.setAttribute(UIConstants.ATTRIBUTE_CODE,
					filesAc.getId());
			cellOpenFile.setParent(item);


		}

		@Override
		public void onEvent(Event event) throws Exception {
			if (event.getName().equalsIgnoreCase(Events.ON_CLICK)) {
				Listcell cell = (Listcell) event.getTarget();
				// String toDo = (String) cell.getAttribute(UIConstants.TODO);				
				Listcell button = (Listcell) event.getTarget();
				String toDo = (String) button.getAttribute(UIConstants.TODO);
				SygFilesAc fileAc = BeanLocator.defaultLookup(FilesAcSession.class).findById((Long)cell.getAttribute(UIConstants.ATTRIBUTE_CODE));
				if (toDo.equalsIgnoreCase("openFile")){
					String nomfichier=(String) fileAc.getFileSrc();
					step0.setVisible(false);
					step1.setVisible(false);
					step2.setVisible(false);
					step3.setVisible(true);
					String filepath = cheminDossier +  nomfichier;
					File f = new File(filepath.replaceAll("\\\\", "/"));

					org.zkoss.util.media.AMedia mymedia = null;
					try {
						mymedia = new AMedia(f, null, null);
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					if (mymedia != null)
						idIframe.setContent(mymedia);
					else
						idIframe.setSrc("");

					idIframe.setHeight("600px");
					idIframe.setWidth("95%");
				}else if (toDo.equalsIgnoreCase("delLineFiles")){
					HashMap<String, String> display = new HashMap<String, String>();
					display.put(MessageBoxController.DSP_MESSAGE, Labels
							.getLabel("kermel.common.form.question.supprimer")
							+ " ?");
					display.put(MessageBoxController.DSP_TITLE, Labels
							.getLabel("kermel.courrier.file.supprimer.title"));
					display.put(MessageBoxController.DSP_HEIGHT, "150px");
					display.put(MessageBoxController.DSP_WIDTH, "100px");
					HashMap<String, Object> data = new HashMap<String, Object>();
					data
					.put(CURRENT, cell
							.getAttribute(UIConstants.ATTRIBUTE_CODE));
					data.put(TODO, cell.getAttribute(UIConstants.TODO));
					showMessageBox(display, data);
				}				
			}
		}

	}


	public void onClick$menuNextStep0(){
		if(lstAutCont.getSelectedItem()==null) {
			throw new WrongValueException(lstAutCont, Labels.getLabel("kermel.error.select.item"));
		}
		autoriteContractante = (SygAutoriteContractante) lstAutCont.getSelectedItem().getValue();
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
		setLabelStep1();

	}
	
	public void onClose(){
		detach();
	}
	public void onClick$menuNextStep1(){
		if(isRequieredValue()){
			step0.setVisible(false);
			step1.setVisible(false);
			step2.setVisible(true);
			setLabelStep2();
			Events.postEvent(LIST_FILES_MODEL_CHANGE, this, null);

		}else{
			lblStatusbar.setValue(errorMsg);
			lblStatusbar.setStyle(ERROR_MSG_STYLE);
			throw new WrongValueException(errorComponent, errorMsg);
		}

	}

	public void onClick$menuPrevious1(){
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);

	}

	public void onClick$menuPrevious2(){
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
	}

	public void onSelect$lstTypeAc(){
		typeAutoriteContractante = (SygTypeAutoriteContractante) lstTypeAc.getSelectedItem().getValue();
		if(typeAutoriteContractante!=null){
			bdTypeAc.setValue(typeAutoriteContractante.getLibelle());
		}
		bdTypeAc.close();
	}

	public void onSelect$lstTypeCourrier(){
		typeCourrier = (SygTypesCourrier) lstTypeCourrier.getSelectedItem().getValue();
		if(typeCourrier!=null){
			bdTypeCourrier.setValue(typeCourrier.getLibelletypecourrier());
		}
		bdTypeCourrier.close();
	}

	public void onSelect$lstTypeNature(){
		natureCourrier = (SygNatureCourrier) lstTypeNature.getSelectedItem().getValue();
		if(natureCourrier!=null){
			bdNature.setValue(natureCourrier.getLibellenaturecourrier());
		}
		bdNature.close();
	}

//	public void onSelect$lstModeTraitement(){
//		modeTraitement = (SygModeTraitement) lstModeTraitement.getSelectedItem().getValue();
//		if(modeTraitement!=null){
//			bdModeTraitment.setValue(modeTraitement.getLibellemodetraitement());
//		}
//		bdModeTraitment.close();
//	}

	public void onSelect$lstModereception(){
		modeReception = (SygModeReception) lstModereception.getSelectedItem().getValue();
		if(modeReception!=null){
			bdModereception.setValue(modeReception.getLibellemodereception());
		}
		bdModereception.close();
	} 

	public void onSelect$lstCourrierRef(){
		courrier = (SygCourrierAc) lstCourrierRef.getSelectedItem().getValue();
		if(courrier!=null){
			bdCourrierRef.setValue(courrier.getCourrierReference());
		}
		bdCourrierRef.close();
	}



	private void chargeFormAcc(SygFilesAc filesAc) {
		//txtNomFIchier.setValue(filesAc.getNameFile());
		dateSaisieFile.setValue(filesAc.getDateFile());
		txtFichiersrc.setValue(filesAc.getFileSrc());
		txtObjetFile.setValue(filesAc.getObjectFile());
		this.filesAc = filesAc;

	}

	public void onSelect$listeAccompagnant() {
		filesAc = (SygFilesAc) lstFiles.getSelectedItem().getValue();

		if (filesAc != null) {
			chargeFormAcc(filesAc);
		}
	}


	private boolean isRequieredValue() {
		try {

			/* Controle champ obligatoire */

			if ( dateReception.getValue() == null) {
				errorMsg = Labels .getLabel("kermel.courrier.nouveau.champdatereceptionobligatoire");
				errorComponent = dateReception;
				return false;
			}

			if (dateCourrier.getValue() == null) {
				errorMsg = Labels .getLabel("kermel.courrier.nouveau.champdatecourrierobligatoire");
				errorComponent = dateCourrier;
				return false;
			}
			if (dateReception.getValue().before(dateCourrier.getValue())) {
				errorMsg = Labels .getLabel("kermel.courrier.form.dateReception")+" "
						+Labels .getLabel("kermel.referentiel.date.posterieure")+" "
						+Labels .getLabel("kermel.courrier.form.datecourrier");
				errorComponent = dateReception;

				return false;
			}
			if (dateReception.getValue().after(new Date())) {
				errorMsg = Labels .getLabel("kermel.courrier.form.dateReception")+" "
						+Labels .getLabel("kermel.referentiel.date.posterieure")+" "
						+Labels .getLabel("kermel.referentiel.date.dujour");
				errorComponent = dateReception;

				return false;
			}

			if (txtReference.getValue().equals("") || txtReference.getValue() == null) {
				errorMsg = Labels
						.getLabel("kermel.courrier.nouveau.champreferencecourrierobligatoire");
				errorComponent = txtReference;
				return false;
			}
			if (txtObjet.getValue() == null || txtObjet.getValue().equals("")) {
				errorMsg = Labels
						.getLabel("kermel.courrier.nouveau.champobjetcourrierobligatoire");
				errorComponent = txtObjet;
				return false;
			}

			if (bandTypeDossier.getValue().equals("") || bandTypeDossier.getValue() == null) {
				errorMsg = Labels
						.getLabel("kermel.courrier.nouveau.champtypedossierobligatoire");
				errorComponent = bandTypeDossier;
				return false;
			}
			if (bdTypeCourrier.getValue().equals("") || bdTypeCourrier.getValue() == null) {
				errorMsg = Labels
						.getLabel("kermel.courrier.nouveau.champtypecourrierobligatoire");
				errorComponent = bdTypeCourrier;
				return false;
			}
			if (bdNature.getValue().equals("")
					|| bdNature.getValue() == null) {
				errorMsg = Labels
						.getLabel("kermel.courrier.nouveau.champnaturecourrierobligatoire");
				errorComponent = bdNature;
				return false;
			}
			if (bdModereception.getValue() == null || bdModereception.getValue().equals("")) {
				errorMsg = Labels
						.getLabel("kermel.courrier.nouveau.champmodereceptionobligatoire");
				errorComponent = bdModereception;
				return false;
			}

			return true;
		} catch (Exception e) {
			lblStatusbar.setValue(e.toString());

			return false;
		}
	}

	public void setFormCourrier(){

		courrier.setCourrierAutoriteContractante(autoriteContractante);
		courrier.setCourrierDate(dateCourrier.getValue());
		courrier.setCourrierDateReception(dateReception.getValue());
		courrier.setCourrierModeReception(modeReception);
		courrier.setCourrierReference(txtReference.getValue());
		courrier.setCourrierObjet(txtObjet.getValue());
		courrier.setCourrierType(typeCourrier);
		courrier.setNatureCourrier(natureCourrier);
		courrier.setTypesDossiers(typeDossier);
		courrier.setNature(UIConstants.COURRIERARRIVE);
		//courrier.setModetraitement(modeTraitement);
		courrier.setAmplitaire(txtAmpliataire.getValue());
		courrier.setNumEnregistrement(txtnumenregostrement.getValue());
		courrier.setCourrierDateSaisie(new Date());
		if(courrierRef!=null) {
			courrier.setCourrierRef(courrierRef.getCourrierReference());
		}

	}

	private boolean saveCourrier() {

		try {
			courrier = BeanLocator.defaultLookup(CourriersAcSession.class).save(courrier,UIConstants.NUMENREGISTREMENT);

			return true;
		} catch (RuntimeException runtimeException) {
			if (runtimeException.getCause().getClass().getPackage().getName()
					.equals("javax.transaction")) {
				lblStatusbar.setValue(Labels
						.getLabel("atlantis.common.erreur.commit"));
				lblStatusbar.setStyle(ERROR_MSG_STYLE);
			}
			return false;
		} catch (Exception e) {
			lblStatusbar.setValue(e.toString());

			return false;
		}

	}

	private boolean updateCourrier() {

		try {
			courrier = BeanLocator.defaultLookup(CourriersAcSession.class).update(
					courrier);

			return true;
		} catch (RuntimeException runtimeException) {
			if (runtimeException.getCause().getClass().getPackage().getName()
					.equals("javax.transaction")) {
				lblStatusbar.setValue(Labels
						.getLabel("atlantis.common.erreur.commit"));
				lblStatusbar.setStyle(ERROR_MSG_STYLE);
			}
			return false;
		} catch (Exception e) {
			lblStatusbar.setValue(e.toString());

			return false;
		}

	}

	public void onClick$menuValiderStep1() {

		if (isRequieredValue()) {

			if (mode==null || mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				courrier = new SygCourrierAc();
				Enregistrecourrier();
				setFormCourrier();
				if (saveCourrier()) {
					BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_COURRIERAC", Labels.getLabel("kermel.referentiel.common.courriers.ajouter")+" "+courrier.getCourrierRef()+" :" + new Date(), login);
					
					menuValiderStep1.setDisabled(true);
					menuNextStep1.setDisabled(false);
					lblStatusbar.setValue(Labels.getLabel("kermel.enregistrement.succes"));
					lblStatusbar.setStyle("color:green;font-weight:bold;text-decoration:blank");
					Events.postEvent(LIST_FILES_MODEL_CHANGE, this, null);
				} else {
					lblStatusbar.setValue(errorMsg);
					lblStatusbar.setStyle(ERROR_MSG_STYLE);
					throw new WrongValueException(errorComponent, errorMsg);
				}
			} else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				setFormCourrier();
				if (updateCourrier()) {
					BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_COURRIERAC", Labels.getLabel("kermel.referentiel.common.courriers.modifier")+" "+courrier.getCourrierRef()+" :" + new Date(), login);
						

					menuValiderStep1.setDisabled(true);
					lblStatusbar.setValue(Labels.getLabel("kermel.enregistrement.succes"));
					lblStatusbar.setStyle("color:green;font-weight:bold;text-decoration:blank");

					Events.postEvent(LIST_FILES_MODEL_CHANGE, this, null);
				} else {
					lblStatusbar.setValue(errorMsg);
					lblStatusbar.setStyle(ERROR_MSG_STYLE);
					throw new WrongValueException(errorComponent, errorMsg);
				}
			}

		} else {
			lblStatusbar.setValue(errorMsg);
			lblStatusbar.setStyle(ERROR_MSG_STYLE);
			throw new WrongValueException(errorComponent, errorMsg);
		}

	}
	private void setLabelStep1() {


		lblDenomination.setValue(autoriteContractante.getDenomination());
		lblSigle.setValue(autoriteContractante.getSigle());
		

	}

	private void setLabelStep2() {
		lblDenomination1.setValue(autoriteContractante.getDenomination());
		lblSigle1.setValue(autoriteContractante.getSigle());
		lblreference.setValue(courrier.getCourrierReference());
		lbldatecourrier.setValue(UtilVue.getInstance().formateLaDate(courrier.getCourrierDate()));
		lblObjet.setValue(courrier.getCourrierObjet());

	}

	private boolean isRequiredFieldFile() {
		try {
			/* Controle champ obligatoire */
			//	    if (txtNomFIchier.getValue() == null || txtNomFIchier.getValue().equals("")) {
			//		errorMsg = Labels
			//			.getLabel("kermel.file.nouveau.champnomfichierobligatoire");
			//		errorComponent = txtNomFIchier;
			//		return false;
			//	    }
			
			if (txtFichiersrc.getValue() == null || txtFichiersrc.getValue().equals("")) {
				errorMsg = Labels
						.getLabel("kermel.file.nouveau.champfichiersrcobligatoire");
				errorComponent = txtFichiersrc;
				return false;
			}
			//	    if (txtObjetFile.getValue() == null || txtObjetFile.getValue().equals("")) {
			//		errorMsg = Labels
			//			.getLabel("kermel.file.nouveau.champobjetfileobligatoire");
			//		errorComponent = txtObjetFile;
			//		return false;
			//	    }
			
			
//			if (dateSaisieFile.getValue() == null || dateSaisieFile.getValue().equals("")) {
//				errorMsg = Labels
//						.getLabel("kermel.file.nouveau.champodatesaisieobligatoire");
//				errorComponent = dateSaisieFile;
//				return false;
//			}

			return true;
		} catch (Exception e) {

			return true;
		}

	}

	public void onClick$cellValider3() {

		if (isRequiredFieldFile()) {
			if (filesAc == null || filesAc.getId() == 0) {
				filesAc = new SygFilesAc();
				filesAc.setCourrier(courrier);
				dateSaisieFile.setValue(new Date());
			}
			setFormFile();
			if (saveFile()) {
				initFormAll();
				Events.postEvent(LIST_FILES_MODEL_CHANGE, this, null);
			} else {
				lblStatusbar2.setValue(errorMsg);
				lblStatusbar2.setStyle(ERROR_MSG_STYLE);
				throw new WrongValueException(lstFiles, errorMsg);
			}

		} else {
			lblStatusbar2.setValue(errorMsg);
			lblStatusbar2.setStyle(ERROR_MSG_STYLE);
			throw new WrongValueException(lstFiles, errorMsg);
		}
	}

	private boolean saveFile() {
		try {
			BeanLocator.defaultLookup(FilesAcSession.class).save(filesAc);
			return true;
		} catch (RuntimeException runtimeException) {
			if (runtimeException.getCause().getClass().getPackage().getName()
					.equals("javax.transaction")) {
				lblStatusbar2.setValue(Labels
						.getLabel("atlantis.common.erreur.commit"));
				lblStatusbar2.setStyle(ERROR_MSG_STYLE);
			}
			return false;
		} catch (Exception e) {
			lblStatusbar2.setValue(e.toString());

			return false;
		}
	}

	private void setFormFile() {

		filesAc.setCourrier(courrier);
		//filesAc.setNameFile(txtNomFIchier.getValue());
		filesAc.setFileSrc(txtFichiersrc.getValue());
		filesAc.setObjectFile(txtObjetFile.getValue());
		filesAc.setDateFile(dateSaisieFile.getValue());
	}

	private void chargeFormAll(SygFilesAc file) {
		//txtNomFIchier.setValue(filesAc.getNameFile());
		txtFichiersrc.setValue(filesAc.getFileSrc());
		txtObjetFile.setValue(filesAc.getObjectFile());
		dateSaisieFile.setValue(filesAc.getDateFile());

	}

	private void initFormAll() {

		//txtNomFIchier.setValue(null);
		txtFichiersrc.setValue(null);
		txtObjetFile.setValue(null);
		dateSaisieFile.setValue(null);
	}

	public void onSelect$lstFiles() {

		filesAc =  (SygFilesAc) lstFiles.getSelectedItem().getValue();
		if (filesAc != null) {
			chargeFormAll(filesAc);
		}
	}

	private void initFormCourrier(SygCourrierAc courrier){
		menuNextStep1.setDisabled(false);
		typeCourrier = courrier.getCourrierType();
		bdTypeCourrier.setValue(typeCourrier.getLibelletypecourrier());
		codeType = typeCourrier.getCodetypecourrier();

		natureCourrier = courrier.getNatureCourrier();
		bdNature.setValue(natureCourrier.getLibellenaturecourrier());
		codeNature = natureCourrier.getCodenaturecourrier();
		codeautorite=courrier.getCourrierAutoriteContractante().getId();

//		modeTraitement = courrier.getModetraitement();
//		bdModeTraitment.setValue(modeTraitement.getLibellemodetraitement());
//		codeModeT = modeTraitement.getCodemodetraitement();

		modeReception = courrier.getCourrierModeReception();
		bdModereception.setValue(modeReception.getLibellemodereception());
		codeModeR = modeReception.getCodemodereception();

		autoriteContractante = courrier.getCourrierAutoriteContractante();
		codeAc = autoriteContractante.getSigle();
		txtReference.setValue(courrier.getCourrierReference());

		txtObjet.setValue(courrier.getCourrierObjet());

		dateReception.setValue(courrier.getCourrierDateReception());

		dateCourrier.setValue(courrier.getCourrierDate());

		txtAmpliataire.setValue(courrier.getAmplitaire());

		txtnumenregostrement.setValue(courrier.getNumEnregistrement());
		typeDossier=courrier.getTypesDossiers();
		bandTypeDossier.setValue(typeDossier.getLibelle());

	}

	public void onUpload(UploadEvent event){


		Messagebox.show((String) Executions.getCurrent().getAttribute("FICHIER"));
	}

	public void onClick$btnRechercher(){
		if(txtDenomination.getValue().trim()!=null &&!txtDenomination.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.denomination"))){
			libelleAc = txtDenomination.getValue();
		}else{
			libelleAc = null;
		}

		if(bdTypeAc.getValue().trim()!=null){
			libelleTypeAc = bdTypeAc.getValue();
		}else{
			libelleTypeAc = null;
		}

		Events.postEvent(LIST_AC_MODEL_CHANGE, this, null);
	}

	public void onOK$txtDenomination(){
		onClick$btnRechercher();
	}

	public void onClick$menuCancelStep1(){
		loadApplicationState("courrier_ac");
		detach();
	}
	
	public void onOK$bdTypeAc(){
		onClick$btnRechercher();
	}

	public void onClick$btnRechercherTypeAc(){
		if(txtRechercherTypeAc.getValue().trim()!=null && !txtRechercherTypeAc.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.marche.Type")) ){
			libelleTypeAc = txtRechercherTypeAc.getValue();
		}else{
			libelleTypeAc = null;
		}

		Events.postEvent(LIST_TYPE_AC_MODEL_CHANGE, this, null);
	}



	public void onFocus$txtDenomination(){
		if(txtDenomination.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.denomination"))){
			txtDenomination.setValue(null);
		}
	}

	public void onBlur$txtDenomination(){
		if(txtDenomination.getValue().equalsIgnoreCase("")||txtDenomination.getValue()==null){
			txtDenomination.setValue(Labels.getLabel("kermel.referentiel.common.denomination"));
		}
	}

	public void onFocus$txtRechercherTypeAc(){
		if(txtRechercherTypeAc.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.marche.Type"))){
			txtRechercherTypeAc.setValue(null);
		}
	}

	public void onBlur$txtRechercherTypeAc(){
		if(txtRechercherTypeAc.getValue().equalsIgnoreCase("")||txtRechercherTypeAc.getValue()==null){
			txtRechercherTypeAc.setValue(Labels.getLabel("kermel.referentiel.common.marche.Type"));
		}
	}

	public void onFocus$txtRechercherTypeCourrier(){
		if(txtRechercherTypeCourrier.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.courrier.form.choix.type"))){
			txtRechercherTypeCourrier.setValue(null);
		}
	}

	public void onBlur$txtRechercherTypeCourrier(){
		if(txtRechercherTypeCourrier.getValue().equalsIgnoreCase("")||txtRechercherTypeCourrier.getValue()==null){
			txtRechercherTypeCourrier.setValue(Labels.getLabel("kermel.courrier.form.choix.type"));
		}
	}

	public void onClick$btnRechercherTypeCourrier(){
		if(txtRechercherTypeCourrier.getValue().trim()!=null && !txtRechercherTypeCourrier.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.courrier.form.choix.type")) ){
			libelleType = txtRechercherTypeCourrier.getValue();
		}else{
			libelleType = null;
		}

		Events.postEvent(LIST_TYPE_COURRIER_MODEL_CHANGE, this, null);
	}

	public void onClick$btnRechercherNature(){
		if(txtRechercherNature.getValue().trim()!=null && !txtRechercherNature.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.courrier.form.choix.nature")) ){
			libelleNature = txtRechercherNature.getValue();
		}else{
			libelleNature = null;
		}

		Events.postEvent(LIST_NATURE_COURRIER_MODEL_CHANGE, this, null);
	}

	public void onFocus$txtRechercherNature(){
		if(txtRechercherNature.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.courrier.form.choix.nature"))){
			txtRechercherNature.setValue(null);
		}
	}

	public void onBlur$txtRechercherNature(){
		if(txtRechercherNature.getValue().equalsIgnoreCase("")||txtRechercherNature.getValue()==null){
			txtRechercherNature.setValue(Labels.getLabel("kermel.courrier.form.choix.nature"));
		}
	}
//	public void onClick$btnRechercherModeTraitement(){
//		if(txtRechercherModeTraitement.getValue().trim()!=null && !txtRechercherModeTraitement.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.courrier.form.choix.modetraitement")) ){
//			libelleModeT = txtRechercherModeTraitement.getValue();
//		}else{
//			libelleModeT = null;
//		}
//
//		Events.postEvent(LIST_MODETRAITEMENT_COURRIER_MODEL_CHANGE, this, null);
//	}
//
//	public void onFocus$txtRechercherModeTraitement(){
//		if(txtRechercherModeTraitement.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.courrier.form.choix.modetraitement"))){
//			txtRechercherModeTraitement.setValue(null);
//		}
//	}

//	public void onBlur$txtRechercherModeTraitement(){
//		if(txtRechercherModeTraitement.getValue().equalsIgnoreCase("")||txtRechercherModeTraitement.getValue()==null){
//			txtRechercherModeTraitement.setValue(Labels.getLabel("kermel.courrier.form.choix.modetraitement"));
//		}
//	}

	public void onFocus$txtRechercherModeReception(){
		if(txtRechercherModeReception.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.courrier.form.choix.modereception"))){
			txtRechercherModeReception.setValue(null);
		}
	}

	public void onBlur$txtRechercherModeReception(){
		if(txtRechercherModeReception.getValue().equalsIgnoreCase("")||txtRechercherModeReception.getValue()==null){
			txtRechercherModeReception.setValue(Labels.getLabel("kermel.courrier.form.choix.modereception"));
		}
	}
	public void onClick$btnRechercherModeReception(){
		if(txtRechercherModeReception.getValue().trim()!=null && !txtRechercherModeReception.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.courrier.form.choix.modereception")) ){
			libelleModeR = txtRechercherModeReception.getValue();
		}else{
			libelleModeR = null;
		}

		Events.postEvent(LIST_MODERECPTION_COURRIER_MODEL_CHANGE, this, null);
	}

	public void onClick$bcUploadFile(){
		if (ToolKermel.isWindows()) {
			nomFichier = FileLoader.uploadPieceDossier(UtilVue.getInstance().formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
		} else {
			nomFichier = FileLoader.uploadPieceDossier(UtilVue.getInstance().formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));
		}

		txtFichiersrc.setValue(nomFichier);
	}
	
	public void onClick$menuFermer2() {
		
		loadApplicationState("courrier_ac");
		detach();
	}
public void onClick$menuCancelStep0() {
		
		loadApplicationState("courrier_ac");
		detach();
	}
	  private class TypeDossierRenderer implements ListitemRenderer{

			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
			    SygTypesDossiers dossiers = (SygTypesDossiers) data;
			    item.setValue(dossiers);

			    Listcell cellcode = new Listcell(dossiers.getCode());
			    cellcode.setParent(item);
			    Listcell celllibelle = new Listcell(dossiers.getLibelle());
			    celllibelle.setParent(item);

			}

		    }
	  
	  public void onSelect$listTypesdossiers(){
			typeDossier = (SygTypesDossiers) listTypesdossiers.getSelectedItem().getValue();
			if(typeDossier!=null){
			    bandTypeDossier.setValue(typeDossier.getLibelle());
			}
			bandTypeDossier.close();
		    }
	  
	  private synchronized void saveCourriers() {
			if(mode==null || mode.equalsIgnoreCase(UIConstants.MODE_NEW))
			txtnumenregostrement.setValue(BeanLocator.defaultLookup(CourriersAcSession.class).getGeneratedCode(UIConstants.NUMENREGISTREMENT));
			
	  }
	  
	  public void onClick$menuCancelStep2(){
		  loadApplicationState("courrier_ac");
			detach();
	  }
	  void Enregistrecourrier()  {
		    lock.lock();
		    try {
		    	saveCourriers();
		    } finally {
		        lock.unlock();
		    }
		}
}