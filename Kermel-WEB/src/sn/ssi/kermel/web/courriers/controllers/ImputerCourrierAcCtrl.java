package sn.ssi.kermel.web.courriers.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.MessageManager;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.courriers.ejb.InstructionSession;
import sn.ssi.kermel.be.courriers.ejb.TCourriersAcSession;
import sn.ssi.kermel.be.entity.EcoGroupes;
import sn.ssi.kermel.be.entity.EcoGroupesImputation;
import sn.ssi.kermel.be.entity.EcoGroupesImputilisateur;
import sn.ssi.kermel.be.entity.SygBureauxdcmp;
import sn.ssi.kermel.be.entity.SygCourrierAc;
import sn.ssi.kermel.be.entity.SygInstruction;
import sn.ssi.kermel.be.entity.SygModeTraitement;
import sn.ssi.kermel.be.entity.SygTCourrierAC;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.BureauxdcmpSession;
import sn.ssi.kermel.be.referentiel.ejb.GroupeImputationSession;
import sn.ssi.kermel.be.referentiel.ejb.GroupeImpututilisateurSession;
import sn.ssi.kermel.be.security.UtilisateurSession;
import sn.ssi.kermel.be.traitementdossier.ejb.ModeTraitementSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class ImputerCourrierAcCtrl extends AbstractWindow implements AfterCompose, EventListener , ListitemRenderer{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode ;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
	String code;
	private int activePage;
	private String page = null;
	private SygTCourrierAC tcourrier;
	Date date = new Date();
	private Textbox txtInstruction,txtlibelleInstruction,txtRechercherModeTraitement ;
	private Textbox txtRechercherService,txtRechercherAgent,txtRechercherGroupe;
	private Bandbox bdInstructions;//bdService,
	private Combobox comboImputationPour;
	private Paging pgInstruction;//pgService,
	private Listbox listInstruction;//lstService,
	private String serviceLibelle = null,groupeLibelle = null;
	//private Bandbox bdAgent,bdGroupe;
	private Paging pgGroupe;//pgAgent,
	private Listbox lstGroupe;//lstAgent,
	private EcoGroupes groupes;
	private SygInstruction instructions;
	private Utilisateur agent;
	private String agentPrenom = null,instructionLibelle;
	Session session =getHttpSession();
	private SygCourrierAc courrier;
	private Long idOrigne;
	private int nbrcourrierBureau,nbrcourrierAgent;
	private Datebox dbMaxTraitement, dbImputation;
	private Label referenceid,datecourrier,autorite,datereception,referenceidstep1,datecourrierstep1,datereceptionstep1,objetstep1,autoritestep1,objets;
	private Groupbox gbInstruction,gbCstep1,gbImputationStep1,gbImputationAgent,gbImputationGroup;
	private Checkbox chknotifMail,chkaccusRecept;
	private SygBureauxdcmp services;
	private Radio rdoui,rdnon;
	private Utilisateur user;
	private Div step1,step2;
	private Tree tree;
	private Treechildren child = new Treechildren();
	public Textbox txtPrenom ,txtNom;
	private String prenom=null;
	private String nom=null;
	private SygBureauxdcmp uniteorga=new SygBureauxdcmp();
	private Long codeuser=null;
	private String uniteorg=null;
	private List<Utilisateur> userSelected = new ArrayList<Utilisateur>();;
	List<SygBureauxdcmp> listUniteOrg = new ArrayList<SygBureauxdcmp>();
	private Listbox lstUser;
	private Paging pgUser;
	private String codeModeT, libelleModeT;
	private Paging pgModeTraitment; 
	private Bandbox bdModeTraitment;
	public static final String LIST_MODETRAITEMENT_COURRIER_MODEL_CHANGE="onModeTModelChange";
	private SygModeTraitement modeTraitement;
	private Listbox lstModeTraitement;
	//private SygInstruction ;
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION1, this);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_AGENTS, this);
		
		listInstruction.setItemRenderer(this);
		pgInstruction.setPageSize(byPageBandBox);
		pgInstruction.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION1, this, null);
			
		//Groupe
		lstGroupe.setItemRenderer(new GroupeRenderer());
		pgGroupe.setPageSize(byPageBandBox);
		pgGroupe.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTRE_ACTION1);
		
		//User
		addEventListener(ApplicationEvents.ON_USER, this);
		lstUser.setItemRenderer(new UtilisateurRenderer());
		
		
		addEventListener(LIST_MODETRAITEMENT_COURRIER_MODEL_CHANGE, this);
		lstModeTraitement.setItemRenderer(new ModeTraitementRender());
		pgModeTraitment.setPageSize(byPageBandBox);
		pgModeTraitment.addForward("onPaging", this, LIST_MODETRAITEMENT_COURRIER_MODEL_CHANGE);
		Events.postEvent(LIST_MODETRAITEMENT_COURRIER_MODEL_CHANGE, this, null);
		
	}
	
	@Override
	public void onEvent(Event event) throws Exception {
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)){
			//Service
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandBox = 3;//UIConstants.DSP_BANDBOX_BY_PAGE;				
				pgInstruction.setPageSize(byPageBandBox);			
			}
			else {
				byPageBandBox = 3;//UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgInstruction.getActivePage() * byPageBandBox;
				pgInstruction.setPageSize(byPageBandBox);
			}			
			List<SygInstruction> instructions = BeanLocator.defaultLookup(InstructionSession.class).find(activePage, byPageBandBox, instructionLibelle);
			listInstruction.setModel(new SimpleListModel(instructions));
			pgInstruction.setTotalSize(BeanLocator.defaultLookup(InstructionSession.class).count(instructionLibelle));
		}
		else if(event.getName().equalsIgnoreCase(LIST_MODETRAITEMENT_COURRIER_MODEL_CHANGE)){
		List<SygModeTraitement>  traitement = BeanLocator.defaultLookup(ModeTraitementSession.class).find(pgModeTraitment.getActivePage()*byPageBandBox,byPageBandBox,libelleModeT,null,null);
		SimpleListModel listModel = new SimpleListModel(traitement);
		lstModeTraitement.setModel(listModel);
		pgModeTraitment.setTotalSize(BeanLocator.defaultLookup(ModeTraitementSession.class).count(libelleModeT));

	}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)){
			
		}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION1)){			
			//Agent
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;				
				pgGroupe.setPageSize(byPageBandBox);			
			}
			else {
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgGroupe.getActivePage() * byPageBandBox;
				pgGroupe.setPageSize(byPageBandBox);
			}
			List<EcoGroupesImputation> groupe = BeanLocator.defaultLookup(GroupeImputationSession.class).find(activePage, byPageBandBox,groupeLibelle);
			lstGroupe.setModel(new SimpleListModel(groupe));
			pgGroupe.setTotalSize(BeanLocator.defaultLookup(GroupeImputationSession.class).count(groupeLibelle));
			
		}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AGENTS)){
			//Agent
//			if (event.getData() != null) {
//				activePage = Integer.parseInt((String) event.getData());
//				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;				
//				pgService.setPageSize(byPageBandBox);			
//			}
//			else {
//				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
//				activePage = pgAgent.getActivePage() * byPageBandBox;
//				pgAgent.setPageSize(byPageBandBox);
//			}		
//			
//			List<Utilisateur> agents = BeanLocator.defaultLookup(UtilisateurSession.class).findByBureau(activePage, byPageBandBox, null, services);
//			lstAgent.setModel(new SimpleListModel(agents));
//			pgAgent.setTotalSize(BeanLocator.defaultLookup(UtilisateurSession.class).countUtilByBureau(null, services));
		}
	}
	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		mode = (String) Executions.getCurrent().getAttribute("MODE");
		user = (Utilisateur) getHttpSession().getAttribute("utilisateur");
		idOrigne = ((Utilisateur) session.getAttribute("utilisateur")).getId();
		code = (String) session.getAttribute("code");
		courrier = (SygCourrierAc) session.getAttribute("courrierAcInssession");
		
		if(courrier.getCourrierDate()!=null)
		{
			datecourrier.setValue(UtilVue.getInstance().formateLaDate(courrier.getCourrierDate()));
			datecourrierstep1.setValue(UtilVue.getInstance().formateLaDate(courrier.getCourrierDate()));
		}
			
		if(courrier.getCourrierDateReception()!=null)
		{
			datereception.setValue(UtilVue.getInstance().formateLaDate(courrier.getCourrierDateReception()));
			datereceptionstep1.setValue(UtilVue.getInstance().formateLaDate(courrier.getCourrierDateReception()));	
		}
				
		referenceidstep1.setValue(courrier.getCourrierReference());
		referenceid.setValue(courrier.getCourrierReference());	
		autorite.setValue(courrier.getCourrierAutoriteContractante().getDenomination()+"("+courrier.getCourrierAutoriteContractante().getSigle()+")");
		autoritestep1.setValue(courrier.getCourrierAutoriteContractante().getDenomination()+"("+courrier.getCourrierAutoriteContractante().getSigle()+")");
		objets.setValue(courrier.getCourrierObjet());
		objetstep1.setValue(courrier.getCourrierObjet());
		step2.setVisible(false);
		step2.setVisible(true);		
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			tcourrier = (SygTCourrierAC)Executions.getCurrent().getAttribute("TCOURRIER");//map.get(PARAM_WIDOW_CODE);
			if(tcourrier.isImpForGroup()){
				gbImputationGroup.setVisible(true);
				gbImputationAgent.setVisible(false);
				rdnon.setChecked(true);
				rdoui.setChecked(false);
			}
			else{
				gbImputationGroup.setVisible(false);
				gbImputationAgent.setVisible(true);
				rdnon.setChecked(false);
				rdoui.setChecked(true);
			}
			modeTraitement = tcourrier.getModetraitement();
			if(modeTraitement!=null)
			{
				bdModeTraitment.setValue(modeTraitement.getLibellemodetraitement());
				codeModeT = modeTraitement.getCodemodetraitement();
			}
			
			txtInstruction.setValue(tcourrier.getTcourrierInstruction());	
			dbImputation.setValue(tcourrier.getTdateImputation());
			dbMaxTraitement.setValue(tcourrier.getTdateMaxTraitement());
			agent=tcourrier.getAgent();
			codeuser = agent.getId();
			if(tcourrier.isAccuseReception())
				chkaccusRecept.setChecked(true);
			else
				chkaccusRecept.setChecked(false);
			
			if(tcourrier.isNotificationParMail())
				chknotifMail.setChecked(true);
			else
				chknotifMail.setChecked(false);
			
			if(tcourrier.getTimputationPour()==null ){
				comboImputationPour.setValue("Traitement");
			}else {
				comboImputationPour.setValue(tcourrier.getTimputationPour());	
			}
			
			
			
		}else{
			comboImputationPour.setValue("Traitement");
			dbImputation.setValue(new Date());
			dbMaxTraitement.setValue(ToolKermel.dateDecalageJour(dbImputation.getValue(), courrier.getCourrierType().getDureTraitementJour()));
			rdoui.setSelected(true);
			gbImputationAgent.setVisible(true);
			gbImputationGroup.setVisible(false);
//			bdService.setDisabled(true);
//			bdAgent.setDisabled(true);
		}
		initTree();
	}

	private void initTree() {
		child.setParent(tree);
		treeProcedureBuilder();

	    	
		
	}
	
	public void treeProcedureBuilder() {
		String key = null;		
		listUniteOrg = BeanLocator.defaultLookup(BureauxdcmpSession.class).findRech(0, -1, null,Long.parseLong("1"),null);
		if(listUniteOrg.size() > 0){
			Treechildren uniteorgTreechildren = new Treechildren();
			for (SygBureauxdcmp UniteOrg : listUniteOrg) {
				Treeitem uniteorgItem = new Treeitem();
				uniteorgItem.setLabel(UniteOrg.getLibelle());
				uniteorgItem.setTooltiptext(UniteOrg.getLibelle());
				uniteorgItem.setValue(UniteOrg);
				uniteorgItem.setOpen(false);
				uniteorgItem.setParent(child);
				//Niveau 1 :
				List<Utilisateur> listUser0 = BeanLocator.defaultLookup(
						UtilisateurSession.class).findByBureau(0, -1,null,UniteOrg);
				if(listUser0.size() > 0){
					for (Utilisateur user : listUser0) {
						Treeitem user1Item = new Treeitem();

						user1Item.setLabel(user.getPrenom()+" "+user.getNom());
						user1Item.setTooltiptext(user.getPrenom()+" "+user.getNom());
						user1Item.setImage("/images/priv.png");


						user1Item.setValue(user);
						user1Item.setOpen(false);

						if (codeuser!=null) {
							if(codeuser.intValue()==user.getId().intValue())
							{
								uniteorgItem.setOpen(true); 

								user1Item.setSelected(true);
							}

						}

						user1Item.setParent(child);

					}
				}
				//Niveau 2 :Direction	 
				List<SygBureauxdcmp> listDirection = BeanLocator.defaultLookup(
						BureauxdcmpSession.class).findRech(0, -1,null,Long.parseLong("2"),UniteOrg.getId());

				if(listDirection.size() > 0){
					Treechildren directionTreechildren = new Treechildren();
					for (SygBureauxdcmp direction : listDirection) {
						Treeitem directionItem = new Treeitem();	
						directionItem.setLabel(direction.getLibelle());
						directionItem.setTooltiptext(direction.getLibelle());			 
						directionItem.setImage("/images/direct.png");	 
						directionItem.setId(direction.getId().toString());	 
						directionItem.setValue(direction);
						directionItem.setOpen(true);			 
						directionItem.setParent(directionTreechildren);
						//Treechildren myuniteorgTreechildren = new Treechildren();
						//Niveau 2 :
						List<Utilisateur> listUser1 = BeanLocator.defaultLookup(
								UtilisateurSession.class).findByBureau(0, -1,null,direction);
						System.out.println("==============================ok"+listUser1.size());

						if(listUser1.size() > 0){
							//Treechildren unite2Treechildren = new Treechildren();

							for (Utilisateur user : listUser1) {
								Treeitem user1Item = new Treeitem();

								user1Item.setLabel(user.getPrenom()+" "+user.getNom());
								user1Item.setTooltiptext(user.getPrenom()+" "+user.getNom());
								user1Item.setImage("/images/priv.png");


								user1Item.setValue(user);
								user1Item.setOpen(false);

								if (codeuser!=null) {
									if(codeuser.intValue()==user.getId().intValue())
									{
										directionItem.setOpen(true); 

										user1Item.setSelected(true);
									}

								}

								user1Item.setParent(directionTreechildren);

							}
						}
						//Niveau 3:Division
						List<SygBureauxdcmp> listDivision = BeanLocator.defaultLookup(
								BureauxdcmpSession.class).findRech(0, -1,null,Long.parseLong("3"),direction.getId());
						System.out.println("==============================ok"+listDivision.size());

						if(listDivision.size() > 0){
							Treechildren divisionTreechildren = new Treechildren();
							for (SygBureauxdcmp division : listDivision) {
								Treeitem divisionItem = new Treeitem();
								//Treechildren mydirectionTreechildren = new Treechildren();
								divisionItem.setLabel(division.getLibelle());
								divisionItem.setTooltiptext(division.getLibelle());
								divisionItem.setValue(division);
								divisionItem.setOpen(false);
								divisionItem.setParent(divisionTreechildren);
								//Niveau 3 :
								List<Utilisateur> listUser3 = BeanLocator.defaultLookup(
										UtilisateurSession.class).findByBureau(0, -1,null,division);
								System.out.println("==============================ok"+listUser3.size());

								if(listUser3.size() > 0){
									//Treechildren unite2Treechildren = new Treechildren();

									for (Utilisateur user : listUser3) {
										Treeitem user1Item = new Treeitem();

										user1Item.setLabel(user.getPrenom()+" "+user.getNom());
										user1Item.setTooltiptext(user.getPrenom()+" "+user.getNom());
										user1Item.setImage("/images/priv.png");


										user1Item.setValue(user);
										user1Item.setOpen(false);

										if (codeuser!=null) {
											if(codeuser.intValue()==user.getId().intValue())
											{
												divisionItem.setOpen(true); 

												user1Item.setSelected(true);
											}

										}

										user1Item.setParent(divisionTreechildren);

									}
									//directionTreechildren.setParent(divisionItem);
								}
								// divisionItem.setParent(directionTreechildren);

								//Niveau 4:D�partement

								List<SygBureauxdcmp> listDepartement = BeanLocator.defaultLookup(
										BureauxdcmpSession.class).findRech(0, -1,null,Long.parseLong("4"),division.getId());
								System.out.println("==============================ok"+listDepartement.size());

								if(listDepartement.size() > 0){
									Treechildren departementTreechildren = new Treechildren();

									for (SygBureauxdcmp departement : listDepartement) {
										Treeitem departementItem = new Treeitem();
										//Treechildren mydepartementItem = new Treechildren();
										departementItem.setLabel(departement.getLibelle());
										departementItem.setTooltiptext(departement.getLibelle());

										// tacheItem.setImage("g");
										departementItem.setValue(departement);
										departementItem.setOpen(false);
										departementItem.setParent(departementTreechildren);
										//Niveau 4 :
										List<Utilisateur> listUser4 = BeanLocator.defaultLookup(
												UtilisateurSession.class).findByBureau(0, -1,null,departement);
										System.out.println("==============================ok"+listUser4.size());

										if(listUser4.size() > 0){
											//Treechildren unite2Treechildren = new Treechildren();

											for (Utilisateur user : listUser4) {
												Treeitem user1Item = new Treeitem();

												user1Item.setLabel(user.getPrenom()+" "+user.getNom());
												user1Item.setTooltiptext(user.getPrenom()+" "+user.getNom());
												user1Item.setImage("/images/priv.png");


												user1Item.setValue(user);
												user1Item.setOpen(false);

												if (codeuser!=null) {
													if(codeuser.intValue()==user.getId().intValue())
													{
														departementItem.setOpen(true); 

														user1Item.setSelected(true);
													}

												}

												user1Item.setParent(departementTreechildren);

											}
										}
										//Niveau 5:Service

										List<SygBureauxdcmp> listService = BeanLocator.defaultLookup(
												BureauxdcmpSession.class).findRech(0, -1,null,Long.parseLong("5"),departement.getId());
										if(listService.size() > 0){
											Treechildren serviceTreechildren = new Treechildren();


										}  //Fin niveau 5 service 
										departementTreechildren.setParent(divisionItem);
									}

								}  //Fin niveau 4 departement  
								divisionTreechildren.setParent(directionItem);
							}

						}//Fin niveau 3 division
						directionTreechildren.setParent(uniteorgItem);
					}
				}
				//uniteorgTreechildren.setParent(child); 
			}//Fin niveau 2 direction
		}

	}
	
	public void onClick$menuCancelStep1() {
		detach();
	}

	public void onClick$menuNextStep1() {
		userSelected.clear();
		if(rdoui.isChecked()){
			for (int i = 0; i < tree.getSelectedCount(); i++) {			
				if(((Treeitem) tree.getSelectedItems().toArray()[i]).getValue() instanceof Utilisateur){				
					userSelected.add((Utilisateur)((Treeitem) tree.getSelectedItems().toArray()[i]).getValue());				
				}else if(((Treeitem) tree.getSelectedItems().toArray()[i]).getValue() instanceof SygBureauxdcmp){	
					Long myUniteId = ((SygBureauxdcmp)((Treeitem) tree.getSelectedItems().toArray()[i]).getValue()).getId();
					Utilisateur myUser = BeanLocator.defaultLookup(UtilisateurSession.class).findUserChefService(myUniteId, Boolean.TRUE);
					if(myUser!=null)
						userSelected.add(myUser);
				}
			}
			if( tree.getSelectedCount()==0){
				throw new WrongValueException(tree, Labels.getLabel("kermel.error.select.item"));
			}else {				
				if(userSelected.size()>0){
					lstUser.setModel(new SimpleListModel(userSelected));
					agent  = userSelected.get(0);
					step1.setVisible(false);
					step2.setVisible(true);
				}else
					throw new WrongValueException(tree, "Pas d'agent dans :"+" "+((SygBureauxdcmp)((Treeitem) tree.getSelectedItem()).getValue()).getLibelle());
					
			}
		}else{
			if(lstGroupe.getSelectedItem()==null){
				throw new WrongValueException(lstGroupe, Labels.getLabel("kermel.error.select.item"));
			}
			EcoGroupesImputation myGroupe = (EcoGroupesImputation)lstGroupe.getSelectedItem().getValue();			
			List<EcoGroupesImputilisateur> goupImp = BeanLocator.defaultLookup(GroupeImpututilisateurSession.class).findByGroup(myGroupe);
			List<Utilisateur> listutilisateur = new ArrayList<Utilisateur>();
			for(EcoGroupesImputilisateur mygroup : goupImp){
				listutilisateur.add(mygroup.getUtilisateur());
				/////////Le 25/05/2013//////::
				if(mygroup.getUtilisateur()!=null)
					userSelected.add(mygroup.getUtilisateur());
			}				
			lstUser.setModel(new SimpleListModel(listutilisateur));
			step1.setVisible(false);
			step2.setVisible(true);
		}
	}
	
	public void onClick$menuPreviousStep2() {
		step1.setVisible(true);
		step2.setVisible(false);
	}
//	public void onClick$menuNextStep2() {
//		step1.setVisible(true);
//		step2.setVisible(false);
//	}
	public void onBlur$dbImputation(){
		 if(dbImputation.getValue()!=null){
			 dbMaxTraitement.setValue(ToolKermel.dateDecalageJour(dbImputation.getValue(), courrier.getCourrierType().getDureTraitementJour()));
		 }
		
	 }
	public void onCheck$rdnon() {
		gbImputationAgent.setVisible(false);
		gbImputationGroup.setVisible(true);
//		bdGroupe.setDisabled(false);
//		bdService.setDisabled(true);
//		bdAgent.setDisabled(true);
//		bdService.setValue(null);
//		bdAgent.setValue(null);
		agent=null;
		services = null;
//		lstService.setSelectedIndex(-1);
//		lstAgent.setSelectedIndex(-1);
		rdoui.setChecked(false);
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION1, this, null);

	}  

	public void onCheck$rdoui() {
		gbImputationAgent.setVisible(true);
		gbImputationGroup.setVisible(false);
		
//		bdGroupe.setDisabled(true);
//		bdGroupe.setValue(null);
		groupes =null;
//		bdService.setDisabled(false);
//		bdAgent.setDisabled(false);
//		lstGroupe.setSelectedIndex(-1);
		rdnon.setChecked(false);
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);

	}
	public boolean verifField(){
		if(userSelected.size()==0)
			throw new WrongValueException(lstUser, "Vous devez choisir un service");
//		if(rdoui.isChecked()){
//			if(lstService.getSelectedItem()==null)
//				throw new WrongValueException(bdService, "Vous devez choisir un service");
//			if(lstAgent.getSelectedItem()==null)
//				throw new WrongValueException(bdAgent, "Vous devez choisir un agent");
//			
//		}
//		if(rdnon.isChecked()){
//			if(lstGroupe.getSelectedItem()==null)
//				throw new WrongValueException(bdGroupe, "Vous devez choisir un groupe");
//		}
		
		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			SygTCourrierAC tcourr = BeanLocator.defaultLookup(TCourriersAcSession.class).findByCourrierAndAgent(courrier, agent) ;
			if(tcourr!=null){
				if(agent==null)
					agent=tcourr.getAgent();
				throw new WrongValueException(lstUser, "Ce courrier est d�j� imput� �  "+agent.getPrenom()+" "+agent.getNom());
			}			
		}		
		return true;
	}
	public void onOK() {	
		
		if(verifField())
		{
			if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				if(rdnon.isChecked()){
					//Imputation du courrier aux utilisateurs du groupe
					for(int i=0;i<lstUser.getItemCount();i++){
						tcourrier = new SygTCourrierAC();
						agent = (Utilisateur)((Listitem)lstUser.getItemAtIndex(i)).getValue();
						tcourrier.setImpForGroup(true);
						tcourrier.setAgent(agent);
						tcourrier.setBureau(agent.getBureaux());
						tcourrier.setCourrierac(courrier);
						tcourrier.setIdOrigne(idOrigne);
						tcourrier.setTcourrierDateSaisie(new Date());
						tcourrier.setTdateImputation(dbImputation.getValue());
						tcourrier.setTdateMaxTraitement(dbMaxTraitement.getValue());					
						tcourrier.setTimputationPour(comboImputationPour.getValue());
						tcourrier.setImputerpar(user.getLogin());
						tcourrier.setTcourrierInstruction(txtInstruction.getValue());
						if(modeTraitement!=null)
						tcourrier.setModetraitement(modeTraitement);
						if(chkaccusRecept.isChecked())
							tcourrier.setAccuseReception(true);
						else
							tcourrier.setAccuseReception(false);
						
						if(chknotifMail.isChecked())
							tcourrier.setNotificationParMail(true);
						else
							tcourrier.setNotificationParMail(false);				
						BeanLocator.defaultLookup(TCourriersAcSession.class).save(tcourrier);
					}
					
				}else{
					//Imputation du courrier � l'utilisateur
					tcourrier = new SygTCourrierAC();
					tcourrier.setImpForGroup(false);
					tcourrier.setAgent(agent);
					tcourrier.setBureau(agent.getBureaux());
					tcourrier.setCourrierac(courrier);
					tcourrier.setIdOrigne(idOrigne);
					tcourrier.setTcourrierDateSaisie(new Date());
					tcourrier.setTdateImputation(dbImputation.getValue());
					tcourrier.setTdateMaxTraitement(dbMaxTraitement.getValue());
					tcourrier.setTimputationPour(comboImputationPour.getValue());
					tcourrier.setImputerpar(user.getLogin());
					tcourrier.setTcourrierInstruction(txtInstruction.getValue());
					if(chkaccusRecept.isChecked())
						tcourrier.setAccuseReception(true);
					else
						tcourrier.setAccuseReception(false);
					
					if(chknotifMail.isChecked())
						tcourrier.setNotificationParMail(true);
					else
						tcourrier.setNotificationParMail(false);	
					if(modeTraitement!=null)
						tcourrier.setModetraitement(modeTraitement);
					BeanLocator.defaultLookup(TCourriersAcSession.class).save(tcourrier);
				}				
				
				if(chknotifMail.isChecked()){
					if(rdnon.isChecked()){
						for(int i=0;i<lstUser.getItemCount();i++){
							String subject = "Imputation courrier";
							///////////////////////////////////////////////////
							String message = "";
							String url = "http://localhost:8080/Kermel-WEB/";
							String site = "";
							
							
							message ="<html>";
							message +="<style type='text/css'>";
							message +="<!--";
							message +=".Style1 {color: #0000FF}";
							message +="-->";
							message +="</style>";
							message +="<body>";
							message +="<table width='788' border='0' align='center' cellpadding='0' cellspacing='0'>";
							message +="<tr><td height='169'><img src='"+url+"/icone/backA.png' width='788' height='169' /></td> </tr>";
							message +="<tr><td valign='top' background='"+url+"/icone/BackB.png'><table width='80%' height='194' border='0' align='center' cellpadding='0' cellspacing='0'><tr>";
							message +="<td width='38%' height='135' valign='top'><img src='"+url+"/icone/env.jpg' width='225' height='138' /></td>";
							message +="<td width='62%' valign='top'>Imputation  $sujet $titre.";
							message +="<p>Imput� le: $dateimputation</p>";
							message +="<p>$veuillez</p><p>";message +=tcourrier.getTcourrierInstruction()+"</p>";
							message +="<p>Cliquer sur le lien ci-dessous pour acceder au courrier</p><p><a href='"+url+site+"'>Ecourrier</a></p>";
							message +="</td></tr></table></td></tr><tr>";
							message +="<td height='46' valign='top'><img src='"+url+"/icone/BackC.png' width='788' height='46' /></td></tr>";
							message +="</table></body></html>"; 
							
											
//							String content = "Imputation courrier";
//							content +="<html>";
//							content +="<style type='text/css'>";
//							content +="<!--";
//							content +=".Style1 {color: #0000FF}";
//							content +="-->";
//							content +="</style>";
//							content +="<body>";
//							content+=tcourrier.getTcourrierInstruction();

							String usermail = ((Utilisateur)((Listitem)lstUser.getItemAtIndex(i)).getValue()).getMail();//agent.getMail();
							MessageManager.sendMail(subject, message,  usermail,null,null,null,null);
						}
					}else{
						String subject = "Imputation courrier";
						///////////////////////////////////////////////////
//						String content = "Imputation courrier";
//						content +="<html>";
//						content +="<style type='text/css'>";
//						content +="<!--";
//						content +=".Style1 {color: #0000FF}";
//						content +="-->";
//						content +="</style>";
//						content +="<body>";
//						content+=tcourrier.getTcourrierInstruction();
						
						String message = "";
						String url = "http://localhost:8080/Kermel-WEB/";
						String site = "";
						
						
						message ="<html>";
						message +="<style type='text/css'>";
						message +="<!--";
						message +=".Style1 {color: #0000FF}";
						message +="-->";
						message +="</style>";
						message +="<body>";
						message +="<table width='788' border='0' align='center' cellpadding='0' cellspacing='0'>";
						message +="<tr><td height='169'><img src='"+url+"/icone/backA.png' width='788' height='169' /></td> </tr>";
						message +="<tr><td valign='top' background='"+url+"/icone/BackB.png'><table width='80%' height='194' border='0' align='center' cellpadding='0' cellspacing='0'><tr>";
						message +="<td width='38%' height='135' valign='top'><img src='"+url+"/icone/env.jpg' width='225' height='138' /></td>";
						message +="<td width='62%' valign='top'>Imputation  $sujet $titre.";
						message +="<p>Imput� le: $dateimputation</p>";
						message +="<p>$veuillez</p><p>";message +=tcourrier.getTcourrierInstruction()+"</p>";
						message +="<p>Cliquer sur le lien ci-dessous pour acceder au courrier</p><p><a href='"+url+site+"'>Ecourrier</a></p>";
						message +="</td></tr></table></td></tr><tr>";
						message +="<td height='46' valign='top'><img src='"+url+"/icone/BackC.png' width='788' height='46' /></td></tr>";
						message +="</table></body></html>"; 

						String usermail = agent.getMail();
						MessageManager.sendMail(subject, message,  usermail,null,null,null,null);
					}
					
				}				
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				tcourrier.setAgent(agent);
				//tcourrier.setBureau(bureau);
				tcourrier.setCourrierac(courrier);
				tcourrier.setIdOrigne(idOrigne);
				tcourrier.setTcourrierDateSaisie(courrier.getCourrierDate());
				tcourrier.setTdateImputation(dbImputation.getValue());
				tcourrier.setTdateMaxTraitement(dbMaxTraitement.getValue());
				tcourrier.setTcourrierInstruction(txtInstruction.getValue());
				if(chkaccusRecept.isChecked())
					tcourrier.setAccuseReception(true);
				else
					tcourrier.setAccuseReception(false);
				
				if(chknotifMail.isChecked())
					tcourrier.setNotificationParMail(true);
				else
					tcourrier.setNotificationParMail(false);
				if(modeTraitement!=null)
					tcourrier.setModetraitement(modeTraitement);
				BeanLocator.defaultLookup(TCourriersAcSession.class).update(tcourrier);
			}
			
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}		
	
	}
	public void onSelect$lstService(){
//		services = (EcoUniteorga) lstService.getSelectedItem().getValue();
//		bdService.setValue(services.getLibelle());	
//		bdService.close();
//		Events.postEvent(ApplicationEvents.ON_AGENTS, this, null);

	}
	
	public class ServiceRenderer implements ListitemRenderer {
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
				SygBureauxdcmp myService = (SygBureauxdcmp) data;
				item.setValue(myService);
//				nbrcourrierAgent=BeanLocator.defaultLookup(TCourriersAcSession.class).countByServiceAndAgent(null, myagent);
				Listcell cellLibelle = new Listcell(myService.getLibelle());
				cellLibelle.setParent(item);
				
//				EcoServices service=BeanLocator.defaultLookup(ServiceSession.class).findById(myagent.getServices().getId());
//				Listcell cellLibelle = new Listcell(service.getLibelle());
//				cellLibelle.setParent(item);

		}

		
	}
	
	
	public void onFocus$txtRechercherService(){
		 if(txtRechercherService.getValue().equalsIgnoreCase(Labels.getLabel("ecourrier.common.form.rechercher"))) {
			 txtRechercherService.setValue("");
		 }		 
	}
	
	public void  onClick$btnRechercherService(){
		if(txtRechercherService.getValue().equalsIgnoreCase(Labels.getLabel("ecourrier.common.form.rechercher"))) {
			serviceLibelle = null;
			page = null;
		}
		else
		{
			serviceLibelle = txtRechercherService.getValue();
			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}



	
	//Agent
	public void onSelect$lstAgent(){
//		 agent = (Utilisateur) lstAgent.getSelectedItem().getValue();
//		 bdAgent.setValue(agent.getPrenom()+ " "+agent.getNom());
//		 bdAgent.close();
	}
	
	public class AgentRenderer implements ListitemRenderer {
		public void render(Listitem item, Object data, int index)  throws Exception {
			Utilisateur myagent = (Utilisateur) data;
			item.setValue(myagent);
			nbrcourrierAgent=BeanLocator.defaultLookup(TCourriersAcSession.class).countByServiceAndAgent(null, myagent,null);
			Listcell cellLibelle = new Listcell(myagent.getPrenom()+" "+myagent.getNom()+"( nombre de dossier :"+nbrcourrierAgent+" )");
			cellLibelle.setParent(item);

		} 
	}
	
	public class UtilisateurRenderer implements ListitemRenderer {
		public void render(Listitem item, Object data, int index)  throws Exception {
			Utilisateur myagent = (Utilisateur) data;
			item.setValue(myagent);
			nbrcourrierAgent=BeanLocator.defaultLookup(TCourriersAcSession.class).countByServiceAndAgent(null, myagent,null);
			Listcell cellPrenom = new Listcell(myagent.getPrenom());
			cellPrenom.setParent(item);
			Listcell cellNom = new Listcell(myagent.getNom());
			cellNom.setParent(item);
			Listcell cellNbrCourrier = new Listcell("( nombre de dossier :"+nbrcourrierAgent+" )");
			cellNbrCourrier.setParent(item);

		} 
	}
	//Groupes
	public void onSelect$lstGroupe(){
//		 groupes = (EcoGroupes) lstGroupe.getSelectedItem().getValue();
//		 bdGroupe.setValue(groupes.getLibelle());
//		 bdGroupe.close();
	}
	
	
	public class GroupeRenderer implements ListitemRenderer {
		public void render(Listitem item, Object data, int index)  throws Exception {
			EcoGroupesImputation mygroupe = (EcoGroupesImputation) data;
			item.setValue(mygroupe);
//			nbrcourrierAgent=BeanLocator.defaultLookup(TCourriersAcSession.class).countByServiceAndAgent(null, myagent);
			Listcell cellCode = new Listcell(mygroupe.getId().toString());
			cellCode.setParent(item);
			Listcell cellLibelle = new Listcell(mygroupe.getLibelle());
			cellLibelle.setParent(item);
			Listcell cellDescription = new Listcell(mygroupe.getDescription());
			cellDescription.setParent(item);

		} 
	}
	
	public void onFocus$txtRechercherAgent(){
		 if(txtRechercherAgent.getValue().equalsIgnoreCase(Labels.getLabel("ecourrier.common.form.rechercher"))) {
			 txtRechercherAgent.setValue("");
		 }		 
	}
	
	public void  onClick$btnRechercherAgent(){
		if(txtRechercherAgent.getValue().equalsIgnoreCase(Labels.getLabel("ecourrier.common.form.rechercher"))) {
			agentPrenom = null;
			page = null;
		}
		else
		{
			agentPrenom = txtRechercherAgent.getValue();
			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_AGENTS, this, page);
	}

	public void onFocus$txtRechercherGroupe(){
		 if(txtRechercherGroupe.getValue().equalsIgnoreCase(Labels.getLabel("ecourrier.common.form.rechercher"))) {
			 txtRechercherGroupe.setValue("");
		 }		 
	}
	
	public void  onClick$btnRechercherGroupe(){
		if(txtRechercherGroupe.getValue().equalsIgnoreCase(Labels.getLabel("ecourrier.common.form.rechercher"))) {
			groupeLibelle = null;
			page = null;
		}
		else
		{
			groupeLibelle = txtRechercherGroupe.getValue();
			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION1, this, page);
	}

	//Instruction
	public void onSelect$listInstruction(){
//		instructions=listInstruction.getSelectedItem().getValue();
//		bdInstructions.setValue(instructions.getLibelle());
//		bdInstructions.close();
//		if(txtInstruction.getValue()==null || "".equals(txtInstruction.getValue()))
//			txtInstruction.setValue(((EcoInstructionsPredefinies) listInstruction.getSelectedItem().getValue()).getLibelle());
//		else
//			txtInstruction.setValue(";"+((EcoInstructionsPredefinies) listInstruction.getSelectedItem().getValue()).getLibelle());
		 
	}
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		// TODO Auto-generated method stub
		SygInstruction instruction = (SygInstruction) data;
		//item.setAttribute(UIConstants.ATTRIBUTE_LIBELLE, instruction.getLibelle());
		item.setValue(instruction);
		Listcell cellLibelle = new Listcell(instruction.getLibelle());
		cellLibelle.setParent(item);
	}
	  
	public void onFocus$txtlibelleInstruction(){
		 if(txtlibelleInstruction.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.rechercher"))) {
			 txtlibelleInstruction.setValue("");
		 }		 
	}
	
	public void onOk$txtlibelleInstruction(){
		onClick$btnRechercherInst();	 
	}
	
	public void  onClick$btnRechercherInst(){
		if(txtlibelleInstruction.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.rechercher"))) {
			instructionLibelle = null;
			page = null;
		}
		else
		{
			instructionLibelle = txtlibelleInstruction.getValue();
			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	public void onClick$btnInstruction(){
		if(listInstruction.getSelectedItem()==null) {
			throw new WrongValueException(listInstruction, "Veuillez s�lectionner un �l�ment");
		}else{
			if(txtInstruction.getValue()==null || "".equals(txtInstruction.getValue()))
				txtInstruction.setValue(((SygInstruction) listInstruction.getSelectedItem().getValue()).getLibelle());
			else
				txtInstruction.setValue(txtInstruction.getValue()+";"+((SygInstruction) listInstruction.getSelectedItem().getValue()).getLibelle());
		}
		
	}
	
	public void onSelect$lstModeTraitement(){
	modeTraitement = (SygModeTraitement) lstModeTraitement.getSelectedItem().getValue();
	if(modeTraitement!=null){
		bdModeTraitment.setValue(modeTraitement.getLibellemodetraitement());
	}
	bdModeTraitment.close();
}
	
	public void onClick$btnRechercherModeTraitement(){
	if(txtRechercherModeTraitement.getValue().trim()!=null && !txtRechercherModeTraitement.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.courrier.form.choix.modetraitement")) ){
		libelleModeT = txtRechercherModeTraitement.getValue();
	}else{
		libelleModeT = null;
	}

	Events.postEvent(LIST_MODETRAITEMENT_COURRIER_MODEL_CHANGE, this, null);
}

public void onFocus$txtRechercherModeTraitement(){
	if(txtRechercherModeTraitement.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.courrier.form.choix.modetraitement"))){
		txtRechercherModeTraitement.setValue(null);
	}
}

public void onBlur$txtRechercherModeTraitement(){
	if(txtRechercherModeTraitement.getValue().equalsIgnoreCase("")||txtRechercherModeTraitement.getValue()==null){
		txtRechercherModeTraitement.setValue(Labels.getLabel("kermel.courrier.form.choix.modetraitement"));
	}
}

private class ModeTraitementRender implements ListitemRenderer{

	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygModeTraitement rapport = (SygModeTraitement) data;
		item.setValue(rapport);

		Listcell cellcodemodetraitement = new Listcell(rapport.getCodemodetraitement());
		cellcodemodetraitement.setParent(item);
		Listcell celllibellemodetraitement = new Listcell(rapport.getLibellemodetraitement());
		celllibellemodetraitement.setParent(item);
//		if(codeModeT!=null && codeModeT.equalsIgnoreCase(rapport.getCodemodetraitement())){
//			item.setSelected(true);
//
//		}

	}

}
}
