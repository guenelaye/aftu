package sn.ssi.kermel.web.courriers.controllers;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

public class TabCourriersAc extends org.zkoss.zul.Tabbox {
    
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void onCreate() {
	}
    
	// Pour le chargement  la demande
	public void onSelect() {
        
		Tab item = getSelectedTab();
		
		if(item != null && item.getId().equals("TAB_IPUTATION")) {
			Include inc = (Include) this.getFellowIfAny("incIPUTATION");
            inc.setSrc("/courriers/dossiercourriers/listimputation.zul");
        }
		
		if(item != null && item.getId().equals("TAB_PJ")) {
			Include inc = (Include) this.getFellowIfAny("incPJ");
            inc.setSrc("/courriers/dossiercourriers/piecejointes.zul");
        }
		
		if(item != null && item.getId().equals("TAB_REPONSE")) {
			Include inc = (Include) this.getFellowIfAny("incREPONSE");
            inc.setSrc("/courriers/dossiercourriers/reponseCourrier.zul");
        }	
		
    }
}
