package sn.ssi.kermel.web.courriers.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.courriers.ejb.TCourriersAcSession;
import sn.ssi.kermel.be.entity.SygBureauxdcmp;
import sn.ssi.kermel.be.entity.SygCourrierAc;
import sn.ssi.kermel.be.entity.SygTCourrierAC;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.BureauxdcmpSession;
import sn.ssi.kermel.be.security.UtilisateurSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

public class ListImputationController extends AbstractWindow implements
EventListener, AfterCompose, ListitemRenderer {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private Listbox lstListe;
    private Paging pgPagination;
    private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage,page;
    public static final String CURRENT_MODULE="CURRENT_MODULE";
    private Listheader lshService;
    Session session = getHttpSession();    
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherService1,txtRechercherAgent1;
    private Bandbox bdAgent1;
	private Paging pgAgent1;
	private Utilisateur agent;
	private Listbox lstAgent1;
	private Bandbox bdService1;
	private Paging pgService1;
	private Listbox lstService1;
	private SygBureauxdcmp bureau;
	private SygCourrierAc courrier;
	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private String serviceLibelle1 = null;
    @Override
    public void onEvent(Event event) throws Exception {		
	// TODO Auto-generated method stub
	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)){
	    if (event.getData() != null) {
		activePage = Integer.parseInt((String) event.getData());
		byPage = -1;
		pgPagination.setPageSize(1000);
	    } else {
		byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
		activePage = pgPagination.getActivePage() * byPage;
		pgPagination.setPageSize(byPage);
	    }
	    List<SygTCourrierAC> tcourrier = BeanLocator.defaultLookup(TCourriersAcSession.class).findByServiceAndAgent(activePage,byPage, bureau, agent,courrier);
	    SimpleListModel listModel = new SimpleListModel(tcourrier);
	    lstListe.setModel(listModel);
	    pgPagination.setTotalSize(BeanLocator.defaultLookup(TCourriersAcSession.class).countByServiceAndAgent(bureau, agent,courrier));
	}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)){
	    //final String uri = "/courriers/dossiercourriers/imputationAgents.zul";
	    final String uri = "/courriers/dossiercourriers/imputerAgent.zul";
	    final HashMap<String, String> display = new HashMap<String, String>();
	    display.put(DSP_TITLE, Labels.getLabel("kermel.courrierac.dossiers.ajout.imputation"));
	    display.put(DSP_HEIGHT,"600px");
	    display.put(DSP_WIDTH, "80%");

	    final HashMap<String, Object> data = new HashMap<String, Object>();
	    //		data.put(ModeReceptionFormController.WINDOW_PARAM_MODE,
	    //				UIConstants.MODE_NEW);
	    Executions.getCurrent().setAttribute("MODE", UIConstants.MODE_NEW);

	    showPopupWindow(uri, data, display);
	}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)){
	    //final String uri = "/courriers/dossiercourriers/imputationAgents.zul";
	    final String uri = "/courriers/dossiercourriers/imputerAgent.zul";
	    if(lstListe.getSelectedItem()==null) {
		throw new WrongValueException(lstListe, "");
	    }
	    final HashMap<String, String> display = new HashMap<String, String>();
	    display.put(DSP_TITLE, Labels.getLabel("kermel.courrierac.dossiers.modif.imputation"));
	    display.put(DSP_HEIGHT,"600px");
	    display.put(DSP_WIDTH, "80%");

	    final HashMap<String, Object> data = new HashMap<String, Object>();	    
	    Executions.getCurrent().setAttribute("MODE", UIConstants.MODE_EDIT);
	    Executions.getCurrent().setAttribute("TCOURRIER", lstListe.getSelectedItem().getValue());
	    showPopupWindow(uri, data, display);
		}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)){
			//Service
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;				
				pgService1.setPageSize(byPageBandBox);			
			}
			else {
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgService1.getActivePage() * byPageBandBox;
				pgService1.setPageSize(byPageBandBox);
			}			
			List<SygBureauxdcmp> services = BeanLocator.defaultLookup(BureauxdcmpSession.class).findRech(activePage, byPageBandBox, serviceLibelle1, null, null);
			lstService1.setModel(new SimpleListModel(services));
			pgService1.setTotalSize(BeanLocator.defaultLookup(BureauxdcmpSession.class).countRech(serviceLibelle1, null,null));
		}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION1)){			
			//Agent
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;				
				pgService1.setPageSize(byPageBandBox);			
			}
			else {
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgAgent1.getActivePage() * byPageBandBox;
				pgAgent1.setPageSize(byPageBandBox);
			}
			List<Utilisateur> depart = BeanLocator.defaultLookup(UtilisateurSession.class).findUtilisateurs(activePage, byPageBandBox, null, courrier.getCourrierAutoriteContractante(), null);
			lstAgent1.setModel(new SimpleListModel(depart));
			pgAgent1.setTotalSize(BeanLocator.defaultLookup(UtilisateurSession.class).countAllUtilisateurs(null, courrier.getCourrierAutoriteContractante(), null));
			
		}else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)) {
			if (lstListe.getSelectedItem() == null)
				throw new WrongValueException(lstListe, Labels.getLabel("grh.error.select.item"));

			HashMap<String, String> display = new HashMap<String, String>();
			display.put(MessageBoxController.DSP_MESSAGE, Labels.getLabel("kermel.common.form.question.supprimer") + " ?");
			display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
			display.put(MessageBoxController.DSP_HEIGHT, "150px");
			display.put(MessageBoxController.DSP_WIDTH, "100px");

			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
			showMessageBox(display, map);
		} else if (event.getName().equals(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {
			SygTCourrierAC imputationagent = (SygTCourrierAC) ((HashMap<String, Object>) event.getData()).get(CURRENT_MODULE);
			BeanLocator.defaultLookup(TCourriersAcSession.class).delete(imputationagent.getTcourrierId());
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);			
		}
    }
    @Override
    public void afterCompose() {
	// TODO Auto-generated method stub
	Components.wireFellows(this, this);
	Components.addForwards(this, this);

	courrier = (SygCourrierAc) session.getAttribute("courrierAcInssession");
	
	addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
	addEventListener(ApplicationEvents.ON_AUTRE_ACTION1, this);
	addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
	addEventListener(ApplicationEvents.ON_ADD, this);
	addEventListener(ApplicationEvents.ON_EDIT, this);
	addEventListener(ApplicationEvents.ON_DETAILS, this);
	addEventListener(ApplicationEvents.ON_DELETE, this);
	addEventListener(ApplicationEvents.ON_SUIVI, this);
	addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	//Service
	lstService1.setItemRenderer(new ServiceRenderer());
	pgService1.setPageSize(byPageBandBox);
	pgService1.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTRE_ACTION);
	Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);		
	//Agent
	lstAgent1.setItemRenderer(new AgentRenderer());
	pgAgent1.setPageSize(byPageBandBox);
	pgAgent1.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTRE_ACTION1);
	Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION1, this, null);
	lshService.setSortAscending(new FieldComparator("courrierReference", false));
	lshService.setSortDescending(new FieldComparator("courrierReference", true));

	lstListe.setItemRenderer(this);
	pgPagination.setPageSize(byPage);
	pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	//login = ((String) getHttpSession().getAttribute("user"));
    }
    @Override
    public void render(Listitem item, Object data, int index)  throws Exception {
	// TODO Auto-generated method stub
	    SygTCourrierAC myCourrier = (SygTCourrierAC) data;
		item.setValue(myCourrier);
	
		Listcell cellService = new Listcell(myCourrier.getBureau().getLibelle());
		cellService.setParent(item);
	
		Listcell cellNomagent = new Listcell("");
		if(myCourrier.getAgent()!=null) {
			cellNomagent.setLabel(myCourrier.getAgent().getNom());
		}
		cellNomagent.setParent(item);
	
		Listcell cellPrenomagent = new Listcell("");
		if(myCourrier.getAgent()!=null) {
			cellPrenomagent.setLabel(myCourrier.getAgent().getPrenom());
		}
		cellPrenomagent.setParent(item);
		
		Listcell cellInstruction = new Listcell(myCourrier.getTcourrierInstruction());		
		cellInstruction.setParent(item);
		
		Listcell cellModeTraitement = new Listcell("");
		if(myCourrier.getModetraitement()!=null) {
			cellModeTraitement.setLabel(myCourrier.getModetraitement().getLibellemodetraitement());
		}
		cellModeTraitement.setParent(item);

    }
    
    public void onSelect$lstService1(){
		 bureau = (SygBureauxdcmp) lstService1.getSelectedItem().getValue();
		 bdService1.setValue(bureau.getLibelle());		
		 bdService1.close();
		 //Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);

	}
    
    public class ServiceRenderer implements ListitemRenderer {

		public void render(Listitem item, Object data, int index)  throws Exception {
			SygBureauxdcmp services = (SygBureauxdcmp) data;
			item.setValue(services);			
			Listcell cellLibelle = new Listcell(services.getLibelle());
			cellLibelle.setParent(item);

		}
	}
    
    public void onFocus$txtRechercherService1(){
		 if(txtRechercherService1.getValue().equalsIgnoreCase(Labels.getLabel("gred.common.form.rechercher"))) {
			 txtRechercherService1.setValue("");
		 }		 
	}
	
	public void  onClick$btnRechercherService1(){
		if(txtRechercherService1.getValue().equalsIgnoreCase(Labels.getLabel("gred.common.form.rechercher"))) {
			serviceLibelle1 = null;
			//page = null;
		}
		else
		{
			serviceLibelle1 = txtRechercherService1.getValue();
			//page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}
    
    public class AgentRenderer implements ListitemRenderer {
		public void render(Listitem item, Object data, int index)  throws Exception {
			Utilisateur myagent = (Utilisateur) data;
			item.setValue(myagent);
			
			Listcell cellLibelle = new Listcell(myagent.getPrenom());
			cellLibelle.setParent(item);

		} 
	}
    
    public void onSelect$lstAgent1(){
		 agent = (Utilisateur) lstAgent1.getSelectedItem().getValue();
		 bdAgent1.setValue(agent.getPrenom()+ " "+agent.getNom());
		 bdAgent1.close();
	}
    public void onFocus$txtRechercherAgent1(){
		 if(txtRechercherAgent1.getValue().equalsIgnoreCase(Labels.getLabel("gred.common.form.rechercher"))) {
			 txtRechercherAgent1.setValue("");
		 }		 
	}
	
	public void  onClick$btnRechercherAgent1(){
		if(txtRechercherAgent1.getValue().equalsIgnoreCase(Labels.getLabel("gred.common.form.rechercher"))) {
			//agentPrenom1 = null;
			//page = null;
		}
		else
		{
//			agentPrenom1 = txtRechercherAgent1.getValue();
//			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION1, this, page);
	}
}
