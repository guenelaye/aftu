package sn.ssi.kermel.web.categindicstatgle.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygCatIndStatGle;
import sn.ssi.kermel.be.session.CatIndStatGleSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

public class ListeController  extends AbstractWindow implements AfterCompose, EventListener, ListitemRenderer {
	private Listbox list;
	public Textbox txtCode,txtLibelle;
    String code, libelle;
	private Paging pg;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	public static final String WINDOW_PARAM_MODE = "MODE";
//	private GrhSousMenu monSousMenu;
//	private Menuitem ADD_UNITES, MOD_UNITES, DEL_UNITES;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		/*
		 * On indique que la fenetre est notifiee a chaque fois que le "model"
		 * des modules est mis a jour soit apres une insertion, supresion,
		 * modif,...
		 */
//		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
//		monSousMenu.setFea_code("FEA_UNITES");
//		monSousMenu.afterCompose();
//		/* pour les menuitem eventuellement crees */
//		Components.wireFellows(this, this); // mais le wireFellows precedent
//		// reste indispensable
//		/* reprise des forwards definis dans le .zul */
//		if (ADD_UNITES != null) {
//			ADD_UNITES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
//		}
//		if (MOD_UNITES != null) {
//			MOD_UNITES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
//		}
//		if (DEL_UNITES != null) {
//			DEL_UNITES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
//		}
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		list.setItemRenderer(this);

		pg.setPageSize(byPage);
		
		pg.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	/**
	 * Permet de gerer les evenements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 List<SygCatIndStatGle> unites = BeanLocator.defaultLookup(CatIndStatGleSession.class).find(pg.getActivePage()*byPage,byPage, code, libelle);
			 SimpleListModel listModel = new SimpleListModel(unites);
			list.setModel(listModel);
			pg.setTotalSize(BeanLocator.defaultLookup(CatIndStatGleSession.class).count());
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/categIndicStatGle/form.zul";

		
			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("ajout"));
			display.put(DSP_HEIGHT,"160px");
			display.put(DSP_WIDTH, "25%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormController.WINDOW_PARAM_MODE,UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		}
		
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				throw new WrongValueException(list, Labels.getLabel("alert.selection"));
				
			}
			final String uri = "/categIndicStatGle/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"160px");
			display.put(DSP_WIDTH, "25%");
			display.put(DSP_TITLE, Labels.getLabel("modification"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(FormController.PARAM_WIDOW_CODE, list
					.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
					throw new WrongValueException(list, Labels.getLabel("alert.selection"));
				HashMap<String, String> display = new HashMap<String, String>();
				// permet de fixer les dimensions du popup
				
				display.put(MessageBoxController.DSP_MESSAGE, Labels.getLabel("alert.suppression"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("suppression"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>();
				// permet de passer des parametres au popup
				
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){		
			for (int i = 0; i < list.getSelectedCount(); i++) {
				Integer codes = (Integer)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
				System.out.println(codes);
				BeanLocator.defaultLookup(CatIndStatGleSession.class).delete(codes);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
	}

	/**
	 * Definit comment un element de la liste est affiche.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygCatIndStatGle Unites = (SygCatIndStatGle) data;
		item.setValue(Unites.getId());

		 Listcell cellCode = new Listcell(Unites.getCode());
		cellCode.setParent(item);
		 Listcell cellLibelle = new Listcell(Unites.getLibelle());
		cellLibelle.setParent(item);
	}
	
	public void onClick$bchercher()
	{
		onOK();
	}
	
	public void onOK()
	{
		code = txtCode.getValue();
		libelle = txtLibelle.getValue();
		
		if(!code.equalsIgnoreCase(Labels.getLabel("code")))
		{
			if(libelle.equalsIgnoreCase(Labels.getLabel("libelle")))
				 libelle="";
		    Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
		
		if(!libelle.equalsIgnoreCase(Labels.getLabel("libelle")))
		{
			if(code.equalsIgnoreCase(Labels.getLabel("code")))
				 code="";
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
		
		if(code.equalsIgnoreCase(Labels.getLabel("code")) && libelle.equalsIgnoreCase(Labels.getLabel("libelle")))
		{
			code=""; 
			libelle="";
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}	
	}
	
	public void onFocus$txtCode()
	{
		if(txtCode.getValue().equalsIgnoreCase("code"))
			txtCode.setValue("");
		else
			code=txtCode.getValue();
	}
	public void onFocus$txtLibelle()
	{
		if(txtLibelle.getValue().equalsIgnoreCase("libelle"))
			txtLibelle.setValue("");
		else
			libelle=txtLibelle.getValue();
	}
	public void onBlur$txtCode()
	{
		if(txtCode.getValue().equals(""))
			txtCode.setValue("code");
	}
	public void onBlur$txtLibelle()
	{
		if(txtLibelle.getValue().equals(""))
			txtLibelle.setValue("libelle");
	}

}
