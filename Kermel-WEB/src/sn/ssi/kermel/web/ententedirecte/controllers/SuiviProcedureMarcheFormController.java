package sn.ssi.kermel.web.ententedirecte.controllers;


import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAttributions;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygGarantiesDossiers;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygPrestatairegreagre;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.proceduresderogatoires.ejb.ProcedurederogatoireSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



/**

 * @author Adama samb
 * @since mardi 11 Janvier 2011, 12:34
 
 */
public class SuiviProcedureMarcheFormController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private Include pgActes;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	Session session = getHttpSession();
	private Label lblObjets,lblModePassation,lblMontant,lblModeSelection,lblType,lgtitre,lblDatecreation;
	private String LibelleTab,libelle,avalider="oui";
	private Tab TAB_TDOSSIERS;
	private Tree tree;
	private Treecell celltdossiers,cellsouscription,cellsouscriptions,cellfactureproforma,cellmiseenvalidation,cellnotificationmarche,cellapprobation;
	private Include idinclude;
	private Treeitem treetdossiers,itemsouscription,itemfactureproforma,itemmiseenvalidation,itemnotificationmarche,itemapprobation;
	private Treeitem treeitem;
	List<SygGarantiesDossiers> garanties = new ArrayList<SygGarantiesDossiers>();
	SygPrestatairegreagre prestataire=new SygPrestatairegreagre();
	
    List<SygDocuments> documents = new ArrayList<SygDocuments>();
	SygDossiers dossier=new SygDossiers();
	private int nombre;
	SygAttributions attributaire=new SygAttributions();
	List<SygLots> lots = new ArrayList<SygLots>();
	 SygAutoriteContractante autorite=new SygAutoriteContractante();
	 
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		


	}

	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		LibelleTab=(String) session.getAttribute("libelle");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		lblObjets.setValue(appel.getApoobjet());
		lblModePassation.setValue(appel.getModepassation().getLibelle());
		lblMontant.setValue(ToolKermel.format2Decimal(appel.getApomontantestime()));
		lblModeSelection.setValue(appel.getModeselection().getLibelle());
		lblType.setValue(appel.getTypemarche().getLibelle());
		
		lblDatecreation.setValue(UtilVue.getInstance().formateLaDate2(appel.getApodatecreation()));
		
		prestataire=BeanLocator.defaultLookup(ProcedurederogatoireSession.class).getPrestataire(autorite, appel);
		
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		
	
		if(prestataire!=null)
		{
	
			celltdossiers.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			cellsouscriptions.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			celltdossiers.setImage("/images/puce.png");
			cellsouscriptions.setImage("/images/puce.png");
			cellfactureproforma.setImage("/images/rdo_on.png");
			cellfactureproforma.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
			if(dossier!=null)
			{
				if(dossier.getDosDateSignature()!=null)
				{
					cellsouscription.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					nombre=nombre+1;
					cellsouscription.setImage("/images/rdo_on.png");
					cellsouscription.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.souscritle")+ " "
					+UtilVue.getInstance().formateLaDate(dossier.getDosDateSignature()));
				}
				if(dossier.getDosDateMiseValidation()==null)
					cellmiseenvalidation.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.enattentevalidation"));
				
				if(dossier.getDosDateValidation()!=null)
				{
					cellsouscription.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					nombre=nombre+1;
					cellmiseenvalidation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellmiseenvalidation.setImage("/images/rdo_on.png");
				    cellmiseenvalidation.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validationdossier")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateValidation()));
			
				}
				if(dossier.getDosDateNotification()!=null)
				{
					nombre=nombre+ 1;
					cellnotificationmarche.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellnotificationmarche.setImage("/images/rdo_on.png");
					cellnotificationmarche.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.notificationmarche.notifiele")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateNotification()));

				}
				if(dossier.getDosDateApprobation()!=null)
				{
					nombre=nombre+ 1;
					cellapprobation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellapprobation.setImage("/images/rdo_on.png");
					cellapprobation.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.approbationmarche.approuvele")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateApprobation()));

				}
				if(nombre==4)
				{
					celltdossiers.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					celltdossiers.setImage("/images/rdo_on.png");
					cellsouscriptions.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellsouscriptions.setImage("/images/rdo_on.png");
				}
				
			}

		}
		
		if(LibelleTab!=null)
		{
			if(LibelleTab.equals("traitementsdossiers"))
			{
				 idinclude.setSrc("/passationsmarches/ententedirecte/dossierssouscription.zul");	
				 tree.setSelectedItem(treetdossiers);    
				 
			}
			if(LibelleTab.equals("factureproforma"))
			{
				 idinclude.setSrc("/passationsmarches/ententedirecte/formprestataire.zul");
				 tree.setSelectedItem(itemfactureproforma);   
				 
			}
			if(LibelleTab.equals("souscriptiondumarche"))
			{
				 idinclude.setSrc("/passationsmarches/ententedirecte/souscriptiondumarche.zul");
				 tree.setSelectedItem(itemsouscription);   
				 
			}
			if(LibelleTab.equals("validationdossier"))
			{
				idinclude.setSrc("/passationsmarches/ententedirecte/soumissionpourvalidation.zul");	
				tree.setSelectedItem(itemmiseenvalidation);
			}
			if(LibelleTab.equals("notificationmarche"))
			{
				idinclude.setSrc("/passationsmarches/ententedirecte/formnotificationmarche.zul");	
				tree.setSelectedItem(itemnotificationmarche);
			}
			if(LibelleTab.equals("approbationmarche"))
			{
				idinclude.setSrc("/passationsmarches/ententedirecte/formapprobationmarche.zul");	
				tree.setSelectedItem(itemapprobation);
				
			}
			
		}
		else
		{
			 idinclude.setSrc("/passationsmarches/ententedirecte/dossierssouscription.zul");	
			 tree.setSelectedItem(treetdossiers);     
			
		}
		  
		lgtitre.setValue(Labels.getLabel("kermel.plansdepassation.infos.generales.referencedossier")+": "+appel.getAporeference());
		
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}


	public void onSelect$tree() throws Exception
	{
		InfosTree();
	}
	

	public void InfosTree() throws Exception{
		
		/////////////Preparation du dossier//////////
		if(tree.getSelectedItem().getId().equals("itemfactureproforma"))
		{
			idinclude.setSrc("/passationsmarches/ententedirecte/formprestataire.zul");
		}
		
		if(tree.getSelectedItem().getId().equals("itemsouscription"))
		{
			if(prestataire==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ententedirecte.saisirfactureproforma"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				idinclude.setSrc("/passationsmarches/ententedirecte/souscriptiondumarche.zul");	
					
			
			}
			
		}
		if(tree.getSelectedItem().getId().equals("itemmiseenvalidation"))
		{
			if(prestataire==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ententedirecte.saisirfactureproforma"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				if(dossier==null)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.souscriredossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				}
				else
				{
					idinclude.setSrc("/passationsmarches/ententedirecte/soumissionpourvalidation.zul");	
						
				
				}
					
			
			}
			
		}
		if((tree.getSelectedItem().getId().equals("itemnotificationmarche")))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				 if(dossier.getDosDateValidation()==null)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validerdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
						 idinclude.setSrc("/passationsmarches/ententedirecte/formnotificationmarche.zul");
				 }
			}
			
		}
		if((tree.getSelectedItem().getId().equals("itemapprobation")))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				 if(dossier.getDosDateNotification()==null)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.notifierdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
		              idinclude.setSrc("/passationsmarches/ententedirecte/formapprobationmarche.zul");
				 }
			}
		}
		if(tree.getSelectedItem().getId().equals("treetdossiers"))
		{
			 idinclude.setSrc("/passationsmarches/ententedirecte/dossierssouscription.zul");	
		}
	}

}