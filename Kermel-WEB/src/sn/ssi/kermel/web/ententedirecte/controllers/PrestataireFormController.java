package sn.ssi.kermel.web.ententedirecte.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygPays;
import sn.ssi.kermel.be.entity.SygPrestatairegreagre;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.proceduresderogatoires.ejb.ProcedurederogatoireSession;
import sn.ssi.kermel.be.referentiel.ejb.FournisseurSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;
import sn.ssi.kermel.web.referentiel.controllers.FournisseurFormController;

@SuppressWarnings("serial")
public class PrestataireFormController extends AbstractWindow implements
EventListener, AfterCompose {

	private Listbox lstListe,lstBailleur;
	private Paging pgPagination,pgBailleur;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtcommentaires,txtVersionElectronique,txtmail,txtTelephone,txtFax;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference,libellebailleur=null;
    private int parent;
     List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherFournisseur;
    SygBailleurs bailleur=null;
    private Decimalbox dcmontantprevu;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idbailleur;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	private Label lbltitre;
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuValider;
	private Datebox dtdatepublication;
	private String nomFichier;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	private Image image;
	private Div step0,step1;
	private Iframe idIframe;
	private String extension,images,nomfournisseur;
	SygDocuments document=new SygDocuments();
	SygPrestatairegreagre prestataire=new SygPrestatairegreagre();
	SygPrestatairegreagre prestataires=new SygPrestatairegreagre();
	SygPays pays=new SygPays();
	SygAutoriteContractante autorite=new SygAutoriteContractante();
	private Listbox lstFournisseur;
	private Paging pgFournisseur;
	private Bandbox bdFournisseur;
	SygFournisseur fournisseur=new SygFournisseur();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	   
			
		addEventListener(ApplicationEvents.ON_FOURNISSEUR, this);
		pgFournisseur.setPageSize(byPageBandbox);
		pgFournisseur.addForward("onPaging", this, ApplicationEvents.ON_FOURNISSEUR);
		lstFournisseur.setItemRenderer(new PaysRenderer());
		Events.postEvent(ApplicationEvents.ON_FOURNISSEUR, this, null);
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		autorite=appel.getAutorite();
		prestataires=BeanLocator.defaultLookup(ProcedurederogatoireSession.class).getPrestataire(autorite, appel);
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(prestataires!=null)
		{
			prestataire=prestataires;
			pays=prestataire.getPays();
		    fournisseur=BeanLocator.defaultLookup(FournisseurSession.class).findById(prestataire.getFournisseur().getId());
		    bdFournisseur.setValue(fournisseur.getNom());
			menuValider.setDisabled(true);
			txtcommentaires.setValue(prestataire.getCommentaire());
			txtmail.setValue(prestataire.getAdresseMail());
			txtVersionElectronique.setValue(prestataire.getFacture());
			txtTelephone.setValue(prestataire.getNumero());
			Infosdossiers();
		
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {
	 if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_FOURNISSEUR)){
				if (event.getData() != null) {
					if ((Integer) event.getData()==-1000) {
						fournisseur=(SygFournisseur) Executions.getCurrent().getAttribute("fournisseur");
						bdFournisseur.setValue(fournisseur.getNom());
						txtTelephone.setValue(fournisseur.getTel());
						txtFax.setValue(fournisseur.getFax());
						txtmail.setValue(fournisseur.getEmail());
						
						byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
						activePage = pgFournisseur.getActivePage() * byPageBandbox;
						pgFournisseur.setPageSize(byPageBandbox);
					}
					else
					{
						activePage = Integer.parseInt((String) event.getData());
						byPageBandbox = -1;
						pgFournisseur.setPageSize(1000);
					}
				}
				else
				{
					byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
					activePage = pgFournisseur.getActivePage() * byPageBandbox;
					pgFournisseur.setPageSize(byPageBandbox);
				}
			
				List<SygFournisseur> fournisseurs = BeanLocator.defaultLookup(FournisseurSession.class).find(activePage, byPageBandbox,nomfournisseur,null,null);
				lstFournisseur.setModel(new SimpleListModel(fournisseurs));
				pgFournisseur.setTotalSize(BeanLocator.defaultLookup(FournisseurSession.class).count(nomfournisseur,null,null));
				
			}
	}
	public void Infosdossiers()
	{
		if(prestataire.getFacture()!=null)
		{
			extension=prestataire.getFacture().substring(prestataire.getFacture().length()-3,  prestataire.getFacture().length());
			 if(extension.equalsIgnoreCase("pdf"))
				 images="/images/icone_pdf.png";
			 else  
				 images="/images/word.jpg";
			 
			image.setVisible(true);
			image.setSrc(images);
			nomFichier=prestataire.getFacture();
		}
	}
private boolean checkFieldConstraints() {
		
		try {
		
		
			if(bdFournisseur.getValue().equals(""))
		     {
        errorComponent = bdFournisseur;
        errorMsg = Labels.getLabel("kermel.paiement.consultation.fournisseur")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtmail.getValue().equals(""))
		     {
        errorComponent = txtmail;
        errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.presencerepresentantsoum.mail")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
          if (ToolKermel.ControlValidateEmail(txtmail.getValue())) {
				
			} else {
				errorComponent = txtmail;
                errorMsg = Labels.getLabel("kermel.referentiel.personne.email.incorrect");
 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
 				lbStatusBar.setValue(errorMsg);
 				throw new WrongValueException(errorComponent,errorMsg);
			}
          if(txtVersionElectronique.getValue().equals(""))
		     {
     errorComponent = txtVersionElectronique;
     errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ententedirecte.factureproforma")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			return true;
			
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

		public void onClick$menuValider() {
			if(checkFieldConstraints())
			{
				
				prestataire.setAppel(appel);
				prestataire.setAutorite(appel.getAutorite());
				prestataire.setCommentaire(txtcommentaires.getValue());
				prestataire.setAdresseMail(txtmail.getValue());
				prestataire.setRaisonsociale(fournisseur.getNom());
				prestataire.setFacture(txtVersionElectronique.getValue());
				prestataire.setPays(pays);
				prestataire.setFournisseur(fournisseur);
				prestataire.setNumero(txtTelephone.getValue());
				if(prestataires==null)
				  BeanLocator.defaultLookup(ProcedurederogatoireSession.class).savePrestataire(prestataire);
				else
				  BeanLocator.defaultLookup(ProcedurederogatoireSession.class).updatePrestataire(prestataire);
//				dossier.setDosDatePublication(dtdatepublication.getValue());
//				dossier.setDosCommentairePublication(txtcommentaires.getValue());
//				if(!txtVersionElectronique.getValue().equals(""))
//				dossier.setTexte_html(txtVersionElectronique.getValue());
//				BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).update(dossier);
//				
//				document.setAppel(appel);
//				if(!txtVersionElectronique.getValue().equals(""))
//				document.setNomFichier(txtVersionElectronique.getValue());
//				document.setTypeDocument(UIConstants.PARAM_TYPEDOCUMENTS_PUBDAO);
//				document.setLibelle(Labels.getLabel("kermel.plansdepassation.proceduresmarches.documents.mepdao"));
//				document.setDossier(dossier);
//				document.setDate(new Date());
//				document.setHeure(Calendar.getInstance().getTime());
//				BeanLocator.defaultLookup(DocumentsSession.class).save(document);
				
				session.setAttribute("libelle", "factureproforma");
				loadApplicationState("entente_directe");
				
			}
		}
		
		public void onClick$btnChoixFichier() {
			//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
				if (ToolKermel.isWindows())
					nomFichier = FileLoader.uploadPieceDossierAO(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
				else
					nomFichier = FileLoader.uploadPieceDossierAO(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

				txtVersionElectronique.setValue(nomFichier);
			}
		
		public org.zkoss.util.media.AMedia fetchFile(File file) {

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(file, null, null);
				return mymedia;
			} catch (Exception e) {

				e.printStackTrace();
				return null;
			}

		}
		public void onClick$menuFermer() {
			step0.setVisible(true);
			step1.setVisible(false);
		}
		public void onClick$image() {
			step0.setVisible(false);
			step1.setVisible(true);
			String filepath = cheminDossier + prestataire.getFacture();
			File f = new File(filepath.replaceAll("\\\\", "/"));

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(f, null, null);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (mymedia != null)
				idIframe.setContent(mymedia);
			else
				idIframe.setSrc("");

			idIframe.setHeight("600px");
			idIframe.setWidth("100%");
		} 
		
///////////Pays///////// 
	public void onSelect$lstFournisseur(){
		fournisseur= (SygFournisseur) lstFournisseur.getSelectedItem().getValue();
		pays=fournisseur.getPays();
	bdFournisseur.setValue(fournisseur.getNom());
	txtTelephone.setValue(fournisseur.getTel());
	txtFax.setValue(fournisseur.getFax());
	txtmail.setValue(fournisseur.getEmail());
	bdFournisseur.close();
	
	}
	
	public class PaysRenderer implements ListitemRenderer{
	
	
	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygFournisseur fournisseur = (SygFournisseur) data;
		item.setValue(fournisseur);
		
		Listcell cellNom = new Listcell(fournisseur.getNom());
		cellNom.setParent(item);
		
		
		}
	}
	public void onFocus$txtRechercherFournisseur(){
	if(txtRechercherFournisseur.getValue().equalsIgnoreCase(Labels.getLabel("kermel.paiement.consultation.fournisseur"))){
		txtRechercherFournisseur.setValue("");
	}		 
	}
	
	public void  onOK$btnRechercherFournisseurs(){
		onClick$btnRechercherFournisseurs();
	}
	public void  onOK$txtRechercherFournisseur(){
		onClick$btnRechercherFournisseurs();
	}
	public void  onClick$btnRechercherFournisseurs(){
	if(txtRechercherFournisseur.getValue().equalsIgnoreCase(Labels.getLabel("kermel.paiement.consultation.fournisseur")) || txtRechercherFournisseur.getValue().equals("")){
		nomfournisseur = null;
	page=null;
	}else{
		nomfournisseur = txtRechercherFournisseur.getValue();
	page="0";
	}
	Events.postEvent(ApplicationEvents.ON_FOURNISSEUR, this, page);
	}
	
	 public void onClick$btnNouveau(){
			final String uri = "/referentiel/fournisseur/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FournisseurFormController.WINDOW_PARAM_FICHIER, "RETRAIT");
			data.put(FournisseurFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
 		showPopupWindow(uri, data, display);
	 }
}