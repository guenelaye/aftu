package sn.ssi.kermel.web.processus.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.SimpleGroupsModel;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SysProfil;
import sn.ssi.kermel.be.security.ProfilsSession;
import sn.ssi.kermel.be.workflow.entity.SysArrowperm;
import sn.ssi.kermel.be.workflow.entity.SysProfilarrowperm;
import sn.ssi.kermel.be.workflow.entity.SysRole;
import sn.ssi.kermel.be.workflow.entity.SysState;
import sn.ssi.kermel.be.workflow.entity.SysStateperm;
import sn.ssi.kermel.be.workflow.manager.ejb.WorkflowManagerSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings( { "serial", "unchecked" })
public class ProcessFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstProcessus;
	private Grid grdStates;
	private Textbox searchBox;
	private Button btOK;
	public static final String CURRENT_PROCESSUS = "CURRENT_Processus";
	public static final String WINDOW_PARAM_PROFIL_CODE = "PROFIL_CODE";
	String profilCode;

	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);

		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lstProcessus.setItemRenderer(this);
		grdStates.setRowRenderer(new RowRendererGrid());
		searchBox.addEventListener(Events.ON_CHANGING, this);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	public void onCreate(final CreateEvent createEvent) {
		final Map windowParams = createEvent.getArg();
		profilCode = (String) windowParams.get(WINDOW_PARAM_PROFIL_CODE);

	}

	private boolean inList(final String stateCode, final List removables) {
		for (int i = 0; i < removables.size(); i++)
			if (removables.get(i).toString().equals(stateCode))
				return true;

		return false;
	}

	private List<SysState> removeDoubles(final List<SysState> states) {
		final ArrayList removables = new ArrayList();
		final ArrayList<SysState> finalStates = new ArrayList();

		for (int i = 0; i < states.size(); i++)
			if (!inList(states.get(i).getCode(), removables)) {
				finalStates.add(states.get(i));
				removables.add(states.get(i).getCode());
			}
		return finalStates;
	}

	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			final List<SysRole> processus = BeanLocator.defaultLookup(
					WorkflowManagerSession.class).findRoles(0, 1000);
			final SimpleListModel listModel = new SimpleListModel(processus);
			lstProcessus.setModel(listModel);
		} else if (event.getName().equalsIgnoreCase(Events.ON_CLICK)) {
			final Listitem listitem = (Listitem) event.getTarget();
			final String roleCode = listitem.getValue().toString();
			List<SysState> states = BeanLocator.defaultLookup(
					WorkflowManagerSession.class).findRoleStates(
					listitem.getValue().toString());
			states = removeDoubles(states);
			final Object[][] datas = new Object[states.size()][];
			final Object[] heads = new Object[states.size()];

			for (int i = 0; i < states.size(); i++) {
				final String stateCode = states.get(i).getCode();
				final List<SysArrowperm> apms = BeanLocator.defaultLookup(
						WorkflowManagerSession.class).findStateArrows(roleCode,
						stateCode);
				datas[i] = new Object[4 + apms.size()];
				final ProcessRow pr = new ProcessRow("0", stateCode, listitem
						.getValue().toString(), profilCode, stateCode);
				heads[i] = pr;
				final ProcessRow pr1 = new ProcessRow("1", "canview", listitem
						.getValue().toString(), profilCode, stateCode);
				datas[i][0] = pr1;
				final ProcessRow pr2 = new ProcessRow("2", "canedit", listitem
						.getValue().toString(), profilCode, stateCode);
				datas[i][1] = pr2;
				final ProcessRow pr3 = new ProcessRow("3", "candelete",
						listitem.getValue().toString(), profilCode, stateCode);
				datas[i][2] = pr3;
				final ProcessRow pr4 = new ProcessRow("4", "canprint", listitem
						.getValue().toString(), profilCode, stateCode);
				datas[i][3] = pr4;
				for (int j = 0; j < apms.size(); j++) {
					final ProcessRow pr5 = new ProcessRow("5", String
							.valueOf(apms.get(j).getId()), listitem.getValue()
							.toString(), profilCode, stateCode);
					datas[i][j + 4] = pr5;
				}
			}

			final SimpleGroupsModel gridModel = new SimpleGroupsModel(datas,
					heads);
			grdStates.setModel(gridModel);
			if (states.size() > 0) {
				btOK.setVisible(true);
			} else {
				btOK.setVisible(false);
			}

		} else if (event.getName().equalsIgnoreCase(Events.ON_CHANGING)) {
			final List<SysRole> processus = BeanLocator.defaultLookup(
					WorkflowManagerSession.class).findRoles(
					searchBox.getValue());
			final SimpleListModel listModel = new SimpleListModel(processus);
			lstProcessus.setModel(listModel);
			/*
			 * for (int i=0;i<lstProcessus.getItemCount();i++) {
			 * 
			 * 
			 * if
			 * (((Listcell)lstProcessus.getItemAtIndex(i).getChildren().get(1)
			 * ).getLabel().startsWith(searchBox.getValue()))
			 * lstProcessus.getItemAtIndex(i).setVisible(true); else
			 * lstProcessus.removeItemAt(i); }
			 */

		}

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		final SysRole processus = (SysRole) data;
		item.setValue(processus.getCode());
		item.addEventListener(Events.ON_CLICK, this);
		final Listcell cellImage = new Listcell();
		cellImage
				.setImage("/img/if_calendar.png");
		cellImage.setParent(item);

		final Listcell cellLibelle = new Listcell(processus.getDesc());
		cellLibelle.setParent(item);
	}

	public void onOK() {

		final SysProfil profil = BeanLocator
				.defaultLookup(ProfilsSession.class).findByCode(profilCode);

		final Listitem listitem = lstProcessus.getSelectedItem();
		final String roleCode = listitem.getValue().toString();

		final SysRole role = BeanLocator.defaultLookup(
				WorkflowManagerSession.class).findByCode(roleCode);

		for (int i = 0; i < grdStates.getGroupsModel().getGroupCount(); i++) {
			final String stateCode = ((ProcessRow) grdStates.getGroupsModel()
					.getGroup(i)).getStateCode();
			final SysState state = BeanLocator.defaultLookup(
					WorkflowManagerSession.class).findState(stateCode);

			SysStateperm spm;
			if (BeanLocator.defaultLookup(WorkflowManagerSession.class)
					.findStateperm(profilCode, roleCode, stateCode) != null) {
				spm = BeanLocator.defaultLookup(WorkflowManagerSession.class)
						.findStateperm(profilCode, roleCode, stateCode);
			} else {
				spm = new SysStateperm();
			}

			spm.setSysRole(role);
			spm.setSysProfil(profil);
			spm.setSysState(state);
			if (((Checkbox) grdStates.getFellow(stateCode + "_canview"))
					.isChecked()) {
				spm.setCanview((short) 1);
			} else {
				spm.setCanview((short) 0);
			}

			if (((Checkbox) grdStates.getFellow(stateCode + "_canedit"))
					.isChecked()) {
				spm.setCanedit((short) 1);
			} else {
				spm.setCanedit((short) 0);
			}

			if (((Checkbox) grdStates.getFellow(stateCode + "_candelete"))
					.isChecked()) {
				spm.setCandelete((short) 1);
			} else {
				spm.setCandelete((short) 0);
			}

			if (((Checkbox) grdStates.getFellow(stateCode + "_canprint"))
					.isChecked()) {
				spm.setCanprint((short) 1);
			} else {
				spm.setCanprint((short) 0);
			}

			BeanLocator.defaultLookup(WorkflowManagerSession.class)
					.saveStateperm(spm);
			final List<SysArrowperm> apms = BeanLocator.defaultLookup(
					WorkflowManagerSession.class).findStateArrows(roleCode,
					stateCode);
			for (int j = 0; j < apms.size(); j++) {
				final SysArrowperm apm = apms.get(j);
				SysProfilarrowperm papm;
				papm = BeanLocator.defaultLookup(WorkflowManagerSession.class)
						.findProfilarrowperm(apm.getId(), profilCode);
				if (papm == null) {
					papm = new SysProfilarrowperm();
				}

				papm.setSysArrowperm(apm);
				if (((Checkbox) grdStates.getFellow(apm.getId() + "_apm"))
						.isChecked()) {
					papm.setAllow((short) 1);
				} else {
					papm.setAllow((short) 0);
				}

				papm.setSysProfil(profil);
				BeanLocator.defaultLookup(WorkflowManagerSession.class)
						.saveProfilArrowperm(papm);
			}
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
	}
}