package sn.ssi.kermel.web.processus.controllers;

public class ProcessRow {
	String type;
	String code;
	String roleCode;
	String profilCode;
	String stateCode;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getProfilCode() {
		return profilCode;
	}
	public void setProfilCode(String profilCode) {
		this.profilCode = profilCode;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public ProcessRow(String type, String code, String roleCode,
			String profilCode, String stateCode) {
		super();
		this.type = type;
		this.code = code;
		this.roleCode = roleCode;
		this.profilCode = profilCode;
		this.stateCode = stateCode;
	}
	
}
