package sn.ssi.kermel.web.processus.controllers;

import java.util.HashMap;

import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.workflow.entity.SysArrowperm;
import sn.ssi.kermel.be.workflow.entity.SysProfilarrowperm;
import sn.ssi.kermel.be.workflow.entity.SysState;
import sn.ssi.kermel.be.workflow.manager.ejb.WorkflowManagerSession;

public class RowRendererGrid implements RowRenderer, EventListener {

	@Override
	public void onEvent(final Event event) throws Exception {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("unchecked")
	@Override
	public void render(final Row row, final Object data, int index) throws Exception {

		final ProcessRow pr = (ProcessRow) data;
		final String x = pr.getType();
		final String stateCode = pr.getStateCode();
		pr.getRoleCode();
		final String profilCode = pr.getProfilCode();
		final String code = pr.getCode();

		final SysState state = BeanLocator.defaultLookup(
				WorkflowManagerSession.class).findState(stateCode);

		final Checkbox c = new Checkbox();

		final HashMap m = new HashMap();
		m.put("state", state);
		m.put("perm", code);

		c.setId(stateCode + "_" + code);

		if (x.equals("1")) {
			c.setLabel("Voir");
			c.setParent(row);
			c.setChecked(BeanLocator
					.defaultLookup(WorkflowManagerSession.class).canSomething(
							profilCode, state.getCode(), code));
		} else if (x.equals("2")) {
			c.setLabel("Modifier");
			c.setParent(row);
			c.setChecked(BeanLocator
					.defaultLookup(WorkflowManagerSession.class).canSomething(
							profilCode, state.getCode(), code));
		} else if (x.equals("3")) {
			c.setLabel("Supprimer");
			c.setParent(row);
			c.setChecked(BeanLocator
					.defaultLookup(WorkflowManagerSession.class).canSomething(
							profilCode, state.getCode(), code));
		} else if (x.equals("4")) {
			c.setLabel("Imprimer");
			c.setParent(row);
			c.setChecked(BeanLocator
					.defaultLookup(WorkflowManagerSession.class).canSomething(
							profilCode, state.getCode(), code));
		} else if (x.equals("5")) {
			final SysArrowperm apm = BeanLocator.defaultLookup(
					WorkflowManagerSession.class).findArrowperm(
					Integer.parseInt(code));
			if (apm.getSysArrow().getLibelle() != null) {
				c.setLabel(apm.getSysArrow().getLibelle());
			} else {
				c.setLabel(apm.getSysArrow().getCode());
			}
			c.setParent(row);
			boolean allow = false;
			final SysProfilarrowperm papm = BeanLocator.defaultLookup(
					WorkflowManagerSession.class).findProfilarrowperm(
					apm.getId(), profilCode);
			if (papm != null)
				if (papm.getAllow() == 1) {
					allow = true;
				}
			c.setChecked(allow);
			c.setId(apm.getId() + "_apm");
			row.setValue(apm.getId());
		} else {
			final Label l = new Label();
			if (state.getLibelle() != null) {
				l.setValue(state.getLibelle());
			} else {
				l.setValue(state.getCode());
			}
			m.put("isstate", true);
			l.setParent(row);
		}

	}
}