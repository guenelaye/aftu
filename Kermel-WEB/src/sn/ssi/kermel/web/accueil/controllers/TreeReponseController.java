package sn.ssi.kermel.web.accueil.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.West;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.session.QuestionSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class TreeReponseController extends AbstractWindow implements
AfterCompose, IdSpace {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Textbox txtSearch;
    private Include content;
    // private Combobox listCategory;
    private Tree tree;
    private final Treechildren child = new Treechildren();
    private final Session session = getHttpSession();
   
    private String openWest;
    private West wp;
    private String libelle;
    private SygDossiers dossier;
  

    @Override
    public void afterCompose() {
	Components.wireFellows(this, this);
	Components.addForwards(this, this);
	wp.setOpen(true);
	/*getHttpSession().setAttribute("mystate",
		Executions.getCurrent().getAttribute("mystate"));
	region = (SiapDecoupage1) Executions.getCurrent().getAttribute(UIConstants.REGION);
	openWest = (String) Executions.getCurrent().getAttribute("openWest");
	wp.setOpen(true);
	campagne = BeanLocator.defaultLookup(CampagneAgricolSession.class).findByCode(getCampagne());
	pays = (SiapPays) getHttpSession().getAttribute(UIConstants.PAYS);
	*/
	txtSearch.setValue("Recherche");
	initTree();
	/*if (region != null) {
	    wp.setOpen(true);
	}*/

    }

    private void initTree() {
	child.setParent(tree);
	treeProcedureBuilder();
	// chargerRegion();
	// chargerAll();
	
	dossier=BeanLocator.defaultLookup(
			DossiersAppelsOffresSession.class).findAll(0, -1,null, null).get(0);
	
	 session.setAttribute("dossier", dossier);
	content.setSrc("/reponse/fiche.zul");
	content.setHeight("600px");
    }

    public void onSelect$tree() {
	tree.isInvalidated();
	Object selobj = tree.getSelectedItem().getValue();

	session.setAttribute("selObj", selobj);
	content.invalidate();
	// content.setSrc("/procedure/list.zul");
	// chargerRegion();
	if (selobj instanceof SygDossiers) {

	//debut
		
		 List<String> status = new ArrayList<String>();
		 dossier = (SygDossiers) selobj;
		    // session.setAttribute("poste",poste.getId());
		    session.setAttribute("dossier", dossier);
		
		     content.setSrc("/reponse/fiche.zul");
		    // content.setProgressing(true);
		    content.setMode("defer");
		
   // fin de la fonction
		
	}
    }

    public void onSelect$listCategory() {
	// txtSearch.setDisabled(true);
	//
	// txtSearch.setDisabled(false);
	// txtSearch.setValue("");
	//
	// List<String> status = new ArrayList<String>();
	// status.add(UIConstants.PUBLISHED);
	//
	// if(listCategory.getSelectedItem().getValue().toString().equalsIgnoreCase(UIConstants.PROCEDURE)){
	//
	// if(session.getAttribute("category_mode") != null &&
	// !session.getAttribute("category_mode").equals(UIConstants.PROCESS)){
	// removeTreeChildren();
	// //treeProcedureBuilder();
	// chargerRegion();
	// }
	//
	// session.setAttribute("category_mode", UIConstants.PROCEDURE);
	//
	// } else
	// if(listCategory.getSelectedItem().getValue().toString().equalsIgnoreCase(UIConstants.ETAPE)){
	// session.setAttribute("category_mode", UIConstants.ETAPE);
	//
	// treeEtapeBuilder();
	//
	// } else
	// if(listCategory.getSelectedItem().getValue().toString().equalsIgnoreCase(UIConstants.TACHE)){
	// session.setAttribute("category_mode", UIConstants.TACHE);
	//
	// treeTacheBuilder();
	//
	// } else
	// if(listCategory.getSelectedItem().getValue().toString().equalsIgnoreCase(UIConstants.OPERATION)){
	// session.setAttribute("category_mode", UIConstants.OPERATION);
	//
	// treeOperationBuilder();
	//
	// }

    }

    // public void onOK$txtSearch(){
    // search();
    // }

    public void onClick$search_img() {
	search();
    }

    public void search() {
	//
	// List<String> status = new ArrayList<String>();
	// status.add(UIConstants.PUBLISHED);
	//
	// removeTreeChildren();
	//
	// if(listCategory.getSelectedItem().getValue().toString().equalsIgnoreCase(UIConstants.PROCEDURE)){
	// session.setAttribute("category_mode", UIConstants.PROCEDURE);
	//
	// //treeProcedureBuilder();
	// chargerRegion();
	//
	// } else
	// if(listCategory.getSelectedItem().getValue().toString().equalsIgnoreCase(UIConstants.ETAPE)){
	// session.setAttribute("category_mode", UIConstants.ETAPE);
	//
	// treeEtapeBuilder();
	//
	// } else
	// if(listCategory.getSelectedItem().getValue().toString().equalsIgnoreCase(UIConstants.TACHE)){
	// session.setAttribute("category_mode", UIConstants.TACHE);
	//
	// treeTacheBuilder();
	//
	// } else
	// if(listCategory.getSelectedItem().getValue().toString().equalsIgnoreCase(UIConstants.OPERATION)){
	// session.setAttribute("category_mode", UIConstants.OPERATION);
	//
	// treeOperationBuilder();
	//
	// }

    }

    public void treeBuilder() {
	// String key = null;
	//
	// if(!"".equals(txtSearch.getValue()) &&
	// !txtSearch.getValue().equals("Recherche"))
	// key = txtSearch.getValue();
	//
	// List<String> status = new ArrayList<String>();
	// status.add(UIConstants.PUBLISHED);
	//
	// List<OhProcess> listProcess =
	// BeanLocator.defaultLookup(ProcessSession.class).findProcessesByStatut(key,
	// status);
	//
	// List<OhProcedure> listProcedure =
	// BeanLocator.defaultLookup(ProcedureSession.class).findAll(null,
	// status);
	//
	// if(listProcedure.size() > 0){
	//
	// for (OhProcedure ohProcedure : listProcedure) {
	// Treeitem procedureItem = new Treeitem();
	//
	// procedureItem.setLabel(ohProcedure.getProLibelle());
	// procedureItem.setTooltiptext(ohProcedure.getProLibelle());
	//
	// procedureItem.setImage("g");
	// procedureItem.setValue(ohProcedure);
	// procedureItem.setOpen(false);
	//
	// procedureItem.setParent(child);
	//
	// List <OhEtape> listEtapes =
	// BeanLocator.defaultLookup(ProcedureSession.class).findEtapesByProcedure(ohProcedure.getProID(),
	// null, status);
	//
	// if(listEtapes.size() > 0){
	//
	// Treechildren procedureTreechildren = new Treechildren();
	// for (OhEtape ohEtape : listEtapes) {
	// Treeitem etapeItem = new Treeitem();
	//
	// etapeItem.setLabel(ohEtape.getEtaLibelle());
	// etapeItem.setTooltiptext(ohEtape.getEtaLibelle());
	//
	//
	// etapeItem.setValue(ohEtape);
	// etapeItem.setOpen(false);
	//
	// etapeItem.setParent(procedureTreechildren);
	//
	// List <OhTache> listTaches =
	// BeanLocator.defaultLookup(EtapeSession.class).findTachesByEtape(ohEtape.getEtaID(),
	// null, status);
	// if(listTaches.size() > 0){
	// Treechildren etapeTreechildren = new Treechildren();
	// for (OhTache ohTache : listTaches) {
	// Treeitem tacheItem = new Treeitem();
	//
	// tacheItem.setLabel(ohTache.getTacLibelle());
	// tacheItem.setTooltiptext(ohTache.getTacLibelle());
	//
	// tacheItem.setImage("g");
	// tacheItem.setValue(ohTache);
	// tacheItem.setOpen(false);
	//
	// tacheItem.setParent(etapeTreechildren);
	//
	// List <OhOperation> listOperations =
	// BeanLocator.defaultLookup(TacheSession.class).findOperationsByTache(ohTache.getTacID(),
	// null, status, null);
	// if(listOperations.size() > 0){
	// Treechildren tacheTreechildren = new Treechildren();
	//
	// for (OhOperation ohOperation : listOperations) {
	// Treeitem operationItem = new Treeitem();
	//
	// operationItem.setLabel(ohOperation.getOpLibelle());
	// operationItem.setTooltiptext(ohOperation.getOpLibelle());
	//
	// operationItem.setImage("g");
	// operationItem.setValue(ohOperation);
	// operationItem.setOpen(false);
	//
	// operationItem.setParent(tacheTreechildren);
	// }
	// tacheTreechildren.setParent(tacheItem);
	// }
	// }
	// etapeTreechildren.setParent(etapeItem);
	// }
	// }
	// procedureTreechildren.setParent(procedureItem);
	// }
	// }
	// }
    }

    public void treeProcedureBuilder() {

	String key = null;
	
	List<SygDossiers> listProcedure = BeanLocator.defaultLookup(
		DossiersAppelsOffresSession.class).findAll(0, -1,null, null);
	for (SygDossiers ohProcedure : listProcedure) {

	    Treeitem procedureItem = new Treeitem();

	    procedureItem.setLabel(ohProcedure.getAppel().getApoobjet()+" ("+ BeanLocator.defaultLookup(QuestionSession.class).find(ohProcedure).size()+")");
	    procedureItem.setTooltiptext(ohProcedure.getAppel().getApoobjet());

	    procedureItem.setImage("/images/puce.png");
	    procedureItem.setValue(ohProcedure);
	    procedureItem.setOpen(false);

	    procedureItem.setParent(child);

	    List<SygDossiers> listEtapes = new ArrayList<SygDossiers>();
	    	
	    	/*BeanLocator.defaultLookup(
		    Decoupage2Session.class).findbyRegion(0, -1,
			    ohProcedure.getId());  */

	    if (listEtapes.size() > 0) {
		Treechildren procedureTreechildren = new Treechildren();
		for (SygDossiers ohEtape : listEtapes) {
		    Treeitem etapeItem = new Treeitem();

		    etapeItem.setLabel(ohEtape.getDosID().toString());
		    etapeItem.setTooltiptext(ohEtape.getDosID().toString());

		    etapeItem.setImage("/images/arrow.png");

		    etapeItem.setValue(ohEtape);
		    etapeItem.setOpen(false);

		    etapeItem.setParent(procedureTreechildren);
		    

		    List<SygDossiers> listTaches = new ArrayList<SygDossiers>();
		    // BeanLocator.defaultLookup(PostePluvioSession.class).findbyDepartement(ohEtape.getId());
		    // System.out.println("==============================ok"+listTaches.size());

		    if (listTaches.size() > 0) {
			Treechildren etapeTreechildren = new Treechildren();
			for (SygDossiers ohTache : listTaches) {
			    Treeitem tacheItem = new Treeitem();

			    tacheItem.setLabel(ohTache.getDosAdresseRetrait());
			    tacheItem.setTooltiptext(ohTache.getDosAdresseRetrait());

			    // tacheItem.setImage("g");
			    tacheItem.setValue(ohTache);
			    tacheItem.setOpen(false);

			    tacheItem.setParent(etapeTreechildren);

			    List<SygDossiers> listOperations = new ArrayList<SygDossiers>();
			    // BeanLocator.defaultLookup(PostePluvioSession.class).findAll();
			    if (listOperations.size() > 0) {
				Treechildren tacheTreechildren = new Treechildren();

				/*
				 * for (SiapPostePluvio ohOperation :
				 * listOperations) { Treeitem operationItem =
				 * new Treeitem();
				 * 
				 * 
				 * operationItem.setLabel(ohOperation.getLibelle(
				 * ));operationItem.setTooltiptext(ohOperation.
				 * getLibelle());
				 * 
				 * // operationItem.setImage("g");
				 * operationItem.setValue(ohOperation);
				 * 
				 * operationItem.setParent(tacheTreechildren); }
				 */

				// tacheTreechildren.setParent(tacheItem);
			    }
			}
			etapeTreechildren.setParent(etapeItem);
		    }
		}
		procedureTreechildren.setParent(procedureItem);
	    }
	}
    }

    public void treeEtapeBuilder() {
	// String key = null;
	//
	// if(!"".equals(txtSearch.getValue()) &&
	// !txtSearch.getValue().equals("Recherche"))
	// key = txtSearch.getValue();
	//
	// List<String> status = new ArrayList<String>();
	// status.add(UIConstants.PUBLISHED);
	//
	// removeTreeChildren();
	//
	// List <OhEtape> listEtapes =
	// BeanLocator.defaultLookup(EtapeSession.class).findAll(key, status);
	//
	// if(listEtapes.size() > 0){
	//
	// for (OhEtape ohEtape : listEtapes) {
	// Treeitem etapeItem = new Treeitem();
	//
	// etapeItem.setLabel(ohEtape.getEtaLibelle());
	// etapeItem.setTooltiptext(ohEtape.getEtaLibelle());
	//
	// etapeItem.setImage("g");
	//
	// etapeItem.setValue(ohEtape);
	// etapeItem.setOpen(false);
	//
	// etapeItem.setParent(child);
	//
	// List <OhTache> listTaches =
	// BeanLocator.defaultLookup(EtapeSession.class).findTachesByEtape(ohEtape.getEtaID(),
	// null, status);
	// if(listTaches.size() > 0){
	// Treechildren etapeTreechildren = new Treechildren();
	// for (OhTache ohTache : listTaches) {
	// Treeitem tacheItem = new Treeitem();
	//
	// tacheItem.setLabel(ohTache.getTacLibelle());
	// tacheItem.setTooltiptext(ohTache.getTacLibelle());
	//
	// tacheItem.setImage("g");
	// tacheItem.setValue(ohTache);
	// tacheItem.setOpen(false);
	//
	// tacheItem.setParent(etapeTreechildren);
	//
	// List <OhOperation> listOperations =
	// BeanLocator.defaultLookup(TacheSession.class).findOperationsByTache(ohTache.getTacID(),
	// null, status, null);
	// if(listOperations.size() > 0){
	// Treechildren tacheTreechildren = new Treechildren();
	//
	// for (OhOperation ohOperation : listOperations) {
	// Treeitem operationItem = new Treeitem();
	//
	// operationItem.setLabel(ohOperation.getOpLibelle());
	// operationItem.setTooltiptext(ohOperation.getOpLibelle());
	//
	// operationItem.setImage("g");
	// operationItem.setValue(ohOperation);
	//
	// operationItem.setParent(tacheTreechildren);
	// }
	// tacheTreechildren.setParent(tacheItem);
	// }
	// }
	// etapeTreechildren.setParent(etapeItem);
	// }
	// }
	// }
    }

    public void treeTacheBuilder() {
	// String key = null;
	//
	// if(!"".equals(txtSearch.getValue()) &&
	// !txtSearch.getValue().equals("Recherche"))
	// key = txtSearch.getValue();
	//
	// List<String> status = new ArrayList<String>();
	// status.add(UIConstants.PUBLISHED);
	//
	// removeTreeChildren();
	//
	// List <OhTache> listTaches =
	// BeanLocator.defaultLookup(TacheSession.class).findAll(key, status);
	// if(listTaches.size() > 0){
	//
	// for (OhTache ohTache : listTaches) {
	// Treeitem tacheItem = new Treeitem();
	//
	// tacheItem.setLabel(ohTache.getTacLibelle());
	// tacheItem.setTooltiptext(ohTache.getTacLibelle());
	//
	// tacheItem.setImage("g");
	// tacheItem.setValue(ohTache);
	// tacheItem.setOpen(false);
	//
	// tacheItem.setParent(child);
	//
	// List <OhOperation> listOperations =
	// BeanLocator.defaultLookup(TacheSession.class).findOperationsByTache(ohTache.getTacID(),
	// null, status, null);
	// if(listOperations.size() > 0){
	// Treechildren tacheTreechildren = new Treechildren();
	//
	// for (OhOperation ohOperation : listOperations) {
	// Treeitem operationItem = new Treeitem();
	//
	// operationItem.setLabel(ohOperation.getOpLibelle());
	// operationItem.setTooltiptext(ohOperation.getOpLibelle());
	//
	// operationItem.setImage("g");
	// operationItem.setValue(ohOperation);
	//
	// operationItem.setParent(tacheTreechildren);
	// }
	// tacheTreechildren.setParent(tacheItem);
	// }
	// }
	// }
    }

    public void treeOperationBuilder() {
	// String key = null;
	//
	// if(!"".equals(txtSearch.getValue()) &&
	// !txtSearch.getValue().equals("Recherche"))
	// key = txtSearch.getValue();
	//
	// List<String> status = new ArrayList<String>();
	// status.add(UIConstants.PUBLISHED);
	//
	// removeTreeChildren();
	//
	// List <OhOperation> listOperations =
	// BeanLocator.defaultLookup(OperationSession.class).findAll(key,
	// status, null);
	// if(listOperations.size() > 0){
	//
	// for (OhOperation ohOperation : listOperations) {
	// Treeitem operationItem = new Treeitem();
	//
	// operationItem.setLabel(ohOperation.getOpLibelle());
	// operationItem.setTooltiptext(ohOperation.getOpLibelle());
	//
	//
	// operationItem.setValue(ohOperation);
	//
	//
	// operationItem.setParent(child);
	// }
	// }
    }

    public void removeTreeChildren() {
	while (child.getItemCount() > 0) {
	    child.removeChild(child.getFirstChild());
	}
    }

    public void chargerRegion() {
	
    }

    public void chargerDepartement(Integer regId, int index) {

	

    }

    public void chargerTache(Long etaID, List<String> status, String index) {
	// try {
	// List <OhTache> listTaches =
	// BeanLocator.defaultLookup(EtapeSession.class).findTachesByEtape(etaID,
	// null, status);
	// if(listTaches.size() > 0){
	// Treechildren etapeTreechildren = new Treechildren();
	// for (OhTache ohTache : listTaches) {
	// Treeitem tacheItem = new Treeitem();
	// tacheItem.setId("t@ch_"+ohTache.getTacID());
	// tacheItem.setLabel(ohTache.getTacLibelle());
	// tacheItem.setTooltiptext(ohTache.getTacLibelle());
	//
	//
	// tacheItem.setValue(ohTache);
	// tacheItem.setOpen(false);
	//
	// tacheItem.setParent(etapeTreechildren);
	// }
	// etapeTreechildren.setParent((Component) child.getFellowIfAny(index));
	// }
	// } catch (Exception e) {
	// // afterCompose();
	// }
	//
    }

    public void chargerOperation(Long tacID, List<String> status, String idc) {
	// try {
	// List <OhOperation> listOperations =
	// BeanLocator.defaultLookup(TacheSession.class).findOperationsByTache(tacID,
	// null, status, null);
	// if(listOperations.size() > 0){
	// Treechildren tacheTreechildren = new Treechildren();
	//
	// for (OhOperation ohOperation : listOperations) {
	// Treeitem operationItem = new Treeitem();
	//
	// operationItem.setLabel(ohOperation.getOpLibelle());
	// operationItem.setTooltiptext(ohOperation.getOpLibelle());
	//
	//
	// operationItem.setValue(ohOperation);
	//
	// operationItem.setParent(tacheTreechildren);
	// }
	// tacheTreechildren.setParent((Component)child.getFellowIfAny(idc));
	// }
	// } catch (Exception e) {
	// //afterCompose();
	// }
    }

    public void onOpen$tree() {

	System.out.println(tree.getSelectedItem().getId());
    }

    public void onFocus$txtSearch() {
	if (txtSearch.getValue().equalsIgnoreCase("Recherche")) {
	    txtSearch.setValue(null);
	}
    }

    public void onBlur$txtSearch() {
	if (txtSearch.getValue().equalsIgnoreCase("")
		|| (txtSearch.getValue().equalsIgnoreCase(null))) {
	    txtSearch.setValue("Recherche");
	}
    }

    public void onOK$txtSearch() {
	if (txtSearch.getValue() != null
		&& !txtSearch.getValue().trim().equals("")) {
	    libelle = txtSearch.getValue();
	} else {
	    libelle = null;
	}
	chargerSearch();
    }

    public void chargerSearch() {
	removeTreeChildren();
	// chargerRegion();
	treeProcedureBuilder();

    }

    public void chargerAll() {
	
    }

    // public void

   

    

}