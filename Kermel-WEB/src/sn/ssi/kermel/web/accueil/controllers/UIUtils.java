package sn.ssi.kermel.web.accueil.controllers;

import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Fileupload;
import org.zkoss.zul.Messagebox;

public class UIUtils {

	public static void uploadFile(Component aComponent) {

		aComponent.getDesktop().setAttribute("org.zkoss.zul.Fileupload.target", aComponent);

		try {
			Fileupload.get(true);
		} catch (Exception e) {
			Messagebox.show("Uploading file failed");
		}
	}

	public static String uploadFile(String dgrCode, String pijcode, String cheminDossierPieces) {
		String fichierPieceJointe = null;
		// aComponent.getDesktop().setAttribute("org.zkoss.zul.Fileupload.target",
		// aComponent);
		fichierPieceJointe = cheminDossierPieces + dgrCode + "_" + pijcode + "_";
		try {
			Fileupload.get(true);
		} catch (Exception e) {
			Messagebox.show("Uploading file failed");
		}
		return fichierPieceJointe;
	}

	public static Object uploadFileXml(Component aComponent) {

		aComponent.getDesktop().setAttribute("org.zkoss.zul.Fileupload.target", aComponent);

		try {
			final Object media = Fileupload.get(true);
			return media;
		} catch (Exception e) {
			Messagebox.show("Uploading file failed");
			return null;
		}
	}
}
