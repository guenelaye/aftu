package sn.ssi.kermel.web.accueil.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Group;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.PkQuestion;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.session.QuestionSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class FicheReponseContoller extends AbstractWindow
implements AfterCompose, EventListener {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String mode, libDist, libEnqueteur, libMarche;
    int idProjet;
    private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
    private final int byPage1 = 5;
    Grid fmnsistGrid;
    Paging pgPagination, pgDist, pgEnqueteur, pgMarche;
    Listbox lstDist, listEnqueteur, listMarche;
    SygDossiers fiche;
    Integer idFiche, distID, enqueteurID, marcheID;
    Textbox txtnumero, txtstatu, txtDrs, txtDist, anneemoi, txtRechercherDist,
    txtRechercherEnqueteur, txtObservation;
    Label nbre, lblMarche, lblEnqueteur, lblDate, lblRegion, lblDepartement,
    lblCampagne, lblSup1, lblSup2;
    Datebox datecree;
    Intbox nbretotalfs, nbretotalconsmois;
    Bandbox bdservice, bdDist, bdFmnsist, bdEnqueteur, bdMarche;
    Button btnGenerer, btnRechercherDist, btnRechercherMarcher,
    btnRechercherEnqueteur;
    Menuitem menuPreviousStep1, Importer1,generer1;
    // Prodistrict district;
    // Proregionmedical regMed;
    private Div step0, step1, step;
    private Iframe iframePrint;
    private final boolean estGenere = false;
    private final String datejour = UtilVue.getInstance().formateLaDate(
	    new Date());
   // private final int annee = Integer.parseInt(ToolGrh.getAnneeCourante());
    private final String mois = datejour.substring(3, 5);
    Session session = getHttpSession();
  
    private Group categorie;
    private String mystate;
    private Label lblDossier,ref,lblnombre,lblreponse,lblrestant;

    @Override
    public void afterCompose() {
	Components.wireFellows(this, this);
	Components.addForwards(this, this);
	
	addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
	addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
	addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);  
	//String niveau =(String) session.getAttribute("niveau");
		 
	fiche = (SygDossiers) session.getAttribute("dossier");
	
	lblDossier.setValue(fiche.getAppel().getApoobjet());
	ref.setValue(fiche.getAppel().getAporeference());
	lblnombre.setValue("0");
	int nb=BeanLocator.defaultLookup(QuestionSession.class).find(fiche).size();
	if(nb>0)
		lblnombre.setValue(String.valueOf(nb));
	
	lblreponse.setValue("0");
	int nbr=BeanLocator.defaultLookup(QuestionSession.class).findRepond(fiche).size();
	 if(nbr>0)
	lblreponse.setValue(String.valueOf(nbr));
	 
	 lblrestant.setValue(String.valueOf(nb-nbr));
	
	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

    }

    public void onCreate(final CreateEvent createEvent) {
	final Map windowParams = createEvent.getArg();

	
    }

    @Override
    public void onEvent(Event event) throws Exception {
	if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {

	    List<PkQuestion> valLignes = findLignesFiche(fiche);

	    fmnsistGrid.setRowRenderer(new EditsRowReponseRenderer());

	    fmnsistGrid.setModel(new ListModelList(valLignes));

	} else if (event.getName().equalsIgnoreCase(
		ApplicationEvents.ON_AUTRE_ACTION)) {

	    String uri = "/speculation/ficheproductionlait/chargement.zul";
	    HashMap<String, String> display = new HashMap<String, String>();
	    HashMap<String, Object> data = new HashMap<String, Object>();
	    display.put(DSP_TITLE, Labels.getLabel("aprecose.form.charger.titre"));
	    display.put(DSP_HEIGHT, "30%");
	    display.put(DSP_WIDTH, "40%");
	    // Long code = (Long) list.getSelectedItem().getValue();
	    // data.put(PointageFormController.PARAM_WIDOW_CODE, code);
	    // data.put(PointageFormController.PARAM_WINDOW_MODE,
	    // UIConstants.MODE_EDIT);
	 //   data.put(ChargerController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
	    showPopupWindow(uri, null, display);

	}

	else if (event.getName().equalsIgnoreCase(
		ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {

//	    WriteExcelProdlait excel = new WriteExcelProdlait();
//
//	    String chemin = Sessions.getCurrent().getWebApp().getRealPath(
//		    Executions.getCurrent().getContextPath());
//	    chemin = chemin.replaceAll("\\\\", "/").substring(0,
//		    chemin.replaceAll("\\\\", "/").length() - 12)
//		    + "excels/productionlait" + ".xls";
//	    System.out.println("################" + chemin);
//	    excel.setOutputFile(chemin);
//
//	    //	    excel.setOutputFile("UtilisationProductionViande"+fiche.getId()+"("+campagne.getDateDebut()+" au "+campagne.getDateFin()+")"+".xls");
//
//	    try {
//		excel.write();
//	    } catch (WriteException e) {
//		e.printStackTrace();
//	    } catch (IOException e) {
//		e.printStackTrace();
//	    }
//
//	    // ouverture du fichier
//	    /*
//	     * Desktop desktop = Desktop.getDesktop(); try { desktop.open(new
//	     * File("UtilisationProductionViande"+fiche.getId()+"("+campagne.
//	     * getDateDebut()+" au "+campagne.getDateFin()+")"+".xls" )); }
//	     * catch (IOException e) {
//	     * e.printStackTrace(); }
//	     */
//
//	    loadApplicationState("lien_prod_lait");
//	    // fin du traitement
//
//	    // Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

	}

    }

    public void onClick$Exporter() {

	final HashMap<String, String> display = new HashMap<String, String>();
	display.put(MessageBoxController.DSP_MESSAGE, Labels
		.getLabel("aprecose.prodlaitt"));
	display.put(MessageBoxController.DSP_TITLE, Labels
		.getLabel("aprecose.exportation"));
	final HashMap<String, Object> data = new HashMap<String, Object>();
	// data.put("CODE", lstFmnsist.getSelectedItem().getValue());
	showMessageBox(display, data);

    }

    public void onClick$Importer1() {

	System.out.println("====================================>>>>>>");
	Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);
    }

    public void onClick$menuCancel() {
	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
	detach();
    }

    public void onClick$Fermer1() {
	// Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE,getOpener(),
	// null);
	// /detach();
	loadApplicationState("fiche_production_lait");
    }

    // public void initialiser(){
    //
    // createFiche(fiche);
    // }

    public void onClick$menuValider1() {

	// verifierChamps();
	// updateFiche();

	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
	detach();
    }

    public void onClick$menuValider2() {
	loadApplicationState("liste_fiche_collecte_production");
	// Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE,getOpener(),
	// null);
	// detach();
    }

    public void onClick$menuPreviousStep1() {

	step0.setVisible(true);
	step1.setVisible(false);
    }

    public void onClick$menuPreviousStep0() {
	step.setVisible(true);
	step0.setVisible(false);
	step1.setVisible(false);
    }

    public void onClick$menuCancel1() {

	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
	detach();

    }

    private List<PkQuestion> findLignesFiche(SygDossiers fiche) {

	List<PkQuestion> vals = new ArrayList<PkQuestion>();
	
	vals=BeanLocator.defaultLookup(QuestionSession.class).find(fiche);
	return vals;
    }
    
    
    public void onClick$generer1(){
		
//	
//			List<Object> valeurs= new ArrayList<Object>();
//			SiapDecoupage1  reg=(SiapDecoupage1) session.getAttribute("region");
//			String  idCampagne = getCampagne();
//				campagne = BeanLocator.defaultLookup(CampagneAgricolSession.class).findByCode(idCampagne);
//            
//				System.out.println("Campagne#####################"+campagne.getLibelle()+"#######region"+reg.getLibelle());
//			valeurs=BeanLocator.defaultLookup(LigneFicheProdLaitSession.class).findLigneAgregation(campagne, null, reg);
//		
//			//String idFiche=(String) getHttpSession().getAttribute("fiche");
//			fiche = (SiapFicheProdLait)getHttpSession().getAttribute("fiche");
//		
//			for(Object ligne :valeurs)	{
//			Object[] elmtligne = (Object[]) ligne;
//			System.out.println("#####Speculation"+ (String)elmtligne[0]+"TTTTTT#####Superficie#############TTTTTTT######Superficie"+  (Double)elmtligne[1]);
//			
//			
//			SiapSpeculation speculation= BeanLocator.defaultLookup(SpeculationSession.class).findByCode((String)elmtligne[0]);
//			SiapLigneFicheProdLait ligneFiche=BeanLocator.defaultLookup(LigneFicheProdLaitSession.class).find(fiche,speculation);
//		
//		  System.out.println("fivhe"+ligneFiche.getId()+"#######################################fiche"+fiche.getId()+"sepculation"+speculation.getCode());
//			
//			ligneFiche.setProdSysTraditionnel((Double)elmtligne[1]);
//			ligneFiche.setProdSysAmeliore((Double)elmtligne[2]);
//			ligneFiche.setProdSysIntensif((Double)elmtligne[3]);
//			ligneFiche.setProductionLait((Double)elmtligne[4]);
//			ligneFiche.setPrixMoyenLitre((Double)elmtligne[5]);
//			ligneFiche.setValProdLait((Double)elmtligne[6]);
//			  
//			BeanLocator.defaultLookup(LigneFicheProdLaitSession.class).update(ligneFiche);
//			
		//	}
		
			
			//loadApplicationState("tree_production");
			
			//Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			
		
			
	}

}
