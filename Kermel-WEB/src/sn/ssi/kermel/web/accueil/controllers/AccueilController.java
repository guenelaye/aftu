package sn.ssi.kermel.web.accueil.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SysParametresGeneraux;
import sn.ssi.kermel.be.entity.SysProfilAction;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.plansdepassation.ejb.PlansdepassationSession;
import sn.ssi.kermel.be.security.ProfilsSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class AccueilController  extends AbstractWindow implements AfterCompose, EventListener, ListitemRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Utilisateur infoscompte;
    private SygAutoriteContractante autorite=null;
   
    Session session = getHttpSession();
    private Label lblvalidationppm,lblpublicationppm,lblvalidationmajppm,lblpublicationmajppm,lblsoumettrevppm,lblrevue,lblvalidation,lblexamen,
    lblretour,lblimmatriculation,lblsoumettrevmajppm,lbltraitementdossier,lblrevueccmp,lblvalidationccmp,lblexamenccmp,lblvide;
	private Listbox lstListe;
 	private Paging pgPaging;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private Long ecart;
	List<SygPlansdepassation> plans = new ArrayList<SygPlansdepassation>();
	List<SygPlansdepassation> mavplans = new ArrayList<SygPlansdepassation>();
	private int nbreppmavalider=0,nbreppmapublier=0,nbreppmmajavalider=0,nbreppmmajapublier=0,nbrasoumettre=0,nbramajsoumettre=0;
	List<SysProfilAction> actions = new ArrayList<SysProfilAction>();
	private int nombredao=0,nombrevap=0,nombreejt=0,nombrerda=0,nombredi=0,nombredaoccmp=0,nombrevapccmp=0,nombreejtccmp=0,nombrenouveaudao=0,nbresuividao=0;
	private int nombreppp=0,nombremppp=0,nombretpppv=0,nombretpppp=0,nombretmpppv=0,nombretmpppp=0,nbrevideun=0,nbrevidedeux=0,nombreplanformation=0,nombresuiviformation=0;
	private Image imagetraitement,imagepassation;
	private Div divun,divdeux,divtraitement;
	private Label lbltravaux,lblservices,lblfournitures,lblpi,lbldsp,lblpassation;
	private int nbrefourniture=0,nbretravaux=0,nbreservice=0,nbrepi=0,nbredsp=0,nombrepassation=0;
	private int nombrenouveautaxe=0,nombresuivitaxe=0;
	private Div divpassation,divplanpassation;
	List<SygRealisations> realisationspartypes = new ArrayList<SygRealisations>();
	List<SygRealisations> realisationsparmodes = new ArrayList<SygRealisations>();
	private int nbrecodemarches=0,nbretextereglementaire=0,nbrelexique=0,nbrejournalmp=0
			,nbrerapportarmp=0,nbrearretesresiliations=0,nbrelrfournisseurs=0,nbrerelance=0;
	
	SysParametresGeneraux param;

	
	@Override
	public void afterCompose() {
		
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		lstListe.setItemRenderer(this);
		pgPaging.setPageSize(byPageBandbox);
		pgPaging.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		
		
	}

	public void onCreate(CreateEvent e) {
//		tabdemandes.setSelected(true);
//		Include inc = (Include) this.getFellowIfAny("incDemande");
//        inc.setSrc("/demande/listedemandeaccueil.zul");	
	//	param=BeanLocator.defaultLookup(ParametresGenerauxSession.class).findById(UIConstants.URL_SYGMAP);
		//gestion = Calendar.getInstance().get(Calendar.YEAR);
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
		{
			autorite=infoscompte.getAutorite();
		//	auteur=1;
		}
		
		plans=BeanLocator.defaultLookup(PlansdepassationSession.class).Plans(0,-1,null,null,null,autorite, 1,-1, null, null);
		mavplans=BeanLocator.defaultLookup(PlansdepassationSession.class).PlansMAJ_AValides(autorite, 1,-1);
		for(int i=0;i<plans.size();i++)
		{
			if(plans.get(i).getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.miseenvalidation")))
				nbreppmavalider=nbreppmavalider+1;
			if(plans.get(i).getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.valider")))
				nbreppmapublier=nbreppmapublier+1;
			if((plans.get(i).getStatus().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.saisie")))||(plans.get(i).getStatus().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.rejeter"))))
				 nbrasoumettre=nbrasoumettre+1;
			
		}
		lblvalidationppm.setValue(Labels.getLabel("kermel.common.acceuil.validationppm")+" ("+nbreppmavalider+")");
        lblpublicationppm.setValue(Labels.getLabel("kermel.common.acceuil.publicationppm")+" ("+nbreppmapublier+")");
			
		for(int j=0;j<mavplans.size();j++)
		{
			if(mavplans.get(j).getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.miseenvalidation")))
				nbreppmmajavalider=nbreppmmajavalider+1;
			if(mavplans.get(j).getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.valider")))
				nbreppmmajapublier=nbreppmmajapublier+1;
			if((mavplans.get(j).getStatus().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.saisie")))||(mavplans.get(j).getStatus().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.rejeter"))))
				nbramajsoumettre=nbramajsoumettre+1;
			
		}
       lblvalidationmajppm.setValue(Labels.getLabel("kermel.common.acceuil.validationmajppm")+" ("+nbreppmmajavalider+")");
	   lblpublicationmajppm.setValue(Labels.getLabel("kermel.common.acceuil.publicationmajppm")+" ("+nbreppmmajapublier+")");
	 
	   actions=BeanLocator.defaultLookup(ProfilsSession.class).Liste_Droits((String) getHttpSession().getAttribute("profil"));
	   for(int i=0;i<actions.size();i++)
	   {
		   if(actions.get(i).getSysAction().getActCode().equals("REVUEDAO"))
			   nombredao=nombredao+1;
		   if(actions.get(i).getSysAction().getActCode().equals("VALIDAP"))
			   nombrevap=nombrevap+1;
		   if(actions.get(i).getSysAction().getActCode().equals("EXAMENJUR"))
			   nombreejt=nombreejt+1;
		   if(actions.get(i).getSysAction().getActCode().equals("RETOURAPPROBATION"))
			   nombrerda=nombrerda+1;
		   if(actions.get(i).getSysAction().getActCode().equals("MATDOSSIER"))
			   nombredi=nombredi+1;
		   
		   if(actions.get(i).getSysAction().getActCode().equals("WSOUM_PLANPASSATION"))
			   nombreppp=nombreppp+1; 
		   if(actions.get(i).getSysAction().getActCode().equals("MAJPLANPASSATION"))
			   nombremppp=nombremppp+1; 
		  
		   if(actions.get(i).getSysAction().getActCode().equals("WVALID_PLANPASSATION"))
			   nombretpppv=nombretpppv+1; 
		   if(actions.get(i).getSysAction().getActCode().equals("WWPUB_PLANPASSATION"))
			   nombretpppp=nombretpppp+1; 
		   
		   if(actions.get(i).getSysAction().getActCode().equals("MWVALID_PLANPASSATION"))
			   nombretmpppv=nombretmpppv+1; 
		   if(actions.get(i).getSysAction().getActCode().equals("MWWPUB_PLANPASSATION"))
			   nombretmpppp=nombretmpppp+1; 
		   
		  
		   if(actions.get(i).getSysAction().getActCode().equals("REVUEDAOCCMP"))
			   nombredaoccmp=nombredaoccmp+1;
		   if(actions.get(i).getSysAction().getActCode().equals("VALIDAPCCMP"))
			   nombrevapccmp=nombrevapccmp+1;
		   if(actions.get(i).getSysAction().getActCode().equals("EXAMENJURCCMP"))
			   nombreejtccmp=nombreejtccmp+1;
		   
		   if(actions.get(i).getSysAction().getActCode().equals("WGER_PROCEDURESPASSATIONS"))
			   nombrepassation=nombrepassation+1;
		   
		   if(actions.get(i).getSysAction().getActCode().equals("RECOUVREMENT"))
			   nombrenouveaudao=nombrenouveaudao+1;
		   
		   if(actions.get(i).getSysAction().getActCode().equals("ADD_RECOUVREMENT")||actions.get(i).getSysAction().getActCode().equals("EDIT_DOSSIER"))
			   nbresuividao=nbresuividao+1;
		   
		   if(actions.get(i).getSysAction().getActCode().equals("ADD_FORM")||actions.get(i).getSysAction().getActCode().equals("DETAIL_FORM")
				   ||actions.get(i).getSysAction().getActCode().equals("MOD_FORM")||actions.get(i).getSysAction().getActCode().equals("SUP_FORM")
				   ||actions.get(i).getSysAction().getActCode().equals("WADD_AUTORITECONT")||actions.get(i).getSysAction().getActCode().equals("WADD_GROUPEFORMATION"))
			   nombreplanformation=nombreplanformation+1;
		   
		   if(actions.get(i).getSysAction().getActCode().equals("SUIVI_FORM"))
			   nombresuiviformation=nombresuiviformation+1;
		   
		   if(actions.get(i).getSysAction().getActCode().equals("PAIEMENT"))
			   nombrenouveautaxe=nombrenouveautaxe+1;
		   
		   if(actions.get(i).getSysAction().getActCode().equals("ADD_PAIEMENT")||actions.get(i).getSysAction().getActCode().equals("EDIT_CONTRAT"))
			   nombresuivitaxe=nombresuivitaxe+1;
		   
		  
		   
		   if(actions.get(i).getSysAction().getActCode().equals("ADD_PAIEMENT")||actions.get(i).getSysAction().getActCode().equals("EDIT_CONTRAT"))
			   nombresuivitaxe=nombresuivitaxe+1;
		   
		   if(actions.get(i).getSysAction().getActCode().equals("ADD_CODE_MARCHE")||actions.get(i).getSysAction().getActCode().equals("MOD_CODE_MARCHE")
				   ||actions.get(i).getSysAction().getActCode().equals("SUP_CODE_MARCHE"))
			   nbrecodemarches=nbrecodemarches+1;
		   
		   if(actions.get(i).getSysAction().getActCode().equals("ADD_TEXTE_REGLEMENTAIRE"))
			   nbretextereglementaire=nbretextereglementaire+1;
		   
		   if(actions.get(i).getSysAction().getActCode().equals("ADD_GLOSSAIRE")||actions.get(i).getSysAction().getActCode().equals("DEL_GLOSSAIRE")
				   ||actions.get(i).getSysAction().getActCode().equals("MOD_GLOSSAIRE"))
			   nbrelexique=nbrelexique+1;
		   
		   if(actions.get(i).getSysAction().getActCode().equals("ADD_JOURNAL")||actions.get(i).getSysAction().getActCode().equals("DEL_JOURNAL")
				   ||actions.get(i).getSysAction().getActCode().equals("MOD_JOURNAL"))
			   nbrejournalmp=nbrejournalmp+1;
		   
		   if(actions.get(i).getSysAction().getActCode().equals("ADD_RAPARMP")||actions.get(i).getSysAction().getActCode().equals("MOD_RAPARMP")
				   ||actions.get(i).getSysAction().getActCode().equals("SUPP_RAPARMP"))
			   nbrerapportarmp=nbrerapportarmp+1;
		   
		   if(actions.get(i).getSysAction().getActCode().equals("ADD_ARRETES")||actions.get(i).getSysAction().getActCode().equals("MOD_ARRETES")
				   ||actions.get(i).getSysAction().getActCode().equals("SUPP_ARRETES")||actions.get(i).getSysAction().getActCode().equals("WDEPUBLIER_ARRETES")
				   ||actions.get(i).getSysAction().getActCode().equals("WPUBLIER_ARRETES"))
			   nbrearretesresiliations=nbrearretesresiliations+1;
		   
		   if(actions.get(i).getSysAction().getActCode().equals("ADD_LISTE_ROUGE_F")||actions.get(i).getSysAction().getActCode().equals("MOD_LISTE_ROUGE_F")
				   ||actions.get(i).getSysAction().getActCode().equals("SUP_LISTE_ROUGE_F"))
			   nbrelrfournisseurs=nbrelrfournisseurs+1;
		   
		   if(actions.get(i).getSysAction().getActCode().equals("RELANCESACTIVITES"))
			   nbrerelance=nbrerelance+1;
	   }
	   if(nombredao>0)
	   {
		 //  nbredao=BeanLocator.defaultLookup( DossiersAppelsOffresSession.class).countRevueDAO( null,autorite, UIConstants.DOSSIERAVALIDER, gestion,null,null,BeConstants.PARAM_DNCMP);
		   lbltraitementdossier.setVisible(true);
		   lblrevue.setVisible(true); 
		 //  lblrevue.setValue(Labels.getLabel("kermel.common.acceuil.traitementdossier.revue")+" ("+nbredao+")");
		   lblrevue.setValue(Labels.getLabel("kermel.common.acceuil.traitementdossier.revue"));
		   imagetraitement.setVisible(true);
		   divun.setVisible(true); 
		   divtraitement.setVisible(true);
	   }
	   if(nombrevap>0)
	   {
		 //  nbrevap=BeanLocator.defaultLookup( DossiersAppelsOffresSession.class).countValidationAttributionprovisoire( null,autorite, UIConstants.DOSSIERAVALIDER, gestion,null,null,BeConstants.PARAM_DNCMP);
		   lbltraitementdossier.setVisible(true);
		   lblvalidation.setVisible(true);  
		   imagetraitement.setVisible(true);
		   divun.setVisible(true); 
		   divtraitement.setVisible(true);
		  // lblvalidation.setValue(Labels.getLabel("kermel.common.acceuil.traitementdossier.validation")+" ("+nbrevap+")");
		   lblvalidation.setValue(Labels.getLabel("kermel.common.acceuil.traitementdossier.validation"));
	   }
	   if(nombreejt>0)
	   {
		  // nbreejt=BeanLocator.defaultLookup( DossiersAppelsOffresSession.class).countExamenJurique(null, autorite, UIConstants.DOSSIERAVALIDER, gestion,null,null,BeConstants.PARAM_DNCMP);
		   lbltraitementdossier.setVisible(true);
		   lblexamen.setVisible(true);  
		   imagetraitement.setVisible(true);
		   divun.setVisible(true); 
		   divtraitement.setVisible(true);
		   //lblexamen.setValue(Labels.getLabel("kermel.common.acceuil.traitementdossier.examen")+" ("+nbreejt+")");
		   lblexamen.setValue(Labels.getLabel("kermel.common.acceuil.traitementdossier.examen"));
	   }
	   if(nombrerda>0)
	   {
		 //  nbrerda=BeanLocator.defaultLookup( DossiersAppelsOffresSession.class).countRetourApprobation(null, autorite,  gestion,null,null,null);
		   lbltraitementdossier.setVisible(true);
		   lblretour.setVisible(true);  
		   imagetraitement.setVisible(true);
		   divun.setVisible(true); 
		   divtraitement.setVisible(true);
		  // lblretour.setValue(Labels.getLabel("kermel.common.acceuil.traitementdossier.retour")+" ("+nbrerda+")");
		   lblretour.setValue(Labels.getLabel("kermel.common.acceuil.traitementdossier.retour"));
	   }
	   if(nombredi>0)
	   {
		  // nbredi=BeanLocator.defaultLookup( ContratsSession.class).countContrats(null, autorite, null, null,null, null, 0,gestion);
		   lbltraitementdossier.setVisible(true);
		   lblimmatriculation.setVisible(true);  
		   imagetraitement.setVisible(true);
		   divun.setVisible(true);
		   divtraitement.setVisible(true);
		  //.setValue(Labels.getLabel("kermel.common.acceuil.traitementdossier.immatriculation")+" ("+nbredi+")");
		   lblimmatriculation.setValue(Labels.getLabel("kermel.common.acceuil.traitementdossier.immatriculation"));
	   }
	   if(nombredaoccmp>0)
	   {
		  // nbredaoccmp=BeanLocator.defaultLookup( DossiersAppelsOffresSession.class).countRevueDAO( null,autorite, UIConstants.DOSSIERAVALIDER, gestion,null,null,BeConstants.PARAM_CCMP);
		   lbltraitementdossier.setVisible(true);
		   lblrevueccmp.setVisible(true);
		   imagetraitement.setVisible(true);
		   divdeux.setVisible(true); 
		   divtraitement.setVisible(true);
		   //lblrevueccmp.setValue(Labels.getLabel("kermel.common.acceuil.traitementdossier.revue")+" ("+nbredaoccmp+")");
		   lblrevueccmp.setValue(Labels.getLabel("kermel.common.acceuil.traitementdossier.revue"));
	   }
	   if(nombrevapccmp>0)
	   {
		  // nbrevapccmp=BeanLocator.defaultLookup( DossiersAppelsOffresSession.class).countValidationAttributionprovisoire( null,autorite, UIConstants.DOSSIERAVALIDER, gestion,null,null,BeConstants.PARAM_CCMP);
		   lbltraitementdossier.setVisible(true);
		   lblvalidationccmp.setVisible(true); 
		   imagetraitement.setVisible(true);
		   divdeux.setVisible(true); 
		   divtraitement.setVisible(true);
		  // lblvalidationccmp.setValue(Labels.getLabel("kermel.common.acceuil.traitementdossier.validation")+" ("+nbrevapccmp+")");
		   lblvalidationccmp.setValue(Labels.getLabel("kermel.common.acceuil.traitementdossier.validation"));
	   }
	   if(nombreejtccmp>0)
	   {
		  // nbreejtccmp=BeanLocator.defaultLookup( DossiersAppelsOffresSession.class).countExamenJurique(null, autorite, UIConstants.DOSSIERAVALIDER, gestion,null,null,BeConstants.PARAM_CCMP);
		   lbltraitementdossier.setVisible(true);
		   lblexamenccmp.setVisible(true);
		   imagetraitement.setVisible(true);
		   divdeux.setVisible(true); 
		   divtraitement.setVisible(true);
		 //  lblexamenccmp.setValue(Labels.getLabel("kermel.common.acceuil.traitementdossier.examen")+" ("+nbreejtccmp+")");
		   lblexamenccmp.setValue(Labels.getLabel("kermel.common.acceuil.traitementdossier.examen"));
	   }
	   if(infoscompte.getAutorite()!=null)
		{
		   if(nombreppp>0)
		   {
			   if(nbrasoumettre>0)
				{
					lblsoumettrevppm.setValue(Labels.getLabel("kermel.common.acceuil.plans.completer"));
					lblsoumettrevppm.setVisible(true);
					nbrevideun=nbrevideun+nbrasoumettre;  
				}
		   }
		   if(nombremppp>0)
		   {
			   if(nbramajsoumettre>0)
				{
					lblsoumettrevmajppm.setValue(Labels.getLabel("kermel.common.acceuil.plans.completermaj"));
					lblsoumettrevmajppm.setVisible(true);
					nbrevideun=nbrevideun+nbramajsoumettre;  
				}
		   }
		}
	  
	   if(nombretpppv>0)
	   {
		   if(nbreppmavalider>0)
			   lblvalidationppm.setVisible(true);
	   }
	   if(nombretpppp>0)
	   {
		   if(nbreppmapublier>0)
			   lblpublicationppm.setVisible(true);
	   }
	   if(nombretmpppv>0) 
	   {
		   if(nbreppmmajavalider>0)
			   lblvalidationmajppm.setVisible(true);
	   }
	   if(nombretmpppp>0)
	   {
		   if(nbreppmmajapublier>0)
		     lblpublicationmajppm.setVisible(true);
	   }
		
		nbrevidedeux=nbreppmmajavalider+nbreppmavalider+nbreppmmajapublier+nbreppmapublier;
		lblvide.setValue(Labels.getLabel("kermel.common.acceuil.traitementplan"));
	   if(autorite!=null)
	   {
		   if(nbrevideun==0)
			   lblvide.setVisible(true);
	   }
	   else
	   {
		   if(nbrevidedeux==0)
			   lblvide.setVisible(true);
	   }
	  
	   if(autorite!=null)
	   {
		   if(nombrepassation>0)
		   {
			   nbretravaux=BeanLocator.defaultLookup( AppelsOffresSession.class).count(UIConstants.PARAM_TMTRAVAUX,null,-1,autorite,  null, null,null);
			   nbrefourniture=BeanLocator.defaultLookup( AppelsOffresSession.class).count(UIConstants.PARAM_TMFOURNITURES,null,-1,autorite, null, null,null);
			   nbreservice=BeanLocator.defaultLookup( AppelsOffresSession.class).count(UIConstants.PARAM_TMSERVICES,null,-1,autorite, null,  null,null);
			   nbrepi=BeanLocator.defaultLookup( AppelsOffresSession.class).count(UIConstants.PARAM_TMPI,null,-1,autorite, null, null, null);
			   nbredsp=BeanLocator.defaultLookup( AppelsOffresSession.class).count(UIConstants.PARAM_TMDSP,null,-1,autorite, null, null, null);

			   lbltravaux.setValue(Labels.getLabel("kermel.plansdepassation.realisation.type.travaux")+" ("+nbretravaux+")");
			   lblfournitures.setValue(Labels.getLabel("kermel.plansdepassation.realisation.type.fournitures")+" ("+nbrefourniture+")");
			   lblservices.setValue(Labels.getLabel("kermel.plansdepassation.realisation.type.services")+" ("+nbreservice+")");
			   lblpi.setValue(Labels.getLabel("kermel.plansdepassation.realisation.type.pi")+" ("+nbrepi+")");
			   lbldsp.setValue(Labels.getLabel("kermel.plansdepassation.realisation.type.dsp")+" ("+nbredsp+")");
			   
			   lbltravaux.setVisible(true);
			   lblfournitures.setVisible(true);
			   lblservices.setVisible(true);
			   lblpi.setVisible(true);
			   lbldsp.setVisible(true);
			   
			   lblpassation.setVisible(true);
			   imagepassation.setVisible(true);
			   divpassation.setVisible(true);
		   }
	   }
	   if(nombreppp>0||nombremppp>0||nombretpppv>0||nombretpppp>0||nombretmpppv>0||nombretmpppp>0)
	   {
		   divplanpassation.setVisible(true);
	   }
	 
			
	}

	
	@Override
	public void onEvent(Event event) throws Exception {
	
		
	}

	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygRealisations realisations = (SygRealisations) data;
		item.setValue(realisations);
		
		Listcell cellReference = new Listcell(realisations.getReference());
		cellReference.setParent(item);
	
		Listcell cellObjet = new Listcell(realisations.getLibelle());
		cellObjet.setParent(item);
		
		Listcell cellDemarrage = new Listcell("");
		Listcell cellEcart = new Listcell("");
		 
		 
		 if(realisations.getTypemarche().getId().intValue()==UIConstants.PARAM_TMPI.intValue())
			{
			   ecart=ToolKermel.DifferenceDate(realisations.getDateavisccmpdncmpami(), new Date(), "jj");
			   cellDemarrage.setLabel(UtilVue.getInstance().formateLaDate(realisations.getDateavisccmpdncmpami()));
			   item.setAttribute("datedemarrage", UtilVue.getInstance().formateLaDate(realisations.getDateavisccmpdncmpami()));
			
			 }
			 else
			 {
				  ecart=ToolKermel.DifferenceDate(realisations.getDatereceptionavisccmpdncmpappel(), new Date(), "jj");
				  cellDemarrage.setLabel(UtilVue.getInstance().formateLaDate(realisations.getDatereceptionavisccmpdncmpappel()));
				  item.setAttribute("datedemarrage", UtilVue.getInstance().formateLaDate(realisations.getDatereceptionavisccmpdncmpappel()));
			 }
			
		 cellEcart.setLabel(ecart+"");
			 
		 cellDemarrage.setParent(item);
		 cellEcart.setParent(item);
		
		 
	
		
	}

	
	public void onClick$lblvalidationppm() {
		loadApplicationState("plans_avalider");
	}
	
	public void onClick$lblpublicationppm() {
		loadApplicationState("plans_avalider");
	}
	public void onClick$lblvalidationmajppm() {
		loadApplicationState("maj_plans_avalider");
	}
	
	public void onClick$lblpublicationmajppm() {
		loadApplicationState("maj_plans_avalider");
	}
	
	public void onClick$lblsoumettrevppm() {
		loadApplicationState("plans");
	}
	
	public void onClick$lblsoumettrevmajppm() {
		loadApplicationState("maj_plan");
	}
	
	public void onClick$lblrevue() {
		loadApplicationState("revue_dao");
	}
	public void onClick$lblvalidation() {
		loadApplicationState("validation_attributionprovisoire");
	}
	
	public void onClick$lblexamen() {
		loadApplicationState("examen_juridique");
	}
	
	public void onClick$lblretour() {
		loadApplicationState("retour_approbation");
	}
	
	public void onClick$lblimmatriculation() {
		loadApplicationState("immatriculation_contrats");
	}
	public void onClick$lblrevueccmp() {
		loadApplicationState("revue_dao_ccmp");
	}
	public void onClick$lblvalidationccmp() {
		loadApplicationState("validation_attributionprovisoire_ccmp");
	}
	public void onClick$lblexamenccmp() {
		loadApplicationState("examen_juridique_ccmp");
	}
	public void onClick$lbltravaux() {
		session.setAttribute("LibelleTab","REALTF");
		loadApplicationState("procedures_passations");
	}
	public void onClick$lblservices() {
		session.setAttribute("LibelleTab","REALSERVICES");
		loadApplicationState("procedures_passations");
	}
	public void onClick$lblfournitures() {
		session.setAttribute("LibelleTab","REALSFOURN");
		loadApplicationState("procedures_passations");
	}
	public void onClick$lblpi() {
		session.setAttribute("LibelleTab","REALPI");
		loadApplicationState("procedures_passations");
	}
	public void onClick$lbldsp() {
		session.setAttribute("LibelleTab","REALSDSP");
		loadApplicationState("procedures_passations");
	}
	

	


	
	
}
