package sn.ssi.kermel.web.accueil.controllers;


import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.PkQuestion;
import sn.ssi.kermel.be.session.QuestionSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.dspaoo.controllers.EditableDiv;
import sn.ssi.kermel.web.dspaoo.controllers.EditableRow;

public class EditsRowReponseRenderer extends AbstractWindow implements
		AfterCompose, RowRenderer, RowRendererExt {

	Integer fichecollecte;
	Session session = getHttpSession();
	

	@Override
	public void afterCompose() {
		// TODO Auto-generated method stub
		// fichecollecte = (Sim_FichesCollectes) session.getAttribute("fiche");
		// marche = BeanLocator.defaultLookup(MarchesSession.class).findByCode(
		// fichecollecte.getId());
		// System.out.println(marche.getLibelle() + "ok");
		// localite = BeanLocator.defaultLookup(LocalitesSession.class)
		// .findByCode(marche.getId());
		// System.out.print(localite.getLibelle() + "otre");
	}

	@Override
	public Row newRow(Grid grid) {
		// Create EditableRow instead of Row(default)
		Row row = new EditableRow();
		row.applyProperties();
		return row;
	}

	@Override
	public Component newCell(Row row) {
		return null;// Default Cell
	}

	

	@Override
	public int getControls() {
		return RowRendererExt.DETACH_ON_RENDER; // Default Value
	}


	@Override
	public void render(Row row, Object data, int index) throws Exception {
		final PkQuestion valeurProduit = (PkQuestion) data;	
		
		final EditableRow editRow = (EditableRow) row;
		final EditableDiv unP, deuxP, troisP, quatreP, cinqP,sixP;
		final Button editBtn;
		final Div ctrlDiv;
		final Button submitBtn = new Button(null, "/images/ok.png");
		final Button cancelBtn = new Button(null,"/images/cancel.png");
		

		final EditableDiv libelle = new EditableDiv(
				valeurProduit.getDescription(), false);
		
		   if(valeurProduit.getReponse()!=null)
			libelle.setStyle("background-color:#00FF00;");
			else
			libelle.setStyle("background-color:#FF0000;");
		   
			libelle.txb.setReadonly(true);
			libelle.setAlign("left");
			//libelle.setHeight("30px");
			
			
			libelle.setParent(editRow);
			
			unP = new EditableDiv(valeurProduit.getReponse(), true);
			unP.setStyle("background-color:#00FF00;");   
			unP.setParent(editRow);
			//unP.setAlign("center");
			
			
			
		
		
		 ctrlDiv = new Div();
		ctrlDiv.setParent(editRow);
		editBtn = new Button(null,"/images/pencil-small.png");
		editBtn.setMold("os");
		editBtn.setHeight("20px");
		editBtn.setWidth("30px");

			editBtn.setParent(ctrlDiv);
		editBtn.addEventListener(Events.ON_CLICK, new EventListener() {
			public void onEvent(Event event) throws Exception {
				final Button submitBtn = (Button) new Button(null, "/images/tick-small.png");
				final Button cancelBtn = (Button) new Button(null, "/images/cross-small.png");

				submitBtn.setMold("os");
				submitBtn.setHeight("20px");
				submitBtn.setWidth("30px");
				submitBtn.setTooltiptext("Valider la saisie");
				submitBtn.addEventListener(Events.ON_CLICK, new EventListener() {
					public void onEvent(Event event) throws Exception {

						editRow.toggleEditable(true);

			
						valeurProduit.setReponse(unP.txb.getValue());
					
						BeanLocator.defaultLookup(QuestionSession.class).update(valeurProduit);
						submitBtn.detach();
						cancelBtn.detach();
						editBtn.setParent(ctrlDiv);
						
					}
				});

				editRow.addEventListener(Events.ON_OK, new EventListener() {
					public void onEvent(Event event) throws Exception {

						editRow.toggleEditable(true);
						editRow.toggleEditable(true);
						valeurProduit.setReponse(unP.txb.getValue());
					
						BeanLocator.defaultLookup(QuestionSession.class).update(valeurProduit);
				
						submitBtn.detach();
						cancelBtn.detach();
						editBtn.setParent(ctrlDiv);
						
					}
				});
				cancelBtn.setMold("os");
				cancelBtn.setHeight("20px");
				cancelBtn.setWidth("30px");
				cancelBtn.setTooltiptext("Annuler la saisie");
				cancelBtn.addEventListener(Events.ON_CLICK, new EventListener() {
					public void onEvent(Event event) throws Exception {
						editRow.toggleEditable(false);
						submitBtn.detach();
						cancelBtn.detach();
						editBtn.setParent(ctrlDiv);
					}
				});
				submitBtn.setParent(ctrlDiv);
				cancelBtn.setParent(ctrlDiv);
				editRow.toggleEditable(true);
				editBtn.detach();
			}
		});
		
		
	}


}
