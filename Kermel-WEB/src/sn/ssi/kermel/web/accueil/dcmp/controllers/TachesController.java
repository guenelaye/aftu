package sn.ssi.kermel.web.accueil.dcmp.controllers;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygTachesafaire;
import sn.ssi.kermel.be.entity.SygTypesTaches;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.tachesafaire.ejb.TachesAFaireSession;
import sn.ssi.kermel.be.tachesafaire.ejb.TypeTachesSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class TachesController extends AbstractWindow implements AfterCompose,
		EventListener, ListitemRenderer {

	private Listbox lstTaches;
	private Paging tachePagin;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle,txtCode;
    String libelle=null,page=null,code=null,login;
    private Listheader lshLibelle,lshCode;
    Session session = getHttpSession();
    private Radiogroup rdDays;
    private Radio rdToday,rdTomorrow,rdNextWeek;
    private Date date;
    private Bandbox bdTypeTaches;
    private Textbox txtRechercherType;
    private Listbox lstType;
    private Paging pgType;
    private Long type;
    private static final String ON_MODEL_TYPE_CHANGE="onModelTypeChange";
	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
//		Object[] tache = (Object[])data;
//		item.setValue(tache[2]);
		
		SygTachesafaire tache = (SygTachesafaire)data;
		item.setValue(tache);
		
		Listcell img = new Listcell("","/images/tachesm.png");
		img.setParent(item);
		
		Listcell date = new Listcell(UtilVue.getInstance().formateLaDate(tache.getDateauplutard()));
		
		date.setParent(item);
	
		Listcell libelle = new Listcell(tache.getLibelle());
		
		libelle.setParent(item);
		
	if(tache.getDate().after(tache.getDateauplutard())){
		Listcell img2 = new Listcell("","/images/outlook.png");
		img2.setStyle("height:16px;width:16px");
		date.setStyle("color:red");
		libelle.setStyle("color:red");
		img2.setParent(item);
	}else{
		Listcell img2 = new Listcell("","/images/outlookv.png");
		img2.setParent(item);
	}
			
		

	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) 
		  {
			Utilisateur utilisateur = (Utilisateur) getHttpSession().getAttribute("utilisateur");
//			if (event.getData() != null) {
//				activePage = Integer.parseInt((String) event.getData());
//				byPage = -1;
//				tachePagin.setPageSize(1000);
//			} else {
//				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
//				activePage = tachePagin.getActivePage() * byPage;
//				tachePagin.setPageSize(byPage);
//				
//			}
			
			//List<Object[]> taches = BeanLocator.defaultLookup(TachesAFaireSession.class).getTacheToDoByType(activePage,byPage,utilisateur);
			List<SygTachesafaire> taches = BeanLocator.defaultLookup(TachesAFaireSession.class).findByDate(activePage,byPage,utilisateur,date, type);
			SimpleListModel listModel = new SimpleListModel(taches);
			lstTaches.setModel(listModel);
			//tachePagin.setTotalSize(BeanLocator.defaultLookup(SygTypeElementArbreSession.class).count());
		} else if(event.getName().equalsIgnoreCase(ON_MODEL_TYPE_CHANGE)){
			List<SygTypesTaches> typesTaches = BeanLocator.defaultLookup(TypeTachesSession.class).find(0, -1, null, libelle);
			SimpleListModel listModel = new SimpleListModel(typesTaches);
			lstType.setModel(listModel);
			pgType.setTotalSize(BeanLocator.defaultLookup(TypeTachesSession.class).count(null, libelle));
		}

	}

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ON_MODEL_TYPE_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_MODESSELECTIONS, this);
		
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		

		lstTaches.setItemRenderer(this);
		tachePagin.setPageSize(byPage);
		tachePagin.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		
		lstType.setItemRenderer(new TypeTacheRenderer());
		pgType.setPageSize(byPage);
		pgType.addForward("onPaging", this,ON_MODEL_TYPE_CHANGE);
		
		date = new Date();;
    	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	Events.postEvent(ON_MODEL_TYPE_CHANGE, this, null);
    	login = ((String) getHttpSession().getAttribute("user"));
		
		

	}
	
	public void onCheck$rdDays(){
		GregorianCalendar calendar = new GregorianCalendar();
		//calendar.setTime(GregorianCalendar.getInstance().getTime());
		if(rdDays.getSelectedItem()!=null&&((String) rdDays.getSelectedItem().getValue()).equalsIgnoreCase("today")){
			
			date = calendar.getTime();
			
		}else if(rdDays.getSelectedItem()!=null&&((String) rdDays.getSelectedItem().getValue()).equalsIgnoreCase("demain")){
			calendar.add(calendar.DAY_OF_MONTH, 1);
			date = calendar.getTime();
		}else if(rdDays.getSelectedItem()!=null&&((String) rdDays.getSelectedItem().getValue()).equalsIgnoreCase("semaine")){
			calendar.add(calendar.DAY_OF_MONTH, 7);
			date = calendar.getTime();
		}
		//System.out.println(date.toString());
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, date);
	}
	
	private class TypeTacheRenderer implements ListitemRenderer{

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypesTaches tache = (SygTypesTaches) data;
			item.setValue(tache.getId());
			item.setAttribute("LIBELLE", tache.getLibelle());
			Listcell libelle = new Listcell(tache.getLibelle());
			libelle.setParent(item);
		}
		
	}

	public void onSelect$lstType(){
		bdTypeTaches.setValue((String) lstType.getSelectedItem().getAttribute("LIBELLE"));
		type = (Long) lstType.getSelectedItem().getValue();
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		bdTypeTaches.close();
	}
}
