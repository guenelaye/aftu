package sn.ssi.kermel.web.servicetravaux.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygLotsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.LotsSoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class ClassementFinaleController extends AbstractWindow
		implements AfterCompose, EventListener,RowRenderer, RowRendererExt {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String mode, libDist, libEnqueteur, libMarche;
	int idProjet;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private final int byPage1 = 5;
	private Object selobj;
	// private Session session = getHttpSession();
	Grid GridLots;
	Paging pgPagination, pgDist, pgEnqueteur, pgMarche;
	Listbox lstDist, listEnqueteur, listMarche;
	Integer idFiche, distID, enqueteurID, marcheID;
	Textbox txtnumero, txtstatu, txtDrs, txtDist, anneemoi, txtRechercherDist,
			txtRechercherEnqueteur, txtObservation;
	Label nbre, lblMarche, lblEnqueteur, lblDate, lblRegion, lblDepartement,
			lblCampagne, lblSup1, lblSup2;
	Datebox datecree;
	Intbox nbretotalfs, nbretotalconsmois;
	Bandbox bdservice, bdDist, bdFmnsist, bdEnqueteur, bdMarche;
	Button btnGenerer, btnRechercherDist, btnRechercherMarcher,
			btnRechercherEnqueteur;
	Menuitem menuPreviousStep1;
	// Prodistrict district;
	// Proregionmedical regMed;
	private Div step1;
	private Iframe iframePrint;
	private boolean estGenere = false;
	private String datejour = UtilVue.getInstance().formateLaDate(new Date()),attributaireprovisoire,qualifie;
	private int annee = Integer.parseInt(ToolKermel.getAnneeCourante());
	private String mois = datejour.substring(3, 5);
	Session session = getHttpSession();
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	SygDossiers dossier=new SygDossiers();
    SygAutoriteContractante autorite;
	List<SygLotsSoumissionnaires> lotssoum = new ArrayList<SygLotsSoumissionnaires>();
	SygLotsSoumissionnaires lot=new SygLotsSoumissionnaires();

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
	


	}

	public void onCreate(final CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);

		if(dossier!=null)
		{
			
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			
		}
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {


			GridLots.setRowRenderer(this);

			    List<SygLotsSoumissionnaires> valLignes = new ArrayList<SygLotsSoumissionnaires>();
			    List<SygPlisouvertures> plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,1,1,1, -1, 1, -1, null, -1, null, null);
							
			  for (SygPlisouvertures pli : plis) {
				  SygLotsSoumissionnaires categ=new SygLotsSoumissionnaires();
			      
			      categ.setPlilibelle(pli.getRetrait().getNomSoumissionnaire());
			       List<SygLotsSoumissionnaires> vals = BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).find(0, -1, dossier, pli, null,-1,"oui",-1,-1, 2, null);
			   	   if(vals.size()!=0)   
				     valLignes.add(categ);	
		 		       for (SygLotsSoumissionnaires valLigne : vals) {
			 		       valLignes.add(valLigne);
			 		      
				        }
		 		      GridLots.setModel(new ListModelList(valLignes));	
			  		
	
			  }
		     	
		} 
		
								

	

		

	}


	

	 
	   @Override
		public Row newRow(Grid grid) {
			// Create EditableRow instead of Row(default)
			Row row = new EditableRow();
			row.applyProperties();
			return row;
		}

		@Override
		public Component newCell(Row row) {
			return null;// Default Cell
		}

		@Override
		public int getControls() {
			return RowRendererExt.DETACH_ON_RENDER; // Default Value
		}

		@Override
		public void render(Row row, Object data, int index) throws Exception {
//			final SiapzoneaRisque zoneaRisque = (SiapzoneaRisque) data;
			final SygLotsSoumissionnaires lots = (SygLotsSoumissionnaires) data;
			
			final EditableRow editRow = (EditableRow) row;
			
			if (lots.getPlilibelle() != null) {
				final EditableDiv libelle =	new EditableDiv(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial")+":  " +lots.getPlilibelle(),false);
				
				libelle.setHeight("30px");
	 			libelle.setStyle("color:#000;");
				libelle.txb.setReadonly(true);
				libelle.setParent(editRow);	
				
				final EditableDiv etat =	new EditableDiv(" ",false);
				etat.setParent(editRow);
				
				final EditableDiv etata =	new EditableDiv(" ",false);
				etata.setParent(editRow);
				
				final EditableDiv etataa =	new EditableDiv(" ",false);
				etataa.setParent(editRow);
				
				final EditableDiv action =	new EditableDiv(" ",false);
				action.setParent(editRow);	
			}
			else
			{
			
			lot=BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).getAttributaire(dossier, null, lots.getLot(), 1, "oui", 1, UIConstants.MOINSDISANTQUALIFIE);
			if(lot!=null)
			{
				if(lot.getPlis().getId().intValue()==lots.getPlis().getId().intValue())
				{
					qualifie=Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.evaluation.classementfinal.qualifie");
					attributaireprovisoire=Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.evaluation.classementfinal.attributaire");
					lots.setPlilattributaireProvisoire(1);
					row.setStyle("background-color:#00FF00;");
				}
				else
				{
					qualifie=Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.evaluation.classementfinal.nonqualifie");
					attributaireprovisoire="";
					lots.setPlilattributaireProvisoire(0);
				}
					
			}
			else
			{
				qualifie=Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.evaluation.classementfinal.nonqualifie");
				attributaireprovisoire="";
				lots.setPlilattributaireProvisoire(0);
			}
			BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).update(lots);
			final EditableDiv libelle =	new EditableDiv(lots.getLot().getLibelle(),false);
			libelle.setAlign("left");
			libelle.setStyle("margin-left:10px;color:#000;");
			libelle.txb.setReadonly(true);
			libelle.setParent(editRow);	
			
			final EditableDiv garantie =	new EditableDiv(ToolKermel.format2Decimal(lots.getPlilmontantoffert()),false);
			garantie.setAlign("center");
			garantie.setStyle("font-weight:bold;color:green");
			garantie.txb.setReadonly(true);
			garantie.setParent(editRow);	
			
			final EditableDiv montant =	new EditableDiv(ToolKermel.format2Decimal(lots.getPlilsrixevalue()),false);
			montant.setAlign("center");
			montant.setStyle("font-weight:bold;color:green");
			montant.txb.setReadonly(true);
			montant.setParent(editRow);	
			
			final EditableDiv qualifies =	new EditableDiv(qualifie,false);
			qualifies.setAlign("center");
			qualifies.setStyle("font-weight:bold;color:green");
			qualifies.txb.setReadonly(true);
			qualifies.setParent(editRow);
			
			final EditableDiv attributaire =	new EditableDiv(attributaireprovisoire,false);
			attributaire.setAlign("center");
			attributaire.setStyle("font-weight:bold;color:green");
			attributaire.txb.setReadonly(true);
			attributaire.setParent(editRow);
			attributaire.setStyle("font-weight:bold;color:green");
			
			//row.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			}
		}
	

		
	    
}
