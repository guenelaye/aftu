package sn.ssi.kermel.web.servicetravaux.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAttributions;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygAvisAttribution;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDevise;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygLotsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.AttributionsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.AvisAttributionSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DeviseSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DocumentsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.LotsSoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class AttributionprovisoireController extends AbstractWindow implements
EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstAttributaires;
	private Paging pgPagination,pgAttributaires;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference,libellebailleur=null;
    private int parent;
    List<SygDocuments> documents = new ArrayList<SygDocuments>();
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtVersionElectronique;
    SygBailleurs bailleur=null;
    private Decimalbox dcmontantprevu;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idbailleur;
	List<SygPlisouvertures> plisselectionnes = new ArrayList<SygPlisouvertures>();
	SygDocuments document=new SygDocuments();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private String nomFichier;
	private final String cheminDossier = UIConstants.PATH_PJ_PORTAIL;
	UtilVue utilVue = UtilVue.getInstance();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuValider,menuEditer,menuAttributaire;
	private Image image;
	private Label lbltitre,lbldeuxpoints;
	private Div step0,step1,step2,step3;
	private Iframe idIframe;
	private String extension,images;
	SygPlisouvertures plis=new SygPlisouvertures();
	SygLotsSoumissionnaires lot=new SygLotsSoumissionnaires();
	private Textbox txtAttributaire,txtReference,txtMonnaie,txtCommentaires;
	private Intbox indelaimois,indelaisemaine,indelaijours;
	private Datebox dtdateattribution;
	private Decimalbox dcmontantlu,dcmontantcorrige,dcmontantmarche,dcmontantdefinitif;
	private BigDecimal  montantcorrige,taux,montantmarche,montantdefinitif,montantlu;
	SygDevise devise=new SygDevise();
	SygAttributions attributaire=new SygAttributions();
	SygAttributions attributaires=new SygAttributions();
	public static final String EDIT_URL = "http://"+UIConstants.IP_SSI+":"+UIConstants.PORT_SSI+"/EtatsKermel/OuverturePlisPDFServlet";
	SygAvisAttribution avisattribution=new SygAvisAttribution(); 
	List<SygLotsSoumissionnaires> lots = new ArrayList<SygLotsSoumissionnaires>();
	private int nombre=0,lotpreselectionne=0;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		lstAttributaires.setItemRenderer(this);
		pgAttributaires.setPageSize(byPage);
		pgAttributaires.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		
		
		addEventListener(ApplicationEvents.ON_LOTS, this);
    	lstListe.setItemRenderer(new LotsRenderer());
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
	}

	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier==null)
		{
			menuValider.setDisabled(true);
			menuAttributaire.setDisabled(true);
			menuEditer.setDisabled(true);
			image.setVisible(false);
		}
		else
		{
			
			plisselectionnes=BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,1,1,1, 2, 1, -1, null, -1, null, null);
			for(int i=0;i<plisselectionnes.size();i++)
			{nombre=0;
				plis=plisselectionnes.get(i);
				lots= BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).find(activePage,byPageBandbox,dossier, plis, null, 1, "oui", 1,1, -1, null);
				
				for(int k=0;k<lots.size();k++)
				{
//					lot=BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).getAttributaire(dossier, plis, lots.get(k).getLot(), 1, "oui", 1, UIConstants.MOINSDISANTQUALIFIE);
//					if(lot.getPlis().getId().intValue()==lots.get(k).getPlis().getId().intValue())
//					{
//						nombre=nombre+1;
//					}
					if(lots.get(k).getPlilattributaireProvisoire()==1)
						nombre=nombre+1;
				}
				if(nombre>0)
				  plis.setAttributaireProvisoire(1);
				else
				  plis.setAttributaireProvisoire(0);
				BeanLocator.defaultLookup(RegistrededepotSession.class).update(plis);
					
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
		
	}

	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgAttributaires.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgAttributaires.getActivePage() * byPage;
				pgAttributaires.setPageSize(byPage);
			}
			 List<SygPlisouvertures> plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(activePage,byPageBandbox,dossier,null,null,null,1,1,1, 2, 1, 1, null, -1, null, null);
			 lstAttributaires.setModel(new SimpleListModel(plis));
			 pgAttributaires.setTotalSize(BeanLocator.defaultLookup(RegistrededepotSession.class).count(dossier,null,null,null,1,1,1, 2, 1, 1, null, -1, null, null));
			
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_LOTS)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygLotsSoumissionnaires> lots = BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).find(activePage,byPageBandbox,dossier, plis, null, 1, "oui", 1,1, -1, null);
			 lstListe.setModel(new SimpleListModel(lots));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).count(dossier, plis, null, 1, "oui", 1,1, -1, null));
			
		} 
	}
	public void infos(SygDossiers dossier,SygAppelsOffres appel) {
		documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossier, appel,UIConstants.PARAM_TYPEDOCUMENTS_ATTIBUTIONS,lot.getLot().getId(), null);
		attributaires=BeanLocator.defaultLookup(AttributionsSession.class).findAttributaire(dossier, plis,lot.getLot());
		
		if(documents.size()>0)
		{
			extension=documents.get(0).getNomFichier().substring(documents.get(0).getNomFichier().length()-3,  documents.get(0).getNomFichier().length());
			 if(extension.equalsIgnoreCase("pdf"))
				 images="/images/icone_pdf.png";
			 else  
				 images="/images/word.jpg";
			 
			image.setVisible(true);
			image.setSrc(images);
			lbldeuxpoints.setValue(":");
			lbltitre.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.procesverbalouverture.versionfichier"));
				
		
		}
	}
	public void onClick$menuAttributaire() {
		if (lstListe.getSelectedItem() == null)
			  throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		lot=(SygLotsSoumissionnaires) lstListe.getSelectedItem().getValue();
		plis=lot.getPlis();
		devise=BeanLocator.defaultLookup(DeviseSession.class).getMonnaieCFA(dossier, UIConstants.PARAM_CFA);
		if(devise!=null)
			taux=devise.getDevTauxConversion();
		else
			taux=new BigDecimal(0);
		if(plis!=null)
		{
			attributaires=BeanLocator.defaultLookup(AttributionsSession.class).findAttributaire(dossier, plis,lot.getLot());
			
			txtAttributaire.setValue(plis.getRetrait().getNomSoumissionnaire());
			txtReference.setValue(realisation.getReference());
			if(dossier.getDosDateAttributionProvisoire()!=null)
			  dtdateattribution.setValue(dossier.getDosDateAttributionProvisoire());
			txtMonnaie.setValue(lot.getMonnaie().getMonCode());
			
//			montantlu=lot.getPlilmontantoffert();
//			montantcorrige=lot.getPlilsrixevalue();
//			montantmarche=lot.getPlilsrixevalue();
			
			if (lot.getMonnaie().getMonCode().equals(UIConstants.PARAM_CFA))
			{
				montantlu=lot.getPlilmontantoffert();
				montantcorrige=lot.getPlilsrixevalue();
				montantmarche=lot.getPlilsrixevalue();
			}
			else
			{
				if(taux.intValue()>0)
				{
					montantlu=lot.getPlilmontantoffert().multiply(taux);
					montantcorrige=lot.getPlilsrixevalue().multiply(taux);
					montantmarche=lot.getPlilsrixevalue().multiply(taux);
				}
				else
				{
					montantlu=lot.getPlilmontantoffert();
					montantcorrige=lot.getPlilsrixevalue();
					montantmarche=lot.getPlilsrixevalue();
				}
					
			}
		//	montantdefinitif=plis.getMontantdefinitif();
			montantdefinitif=lot.getPlilsrixevalue();
			dcmontantlu.setValue(montantlu);
			dcmontantcorrige.setValue(montantcorrige);
			dcmontantmarche.setValue(montantmarche);
			dcmontantdefinitif.setValue(montantdefinitif);
			if(attributaires!=null)
			{
				attributaire=attributaires;
				indelaimois.setValue(attributaire.getMoisExecution());
				indelaisemaine.setValue(attributaire.getSemaineExecution());
				indelaijours.setValue(attributaire.getJoursExecution());
				dtdateattribution.setValue(attributaire.getDateattribution());
				txtCommentaires.setValue(attributaire.getCommentaire());
				menuValider.setDisabled(true); 
			}
			else
			{
				attributaire=new SygAttributions();
			}
			
		}
		infos(dossier,appel);
		step2.setVisible(false);
		step0.setVisible(true);
		step1.setVisible(false);
		step3.setVisible(false);
	}
	public void onClick$menuLots() {
		if (lstAttributaires.getSelectedItem() == null)
			  throw new WrongValueException(lstAttributaires, Labels.getLabel("kermel.error.select.item"));
		plis=(SygPlisouvertures) lstAttributaires.getSelectedItem().getValue();
		Events.postEvent(ApplicationEvents.ON_LOTS, this, null);
		step3.setVisible(false);
		step0.setVisible(false);
		step1.setVisible(false);
		step2.setVisible(true);
		
	}
	public void onClick$menuCancelstep2() {
		step3.setVisible(true);
		step0.setVisible(false);
		step1.setVisible(false);
		step2.setVisible(false);
	}
	public void onClick$menuCancel() {
		step2.setVisible(true);
		step0.setVisible(false);
		step1.setVisible(false);
		step3.setVisible(false);
		menuValider.setDisabled(false);
		lbldeuxpoints.setValue("");
		lbltitre.setValue("");
		image.setVisible(false);
	}
	public void onClick$btnChoixFichier() {
		//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
			if (ToolKermel.isWindows())
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectronique.setValue(nomFichier);
		}
	
private boolean checkFieldConstraints() {
		
		try {
		
			if(indelaimois.getValue()==null)
		     {
         errorComponent = indelaimois;
         errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.mois")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(indelaimois.getValue()>48)
		     {
        errorComponent = indelaimois;
        errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai")+" "+ Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.mois")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.valeur")+":48 ";
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(indelaisemaine.getValue()==null)
		     {
        errorComponent = indelaisemaine;
        errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.semaines")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(indelaisemaine.getValue()>3)
		     {
       errorComponent = indelaisemaine;
       errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.semaines")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.valeur")+":3 ";
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(indelaijours.getValue()==null)
		     {
        errorComponent = indelaijours;
        errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.jours")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(indelaijours.getValue()>30)
		     {
      errorComponent = indelaijours;
      errorMsg =  Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.jours")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.delai.valeur")+":30 ";
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dtdateattribution.getValue()==null)
		     {
          errorComponent = dtdateattribution;
          errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.dateattribution")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtVersionElectronique.getValue().equals(""))
		     {
         errorComponent = txtVersionElectronique;
         errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.attribution.procesverbal")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	
		public void onClick$menuValider() {
			if(checkFieldConstraints())
			{
				attributaires=BeanLocator.defaultLookup(AttributionsSession.class).findAttributaire(dossier, plis,lot.getLot());
				documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossier, appel,UIConstants.PARAM_TYPEDOCUMENTS_ATTIBUTIONS,lot.getLot().getId(), null);
				if(attributaires!=null)
					attributaire=attributaires;
				
				attributaire.setDossier(dossier);
				attributaire.setPlis(plis);
				attributaire.setLot(lot.getLot());
				attributaire.setCommentaire(txtCommentaires.getValue());
				attributaire.setDateattribution(dtdateattribution.getValue());
				attributaire.setMontantdefinitif(dcmontantdefinitif.getValue());
				attributaire.setMontantMarche(dcmontantmarche.getValue());
				attributaire.setMoisExecution(indelaimois.getValue());
				attributaire.setSemaineExecution(indelaisemaine.getValue());
				attributaire.setJoursExecution(indelaijours.getValue());
				attributaire.setAttributaireProvisoire(plis.getRaisonsociale());
			
				avisattribution.setAutorite(autorite);
				avisattribution.setDossier(dossier);
				avisattribution.setAttriRef(appel.getAporeference());
				avisattribution.setAttriType("provisoire");
				avisattribution.setAttriDate(dtdateattribution.getValue());
				avisattribution.setAttriObjet(appel.getApoobjet());
				avisattribution.setAttriPub(UIConstants.NPARENT);
				avisattribution.setAttriRaisonsocial(plis.getRaisonsociale());
				avisattribution.setAttrifichier(txtVersionElectronique.getValue());
				
				if(attributaires!=null)
				{
					BeanLocator.defaultLookup(AttributionsSession.class).save(attributaire);
					BeanLocator.defaultLookup(AvisAttributionSession.class).save(avisattribution);
				}
				else
				{
					BeanLocator.defaultLookup(AttributionsSession.class).update(attributaire);
					BeanLocator.defaultLookup(AvisAttributionSession.class).update(avisattribution);
				}
				
				if(documents.size()>0)
				document=documents.get(0);
				document.setAppel(appel);
				document.setDossier(dossier);
				document.setIdlot(lot.getLot().getId());
				if(!txtVersionElectronique.getValue().equals(""))
				document.setNomFichier(txtVersionElectronique.getValue());
				document.setTypeDocument(UIConstants.PARAM_TYPEDOCUMENTS_ATTIBUTIONS);
				document.setDate(new Date());
				document.setHeure(Calendar.getInstance().getTime());
				if(documents.size()==0)
				{
					BeanLocator.defaultLookup(DocumentsSession.class).save(document);
				}
				else
				{
					BeanLocator.defaultLookup(DocumentsSession.class).update(document);
				}
			
				dossier.setDosDateAttributionProvisoire(dtdateattribution.getValue());
				BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).update(dossier);
				//BeanLocator.defaultLookup(AppelsOffresSession.class).update(appel);
				
				session.setAttribute("libelle", "attributionprovisoire");
				loadApplicationState("suivi_service_trav");
			}
		}
		
		public void onClick$image() {
			step0.setVisible(false);
			step1.setVisible(true);
			 step3.setVisible(false);
			 step2.setVisible(false);
			String filepath = cheminDossier +  documents.get(0).getNomFichier();
			File f = new File(filepath.replaceAll("\\\\", "/"));

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(f, null, null);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (mymedia != null)
				idIframe.setContent(mymedia);
			else
				idIframe.setSrc("");

			idIframe.setHeight("600px");
			idIframe.setWidth("100%");
		}
		
		public org.zkoss.util.media.AMedia fetchFile(File file) {

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(file, null, null);
				return mymedia;
			} catch (Exception e) {

				e.printStackTrace();
				return null;
			}

		}
		public void onClick$menuFermer() {
			step0.setVisible(false);
			step1.setVisible(false);
			 step2.setVisible(false);
			 step3.setVisible(true);
		}
		
		 public void onClick$menuEditer(){
				
					step0.setVisible(false);
				    step1.setVisible(true);
				    step2.setVisible(false);
				    step3.setVisible(false);
				    idIframe.setSrc(EDIT_URL + "?code="+dossier.getDosID()+ "&libelle=pvattributionprovisoire");
					
				
			}
		 
			@Override
			public void render(final Listitem item, final Object data, int index) throws Exception {
				
				SygPlisouvertures plis = (SygPlisouvertures) data;
				item.setValue(plis);

				 Listcell cellNom = new Listcell(plis.getRetrait().getNomSoumissionnaire());
				 cellNom.setParent(item);
				 
				
			}
			
			 private class LotsRenderer implements ListitemRenderer {
					
					@Override
					public void render(Listitem item, Object data, int index)  throws Exception {
						SygLotsSoumissionnaires lots = (SygLotsSoumissionnaires) data;
						item.setValue(lots);

						 Listcell cellNumero = new Listcell(lots.getLot().getNumero());
						 cellNumero.setParent(item);
						 
						 Listcell cellLibelle = new Listcell(lots.getLot().getLibelle());
						 cellLibelle.setParent(item);
						 
						 Listcell cellMontant = new Listcell(ToolKermel.format2Decimal(lots.getPlilsrixevalue()));
						 cellMontant.setParent(item);
						 
						
					}
			 }
				
}