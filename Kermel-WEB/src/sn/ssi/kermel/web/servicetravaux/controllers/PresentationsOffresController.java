package sn.ssi.kermel.web.servicetravaux.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygContenusEnveloppesOffres;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPresentationsOffres;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.PresentationsOffresSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class PresentationsOffresController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstListeDetails,lstEnveloppe;
	private Paging pgPagination,pgPaginationDetails,pgEnveloppe;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelleenveloppe=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherEnveloppe;
    SygBailleurs bailleur=null;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idenveloppe=null,iddetails=null;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	SygPresentationsOffres enveloppe=new SygPresentationsOffres();
	private Div step0,step1,step2,step3;
	private Menuitem menuAjouter,menuModifier,menuSupprimer;
	private Textbox txtEnveloppe,txtDetails;
	private Label lblEnveloppe,lblEnveloppestep3;
	SygContenusEnveloppesOffres contenu=new SygContenusEnveloppesOffres();
	private Bandbox bdEnveloppe;
	SygPresentationsOffres enveloppeselect=new SygPresentationsOffres();
	private Radio   rdaffichage,rdnonaffichage,rdsignature,rdnonsignature,rdchiffre,rdnonchiffre  ;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
    	lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		
		lstListeDetails.setItemRenderer(new DetailsRenderer());
		pgPaginationDetails.setPageSize(byPage);
		pgPaginationDetails.addForward("onPaging", this, ApplicationEvents.ON_DETAILS);
		
		
		addEventListener(ApplicationEvents.ON_DOSSIERS, this);
		pgEnveloppe.setPageSize(byPage);
		pgEnveloppe.addForward("onPaging", this, ApplicationEvents.ON_DOSSIERS);
		lstEnveloppe.setItemRenderer(new EnveloppesRenderer());
    	
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier==null)
		{
			menuAjouter.setDisabled(true);
			menuModifier.setDisabled(true);
			menuSupprimer.setDisabled(true);
		}
		else
		{
//			if(dossier.getDosDateMiseValidation()!=null||dossier.getDosDatePublication()!=null)
//			{
//				menuAjouter.setDisabled(true);
//				menuModifier.setDisabled(true);
//				menuSupprimer.setDisabled(true);
//			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			
		}
		
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygPresentationsOffres> enveloppes = BeanLocator.defaultLookup(PresentationsOffresSession.class).find(activePage,byPageBandbox,dossier,null, null);
			 lstListe.setModel(new SimpleListModel(enveloppes));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(PresentationsOffresSession.class).count(dossier,null, null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DETAILS)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPaginationDetails.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPaginationDetails.getActivePage() * byPage;
				pgPaginationDetails.setPageSize(byPage);
			}
			 List<SygContenusEnveloppesOffres> details = BeanLocator.defaultLookup(PresentationsOffresSession.class).Contenus(activePage,byPageBandbox,enveloppe);
			 lstListeDetails.setModel(new SimpleListModel(details));
			 pgPaginationDetails.setTotalSize(BeanLocator.defaultLookup(PresentationsOffresSession.class).countContenus(enveloppe));
		} 
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			String nature = (String) ((HashMap<String, Object>) event .getData()).get(CURRENT_MODULE);
			if(nature.equals("SUPENV"))
			{
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
					Long codes = ((SygPresentationsOffres)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue()).getId();
					BeanLocator.defaultLookup(PresentationsOffresSession.class).delete(codes);
				}
				session.setAttribute("libelle", "presentationsoffres");
				loadApplicationState("suivi_service_trav");
			}
			else
			{
				if(nature.equals("SUPDETAILS"))
				{
					for (int i = 0; i < lstListeDetails.getSelectedCount(); i++) {
						Long codes = ((SygContenusEnveloppesOffres)((Listitem) lstListeDetails.getSelectedItems().toArray()[i]).getValue()).getId();
						BeanLocator.defaultLookup(PresentationsOffresSession.class).deleteContenu(codes);
					}
					Events.postEvent(ApplicationEvents.ON_DETAILS, this, null);
				}
				
			}
			
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DOSSIERS)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgEnveloppe.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgEnveloppe.getActivePage() * byPageBandbox;
				pgEnveloppe.setPageSize(byPageBandbox);
			}
			 List<SygPresentationsOffres> enveloppes = BeanLocator.defaultLookup(PresentationsOffresSession.class).find(activePage,byPageBandbox,dossier,libelleenveloppe, null);
			 lstEnveloppe.setModel(new SimpleListModel(enveloppes));
			 pgEnveloppe.setTotalSize(BeanLocator.defaultLookup(PresentationsOffresSession.class).count(dossier,libelleenveloppe, null));
		}
		
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygPresentationsOffres enveloppes = (SygPresentationsOffres) data;
		item.setValue(enveloppes);

		 Listcell cellIntitule = new Listcell(enveloppes.getIntituleenveloppe());
		 cellIntitule.setParent(item);
		 
		 Listcell cellChiffre = new Listcell("");
			if(enveloppes.getChiffre()==1)
			   cellChiffre.setLabel("Oui");
			else
			   cellChiffre.setLabel("Non");
			cellChiffre.setParent(item);
		
	}

	public void onClick$menuSupprimer()
	{
		if (lstListe.getSelectedItem() == null)
			
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		

		HashMap<String, String> display = new HashMap<String, String>(); // permet
		display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
		display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
		display.put(MessageBoxController.DSP_HEIGHT, "250px");
		display.put(MessageBoxController.DSP_WIDTH, "47%");

		HashMap<String, Object> map = new HashMap<String, Object>(); // permet
		map.put(CURRENT_MODULE, "SUPENV");
		showMessageBox(display, map);
		
	}
	public void onClick$menuModifier()
	{
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		enveloppe=(SygPresentationsOffres) lstListe.getSelectedItem().getValue();
		idenveloppe=enveloppe.getId();
		txtEnveloppe.setValue(enveloppe.getIntituleenveloppe());
		Events.postEvent(ApplicationEvents.ON_DOSSIERS, this, null);
		if(enveloppe.getEnveloppe()!=null)
		{
			enveloppeselect=enveloppe.getEnveloppe();
			bdEnveloppe.setValue(enveloppeselect.getIntituleenveloppe());
		}
		if(enveloppe.getChiffre()==1)
			rdchiffre.setSelected(true);	
		else
			rdnonchiffre.setSelected(true);
		
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
		step3.setVisible(false);
	}
	
	
	

	private boolean checkFieldConstraints() {
		
		try {
		
			if(txtEnveloppe.getValue().equals(""))
		     {
               errorComponent = txtEnveloppe;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.presentationoffres.intituleenveloppe")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			return true;
			
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	public void  onClick$menuFermer(){
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
		step3.setVisible(false);
	}
	public void  onClick$menuAjouter(){
		Events.postEvent(ApplicationEvents.ON_DOSSIERS, this, null);
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
		step3.setVisible(false);
		bdEnveloppe.setValue("");
		idenveloppe=null;
		txtEnveloppe.setValue("");
		
	}
	public void  onClick$menuValider(){
		if(checkFieldConstraints())
		{
			
			enveloppe.setDossier(dossier);
			enveloppe.setIntituleenveloppe(txtEnveloppe.getValue());
			if(bdEnveloppe.getValue()!=null&&!bdEnveloppe.getValue().equals(""))
				enveloppe.setEnveloppe(enveloppeselect);
			else
				enveloppe.setEnveloppe(null);
			
			if(rdchiffre.isSelected()==true)
				enveloppe.setChiffre(1);
			else
				enveloppe.setChiffre(0);
		
			if(idenveloppe==null)
				BeanLocator.defaultLookup(PresentationsOffresSession.class).save(enveloppe);
			else
				BeanLocator.defaultLookup(PresentationsOffresSession.class).update(enveloppe);
			session.setAttribute("libelle", "presentationsoffres");
			loadApplicationState("suivi_service_trav");
			
		}
	}
	
	public void  onClick$menuDetails(){
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		enveloppe=(SygPresentationsOffres) lstListe.getSelectedItem().getValue();
	    lblEnveloppe.setValue(enveloppe.getIntituleenveloppe());
	    lblEnveloppestep3.setValue(enveloppe.getIntituleenveloppe());
	    Events.postEvent(ApplicationEvents.ON_DETAILS, this, null);
		step0.setVisible(false);
		step1.setVisible(false);
		step2.setVisible(true);
		step3.setVisible(false);
	}
	
	public class DetailsRenderer implements ListitemRenderer{
		
    	@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygContenusEnveloppesOffres contenus = (SygContenusEnveloppesOffres) data;
			item.setValue(contenus);
			
			
			Listcell cellLibelle = new Listcell(contenus.getLibelle());
			cellLibelle.setParent(item);
			
			Listcell cellAffichage = new Listcell("");
			if(contenus.getAffichage()==1)
				cellAffichage.setLabel("Oui");
			else
				cellAffichage.setLabel("Non");
			cellAffichage.setParent(item);
    	
	    	Listcell cellSignature = new Listcell("");
			if(contenus.getSignature()==1)
				cellSignature.setLabel("Oui");
			else
				cellSignature.setLabel("Non");
			cellSignature.setParent(item);
         
		}
	}
	public void  onClick$menuAjouterstep2(){
		step0.setVisible(false);
		step1.setVisible(false);
		step2.setVisible(false);
		step3.setVisible(true);
		iddetails=null;
		txtDetails.setValue("");
		
	}
	
	private boolean checkFieldConstraintsstep3() {
		
		try {
		
			if(txtDetails.getValue().equals(""))
		     {
               errorComponent = txtDetails;
               errorMsg = Labels.getLabel("kermel.referentiel.common.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			return true;
			
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	
	public void  onClick$menuValiderstep3(){
		if(checkFieldConstraintsstep3())
		{
			
			contenu.setEnveloppe(enveloppe);
			contenu.setLibelle(txtDetails.getValue());
			if(rdaffichage.isSelected()==true)
				contenu.setAffichage(1);
			else
				contenu.setAffichage(0);
			
			if(rdsignature.isSelected()==true)
				contenu.setSignature(1);
			else
				contenu.setSignature(0);
			
			if(iddetails==null)
				BeanLocator.defaultLookup(PresentationsOffresSession.class).saveContenu(contenu);
			else
				BeanLocator.defaultLookup(PresentationsOffresSession.class).updateContenu(contenu);
			
			Events.postEvent(ApplicationEvents.ON_DETAILS, this, null);
			step0.setVisible(false);
			step1.setVisible(false);
			step2.setVisible(true);
			step3.setVisible(false);
			
		}
	}
	
	public void  onClick$menuFermerstep3(){
		step0.setVisible(false);
		step1.setVisible(false);
		step2.setVisible(true);
		step3.setVisible(false);
	}
	
	public void onClick$menuModifierstep2()
	{
		if (lstListeDetails.getSelectedItem() == null)
			throw new WrongValueException(lstListeDetails, Labels.getLabel("kermel.error.select.item"));
	    contenu=(SygContenusEnveloppesOffres) lstListeDetails.getSelectedItem().getValue();
		iddetails=contenu.getId();
		txtDetails.setValue(contenu.getLibelle());
		if(contenu.getAffichage()==1)
			rdaffichage.setSelected(true);	
		else
			rdnonaffichage.setSelected(true);
		
		if(contenu.getSignature()==1)
			rdsignature.setSelected(true);	
		else
			rdnonsignature.setSelected(true);
		step0.setVisible(false);
		step1.setVisible(false);
		step2.setVisible(false);
		step3.setVisible(true);
	}
	
	public void onClick$menuSupprimerstep2()
	{
		if (lstListeDetails.getSelectedItem() == null)
			
			throw new WrongValueException(lstListeDetails, Labels.getLabel("kermel.error.select.item"));
		

		HashMap<String, String> display = new HashMap<String, String>(); // permet
		display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
		display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
		display.put(MessageBoxController.DSP_HEIGHT, "250px");
		display.put(MessageBoxController.DSP_WIDTH, "47%");

		HashMap<String, Object> map = new HashMap<String, Object>(); // permet
		map.put(CURRENT_MODULE, "SUPDETAILS");
		showMessageBox(display, map);
		
	}
	
	public void  onClick$menuFermrstep2(){
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
		step3.setVisible(false);
	}
	
	public void onSelect$lstEnveloppe(){
		enveloppeselect= (SygPresentationsOffres) lstEnveloppe.getSelectedItem().getValue();
		bdEnveloppe.setValue(enveloppeselect.getIntituleenveloppe());
		bdEnveloppe.close();
	
	}
	
	public class EnveloppesRenderer implements ListitemRenderer{
		
		
		
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygPresentationsOffres enveloppe = (SygPresentationsOffres) data;
			item.setValue(enveloppe);
			
			Listcell cellLibelle = new Listcell(enveloppe.getIntituleenveloppe());
			cellLibelle.setParent(item);
		
		
			
		}
		}
		public void onFocus$txtRechercherEnveloppe(){
		if(txtRechercherEnveloppe.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.presentationoffres.enveloppe"))){
			txtRechercherEnveloppe.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherEnveloppe(){
		if(txtRechercherEnveloppe.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.presentationoffres.enveloppe")) || txtRechercherEnveloppe.getValue().equals("")){
			libelleenveloppe = null;
			page=null;
		}else{
			libelleenveloppe = txtRechercherEnveloppe.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_DOSSIERS, this, page);
		}

}