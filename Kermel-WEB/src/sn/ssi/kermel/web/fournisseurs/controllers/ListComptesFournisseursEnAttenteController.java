package sn.ssi.kermel.web.fournisseurs.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.PklComptePerso;
import sn.ssi.kermel.be.entity.PklComptePerso.StatutCompte;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.session.ComptePersoSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class ListComptesFournisseursEnAttenteController extends AbstractWindow
		implements EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE, activePage;
	public static final String CURRENT_MODULE = "CURRENT_MODULE";
	public Textbox txtObjet, txtRef;
	String libelle = null, page = null, code = null, login;
	
	Session session = getHttpSession();
	
	SygDossiers dossier = null;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		// dossiers
		dossier = (SygDossiers) getHttpSession().getAttribute("dossier");

		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);

		addEventListener(ApplicationEvents.ON_DETAILS, this);

		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

	}

	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}

			dossier = (SygDossiers) getHttpSession().getAttribute("dossier");
			List<PklComptePerso> comptes = BeanLocator.defaultLookup(
					ComptePersoSession.class).find(0, -1, StatutCompte.ATTENTE,
					null);
			SimpleListModel listModel = new SimpleListModel(comptes);
			lstListe.setModel(listModel);
//			lstListe.setVisible(true);
			pgPagination
					.setTotalSize(BeanLocator.defaultLookup(
							ComptePersoSession.class).count(
							StatutCompte.ATTENTE, null));
		} else if (event.getName().equalsIgnoreCase(
				ApplicationEvents.ON_DETAILS)) {

			if (lstListe.getSelectedItem() == null)

				throw new WrongValueException(lstListe,
						Labels.getLabel("kermel.error.select.item"));

			final String uri = "/fournisseurs/comptes/formactivercompte.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, "Activation compte fournisseur");
			display.put(DSP_HEIGHT, "500px");
			display.put(DSP_WIDTH, "80%");

			getHttpSession().setAttribute("compte",
					lstListe.getSelectedItem().getValue());

			showPopupWindow(uri, null, display);
		}
	}

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		PklComptePerso compte = (PklComptePerso) data;
		item.setValue(compte);

		Listcell cellPrenom = new Listcell(compte.getPrenom());
		cellPrenom.setParent(item);

		Listcell cellNom = new Listcell(compte.getNom());
		cellNom.setParent(item);

		Listcell cellEmail = new Listcell(compte.getMail());
		cellEmail.setParent(item);

		Listcell cellTel = new Listcell(compte.getTelephone());
		cellTel.setParent(item);

		Listcell cellFournisseur = new Listcell();
		if (compte.getFournisseur() != null)
			cellFournisseur.setLabel(compte.getFournisseur().getNom());
		cellFournisseur.setParent(item);

		Listcell cellDate = new Listcell("");
		if (compte.getDateInscription() != null)
			cellDate.setLabel(ToolKermel.dateToString(compte
					.getDateInscription()));
		cellDate.setParent(item);

	}

}