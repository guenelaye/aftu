package sn.ssi.kermel.web.fournisseurs.controllers;


import java.io.File;
import java.util.Date;
import java.util.Map;

import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.MessageManager;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.PklComptePerso;
import sn.ssi.kermel.be.entity.PklComptePerso.StatutCompte;
import sn.ssi.kermel.be.session.ComptePersoSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class FormActiverCompteController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	private static final String PARAM_WINDOW_CODE = "CODE";
	private static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	private static final String WINDOW_PARAM_MODE = "MODE";
	
	private PklComptePerso compte = null;
   
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	
	private Label lblAdmin,lblAdminFonction,lblAdminTelephone,lblAdminEmail,lblTelephone, 
	lblEmail,lblNom, lblFonction, lblRaisonSociale, lblFormJuridique,lblNinea,
	lblRccm , lblPays, lblAdresse, lblAdminAdresse;
	
	private Iframe idIframeNinea, idIframeRc;
	
	
	
	@Override
	public void onEvent(Event event) throws Exception {

		
		System.out.println("jfhh");

	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		
		if(getHttpSession().getAttribute("compte") != null)
			compte = (PklComptePerso)  getHttpSession().getAttribute("compte");
		if(compte != null)
		{
			if(compte.getFournisseur() != null) {
				lblRaisonSociale.setValue(compte.getFournisseur().getNom());
				lblNinea.setValue(compte.getFournisseur().getNinea());
				lblRccm.setValue(compte.getFournisseur().getRegistreCommerce());
				if(compte.getFournisseur().getPays() != null) {
					lblPays.setValue(compte.getFournisseur().getPays().getLibelle());
				}
//				lblFormJuridique.setValue(compte.getFournisseur().get);
			}
			
			lblNom.setValue(compte.getFournisseur().getRmPrenom()+ " " + compte.getFournisseur().getRmNom());
			lblFonction.setValue(compte.getFournisseur().getRmFonction());
			lblEmail.setValue(compte.getFournisseur().getRmEmail());
			lblTelephone.setValue(compte.getFournisseur().getRmTel());
			lblAdresse.setValue(compte.getFournisseur().getRmAdresse());
			
			lblAdmin.setValue(compte.getPrenom()+ " " + compte.getNom());
			lblAdminFonction.setValue(compte.getFonction());
			lblAdminEmail.setValue(compte.getMail());
			lblAdminTelephone.setValue(compte.getTelephone());
			lblAdminAdresse.setValue(compte.getAdresse());
			
			String cheminNinea = "/home/moussa/Documents/pdfservlet.pdf";
			File ninea = new File(cheminNinea);
			if(ninea.exists()) {
				idIframeNinea.setContent(fetchFile(ninea));
			}
			
		}
		
	}

	public void onOK() {	
		
		if(compte != null) {
			compte.setStatut(StatutCompte.ACTIVE);
			String hash = ToolKermel.generateHash();
			compte.setDateActivation(new Date());
			compte.setHash(hash);
			
			String link = UIConstants.urlKermel+ "/active?h="+hash;
			
			BeanLocator.defaultLookup(ComptePersoSession.class).update(compte);
			String message = "<html xmlns=\"http://www.w3.org/1999/xhtml\"><head>  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">  <meta name=\"viewport\" content=\"initial-scale=1.0\">  <meta name=\"format-detection\" content=\"telephone=no\">  <title></title>  <style type=\"text/css\"> 	body {		width: 100%;		margin: 0;		padding: 0;		-webkit-font-smoothing: antialiased;	}	@media only screen and (max-width: 600px) {		table[class=\"table-row\"] {			float: none !important;			width: 98% !important;			padding-left: 20px !important;			padding-right: 20px !important;		}		table[class=\"table-row-fixed\"] {			float: none !important;			width: 98% !important;		}		table[class=\"table-col\"], table[class=\"table-col-border\"] {			float: none !important;			width: 100% !important;			padding-left: 0 !important;			padding-right: 0 !important;			table-layout: fixed;		}		td[class=\"table-col-td\"] {			width: 100% !important;		}		table[class=\"table-col-border\"] + table[class=\"table-col-border\"] {			padding-top: 12px;			margin-top: 12px;			border-top: 1px solid #E8E8E8;		}		table[class=\"table-col\"] + table[class=\"table-col\"] {			margin-top: 15px;		}		td[class=\"table-row-td\"] {			padding-left: 0 !important;			padding-right: 0 !important;		}		table[class=\"navbar-row\"] , td[class=\"navbar-row-td\"] {			width: 100% !important;		}		img {			max-width: 100% !important;			display: inline !important;		}		img[class=\"pull-right\"] {			float: right;			margin-left: 11px;            max-width: 125px !important;			padding-bottom: 0 !important;		}		img[class=\"pull-left\"] {			float: left;			margin-right: 11px;			max-width: 125px !important;			padding-bottom: 0 !important;		}		table[class=\"table-space\"], table[class=\"header-row\"] {			float: none !important;			width: 98% !important;		}		td[class=\"header-row-td\"] {			width: 100% !important;		}	}	@media only screen and (max-width: 480px) {		table[class=\"table-row\"] {			padding-left: 16px !important;			padding-right: 16px !important;		}	}	@media only screen and (max-width: 320px) {		table[class=\"table-row\"] {			padding-left: 12px !important;			padding-right: 12px !important;		}	}	@media only screen and (max-width: 458px) {		td[class=\"table-td-wrap\"] {			width: 100% !important;		}	}  </style> </head> <body marginwidth=\"0\" marginheight=\"0\" topmargin=\"0\" leftmargin=\"0\" bgcolor=\"#E4E6E9\" style=\"font-family: Arial, sans-serif; font-size: 13px; color: rgb(68, 68, 68); min-height: 200px;\"> <table width=\"100%\" height=\"100%\" bgcolor=\"#E4E6E9\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"> <tbody><tr><td width=\"100%\" align=\"center\" valign=\"top\" bgcolor=\"#E4E6E9\" style=\"background-color: #009688;min-height: 200px;\"><table><tbody><tr><td class=\"table-td-wrap\" align=\"center\" width=\"458\"><table class=\"table-space\" height=\"18\" style=\"height: 18px; font-size: 0px; line-height: 0; width: 450px; background-color: #e4e6e9;\" width=\"450\" bgcolor=\"#E4E6E9\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td class=\"table-space-td\" valign=\"middle\" height=\"18\" style=\"height: 18px; width: 450px; background-color: #e4e6e9;\" width=\"450\" bgcolor=\"#E4E6E9\" align=\"left\">&nbsp;</td></tr></tbody></table><table class=\"table-space\" height=\"8\" style=\"height: 8px; font-size: 0px; line-height: 0; width: 450px; background-color: #ffffff;\" width=\"450\" bgcolor=\"#FFFFFF\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td class=\"table-space-td\" valign=\"middle\" height=\"8\" style=\"height: 8px; width: 450px; background-color: #ffffff;\" width=\"450\" bgcolor=\"#FFFFFF\" align=\"left\">&nbsp;</td></tr></tbody></table><table class=\"table-row\" width=\"450\" bgcolor=\"#FFFFFF\" style=\"table-layout: fixed; background-color: #ffffff;\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td class=\"table-row-td\" style=\"font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;\" valign=\"top\" align=\"left\">  <table class=\"table-col\" align=\"left\" width=\"378\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"table-layout: fixed;\"><tbody><tr><td class=\"table-col-td\" width=\"378\" style=\"font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; width: 378px;\" valign=\"top\" align=\"left\">    <table class=\"header-row\" width=\"378\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"table-layout: fixed;\"><tbody><tr><td class=\"header-row-td\" width=\"378\" style=\"font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; color: #478fca; margin: 0px; font-size: 18px; padding-bottom: 10px; padding-top: 15px;\" valign=\"top\" align=\"left\">Votre compte administrateur sur KERMEL a été activé !</td></tr></tbody></table>    <div style=\"font-family: Arial, sans-serif; line-height: 20px; color: #444444; font-size: 13px;\">      <b style=\"color: #777777;\">Nous sommes heureux de vous annoncer la validation de votre compte</b>      <br>Pour accéder à votre compte, cliquer sur le bouton ci-dessous</div>  </td></tr></tbody></table></td></tr></tbody></table>    <table class=\"table-space\" height=\"12\" style=\"height: 12px; font-size: 0px; line-height: 0; width: 450px; background-color: #ffffff;\" width=\"450\" bgcolor=\"#FFFFFF\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td class=\"table-space-td\" valign=\"middle\" height=\"12\" style=\"height: 12px; width: 450px; background-color: #ffffff;\" width=\"450\" bgcolor=\"#FFFFFF\" align=\"left\">&nbsp;</td></tr></tbody></table><table class=\"table-space\" height=\"12\" style=\"height: 12px; font-size: 0px; line-height: 0; width: 450px; background-color: #ffffff;\" width=\"450\" bgcolor=\"#FFFFFF\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td class=\"table-space-td\" valign=\"middle\" height=\"12\" style=\"height: 12px; width: 450px; padding-left: 16px; padding-right: 16px; background-color: #ffffff;\" width=\"450\" bgcolor=\"#FFFFFF\" align=\"center\">&nbsp;<table bgcolor=\"#E8E8E8\" height=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td bgcolor=\"#E8E8E8\" height=\"1\" width=\"100%\" style=\"height: 1px; font-size:0;\" valign=\"top\" align=\"left\">&nbsp;</td></tr></tbody></table></td></tr></tbody></table><table class=\"table-space\" height=\"16\" style=\"height: 16px; font-size: 0px; line-height: 0; width: 450px; background-color: #ffffff;\" width=\"450\" bgcolor=\"#FFFFFF\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td class=\"table-space-td\" valign=\"middle\" height=\"16\" style=\"height: 16px; width: 450px; background-color: #ffffff;\" width=\"450\" bgcolor=\"#FFFFFF\" align=\"left\">&nbsp;</td></tr></tbody></table><table class=\"table-row\" width=\"450\" bgcolor=\"#FFFFFF\" style=\"table-layout: fixed; background-color: #ffffff;\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td class=\"table-row-td\" style=\"font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;\" valign=\"top\" align=\"left\">  <table class=\"table-col\" align=\"left\" width=\"378\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"table-layout: fixed;\"><tbody><tr><td class=\"table-col-td\" width=\"378\" style=\"font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; width: 378px;\" valign=\"top\" align=\"left\">    <div style=\"font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; text-align: center;\">      <a href=\""+link+"\" style=\"color: #ffffff; text-decoration: none; margin: 0px; text-align: center; vertical-align: baseline; border: 4px solid #6fb3e0; padding: 4px 9px; font-size: 15px; line-height: 21px; background-color: #6fb3e0;\">&nbsp; Continuer &nbsp;</a>    </div>    <table class=\"table-space\" height=\"16\" style=\"height: 16px; font-size: 0px; line-height: 0; width: 378px; background-color: #ffffff;\" width=\"378\" bgcolor=\"#FFFFFF\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td class=\"table-space-td\" valign=\"middle\" height=\"16\" style=\"height: 16px; width: 378px; background-color: #ffffff;\" width=\"378\" bgcolor=\"#FFFFFF\" align=\"left\">&nbsp;</td></tr></tbody></table>  </td></tr></tbody></table></td></tr></tbody></table><table class=\"table-space\" height=\"6\" style=\"height: 6px; font-size: 0px; line-height: 0; width: 450px; background-color: #ffffff;\" width=\"450\" bgcolor=\"#FFFFFF\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td class=\"table-space-td\" valign=\"middle\" height=\"6\" style=\"height: 6px; width: 450px; background-color: #ffffff;\" width=\"450\" bgcolor=\"#FFFFFF\" align=\"left\">&nbsp;</td></tr></tbody></table><table class=\"table-row-fixed\" width=\"450\" bgcolor=\"#FFFFFF\" style=\"table-layout: fixed; background-color: #ffffff;\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td class=\"table-row-fixed-td\" style=\"font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 1px; padding-right: 1px;\" valign=\"top\" align=\"left\">  <table class=\"table-col\" align=\"left\" width=\"448\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"table-layout: fixed;\"><tbody><tr><td class=\"table-col-td\" width=\"448\" style=\"font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;\" valign=\"top\" align=\"left\">    <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"table-layout: fixed;\"><tbody><tr><td width=\"100%\" align=\"center\" bgcolor=\"#f5f5f5\" style=\"font-family: Arial, sans-serif; line-height: 24px; color: #bbbbbb; font-size: 13px; font-weight: normal; text-align: center; padding: 9px; border-width: 1px 0px 0px; border-style: solid; border-color: #e3e3e3; background-color: #f5f5f5;\" valign=\"top\">      <a href=\"#\" style=\"color: #428bca; text-decoration: none; background-color: transparent;\">KERMEL © 2017</a>      <br>            .            .          </td></tr></tbody></table>  </td></tr></tbody></table></td></tr></tbody></table><table class=\"table-space\" height=\"1\" style=\"height: 1px; font-size: 0px; line-height: 0; width: 450px; background-color: #ffffff;\" width=\"450\" bgcolor=\"#FFFFFF\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td class=\"table-space-td\" valign=\"middle\" height=\"1\" style=\"height: 1px; width: 450px; background-color: #ffffff;\" width=\"450\" bgcolor=\"#FFFFFF\" align=\"left\">&nbsp;</td></tr></tbody></table><table class=\"table-space\" height=\"36\" style=\"height: 36px; font-size: 0px; line-height: 0; width: 450px; background-color: #e4e6e9;\" width=\"450\" bgcolor=\"#E4E6E9\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\"><tbody><tr><td class=\"table-space-td\" valign=\"middle\" height=\"36\" style=\"height: 36px; width: 450px; background-color: #e4e6e9;\" width=\"450\" bgcolor=\"#E4E6E9\" align=\"left\">&nbsp;</td></tr></tbody></table></td></tr></tbody></table></td></tr> </tbody></table>  </body></html>";
			try {
				MessageManager.sendMail("Activation de votre compte KERMEL", message , compte.getMail());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		    
	}
	
	public void onClose() {
		
		this.detach();
	    
	}
	
	public org.zkoss.util.media.AMedia fetchFile(File file) {

		org.zkoss.util.media.AMedia mymedia = null;
		try {
			mymedia = new AMedia(file, null, null);
			return mymedia;
		} catch (Exception e) {

			e.printStackTrace();
			return null;
		}

	}

	

	
	
	

}
