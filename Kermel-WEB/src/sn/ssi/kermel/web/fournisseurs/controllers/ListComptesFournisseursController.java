package sn.ssi.kermel.web.fournisseurs.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.PklComptePerso.StatutCompte;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.session.ComptePersoSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


@SuppressWarnings("serial")
public class ListComptesFournisseursController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtObjet,txtRef;
    String libelle=null,page=null,code=null,login;
    private Listheader lshObjet,lshRef;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem  TRAITER_REPONSE, WMODP_TYPESMARCHES;
    SygDossiers  dossier;
    private Include  includePage1,includePage2;
    private Tab   page1,page2; 
    private Label   lblRef,lblObjet,lblpublier,lblNbr;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		//dossiers
//		dossier=(SygDossiers) getHttpSession().getAttribute("dossier");
//		lblRef.setValue(dossier.getAppel().getAporeference());
//		lblObjet.setValue(dossier.getAppel().getApoobjet());
//		lblpublier.setValue(ToolKermel.dateToString(dossier.getDosDatePublication()));
//		lblNbr.setValue("");
//		
		
		
		
		
    	login = ((String) getHttpSession().getAttribute("user"));
    	page1.setLabel("Comptes en attentes d'activation ("+BeanLocator.defaultLookup(
				ComptePersoSession.class).count(StatutCompte.ATTENTE, null)+")");
    	page2.setLabel("Comptes activés ("+BeanLocator.defaultLookup(
				ComptePersoSession.class).count(StatutCompte.ACTIVE, null)+")");
    	
    	
    	
    	includePage1.setSrc("/fournisseurs/comptes/comptesenattentes.zul");
    	
    	includePage2.setSrc("/fournisseurs/comptes/comptesactives.zul");
	}

	 
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		
	}


	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub
		
	}

	
	
}