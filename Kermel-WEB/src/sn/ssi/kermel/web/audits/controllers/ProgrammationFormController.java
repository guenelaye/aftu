package sn.ssi.kermel.web.audits.controllers;


import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;

import sn.ssi.kermel.be.audit.ejb.AuditsPrestatairesSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAuditsPrestataires;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


@SuppressWarnings("serial")
public class ProgrammationFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */

	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	//private String mode;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Datebox txtdatefinprevu,txtdatedebutprevu,txtdatefinreel,txtdatedebutreel;
	//private SygAuditsPrestataires auditsprest =new SygAuditsPrestataires();
	//private SimpleListModel lst;
	//private final String ON_LOCALITE = "onLocalite";
	//private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	Long code;
	//private SygAutoriteContractante autorite;

	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Label lbStatusBar;
	private Long idpaac;
	//private SygAudit audit ;
	Session session = getHttpSession();
	private SygAuditsPrestataires autoritesaudit;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

			
		idpaac=(Long) session.getAttribute("codeautoritesaudit");    
		autoritesaudit= BeanLocator.defaultLookup(AuditsPrestatairesSession.class).findById(idpaac);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	
	}
	
	
	@Override
	public void onEvent(Event arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	
	
	public void onCreate(CreateEvent event) {
	
		if(autoritesaudit!=null){

			
			txtdatedebutprevu.setValue(autoritesaudit.getDatedebutprevu());
			txtdatefinprevu.setValue(autoritesaudit.getDatefinprevu());
			txtdatedebutreel.setValue(autoritesaudit.getDatedebutreel());
			txtdatefinreel.setValue(autoritesaudit.getDatefinreel());
			
		}
			
	}
	
	
	private boolean checkFieldConstraints() {

		try {
			
			if (txtdatedebutprevu.getValue() == null) {

				errorComponent = txtdatedebutprevu;
				errorMsg = Labels.getLabel("kermel.common.form.datedebutprevu") + ": " + Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);

			}
			
			
			if (txtdatefinprevu.getValue() == null) {

				errorComponent = txtdatefinprevu;
				errorMsg = Labels.getLabel("kermel.common.form.datefinprevu") + ": " + Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);

			}
			
		


			/* Controle datecourier inferieure � daterecours */

//			if (datecourier.getValue() != null) {
//				
//				date = daterecours.getValue();
//				if ((datecourier.getValue()).after((date))) {
//				errorComponent = datecourier;
//					errorMsg = Labels.getLabel("kermel.contentieux.Controledate")+": "+ UtilVue.getInstance().formateLaDate(date);
//					lbStatusBar.setStyle(ERROR_MSG_STYLE);
//					lbStatusBar.setValue(errorMsg);
//					throw new WrongValueException(errorComponent, errorMsg);				}
//			}
			return true;

		} catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString() + " [checkFieldConstraints]";
			errorComponent = null;
			return false;

		}

	}
	
	public void onOK() {
		if (checkFieldConstraints()) {
			autoritesaudit.setDatedebutprevu(txtdatedebutprevu.getValue());
			autoritesaudit.setDatefinprevu(txtdatefinprevu.getValue());
			autoritesaudit.setDatedebutreel(txtdatedebutreel.getValue());
			autoritesaudit.setDatefinreel(txtdatefinreel.getValue());
	
	
			BeanLocator.defaultLookup(AuditsPrestatairesSession.class).update(autoritesaudit);
		
			Executions.getCurrent().setAttribute("codeautoritesaudit",idpaac);
			session.setAttribute("suiviaudit","programmations");    
			 session.setAttribute("ongletsuiviaudit", "autoritesaudits");
		    loadApplicationState("suivi_audit");
		}
	}
	
	public void onClick$menuFermer() {
		loadApplicationState("suivi_audit");
		//detach();
	}
	
	
	
}