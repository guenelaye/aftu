package sn.ssi.kermel.web.audits.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.audit.ejb.AuditSession;
import sn.ssi.kermel.be.audit.ejb.PieceJointeAuditsSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygCourrierAudits;
import sn.ssi.kermel.be.entity.SygPieceJointeAudits;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;
import sn.ssi.kermel.web.referentiel.controllers.DownloaDocsController;




@SuppressWarnings("serial")
public class ListPiecejointeController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox list;
	private Paging pgpiece;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private Date sygmdate;
	private Datebox date;
	private Listheader lshdatecourrier,lshdatereception;
	private Textbox txtobjet,txtdenonciation,txtfichier;
	private Intbox txttraiter,txtpublier,txtpoubelle;
	private String extension,images;
	Long code;
	private Menuitem ADD_COURRIER, MOD_COURRIER, SUPP_COURRIER;
	//private KermelSousMenu monSousMenu;
	Session session = getHttpSession();
	SygCourrierAudits CourrierAudits=new SygCourrierAudits();
	private final String cheminDossier = UIConstants.PATH_DENOCIATION;
	private SygAudit audit =new SygAudit();

	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
//		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
//		monSousMenu.setFea_code(UIConstants.REF_COURRIER);
//		monSousMenu.afterCompose();
		Components.wireFellows(this, this); // mais le wireFellows precedent
		Components.wireFellows(this, this);
//		if (ADD_COURRIER != null)
//			ADD_COURRIER.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
//		if (MOD_COURRIER != null)
//			MOD_COURRIER.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
//		if (SUPP_COURRIER != null)
//			SUPP_COURRIER.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
//		if (WSUIVI_DENONCIATIONC != null)
//			WSUIVI_DENONCIATIONC.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DETAILS);
//		if (WCLASSER_SANS_SUITE_DENONCIATIONC != null)
//			WCLASSER_SANS_SUITE_DENONCIATIONC.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_CLASSER);
//		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_CLASSER, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
//		lshdatecourrier.setSortAscending(new FieldComparator("datecourrier", false));
//		lshdatecourrier.setSortDescending(new FieldComparator("datecourrier", true));
//		
//		lshdatereception.setSortAscending(new FieldComparator("datereception", false));
//		lshdatereception.setSortDescending(new FieldComparator("datereception", true));
		
		list.setItemRenderer(this);

		pgpiece.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pgpiece.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		
	}

	public void onCreate(CreateEvent event) {
		code = (Long) session.getAttribute("numAudit");
		audit = BeanLocator.defaultLookup(AuditSession.class) .findById(code);
		
	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 List<SygPieceJointeAudits> PieceJointeAudits = BeanLocator.defaultLookup(PieceJointeAuditsSession.class).find(pgpiece.getActivePage()*byPage,byPage, null,null,audit);
			 SimpleListModel listModel = new SimpleListModel(PieceJointeAudits);
			list.setModel(listModel);
			pgpiece.setTotalSize(BeanLocator.defaultLookup(PieceJointeAuditsSession.class).count(null,null,audit));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/audits/formpiercej.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(PieceJointeFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/audits/formpiercej.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(PieceJointeFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(PieceJointeFormController.PARAM_WIDOW_CODE, list
					.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				
				HashMap<String, String> display = new HashMap<String, String>(); // permet
				// de
				// fixer
				// les
				// dimenisons
				// du
				// popup
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// de
				// passer
				// des
				// parametres
				// au
				// popup
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < list.getSelectedCount(); i++) {
					Long codes = (Long)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					System.out.println(codes);
				BeanLocator.defaultLookup(PieceJointeAuditsSession.class).delete(codes);
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	
//			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DETAILS)){
//				if (list.getSelectedItem()==null) {
//					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
//				}
//				session.setAttribute("numdenonimation",list.getSelectedItem().getValue());
//				loadApplicationState("suivi_denonimationcourrier");
//			}
//			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLASSER)){
//				if (list.getSelectedItem()==null) {
//					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
//				}
//				 for (int i = 0; i < list.getSelectedCount(); i++) {
//					 Long code=(Long) ((Listitem) list.getSelectedItems().toArray()[i]).getValue();
//					 denonciation= BeanLocator.defaultLookup(DenonciationSession.class).findById(code);
//			    	   denonciation.setPoubelle(0);
//			      	   BeanLocator.defaultLookup(DenonciationSession.class).update(denonciation);
//			       }
//				 session.setAttribute("LibelleTab","COURRIER");
//				 loadApplicationState("denonciations");
//			}
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) {
				Listcell button = (Listcell) event.getTarget();
				String nomFichier = (String)  button.getAttribute("fichier");
				String uri = null;

				HashMap<String, String> display = null;
				HashMap<String, Object> data = null;

				uri = "/referentiel/proformation/suivie/doc.zul";

				data = new HashMap<String, Object>();
				display = new HashMap<String, String>();

				
	 
				data.put(DownloaDocsController.NOM_FICHIER, nomFichier);

				display.put(DSP_TITLE, Labels
						.getLabel("ecoagris.titre.for.fichier"));
				display.put(DSP_WIDTH, "90%");
				display.put(DSP_HEIGHT, "600px");

				showPopupWindow(uri, data, display);

			}
			
			
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygPieceJointeAudits PieceJointeAudits = (SygPieceJointeAudits) data;
		item.setValue(PieceJointeAudits.getId());

		Listcell celldate = new Listcell("");
		if(PieceJointeAudits.getDate()!=null)
		celldate.setLabel(UtilVue.getInstance().formateLaDate2(PieceJointeAudits.getDate()));
		celldate.setParent(item);
		
		Listcell cellFichier = new Listcell("");
		if (PieceJointeAudits.getFichier() != null) {
			

			cellFichier.setImage("/images/PaperClip-16x16.png");
			cellFichier.setAttribute(UIConstants.TODO, "VIEW");
			cellFichier.addEventListener(Events.ON_CLICK, this);
			cellFichier.setAttribute("fichier", PieceJointeAudits.getFichier());
			
		} 
		cellFichier.addEventListener(Events.ON_CLICK, ListPiecejointeController.this);
		cellFichier.setParent(item);
		 
	}
	
	public void onOK$date() {
		onClick$btnRechercher();
	}
	
	public void onClick$btnRechercher() {
		if (date.getValue() == null) {
			sygmdate = null;
		} else {
			sygmdate  = date.getValue();
		}	

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
		 
	}
}