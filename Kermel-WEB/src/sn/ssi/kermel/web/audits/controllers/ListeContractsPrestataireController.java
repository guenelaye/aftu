package sn.ssi.kermel.web.audits.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.audit.ejb.AuditSession;
import sn.ssi.kermel.be.audit.ejb.AuditsPrestatairesSession;
import sn.ssi.kermel.be.audit.ejb.ContractsPrestatairesSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygAuditsPrestataires;
import sn.ssi.kermel.be.entity.SygContratsPrestataires;
import sn.ssi.kermel.be.entity.SygPrestataire;
import sn.ssi.kermel.be.referentiel.ejb.PrestataireSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;



@SuppressWarnings("serial")
public class ListeContractsPrestataireController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {
	
	
	
	private Listbox list;
	private Div step0,step1,step2;
	private Paging pgContract;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private Date sygmdatedebut,sygmdatefin;
	private Listheader lshnumcontract,lshdatedebutcontract,lshdatefincontract;
	private Datebox txtdatedebutcontract,txtdatefincontract,datebxsignature;
	private Textbox txtnumcontract,txtRechercherPrestataire;
	private Label lbStatusBar,lblprestaire;
	Long code;
	private SygContratsPrestataires contract =new SygContratsPrestataires();
	//private SygAuditsPrestataires auditspres;
	private String identifiant = null,page= null,mode;
	private SygPrestataire prestataire;
	private Listbox ListPrestataire;
	private Paging pgPrestataire;
	private Bandbox bandPrestataire;
	private String errorMsg;
	private Component errorComponent;
	private Long idcontrat=null;
	Session session = getHttpSession();
	private SygAudit audit =new SygAudit();
	
	private Label lblNbrePrestaireRestant;
	private Integer nbreprestaire, nbreprestairerestant,nbrprestaireaauditer;
	private Menuitem menuAjouter;
	
	private SygContratsPrestataires contratprestaire = new SygContratsPrestataires();
	private Listbox listAutoritepres;
	private Paging pgAutoritepres;
	private SygAuditsPrestataires  autoritesaudit = new SygAuditsPrestataires();
	private static final String CONFIRMSUPSTEP2 = "CONFIRMSUPSTEP2";
	private String ongletsuiviaudit;
	//private SygPrestatairesAudits presaudit =new SygPrestatairesAudits();
	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		/*
		 * On indique qula fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		code = (Long) session.getAttribute("numAudit");
		audit = BeanLocator.defaultLookup(AuditSession.class) .findById(code);
		ongletsuiviaudit = (String) session.getAttribute("ongletsuiviaudit");
		if((ongletsuiviaudit!=null)){
		  if( (ongletsuiviaudit.equals("ajoutautoriteprest")) || (ongletsuiviaudit.equals("supautoritepres"))){
			  
				idcontrat=(Long) session.getAttribute("codecontrat");   
				if(idcontrat!=null)
				{
					contratprestaire= BeanLocator.defaultLookup(ContractsPrestatairesSession.class).findById(idcontrat);
					
					   lblprestaire.setValue(contratprestaire.getPrestataire().getRaisonsociale());	
					  
					    step1.setVisible(false);
						step0.setVisible(false);
						step2.setVisible(true);
						 Events.postEvent(ApplicationEvents.ON_AUTORITESPRES, this, null);
				}
				else
				{
					 step1.setVisible(false);
						step0.setVisible(true);
						step2.setVisible(false);
				}
				
		  }else  if( ongletsuiviaudit.equals("actioncontrat") ){
			   step1.setVisible(false);
				step0.setVisible(true);
				step2.setVisible(false);
		  }
		  }
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
		lshnumcontract.setSortAscending(new FieldComparator("numcontract", false));
		lshnumcontract.setSortDescending(new FieldComparator("numcontract", true));
	
        lshdatedebutcontract.setSortAscending(new FieldComparator("datedebutcontract", false));
        lshdatedebutcontract.setSortDescending(new FieldComparator("datedebutcontract", true));
	
        lshdatefincontract.setSortAscending(new FieldComparator("datefincontract", false));
        lshdatefincontract.setSortDescending(new FieldComparator("datefincontract", true));

        
        addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		list.setItemRenderer(this);
    	pgContract.setPageSize(byPage);
		pgContract.addForward("onPaging", this,ApplicationEvents.ON_MODEL_CHANGE);
	
		
		 addEventListener(ApplicationEvents.ON_PRESTATAIRE, this);
		 ListPrestataire.setItemRenderer(new PrestatairesRenderer());
	    	pgPrestataire.setPageSize(byPage);
			pgPrestataire.addForward("onPaging", this,ApplicationEvents.ON_PRESTATAIRE);
			Events.postEvent(ApplicationEvents.ON_PRESTATAIRE, this, null);
			
			//AC prestaire
			addEventListener(ApplicationEvents.ON_AUTORITESPRES, this);
			listAutoritepres.setItemRenderer(new AutoritePresRenderer());
			pgAutoritepres.setPageSize(byPage);
			pgAutoritepres.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTORITESPRES);
	}

	
	public void onCreate(CreateEvent event) {
		
		
		
		
		
		nbreprestaire=audit.getNombreprestataire();
		
		 List<SygContratsPrestataires> contractpres = BeanLocator.defaultLookup(ContractsPrestatairesSession .class).find(0,-1,null,sygmdatedebut,sygmdatefin,audit,null);
		nbrprestaireaauditer=contractpres.size();
		nbreprestairerestant=nbreprestaire - nbrprestaireaauditer;
		lblNbrePrestaireRestant.setValue(nbreprestairerestant.toString());
		
		if(nbreprestairerestant==0){
			menuAjouter.setDisabled(true);
		}
		
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

	}
	public class PrestatairesRenderer implements ListitemRenderer{

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygPrestataire prestataire = (SygPrestataire) data;
			item.setValue(prestataire);
			Listcell cellidentifiant = new Listcell(prestataire.getIdentifiant());
			cellidentifiant.setParent(item);
			Listcell cellraisonsocial = new Listcell(prestataire.getRaisonsociale());
			cellraisonsocial.setParent(item);

		}
	}
	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 List<SygContratsPrestataires> contract = BeanLocator.defaultLookup(ContractsPrestatairesSession .class).find(pgContract.getActivePage()*byPage,byPage,null,sygmdatedebut,sygmdatefin,audit,null);
			 SimpleListModel listModel = new SimpleListModel(contract);
			list.setModel(listModel);
			pgContract.setTotalSize(BeanLocator.defaultLookup(
					ContractsPrestatairesSession .class).count(null,sygmdatedebut,sygmdatefin,audit,null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_PRESTATAIRE)) {
			List<SygPrestataire> prestataire = BeanLocator.defaultLookup(PrestataireSession.class).ListesPrestataire(pgPrestataire.getActivePage()*byPage, byPage, audit.getIdaudit());
			ListPrestataire.setModel(new SimpleListModel(prestataire));
			pgPrestataire.setTotalSize(BeanLocator.defaultLookup(PrestataireSession.class).ListesPrestataire(audit.getIdaudit()).size());

		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			mode=UIConstants.MODE_NEW;
			step1.setVisible(true);
			step0.setVisible(false);
			
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				// de
				// fixer
				// les
				// dimenisons
				// du
				// popup
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// de
				// passer
				// des
				// parametres
				// au
				// popup
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				 String confirmer = (String) ((HashMap<String, Object>) event.getData()).get(CONFIRMSUPSTEP2);
				 
				 if (confirmer != null && confirmer.equalsIgnoreCase("Confirmer")) 
				 {
					 for (int i = 0; i < listAutoritepres.getSelectedCount(); i++) {
						 autoritesaudit =( (SygAuditsPrestataires)((Listitem) listAutoritepres.getSelectedItems().toArray()[i]).getValue());
						 autoritesaudit.setContprest(null);
						 BeanLocator.defaultLookup(AuditsPrestatairesSession.class).update(autoritesaudit);
						 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null); 
					 }
					  session.setAttribute("ongletsuiviaudit", "supautoritepres");
					 
				  }else{
					 
				  for (int i = 0; i < list.getSelectedCount(); i++) {
					Long codes =( (SygContratsPrestataires)((Listitem) list.getSelectedItems().toArray()[i]).getValue()).getIdcontract();
					System.out.println(codes);
				  BeanLocator.defaultLookup(ContractsPrestatairesSession .class).delete(codes);
				  }
				  session.setAttribute("ongletsuiviaudit", "actioncontrat");
				}
				 
				 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
				   loadApplicationState("suivi_audit");
			}
		
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTORITESPRES)) {
				if (event.getData() != null) {
					activePage = Integer.parseInt((String) event.getData());
					byPage = -1;
					pgAutoritepres.setPageSize(1000);
				} else {
					byPage = UIConstants.DSP_LIST_BY_PAGES;
					activePage = pgAutoritepres.getActivePage() * byPage;
					pgAutoritepres.setPageSize(byPage);
				}
				List<SygAuditsPrestataires> autoritepres = BeanLocator.defaultLookup(AuditsPrestatairesSession.class).findauditpresaut(activePage, byPage,audit.getIdaudit(),null,contratprestaire.getIdcontract(),null);
				listAutoritepres.setModel(new SimpleListModel(autoritepres));
				pgAutoritepres.setTotalSize(BeanLocator.defaultLookup(AuditsPrestatairesSession.class).countauditpresaut(audit.getIdaudit(),null,contratprestaire.getIdcontract(),null));
			}
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
		public void render(final Listitem item, final Object data, int index) throws Exception {
			SygContratsPrestataires contract = (SygContratsPrestataires) data;
			item.setValue(contract);

			
			Listcell cellnumcontract = new Listcell(contract.getNumcontract());
			cellnumcontract.setParent(item);
			Listcell cellraisonsocial = new Listcell(contract.getPrestataire().getRaisonsociale());
			cellraisonsocial.setParent(item);
			Listcell cellDatedebutcontract = new Listcell("");
			if(contract.getDatedebutcontract()!= null)
				cellDatedebutcontract.setLabel(UtilVue.getInstance().formateLaDate(contract.getDatedebutcontract()));
			cellDatedebutcontract.setParent(item);
			
			Listcell cellDatefincontract = new Listcell(UtilVue.getInstance().formateLaDate(contract.getDatefincontract()));
			if(contract.getDatefincontract()!= null)
				cellDatefincontract.setLabel(UtilVue.getInstance().formateLaDate(contract.getDatefincontract()));
			cellDatefincontract.setParent(item);	
			
			
			Listcell cellDatesignature = new Listcell(UtilVue.getInstance().formateLaDate(contract.getDatesignature()));
			if(contract.getDatesignature()!= null)
				cellDatefincontract.setLabel(UtilVue.getInstance().formateLaDate(contract.getDatesignature()));
			cellDatesignature.setParent(item);	

			
		}
	
	public class AutoritePresRenderer implements ListitemRenderer{

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygAuditsPrestataires  autoritesaudit = (SygAuditsPrestataires) data;
			item.setValue(autoritesaudit);
			
			Listcell cellLibelle = new Listcell(autoritesaudit.getAutorite().getDenomination());
			cellLibelle.setParent(item);

		}
	}
		public void onSelect$ListPrestataire() {
			prestataire=(SygPrestataire) ListPrestataire.getSelectedItem().getValue();
			//bandPrestataire.setValue(prestataire.getIdentifiant()+ " "+prestataire.getRaisonsociale());
			bandPrestataire.setValue(prestataire.getRaisonsociale());
			bandPrestataire.close();
		}
	
		public void onClick$btnRechercherPrestataire() {
			if (!txtRechercherPrestataire.getValue().equals("") && !txtRechercherPrestataire.getValue().equals("Prestataire")) {
				identifiant = txtRechercherPrestataire.getValue();
			} else {
				identifiant = null;
				page="0";
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}

		public void onClick$menuValider() {
			if (checkFieldConstraints()) {
				contract.setNumcontract(txtnumcontract.getValue());
				contract.setDatedebutcontract(txtdatedebutcontract.getValue());
				contract.setDatefincontract(txtdatefincontract.getValue());
				contract.setDatesignature(datebxsignature.getValue());
				
				if (ListPrestataire.getSelectedItem() == null) {
					errorMsg = "Veuillez selectionner un Prestataire!";
					errorComponent = bandPrestataire;
					throw new WrongValueException(errorComponent, errorMsg);
		
				}
				
				contract.setPrestataire(prestataire);
				contract.setAudit(audit);
			if (idcontrat==null) {
				BeanLocator.defaultLookup(ContractsPrestatairesSession.class).save(contract);
	
			} 
			else  {
				BeanLocator.defaultLookup(ContractsPrestatairesSession.class).update(contract);
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			 step1.setVisible(false);
			 step0.setVisible(true);
			 session.setAttribute("ongletsuiviaudit", "actioncontrat");
			 loadApplicationState("suivi_audit");
			
		}
	
		}
	 public void onClick$menuModifier() {
		 if (list.getSelectedItem() == null)
				
				throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				contract=(SygContratsPrestataires) list.getSelectedItem().getValue();
				txtnumcontract.setValue(contract.getNumcontract());
				txtdatedebutcontract.setValue(contract.getDatedebutcontract());
				txtdatefincontract.setValue(contract.getDatefincontract());
				datebxsignature.setValue(contract.getDatesignature());
				prestataire=contract.getPrestataire();
				bandPrestataire.setValue(prestataire.getIdentifiant()+""+prestataire.getRaisonsociale());
				idcontrat=contract.getIdcontract();
				 step1.setVisible(true);
				 step0.setVisible(false);
	 }
		public void onClick$menuAutorite() {
	 if ( list.getSelectedItem() == null)
		 throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
	 
	     contratprestaire=(SygContratsPrestataires) list.getSelectedItem().getValue();
	     lblprestaire.setValue(contratprestaire.getPrestataire().getRaisonsociale());	
	  
	     step1.setVisible(false);
		 step0.setVisible(false);
		 step2.setVisible(true);
		 
		 Events.postEvent(ApplicationEvents.ON_AUTORITESPRES, this, null);
	  

		}

		public void onClick$menuFermer() {
				    step1.setVisible(false);
					step0.setVisible(true);
				     }		
			 
			 private boolean checkFieldConstraints()
				{
					try {
				
						 if(bandPrestataire.getValue().equals(""))
					       {
			              errorComponent = bandPrestataire;
			              errorMsg = Labels.getLabel("kermel.common.form.prestation")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
							lbStatusBar.setStyle("color:red");
							lbStatusBar.setValue(errorMsg);
							throw new WrongValueException (errorComponent, errorMsg);
					        }
						
						
						if (txtnumcontract.getValue() == null || txtnumcontract.getValue().trim().equalsIgnoreCase("")) {
							errorComponent = txtnumcontract;
							errorMsg = Labels.getLabel("kermel.audit.prestaire.referencecontract") + ": " + Labels.getLabel("kermel.erreur.champobligatoire");
							lbStatusBar.setValue(errorMsg);
							lbStatusBar.setStyle("color:red");
							return false;
						}

					   if (txtdatedebutcontract.getValue() == null) {
							errorComponent = txtdatedebutcontract;
							errorMsg = Labels.getLabel("kermel.audit.prestaire.datedebutprestion") + ": " + Labels.getLabel("kermel.erreur.champobligatoire");
							lbStatusBar.setValue(errorMsg);
							lbStatusBar.setStyle("color:red");
							return false;
						}
						
							if (txtdatefincontract.getValue() == null) {
							errorComponent = txtdatefincontract;
							errorMsg = Labels.getLabel("kermel.audit.prestaire.datefinprestion") + ": " + Labels.getLabel("kermel.erreur.champobligatoire");
							lbStatusBar.setValue(errorMsg);
							lbStatusBar.setStyle("color:red");
							return false;
						}
							
							
							if (datebxsignature.getValue() == null) {
								errorComponent = datebxsignature;
								errorMsg = Labels.getLabel("kermel.audit.prestaire.datesignature") + ": " + Labels.getLabel("kermel.erreur.champobligatoire");
								lbStatusBar.setValue(errorMsg);
								lbStatusBar.setStyle("color:red");
								return false;
							}
							
						
						return true;
					} catch (Exception e) {

						lbStatusBar.setValue(errorMsg);
						// lbStatusBar.setStyle(UIConstants.STYLE_STATUSBAR_ERROR);
						e.printStackTrace();
						throw new WrongValueException(errorComponent, errorMsg);
					}
						
				}
				
				public void onBlur$txtdatedebutcontract() {
				
				Date datedebutcontract =  txtdatedebutcontract.getValue();
				Date datefincontract = (Date)txtdatefincontract.getValue();
				if (txtdatefincontract.getValue() != null) {
					if (datefincontract.compareTo(datedebutcontract ) < 0) {
						errorMsg = "La date fin doit etre inferieure la date de debut . Veuillez corriger cette incoh�rence.";
						errorComponent = txtdatefincontract;
						throw new WrongValueException(errorComponent, errorMsg);
					}
				}
				
			  }
			     public void onBlur$txtDateFincontract()
			     { 
			    	 Date datefincontract =  txtdatefincontract.getValue();
			    	 Date datedebutcontract  = (Date) txtdatedebutcontract.getValue();
			     if (txtdatedebutcontract.getValue() != null) {
			      if (datedebutcontract.compareTo(datefincontract ) > 0) {
				  errorMsg = "La Date Debut doit etre sup�rieur � la date de fin. Veuillez corriger cette incoh�rence.";
				errorComponent = txtdatedebutcontract;
				throw new WrongValueException(errorComponent, errorMsg);
				
			      }
			    }
			   }
			     
/////////////**********************step2*******************************************************************////
			     
			     public void onClick$menuAjouterStep2(){

			 	    final String uri = "/audits/prestatairesautorites.zul";
			 	    //cas o� on quitte le view selectionne le ocntrat et click sur autorite
			 	   if((ongletsuiviaudit==null)){
			 		  contratprestaire=(SygContratsPrestataires) list.getSelectedItem().getValue();
			 	   }
			 	     session.setAttribute("codecontrat", contratprestaire.getIdcontract()); 
			    	 		final HashMap<String, String> display = new HashMap<String, String>();
			    	 		display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.titre"));
			    	 		display.put(DSP_HEIGHT,"535px");
			    	 		display.put(DSP_WIDTH, "80%");
			    	 
			    	 		final HashMap<String, Object> data = new HashMap<String, Object>();
			    	 		data.put(AutoritesController.PARAM_WIDOW_AUT,	contratprestaire);
			    	 		
			    	 
			    	 		showPopupWindow(uri, data, display);
						

						}
			     
			    
				 public void onClick$menuFermerStep2(){

					 step1.setVisible(false);
					 step0.setVisible(true);
					 step2.setVisible(false);
						
						}
			     
			     public void onClick$menuSupprimerStep2(){
			    	 
			    	 if ( listAutoritepres.getSelectedItem() == null)
			    		 throw new WrongValueException(listAutoritepres, Labels.getLabel("kermel.error.select.item"));
			    	 
			    	    autoritesaudit=(SygAuditsPrestataires) listAutoritepres.getSelectedItem().getValue();
			    	 
			    	    HashMap<String, String> display = new HashMap<String, String>(); 
					
						display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
						display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
						display.put(MessageBoxController.DSP_HEIGHT, "250px");
						display.put(MessageBoxController.DSP_WIDTH, "47%");

						HashMap<String, Object> map = new HashMap<String, Object>();
					
						map.put(CONFIRMSUPSTEP2, "Confirmer");
						showMessageBox(display, map);
			    	 
			     }
}