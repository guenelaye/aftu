package sn.ssi.kermel.web.audits.controllers;


import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.audit.ejb.AuditSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.workflow.ejb.WorkflowSession;
import sn.ssi.kermel.be.workflow.entity.SysState;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;




@SuppressWarnings("serial")
public class AuditFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,codelocalite,annee;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtlibelleaudit;
	private SygAudit audit =new SygAudit();
	private SimpleListModel lst;
	private Datebox txtdatestatut;
	private Paging pgPiece,pg;
	private final String ON_LOCALITE = "onLocalite";
	private ArrayList<String> listValeursAnnees;
	Long code;
    private Paging pgAutorite;
	private Listbox lstAutorite,list;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE,activePage;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private Label lbStatusBar;
	private String errorMsg;
	private Component errorComponent;
	private Combobox cbannee;
	private SysState state=new SysState();
	private Intbox intnbrautorite,intnbrprestaire;

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		state=BeanLocator.defaultLookup(WorkflowSession.class).findToState(UIConstants.ARR_INIAUDIT);
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WIDOW_CODE);
		   audit = BeanLocator.defaultLookup(AuditSession.class)
					.findById(code);
			txtlibelleaudit.setValue(audit.getLibelleaudit());
			txtdatestatut.setValue(audit.getDatestatut());
			intnbrautorite.setValue(audit.getNombreautorite());
			intnbrprestaire.setValue(audit.getNombreprestataire());
			cbannee.setValue(audit.getGestion().toString());	
		}
		  annee = UtilVue.getInstance().anneecourant(new Date());
			listValeursAnnees = new ArrayList<String>();
			for (int i = 2006; i < Integer.parseInt(annee)+1 ; i++) {
				listValeursAnnees.add(i+"");
			}
			cbannee.setModel(new SimpleListModel(listValeursAnnees));
	}
			
	public void onOK() {
		if (checkFieldConstraints()) {
		
		
		audit.setLibelleaudit(txtlibelleaudit.getValue());
		audit.setStatut("Saisi");
		audit.setDatestatut(txtdatestatut.getValue());
		audit.setNombreautorite(intnbrautorite.getValue());
		audit.setNombreprestataire(intnbrprestaire.getValue());
		audit.setState(state);
		if(cbannee.getSelectedItem()!=null)
		  audit.setGestion(Integer.parseInt (cbannee.getSelectedItem().getValue().toString()));
		
		
		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			BeanLocator.defaultLookup(AuditSession.class).save(audit);

		} 
		else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			BeanLocator.defaultLookup(AuditSession.class).update(audit);
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();
	}else{
		throw new WrongValueException(errorComponent, errorMsg);
	}

	}
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		
		
		lbStatusBar.setValue(Labels.getLabel("kermel.common.form.champs.obligatoire"));
	
	}
	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		
	}
	
	public class TypeAutoritesRenderer implements ListitemRenderer
	{
			
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			
		}
		
		
		public class AutoritesRenderer implements ListitemRenderer{
			@Override
			public void render(Listitem arg0, Object arg1, int index) throws Exception {
				// TODO Auto-generated method stub
				
			}
	 
		}
	}
	
	private boolean checkFieldConstraints()
	{
		try {
	
			
			if (txtlibelleaudit.getValue() == null || txtlibelleaudit.getValue().trim().equalsIgnoreCase("")) {
				errorComponent = txtlibelleaudit;
				errorMsg = "Veuillez renseigner le champ Libell�";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}
			
			 if (txtdatestatut.getValue() == null) {
					errorComponent = txtdatestatut;
					errorMsg = "Veuillez renseigner le champ Date de Debut";
					lbStatusBar.setValue(errorMsg);
					lbStatusBar.setStyle("color:red");
					return false;
				}

			if (cbannee.getSelectedItem() == null ) {
				errorComponent = cbannee;
				errorMsg = "Veuillez renseigner le champ de gestion svp";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}
			
			 if (intnbrautorite.getValue() == null) {
					errorComponent = intnbrautorite;
					errorMsg = Labels.getLabel("kermel.audit.form.nbreautorite")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
					lbStatusBar.setValue(errorMsg);
					lbStatusBar.setStyle("color:red");
					return false;
				}
			 
			 if (intnbrprestaire.getValue() == null) {
					errorComponent = intnbrprestaire;
					errorMsg = Labels.getLabel("kermel.audit.form.nbreprestataire")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
					lbStatusBar.setValue(errorMsg);
					lbStatusBar.setStyle("color:red");
					return false;
				}
		  	
			return true;
		} catch (Exception e) {

			lbStatusBar.setValue(errorMsg);
			// lbStatusBar.setStyle(UIConstants.STYLE_STATUSBAR_ERROR);
			e.printStackTrace();
			throw new WrongValueException(errorComponent, errorMsg);
		}
			
	}

	@Override
	public void onEvent(Event arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
