package sn.ssi.kermel.web.audits.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.audit.ejb.AuditSession;
import sn.ssi.kermel.be.audit.ejb.PrestatairesAuditsSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPrestataire;
import sn.ssi.kermel.be.entity.SygPrestatairesAudits;
import sn.ssi.kermel.be.referentiel.ejb.PrestataireSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;



@SuppressWarnings("serial")
public class AuditsPrestataireController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox list;
	private Div step0,step1;
		private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private Date sygmdatedebut,sygmdatefin;
	private Listheader lshnumcontract,lshdatedebutcontract,lshdatefincontract,lshninea, lshraisonsociale;
	private Datebox datedebutcontract,datefincontract,txtdatedebutcontract,txtdatefincontract;
	private Textbox txtnumcontract,txtRechercherPrestataire;
	private Label lbStatusBar;
	Long code;
	private SygPrestatairesAudits auditsprest =new SygPrestatairesAudits();
	private String Numeroprest = null,page= null,mode;
	private SygPrestataire prestataire;
	private SygAutoriteContractante autorite;
	private SygPrestataire prestataires;
	private SygAudit audits;	
	private Listbox ListPrestataire;
	private Paging pgPrestataire,pgPrestataires;
	private String errorMsg;
	private Component errorComponent;
	private Long idcontrat=null;
	Session session = getHttpSession();
	//SygContratsPrestataires contprest ;
	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		/*
		 * On indique qula fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
			
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
        
        addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		list.setItemRenderer(this);
		pgPrestataire.setPageSize(byPage);
		pgPrestataire.addForward("onPaging", this,ApplicationEvents.ON_MODEL_CHANGE);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
		 addEventListener(ApplicationEvents.ON_PRESTATAIRE, this);
		 ListPrestataire.setItemRenderer(new PrestatairesRenderer());
		 pgPrestataires.setPageSize(byPage);
		 pgPrestataires.addForward("onPaging", this,ApplicationEvents.ON_PRESTATAIRE);
			
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		code = (Long) session.getAttribute("numAudit");
		audits = BeanLocator.defaultLookup(AuditSession.class) .findById(code);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

	}
	public class PrestatairesRenderer implements ListitemRenderer{

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygPrestataire prestataire = (SygPrestataire) data;
			item.setValue(prestataire);
			Listcell cellidentifiant = new Listcell(prestataire.getIdentifiant());
			cellidentifiant.setParent(item);
			Listcell cellraisonsocial = new Listcell(prestataire.getRaisonsociale());
			cellraisonsocial.setParent(item);

		}
	}
	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 List<SygPrestatairesAudits> auditsprest = BeanLocator.defaultLookup(PrestatairesAuditsSession .class).find(pgPrestataire.getActivePage()*byPage,byPage,null,null,audits,null);
			 SimpleListModel listModel = new SimpleListModel(auditsprest);
			list.setModel(listModel);
			pgPrestataire.setTotalSize(BeanLocator.defaultLookup(
					PrestatairesAuditsSession .class).count(null, null,audits,null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_PRESTATAIRE)) {
			List<SygPrestataire> prestataire = BeanLocator.defaultLookup(PrestataireSession.class).ListesPrestataires(pgPrestataires.getActivePage()*byPage, byPage, audits.getIdaudit());
			ListPrestataire.setModel(new SimpleListModel(prestataire));
			pgPrestataires.setTotalSize(BeanLocator.defaultLookup(PrestataireSession.class).ListesPrestataires(audits.getIdaudit()).size());

		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			Events.postEvent(ApplicationEvents.ON_PRESTATAIRE, this, null);
			step0.setVisible(false);
			step1.setVisible(true);
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				// de
				// fixer
				// les
				// dimenisons
				// du
				// popup
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// de
				// passer
				// des
				// parametres
				// au
				// popup
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < list.getSelectedCount(); i++) {
					Long codes = (Long)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					System.out.println(codes);
				BeanLocator.defaultLookup(PrestatairesAuditsSession .class).delete(codes);
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
		public void render(final Listitem item, final Object data, int index) throws Exception {
		SygPrestatairesAudits auditsprest = (SygPrestatairesAudits) data;
			item.setValue(auditsprest.getIdpaac());

			Listcell cellidentifiant = new Listcell(auditsprest.getPrestataire().getIdentifiant());
			cellidentifiant.setParent(item);
			
			Listcell cellraisonsocial = new Listcell(auditsprest.getPrestataire().getRaisonsociale());
			cellraisonsocial.setParent(item);
		}
	
		public void onClick$btnRechercherPrestataire() {
			if (!txtRechercherPrestataire.getValue().equals("") && !txtRechercherPrestataire.getValue().equals("Prestataire")) {
				Numeroprest = txtRechercherPrestataire.getValue();
			} else {
				Numeroprest = null;
				page="0";
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}

			 public void onClick$menuFermer() {
				    step0.setVisible(true);
					step1.setVisible(false);
				     }		
 
			 public void onClick$menuValider()
				{
			       if (ListPrestataire.getSelectedItem() == null)
						throw new WrongValueException(ListPrestataire, Labels.getLabel("kermel.error.select.item"));
			       
			       for (int i = 0; i < ListPrestataire.getSelectedCount(); i++) {
			    	   SygPrestatairesAudits prestataireaudit=new SygPrestatairesAudits();
			    	   prestataire=(SygPrestataire) ((Listitem) ListPrestataire.getSelectedItems().toArray()[i]).getValue();
			    	   prestataireaudit.setAudit(audits);
			    	   prestataireaudit.setPrestataire(prestataire);
			    	   BeanLocator.defaultLookup(PrestatairesAuditsSession.class).save(prestataireaudit);
			       }
			       Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
					step0.setVisible(true);
					step1.setVisible(false);
					
				}
}