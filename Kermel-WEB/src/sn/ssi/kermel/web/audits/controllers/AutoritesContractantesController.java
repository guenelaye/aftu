package sn.ssi.kermel.web.audits.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.audit.ejb.AuditSession;
import sn.ssi.kermel.be.audit.ejb.AuditsPrestatairesSession;
import sn.ssi.kermel.be.audit.ejb.PrestatairesAuditsSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygAuditsPrestataires;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPrestatairesAudits;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.referentiel.ejb.TypeAutoriteContractanteSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;



@SuppressWarnings("serial")
public class AutoritesContractantesController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	public static final String PARAM_WIDOW_AUT = "AUT";
	private Listbox list;
	private Div step0,step1,step2,step;
	private Paging pgPagination,pgPrestataire;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";

	private Label lbStatusBar,entete2;
	Long code;
	private Iframe dossier;
	private String mode;
	private SygAutoriteContractante autorite;
	private SygAuditsPrestataires auditspres;
	private SygTypeAutoriteContractante autorite1;
	private SygAuditsPrestataires auditsprest =new SygAuditsPrestataires();
	
	private SygPrestatairesAudits prestataires;
	
	private SygAudit audits;	
	private Listbox ListAutorites,ListPrestataire,lstAutorite;
	private Paging pgAutorites,pgAutorite;
	
	Session session = getHttpSession();
	private Bandbox bandAutorite;
	private Label lblNbreAutoriteRestant,lblAutorite;
	private Integer nbreautorite, nbreautoriterestant,nbrautoriteaauditer;
	private Menuitem menuAjouter;
	
	private SygAuditsPrestataires autoritesaudit =new SygAuditsPrestataires();
	private Tab TAB_VersionProvisoireRapport,TAB_ObservationAutoriteContractante,TAB_VersionDefinitif,TAB_Programmation;
	
	//Recher Ac
	public Textbox txtDenomination;
	private String denomination=null,suiviaudit;
	private Include incProgrammation;
	private Long idpaac;
	
	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		/*
		 * On indique qula fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		code = (Long) session.getAttribute("numAudit");
		audits = BeanLocator.defaultLookup(AuditSession.class) .findById(code);	
		
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

       
        addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);

		  //table SygAuditsPrestataires 
		    addEventListener(ApplicationEvents.ON_AUTORITESAUDITS, this);
			list.setItemRenderer(new AutoritesauditsRenderer());
			pgPagination.setPageSize(byPage);
			pgPagination.addForward("onPaging", this,ApplicationEvents.ON_AUTORITESAUDITS);
		    Events.postEvent(ApplicationEvents.ON_AUTORITESAUDITS, this, null);

		
//		addEventListener(ApplicationEvents.ON_TYPE_AUTORITES, this);
//		lstAutorite.setItemRenderer(new TypeAutoritesRenderer());
//		pgAutorite.setPageSize(byPage);
//		pgAutorite.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_TYPE_AUTORITES);
//		addEventListener(ApplicationEvents.ON_AUTORITES, this);

		

		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	
		lbStatusBar.setValue(Labels.getLabel("kermel.common.form.champs.obligatoire"));
		 
	     addEventListener(ApplicationEvents.ON_PRESTATAIRE, this);
	          ListPrestataire.setItemRenderer(new PrestatairesRenderer ());
			pgPrestataire.setPageSize(byPage);
			pgPrestataire.addForward("onPaging", this,ApplicationEvents.ON_PRESTATAIRE);
			
			
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		 suiviaudit=(String) session.getAttribute("suiviaudit");
		 if(Executions.getCurrent()!=null)
		 {
			 idpaac=(Long) Executions.getCurrent().getAttribute("codeautoritesaudit"); 
			 if(idpaac!=null)
			 {
				   autoritesaudit= BeanLocator.defaultLookup(AuditsPrestatairesSession.class).findById(idpaac); 
			       step.setVisible(true);
			       step0.setVisible(false);
			       step1.setVisible(false);
			       step2.setVisible(false);
			       InfosSuivi();
			 }
	      
		 }
	
		
		
      Map<String, Object> map = (Map<String, Object>) event.getArg();
		auditspres = (SygAuditsPrestataires) map.get(PARAM_WIDOW_AUT);
		
		nbreautorite=audits.getNombreautorite();
		
		List<SygAuditsPrestataires> autoriteaauditer = BeanLocator.defaultLookup(AuditsPrestatairesSession.class).find(0, -1, audits.getIdaudit(),null,null);
		nbrautoriteaauditer=autoriteaauditer.size();
		nbreautoriterestant=nbreautorite - nbrautoriteaauditer;
		lblNbreAutoriteRestant.setValue(nbreautoriterestant.toString());
		
		if(nbreautoriterestant==0){
			menuAjouter.setDisabled(true);
		}
		

	}
       public class TypeAutoritesRenderer implements ListitemRenderer{
		
		
		
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypeAutoriteContractante autorites = (SygTypeAutoriteContractante) data;
			item.setValue(autorites);
			
			Listcell cellLibelle = new Listcell(autorites.getLibelle());
			cellLibelle.setParent(item);
			if (mode!=null && mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				if (autorites.getId().equals(autorite.getType().getId()))
					item.setSelected(true);
					
			}
			
		}
	}

	
	
	public class AutoritesauditsRenderer implements ListitemRenderer{

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygAuditsPrestataires  autoritesaudit = (SygAuditsPrestataires) data;
			item.setValue(autoritesaudit);
			
			Listcell cellLibelle = new Listcell(autoritesaudit.getAutorite().getDenomination());
			cellLibelle.setParent(item);

		}
	}
	
	public class PrestatairesRenderer implements ListitemRenderer{

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygPrestatairesAudits  prestataires = (SygPrestatairesAudits) data;
			item.setValue(prestataires);
			
			Listcell cellAudit= new Listcell(prestataires.getPrestataire().getIdentifiant());
			cellAudit.setParent(item);
			Listcell cellAdresse= new Listcell(prestataires.getPrestataire().getAdresse());
			cellAdresse.setParent(item);
			Listcell cellRaisonsociale= new Listcell(prestataires.getPrestataire().getRaisonsociale());
			cellRaisonsociale.setParent(item);
			Listcell cellPays= new Listcell(prestataires.getPrestataire().getPays().getLibelle());
			cellPays.setParent(item);
			Listcell cellMail= new Listcell(prestataires.getPrestataire().getMail());
			cellMail.setParent(item);
		}
	}
	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		
		

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_TYPE_AUTORITES)) {
			
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgAutorite.setPageSize(1000);
			} else {
				byPage= UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgAutorite.getActivePage() * byPage;
				pgAutorite.setPageSize(byPage);
			}
			
			List<SygTypeAutoriteContractante> autorites = BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).find(activePage, byPage,null,null);
			lstAutorite.setModel(new SimpleListModel(autorites));
			pgAutorite.setTotalSize(BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).count(null,null));
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTORITESAUDITS)) {
			 List<SygAuditsPrestataires> autoritesaudit = BeanLocator.defaultLookup(AuditsPrestatairesSession .class).find(pgPagination.getActivePage()*byPage,byPage,audits.getIdaudit(),null,denomination);
			 SimpleListModel listModel = new SimpleListModel(autoritesaudit);
			list.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup(AuditsPrestatairesSession .class).count(audits.getIdaudit(),null,denomination));
		} 
 
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_PRESTATAIRE)) {
			 List<SygPrestatairesAudits> auditsprest = BeanLocator.defaultLookup(PrestatairesAuditsSession .class).find(pgPrestataire.getActivePage()*byPage,byPage,null,null,audits,null);
			 SimpleListModel listModel = new SimpleListModel(auditsprest);
			 ListPrestataire.setModel(listModel);
			pgPrestataire.setTotalSize(BeanLocator.defaultLookup(
					PrestatairesAuditsSession .class).count(null,null,audits,null));
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);
			//step.setVisible(false);
			step0.setVisible(true);
			step1.setVisible(false);
			step2.setVisible(false);
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				// de
				// fixer
				// les
				// dimenisons
				// du
				// popup
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// de
				// passer
				// des
				// parametres
				// au
				// popup
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < list.getSelectedCount(); i++) {
					Long codes = ((SygAuditsPrestataires)((Listitem) list.getSelectedItems().toArray()[i]).getValue()).getIdpaac();
					System.out.println(codes);
				BeanLocator.defaultLookup(AuditsPrestatairesSession .class).delete(codes);
				}
				Events.postEvent(ApplicationEvents.ON_AUTORITESAUDITS, this, null);
				
				loadApplicationState("suivi_audit");
			}
		       

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	
	@Override
		public void render(final Listitem item, final Object data, int index) throws Exception {
		SygAuditsPrestataires autorites = (SygAuditsPrestataires) data;
			item.setValue(autorites);
			
			Listcell cellLibelle = new Listcell(autorites.getAutorite().getDenomination());
			cellLibelle.setParent(item);
			

		}
	

		public void onClick$menuNext4(){
			if (list.getSelectedItem() == null)
				throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
			
			autorite1=(SygTypeAutoriteContractante) lstAutorite.getSelectedItem().getValue();
			entete2.setValue(autorite1.getLibelle());
			step0.setVisible(false);
			step.setVisible(false);
			step1.setVisible(true);
			Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);

		}
		 public void onClick$menuPrevious(){
				if (lstAutorite.getSelectedItem() == null)
					throw new WrongValueException(lstAutorite, Labels.getLabel("kermel.error.select.item"));
				
				autorite1=(SygTypeAutoriteContractante) lstAutorite.getSelectedItem().getValue();
				    step0.setVisible(true);
				    step1.setVisible(false);
					step.setVisible(false);
					step2.setVisible(false);
				Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);

			}
		 public void onClick$menuNext(){
			 if (ListAutorites.getSelectedItem() == null)
			throw new WrongValueException(ListAutorites, Labels.getLabel("kermel.error.select.item"));

			 step.setVisible(false);
			 step1.setVisible(false);
			 step0.setVisible(false);
			 step2.setVisible(true);
		 }
			 public void onClick$menuPrecedent() {
				    step.setVisible(true);
				    step1.setVisible(false);
					step0.setVisible(false);
					step2.setVisible(false);
			 }	
			
			 public void onClick$menuPrecedent5(){
				 step0.setVisible(true);
				 step.setVisible(false);
				 step1.setVisible(false);
				 step2.setVisible(false);
			 } 
			 public void onClick$menuAjouter(){
				
				    final String uri = "/audits/autorites.zul";

					final HashMap<String, String> display = new HashMap<String, String>();
					display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.typeAutoritecontractante"));
					display.put(DSP_HEIGHT,"535px");
					display.put(DSP_WIDTH, "80%");

					final HashMap<String, Object> data = new HashMap<String, Object>();
					data.put(AutoritesController.PARAM_WIDOW_AUT,	autoritesaudit);
					showPopupWindow(uri, data, display);

				}
			 public void onClick$menuprecedent4(){
				 if ( list.getSelectedItem() == null)
					 throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
                     autoritesaudit=(SygAuditsPrestataires) list.getSelectedItem().getValue();
					step.setVisible(false);
				    step1.setVisible(false);
					step0.setVisible(true);
					step2.setVisible(false);
					Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);

				}
			 
			 public void InfosSuivi()
			 {
				
				 List<SygAuditsPrestataires> auditsprest = BeanLocator.defaultLookup(AuditsPrestatairesSession.class).find(0, -1,audits.getIdaudit(),autoritesaudit.getAutorite().getId(),null);
				 lblAutorite.setValue(auditsprest.get(0).getAutorite().getDenomination());
				
					if (auditsprest.size()!=0)
					{

						  if (auditsprest.get(0).getDatedebutprevu()!=null || auditsprest.get(0).getDatefinprevu()!=null || auditsprest.get(0).getDatedebutreel()!=null || auditsprest.get(0).getDatefinreel()!=null)
						  {
							TAB_VersionProvisoireRapport.setDisabled(false);
							
						  }else{
							  TAB_VersionProvisoireRapport.setDisabled(true);
							  
						  }

							
							if (auditsprest.get(0).getDaterapportprov()!=null || auditsprest.get(0).getDateenvoirapprov()!=null || auditsprest.get(0).getNomfichierrapprov()!=null)
							{
								TAB_ObservationAutoriteContractante.setDisabled(false);
								
							}else{
								TAB_ObservationAutoriteContractante.setDisabled(true);
							}
						
							
							if (auditsprest.get(0).getDaterecepoberservation()!=null || auditsprest.get(0).getNomfichierobs()!=null)
							{
								TAB_VersionDefinitif.setDisabled(false);
								
							}else{
								TAB_VersionDefinitif.setDisabled(true);
								
							}
					}
					
			 
						if(suiviaudit!=null)
						{
							
							if(suiviaudit.equals("RapportProvisoire"))
							  {
								TAB_VersionProvisoireRapport.setSelected(true);
								Include inc = (Include) this.getFellowIfAny("incVersionProvisoireRapport");
								inc.setSrc("/audits/rapportprovisoire.zul");		
							  }
							else if(suiviaudit.equals("observations"))
							  {
								TAB_ObservationAutoriteContractante.setSelected(true);
								Include inc = (Include) this.getFellowIfAny("incObservationAutoriteContractante");
								inc.setSrc("/audits/observation.zul");		
							  }
							else if(suiviaudit.equals("RapportDefinitive"))
							  {
								TAB_VersionDefinitif.setSelected(true);
								Include inc = (Include) this.getFellowIfAny("incVersionDefinitif");
								inc.setSrc("/audits/rapportdefinitif.zul");		
							  }
							else 
							  {
								TAB_Programmation.setSelected(true);
								Include inc = (Include) this.getFellowIfAny("incProgrammation");
								inc.setSrc("/audits/programmation.zul");		
							  }
						}
						else
						{
							 Include inc = (Include) this.getFellowIfAny("incProgrammation");
							  inc.setSrc("/audits/programmation.zul");
						}
			
			 }
			 public void onClick$menuSuivie(){
				 if ( list.getSelectedItem() == null)
					 throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				    
					step.setVisible(true);

					step0.setVisible(false);
					 autoritesaudit=(SygAuditsPrestataires) list.getSelectedItem().getValue();
					 session.setAttribute("suiviaudit",null);
					 session.setAttribute("codeautoritesaudit", autoritesaudit.getIdpaac());
					InfosSuivi();
				}
			 public void onClick$menuFermer(){
//				 step.setVisible(false);
//				 step1.setVisible(false);
//				 step2.setVisible(false);
//
				 session.setAttribute("ongletsuiviaudit", "autoritesaudits");
				 loadApplicationState("suivi_audit");
			 }
			 
			 public void onClick$menuValider()
				{
			       if (ListPrestataire.getSelectedItem() == null)
						throw new WrongValueException(ListPrestataire, Labels.getLabel("kermel.error.select.item"));
			       prestataires=(SygPrestatairesAudits) ListPrestataire.getSelectedItem().getValue();
			       for (int i = 0; i < ListAutorites.getSelectedCount(); i++) {
			    	   SygAuditsPrestataires auditprestataire=new SygAuditsPrestataires();
			    	   autorite=(SygAutoriteContractante) ((Listitem) ListAutorites.getSelectedItems().toArray()[i]).getValue();
			    	   auditprestataire.setAudit(audits);
			    	   auditprestataire.setAutorite(autorite);
			    	   //auditprestataire.setPrestataire(prestataires);
			    	   BeanLocator.defaultLookup(AuditsPrestatairesSession.class).save(auditprestataire);
			       }
			       Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			        step.setVisible(false);
					step0.setVisible(false);
					step1.setVisible(true);
					step2.setVisible(false);
					
				}
			 
			 public void onClick$menuValiderstep2()
				{
			       if (ListPrestataire.getSelectedItem() == null)
						throw new WrongValueException(ListPrestataire, Labels.getLabel("kermel.error.select.item"));
			       
			       prestataires=(SygPrestatairesAudits) ListPrestataire.getSelectedItem().getValue();
			      //  auditsprest.setPrestataire(prestataires);
			       BeanLocator.defaultLookup(AuditsPrestatairesSession.class).update(auditsprest);
			     
			       Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
					step0.setVisible(true);
					step1.setVisible(false);
					step2.setVisible(false);
					
				}
			 public void onSelect$lstAutorite() {
					autorite1=(SygTypeAutoriteContractante) lstAutorite.getSelectedItem().getValue();
					bandAutorite.setValue(autorite1.getLibelle());
					bandAutorite.close();
				}

			 
//////////////*******************Denomination*******//////////////////////

				// function du bouton recherche par Denomination
				public void onFocus$txtDenomination() {
					if (txtDenomination.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.denomination")))
						txtDenomination.setValue("");

				}

				public void onBlur$txtDenomination() {
					if (txtDenomination.getValue().equals(""))
						txtDenomination.setValue(Labels.getLabel("kermel.referentiel.common.denomination"));
				}

				public void onOK$txtDenomination() {
					onClick$bchercherAC();
				}

				
				public void onClick$bchercherAC() {

					if (txtDenomination.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.denomination")) || txtDenomination.getValue().equals("")) {
						denomination = null;
					} else {
						denomination = txtDenomination.getValue();
					}

					Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);

				}

				public void onOK$bchercherAC() {
					onClick$bchercherAC();
				}

				//////////////*******************Denomination*******//////////////////////
			 
}