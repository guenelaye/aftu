package sn.ssi.kermel.web.audits.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.audit.ejb.AuditSession;
import sn.ssi.kermel.be.audit.ejb.ContractsPrestatairesSession;
import sn.ssi.kermel.be.audit.ejb.PrestatairesAuditsSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPrestataire;
import sn.ssi.kermel.be.entity.SygPrestatairesAudits;
import sn.ssi.kermel.be.referentiel.ejb.PrestataireSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;



@SuppressWarnings("serial")
public class PrestatairesController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox list;
	private Div step0,step1;
	private Paging pgContract;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private Date sygmdatedebut,sygmdatefin;
	private Listheader lshnumcontract,lshdatedebutcontract,lshdatefincontract,lshninea, lshraisonsociale;
	private Datebox datedebutcontract,datefincontract,txtdatedebutcontract,txtdatefincontract;
	private Textbox txtnumcontract,txtRechercherPrestataire;
	private Label lbStatusBar;
	Long code;
	private SygPrestataire prestataireaudits =new SygPrestataire();
	private String Numeroprest = null,page= null,mode;
	private SygPrestataire prestataire;
	private SygPrestatairesAudits auditsprest;
	private SygAutoriteContractante autorite;
	private Listbox ListPrestataire;
	private Paging pgPrestataire;
	private Bandbox bandPrestataire;
	private String errorMsg;
	private Component errorComponent;
	private Long idpaac=null;
	Session session = getHttpSession();
	private SygAudit audit =new SygAudit();
	//SygContratsPrestataires contprest ;
	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		/*
		 * On indique qula fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
//		lshnumcontract.setSortAscending(new FieldComparator("numcontract", false));
//		lshnumcontract.setSortDescending(new FieldComparator("numcontract", true));
//	
//        lshdatedebutcontract.setSortAscending(new FieldComparator("datedebutcontract", false));
//        lshdatedebutcontract.setSortDescending(new FieldComparator("datedebutcontract", true));
//	
//        lshdatefincontract.setSortAscending(new FieldComparator("datefincontract", false));
//        lshdatefincontract.setSortDescending(new FieldComparator("datefincontract", true));
// 
//        lshninea.setSortAscending(new FieldComparator("ninea", false));
//        lshninea.setSortDescending(new FieldComparator("ninea", true));
//        
//        lshraisonsociale.setSortAscending(new FieldComparator("raison social", false));
//        lshraisonsociale.setSortDescending(new FieldComparator("raison social", true));
        
        addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		list.setItemRenderer(this);
		pgContract.setPageSize(byPage);
		pgContract.addForward("onPaging", this,ApplicationEvents.ON_MODEL_CHANGE);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
		 addEventListener(ApplicationEvents.ON_PRESTATAIRE, this);
		 ListPrestataire.setItemRenderer(new PrestatairesRenderer());
	    	pgPrestataire.setPageSize(byPage);
			pgPrestataire.addForward("onPaging", this,ApplicationEvents.ON_PRESTATAIRE);
			Events.postEvent(ApplicationEvents.ON_PRESTATAIRE, this, null);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		code = (Long) session.getAttribute("numAudit");
		audit = BeanLocator.defaultLookup(AuditSession.class) .findById(code);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

	}
	public class PrestatairesRenderer implements ListitemRenderer{

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygPrestataire prestataire = (SygPrestataire) data;
			item.setValue(prestataire);
			Listcell cellidentifiant = new Listcell(prestataire.getIdentifiant());
			cellidentifiant.setParent(item);
			Listcell cellraisonsocial = new Listcell(prestataire.getRaisonsociale());
			cellraisonsocial.setParent(item);

		}
	}
	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 List<SygPrestatairesAudits> prestataires = BeanLocator.defaultLookup(PrestatairesAuditsSession.class).find(pgContract.getActivePage()*byPage,byPage,autorite,prestataireaudits, audit,null);
			 SimpleListModel listModel = new SimpleListModel(prestataires);
			list.setModel(listModel);
			pgContract.setTotalSize(BeanLocator.defaultLookup(PrestatairesAuditsSession .class).count(autorite,prestataireaudits, audit,null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_PRESTATAIRE)) {
			List<SygPrestataire> prestataire = BeanLocator.defaultLookup(PrestataireSession.class).find(activePage, byPage, Numeroprest,null,null,null,null,null);
			ListPrestataire.setModel(new SimpleListModel(prestataire));
			pgPrestataire.setTotalSize(BeanLocator.defaultLookup(PrestataireSession.class).count(Numeroprest,null,null,null,null,null));

		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			step0.setVisible(false);
			step1.setVisible(true);
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				// de
				// fixer
				// les
				// dimenisons
				// du
				// popup
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// de
				// passer
				// des
				// parametres
				// au
				// popup
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < list.getSelectedCount(); i++) {
					Long codes = (Long)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					System.out.println(codes);
				BeanLocator.defaultLookup(ContractsPrestatairesSession .class).delete(codes);
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
		public void render(final Listitem item, final Object data, int index) throws Exception {
		    SygPrestatairesAudits auditsprest = (SygPrestatairesAudits) data;
			item.setValue(auditsprest.getIdpaac());
			
			Listcell cellidentifiant = new Listcell(auditsprest.getPrestataire().getIdentifiant());
			cellidentifiant.setParent(item);
			
			Listcell cellraisonsocial = new Listcell(auditsprest.getPrestataire().getRaisonsociale());
			cellraisonsocial.setParent(item);
		}
		public void onSelect$ListPrestataire() {
			prestataire=(SygPrestataire) ListPrestataire.getSelectedItem().getValue();
			bandPrestataire.setValue(prestataire.getIdentifiant()+ " "+prestataire.getRaisonsociale());
			bandPrestataire.close();
		}
	
		public void onClick$btnRechercherPrestataire() {
			if (!txtRechercherPrestataire.getValue().equals("") && !txtRechercherPrestataire.getValue().equals("Prestataire")) {
				Numeroprest = txtRechercherPrestataire.getValue();
			} else {
				Numeroprest = null;
				page="0";
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}

		public void onClick$menuValider() {
			if (checkFieldConstraints()) {
				auditsprest= new SygPrestatairesAudits();
//				auditsprest.setNumcontract(txtnumcontract.getValue());
//				auditsprest.setDatedebutcontract(txtdatedebutcontract.getValue());
//				auditsprest.setDatefincontract(txtdatefincontract.getValue());
				
				if (ListPrestataire.getSelectedItem() == null) {
					errorMsg = "Veuillez selectionner un Prestataire!";
					errorComponent = bandPrestataire;
					throw new WrongValueException(errorComponent, errorMsg);
		
				}
				prestataire=((SygPrestataire) ListPrestataire.getSelectedItem().getValue());
				 auditsprest.setPrestataire(prestataire);
				 auditsprest.setAudit(audit);
			if (idpaac==null) {
				BeanLocator.defaultLookup(PrestatairesAuditsSession.class).save(auditsprest);
	
			} 
			else  {
				BeanLocator.defaultLookup(PrestatairesAuditsSession.class).update(auditsprest);
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			 step0.setVisible(true);
			 step1.setVisible(false);
			
		}else{
			throw new WrongValueException(errorComponent, errorMsg);
		}
	
		}
	 public void onClick$menuModifier() {
		 if (list.getSelectedItem() == null)
				
				throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
		        auditsprest=(SygPrestatairesAudits) list.getSelectedItem().getValue();
//				txtnumcontract.setValue(auditsprest.getNumcontract());
//				txtdatedebutcontract.setValue(auditsprest.getDatedebutcontract());
//				txtdatefincontract.setValue(auditsprest.getDatefincontract());
//				prestataire=auditsprest.getPrestataire();
				bandPrestataire.setValue(prestataire.getIdentifiant()+""+prestataire.getRaisonsociale());
				 idpaac=auditsprest.getIdpaac();
				 step0.setVisible(false);
				 step1.setVisible(true);
	 }
			 public void onClick$menuFermer() {
				    step0.setVisible(true);
					step1.setVisible(false);
				     }		
			 
      		 private boolean checkFieldConstraints(){
      			return false;
      		 }
//				{
//					try {
//				
//						
//						if (txtnumcontract.getValue() == null || txtnumcontract.getValue().trim().equalsIgnoreCase("")) {
//							errorComponent = txtnumcontract;
//							errorMsg = "Veuillez renseigner le champ R�f�rence";
//							lbStatusBar.setValue(errorMsg);
//							lbStatusBar.setStyle("color:red");
//							return false;
//						}
//
//					   if (txtdatedebutcontract.getValue() == null) {
//							errorComponent = txtdatedebutcontract;
//							errorMsg = "Veuillez renseigner le champ Date de Debut";
//							lbStatusBar.setValue(errorMsg);
//							lbStatusBar.setStyle("color:red");
//							return false;
//						}
//						
//							if (txtdatefincontract.getValue() == null) {
//							errorComponent = txtdatefincontract;
//							errorMsg = "Veuillez renseigner le champ Date de Fin";
//							lbStatusBar.setValue(errorMsg);
//							lbStatusBar.setStyle("color:red");
//							return false;
//						}
//							
//						
//						return true;
//					} catch (Exception e) {
//
//						lbStatusBar.setValue(errorMsg);
//						// lbStatusBar.setStyle(UIConstants.STYLE_STATUSBAR_ERROR);
//						e.printStackTrace();
//						throw new WrongValueException(errorComponent, errorMsg);
//					}
//						
//				}
//				
//				public void onBlur$txtdatedebutcontract() {
//				
//				Date datedebutcontract =  txtdatedebutcontract.getValue();
//				Date datefincontract = (Date)txtdatefincontract.getValue();
//				if (txtdatefincontract.getValue() != null) {
//					if (datefincontract.compareTo(datedebutcontract ) < 0) {
//						errorMsg = "La date fin doit etre inferieure la date de debut . Veuillez corriger cette incoh�rence.";
//						errorComponent = txtdatefincontract;
//						throw new WrongValueException(errorComponent, errorMsg);
//					}
//				}
//				
//			  }
//			     public void onBlur$txtDateFincontract()
//			     { 
//			    	 Date datefincontract =  txtdatefincontract.getValue();
//			    	 Date datedebutcontract  = (Date) txtdatedebutcontract.getValue();
//			     if (txtdatedebutcontract.getValue() != null) {
//			      if (datedebutcontract.compareTo(datefincontract ) > 0) {
//				  errorMsg = "La Date Debut doit etre sup�rieur � la date de fin. Veuillez corriger cette incoh�rence.";
//				errorComponent = txtdatedebutcontract;
//				throw new WrongValueException(errorComponent, errorMsg);
//				
//			      }
			    
 }
