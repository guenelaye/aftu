package sn.ssi.kermel.web.audits.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tabpanel;

import sn.ssi.kermel.be.audit.ejb.AuditSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.workflow.ejb.WorkflowSession;
import sn.ssi.kermel.web.common.components.DossierMenu;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



public class DossiersAutoriteContractanteFormController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private Long code;
	UtilVue utilVue = UtilVue.getInstance();
	private Label lblLibelle,lblGestion,lblStatus,lblDateStatus;
	Session session = getHttpSession();
	private DossierMenu monDossierMenu;
	private Tabpanel tabAutorites;
	private Include incAutorites;
	private SygAudit audit =new SygAudit();

	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		tabAutorites.addEventListener(Events.ON_CLICK, this);
		code = (Long) session.getAttribute("numAudit");
		audit = BeanLocator.defaultLookup(AuditSession.class) .findById(code);
		lblLibelle.setValue(audit.getLibelleaudit());
		lblGestion.setValue(audit.getGestion().toString());
		lblStatus.setValue(BeanLocator.defaultLookup(WorkflowSession.class).findState(audit.getState().getCode()).getLibelle());
		lblDateStatus.setValue(UtilVue.getInstance().formateLaDate(audit.getDatestatut()));
		monDossierMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monDossierMenu.setDosCode(audit.getIdaudit());
        monDossierMenu.setState(audit.getState().getCode());
        monDossierMenu.afterCompose();

	}

	
	public void onCreate(CreateEvent createEvent) {
		
	
		
		 Include inc = (Include) this.getFellowIfAny("incAutorites");
         inc.setSrc("/audits/autoritescontractantes.zul");
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	
}