package sn.ssi.kermel.web.audits.controllers;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.HashMap;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.audit.ejb.AuditSession;
import sn.ssi.kermel.be.audit.ejb.CourrierAuditSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygCourrierAudits;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;




@SuppressWarnings("serial")
public class RapportGlobalController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Datebox txtdatepublicrapport,txtdaterapport;
	
	
	private Textbox txtVersionElectronique;
	
	
	Long code;
	
	UtilVue utilVue = UtilVue.getInstance();
	private String nomFichier;
	private final String cheminDossier = UIConstants.PATH_PJ;
	
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Label lbStatusBar;
	private Long idpaac,idcourrier,codeaudit;
	
	Session session = getHttpSession();
	private SygAudit audit;
	private Image image;
	private String extension,images;
	private Div step0,step1;
	private Iframe idIframe;
	private String Statut;
	 private static final String CONFIRMPUBLIER = "CONFIRMPUBLIER";
	 private Menuitem menuValider,menuPublier,menuDePublier;
	 
	 //table pmb_courriers_audits
	 private SygCourrierAudits courrier = new SygCourrierAudits();
		private SygCourrierAudits courriers = new SygCourrierAudits();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		
		
		
		
		codeaudit = (Long) session.getAttribute("numAudit");
		audit = BeanLocator.defaultLookup(AuditSession.class) .findById(codeaudit);
		

		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	
	}
	
	@Override
	public void onEvent(Event event) throws Exception {
		// TODO Auto-generated method stub
		if(event.getName().equals(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			 String confirmer = (String) ((HashMap<String, Object>) event.getData()).get(CONFIRMPUBLIER);
			 if (confirmer != null && confirmer.equalsIgnoreCase("Publication_Confirmer")) 
			 {
				 courrier.setStatut("PUB");
				 BeanLocator.defaultLookup(CourrierAuditSession.class).update(courrier);
				 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			 }else if (confirmer != null && confirmer.equalsIgnoreCase("DePublication_Confirmer")) 
			 {
				 courrier.setStatut("Saisie");
				 BeanLocator.defaultLookup(CourrierAuditSession.class).update(courrier);
				 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			 }
			 
			   Executions.getCurrent().setAttribute("numAudit",codeaudit);
			   session.setAttribute("suiviaudit","RapportGlobal");    
			   session.setAttribute("ongletsuiviaudit", "RapportGlobal");
			    loadApplicationState("suivi_audit");
			
				
			   


		 }
		
	}
	
	public void onCreate(CreateEvent event) {
		if(audit!=null){


			
			courriers = BeanLocator.defaultLookup(CourrierAuditSession.class).findAuditCourrier(codeaudit,"RapportGlobal");
			if(courriers!=null){
				courrier=courriers;
				idcourrier=courriers.getCourrierId();
				txtdaterapport.setValue(courriers.getCourrierDate());
				txtdatepublicrapport.setValue(courriers.getDatepubrapport());
				txtVersionElectronique.setValue(courriers.getFichier());
				//Controle des menu
				Statut=courriers.getStatut();
				if(Statut.equals("PUB")){
					menuValider.setDisabled(true);
					menuPublier.setDisabled(true);
				}else if(Statut.equals("Saisie")){
					menuDePublier.setDisabled(true);
				}
			}else{
				menuPublier.setDisabled(true);
				menuDePublier.setDisabled(true);
			}
			
			
			if(courrier.getFichier()!="" && courrier.getFichier()!=null)
			{
				extension=courrier.getFichier().substring(courrier.getFichier().length()-3,  courrier.getFichier().length());
				 if(extension.equalsIgnoreCase("pdf"))
					 images="/images/icone_pdf.png";
				 else  
					 images="/images/word.jpg";
				 
				image.setVisible(true);
				image.setSrc(images);
				nomFichier=courrier.getFichier();
			}
		}
			
	}
	
	private boolean checkFieldConstraints() {

		try {
			
			if (txtdaterapport.getValue() == null) {

				errorComponent = txtdaterapport;
				errorMsg = Labels.getLabel("kermel.common.form.daterapportglobal") + ": " + Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);

			}
			
			
			if (txtdatepublicrapport.getValue() == null) {

				errorComponent = txtdatepublicrapport;
				errorMsg = Labels.getLabel("kermel.common.form.datepublicrapportglobal") + ": " + Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);

			}
			
			if(txtVersionElectronique.getValue().equals(""))
		     {
        errorComponent = txtVersionElectronique;
        errorMsg = Labels.getLabel("kermel.referentiel.common.fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		

			return true;

		} catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString() + " [checkFieldConstraints]";
			errorComponent = null;
			return false;

		}

	}
	
	public void onOK() {
		if (checkFieldConstraints()) {
			
			 //table pmb_courriers_audits
			
			courrier.setCourrierDate(txtdaterapport.getValue());
			courrier.setDatepubrapport(txtdatepublicrapport.getValue());
			
			courrier.setCourrierOrigine("RapportGlobal");
		
			courrier.setAudit(audit);
			courrier.setFichier(txtVersionElectronique.getValue());
			courrier.setStatut("Saisie");
			if (idcourrier==null){ 
				BeanLocator.defaultLookup(CourrierAuditSession.class).save(courrier);
			}else{
				BeanLocator.defaultLookup(CourrierAuditSession.class).update(courrier);
			}
			

	   
	   courriers = BeanLocator.defaultLookup(CourrierAuditSession.class).findAuditCourrier(codeaudit,"RapportGlobal");
		idcourrier=courriers.getCourrierId();
		courrier=courriers;

	   Executions.getCurrent().setAttribute("numAudit",codeaudit);
	   
	   session.setAttribute("suiviaudit","RapportGlobal");    
	   session.setAttribute("ongletsuiviaudit", "RapportGlobal");
	    loadApplicationState("suivi_audit");
	
		
		}
	}
	
	
	public void onClick$menuFermer() {
		loadApplicationState("suivi_audit");
		
	}
	
	
	
	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	  public void onClick$btnChoixFichier() {
			//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
				if (ToolKermel.isWindows())
					nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
				else
					nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

				txtVersionElectronique.setValue(nomFichier);
			}
	  
	  public void onClick$image() {
			step0.setVisible(false);
			step1.setVisible(true);
			String filepath = cheminDossier +  nomFichier;
			File f = new File(filepath.replaceAll("\\\\", "/"));

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(f, null, null);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (mymedia != null)
				idIframe.setContent(mymedia);
			else
				idIframe.setSrc("");

			idIframe.setHeight("600px");
			idIframe.setWidth("100%");
		}

		public org.zkoss.util.media.AMedia fetchFile(File file) {

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(file, null, null);
				return mymedia;
			} catch (Exception e) {

				e.printStackTrace();
				return null;
			}

		}
		public void onClick$menuFermerstep1() {
			step0.setVisible(true);
			step1.setVisible(false);
		}
		
		//Menu publier ************************
		public void onClick$menuPublier() throws InterruptedException {
			
			if ((idcourrier!=null)){
				Statut=courrier.getStatut();
				if(Statut.equals("Saisie")){
					HashMap<String, String> display = new HashMap<String, String>(); // permet
					 display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.plansdepassation.publier"));
					 display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.audit.rapportdefinitif.publier.titre"));
					 display.put(MessageBoxController.DSP_HEIGHT, "150px");
					 display.put(MessageBoxController.DSP_WIDTH, "100px");
			        HashMap<String, Object> map = new HashMap<String, Object>(); // permet
			        map.put(CONFIRMPUBLIER, "Publication_Confirmer");
					 showMessageBox(display, map);
				}
			
			
			}else{
				   Messagebox.show(Labels.getLabel("kermel.audit.rapportdefinitif.statut.publier.controle"), "Erreur", Messagebox.OK, Messagebox.ERROR);
			      }
	
			
		}
		
		//Menu d�publier
		public void onClick$menuDePublier() throws InterruptedException {
			if(Statut.equals("PUB")){
				HashMap<String, String> display = new HashMap<String, String>(); // permet
				 display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.audit.rapportdefinitif.d�publier"));
				 display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.audit.rapportdefinitif.depublier.titre"));
				 display.put(MessageBoxController.DSP_HEIGHT, "150px");
				 display.put(MessageBoxController.DSP_WIDTH, "100px");
		        HashMap<String, Object> map = new HashMap<String, Object>(); // permet
		        map.put(CONFIRMPUBLIER, "DePublication_Confirmer");
				 showMessageBox(display, map);
			}
		}
}