package sn.ssi.kermel.web.audits.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.audit.ejb.AuditSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;


@SuppressWarnings("serial")
public class ListeAuditController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox list;
	private Paging pgAudit;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private String sygmlibelleaudit,annee;

	private Textbox txtlibelleaudit;
	Long code;
	Session session = getHttpSession();
	private Menuitem ADD_AUDIT, MOD_AUDIT, SUPP_AUDIT,WSUIVI_AUDITS,WCONTRATS;
	private KermelSousMenu monSousMenu;
	private ArrayList<String> listValeursAnnees;
	private Combobox cbannee;
	private Integer gestionannee;

	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.AUDITS);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
			if (ADD_AUDIT!= null)
		    ADD_AUDIT.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
		if (MOD_AUDIT != null)
			MOD_AUDIT.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
		if (SUPP_AUDIT != null)
			SUPP_AUDIT.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
		if (WSUIVI_AUDITS != null)
			WSUIVI_AUDITS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DETAILS);
		if (WCONTRATS != null)
			WCONTRATS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_CONTRAT);
		/*
		 * On indique qula fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_CONTRAT, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
	
//		lshlibelleaudit.setSortAscending(new FieldComparator("libelleaudit", false));
//		lshlibelleaudit.setSortDescending(new FieldComparator("libelleaudit", true));
//		lshstatut.setSortAscending(new FieldComparator("statut", false));
//		lshstatut.setSortDescending(new FieldComparator("statut", true));
//        lshdatestatut.setSortAscending(new FieldComparator("datestatut", false));
//        lshdatestatut.setSortDescending(new FieldComparator("datestatut", true));
//        lshgestion.setSortAscending(new FieldComparator("gestion", false));
//		lshgestion.setSortDescending(new FieldComparator("gestion", true));

 
		list.setItemRenderer(this);

		pgAudit.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pgAudit.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	
	public void onCreate(CreateEvent event) {
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
		
		annee = UtilVue.getInstance().anneecourant(new Date());
		listValeursAnnees = new ArrayList<String>();
		for (int i = 2006; i < Integer.parseInt(annee)+5 ; i++) {
			listValeursAnnees.add(i+"");
		}
		cbannee.setModel(new SimpleListModel(listValeursAnnees));

	}
	
	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */

	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 List<SygAudit> audit = BeanLocator.defaultLookup(AuditSession.class).find(pgAudit.getActivePage()*byPage,byPage,null,sygmlibelleaudit,null,gestionannee);
			 SimpleListModel listModel = new SimpleListModel(audit);
			list.setModel(listModel);
			pgAudit.setTotalSize(BeanLocator.defaultLookup(
					AuditSession.class).count());
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/audits/formaudit.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.suivitaudit"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "60%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(AuditFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/audits/formaudit.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "60%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(AuditFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(AuditFormController.PARAM_WIDOW_CODE, list
					.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				// de
				// fixer
				// les
				// dimenisons
				// du
				// popup
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// de
				// passer
				// des
				// parametres
				// au
				// popup
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < list.getSelectedCount(); i++) {
					Long codes = (Long)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					System.out.println(codes);
				BeanLocator.defaultLookup(AuditSession.class).delete(codes);
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DETAILS)){
				if (list.getSelectedItem()==null) {
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				}
				session.setAttribute("numAudit",list.getSelectedItem().getValue());
				 session.setAttribute("ongletsuiviaudit", "autoritesaudits");
				loadApplicationState("suivi_audit");
			}
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CONTRAT)) {
				
				
				if (list.getSelectedItem()==null) {
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				}
				
				final String uri = "/audits/contratsprestataires.zul";

				final HashMap<String, String> display = new HashMap<String, String>();
				display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveaucontrat"));
				display.put(DSP_HEIGHT,"500px");
				display.put(DSP_WIDTH, "80%");

				final HashMap<String, Object> data = new HashMap<String, Object>();
				data.put(AuditFormController.WINDOW_PARAM_MODE,
						UIConstants.MODE_NEW);

				showPopupWindow(uri, data, display);
			}
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	    @Override
		public void render(final Listitem item, final Object data, int index) throws Exception {
			SygAudit audit = (SygAudit) data;
			item.setValue(audit.getIdaudit());
			
			Listcell cellgestion = new Listcell(audit.getGestion().toString());
			cellgestion.setParent(item);
			Listcell celledatestatut = new Listcell(UtilVue.getInstance().formateLaDate(audit.getDatestatut()));
			celledatestatut.setParent(item);
			Listcell celllibelleaudit = new Listcell(audit.getLibelleaudit());
			celllibelleaudit.setParent(item);
			
			Listcell cellNbreAutorite = new Listcell(audit.getNombreautorite().toString());
			cellNbreAutorite.setParent(item);
			
			Listcell cellNbrePrestaire = new Listcell(audit.getNombreprestataire().toString());
			cellNbrePrestaire.setParent(item);
//			Listcell cellstatut = new Listcell(audit.getState().getLibelle());
//			cellstatut.setParent(item);
			
				 
		}
	    
	    ////////Recherche ********Recherche**************///////////////////
	    public void onClick$btnRechercher() {
			
			if (txtlibelleaudit.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.libelle")) || txtlibelleaudit.getValue().equals("")) {
				sygmlibelleaudit = null;
			} else {
				sygmlibelleaudit  = txtlibelleaudit.getValue();
			}	
	
			if (cbannee.getValue() == null) {
				gestionannee = null;
			} else {
				gestionannee  = Integer.parseInt(cbannee.getValue());
			}	
		
		
	 
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
	
	    //Libell� Audit************************************************************
		 
		  public void onFocus$txtlibelleaudit() {
			if (txtlibelleaudit.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.libelle"))) {
				txtlibelleaudit.setValue("");
	
			  }
	     }
	
			
				
				public void onBlur$txtlibelleaudit() {
					if (txtlibelleaudit.getValue().equalsIgnoreCase("")) {
						txtlibelleaudit.setValue(Labels.getLabel("kermel.referentiel.common.libelle"));
					}	
			}
				
				
				 public void onOK$txtlibelleaudit() {
						onClick$btnRechercher();
					      }
	
			//Gestion *****************************************
				public void onFocus$cbannee() {
					if (cbannee.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.gestion"))) {
						cbannee.setValue("");
						
		
					}
				}
				
				public void onBlur$cbannee() {
					if (cbannee.getValue().equalsIgnoreCase("")) {
						cbannee.setValue(Labels.getLabel("kermel.common.form.gestion"));
					}
				}		
					
				  public void onOK$cbannee() {
						onClick$btnRechercher();
					     }
				  
				  
					
	}