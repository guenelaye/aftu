package sn.ssi.kermel.web.audits.controllers;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygContratsPrestataires;
import sn.ssi.kermel.be.entity.SygPrestataire;
import sn.ssi.kermel.be.referentiel.ejb.PrestataireSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
//import com.arjuna.ats.internal.jdbc.drivers.modifiers.list;
import sn.ssi.kermel.be.audit.ejb.ContractsPrestatairesSession;




@SuppressWarnings("serial")
public class ContratsPrestataireFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,codelocalite,annee,page=null;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtnumcontract,txtRechercherPrestataire;
	private SygContratsPrestataires contract =new SygContratsPrestataires();
	private SimpleListModel lst;
	private Datebox txtdatedebutcontract,txtdatefincontract,datefincontract,datedebutcontract;
	private Paging pgContract;
	private final String ON_LOCALITE = "onLocalite";
	Long code;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE,activePage;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private Label lbStatusBar;
	private String errorMsg;
	private Component errorComponent;
	private Div step0,step1,step2;
	private Label lblAutorite,entete1,entete2,entete3;
	private Bandbox bandPrestataire;
	private Paging pg;
	private String identifiant = null;
	private SygPrestataire prestataire;
	private Listbox ListPrestataire;
	
	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WIDOW_CODE);
			contract = BeanLocator.defaultLookup(ContractsPrestatairesSession.class)
					.findById(code);
			prestataire = BeanLocator.defaultLookup(PrestataireSession.class).findById(contract.getPrestataire().getIdprestataire());
			bandPrestataire.setValue(prestataire.getIdentifiant());
			txtnumcontract.setValue(contract.getNumcontract());
			txtdatedebutcontract.setValue(contract.getDatedebutcontract());
			txtdatefincontract.setValue(contract.getDatefincontract());
			
		}
			
	}
	public void onOK() {
		if (checkFieldConstraints()) {
			contract= new SygContratsPrestataires ();
		
		
			contract.setNumcontract(txtnumcontract.getValue());
			contract.setDatedebutcontract(txtdatedebutcontract.getValue());
			contract.setDatefincontract(txtdatefincontract.getValue());
			
			if (ListPrestataire.getSelectedItem() == null) {
				errorMsg = "Veuillez selectionner un Prestataire!";
				errorComponent = bandPrestataire;
				throw new WrongValueException(errorComponent, errorMsg);
	
			}
			prestataire=((SygPrestataire) ListPrestataire.getSelectedItem().getValue());
			contract.setPrestataire(prestataire);
		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			BeanLocator.defaultLookup(ContractsPrestatairesSession.class).save(contract);

		} 
		else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			BeanLocator.defaultLookup(ContractsPrestatairesSession.class).update(contract);
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();
	}else{
		throw new WrongValueException(errorComponent, errorMsg);
	}

	}
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		ListPrestataire.setItemRenderer(this);
		pg.setPageSize(byPage);
		pg.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	
	
	
	
	
	}
	public void onClick$menuNext(){
		if (ListPrestataire.getSelectedItem() == null)
			throw new WrongValueException(ListPrestataire, Labels.getLabel("kermel.error.select.item"));
		
		prestataire=(SygPrestataire) ListPrestataire.getSelectedItem().getValue();
		entete1.setValue(prestataire.getIdentifiant());
		step0.setVisible(false);
		step1.setVisible(true);
		Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);

	}
	
	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		// TODO Auto-generated method stub
		SygPrestataire prestataire = (SygPrestataire) data;
		item.setValue(prestataire);
		item.setAttribute("Identifiant", prestataire.getIdentifiant());
		addCell(item, prestataire.getIdentifiant());
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			if (prestataire.getIdprestataire() == this.prestataire.getIdprestataire())
				item.setSelected(true);
		}
	
}
	public void onSelect$ListPrestataire() {
		bandPrestataire.setValue(ListPrestataire.getSelectedItem().getAttribute("Numeroprest").toString());
		bandPrestataire.close();
	}

	public void onClick$btnRechercherPrestataire() {
		if (!txtRechercherPrestataire.getValue().equals("") && !txtRechercherPrestataire.getValue().equals("Prestataire")) {
			identifiant = txtRechercherPrestataire.getValue();
		} else {
			identifiant = null;
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	public void onOK$txtRechercherPrestataire() {
		onClick$btnRechercherPrestataire();
	}

	public void onFocus$txtRechercherPrestataire() {
		txtRechercherPrestataire.setValue("");
	}	
	@Override
	public void onEvent(Event event) throws Exception {
		// TODO Auto-generated method stub
		
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			List<SygPrestataire> prestataire = BeanLocator.defaultLookup(PrestataireSession.class).find(activePage, byPage,identifiant,null,null,null,null,null);
			ListPrestataire.setModel(new SimpleListModel(prestataire));
			pg.setTotalSize(BeanLocator.defaultLookup(PrestataireSession.class).count(identifiant,null,null,null,null,null));

		} 
			}
	
	
	
	private boolean checkFieldConstraints()
	{
		try {
	
			
			if (txtnumcontract.getValue() == null || txtnumcontract.getValue().trim().equalsIgnoreCase("")) {
				errorComponent = txtnumcontract;
				errorMsg = "Veuillez renseigner le champ R�f�rence";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}

		   if (txtdatedebutcontract.getValue() == null) {
				errorComponent = txtdatedebutcontract;
				errorMsg = "Veuillez renseigner le champ Date de Debut";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}
			
				if (txtdatefincontract.getValue() == null) {
				errorComponent = txtdatefincontract;
				errorMsg = "Veuillez renseigner le champ Date de Fin";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}
				
			
			return true;
		} catch (Exception e) {

			lbStatusBar.setValue(errorMsg);
			// lbStatusBar.setStyle(UIConstants.STYLE_STATUSBAR_ERROR);
			e.printStackTrace();
			throw new WrongValueException(errorComponent, errorMsg);
		}
			
	}
	
	public void onBlur$txtdatedebutcontract() {
	
	Date datedebutcontract =  txtdatedebutcontract.getValue();
	Date datefincontract = (Date)txtdatefincontract.getValue();
	if (txtdatefincontract.getValue() != null) {
		if (datefincontract.compareTo(datedebutcontract ) < 0) {
			errorMsg = "La date fin doit etre inferieure la date de debut . Veuillez corriger cette incoh�rence.";
			errorComponent = txtdatefincontract;
			throw new WrongValueException(errorComponent, errorMsg);
		}
	}
	
  }
     public void onBlur$txtDateFin()
     { 
    	 Date datefincontract =  txtdatefincontract.getValue();
    	 Date datedebutcontract  = (Date) txtdatedebutcontract.getValue();
     if (txtdatedebutcontract.getValue() != null) {
      if (datedebutcontract.compareTo(datefincontract ) > 0) {
	  errorMsg = "La Date Debut doit etre sup�rieur � la date de fin. Veuillez corriger cette incoh�rence.";
	errorComponent = txtdatedebutcontract;
	throw new WrongValueException(errorComponent, errorMsg);
	
      }
    }
   }
}