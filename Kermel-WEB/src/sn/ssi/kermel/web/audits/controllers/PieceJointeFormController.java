package sn.ssi.kermel.web.audits.controllers;


import java.util.Calendar;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.audit.ejb.AuditSession;
import sn.ssi.kermel.be.audit.ejb.PieceJointeAuditsSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygPieceJointeAudits;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;




@SuppressWarnings("serial")
public class PieceJointeFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,codelocalite,annee,nomFichier;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtfichier,txtreference,txtobjet,txtVersionElectronique,txtlibelle;
	private Intbox txttraiter,txtpublier,txtpoubelle;
	private SygPieceJointeAudits PieceJointeAudits =new SygPieceJointeAudits();
	private SimpleListModel lst;
	private Paging pgdenonciation;
	private Datebox txtdate,datereception;
	private final String ON_LOCALITE = "onLocalite";
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	Long code;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	private String errorMsg;
	private Component errorComponent;
	private Label lbStatusBar;
	private SygAudit audit =new SygAudit();
	Session session = getHttpSession();
	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		code = (Long) session.getAttribute("numAudit");
		audit = BeanLocator.defaultLookup(AuditSession.class) .findById(code);
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WIDOW_CODE);
			PieceJointeAudits = BeanLocator.defaultLookup(PieceJointeAuditsSession.class)
					.findById(code);
			
			txtdate.setValue(PieceJointeAudits.getDate());
			txtVersionElectronique.setValue(PieceJointeAudits.getFichier());
			
	
		}
			
	}
	public void onOK(){
	if (checkFieldConstraints()){
		PieceJointeAudits.setDate(txtdate.getValue());
		PieceJointeAudits.setFichier(nomFichier);
		PieceJointeAudits.setAudit(audit);
		
		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			BeanLocator.defaultLookup(PieceJointeAuditsSession.class).save(PieceJointeAudits);

		} 
		else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			BeanLocator.defaultLookup(PieceJointeAuditsSession.class).update(PieceJointeAudits);
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();
	}else{
		throw new WrongValueException(errorComponent, errorMsg);
	}

	}
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	
	}
	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub
		
	}

	
	public void onClick$btnChoixFichier() {
		//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
			if (ToolKermel.isWindows())
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectronique.setValue(nomFichier);
		}
	@Override
	public void onEvent(Event arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}
	private boolean checkFieldConstraints()
	{
		try {
	
			
			if (txtVersionElectronique.getValue() == null || txtVersionElectronique.getValue().trim().equalsIgnoreCase("")) {
				errorComponent = txtreference;
				errorMsg = "Veuillez renseigner le Fichier";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}

			
			if (txtdate.getValue() == null ) {
				errorComponent = txtdate;
				errorMsg = "Veuillez renseigner le champ Date ";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}
			return true;
		}catch (Exception e) {

			lbStatusBar.setValue(errorMsg);
			// lbStatusBar.setStyle(UIConstants.STYLE_STATUSBAR_ERROR);
			e.printStackTrace();
			throw new WrongValueException(errorComponent, errorMsg);
		}
}

}