package sn.ssi.kermel.web.audits.controllers;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.audit.ejb.AuditsPrestatairesSession;
import sn.ssi.kermel.be.audit.ejb.CourrierAuditSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAuditsPrestataires;
import sn.ssi.kermel.be.entity.SygCourrierAudits;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;




@SuppressWarnings("serial")
public class ObservationFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Datebox txtdatecourrier,txtdatereceptionobservation;
	
	
	private Textbox txtreference,txtVersionElectronique,txtVersionElectronique2;
	
	
	Long code;
	private String nomFichier;
	private final String cheminDossier = UIConstants.PATH_PJ;
	
	//private SygCourrierAudits courrier;
	UtilVue utilVue = UtilVue.getInstance();
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Label lbStatusBar;
	private Long idpaac,idcourrier;
	Session session = getHttpSession();
	private SygAuditsPrestataires autoritesaudit;
	private SygCourrierAudits courrier = new SygCourrierAudits();
	private SygCourrierAudits courriers = new SygCourrierAudits();
	private Image image,image2;
	private String extension,images;
	private Div step0,step1;
	private Iframe idIframe;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		
		idpaac=(Long) session.getAttribute("codeautoritesaudit");    
		autoritesaudit= BeanLocator.defaultLookup(AuditsPrestatairesSession.class).findById(idpaac);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	
	}
	
	@Override
	public void onEvent(Event arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	
	public void onCreate(CreateEvent event) {
		if(autoritesaudit!=null){
			
			txtdatereceptionobservation.setValue(autoritesaudit.getDaterecepoberservation());
			txtVersionElectronique.setValue(autoritesaudit.getNomfichierobs());
			txtVersionElectronique2.setValue(autoritesaudit.getNomfichier2obs());
			
			courriers = BeanLocator.defaultLookup(CourrierAuditSession.class).findAutoriteAuditPres(idpaac,"Observation");
			if(courriers!=null){
				courrier=courriers;
				idcourrier=courriers.getCourrierId();
			txtdatecourrier.setValue(courriers.getCourrierDate());
			txtreference.setValue(courriers.getCourrierReference());	
			
			if(autoritesaudit.getNomfichierobs()!="")
			{
				extension=autoritesaudit.getNomfichierobs().substring(autoritesaudit.getNomfichierobs().length()-3,  autoritesaudit.getNomfichierobs().length());
				 if(extension.equalsIgnoreCase("pdf"))
					 images="/images/icone_pdf.png";
				 else  
					 images="/images/word.jpg";
				 
				 image.setVisible(true);
				image.setSrc(images);
				nomFichier=autoritesaudit.getNomfichierobs();
			}
			
			if((autoritesaudit.getNomfichier2obs()!="") && (autoritesaudit.getNomfichier2obs()!=null))
			{
				extension=autoritesaudit.getNomfichier2obs().substring(autoritesaudit.getNomfichier2obs().length()-3,  autoritesaudit.getNomfichier2obs().length());
				 if(extension.equalsIgnoreCase("pdf"))
					 images="/images/icone_pdf.png";
				 else  
					 images="/images/word.jpg";
				 
				 image2.setVisible(true);
				image2.setSrc(images);
				nomFichier=autoritesaudit.getNomfichier2obs();
			}
			
			}
		
		}
			
	}
	
	private boolean checkFieldConstraints() {

		try {
			
			if (txtdatereceptionobservation.getValue() == null) {

				errorComponent = txtdatereceptionobservation;
				errorMsg = Labels.getLabel("kermel.common.form.datereceptionobservation") + ": " + Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);

			}
			
			if (txtdatecourrier.getValue() == null) {

				errorComponent = txtdatecourrier;
				errorMsg = Labels.getLabel("kermel.common.form.datecourrier") + ": " + Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);

			}
			
			if(txtreference.getValue().equals(""))
		     {
          errorComponent = txtreference;
          errorMsg = Labels.getLabel("kermel.referentiel.courrier.reference")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			
			if(txtVersionElectronique.getValue().equals(""))
		     {
         errorComponent = txtVersionElectronique;
         errorMsg = Labels.getLabel("kermel.referentiel.common.fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		

			return true;

		} catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString() + " [checkFieldConstraints]";
			errorComponent = null;
			return false;

		}

	}
	
	public void onOK() {
		
		if (checkFieldConstraints()) {
		
		
		courrier.setCourrierDate(txtdatecourrier.getValue());
		courrier.setCourrierReference(txtreference.getValue());
		courrier.setCourrierOrigine("Observation");
		courrier.setAutoriteauditspres(autoritesaudit);
		courrier.setAudit(autoritesaudit.getAudit());
		courrier.setFichier(txtVersionElectronique.getValue());
		courrier.setFichier2(txtVersionElectronique2.getValue());
		if (idcourrier==null){ 
			BeanLocator.defaultLookup(CourrierAuditSession.class).save(courrier);
		}else{
			BeanLocator.defaultLookup(CourrierAuditSession.class).update(courrier);
		}
		
		autoritesaudit.setDaterecepoberservation(txtdatereceptionobservation.getValue());
		autoritesaudit.setNomfichierobs(txtVersionElectronique.getValue());
		autoritesaudit.setNomfichier2obs(txtVersionElectronique2.getValue());
		
			BeanLocator.defaultLookup(AuditsPrestatairesSession.class).update(autoritesaudit);
			courriers = BeanLocator.defaultLookup(CourrierAuditSession.class).findAutoriteAuditPres(idpaac,"Observation");
			idcourrier=courriers.getCourrierId();
			courrier=courriers;
			Executions.getCurrent().setAttribute("codeautoritesaudit",idpaac);
			 session.setAttribute("suiviaudit","observations");  
			 session.setAttribute("ongletsuiviaudit", "autoritesaudits");
			    loadApplicationState("suivi_audit");
		}
	}
	
	public void onClick$menuFermer() {
		loadApplicationState("suivi_audit");
		
	}
	
	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	  public void onClick$btnChoixFichier() {
			//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
				if (ToolKermel.isWindows())
					nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
				else
					nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

				txtVersionElectronique.setValue(nomFichier);
			}
	  
	  public void onClick$btnChoixFichier2() {
			//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
				if (ToolKermel.isWindows())
					nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
				else
					nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

				txtVersionElectronique2.setValue(nomFichier);
			}
	  
	  public void onClick$image() {
			step0.setVisible(false);
			step1.setVisible(true);
			String filepath = cheminDossier +  nomFichier;
			File f = new File(filepath.replaceAll("\\\\", "/"));

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(f, null, null);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (mymedia != null)
				idIframe.setContent(mymedia);
			else
				idIframe.setSrc("");

			idIframe.setHeight("600px");
			idIframe.setWidth("100%");
		}
	  
	  public void onClick$image2() {
			step0.setVisible(false);
			step1.setVisible(true);
			String filepath = cheminDossier +  nomFichier;
			File f = new File(filepath.replaceAll("\\\\", "/"));

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(f, null, null);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (mymedia != null)
				idIframe.setContent(mymedia);
			else
				idIframe.setSrc("");

			idIframe.setHeight("600px");
			idIframe.setWidth("100%");
		}

		public org.zkoss.util.media.AMedia fetchFile(File file) {

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(file, null, null);
				return mymedia;
			} catch (Exception e) {

				e.printStackTrace();
				return null;
			}

		}
		public void onClick$menuFermerstep1() {
			step0.setVisible(true);
			step1.setVisible(false);
		}
}