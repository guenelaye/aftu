package sn.ssi.kermel.web.audits.controllers;

import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.audit.ejb.AuditSession;
import sn.ssi.kermel.be.audit.ejb.AuditsPrestatairesSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygAuditsPrestataires;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



public class DossierSuivitAuditsFormController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private Long code,idpaac;
	UtilVue utilVue = UtilVue.getInstance();
	//private Label lblLibelle,lblGestion,lblStatus,lblDateStatus;
	Session session = getHttpSession();
	//private DossierMenu monDossierMenu;
	//private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
//	private Tabpanel tabProgrammation,tabVersionProvisoireRapport,tabObservationAutoriteContractante,tabVersionDefinitif;
	private Tab TAB_VersionProvisoireRapport,TAB_ObservationAutoriteContractante,TAB_VersionDefinitif;
	//private SygAutoriteContractante autorite;
	private SygAuditsPrestataires autoritesaudit;
	
	private Label lblAutorite;

	
	private Include incProgrammation;
	private SygAudit audit =new SygAudit();
	
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this); 
		//tabProgrammation.addEventListener(Events.ON_CLICK, this);
		code = (Long) session.getAttribute("numAudit");
		audit = BeanLocator.defaultLookup(AuditSession.class) .findById(code);
		
		idpaac=(Long) session.getAttribute("codeautoritesaudit");    
		autoritesaudit= BeanLocator.defaultLookup(AuditsPrestatairesSession.class).findById(idpaac);
        
		

	}

	
	public void onCreate(CreateEvent createEvent) {
		
	List<SygAuditsPrestataires> auditsprest = BeanLocator.defaultLookup(AuditsPrestatairesSession.class).find(0, -1,audit.getIdaudit(),autoritesaudit.getAutorite().getId(),null);
	lblAutorite.setValue(auditsprest.get(0).getAutorite().getDenomination());
	
	if (auditsprest.size()!=0)
	{

		  if (auditsprest.get(0).getDatedebutprevu()!=null || auditsprest.get(0).getDatefinprevu()!=null || auditsprest.get(0).getDatedebutreel()!=null || auditsprest.get(0).getDatefinreel()!=null)
		  {
			TAB_VersionProvisoireRapport.setDisabled(false);
			
		  }else{
			  TAB_VersionProvisoireRapport.setDisabled(true);
			  
		  }

			
			if (auditsprest.get(0).getDaterapportprov()!=null || auditsprest.get(0).getDateenvoirapprov()!=null || auditsprest.get(0).getNomfichierrapprov()!=null)
			{
				TAB_ObservationAutoriteContractante.setDisabled(false);
				
			}else{
				TAB_ObservationAutoriteContractante.setDisabled(true);
			}
		
			
			if (auditsprest.get(0).getDaterecepoberservation()!=null || auditsprest.get(0).getNomfichierobs()!=null)
			{
				TAB_VersionDefinitif.setDisabled(false);
				
			}else{
				TAB_VersionDefinitif.setDisabled(true);
				
			}
	}
		
		
		//Include inc = (Include) this.getFellowIfAny("incProgrammation");
         //inc.setSrc("/audits/programmation.zul");
         incProgrammation.setSrc("/audits/programmation.zul");
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	
}