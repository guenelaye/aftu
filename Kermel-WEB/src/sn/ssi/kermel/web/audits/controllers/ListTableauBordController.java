package sn.ssi.kermel.web.audits.controllers;

import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.audit.ejb.AuditSession;
import sn.ssi.kermel.be.audit.ejb.AuditsPrestatairesSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygAuditsPrestataires;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.referentiel.ejb.TypeAutoriteContractanteSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


@SuppressWarnings("serial")
public class ListTableauBordController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstAutorite;
	private Paging pgAutorite;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	private String libnombre;
	private SygAudit audits;
	private Long code;
	private Label lblNbreAutorite;
	private Integer nbrautoriteaauditer;
	
	
    public Textbox txtLibelle;
    String libelle=null,page=null,login;
 
    Session session = getHttpSession();
   
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		//Type AC
		addEventListener(ApplicationEvents.ON_TYPE_AUTORITES, this);
		
		lstAutorite.setItemRenderer(this);
		pgAutorite.setPageSize(byPage);
		pgAutorite.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_TYPE_AUTORITES);
		Events.postEvent(ApplicationEvents.ON_TYPE_AUTORITES, this, null);
		

	}

	
	public void onCreate(CreateEvent event) {
		 
		code = (Long) session.getAttribute("numAudit");
		audits = BeanLocator.defaultLookup(AuditSession.class) .findById(code);
		
          
		
		List<SygAuditsPrestataires> autoriteaauditer = BeanLocator.defaultLookup(AuditsPrestatairesSession.class).find(0, -1, audits.getIdaudit(),null,null);
		nbrautoriteaauditer=autoriteaauditer.size();
		
		lblNbreAutorite.setValue(nbrautoriteaauditer.toString());

	}
      
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_TYPE_AUTORITES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgAutorite.setPageSize(1000);
			} else {
				byPage= UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgAutorite.getActivePage() * byPage;
				pgAutorite.setPageSize(byPage);
			}
			List<SygTypeAutoriteContractante> autorites = BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).find(activePage, byPage,null,null);
			lstAutorite.setModel(new SimpleListModel(autorites));
			pgAutorite.setTotalSize(BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).count(null,null));
		} 
		
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygTypeAutoriteContractante type = (SygTypeAutoriteContractante) data;
		item.setValue(type);
		
		Listcell cellLibelle = new Listcell(type.getLibelle());
		cellLibelle.setParent(item);
		 
		Listcell cellNombre = new Listcell("");
		 
		 List<SygAuditsPrestataires> autoritecontra = BeanLocator.defaultLookup(AuditsPrestatairesSession .class).findauditprestypeaut2(0,-1,audits.getIdaudit(),type.getId());
			
		    if (autoritecontra.size() >0)
		    	libnombre = autoritecontra.size() +"";
		    else
		    	libnombre = "0";
		    cellNombre = new Listcell(libnombre);
			cellNombre.setParent(item);
		

	}

	
	
	

}