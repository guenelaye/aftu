package sn.ssi.kermel.web.audits.controllers;

import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Chart;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.PieModel;
import org.zkoss.zul.SimplePieModel;

import sn.ssi.kermel.be.audit.ejb.AuditSession;
import sn.ssi.kermel.be.audit.ejb.AuditsPrestatairesSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygAuditsPrestataires;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.referentiel.ejb.TypeAutoriteContractanteSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


@SuppressWarnings("serial")
public class GraphiqueTableauBordController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	
	private SygAudit audits;
	private Long code;
	
    Session session = getHttpSession();
    
    //Graphique
     private Chart mychart;
    // private Long chartEntrant=(long)0;
 	//private Long chartSortant=(long)0;
	//private Long ef=(long)0;
	private Integer nbrautoriteaauditer=0,ef=0;
	private int nombre=0;
   
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
	
		
		///graphique
		PieModel modelaudit = new SimplePieModel();
		
		
		code = (Long) session.getAttribute("numAudit");
		audits = BeanLocator.defaultLookup(AuditSession.class) .findById(code);
		
		
		 
		List<SygTypeAutoriteContractante> typeautorites = BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).find(0, -1,null,null);
		ef=typeautorites.size();
		 Double e=ef.doubleValue();
		for (int j = 0; j < typeautorites.size(); j++) {
			  List<SygAuditsPrestataires> nbrepartypeac = BeanLocator.defaultLookup(AuditsPrestatairesSession .class).findauditprestypeaut2(0,-1,audits.getIdaudit(),typeautorites.get(j).getId());
			
						if(nbrepartypeac.size() >0) {
							nbrautoriteaauditer = nbrepartypeac.size();
							nombre=nombre+1;
							
					}

						
			 Double pourc=nbrautoriteaauditer.doubleValue();
		
			 
			 if(nbrautoriteaauditer==0)
				 modelaudit.setValue(typeautorites.get(j).getLibelle(), new Double(0));
			 else
				 modelaudit.setValue(typeautorites.get(j).getLibelle(), (pourc/e)*100);
			 nbrautoriteaauditer=0;
		}
		
		if(nombre>0)
		mychart.setModel(modelaudit);
	}

	
	public void onCreate(CreateEvent event) {
		 
		
		
		

	}
      
	
	@Override
	public void onEvent(final Event event) throws Exception {

		
		
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		

	}

	
	
	

}