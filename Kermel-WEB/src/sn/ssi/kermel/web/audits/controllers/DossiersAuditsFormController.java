package sn.ssi.kermel.web.audits.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabpanel;

import sn.ssi.kermel.be.audit.ejb.AuditSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



public class DossiersAuditsFormController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private Long code,codeautoritesaudit;
	UtilVue utilVue = UtilVue.getInstance();
	private Label lblLibelle,lblGestion,lblDateStatus,lblNbrAutorite,lblNbrPrestaire;
	Session session = getHttpSession();
	//private DossierMenu monDossierMenu;
	private Tabpanel tabAutorites;
	private Include incAutorites,incPrestataire,incRapportGlobal;
	private SygAudit audit =new SygAudit();
	private String ongletsuiviaudit;
	private Tab TAB_Autorites,TAB_Prestataire,TAB_RapportGlobal;

	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		//pour afficher l'onglet qu'on veut
		ongletsuiviaudit = (String) session.getAttribute("ongletsuiviaudit");
		
		
		
		tabAutorites.addEventListener(Events.ON_CLICK, this);
		code = (Long) session.getAttribute("numAudit");
		audit = BeanLocator.defaultLookup(AuditSession.class) .findById(code);
		lblLibelle.setValue(audit.getLibelleaudit());
		lblGestion.setValue(audit.getGestion().toString());
		lblNbrAutorite.setValue(audit.getNombreautorite().toString());
		lblNbrPrestaire.setValue(audit.getNombreprestataire().toString());
		lblDateStatus.setValue(UtilVue.getInstance().formateLaDate(audit.getDatestatut()));
//		monDossierMenu.setProfil((String) getHttpSession().getAttribute("profil"));
//		monDossierMenu.setDosCode(audit.getIdaudit());
//        monDossierMenu.setState(audit.getState().getCode());
//        monDossierMenu.afterCompose();

	}

	
	public void onCreate(CreateEvent createEvent) {
		
				if((ongletsuiviaudit!=null)){
					  if( (ongletsuiviaudit.equals("ajoutautoriteprest")) || (ongletsuiviaudit.equals("supautoritepres"))
							  || (ongletsuiviaudit.equals("actioncontrat"))){
						  TAB_Prestataire.setSelected(true);
				    	  incPrestataire.setSrc("/audits/contratsprestataires.zul");
					  }
					  else if(ongletsuiviaudit.equals("RapportGlobal")){
						  TAB_RapportGlobal.setSelected(true);
							incRapportGlobal.setSrc("/audits/rapportglobal.zul");
					  }
					  
					  else
					  {
						  Include inc = (Include) this.getFellowIfAny("incAutorites");
					         inc.setSrc("/audits/autoritescontractantes.zul"); 
					  }
			    
				}
				else
				{
					 Include inc = (Include) this.getFellowIfAny("incAutorites");
			         inc.setSrc("/audits/autoritescontractantes.zul");
				}
		
				
		
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	
}