package sn.ssi.kermel.web.pidrp.controllers;


import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAttributions;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierssouscriteresevaluateurs;
import sn.ssi.kermel.be.entity.SygGarantiesDossiers;
import sn.ssi.kermel.be.entity.SygMontantsSeuils;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;
import sn.ssi.kermel.be.entity.SygTachesEffectues;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.AttributionsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.ContratsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DocumentsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersSouscriteresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.be.referentiel.ejb.MontantsSeuilsSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



/**

 * @author Adama samb
 * @since mardi 11 Janvier 2011, 12:34
 
 */
public class SuiviProcedureMarcheFormController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private Include pgActes;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	Session session = getHttpSession();
	private Label lblObjets,lblModePassation,lblMontant,lblModeSelection,lblType,lgtitre,lblDatecreation;
	private String LibelleTab,libelle,avalider="oui";
	private Tab TAB_TDOSSIERS;
	private Tree tree;
	private Treecell celltdossiers,cellconsultationfournisseur,cellpreparation,cellselectionfournisseur,celllettreinvitation,cellenrgistrementouvertureplis,
	cellregistredepot,celllistepresencemembrescommissions,cellrepresentantssoumissionnaires,cellrepresentantsservicestechniques,cellobservateursindependants,
	cellpiecessoumissionnaires,celllecturesoffres,cellprocesverbalouverture,cellcompositioncommissiontechnique,cellincidents,cellouvertureplis,
	cellevaluationoffre,celltransmissiondossier,cellverificationconformite,celltreenotetechnique,cellrapportevaluation,cellclassement,
	cellevaluationattribution,cellrepresentantssoumissionnairesfinancieres,celllistepresencemembrescommissionsfinancieres,celloffresfinancieres,cellsaisiprixevalues,
	cellclassementfinanciere,cellattributionprovisoire,cellnotification,cellouvertureoffrefinancieres,cellclassementfinal,cellimmatriculation;
	private Include idinclude;
	private Treeitem treetdossiers,treeconsultationfournisseur,treeevaluationattribution,treeenrgistrementouvertureplis,treepreparation,treeselectionfournisseur,
	treelettreinvitation,treeregistredepot,treeouvertureplis,treelistepresencemembrescommissions,treerepresentantssoumissionnaires,treerepresentantsservicestechniques,
	treeobservateursindependants,treepiecessoumissionnaires,treelecturesoffres,treeprocesverbalouverture,treecompositioncommissiontechnique,treeincidents,
	treetransmissiondossier,treeverificationconformite,treetreenotetechnique,treerapportevaluation,treeclassementfinal,treeclassement,treeouvertureoffrefinancieres,
	treeevaluationoffre,treelistepresencemembrescommissionsfinancieres,treerepresentantssoumissionnairesfinancieres,treeoffresfinancieres,treesaisiprixevalues,treenotification,
	treeattributionprovisoire,itemimmatriculation;
	private Treerow rowinfosgenerales;
	private Treeitem treeitem;
	List<SygGarantiesDossiers> garanties = new ArrayList<SygGarantiesDossiers>();
	
	
	
	List<SygRealisationsBailleurs> bailleurs = new ArrayList<SygRealisationsBailleurs>();
	List<SygRetraitregistredao> registreretrait = new ArrayList<SygRetraitregistredao>();
	List<SygPlisouvertures> registredepot = new ArrayList<SygPlisouvertures>();
	
	
	

	
	List<SygPlisouvertures> lecturesoffres = new ArrayList<SygPlisouvertures>();
	List<SygMontantsSeuils> seuils = new ArrayList<SygMontantsSeuils>();
    SygAutoriteContractante autorite=new SygAutoriteContractante();
    SygPlisouvertures soumissionnaires=new SygPlisouvertures();
    List<SygDocuments> documents = new ArrayList<SygDocuments>();
	SygDossiers dossier=new SygDossiers();
	private int nbredossierap=0,nbrefournselectionnes=0,nbrelettreinvitation=0,nbredepot=0,nbreouvertureplis=0,nombreev=0,nombreoof=0,nombreea=0,
			nbreautres=0,nbreoffresfinancieres=0,nombreimmatriculation=0;
	SygAttributions attributaire=new SygAttributions();
	
	List<SygPlisouvertures> plis = new ArrayList<SygPlisouvertures>();
	List<SygDossierssouscriteresevaluateurs> notes = new ArrayList<SygDossierssouscriteresevaluateurs>();
	
	
	List<SygPlisouvertures> lecturesoffresfinancieres = new ArrayList<SygPlisouvertures>();
	SygContrats contrat=new SygContrats();
	SygTachesEffectues tache=null;
		
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		


	}

	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		LibelleTab=(String) session.getAttribute("libelle");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		
		lblObjets.setValue(appel.getApoobjet());
		lblModePassation.setValue(appel.getModepassation().getLibelle());
		lblMontant.setValue(ToolKermel.format2Decimal(appel.getApomontantestime()));
		lblModeSelection.setValue(appel.getModeselection().getLibelle());
		lblType.setValue(appel.getTypemarche().getLibelle());
		
		lblDatecreation.setValue(UtilVue.getInstance().formateLaDate2(appel.getApodatecreation()));
		
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		
		seuils = BeanLocator.defaultLookup(MontantsSeuilsSession.class).find(0,-1, autorite.getType(), appel.getTypemarche(), appel.getModepassation(), null, null,UIConstants.TYPE_SEUILSRAPRIORI);
		
		if(seuils.size()>0)
		{
			if(seuils.get(0).getMontantinferieur()!=null)
			{
				if((appel.getApomontantestime().compareTo(seuils.get(0).getMontantinferieur())==1)||(appel.getApomontantestime().equals(seuils.get(0).getMontantinferieur())))
		
				{
					avalider="oui";
				//	itemvalidationdossier.setVisible(false);
				}
				else
				{
					avalider="non";
				}
					
			}
			
		}
		if(dossier!=null)
		{
	
			tache=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Taches(dossier.getDosID());
			session.setAttribute("tache",tache);
			celltdossiers.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			cellpreparation.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			cellconsultationfournisseur.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			celltdossiers.setImage("/images/puce.png");
			cellpreparation.setImage("/images/puce.png");
			cellconsultationfournisseur.setImage("/images/puce.png");
			
			
			registredepot=BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,-1,-1,-1, -1, -1,-1, null, -1, null, null);
		
		
			lecturesoffres = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,-1,UIConstants.PARENT,-1, -1, -1,-1, null, -1, null, null);
			plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0, -1, dossier, null,null,null,UIConstants.PARENT,-1,-1, -1, -1,-1, null, -1, null, null);
		//	lecturesoffresfinancieres= BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,-1,UIConstants.PARENT,-1, -1, -1,-1, null);
			lecturesoffresfinancieres= BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,null,dossier, 1);
			
			attributaire=BeanLocator.defaultLookup(AttributionsSession.class).findAttributaire(dossier, null,null);
			if(tache.getPieceadministrative()==1)
				nbredossierap=nbredossierap+ 1;
			if(tache.getCriterequalification()==1)
				nbredossierap=nbredossierap+ 1;
			if(tache.getDevise()==1)
				nbredossierap=nbredossierap+ 1;
			if(tache.getPreselection()==1)
			{
				nbrefournselectionnes=nbrefournselectionnes+1;
				cellselectionfournisseur.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellselectionfournisseur.setImage("/images/rdo_on.png");
			}
			
			if(nbredossierap==3)
			{
				cellconsultationfournisseur.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellconsultationfournisseur.setImage("/images/rdo_on.png");
			}
			if(dossier.getDosdateinvitation()!=null)
			{
				celllettreinvitation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				nbrelettreinvitation=nbrelettreinvitation+1;
				celllettreinvitation.setImage("/images/rdo_on.png");
			}
			if(nbredossierap+nbrefournselectionnes+nbrelettreinvitation==5)
			{
				cellpreparation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellpreparation.setImage("/images/rdo_on.png");
			}
			if(registredepot.size()>0)
			{
				cellregistredepot.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellenrgistrementouvertureplis.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellregistredepot.setImage("/images/rdo_on.png");
				cellenrgistrementouvertureplis.setImage("/images/puce.png");
				nbredepot=nbredepot+1;
			}
		
			if(tache.getCommissionspassation()==1)
			{
				nbreouvertureplis=nbreouvertureplis+ 1;
				celllistepresencemembrescommissions.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				celllistepresencemembrescommissions.setImage("/images/rdo_on.png");
			}
			if(tache.getRepresentantssoumis()==1)
			{
				nbreouvertureplis=nbreouvertureplis+ 1;
				cellrepresentantssoumissionnaires.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellrepresentantssoumissionnaires.setImage("/images/rdo_on.png");
			}
			if(tache.getServicestechniques()==1)
			{
				nbreouvertureplis=nbreouvertureplis+ 1;
				cellrepresentantsservicestechniques.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellrepresentantsservicestechniques.setImage("/images/rdo_on.png");
			}
			if(tache.getObservateurs()==1)
			{
				nbreouvertureplis=nbreouvertureplis+ 1;
				cellobservateursindependants.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellobservateursindependants.setImage("/images/rdo_on.png");
			}
			if(tache.getPiecessoumis()==1)	
			{
				nbreouvertureplis=nbreouvertureplis+ 1;
				cellpiecessoumissionnaires.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellpiecessoumissionnaires.setImage("/images/rdo_on.png");
			}
			if(tache.getCommissiontecniques()==1)	
			{
				nbreouvertureplis=nbreouvertureplis+ 1;
				cellcompositioncommissiontechnique.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellcompositioncommissiontechnique.setImage("/images/rdo_on.png");
			}
			
			if(lecturesoffres.size()>0)	
			{
				nbreouvertureplis=nbreouvertureplis+ 1;
				celllecturesoffres.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				celllecturesoffres.setImage("/images/rdo_on.png");
			}
			if(dossier.getDosIncidents()!=null)
			{
				cellincidents.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellincidents.setImage("/images/rdo_on.png");
			}
			if(appel.getApoDatepvouverturepli()!=null)
			{
				nbreouvertureplis=nbreouvertureplis+ 1;
				cellprocesverbalouverture.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellprocesverbalouverture.setImage("/images/rdo_on.png");
			}
			if(nbreouvertureplis>0)
			{
				cellenrgistrementouvertureplis.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			    cellouvertureplis.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			    cellenrgistrementouvertureplis.setImage("/images/puce.png");
			    cellouvertureplis.setImage("/images/puce.png");
				if(nbreouvertureplis==8)
				{
					cellouvertureplis.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellouvertureplis.setImage("/images/rdo_on.png");
				}
					
				nbreouvertureplis=nbreouvertureplis+ nbredepot;
				  if(nbreouvertureplis==9)
				  {
					  cellenrgistrementouvertureplis.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					  cellincidents.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					  cellenrgistrementouvertureplis.setImage("/images/rdo_on.png");
				  }
					 
				  if(appel.getApoDatepvouverturepli()!=null)
					  cellouvertureplis.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.plisouvertle")+" "+UtilVue.getInstance().formateLaDate2(appel.getApoDatepvouverturepli()));

			}	
			if(dossier.getDateRemiseDossierTechnique()!=null||dossier.getDateLimiteDossierTechnique()!=null)
			{
				nombreev=nombreev+1;
				celltransmissiondossier.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellevaluationoffre.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellevaluationattribution.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				celltransmissiondossier.setImage("/images/rdo_on.png");
				cellevaluationoffre.setImage("/images/puce.png");
				cellevaluationattribution.setImage("/images/puce.png");
			}
			if(plis.size()>0)
			{
				nombreev=nombreev+1;
				cellverificationconformite.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellverificationconformite.setImage("/images/rdo_on.png");
				cellevaluationoffre.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellevaluationattribution.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellevaluationoffre.setImage("/images/puce.png");
				cellevaluationattribution.setImage("/images/puce.png");
			}
			notes=BeanLocator.defaultLookup(DossiersSouscriteresSession.class).ListesNotes(0, -1, dossier,null, null, null);
			if(notes.size()>0)
			{
				nombreev=nombreev+1;
				celltreenotetechnique.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellevaluationoffre.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellevaluationattribution.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellevaluationoffre.setImage("/images/puce.png");
				cellevaluationattribution.setImage("/images/puce.png");
				celltreenotetechnique.setImage("/images/rdo_on.png");
			}
			documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossier, appel,UIConstants.PARAM_TYPEDOCUMENTS_EVALUATION,null, null);
			if(documents.size()>0)
			{
				nombreev=nombreev+1;
				cellrapportevaluation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellevaluationoffre.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellevaluationattribution.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellevaluationoffre.setImage("/images/puce.png");
				cellevaluationattribution.setImage("/images/puce.png");
				cellrapportevaluation.setImage("/images/rdo_on.png");
			}
			if(nombreev==4)
			{
				cellevaluationoffre.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellclassement.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellevaluationoffre.setImage("/images/rdo_on.png");
				cellclassement.setImage("/images/rdo_on.png");
			}
			if(tache.getRepresentantssoumis()==1)	
			{
				nombreoof=nombreoof+ 2;
				cellrepresentantssoumissionnairesfinancieres.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellrepresentantssoumissionnairesfinancieres.setImage("/images/rdo_on.png");
				nbreoffresfinancieres=nbreoffresfinancieres+1;
			
				celloffresfinancieres.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				celloffresfinancieres.setImage("/images/rdo_on.png");
				cellsaisiprixevalues.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellsaisiprixevalues.setImage("/images/rdo_on.png");
				cellclassementfinanciere.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellclassementfinanciere.setImage("/images/rdo_on.png");
			}
			if(tache.getCommissionspassation()==1)
			{
				nombreoof=nombreoof+ 1;
				nbreoffresfinancieres=nbreoffresfinancieres+1;
				celllistepresencemembrescommissionsfinancieres.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				celllistepresencemembrescommissionsfinancieres.setImage("/images/rdo_on.png");
			}
			if(attributaire!=null)
			{
				cellattributionprovisoire.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellattributionprovisoire.setImage("/images/rdo_on.png");
				cellclassementfinal.setImage("/images/rdo_on.png");
				cellclassementfinal.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				nombreea=nombreea+1;
			}
			if(dossier.getDosDateNotification()!=null)
			{
				cellnotification.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellnotification.setImage("/images/rdo_on.png");
				nombreea=nombreea+1;
				cellnotification.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.notificationmarche.notifiele")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateNotification()));
				contrat=BeanLocator.defaultLookup(ContratsSession.class).getContrat(dossier,autorite, null,null,null);
			}
			if(lecturesoffres.size()>0)
			{
				nombreoof=nombreoof+1;
				celllecturesoffres.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				celllecturesoffres.setImage("/images/rdo_on.png");
			}
			if(tache.getServicestechniques()==1)
			{
				nombreoof=nombreoof+1;
//				                  
			}

			if(nbreoffresfinancieres>0)
			{
				cellouvertureoffrefinancieres.setImage("/images/puce.png");
				cellouvertureoffrefinancieres.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				if(nbreoffresfinancieres==2)
				{
					cellouvertureoffrefinancieres.setImage("/images/rdo_on.png");
					cellouvertureoffrefinancieres.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				}
			}
			if(contrat!=null)
			{
				if(contrat.getDatedemandeimmatriculation()!=null)
				{
					cellimmatriculation.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
					cellimmatriculation.setImage("/images/puce.png");
					cellimmatriculation.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.demande").substring(0, 15)+"...");
					cellimmatriculation.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.demande")+" "+UtilVue.getInstance().formateLaDate2(contrat.getDatedemandeimmatriculation()));

				}
				if(contrat.getDateimmatriculation()!=null)
				{
					cellimmatriculation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					nombreimmatriculation=nombreimmatriculation+1;
					cellimmatriculation.setImage("/images/rdo_on.png");
					cellimmatriculation.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.marche").substring(0, 15)+"...");
					cellimmatriculation.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.marche")+" "+UtilVue.getInstance().formateLaDate2(contrat.getDateimmatriculation()));
					
				}
			}
			if(nombreea+nombreoof+nombreev+nombreimmatriculation>=11)
			{
				cellevaluationattribution.setImage("/images/rdo_on.png");
				cellevaluationattribution.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				
			}
			if(nombreea+nombreoof+nombreev+nbredossierap+nbrefournselectionnes+nbrelettreinvitation+nbreouvertureplis+nombreimmatriculation>=26)
			{
				celltdossiers.setImage("/images/rdo_on.png");
				celltdossiers.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
			}
//			if(nombreev+nombreoof+nombreeap==12)
//			{
//				cellouvertureevaluationoffresfin.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
//				cellclassementfinal.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
//			}
		}
		if(LibelleTab!=null)
		{
			if(LibelleTab.equals("traitementsdossiers"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/traitementsdossiers.zul");	
				 tree.setSelectedItem(treetdossiers);    
			}
			if((LibelleTab.equals("infogenerales"))||(LibelleTab.equals("devise"))||(LibelleTab.equals("piecesadministratives"))
					||(LibelleTab.equals("criteresqualifications"))||(LibelleTab.equals("allotissement"))||(LibelleTab.equals("presentationsoffres")))
			{
				idinclude.setSrc("/passationsmarches/pidrp/dossierconsultation.zul");	
				 tree.setSelectedItem(treeconsultationfournisseur);  
				 TreeTreepreparation();
			}
			if(LibelleTab.equals("fournisseursselectionnes"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/fournisseurs.zul");	
				 tree.setSelectedItem(treeselectionfournisseur); 
				 TreeTreepreparation();
			}
			if(LibelleTab.equals("lettreinvitation"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/formlettreinvitation.zul");	
				 tree.setSelectedItem(treelettreinvitation); 
				 TreeTreepreparation();
			}
			if(LibelleTab.equals("plisouvertures"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/registredepot.zul")	;
				 tree.setSelectedItem(treeregistredepot); 
				 TreeTreeenregistrementplis();
			}
			if(LibelleTab.equals("listepresencemembrescommissions"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/listepresencemembrescommissions.zul");
				tree.setSelectedItem(treelistepresencemembrescommissions);
				TreeTreeenregistrementplis();
				
			}
			if(LibelleTab.equals("representantssoumissionnaires"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/representantssoumissionnaires.zul");
				tree.setSelectedItem(treerepresentantssoumissionnaires);
				TreeTreeenregistrementplis();
				
			}
			if(LibelleTab.equals("procesverbalouverture"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/procesverbalouverture.zul");
				tree.setSelectedItem(treeprocesverbalouverture);
				TreeTreeenregistrementplis();
			}
			if(LibelleTab.equals("incidents"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/incidents.zul");
				tree.setSelectedItem(treeincidents);
				
			}
			if(LibelleTab.equals("compositioncommissiontechnique"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/compositioncommissiontechnique.zul");
				tree.setSelectedItem(treecompositioncommissiontechnique);
				TreeTreeenregistrementplis();
				
			}
			if(LibelleTab.equals("lecturesoffres"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/formlecturesoffres.zul");
				tree.setSelectedItem(treelecturesoffres);
				TreeTreeenregistrementplis();
				
			}
			if(LibelleTab.equals("observateursindependants"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/observateursindependants.zul");
				tree.setSelectedItem(treeobservateursindependants);
				TreeTreeenregistrementplis();
				
			}
			if(LibelleTab.equals("piecessoumissionnaires"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/piecessoumissionnaires.zul");
				tree.setSelectedItem(treepiecessoumissionnaires);
				TreeTreeenregistrementplis();
				
			}
			if(LibelleTab.equals("representantsservicestechniques"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/representantsservicestechniques.zul");
				tree.setSelectedItem(treerepresentantsservicestechniques);
				TreeTreeenregistrementplis();
				
			}
			if(LibelleTab.equals("incidents"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/incidents.zul");
				tree.setSelectedItem(treeincidents);
				TreeTreeenregistrementplis();
				
			}
			if(LibelleTab.equals("formtransmissiondossier"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/formtransmissiondossier.zul");
				tree.setSelectedItem(treetransmissiondossier);
				TreeTreeevaluationattribution();
				treeouvertureoffrefinancieres.setOpen(false);
				treeevaluationoffre.setOpen(true);
			}
			if(LibelleTab.equals("formverificationconformite"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/formverificationconformite.zul");
				tree.setSelectedItem(treeverificationconformite);
				TreeTreeevaluationattribution();
				treeouvertureoffrefinancieres.setOpen(false);
				treeevaluationoffre.setOpen(true);
			}
			if(LibelleTab.equals("rapportevaluation"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/rapportevaluation.zul");
				tree.setSelectedItem(treerapportevaluation);
				TreeTreeevaluationattribution();
			}
			if(LibelleTab.equals("formclassementfinal"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/formclassementfinal.zul");
				tree.setSelectedItem(treeclassementfinal);
				TreeTreeevaluationattribution();
				idinclude.setSrc("/passationsmarches/pidrp/formclassement.zul");
			}
			if(LibelleTab.equals("saisienotetechnique"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/formsaisienotetechnique.zul");
				tree.setSelectedItem(treetreenotetechnique);
				TreeTreeevaluationattribution();
			}
			if(LibelleTab.equals("classementfinanciere"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/formclassementfinanciere.zul");
				tree.setSelectedItem(treeclassement);
				TreeTreeevaluationattribution();
				treeevaluationoffre.setOpen(false);
				treeouvertureoffrefinancieres.setOpen(true);
			}
			if(LibelleTab.equals("listepresencemembrescommissionsfinancieres"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/listepresencemembrescommissionsfinancieres.zul");	
				tree.setSelectedItem(treelistepresencemembrescommissionsfinancieres);
				TreeTreeevaluationattribution();
				treeevaluationoffre.setOpen(false);
				treeouvertureoffrefinancieres.setOpen(true);
			}
			if(LibelleTab.equals("representantssoumissionnairesfinancieres"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/representantssoumissionnairesfinancieres.zul");	
				tree.setSelectedItem(treerepresentantssoumissionnairesfinancieres);
				TreeTreeevaluationattribution();
				treeevaluationoffre.setOpen(false);
				treeouvertureoffrefinancieres.setOpen(true);
			}
			if(LibelleTab.equals("offresfinancieres"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/offresfinancieres.zul");	
				tree.setSelectedItem(treeoffresfinancieres);
				TreeTreeevaluationattribution();
				treeevaluationoffre.setOpen(false);
				treeouvertureoffrefinancieres.setOpen(true);
			}
			if(LibelleTab.equals("saisiprixevalues"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/formcorrectionoffre.zul");	
				tree.setSelectedItem(treesaisiprixevalues);
				TreeTreeevaluationattribution();
			}
			if(LibelleTab.equals("evaluationattribution"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/evaluationoffres.zul");	
				tree.setSelectedItem(treeevaluationoffre);
				TreeTreeevaluationattribution();
				treeevaluationoffre.setOpen(true);
				treeouvertureoffrefinancieres.setOpen(false);
			}
			if(LibelleTab.equals("ouvertureoffrefinancieres"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/ouverturesoffresfinancieres.zul");
				tree.setSelectedItem(treeouvertureoffrefinancieres);
				TreeTreeevaluationattribution();
				treeevaluationoffre.setOpen(false);
				treeouvertureoffrefinancieres.setOpen(true);
			}
			if(LibelleTab.equals("classement"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/formclassement.zul");
				tree.setSelectedItem(treeclassement);
				TreeTreeevaluationattribution();
				treeevaluationoffre.setOpen(true);
			}
			if(LibelleTab.equals("attributionprovisoire"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/attributionprovisoire.zul");
				tree.setSelectedItem(treeattributionprovisoire);
				TreeTreeevaluationattribution();
				treeevaluationoffre.setOpen(false);
				treeouvertureoffrefinancieres.setOpen(false);
			}
			if(LibelleTab.equals("notificationmarche"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/formnotificationmarche.zul");
				tree.setSelectedItem(treenotification);
				TreeTreeevaluationattribution();
				treeevaluationoffre.setOpen(false);
				treeouvertureoffrefinancieres.setOpen(false);
			}
			if(LibelleTab.equals("immatriculation"))
			{
				idinclude.setSrc("/passationsmarches/pidrp/formdemandeimmatriculation.zul");
				tree.setSelectedItem(itemimmatriculation);
				TreeImmatriculation();
			}
		}
		else
		{
			idinclude.setSrc("/passationsmarches/pidrp/dossierappeloffre.zul");	
			 tree.setSelectedItem(treetdossiers);     
		}
		  
		lgtitre.setValue(Labels.getLabel("kermel.plansdepassation.infos.generales.referencedossier")+": "+appel.getAporeference());
	
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}


	public void onSelect$tree() throws Exception
	{
		InfosTree();
	}
	
	public void TreeTreepreparation()
	{
		
		treepreparation.setOpen(true);
		treeenrgistrementouvertureplis.setOpen(false);
		treeevaluationattribution.setOpen(false);
		
	}
	
	public void TreeTreedossiers()
	{
		
		treepreparation.setOpen(false);
		treeenrgistrementouvertureplis.setOpen(false);
		treeevaluationattribution.setOpen(false);
		
	}
	public void TreeTreeenregistrementplis()
	{
		
		treepreparation.setOpen(false);
		treeenrgistrementouvertureplis.setOpen(true);
		treeevaluationattribution.setOpen(false);
		
	}

	public void TreeTreeevaluationattribution()
	{
		
		treepreparation.setOpen(false);
		treeenrgistrementouvertureplis.setOpen(false);
		treeevaluationattribution.setOpen(true);
		
	}
	public void TreeAutres()
	{
		treepreparation.setOpen(false);
		treeenrgistrementouvertureplis.setOpen(false);
		treeevaluationattribution.setOpen(false);
	}
	public void InfosTree() throws Exception{
		
		/////////////Preparation du dossier//////////
		if(tree.getSelectedItem().getId().equals("treepreparation"))
		{
			TreeTreepreparation();
			idinclude.setSrc("/passationsmarches/pidrp/dossierspreparation.zul");
			TreeTreepreparation();
		}
		if(tree.getSelectedItem().getId().equals("treeconsultationfournisseur"))
		{
			TreeTreepreparation();
			idinclude.setSrc("/passationsmarches/pidrp/dossierconsultation.zul");
		}
		if(tree.getSelectedItem().getId().equals("treetdossiers"))
		{
			TreeTreedossiers();
			idinclude.setSrc("/passationsmarches/pidrp/traitementsdossiers.zul");
		}
		if(tree.getSelectedItem().getId().equals("treeselectionfournisseur"))
		{
			TreeTreepreparation();
			idinclude.setSrc("/passationsmarches/pidrp/fournisseurs.zul");
		}
		
		if(tree.getSelectedItem().getId().equals("treelettreinvitation"))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				if(tache.getPreselection()==1)
				{
					 TreeTreepreparation();
					 idinclude.setSrc("/passationsmarches/pidrp/formlettreinvitation.zul");
				}
				else
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.travauxdrp.candidats.ainvites")+" "+dossier.getDosSoumission(), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				}
					
			
			}
			
		}
		if(tree.getSelectedItem().getId().equals("treeregistredepot"))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
			}
			else
			{
				if(tache.getPreselection()==1)
				{
					TreeTreeenregistrementplis();
					idinclude.setSrc("/passationsmarches/pidrp/registredepot.zul");
				}
				else
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.saisirregistreretrait"), "Erreur", Messagebox.OK, Messagebox.ERROR);

				}
			}
			
		}
		if((tree.getSelectedItem().getId().equals("treeenrgistrementouvertureplis")))
		{
			TreeTreeenregistrementplis();
			 idinclude.setSrc("/passationsmarches/pidrp/dossiersouverturesplis.zul");
		}
		if(tree.getSelectedItem().getId().equals("treelistepresencemembrescommissions"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/pidrp/listepresencemembrescommissions.zul");
			treeouvertureplis.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treerepresentantssoumissionnaires"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/pidrp/representantssoumissionnaires.zul");
			treeouvertureplis.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treerepresentantsservicestechniques"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/pidrp/representantsservicestechniques.zul");
			treeouvertureplis.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treeobservateursindependants"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/pidrp/observateursindependants.zul");
			treeouvertureplis.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treepiecessoumissionnaires"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/pidrp/piecessoumissionnaires.zul");
			treeouvertureplis.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treelecturesoffres"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/pidrp/formlecturesoffres.zul");
			treeouvertureplis.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treeprocesverbalouverture"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/pidrp/procesverbalouverture.zul");
			treeouvertureplis.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treecompositioncommissiontechnique"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/pidrp/compositioncommissiontechnique.zul");
			treeouvertureplis.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treeincidents"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/pidrp/incidents.zul");
			treeouvertureplis.setOpen(true);
		}	

		if(tree.getSelectedItem().getId().equals("treeouvertureplis"))
		{
			TreeTreeenregistrementplis();
			idinclude.setSrc("/passationsmarches/pidrp/ouvertureplis.zul");
			
		}
		if((tree.getSelectedItem().getId().equals("treetransmissiondossier")))
		{
			TreeTreeevaluationattribution();
			idinclude.setSrc("/passationsmarches/pidrp/formtransmissiondossier.zul");
			
		}	
		if((tree.getSelectedItem().getId().equals("treeverificationconformite")))
		{
			TreeTreeevaluationattribution();
			idinclude.setSrc("/passationsmarches/pidrp/formverificationconformite.zul");
			
		}	
		if((tree.getSelectedItem().getId().equals("treerapportevaluation")))
		{
			TreeTreeevaluationattribution();
			idinclude.setSrc("/passationsmarches/pidrp/rapportevaluation.zul");
		}	
		if((tree.getSelectedItem().getId().equals("treeclassement")))
		{
			TreeTreeevaluationattribution();
			idinclude.setSrc("/passationsmarches/pidrp/formclassement.zul");
			
		}	
		if(tree.getSelectedItem().getId().equals("treetreenotetechnique"))
		{
			TreeTreeevaluationattribution();
			idinclude.setSrc("/passationsmarches/pidrp/formsaisienotetechnique.zul");
		}
		if(tree.getSelectedItem().getId().equals("itemevaluationattributionprovisoire"))
		{
			TreeTreeevaluationattribution();
			idinclude.setSrc("/passationsmarches/pidrp/dossiersevaluationsdp.zul");
		}
		
		if(tree.getSelectedItem().getId().equals("treeclassementfinanciere"))
		{
			TreeTreeevaluationattribution();
			idinclude.setSrc("/passationsmarches/pidrp/formclassementfinanciere.zul");
			treeouvertureoffrefinancieres.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treeattributionprovisoire"))
		{
			TreeTreeevaluationattribution();
			idinclude.setSrc("/passationsmarches/pidrp/attributionprovisoire.zul");
			treeouvertureoffrefinancieres.setOpen(false);
		}
		if(tree.getSelectedItem().getId().equals("treenotification"))
		{
			TreeTreeevaluationattribution();
			idinclude.setSrc("/passationsmarches/pidrp/formnotificationmarche.zul");
		}
		if(tree.getSelectedItem().getId().equals("treeevaluationoffre"))
		{
			TreeTreeevaluationattribution();
			idinclude.setSrc("/passationsmarches/pidrp/evaluationoffres.zul");
			treeevaluationoffre.setOpen(true);
		}
		if(tree.getSelectedItem().getId().equals("treeevaluationattribution"))
		{
			TreeTreeevaluationattribution();
			treeevaluationoffre.setOpen(false);
			treeouvertureoffrefinancieres.setOpen(false);
			idinclude.setSrc("/passationsmarches/pidrp/dossiersevaluationsattribution.zul");
		}
		if(tree.getSelectedItem().getId().equals("treeouvertureoffrefinancieres"))
		{
			TreeTreeevaluationattribution();
			idinclude.setSrc("/passationsmarches/pidrp/ouverturesoffresfinancieres.zul");
			 treeouvertureoffrefinancieres.setOpen(true);
			 treeevaluationoffre.setOpen(false);
		}
		if(tree.getSelectedItem().getId().equals("treelistepresencemembrescommissionsfinancieres"))
		{
			TreeTreeevaluationattribution();
			idinclude.setSrc("/passationsmarches/pidrp/listepresencemembrescommissionsfinancieres.zul");
			 treeouvertureoffrefinancieres.setOpen(true);
			 treeevaluationoffre.setOpen(false);
		}
		if(tree.getSelectedItem().getId().equals("treerepresentantssoumissionnairesfinancieres"))
		{
			TreeTreeevaluationattribution();
			idinclude.setSrc("/passationsmarches/pidrp/representantssoumissionnairesfinancieres.zul");
			 treeouvertureoffrefinancieres.setOpen(true);
			 treeevaluationoffre.setOpen(false);
		}
		if(tree.getSelectedItem().getId().equals("treeoffresfinancieres"))
		{
			TreeTreeevaluationattribution();
			idinclude.setSrc("/passationsmarches/pidrp/offresfinancieres.zul");
			 treeouvertureoffrefinancieres.setOpen(true);
			 treeevaluationoffre.setOpen(false);
		}
		if(tree.getSelectedItem().getId().equals("treesaisiprixevalues"))
		{
			TreeTreeevaluationattribution();
			idinclude.setSrc("/passationsmarches/pidrp/formcorrectionoffre.zul");
			 treeouvertureoffrefinancieres.setOpen(true);
			 treeevaluationoffre.setOpen(false);
		}
		if(tree.getSelectedItem().getId().equals("treeclassementfinal"))
		{
			TreeTreeevaluationattribution();
			idinclude.setSrc("/passationsmarches/pidrp/formclassementfinal.zul");
			 treeouvertureoffrefinancieres.setOpen(false);
		}
		if((tree.getSelectedItem().getId().equals("treerecours")))
		{
			TreeAutres();
			idinclude.setSrc("/passationsmarches/pidrp/recours.zul");
		}
		if((tree.getSelectedItem().getId().equals("treedocuments")))
		{
			TreeAutres();
			idinclude.setSrc("/passationsmarches/pidrp/documents.zul");
		}
		if(tree.getSelectedItem().getId().equals("itemimmatriculation"))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				if(dossier.getDosDateNotification()==null)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.notifierdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
					 TreeImmatriculation();
			       idinclude.setSrc("/passationsmarches/pidrp/formdemandeimmatriculation.zul");
				 }
			}
		}	
	}
	
	public void TreeImmatriculation()
	{
		treepreparation.setOpen(false);
		treeenrgistrementouvertureplis.setOpen(false);
		treeevaluationattribution.setOpen(false);
		
		
	}

}