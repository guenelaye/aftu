package sn.ssi.kermel.web.pidrp.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossiersFournisseurs;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrederetraitdaoSession;
import sn.ssi.kermel.be.plansdepassation.ejb.DossiersFournisseursSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class DossiersOuverturePlisController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code;
	private Tab TAB_infosgenerales,TAB_garanties,TAB_piecesadministratives,TAB_criteresqualifications,TAB_devise,TAB_financement;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblInfos;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygDossiers dossier=new SygDossiers();
	List<SygRetraitregistredao> registreretrait = new ArrayList<SygRetraitregistredao>();
	List<SygDossiersFournisseurs> fournisseursselectionnes = new ArrayList<SygDossiersFournisseurs>();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier!=null)
		{
			if(dossier.getDosDatePublication()!=null)
			{
			 registreretrait = BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).find(0,-1,dossier);
			}
		}
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	
	
	
	public void onClick$registredepot() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		}
		else
		{
			fournisseursselectionnes = BeanLocator.defaultLookup(DossiersFournisseursSession.class).find(0,-1,null,appel,null);
			if(fournisseursselectionnes.size()==dossier.getDosSoumission())
			{
				session.setAttribute("libelle", "plisouvertures");
				loadApplicationState("suivi_pi_drp");
			}
			else
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.saisirregistreretrait"), "Erreur", Messagebox.OK, Messagebox.ERROR);

				
			}
			
		}
	}
	
	public void onClick$listepresencemembrescommissions() {
		session.setAttribute("libelle", "listepresencemembrescommissions");
		loadApplicationState("suivi_pi_drp");
	}
	public void onClick$representantssoumissionnaires() {
		session.setAttribute("libelle", "representantssoumissionnaires");
		loadApplicationState("suivi_pi_drp");
	}
	public void onClick$representantsservicestechniques() {
		session.setAttribute("libelle", "representantsservicestechniques");
		loadApplicationState("suivi_pi_drp");
	}
	public void onClick$observateursindependants() {
		session.setAttribute("libelle", "observateursindependants");
		loadApplicationState("suivi_pi_drp");
	}
	public void onClick$garantiesoumission() {
		session.setAttribute("libelle", "garantiesoumission");
		loadApplicationState("suivi_pi_drp");
	}
	public void onClick$piecessoumissionnaires() {
		session.setAttribute("libelle", "piecessoumissionnaires");
		loadApplicationState("suivi_pi_drp");
	}
	public void onClick$compositioncommissiontechnique() {
		session.setAttribute("libelle", "compositioncommissiontechnique");
		loadApplicationState("suivi_pi_drp");
	}
	public void onClick$lecturesoffres() {
		session.setAttribute("libelle", "lecturesoffres");
		loadApplicationState("suivi_pi_drp");
	}
	public void onClick$procesverbalouverture() {
		session.setAttribute("libelle", "procesverbalouverture");
		loadApplicationState("suivi_pi_drp");
	}
	public void onClick$incidents() {
		session.setAttribute("libelle", "incidents");
		loadApplicationState("suivi_pi_drp");
	}
}