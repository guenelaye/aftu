package sn.ssi.kermel.web.pidrp.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygMonnaieoffre;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygRecours;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DocumentsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class DocumentsController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libellemonnaie=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherMonnaie;
    SygBailleurs bailleur=null;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idrecours=null;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	SygRecours recours=new SygRecours();
	SygMonnaieoffre monnaie=new SygMonnaieoffre();
	private Textbox txtRefrecours,txtRefappel,txtNom,txtObjet;
	private Div step0,step1;
	private Menuitem menuAjouter,menuModifier,menuSupprimer;
	private Datebox dtexpiration,dtreception;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private Iframe idIframe;
	private String extension,images;
	private final String cheminDossier = UIConstants.PATH_PJ;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
    	lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
	
    
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygDocuments> documents = BeanLocator.defaultLookup(DocumentsSession.class).find(activePage,byPage,dossier,null,null,null, null);
			 lstListe.setModel(new SimpleListModel(documents));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(DocumentsSession.class).count(dossier,null,null,null, null));
		} 
	
		else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
			Listcell button = (Listcell) event.getTarget();
			step0.setVisible(false);
			step1.setVisible(true);
			String filepath = cheminDossier +  (String) button.getAttribute(UIConstants.TODO);
			File f = new File(filepath.replaceAll("\\\\", "/"));

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(f, null, null);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (mymedia != null)
				idIframe.setContent(mymedia);
			else
				idIframe.setSrc("");

			idIframe.setHeight("600px");
			idIframe.setWidth("100%");
		}
		
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygDocuments documents = (SygDocuments) data;
		item.setValue(documents);

		 Listcell cellLibelle= new Listcell(documents.getLibelle());
//		  if(documents.getTypeDocument().equals(UIConstants.PARAM_TYPEDOCUMENTS_PVOUVERTURE))
//			  cellLibelle.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.documents.pvdouverture"));
//		  else  if(documents.getTypeDocument().equals(UIConstants.PARAM_TYPEDOCUMENTS_EVALUATION))
//			  cellLibelle.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.documents.pvevaluation"));
//		  else if(documents.getTypeDocument().equals(UIConstants.PARAM_TYPEDOCUMENTS_ATTIBUTIONS))
//			  cellLibelle.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.documents.pvattribution"));
		 cellLibelle.setParent(item);
		 
		 Listcell cellDate= new Listcell("");
		 if(documents.getDate()!=null&&documents.getHeure()!=null)
			 cellDate.setLabel(UtilVue.getInstance().formateLaDate(documents.getDate())+" � "+
					 UtilVue.getInstance().formateLHeure(documents.getHeure()));
		 else
			 if(documents.getDate()!=null)
			 cellDate.setLabel(UtilVue.getInstance().formateLaDate(documents.getDate()));
		 cellDate.setParent(item);
		 
		 Listcell cellImage = new Listcell();
		 extension=documents.getNomFichier().substring(documents.getNomFichier().length()-3, documents.getNomFichier().length());
		 if(extension.equalsIgnoreCase("pdf"))
			 images="/images/icone_pdf.png";
		 else  
			 images="/images/word.jpg";
		 cellImage.setImage(images);
		 cellImage.setAttribute(UIConstants.TODO, documents.getNomFichier());
		 cellImage.setTooltiptext("D�tails r�alisation");
		 cellImage.addEventListener(Events.ON_CLICK, DocumentsController.this);
		 cellImage.setParent(item);
		 
		
	}

	
	

	
	public void  onClick$menuFermer(){
		step0.setVisible(true);
		step1.setVisible(false);
	}

}