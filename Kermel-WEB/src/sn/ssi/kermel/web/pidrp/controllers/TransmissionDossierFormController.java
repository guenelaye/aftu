package sn.ssi.kermel.web.pidrp.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class TransmissionDossierFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

    public Textbox txtcommentaires;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
     Session session = getHttpSession();
   List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	private String nomFichier;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuValider;
	private Datebox dtremisedossier,dtretourdossier;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		
    	
    
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if((dossier==null))
		{
			
			menuValider.setDisabled(true);
		}
		else
		{
			if(dossier.getDateRemiseDossierTechnique()!=null||dossier.getDateLimiteDossierTechnique()!=null)
			{
				dtremisedossier.setValue(dossier.getDateRemiseDossierTechnique());
				dtretourdossier.setValue(dossier.getDateLimiteDossierTechnique());
				txtcommentaires.setValue(dossier.getCommentaireDelaiTechnique());
				menuValider.setDisabled(true);
			}
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		
		
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		

	}


	
private boolean checkFieldConstraints() {
		
		try {
		
			if(dtremisedossier.getValue()==null)
		     {
               errorComponent = dtremisedossier;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.evaluation.transmissiondossier.dtretourdossiers")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dtretourdossier.getValue()==null)
		     {
              errorComponent = dtretourdossier;
              errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.evaluation.transmissiondossier.dtretourdossiers")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((dtremisedossier.getValue()).after(dtretourdossier.getValue()))
			 {
				errorComponent = dtremisedossier;
				errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.evaluation.transmissiondossier.dtretourdossiers")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.referentiel.date.dujour")+": "+UtilVue.getInstance().formateLaDate(dtretourdossier.getValue());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			if((dtremisedossier.getValue()).after(new Date()))
			 {
				errorComponent = dtremisedossier;
				errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.evaluation.transmissiondossier.dtretourdossiers")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.referentiel.date.dujour")+": "+UtilVue.getInstance().formateLaDate(new Date());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			return true;
			
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

		public void onClick$menuValider() {
			if(checkFieldConstraints())
			{
				dossier.setDateRemiseDossierTechnique(dtremisedossier.getValue());
				dossier.setDateLimiteDossierTechnique(dtretourdossier.getValue());
			    dossier.setCommentaireDelaiTechnique(txtcommentaires.getValue());
				BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).update(dossier);
				lbStatusBar.setValue("");
				session.setAttribute("libelle", "formtransmissiondossier");
				loadApplicationState("suivi_pi_drp");
				
			}
		}
}