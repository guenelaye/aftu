package sn.ssi.kermel.web.pidrp.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygObservateursIndependants;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.ObservateursIndependantsSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class ObservateursIndependantsController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libellemonnaie=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    SygBailleurs bailleur=null;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idrepresentant=null;
	SygDossiers dossier=new SygDossiers();
	SygObservateursIndependants representant=new SygObservateursIndependants();
	SygPlisouvertures structure=new SygPlisouvertures();
	private Div step0,step1;
	private Menuitem menuAjouter,menuModifier,menuSupprimer;
	private Textbox txtRepresentant,txtQualite;
	private Datebox dtdate;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
    	lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
		
    
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier==null)
		{
			menuAjouter.setDisabled(true);
			menuModifier.setDisabled(true);
			menuSupprimer.setDisabled(true);
		}
		else
		{
			if(dossier.getDateRemiseDossierTechnique()!=null)
			{
				menuAjouter.setDisabled(true);
				menuModifier.setDisabled(true);
				menuSupprimer.setDisabled(true);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			
		}
		
		
	}

	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygObservateursIndependants> representants = BeanLocator.defaultLookup(ObservateursIndependantsSession.class).find(activePage,byPageBandbox,dossier,-1);
			 lstListe.setModel(new SimpleListModel(representants));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(ObservateursIndependantsSession.class).count(dossier,-1));
		} 
	
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = ((SygObservateursIndependants)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue()).getId();
				BeanLocator.defaultLookup(ObservateursIndependantsSession.class).delete(codes);
			}
			session.setAttribute("libelle", "observateursindependants");
			loadApplicationState("suivi_pi_drp");
		}
		
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygObservateursIndependants representants = (SygObservateursIndependants) data;
		item.setValue(representants);

		 Listcell cellRepresentant = new Listcell(representants.getRepresentant());
		 cellRepresentant.setParent(item);
		 
		 Listcell cellQualite = new Listcell(representants.getQualite());
		 cellQualite.setParent(item);
		 
		 Listcell cellDAte = new Listcell("");
		 if(representants.getDateconvocation()!=null)
			 cellDAte.setLabel(UtilVue.getInstance().formateLaDate(representants.getDateconvocation()));
		 cellDAte.setParent(item);
		 
		
	}

	public void onClick$menuSupprimer()
	{
		if (lstListe.getSelectedItem() == null)
			
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		

		HashMap<String, String> display = new HashMap<String, String>(); // permet
		display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
		display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
		display.put(MessageBoxController.DSP_HEIGHT, "250px");
		display.put(MessageBoxController.DSP_WIDTH, "47%");

		HashMap<String, Object> map = new HashMap<String, Object>(); // permet
		map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
		showMessageBox(display, map);
		
	}
	public void onClick$menuModifier()
	{
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		representant=(SygObservateursIndependants) lstListe.getSelectedItem().getValue();
		idrepresentant=representant.getId();
		txtRepresentant.setValue(representant.getRepresentant());
		txtQualite.setValue(representant.getQualite());
		if(representant.getDateconvocation()!=null)
		dtdate.setValue(representant.getDateconvocation());
		step0.setVisible(false);
		step1.setVisible(true);
	}
	
	
	

	private boolean checkFieldConstraints() {
		
		try {
		
			
			if(txtRepresentant.getValue().equals(""))
		     {
              errorComponent = txtRepresentant;
              errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.presencerepresentantsoum.nomrepresentant")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtQualite.getValue().equals(""))
		     {
             errorComponent = txtQualite;
             errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.presencerepresentantsoum.prenom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		
			return true;
			
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	public void  onClick$menuFermer(){
		step0.setVisible(true);
		step1.setVisible(false);
	}
	public void  onClick$menuAjouter(){
		step0.setVisible(false);
		step1.setVisible(true);
		idrepresentant=null;
		
		
	}
	public void  onClick$menuValider(){
		if(checkFieldConstraints())
		{
			
			representant.setDossier(dossier);
			representant.setRepresentant(txtRepresentant.getValue());
			representant.setQualite(txtQualite.getValue());
			representant.setEtape(0);
		    if(dtdate.getValue()!=null)
		    	representant.setDateconvocation(dtdate.getValue());
			if(idrepresentant==null)
				BeanLocator.defaultLookup(ObservateursIndependantsSession.class).save(representant);
			else
				BeanLocator.defaultLookup(ObservateursIndependantsSession.class).update(representant);
			session.setAttribute("libelle", "observateursindependants");
			loadApplicationState("suivi_pi_drp");
			
		}
	}
}