package sn.ssi.kermel.web.pidrp.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.entity.SygTachesafaire;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.ContratsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.tachesafaire.ejb.TachesAFaireSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class DemandeImmatriculationController extends AbstractWindow implements
		 AfterCompose {

	private Listbox lstListe,lstBailleur;
	private Paging pgPagination,pgBailleur;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtcommentaires;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference,libellebailleur=null;
    private int parent;
     List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherBailleur,txtChapitre;
    SygBailleurs bailleur=null;
    private Decimalbox dcmontantprevu;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idbailleur;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuValider;
	private Datebox dtdate;
	SygContrats contrat=new SygContrats();
	private SygAutoriteContractante autorite;
	SygTachesafaire tache=new SygTachesafaire();
	private Utilisateur infoscompte;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	   
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		autorite=appel.getAutorite();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if((dossier==null))
		{
			menuValider.setDisabled(true);
		}
		else
		{
			contrat=BeanLocator.defaultLookup(ContratsSession.class).getContrat(dossier,autorite, null,null,null);
			if(contrat!=null)
			{
				if(contrat.getDatedemandeimmatriculation()!=null)
				{
					menuValider.setDisabled(true);
					dtdate.setValue(contrat.getDatedemandeimmatriculation());
					txtcommentaires.setValue(contrat.getConcommentairesdmdmat());
				}
			}
			else
			{
				menuValider.setDisabled(true);
			}
			
		}
	}

private boolean checkFieldConstraints() {
		
		try {
		
			if(dtdate.getValue()==null)
		     {
               errorComponent = dtdate;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.resultatnegociation.date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((dtdate.getValue()).after(new Date()))
			 {
				errorComponent = dtdate;
				errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.resultatnegociation.date")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.referentiel.date.dujour")+": "+UtilVue.getInstance().formateLaDate(new Date());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			
			return true;
			
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

		public void onClick$menuValider() {
			if(checkFieldConstraints())
			{
				contrat.setDatedemandeimmatriculation(dtdate.getValue());
				contrat.setConcommentairesdmdmat(txtcommentaires.getValue());
				contrat.setImmatriculation(0);
				BeanLocator.defaultLookup(ContratsSession.class).update(contrat);
				
				tache.setDate(new Date());
				tache.setLibelle(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.demande")+": "+realisation.getReference());
				tache.setUsers(infoscompte);
				tache.setTraitepar("DCMP");
				tache.setFait(0);
				BeanLocator.defaultLookup(TachesAFaireSession.class).save(tache);
				session.setAttribute("libelle", "immatriculation");
				loadApplicationState("suivi_pi_drp");
				
				
			}
		}
	
}