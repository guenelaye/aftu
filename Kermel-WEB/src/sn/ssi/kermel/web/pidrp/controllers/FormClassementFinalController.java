package sn.ssi.kermel.web.pidrp.controllers;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPieces;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class FormClassementFinalController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstPieces;
	private Paging pgPagination,pgPieces;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
  	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	SygPieces piece=new SygPieces();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuAjouter,menuSupprimer;
	private Div step0,step1;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private int nombre=1;
	List<SygPlisouvertures> plis = new ArrayList<SygPlisouvertures>();
	SygPlisouvertures soumissionnaire=new SygPlisouvertures();
	private BigDecimal valeurpi=new BigDecimal(0);
	private BigDecimal valeurpx=new BigDecimal(0); 
	private BigDecimal notefinance=new BigDecimal(0); 
	private BigDecimal noteobtenue=new BigDecimal(0);
	private Label lbSoumissionnaire,lbAdresse,lbOffreFinancier,lbNoteTechnique,lbNoteFinancier,lbNoteFinale;
	private BigDecimal notetechnique=new BigDecimal(0); 
	private BigDecimal notetechminimale=new BigDecimal(0); 
	private BigDecimal notefinale=new BigDecimal(0.0); 
	private int pondt,pondf;
	private Long idsoumissionnaire;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
    
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier!=null)
		{
			plis = BeanLocator.defaultLookup(RegistrededepotSession.class).ClassementFinanciere(dossier, 1);
			pondt=dossier.getDosponderationtechnique();
			pondf=dossier.getDosponderationfinanciere();
			if(!Integer.toString(dossier.getDosNoteEliminatoireAmi()).equals(""))
			{
			  notetechminimale=new BigDecimal(dossier.getDosNoteEliminatoireAmi());
			}
			else
			{
			  notetechminimale=new BigDecimal(0);
			}
				if(plis.size()>0)
			 	{
					 for (int i = 0; i < plis.size(); i++) {
					 soumissionnaire=plis.get(i);
					 notetechnique=soumissionnaire.getNotepreselectionne().multiply(new BigDecimal(pondt));
					 notefinance=soumissionnaire.getScorefinancier().multiply(new BigDecimal(pondf));
					 System.out.println(notetechnique+"  "+notefinance);
					 notefinale=(notetechnique.add(notefinance)).divide(new BigDecimal(100),4);
					 
				     soumissionnaire.setScorefinal(notefinale);
					 BeanLocator.defaultLookup(RegistrededepotSession.class).update(soumissionnaire);
					 
					}
					 soumissionnaire=plis.get(0);
				 	 idsoumissionnaire=soumissionnaire.getId();
				     lbSoumissionnaire.setValue(soumissionnaire.getRetrait().getNomSoumissionnaire());
				     lbAdresse.setValue(soumissionnaire.getPays());
				     lbOffreFinancier.setValue(ToolKermel.format2Decimal(soumissionnaire.getMontantdefinitif()));
				     lbNoteTechnique.setValue(soumissionnaire.getNotepreselectionne().toString());
				     lbNoteFinancier.setValue(soumissionnaire.getScorefinancier().toString());
				     lbNoteFinale.setValue(soumissionnaire.getScorefinal().toString());
			 	}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
		
	}
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygPlisouvertures> candidats = BeanLocator.defaultLookup(RegistrededepotSession.class).ListeClassementFinal(pgPagination.getActivePage() * byPage,byPage,dossier, 1,idsoumissionnaire);
			 lstListe.setModel(new SimpleListModel(candidats));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(RegistrededepotSession.class).countClassementFinal(dossier, 1,idsoumissionnaire));
			 
			
		} 

	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygPlisouvertures plis = (SygPlisouvertures) data;
		item.setValue(plis);

		Listcell cellRaisonsocial = new Listcell(plis.getRetrait().getNomSoumissionnaire());
		 cellRaisonsocial.setParent(item);
		 
		 Listcell cellPays = new Listcell(plis.getPays());
		 cellPays.setParent(item);
		 
		Listcell cellMontant = new Listcell("");
		   if(plis.getMontantdefinitif()!=null)
			   cellMontant.setLabel(ToolKermel.format3Decimal(plis.getMontantdefinitif()));
		   cellMontant.setParent(item);
		
		Listcell cellNoteTechnique = new Listcell("");
		   if(plis.getNotepreselectionne()!=null)
			   cellNoteTechnique.setLabel(ToolKermel.format3Decimal(plis.getNotepreselectionne()));
		   cellNoteTechnique.setParent(item);
		
		 Listcell cellNoteFinanciere = new Listcell("");
		   if(plis.getScorefinancier()!=null)
			   cellNoteFinanciere.setLabel(ToolKermel.format3Decimal(plis.getScorefinancier()));
		   cellNoteFinanciere.setParent(item);
		   
		   Listcell cellNoteFinal = new Listcell("");
		   if(plis.getScorefinal()!=null)
			   cellNoteFinal.setLabel(ToolKermel.format3Decimal(plis.getScorefinal()));
		   cellNoteFinal.setParent(item);
		   
		nombre=nombre+1;
		Listcell cellRang = new Listcell(nombre+"");
		cellRang.setParent(item);
	
	}

	
	
	
}