package sn.ssi.kermel.web.pidrp.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabpanel;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class OuverturesPlisController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code;
	private Tabpanel tabDetailsfecp;
	private Include incDetailsfecp;
	private Tab TAB_listepresencemembrescommissions,TAB_representantssoumissionnaires,TAB_representantsservicestechniques,TAB_observateursindependants,
	TAB_garantiesoumission,TAB_piecessoumissionnaires,TAB_compositioncommissiontechnique,TAB_lecturesoffres,TAB_procesverbalouverture;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblInfos;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		

	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	public void onClick$listepresencemembrescommissions() {
		session.setAttribute("libelle", "listepresencemembrescommissions");
		loadApplicationState("suivi_pi_drp");
	}
	public void onClick$representantssoumissionnaires() {
		session.setAttribute("libelle", "representantssoumissionnaires");
		loadApplicationState("suivi_pi_drp");
	}
	public void onClick$representantsservicestechniques() {
		session.setAttribute("libelle", "representantsservicestechniques");
		loadApplicationState("suivi_pi_drp");
	}
	public void onClick$observateursindependants() {
		session.setAttribute("libelle", "observateursindependants");
		loadApplicationState("suivi_pi_drp");
	}
	public void onClick$garantiesoumission() {
		session.setAttribute("libelle", "garantiesoumission");
		loadApplicationState("suivi_pi_drp");
	}
	public void onClick$piecessoumissionnaires() {
		session.setAttribute("libelle", "piecessoumissionnaires");
		loadApplicationState("suivi_pi_drp");
	}
	public void onClick$compositioncommissiontechnique() {
		session.setAttribute("libelle", "compositioncommissiontechnique");
		loadApplicationState("suivi_pi_drp");
	}
	public void onClick$lecturesoffres() {
		session.setAttribute("libelle", "lecturesoffres");
		loadApplicationState("suivi_pi_drp");
	}
	public void onClick$procesverbalouverture() {
		session.setAttribute("libelle", "procesverbalouverture");
		loadApplicationState("suivi_pi_drp");
	}
	
	public void onClick$incidents() {
		session.setAttribute("libelle", "incidents");
		loadApplicationState("suivi_pi_drp");
	}
}