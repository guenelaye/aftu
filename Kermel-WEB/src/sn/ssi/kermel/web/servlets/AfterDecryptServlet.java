package sn.ssi.kermel.web.servlets;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.PklEnveloppesFichiers;
import sn.ssi.kermel.be.entity.PklPlis;
import sn.ssi.kermel.be.entity.PklPlisEnveloppes;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.be.passationsmarches.ejb.SoumissionElectroniqueSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.core.utils.MyZip;

/**
 * Servlet implementation class AfterDecryptServlet
 */
public class AfterDecryptServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AfterDecryptServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SygPlisouvertures pliov = BeanLocator.defaultLookup(
				RegistrededepotSession.class).findById(
				new Long((String)request.getParameter("id")));
		String plireq = (String) request.getParameter("pli");
		String message = "Ouverture du pli <b>N° "+ pliov.getNumero()+"</b>  effectué avec succès";
	
		
		if (plireq != null) {
			String chemin = request.getParameter("chemin");

			String nompli = plireq;

			//File zipfile = new File(BeConstants.zipsRepertoire+nompli+".zip"); 
			File zipfile = new File(UIConstants.zipsRepertoire+nompli);
			File folder = new File(UIConstants.dezipsRepertoire+nompli);
			
			try {
				MyZip.unzip(zipfile, folder);				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			/***************** debut de traitement ***************/
			PklPlis pli = null;

			pli = BeanLocator.defaultLookup(
					SoumissionElectroniqueSession.class).findPlisExiste(
					pliov.getDossier(), pliov.getFournisseur());

			if (pli != null) {
				List<PklPlisEnveloppes> envs = BeanLocator.defaultLookup(
						SoumissionElectroniqueSession.class).Enveloppes(0,
						-1, pli);
				if (envs != null) {
					for (PklPlisEnveloppes env : envs) {
						List<PklEnveloppesFichiers> fichiers = BeanLocator
								.defaultLookup(
										SoumissionElectroniqueSession.class)
								.Fichiers(0, -1, env);
						if (fichiers != null) {
							for (PklEnveloppesFichiers fichier : fichiers) {
								BeanLocator
										.defaultLookup(
												SoumissionElectroniqueSession.class)
										.deleteFichier(fichier.getId());
							}
						}
						BeanLocator.defaultLookup(
								SoumissionElectroniqueSession.class)
								.deleteEnveloppe(env.getId());
					}
				}
				
				MyZip.enregistrer(UIConstants.dezipsRepertoire
						+ nompli, pli);

			} else {

				pli = new PklPlis();
				pli.setLibelle(nompli);

				pli.setDossier(pliov.getDossier());
				pli.setFournisseur(pliov.getFournisseur());

				pli = BeanLocator.defaultLookup(
						SoumissionElectroniqueSession.class).savePlis(pli);

				MyZip.enregistrer(UIConstants.dezipsRepertoire
						+ nompli, pli);

			}
			pliov.setOuvert("oui");
			BeanLocator.defaultLookup(RegistrededepotSession.class).update(pliov);

			/***************  fin de traitement***************/

		}
		
        
		request.setAttribute("message", message);
		
		request.getRequestDispatcher("/jsp/afterd.jsp").forward(request, response);	

	}

}
