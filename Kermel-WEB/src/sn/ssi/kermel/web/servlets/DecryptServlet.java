package sn.ssi.kermel.web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteContractanteSession;
import sn.ssi.kermel.web.common.constants.UIConstants;

/**
 * Servlet implementation class DecryptServlet
 */
public class DecryptServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DecryptServlet() {
        super();
      
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doPost(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Long id =  Long.parseLong((String) request.getParameter("id"));
		SygPlisouvertures pliov = BeanLocator.defaultLookup(
				RegistrededepotSession.class).findById(id);
		Long autoid = Long.parseLong(request.getParameter("aut"));

		
		String encryptedDataPath = "", encryptedXmlPath = "";
		String privateCertPath = "", privateCertPass = "";
		
		String cheminxmlov = null;
		String cheminov = null;
		if (pliov != null) {
			cheminxmlov = pliov.getChemin() + "/" + pliov.getNomPli()
					+ ".xml";
			cheminov = pliov.getChemin();
		
			encryptedXmlPath = UIConstants.urlServeurPlis+cheminxmlov;
			encryptedDataPath = UIConstants.urlServeurPlis+cheminov;
		}
		
		 
		
		SygAutoriteContractante autorite = BeanLocator.defaultLookup(AutoriteContractanteSession.class).findById(autoid);
		 if(autorite != null && autorite.getPrivateCer() != null && autorite.getPinCode() != null) {
			 privateCertPath =  UIConstants.certificatsUrl+autorite.getPrivateCer();
			 privateCertPass = autorite.getPinCode();
		
	    }
        
		
		request.setAttribute("encryptedXmlPath", encryptedXmlPath);
		request.setAttribute("encryptedDataPath", encryptedDataPath);
		request.setAttribute("privateCertPath", privateCertPath);
		request.setAttribute("privateCertPass", privateCertPass);
		request.setAttribute("id", request.getParameter("id"));
		
		request.getRequestDispatcher("/jsp/indexd.jsp").forward(request, response);	
		
	}

}
