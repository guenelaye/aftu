package sn.ssi.kermel.web.traitementdossier.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygTDossierCourrier;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.security.UtilisateurSession;
import sn.ssi.kermel.be.traitementdossier.ejb.DossierAcSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class ListTraitementController extends AbstractWindow implements
ListitemRenderer, AfterCompose, EventListener{

    private Listbox lstListe;
    private Paging pgPagination;
    private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
    public static final String CURRENT_MODULE="CURRENT_MODULE";
    private Listheader libellerapport;
    private String Sgmlibelle;
    private Textbox txtlibelle,txtcode,txtdescription,txtfichier;
    Long code;
    String dossier;
    private SygDossierCourrier dossierCourrier;
    private Menuitem ADD_TRAITEMENT_DOSSIER, MOD_TRAITEMENT_DOSSIER, SUPP_TRAITEMENT_DOSSIER;
    private KermelSousMenu monSousMenu;
    @Override
    public void onEvent(Event event) throws Exception {
	if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
	    List<SygTDossierCourrier>  traitements = BeanLocator.defaultLookup(DossierAcSession.class).findTdossier(pgPagination.getActivePage()*byPage,byPage,dossierCourrier.getCode());
	    SimpleListModel listModel = new SimpleListModel(traitements);
	    lstListe.setModel(listModel);
	    pgPagination.setTotalSize(BeanLocator.defaultLookup(DossierAcSession.class).countTDossier(dossierCourrier.getCode()));
	} 
	else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
	    final String uri = "/traitementdossier/formmodeledocument.zul";

	    final HashMap<String, String> display = new HashMap<String, String>();
	    display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.modele"));
	    display.put(DSP_HEIGHT,"500px");
	    display.put(DSP_WIDTH, "80%");

	    final HashMap<String, Object> data = new HashMap<String, Object>();
	    data.put(ModeleDocumentFormController.WINDOW_PARAM_MODE,
		    UIConstants.MODE_NEW);

	    showPopupWindow(uri, data, display);
	} 

    }

    @Override
    public void afterCompose() {
	Components.wireFellows(this, this);
	Components.addForwards(this, this);
	monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
	monSousMenu.setFea_code(UIConstants.TRAITEMENT_DOSSIER);
	monSousMenu.afterCompose();
	/* pour les menuitem eventuellement crees */
	Components.wireFellows(this, this); // mais le wireFellows precedent
	Components.wireFellows(this, this);
	if (ADD_TRAITEMENT_DOSSIER != null) {
	    ADD_TRAITEMENT_DOSSIER.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
	}
	if (MOD_TRAITEMENT_DOSSIER != null) {
	    MOD_TRAITEMENT_DOSSIER.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
	}
	if (SUPP_TRAITEMENT_DOSSIER != null) {
	    SUPP_TRAITEMENT_DOSSIER.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
	}

	addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
	addEventListener(ApplicationEvents.ON_ADD, this);
	addEventListener(ApplicationEvents.ON_EDIT, this);
	addEventListener(ApplicationEvents.ON_DELETE, this);

	addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	dossierCourrier = (SygDossierCourrier) getHttpSession().getAttribute("DOSSIERCOURRIER");

	lstListe.setItemRenderer(this);

	pgPagination.setPageSize(byPage);
	/*
	 * Apr�s une pagination, le mod�le de liste est mis � jour.
	 */
	pgPagination.addForward("onPaging", this,
		ApplicationEvents.ON_MODEL_CHANGE);

	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);



    }

    @Override
    public void render(Listitem item, Object data, int index)  throws Exception {
	SygTDossierCourrier tDossierCourrier = (SygTDossierCourrier) data;
	item.setValue(tDossierCourrier);

	Listcell  origine = new Listcell(tDossierCourrier.getExpediteur().getPrenom()+" "+tDossierCourrier.getExpediteur().getNom());
	origine.setParent(item);
	Utilisateur utilisateur = BeanLocator.defaultLookup(UtilisateurSession.class).findByCode(tDossierCourrier.getDestinataire());
	Listcell  destinataire = new Listcell(utilisateur.getPrenom()+" "+utilisateur.getNom());
	destinataire.setParent(item);

	Listcell  dateTransmission = new Listcell("");
	if(tDossierCourrier.getDateTransmis()!=null) {
	    dateTransmission = new Listcell(UtilVue.getInstance().formateLaDate(tDossierCourrier.getDateTransmis()));
	}
	dateTransmission.setParent(item);

	Listcell  intructions = new Listcell("");
	if(tDossierCourrier.getInstruction()!=null){
	    intructions = new Listcell(tDossierCourrier.getInstruction());
	}
	intructions.setParent(item);

    }

}
