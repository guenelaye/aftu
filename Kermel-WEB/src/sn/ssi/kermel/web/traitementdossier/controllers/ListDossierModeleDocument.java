package sn.ssi.kermel.web.traitementdossier.controllers;

import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Image;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygDossierModeleDoc;
import sn.ssi.kermel.be.traitementdossier.ejb.DossierModeleDocSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class ListDossierModeleDocument extends AbstractWindow implements AfterCompose , EventListener,ListitemRenderer {

    private Listbox lstListe;
    private Paging pgPagination;
    private SygDossierCourrier dossierCourrier;

    @Override
    public void render(Listitem item, Object data, int index)  throws Exception {
	SygDossierModeleDoc modeledoc = (SygDossierModeleDoc) data;
	item.setValue(modeledoc);

	Listcell pieces = new Listcell(modeledoc.getModeleDocument().getDocument().getLibelle());
	pieces.setParent(item);

	Listcell fichier = new Listcell();
	Image imageF = new Image();
	imageF.setSrc("/images/PaperClip-16x16.png");
	imageF.setAttribute("FICHIER", modeledoc.getModeleDocument().getDocument().getFichier());
	imageF.addEventListener(Events.ON_DOUBLE_CLICK, this);
	imageF.setParent(fichier);
	fichier.setParent(item);

	Image image = new Image();
	Listcell statut = new Listcell();
	if(modeledoc.getStatut()==0){
	    image.setSrc("/images/cancel.png");
	}else{
	    image.setSrc("/images/ok.png");
	}
	image.addEventListener(Events.ON_DOUBLE_CLICK, this);
	image.setAttribute("PIECE", modeledoc);
	image.setParent(statut);
	statut.setParent(item);

    }

    @Override
    public void onEvent(Event event) throws Exception {

	if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
	    List<SygDossierModeleDoc> dossiermodeldoc = BeanLocator.defaultLookup(DossierModeleDocSession.class).findByDossier( dossierCourrier);
	    SimpleListModel listModel = new SimpleListModel(dossiermodeldoc);
	    lstListe.setModel(listModel);
	    //  pgPagination.setTotalSize(BeanLocator.defaultLookup(DossierAcSession.class).countByType(type));
	} 
	else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DOUBLE_CLICK)) {
	    //	    Long code = (Long) ((HashMap<String, Object>) event.getData())
	    //		    .get("PIECE");

	    Image image = (Image) event.getTarget();
	    String file = (String) image.getAttribute("FICHIER");
	    SygDossierModeleDoc modeleDoc = (SygDossierModeleDoc) image.getAttribute("PIECE");

	    if(modeleDoc!=null){
		if(modeleDoc.getStatut()==1) {
		    modeleDoc.setStatut(0);
		} else {
		    modeleDoc.setStatut(1);
		}
		BeanLocator.defaultLookup(DossierModeleDocSession.class).update(modeleDoc);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	    }
	} 

    }	

    @Override
    public void afterCompose() {
	Components.wireFellows(this, this);
	Components.addForwards(this, this);

	addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
	addEventListener(Events.ON_DOUBLE_CLICK, this);
	addEventListener(ApplicationEvents.ON_ADD, this);
	addEventListener(ApplicationEvents.ON_EDIT, this);
	addEventListener(ApplicationEvents.ON_DETAILS, this);
	addEventListener(ApplicationEvents.ON_DELETE, this);

	lstListe.setItemRenderer(this);
	dossierCourrier = (SygDossierCourrier) getHttpSession().getAttribute("DOSSIERCOURRIER");

	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

    }

}
