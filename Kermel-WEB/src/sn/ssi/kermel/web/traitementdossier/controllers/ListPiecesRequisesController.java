package sn.ssi.kermel.web.traitementdossier.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygPiecesrequises;
import sn.ssi.kermel.be.entity.SygTypesDossiers;
import sn.ssi.kermel.be.traitementdossier.ejb.PiecesRequisesSession;
import sn.ssi.kermel.be.traitementdossier.ejb.TypesDossiersSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

public class ListPiecesRequisesController extends AbstractWindow implements AfterCompose, EventListener, ListitemRenderer {

	private static final long serialVersionUID = -2705843672012672474L;
	public static final String ERROR_MSG_STYLE = "color:red";
	private Listbox lstListe;
	private Listheader lshLibelle;
	private Paging pgPagination;
	private KermelSousMenu monSousMenu;
	//GrhAvenant myAvenant = new GrhAvenant();
	private Menuitem ADD_PIECESREQUISES, SUPP_PIECESREQUISES;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE, activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private Long code = null;
	SygTypesDossiers dossiers = null;
	Session session = getHttpSession();
	private Label lblcode, lbllibelle;
	private Textbox txtlibelle;
	private String libelle = null, codedossier;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.PIECESREQUISES);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		Components.wireFellows(this, this);
		if (ADD_PIECESREQUISES != null)
			ADD_PIECESREQUISES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
		if (SUPP_PIECESREQUISES != null)
			SUPP_PIECESREQUISES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
		 
		lstListe.setItemRenderer(this);

		pgPagination.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pgPagination.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
		code = (Long) session.getAttribute("numconfiguration");
		dossiers = BeanLocator.defaultLookup(TypesDossiersSession.class).findById(code);
	}

	public void onCreate(CreateEvent createEvent) {
		
//		lblcode.setValue(dossiers.getCode());
//		lbllibelle.setValue(dossiers.getLibelle());
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = 10;
				pgPagination.setPageSize(10);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			List<SygPiecesrequises> Piecesrequises = BeanLocator.defaultLookup(PiecesRequisesSession.class).find1(activePage, byPage,dossiers.getCode(),libelle);
			lstListe.setModel(new SimpleListModel(Piecesrequises));
			pgPagination.setTotalSize(BeanLocator.defaultLookup(PiecesRequisesSession.class).count(dossiers.getCode(),libelle));

		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {

			String uri = "/traitementdossier/formpiecesrequises.zul";
			HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, "Ajouter attendu");
			HashMap<String, Object> data = null;
			data = new HashMap<String, Object>();

			data.put(AjoutPiecesDossiersController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_HEIGHT, "90%");

			showPopupWindow(uri, data, display);
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)) {
			if (lstListe.getSelectedItem() == null)
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));

			// de
			// fixer
			// les
			// dimenisons
			// du
			// popup
			HashMap<String, String> display = new HashMap<String, String>();
			display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
			display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
			display.put(MessageBoxController.DSP_HEIGHT, "250px");
			display.put(MessageBoxController.DSP_WIDTH, "47%");

			HashMap<String, Object> map = new HashMap<String, Object>(); // permet
			// de
			// passer
			// des
			// parametres
			// au
			// popup
			map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
			showMessageBox(display, map);
			
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {
			for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				SygPiecesrequises Piecesrequises = (SygPiecesrequises) ((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(PiecesRequisesSession.class).delete(Piecesrequises.getIdpiecesrequises());
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
	}

	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygPiecesrequises pieces = (SygPiecesrequises) data;
		item.setValue(pieces);

		Listcell cellcode = new Listcell(pieces.getPiece().getCodepiece());
		cellcode.setParent(item);

		Listcell celllibelle = new Listcell(pieces.getPiece().getLibelle());
		celllibelle.setParent(item);
	
		Listcell celldescription = new Listcell(pieces.getPiece().getDescription());
		celldescription.setParent(item);
	}

	public void onClick$menuFermer() {
		session.setAttribute("reference", "dossiers");
		loadApplicationState("pan_configuration");
	}

	
	public void onOK$txtlibelle() {
		onClick$btnRechercher();
	}
	
	public void onClick$btnRechercher() {
		
		if (txtlibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.libelle")) || txtlibelle.getValue().equals("")) {
			libelle = null;
		} else {
			libelle  = txtlibelle.getValue();
		}	

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	
	public void onFocus$txtlibelle() {
		if (txtlibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.libelle"))) {
			txtlibelle.setValue("");
			
		}
	}
	public void onBlur$txtlibelle() {
		if (txtlibelle.getValue().equalsIgnoreCase("")) {
			txtlibelle.setValue(Labels.getLabel("kermel.common.form.libelle"));
		}	
	}

}