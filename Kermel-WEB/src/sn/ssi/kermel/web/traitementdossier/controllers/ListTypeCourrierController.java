package sn.ssi.kermel.web.traitementdossier.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygTypesCourrier;
import sn.ssi.kermel.be.traitementdossier.ejb.TypeCourrierSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;



@SuppressWarnings("serial")
public class ListTypeCourrierController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox list;
	private Paging pgCourrier;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private String sygmlibelle,typeLibelle;
	private Textbox txtlibelletypecourriertypecourrier,txtlibelletypecourrier,txtdescriptiontypecourrier;
	private Menuitem ADD_TYPECOURRIER, MOD_TYPECOURRIER, SUPP_TYPECOURRIER;
	 private KermelSousMenu monSousMenu;
	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.TYPECOURRIER);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		Components.wireFellows(this, this);
		if (ADD_TYPECOURRIER != null)
			ADD_TYPECOURRIER.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
		if (MOD_TYPECOURRIER != null)
			MOD_TYPECOURRIER.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
		if (SUPP_TYPECOURRIER != null)
			SUPP_TYPECOURRIER.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
 
		list.setItemRenderer(this);

		pgCourrier.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pgCourrier.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 List<SygTypesCourrier>  courrier = BeanLocator.defaultLookup(TypeCourrierSession.class).find(pgCourrier.getActivePage()*byPage,byPage,typeLibelle,sygmlibelle,null);
			 SimpleListModel listModel = new SimpleListModel(courrier);
			list.setModel(listModel);
			pgCourrier.setTotalSize(BeanLocator.defaultLookup(TypeCourrierSession.class).count(typeLibelle));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/traitementdossier/formtypecourrier.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.typecourrier"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(TypeCourrierFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/traitementdossier/formtypecourrier.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(TypeCourrierFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(TypeCourrierFormController.PARAM_WIDOW_CODE, list
					.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				// de
				// fixer
				// les
				// dimenisons
				// du
				// popup
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// de
				// passer
				// des
				// parametres
				// au
				// popup
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < list.getSelectedCount(); i++) {
					Long codes = (Long)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					System.out.println(codes);
				BeanLocator.defaultLookup(TypeCourrierSession.class).delete(codes);
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygTypesCourrier courrier = (SygTypesCourrier) data;
		item.setValue(courrier.getIdtypecourrier());
		
		Listcell cellcodetypecourrier = new Listcell(courrier.getCodetypecourrier());
		cellcodetypecourrier.setParent(item);
		Listcell celllibelletypecourrier = new Listcell(courrier.getLibelletypecourrier());
		celllibelletypecourrier.setParent(item);
		Listcell celldescriptiontypecourrier = new Listcell(courrier.getDescriptiontypecourrier());
		celldescriptiontypecourrier.setParent(item);
	}
	
	public void onClick$btnRechercher() {
		
		if (txtlibelletypecourrier.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.libelletypecourrier")) || txtlibelletypecourrier.getValue().equals("")) {
			typeLibelle = null;
		} else {
			typeLibelle  = txtlibelletypecourrier.getValue();
		}	

 
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
	}
	
	public void onFocus$txtlibelletypecourrier()
	{
		if(txtlibelletypecourrier.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.libelletypecourrier")))
			txtlibelletypecourrier.setValue("");
		else
			typeLibelle=txtlibelletypecourrier.getValue();
	}
	
	public void onBlur$txtlibelletypecourrier()
	{
		if(txtlibelletypecourrier.getValue().equals(""))
			txtlibelletypecourrier.setValue(Labels.getLabel("kermel.common.form.libelletypecourrier"));
	}
}