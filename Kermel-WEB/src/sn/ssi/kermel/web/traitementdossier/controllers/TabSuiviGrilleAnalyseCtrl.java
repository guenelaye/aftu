package sn.ssi.kermel.web.traitementdossier.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.web.common.components.DossierMenu;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Boubacar Doumbouya
 * @since 31 f�vrier 2013, 7:35

 */
public class TabSuiviGrilleAnalyseCtrl extends AbstractWindow implements
AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code;
	private Tab TAB_GRILLEANA,TAB_COURRIER,TAB_PIECE,TAB_criteresqualifications;
	private String LibelleTab;
	Session session = getHttpSession();
	private Long idplan,idrealisation;
	private Label lblInfos, lbTitle, lbNumero,lbType,lbDate,lbStatut, lblRefCourrier, lblDateCourrier, lblOrigine, lblObjet;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private final SygAutoriteContractante autorite=null;
	private SygDossierCourrier dossierCourrier;
	private DossierMenu monDossierMenu;
	private Tab TAB_GRILLE,TAB_PLAND;


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		dossierCourrier = (SygDossierCourrier) getHttpSession().getAttribute("DOSSIERCOURRIER");

		//	SygTCourrierAC courrierAC = BeanLocator.defaultLookup(TCourriersAcSession.class).findByDossier(dossierCourrier);


	}


	public void onCreate(CreateEvent createEvent) {

		TAB_GRILLE.setSelected(true);
		Include inc = (Include) this.getFellowIfAny("incGrille");
		inc.setSrc("/traitementdossier/suividossier/gridCriteres.zul");	



	}

	@Override
	public void onEvent(Event event) throws Exception {

	}

	public void onClick$menuCancelStep2()
	{

		loadApplicationState("dossier_ac");
	}
}