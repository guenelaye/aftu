package sn.ssi.kermel.web.traitementdossier.controllers;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygDossierPiecesJointes;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.traitementdossier.ejb.DossierPiecesJointesSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class ListPieceDossier extends AbstractWindow implements EventListener,
AfterCompose, ListitemRenderer {

    public static final String TODO = "TODO";

    public static final String LIST_FILES_MODEL_CHANGE="onFilesModelChange";

    public static final String CURRENT = "CURRENT";
    Session session = getHttpSession();
    SygTypeAutoriteContractante type=new SygTypeAutoriteContractante();
    private Listbox lstFiles;


    private String errorMsg;
    private static final String ERROR_MSG_STYLE = "color:red";
    private static final String READY_MSG_STYLE = "";
    private Component errorComponent;

    private SygDossierPiecesJointes filesDossier;
    private Textbox txtObjetFile;
    private Textbox txtFichiersrc;
    private Datebox dateSaisieFile;
    private Label lblStatusbar2;
    private final String cheminDossier = UIConstants.PATH_PJ;
    private String nomFichier;
    private SygDossierCourrier dossierCourrier;
    @Override
    public void render(Listitem item, Object data, int index)  throws Exception {
	SygDossierPiecesJointes file = (SygDossierPiecesJointes)data;
	item.setValue(file);

	//	    Listcell nomFile = new Listcell(filesAc.getNameFile());
	//	    nomFile.setParent(item);

	Listcell dateFile = new Listcell(UtilVue.getInstance().formateLaDate(file.getDateFile()));
	dateFile.setParent(item);

	Listcell objectFile = new Listcell("");
	if(file.getObjectFile()!=null){
	    objectFile = new Listcell(file.getObjectFile());
	}
	objectFile.setParent(item);

	Listcell cellSupFile = new Listcell("", "/images/delete.png");
	cellSupFile.addEventListener(Events.ON_CLICK, this);
	// cellSupLigneDevis.addEventListener(Events.ON_CLICK,
	// (EventListener) this);
	cellSupFile.setAttribute(UIConstants.TODO, "delLineFiles");
	cellSupFile.setAttribute(UIConstants.ATTRIBUTE_CODE,
		file.getId());
	cellSupFile.setParent(item);

	Listcell cellOpenFile = new Listcell("", "/images/PaperClip-16x16.png");
	cellOpenFile.addEventListener(Events.ON_DOUBLE_CLICK, this);
	// cellSupLigneDevis.addEventListener(Events.ON_CLICK,
	// (EventListener) this);
	cellOpenFile.setAttribute(UIConstants.TODO, "openFile");
	cellOpenFile.setAttribute(UIConstants.ATTRIBUTE_CODE,
		file.getId());
	cellOpenFile.setParent(item);

    }

    @Override
    public void afterCompose() {
	Components.wireFellows(this, this);
	Components.addForwards(this, this);
	addEventListener(LIST_FILES_MODEL_CHANGE, this);
	addEventListener(Events.ON_UPLOAD, this);
	addEventListener(Events.ON_CLICK, this);

	addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	lstFiles.setItemRenderer(this);
	dossierCourrier = (SygDossierCourrier) getHttpSession().getAttribute("DOSSIERCOURRIER");
	Events.postEvent(LIST_FILES_MODEL_CHANGE, this, null);

    }

    @Override
    public void onEvent(Event event) throws Exception {
	if(event.getName().equalsIgnoreCase(LIST_FILES_MODEL_CHANGE)){
	    List<SygDossierPiecesJointes>  filesAcs = BeanLocator.defaultLookup(DossierPiecesJointesSession.class).findByDossier(dossierCourrier);
	    SimpleListModel listModel = new SimpleListModel(filesAcs);
	    lstFiles.setModel(listModel);
	}else if (event.getName().equals(
		ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {

	    Long code = (Long) ((HashMap<String, Object>) event.getData())
		    .get(CURRENT);
	    String toDo = (String) ((HashMap<String, Object>) event.getData())
		    .get(TODO);

	    if (toDo != null && toDo.equalsIgnoreCase("delLineFiles")) {

		BeanLocator.defaultLookup(DossierPiecesJointesSession.class)
		.delete(code);
		initFormAll();
		Events.postEvent(LIST_FILES_MODEL_CHANGE, this, null);

	    } else if (toDo != null && toDo.equalsIgnoreCase("openFile")) {


	    }

	}if (event.getName().equalsIgnoreCase(Events.ON_DOUBLE_CLICK)) {
	    Listcell cell = (Listcell) event.getTarget();
	    // String toDo = (String) cell.getAttribute(UIConstants.TODO);
	    HashMap<String, String> display = new HashMap<String, String>();
	    display.put(MessageBoxController.DSP_MESSAGE, Labels
		    .getLabel("kermel.common.form.question.supprimer")
		    + " ?");
	    display.put(MessageBoxController.DSP_TITLE, Labels
		    .getLabel("kermel.courrier.file.supprimer.title"));
	    display.put(MessageBoxController.DSP_HEIGHT, "150px");
	    display.put(MessageBoxController.DSP_WIDTH, "100px");
	    HashMap<String, Object> data = new HashMap<String, Object>();
	    data
	    .put(CURRENT, cell
		    .getAttribute(UIConstants.ATTRIBUTE_CODE));
	    data.put(TODO, cell.getAttribute(UIConstants.TODO));
	    showMessageBox(display, data);
	}
    }

    public void onClick$bcUploadFile(){
	if (ToolKermel.isWindows()) {
	    nomFichier = FileLoader.uploadPieceDossier(UtilVue.getInstance().formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
	} else {
	    nomFichier = FileLoader.uploadPieceDossier(UtilVue.getInstance().formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));
	}

	txtFichiersrc.setValue(nomFichier);
    }


    private boolean isRequiredFieldFile() {
	try {
	    /* Controle champ obligatoire */
	    //	    if (txtNomFIchier.getValue() == null || txtNomFIchier.getValue().equals("")) {
	    //		errorMsg = Labels
	    //			.getLabel("kermel.file.nouveau.champnomfichierobligatoire");
	    //		errorComponent = txtNomFIchier;
	    //		return false;
	    //	    }
	    if (txtFichiersrc.getValue() == null || txtFichiersrc.getValue().equals("")) {
		errorMsg = Labels
			.getLabel("kermel.file.nouveau.champfichiersrcobligatoire");
		errorComponent = txtFichiersrc;
		return false;
	    }
	    //	    if (txtObjetFile.getValue() == null || txtObjetFile.getValue().equals("")) {
	    //		errorMsg = Labels
	    //			.getLabel("kermel.file.nouveau.champobjetfileobligatoire");
	    //		errorComponent = txtObjetFile;
	    //		return false;
	    //	    }
	    if (dateSaisieFile.getValue() == null || dateSaisieFile.getValue().equals("")) {
		errorMsg = Labels
			.getLabel("kermel.file.nouveau.champodatesaisieobligatoire");
		errorComponent = dateSaisieFile;
		return false;
	    }

	    return true;
	} catch (Exception e) {

	    return true;
	}

    }

    public void onClick$cellValider3() {

	if (isRequiredFieldFile()) {
	    if (filesDossier == null || filesDossier.getId() == 0) {
		filesDossier = new SygDossierPiecesJointes();
		filesDossier.setDossierCourrier(dossierCourrier);
	    }
	    setFormFile();
	    if (saveFile()) {
		initFormAll();
		Events.postEvent(LIST_FILES_MODEL_CHANGE, this, null);
	    } else {
		lblStatusbar2.setValue(errorMsg);
		lblStatusbar2.setStyle(ERROR_MSG_STYLE);
		throw new WrongValueException(lstFiles, errorMsg);
	    }

	} else {
	    lblStatusbar2.setValue(errorMsg);
	    lblStatusbar2.setStyle(ERROR_MSG_STYLE);
	    throw new WrongValueException(lstFiles, errorMsg);
	}
    }

    private boolean saveFile() {
	try {
	    BeanLocator.defaultLookup(DossierPiecesJointesSession.class).save(filesDossier);
	    return true;
	} catch (RuntimeException runtimeException) {
	    if (runtimeException.getCause().getClass().getPackage().getName()
		    .equals("javax.transaction")) {
		lblStatusbar2.setValue(Labels
			.getLabel("atlantis.common.erreur.commit"));
		lblStatusbar2.setStyle(ERROR_MSG_STYLE);
	    }
	    return false;
	} catch (Exception e) {
	    lblStatusbar2.setValue(e.toString());

	    return false;
	}
    }

    private void setFormFile() {

	filesDossier.setDossierCourrier(dossierCourrier);
	//filesAc.setNameFile(txtNomFIchier.getValue());
	//filesAc.setFileSrc(txtFichiersrc.getValue());
	filesDossier.setObjectFile(txtObjetFile.getValue());
	filesDossier.setDateFile(dateSaisieFile.getValue());
    }

    private void chargeFormAll(SygDossierPiecesJointes file) {
	//txtNomFIchier.setValue(filesAc.getNameFile());
	txtFichiersrc.setValue(filesDossier.getFileSrc());
	txtObjetFile.setValue(filesDossier.getObjectFile());
	dateSaisieFile.setValue(filesDossier.getDateFile());

    }

    private void initFormAll() {

	//txtNomFIchier.setValue(null);
	txtFichiersrc.setValue(null);
	txtObjetFile.setValue(null);
	dateSaisieFile.setValue(null);
	filesDossier= null;
    }

    public void onSelect$lstFiles() {

	filesDossier =  (SygDossierPiecesJointes) lstFiles.getSelectedItem().getValue();
	if (filesDossier != null) {
	    chargeFormAll(filesDossier);
	}
    }
}
