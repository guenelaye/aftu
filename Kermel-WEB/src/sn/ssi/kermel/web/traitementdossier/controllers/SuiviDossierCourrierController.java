package sn.ssi.kermel.web.traitementdossier.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.courriers.ejb.TCourriersAcSession;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygTCourrierAC;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.web.common.components.DossierMenu;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



/**

 * @author Boubacar Doumbouya
 * @since 31 f�vrier 2013, 7:35

 */
public class SuiviDossierCourrierController extends AbstractWindow implements
AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code;
	private Tab TAB_GRILLEANA,TAB_COURRIER,TAB_PIECE,TAB_criteresqualifications;
	private String LibelleTab;
	Session session = getHttpSession();
	private Long idplan,idrealisation;
	private Label lblInfos, lbTitle, lbNumero,lbType,lbDate,lbStatut, lblRefCourrier, lblDateCourrier, lblOrigine, lblObjet;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private final SygAutoriteContractante autorite=null;
	private SygDossierCourrier dossierCourrier;
	private DossierMenu monDossierMenu;
	private Tab TAB_PLAN,TAB_PLAND;


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		dossierCourrier = (SygDossierCourrier) getHttpSession().getAttribute("DOSSIERCOURRIER");

		SygTCourrierAC courrierAC = BeanLocator.defaultLookup(TCourriersAcSession.class).findByDossier(dossierCourrier);

		lblRefCourrier.setValue(courrierAC.getCourrierac().getCourrierReference());
		lblDateCourrier.setValue(UtilVue.getInstance().formateLaDate(courrierAC.getCourrierac().getCourrierDate()));
		lblOrigine.setValue(courrierAC.getCourrierac().getCourrierAutoriteContractante().getDenomination());
		lblObjet.setValue(courrierAC.getCourrierac().getCourrierObjet());
		lbTitle.setValue("Informations g�n�rales");
		//lbTitle.getValue().toUpperCase();
		//lbTitle.setStyle("");
		lbNumero.setValue(dossierCourrier.getCode());
		lbType.setValue(dossierCourrier.getTypesDossiers().getLibelle());
		lbDate.setValue(UtilVue.getInstance().formateLaDate(dossierCourrier.getDateStatus()));
		lbStatut.setValue(dossierCourrier.getState().getLibelle());
		lbStatut.setStyle("color:green;font-weight:bold");

		if(dossierCourrier.getTypesDossiers().getCode().equals(UIConstants.VAL_DMD_PROC_DEROG)
				||dossierCourrier.getTypesDossiers().getCode().equals(UIConstants.VAL_AVENANT))
		{
			TAB_PLAN.setVisible(true);
			TAB_PLAND.setVisible(false);

		}
		else
		{

			TAB_PLAN.setVisible(false);
			TAB_PLAND.setVisible(true);
		}

		monDossierMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monDossierMenu.setDosCode(dossierCourrier.getId());
		monDossierMenu.setState(dossierCourrier.getState().getCode());
		monDossierMenu.afterCompose();
	}


	public void onCreate(CreateEvent createEvent) {

		TAB_COURRIER.setSelected(true);
		Include inc = (Include) this.getFellowIfAny("incCourriers");
		inc.setSrc("/traitementdossier/suividossier/courrier.zul");	



	}

	@Override
	public void onEvent(Event event) throws Exception {

	}

	public void onClick$menuCancelStep2()
	{

		loadApplicationState("dossier_ac");
	}
}