package sn.ssi.kermel.web.traitementdossier.controllers;

import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygCritereAnalyse;
import sn.ssi.kermel.be.entity.SygGrillesAnalyses;
import sn.ssi.kermel.be.entity.SygTypesDossiers;
import sn.ssi.kermel.be.traitementdossier.ejb.GrillesAnalysesSession;
import sn.ssi.kermel.be.traitementdossier.ejb.ModeCritereAnalyseSession;
import sn.ssi.kermel.be.traitementdossier.ejb.TypesDossiersSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


@SuppressWarnings("serial")

public class AjoutGrillesAnalysesController extends AbstractWindow implements EventListener, AfterCompose, ListitemRenderer {

	public static final String WINDOW_PARAM_MODE = null;
	private Listbox lstListe;
	private Listheader lshLibelle;
	private Paging pgPagination;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGES, activePage;
	public static String CURRENT_CODESENS;
	private Textbox txtLibelle,txtcode;
	Session session = getHttpSession();
	SygCritereAnalyse analyse = null;
	SygTypesDossiers dossiers  = null;
	private Label lblNumero, lbllibelle;
	private Menuitem menuValider;
	private Label lblcode;
	private String libelle = null, codedossier;
	private Long code = null;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);

		// lshLibelle.setSortAscending(new
		// FieldComparator("typedossier.typdLibelle", false));
		// lshLibelle.setSortDescending(new
		// FieldComparator("typedossier.typdLibelle", true));
		
		code = (Long) session.getAttribute("numconfiguration");
		dossiers = BeanLocator.defaultLookup(TypesDossiersSession.class).findById(code);

	}

	public void onCreate(CreateEvent createEvent) {
		
		lblcode.setValue(dossiers.getCode());
		lbllibelle.setValue(dossiers.getLibelle());
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			// if (event.getData() != null) {
			// activePage = Integer.parseInt((String) event.getData());
			// byPage = 10;
			// pgPagination.setPageSize(10);
			// } else {
			// byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
			// activePage = pgPagination.getActivePage() * byPage;
			// pgPagination.setPageSize(byPage);
			// }
			List<SygCritereAnalyse> analyse = BeanLocator.defaultLookup(ModeCritereAnalyseSession.class).findNotInDossier(pgPagination.getActivePage() * byPage, byPage,dossiers,null);
			lstListe.setModel(new SimpleListModel(analyse));
	        pgPagination.setTotalSize(BeanLocator.defaultLookup(ModeCritereAnalyseSession.class).countNotInDossier(dossiers,null));

		}

	}

	public void onOK() {
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(menuValider, Labels.getLabel("kermel.common.form.editer"));
		for (int i = 0; i < lstListe.getSelectedCount(); i++) {
			SygGrillesAnalyses GrillesAnalyses = new SygGrillesAnalyses();
			analyse = (SygCritereAnalyse) ((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
			GrillesAnalyses.setAnalyse(analyse);
			GrillesAnalyses.setDossiers(dossiers);
		
			BeanLocator.defaultLookup(GrillesAnalysesSession.class).save(GrillesAnalyses);
			
            BeanLocator.defaultLookup(GrillesAnalysesSession.class).update(GrillesAnalyses);
			
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();

		}

//		loadApplicationState("GrillesAnalyses");
//		detach();
	}

	public void onClick$menuFermer() {
		detach();

	}

	public void onOK$txtLibelle() {
		onClick$btnRechercher();
	}

	public void onOK$btnRechercher() {
		onClick$btnRechercher();
	}

	public void onFocus$txtcode() {
		if (txtcode.getValue().equals("code")) {
			txtcode.setValue("");
		}

	}

	public void onBlur$txtLibelle() {
		if (txtcode.getValue().equalsIgnoreCase("")) {
			txtcode.setValue("code");
		}
	}

	public void onClick$btnRechercher() {

		String page = null;
		if (txtcode.getValue().equalsIgnoreCase("") || txtcode.getValue().equalsIgnoreCase("code")) {
			codedossier = null;
		} else {
			codedossier = txtcode.getValue();
			page = "0";
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}

	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygCritereAnalyse analyse = (SygCritereAnalyse) data;
		item.setValue(analyse);

		Listcell cellCodepiece = new Listcell(analyse.getCodecritereanalyse());
		cellCodepiece.setParent(item);
		
		Listcell cellLibelle = new Listcell(analyse.getLibellecritereanalyse());
		cellLibelle.setParent(item);

		Listcell cellDescription = new Listcell(analyse.getDescriptionanalyse());
		cellDescription.setParent(item);
	}

}