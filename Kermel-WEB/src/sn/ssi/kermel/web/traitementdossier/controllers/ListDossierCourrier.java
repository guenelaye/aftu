package sn.ssi.kermel.web.traitementdossier.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.traitementdossier.ejb.DossierAcSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class ListDossierCourrier extends AbstractWindow implements
AfterCompose, EventListener, ListitemRenderer {

	private Listbox list;
	private Paging pgDossier;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private Textbox txtcode,txtlibelle,txtdescription;
	private Intbox txtdelaitraitement;
	Long code;
	private String type;
	private Menuitem ADD_DOSSIERAC, MOD_DOSSIERAC, SUPP_DOSSIERAC, SUIVI_DOSSIERAC;
	private KermelSousMenu monSousMenu;
	private final SygDossierCourrier dossier = null;
	private Object selobj;
	private String status,cod;
	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.DOSSIERAC);
		monSousMenu.afterCompose();
		Components.wireFellows(this, this); // mais le wireFellows precedent
		Components.wireFellows(this, this);
		if (ADD_DOSSIERAC != null) {
			ADD_DOSSIERAC.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
		}
		if (MOD_DOSSIERAC != null) {
			MOD_DOSSIERAC.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
		}
		if (SUPP_DOSSIERAC != null) {
			SUPP_DOSSIERAC.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
		}

		if (SUIVI_DOSSIERAC != null) {
			SUIVI_DOSSIERAC.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_SUIVI);
		}

		type = null;
		selobj = Sessions.getCurrent().getAttribute("selObj");

		if(selobj!=null && selobj instanceof String){

			String tab[] = ((String)selobj).split("�");
			if( tab.length>1){
				type=  tab[0];
				status =  tab[1];
			}else if( tab.length==1) {
				type=  tab[0];
			}


			// type = dossier.getTypesDossiers().getCode();
		} 
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_SUIVI, this);

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);


		list.setItemRenderer(this);

		pgDossier.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pgDossier.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		Utilisateur utilisateur = (Utilisateur) getHttpSession().getAttribute("utilisateur");
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			List<SygDossierCourrier> dossiers = BeanLocator.defaultLookup(DossierAcSession.class).getDossierInMyStates(pgDossier.getActivePage()*byPage,byPage,utilisateur.getId(),type);
			SimpleListModel listModel = new SimpleListModel(dossiers);
			list.setModel(listModel);
			pgDossier.setTotalSize(BeanLocator.defaultLookup(DossierAcSession.class).countByType(type,cod));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/traitementdossier/formdossiers.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"600px");
			display.put(DSP_WIDTH, "90%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			Executions.getCurrent().setAttribute("MODE",
					UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/traitementdossier/formdossiers.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "90%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			Executions.getCurrent().setAttribute("MODE",
					UIConstants.MODE_EDIT);
			Executions.getCurrent().setAttribute("CODE",
					list
					.getSelectedItem().getValue());
			showPopupWindow(uri, data, display);
		} 
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
			if (list.getSelectedItem() == null) {
				throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
			}


			HashMap<String, String> display = new HashMap<String, String>(); 
			display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
			display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
			display.put(MessageBoxController.DSP_HEIGHT, "250px");
			display.put(MessageBoxController.DSP_WIDTH, "47%");

			HashMap<String, Object> map = new HashMap<String, Object>(); 
			map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
			showMessageBox(display, map);


		}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){

			for (int i = 0; i < list.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
				System.out.println(codes);
				BeanLocator.defaultLookup(DossierAcSession.class).delete(codes);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}

		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_SUIVI)) {
			if (list.getSelectedItem() == null) {
				throw new WrongValueException(list, Labels.getLabel(""));
			}
			getHttpSession().setAttribute("DOSSIERCOURRIER", list.getSelectedItem().getValue());
			getHttpSession().setAttribute("idplan", ((SygDossierCourrier)list.getSelectedItem().getValue()).getPlansdepassation().getIDinfoplan());
			loadApplicationState("suividossiercourrier");

		} 


	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygDossierCourrier dossiers = (SygDossierCourrier) data;
		item.setValue(dossiers);

		Listcell cellcode = new Listcell(dossiers.getCode());
		cellcode.setParent(item);
		Listcell celllibelle = new Listcell(dossiers.getLibelle());
		celllibelle.setParent(item);

		Listcell celldescription = new Listcell(dossiers.getTypesDossiers().getLibelle());
		celldescription.setParent(item);

		Listcell celldelaitraitement = new Listcell(UtilVue.getInstance().formateLaDate(dossiers.getDateStatus()));
		celldelaitraitement.setParent(item);

	}

	public void onOK$txtcode() {

		onClick$btnRechercher();
	}

	public void onClick$btnRechercher() {

		if (txtcode.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.code")) || txtcode.getValue().equals("")) {
			cod = null;
		} else {
			cod  = txtcode.getValue();
		}	


		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

	}

	public void onFocus$txtcode()
	{
		if(txtcode.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.code"))) {
			txtcode.setValue("");
		} else {
			cod=txtcode.getValue();
		}
	}

	public void onBlur$txtcode()
	{
		if(txtcode.getValue().equals("")) {
			txtcode.setValue(Labels.getLabel("kermel.common.form.code"));
		}
	}

}
