package sn.ssi.kermel.web.traitementdossier.controllers;

import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygModeleDocument;
import sn.ssi.kermel.be.entity.SygTypeModeleDocument;
import sn.ssi.kermel.be.entity.SygTypesDossiers;
import sn.ssi.kermel.be.traitementdossier.ejb.ModeleDocumentSession;
import sn.ssi.kermel.be.traitementdossier.ejb.TypeModelesDocumentsSession;
import sn.ssi.kermel.be.traitementdossier.ejb.TypesDossiersSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


@SuppressWarnings("serial")

public class AjoutTypeModeledocumentController extends AbstractWindow implements EventListener, AfterCompose, ListitemRenderer {

	public static final String WINDOW_PARAM_MODE = null;
	private Listbox lstListe;
	private Listheader lshLibelle;
	private Paging pgPagination;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGES, activePage;
	public static String CURRENT_CODESENS;
	private Textbox txtcode;
	Session session = getHttpSession();
	SygModeleDocument document = null;
	SygTypesDossiers dossiers  = null;
	private Label lblNumero, lbllibelle;
	private Menuitem menuValider;
	private Label lblcode;
	private String libelle = null, codedossier,images,extension;
	private Long code = null;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);

		code = (Long) session.getAttribute("numconfiguration");
		dossiers = BeanLocator.defaultLookup(TypesDossiersSession.class).findById(code);
		
		// lshLibelle.setSortAscending(new
		// FieldComparator("typedossier.typdLibelle", false));
		// lshLibelle.setSortDescending(new
		// FieldComparator("typedossier.typdLibelle", true));

	}

	public void onCreate(CreateEvent createEvent) {
		
		lblcode.setValue(dossiers.getCode());
		lbllibelle.setValue(dossiers.getLibelle());
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			// if (event.getData() != null) {
			// activePage = Integer.parseInt((String) event.getData());
			// byPage = 10;
			// pgPagination.setPageSize(10);
			// } else {
			// byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
			// activePage = pgPagination.getActivePage() * byPage;
			// pgPagination.setPageSize(byPage);
			// }
			List<SygModeleDocument> document = BeanLocator.defaultLookup(ModeleDocumentSession.class).findNotInDossier(pgPagination.getActivePage() * byPage, byPage,dossiers,null);
			lstListe.setModel(new SimpleListModel(document));
	        pgPagination.setTotalSize(BeanLocator.defaultLookup(ModeleDocumentSession.class).countNotInDossier(dossiers,null));

		}

	}

	public void onOK() {
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(menuValider, Labels.getLabel("kermel.common.form.editer"));
		for (int i = 0; i < lstListe.getSelectedCount(); i++) {
			SygTypeModeleDocument modeledocument = new SygTypeModeleDocument();
			document = (SygModeleDocument) ((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
			modeledocument.setDocument(document);
			modeledocument.setDossiers(dossiers);
		
			BeanLocator.defaultLookup(TypeModelesDocumentsSession.class).save(modeledocument);
			
			BeanLocator.defaultLookup(TypeModelesDocumentsSession.class).update(modeledocument);
			
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();

		}

//		loadApplicationState("TypeModeleDocument");
//		detach();
	}

	public void onClick$menuFermer() {
		detach();

	}

	public void onOK$txtcode() {
		onClick$btnRechercher();
	}

	public void onOK$btnRechercher() {
		onClick$btnRechercher();
	}

	public void onFocus$txtLibelle() {
		if (txtcode.getValue().equals("code")) {
			txtcode.setValue("");
		}

	}

	public void onBlur$txtLibelle() {
		if (txtcode.getValue().equalsIgnoreCase("")) {
			txtcode.setValue("code");
		}
	}

	public void onClick$btnRechercher() {

		String page = null;
		if (txtcode.getValue().equalsIgnoreCase("") || txtcode.getValue().equalsIgnoreCase("code")) {
			codedossier = null;
		} else {
			codedossier = txtcode.getValue();
			page = "0";
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}

	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygModeleDocument document = (SygModeleDocument) data;
		item.setValue(document);

		Listcell cellCodepiece = new Listcell(document.getCode());
		cellCodepiece.setParent(item);
		
		Listcell cellLibelle = new Listcell(document.getLibelle());   
		cellLibelle.setParent(item);

		Listcell cellDescription = new Listcell(document.getDescription());
		cellDescription.setParent(item);
		
         Listcell cellimage = new Listcell();
		
		if (document.getFichier()!=null)
		extension=document.getFichier().substring(document.getFichier().length()-3,  document.getFichier().length());
		else extension="";
		 if(extension.equalsIgnoreCase("pdf"))
			 images="/images/icone_pdf.png";
		 else  
			 images="/images/word.jpg";

		 cellimage.setImage(images);
		 cellimage.setParent(item);
	}

}