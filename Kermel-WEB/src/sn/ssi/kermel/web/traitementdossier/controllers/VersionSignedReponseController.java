package sn.ssi.kermel.web.traitementdossier.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCourrierAc;
import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygFilesAc;
import sn.ssi.kermel.be.entity.SygProjeReponse;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.referentiel.ejb.TypeAutoriteContractanteSession;
import sn.ssi.kermel.be.traitementdossier.ejb.FilesAcSession;
import sn.ssi.kermel.be.traitementdossier.ejb.ProjetReponseSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class VersionSignedReponseController extends AbstractWindow implements
AfterCompose, ListitemRenderer,org.zkoss.zk.ui.event.EventListener {

	private Div step0;
	private Div step1;
	private Div step2;
	private Listbox lstTypeAc;
	private Listbox lstAutCont;
	private Listbox lstTypeCourrier;
	private Image lastversion;
	private Listbox lstTypeNature;
	private Listbox lstModeTraitement;
	private Listbox lstModereception;
	private Listbox lstFiles;
	private Listbox lstCourrierRef;
	private Bandbox bdTypeAc;

	private Bandbox bdCourrierRef;
	private Paging pgTypeAC; 
	private Paging pgAc; 
	private Paging pgTypeCourrier; 
	private Paging pgNature; 
	private Paging pgModeTraitment; 
	private Paging pgModeReception; 

	private String codeTypeAc, libelleTypeAc;
	public static final String TODO = "TODO";

	private Label lblDenomination1,lblAdresse1,lblTel1,lblFax1,lblMail1,lblreference,lbldatecourrier,lblObjet;
	private final  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private int activePage;
	private final int byPageb = UIConstants.DSP_BANDBOXS_BY_PAGES;
	public static final String LIST_TYPE_AC_MODEL_CHANGE="onTypeACModelChange";
	public static final String LIST_FILES_MODEL_CHANGE="onFilesModelChange";
	public static final String LIST_TYPE_COURRIER_MODEL_CHANGE="onTypeCModelChange";
	public static final String LIST_AC_MODEL_CHANGE="onACModelChange";
	public static final String LIST_NATURE_COURRIER_MODEL_CHANGE="onNatureCModelChange";
	public static final String LIST_MODETRAITEMENT_COURRIER_MODEL_CHANGE="onModeTModelChange";
	public static final String LIST_MODERECPTION_COURRIER_MODEL_CHANGE="onModeRModelChange";
	public static final String LIST_COURRIERREF_COURRIER_MODEL_CHANGE="onCourrierREFModelChange";
	private Listheader lshCode,lshLibelle;
	public static final String CURRENT = "CURRENT";
	Session session = getHttpSession();
	SygTypeAutoriteContractante type=new SygTypeAutoriteContractante();
	private Long idtype;
	private Label lblType;
	private String login;
	private String mode ;


	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private static final String READY_MSG_STYLE = "";
	private Component errorComponent;
	private SygAutoriteContractante autoriteContractante;

	private SygCourrierAc courrier;
	private SygProjeReponse projeReponse;
	private SygFilesAc filesAc;
	private Textbox txtDenomination, txtRechercherTypeAc;
	private Textbox txtObjetFile, txtRechercherCourrierRef, txtReference, txtObjet, txtAmpliataire;
	private Textbox  txtRechercherTypeCourrier, txtRechercherNature, txtRechercherModeTraitement, txtRechercherModeReception;
	private Datebox dateSaisieFile, dateReception, dateCourrier;
	private Label lblDenomination, lblAdresse,lblTel, lblFax, lblMail;
	private Label lblStatusbar,txtFichiersrc;
	private Label lblStatusbar2;
	private final String cheminDossier = UIConstants.PATH_PJ_DOSSIER_URL;
	private String nomFichier;
	private Menuitem menuValiderStep1;
	private SygDossierCourrier dossierCourrier;
	private Iframe idIframe;

	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		addEventListener(LIST_TYPE_AC_MODEL_CHANGE, this);
		addEventListener(LIST_TYPE_COURRIER_MODEL_CHANGE, this);
		addEventListener(LIST_NATURE_COURRIER_MODEL_CHANGE, this);
		addEventListener(LIST_MODETRAITEMENT_COURRIER_MODEL_CHANGE, this);
		addEventListener(LIST_MODERECPTION_COURRIER_MODEL_CHANGE, this);
		addEventListener(LIST_COURRIERREF_COURRIER_MODEL_CHANGE, this);
		addEventListener(LIST_AC_MODEL_CHANGE, this);
		addEventListener(LIST_FILES_MODEL_CHANGE, this);
		addEventListener(Events.ON_UPLOAD, this);

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);


	}

	public void onCreate(CreateEvent event) {
		login = (String) getHttpSession().getAttribute("user");
		mode = (String) Executions.getCurrent().getAttribute("MODE");
		dossierCourrier = (SygDossierCourrier) getHttpSession().getAttribute("DOSSIERCOURRIER");
		autoriteContractante= dossierCourrier.getAutoriteContractante();
		//if(mode!=null && mode.equalsIgnoreCase(UIConstants.MODE_EDIT)){
		List<SygProjeReponse> projets = BeanLocator.defaultLookup(ProjetReponseSession.class).findByDossier(0, -1, null, dossierCourrier.getCode());

		for (SygProjeReponse projeReponse : projets) {
			if(projeReponse.isVesionFinal()){
				this.projeReponse = projeReponse;
				break;
			}
		}
		//projeReponse = BeanLocator.defaultLookup(ProjetReponseSession.class).findLastVersion(dossierCourrier.getCode());
		if(projeReponse!=null){
			initFormCourrier(projeReponse);
		}
		//}



	}

	@Override
	public void onEvent(Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(LIST_TYPE_AC_MODEL_CHANGE)) {

			List<SygTypeAutoriteContractante> types = BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).find(activePage,byPage,codeTypeAc,libelleTypeAc);
			SimpleListModel listModel = new SimpleListModel(types);
			lstTypeAc.setModel(listModel);
			pgTypeAC.setTotalSize(BeanLocator.defaultLookup( TypeAutoriteContractanteSession.class).count(codeTypeAc,libelleTypeAc));
		}else if (event.getName().equals(
				ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {

			Long code = (Long) ((HashMap<String, Object>) event.getData())
					.get(CURRENT);
			String toDo = (String) ((HashMap<String, Object>) event.getData())
					.get(TODO);

			if (toDo != null && toDo.equalsIgnoreCase("delLineFiles")) {

				BeanLocator.defaultLookup(FilesAcSession.class)
				.delete(code);
				Events.postEvent(LIST_FILES_MODEL_CHANGE, this, null);

			} else if (toDo != null && toDo.equalsIgnoreCase("openFile")) {


			}

		}else if(event.getName().equalsIgnoreCase(Events.ON_UPLOAD)){

		}


	}

	private class TypeAcRender implements ListitemRenderer{



		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypeAutoriteContractante typeAutoriteContractante = (SygTypeAutoriteContractante)data;
			item.setValue(typeAutoriteContractante);

			Listcell code = new Listcell(typeAutoriteContractante.getCode());
			code.setParent(item);

			Listcell libelle = new Listcell(typeAutoriteContractante.getLibelle());
			libelle.setParent(item);

			if(codeTypeAc!=null && codeTypeAc.equals(typeAutoriteContractante.getCode())){
				item.setSelected(true);
			}


		}

	}


	private class FilesRefRender implements ListitemRenderer, EventListener{

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygFilesAc filesAc = (SygFilesAc)data;
			item.setValue(filesAc);

			//	    Listcell nomFile = new Listcell(filesAc.getNameFile());
			//	    nomFile.setParent(item);

			Listcell dateFile = new Listcell(UtilVue.getInstance().formateLaDate(filesAc.getDateFile()));
			dateFile.setParent(item);

			Listcell objectFile = new Listcell("");
			if(filesAc.getObjectFile()!=null){
				objectFile = new Listcell(filesAc.getObjectFile());
			}
			objectFile.setParent(item);

			Listcell cellSupFile = new Listcell("", "/images/delete.png");
			cellSupFile.addEventListener(Events.ON_CLICK, this);
			// cellSupLigneDevis.addEventListener(Events.ON_CLICK,
			// (EventListener) this);
			cellSupFile.setAttribute(UIConstants.TODO, "delLineFiles");
			cellSupFile.setAttribute(UIConstants.ATTRIBUTE_CODE,
					filesAc.getId());
			cellSupFile.setParent(item);

			Listcell cellOpenFile = new Listcell("", "/images/PaperClip-16x16.png");
			cellOpenFile.addEventListener(Events.ON_DOUBLE_CLICK, this);
			// cellSupLigneDevis.addEventListener(Events.ON_CLICK,
			// (EventListener) this);
			cellOpenFile.setAttribute(UIConstants.TODO, "openFile");
			cellOpenFile.setAttribute(UIConstants.ATTRIBUTE_CODE,
					filesAc.getId());
			cellOpenFile.setParent(item);


		}

		@Override
		public void onEvent(Event event) throws Exception {
			if (event.getName().equalsIgnoreCase(Events.ON_CLICK)) {
				Listcell cell = (Listcell) event.getTarget();
				// String toDo = (String) cell.getAttribute(UIConstants.TODO);
				HashMap<String, String> display = new HashMap<String, String>();
				display.put(MessageBoxController.DSP_MESSAGE, Labels
						.getLabel("kermel.common.form.question.supprimer")
						+ " ?");
				display.put(MessageBoxController.DSP_TITLE, Labels
						.getLabel("kermel.courrier.file.supprimer.title"));
				display.put(MessageBoxController.DSP_HEIGHT, "150px");
				display.put(MessageBoxController.DSP_WIDTH, "100px");
				HashMap<String, Object> data = new HashMap<String, Object>();
				data
				.put(CURRENT, cell
						.getAttribute(UIConstants.ATTRIBUTE_CODE));
				data.put(TODO, cell.getAttribute(UIConstants.TODO));
				showMessageBox(display, data);
			}else if (event.getName().equalsIgnoreCase(Events.ON_DOUBLE_CLICK)) {

			}

		}

	}


	public void onClick$menuNextStep0(){
		if(lstAutCont.getSelectedItem()==null) {
			throw new WrongValueException(lstAutCont, Labels.getLabel(""));
		}
		autoriteContractante = (SygAutoriteContractante) lstAutCont.getSelectedItem().getValue();
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
		setLabelStep1();

	}

	public void onClick$menuNextStep1(){
		if(isRequieredValue()){
			step0.setVisible(false);
			step1.setVisible(false);
			step2.setVisible(true);
			setLabelStep2();
			Events.postEvent(LIST_FILES_MODEL_CHANGE, this, null);

		}else{
			lblStatusbar.setValue(errorMsg);
			lblStatusbar.setStyle(ERROR_MSG_STYLE);
			throw new WrongValueException(errorComponent, errorMsg);
		}

	}

	public void onClick$menuPrevious1(){
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);

	}

	public void onClick$menuPrevious2(){
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
	}


	public void onSelect$lstCourrierRef(){
		courrier = (SygCourrierAc) lstCourrierRef.getSelectedItem().getValue();
		if(courrier!=null){
			bdCourrierRef.setValue(courrier.getCourrierReference());
		}
		bdCourrierRef.close();
	}



	private void chargeFormAcc(SygFilesAc filesAc) {
		//txtNomFIchier.setValue(filesAc.getNameFile());
		dateSaisieFile.setValue(filesAc.getDateFile());
		txtFichiersrc.setValue(filesAc.getFileSrc());
		txtObjetFile.setValue(filesAc.getObjectFile());
		this.filesAc = filesAc;

	}

	public void onSelect$listeAccompagnant() {
		filesAc = (SygFilesAc) lstFiles.getSelectedItem().getValue();

		if (filesAc != null) {
			chargeFormAcc(filesAc);
		}
	}


	private boolean isRequieredValue() {
		try {

			/* Controle champ obligatoire */


			if (dateReception.getValue().equals("")
					|| dateReception.getValue() == null) {
				errorMsg = Labels
						.getLabel("kermel.courrier.nouveau.champdatereceptionobligatoire");
				errorComponent = dateReception;
				return false;
			}



			if (txtReference.getValue().equals("") || txtReference.getValue() == null) {
				errorMsg = Labels
						.getLabel("kermel.courrier.nouveau.champreferencecourrierobligatoire");
				errorComponent = txtReference;
				return false;
			}

			if (txtAmpliataire.getValue().equals("") || txtAmpliataire.getValue() == null) {
				errorMsg = Labels
						.getLabel("kermel.courrier.nouveau.champampliatairesobligatoire");
				errorComponent = txtAmpliataire;
				return false;
			}

			if (dateCourrier.getValue().equals("")
					|| dateCourrier.getValue() == null) {
				errorMsg = Labels
						.getLabel("kermel.courrier.nouveau.champdatecourrierobligatoire");
				errorComponent = dateCourrier;
				return false;
			}
			if (txtObjet.getValue() == null || txtObjet.getValue().equals("")) {
				errorMsg = Labels
						.getLabel("kermel.courrier.nouveau.champobjetcourrierobligatoire");
				errorComponent = txtObjet;
				return false;
			}

			if (dateCourrier.getValue().before(dateReception.getValue())) {
				errorMsg = Labels
						.getLabel("kermel.courrier.erreur.datesnoncoherentes")
						+ ": \""
						+ Labels
						.getLabel("kermel.courrier.erreur.datecourrier")
						+ "\"";
				errorComponent = dateCourrier;

				return false;
			}

			return true;
		} catch (Exception e) {
			lblStatusbar.setValue(e.toString());

			return false;
		}
	}

	public void setFormCourrier(){

		projeReponse.setCourrierAutoriteContractante(dossierCourrier.getAutoriteContractante());
		projeReponse.setCourrierDate(dateCourrier.getValue());
		projeReponse.setCourrierDateReception(dateReception.getValue());
		projeReponse.setCourrierRef(txtReference.getValue());
		projeReponse.setCourrierObjet(txtObjet.getValue());
		projeReponse.setAmplitaire(txtAmpliataire.getValue());
		projeReponse.setCourrierDateSaisie(new Date());
		projeReponse.setFile(nomFichier);
		projeReponse.setDossierCourrier(dossierCourrier);


	}

	private boolean saveCourrier() {

		try {
			projeReponse = BeanLocator.defaultLookup(ProjetReponseSession.class).save(
					projeReponse);

			return true;
		} catch (RuntimeException runtimeException) {
			if (runtimeException.getCause().getClass().getPackage().getName()
					.equals("javax.transaction")) {
				lblStatusbar.setValue(Labels
						.getLabel("atlantis.common.erreur.commit"));
				lblStatusbar.setStyle(ERROR_MSG_STYLE);
			}
			return false;
		} catch (Exception e) {
			lblStatusbar.setValue(e.toString());

			return false;
		}

	}

	private boolean updateCourrier() {

		try {
			projeReponse = BeanLocator.defaultLookup(ProjetReponseSession.class).update(
					projeReponse);

			return true;
		} catch (RuntimeException runtimeException) {
			if (runtimeException.getCause().getClass().getPackage().getName()
					.equals("javax.transaction")) {
				lblStatusbar.setValue(Labels
						.getLabel("atlantis.common.erreur.commit"));
				lblStatusbar.setStyle(ERROR_MSG_STYLE);
			}
			return false;
		} catch (Exception e) {
			lblStatusbar.setValue(e.toString());

			return false;
		}

	}

	public void onClick$menuValiderStep1() {

		if (isRequieredValue()) {

			if (mode!=null && mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				projeReponse = new SygProjeReponse();
				setFormCourrier();
				if (saveCourrier()) {

					menuValiderStep1.setDisabled(true);
					lblStatusbar.setValue(Labels.getLabel("kermel.enregistrement.succes"));
					lblStatusbar.setStyle("color:green;font-weight:bold;text-decoration:blank");
					Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
					detach();
				} else {
					lblStatusbar.setValue(errorMsg);
					lblStatusbar.setStyle(ERROR_MSG_STYLE);
					throw new WrongValueException(errorComponent, errorMsg);
				}
			} else if (mode!=null &&mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				setFormCourrier();
				if (updateCourrier()) {


					menuValiderStep1.setDisabled(true);
					lblStatusbar.setValue(Labels.getLabel("kermel.enregistrement.succes"));
					lblStatusbar.setStyle("color:green;font-weight:bold;text-decoration:blank");

					Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
					detach();
				} else {
					lblStatusbar.setValue(errorMsg);
					lblStatusbar.setStyle(ERROR_MSG_STYLE);
					throw new WrongValueException(errorComponent, errorMsg);
				}
			}

		} else {
			lblStatusbar.setValue(errorMsg);
			lblStatusbar.setStyle(ERROR_MSG_STYLE);
			throw new WrongValueException(errorComponent, errorMsg);
		}

	}
	private void setLabelStep1() {


		lblDenomination.setValue(autoriteContractante.getDenomination());

		if(autoriteContractante.getAdresse()!=null) {
			lblAdresse.setValue(autoriteContractante.getAdresse());
		}
		if(autoriteContractante.getFax()!=null) {
			lblFax.setValue(autoriteContractante.getFax());
		}

		if(autoriteContractante.getTelephone()!=null) {
			lblTel.setValue(autoriteContractante.getTelephone());
		}

		if(autoriteContractante.getEmail()!=null) {
			lblMail.setValue(autoriteContractante.getEmail());
		}

	}

	private void setLabelStep2() {
		lblDenomination1.setValue(lblDenomination1.getValue());
		lblAdresse1.setValue(lblAdresse.getValue());
		lblTel1.setValue(lblTel.getValue());
		lblFax1.setValue(lblFax.getValue());
		lblMail1.setValue(lblMail.getValue());
		lblreference.setValue(courrier.getCourrierReference());
		lbldatecourrier.setValue(UtilVue.getInstance().formateLaDate(courrier.getCourrierDate()));
		lblObjet.setValue(courrier.getCourrierObjet());

	}

	private boolean isRequiredFieldFile() {
		try {
			/* Controle champ obligatoire */
			//	    if (txtNomFIchier.getValue() == null || txtNomFIchier.getValue().equals("")) {
			//		errorMsg = Labels
			//			.getLabel("kermel.file.nouveau.champnomfichierobligatoire");
			//		errorComponent = txtNomFIchier;
			//		return false;
			//	    }
			if (txtFichiersrc.getValue() == null || txtFichiersrc.getValue().equals("")) {
				errorMsg = Labels
						.getLabel("kermel.file.nouveau.champfichiersrcobligatoire");
				errorComponent = txtFichiersrc;
				return false;
			}
			//	    if (txtObjetFile.getValue() == null || txtObjetFile.getValue().equals("")) {
			//		errorMsg = Labels
			//			.getLabel("kermel.file.nouveau.champobjetfileobligatoire");
			//		errorComponent = txtObjetFile;
			//		return false;
			//	    }
			if (dateSaisieFile.getValue() == null || dateSaisieFile.getValue().equals("")) {
				errorMsg = Labels
						.getLabel("kermel.file.nouveau.champodatesaisieobligatoire");
				errorComponent = dateSaisieFile;
				return false;
			}

			return true;
		} catch (Exception e) {

			return true;
		}

	}

	public void onClick$cellValider3() {

		if (isRequiredFieldFile()) {
			if (filesAc == null || filesAc.getId() == 0) {
				filesAc = new SygFilesAc();
				filesAc.setCourrier(courrier);
			}
			setFormFile();
			if (saveFile()) {
				initFormAll();
				Events.postEvent(LIST_FILES_MODEL_CHANGE, this, null);
			} else {
				lblStatusbar2.setValue(errorMsg);
				lblStatusbar2.setStyle(ERROR_MSG_STYLE);
				throw new WrongValueException(lstFiles, errorMsg);
			}

		} else {
			lblStatusbar2.setValue(errorMsg);
			lblStatusbar2.setStyle(ERROR_MSG_STYLE);
			throw new WrongValueException(lstFiles, errorMsg);
		}
	}

	private boolean saveFile() {
		try {
			BeanLocator.defaultLookup(FilesAcSession.class).save(filesAc);
			return true;
		} catch (RuntimeException runtimeException) {
			if (runtimeException.getCause().getClass().getPackage().getName()
					.equals("javax.transaction")) {
				lblStatusbar2.setValue(Labels
						.getLabel("atlantis.common.erreur.commit"));
				lblStatusbar2.setStyle(ERROR_MSG_STYLE);
			}
			return false;
		} catch (Exception e) {
			lblStatusbar2.setValue(e.toString());

			return false;
		}
	}

	private void setFormFile() {

		filesAc.setCourrier(courrier);
		//filesAc.setNameFile(txtNomFIchier.getValue());
		//filesAc.setFileSrc(txtFichiersrc.getValue());
		filesAc.setObjectFile(txtObjetFile.getValue());
		filesAc.setDateFile(dateSaisieFile.getValue());
	}

	private void chargeFormAll(SygFilesAc file) {
		//txtNomFIchier.setValue(filesAc.getNameFile());
		txtFichiersrc.setValue(filesAc.getFileSrc());
		txtObjetFile.setValue(filesAc.getObjectFile());
		dateSaisieFile.setValue(filesAc.getDateFile());

	}

	private void initFormAll() {

		//txtNomFIchier.setValue(null);
		txtFichiersrc.setValue(null);
		txtObjetFile.setValue(null);
		dateSaisieFile.setValue(null);
	}

	public void onSelect$lstFiles() {

		filesAc =  (SygFilesAc) lstFiles.getSelectedItem().getValue();
		if (filesAc != null) {
			chargeFormAll(filesAc);
		}
	}

	private void initFormCourrier(SygProjeReponse courrier){

		autoriteContractante = courrier.getCourrierAutoriteContractante();
		//codeAc = autoriteContractante.getSigle();
		txtReference.setValue(courrier.getCourrierRef());

		txtObjet.setValue(courrier.getCourrierObjet());

		dateReception.setValue(courrier.getCourrierDateReception());

		dateCourrier.setValue(courrier.getCourrierDate());

		txtAmpliataire.setValue(courrier.getAmplitaire());
		lastversion.setTooltip(courrier.getFile());





	}

	public void onClick$lastversion() {
		step0.setVisible(false);
		step1.setVisible(true);
		String filepath = cheminDossier +  projeReponse.getFile();
		File f = new File(filepath.replaceAll("\\\\", "/"));

		org.zkoss.util.media.AMedia mymedia = null;
		try {
			mymedia = new AMedia(f, null, null);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (mymedia != null)
			idIframe.setContent(mymedia);
		else
			idIframe.setSrc("");

		idIframe.setHeight("600px");
		idIframe.setWidth("100%");
	}
	public void onClick$lastversion___(){
		lastversion.setSrc(UIConstants.PATH_PJ_DOSSIER_URL+"/"+projeReponse.getFile());
	}
	public void onUpload(UploadEvent event){

		Messagebox.show((String) Executions.getCurrent().getAttribute("FICHIER"));
	}





	public void onClick$btnRechercherTypeAc(){
		if(txtRechercherTypeAc.getValue().trim()!=null && !txtRechercherTypeAc.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.marche.Type")) ){
			libelleTypeAc = txtRechercherTypeAc.getValue();
		}else{
			libelleTypeAc = null;
		}

		Events.postEvent(LIST_TYPE_AC_MODEL_CHANGE, this, null);
	}



	public void onFocus$txtDenomination(){
		if(txtDenomination.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.denomination"))){
			txtDenomination.setValue(null);
		}
	}

	public void onBlur$txtDenomination(){
		if(txtDenomination.getValue().equalsIgnoreCase("")||txtDenomination.getValue()==null){
			txtDenomination.setValue(Labels.getLabel("kermel.referentiel.common.denomination"));
		}
	}

	public void onFocus$txtRechercherTypeAc(){
		if(txtRechercherTypeAc.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.marche.Type"))){
			txtRechercherTypeAc.setValue(null);
		}
	}

	public void onBlur$txtRechercherTypeAc(){
		if(txtRechercherTypeAc.getValue().equalsIgnoreCase("")||txtRechercherTypeAc.getValue()==null){
			txtRechercherTypeAc.setValue(Labels.getLabel("kermel.referentiel.common.marche.Type"));
		}
	}

	public void onFocus$txtRechercherTypeCourrier(){
		if(txtRechercherTypeCourrier.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.courrier.form.choix.type"))){
			txtRechercherTypeCourrier.setValue(null);
		}
	}

	public void onBlur$txtRechercherTypeCourrier(){
		if(txtRechercherTypeCourrier.getValue().equalsIgnoreCase("")||txtRechercherTypeCourrier.getValue()==null){
			txtRechercherTypeCourrier.setValue(Labels.getLabel("kermel.courrier.form.choix.type"));
		}
	}


	public void onFocus$txtRechercherNature(){
		if(txtRechercherNature.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.courrier.form.choix.nature"))){
			txtRechercherNature.setValue(null);
		}
	}

	public void onBlur$txtRechercherNature(){
		if(txtRechercherNature.getValue().equalsIgnoreCase("")||txtRechercherNature.getValue()==null){
			txtRechercherNature.setValue(Labels.getLabel("kermel.courrier.form.choix.nature"));
		}
	}


	public void onFocus$txtRechercherModeTraitement(){
		if(txtRechercherModeTraitement.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.courrier.form.choix.modetraitement"))){
			txtRechercherModeTraitement.setValue(null);
		}
	}

	public void onBlur$txtRechercherModeTraitement(){
		if(txtRechercherModeTraitement.getValue().equalsIgnoreCase("")||txtRechercherModeTraitement.getValue()==null){
			txtRechercherModeTraitement.setValue(Labels.getLabel("kermel.courrier.form.choix.modetraitement"));
		}
	}

	public void onFocus$txtRechercherModeReception(){
		if(txtRechercherModeReception.getValue().trim().equalsIgnoreCase(Labels.getLabel("kermel.courrier.form.choix.modereception"))){
			txtRechercherModeReception.setValue(null);
		}
	}

	public void onBlur$txtRechercherModeReception(){
		if(txtRechercherModeReception.getValue().equalsIgnoreCase("")||txtRechercherModeReception.getValue()==null){
			txtRechercherModeReception.setValue(Labels.getLabel("kermel.courrier.form.choix.modereception"));
		}
	}


	public void onClick$bcUploadFile(){
		if (ToolKermel.isWindows()) {
			nomFichier = FileLoader.uploadPieceDossier(UtilVue.getInstance().formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
		} else {
			nomFichier = FileLoader.uploadPieceDossier(UtilVue.getInstance().formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));
		}
		txtFichiersrc.setValue("");
		txtFichiersrc.setValue(nomFichier);
	}

	public void onClick$menuCancelStep1(){
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();
	}
	
	public org.zkoss.util.media.AMedia fetchFile(File file) {

		org.zkoss.util.media.AMedia mymedia = null;
		try {
			mymedia = new AMedia(file, null, null);
			return mymedia;
		} catch (Exception e) {

			e.printStackTrace();
			return null;
		}

	}
	public void onClick$menuFermer() {
		step0.setVisible(true);
		step1.setVisible(false);
	}
}