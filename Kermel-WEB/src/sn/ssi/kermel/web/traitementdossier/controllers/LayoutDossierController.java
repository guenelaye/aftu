package sn.ssi.kermel.web.traitementdossier.controllers;

import java.util.Date;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Include;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.West;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.traitementdossier.ejb.DossierAcSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class LayoutDossierController extends AbstractWindow implements
AfterCompose, ListitemRenderer, EventListener {

    private Textbox txtSearch;
    private Include content;
    private Combobox listCategory;
    private Tree tree;
    private final Treechildren child = new Treechildren();
    private final Session session = getHttpSession();
    private Listbox list, listProcedures;
    private SygDossierCourrier dossier = null;
    private String openWest;
    private West wps;
    private Object selobj;
    private String codeType;
    @Override
    public void onEvent(Event arg0) throws Exception {
	// TODO Auto-generated method stub

    }

    @Override
    public void render(Listitem arg0, Object arg1, int index) throws Exception {
	// TODO Auto-generated method stub

    }

    @Override
    public void afterCompose() {
	Components.wireFellows(this, this);
	Components.addForwards(this, this);
	dossier = (SygDossierCourrier) Executions.getCurrent().getAttribute("dossier");
	openWest = (String) Executions.getCurrent().getAttribute("openWest");
	txtSearch.setValue("Recherche");
	List<SygDossierCourrier> dossiers = BeanLocator.defaultLookup(DossierAcSession.class).find(0, -1, null, null);
	for (SygDossierCourrier dossier : dossiers) {
		int delai = dossier.getTypesDossiers().getDelaitraitement();
		long nbjr = ToolKermel.DifferenceDate(new Date(), dossier.getDateStatus(), "jj");
		if(delai<nbjr){
			dossier.setStatus(UIConstants.DOSSIERAC_RETARD);
		}else{
			dossier.setStatus(UIConstants.DOSSIERAC_ENCOURS);
		}
		BeanLocator.defaultLookup(DossierAcSession.class).update(dossier);
	}

	initTree();



    }

    private void initTree(){
	child.setParent(tree);
	chargerDossier();
	content.setSrc("/traitementdossier/list.zul");
    }

    public void onSelect$tree(){

	tree.isInvalidated();
	Object selobj = tree.getSelectedItem().getValue();

	session.setAttribute("selObj", selobj);
	content.invalidate();

	if(selobj instanceof String){

	    codeType= (String)selobj;
	    content.setSrc("/traitementdossier/list.zul");
	    content.setMode("defer");

	} 
    }

    public void onFocus$txtSearch(){
	if(txtSearch.getValue() == "Recherche") {
	    txtSearch.setValue("");
	}
    }

    public void onBlur$txtSearch(){
	if(txtSearch.getValue()=="") {
	    txtSearch.setValue("Recherche");
	}
    }

    public void chargerDossier(){
	String key = null;

	if(!"".equals(txtSearch.getValue()) && !txtSearch.getValue().equals("Recherche")) {
	    key = txtSearch.getValue();
	}


	try {
	    List<Object[]> dossierCourriers = BeanLocator.defaultLookup(DossierAcSession.class).groupByType(0, -1,null);

	    if(dossierCourriers.size() > 0){
		int i = 0;
		for (Object[] dossier : dossierCourriers) {
		    Treeitem processItem = new Treeitem();
		    processItem.setAttribute("index", i);

		    processItem.setLabel((String) dossier[1]+" ["+dossier[0]+"]");
		    processItem.setImage("/img/z-bullet1.gif");
		    processItem.setValue(dossier[2]);
		    processItem.setOpen(true);
		    processItem.setParent(child);
		    //		    		    if(this.dossier!=null && (dossier.getId()==this.dossier.getId())){
		    //		    			processItem.setSelected(true);
		    //		    			processItem.setOpen(true);
		    //		    			tree.setSelectedItem(processItem);
		    //		    
		    //		    			onSelect$tree();
		    //		    		    }
		    i++;

		    List<Object[]> dossierCourriersBystate = BeanLocator.defaultLookup(DossierAcSession.class).groupByStateAndType(0, -1, (String) dossier[2]);
		    Treechildren procedureTreechildren = new Treechildren();

		    for (Object[] dossierState : dossierCourriersBystate) {
			Treeitem etapeItem = new Treeitem();

			etapeItem.setLabel((String) dossierState[1]+" ["+dossierState[0]+"]");
			if(((String)dossierState[1]).equalsIgnoreCase(UIConstants.DOSSIERAC_ENCOURS)) {
			    etapeItem.setImage("/images/rdo_on.png");
			}else if(((String)dossierState[1]).equalsIgnoreCase(UIConstants.DOSSIERAC_RETARD)){
			    etapeItem.setImage("/images/rdo_off.png");
			}


			etapeItem.setValue(dossierState[2]+"�"+dossierState[1]);
			etapeItem.setOpen(true);

			etapeItem.setParent(procedureTreechildren);
		    }
		    procedureTreechildren.setParent(processItem);
		}

	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

}
