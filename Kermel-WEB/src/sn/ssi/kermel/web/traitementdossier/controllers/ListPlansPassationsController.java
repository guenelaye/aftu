package sn.ssi.kermel.web.traitementdossier.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.plansdepassation.ejb.PlansdepassationSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;
import sn.ssi.kermel.web.referentiel.controllers.AutoriteContractanteFormController;

@SuppressWarnings("serial")
public class ListPlansPassationsController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtNom,txtFonction,txtService;
    String prenom=null,reference,callBack,fonction=null,service=null,page=null;
    private Listheader lshNumero,lshAnnee,lshDatecreation,lshDatemev;
    Session session = getHttpSession();
    String login,status;
   // private KermelSousMenu monSousMenu;
  //  private Menuitem ADD_PLANPASSATION, WREAL_PLANPASSATION, SUPP_PLANPASSATION, WSOUM_PLANPASSATION, WVALID_PLANPASSATION, WWPUB_PLANPASSATION;
    List<SygRealisations> realisations = new ArrayList<SygRealisations>();
    SygPlansdepassation plan=new SygPlansdepassation();
    private String type;
    private Utilisateur infoscompte;
    private SygAutoriteContractante autorite;
    private Intbox annee;
    private int gestion=-1;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
    
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_REALISATIONS, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_VALIDATIONS, this);
		addEventListener(ApplicationEvents.ON_PUBLIER, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lshNumero.setSortAscending(new FieldComparator("Numplan", false));
		lshNumero.setSortDescending(new FieldComparator("Numplan", true));
		lshAnnee.setSortAscending(new FieldComparator("annee", false));
		lshAnnee.setSortDescending(new FieldComparator("annee", true));
		lshDatecreation.setSortAscending(new FieldComparator("datecreation", false));
		lshDatecreation.setSortDescending(new FieldComparator("datecreation", true));
		lshDatemev.setSortAscending(new FieldComparator("dateMiseEnValidation", false));
		lshDatemev.setSortDescending(new FieldComparator("dateMiseEnValidation", true));
				
				
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
     	login = ((String) getHttpSession().getAttribute("user"));
	}

	@SuppressWarnings("unchecked")
	public void onCreate(final CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if((infoscompte.getType().equals(UIConstants.USERS_TYPES_AC)))
		{
			autorite=infoscompte.getAutorite();
		}
		else
		{
			autorite=null;
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygPlansdepassation> plans = BeanLocator.defaultLookup(PlansdepassationSession.class).Plans(activePage,byPage,null,null,null,autorite, 1,gestion);
			 SimpleListModel listModel = new SimpleListModel(plans);
			 lstListe.setModel(listModel);
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(PlansdepassationSession.class).countPlans(null,null,null,autorite, 1,gestion));
		} 
	
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DETAILS)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			Long codes=(Long) lstListe .getSelectedItem().getValue();
			plan=BeanLocator.defaultLookup(PlansdepassationSession.class).findById(codes);
			realisations=BeanLocator.defaultLookup(RealisationsSession.class).find(0,-1,null,null,plan, null);
			if(realisations.size()>0)
			{
				final String uri = "/plansdepassation/formsousmissionvalidation.zul";

				final HashMap<String, String> display = new HashMap<String, String>();
				display.put(DSP_HEIGHT,"650px");
				display.put(DSP_WIDTH, "80%");
				display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.soumettre.validation"));

				final HashMap<String, Object> data = new HashMap<String, Object>();
				data.put(AutoriteContractanteFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
				data.put(AutoriteContractanteFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

				showPopupWindow(uri, data, display);
			}
			else
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.realisation.saisir"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			
		} 

	
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
				
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				
				Date datevalidation=(Date) lstListe.getSelectedItem().getAttribute("datevalidation");
				if(datevalidation==null)
				{
					HashMap<String, String> display = new HashMap<String, String>(); // permet
					display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
					display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
					display.put(MessageBoxController.DSP_HEIGHT, "250px");
					display.put(MessageBoxController.DSP_WIDTH, "47%");
	
					HashMap<String, Object> map = new HashMap<String, Object>(); // permet
					map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
					showMessageBox(display, map);
				}
				else
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.suppressionplan"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				}
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(PlansdepassationSession.class).delete(codes);
				BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_PLANPASSATION", Labels.getLabel("kermel.plansdepassation.suppressionn")+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_REALISATIONS)) {
				if (lstListe.getSelectedItem() == null)
				  throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				
				session.setAttribute("idplan", lstListe.getSelectedItem().getValue());
				loadApplicationState("realisations");
			} 
	}
	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygPlansdepassation plans = (SygPlansdepassation) data;
		item.setValue(plans.getIDinfoplan());
		item.setAttribute("status", plans.getStatus());
		

		 Listcell cellNumero = new Listcell(plans.getNumplan());
		 cellNumero.setParent(item);
		 
		 Listcell cellAnnee = new Listcell(Integer.toString(plans.getAnnee()));
		 cellAnnee.setParent(item);
		 
		 Listcell cellDatecreation = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDatecreation()));
		 cellDatecreation.setParent(item);
		 
		 Listcell cellDatemiseenvalidation = new Listcell("");
		  if(plans.getDateMiseEnValidation()!=null)
		  {
			  cellDatemiseenvalidation.setLabel(UtilVue.getInstance().formateLaDate(plans.getDateMiseEnValidation()));
			  item.setAttribute("datevalidation", plans.getDateMiseEnValidation());
		  }
		  else
		  {
			  item.setAttribute("datevalidation", null);
		  }
		 cellDatemiseenvalidation.setParent(item);
		 
		 Listcell cellStatut = new Listcell(plans.getStatus());
		   if(plans.getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.saisie")))
			   cellStatut.setLabel(Labels.getLabel("kermel.plansdepassation.statut.saisies"));
		   else  if(plans.getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.miseenvalidation")))
			   cellStatut.setLabel(Labels.getLabel("kermel.plansdepassation.statut.miseenvalidations"));
		   else  if(plans.getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.valider")))
			   cellStatut.setLabel(Labels.getLabel("kermel.plansdepassation.statut.validers")+" "+UtilVue.getInstance().formateLaDate(plans.getDateValidation()));
		   else  if(plans.getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.rejeter")))
			   cellStatut.setLabel(Labels.getLabel("kermel.plansdepassation.statut.rejeters")+" "+UtilVue.getInstance().formateLaDate(plans.getDateRejet()));
		   else  if(plans.getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.publier")))
			   cellStatut.setLabel(Labels.getLabel("kermel.plansdepassation.statut.publiers")+" "+UtilVue.getInstance().formateLaDate(plans.getDatepublication()));
	   	 cellStatut.setParent(item);
		
		 Listcell cellCommentaires = new Listcell(plans.getCommentaires());
		 cellCommentaires.setParent(item);

	}
	public void onClick$bchercher()
	{
		
		if(annee.getValue()==null)
		 {
			gestion=-1;
		
		 }
		else
		 {
			gestion=annee.getValue();
			page="0";
		 }
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	

	
	public void onOK$annee()
	{
		onClick$bchercher();
	}
	
}