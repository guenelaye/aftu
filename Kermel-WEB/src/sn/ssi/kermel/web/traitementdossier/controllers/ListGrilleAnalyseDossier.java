package sn.ssi.kermel.web.traitementdossier.controllers;

import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Image;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygDossierGrilleAnalyse;
import sn.ssi.kermel.be.traitementdossier.ejb.DossierAcSession;
import sn.ssi.kermel.be.traitementdossier.ejb.DossierGrilleAnalyseSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class ListGrilleAnalyseDossier extends AbstractWindow implements AfterCompose , EventListener,ListitemRenderer {

    private Grid lstListe;
    private Paging pgPagination;
    private SygDossierCourrier dossierCourrier;
    private Radiogroup rdIssue;

    private SygDossierGrilleAnalyse  grilleanalyse;
    @Override
    public void render(Listitem item, Object data, int index)  throws Exception {
	SygDossierGrilleAnalyse  grilleanalyse = (SygDossierGrilleAnalyse) data;
	item.setValue(grilleanalyse);

	Listcell pieces = new Listcell(grilleanalyse.getGrillesAnalyse().getAnalyse().getLibellecritereanalyse());
	pieces.setParent(item);

	Image image = new Image();
	Listcell statut = new Listcell();
	if(grilleanalyse.getStatut()==0){
	    image.setSrc("/images/cancel.png");
	}else{
	    image.setSrc("/images/ok.png");
	}
	image.addEventListener(Events.ON_DOUBLE_CLICK, this);
	image.setAttribute("PIECE", grilleanalyse);
	image.setParent(statut);
	statut.setParent(item);

    }

    @Override
    public void onEvent(Event event) throws Exception {

	if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
	    lstListe.setRowRenderer(new EditsRowBtnGrilleAnalyseRenderer());
	    List<SygDossierGrilleAnalyse> grilleanalyses = BeanLocator.defaultLookup(DossierGrilleAnalyseSession.class).findByDossier(dossierCourrier);
	    SimpleListModel listModel = new SimpleListModel(grilleanalyses);
	    lstListe.setModel(listModel);
	    //  pgPagination.setTotalSize(BeanLocator.defaultLookup(DossierAcSession.class).countByType(type));
	} 
	else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DOUBLE_CLICK)) {
	    Image image = (Image) event.getTarget();
	    SygDossierGrilleAnalyse analyse = (SygDossierGrilleAnalyse) image.getAttribute("PIECE");
	    if(analyse!=null){
		if(analyse.getStatut()==1){
		    analyse.setStatut(0);
		}else{
		    analyse.setStatut(1);
		}

		BeanLocator.defaultLookup(DossierGrilleAnalyseSession.class).update(analyse);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	    }
	} 

    }

    @Override
    public void afterCompose() {
	Components.wireFellows(this, this);
	Components.addForwards(this, this);

	addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
	addEventListener(Events.ON_DOUBLE_CLICK, this);
	addEventListener(ApplicationEvents.ON_ADD, this);
	addEventListener(ApplicationEvents.ON_EDIT, this);
	addEventListener(ApplicationEvents.ON_DETAILS, this);
	addEventListener(ApplicationEvents.ON_DELETE, this);


	dossierCourrier = (SygDossierCourrier) getHttpSession().getAttribute("DOSSIERCOURRIER");

	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

    }

    public void onClick$menuValider(){
    	
    	if(rdIssue.getSelectedItem().getValue()!=null){
    		dossierCourrier.setIssue(Integer.parseInt((String) rdIssue.getSelectedItem().getValue()));
    	}
    	BeanLocator.defaultLookup(DossierAcSession.class).update(dossierCourrier);
    	
    }
}
