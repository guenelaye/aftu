package sn.ssi.kermel.web.traitementdossier.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.courriers.ejb.CourriersAcSession;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCourrierAc;
import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygModeReception;
import sn.ssi.kermel.be.entity.SygTypesCourrier;
import sn.ssi.kermel.be.traitementdossier.ejb.TypeCourrierSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class ListCourrierSuivi extends AbstractWindow implements
AfterCompose, EventListener, ListitemRenderer {
    private static final long serialVersionUID = 1L;
    private Listbox lstListe,lstTypeCourrier;
    private Paging pgPagination,pgTypeCourrier;
    private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage,page;
    public static final String CURRENT_MODULE="CURRENT_MODULE";
    private String courrierReference,typecourrierlibelle;
    private Date courrierDateReception, courrierDate;
    private SygDossierCourrier dossierCourrier;
    private SygModeReception modeReception;
    private SygAutoriteContractante ac;
    private SygTypesCourrier typecourrier;
    private Textbox txtcourRef,txtRechercherTypeCourrier;
    private Datebox dbDateReception, dbDateCourrier;
    private Bandbox bandTypecourrier;
    private SygTypesCourrier typeCourrier;
    private Listheader lshcourrierReference,lshcourrierDateReception,lshcourrierDate;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_COURRIERACDOS, SUP_COURRIERACDOS;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private String nameFile;

    @Override
    public void onEvent(Event event) throws Exception {		
	// TODO Auto-generated method stub
	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)){
	    if (event.getData() != null) {
		activePage = Integer.parseInt((String) event.getData());
		byPage = -1;
		pgPagination.setPageSize(1000);
	    } else {
		byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
		activePage = pgPagination.getActivePage() * byPage;
		pgPagination.setPageSize(byPage);
	    }
	    List<SygCourrierAc> courrier = BeanLocator.defaultLookup(CourriersAcSession.class).findByDossier(activePage,byPage, dossierCourrier.getId());
	    SimpleListModel listModel = new SimpleListModel(courrier);
	    lstListe.setModel(listModel);
	    pgPagination.setTotalSize(BeanLocator.defaultLookup(CourriersAcSession.class).countByDossier(dossierCourrier.getId()));
	}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)){
	    if (event.getData() != null) {
		activePage = Integer.parseInt((String) event.getData());
		byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
		pgTypeCourrier.setPageSize(byPageBandbox);				

	    }
	    else {
		byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
		activePage = pgTypeCourrier.getActivePage() * byPageBandbox;
		pgTypeCourrier.setPageSize(byPageBandbox);				

	    }
	    //intrant
	    List<SygTypesCourrier> typecourrier = BeanLocator.defaultLookup(TypeCourrierSession.class).find(activePage, byPageBandbox, typecourrierlibelle, null, null);
	    lstTypeCourrier.setModel(new SimpleListModel(typecourrier));
	    pgTypeCourrier.setTotalSize(BeanLocator.defaultLookup(TypeCourrierSession.class).count(typecourrierlibelle));

	}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)){
	    final String uri = "/traitementdossier/suividossier/addCourrier.zul";

	    final HashMap<String, String> display = new HashMap<String, String>();
	    display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.courrier"));
	    display.put(DSP_HEIGHT,"600px");
	    display.put(DSP_WIDTH, "80%");

	    final HashMap<String, Object> data = new HashMap<String, Object>();
	    //		data.put(ModeReceptionFormController.WINDOW_PARAM_MODE,
	    //				UIConstants.MODE_NEW);
	    Executions.getCurrent().setAttribute("MODE", UIConstants.MODE_NEW);

	    showPopupWindow(uri, data, display);
	}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)){

	    if(lstListe.getSelectedItem()==null) {
		throw new WrongValueException(lstListe, "");
	    }

	    final String uri = "/courriers/form.zul";
	    final HashMap<String, String> display = new HashMap<String, String>();
	    display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.courrier.update"));
	    display.put(DSP_HEIGHT,"600px");
	    display.put(DSP_WIDTH, "80%");

	    final HashMap<String, Object> data = new HashMap<String, Object>();	    
	    Executions.getCurrent().setAttribute("MODE", UIConstants.MODE_EDIT);
	    Executions.getCurrent().setAttribute("COURRIER", lstListe.getSelectedItem().getValue());

	    showPopupWindow(uri, data, display);
	}else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_SUIVI)) {

	    if (lstListe.getSelectedItem() == null) {
		throw new WrongValueException(lstListe, "Veillez selectionnez un element svp!");
	    } else {
		if (lstListe.getSelectedCount() > 1) {
		    throw new WrongValueException(lstListe, "Veillez selectionnez un seul element svp!");
		} else {			
		    session.setAttribute("courrierAcInssession", lstListe.getSelectedItem().getValue());
		    loadApplicationState("suivi_courrierac");

		}
	    }
	}
    }
    @Override
    public void afterCompose() {
	// TODO Auto-generated method stub
	Components.wireFellows(this, this);
	Components.addForwards(this, this);


	monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
	monSousMenu.setFea_code(UIConstants.FEA_COURRIERACDOSS);
	monSousMenu.afterCompose();
	/* pour les menuitem eventuellement crees */
	Components.wireFellows(this, this); // mais le wireFellows precedent
	// reste indispensable
	/* reprise des forwards definis dans le .zul */
	if (ADD_COURRIERACDOS != null) { ADD_COURRIERACDOS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
	if (SUP_COURRIERACDOS != null) { SUP_COURRIERACDOS.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }

	addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
	addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
	addEventListener(ApplicationEvents.ON_ADD, this);
	addEventListener(ApplicationEvents.ON_EDIT, this);
	addEventListener(ApplicationEvents.ON_DETAILS, this);
	addEventListener(ApplicationEvents.ON_DELETE, this);
	addEventListener(ApplicationEvents.ON_SUIVI, this);

	addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	lshcourrierReference.setSortAscending(new FieldComparator("courrierReference", false));
	lshcourrierReference.setSortDescending(new FieldComparator("courrierReference", true));

	lshcourrierDate.setSortAscending(new FieldComparator("courrierDate", false));
	lshcourrierDate.setSortDescending(new FieldComparator("courrierDate", true));

	lshcourrierDateReception.setSortAscending(new FieldComparator("courrierDateReception", false));
	lshcourrierDateReception.setSortDescending(new FieldComparator("courrierDateReception", true));

	lstListe.setItemRenderer(this);
	pgPagination.setPageSize(byPage);
	pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
	nameFile =(String) Executions.getCurrent().getParameter("nameFile");
	
	
	if(nameFile!=null){
	    Events.postEvent(ApplicationEvents.ON_EDIT, this, null);
	}else{
	    Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	dossierCourrier = (SygDossierCourrier) getHttpSession().getAttribute("DOSSIERCOURRIER");

	//login = ((String) getHttpSession().getAttribute("user"));
    }
    @Override
    public void render(Listitem item, Object data, int index)  throws Exception {
	// TODO Auto-generated method stub
	SygCourrierAc myCourrier = (SygCourrierAc) data;
	item.setValue(myCourrier);

	Listcell cellReference = new Listcell(myCourrier.getCourrierReference());
	cellReference.setParent(item);

	Listcell cellTypeCourrier = new Listcell("");
	if(myCourrier.getCourrierType()!=null) {
	    cellTypeCourrier.setLabel(myCourrier.getCourrierType().getLibelletypecourrier());
	}
	cellTypeCourrier.setParent(item);

	Listcell cellDateCourrier = new Listcell(UtilVue.getInstance().formateLaDate(myCourrier.getCourrierDate()));
	cellDateCourrier.setParent(item);

	Listcell cellDateReception = new Listcell(UtilVue.getInstance().formateLaDate(myCourrier.getCourrierDateReception()));
	cellDateReception.setParent(item);

	Listcell cellAC = new Listcell("");
	if(myCourrier.getCourrierAutoriteContractante()!=null) {
	    cellAC.setLabel(myCourrier.getCourrierAutoriteContractante().getDenomination()+"("+myCourrier.getCourrierAutoriteContractante().getSigle()+")");
	}
	cellAC.setParent(item);

	Listcell cellModeReception = new Listcell("");
	if(myCourrier.getCourrierModeReception()!=null) {
	    cellModeReception.setLabel(myCourrier.getCourrierModeReception().getLibellemodereception());
	}
	cellModeReception.setParent(item);
    }


    //Recherche sur TypeCourrier
    public void onSelect$lstTypeCourrier(){
	typeCourrier = (SygTypesCourrier) lstTypeCourrier.getSelectedItem().getValue();
	bandTypecourrier.setValue(typeCourrier.getLibelletypecourrier());		
	bandTypecourrier.close();
    }

    public class TypeCourrierRenderer implements ListitemRenderer {

	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
	    SygTypesCourrier type = (SygTypesCourrier) data;
	    item.setValue(type);

	    Listcell cellLibelle = new Listcell(type.getLibelletypecourrier());
	    cellLibelle.setParent(item);

	}
    }


    public void onFocus$txtRechercherTypeCourrier(){
	if(txtRechercherTypeCourrier.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.rechercher"))){
	    txtRechercherTypeCourrier.setValue("");
	}		 
    }

    public void  onClick$btnRechercherTypeCourrier(){
	if(txtRechercherTypeCourrier.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.rechercher")))
	{
	    typecourrierlibelle = null;
	    //page = null;
	}
	else
	{
	    typecourrierlibelle = txtRechercherTypeCourrier.getValue();
	    //page="0";
	}

	Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
    }


}
