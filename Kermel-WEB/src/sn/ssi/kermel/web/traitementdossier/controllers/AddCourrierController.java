package sn.ssi.kermel.web.traitementdossier.controllers;

import java.util.Date;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.courriers.ejb.CourriersAcSession;
import sn.ssi.kermel.be.courriers.ejb.TCourriersAcSession;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCourrierAc;
import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygDossierGrilleAnalyse;
import sn.ssi.kermel.be.entity.SygDossierModeleDoc;
import sn.ssi.kermel.be.entity.SygDossierPieceRequises;
import sn.ssi.kermel.be.entity.SygGrillesAnalyses;
import sn.ssi.kermel.be.entity.SygModeReception;
import sn.ssi.kermel.be.entity.SygPiecesrequises;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygTCourrierAC;
import sn.ssi.kermel.be.entity.SygTDossierCourrier;
import sn.ssi.kermel.be.entity.SygTypeModeleDocument;
import sn.ssi.kermel.be.entity.SygTypesCourrier;
import sn.ssi.kermel.be.entity.SygTypesDossiers;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.plansdepassation.ejb.PlansdepassationSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsSession;
import sn.ssi.kermel.be.traitementdossier.ejb.DossierAcSession;
import sn.ssi.kermel.be.traitementdossier.ejb.DossierGrilleAnalyseSession;
import sn.ssi.kermel.be.traitementdossier.ejb.DossierModeleDocSession;
import sn.ssi.kermel.be.traitementdossier.ejb.DossierPiecesRequises;
import sn.ssi.kermel.be.traitementdossier.ejb.GrillesAnalysesSession;
import sn.ssi.kermel.be.traitementdossier.ejb.PiecesRequisesSession;
import sn.ssi.kermel.be.traitementdossier.ejb.TypeModelesDocumentsSession;
import sn.ssi.kermel.be.traitementdossier.ejb.TypesDossiersSession;
import sn.ssi.kermel.be.workflow.ejb.WorkflowSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class AddCourrierController extends AbstractWindow implements
AfterCompose, EventListener, ListitemRenderer {

	private Listbox lstListe,lstTypeCourrier;
	private Paging pgPagination,pgTypeCourrier;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage,page;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private String courrierReference,typecourrierlibelle;
	private Date courrierDateReception, courrierDate;
	private SygModeReception modeReception;
	private SygAutoriteContractante ac;
	private SygTCourrierAC courrierAC;
	private Datebox dateCreation;
	private Textbox txtObservation;
	private SygDossierCourrier dossierCourrier;
	private Label lblrefr, lbltype, lblmodepassation, lbldatelancement;
	private SygTypesCourrier typeCourrier;
	private Textbox txtcourRef,txtRechercherTypeCourrier;
	private Datebox dbDateReception, dbDateCourrier;
	private Bandbox bandTypecourrier, bandTypeDossier;
	private SygTypesDossiers typeDossier;
	private SygPlansdepassation plansdepassation;
	private SygRealisations realisation;
	private org.zkoss.zul.Div step0, step1, step2;
	private Listheader lshcourrierReference,lshcourrierDateReception,lshcourrierDate;
	Session session = getHttpSession();
	private Datebox dtdebut,dtfin, dateStatut;
	private Label lblref,lblType,lblDaterecp, lblAutC, lblnumppm, lblann, lblDatecrea, lbldatemev;
	private Date datedebut,datefin;
	private KermelSousMenu monSousMenu;
	private Menuitem ADD_COURRIERAC, MOD_COURRIERAC, SUP_COURRIERAC, WSUIVI_COURRIERAC;
	public static final String LIST_TYPE_DOSSIER_MODEL_CHANGE="onTypeDModelChange";
	public static final String LIST_PLAN_PASSATION_MODEL_CHANGE="onPlanPModelChange";
	public static final String LIST_REALISATION_MODEL_CHANGE="onRealPModelChange";
	private final int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	String numero=null,prenom=null,reference,callBack,fonction=null,service=null;
	private String nameFile, statut;
	private Listbox listTypesdossiers, lstPassations;
	private Paging pgTypesdossiers, pgPassation;
	private String sygmlibelle, MODE;
	private SygAutoriteContractante autorite;
	private Listbox lstRealisations;
	private Paging pgRealisation;
	private Div step3;
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygTCourrierAC myCourrier = (SygTCourrierAC) data;
		item.setValue(myCourrier);

		Listcell cellReference = new Listcell(myCourrier.getCourrierac().getCourrierReference());
		cellReference.setParent(item);

		Listcell cellTypeCourrier = new Listcell("");
		if(myCourrier.getCourrierac().getCourrierType()!=null) {
			cellTypeCourrier.setLabel(myCourrier.getCourrierac().getCourrierType().getLibelletypecourrier());
		}
		cellTypeCourrier.setParent(item);

		Listcell cellDateCourrier = new Listcell(UtilVue.getInstance().formateLaDate(myCourrier.getCourrierac().getCourrierDate()));
		cellDateCourrier.setParent(item);

		Listcell cellDateReception = new Listcell(UtilVue.getInstance().formateLaDate(myCourrier.getCourrierac().getCourrierDateReception()));
		cellDateReception.setParent(item);

		Listcell cellAC = new Listcell("");
		if(myCourrier.getCourrierac().getCourrierAutoriteContractante()!=null) {
			cellAC.setLabel(myCourrier.getCourrierac().getCourrierAutoriteContractante().getDenomination()+"("+myCourrier.getCourrierac().getCourrierAutoriteContractante().getSigle()+")");
		}
		cellAC.setParent(item);

		Listcell cellModeReception = new Listcell("");
		if(myCourrier.getCourrierac().getCourrierModeReception()!=null) {
			cellModeReception.setLabel(myCourrier.getCourrierac().getCourrierModeReception().getLibellemodereception());
		}
		cellModeReception.setParent(item);

	}

	@Override
	public void onEvent(Event event) throws Exception {
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)){
			Utilisateur agent = (Utilisateur)getHttpSession().getAttribute("utilisateur");
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			List<SygTCourrierAC> courrier = BeanLocator.defaultLookup(TCourriersAcSession.class).findByCourrierAndAgent(pgPagination.getActivePage()*byPage,byPage,null, agent,courrierReference, courrierDateReception, courrierDate, typeCourrier);
			SimpleListModel listModel = new SimpleListModel(courrier);
			lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup(TCourriersAcSession.class).countByCourrierAndAgent(null, agent,courrierReference, courrierDateReception, courrierDate, typeCourrier));
		}else if(event.getName().equalsIgnoreCase(LIST_TYPE_DOSSIER_MODEL_CHANGE)){

			List<SygTypesDossiers>  dossiers = BeanLocator.defaultLookup(TypesDossiersSession.class).find(pgTypesdossiers.getActivePage()*byPage,byPage,null,sygmlibelle,null,null);
			SimpleListModel listModel = new SimpleListModel(dossiers);
			listTypesdossiers.setModel(listModel);
			pgTypesdossiers.setTotalSize(BeanLocator.defaultLookup(TypesDossiersSession.class).count(null));
		}else if(event.getName().equalsIgnoreCase(LIST_PLAN_PASSATION_MODEL_CHANGE)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			List<SygPlansdepassation> plans = BeanLocator.defaultLookup(PlansdepassationSession.class).find(activePage,byPage,numero,datedebut,datefin,autorite, -1, statut);
			SimpleListModel listModel = new SimpleListModel(plans);
			lstPassations.setModel(listModel);
			pgPassation.setTotalSize(BeanLocator.defaultLookup(PlansdepassationSession.class).count(numero,datedebut,datefin,autorite, -1,statut));
		}else if(event.getName().equalsIgnoreCase(LIST_REALISATION_MODEL_CHANGE)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			List<SygRealisations> realisations = BeanLocator.defaultLookup(RealisationsSession.class).find(activePage, byPage, null, null, null, plansdepassation, -1, statut, autorite, datedebut, datefin);
			SimpleListModel listModel = new SimpleListModel(realisations);
			lstRealisations.setModel(listModel);
			pgRealisation.setTotalSize(BeanLocator.defaultLookup( RealisationsSession.class).count(null, null, null, plansdepassation, -1, statut, autorite, datedebut, datefin));
		}

	}

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(LIST_TYPE_DOSSIER_MODEL_CHANGE, this);
		addEventListener(LIST_PLAN_PASSATION_MODEL_CHANGE, this);
		addEventListener(LIST_REALISATION_MODEL_CHANGE, this);

		lshcourrierReference.setSortAscending(new FieldComparator("courrierReference", false));
		lshcourrierReference.setSortDescending(new FieldComparator("courrierReference", true));

		lshcourrierDate.setSortAscending(new FieldComparator("courrierDate", false));
		lshcourrierDate.setSortDescending(new FieldComparator("courrierDate", true));

		lshcourrierDateReception.setSortAscending(new FieldComparator("courrierDateReception", false));
		lshcourrierDateReception.setSortDescending(new FieldComparator("courrierDateReception", true));

		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);

		listTypesdossiers.setItemRenderer(new TypeDossierRenderer());
		pgTypesdossiers.setPageSize(byPage);
		pgTypesdossiers.addForward("onPaging", this, LIST_TYPE_DOSSIER_MODEL_CHANGE);

		//	lstPassations.setItemRenderer(new PlanPassationRenderer());
		//	pgPassation.setPageSize(byPage);
		//	pgPassation.addForward("onPaging", this, LIST_PLAN_PASSATION_MODEL_CHANGE);
		//
		//	lstRealisations.setItemRenderer(new RealisationRenderer());
		//	pgRealisation.setPageSize(byPage);
		//	pgRealisation.addForward("onPaging", this, LIST_REALISATION_MODEL_CHANGE);

		dossierCourrier = (SygDossierCourrier) getHttpSession().getAttribute("DOSSIERCOURRIER");
		MODE = (String) Executions.getCurrent().getAttribute("MODE");
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		Events.postEvent(LIST_TYPE_DOSSIER_MODEL_CHANGE, this, null);


	}


	//Recherche sur TypeCourrier
	public void onSelect$lstTypeCourrier(){
		typeCourrier = (SygTypesCourrier) lstTypeCourrier.getSelectedItem().getValue();
		bandTypecourrier.setValue(typeCourrier.getLibelletypecourrier());		
		bandTypecourrier.close();
	}

	public class TypeCourrierRenderer implements ListitemRenderer {
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypesCourrier type = (SygTypesCourrier) data;
			item.setValue(type);

			Listcell cellLibelle = new Listcell(type.getLibelletypecourrier());
			cellLibelle.setParent(item);

		}
	}


	public void onFocus$txtRechercherTypeCourrier(){
		if(txtRechercherTypeCourrier.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.rechercher"))){
			txtRechercherTypeCourrier.setValue("");
		}		 
	}

	public void  onClick$btnRechercherTypeCourrier(){
		if(txtRechercherTypeCourrier.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.rechercher"))){
			typecourrierlibelle = null;
			//page = null;
		}
		else{
			typecourrierlibelle = txtRechercherTypeCourrier.getValue();
			//page="0";
		}

		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}



	//Ref
	public void onFocus$txtcourRef() {
		if (txtcourRef.getValue().equalsIgnoreCase(Labels.getLabel("kermel.courrierac.reference"))) {
			txtcourRef.setValue("");

		}
	}
	public void onBlur$txtcourRef() {
		if (txtcourRef.getValue().equalsIgnoreCase("")) {
			txtcourRef.setValue(Labels.getLabel("kermel.courrierac.reference"));
		}	
	}

	public void onOK() {
		onClick$bchercher();
	}

	public void onClick$bchercher() {

		if (txtcourRef.getValue().equalsIgnoreCase(Labels.getLabel("kermel.courrierac.reference")) || txtcourRef.getValue().equals("")) {
			courrierReference = null;
		} else {
			courrierReference  = txtcourRef.getValue();
		}	

		typecourrierlibelle = bandTypecourrier.getValue();
		courrierDateReception = dbDateReception.getValue();
		courrierDate = dbDateCourrier.getValue();

		if(typecourrierlibelle.equalsIgnoreCase(Labels.getLabel("kermel.courrierac.type")) || "".equals(typecourrierlibelle))
		{
			typeCourrier = null;
		}


		bandTypecourrier.setValue(Labels.getLabel("ecourrier.referentiel.common.types"));
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

	}



	private class TypeDossierRenderer implements ListitemRenderer{

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypesDossiers dossiers = (SygTypesDossiers) data;
			item.setValue(dossiers);

			Listcell cellcode = new Listcell(dossiers.getCode());
			cellcode.setParent(item);
			Listcell celllibelle = new Listcell(dossiers.getLibelle());
			celllibelle.setParent(item);
			if(dossierCourrier.getTypesDossiers()!=null&&dossierCourrier.getTypesDossiers().getCode().equalsIgnoreCase(dossiers.getCode())){
				item.setSelected(true);
				bandTypeDossier.setValue(dossiers.getLibelle());
				bandTypeDossier.setDisabled(true);
				typeDossier = dossiers;
			}

		}

	}

	private class PlanPassationRenderer implements ListitemRenderer{

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygPlansdepassation plans = (SygPlansdepassation) data;
			item.setValue(plans);
			item.setAttribute("status", plans.getStatus());


			Listcell cellNumero = new Listcell(plans.getNumplan());
			cellNumero.setParent(item);

			Listcell cellAnnee = new Listcell(Integer.toString(plans.getAnnee()));
			cellAnnee.setParent(item);

			Listcell cellDatecreation = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDatecreation()));
			cellDatecreation.setParent(item);

			Listcell cellDatemiseenvalidation = new Listcell("");
			if(plans.getDateMiseEnValidation()!=null)
			{
				cellDatemiseenvalidation.setLabel(UtilVue.getInstance().formateLaDate(plans.getDateMiseEnValidation()));
				item.setAttribute("datevalidation", plans.getDateMiseEnValidation());
			}
			else
			{
				item.setAttribute("datevalidation", null);
			}
			cellDatemiseenvalidation.setParent(item);

			Listcell cellStatut = new Listcell(plans.getStatus());
			if(plans.getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.saisie"))) {
				cellStatut.setLabel(Labels.getLabel("kermel.plansdepassation.statut.saisies"));
			} else  if(plans.getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.miseenvalidation"))) {
				cellStatut.setLabel(Labels.getLabel("kermel.plansdepassation.statut.miseenvalidations"));
			} else  if(plans.getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.valider"))) {
				cellStatut.setLabel(Labels.getLabel("kermel.plansdepassation.statut.validers")+" "+UtilVue.getInstance().formateLaDate(plans.getDateValidation()));
			} else  if(plans.getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.rejeter"))) {
				cellStatut.setLabel(Labels.getLabel("kermel.plansdepassation.statut.rejeters")+" "+UtilVue.getInstance().formateLaDate(plans.getDateRejet()));
			} else  if(plans.getStatus().equals(Labels.getLabel("kermel.plansdepassation.statut.publier"))) {
				cellStatut.setLabel(Labels.getLabel("kermel.plansdepassation.statut.publiers")+" "+UtilVue.getInstance().formateLaDate(plans.getDatepublication()));
			}
			cellStatut.setParent(item);

			Listcell cellCommentaires = new Listcell(plans.getCommentaires());
			cellCommentaires.setParent(item);

		}

	}

	public void onSelect$listTypesdossiers(){
		typeDossier = (SygTypesDossiers) listTypesdossiers.getSelectedItem().getValue();
		if(typeDossier!=null){
			bandTypeDossier.setValue(typeDossier.getLibelle());
		}
		bandTypeDossier.close();
	}

	public void onClick$menuNextStep0(){
		if(lstListe.getSelectedItem()==null){
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		}

		if(typeDossier!=null &&(typeDossier.getCode().equals(UIConstants.VAL_NPPM)||typeDossier.getCode().equals(UIConstants.VALP_MPPM))){
			statut = "VAL";
		}else{
			statut = "PUB";
		}
		courrierAC = (SygTCourrierAC) lstListe.getSelectedItem().getValue();
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
		setLabelCourrier();
		Events.postEvent(LIST_PLAN_PASSATION_MODEL_CHANGE, this, null);
	}

	public void onClick$menuNextStep1(){
		if(lstPassations.getSelectedItem()==null){
			throw new WrongValueException(lstPassations, Labels.getLabel("kermel.error.select.item"));
		}
		plansdepassation = (SygPlansdepassation) lstPassations.getSelectedItem().getValue();
		step0.setVisible(false);
		step1.setVisible(false);
		step2.setVisible(true);
		setLabelPpm();

		Events.postEvent(LIST_REALISATION_MODEL_CHANGE, this, null);
	}

	public void onClick$menuNextStep2(){
		if(lstRealisations.getSelectedItem()==null){
			throw new WrongValueException(lstRealisations, Labels.getLabel("kermel.error.select.item"));
		}
		realisation = (SygRealisations) lstRealisations.getSelectedItem().getValue();
		step0.setVisible(false);
		step1.setVisible(false);
		step2.setVisible(false);
		step3.setVisible(true);
		setLabelrealisation();

		//Events.postEvent(LIST_REALISATION_MODEL_CHANGE, this, null);
	}

	private class RealisationRenderer implements ListitemRenderer{

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygRealisations plans = (SygRealisations) data;
			item.setValue(plans);

			Listcell cellReference = new Listcell(plans.getReference());
			cellReference.setParent(item);

			//	    Listcell cellLibelle = new Listcell(plans.getLibelle());
			//	    cellLibelle.setParent(item);

			Listcell cellTypemarche = new Listcell(plans.getTypemarche().getLibelle());
			cellTypemarche.setParent(item);

			Listcell cellModepassation = new Listcell(plans.getModepassation().getLibelle());
			cellModepassation.setParent(item);

			Listcell cellDateLancement = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDatelancement()));
			cellDateLancement.setParent(item);

			//	    Listcell cellDateattribution = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDateattribution()));
			//	    cellDateattribution.setParent(item);
			//
			//	    Listcell cellDatedemarrage = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDatedemarrage()));
			//	    cellDatedemarrage.setParent(item);
			//
			//	    Listcell cellDateachevement = new Listcell(UtilVue.getInstance().formateLaDate(plans.getDateachevement()));
			//	    cellDateachevement.setParent(item);
			//
			//	    Listcell cellMontant = new Listcell(ToolKermel.format3Decimal(plans.getMontant()));
			//	    cellMontant.setParent(item);

		}

	}

	public void setLabelCourrier(){
		lblref.setValue(courrierAC.getCourrierac().getCourrierReference());
		lblDaterecp.setValue(UtilVue.getInstance().formateLaDate(courrierAC.getCourrierac().getCourrierDateReception()));
		lblType.setValue(courrierAC.getCourrierac().getCourrierType().getLibelletypecourrier());
		lblAutC.setValue(courrierAC.getCourrierac().getCourrierAutoriteContractante().getDenomination());
	}

	public void setLabelPpm(){
		lblnumppm.setValue(plansdepassation.getNumplan());
		lblann.setValue(String.valueOf(plansdepassation.getAnnee()));
		lbldatemev.setValue(UtilVue.getInstance().formateLaDate(plansdepassation.getDateMiseEnValidation()));
		lblDatecrea.setValue(UtilVue.getInstance().formateLaDate(plansdepassation.getDatecreation()));
	}

	public void setLabelrealisation(){
		lblnumppm.setValue(plansdepassation.getNumplan());
		lblann.setValue(String.valueOf(plansdepassation.getAnnee()));
		lbldatemev.setValue(UtilVue.getInstance().formateLaDate(plansdepassation.getDateMiseEnValidation()));
		lblDatecrea.setValue(UtilVue.getInstance().formateLaDate(plansdepassation.getDatecreation()));
	}
	public void onClick$menuPreviousStep2(){
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
	}
	public void onClick$menuPreviousStep1(){
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
	}

	public void onClick$menuValiderStep3(){
		if(MODE!=null && MODE.equals(UIConstants.MODE_NEW)){
			Utilisateur utilisateur = (Utilisateur) getHttpSession().getAttribute("utilisateur");
			try {
				dossierCourrier = new SygDossierCourrier();
				dossierCourrier.setTypesDossiers(typeDossier);
				dossierCourrier.setDescription(txtObservation.getValue());
				dossierCourrier.setDateStatus(dateStatut.getValue());
				dossierCourrier.setPlansdepassation(plansdepassation);
				dossierCourrier.setRealisation(realisation);
				dossierCourrier.setDateStatus(new Date());
				dossierCourrier.setState(BeanLocator.defaultLookup(WorkflowSession.class).findState("SAISIDOSSIER_STATE"));
				dossierCourrier = BeanLocator.defaultLookup(DossierAcSession.class).save(dossierCourrier, null);

				SygTDossierCourrier tDossierCourrier = new SygTDossierCourrier();
				tDossierCourrier.setCourrier(dossierCourrier);
				tDossierCourrier.setCode(dossierCourrier.getCode());
				tDossierCourrier.setExpediteur(utilisateur);
				tDossierCourrier.setDestinataire(utilisateur.getId());
				tDossierCourrier.setDateStatus(new Date());
				tDossierCourrier.setInstruction("Ce dossier doit �tre trait� dans les "+typeDossier.getDelaitraitement()+" jrs � partir de la date de r�ception!!!");
				tDossierCourrier.setStatus(tDossierCourrier.getStatus());
				BeanLocator.defaultLookup(DossierAcSession.class).saveTdossier(tDossierCourrier);
				SygCourrierAc courrier = courrierAC.getCourrierac();
				courrier.setDossierCourrier(dossierCourrier);
				BeanLocator.defaultLookup(CourriersAcSession.class).update(courrier);
				initPiecesRequises();
				initGrilleAnalyse();
				initModelDoc();
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
				detach();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}else{
			try {
				dossierCourrier.setTypesDossiers(typeDossier);
				dossierCourrier.setDescription(txtObservation.getValue());
				dossierCourrier.setDateStatus(dateStatut.getValue());
				dossierCourrier.setPlansdepassation(plansdepassation);
				dossierCourrier.setRealisation(realisation);
				BeanLocator.defaultLookup(DossierAcSession.class).update(dossierCourrier);
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
				detach();
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
	}

	private void initPiecesRequises(){
		List<SygPiecesrequises> piecesrequises = BeanLocator.defaultLookup(PiecesRequisesSession.class).find(0, -1, null, dossierCourrier.getTypesDossiers());
		for (SygPiecesrequises sygPiecesrequises : piecesrequises) {
			SygDossierPieceRequises dossierPieceRequises = new SygDossierPieceRequises();
			dossierPieceRequises.setDossierCourrier(dossierCourrier);
			dossierPieceRequises.setPiecesrequises(sygPiecesrequises);
			dossierPieceRequises.setStatut(0);
			BeanLocator.defaultLookup(DossierPiecesRequises.class).save(dossierPieceRequises);
		}
	}

	private void initGrilleAnalyse(){
		List<SygGrillesAnalyses> grillesAnalyses = BeanLocator.defaultLookup(GrillesAnalysesSession.class).find(0, -1, null, dossierCourrier.getTypesDossiers());
		for (SygGrillesAnalyses sygGrillesAnalyses : grillesAnalyses) {
			SygDossierGrilleAnalyse dossierGrilleAnalyse = new SygDossierGrilleAnalyse();
			dossierGrilleAnalyse.setGrillesAnalyse(sygGrillesAnalyses);
			dossierGrilleAnalyse.setDossierCourrier(dossierCourrier);
			dossierGrilleAnalyse.setStatut(0);
			BeanLocator.defaultLookup(DossierGrilleAnalyseSession.class).save(dossierGrilleAnalyse);

		}
	}
	private void initModelDoc(){
		List<SygTypeModeleDocument> modeleDocuments = BeanLocator.defaultLookup(TypeModelesDocumentsSession.class).find(0, -1, null, dossierCourrier.getTypesDossiers());
		for (SygTypeModeleDocument sygTypeModeleDocument : modeleDocuments) {
			SygDossierModeleDoc dossierModeleDoc = new SygDossierModeleDoc();
			dossierModeleDoc.setDossierCourrier(dossierCourrier);
			dossierModeleDoc.setModeleDocument(sygTypeModeleDocument);
			dossierModeleDoc.setStatut(0);
			BeanLocator.defaultLookup(DossierModeleDocSession.class).save(dossierModeleDoc);
		}

	}
	public void onClick$menuValiderStep0(){
		if(lstListe.getSelectedItem()==null){
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		}

		SygCourrierAc courrier = ((SygTCourrierAC) lstListe.getSelectedItem().getValue()).getCourrierac();;
		courrier.setDossierCourrier(dossierCourrier);
		BeanLocator.defaultLookup(CourriersAcSession.class).update(courrier);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();
	}
}
