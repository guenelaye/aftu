package sn.ssi.kermel.web.traitementdossier.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygModeTraitement;
import sn.ssi.kermel.be.traitementdossier.ejb.ModeTraitementSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;



@SuppressWarnings("serial")
public class ListModeTraitementController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox list;
	private Paging pgtraitement;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private Listheader libellemodetraitement;
	private String sygmlibelle;
	private Textbox txtcodemodetraitement,txtlibellemodetraitement,txtdescriptionmodetraitement;
	Long code;
	private Menuitem ADD_TRAITEMENT, MOD_TRAITEMENT, SUPP_TRAITEMENT;
	 private KermelSousMenu monSousMenu;
	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.MODETRAITEMENT);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		Components.wireFellows(this, this);
		if (ADD_TRAITEMENT != null)
			ADD_TRAITEMENT.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
		if (MOD_TRAITEMENT != null)
			MOD_TRAITEMENT.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
		if (SUPP_TRAITEMENT != null)
			SUPP_TRAITEMENT.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
 
		list.setItemRenderer(this);

		pgtraitement.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pgtraitement.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 List<SygModeTraitement>  traitement = BeanLocator.defaultLookup(ModeTraitementSession.class).find(pgtraitement.getActivePage()*byPage,byPage,null,null,sygmlibelle);
			 SimpleListModel listModel = new SimpleListModel(traitement);
			list.setModel(listModel);
			pgtraitement.setTotalSize(BeanLocator.defaultLookup(ModeTraitementSession.class).count(null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/traitementdossier/ formmodetraitement.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.modetraitement"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ModeTraitementFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/traitementdossier/ formmodetraitement.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ModeTraitementFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(ModeTraitementFormController.PARAM_WIDOW_CODE, list
					.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				// de
				// fixer
				// les
				// dimenisons
				// du
				// popup
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// de
				// passer
				// des
				// parametres
				// au
				// popup
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < list.getSelectedCount(); i++) {
					Long codes = (Long)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					System.out.println(codes);
				BeanLocator.defaultLookup(ModeTraitementSession.class).delete(codes);
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygModeTraitement rapport = (SygModeTraitement) data;
		item.setValue(rapport.getIdmodetraitement());
		
		Listcell cellcodemodetraitement = new Listcell(rapport.getCodemodetraitement());
		cellcodemodetraitement.setParent(item);
		Listcell celllibellemodetraitement = new Listcell(rapport.getLibellemodetraitement());
		celllibellemodetraitement.setParent(item);
		Listcell celldescriptionmodetraitement = new Listcell(rapport.getDescriptionmodetraitement());
		celldescriptionmodetraitement.setParent(item);
	}
	
	public void onOK$libellemodetraitement() {
		onClick$btnRechercher();
	}
  public void onClick$btnRechercher() {
			
			if (txtlibellemodetraitement.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.libellemodetraitement")) || txtlibellemodetraitement.getValue().equals("")) {
				sygmlibelle = null;
			} else {
				sygmlibelle  = txtlibellemodetraitement.getValue();
			}	

			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
	
  public void onFocus$txtlibellemodetraitement() {
		if (txtlibellemodetraitement.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.libellemodetraitement"))) {
			txtlibellemodetraitement.setValue("");
			

		}
	}
			public void onBlur$txtlibellemodetraitement() {
				if (txtlibellemodetraitement.getValue().equalsIgnoreCase("")) {
					txtlibellemodetraitement.setValue(Labels.getLabel("kermel.common.form.libellemodetraitement"));
				}	
		}

}