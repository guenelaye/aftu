package sn.ssi.kermel.web.traitementdossier.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tabpanel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygTypesDossiers;
import sn.ssi.kermel.be.traitementdossier.ejb.TypesDossiersSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



public class DossiersTypesDossiersFormController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private Long code;
	UtilVue utilVue = UtilVue.getInstance();
	private Label lblcode,lbllibelle,lbldelaitraitement,lbldescription;
	Session session = getHttpSession();
	private Tabpanel tabComposition;
	private Include incComposition;
	private SygTypesDossiers dossiers =new SygTypesDossiers();

	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		tabComposition.addEventListener(Events.ON_CLICK, this);
		code = (Long) session.getAttribute("numconfiguration");
		dossiers = BeanLocator.defaultLookup(TypesDossiersSession.class) .findById(code);
		lblcode.setValue(dossiers.getCode());
		lbllibelle.setValue(dossiers.getLibelle());
		lbldelaitraitement.setValue(dossiers.getDelaitraitement().toString());
		lbldescription.setValue(dossiers.getDescription());
     

	}

	
	public void onCreate(CreateEvent createEvent) {
		
	
		
		 Include inc = (Include) this.getFellowIfAny("incComposition");
         inc.setSrc("/traitementdossier/listpiecerequises.zul");
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	public void onClick$menuFermer() {
		loadApplicationState("TypesDossiers");
	}
	
}