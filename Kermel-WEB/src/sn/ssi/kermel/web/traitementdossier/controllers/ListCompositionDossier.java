package sn.ssi.kermel.web.traitementdossier.controllers;

import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Image;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygDossierPieceRequises;
import sn.ssi.kermel.be.traitementdossier.ejb.DossierPiecesRequises;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class ListCompositionDossier extends AbstractWindow implements AfterCompose , EventListener,ListitemRenderer {

    private Listbox lstListe;
    private Paging pgPagination;
    private SygDossierCourrier dossierCourrier;

    @Override
    public void render(Listitem item, Object data, int index)  throws Exception {
	SygDossierPieceRequises dossierPieceRequises = (SygDossierPieceRequises) data;
	item.setValue(dossierPieceRequises);

	Listcell pieces = new Listcell(dossierPieceRequises.getPiecesrequises().getPiece().getLibelle());
	pieces.setParent(item);

	Image image = new Image();
	Listcell statut = new Listcell();
	if(dossierPieceRequises.getStatut()==0){
	    image.setSrc("/images/cancel.png");
	}else{
	    image.setSrc("/images/ok.png");
	}
	image.addEventListener(Events.ON_DOUBLE_CLICK, this);
	image.setAttribute("PIECE", dossierPieceRequises);
	image.setParent(statut);
	statut.setParent(item);

    }

    @Override
    public void onEvent(Event event) throws Exception {

	if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
	    List<SygDossierPieceRequises> piecesrequise = BeanLocator.defaultLookup(DossierPiecesRequises.class).findByDossier(0, -1, dossierCourrier);
	    SimpleListModel listModel = new SimpleListModel(piecesrequise);
	    lstListe.setModel(listModel);
	    //  pgPagination.setTotalSize(BeanLocator.defaultLookup(DossierAcSession.class).countByType(type));
	} 
	else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DOUBLE_CLICK)) {
	    Image image = (Image) event.getTarget();
	    SygDossierPieceRequises pieces = (SygDossierPieceRequises) image.getAttribute("PIECE");
	    if(pieces!=null){
		if(pieces.getStatut()==1) {
		    pieces.setStatut(0);
		} else {
		    pieces.setStatut(1);
		}
		BeanLocator.defaultLookup(DossierPiecesRequises.class).update(pieces);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	    }
	} 

    }

    @Override
    public void afterCompose() {
	Components.wireFellows(this, this);
	Components.addForwards(this, this);

	addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
	addEventListener(Events.ON_DOUBLE_CLICK, this);
	addEventListener(ApplicationEvents.ON_ADD, this);
	addEventListener(ApplicationEvents.ON_EDIT, this);
	addEventListener(ApplicationEvents.ON_DETAILS, this);
	addEventListener(ApplicationEvents.ON_DELETE, this);

	lstListe.setItemRenderer(this);
	dossierCourrier = (SygDossierCourrier) getHttpSession().getAttribute("DOSSIERCOURRIER");

	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

    }

}
