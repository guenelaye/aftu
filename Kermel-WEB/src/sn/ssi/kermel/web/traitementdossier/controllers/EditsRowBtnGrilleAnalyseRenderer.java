package sn.ssi.kermel.web.traitementdossier.controllers;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygDossierGrilleAnalyse;
import sn.ssi.kermel.be.traitementdossier.ejb.DossierGrilleAnalyseSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
public class EditsRowBtnGrilleAnalyseRenderer extends AbstractWindow implements
AfterCompose, RowRenderer, RowRendererExt {

    Integer fichecollecte;
    Session session = getHttpSession();


    @Override
    public void afterCompose() {
	// TODO Auto-generated method stub
	// fichecollecte = (Sim_FichesCollectes) session.getAttribute("fiche");
	// marche = BeanLocator.defaultLookup(MarchesSession.class).findByCode(
	// fichecollecte.getId());
	// System.out.println(marche.getLibelle() + "ok");
	// localite = BeanLocator.defaultLookup(LocalitesSession.class)
	// .findByCode(marche.getId());
	// System.out.print(localite.getLibelle() + "otre");
    }

    @Override
    public Row newRow(Grid grid) {
	// Create EditableRow instead of Row(default)
	Row row = new EditableRow();
	row.applyProperties();
	return row;
    }

    @Override
    public Component newCell(Row row) {
	return null;// Default Cell
    }



    @Override
    public int getControls() {
	return RowRendererExt.DETACH_ON_RENDER; // Default Value
    }

    @Override
    public void render(Row row, Object data, int index) throws Exception {
	//		final SiapzoneaRisque zoneaRisque = (SiapzoneaRisque) data;
	final SygDossierGrilleAnalyse grilleana = (SygDossierGrilleAnalyse) data;

	final EditableRow editRow = (EditableRow) row;

	final EditableDiv critere = new EditableDiv(grilleana.getGrillesAnalyse().getAnalyse().getLibellecritereanalyse(), false);
	critere.txb.setReadonly(true);
	critere.setAlign("left");
//	critere.setHeight("40px");
	critere.setParent(editRow);

	String etat="";
	if(grilleana.getStatut()==0) {
	    etat = "NON";
	} else if(grilleana.getStatut()==1) {
	    etat = "OUI";
	}else {
	    etat = "NA";
	}
	final EditableRadioDiv etatfiche =	new EditableRadioDiv(etat,false);
	etatfiche.setAlign("center");
	etatfiche.setStyle("font-weight:bold;color:green");
	if(etat.equalsIgnoreCase("OUI")){
	    etatfiche.radio1.setSelected(true);  
	}else if(etat.equalsIgnoreCase("NON")){
	    etatfiche.radio2.setSelected(true);
	}else {
	    etatfiche.radio3.setSelected(true);
	}
	etatfiche.setParent(editRow);	



	final EditableDiv   commentaire = new EditableDiv(grilleana.getCommentaire(), false);
	commentaire.setAlign("left");
//	commentaire.setHeight("40px");
	commentaire.setParent(editRow);


	final Div ctrlDiv = new Div();
	ctrlDiv.setParent(editRow);
	final Button editBtn = new Button(null, "/images/pencil-small.png");
	editBtn.setMold("os");
//	editBtn.setHeight("20px");
	editBtn.setWidth("30px");
	editBtn.setParent(ctrlDiv);
	// Button listener - control the editable of row
	editBtn.addEventListener(Events.ON_CLICK, new EventListener() {
	    @Override
	    public void onEvent(Event event) throws Exception {
		final Button submitBtn = (Button) new Button(null, "/images/tick-small.png");
		final Button cancelBtn = (Button) new Button(null, "/images/cross-small.png");

		submitBtn.setMold("os");
//		submitBtn.setHeight("20px");
		submitBtn.setWidth("30px");
		submitBtn.setTooltiptext("Valider la saisie");
		submitBtn.addEventListener(Events.ON_CLICK, new EventListener() {
		    @Override
		    public void onEvent(Event event) throws Exception {

			editRow.toggleEditable(true);

			if(etatfiche.radio1.isChecked()){
			    grilleana.setStatut(1);
			}else if(etatfiche.radio2.isChecked()){
			    grilleana.setStatut(0);
			}else if(etatfiche.radio3.isChecked()){
			    grilleana.setStatut(-1);
			}
			grilleana.setCommentaire(commentaire.txb.getValue());
			BeanLocator.defaultLookup(DossierGrilleAnalyseSession.class).update(grilleana);



			submitBtn.detach();
			cancelBtn.detach();
			editBtn.setParent(ctrlDiv);
		    }
		});

		cancelBtn.setMold("os");
//		cancelBtn.setHeight("20px");
		cancelBtn.setWidth("30px");
		cancelBtn.setTooltiptext("Annuler la saisie");
		cancelBtn.addEventListener(Events.ON_CLICK, new EventListener() {
		    @Override
		    public void onEvent(Event event) throws Exception {
			editRow.toggleEditable(false);
			submitBtn.detach();
			cancelBtn.detach();
			editBtn.setParent(ctrlDiv);
		    }
		});
		submitBtn.setParent(ctrlDiv);
		cancelBtn.setParent(ctrlDiv);
		editRow.toggleEditable(true);
		editBtn.detach();
	    }
	});

    }



}
