package sn.ssi.kermel.web.suivimarches.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygCalendrierPaiement;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDemandePaiement;
import sn.ssi.kermel.be.entity.SygPenaliteRetard;
import sn.ssi.kermel.be.referentiel.ejb.ContratSession;
import sn.ssi.kermel.be.session.CalendrierPaiementSession;
import sn.ssi.kermel.be.session.DemandePaiementSession;
import sn.ssi.kermel.be.session.PenaliteRetardSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

/**
 * 
 * @author Adama Samb
 * 
 */
@SuppressWarnings("serial")
public class ListCalendrierPaiementController extends AbstractWindow implements EventListener, AfterCompose, ListitemRenderer {
	

	private Paging pgLignes,pgProduit;
	private static final String ERROR_MSG_STYLE = "color:red";
	private static final String READY_MSG_STYLE = "";
	private Component errorComponent;
	private String errorMsg;
	private final int byPage = UIConstants.DSP_GRID_ROWS_DOSSIERREC_BY_PAGE;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private Long numDos;
	Session session = getHttpSession();
	private Listbox  listeLignes;
	private Label lblStatus;
	private Textbox txtProduit;
	private Decimalbox decvaleur;
	private Doublebox dbquantite,dbpoids;
	private Listbox lstProduit;
	private Bandbox  bdProduit;
	private int activePage;
	private String page="0",codeproduit=null;
	public static final String CURRENT = "CURRENT";
	public static final String TODO = "TODO";
	private Long idproduit=null;
	private Textbox txtCodeProduit;
	private Textbox txtPrecUemoa;
	private Textbox txtPrecSen;
	private Label   sommePaye;
	
	private Bandbox bdUnit;
	private Listbox lstUnit;
	private Textbox txtUnit;
	private Paging pgUnit;
	private String codeunit=null;
	private String pageunit="0";
    private Datebox   datepaiement;
   private SygCalendrierPaiement lignepaiement;
   private Textbox txtLivrable,commentaire;
   private Intbox   taux;
   private Doublebox  montant;
   private Label  montantVal,datedebut,datefin;
   private SygContrats  contrat;
   private double montantCalendrier=0;
   private double somme=0;
   
	
	@Override
	public void afterCompose() {
		// TODO Auto-generated method stub
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	   // somme=0;
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_UNIT, this);
		addEventListener(ApplicationEvents.ON_SEARCH_UNIT_BANDBOX, this);
		pgLignes.setPageSize(byPage);
		pgLignes.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		listeLignes.setItemRenderer(this);
		numDos = (Long) getHttpSession().getAttribute(UIConstants.SESSION_NUMDOS);
		//demande = BeanLocator.defaultLookup(DemandeSession.class).findByCode(numDos);
		
		 Long idcontrat = (Long) session.getAttribute("contrat");
		   contrat=BeanLocator.defaultLookup(ContratSession.class).findById(idcontrat);
	//	  montantVal.setValue(contrat.getMontant().toString()); 
		 // datedebut.setValue(contrat.get);
		Events.postEvent(ApplicationEvents.ON_SEARCH_UNIT_BANDBOX,this,null);
		Events.postEvent(ApplicationEvents.ON_PRODUITS, this, null);
		
	
	}
	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent e) {

		numDos = (Long) getHttpSession().getAttribute(UIConstants.SESSION_NUMDOS);
		//demande = BeanLocator.defaultLookup(DemandeSession.class).findByCode(numDos);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}


	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(Event event) throws Exception {
		if(event.getName().equals(ApplicationEvents.ON_MODEL_CHANGE)){
			List<SygCalendrierPaiement> produits = BeanLocator.defaultLookup(CalendrierPaiementSession.class).find(pgLignes.getActivePage()*byPage, byPage,contrat.getConID(), null);
			for(SygCalendrierPaiement cpaye:produits){
			
		if ((cpaye.getPayer()!=null)&&(cpaye.getPayer().equalsIgnoreCase("oui")))
			somme+=cpaye.getMontantpaye();	
			}
			sommePaye.setValue("Somme d�j� pay�e :"+ToolKermel.formatDecimal(somme));
			
			listeLignes.setModel(new SimpleListModel(produits));
    		pgLignes.setTotalSize(BeanLocator.defaultLookup(CalendrierPaiementSession.class).count(null,null));
			
    		for (SygCalendrierPaiement calen: produits ){
    			montantCalendrier+=calen.getMontantpaye();	
    		}
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_PRODUITS)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = 5;
				pgProduit.setPageSize(5);
			} else {
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgProduit.getActivePage() * byPageBandbox;
				pgProduit.setPageSize(byPageBandbox);
			}
			/*
			List<GredTexteDemande> demande = BeanLocator.defaultLookup(TexteDemandeSession.class).findBYDemande(0, -1, numDos);
			List<GredProduit> produits =new ArrayList<GredProduit>();
			int page=0;
			for(GredTexteDemande txt:demande)
			{
				GredTextesTypedmd txttpdmd=txt.getTexte();
				List<GredProduitTexte> produittxt=BeanLocator.defaultLookup(ProduitTexteSession.class).search(pgProduit.getActivePage() * byPageBandbox, byPageBandbox, txttpdmd.getTexte().getTex_id(),codeproduit);
				
				for(GredProduitTexte prodtxt:produittxt)
				{
					produits.add(prodtxt.getProduit());
				}
				page +=BeanLocator.defaultLookup(ProduitTexteSession.class).countproduittexte(txttpdmd.getTexte().getTex_id(),codeproduit);
			}
			  */
		//	lstProduit.setModel(new SimpleListModel(produits));
		//	pgProduit.setTotalSize(page);

		}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_UNIT)){
			
				
			
		}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_SEARCH_UNIT_BANDBOX)){
			
				
	    }else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) {

			Listcell cell = (Listcell) event.getTarget();
			// String toDo = (String) cell.getAttribute(UIConstants.TODO);
			HashMap<String, String> display = new HashMap<String, String>();
			display.put(MessageBoxController.DSP_MESSAGE, Labels
					.getLabel("gred.common.form.question.supprimer")
					+ " ?");
			display
					.put(
							MessageBoxController.DSP_TITLE,Labels.getLabel("gred.common.form.supprimer"));
			display.put(MessageBoxController.DSP_HEIGHT, "150px");
			display.put(MessageBoxController.DSP_WIDTH, "100px");
			HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(CURRENT, (SygCalendrierPaiement) cell .getAttribute(UIConstants.ATTRIBUTE_CODE));
			data.put(TODO, (String) cell.getAttribute(UIConstants.TODO));
			showMessageBox(display, data);

		} else if (event.getName().equals(
				ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {

			SygCalendrierPaiement produit = (SygCalendrierPaiement) ((HashMap<String, Object>) event.getData())
					.get(CURRENT);
			Long code=produit.getCalenpaId();
			
			String toDo = (String) ((HashMap<String, Object>) event.getData())
					.get(TODO);
			
				if (toDo != null && toDo.equalsIgnoreCase("delLigneDevis")) {
                    //traitement des demandes      
			      
			SygDemandePaiement demande= BeanLocator.defaultLookup(DemandePaiementSession.class).findDemande(code);
			
			//suppression de penalite
			SygPenaliteRetard penalite = BeanLocator.defaultLookup(PenaliteRetardSession.class).findPenalite(demande.getDemPaId()); 
			if(penalite !=null)
			BeanLocator.defaultLookup(PenaliteRetardSession.class).delete(penalite.getPenId());
			   // fin de traitement
			BeanLocator.defaultLookup(DemandePaiementSession.class).delete(demande.getDemPaId());
					// fin traitement
					BeanLocator.defaultLookup(CalendrierPaiementSession.class).delete(code);
					
					 idproduit=null;
					Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE,
							this, null);

				}
			
		}
	}

	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygCalendrierPaiement produits = (SygCalendrierPaiement) data;
		item.setValue(produits);
//		String codeprod = produit.getProduitPK().getCprod()+" "+produit.getProduitPK().getPrecsen()+" "+produit.getProduitPK().getPrecuemoa();
		Listcell cellProduit = new Listcell(ToolKermel.dateToString(produits.getDatepaiement()));					
		cellProduit.setParent(item);
		
		Listcell cellPrecum = new Listcell(produits.getLivrable());					
		cellPrecum.setParent(item);
		
		Listcell cellPrecsen = new Listcell(ToolKermel.format1Decimal(produits.getTaux())+"%");					
		cellPrecsen.setParent(item);
		
		Listcell cellesp = new Listcell(ToolKermel.formatDecimal(produits.getMontantpaye()));					
		cellesp.setParent(item);
		
		Listcell cellValeur = new Listcell(produits.getCommentaire());
		cellValeur.setParent(item);
		
		Listcell cellEcheance = new Listcell("", "/images/cancel.png");
		 SimpleDateFormat inputDf = new SimpleDateFormat("yyyy-MM-dd");
		 Date maDateAvecFormat=new Date();
		 String datejour=inputDf.format(maDateAvecFormat);
		 
		if((produits.getDatepaiement().getTime()- new Date().getTime())>=0)
		 cellEcheance = new Listcell("", "/images/ok.png");
			  
		//System.out.println(ToolKermel.DifferenceDate(produits.getDatepaiement(),new Date(),"jj")+"######################"+produits.getDatepaiement()+"########"+inputDf.parse(datejour));
		cellEcheance.setParent(item);
		
		Listcell cellPayer = new Listcell("", "/images/cancel.png");
		if ((produits.getPayer()!=null)&&(produits.getPayer().equalsIgnoreCase("oui"))){
		     cellPayer = new Listcell("", "/images/ok.png");
		    // somme+=produits.getMontantpaye();
		}
		cellPayer.setParent(item);
		
		Listcell cellDemande = new Listcell("", "/images/cancel.png");
		if ((produits.getPayer()!=null))
			cellDemande = new Listcell("", "/images/ok.png");
		cellDemande.setParent(item);
		
		Listcell cellSupLigneDevis = new Listcell("", "/images/delete.png");
		cellSupLigneDevis.addEventListener(Events.ON_CLICK, this);
		cellSupLigneDevis.setAttribute(UIConstants.TODO, "delLigneDevis");
		cellSupLigneDevis.setAttribute(UIConstants.ATTRIBUTE_CODE, produits);
		cellSupLigneDevis.setParent(item);
	
	}
	 public void onSelect$listeLignes(){
		   lignepaiement = (SygCalendrierPaiement) listeLignes.getSelectedItem().getValue();
		
		   if (!((lignepaiement.getPayer()!=null)&&(lignepaiement.getPayer().equalsIgnoreCase("oui")))){
		   
		     idproduit=lignepaiement.getCalenpaId();
		     datepaiement.setValue(lignepaiement.getDatepaiement());
		     txtLivrable.setValue(lignepaiement.getLivrable());
		     taux.setValue(lignepaiement.getTaux());
		     montant.setValue(lignepaiement.getMontantpaye());
		     commentaire.setValue(lignepaiement.getCommentaire());
//		     idproduit=ligneproduit.getProid();
////		     txtCodeProduit.setValue(produit.getCatprd());
//		     String codeprod="";
//			 codeprod = produit.getProduitPK().getCprod()+"/"+produit.getProduitPK().getPrecsen()+"/"+produit.getProduitPK().getPrecuemoa()+"  "+produit.getLibTar();
//
//		     bdProduit.setValue(produit.getLibTar());
//			 decvaleur.setValue(ligneproduit.getProvaleurht());
//			 dbquantite.setValue(ligneproduit.getProquantite());
//			 dbpoids.setValue(ligneproduit.getPropoids());
//			 bdUnit.setValue(ligneproduit.getProdunite().getLibelleunite());
		   
		   }else{
			   throw new WrongValueException(listeLignes,"Veuillez selectionner une demande non payer");
				
		   }
		 
	 }
	 public void onClick$cellValider(){
		 
		 
		 if(isSaisieLigneDevisCompleted())
		 {
			 if(!isSaisieLigneDevisCompleted()){
				 lblStatus.setStyle(ERROR_MSG_STYLE);
 				 lblStatus.setValue("erreur");
		  	 throw new WrongValueException(errorComponent, errorMsg);
			 
			 }
//			 lblStatus.setStyle(READY_MSG_STYLE);
	//		 lblStatus.setValue(Labels.getLabel("atlantis.common.form.autre.pret"));
			 if(idproduit==null)
			 {
				 lignepaiement = new SygCalendrierPaiement();
			 }
			// lignepaiement.setProduit(produit);
			 lignepaiement.setContrat(contrat);
			 lignepaiement.setDatepaiement(datepaiement.getValue());
			 lignepaiement.setLivrable(txtLivrable.getValue());
			 lignepaiement.setTaux(taux.getValue());
			 if(montant.getValue()!=null)
			 lignepaiement.setMontantpaye(montant.getValue());
			 lignepaiement.setCommentaire(commentaire.getValue());
			// GredUnite unit=(GredUnite)lstUnit.getSelectedItem().getValue();
			 //ligneproduit.setProdunite(unit);
			 if(idproduit==null)
			 {
				 BeanLocator.defaultLookup(CalendrierPaiementSession.class).save(lignepaiement);
			 }
			 else
			 {
				 BeanLocator.defaultLookup(CalendrierPaiementSession.class).update(lignepaiement);
			 }
			 idproduit=null;
			 reInitProduitForm();
			 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		 }
	 }

	 private boolean isSaisieLigneDevisCompleted() {
			try {
				
				/*if (produit==null) {
					errorMsg = Labels.getLabel("gred.produit.produit");
					errorComponent = bdProduit;
					return false;
				}
				  */
				
				if (taux.getValue()==null) {
					errorMsg = Labels.getLabel("gred.produit.poids");
					errorComponent = taux;
					return false;
				}
				if (taux.getValue()>=100) {
					errorMsg = Labels.getLabel("gred.produit.poids");
					errorComponent = taux;
					return false;
				}
				
				
				if (datepaiement.getValue()==null) {
					errorMsg = Labels.getLabel("gred.produit.valeur");
					errorComponent = datepaiement;
					return false;
				}
				if (txtLivrable.getValue()==null) {
					errorMsg = Labels.getLabel("gred.produit.quantite");
					errorComponent = txtLivrable;
					return false;
				}
				
				
				return true;
			}
			catch (Exception e) {
			//	lblStatus.setValue(e.toString());
				return false;
			}
		}


	 
	 public void onBlur$taux(){
//		 if(txtProduit.getValue().equalsIgnoreCase(Labels.getLabel(""))){
//			 txtProduit.setValue("");
//		 }	
		 if (taux.getValue()!=null ){ 
		
		 if (taux.getValue()>100){
				 throw new WrongValueException(taux, " le taux doit �tre entre  0 et 100");
		 }else{
			
	     		 
		 montant.setValue(((taux.getValue()* contrat.getMontant().doubleValue())/100));
		 //traitement
		 
			List<SygCalendrierPaiement> produits = BeanLocator.defaultLookup(CalendrierPaiementSession.class).find(pgLignes.getActivePage()*byPage, byPage,contrat.getConID(), null);
			montantCalendrier=0;
    		for (SygCalendrierPaiement calen: produits ){
    			montantCalendrier+=calen.getMontantpaye();	
    		}
		 // fin traitement
		 if ((montantCalendrier+montant.doubleValue()) > contrat.getMontant().doubleValue()){
			 
			 throw new WrongValueException(taux, " le taux est  trop grand  veuillez reduire la somme ");
			 
		 }
		 
		 }
				 
		 }
	 }
	 public void onFocus$bdProduit()
	 {		
//		if (lstProduit.getSelectedCount()!=0)
//		{	lstProduit.setSelectedIndex(0);
//			GredProduit prod =(GredProduit)lstProduit.getSelectedItem().getValue(); 
//			bdProduit.setValue(prod.getLibTar());
//			txtCodeProduit.setValue(prod.getProduitPK().getCprod());
//			txtPrecSen.setValue(prod.getProduitPK().getPrecsen());
//			txtPrecUemoa.setValue(prod.getProduitPK().getPrecuemoa());
//			bdProduit.open();
//		}
	 }
	 public void  onClick$btnRechercherProduit(){
		 if(txtProduit.getValue().equals("")){
			 codeproduit = null;
		 }else{
			 codeproduit = txtProduit.getValue();
			 page="0";
		 }
			 Events.postEvent(ApplicationEvents.ON_PRODUITS, this, page);
	 }
	 
	 
	 private void reInitProduitForm() {
//		 txtCodeProduit.setValue("");
		 datepaiement.setValue(null);
		 txtLivrable.setValue("");
		 taux.setValue(null);
		 montant.setValue(null);
		 commentaire.setValue("");
		
	 }
	 
	 
	 public void onSelect$lstUnit()
	 {
//		 unit =(GredUnit) lstUnit.getSelectedItem().getAttribute(name)
		 bdUnit.setValue(lstUnit.getSelectedItem().getAttribute("libelle").toString());
		 bdUnit.close();
	 }
	 
	 public void onFocus$bbUnit()
	 {
//		 if(lstUnit.getSelectedCount()==0)
//		   lstUnit.setSelectedIndex(0);
//			GredUnite unit =(GredUnite)lstUnit.getSelectedItem().getValue(); 
//			bdUnit.setValue(unit.getLibelleunite());
//			bdUnit.open();
	 }
	 
	 public void  onClick$btnRechercherUnit(){
		 if(txtUnit.getValue().equals("")){
			 codeunit = null;
			 
		 }else{
			 codeunit = txtUnit.getValue();
			 pageunit="0";
			 
		 }
		 Events.postEvent(ApplicationEvents.ON_SEARCH_UNIT_BANDBOX, this, pageunit);
	 }
	 
}
