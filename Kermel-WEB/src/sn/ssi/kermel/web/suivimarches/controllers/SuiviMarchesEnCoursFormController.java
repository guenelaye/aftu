package sn.ssi.kermel.web.suivimarches.controllers;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabpanel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.ContratSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class SuiviMarchesEnCoursFormController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code;
	private Tabpanel tabDetailsfecp;
	private Include incDetailsfecp;
	private Tab TAB_REALTRAVAUX,TAB_REALPI,TAB_REALSERV,TAB_FOURNITURES,TAB_REALDSP;
	private String LibelleTab;
	Session session = getHttpSession();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		TAB_REALTRAVAUX.setLabel(Labels.getLabel("kermel.plansdepassation.realisation.type.travaux")+" ("+BeanLocator.defaultLookup(ContratSession.class).count( null, null, null,new Long(3))+")");
		TAB_FOURNITURES.setLabel(Labels.getLabel("kermel.plansdepassation.realisation.type.fournitures")+" ("+BeanLocator.defaultLookup(ContratSession.class).count( null, null, null,new Long(1))+")");
		TAB_REALPI.setLabel(Labels.getLabel("kermel.plansdepassation.realisation.type.pi")+" ("+BeanLocator.defaultLookup(ContratSession.class).count( null, null, null,new Long(4))+")");
		TAB_REALSERV.setLabel(Labels.getLabel("kermel.plansdepassation.realisation.type.services")+" ("+BeanLocator.defaultLookup(ContratSession.class).count( null, null, null,new Long(2))+")");
		TAB_REALDSP.setLabel(Labels.getLabel("kermel.plansdepassation.realisation.type.dsp")+" ("+BeanLocator.defaultLookup(ContratSession.class).count( null, null, null,new Long(5))+")");
		
		    LibelleTab = (String) session.getAttribute("LibelleTab");
		    if(LibelleTab!=null)
		    {
		    	 if(LibelleTab.equals("REALTF"))
				  {
		    		 TAB_REALTRAVAUX.setSelected(true);
					Include inc = (Include) this.getFellowIfAny("incTRAVAUX");
			        inc.setSrc("/suivimarches/contrats.zul");	
				  }
				else
				{
					 if(LibelleTab.equals("REALPI"))
					  {
						 TAB_REALPI.setSelected(true);
						Include inc = (Include) this.getFellowIfAny("incREALPI");
				        inc.setSrc("/suivimarches/contrats.zul");	
					  }
					else
					{
						if(LibelleTab.equals("REALSERVICES"))
						  {
							TAB_REALSERV.setSelected(true);
							Include inc = (Include) this.getFellowIfAny("incREALSERV");
					        inc.setSrc("/suivimarches/contrats.zul");	
						  }
						else
						{
							if(LibelleTab.equals("REALDSP"))
							  {
								TAB_REALDSP.setSelected(true);
								Include inc = (Include) this.getFellowIfAny("incREALDSP");
						        inc.setSrc("/suivimarches/contrats.zul");	
							  }
							else
							{
								Include inc = (Include) this.getFellowIfAny("incTRAVAUX");
						        inc.setSrc("/suivimarches/contrats.zul");	
							}
						}
					}
				}
		    }
		    else
		    {
		    	 Include inc = (Include) this.getFellowIfAny("incTRAVAUX");
		    	inc.setSrc("/suivimarches/contrats.zul");	
		    }
		   
	
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	
}