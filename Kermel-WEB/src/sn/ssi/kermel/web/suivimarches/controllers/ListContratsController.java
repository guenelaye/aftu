package sn.ssi.kermel.web.suivimarches.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.passationsmarches.ejb.ContratsSession;
import sn.ssi.kermel.be.referentiel.ejb.ContratSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;


@SuppressWarnings("serial")
public class ListContratsController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null;
    private Listheader lshLibelle;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_SUIVI, MOD_GLOSSAIRE, DEL_GLOSSAIRE;
    private String login;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.FEA_SUIVIMARCHE);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_SUIVI != null) { ADD_SUIVI.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	//login = ((String) getHttpSession().getAttribute("user"));
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
		
			Long type;
			if(session.getAttribute("type")!=null){
			    type=(Long) session.getAttribute("type"); 
			}else
				type=new Long(3);
			 List<SygContrats> procedures = BeanLocator.defaultLookup(ContratSession.class).find(0,-1, null, null, null,type);
				 
			 SimpleListModel listModel = new SimpleListModel(procedures);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup( ContratSession.class).count(null,null,null,null));
		  
		     
		} 
		  
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			
           if (lstListe.getSelectedItem() == null)	
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			//final String uri = "/suivimarches/formsuivi.zul";

			
			session.setAttribute("contrat",lstListe .getSelectedItem().getValue());
			loadApplicationState("form_suivi");
			
			/*final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.suivismarches.titre"));
			display.put(DSP_HEIGHT,"600px");
			display.put(DSP_WIDTH, "95%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			//data.put(TypeServiceFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    	
			showPopupWindow(uri, data, display);
			*/
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			final String uri = "/referentiel/glossaire/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			//data.put(TypeServiceFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			//data.put(TypeServiceFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
				
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(ContratsSession.class).delete(codes);
			//	BeanLocator.defaultLookup(JournalMSession.class).logAction("SUPP_TYPESERVICE", Labels.getLabel("kermel.referentiel.common.typeservice.suppression")+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygContrats type = (SygContrats) data;
		item.setValue(type.getConID());

		 Listcell cellNum = new Listcell(String.valueOf(type.getConID()));
		 cellNum.setParent(item);
		 Listcell cellImmat = new Listcell(ToolKermel.dateToString(type.getDateimmatriculation()));
		 cellImmat.setParent(item);
		 
		 Listcell cellMontant = new Listcell("");
		 if(type.getMontant()!=null)
		 cellMontant = new Listcell(ToolKermel.formatDecimal(type.getMontant().doubleValue()));
		 cellMontant.setParent(item);
		 
		 Listcell cellObj = new Listcell("");
		 if((type.getDossier()!=null)&&(type.getDossier().getAppel()!=null))
		 cellObj = new Listcell(String.valueOf(type.getDossier().getAppel().getApoobjet()));
		 cellObj.setParent(item);
		 Listcell cellFournisseur = new Listcell(String.valueOf(type.getAutorite().getDenomination()));
		 cellFournisseur.setParent(item);
		 Listcell cellSignature = new Listcell(ToolKermel.dateToString(type.getDatedemandeimmatriculation()));
		 cellSignature.setParent(item);
		 
		 
		

	}
	public void onClick$bchercher()
	{
		
		if((txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.banque.libelle")))||(txtLibelle.getValue().equals("")))
		 {
			libelle=null;
			page=null;
		 }
		else
		{
			libelle=txtLibelle.getValue();
			page="0";
		}
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	
	public void onFocus$txtLibelle()
	{
		if(txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.banque.libelle")))
			txtLibelle.setValue("");
		
	}
	
	public void onBlur$txtLibelle()
	{
		if(txtLibelle.getValue().equals(""))
			txtLibelle.setValue(Labels.getLabel("kermel.referentiel.banque.libelle"));
	}

	public void onOK$txtLibelle()
	{
		onClick$bchercher();
	}
	
	
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
}