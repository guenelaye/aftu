package sn.ssi.kermel.web.suivimarches.controllers;


import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.referentiel.ejb.ContratSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



/**

 * @author Adama samb
 * @since mardi 11 Janvier 2011, 12:34
 
 */
public class SuiviProcedureFormController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private Include pgActes;
	SygContrats contrat=new SygContrats();
	private Long idcontrat;
	Session session = getHttpSession();
	private Label lblObjets,lblModePassation,lblMontant,lblModeSelection,lblType,lgtitre,lblDatecreation;
	private String LibelleTab;
	private Tab TAB_TDOSSIERS;
	private Tree tree;
	private Treecell financement,infosgenerales,celldossiers,demarrage
	,paiement,caution,calendrier,provisoire,definitive,nano,suivi;
	private Include idinclude;
	private Treeitem iteminfosgenerales,itemdossiers;
	private Treerow rowinfosgenerales;
	private Treeitem treeitem,treetdemarrage;
	
	
	 
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		


	}

	public void onCreate(CreateEvent createEvent) {
		
		  idcontrat=(Long) session.getAttribute("contrat");
		   contrat=BeanLocator.defaultLookup(ContratSession.class).findById(idcontrat);
		/*
		if(contrat.getTypemarche().getId().intValue()==UIConstants.PARAM_TMTRAVAUX)
			LibelleTab="REALTF";
			 */
	    lblObjets.setValue(contrat.getDossier().getAppel().getApoobjet());
		lblModePassation.setValue(contrat.getAutorite().getDenomination());
		lblMontant.setValue(ToolKermel.formatDecimal(contrat.getMontant().doubleValue()));
		lblModeSelection.setValue(contrat.getDossier().getAppel().getModeselection().getLibelle());
		lblType.setValue(contrat.getDossier().getAppel().getTypemarche().getLibelle());
		//session.setAttribute("LibelleTab",LibelleTab);
		if(contrat.getConDateRecepDefinitive()!=null)
		lblDatecreation.setValue(UtilVue.getInstance().formateLaDate2(contrat.getConDateRecepDefinitive()));
		  session.setAttribute("iddossier",contrat.getDossier().getDosID());
		  demarrage.setStyle("color:red;font-family: sans-serif; font-size: 14px");
		 idinclude.setSrc("/suivimarches/formordredeservicededemarragecontrat.zul");	
		/* tree.setSelectedItem(iteminfosgenerales);    
		 lgtitre.setValue(Labels.getLabel("kermel.plansdepassation.infos.generales.referencedossier")+": "+contrat.getAporeference());
	     */
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}
	

	
	public void onClick$demarrage() {
		 session.setAttribute("iddossier",contrat.getDossier().getDosID());
		 demarrage.setStyle("color:red;font-family: sans-serif; font-size: 14px");
		 provisoire.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
		 definitive.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
		idinclude.setSrc("/suivimarches/formordredeservicededemarragecontrat.zul");
		
	}
	
	public void onClick$provisoire() {
		 provisoire.setStyle("color:red;font-family: sans-serif; font-size: 14px");
		 demarrage.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
		 definitive.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
	
		 session.setAttribute("iddossier",contrat.getDossier().getDosID());
		idinclude.setSrc("/passationsmarches/procedurespassations/formreceptionprovisoire.zul");
	}
	
	public void onClick$definitive() {
		 session.setAttribute("iddossier",contrat.getDossier().getDosID());
		 definitive.setStyle("color:red;font-family: sans-serif; font-size: 14px");
		 provisoire.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
		 demarrage.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
	
		idinclude.setSrc("/passationsmarches/procedurespassations/formreceptiondefinitive.zul");
	}
	
	public void onClick$calendrierpaiement() {
		idinclude.setSrc("/suivimarches/calendrierpaiement.zul");
	}
	
	public void onClick$demandepaiement() {
		idinclude.setSrc("/suivimarches/demandepaiement/list.zul");
	}
	
	public void onClick$penalite() {
		idinclude.setSrc("/suivimarches/penalite/list.zul");
	}
	
	public void onClick$commissions() {
		idinclude.setSrc("/passationsmarches/procedurespassations/commissions.zul");
	}
	
	public void onClick$validationdossier() {
		idinclude.setSrc("/passationsmarches/procedurespassations/soumissionpourvalidation.zul");
	}
	
	public void onClick$publicationdossier() {
		idinclude.setSrc("/passationsmarches/procedurespassations/publicationdossier.zul");
	}
	
	public void onClick$registreretraitdao() {
		idinclude.setSrc("/passationsmarches/procedurespassations/registreretraitdao.zul");
	}
	
	public void onClick$plisouvertures() {
		idinclude.setSrc("/passationsmarches/procedurespassations/registredepotplis.zul");
	}
	
	public void onClick$listepresencemembrescommissions() {
		idinclude.setSrc("/passationsmarches/procedurespassations/listepresencemembrescommissions.zul");
	}
	
	public void onClick$representantssoumissionnaires() {
		idinclude.setSrc("/passationsmarches/procedurespassations/representantssoumissionnaires.zul");
	}
	
	public void onClick$garantiesoumission() {
		idinclude.setSrc("/passationsmarches/procedurespassations/garantiesoumission.zul");
	}
	
	public void onClick$lecturesoffres() {
		idinclude.setSrc("/passationsmarches/procedurespassations/formlecturesoffres.zul");
	}
	
	public void onClick$procesverbalouverture() {
		idinclude.setSrc("/passationsmarches/procedurespassations/procesverbalouverture.zul");
	}
	
	public void onClick$transmissiondossier() {
		idinclude.setSrc("/passationsmarches/procedurespassations/formtransmissiondossier.zul");
	}
	
	public void onClick$piecessoumissionnaires() {
		idinclude.setSrc("/passationsmarches/procedurespassations/piecessoumissionnaires.zul");
	}
	
	public void onClick$compositioncommissiontechnique() {
		idinclude.setSrc("/passationsmarches/procedurespassations/compositioncommissiontechnique.zul");
	}
	
	public void onClick$verificationconformite() {
		idinclude.setSrc("/passationsmarches/procedurespassations/formverificationconformite.zul");
	}
	
	public void onClick$correctionoffres() {
		idinclude.setSrc("/passationsmarches/procedurespassations/formcorrectionoffre.zul");
	}
	
	public void onClick$vercriteresqualification() {
		idinclude.setSrc("/passationsmarches/procedurespassations/vercriteresqualification.zul");
	}
	
	public void onClick$classementfinal() {
		idinclude.setSrc("/passationsmarches/procedurespassations/formclassementfinal.zul");
	}
	
	public void onClick$rapportevaluation() {
		idinclude.setSrc("/passationsmarches/procedurespassations/rapportevaluation.zul");
	}
	
	public void onClick$attributionprovisoire() {
		idinclude.setSrc("/passationsmarches/procedurespassations/attributionprovisoire.zul");
	}
	
	public void onClick$soumissionpourvalidationattributionprovisoire() {
		idinclude.setSrc("/passationsmarches/procedurespassations/soumissionpourvalidationattributionprovisoire.zul");
	}
	
	public void onClick$publicationattributionprovisoire() {
		idinclude.setSrc("/passationsmarches/procedurespassations/formpublicationattributionprovisoire.zul");
	}
	public void onClick$souscriptiondumarche() {
		idinclude.setSrc("/passationsmarches/procedurespassations/souscriptiondumarche.zul");
	}

	public void onClick$bonsengagement() {
		idinclude.setSrc("/passationsmarches/procedurespassations/formbonsengagement.zul");
	}
	
	public void onClick$approbationmarche() {
		idinclude.setSrc("/passationsmarches/procedurespassations/formapprobationmarche.zul");
	}
	
	public void onClick$notificationmarche() {
		idinclude.setSrc("/passationsmarches/procedurespassations/formnotificationmarche.zul");
	}
	
	public void onClick$publicationattributiondefinitive() {
		idinclude.setSrc("/passationsmarches/procedurespassations/formpublicationattributiondefinitive.zul");
	}
	public void onClick$ordredeservicededemarrage() {
		idinclude.setSrc("/passationsmarches/procedurespassations/formordredeservicededemarrage.zul");
	}
	
	public void onClick$receptionprovisoire() {
		idinclude.setSrc("/passationsmarches/procedurespassations/formreceptionprovisoire.zul");
	}
	
	public void onClick$receptiondefinitive() {
		idinclude.setSrc("/passationsmarches/procedurespassations/formreceptiondefinitive.zul");
	}
	
	public void onClick$recours() {
		//idinclude.setSrc("/passationsmarches/procedurespassations/recours.zul");
	}
	
	public void onClick$representantsservicestechniques() {
		idinclude.setSrc("/passationsmarches/procedurespassations/representantsservicestechniques.zul");
	}
	public void onClick$observateursindependants() {
		idinclude.setSrc("/passationsmarches/procedurespassations/observateursindependants.zul");
	}
	
	public void onSelect$tree(){
		if(tree.getSelectedItem().getId().equals("itemdossiers"))
			idinclude.setSrc("/passationsmarches/procedurespassations/traitementdossier.zul");
		
		
		
	}
	
	public void onSelect$iteminfosgenerales(){
		idinclude.setSrc("/passationsmarches/procedurespassations/observateursindependants.zul");
//        for (int i = 0; i < tree.getChildren().size(); i++) {
//			
//			if(tree.getSelectedItem().isCheckable()==true)
//			{
//				tree.getSelectedItem().setOpen(true);
//			}
//			else
//			{
//				tree.getSelectedItem().setOpen(false);
//			}
//			//treeitem=tree.getSelectedItem()..getParentItem();
//		}
		
	}
}