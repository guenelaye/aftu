package sn.ssi.kermel.web.securite.controleaccess.modules.controllers;

import java.util.Map;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SysModule;
import sn.ssi.kermel.be.security.ModulesSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class ModuleFormController extends AbstractWindow implements
		AfterCompose, EventListener {

	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_MODULE_ID = "MODULE_ID";

	String mode;
	String moduleCode;
	SysModule module;

	private Textbox txtLibelle, txtDescription, txtCode;
	private Intbox intSequence;

	private void initUI() {
		if (mode.equalsIgnoreCase("EDIT")) {
			txtCode.setValue(module.getModCode());
			txtCode.setDisabled(true);
			txtDescription.setValue(module.getModDescription());
			txtLibelle.setValue(module.getModLibelle());
			intSequence.setValue(module.getModSequence());
		}
	}

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(final CreateEvent createEvent) {
		final Map windowParams = createEvent.getArg();
		mode = (String) windowParams.get(WINDOW_PARAM_MODE);
		if (mode.equalsIgnoreCase("EDIT")) {
			moduleCode = (String) windowParams.get(WINDOW_PARAM_MODULE_ID);
			module = BeanLocator.defaultLookup(ModulesSession.class)
					.findByCode(moduleCode);
		}
		initUI();
	}

	@Override
	public void onEvent(final Event event) throws Exception {

	}

	public void onOK() {
		final SysModule module = new SysModule();
		module.setModCode(txtCode.getValue());
		module.setModLibelle(txtLibelle.getValue());
		module.setModDescription(txtDescription.getValue());
		module.setModSequence(intSequence.getValue());
		BeanLocator.defaultLookup(ModulesSession.class).createModule(module);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();
	}

}
