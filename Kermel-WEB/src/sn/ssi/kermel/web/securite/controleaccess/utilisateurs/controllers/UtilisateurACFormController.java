package sn.ssi.kermel.web.securite.controleaccess.utilisateurs.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygMembresCommissionsMarches;
import sn.ssi.kermel.be.entity.SygService;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.entity.SysProfil;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteContractanteSession;
import sn.ssi.kermel.be.referentiel.ejb.MembresCommissionsMarchesSession;
import sn.ssi.kermel.be.referentiel.ejb.ServiceSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeAutoriteContractanteSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.be.security.ProfilsSession;
import sn.ssi.kermel.be.security.UtilisateurSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class UtilisateurACFormController extends AbstractWindow implements AfterCompose, EventListener, ListitemRenderer {

	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_USER_ID = "USER_ID";

	String mode;
	long usrId = -1;
	Utilisateur user;
	private Listbox lstProfil,lstAutorite,lstService,lstTypeAutorite;
	private Textbox txtLogin, txtPassword, txtPrenom,txtRechercherService, txtNom, txtMail,txtRechercherProfil,txtRechercherAutorite,txtPasswordconfirm,
	txtTel,txtFax,txtAdresse,txtRechercherTypeAutorite,txtCodeType;
	private Paging pgProfil,pgAutorite,pgService,pgTypeAutorite;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE,activePage;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private String libelle=null,libelleautorite=null,libelleservice=null,codeautorite=null,libelletype=null;
	private SysProfil profil;
	private SygAutoriteContractante autorite;
	private SygService service;
	private Bandbox bdProfil,bdService;
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg,login,type;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Radio rdoui,rdnon;
	private Utilisateur infoscompte;
	private Div step0,step1,step2;
	private Label lblAutorite,lblType,lblTypeStep1;
	private Menuitem menuNext,menuPrevious;
	private Label lblService;
	private SygTypeAutoriteContractante typeautorite;
	List<SygAutoriteContractante> autorites = new ArrayList<SygAutoriteContractante>();
	private SygMembresCommissionsMarches membre =new SygMembresCommissionsMarches();
	
	private void initUI() {
		if (mode.equalsIgnoreCase("EDIT")) {

			txtLogin.setValue(user.getLogin());
			txtPassword.setValue(user.getPassword());
			txtPasswordconfirm.setValue(user.getPassword());
			txtLogin.setValue(user.getLogin());
			txtPrenom.setValue(user.getPrenom());
			txtNom.setValue(user.getNom());
			txtMail.setValue(user.getMail());
			profil=user.getSysProfil();
		
			bdProfil.setValue(profil.getPfLibelle());
			autorite=user.getAutorite();
			txtTel.setValue(autorite.getTelephone());
			txtFax.setValue(autorite.getFax());
			txtAdresse.setValue(autorite.getAdresse());
			if(user.getActif()==true)
				rdoui.setSelected(true);
            else
            	rdnon.setSelected(true);
			if(user.getServices()!=null)
			{
				service=user.getServices();
				
				bdService.setValue(service.getLibelle());
			}
			
				
			
			// txtPassword.setDisabled(true);
		}
	}

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		lstProfil.setItemRenderer(this);
		pgProfil.setPageSize(byPageBandbox);
		pgProfil.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
	
		
		addEventListener(ApplicationEvents.ON_AUTORITES, this);
		lstAutorite.setItemRenderer(new AutoritesRenderer());
		pgAutorite.setPageSize(byPage);
		pgAutorite.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTORITES);
		
		addEventListener(ApplicationEvents.ON_SERVICES, this);
		lstService.setItemRenderer(new ServicesRenderer());
		pgService.setPageSize(byPageBandbox);
		pgService.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_SERVICES);
		
		addEventListener(ApplicationEvents.ON_TYPES, this);
		lstTypeAutorite.setItemRenderer(new TypesAutorites());
		pgTypeAutorite.setPageSize(byPageBandbox);
		pgTypeAutorite.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_TYPES);
		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(final CreateEvent createEvent) {
		final Map windowParams = createEvent.getArg();
		mode = (String) windowParams.get(WINDOW_PARAM_MODE);
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getType().equals(UIConstants.USERS_TYPES_AC))
		  {
			 type=UIConstants.USERS_TYPES_AGENTAC;
			 autorite=infoscompte.getAutorite();
			 lblAutorite.setValue(autorite.getDenomination());
        	 Events.postEvent(ApplicationEvents.ON_SERVICES, this, null);
			 menuNext.setDisabled(true);
			 menuPrevious.setDisabled(true);
			 step0.setVisible(false);
			 step2.setVisible(false);
			 step1.setVisible(true);
			 lblService.setVisible(true);
		  }
		else
		{
			type=UIConstants.USERS_TYPES_AC;
			bdService.setDisabled(true);
		
			Events.postEvent(ApplicationEvents.ON_TYPES, this, null);
			 lblService.setVisible(false);
		}
		if (mode.equalsIgnoreCase("EDIT")) {
			usrId = (Long) windowParams.get(WINDOW_PARAM_USER_ID);
			user = BeanLocator.defaultLookup(UtilisateurSession.class).findByCode(usrId);
			autorite=user.getAutorite();
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		initUI();
	}

	@Override
	public void onEvent(final Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgProfil.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgProfil.getActivePage() * byPageBandbox;
				pgProfil.setPageSize(byPageBandbox);
			}
			List<SysProfil> profils = BeanLocator.defaultLookup(ProfilsSession.class).findProfils(activePage, byPageBandbox, null, libelle,type,autorite);
			lstProfil.setModel(new SimpleListModel(profils));
			pgProfil.setTotalSize(BeanLocator.defaultLookup(ProfilsSession.class).countAllProfils(null, libelle,type,autorite));
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_TYPES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgTypeAutorite.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgTypeAutorite.getActivePage() * byPageBandbox;
				pgTypeAutorite.setPageSize(byPageBandbox);
			}
			List<SygTypeAutoriteContractante> types = BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).find(activePage, byPageBandbox, null, libelletype);
			lstTypeAutorite.setModel(new SimpleListModel(types));
			pgTypeAutorite.setTotalSize(BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).count(null, libelletype));
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTORITES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgAutorite.setPageSize(1000);
			} else {
				byPage= UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgAutorite.getActivePage() * byPage;
				pgAutorite.setPageSize(byPage);
			}
			List<SygAutoriteContractante> autorites = BeanLocator.defaultLookup(AutoriteContractanteSession.class).find(activePage, byPage, libelleautorite,null,typeautorite,codeautorite, null);
			lstAutorite.setModel(new SimpleListModel(autorites));
			pgAutorite.setTotalSize(BeanLocator.defaultLookup(AutoriteContractanteSession.class).count(libelleautorite, null,typeautorite,codeautorite, null));
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_SERVICES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgService.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgService.getActivePage() * byPageBandbox;
				pgService.setPageSize(byPageBandbox);
			}
			List<SygService> services = BeanLocator.defaultLookup(ServiceSession.class).find(activePage, byPageBandbox, null,libelleservice,autorite, null);
			lstService.setModel(new SimpleListModel(services));
			pgService.setTotalSize(BeanLocator.defaultLookup(ServiceSession.class).count(null,libelleservice,autorite, null));
		}
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			user = new Utilisateur();
			if (usrId > 0) {
				user = BeanLocator.defaultLookup(UtilisateurSession.class).findByCode(usrId);
			}
            if(rdoui.isChecked()==true)
            	user.setActif(true);
            else
            	user.setActif(false);
			user.setLogin(txtLogin.getValue());
			user.setPassword(txtPassword.getValue());
			user.setNom(txtNom.getValue());
			user.setPrenom(txtPrenom.getValue());
			user.setMail(txtMail.getValue());
			user.setAutorite(autorite);
			user.setSysProfil(profil);
			
			user.setTelephone(txtTel.getValue());
			user.setFax(txtFax.getValue());
			user.setAdresse(txtAdresse.getValue());
			if(type.equals(UIConstants.USERS_TYPES_AGENTAC))
			{
				user.setServices(service);
				membre.setNom(txtNom.getValue());
				membre.setPrenom(txtPrenom.getValue());
				membre.setGestion("2013");
			    membre.setTypemembre("Titulaire");
				membre.setTel(txtTel.getValue());
				membre.setEmail(txtMail.getValue());
				membre.setFonction("Responsable");
				membre.setFlagpresident(UIConstants.NPARENT);
				membre.setEtapePI(UIConstants.NPARENT);
				membre.setAutorite(autorite);
				BeanLocator.defaultLookup(MembresCommissionsMarchesSession.class).save(membre);
			}
				
			String login = ((String) getHttpSession().getAttribute("user"));
			if (usrId > 0) {
				BeanLocator.defaultLookup(UtilisateurSession.class).updateUtilisateur(user);
				BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_UTILISATEURAC",Labels.getLabel("kermel.securite.users.modif.ac") + new Date(), login);
				
			} else {
				user.setType(type);
				BeanLocator.defaultLookup(UtilisateurSession.class).createUtilisateur(user);
				BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_UTILISATEURAC",Labels.getLabel("kermel.securite.users.ajout.ac") + new Date(), login);
				
			}
			
			
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
		
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		 SysProfil profil = (SysProfil) data;
		item.setValue(profil);
	
	    Listcell cellCode = new Listcell(profil.getPfCode());
	    cellCode.setParent(item);
		
		Listcell cellLibelle = new Listcell(profil.getPfLibelle());
		cellLibelle.setParent(item);
	}

	///////////Profil///////// 
	public void onSelect$lstProfil(){
		profil= (SysProfil) lstProfil.getSelectedItem().getValue();
		bdProfil.setValue(profil.getPfLibelle());
	   
	    bdProfil.close();
	
	}
	
	public void onClick$menuPrevious(){
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
	}
	public void onClick$menuFermerstep2(){
		detach();
	}
	
	public void onClick$menuFermer(){
		detach();
	}
	
	public void onClick$menuNext(){
		if (lstAutorite.getSelectedItem() == null)
			throw new WrongValueException(lstAutorite, Labels.getLabel("kermel.error.select.item"));
		
		autorite=(SygAutoriteContractante) lstAutorite.getSelectedItem().getValue();
		lblAutorite.setValue(autorite.getDenomination());
		step0.setVisible(false);
		step1.setVisible(true);
	}
	public void onFocus$txtRechercherProfil(){
		if(txtRechercherProfil.getValue().equalsIgnoreCase(Labels.getLabel("kermel.list.user.profil"))){
			txtRechercherProfil.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherProfil(){
			String page;
		if(txtRechercherProfil.getValue().equalsIgnoreCase(Labels.getLabel("kermel.list.user.profil")) || txtRechercherProfil.getValue().equals("")){
			libelle = null;
			page=null;
		}else{
			libelle= txtRechercherProfil.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
		
		
		 
			
		 private boolean checkFieldConstraints() {
		 		
		 		try {
		 			if(bdProfil.getValue().equals(""))
		 		     {
		                errorComponent = bdProfil;
		                errorMsg = Labels.getLabel("kermel.list.user.profil")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
		 				lbStatusBar.setValue(errorMsg);
		 				throw new WrongValueException (errorComponent, errorMsg);
		 		     }
		 			
		 			if(txtPrenom.getValue().equals(""))
		 		     {
		                errorComponent = txtPrenom;
		                errorMsg = Labels.getLabel("kermel.common.form.prenom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
		 				lbStatusBar.setValue(errorMsg);
		 				throw new WrongValueException (errorComponent, errorMsg);
		 		     }
		 			
		 		
		 			if(txtNom.getValue().equals(""))
		 		     {
		                errorComponent = txtNom;
		                errorMsg = Labels.getLabel("kermel.common.form.nom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
		 				lbStatusBar.setValue(errorMsg);
		 				throw new WrongValueException (errorComponent, errorMsg);
		 		     }
		 			if(type.equals(UIConstants.USERS_TYPES_AGENTAC))
		 			{
		 				if(bdService.getValue().equals(""))
			 		     {
			                errorComponent = bdService;
			                errorMsg = Labels.getLabel("kermel.plansdepassation.service.maitredoeuvre")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
			 				lbStatusBar.setValue(errorMsg);
			 				throw new WrongValueException (errorComponent, errorMsg);
			 		     }
		 			}
		 			if(txtMail.getValue().equals(""))
		 		     {
		                errorComponent = txtMail;
		                errorMsg = Labels.getLabel("kermel.referentiel.personne.email")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
		 				lbStatusBar.setValue(errorMsg);
		 				throw new WrongValueException (errorComponent, errorMsg);
		 		     }
		 			if (ToolKermel.ControlValidateEmail(txtMail.getValue())) {
						
					} else {
						errorComponent = txtMail;
		                errorMsg = Labels.getLabel("kermel.referentiel.personne.email.incorrect");
		 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
		 				lbStatusBar.setValue(errorMsg);
		 				throw new WrongValueException(errorComponent,errorMsg);
					}
		 			
		 			 if(txtLogin.getValue().equals(""))
		 			 {
                           errorComponent = txtPrenom;
			                errorMsg = Labels.getLabel("username")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
			 				lbStatusBar.setValue(errorMsg);
			 				throw new WrongValueException (errorComponent, errorMsg);
		 			 }
		 			if(txtPassword.getValue().equals(""))
		 		     {
		                errorComponent = txtPassword;
		                errorMsg = Labels.getLabel("password")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
		 				lbStatusBar.setValue(errorMsg);
		 				throw new WrongValueException (errorComponent, errorMsg);
		 		     }
		 			if(txtPasswordconfirm.getValue().equals(""))
		 		     {
		                errorComponent = txtPasswordconfirm;
		                errorMsg = Labels.getLabel("password.confirm")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
		 				lbStatusBar.setValue(errorMsg);
		 				throw new WrongValueException (errorComponent, errorMsg);
		 		     }
		 	    	if(!txtPasswordconfirm.getValue().equals(txtPassword.getValue()))
		 		     {
		                errorComponent = txtPasswordconfirm;
		                errorMsg = Labels.getLabel("password.confirm.differend");
		 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
		 				lbStatusBar.setValue(errorMsg);
		 				throw new WrongValueException (errorComponent, errorMsg);
		 		     }
		 			
		 			return true;
		 				
		 		}
		 		catch (Exception e) {
		 			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
		 			+ " [checkFieldConstraints]";
		 			errorComponent = null;
		 			return false;

		 			
		 		}
		 		
		 	}
		
		 public void onOK$btnRechercherAutorite(){
			 onClick$btnRechercherAutorite();
			 
		 }
		 public void onOK$txtRechercherAutorite(){
			 onClick$btnRechercherAutorite();
			 
		 }
		 public void onBlur$txtRechercherAutorite(){
				if(txtRechercherAutorite.getValue().equalsIgnoreCase("")){
					txtRechercherAutorite.setValue(Labels.getLabel("kermel.autoritecontractante.libelle"));
				}		 
			}

	
		public void onFocus$txtRechercherAutorite(){
			if(txtRechercherAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.autoritecontractante.libelle"))){
				txtRechercherAutorite.setValue("");
			}		 
		}
	
	
	   public void onBlur$txtCodeType(){
			if(txtCodeType.getValue().equalsIgnoreCase("")){
				txtCodeType.setValue(Labels.getLabel("kermel.referentiel.common.autorisation.code"));
			}		 
		}


			public void onFocus$txtCodeType(){
				if(txtCodeType.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.autorisation.code"))){
					txtCodeType.setValue("");
				}		 
			}

			public void onOK$txtCodeType(){
				 onClick$btnRechercherAutorite();
				 
			}
			
		public void  onClick$btnRechercherAutorite(){
			String page=null;
			if(txtRechercherAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.autoritecontractante.libelle")) || txtRechercherAutorite.getValue().equals(""))
			   {
				  libelleautorite = null;
			   }
			else
			   {
				  libelleautorite= txtRechercherAutorite.getValue();
				  page="0";
			   }
			if(txtCodeType.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.autorisation.code")) || txtCodeType.getValue().equals(""))
			   {
				  codeautorite = null;
			   }
			else
			   {
				codeautorite= txtCodeType.getValue();
				  page="0";
			   }
		Events.postEvent(ApplicationEvents.ON_AUTORITES, this, page);
		}
		
		public class AutoritesRenderer implements ListitemRenderer{
			
			
			
			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
				SygAutoriteContractante autorites = (SygAutoriteContractante) data;
				item.setValue(autorites);
				
				Listcell cellCode = new Listcell(autorites.getSigle());
				cellCode.setParent(item);
			
				Listcell cellLibelle = new Listcell(autorites.getDenomination());
				cellLibelle.setParent(item);
				if (mode.equalsIgnoreCase("EDIT")) 
				{
					if(user.getAutorite().getId().intValue()==autorites.getId().intValue())
						item.setSelected(true);
				}
			}
		}
		
		
	
///////////Services///////// 
	public void onSelect$lstService(){
		service= (SygService) lstService.getSelectedItem().getValue();
		bdService.setValue(service.getLibelle());
		
		bdService.close();
	
	}
	
	public void onFocus$txtRechercherService(){
		if(txtRechercherService.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.service.maitredoeuvre"))){
			txtRechercherService.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherService(){
			String page;
		if(txtRechercherService.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.service.maitredoeuvre")) || txtRechercherService.getValue().equals("")){
			libelleservice = null;
			page=null;
		}else{
			libelleservice= txtRechercherService.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_SERVICES, this, page);
		}
		
		public class ServicesRenderer implements ListitemRenderer{
			
			
			
			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
				SygService services = (SygService) data;
				item.setValue(services);
				
				Listcell cellCode = new Listcell(services.getCodification());
				cellCode.setParent(item);
			
				Listcell cellLibelle = new Listcell(services.getLibelle());
				cellLibelle.setParent(item);
			}
		}
		
	
	
		 public class TypesAutorites implements ListitemRenderer{
				
				
				
				@Override
				public void render(Listitem item, Object data, int index)  throws Exception {
					SygTypeAutoriteContractante types = (SygTypeAutoriteContractante) data;
					item.setValue(types);
					
					Listcell cellCode = new Listcell(types.getCode());
					cellCode.setParent(item);
				
					Listcell cellLibelle = new Listcell(types.getLibelle());
					cellLibelle.setParent(item);
					
					autorites = BeanLocator.defaultLookup(AutoriteContractanteSession.class).find(0, -1, null,null,types,null, null);
					
					Listcell cellNombre = new Listcell(Integer.toString(autorites.size()));
					cellNombre.setParent(item);
					
					if (mode.equalsIgnoreCase("EDIT")) 
					{
						if(user.getAutorite().getType().getId().intValue()==types.getId().intValue())
							item.setSelected(true);
					}
				}
			}
			
			public void onClick$menuNextstep2(){
				if (lstTypeAutorite.getSelectedItem() == null)
					throw new WrongValueException(lstTypeAutorite, Labels.getLabel("kermel.error.select.item"));
				
				typeautorite=(SygTypeAutoriteContractante) lstTypeAutorite.getSelectedItem().getValue();
				lblType.setValue(typeautorite.getLibelle());
				lblTypeStep1.setValue(typeautorite.getLibelle());
				if (mode.equalsIgnoreCase("EDIT"))
				  codeautorite=autorite.getSigle();
				step2.setVisible(false);
				step0.setVisible(true);
				step1.setVisible(false);
				Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);
			}
			
			public void onClick$menuPreviousStep0(){
				step0.setVisible(false);
				step1.setVisible(false);
				step2.setVisible(true);
			}
			
			
			 public void onOK$btnRechercherTypeAutorite(){
				 onClick$btnRechercherTypeAutorite();
				 
			 }
			 
			 public void onOK$txtRechercherTypeAutorite(){
				 onClick$btnRechercherTypeAutorite();
				 
			 }
			
			 public void onBlur$txtRechercherTypeAutorite(){
					if(txtRechercherTypeAutorite.getValue().equalsIgnoreCase("")){
						txtRechercherTypeAutorite.setValue(Labels.getLabel("kermel.autoritecontractante.libelle"));
					}		 
				}

		
		public void onFocus$txtRechercherTypeAutorite(){
			if(txtRechercherTypeAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.autoritecontractante.libelle"))){
				txtRechercherTypeAutorite.setValue("");
			}		 
		}
		
	
			
			public void  onClick$btnRechercherTypeAutorite(){
				String page=null;
				if(txtRechercherTypeAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.autoritecontractante.libelle")) || txtRechercherTypeAutorite.getValue().equals(""))
				   {
					  libelletype = null;
				   }
				else
				   {
					libelletype= txtRechercherTypeAutorite.getValue();
					  page="0";
				   }
			
			     Events.postEvent(ApplicationEvents.ON_TYPES, this, page);
			}
			
}
