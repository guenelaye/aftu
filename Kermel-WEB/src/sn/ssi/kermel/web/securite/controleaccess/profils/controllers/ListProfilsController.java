package sn.ssi.kermel.web.securite.controleaccess.profils.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SysProfil;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.security.ProfilsSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.processus.controllers.ProcessFormController;

@SuppressWarnings("serial")
public class ListProfilsController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstProfils;
	private Paging pgProfils;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_PROFIL = "CURRENT_PROFIL";
	private Utilisateur infoscompte;
	private String type;

	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);

		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener("onProcess", this);
		addEventListener("onFeatures", this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lstProfils.setItemRenderer(this);

		pgProfils.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pgProfils.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(final CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if((infoscompte.getType().equals(UIConstants.USERS_TYPES_DCMP)))
		{
			type=UIConstants.USERS_TYPES_DCMP;
		}
		else
		{
			if((infoscompte.getType().equals(UIConstants.USERS_TYPES_ARMP)))
			{
				type=UIConstants.USERS_TYPES_ARMP;
			}
			else
			{
				if((infoscompte.getType().equals(UIConstants.USERS_TYPES_AC))||(infoscompte.getType().equals(UIConstants.USERS_TYPES_AGENTAC)))
				{
					type=UIConstants.USERS_TYPES_AC;
				}
			}
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			final List<SysProfil> profils = BeanLocator.defaultLookup( ProfilsSession.class).findProfils( pgProfils.getActivePage() * byPage, byPage, null, null,type,null);
			final SimpleListModel listModel = new SimpleListModel(profils);
			lstProfils.setModel(listModel);
			pgProfils.setTotalSize(BeanLocator.defaultLookup( ProfilsSession.class).countAllProfils(null, null,type,null));
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/securite/controleaccess/profils/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_HEIGHT, "500px");
			display.put(DSP_TITLE, "Nouveau Profil");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ProfilFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstProfils.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/securite/controleaccess/profils/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_HEIGHT, "500px");
			display.put(DSP_TITLE, "Editer Profil");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ProfilFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(ProfilFormController.WINDOW_PARAM_PROFIL_CODE, lstProfils
					.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} else if (event.getName().equalsIgnoreCase("onProcess")) {
			if (lstProfils.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/processus/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_WIDTH, "800px");
			display.put(DSP_HEIGHT, "500px");
			display.put(DSP_TITLE, "Configuration des processus");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ProcessFormController.WINDOW_PARAM_PROFIL_CODE, lstProfils
					.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} else if (event.getName().equalsIgnoreCase("onFeatures")) {
			if (lstProfils.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/features/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_WIDTH, "800px");
			display.put(DSP_HEIGHT, "500px");
			display.put(DSP_TITLE, "Fonctionnalit�s");
			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ProcessFormController.WINDOW_PARAM_PROFIL_CODE, lstProfils
					.getSelectedItem().getValue());
			showPopupWindow(uri, data, display);
		}  else if (event.getName()
				.equalsIgnoreCase(ApplicationEvents.ON_DELETE)) {
			if (lstProfils.getSelectedItem() == null) {
				alert("");
				return;
			}
			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(MessageBoxController.DSP_MESSAGE, "Supprimer ?");
			display.put(MessageBoxController.DSP_TITLE, "Supprimer ?");
			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(CURRENT_PROFIL, lstProfils.getSelectedItem().getValue());

			showMessageBox(display, data);
		}

		else if (event.getName().equalsIgnoreCase(
				ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {
			final String profilCode = (String) ((HashMap<String, Object>) event
					.getData()).get(CURRENT_PROFIL);
			BeanLocator.defaultLookup(ProfilsSession.class).deleteProfil(
					profilCode);
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}

		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DETAILS)) {
			if (lstProfils.getSelectedItem() == null) {
				alert("");
				return;
			}
			loadApplicationState("profil_actions&"
					+ lstProfils.getSelectedItem().getValue());
		} else if (event.getName().equalsIgnoreCase(Events.ON_DOUBLE_CLICK)) {
			final Listitem listitem = (Listitem) event.getTarget();
			final String uri = "/securite/controleaccess/profils/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_WIDTH, "500px");
			display.put(DSP_HEIGHT, "500px");
			display.put(DSP_TITLE, "Editer Module");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ProfilFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(ProfilFormController.WINDOW_PARAM_PROFIL_CODE, listitem
					.getValue());

			showPopupWindow(uri, data, display);
		}

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		final SysProfil profil = (SysProfil) data;
		item.setValue(profil.getPfCode());
		item.addEventListener(Events.ON_DOUBLE_CLICK, this);

		final Listcell cellCode = new Listcell(profil.getPfCode());
		cellCode.setParent(item);

		final Listcell cellLibelle = new Listcell(profil.getPfLibelle());
		cellLibelle.setParent(item);

		final Listcell cellDescription = new Listcell(profil.getPfDescription());
		cellDescription.setParent(item);

	}

}