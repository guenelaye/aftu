package sn.ssi.kermel.web.securite.controleaccess.utilisateurs.controllers;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SysProfil;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.be.security.ProfilsSession;
import sn.ssi.kermel.be.security.UtilisateurSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class UtilisateurFormController extends AbstractWindow implements AfterCompose, EventListener, ListitemRenderer {

	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_USER_ID = "USER_ID";

	String mode;
	long usrId = -1;
	Utilisateur user;
	private Listbox lstProfil;
	private Textbox txtLogin, txtPassword, txtPrenom, txtNom, txtMail,txtProfil,txtRechercherProfil,txtPasswordconfirm,txtAdresse,txtFax,txtTel,txtFonction,txtService;
	private Paging pgProfil;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE,activePage;
	private String libelle=null,type=null;
	private SysProfil profil;
	private Bandbox bdProfil;
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg,login;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Radio rdoui,rdnon;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite;
	
	private void initUI() {
		if (mode.equalsIgnoreCase("EDIT")) {

			txtLogin.setValue(user.getLogin());
			txtPassword.setValue(user.getPassword());
			txtPasswordconfirm.setValue(user.getPassword());
			txtLogin.setValue(user.getLogin());
			txtPrenom.setValue(user.getPrenom());
			txtNom.setValue(user.getNom());
			txtMail.setValue(user.getMail());
			profil=user.getSysProfil();
			txtProfil.setValue(profil.getPfCode());
			bdProfil.setValue(profil.getPfLibelle());
			txtAdresse.setValue(user.getAdresse());
			txtFax.setValue(user.getFax());
			txtFonction.setValue(user.getFonction());
			txtTel.setValue(user.getTelephone());
			txtService.setValue(user.getService());
			if(user.getActif()==true)
				rdoui.setSelected(true);
            else
            	rdnon.setSelected(true);
		}
	}

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		lstProfil.setItemRenderer(this);
		pgProfil.setPageSize(byPageBandbox);
		pgProfil.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(final CreateEvent createEvent) {
		final Map windowParams = createEvent.getArg();
		mode = (String) windowParams.get(WINDOW_PARAM_MODE);
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		autorite=null;
	      	
		
		if (mode.equalsIgnoreCase("EDIT")) {
			usrId = (Long) windowParams.get(WINDOW_PARAM_USER_ID);
			user = BeanLocator.defaultLookup(UtilisateurSession.class).findByCode(usrId);
		}
		type=UIConstants.USERS_TYPES_ARMP;
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		initUI();
	}

	@Override
	public void onEvent(final Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgProfil.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgProfil.getActivePage() * byPageBandbox;
				pgProfil.setPageSize(byPageBandbox);
			}
			List<SysProfil> profils = BeanLocator.defaultLookup(ProfilsSession.class).findProfils(activePage, byPageBandbox, null, libelle,type,autorite);
			lstProfil.setModel(new SimpleListModel(profils));
			pgProfil.setTotalSize(BeanLocator.defaultLookup(ProfilsSession.class).countAllProfils(null, libelle,type,autorite));
			
			
		}
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			user = new Utilisateur();
			if (usrId > 0) {
				user = BeanLocator.defaultLookup(UtilisateurSession.class).findByCode(usrId);
			}
			 if(rdoui.isChecked()==true)
	            	user.setActif(true);
	            else
	            	user.setActif(false);
			user.setLogin(txtLogin.getValue());
			user.setPassword(txtPassword.getValue());
			user.setNom(txtNom.getValue());
			user.setPrenom(txtPrenom.getValue());
			user.setMail(txtMail.getValue());
			user.setAdresse(txtAdresse.getValue());
			user.setFax(txtFax.getValue());
			user.setFonction(txtFonction.getValue());
			user.setTelephone(txtTel.getValue());
			user.setService(txtService.getValue());
			user.setSysProfil(profil);
			user.setType(UIConstants.USERS_TYPES_ARMP);
		
			String login = ((String) getHttpSession().getAttribute("user"));
			if (usrId > 0) {
				BeanLocator.defaultLookup(UtilisateurSession.class).updateUtilisateur(user);
				BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_UTILSATEUR", Labels.getLabel("kermel.securite.users.modif.armp") + new Date(), login);
					
			} else {
				BeanLocator.defaultLookup(UtilisateurSession.class).createUtilisateur(user);
				BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_UTILSATEUR", Labels.getLabel("kermel.securite.users.ajout.armp") + new Date(), login);
				
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
		
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		 SysProfil profil = (SysProfil) data;
		item.setValue(profil);
	
	    Listcell cellCode = new Listcell(profil.getPfCode());
	    cellCode.setParent(item);
		
		Listcell cellLibelle = new Listcell(profil.getPfLibelle());
		cellLibelle.setParent(item);
	}

	///////////Service///////// 
	public void onSelect$lstProfil(){
		profil= (SysProfil) lstProfil.getSelectedItem().getValue();
		bdProfil.setValue(profil.getPfLibelle());
	    txtProfil.setValue(profil.getPfCode());
	    bdProfil.close();
	
	}
	
	public void onFocus$txtRechercherProfil(){
		if(txtRechercherProfil.getValue().equalsIgnoreCase(Labels.getLabel("kermel.list.user.profil"))){
			txtRechercherProfil.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherProfil(){
			String page;
		if(txtRechercherProfil.getValue().equalsIgnoreCase(Labels.getLabel("kermel.list.user.profil")) || txtRechercherProfil.getValue().equals("")){
			libelle = null;
			page=null;
		}else{
			libelle= txtRechercherProfil.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
		
		 public void onOK$txtProfil() {
		List<SysProfil> profils = BeanLocator.defaultLookup(ProfilsSession.class).findProfils(activePage, byPageBandbox, txtProfil.getValue(), null,type,autorite);
				
				if (profils.size() > 0) {
					profil=(SysProfil)profils.get(0);
					txtProfil.setValue(profil.getPfCode());
					bdProfil.setValue(profil.getPfLibelle());
					
				}
				else {
					bdProfil.setValue(null);
					bdProfil.open();
					txtProfil.setFocus(true);
				}
				
			}
		 
			
		 private boolean checkFieldConstraints() {
		 		
		 		try {

		 			if(bdProfil.getValue().equals(""))
		 		     {
		                errorComponent = bdProfil;
		                errorMsg = Labels.getLabel("kermel.list.user.profil")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
		 				lbStatusBar.setValue(errorMsg);
		 				throw new WrongValueException (errorComponent, errorMsg);
		 		     }
		 			if(txtPrenom.getValue().equals(""))
		 		     {
		                errorComponent = txtPrenom;
		                errorMsg = Labels.getLabel("kermel.common.form.prenom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
		 				lbStatusBar.setValue(errorMsg);
		 				throw new WrongValueException (errorComponent, errorMsg);
		 		     }

		 			if(txtNom.getValue().equals(""))
		 		     {
		                errorComponent = txtNom;
		                errorMsg = Labels.getLabel("kermel.common.form.nom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
		 				lbStatusBar.setValue(errorMsg);
		 				throw new WrongValueException (errorComponent, errorMsg);
		 		     }
		 			 if(txtMail.getValue().equals(""))
		 			 {
                           errorComponent = txtMail;
			                errorMsg = Labels.getLabel("kermel.referentiel.personne.email")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
			 				lbStatusBar.setValue(errorMsg);
			 				throw new WrongValueException (errorComponent, errorMsg);
		 			 }
                 if (ToolKermel.ControlValidateEmail(txtMail.getValue())) {
						
					} else {
						errorComponent = txtMail;
		                errorMsg = Labels.getLabel("kermel.referentiel.personne.email.incorrect");
		 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
		 				lbStatusBar.setValue(errorMsg);
		 				throw new WrongValueException(errorComponent,errorMsg);
					}
		 	    	if(txtPassword.getValue().equals(""))
		 		     {
		                errorComponent = txtPassword;
		                errorMsg = Labels.getLabel("password")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
		 				lbStatusBar.setValue(errorMsg);
		 				throw new WrongValueException (errorComponent, errorMsg);
		 		     }
		 	    	if(txtPasswordconfirm.getValue().equals(""))
		 		     {
		                errorComponent = txtPasswordconfirm;
		                errorMsg = Labels.getLabel("password.confirm")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
		 				lbStatusBar.setValue(errorMsg);
		 				throw new WrongValueException (errorComponent, errorMsg);
		 		     }
		 	    	if(!txtPasswordconfirm.getValue().equals(txtPassword.getValue()))
		 		     {
		                errorComponent = txtPasswordconfirm;
		                errorMsg = Labels.getLabel("password.confirm.differend");
		 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
		 				lbStatusBar.setValue(errorMsg);
		 				throw new WrongValueException (errorComponent, errorMsg);
		 		     }
		 			return true;
		 				
		 		}
		 		catch (Exception e) {
		 			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
		 			+ " [checkFieldConstraints]";
		 			errorComponent = null;
		 			return false;

		 			
		 		}
		 		
		 	}
}
