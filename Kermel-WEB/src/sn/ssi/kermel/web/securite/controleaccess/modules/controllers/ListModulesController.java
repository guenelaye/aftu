package sn.ssi.kermel.web.securite.controleaccess.modules.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SysModule;
import sn.ssi.kermel.be.security.ModulesSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class ListModulesController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstModules;
	private Listheader lshCode;
	private Paging pgModules;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE = "CURRENT_MODULE";

	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);

		lshCode.setSortAscending(new FieldComparator("code", false));
		lshCode.setSortDescending(new FieldComparator("code", true));

		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lstModules.setItemRenderer(this);

		pgModules.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pgModules.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

	}

	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			final List<SysModule> modules = BeanLocator.defaultLookup(
					ModulesSession.class).findModules(
					pgModules.getActivePage() * byPage, byPage);
			final SimpleListModel listModel = new SimpleListModel(modules);
			lstModules.setModel(listModel);
			pgModules.setTotalSize(BeanLocator.defaultLookup(
					ModulesSession.class).countAllModules());
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/securite/controleaccess/modules/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_WIDTH, "500px");
			display.put(DSP_HEIGHT, "500px");
			display.put(DSP_TITLE, "Nouveau Module");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ModuleFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstModules.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/securite/controleaccess/modules/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_WIDTH, "500px");
			display.put(DSP_HEIGHT, "500px");
			display.put(DSP_TITLE, "Editer Module");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ModuleFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(ModuleFormController.WINDOW_PARAM_MODULE_ID, lstModules
					.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} else if (event.getName().equalsIgnoreCase(Events.ON_DOUBLE_CLICK)) {
			final Listitem listitem = (Listitem) event.getTarget();
			final String uri = "/securite/controleaccess/modules/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_WIDTH, "500px");
			display.put(DSP_HEIGHT, "500px");
			display.put(DSP_TITLE, "Editer Module");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ModuleFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(ModuleFormController.WINDOW_PARAM_MODULE_ID, listitem
					.getValue());

			showPopupWindow(uri, data, display);
		} else if (event.getName()
				.equalsIgnoreCase(ApplicationEvents.ON_DELETE)) {
			if (lstModules.getSelectedItem() == null) {
				alert("");
				return;
			}
			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(MessageBoxController.DSP_MESSAGE, "Supprimer ?");
			display.put(MessageBoxController.DSP_TITLE, "Supprimer ?");
			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(CURRENT_MODULE, lstModules.getSelectedItem().getValue());

			showMessageBox(display, data);
		}

		else if (event.getName().equalsIgnoreCase(
				ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {
			final String module = (String) ((HashMap<String, Object>) event
					.getData()).get(CURRENT_MODULE);
			BeanLocator.defaultLookup(ModulesSession.class)
					.deleteModule(module);
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}

		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DETAILS)) {
			if (lstModules.getSelectedItem() == null) {
				alert("");
				return;
			}
			loadApplicationState("module_actions&"
					+ lstModules.getSelectedItem().getValue());
		}

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		final SysModule module = (SysModule) data;
		item.setValue(module.getModCode());
		item.addEventListener(Events.ON_DOUBLE_CLICK, this);

		final Listcell cellCode = new Listcell(module.getModCode());
		cellCode.setParent(item);

		final Listcell cellLibelle = new Listcell(module.getModLibelle());
		cellLibelle.setParent(item);

		final Listcell cellDescription = new Listcell(module
				.getModDescription());
		cellDescription.setParent(item);
	}
}