package sn.ssi.kermel.web.securite.controleaccess.utilisateurs.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.security.UtilisateurSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class ChangerMotDePasseController extends AbstractWindow implements AfterCompose {
	private Utilisateur users = new Utilisateur();
	private Textbox ancienmotdepasse, nouvmotdepasse, confirmmotdepasse;
	List<Utilisateur> arraylistutilisateur = new ArrayList<Utilisateur>();
	private Session session = getHttpSession();
	private String login;
	private Long userid;
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		login = (String) session.getAttribute("user");
		userid = ((Utilisateur) session.getAttribute("utilisateur")).getId();
		users = BeanLocator.defaultLookup(UtilisateurSession.class).findByCode(userid);

	}

	public void onClick$menuValider() {
		try {
			if (checkFieldConstraints()) {
				users.setPassword(nouvmotdepasse.getValue());
				BeanLocator.defaultLookup(UtilisateurSession.class).updateUtilisateur(users);
				Messagebox.show("Le Mot de passe a  été changé avec succés . ", "Erreur", Messagebox.OK, Messagebox.ERROR);

				detach();
			}
		} catch (Exception e) {
		}
	}

	public void onClick$menuFermer() {
		detach();
	}

	private boolean checkFieldConstraints() {
		try {
			if (!ancienmotdepasse.getValue().equals("")) {
				users.setPassword(nouvmotdepasse.getValue());
				arraylistutilisateur = BeanLocator.defaultLookup(UtilisateurSession.class).findUser(login, ancienmotdepasse.getValue());
			}
			if (ancienmotdepasse.getValue().equals("")) {
				errorComponent = ancienmotdepasse;
				errorMsg = "Le Mot de passe est obligatoire.";
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);
			}
			if (nouvmotdepasse.getValue().equals("")) {
				errorComponent = nouvmotdepasse;
				errorMsg = "Nouveau mot de passe est obligatoire.";
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);
			}
			if (confirmmotdepasse.getValue().equals("")) {
				errorComponent = confirmmotdepasse;
				errorMsg = "Confirmation mot de passe est obligatoire.";
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);
			}
			if (arraylistutilisateur.size() == 0) {
				errorComponent = ancienmotdepasse;
				errorMsg = "Le Mot de passe saisi est incorrect.";
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				ancienmotdepasse.setValue("");
				throw new WrongValueException(errorComponent, errorMsg);
			}
			if (!nouvmotdepasse.getValue().equals(confirmmotdepasse.getValue())) {
				errorComponent = confirmmotdepasse;
				errorMsg = "Les deux mots de passe sont différents.";
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				confirmmotdepasse.setValue("");
				throw new WrongValueException(errorComponent, errorMsg);
			}

			return true;

		} catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString() + " [checkFieldConstraints]";
			errorComponent = null;
			return false;

		}

	}
}