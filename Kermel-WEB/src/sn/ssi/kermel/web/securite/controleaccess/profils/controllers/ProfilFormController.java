package sn.ssi.kermel.web.securite.controleaccess.profils.controllers;

import java.util.ArrayList;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SysProfil;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.security.ProfilsSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class ProfilFormController extends AbstractWindow implements
		AfterCompose, EventListener {

	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_PROFIL_CODE = "PROFIL_CODE";

	String mode;
	String profilCode;
	SysProfil profil;
	private ArrayList<String> listValeursTypes;

	private Textbox txtLibelle, txtDescription, txtCode;
	private Combobox cbtype;
	private Utilisateur infoscompte;

	private void initUI() {
		if (mode.equalsIgnoreCase("EDIT")) {
			txtCode.setValue(profil.getPfCode());
			txtCode.setDisabled(true);
			txtDescription.setValue(profil.getPfDescription());
			txtLibelle.setValue(profil.getPfLibelle());
			if(profil.getType().equals(UIConstants.USERS_TYPES_ARMP))
				cbtype.setValue(UIConstants.USERS_TYPES_ARMP);
			if(profil.getType().equals(UIConstants.USERS_TYPES_DCMP))
				cbtype.setValue(UIConstants.USERS_TYPES_DCMP);
			if(profil.getType().equals(UIConstants.USERS_TYPES_AC))
				cbtype.setValue(Labels.getLabel("kermel.autoritecontractante.libelle"));
			if(profil.getType().equals(UIConstants.USERS_TYPES_AGENTAC))
				cbtype.setValue(Labels.getLabel("kermel.plansdepassation.service.maitredoeuvre"));
			
		}
	}

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(final CreateEvent createEvent) {
		final Map windowParams = createEvent.getArg();
		mode = (String) windowParams.get(WINDOW_PARAM_MODE);
		listValeursTypes = new ArrayList<String>();
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if((infoscompte.getType().equals(UIConstants.USERS_TYPES_DCMP)))
		{
			listValeursTypes.add(Labels.getLabel("kermel.autoritecontractante.libelle"));
			listValeursTypes.add(UIConstants.USERS_TYPES_DCMP);
		}
		else
		{
			if((infoscompte.getType().equals(UIConstants.USERS_TYPES_ARMP)))
			{
				listValeursTypes.add(UIConstants.USERS_TYPES_ARMP);
			}
			else
			{
				if((infoscompte.getType().equals(UIConstants.USERS_TYPES_AC))||(infoscompte.getType().equals(UIConstants.USERS_TYPES_AGENTAC)))
				{
					listValeursTypes.add(Labels.getLabel("kermel.autoritecontractante.libelle"));
					listValeursTypes.add(Labels.getLabel("kermel.plansdepassation.service.maitredoeuvre"));
				}
				else
				{
					listValeursTypes.add(UIConstants.USERS_TYPES_DCMP);
					listValeursTypes.add(UIConstants.USERS_TYPES_ARMP);
					listValeursTypes.add(Labels.getLabel("kermel.autoritecontractante.libelle"));
					listValeursTypes.add(Labels.getLabel("kermel.plansdepassation.service.maitredoeuvre"));
				}
			}
		}
		if (mode.equalsIgnoreCase("EDIT")) {
			profilCode = (String) windowParams.get(WINDOW_PARAM_PROFIL_CODE);
			profil = BeanLocator.defaultLookup(ProfilsSession.class)
					.findByCode(profilCode);
		}
		cbtype.setModel(new SimpleListModel(listValeursTypes));
		initUI();
		
	}

	@Override
	public void onEvent(final Event event) throws Exception {

	}

	public void onOK() {
		final SysProfil profil = new SysProfil();
		profil.setPfCode(txtCode.getValue());
		profil.setPfLibelle(txtLibelle.getValue());
		profil.setPfDescription(txtDescription.getValue());
		if(cbtype.getValue().toString().equals("armp"))
			profil.setType(UIConstants.USERS_TYPES_ARMP);
		if(cbtype.getValue().toString().equals("dcmp"))
			profil.setType(UIConstants.USERS_TYPES_DCMP);
		if(cbtype.getValue().toString().equals(Labels.getLabel("kermel.autoritecontractante.libelle")))
			profil.setType(UIConstants.USERS_TYPES_AC);
		if(cbtype.getValue().toString().equals(Labels.getLabel("kermel.plansdepassation.service.maitredoeuvre")))
			profil.setType(UIConstants.USERS_TYPES_ARMP);
		BeanLocator.defaultLookup(ProfilsSession.class).createProfil(profil);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();
	}

}
