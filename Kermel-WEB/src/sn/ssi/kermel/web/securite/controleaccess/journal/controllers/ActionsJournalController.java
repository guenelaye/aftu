package sn.ssi.kermel.web.securite.controleaccess.journal.controllers;

import java.util.Date;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SysJournal;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

/**
 * @author Guillaume GB (guibenissan@gmail.com)
 */
public class ActionsJournalController extends AbstractWindow implements
		AfterCompose, EventListener, ListitemRenderer {

	private static final long serialVersionUID = 2475863992270769593L;

	/* elements de la vue, manipules par le controleur */
	private Datebox dateDebut, dateFin;
	private Listbox listJournal;
	private Paging pagingListJournal;
	private Label labelStatusBar;

	/* Evenements specifiques a cette vue */
	private static final String LISTJOURNAL_MODEL_CHANGE_EVENT = "onLEModelChange";

	/* Criteres de filtre */
	private Date dateBorneInf, dateBorneSup;

	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private static final String ERROR_MSG_STYLE = "color:red";
	// private static final String READY_MSG_STYLE = "";
	private Component errorComponent;
	private String errorMsg;

	@Override
	public void afterCompose() {

		/* liaison des champs de la vue aux proprietes du controleur */
		Components.wireFellows(this, this);

		/* liaison des champs de la vue aux methodes evenement */
		Components.addForwards(this, this);

		/* ajout d'ecouteurs d'evenement specifiques a cette vue */
		addEventListener(LISTJOURNAL_MODEL_CHANGE_EVENT, this);

		/*
		 * specification d'un interpreteur de modele de donnee, pour chaque
		 * liste de la vue
		 */
		listJournal.setItemRenderer(this);

		/* pagination de liste */
		pagingListJournal.setPageSize(byPage);
		pagingListJournal.addForward(ApplicationEvents.ON_PAGING, this,
				LISTJOURNAL_MODEL_CHANGE_EVENT);

		/* envoi d'evenement */
		if (resetSearchCriteria()) {
			Events.postEvent(LISTJOURNAL_MODEL_CHANGE_EVENT, this, null);
		} else {
			labelStatusBar.setValue(errorMsg);
			labelStatusBar.setStyle(ERROR_MSG_STYLE);
			throw new WrongValueException(errorComponent, errorMsg);
		}

	}

	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {

		SysJournal journal = (SysJournal) data;

		item.setValue(journal.getJouId());

		item.appendChild(new Listcell(journal.getActLibelle()));
		item.appendChild(new Listcell(UtilVue.getInstance().formateLaDate(
				journal.getJouDate())));
		item.appendChild(new Listcell(journal.getJouDesc()));
		item.appendChild(new Listcell(journal.getUsrLogin()));

	}

	@Override
	public void onEvent(Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(LISTJOURNAL_MODEL_CHANGE_EVENT)) {
			List<SysJournal> journals = BeanLocator.defaultLookup(
					JournalSession.class).find(dateBorneInf, dateBorneSup,
					pagingListJournal.getActivePage() * byPage, byPage);
			listJournal.setModel(new SimpleListModel(journals));
			pagingListJournal.setTotalSize(BeanLocator.defaultLookup(
					JournalSession.class).count(dateBorneInf, dateBorneSup));

			dateDebut.setFocus(true);
		}

	}

	public void onClick$buttonRechercher() {

		if (getSearchCriteria()) {
			pagingListJournal.setActivePage(0);
			Events.postEvent(LISTJOURNAL_MODEL_CHANGE_EVENT, this, null);
		} else {
			labelStatusBar.setValue(errorMsg);
			labelStatusBar.setStyle(ERROR_MSG_STYLE);
			throw new WrongValueException(errorComponent, errorMsg);
		}

	}

	public void onOK$dateDebut() {

		onClick$buttonRechercher();

	}

	public void onOK$dateFin() {

		onClick$buttonRechercher();

	}

	public void onCancel$dateDebut() {

		if (resetSearchCriteria()) {
			Events.postEvent(LISTJOURNAL_MODEL_CHANGE_EVENT, this, null);
		} else {
			labelStatusBar.setValue(errorMsg);
			labelStatusBar.setStyle(ERROR_MSG_STYLE);
			throw new WrongValueException(errorComponent, errorMsg);
		}

	}

	public void onCancel$dateFin() {

		onCancel$dateDebut();

	}

	private boolean getSearchCriteria() {

		try {
			/* Controle dates non coherentes */

			if ((dateDebut.getValue() != null) && (dateFin.getValue() != null)) {
				if (dateFin.getValue().before(dateDebut.getValue())) {
					errorMsg = Labels
							.getLabel("kermel.error.unavailableperiod");
					errorComponent = null;

					return false;
				}
			}

			/* Recuperation des criteres de recherche */
			dateBorneInf = dateDebut.getValue();
			dateBorneSup = dateFin.getValue();
			return true;
		} catch (Exception e) {
			labelStatusBar.setValue(e.toString());

			return false;
		}

	}

	private boolean resetSearchCriteria() {

		try {
			dateBorneInf = null;
			dateBorneSup = null;
			dateDebut.setValue(null);
			dateFin.setValue(null);

			return true;
		} catch (Exception e) {
			labelStatusBar.setValue(e.toString());

			return false;
		}

	}

}
