package sn.ssi.kermel.web.aooprequalification.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timebox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrederetraitdaoSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class DepotspreselectionnesController extends AbstractWindow implements EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstBailleur;
	private Paging pgPagination,pgPlis;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage,CURRENT_NOMBRE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference,libellebailleur=null;
    private int parent;
    private Label lbltitre;
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherBailleur,txtChapitre;
    SygBailleurs bailleur=null;
    private Decimalbox dcmontantprevu;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idplis=null;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuAjouter,menuModifier,menuSupprimer;
	private Div step0,step1,step2;
     public static final String EDIT_URL = "http://"+UIConstants.IP_SSI+":"+UIConstants.PORT_SSI+"/EtatsKermel/OuverturePlisPDFServlet";
    private Iframe iframe;
    private	SygPlisouvertures  plis= new SygPlisouvertures();
    private	SygRetraitregistredao  retrait= new SygRetraitregistredao();
	private List<SygPlisouvertures> plisdepot = new ArrayList<SygPlisouvertures>();
	private List<SygPlisouvertures> depotspreselections = new ArrayList<SygPlisouvertures>();
	private List<SygRetraitregistredao> retraits = new ArrayList<SygRetraitregistredao>();
	private int etatvalide;
	private Intbox numero;
	private Datebox datedepot;
	private Timebox heuredepot;
	private Textbox observations,observationsdechargecandidat,txtRechercherCandidat;
	private Bandbox bdCandidat;
	private Listbox lstCandidat;
	private Paging pgCandidat;
	private  String raisonsocial=null;
	private Combobox cbmodereception;
	private Combobox cbnumero;
	private ArrayList<String> listValeursNumeros;
	
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
	
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,1);
		if(dossier==null)
		{
			menuAjouter.setDisabled(true);
			menuSupprimer.setDisabled(true);
			menuModifier.setDisabled(true);
		}
		else
		{
			retraits=BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).find(0,-1,dossier);
			if(appel.getApoDatepvouverturepli()!=null)
			{
				menuAjouter.setDisabled(true);
				menuSupprimer.setDisabled(true);
				menuModifier.setDisabled(true);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			
		}
		//listValeursNumeros = new ArrayList<String>();
	
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			depotspreselections = BeanLocator.defaultLookup(RegistrededepotSession.class).find(activePage,byPage,dossier,null,null,null,-1,-1,-1, -1, -1, -1, null,-1, null, null);
			 lstListe.setModel(new SimpleListModel(depotspreselections));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(RegistrededepotSession.class).count(dossier,null,null,null,-1,-1,1, -1, -1, -1, null, -1, null, null));

		} 
	
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygPlisouvertures plis = (SygPlisouvertures) data;
		item.setValue(plis);

		 Listcell cellNumero = new Listcell(Integer.toString(plis.getNumero()));
		 cellNumero.setParent(item);
		 
		 Listcell cellRaison = new Listcell(plis.getRetrait().getNomSoumissionnaire());
		 cellRaison.setParent(item);
		 
		 Listcell cellTelephone = new Listcell(plis.getRetrait().getTelephone());
		 cellTelephone.setParent(item);
		 
		 Listcell cellEmail = new Listcell(plis.getRetrait().getEmail());
		 cellEmail.setParent(item);
		 
		 Listcell cellDate = new Listcell(UtilVue.getInstance().formateLaDate2(plis.getDateDepot()));
		 cellDate.setParent(item);
		 
		 Listcell cellHeure = new Listcell(UtilVue.getInstance().formateLHeure(plis.getHeuredepot()));
		 cellHeure.setParent(item);
		 
		 if(plis.getEtatPreselection()==UIConstants.PARENT)
			 item.setSelected(true);
	}

	public void onClick$menuValider()
	{
       if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
       
       for (int k = 0; k < depotspreselections.size(); k++) {
    	   plis=depotspreselections.get(k);
    	   plis.setEtatPreselection(UIConstants.NPARENT);
      	   BeanLocator.defaultLookup(RegistrededepotSession.class).update(plis);
       }
       for (int i = 0; i < lstListe.getSelectedCount(); i++) {
    	 
    	   plis=(SygPlisouvertures) ((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
    	   plis.setEtatPreselection(UIConstants.PARENT);
    	   plis.setNotifie("non");
      	   BeanLocator.defaultLookup(RegistrededepotSession.class).update(plis);
       }
        session.setAttribute("libelle", "depotspreselectionnes");
		loadApplicationState("aoo_prequalification");
		
		
	}
}