package sn.ssi.kermel.web.aooprequalification.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timebox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrederetraitdaoSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class RegistreDepotplisController extends AbstractWindow implements EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstBailleur;
	private Paging pgPagination,pgPlis;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage,CURRENT_NOMBRE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference,libellebailleur=null;
    private int parent;
    private Label lbltitre;
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherBailleur,txtChapitre;
    SygBailleurs bailleur=null;
    private Decimalbox dcmontantprevu;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idplis=null;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuAjouter,menuModifier,menuSupprimer;
	private Div step0,step1,step2;
     public static final String EDIT_URL = "http://"+UIConstants.IP_SSI+":"+UIConstants.PORT_SSI+"/EtatsKermel/OuverturePlisPDFServlet";
    private Iframe iframe;
    private	SygPlisouvertures  plis= new SygPlisouvertures();
    private	SygRetraitregistredao  retrait= new SygRetraitregistredao();
	private List<SygPlisouvertures> plisdepot = new ArrayList<SygPlisouvertures>();
	private List<SygPlisouvertures> existenumero = new ArrayList<SygPlisouvertures>();
	private List<SygRetraitregistredao> retraits = new ArrayList<SygRetraitregistredao>();
	private int etatvalide;
	private Intbox numero;
	private Datebox datedepot;
	private Timebox heuredepot;
	private Textbox observations,observationsdechargecandidat,txtRechercherCandidat;
	private Bandbox bdCandidat;
	private Listbox lstCandidat;
	private Paging pgCandidat;
	private  String raisonsocial=null;
	private Combobox cbmodereception;
	private Combobox cbnumero;
	private ArrayList<String> listValeursNumeros;
	
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
		addEventListener(ApplicationEvents.ON_PLIS, this);
		lstCandidat.setItemRenderer(new CandidatsRenderer());
		pgCandidat.setPageSize(byPageBandbox);
		pgCandidat.addForward("onPaging", this, ApplicationEvents.ON_PLIS);
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,1);
		if(dossier==null)
		{
			menuAjouter.setDisabled(true);
			menuSupprimer.setDisabled(true);
			menuModifier.setDisabled(true);
		}
		else
		{
			retraits=BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).find(0,-1,dossier);
			if(appel.getApoDatepvouverturepli()!=null)
			{
				menuAjouter.setDisabled(true);
				menuSupprimer.setDisabled(true);
				menuModifier.setDisabled(true);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			
		}
		//listValeursNumeros = new ArrayList<String>();
	
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygPlisouvertures> plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(activePage,byPage,dossier,null,null,null,-1,-1,-1, -1, -1, -1, null, -1, null, null);
			 lstListe.setModel(new SimpleListModel(plis));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(RegistrededepotSession.class).count(dossier,null,null,null,-1,-1,1, -1, -1, -1, null, -1, null, null));
//			 for(int i=1;i<=retraits.size();i++)
//			 {
//				 if(plis.size()>0)
//				 {
//					 for(int j=0;j<plis.size();j++)
//					 {
//						 if(i!=plis.get(j).getNumero())
//							 listValeursNumeros.add(i+"");
//					 } 
//				 }
//				 else
//				 {
//					 listValeursNumeros.add(i+""); 
//				 }
//				
//			 }
//			 cbnumero.setModel(new SimpleListModel(listValeursNumeros));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_PLIS)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgCandidat.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgCandidat.getActivePage() * byPageBandbox;
				pgCandidat.setPageSize(byPage);
			}
			List<SygRetraitregistredao> plis =BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).Candidats(pgCandidat.getActivePage()*byPageBandbox, byPage,dossier,raisonsocial);
			lstCandidat.setModel(new SimpleListModel(plis));
			pgCandidat.setTotalSize(BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).countCandidats(dossier,raisonsocial));
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = ((SygPlisouvertures)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue()).getId();
				BeanLocator.defaultLookup(RegistrededepotSession.class).delete(codes);
			}
			session.setAttribute("libelle", "plisouvertures");
			loadApplicationState("aoo_prequalification");
		}
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygPlisouvertures plis = (SygPlisouvertures) data;
		item.setValue(plis);

		 Listcell cellNumero = new Listcell(Integer.toString(plis.getNumero()));
		 cellNumero.setParent(item);
		 
		 Listcell cellRaison = new Listcell(plis.getRetrait().getNomSoumissionnaire());
		 cellRaison.setParent(item);
		 
		 Listcell cellTelephone = new Listcell(plis.getRetrait().getTelephone());
		 cellTelephone.setParent(item);
		 
		 Listcell cellEmail = new Listcell(plis.getRetrait().getEmail());
		 cellEmail.setParent(item);
		 
		 Listcell cellDate = new Listcell(UtilVue.getInstance().formateLaDate2(plis.getDateDepot()));
		 cellDate.setParent(item);
		 
		 Listcell cellHeure = new Listcell(UtilVue.getInstance().formateLHeure(plis.getHeuredepot()));
		 cellHeure.setParent(item);
	}

	public void onClick$menuAjouter()
	{
		Events.postEvent(ApplicationEvents.ON_PLIS, this, null);
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
		
	}
	
	public void onClick$menuFermer()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
//		session.setAttribute("libelle", "plisouvertures");
//		loadApplicationState("aoo_prequalification");
	}
	
	public void onClick$menuSupprimer()
	{
		if (lstListe.getSelectedItem() == null)
			
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		

		HashMap<String, String> display = new HashMap<String, String>(); // permet
		display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
		display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
		display.put(MessageBoxController.DSP_HEIGHT, "250px");
		display.put(MessageBoxController.DSP_WIDTH, "47%");

		HashMap<String, Object> map = new HashMap<String, Object>(); // permet
		map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
		showMessageBox(display, map);
		
	}
	
   public void onClick$menuEditer(){
		
	   iframe.setSrc(EDIT_URL + "?code="+dossier.getDosID()+ "&libelle=registredepot");
			step0.setVisible(false);
		    step1.setVisible(false);
			step2.setVisible(true);
		
		
	}
   
   public void onClick$menuCancel()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
   
 
 		
 		public class CandidatsRenderer implements ListitemRenderer{
 			
 			
 			
 			@Override
 			public void render(Listitem item, Object data, int index)  throws Exception {
 				SygRetraitregistredao candidat = (SygRetraitregistredao) data;
 				item.setValue(candidat);
 				
 				Listcell cellNom = new Listcell(candidat.getNomSoumissionnaire());
 				cellNom.setParent(item);
 				
 				
 				}
 			}
 	
 		public void onClick$menuModifier()
 		{
 			if (lstListe.getSelectedItem() == null)
 			  throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
 			
 			plis=(SygPlisouvertures) lstListe.getSelectedItem().getValue();
 			idplis=plis.getId();
 			retrait=plis.getRetrait();
 			bdCandidat.setValue(retrait.getNomSoumissionnaire());
			numero.setValue(plis.getNumero());
			datedepot.setValue(plis.getDateDepot());
			cbmodereception.setValue(plis.getModereception());
			heuredepot.setValue(plis.getHeuredepot());
			observations.setValue(plis.getObservationsoffres());
			observationsdechargecandidat.setValue(plis.getObservationscandidats());
 			step0.setVisible(false);
 			step1.setVisible(true);
 			step2.setVisible(false);
 		}
 		public void onClick$menuValider()
 		{
 			
 			if(checkFieldConstraints())
 			{
 				plis.setDossier(dossier);
 				plis.setRetrait(retrait);
 				plis.setRaisonsociale(retrait.getNomSoumissionnaire());
 				plis.setNumero(numero.getValue());
 				plis.setDateDepot(datedepot.getValue());
 				plis.setModereception(cbmodereception.getValue());
 				plis.setHeuredepot(heuredepot.getValue());
 				plis.setObservationsoffres(observations.getValue());
 				plis.setObservationscandidats(observationsdechargecandidat.getValue());
 				plis.setRabais(0);
 				plis.setScoretechnique(0);
 				plis.setSeuilatteint(0);
 				plis.setClassementechnique(0);
 				plis.setClassementgeneral(0);
 				plis.setCandidatrestreint_ID(0);
 				plis.setEtatPreselection(0);
 				plis.setEtatExamenPreliminaire(0);
 				plis.setCritereQualification(0);
 				plis.setAttributaireProvisoire(0);
 				plis.setOffreTechnique(0);
 				plis.setOffreFinanciere(0);
 				plis.setLettreSoumission(0);
 				plis.setValide(0);
 				plis.setGarantie(0);
 				plis.setPiecerequise(0);
 				
 				if(idplis==null)
 				  BeanLocator.defaultLookup(RegistrededepotSession.class).save(plis);
 				else
 				  BeanLocator.defaultLookup(RegistrededepotSession.class).update(plis);
 				
 				session.setAttribute("libelle", "plisouvertures");
 				loadApplicationState("aoo_prequalification");
 			}
 		}
 		
 		private boolean checkFieldConstraints() {
 			
 			try {
 			
 				if(numero.getValue()==null)
 			     {
 	               errorComponent = numero;
 	               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registredepot.numeroordrearrivee")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
 				   lbStatusBar.setStyle(ERROR_MSG_STYLE);
 				   lbStatusBar.setValue(errorMsg);
 				  throw new WrongValueException (errorComponent, errorMsg);
 			     }
 				if(numero.getValue()>retraits.size())
			     {
	               errorComponent = numero;
	               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registredepot.numeroordrearrivee.maximum")+": "+retraits.size();
				   lbStatusBar.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar.setValue(errorMsg);
				  throw new WrongValueException (errorComponent, errorMsg);
			     }
 				plisdepot=BeanLocator.defaultLookup(RegistrededepotSession.class).find(dossier, null, numero.getValue(), null);
 				if (idappel!=null)
 				{
 					if(plis.getNumero()!=numero.getValue())
 					{
 						 if(plisdepot.size()>0)
 					     {
 			                errorComponent = numero;
 							errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registredepot.numeroordrearrivee")+" "+Labels.getLabel("kermel.referentiel.date.existe")+": "+numero.getValue();
 							lbStatusBar.setStyle(ERROR_MSG_STYLE);
 							lbStatusBar.setValue(errorMsg);
 							throw new WrongValueException (errorComponent, errorMsg);
 					    }
 					}
 				 
 				}
 				else
 				{
 					  if(plisdepot.size()>0)
 					  {
 						errorComponent = numero;
 						errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registredepot.numeroordrearrivee")+" "+ Labels.getLabel("kermel.referentiel.existe")+" :"+numero.getValue();
 						lbStatusBar.setStyle(ERROR_MSG_STYLE);
 						lbStatusBar.setValue(errorMsg);
 						throw new WrongValueException (errorComponent, errorMsg);
 					  }
 				}
 						
 				if(bdCandidat.getValue().equals(""))
			     {
	               errorComponent = bdCandidat;
	               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registredepot.nomcandidat")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				   lbStatusBar.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar.setValue(errorMsg);
				  throw new WrongValueException (errorComponent, errorMsg);
			     }
 				if(cbmodereception.getValue().equals(""))
			     {
	               errorComponent = cbmodereception;
	               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registredepot.modereceptionplis")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				   lbStatusBar.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar.setValue(errorMsg);
				  throw new WrongValueException (errorComponent, errorMsg);
			     }
 				if(datedepot.getValue()==null)
			     {
	               errorComponent = datedepot;
	               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registredepot.datereceptionplis")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				   lbStatusBar.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar.setValue(errorMsg);
				  throw new WrongValueException (errorComponent, errorMsg);
			     }
 				if((dossier.getDosDateLimiteDepot()).after(datedepot.getValue()))
 				 {
 					errorComponent = datedepot;
 					errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registredepot.datereceptionplis")+" "+Labels.getLabel("kermel.referentiel.date.posterieure")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.infosgenerales.saisietermereference.datelimitedepot")+": "+UtilVue.getInstance().formateLaDate(dossier.getDosDateLimiteDepot());
 					lbStatusBar.setStyle(ERROR_MSG_STYLE);
 					lbStatusBar.setValue(errorMsg);
 					throw new WrongValueException (errorComponent, errorMsg);
 				  }
 				if((datedepot.getValue()).after(dossier.getDosDateOuvertueDesplis()))
 				 {
 					errorComponent = datedepot;
 					errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registredepot.datereceptionplis")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.dateouvertureplis")+": "+UtilVue.getInstance().formateLaDate(dossier.getDosDateOuvertueDesplis());
 					lbStatusBar.setStyle(ERROR_MSG_STYLE);
 					lbStatusBar.setValue(errorMsg);
 					throw new WrongValueException (errorComponent, errorMsg);
 				  }
 				
 				if((datedepot.getValue()).after(new Date()))
 				 {
 					errorComponent = datedepot;
 					errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registredepot.datereceptionplis")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.referentiel.date.dujour")+": "+UtilVue.getInstance().formateLaDate(new Date());
 					lbStatusBar.setStyle(ERROR_MSG_STYLE);
 					lbStatusBar.setValue(errorMsg);
 					throw new WrongValueException (errorComponent, errorMsg);
 				  }
 				if(heuredepot.getValue()==null)
			     {
	               errorComponent = heuredepot;
	               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registredepot.heurereceptionplis")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				   lbStatusBar.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar.setValue(errorMsg);
				  throw new WrongValueException (errorComponent, errorMsg);
			     }
 				return true;
 			}
 			catch (Exception e) {
 				errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
 				+ " [checkFieldConstraints]";
 				errorComponent = null;
 				return false;

 				
 			}
 			
 		}
 		
 		public void onSelect$lstCandidat(){
 			retrait= (SygRetraitregistredao) lstCandidat.getSelectedItem().getValue();
 			bdCandidat.setValue(retrait.getNomSoumissionnaire());
 			bdCandidat.close();
 		
 		}
 		
 		public void onFocus$txtRechercherCandidat(){
 			if(txtRechercherCandidat.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registredepot.nomcandidat"))){
 				txtRechercherCandidat.setValue("");
 			}		 
 			}
 			
 			public void  onOK$btnRechercherPays(){
 				onClick$btnRechercherCandidat();
 			}
 			public void  onOK$txtRechercherCandidat(){
 				onClick$btnRechercherCandidat();
 			}
 			public void  onClick$btnRechercherCandidat(){
 			if(txtRechercherCandidat.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registredepot.nomcandidat")) || txtRechercherCandidat.getValue().equals("")){
 				raisonsocial = null;
 			page=null;
 			}else{
 				raisonsocial = txtRechercherCandidat.getValue();
 			page="0";
 			}
 			Events.postEvent(ApplicationEvents.ON_PLIS, this, page);
 			}
}