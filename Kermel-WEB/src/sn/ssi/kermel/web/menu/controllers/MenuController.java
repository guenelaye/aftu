package sn.ssi.kermel.web.menu.controllers;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Menuitem;

import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.web.common.components.KermelMenu;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class MenuController extends AbstractWindow implements AfterCompose {

	private static final long serialVersionUID = 1L;
	KermelMenu monMenu;
	Menuitem mdp;
	Session session = getHttpSession();
	private Utilisateur user=new Utilisateur();
	private String autorite="";

	public void afterCompose() {
		Components.wireVariables(this, this);
		if ((String) getHttpSession().getAttribute("profil") != null) {
			user=(Utilisateur) session.getAttribute("utilisateur");
			if(user.getAutorite()!=null)
				autorite=": "+user.getAutorite().getDenomination();
			monMenu.setProfil((String) getHttpSession().getAttribute("profil"));
			mdp.setLabel(Labels.getLabel("kermel.welcome") +" "+ user.getPrenom()+" "+user.getNom()+ ","+Labels.getLabel("kermel.text.welcome") +" "+ user.getSysProfil().getPfLibelle()+autorite);
			monMenu.afterCompose();
		} else {
			Executions.sendRedirect("/index.zul");
		}
	}

}