package sn.ssi.kermel.web.indsurvmulti.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygCatIndSurvMulti;
import sn.ssi.kermel.be.entity.SygIndSurvMulti;
import sn.ssi.kermel.be.session.IndSurvMultiSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

public class ListeController extends AbstractWindow implements EventListener, AfterCompose, ListitemRenderer{
	
	private Listbox list,lstCulture;
	public Textbox txtCode,txtLibelle,txtRechercherCulture;
	public Bandbox bdCulture;
    String code, libelle, description,cultureLibelle,page;
    private SygCatIndSurvMulti culture;
	private Paging pg;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	public static final String WINDOW_PARAM_MODE = "MODE";
//	private GrhSousMenu monSousMenu;
//	private Menuitem ADD_UNITES, MOD_UNITES, DEL_UNITES;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		list.setItemRenderer(this);

		pg.setPageSize(byPage);
		
		pg.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 List<SygIndSurvMulti> intrant = BeanLocator.defaultLookup(IndSurvMultiSession.class).find(pg.getActivePage()*byPage,byPage, code, libelle, culture);
			 SimpleListModel listModel = new SimpleListModel(intrant);
			list.setModel(listModel);
			pg.setTotalSize(BeanLocator.defaultLookup(IndSurvMultiSession.class).count(code, libelle, culture));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/indsurvmulti/form.zul";

		
			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("ajout"));
			display.put(DSP_HEIGHT,"50%");
			display.put(DSP_WIDTH, "50%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormController.WINDOW_PARAM_MODE,UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		}
		
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				throw new WrongValueException(list, Labels.getLabel("alert.selection"));
				
			}
			final String uri = "/indsurvmulti/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"50%");
			display.put(DSP_WIDTH, "50%");
			display.put(DSP_TITLE, Labels.getLabel("modification"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormController.WINDOW_PARAM_MODE,UIConstants.MODE_EDIT);
			data.put(FormController.PARAM_WIDOW_CODE, list.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
			if (list.getSelectedItem() == null)
				throw new WrongValueException(list, Labels.getLabel("alert.selection"));
			HashMap<String, String> display = new HashMap<String, String>();
			// permet de fixer les dimensions du popup
			
			display.put(MessageBoxController.DSP_MESSAGE, Labels.getLabel("alert.suppression"));
			display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("suppression"));
			display.put(MessageBoxController.DSP_HEIGHT, "250px");
			display.put(MessageBoxController.DSP_WIDTH, "47%");

			HashMap<String, Object> map = new HashMap<String, Object>();
			// permet de passer des parametres au popup
			
			map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
			showMessageBox(display, map);
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){		
			for (int i = 0; i < list.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
				System.out.println(codes);
				BeanLocator.defaultLookup(IndSurvMultiSession.class).delete(codes);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
	}
	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygIndSurvMulti intrant = (SygIndSurvMulti) data;
		item.setValue(intrant.getId());

		if(intrant.getCode()!=null) {
			Listcell cellCode = new Listcell(intrant.getCode());
			cellCode.setParent(item);
		}
		else {
			Listcell cellCode = new Listcell("");
			cellCode.setParent(item);
		}
		
		if(intrant.getLibelle() != null) {
			Listcell cellLibelle = new Listcell(intrant.getLibelle());
			cellLibelle.setParent(item);
		}
		else {
			Listcell cellLibelle = new Listcell("");
			cellLibelle.setParent(item);
		}
		
		if(intrant.getSignacation() != null) {
			Listcell cellDescription = new Listcell(intrant.getSignacation());
			cellDescription.setParent(item);
		}
		else {
			Listcell cellDescription = new Listcell("");
			cellDescription.setParent(item);
		}
		
		if(intrant.getCategorie().getLibelle()!=null) {
			Listcell cellCulture = new Listcell(intrant.getCategorie().getLibelle());
			cellCulture.setParent(item);
		}
		else {
			Listcell cellCulture = new Listcell("");
			cellCulture.setParent(item);
		}
		
	}
	
	//Culture
		public void onSelect$lstCulture(){
			 culture = (SygCatIndSurvMulti) lstCulture.getSelectedItem().getValue();
			 bdCulture.setValue(culture.getLibelle());
			
			 bdCulture.close();
			 Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION1, this, null);
		}
		
		public class CultureRenderer implements ListitemRenderer {

			public void render(Listitem item, Object data, int index)  throws Exception {
				SygCatIndSurvMulti type = (SygCatIndSurvMulti) data;
				item.setValue(type);
				
				Listcell cellLibelle = new Listcell(type.getLibelle());
				cellLibelle.setParent(item);

			}
		}
		
		
		public void onFocus$txtRechercherCulture(){
			 if(txtRechercherCulture.getValue().equalsIgnoreCase(Labels.getLabel("rechercher"))){
				 txtRechercherCulture.setValue("");
			 }		 
		}
		
		public void  onClick$btnRechercherCulture(){
			if(txtRechercherCulture.getValue().equalsIgnoreCase(Labels.getLabel("rechercher")))
			{
				cultureLibelle = null;
				page = null;
			}
			else
			{
				cultureLibelle = txtRechercherCulture.getValue();
				page="0";
			}
				
			Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
		}
		
		
		
	public void onClick$bchercher()
	{
		onOK();
	}
	
	public void onOK()
	{
		code = txtCode.getValue();
		libelle = txtLibelle.getValue();
		cultureLibelle = bdCulture.getValue();
		
		if(!code.equalsIgnoreCase(Labels.getLabel("code")))
		{
			if(libelle.equalsIgnoreCase(Labels.getLabel("libelle")))
				 libelle="";
		    Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
		
		if(!libelle.equalsIgnoreCase(Labels.getLabel("libelle")))
		{
			if(code.equalsIgnoreCase(Labels.getLabel("code")))
				 code="";
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
		
		if(code.equalsIgnoreCase(Labels.getLabel("code")) && libelle.equalsIgnoreCase(Labels.getLabel("libelle")))
		{
			code=""; 
			libelle="";
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}	
		
		if(cultureLibelle.equalsIgnoreCase(Labels.getLabel("categorie")) || "".equals(cultureLibelle)) {
			culture = null;
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
	}
	
	public void onFocus$txtCode()
	{
		if(txtCode.getValue().equalsIgnoreCase("code"))
			txtCode.setValue("");
		else
			code=txtCode.getValue();
	}
	public void onFocus$txtLibelle()
	{
		if(txtLibelle.getValue().equalsIgnoreCase("libelle"))
			txtLibelle.setValue("");
		else
			libelle=txtLibelle.getValue();
	}
	public void onBlur$txtCode()
	{
		if(txtCode.getValue().equals(""))
			txtCode.setValue("code");
	}
	public void onBlur$txtLibelle()
	{
		if(txtLibelle.getValue().equals(""))
			txtLibelle.setValue("libelle");
	}


}
