package sn.ssi.kermel.web.indsurvmulti.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygCatIndSurvMulti;
import sn.ssi.kermel.be.entity.SygIndSurvMulti;
import sn.ssi.kermel.be.session.CatIndSurvMultiSession;
import sn.ssi.kermel.be.session.IndSurvMultiSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class FormController extends AbstractWindow implements EventListener, AfterCompose {
	
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode ;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle, txtCode, txtDescription;
	Long code;
	private	SygIndSurvMulti varietes = new SygIndSurvMulti();
	
	private Bandbox bdCulture;
	private Textbox txtRechercherCulture;
	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private Paging pgCulture;
	private Listbox lstCulture;
	private int activePage;
	private SygCatIndSurvMulti culture = new SygCatIndSurvMulti();
	private String CultureLibelle;
	private String page = null;
	
	List<SygIndSurvMulti> var = new ArrayList<SygIndSurvMulti>();

	@Override
	public void onEvent(Event event) throws Exception {
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
				pgCulture.setPageSize(byPageBandBox);
			} 
			else {
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgCulture.getActivePage() * byPageBandBox;
				pgCulture.setPageSize(byPageBandBox);
			}
			List<SygCatIndSurvMulti> culture = BeanLocator.defaultLookup(CatIndSurvMultiSession.class).find(activePage, byPageBandBox, null, CultureLibelle);
			lstCulture.setModel(new SimpleListModel(culture));

			pgCulture.setTotalSize(BeanLocator.defaultLookup(CatIndSurvMultiSession.class).count( null, CultureLibelle));
		}
	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
   		lstCulture.setItemRenderer(new CultureRenderer());
   		pgCulture.setPageSize(byPageBandBox);
   		pgCulture.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTRE_ACTION);
   		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);	
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WIDOW_CODE);
			varietes = BeanLocator.defaultLookup(IndSurvMultiSession.class).findByCode(code);
			
			txtCode.setValue(varietes.getCode());
			txtLibelle.setValue(varietes.getLibelle());
			txtDescription.setValue(varietes.getSignacation());
			bdCulture.setValue(varietes.getCategorie().getLibelle());
			culture = varietes.getCategorie();
		}
	}
	
	public void onOK() {
		
		if(checkFieldConstraints()) {
			varietes.setCode(txtCode.getValue());
			varietes.setLibelle(txtLibelle.getValue());  
			varietes.setSignacation(txtDescription.getValue());
			varietes.setCategorie(culture);
	
			if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(IndSurvMultiSession.class).save(varietes);
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(IndSurvMultiSession.class).update(varietes);
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();

		}	
	}
	
private boolean checkFieldConstraints() {
		
		var=BeanLocator.defaultLookup(IndSurvMultiSession.class).find(0, 0, txtCode.getValue(), null, null);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			if(!varietes.getCode().equalsIgnoreCase(txtCode.getValue())) {
				if(var.size()>0) {
					throw new WrongValueException (txtCode, "Ce code existe déja !");
				}
			}
		}
		
		else {
			if(var.size()>0) {
				throw new WrongValueException (txtCode, "Ce code existe déja !");
			 }
		}
		return true;
		
	}


	//Culture
	public void onSelect$lstCulture(){
		 culture = (SygCatIndSurvMulti) lstCulture.getSelectedItem().getValue();
		 bdCulture.setValue(culture.getLibelle());
		
		 bdCulture.close();
	}
	
	public class CultureRenderer implements ListitemRenderer {
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygCatIndSurvMulti culture = (SygCatIndSurvMulti) data;
			item.setValue(culture);
			
			Listcell cellLibelle = new Listcell(culture.getLibelle());
			cellLibelle.setParent(item);

		}
	}
	
	
	public void onFocus$txtRechercherCulture(){
		 if(txtRechercherCulture.getValue().equalsIgnoreCase("rechercher")){
			 txtRechercherCulture.setValue("");
		 }		 
	}
	
	public void  onClick$btnRechercherCulture(){
		if(txtRechercherCulture.getValue().equalsIgnoreCase("rechercher")) {
			CultureLibelle = null;
			page = null;
		}
		else
		{
			CultureLibelle = txtRechercherCulture.getValue();
			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}


}
