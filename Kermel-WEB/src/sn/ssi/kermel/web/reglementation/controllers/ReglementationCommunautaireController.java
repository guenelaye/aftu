package sn.ssi.kermel.web.reglementation.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;

import sn.ssi.kermel.be.entity.SygContentieux;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class ReglementationCommunautaireController extends AbstractWindow implements AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String PARAM_WINDOW_CODE = "CODE";
	

	List<SygContentieux> contentieuxs = new ArrayList<SygContentieux>();
	UtilVue utilVue = UtilVue.getInstance();
	
	Session session = getHttpSession();
	//private DossierMenu monDossierMenu;
	//private Tabpanel tabConteneurs;
	private Include incDIRECTIVES;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);



	}

	public void onCreate(CreateEvent createEvent) {


		
		incDIRECTIVES.setSrc("/reglementation/communautaire/dossiers/directives.zul");
	}

	@Override
	public void onEvent(Event event) throws Exception {

	}

}