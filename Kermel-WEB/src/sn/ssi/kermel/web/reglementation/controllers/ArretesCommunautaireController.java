package sn.ssi.kermel.web.reglementation.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygReglementation;
import sn.ssi.kermel.be.reglementation.ejb.ReglementationSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;


@SuppressWarnings("serial")
public class ArretesCommunautaireController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null;
    private Listheader lshLibelle;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_REGNAT, MOD_REGNAT, SUPP_REGNAT;
    private String login;
    private Div step0, step1;

  
	
	private SygReglementation reglementation = new SygReglementation();
	private Textbox txtLibelles;
	
	
	UtilVue utilVue = UtilVue.getInstance();

	private Component errorComponent;
	private String errorMsg;
	 private Label lbStatusBar;
	 private static final String ERROR_MSG_STYLE = "color:red";
	 
	
		private Long idreglementation=null;
		
	
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_REGNAT);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_REGNAT != null) { ADD_REGNAT.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_REGNAT != null) { MOD_REGNAT.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUPP_REGNAT != null) { SUPP_REGNAT.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		lshLibelle.setSortAscending(new FieldComparator("libelle", false));
		lshLibelle.setSortDescending(new FieldComparator("libelle", true));
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	login = ((String) getHttpSession().getAttribute("user"));
    	
    	///
    	
    	
	}
	
	
	
	public void onCreate(CreateEvent event) {
//		codecontentieux = (Long) session.getAttribute("numcontentieux");
//		session.setAttribute("numcontentieux", codecontentieux);
//		contentieux = BeanLocator.defaultLookup(ContentieuxSession.class).findById(codecontentieux);
	}

	
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygReglementation> reglementations = BeanLocator.defaultLookup(ReglementationSession.class).findRech(activePage,byPage,UIConstants.REGLEMENTATION_COMMUNAUT,UIConstants.REG_ARRETE,libelle);
			 SimpleListModel listModel = new SimpleListModel(reglementations);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup(ReglementationSession.class).countRech(UIConstants.REGLEMENTATION_COMMUNAUT,UIConstants.REG_ARRETE,libelle));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {

			step0.setVisible(false);
			step1.setVisible(true);
			
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			reglementation = (SygReglementation) lstListe.getSelectedItem().getValue();
			idreglementation=reglementation.getId();
			txtLibelles.setValue(reglementation.getLibelle());
			
			
			step0.setVisible(false);
			step1.setVisible(true);
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
				
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes =( (SygReglementation)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue()).getId();
				BeanLocator.defaultLookup(ReglementationSession.class).delete(codes);
				BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_REGNAT", Labels.getLabel("kermel.referentiel.common.reglementation.suppression")+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
			
		
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygReglementation reglementation = (SygReglementation) data;
		item.setValue(reglementation);

			 
		 Listcell cellLibelle = new Listcell(reglementation.getLibelle());
		 cellLibelle.setParent(item);
		
	}
	public void onClick$bchercher()
	{
		
		if((txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.reglementation.libelle")))||(txtLibelle.getValue().equals("")))
		 {
			libelle=null;
			
		 }
		else
		{
			libelle=txtLibelle.getValue();
			page="0";
		}
		
		
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	
	public void onFocus$txtLibelle()
	{
		if(txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.reglementation.libelle")))
			txtLibelle.setValue("");
		
	}
	
	public void onBlur$txtLibelle()
	{
		if(txtLibelle.getValue().equals(""))
			txtLibelle.setValue(Labels.getLabel("kermel.reglementation.libelle"));
	}

	public void onOK$txtLibelle()
	{
		onClick$bchercher();
	}
	
	
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
	
	
	///
	


	
	
	
private boolean checkFieldConstraints() {
		
		try {
		
			if(txtLibelles.getValue().equals(""))
		     {
             errorComponent = txtLibelles;
             errorMsg = Labels.getLabel("kermel.reglementation.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

	public void onOK() {
		
		if(checkFieldConstraints())
		{
			reglementation.setLibelle(txtLibelles.getValue());
			reglementation.setNature(UIConstants.REGLEMENTATION_COMMUNAUT);
			reglementation.setType(UIConstants.REG_ARRETE);

		

			if (idreglementation==null) 
				BeanLocator.defaultLookup(ReglementationSession.class).save(reglementation);
            else 
            BeanLocator.defaultLookup(ReglementationSession.class).update(reglementation);
			
		
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			step0.setVisible(true);
			step1.setVisible(false);
			effacer();
		} 
	}

	public void effacer(){
		idreglementation=null;
		txtLibelles.setValue("");
		
	}
	
	public void onClick$menuFermer() {
		
		step0.setVisible(true);
		step1.setVisible(false);
	}
	
public void onClick$menuFermer2() {
		
		step0.setVisible(true);
		step1.setVisible(false);
		
	}

}

