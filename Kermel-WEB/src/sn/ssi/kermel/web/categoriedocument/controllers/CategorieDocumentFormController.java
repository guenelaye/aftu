package sn.ssi.kermel.web.categoriedocument.controllers;


import java.util.Map;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.categoriedocument.ejb.CategorieDocumentSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygCategorieDocument;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;




@SuppressWarnings("serial")
public class CategorieDocumentFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtniveauDoc,txtlibelleCatDoc,txtdescriptionDocument;
	private Intbox  txtnbTotalFichiersDoc,txtflagdernierniveauDoc;
	private SygCategorieDocument  document =new SygCategorieDocument ();
	private SimpleListModel lst;
	private final String ON_LOCALITE = "onLocalite";
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	Long code;

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WIDOW_CODE);
			document = BeanLocator.defaultLookup(CategorieDocumentSession.class)
					.findById(code);

			txtniveauDoc.setValue(document.getNiveauDoc());
			txtlibelleCatDoc.setValue(document.getLibelleCatDoc());
			txtdescriptionDocument.setValue(document.getDescriptionDocument());
			txtnbTotalFichiersDoc.setValue(document.getNbTotalFichiersDoc());
			txtflagdernierniveauDoc.setValue(document.getFlagdernierniveauDoc());
			
			}
			
		}
			
	
	public void onOK() {
		
		document.setNiveauDoc(txtniveauDoc.getValue());
		document.setLibelleCatDoc(txtlibelleCatDoc.getValue());
		document.setDescriptionDocument(txtdescriptionDocument.getValue());
		document.setNbTotalFichiersDoc(txtnbTotalFichiersDoc.getValue());
		document.setFlagdernierniveauDoc(txtflagdernierniveauDoc.getValue());
		
		
		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			BeanLocator.defaultLookup(CategorieDocumentSession.class).save(document);

		} 
		else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			BeanLocator.defaultLookup(CategorieDocumentSession.class).update(document);
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();

	}
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	
	}
	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onEvent(Event arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}
}