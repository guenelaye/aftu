package sn.ssi.kermel.web.dspaoo.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygMontantsSeuils;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsBailleursSession;
import sn.ssi.kermel.be.referentiel.ejb.BailleursSession;
import sn.ssi.kermel.be.referentiel.ejb.MontantsSeuilsSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class FinancementsController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstBailleur;
	private Paging pgPagination,pgBailleur;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference,libellebailleur=null;
    private int parent;
    private Label lbltitre;
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherBailleur,txtChapitre;
    SygBailleurs bailleur=null;
    private Decimalbox dcmontantprevu;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg,avalider="oui";
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idbailleur;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
    private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private Listcell cellValiderBailleur;
	SygDossiers dossier=new SygDossiers();
	List<SygMontantsSeuils> seuils = new ArrayList<SygMontantsSeuils>();
	List<SygRealisationsBailleurs> bailleurs = new ArrayList<SygRealisationsBailleurs>();
	private BigDecimal montantmarche=new BigDecimal(0),montantbailleurs=new BigDecimal(0),montantsaisie=new BigDecimal(0),
			montantdifference=new BigDecimal(0),montantamodifier=new BigDecimal(0),montanttotal=new BigDecimal(0);
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
    	
    	addEventListener(ApplicationEvents.ON_BAILLEURS, this);
		lstBailleur.setItemRenderer(new BailleursRenderer());
		pgBailleur.setPageSize(byPageBandbox);
		pgBailleur.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_BAILLEURS);
		
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		
		if(dossier==null)
		{
			cellValiderBailleur.setVisible(false);
		}
		else
		{
			montantmarche=dossier.getDosmontant();
			seuils = BeanLocator.defaultLookup(MontantsSeuilsSession.class).find(0,-1, autorite.getType(), appel.getTypemarche(), appel.getModepassation(), null, null,UIConstants.TYPE_SEUILSRAPRIORI);
			if(seuils.size()>0)
			{
				if(seuils.get(0).getMontantinferieur()!=null)
				{
					if((appel.getApomontantestime().compareTo(seuils.get(0).getMontantinferieur())==1)||(appel.getApomontantestime().equals(seuils.get(0).getMontantinferieur())))
			
					{
						avalider="oui";
						
					}
					else
					{
						avalider="non";
					}
						
				}
				
			}
			if(dossier.getDosDateMiseValidation()!=null||dossier.getDosDatePublication()!=null)
			{
				cellValiderBailleur.setVisible(false);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			Events.postEvent(ApplicationEvents.ON_BAILLEURS, this, null);
		}
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 bailleurs = BeanLocator.defaultLookup(RealisationsBailleursSession.class).find(activePage,byPage,null,realisation,null);
			 lstListe.setModel(new SimpleListModel(bailleurs));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(RealisationsBailleursSession.class).count(null,realisation,null));
			 montantbailleurs=new BigDecimal(0);
			 for(int i=0;i<bailleurs.size();i++)
			 {
				 montantbailleurs=montantbailleurs.add(bailleurs.get(i).getMontant());
			 }
			 
		} 
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_BAILLEURS)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgBailleur.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgBailleur.getActivePage() * byPageBandbox;
				pgBailleur.setPageSize(byPageBandbox);
			}
			List<SygBailleurs> bailleurs = BeanLocator.defaultLookup(BailleursSession.class).find(activePage, byPageBandbox,libellebailleur,autorite);
			lstBailleur.setModel(new SimpleListModel(bailleurs));
			pgBailleur.setTotalSize(BeanLocator.defaultLookup(BailleursSession.class).count(libellebailleur,autorite));
		}
		
			else 	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
				Listcell button = (Listcell) event.getTarget();
				String toDo = (String) button.getAttribute(UIConstants.TODO);
				sources = (SygRealisationsBailleurs) button.getAttribute("sources");
				if (toDo.equalsIgnoreCase("delete"))
				{
					HashMap<String, String> display = new HashMap<String, String>(); // permet
					display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
					display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
					display.put(MessageBoxController.DSP_HEIGHT, "250px");
					display.put(MessageBoxController.DSP_WIDTH, "47%");

					HashMap<String, Object> map = new HashMap<String, Object>(); // permet
					map.put(CURRENT_MODULE, sources);
					showMessageBox(display, map);
				}
				else 
				{
					if (toDo.equalsIgnoreCase("modifier")) {
						idbailleur=sources.getId();
						bailleur=sources.getBailleurs();
						bdBailleur.setValue(bailleur.getLibelle());
			   			dcmontantprevu.setValue(sources.getMontant());
			   			txtChapitre.setValue(sources.getChapitre());
						}
					
				}
				
			}
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				BeanLocator.defaultLookup(RealisationsBailleursSession.class).delete(sources.getId());
				session.setAttribute("libelle", "financements");
				loadApplicationState("suivi_dspaoo");
			}


	}

	public void onSelect$lstListe(){
		sources = (SygRealisationsBailleurs) lstListe.getSelectedItem().getValue();
		idbailleur=sources.getId();
		bailleur=sources.getBailleurs();
		bdBailleur.setValue(bailleur.getLibelle());
		dcmontantprevu.setValue(sources.getMontant());
		txtChapitre.setValue(sources.getChapitre());
		montantamodifier=sources.getMontant();
	}
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygRealisationsBailleurs bailleurs = (SygRealisationsBailleurs) data;
		item.setValue(bailleurs);

		
		 Listcell cellLibelle = new Listcell(bailleurs.getBailleurs().getLibelle());
		 cellLibelle.setParent(item);
		 
		 Listcell cellChapitre = new Listcell(bailleurs.getChapitre());
		 cellChapitre.setParent(item);
		 
		 
		 Listcell cellMontant = new Listcell("");
		 if(bailleurs.getMontant()!=null)
			 cellMontant.setLabel(ToolKermel.format2Decimal(bailleurs.getMontant()));
		 cellMontant.setParent(item);
		
		
//		 Listcell cellImageModif = new Listcell();
//		 cellImageModif.setImage("/images/disk.png");
//		 cellImageModif.setAttribute(UIConstants.TODO, "modifier");
//		 cellImageModif.setAttribute("sources", bailleurs);
//		 cellImageModif.setTooltiptext("Modifier Bailleur");
//		 cellImageModif.addEventListener(Events.ON_CLICK, FinancementsController.this);
	//	 cellImageModif.setParent(item);
		 
		 Listcell cellImageSupprime = new Listcell();
		
		 if(avalider.equals("oui"))
		 {
			 if(dossier.getDosDateMiseValidation()==null)
				{
				 cellImageSupprime.setImage("/images/delete.png");
				 cellImageSupprime.setAttribute(UIConstants.TODO, "delete");
				 cellImageSupprime.setAttribute("sources",bailleurs);
				 cellImageSupprime.setTooltiptext("Supprimer Bailleur");
				 cellImageSupprime.addEventListener(Events.ON_CLICK, FinancementsController.this);
				} 
		 }
		 else
		 {
			 if(dossier.getDosDatePublication()==null)
				{
				 cellImageSupprime.setImage("/images/delete.png");
				 cellImageSupprime.setAttribute(UIConstants.TODO, "delete");
				 cellImageSupprime.setAttribute("sources",bailleurs);
				 cellImageSupprime.setTooltiptext("Supprimer Bailleur");
				 cellImageSupprime.addEventListener(Events.ON_CLICK, FinancementsController.this);
				} 
		 }
		
		
		 cellImageSupprime.setParent(item);

	}
	public void onClick$bchercher()
	{
		
		
		if((txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.banque.libelle")))||(txtLibelle.getValue().equals("")))
		 {
			libelle=null;
			page=null;
		 }
		else
		{
			libelle=txtLibelle.getValue();
			page="0";
		}
		
		
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	
	public void onFocus$txtLibelle()
	{
		if(txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.banque.libelle")))
			txtLibelle.setValue("");
		
	}
	public void onBlur$txtLibelle()
	{
		if(txtLibelle.getValue().equals(""))
			txtLibelle.setValue(Labels.getLabel("kermel.referentiel.banque.libelle"));
	}
	
	public void onOK$txtLibelle()
	{
		onClick$bchercher();
	}
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
	
///////////Bailleurs///////// 
	public void onSelect$lstBailleur(){
	bailleur= (SygBailleurs) lstBailleur.getSelectedItem().getValue();
	bdBailleur.setValue(bailleur.getLibelle());
	bdBailleur.close();
	
	}
	
	public class BailleursRenderer implements ListitemRenderer{
	
	
	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygBailleurs bailleurs = (SygBailleurs) data;
		item.setValue(bailleurs);
		
		
		Listcell cellLibelle = new Listcell("");
		if (bailleurs.getLibelle()!=null){
			cellLibelle.setLabel(bailleurs.getLibelle());
		}
		cellLibelle.setParent(item);
	
	}
	}
	public void onFocus$txtRechercherBailleur(){
	if(txtRechercherBailleur.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.realisation.bailleur"))){
		txtRechercherBailleur.setValue("");
	}		 
	}
	
	public void  onClick$btnRechercherBailleur(){
	if(txtRechercherBailleur.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.realisation.bailleur")) || txtRechercherBailleur.getValue().equals("")){
		libellebailleur = null;
		page=null;
	}else{
		libellebailleur = txtRechercherBailleur.getValue();
		page="0";
	}
	Events.postEvent(ApplicationEvents.ON_BAILLEURS, this, page);
	}
	
	
	private boolean checkFieldConstraintsBailleurs() {
		
		try {
		
			if(bdBailleur.getValue().equals(""))
		     {
               errorComponent = bdBailleur;
               errorMsg = Labels.getLabel("kermel.referentiel.common.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			realisations= BeanLocator.defaultLookup(RealisationsBailleursSession.class).find(0,-1,null,realisation,bailleur);
			if (idbailleur!=null)
			{
				if(sources.getBailleurs().getId().intValue()!=bailleur.getId().intValue())
				{
					 if(realisations.size()>0)
				     {
		                errorComponent = bdBailleur;
						errorMsg =Labels.getLabel("kermel.plansdepassation.realisation.bailleur")+" "+ Labels.getLabel("kermel.referentiel.existe")+": "+bdBailleur.getValue();
						lbStatusBar.setStyle(ERROR_MSG_STYLE);
						lbStatusBar.setValue(errorMsg);
						throw new WrongValueException (errorComponent, errorMsg);
				    }
				}
			 
			}
			else
			{
				  if(realisations.size()>0)
				  {
					errorComponent = bdBailleur;
					errorMsg = Labels.getLabel("kermel.plansdepassation.realisation.bailleur")+" "+Labels.getLabel("kermel.referentiel.existe")+" :"+bdBailleur.getValue();
					lbStatusBar.setStyle(ERROR_MSG_STYLE);
					lbStatusBar.setValue(errorMsg);
					throw new WrongValueException (errorComponent, errorMsg);
				  }
			}
			if(txtChapitre.getValue().equals(""))
		     {
              errorComponent = txtChapitre;
              errorMsg = Labels.getLabel("kermel.plansdepassation.realisations.sourcesfinancement.chapitre")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dcmontantprevu.getValue()==null)
		     {
             errorComponent = dcmontantprevu;
             errorMsg = Labels.getLabel("kermel.plansdepassation.realisations.sourcesfinancement.montant")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(idbailleur==null)
   			{
				montanttotal=montantbailleurs.add(dcmontantprevu.getValue());
				if(montanttotal.compareTo(montantmarche)==1)
				{
					 errorComponent = dcmontantprevu;
		              errorMsg = Labels.getLabel("kermel.plansdepassation.realisations.sourcesfinancement.montant.somme")+": "+ToolKermel.format2Decimal(montantmarche);
					   lbStatusBar.setStyle(ERROR_MSG_STYLE);
					   lbStatusBar.setValue(errorMsg);
					  throw new WrongValueException (errorComponent, errorMsg);
				}
   			}
			else
			{
				montantsaisie=dcmontantprevu.getValue();
				if(montantsaisie.compareTo(montantamodifier)==1)
				{
					montantdifference=montantsaisie.subtract(montantamodifier);
					montanttotal=montantbailleurs.add(montantdifference);
					if(montanttotal.compareTo(montantmarche)==1)
					{
						 errorComponent = dcmontantprevu;
			              errorMsg = Labels.getLabel("kermel.plansdepassation.realisations.sourcesfinancement.montant.somme")+": "+ToolKermel.format2Decimal(montantmarche);
						   lbStatusBar.setStyle(ERROR_MSG_STYLE);
						   lbStatusBar.setValue(errorMsg);
						  throw new WrongValueException (errorComponent, errorMsg);
					}
				}
				
			}
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	
	public void  effacerform(){
		   dcmontantprevu.setValue(new BigDecimal("0"));
			txtChapitre.setValue("");
			idbailleur=null;
			bdBailleur.setValue("");
	}
	
	public void  onClick$cellValiderBailleur(){
		if(checkFieldConstraintsBailleurs())
		{
			 
   			sources.setBailleurs(bailleur);
   			sources.setRealisations(realisation);
   			sources.setMontant(dcmontantprevu.getValue());
   			sources.setChapitre(txtChapitre.getValue());
   			if(idbailleur==null)
   			{
   			 BeanLocator.defaultLookup(RealisationsBailleursSession.class).save(sources);
   		
   			}
   			else
   			{
   			 BeanLocator.defaultLookup(RealisationsBailleursSession.class).update(sources);
   		
   			}
   			session.setAttribute("libelle", "financements");
			loadApplicationState("suivi_dspaoo");
		}
		
	}
	
}