package sn.ssi.kermel.web.dspaoo.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Image;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabpanel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class OuverturesPlisController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code;
	private Tabpanel tabDetailsfecp;
	private Include incDetailsfecp;
	private Tab TAB_listepresencemembrescommissions,TAB_representantssoumissionnaires,TAB_representantsservicestechniques,TAB_observateursindependants,
	TAB_garantiesoumission,TAB_piecessoumissionnaires,TAB_compositioncommissiontechnique,TAB_lecturesoffres,TAB_procesverbalouverture;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label label;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private Image image;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygDossiers dossier=new SygDossiers();
	private int membrescommissions,representantsoummissionnaires,representantservicestechniques,observateursindependants,piecessoumissionnaires,
	compositioncommissiontechnique,lecturesoffres,garantiessoumissions,lots;
	private Label lblpresencemembrescommissions,lbllotssoumissionnaires,lblrepresentantssoumissionnaires,lblrepresentantsservicestechniques,
	lblobservateursindependants,lblgarantiesoumission,lblpiecessoumissionnaires,lbllecturesoffres,lblprocesverbalouverture,
	lblcompositioncommissiontechnique,lblincidents;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		
       idappel=(Long) session.getAttribute("idappel");
    	appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
    	dossier=(SygDossiers) Executions.getCurrent().getAttribute("dossier");
    	if(dossier!=null)
    	{
    		if((dossier.getDosNombreLots()>0)&&dossier.getDosLotDivisible().equals("OUI"))
    		{
    			image.setVisible(true);
	    		label.setVisible(true);
	    		lbllotssoumissionnaires.setVisible(true);
    		}
    		else
    		{
    			image.setVisible(false);
	    		label.setVisible(false);
	    		lbllotssoumissionnaires.setVisible(false);
    		}
	      	
    		membrescommissions=(Integer) Executions.getCurrent().getAttribute("membrescommissions");
    		representantsoummissionnaires=(Integer) Executions.getCurrent().getAttribute("representantsoummissionnaires");
    		representantservicestechniques=(Integer) Executions.getCurrent().getAttribute("representantservicestechniques");
    		observateursindependants=(Integer) Executions.getCurrent().getAttribute("observateursindependants");
    		piecessoumissionnaires=(Integer) Executions.getCurrent().getAttribute("piecessoumissionnaires");
    		compositioncommissiontechnique=(Integer) Executions.getCurrent().getAttribute("compositioncommissiontechnique");
    		lecturesoffres=(Integer) Executions.getCurrent().getAttribute("lecturesoffres");
    		garantiessoumissions=(Integer) Executions.getCurrent().getAttribute("garantiessoumissions");
    		lots=(Integer) Executions.getCurrent().getAttribute("lots");
    		
    		if(lots>0)
    			lbllotssoumissionnaires.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
    		
    		if(membrescommissions>0)
    			lblpresencemembrescommissions.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
    		
    		if(representantsoummissionnaires>0)
    			lblrepresentantssoumissionnaires.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
    		
    		if(representantservicestechniques>0)
    			lblrepresentantsservicestechniques.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
    		
    		if(observateursindependants>0)
    			lblobservateursindependants.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
    		
    		if(piecessoumissionnaires>0)
    			lblpiecessoumissionnaires.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
    		
    		if(lecturesoffres>0)
    			lbllecturesoffres.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
    		
    		if(garantiessoumissions>0)
    			lblgarantiesoumission.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
    		
    		if(appel.getApoDatepvouverturepli()!=null)
    			lblprocesverbalouverture.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
				
			if(dossier.getDosIncidents()!=null)
				lblincidents.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
			
			if(compositioncommissiontechnique>0)
    			lblcompositioncommissiontechnique.setStyle("color:#00FF00;font-family: sans-serif; font-size: 16px;cursor:pointer");
    	
    	}
	
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	public void onClick$lblpresencemembrescommissions() {
		session.setAttribute("libelle", "listepresencemembrescommissions");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$lblrepresentantssoumissionnaires() {
		session.setAttribute("libelle", "representantssoumissionnaires");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$lblrepresentantsservicestechniques() {
		session.setAttribute("libelle", "representantsservicestechniques");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$lblobservateursindependants() {
		session.setAttribute("libelle", "observateursindependants");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$lblgarantiesoumission() {
		session.setAttribute("libelle", "garantiesoumission");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$lblpiecessoumissionnaires() {
		session.setAttribute("libelle", "piecessoumissionnaires");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$lblcompositioncommissiontechnique() {
		session.setAttribute("libelle", "compositioncommissiontechnique");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$lbllecturesoffres() {
		session.setAttribute("libelle", "lecturesoffres");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$lblprocesverbalouverture() {
		session.setAttribute("libelle", "procesverbalouverture");
		loadApplicationState("suivi_dspaoo");
	}
	
	public void onClick$lblincidents() {
		session.setAttribute("libelle", "incidents");
		loadApplicationState("suivi_dspaoo");
	}
	
	public void onClick$lbllotssoumissionnaires() {
		session.setAttribute("libelle", "lotssoumissionnaires");
		loadApplicationState("suivi_dspaoo");
	}
}