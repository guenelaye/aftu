package sn.ssi.kermel.web.dspaoo.controllers;

import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class ExecutionDossiersController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CONFIRMVALIDER="CONFIRMVALIDER";
     String page=null;
     Session session = getHttpSession();
    SygDossiers dossier=new SygDossiers();
     String login;
    private Listbox lstListe;
     private Combobox cbselect;
 	private int etat=-1,gestion=-1;
	private Intbox intgestion;
	 private KermelSousMenu monSousMenu;
	private Menuitem ORDRESERVICEDEMARRAGEDAO,RECEPTIONPROVISOIREDAO,WRECEPTIONDEFINITIVEDAO,WSUIVIDAO;
	 
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.PARAM_EXECUTIONDAO);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
//		if (ORDRESERVICEDEMARRAGEDAO != null) { ORDRESERVICEDEMARRAGEDAO.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ORDRESERVICEDEMARRAGEDAO); }
//		if (RECEPTIONPROVISOIREDAO != null) { RECEPTIONPROVISOIREDAO.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_RECEPTIONPROVISOIREDAO); }
//		if (WRECEPTIONDEFINITIVEDAO != null) { WRECEPTIONDEFINITIVEDAO.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_WRECEPTIONDEFINITIVEDAO); }
		if (WSUIVIDAO != null) { WSUIVIDAO.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_SUIVI); }
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		addEventListener(ApplicationEvents.ON_SUIVI, this);
//		addEventListener(ApplicationEvents.ON_ORDRESERVICEDEMARRAGEDAO, this);
//		addEventListener(ApplicationEvents.ON_RECEPTIONPROVISOIREDAO, this);
//		addEventListener(ApplicationEvents.ON_WRECEPTIONDEFINITIVEDAO, this);
		
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		
		lstListe.setItemRenderer(this);
		
			
	}


	public void onCreate(CreateEvent event) {
		
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygDossiers> dossiers = BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).find(activePage,byPage, null, null, null);
			 SimpleListModel listModel = new SimpleListModel(dossiers);
			 lstListe.setModel(listModel);
			 pgPagination.setTotalSize(BeanLocator.defaultLookup( DossiersAppelsOffresSession.class).count(null, null, null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_SUIVI)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			dossier=(SygDossiers) lstListe .getSelectedItem().getValue();
			session.setAttribute("iddossier", dossier.getDosID());
			loadApplicationState("suivi_dao");
		
		} 
//		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ORDRESERVICEDEMARRAGEDAO)) {
//			if (lstListe.getSelectedItem() == null)
//				
//				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
//			final String uri = "/passationsmarches/dspaoo/formordredeservicededemarrage.zul";
//
//			final HashMap<String, String> display = new HashMap<String, String>();
//			display.put(DSP_HEIGHT,"500px");
//			display.put(DSP_WIDTH, "80%");
//			display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.proceduresmarches.execution.ordreserdemarrage"));
//
//			final HashMap<String, Object> data = new HashMap<String, Object>();
//			data.put(OrdredeservicededemarrageController.PARAM_WINDOW_CODE, dossier.getAppel().getApoid());
//
//			showPopupWindow(uri, data, display);
//		} 
//		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_RECEPTIONPROVISOIREDAO)) {
//			if (lstListe.getSelectedItem() == null)
//				
//				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
//			final String uri = "/passationsmarches/dspaoo/formreceptionprovisoire.zul";
//
//			final HashMap<String, String> display = new HashMap<String, String>();
//			display.put(DSP_HEIGHT,"500px");
//			display.put(DSP_WIDTH, "80%");
//			display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.proceduresmarches.execution.receptionprovisoire.saisie"));
//
//			final HashMap<String, Object> data = new HashMap<String, Object>();
//			data.put(ReceptionProvisoireController.PARAM_WINDOW_CODE, dossier.getAppel().getApoid());
//
//			showPopupWindow(uri, data, display);
//		} 
//		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_WRECEPTIONDEFINITIVEDAO)) {
//			if (lstListe.getSelectedItem() == null)
//				
//				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
//			final String uri = "/passationsmarches/dspaoo/formreceptiondefinitive.zul";
//
//			final HashMap<String, String> display = new HashMap<String, String>();
//			display.put(DSP_HEIGHT,"500px");
//			display.put(DSP_WIDTH, "80%");
//			display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.proceduresmarches.execution.receptiondefinitive.saisie"));
//
//			final HashMap<String, Object> data = new HashMap<String, Object>();
//			data.put(ReceptionDefinitiveController.PARAM_WINDOW_CODE, dossier.getAppel().getApoid());
//
//			showPopupWindow(uri, data, display);
//		} 
	}

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygDossiers dossiers = (SygDossiers) data;
		item.setValue(dossiers);
		
		Listcell cellAutorite = new Listcell(dossiers.getAutorite().getDenomination());
		cellAutorite.setParent(item);
		 
		Listcell cellReference = new Listcell(dossiers.getAppel().getAporeference());
		cellReference.setParent(item);
		 
		Listcell cellObjet = new Listcell(dossiers.getAppel().getApoobjet());
		cellObjet.setParent(item);
		 
		Listcell cellModepassation = new Listcell(dossiers.getAppel().getModepassation().getLibelle());
		cellModepassation.setParent(item);
		 
		
		Listcell cellMontant = new Listcell(ToolKermel.format2Decimal(dossiers.getAppel().getApomontantestime()));
		cellMontant.setParent(item);

	
	
	}
	public void onSelect$lstListe_(){
		dossier=(SygDossiers) lstListe .getSelectedItem().getValue();
		if(dossier.getDosDateOrdreDemarrage()==null)
		{
			if(RECEPTIONPROVISOIREDAO!=null)
				RECEPTIONPROVISOIREDAO.setDisabled(true);
			if(WRECEPTIONDEFINITIVEDAO!=null)
				WRECEPTIONDEFINITIVEDAO.setDisabled(true);
		}
		else
		{
			RECEPTIONPROVISOIREDAO.setDisabled(false);
			WRECEPTIONDEFINITIVEDAO.setDisabled(false);
			if(dossier.getDosDateRecepProvisoire()==null)
			{
				if(WRECEPTIONDEFINITIVEDAO!=null)
					WRECEPTIONDEFINITIVEDAO.setDisabled(true);
			}
		}
	}


	
	public void  onClick$btnchercher(){
		
		if(intgestion.getValue()==null)
		 {
			gestion=-1;
			
		 }
		else
		 {
			gestion=intgestion.getValue();
			page="0";
		 }
		
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
	

		public void onOK$txtReference(){
			onClick$btnchercher();	
		}
		public void onOK$bchercher(){
			onClick$btnchercher();	
		}
}