package sn.ssi.kermel.web.dspaoo.controllers;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.Treerow;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAttributions;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygCriteresQualificationsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygDevise;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossiersEvaluateurs;
import sn.ssi.kermel.be.entity.SygDossierscommissionsmarches;
import sn.ssi.kermel.be.entity.SygDossierspieces;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygGarantiesDossiers;
import sn.ssi.kermel.be.entity.SygGarantiesSoumissionnaires;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygLotsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygMontantsSeuils;
import sn.ssi.kermel.be.entity.SygObservateursIndependants;
import sn.ssi.kermel.be.entity.SygPiecesplisouvertures;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygPresenceouverture;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygRepresentantsServicesTechniques;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.AttributionsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.ContratsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.CriteresQualificationsSoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DeviseSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DocumentsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersEvaluateursSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersPiecesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersSouscriteresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossierscommissionsmarchesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.GarantiesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.GarantisSoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.LotsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.LotsSoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.ObservateursIndependantsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.PiecessoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.Presence0uvertureSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrederetraitdaoSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RepresentantsServicesTechniquesSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsBailleursSession;
import sn.ssi.kermel.be.referentiel.ejb.MontantsSeuilsSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



/**

 * @author Adama samb
 * @since mardi 11 Janvier 2011, 12:34
 
 */
public class SuiviProcedureMarcheFormController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private Include pgActes;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	Session session = getHttpSession();
	private Label lblObjets,lblModePassation,lblMontant,lblModeSelection,lblType,lgtitre,lblDatecreation;
	private String LibelleTab,libelle,avalider="oui";
	private Tab TAB_TDOSSIERS;
	private Tree tree;
	private Treecell financement,infosgenerales,celldossiers,cellpreparation,celltdossiers,validationdossier,publicationdossier,plisouvertures,cellouvertureplis,
	registreretraitdao,ouverturesplis,approbation,souscriptiondumarche,bonsengagement,approbationmarche,notificationmarche,publicationattributiondefinitive,
	attributionprovisoire,publicationattributionprovisoire,soumissionpourvalidationattributionprovisoire,evaattrprovisoire,evaluationsoffre,
	listepresencemembrescommissions	,cellcompositioncommissiontechnique,cellpiecessoumissionnaires,cellobservateursindependants,representantsservicestechniques,representantssoumissionnaires
	,celllecturesoffres,cellincidents,procesverbalouverture,celltransmissiondossier,cellcontrolegarantie,cellverificationconformite,cellcorrectionoffre,
	cellvercriteresqualification,cellclassementfinal,cellrapportevaluation,celllotssoummissionnaires,cellgarantiesoumission,cellinfosgeneral,cellallotissement,cellgaranties,
	cellpieces,cellcriteres,celldevise,cellfinancement,celldemandepublicationdossier,cellimmatriculation;
	private Include idinclude;
	private Treeitem iteminfosgenerales,itemdossiers,itemvalidationdossier,itempublicationdossier,itemregistreretraitdao,itemplisouvertures,evaluationsoffres,treelotssoummissionnaires,
	itemattributionprovisoire,itemsoumissionpourvalidationattributionprovisoire,itempublicationattributionprovisoire,itemsouscriptiondumarche,itembonsengagement,
	itemapprobationmarche,itemnotificationmarche,itempublicationattributiondefinitive,itemouvertureplis,treepreparation,treetdossiers,treeouvertureplis,treeapprobation,
	 treeevaluation,itemlistepresencemembrescommissions,itemrepresentantssoumissionnaires,itemrepresentantsservicestechniques,itemobservateursindependants,itemgarantiesoumission
	 ,itempiecessoumissionnaires,itemcompositioncommissiontechnique,itemlecturesoffres,itemprocesverbalouverture,itemincidents,transmissiondossier,controlegarantie,
	 verificationconformite,correctionoffre,vercriteresqualification,classementfinal,rapportevaluation,treeinfosgeneral,treeallotissement,treegaranties,
	 treepieces,treecriteres,treedevise,treefinancement,treedemandepublicationdossier,itemimmatriculation,treepresentationoffres;
	private Treerow rowinfosgenerales;
	private Treeitem treeitem;
	List<SygGarantiesDossiers> garanties = new ArrayList<SygGarantiesDossiers>();
	List<SygDossierspieces> piecesadministratives = new ArrayList<SygDossierspieces>();
	List<SygDossierssouscriteres> criteresqualifications = new ArrayList<SygDossierssouscriteres>();
	List<SygDevise> devises = new ArrayList<SygDevise>();
	List<SygRealisationsBailleurs> bailleurs = new ArrayList<SygRealisationsBailleurs>();
	List<SygRetraitregistredao> registreretrait = new ArrayList<SygRetraitregistredao>();
	List<SygPlisouvertures> registredepot = new ArrayList<SygPlisouvertures>();
	List<SygDossierscommissionsmarches> membrescommissions = new ArrayList<SygDossierscommissionsmarches>();
	List<SygPresenceouverture> representantsoummissionnaires = new ArrayList<SygPresenceouverture>();
	List<SygRepresentantsServicesTechniques> representantservicestechniques = new ArrayList<SygRepresentantsServicesTechniques>();
	List<SygObservateursIndependants> observateursindependants = new ArrayList<SygObservateursIndependants>();
	List<SygPiecesplisouvertures> piecessoumissionnaires = new ArrayList<SygPiecesplisouvertures>();
	List<SygDossiersEvaluateurs> compositioncommissiontechnique = new ArrayList<SygDossiersEvaluateurs>();
	List<SygLotsSoumissionnaires> lecturesoffres = new ArrayList<SygLotsSoumissionnaires>();
	List<SygMontantsSeuils> seuils = new ArrayList<SygMontantsSeuils>();
    SygAutoriteContractante autorite=new SygAutoriteContractante();
    SygPlisouvertures soumissionnaires=null;
    List<SygDocuments> documents = new ArrayList<SygDocuments>();
	SygDossiers dossier=new SygDossiers();
	private int nbredossierap=0,nbreouvertureplis=0,nbredossieraps=0,nbreouverturepliss=0,nbreevaluationplis=0,nbreevaluationpliss=0,
			nbreapprobation=0,nombreimmatriculation=0,nbrecriteres=0;
	List<SygLots> lots = new ArrayList<SygLots>();
	List<SygAttributions> attributaires = new ArrayList<SygAttributions>();
	List<SygLotsSoumissionnaires> lotssoumissionnaires = new ArrayList<SygLotsSoumissionnaires>();
	List<SygGarantiesSoumissionnaires> garantiessoumissions = new ArrayList<SygGarantiesSoumissionnaires>();
	List<SygCriteresQualificationsSoumissionnaires> criteresqualificationssoum = new ArrayList<SygCriteresQualificationsSoumissionnaires>();
	private BigDecimal montantbailleurs=new BigDecimal(0);
	List<SygMontantsSeuils> seuilscommunautaires = new ArrayList<SygMontantsSeuils>();
	SygContrats contrat=new SygContrats();
	List<SygPlisouvertures> registredepotverifies = new ArrayList<SygPlisouvertures>();
	 
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		


	}

	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		LibelleTab=(String) session.getAttribute("libelle");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		
		lblObjets.setValue(appel.getApoobjet());
		lblModePassation.setValue(appel.getModepassation().getLibelle());
		lblMontant.setValue(ToolKermel.format2Decimal(appel.getApomontantestime()));
		lblModeSelection.setValue(appel.getModeselection().getLibelle());
		lblType.setValue(appel.getTypemarche().getLibelle());
		
		lblDatecreation.setValue(UtilVue.getInstance().formateLaDate2(appel.getApodatecreation()));
		
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		
		seuils = BeanLocator.defaultLookup(MontantsSeuilsSession.class).find(0,-1, autorite.getType(), appel.getTypemarche(), appel.getModepassation(), null, null,UIConstants.TYPE_SEUILSRAPRIORI);
		seuilscommunautaires = BeanLocator.defaultLookup(MontantsSeuilsSession.class).find(0,-1, autorite.getType(), appel.getTypemarche(), appel.getModepassation(), null, null,UIConstants.TYPE_SEUILSCOMMUNAUTAIRE);
		
		if(seuils.size()>0)
		{
			if(seuils.get(0).getMontantinferieur()!=null)
			{
				if((appel.getApomontantestime().compareTo(seuils.get(0).getMontantinferieur())==1)||(appel.getApomontantestime().equals(seuils.get(0).getMontantinferieur())))
		
				{
					avalider="oui";
					itemvalidationdossier.setVisible(true);
				}
				else
				{
					avalider="non";
					itemvalidationdossier.setVisible(false);
				}
					
			}
			
		}
		if(seuilscommunautaires.size()>0)
		{
			if(seuilscommunautaires.get(0).getMontantinferieur()!=null)
			{
				if((appel.getApomontantestime().compareTo(seuilscommunautaires.get(0).getMontantinferieur())==1)||(appel.getApomontantestime().equals(seuilscommunautaires.get(0).getMontantinferieur())))
		
				{
					treedemandepublicationdossier.setVisible(true);
				}
				else
				{
					treedemandepublicationdossier.setVisible(false);
				}
					
			}
			
		}
		////////////////Libelle/////////////////
		if(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis").length()>20)
		   ouverturesplis.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis").substring(0, 20)+"...");
		else
		   ouverturesplis.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis"));
		
		if(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.representantservicetechnique").length()>20)
		   representantsservicestechniques.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.representantservicetechnique").substring(0, 20)+"...");
		else
		   representantsservicestechniques.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.representantservicetechnique"));
		
		if(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.garantiesoumissions").length()>20)
			cellgarantiesoumission.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.garantiesoumissions").substring(0, 20)+"...");
		else
			cellgarantiesoumission.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.garantiesoumissions"));
		
		if(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire").length()>15)
			evaattrprovisoire.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire").substring(0, 15)+"...");
		else
			evaattrprovisoire.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire"));
	
		
		/////////////////////////////////////////
		if(dossier!=null)
		{
			celltdossiers.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			celltdossiers.setImage("/images/puce.png");
			cellpreparation.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			cellpreparation.setImage("/images/puce.png");
			celldossiers.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			celldossiers.setImage("/images/puce.png");
			cellinfosgeneral.setImage("/images/rdo_on.png");
			cellinfosgeneral.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
			/////////Dossiers AO///////
			
			garanties = BeanLocator.defaultLookup(GarantiesSession.class).find(0,-1,dossier,null,null);
			piecesadministratives = BeanLocator.defaultLookup(DossiersPiecesSession.class).find(0,-1,dossier);
			devises= BeanLocator.defaultLookup(DeviseSession.class).find(0,-1,dossier);
			bailleurs = BeanLocator.defaultLookup(RealisationsBailleursSession.class).find(0,-1,null,appel.getRealisation(),null);
			lots = BeanLocator.defaultLookup(LotsSession.class).find(0,-1,dossier,null);
		 	nbredossierap=0;nbredossieraps=0;
			if((dossier.getDosNombreLots()>0)&&dossier.getDosLotDivisible().equals("OUI"))
				treelotssoummissionnaires.setVisible(true);
			else
				treelotssoummissionnaires.setVisible(false);
			if(garanties.size()>0)
			{
				nbredossierap=nbredossierap+ 1;
				cellgaranties.setImage("/images/rdo_on.png");
				cellgaranties.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
			}
			if(piecesadministratives.size()>0)
			{
				nbredossierap=nbredossierap+ 1;
				cellpieces.setImage("/images/rdo_on.png");
				cellpieces.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
			}
			
			if(devises.size()>0)
			{
				nbredossierap=nbredossierap+ 1;
				celldevise.setImage("/images/rdo_on.png");
				celldevise.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
			}
			if(bailleurs.size()>0)
			{
				 montantbailleurs=new BigDecimal(0);
				 for(int i=0;i<bailleurs.size();i++)
				 {
					 montantbailleurs=montantbailleurs.add(bailleurs.get(i).getMontant());
				 }
				if( montantbailleurs.equals(dossier.getDosmontant()))
				 {
					nbredossierap=nbredossierap+ 1;
					cellfinancement.setImage("/images/rdo_on.png");
					cellfinancement.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px"); 
				 }
				
			}
			if(lots.size()>0&&lots.size()==dossier.getDosNombreLots())
			{
				nbredossierap=nbredossierap+ 1;
				cellallotissement.setImage("/images/rdo_on.png");
				cellallotissement.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				for(int i=0;i<lots.size();i++)
				{
					criteresqualifications = BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(0,-1,dossier, null, lots.get(i));
					if(criteresqualifications.size()>0)
					{
						nbrecriteres=nbrecriteres+1;
					}
				}
				if(nbrecriteres==lots.size())
				{
					nbredossierap=nbredossierap+ 1;
					cellcriteres.setImage("/images/rdo_on.png");
					cellcriteres.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				}
				
				
			}
			if(dossier.getDosDateMiseValidation()!=null)
			{
				nbredossieraps=nbredossieraps+ 1;
				validationdossier.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
			    validationdossier.setImage("/images/rdo_on.png");
				if(dossier.getDosDateValidation()==null)
				  validationdossier.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.enattentevalidation"));
				else
				 validationdossier.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validationdossier")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateValidation()));
			}
			if(dossier.getDosdatedemandepublication()!=null)
			{
				celldemandepublicationdossier.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				celldemandepublicationdossier.setImage("/images/rdo_on.png");
			}
			if(dossier.getDosDatePublication()!=null)
			{
				nbredossieraps=nbredossieraps+ 1;
				publicationdossier.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				publicationdossier.setImage("/images/rdo_on.png");
			    publicationdossier.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.publication.publie")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDatePublication()));

			}	
			if(nbredossierap>0)
			{
				
				cellpreparation.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				celldossiers.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				cellpreparation.setImage("/images/puce.png");
				celldossiers.setImage("/images/puce.png");
				if(nbredossierap==6)
				{
					celldossiers.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					celldossiers.setImage("/images/rdo_on.png");
					nbredossierap=nbredossierap+nbredossieraps;
					if(nbredossierap==8)
					{
						cellpreparation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
						cellpreparation.setImage("/images/rdo_on.png");
						registreretrait = BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).find(0,-1,dossier);
					}
						
				}
			
			}
			if(registreretrait.size()>0)
			{
				nbreouverturepliss=nbreouverturepliss+ 1;
				registreretraitdao.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				registreretraitdao.setImage("/images/rdo_on.png");
				registredepot= BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,-1,-1,-1, -1, -1, -1, null, -1, null, null);
				
			}
			if(registredepot.size()>0)
			{
				nbreouverturepliss=nbreouverturepliss+ 1;
				plisouvertures.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				plisouvertures.setImage("/images/rdo_on.png");
				membrescommissions = BeanLocator.defaultLookup(DossierscommissionsmarchesSession.class).find(0,-1,dossier,UIConstants.PARENT,-1);	
				representantsoummissionnaires = BeanLocator.defaultLookup(Presence0uvertureSession.class).find(0,-1,dossier,null,null,-1);
				representantservicestechniques = BeanLocator.defaultLookup(RepresentantsServicesTechniquesSession.class).find(0,-1,dossier,-1);
				observateursindependants = BeanLocator.defaultLookup(ObservateursIndependantsSession.class).find(0,-1,dossier,-1);
				piecessoumissionnaires   = BeanLocator.defaultLookup(PiecessoumissionnairesSession.class).find(0,-1,dossier,null,null,UIConstants.PIECEFOURNIE);
				compositioncommissiontechnique = BeanLocator.defaultLookup(DossiersEvaluateursSession.class).find(0,-1,dossier);
				lecturesoffres = BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).find(0, -1, dossier, null, null,-1,null,1,-1, -1, null);
				lotssoumissionnaires = BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).find(0, -1, dossier, null, null,-1,null,-1,-1, -1, null);
				garantiessoumissions=BeanLocator.defaultLookup(GarantisSoumissionnairesSession.class).find(0, -1, dossier, null, null,"oui");
				criteresqualificationssoum=BeanLocator.defaultLookup(CriteresQualificationsSoumissionnairesSession.class).find(0, -1, dossier, null, null,1, null, null);
		
			}
			if((dossier.getDosNombreLots()>0)&&dossier.getDosLotDivisible().equals("OUI"))
			{
				if(lotssoumissionnaires.size()>0)
				{
					celllotssoummissionnaires.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellouvertureplis.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					celllotssoummissionnaires.setImage("/images/rdo_on.png");
					cellouvertureplis.setImage("/images/puce.png");
					nbreouvertureplis=nbreouvertureplis+1;
				}	
			}
			if(membrescommissions.size()>0)	
			{
				nbreouvertureplis=nbreouvertureplis+ 1;
				listepresencemembrescommissions.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				listepresencemembrescommissions.setImage("/images/rdo_on.png");
			}
			if(representantsoummissionnaires.size()>0)	
			{
				nbreouvertureplis=nbreouvertureplis+ 1;
				representantssoumissionnaires.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				representantssoumissionnaires.setImage("/images/rdo_on.png");
			}
			if(representantservicestechniques.size()>0)	
			{
				nbreouvertureplis=nbreouvertureplis+ 1;
				representantsservicestechniques.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				representantsservicestechniques.setImage("/images/rdo_on.png");
			}
			if(observateursindependants.size()>0)
			{
				nbreouvertureplis=nbreouvertureplis+ 1;
				cellobservateursindependants.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellobservateursindependants.setImage("/images/rdo_on.png");
			}
			if(garantiessoumissions.size()>0)
			{
				nbreouvertureplis=nbreouvertureplis+ 1;
				cellgarantiesoumission.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellgarantiesoumission.setImage("/images/rdo_on.png");
			}
			if(piecessoumissionnaires.size()>0)	
			{
				nbreouvertureplis=nbreouvertureplis+ 1;
				cellpiecessoumissionnaires.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellpiecessoumissionnaires.setImage("/images/rdo_on.png");
			}
			if(lecturesoffres.size()>0)	
			{
				nbreouvertureplis=nbreouvertureplis+ 1;
				celllecturesoffres.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				celllecturesoffres.setImage("/images/rdo_on.png");
			}
			if(dossier.getDosIncidents()!=null)
			{
				 cellincidents.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				 cellincidents.setImage("/images/rdo_on.png");
			}
			if(appel.getApoDatepvouverturepli()!=null)
			{
				nbreouvertureplis=nbreouvertureplis+ 1;
				procesverbalouverture.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				procesverbalouverture.setImage("/images/rdo_on.png");
			}
			
			if(compositioncommissiontechnique.size()>0)	
			{
				nbreouvertureplis=nbreouvertureplis+ 1;
				cellcompositioncommissiontechnique.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellcompositioncommissiontechnique.setImage("/images/rdo_on.png");
			}
			if(nbreouvertureplis>0||nbreouverturepliss>0)
			{
				 ouverturesplis.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				 ouverturesplis.setImage("/images/puce.png");
			}
			if(nbreouvertureplis>0)
			{
			   
			    cellouvertureplis.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
			    cellouvertureplis.setImage("/images/puce.png");
                if((dossier.getDosNombreLots()==1)||dossier.getDosLotDivisible().equals("NON"))
                {
                	if(nbreouvertureplis==9)
                	{
                		cellouvertureplis.setImage("/images/rdo_on.png");
                		cellouvertureplis.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
                	}
    					
    				nbreouvertureplis=nbreouvertureplis+ nbreouverturepliss;
    				  if(nbreouvertureplis==11)
    				  {
    					  ouverturesplis.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
    					  ouverturesplis.setImage("/images/rdo_on.png");
    					  cellincidents.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
    					  cellincidents.setImage("/images/rdo_on.png");
    				  }
                }
                else
                {
                	if(nbreouvertureplis==10)
                	{
                		cellouvertureplis.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
                		cellouvertureplis.setImage("/images/rdo_on.png");
                	}
    					
    				nbreouvertureplis=nbreouvertureplis+ nbreouverturepliss;
    				  if(nbreouvertureplis==12)
    				  {
    					  ouverturesplis.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
    					  ouverturesplis.setImage("/images/rdo_on.png");
    					  cellincidents.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
    					  cellincidents.setImage("/images/rdo_on.png");
    				  }
                }
               
			}
			 if(appel.getApoDatepvouverturepli()!=null)
			  {
				  cellouvertureplis.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.plisouvertle"));
				  cellouvertureplis.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.plisouvertle")+" "+UtilVue.getInstance().formateLaDate2(appel.getApoDatepvouverturepli()));
				
			  }
           if(dossier.getDateRemiseDossierTechnique()!=null||dossier.getDateLimiteDossierTechnique()!=null)
			{
				nbreevaluationplis=nbreevaluationplis+ 1;
				celltransmissiondossier.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				celltransmissiondossier.setImage("/images/rdo_on.png");
				registredepotverifies = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,1,-1,-1, -1, -1, -1, null, -1, null, null);
				documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossier, appel,UIConstants.PARAM_TYPEDOCUMENTS_EVALUATION,null, null);
				soumissionnaires=BeanLocator.defaultLookup(RegistrededepotSession.class).findSoumissionnaire(dossier,UIConstants.PARENT,  UIConstants.MOINSDISANTQUALIFIE);
				
			
			}
	       	if(registredepotverifies.size()>0)
			{
				nbreevaluationplis=nbreevaluationplis+ 1;
				cellverificationconformite.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellverificationconformite.setImage("/images/rdo_on.png");
			}
	
           if(soumissionnaires!=null)
			{
			    nbreevaluationplis=nbreevaluationplis+ 1;
			    cellclassementfinal.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
			    cellclassementfinal.setImage("/images/rdo_on.png");
			}	
          
           if(criteresqualificationssoum.size()>0)
			{
				nbreevaluationplis=nbreevaluationplis+ 1;
				cellvercriteresqualification.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				cellvercriteresqualification.setImage("/images/rdo_on.png");
			}
           if(documents.size()>0)
			 {
				  nbreevaluationplis=nbreevaluationplis+ 1;
				  cellrapportevaluation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px"); 
				  cellrapportevaluation.setImage("/images/rdo_on.png");
			 }
         
			if(nbreevaluationplis>0)
			{
				evaluationsoffre.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				evaluationsoffre.setImage("/images/puce.png");
				if(nbreevaluationplis==5)
				{
					cellcorrectionoffre.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellcorrectionoffre.setImage("/images/rdo_on.png");
					evaluationsoffre.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					evaluationsoffre.setImage("/images/rdo_on.png");
					cellcontrolegarantie.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					cellcontrolegarantie.setImage("/images/rdo_on.png");
					attributaires=BeanLocator.defaultLookup(AttributionsSession.class).find(0, -1, dossier, null);
				}
					
			}
			if(attributaires.size()==dossier.getDosNombreLots())
			{
				nbreevaluationpliss=nbreevaluationpliss+ 1;
				attributionprovisoire.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				attributionprovisoire.setImage("/images/rdo_on.png");
				TreeTreeevaluation();
			}
			if(dossier.getDosDateMiseValidationattribution()!=null)
			{
				nbreevaluationpliss=nbreevaluationpliss+ 1;
				soumissionpourvalidationattributionprovisoire.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				soumissionpourvalidationattributionprovisoire.setImage("/images/rdo_on.png");
				if(dossier.getDosDateValidationPrequalif()==null)
					soumissionpourvalidationattributionprovisoire.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.enattentevalidation"));
					else
					{
						soumissionpourvalidationattributionprovisoire.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire.validele")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateValidationPrequalif()));
						soumissionpourvalidationattributionprovisoire.setTooltip(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire.valideles")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateValidationPrequalif()));
						
					}
				TreeTreeevaluation();
			}
			if(dossier.getDosDatePublicationProvisoire()!=null)
			{
				nbreevaluationpliss=nbreevaluationpliss+ 1;
				publicationattributionprovisoire.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				publicationattributionprovisoire.setImage("/images/rdo_on.png");
				publicationattributionprovisoire.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire.publiele")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDatePublicationProvisoire()));
				publicationattributionprovisoire.setTooltip(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire.publieles")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDatePublicationProvisoire()));
				
				TreeTreeevaluation();
			}
			  if(nbreevaluationplis+nbreevaluationpliss>0)
				{
					evaattrprovisoire.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
					evaattrprovisoire.setImage("/images/puce.png");
					if(nbreevaluationplis+nbreevaluationpliss==8)
					{
						evaattrprovisoire.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
						evaattrprovisoire.setImage("/images/rdo_on.png");
					}
						
				}
			  
//////////////Approbation du march�
			if(dossier.getDosDateSignature()!=null)
			{
				nbreapprobation=nbreapprobation+ 1;
				souscriptiondumarche.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				souscriptiondumarche.setImage("/images/rdo_on.png");
				souscriptiondumarche.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.souscritle")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateSignature()));
	
			}
			if(dossier.getDosDateMiseValidationSignature()!=null)
			{
				nbreapprobation=nbreapprobation+ 1;
				bonsengagement.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				bonsengagement.setImage("/images/rdo_on.png");
				if(dossier.getDosDateValidationSignature()==null)
					bonsengagement.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.examenjuririqueencoursvalidation"));
					else
						bonsengagement.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.examenjuririquevalidation")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateValidationSignature()));

			}
			if(dossier.getDosDateApprobation()!=null)
			{
				nbreapprobation=nbreapprobation+ 1;
				approbationmarche.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				approbationmarche.setImage("/images/rdo_on.png");
				approbationmarche.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.approbationmarche.approuvele")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateApprobation()));

			}
			if(dossier.getDosDateNotification()!=null)
			{
				nbreapprobation=nbreapprobation+ 1;
				notificationmarche.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				notificationmarche.setImage("/images/rdo_on.png");
				notificationmarche.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.notificationmarche.notifiele")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateNotification()));

			}
			if(dossier.getDosDatePublicationDefinitive()!=null)
			{
				nbreapprobation=nbreapprobation+ 1;
				publicationattributiondefinitive.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
				publicationattributiondefinitive.setImage("/images/rdo_on.png");
				publicationattributiondefinitive.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.pubattribdefinitive.publiele")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDatePublicationDefinitive()));
				publicationattributiondefinitive.setTooltip(Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.pubattribdefinitive.publieles")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDatePublicationDefinitive()));
			}
			if(nbreapprobation>0)
			{
				approbation.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
				approbation.setImage("/images/puce.png");
				if(nbreapprobation==5)
				{
					approbation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					approbation.setImage("/images/rdo_on.png");
					contrat=BeanLocator.defaultLookup(ContratsSession.class).getContrat(dossier,autorite, null,null,null);
					
				}
					
				
			}
			if(contrat!=null)
			{
				if(contrat.getDatedemandeimmatriculation()!=null)
				{
					cellimmatriculation.setStyle("color:#0066FF;font-family: sans-serif; font-size: 14px");
					cellimmatriculation.setImage("/images/puce.png");
					cellimmatriculation.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.demande").substring(0, 15)+"...");
					cellimmatriculation.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.demande")+" "+UtilVue.getInstance().formateLaDate2(contrat.getDatedemandeimmatriculation()));

				}
				if(contrat.getDateimmatriculation()!=null)
				{
					cellimmatriculation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					nombreimmatriculation=nombreimmatriculation+1;
					cellimmatriculation.setImage("/images/rdo_on.png");
					cellimmatriculation.setLabel(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.marche").substring(0, 15)+"...");
					cellimmatriculation.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.immatriculation.marche")+" "+UtilVue.getInstance().formateLaDate2(contrat.getDateimmatriculation()));

				}
			}
			if((dossier.getDosNombreLots()==1)||dossier.getDosLotDivisible().equals("NON"))
            {
				if(nbreouvertureplis+nbreevaluationplis+nbreevaluationplis+nbreevaluationpliss+nbreapprobation+nombreimmatriculation==30)
				{
					celltdossiers.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");	
					celltdossiers.setImage("/images/rdo_on.png");
				}
            }
			else
			{
				if(nbreouvertureplis+nbreevaluationplis+nbreevaluationplis+nbreevaluationpliss+nbreapprobation+nombreimmatriculation==31)
				{
					celltdossiers.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");	
					celltdossiers.setImage("/images/rdo_on.png");
				}
			}
		}
		if(LibelleTab!=null)
		{
			if((LibelleTab.equals("infogenerales"))||(LibelleTab.equals("garanties"))||(LibelleTab.equals("piecesadministratives"))
					||(LibelleTab.equals("criteresqualifications"))||(LibelleTab.equals("devise"))||(LibelleTab.equals("financements"))
					||(LibelleTab.equals("allotissement"))||(LibelleTab.equals("presentationsoffres")))
			  {
				idinclude.setSrc("/passationsmarches/dspaoo/dossierappeloffre.zul");	
				 TreeTreepreparation();
				 itemdossiers.setOpen(true);
			  }
			if(LibelleTab.equals("infogenerales"))
			{
				tree.setSelectedItem(treeinfosgeneral);
			}
			if(LibelleTab.equals("presentationsoffres"))
			{
				tree.setSelectedItem(treepresentationoffres);
			}
			if(LibelleTab.equals("allotissement"))
			{
			   tree.setSelectedItem(treeallotissement);
			}
			if(LibelleTab.equals("garanties"))
			{
			   tree.setSelectedItem(treegaranties);
			}
			if(LibelleTab.equals("piecesadministratives"))
			{
			   tree.setSelectedItem(treepieces);
			}
			if(LibelleTab.equals("criteresqualifications"))
			{
			   tree.setSelectedItem(treecriteres);
			}
			if(LibelleTab.equals("devise"))
			{
			   tree.setSelectedItem(treedevise);
			}
			if(LibelleTab.equals("financements"))
			{
			   tree.setSelectedItem(treefinancement);
			}
			if(LibelleTab.equals("validationdossier"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/soumissionpourvalidation.zul");	
				tree.setSelectedItem(itemvalidationdossier);
				TreeTreepreparation();
			}
			if(LibelleTab.equals("demandepublication"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/formdemandepublication.zul");
				tree.setSelectedItem(treedemandepublicationdossier);
				TreeTreepreparation();
			}
			
			if(LibelleTab.equals("publicationdossier"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/publicationdossier.zul");	
				tree.setSelectedItem(itempublicationdossier);
				TreeTreepreparation();
			}
			if(LibelleTab.equals("registreretraitdao"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/registreretraitdao.zul");	
				tree.setSelectedItem(itemregistreretraitdao);
				TreeTreeouvertureplis();
				itemouvertureplis.setOpen(false);
			}
			if(LibelleTab.equals("plisouvertures"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/registredepotplis.zul");	
				tree.setSelectedItem(itemplisouvertures);
				TreeTreeouvertureplis();
				itemouvertureplis.setOpen(false);
			}
			if(LibelleTab.equals("listepresencemembrescommissions"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/listepresencemembrescommissions.zul");
				tree.setSelectedItem(itemlistepresencemembrescommissions);
				
			}
			if(LibelleTab.equals("representantssoumissionnaires"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/representantssoumissionnaires.zul");
				tree.setSelectedItem(itemrepresentantssoumissionnaires);
				
			}
			if(LibelleTab.equals("representantsservicestechniques"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/representantsservicestechniques.zul");
				tree.setSelectedItem(itemrepresentantsservicestechniques);
				
			}
			if(LibelleTab.equals("observateursindependants"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/observateursindependants.zul");
				tree.setSelectedItem(itemobservateursindependants);
				
			}
			if(LibelleTab.equals("garantiesoumission"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/garantiesoumission.zul");
				tree.setSelectedItem(itemgarantiesoumission);
				
			}
			if(LibelleTab.equals("piecessoumissionnaires"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/piecessoumissionnaires.zul");
				tree.setSelectedItem(itempiecessoumissionnaires);
				
			}
			if(LibelleTab.equals("compositioncommissiontechnique"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/compositioncommissiontechnique.zul");
				tree.setSelectedItem(itemcompositioncommissiontechnique);
				
			}
			if(LibelleTab.equals("lecturesoffres"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/lecturesoffres.zul");
				tree.setSelectedItem(itemlecturesoffres);
				
			}
			if(LibelleTab.equals("procesverbalouverture"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/procesverbalouverture.zul");
				tree.setSelectedItem(itemprocesverbalouverture);
				
			}
			if(LibelleTab.equals("incidents"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/incidents.zul");
				tree.setSelectedItem(itemincidents);
				
			}
			if((LibelleTab.equals("listepresencemembrescommissions"))||(LibelleTab.equals("representantssoumissionnaires"))
					||(LibelleTab.equals("representantsservicestechniques"))||(LibelleTab.equals("observateursindependants"))
					||(LibelleTab.equals("garantiesoumission"))||(LibelleTab.equals("piecessoumissionnaires"))||(LibelleTab.equals("compositioncommissiontechnique"))
					||(LibelleTab.equals("lecturesoffres"))||(LibelleTab.equals("procesverbalouverture"))||(LibelleTab.equals("incidents")))
			{
				 
				 TreeTreeouvertureplis();
			}
			
			if(LibelleTab.equals("formtransmissiondossier"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/formtransmissiondossier.zul");
				tree.setSelectedItem(transmissiondossier);
			}
			if(LibelleTab.equals("controlegarantie"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/controlegarantie.zul");
				tree.setSelectedItem(controlegarantie);
			}
			if(LibelleTab.equals("formverificationconformite"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/formverificationconformite.zul");
				tree.setSelectedItem(verificationconformite);
			}
			if(LibelleTab.equals("formcorrectionoffre"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/formcorrectionoffre.zul");
				tree.setSelectedItem(correctionoffre);
			}
			if(LibelleTab.equals("vercriteresqualification"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/vercriteresqualification.zul");
				tree.setSelectedItem(vercriteresqualification);
			}
			if(LibelleTab.equals("formclassementfinal"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/classementfinal.zul");
				tree.setSelectedItem(classementfinal);
			}
			if(LibelleTab.equals("rapportevaluation"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/rapportevaluation.zul");
				tree.setSelectedItem(rapportevaluation);
			}
			if((LibelleTab.equals("formtransmissiondossier"))||(LibelleTab.equals("controlegarantie"))||(LibelleTab.equals("formverificationconformite"))
					||(LibelleTab.equals("formcorrectionoffre"))||(LibelleTab.equals("vercriteresqualification"))
					||(LibelleTab.equals("formclassementfinal"))||(LibelleTab.equals("rapportevaluation")))
			{
				
				 TreeTreeevaluation();
			}
			if(LibelleTab.equals("attributionprovisoire"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/attributionprovisoire.zul");	
				tree.setSelectedItem(itemattributionprovisoire);
				TreeTreeevaluation();
			    evaluationsoffres.setOpen(false);
			}
			if(LibelleTab.equals("soumissionpourvalidationattributionprovisoire"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/soumissionpourvalidationattributionprovisoire.zul");	
				tree.setSelectedItem(itemsoumissionpourvalidationattributionprovisoire);
				TreeTreeevaluation();
				evaluationsoffres.setOpen(false);
			}
			if(LibelleTab.equals("publicationattributionprovisoire"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/formpublicationattributionprovisoire.zul");	
				tree.setSelectedItem(itempublicationattributionprovisoire);
				TreeTreeevaluation();
				evaluationsoffres.setOpen(false);
			}
			if(LibelleTab.equals("souscriptiondumarche"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/souscriptiondumarche.zul");	
				tree.setSelectedItem(itemsouscriptiondumarche);
				TreeTreeapprobation();
			}
			if(LibelleTab.equals("bonsengagement"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/formbonsengagement.zul");	
				tree.setSelectedItem(itembonsengagement);
				TreeTreeapprobation();
			}
			if(LibelleTab.equals("approbationmarche"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/formapprobationmarche.zul");	
				tree.setSelectedItem(itemapprobationmarche);
				TreeTreeapprobation();
			}
			if(LibelleTab.equals("notificationmarche"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/formnotificationmarche.zul");	
				tree.setSelectedItem(itemnotificationmarche);
				TreeTreeapprobation();
			}
			if(LibelleTab.equals("publicationattributiondefinitive"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/formpublicationattributiondefinitive.zul");	
				tree.setSelectedItem(itempublicationattributiondefinitive);
				TreeTreeapprobation();
			}
			if(LibelleTab.equals("traitementsdossiers"))
			{
				if(dossier!=null)
				{
					Executions.getCurrent().setAttribute("dossier", dossier);
					Executions.getCurrent().setAttribute("garanties", garanties.size());
					Executions.getCurrent().setAttribute("piecesadministratives", piecesadministratives.size());
					Executions.getCurrent().setAttribute("criteresqualifications", criteresqualifications.size());
					Executions.getCurrent().setAttribute("devises", devises.size());
					Executions.getCurrent().setAttribute("bailleurs", bailleurs.size());
					Executions.getCurrent().setAttribute("lots", lots.size());
					Executions.getCurrent().setAttribute("registreretrait", registreretrait.size());
					Executions.getCurrent().setAttribute("registredepot", registredepot.size());
					Executions.getCurrent().setAttribute("membrescommissions", membrescommissions.size());
					Executions.getCurrent().setAttribute("representantsoummissionnaires", representantsoummissionnaires.size());
					Executions.getCurrent().setAttribute("representantservicestechniques", representantservicestechniques.size());
					Executions.getCurrent().setAttribute("observateursindependants", observateursindependants.size());
					Executions.getCurrent().setAttribute("piecessoumissionnaires", piecessoumissionnaires.size());
					Executions.getCurrent().setAttribute("compositioncommissiontechnique", compositioncommissiontechnique.size());
					Executions.getCurrent().setAttribute("lecturesoffres", lecturesoffres.size());
					Executions.getCurrent().setAttribute("garantiessoumissions", garantiessoumissions.size());
					Executions.getCurrent().setAttribute("criteresqualificationssoum", criteresqualificationssoum.size());
					
					Executions.getCurrent().setAttribute("registredepotverifies", registredepotverifies.size());
					Executions.getCurrent().setAttribute("criteresqualificationssoum", criteresqualificationssoum.size());
					Executions.getCurrent().setAttribute("documents", documents.size());
					Executions.getCurrent().setAttribute("soumissionnaires", soumissionnaires);
					Executions.getCurrent().setAttribute("attributaires", attributaires.size());
					Executions.getCurrent().setAttribute("contrat", contrat);
				}
				idinclude.setSrc("/passationsmarches/dspaoo/traitementsdossiers.zul");
				tree.setSelectedItem(treetdossiers);
				treetdossiers.setOpen(true);
				treepreparation.setOpen(false);
				treeouvertureplis.setOpen(false);
				treeevaluation.setOpen(false);
				treeapprobation.setOpen(false);
			}
			if(LibelleTab.equals("ouverturesplis"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/ouvertureplis.zul");
				tree.setSelectedItem(itemouvertureplis);
				itemouvertureplis.setOpen(true);
				TreeTreeouvertureplis();
			}
			if(LibelleTab.equals("evaluationsoffres"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/evaluationsoffres.zul");
				tree.setSelectedItem(evaluationsoffres);
				evaluationsoffres.setOpen(true);
				TreeTreeevaluation();
			}
			if(LibelleTab.equals("lotssoumissionnaires"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/lotssoumissionnaires.zul");
				tree.setSelectedItem(treelotssoummissionnaires);
				TreeTreeouvertureplis();
				
			}
			if(LibelleTab.equals("immatriculation"))
			{
				idinclude.setSrc("/passationsmarches/dspaoo/formdemandeimmatriculation.zul");
				tree.setSelectedItem(itemimmatriculation);
				TreeImmatriculation();
			}
		}
		else
		{
			idinclude.setSrc("/passationsmarches/dspaoo/dossierappeloffre.zul");	
			 tree.setSelectedItem(itemdossiers);     
		}
		  
		lgtitre.setValue(Labels.getLabel("kermel.plansdepassation.infos.generales.referencedossier")+": "+appel.getAporeference());
	
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}
	
	public void onClick$financement() {
		idinclude.setSrc("/passationsmarches/dspaoo/financements.zul");
	}
	

	
	public void onClick$infosgenerales() {
		idinclude.setSrc("/passationsmarches/dspaoo/infogenerales.zul");
	}
	
	public void onClick$devise() {
		idinclude.setSrc("/passationsmarches/dspaoo/devise.zul");
	}
	
	public void onClick$piecesadministratives() {
		idinclude.setSrc("/passationsmarches/dspaoo/piecesadministratives.zul");
	}
	
	public void onClick$garanties() {
		idinclude.setSrc("/passationsmarches/dspaoo/garantis.zul");
	}
	
	public void onClick$criteresqualifications() {
		idinclude.setSrc("/passationsmarches/dspaoo/criteresqualifications.zul");
	}
	
	public void onClick$commissions() {
		idinclude.setSrc("/passationsmarches/dspaoo/commissions.zul");
	}
	


	public void onClick$ordredeservicededemarrage() {
		idinclude.setSrc("/passationsmarches/dspaoo/formordredeservicededemarrage.zul");
	}
	
	public void onClick$receptionprovisoire() {
		idinclude.setSrc("/passationsmarches/dspaoo/formreceptionprovisoire.zul");
	}
	
	public void onClick$receptiondefinitive() {
		idinclude.setSrc("/passationsmarches/dspaoo/formreceptiondefinitive.zul");
	}
	
	
	

	
	
	
//	public void onOpen$treepreparation() throws Exception {
//	//	tree.setSelectedItem(treepreparation);  
//		//InfosTree();
//	}
//	
//	public void onOpen$treeouvertureplis() throws Exception {
//		tree.setSelectedItem(treeouvertureplis); 
//		InfosTree();
//		
//	}
//	public void onOpen$treeevaluation() throws Exception {
//		tree.setSelectedItem(treeevaluation); 
//		InfosTree();
//	}
//	public void onOpen$treeapprobation() throws Exception {
//		tree.setSelectedItem(treeapprobation);
//		InfosTree();
//	}
	public void onSelect$tree() throws Exception
	{
		InfosTree();
	}
	
	public void TreeTreepreparation()
	{
		
		treeouvertureplis.setOpen(false);
		treeevaluation.setOpen(false);
		treeapprobation.setOpen(false);
		treepreparation.setOpen(true);
	}
	
	public void TreeTreeouvertureplis()
	{
		treepreparation.setOpen(false);
		treeouvertureplis.setOpen(true);
		treeevaluation.setOpen(false);
		treeapprobation.setOpen(false);
	}
	
	public void TreeTreeevaluation()
	{
		treepreparation.setOpen(false);
		treeouvertureplis.setOpen(false);
		treeevaluation.setOpen(true);
		treeapprobation.setOpen(false);
	}
	
	public void TreeTreeapprobation()
	{
		treepreparation.setOpen(false);
		treeouvertureplis.setOpen(false);
		treeevaluation.setOpen(false);
		treeapprobation.setOpen(true);
	}
	public void TreeAutres()
	{
		treepreparation.setOpen(false);
		treeouvertureplis.setOpen(false);
		treeevaluation.setOpen(false);
		treeapprobation.setOpen(false);
	}
	public void InfosTree() throws Exception{
		
		/////////////Preparation du dossier//////////
		if(tree.getSelectedItem().getId().equals("treepreparation"))
		{
			TreeTreepreparation();
			if(dossier!=null)
			{
				Executions.getCurrent().setAttribute("dossier", dossier);
				Executions.getCurrent().setAttribute("garanties", garanties.size());
				Executions.getCurrent().setAttribute("piecesadministratives", piecesadministratives.size());
				Executions.getCurrent().setAttribute("criteresqualifications", criteresqualifications.size());
				Executions.getCurrent().setAttribute("devises", devises.size());
				Executions.getCurrent().setAttribute("bailleurs", bailleurs.size());
				Executions.getCurrent().setAttribute("lots", lots.size());
			}
			
			idinclude.setSrc("/passationsmarches/dspaoo/dossierspreparation.zul");
			
		}
		if(tree.getSelectedItem().getId().equals("itemdossiers"))
		{
			idinclude.setSrc("/passationsmarches/dspaoo/dossierappeloffre.zul");
			TreeTreepreparation();
		}
	
	   if(tree.getSelectedItem().getId().equals("itemvalidationdossier"))
			{
		     TreeTreepreparation();
				if(dossier==null)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				}
				else
				{
					if(garanties.size()==0)
					{
						 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirgaranties"), "Erreur", Messagebox.OK, Messagebox.ERROR);
							
					}
					else
					{
						if(piecesadministratives.size()==0)
						{
							 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirpieces"), "Erreur", Messagebox.OK, Messagebox.ERROR);
								
						}
						else
						{
							if(criteresqualifications.size()==0)
							{
								 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisircriteres"), "Erreur", Messagebox.OK, Messagebox.ERROR);
									
							}
							else
							{
								if(devises.size()==0)
								{
									 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirdevises"), "Erreur", Messagebox.OK, Messagebox.ERROR);
										
								}
								else
								{
									if(bailleurs.size()==0)
									{
										 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirbailleurs"), "Erreur", Messagebox.OK, Messagebox.ERROR);
											
									}
									else
									{
										idinclude.setSrc("/passationsmarches/dspaoo/soumissionpourvalidation.zul");
									}
								}
							}
						}
					}
					
				}
			
			}
			if(tree.getSelectedItem().getId().equals("itempublicationdossier"))
			{
				TreeTreepreparation();
				if(dossier==null)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				}
				else
				{
					if(avalider.equals("oui"))
					{
						if(dossier.getDosDateValidation()==null)
							 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validerdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
							else
							   idinclude.setSrc("/passationsmarches/dspaoo/publicationdossier.zul");
					}
					else
					{
						 idinclude.setSrc("/passationsmarches/dspaoo/publicationdossier.zul");
					}
				
				}
				
			}
		
		
         /////////////Ouverture des plis//////////
			
			if((tree.getSelectedItem().getId().equals("treeouvertureplis")))
			{
				Executions.getCurrent().setAttribute("dossier", dossier);
			    Executions.getCurrent().setAttribute("registreretrait", registreretrait.size());
    			Executions.getCurrent().setAttribute("registredepot", registredepot.size());
				Executions.getCurrent().setAttribute("membrescommissions", membrescommissions.size());
				Executions.getCurrent().setAttribute("representantsoummissionnaires", representantsoummissionnaires.size());
				Executions.getCurrent().setAttribute("representantservicestechniques", representantservicestechniques.size());
				Executions.getCurrent().setAttribute("observateursindependants", observateursindependants.size());
				Executions.getCurrent().setAttribute("piecessoumissionnaires", piecessoumissionnaires.size());
				Executions.getCurrent().setAttribute("compositioncommissiontechnique", compositioncommissiontechnique.size());
				Executions.getCurrent().setAttribute("lecturesoffres", lecturesoffres.size());
				Executions.getCurrent().setAttribute("garantiessoumissions", garantiessoumissions.size());
				Executions.getCurrent().setAttribute("criteresqualificationssoum", criteresqualificationssoum.size());
				TreeTreeouvertureplis();
				 idinclude.setSrc("/passationsmarches/dspaoo/dossiersouverturesplis.zul");
			}
		
			if(tree.getSelectedItem().getId().equals("itemregistreretraitdao"))
			{
				TreeTreeouvertureplis();
				if(dossier==null)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				}
				else
				{
					if(dossier.getDosDatePublication()==null)
					 {
						 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.publieravis"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					 }
					 else
					 {
						 idinclude.setSrc("/passationsmarches/dspaoo/registreretraitdao.zul");
					 }
				}
			}
			if(tree.getSelectedItem().getId().equals("itemplisouvertures"))
			{
				TreeTreeouvertureplis();
				if(dossier==null)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				}
				else
				{
					if(registreretrait.size()==0)
					 {
						 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.saisirregistreretrait"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					 }
					 else
					 {
						 idinclude.setSrc("/passationsmarches/dspaoo/registredepotplis.zul");
					 }
				}
			}
			if(tree.getSelectedItem().getId().equals("itemouvertureplis"))
			{
				Executions.getCurrent().setAttribute("dossier", dossier);
				Executions.getCurrent().setAttribute("membrescommissions", membrescommissions.size());
				Executions.getCurrent().setAttribute("representantsoummissionnaires", representantsoummissionnaires.size());
				Executions.getCurrent().setAttribute("representantservicestechniques", representantservicestechniques.size());
				Executions.getCurrent().setAttribute("observateursindependants", observateursindependants.size());
				Executions.getCurrent().setAttribute("piecessoumissionnaires", piecessoumissionnaires.size());
				Executions.getCurrent().setAttribute("compositioncommissiontechnique", compositioncommissiontechnique.size());
				Executions.getCurrent().setAttribute("lecturesoffres", lecturesoffres.size());
				Executions.getCurrent().setAttribute("garantiessoumissions", garantiessoumissions.size());
				Executions.getCurrent().setAttribute("lots", lots.size());
				
				TreeTreeouvertureplis();
				idinclude.setSrc("/passationsmarches/dspaoo/ouvertureplis.zul");
				itemouvertureplis.setOpen(true);
			}
			if(tree.getSelectedItem().getId().equals("itemlistepresencemembrescommissions"))
			{
				TreeTreeouvertureplis();
				idinclude.setSrc("/passationsmarches/dspaoo/listepresencemembrescommissions.zul");
			}	
			if(tree.getSelectedItem().getId().equals("itemrepresentantssoumissionnaires"))
			{
				TreeTreeouvertureplis();
				idinclude.setSrc("/passationsmarches/dspaoo/representantssoumissionnaires.zul");
			}
			if(tree.getSelectedItem().getId().equals("itemrepresentantsservicestechniques"))
			{
				TreeTreeouvertureplis();
				idinclude.setSrc("/passationsmarches/dspaoo/representantsservicestechniques.zul");
			}
			if(tree.getSelectedItem().getId().equals("itemobservateursindependants"))
			{
				TreeTreeouvertureplis();
				idinclude.setSrc("/passationsmarches/dspaoo/observateursindependants.zul");
			}
			if(tree.getSelectedItem().getId().equals("itemgarantiesoumission"))
			{
				TreeTreeouvertureplis();
				idinclude.setSrc("/passationsmarches/dspaoo/garantiesoumission.zul");
			}
			if(tree.getSelectedItem().getId().equals("itempiecessoumissionnaires"))
			{
				TreeTreeouvertureplis();
				idinclude.setSrc("/passationsmarches/dspaoo/piecessoumissionnaires.zul");
			}
			if(tree.getSelectedItem().getId().equals("itemlecturesoffres"))
			{
				TreeTreeouvertureplis();
				idinclude.setSrc("/passationsmarches/dspaoo/lecturesoffres.zul");
			}
			if(tree.getSelectedItem().getId().equals("itemprocesverbalouverture"))
			{
				TreeTreeouvertureplis();
				idinclude.setSrc("/passationsmarches/dspaoo/procesverbalouverture.zul");
			}
			if(tree.getSelectedItem().getId().equals("itemcompositioncommissiontechnique"))
			{
				TreeTreeouvertureplis();
				idinclude.setSrc("/passationsmarches/dspaoo/compositioncommissiontechnique.zul");
			}
			if(tree.getSelectedItem().getId().equals("itemincidents"))
			{
				TreeTreeouvertureplis();
				idinclude.setSrc("/passationsmarches/dspaoo/incidents.zul");
			}	
			if(tree.getSelectedItem().getId().equals("treelotssoummissionnaires"))
			{
				TreeTreeouvertureplis();
				idinclude.setSrc("/passationsmarches/dspaoo/lotssoumissionnaires.zul");
			}
		
		 /////////////Evaluation des plis//////////
		if((tree.getSelectedItem().getId().equals("treeevaluation")))
		{
			TreeTreeevaluation();
			Executions.getCurrent().setAttribute("dossier", dossier);
			Executions.getCurrent().setAttribute("registredepotverifies", registredepotverifies.size());
			Executions.getCurrent().setAttribute("criteresqualificationssoum", criteresqualificationssoum.size());
			Executions.getCurrent().setAttribute("documents", documents.size());
			Executions.getCurrent().setAttribute("soumissionnaires", soumissionnaires);
			Executions.getCurrent().setAttribute("attributaires", attributaires.size());
			
			idinclude.setSrc("/passationsmarches/dspaoo/dossiersevaluationsplis.zul");
			
		}	
		if((tree.getSelectedItem().getId().equals("transmissiondossier")))
		{
			TreeTreeevaluation();
			idinclude.setSrc("/passationsmarches/dspaoo/formtransmissiondossier.zul");
			
		}	
		if((tree.getSelectedItem().getId().equals("controlegarantie")))
		{
			TreeTreeevaluation();
			idinclude.setSrc("/passationsmarches/dspaoo/controlegarantie.zul");
			
		}	
		if((tree.getSelectedItem().getId().equals("verificationconformite")))
		{
			TreeTreeevaluation();
			idinclude.setSrc("/passationsmarches/dspaoo/formverificationconformite.zul");
			
		}	
		if((tree.getSelectedItem().getId().equals("correctionoffre")))
		{
			TreeTreeevaluation();
			idinclude.setSrc("/passationsmarches/dspaoo/formcorrectionoffre.zul");
			
		}	
		if((tree.getSelectedItem().getId().equals("vercriteresqualification")))
		{
			TreeTreeevaluation();
			idinclude.setSrc("/passationsmarches/dspaoo/vercriteresqualification.zul");
			
		}	
		if((tree.getSelectedItem().getId().equals("classementfinal")))
		{
			TreeTreeevaluation();
			idinclude.setSrc("/passationsmarches/dspaoo/classementfinal.zul");
			
		}	
		if((tree.getSelectedItem().getId().equals("rapportevaluation")))
		{
			TreeTreeevaluation();
			idinclude.setSrc("/passationsmarches/dspaoo/rapportevaluation.zul");
			
		}	
		if((tree.getSelectedItem().getId().equals("itemattributionprovisoire")))
		{
			TreeTreeevaluation();
			idinclude.setSrc("/passationsmarches/dspaoo/attributionprovisoire.zul");
			
		}	
		if((tree.getSelectedItem().getId().equals("itemsoumissionpourvalidationattributionprovisoire")))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				if(attributaires.size()==dossier.getDosNombreLots())
				{
					TreeTreeevaluation();
					idinclude.setSrc("/passationsmarches/dspaoo/soumissionpourvalidationattributionprovisoire.zul");
				}
				else
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.saisirprocesverbal"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				}
			}
			
			
			
		}	
		if((tree.getSelectedItem().getId().equals("itempublicationattributionprovisoire")))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				 if(dossier.getDosDateValidationPrequalif()==null)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validerdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
				  TreeTreeevaluation();
				  idinclude.setSrc("/passationsmarches/dspaoo/formpublicationattributionprovisoire.zul");
				 }
			}
			
		}	
		 /////////////Approbation//////////
		if((tree.getSelectedItem().getId().equals("treeapprobation")))
		{
			TreeTreeapprobation();
			Executions.getCurrent().setAttribute("dossier", dossier);
			idinclude.setSrc("/passationsmarches/dspaoo/dossiersapprobation.zul");
		}
		if((tree.getSelectedItem().getId().equals("itemsouscriptiondumarche")))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				 if(dossier.getDosDatePublicationProvisoire()==null)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.publierdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
					    TreeTreeapprobation();
						idinclude.setSrc("/passationsmarches/dspaoo/souscriptiondumarche.zul");
				 }
			}
			
		}
		if((tree.getSelectedItem().getId().equals("itembonsengagement")))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				 if(dossier.getDosDatePublicationProvisoire()==null)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.souscriredossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
					    TreeTreeapprobation();
						idinclude.setSrc("/passationsmarches/dspaoo/formbonsengagement.zul");
				 }
			}
			
		}
		if((tree.getSelectedItem().getId().equals("itemapprobationmarche")))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				 if(dossier.getDosDateValidationSignature()==null)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validerdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
		        	TreeTreeapprobation();
			        idinclude.setSrc("/passationsmarches/dspaoo/formapprobationmarche.zul");
				 }
			}
		}
		if((tree.getSelectedItem().getId().equals("itemnotificationmarche")))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				 if(dossier.getDosDateApprobation()==null)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.approuverdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
					 TreeTreeapprobation();
					 idinclude.setSrc("/passationsmarches/dspaoo/formnotificationmarche.zul");
				 }
			}
			
		}
		if((tree.getSelectedItem().getId().equals("itempublicationattributiondefinitive")))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				 if(dossier.getDosDateNotification()==null)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.notifierdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
						TreeTreeapprobation();
						idinclude.setSrc("/passationsmarches/dspaoo/formpublicationattributiondefinitive.zul");
				 }
			}
		
		}
	
		/////////////:REste/////
		if((tree.getSelectedItem().getId().equals("treerecours")))
		{
			TreeAutres();
			idinclude.setSrc("/passationsmarches/dspaoo/recours.zul");
		}
		if((tree.getSelectedItem().getId().equals("treedocuments")))
		{
			TreeAutres();
			idinclude.setSrc("/passationsmarches/dspaoo/documents.zul");
		}
		if(tree.getSelectedItem().getId().equals("evaluationsoffres"))
		{
			 if(appel.getApoDatepvouverturepli()==null)
			 {
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.saisirproces"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			 }
			 else
			 {
					Executions.getCurrent().setAttribute("dossier", dossier);
					Executions.getCurrent().setAttribute("registredepotverifies", registredepotverifies.size());
					Executions.getCurrent().setAttribute("criteresqualificationssoum", criteresqualificationssoum.size());
					Executions.getCurrent().setAttribute("documents", documents.size());
					Executions.getCurrent().setAttribute("soumissionnaires", soumissionnaires);
				    idinclude.setSrc("/passationsmarches/dspaoo/evaluationsoffres.zul");
			 }
		}
		if(tree.getSelectedItem().getId().equals("treeinfosgeneral"))
		{
			session.setAttribute("libelle", "infogenerales");
			loadApplicationState("suivi_dspaoo");
		}
		if(tree.getSelectedItem().getId().equals("treepresentationoffres"))
		{
			session.setAttribute("libelle", "presentationsoffres");
			loadApplicationState("suivi_dspaoo");
		}
		if(tree.getSelectedItem().getId().equals("treeallotissement"))
		{
			session.setAttribute("libelle", "allotissement");
			loadApplicationState("suivi_dspaoo");
		}
		if(tree.getSelectedItem().getId().equals("treegaranties"))
		{
			session.setAttribute("libelle", "garanties");
			loadApplicationState("suivi_dspaoo");
		}
		if(tree.getSelectedItem().getId().equals("treepieces"))
		{
			session.setAttribute("libelle", "piecesadministratives");
			loadApplicationState("suivi_dspaoo");
		}
		if(tree.getSelectedItem().getId().equals("treecriteres"))
		{
			session.setAttribute("libelle", "criteresqualifications");
			loadApplicationState("suivi_dspaoo");
		}
		if(tree.getSelectedItem().getId().equals("treedevise"))
		{
			session.setAttribute("libelle", "devise");
			loadApplicationState("suivi_dspaoo");
		}
		if(tree.getSelectedItem().getId().equals("treefinancement"))
		{
			session.setAttribute("libelle", "financements");
			loadApplicationState("suivi_dspaoo");
		}
		if(tree.getSelectedItem().getId().equals("treedemandepublicationdossier"))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				 if(dossier.getDosDateValidation()==null)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validerdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
					 TreeTreepreparation();
					 idinclude.setSrc("/passationsmarches/dspaoo/formdemandepublication.zul");
				 }
			}
			
		}
		if(tree.getSelectedItem().getId().equals("itemimmatriculation"))
		{
			if(dossier==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				 if(dossier.getDosDatePublicationDefinitive()==null)
				 {
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.publierattrribution"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				 }
				 else
				 {
					 TreeImmatriculation();
			       idinclude.setSrc("/passationsmarches/dspaoo/formdemandeimmatriculation.zul");
				 }
			}
		}	
		
		if(tree.getSelectedItem().getId().equals("treetdossiers"))
		{
			if(dossier!=null)
			{
				Executions.getCurrent().setAttribute("dossier", dossier);
				Executions.getCurrent().setAttribute("garanties", garanties.size());
				Executions.getCurrent().setAttribute("piecesadministratives", piecesadministratives.size());
				Executions.getCurrent().setAttribute("criteresqualifications", criteresqualifications.size());
				Executions.getCurrent().setAttribute("devises", devises.size());
				Executions.getCurrent().setAttribute("bailleurs", bailleurs.size());
				Executions.getCurrent().setAttribute("lots", lots.size());
				Executions.getCurrent().setAttribute("registreretrait", registreretrait.size());
				Executions.getCurrent().setAttribute("registredepot", registredepot.size());
				Executions.getCurrent().setAttribute("membrescommissions", membrescommissions.size());
				Executions.getCurrent().setAttribute("representantsoummissionnaires", representantsoummissionnaires.size());
				Executions.getCurrent().setAttribute("representantservicestechniques", representantservicestechniques.size());
				Executions.getCurrent().setAttribute("observateursindependants", observateursindependants.size());
				Executions.getCurrent().setAttribute("piecessoumissionnaires", piecessoumissionnaires.size());
				Executions.getCurrent().setAttribute("compositioncommissiontechnique", compositioncommissiontechnique.size());
				Executions.getCurrent().setAttribute("lecturesoffres", lecturesoffres.size());
				Executions.getCurrent().setAttribute("garantiessoumissions", garantiessoumissions.size());
				Executions.getCurrent().setAttribute("criteresqualificationssoum", criteresqualificationssoum.size());
				
				Executions.getCurrent().setAttribute("registredepotverifies", registredepotverifies.size());
				Executions.getCurrent().setAttribute("criteresqualificationssoum", criteresqualificationssoum.size());
				Executions.getCurrent().setAttribute("documents", documents.size());
				Executions.getCurrent().setAttribute("soumissionnaires", soumissionnaires);
				Executions.getCurrent().setAttribute("attributaires", attributaires.size());
				Executions.getCurrent().setAttribute("contrat", contrat);
			}
			idinclude.setSrc("/passationsmarches/dspaoo/traitementsdossiers.zul");
		}
		
		
	}
	
	public void onSelect$iteminfosgenerales(){
		idinclude.setSrc("/passationsmarches/dspaoo/observateursindependants.zul");
//        for (int i = 0; i < tree.getChildren().size(); i++) {
//			
//			if(tree.getSelectedItem().isCheckable()==true)
//			{
//				tree.getSelectedItem().setOpen(true);
//			}
//			else
//			{
//				tree.getSelectedItem().setOpen(false);
//			}
//			//treeitem=tree.getSelectedItem()..getParentItem();
//		}
		
	}
	public void TreeImmatriculation()
	{
		treepreparation.setOpen(false);
		treeouvertureplis.setOpen(false);
		treeevaluation.setOpen(false);
		treeapprobation.setOpen(false);
		
	}
}