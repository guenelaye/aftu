package sn.ssi.kermel.web.dspaoo.controllers;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.West;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCategori;
import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygModeselection;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.be.entity.SygTypesmarchesmodespassations;
import sn.ssi.kermel.be.entity.SygTypesmarchesmodesselections;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsSession;
import sn.ssi.kermel.be.referentiel.ejb.CategoriSession;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesSession;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesmodespassationsSession;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesmodesselectionsSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

/**

 * @author Adama samb
 * @since mardi 11 Janvier 2011, 12:34
 
 */
public class ProcedureFormController extends AbstractWindow implements
		AfterCompose, EventListener ,ListitemRenderer{

	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	public static final String WINDOW_PARAM_TYPE = "TYPE";
	public static final String WINDOW_PARAM_MODEPASSATION = "MODEPASSATION";
	private Paging pgPagination,pgMode,pgModeSelection;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    private   String page=null,mode;
    private Long type,code,idmode=null,codeactivite=null;
    private Listheader lshReference,lshLibelle,lshDatelancement,lshDateattribution,lshDatedemarrage,lshDateachevement,lshMontant;
    Session session = getHttpSession();
    SygRealisations realisation=new SygRealisations();
    SygRealisations realisations=new SygRealisations();
    SygCategori categorie=new SygCategori();
    SygTypesmarches typemarche=new SygTypesmarches();
    SygAppelsOffres appel=new SygAppelsOffres();
     String login,libelle=null,libellemode=null,libellemodeselection=null;
    private Listbox lstListe,lstMode,lstModeSelection;
    private Div step0,step1,step2;
    private Label lblRealisations,lblRealisationsStep2,lblCategories,lblTypemarche,lblTypemarcheStep2;
    private Bandbox bdMode,bdModeSelection;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherMode,txtRechercherModeSelection,txtObjet;
    SygModepassation modepassation=null;
    SygModeselection modeselection=null;
    private Textbox txtObjets,txtReferences,txtReference;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg,LibelleTab,objet=null,annees;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Datebox dtcreation;
	private Decimalbox dcmontant;
	private Utilisateur infoscompte;
    private SygAutoriteContractante autorite=null;
    private Long idrealisation=null;
    private Intbox annee;
    private int gestion=-1;
	private Tree tree;
	private Treechildren child = new Treechildren();
	
	private String openWest;
	private West wp;

	List<SygCategori> listCategori = new ArrayList<SygCategori>();
	private Menuitem menuNextStep1;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		lshReference.setSortAscending(new FieldComparator("reference", false));
		lshReference.setSortDescending(new FieldComparator("reference", true));
		lshLibelle.setSortAscending(new FieldComparator("libelle", false));
		lshLibelle.setSortDescending(new FieldComparator("libelle", true));
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		lstListe.setItemRenderer(this);
		
//		addEventListener(ApplicationEvents.ON_CATEGORIES, this);
//		pgCategorie.setPageSize(byPage);
//		pgCategorie.addForward("onPaging", this, ApplicationEvents.ON_CATEGORIES);
//		lstCategorie.setItemRenderer(new CaracteresRenderer());
		
		addEventListener(ApplicationEvents.ON_MODES, this);
		lstMode.setItemRenderer(new ModesRenderer());
		pgMode.setPageSize(byPageBandbox);
		pgMode.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODES);
		
		
		addEventListener(ApplicationEvents.ON_MODESSELECTIONS, this);
		lstModeSelection.setItemRenderer(new SelectionsRenderer());
		pgModeSelection.setPageSize(byPageBandbox);
		pgModeSelection.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODESSELECTIONS);
		

	}

	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		type = (Long) map.get(WINDOW_PARAM_TYPE);
		mode = (String) map.get(WINDOW_PARAM_MODE);
	//	idmode=(Long) map.get(WINDOW_PARAM_MODEPASSATION);
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		if(type.intValue()==UIConstants.PARAM_TMTRAVAUX)
			LibelleTab="REALTF";
		if(type.intValue()==UIConstants.PARAM_TMPI)
			LibelleTab="REALPI";
		if(type.intValue()==UIConstants.PARAM_TMSERVICES)
			LibelleTab="REALSERVICES";
		if(type.intValue()==UIConstants.PARAM_TMFOURNITURES)
			LibelleTab="REALSFOURN";
		if(type.intValue()==UIConstants.PARAM_TMDSP)
			LibelleTab="REALSDSP";
		session.setAttribute("LibelleTab",LibelleTab);
		typemarche=BeanLocator.defaultLookup(TypesmarchesSession.class).findById(type);
		
		lblTypemarcheStep2.setValue(typemarche.getLibelle());
		lblTypemarche.setValue(typemarche.getLibelle());
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(WINDOW_PARAM_CODE);
			appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(code);
			modepassation=appel.getModepassation();
			bdMode.setValue(modepassation.getLibelle());
			modeselection=appel.getModeselection();
			bdModeSelection.setValue(modeselection.getLibelle());
			typemarche=appel.getTypemarche();
			realisations=appel.getRealisation();
			dtcreation.setValue(appel.getApodatecreation());
			txtObjets.setValue(appel.getApoobjet());
			dcmontant.setValue(appel.getApomontantestime());
			categorie=appel.getCategorie();
			txtReference.setValue(appel.getAporeference());
			codeactivite=categorie.getId();
			idrealisation=realisations.getIdrealisation();
		}
		annees = UtilVue.getInstance().anneecourant(new Date());
		gestion=Integer.parseInt(annees);
		annee.setValue(gestion);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		Events.postEvent(ApplicationEvents.ON_CATEGORIES, this, null);
		Events.postEvent(ApplicationEvents.ON_MODESSELECTIONS, this, null);
		Events.postEvent(ApplicationEvents.ON_MODES, this, null);
		
        listCategori = BeanLocator.defaultLookup(CategoriSession.class).findRech(0, -1, null,null,Long.parseLong("1"));
	
		
//		openWest = (String) Executions.getCurrent().getAttribute("openWest");
//		wp.setOpen(true);

		initTree();
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygRealisations> reals = BeanLocator.defaultLookup(RealisationsSession.class).Realisations(activePage,byPage,objet,type,UIConstants.NPARENT, Labels.getLabel("kermel.plansdepassation.statut.publier"),autorite, mode, idrealisation, gestion, 0);
			 SimpleListModel listModel = new SimpleListModel(reals);
			 lstListe.setModel(listModel);
			 pgPagination.setTotalSize(BeanLocator.defaultLookup( RealisationsSession.class).countRealisations(objet,type,UIConstants.NPARENT, Labels.getLabel("kermel.plansdepassation.statut.publier"),autorite, mode,idrealisation, gestion, 0 ));
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CATEGORIES)) {
//			if (event.getData() != null) {
//				activePage = Integer.parseInt((String) event.getData());
//				byPage = -1;
//				pgCategorie.setPageSize(1000);
//			} else {
//				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
//				activePage = pgCategorie.getActivePage() * byPage;
//				pgCategorie.setPageSize(byPage);
//			}
//			 List<SygSecteursactivites> categories = BeanLocator.defaultLookup(SecteursactivitesSession.class).find(activePage,byPage,codeactivite,libelle,null);
//			 SimpleListModel listModel = new SimpleListModel(categories);
//			 lstCategorie.setModel(listModel);
//			 pgCategorie.setTotalSize(BeanLocator.defaultLookup( SecteursactivitesSession.class).count(codeactivite,libelle,null));
		}
		else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODES)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgMode.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgMode.getActivePage() * byPageBandbox;
				pgMode.setPageSize(byPageBandbox);
			}
			List<SygTypesmarchesmodespassations> modes = BeanLocator.defaultLookup(TypesmarchesmodespassationsSession.class).find(activePage, byPageBandbox,libellemode,typemarche,null);
			lstMode.setModel(new SimpleListModel(modes));
			pgMode.setTotalSize(BeanLocator.defaultLookup(TypesmarchesmodespassationsSession.class).count(libellemode,typemarche,null));
			
		}
		
		else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODESSELECTIONS)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgModeSelection.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgModeSelection.getActivePage() * byPageBandbox;
				pgModeSelection.setPageSize(byPageBandbox);
			}
			List<SygTypesmarchesmodesselections> modes = BeanLocator.defaultLookup(TypesmarchesmodesselectionsSession.class).find(activePage, byPageBandbox,libellemodeselection,typemarche,null);
			lstModeSelection.setModel(new SimpleListModel(modes));
			pgModeSelection.setTotalSize(BeanLocator.defaultLookup(TypesmarchesmodesselectionsSession .class).count(libellemodeselection,typemarche,null));
			if(modes.size()>0)
			{
				modeselection=modes.get(0).getMode();
				bdModeSelection.setValue(modeselection.getLibelle());
			}
			
		}
	}

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygRealisations real= (SygRealisations) data;
		item.setValue(real);

		 Listcell cellReference = new Listcell(real.getReference());
		 cellReference.setParent(item);
		 
		 Listcell cellLibelle = new Listcell(real.getLibelle());
		 cellLibelle.setParent(item);
		 
		 Listcell cellModepassation = new Listcell(real.getModepassation().getLibelle());
		 cellModepassation.setParent(item);
		 
		 Listcell cellMontant = new Listcell(ToolKermel.format2Decimal(real.getMontant()));
		 cellMontant.setParent(item);
		 
		 if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
		if(real.getIdrealisation().intValue()==realisations.getIdrealisation().intValue())
			item.setSelected(true);
		 }
		 
	}
	
    public class CaracteresRenderer implements ListitemRenderer{
		
		
		
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygSecteursactivites secteur = (SygSecteursactivites) data;
			item.setValue(secteur);

			 Listcell cellLibelle = new Listcell("");
//			 if((secteur.getCode().length()/3)==1)
//				 cellLibelle.setLabel("---"+secteur.getLibelle());
//			 else  if((secteur.getCode().length()/3)==2)
//				 cellLibelle.setLabel("------"+secteur.getLibelle());
//			 else  if((secteur.getCode().length()/3)==3)
//				 cellLibelle.setLabel("---------"+secteur.getLibelle());
//			 else  if((secteur.getCode().length()/3)==4)
//				 cellLibelle.setLabel("------------"+secteur.getLibelle());
//			 else  if((secteur.getCode().length()/3)==4)
//				 cellLibelle.setLabel("---------------"+secteur.getLibelle());
//			 else  if((secteur.getCode().length()/3)==6)
//				 cellLibelle.setLabel("------------------"+secteur.getLibelle());
//			 else  if((secteur.getCode().length()/3)==7)
//				 cellLibelle.setLabel("---------------------"+secteur.getLibelle());
//			 else  if((secteur.getCode().length()/3)==8)
//				 cellLibelle.setLabel("------------------------"+secteur.getLibelle());
//			 else  if((secteur.getCode().length()/3)==9)
//				 cellLibelle.setLabel("---------------------------"+secteur.getLibelle());
//			 else  if((secteur.getCode().length()/3)==10)
//				 cellLibelle.setLabel("------------------------------"+secteur.getLibelle());
//			 else  cellLibelle.setLabel(secteur.getLibelle());
//			 cellLibelle.setParent(item);
			 
			 cellLibelle.setLabel(secteur.getLibelle());
			 cellLibelle.setParent(item);
//			 
//			 if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
//			 if(secteur.getCode().equals(categorie.getCode()))
//					item.setSelected(true);
//			 }
		     }
		}

	public void onClick$menuNextStep0()
	{
		if (lstListe.getSelectedItem() == null)
			
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		realisation=(SygRealisations) lstListe.getSelectedItem().getValue();
		lblRealisations.setValue(realisation.getLibelle());
		lblRealisationsStep2.setValue(realisation.getLibelle());
		txtReference.setValue(realisation.getReference());
		txtReferences.setValue(realisation.getReference());
		modepassation=realisation.getModepassation();
		bdMode.setValue(modepassation.getLibelle());
		dcmontant.setValue(realisation.getMontant());
		txtObjets.setValue(realisation.getLibelle());
		dtcreation.setValue(new Date());
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
	}
	
	public void onClick$menuCancelStep0()
	{
		detach();
	}
	
	public void onClick$menuPreviousStep1()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
	}
	
	public void onClick$menuNextStep1()
	{
      if (categorie == null)
			throw new WrongValueException(menuNextStep1, Labels.getLabel("kermel.error.select.item"));
           lblCategories.setValue(categorie.getLibelle());
           step0.setVisible(false);
   		   step1.setVisible(false);
   		   step2.setVisible(true);
	}
	
	public void onClick$menuNextStep1___()
	{
  //    if (lstCategorie.getSelectedItem() == null)
			
//			throw new WrongValueException(lstCategorie, Labels.getLabel("kermel.error.select.item"));
//           categorie=(SygSecteursactivites) lstCategorie.getSelectedItem().getValue();
//           lblCategories.setValue(categorie.getLibelle()+" "+categori.getLibelle());
//           step0.setVisible(false);
//   		   step1.setVisible(false);
//   		   step2.setVisible(true);
	}
	
	public void onClick$menuCancelStep1()
	{
		detach();
	}
	
	public void onClick$menuPreviousStep2()
	{
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
	}
	
	public void onClick$menuCancelStep2()
	{
		detach();
	}
	

	
///////////Mode de passation///////// 
		public void onSelect$lstMode(){
		modepassation= (SygModepassation) lstMode.getSelectedItem().getValue();
		bdMode.setValue(modepassation.getLibelle());
		bdMode.close();
		
		}
		
		public class ModesRenderer implements ListitemRenderer{
		
		
		
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypesmarchesmodespassations modes = (SygTypesmarchesmodespassations) data;
			item.setValue(modes.getMode());
			
			
			Listcell cellLibelle = new Listcell("");
			if (modes.getMode().getLibelle()!=null){
				cellLibelle.setLabel(modes.getMode().getLibelle());
			}
			cellLibelle.setParent(item);
		
		}
		}
		public void onFocus$txtRechercherMode(){
		if(txtRechercherMode.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.modepassation"))){
			txtRechercherMode.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherMode(){
		if(txtRechercherMode.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.modepassation")) || txtRechercherMode.getValue().equals("")){
			libellemode = null;
			page=null;
		}else{
			libellemode = txtRechercherMode.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_MODES, this, page);
		}
		
		
///////////Mode de passation///////// 
	public void onSelect$lstModeSelection(){
	modeselection= (SygModeselection) lstModeSelection.getSelectedItem().getValue();
	bdModeSelection.setValue(modeselection.getLibelle());
	bdModeSelection.close();
	
	}
	
	public class SelectionsRenderer implements ListitemRenderer{
	
	
	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygTypesmarchesmodesselections modes = (SygTypesmarchesmodesselections) data;
		item.setValue(modes.getMode());
		
		
		Listcell cellLibelle = new Listcell("");
		if (modes.getMode().getLibelle()!=null){
			cellLibelle.setLabel(modes.getMode().getLibelle());
		}
		cellLibelle.setParent(item);
	
	}
	}
	public void onFocus$txtRechercherModeSelection(){
	if(txtRechercherModeSelection.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.modeselection"))){
		txtRechercherModeSelection.setValue("");
	}		 
	}
	
	public void  onClick$btnRechercherModeSelection(){
	if(txtRechercherModeSelection.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.modeselection")) || txtRechercherModeSelection.getValue().equals("")){
		libellemodeselection = null;
		page=null;
	}else{
		libellemodeselection = txtRechercherModeSelection.getValue();
		page="0";
	}
	Events.postEvent(ApplicationEvents.ON_MODESSELECTIONS, this, page);
	}
	
private boolean checkFieldConstraints() {
		
		try {
			if(dtcreation.getValue()==null)
		     {
              errorComponent = dtcreation;
              errorMsg = Labels.getLabel("kermel.plansdepassation.passationsmarches.datecreation")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((dtcreation.getValue()).after(new Date()))
			 {
				errorComponent = dtcreation;
				errorMsg =Labels.getLabel("kermel.plansdepassation.passationsmarches.datecreation")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.referentiel.date.dujour")+": "+UtilVue.getInstance().formateLaDate(new Date());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			if(modeselection==null)
		     {
               errorComponent = bdModeSelection;
               errorMsg = Labels.getLabel("kermel.referentiel.modeselection")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(txtReferences.getValue().equals(""))
		     {
              errorComponent = txtReferences;
              errorMsg = Labels.getLabel("kermel.plansdepassation.passationsmarches.reference.dossier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(txtObjets.getValue().equals(""))
		     {
             errorComponent = txtObjets;
             errorMsg = Labels.getLabel("kermel.plansdepassation.passationsmarches.objets")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dcmontant.getValue()==null)
		     {
            errorComponent = dcmontant;
            errorMsg = Labels.getLabel("kermel.plansdepassation.passationsmarches.montantestime")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

	public void onClick$menuValider()
		{
			if(checkFieldConstraints())
			{
				appel.setModepassation(modepassation);
				appel.setModeselection(modeselection);
				appel.setTypemarche(typemarche);
				appel.setRealisation(realisation);
				appel.setApodatecreation(dtcreation.getValue());
				appel.setApoobjet(txtObjets.getValue());
				appel.setApomontantestime(dcmontant.getValue());
				appel.setCategorie(categorie);
				appel.setAporeference(txtReference.getValue());
				appel.setAutorite(autorite);
				appel.setApomontantversement(new BigDecimal(0));
				appel.setApoStatut(UIConstants.CONTRAT_NPAYE);
				if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
					
				BeanLocator.defaultLookup(AppelsOffresSession.class).save(appel);
				}
				else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
					BeanLocator.defaultLookup(AppelsOffresSession.class).update(appel);
					if(realisations!=realisation)
					{
						realisation.setMontant(dcmontant.getValue());
						realisations.setAppel(UIConstants.NPARENT);
						BeanLocator.defaultLookup(RealisationsSession.class).update(realisations);
					}
				}
				realisation.setAppel(UIConstants.PARENT);
				BeanLocator.defaultLookup(RealisationsSession.class).update(realisation);
				loadApplicationState("procedures_passations");
				detach();
			}
		}
	
	
	public void  onClick$btnchercher(){
		if((txtObjet.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.recours.objet")))||(txtObjet.getValue().equals("")))
		 {
			objet=null;
		 }
		else
		 {
			objet=txtObjet.getValue();
			page="0";
		 }
		if(annee.getValue()==null)
		 {
			gestion=-1;
		
		 }
		else
		 {
			gestion=annee.getValue();
			page="0";
		 }

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
	
	 public void onFocus$txtObjet(){
			if(txtObjet.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.recours.objet"))){
				txtObjet.setValue("");
			}		 
			}
		public void onBlur$txtObjet(){
			if(txtObjet.getValue().equalsIgnoreCase("")){
				txtObjet.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.recours.objet"));
			}		 
			}
		public void onOK$txtObjet(){
			onClick$btnchercher();	
		}
		public void onOK$btnchercher(){
			onClick$btnchercher();	
		}
		public void onOK$annee()
		{
			onClick$btnchercher();
		}
		
		private void initTree() {
			child.setParent(tree);
			treeProcedureBuilder();

		    	
			
		}
		
		public void onSelect$tree() {
			tree.isInvalidated();
			Object selobj = tree.getSelectedItem().getValue();

			session.setAttribute("selObj", selobj);
		
			
			if (selobj instanceof SygCategori) {

				List<String> status = new ArrayList<String>();
				categorie = (SygCategori) selobj;
				
				
				//if(direction.getDivision().getId()!=5){
				 System.out.println(tree.getSelectedItem().getId());
				
			      //}
			} 

		}
		public void treeProcedureBuilder() {

			String key = null;
			
				System.out.println("==============================ok");
				
				 for (SygCategori cat : listCategori) {
			
					
					 Treeitem debutItem = new Treeitem();
					 debutItem.setId(cat.getId().toString());
					 debutItem.setLabel(cat.getLibelle());
					 debutItem.setTooltiptext(cat.getLibelle()+" "+cat.getId());
					 debutItem.setValue(cat);
					 debutItem.setOpen(false);
					
					 if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
						 if(codeactivite.intValue()==cat.getId().intValue())
						 {
							 debutItem.setSelected(true);
							 
						 }
							 
					 }
					 debutItem.setParent(child);
			
					 
			 List<SygCategori> listNivo2 = BeanLocator.defaultLookup(
					 CategoriSession.class).findRech(0, -1,null,Long.parseLong("2"),cat.getId());
							
						 if(listNivo2.size() > 0){
						 Treechildren procedureTreechildren = new Treechildren();
						 for (SygCategori service : listNivo2) {
						 Treeitem etapeItem = new Treeitem();
						
						 etapeItem.setLabel(service.getLibelle());
						 etapeItem.setTooltiptext(service.getLibelle());
						 etapeItem.setValue(service);
						 etapeItem.setOpen(false);
						 if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
							 if(codeactivite.intValue()==service.getId().intValue())
							 {
								 etapeItem.setSelected(true);
								 debutItem.setOpen(true);
								
							 }
								 
						 }
						 etapeItem.setParent(procedureTreechildren);
						
				
			 List <SygCategori> listNivo3 =BeanLocator.defaultLookup(CategoriSession.class)
			 .findRech(0, -1,null,Long.parseLong("3"),service.getId());
				System.out.println("==============================ok"+listNivo3.size());
				
			 if(listNivo3.size() > 0){
			 Treechildren etapeTreechildren = new Treechildren();
			 for (SygCategori autreservice : listNivo3) {
			 Treeitem tacheItem = new Treeitem();
			
			 tacheItem.setLabel(autreservice.getLibelle());
			 tacheItem.setTooltiptext(autreservice.getLibelle());
			 tacheItem.setValue(autreservice);
			 tacheItem.setOpen(false);
			 if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				 if(codeactivite.intValue()==autreservice.getId().intValue())
				 {
					 tacheItem.setSelected(true); 
					 debutItem.setOpen(true);
					 etapeItem.setOpen(true);
				 }
					 
			 }
			 tacheItem.setParent(etapeTreechildren);
		  
			 }
			 etapeTreechildren.setParent(etapeItem);
			 }
			 }
			 procedureTreechildren.setParent(debutItem);
			 }
		   }
		
		}


		public void removeTreeChildren() {
			while (child.getItemCount() > 0) {
				child.removeChild(child.getFirstChild());
			}
		}


		public void onOpen$tree() {

			System.out.println(tree.getSelectedItem().getId());
		}
}