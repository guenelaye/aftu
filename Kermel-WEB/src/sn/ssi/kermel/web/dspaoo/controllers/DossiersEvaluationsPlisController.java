package sn.ssi.kermel.web.dspaoo.controllers;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class DossiersEvaluationsPlisController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code;
	private Tab TAB_infosgenerales,TAB_garanties,TAB_piecesadministratives,TAB_criteresqualifications,TAB_devise,TAB_financement;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblInfos,lblevaattrprovisoire,lblevaluation,lbltransmissiondossier,lblcontrolegarantie,lblverificationconformite,lblformcorrectionoffre,
	lblvercriteresqualification,lblformclassementfinal,lblrapportevaluation,lblattributionprovisoire,lblsouscriptiondumarche,lblpublicationattributionprovisoire;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	SygDossiers dossier=new SygDossiers();
	private int registredepotverifies,criteresqualificationssoum,documents;
	SygPlisouvertures soumissionnaires=new SygPlisouvertures();
	private int nombre=0,attributaires;
	 
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=(SygDossiers) Executions.getCurrent().getAttribute("dossier");
		if(dossier!=null)
		{
			registredepotverifies=(Integer) Executions.getCurrent().getAttribute("registredepotverifies");
			criteresqualificationssoum=(Integer) Executions.getCurrent().getAttribute("criteresqualificationssoum");
			documents=(Integer) Executions.getCurrent().getAttribute("documents");
			soumissionnaires=(SygPlisouvertures) Executions.getCurrent().getAttribute("soumissionnaires");
			attributaires=(Integer) Executions.getCurrent().getAttribute("attributaires");
			
			lblevaattrprovisoire.setStyle("color:#0066FF;font-size:20px; ");
			lblevaluation.setStyle("color:#0066FF;font-size:20px; ");
		
			 if(dossier.getDateRemiseDossierTechnique()!=null&&dossier.getDateLimiteDossierTechnique()!=null)
				{
				 lbltransmissiondossier.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				 nombre=nombre+1;
				}
			 if(registredepotverifies>0)
			 {
				 lblverificationconformite.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				 nombre=nombre+1;
			 }
			 if(criteresqualificationssoum>0)
			 {
				 lblvercriteresqualification.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				 nombre=nombre+1;
			 }
			 if(documents>0)
			 {
				 lblrapportevaluation.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				 nombre=nombre+1;
			 }
			 
			 if(soumissionnaires!=null)
			 {
				 lblformclassementfinal.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				 nombre=nombre+1;
			 }
			 if(nombre==5)
			 {
				 lblcontrolegarantie.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				 lblevaluation.setStyle("color:#00FF00;font-family: sans-serif; font-size: 20px;");
				 lblformclassementfinal.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				 lblformcorrectionoffre.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
			 }
			 if(attributaires==dossier.getDosNombreLots())
				{
				 nombre=nombre+1;
				 lblattributionprovisoire.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				}
			 
			 if(dossier.getDosDateMiseValidationattribution()!=null)
				{
				 nombre=nombre+1;
				 lblsouscriptiondumarche.setStyle("color:#00FF00;font-family: sans-serif; font-size: 14px");
					if(dossier.getDosDateValidationPrequalif()==null)
						lblsouscriptiondumarche.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.enattentevalidation"));
						else
						{
							lblsouscriptiondumarche.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire.validele")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateValidationPrequalif()));
							lblsouscriptiondumarche.setTooltip(Labels.getLabel("kermel.plansdepassation.proceduresmarches.evaattrprovisoire.pubattriprovisoire.valideles")+" "+UtilVue.getInstance().formateLaDate2(dossier.getDosDateValidationPrequalif()));
							
						}
				 lblsouscriptiondumarche.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				}
			 if(dossier.getDosDatePublicationProvisoire()!=null)
				{
				 nombre=nombre+1;
				 lblpublicationattributionprovisoire.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				}
			 if(nombre==8)
			 {
				 lblevaattrprovisoire.setStyle("color:#00FF00;font-family: sans-serif; font-size: 20px;");
			 }
		}
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	public void onClick$lbltransmissiondossier() throws InterruptedException {
		session.setAttribute("libelle", "formtransmissiondossier");
		loadApplicationState("suivi_dspaoo");
	}
	
	public void onClick$lblcontrolegarantie() throws InterruptedException {
		session.setAttribute("libelle", "controlegarantie");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$lblverificationconformite() throws InterruptedException {
		session.setAttribute("libelle", "formverificationconformite");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$lblformcorrectionoffre() throws InterruptedException {
		session.setAttribute("libelle", "formcorrectionoffre");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$lblvercriteresqualification() throws InterruptedException {
		session.setAttribute("libelle", "vercriteresqualification");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$lblrapportevaluation() throws InterruptedException {
		session.setAttribute("libelle", "rapportevaluation");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$lblformclassementfinal() throws InterruptedException {
		session.setAttribute("libelle", "formclassementfinal");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$lblattributionprovisoire() throws InterruptedException {
		session.setAttribute("libelle", "attributionprovisoire");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$lblsoumissionpourvalidationattributionprovisoire() throws InterruptedException {
		session.setAttribute("libelle", "soumissionpourvalidationattributionprovisoire");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$lblpublicationattributionprovisoire() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				
		}
		else
		{
			 if(dossier.getDosDateValidationPrequalif()==null)
			 {
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validerdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			 }
			 else
			 {
				session.setAttribute("libelle", "publicationattributionprovisoire");
				loadApplicationState("suivi_dspaoo");
			 }
		}
	}
}