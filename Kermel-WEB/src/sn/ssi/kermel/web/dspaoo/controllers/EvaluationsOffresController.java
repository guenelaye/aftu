package sn.ssi.kermel.web.dspaoo.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabpanel;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class EvaluationsOffresController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code;
	private Tabpanel tabDetailsfecp;
	private Include incDetailsfecp;
	private Tab TAB_formtransmissiondossier,TAB_controlegarantie,TAB_verificationconformite,TAB_correctionoffres,TAB_vercriteresqualification,
	TAB_classementfinal,TAB_rapportevaluation;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblInfos,lbltransmissiondossier,lblcontrolegarantie,lblverificationconformite,lblcorrectionoffres,lblvercriteresqualification,
	lblclassementfinal,lblrapportevaluation;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygDossiers dossier=new SygDossiers();
	private int registredepotverifies,criteresqualificationssoum,documents;
	SygPlisouvertures soumissionnaires=new SygPlisouvertures();
	private int nombre=0;
	
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		
		dossier=(SygDossiers) Executions.getCurrent().getAttribute("dossier");
		if(dossier!=null)
		{
			registredepotverifies=(Integer) Executions.getCurrent().getAttribute("registredepotverifies");
			criteresqualificationssoum=(Integer) Executions.getCurrent().getAttribute("criteresqualificationssoum");
			documents=(Integer) Executions.getCurrent().getAttribute("documents");
			soumissionnaires=(SygPlisouvertures) Executions.getCurrent().getAttribute("soumissionnaires");
				
			if(dossier.getDateRemiseDossierTechnique()!=null||dossier.getDateLimiteDossierTechnique()!=null)
				{
				 lbltransmissiondossier.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				 nombre=nombre+1;
				}
			 if(registredepotverifies>0)
			 {
				 lblverificationconformite.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				 nombre=nombre+1;
			 }
			 if(criteresqualificationssoum>0)
			 {
				 lblvercriteresqualification.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				 nombre=nombre+1;
			 }
			 if(documents>0)
			 {
				 lblrapportevaluation.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				 nombre=nombre+1;
			 }
			 
			 if(soumissionnaires!=null)
			 {
				 lblclassementfinal.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				 nombre=nombre+1;
			 }
			 if(nombre==5)
			 {
				 lblcontrolegarantie.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
				 lblcorrectionoffres.setStyle("color:#00FF00;cursor:pointer;font-family: sans-serif; font-size: 16px;");
			 }

		}
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	public void onClick$transmissiondossier(){
		session.setAttribute("libelle", "formtransmissiondossier");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$controlegarantie(){
		session.setAttribute("libelle", "controlegarantie");
		loadApplicationState("suivi_dspaoo");
	}
	
	public void onClick$verificationconformite(){
		session.setAttribute("libelle", "formverificationconformite");
		loadApplicationState("suivi_dspaoo");
	}
	
	public void onClick$correctionoffres(){
		session.setAttribute("libelle", "formcorrectionoffre");
		loadApplicationState("suivi_dspaoo");
	}
	
	public void onClick$vercriteresqualification(){
		session.setAttribute("libelle", "vercriteresqualification");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$classementfinal(){
		session.setAttribute("libelle", "formclassementfinal");
		loadApplicationState("suivi_dspaoo");
	}
	public void onClick$rapportevaluation(){
		session.setAttribute("libelle", "rapportevaluation");
		loadApplicationState("suivi_dspaoo");
	}
	
}