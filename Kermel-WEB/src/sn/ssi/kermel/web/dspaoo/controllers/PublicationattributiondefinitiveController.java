package sn.ssi.kermel.web.dspaoo.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAttributions;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.AttributionsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DocumentsSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class PublicationattributiondefinitiveController extends AbstractWindow implements
		 AfterCompose {

	private Listbox lstListe,lstBailleur;
	private Paging pgPagination,pgBailleur;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtcommentaires;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference,libellebailleur=null;
    private int parent;
     List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private Bandbox bdBailleur;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherBailleur,txtChapitre;
    SygBailleurs bailleur=null;
    private Decimalbox dcmontantprevu;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idbailleur;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuValider;
	private Datebox dtdatepublication;
	SygAttributions attributaire=new SygAttributions();
	List<SygAttributions> attributaires = new ArrayList<SygAttributions>();
	private Textbox txtVersionElectronique;
	private String nomFichier;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	private Image image;
	private Label lbltitre,lbldeuxpoints;
	private Div step0,step1;
	private Iframe idIframe;
	private String extension,images;
	List<SygDocuments> documents = new ArrayList<SygDocuments>();
	SygDocuments document=new SygDocuments();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	   
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if((dossier==null))
		{
			menuValider.setDisabled(true);
		}
		else
		{
			attributaires=BeanLocator.defaultLookup(AttributionsSession.class).find(0, -1, dossier, null);
			if(dossier.getDosDatePublicationDefinitive()!=null)
			{
				menuValider.setDisabled(true);
				dtdatepublication.setValue(dossier.getDosDatePublicationDefinitive());
				txtcommentaires.setValue(dossier.getDosCommentaireAttributionDefinitive());
				infos(dossier,appel);
			}
		}
	}

private boolean checkFieldConstraints() {
		
		try {
		
			if(dtdatepublication.getValue()==null)
		     {
               errorComponent = dtdatepublication;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.pubattribdefinitive.date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((dtdatepublication.getValue()).after(new Date()))
			 {
				errorComponent = dtdatepublication;
				errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.approbation.pubattribdefinitive.date")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.referentiel.date.dujour")+": "+UtilVue.getInstance().formateLaDate(new Date());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			if(txtVersionElectronique.getValue().equals(""))
		     {
         errorComponent = txtVersionElectronique;
         errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.documents.pvattributiondefinitive")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			return true;
			
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

		public void onClick$menuValider() {
			if(checkFieldConstraints())
			{
				dossier.setDosDatePublicationDefinitive(dtdatepublication.getValue());
				dossier.setDosCommentaireAttributionDefinitive(txtcommentaires.getValue());
				BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).update(dossier);
				
				
				for (int i = 0; i < attributaires.size(); i++) {
					attributaire=attributaires.get(i);
					attributaire.setDatePublicationDefinitive(dtdatepublication.getValue());
					BeanLocator.defaultLookup(AttributionsSession.class).update(attributaire);
				}
				
				documents=BeanLocator.defaultLookup(DocumentsSession.class).find(0, -1, dossier, appel,UIConstants.PARAM_TYPEDOCUMENTS_ATTIBUTIONSDEFINITIVE,null, null);
				if(documents.size()>0)
				document=documents.get(0);
				document.setAppel(appel);
				document.setDossier(dossier);
				document.setNomFichier(txtVersionElectronique.getValue());
				document.setTypeDocument(UIConstants.PARAM_TYPEDOCUMENTS_ATTIBUTIONSDEFINITIVE);
				document.setLibelle(Labels.getLabel("kermel.plansdepassation.proceduresmarches.documents.pvattributiondefinitive"));
				document.setDate(new Date());
				document.setHeure(Calendar.getInstance().getTime());
				if(documents.size()==0)
				{
					BeanLocator.defaultLookup(DocumentsSession.class).save(document);
				}
				else
				{
					BeanLocator.defaultLookup(DocumentsSession.class).update(document);
				}
				session.setAttribute("libelle", "publicationattributiondefinitive");
				loadApplicationState("suivi_dspaoo");
				
				
			}
		}
		
		public void infos(SygDossiers dossier,SygAppelsOffres appel) {
			if(dossier.getDosDateSignature()!=null)
			{
				menuValider.setDisabled(true);
				extension=dossier.getDosfichierSignature().substring(dossier.getDosfichierSignature().length()-3,  dossier.getDosfichierSignature().length());
				 if(extension.equalsIgnoreCase("pdf"))
					 images="/images/icone_pdf.png";
				 else  
					 images="/images/word.jpg";
				 
				image.setVisible(true);
				image.setSrc(images);
				lbldeuxpoints.setValue(":");
				lbltitre.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.procesverbalouverture.versionfichier"));
				dtdatepublication.setValue(dossier.getDosDateSignature());
			}
		}
		public void onClick$btnChoixFichier() {
			//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
				if (ToolKermel.isWindows())
					nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
				else
					nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

				txtVersionElectronique.setValue(nomFichier);
			}
		
		public void onClick$image() {
			step0.setVisible(false);
			step1.setVisible(true);
			String filepath = cheminDossier + dossier.getDosfichierSignature();
			File f = new File(filepath.replaceAll("\\\\", "/"));

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(f, null, null);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (mymedia != null)
				idIframe.setContent(mymedia);
			else
				idIframe.setSrc("");

			idIframe.setHeight("600px");
			idIframe.setWidth("100%");
		}
		
		public org.zkoss.util.media.AMedia fetchFile(File file) {

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(file, null, null);
				return mymedia;
			} catch (Exception e) {

				e.printStackTrace();
				return null;
			}

		}
		public void onClick$menuFermer() {
			step0.setVisible(true);
			step1.setVisible(false);
		}
}