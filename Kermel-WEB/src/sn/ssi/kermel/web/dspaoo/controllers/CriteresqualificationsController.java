package sn.ssi.kermel.web.dspaoo.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCritere;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersSouscriteresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.LotsSession;
import sn.ssi.kermel.be.referentiel.ejb.CritereSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class CriteresqualificationsController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstCriteres,lstLots;
	private Paging pgPagination,pgCriteres,pgLots;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
  	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	SygCritere critere=new SygCritere();
	SygCritere newcritere=new SygCritere();
	SygDossiers dossier=new SygDossiers();
	private Menuitem menuAjouter,menuSupprimer,menuNouveauCritere;
	private Div step0,step1,step2,step00;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private Label lbStatusBar,lblLot,lblLots;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Textbox txtlibelle;
    SygLots lot;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
    	
		addEventListener(ApplicationEvents.ON_CRITERES, this);
		lstCriteres.setItemRenderer(new PiecesRenderer());
		pgCriteres.setPageSize(byPage);
		pgCriteres.addForward("onPaging", this, ApplicationEvents.ON_CRITERES);
		
		addEventListener(ApplicationEvents.ON_LOTS, this);
		lstLots.setItemRenderer(new LotsRenderer());
		pgLots.setPageSize(byPage);
		pgLots.addForward("onPaging", this, ApplicationEvents.ON_LOTS);
	}


	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier==null)
		{
			menuAjouter.setDisabled(true);
			menuSupprimer.setDisabled(true);
			menuNouveauCritere.setDisabled(true);
		}
		else
		{
			if(dossier.getDosDateMiseValidation()!=null||dossier.getDosDatePublication()!=null)
			{
				menuAjouter.setDisabled(true);
				menuSupprimer.setDisabled(true);
				menuNouveauCritere.setDisabled(true);
			}
			Events.postEvent(ApplicationEvents.ON_LOTS, this, null);
			
		}
		
	}
	
	@Override
	public void onEvent(final Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_LOTS)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgLots.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgLots.getActivePage() * byPage;
				pgLots.setPageSize(byPage);
			}
			 List<SygLots> lots = BeanLocator.defaultLookup(LotsSession.class).find(activePage,byPage,dossier,null);
			 lstLots.setModel(new SimpleListModel(lots));
			 pgLots.setTotalSize(BeanLocator.defaultLookup(LotsSession.class).count(dossier,null));
		}
		else	if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
		    List<SygDossierssouscriteres> criteres = BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(activePage,byPage,dossier, null,lot);
			 lstListe.setModel(new SimpleListModel(criteres));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(DossiersSouscriteresSession.class).count(dossier, null,lot));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CRITERES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgCriteres.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgCriteres.getActivePage() * byPage;
				pgCriteres.setPageSize(byPage);
			}
			 List<SygCritere> criteres = BeanLocator.defaultLookup(CritereSession.class).ListeCriteres(activePage,byPage,libelle,dossier.getDosID(),autorite.getId(), lot);
			 lstCriteres.setModel(new SimpleListModel(criteres));
			 pgCriteres.setTotalSize(BeanLocator.defaultLookup(CritereSession.class).ListeCriteres(libelle,dossier.getDosID(),autorite.getId(), lot).size());
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = ((SygDossierssouscriteres)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue()).getId();
				BeanLocator.defaultLookup(DossiersSouscriteresSession.class).delete(codes);
			}
			 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
//			session.setAttribute("libelle", "criteresqualifications");
//			loadApplicationState("suivi_dspaoo");
		}
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygDossierssouscriteres dossiers = (SygDossierssouscriteres) data;
		item.setValue(dossiers);

		 Listcell cellLibelle = new Listcell(dossiers.getCritere().getLibelle());
		 cellLibelle.setParent(item);
		 
		
	}

	
	public class PiecesRenderer implements ListitemRenderer{
		
		
		
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygCritere criteres = (SygCritere) data;
		item.setValue(criteres);

		 Listcell cellLibelle = new Listcell(criteres.getLibelle());
		 cellLibelle.setParent(item);
		 
		
	
	}
	}
	public void onClick$menuSupprimer()
	{
		if (lstListe.getSelectedItem() == null)
			
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		

		HashMap<String, String> display = new HashMap<String, String>(); // permet
		display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
		display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
		display.put(MessageBoxController.DSP_HEIGHT, "250px");
		display.put(MessageBoxController.DSP_WIDTH, "47%");

		HashMap<String, Object> map = new HashMap<String, Object>(); // permet
		map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
		showMessageBox(display, map);
		
	}
	public void onClick$menuAjouter()
	{
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
		step00.setVisible(false);
		Events.postEvent(ApplicationEvents.ON_CRITERES, this, null);
	}
	public void onClick$menuFermer()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
		step00.setVisible(false);
		
	}
	public void onClick$menuValider()
	{
       if (lstCriteres.getSelectedItem() == null)
			throw new WrongValueException(lstCriteres, Labels.getLabel("kermel.error.select.item"));
       
       for (int i = 0; i < lstCriteres.getSelectedCount(); i++) {
    	   SygDossierssouscriteres dossiercritere=new SygDossierssouscriteres();
    	   critere=(SygCritere) ((Listitem) lstCriteres.getSelectedItems().toArray()[i]).getValue();
    	   dossiercritere.setCritere(critere);
    	   dossiercritere.setDossier(dossier);
    	   dossiercritere.setLot(lot);
    	   BeanLocator.defaultLookup(DossiersSouscriteresSession.class).save(dossiercritere);
       }
         Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	     step0.setVisible(true);
		 step1.setVisible(false);
		 step2.setVisible(false);
		 step00.setVisible(false);
//       session.setAttribute("libelle", "criteresqualifications");
//		loadApplicationState("suivi_dspaoo");
	}
	
	public void onClick$menuNouveauCritere()
	{
		step0.setVisible(false);
		step1.setVisible(false);
		step2.setVisible(true);
		step00.setVisible(false);
	}
	
	 public void onClick$menuValiderNouveau()  {
		 if(checkFieldConstraints())
		 {
		 newcritere.setLibelle(txtlibelle.getValue());
		 newcritere.setAutorite(autorite);
		 newcritere= BeanLocator.defaultLookup(CritereSession.class).save(newcritere); 
		 
		 SygDossierssouscriteres dossiercritere=new SygDossierssouscriteres();
  	     dossiercritere.setCritere(newcritere);
  	     dossiercritere.setDossier(dossier);
  	     dossiercritere.setLot(lot);
  	     BeanLocator.defaultLookup(DossiersSouscriteresSession.class).save(dossiercritere);
  	     Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
  	     step0.setVisible(true);
		 step1.setVisible(false);
		 step2.setVisible(false);
		 step00.setVisible(false);
		 }

	 }
	 
	 private boolean checkFieldConstraints() {
			try {
				
				 if(txtlibelle.getValue().equals(""))
			     {
				 errorComponent = txtlibelle;
				 errorMsg = Labels.getLabel("kermel.referentiel.common.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				 lbStatusBar.setStyle(ERROR_MSG_STYLE);
				 lbStatusBar.setValue(errorMsg);
				 throw new WrongValueException (errorComponent, errorMsg);
			    }
				
				
				return true;
					
			}
			catch (Exception e) {
				errorMsg = Labels.getLabel("kermel.erreur.champobligatoire") + ": " + e.toString()
				+ " [checkFieldConstraints]";
				errorComponent = null;
				return false;

				
			}
			
		}
	
	 public void onClick$menuFermerNouveau()
		{
			step0.setVisible(true);
			step1.setVisible(false);
			step2.setVisible(false);
			step00.setVisible(false);
		}
	 
	 public class LotsRenderer implements ListitemRenderer{
			
			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
				SygLots lots = (SygLots) data;
				item.setValue(lots);

				 Listcell cellNumero = new Listcell(lots.getNumero());
				 cellNumero.setParent(item);
				 
				 Listcell cellLibelle = new Listcell(lots.getLibelle());
				 cellLibelle.setParent(item);
				 
					 
				 Listcell cellCommentaires = new Listcell(lots.getCommentaires());
				 cellCommentaires.setParent(item);
				 
			}
	}
	 
	 public void onClick$menuCritere()
		{
		 if (lstLots.getSelectedItem() == null)
				throw new WrongValueException(lstLots, Labels.getLabel("kermel.error.select.item"));
		    lot=(SygLots) lstLots.getSelectedItem().getValue();
		    lblLot.setValue(lot.getLibelle());
		    lblLots.setValue(lot.getLibelle());
		    Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		   step00.setVisible(false);
		   step0.setVisible(true);
		   step1.setVisible(false);
		   step2.setVisible(false);
		}
	 
	 public void onClick$menuCancel()
	 {
		   step00.setVisible(true);
		   step0.setVisible(false);
		   step1.setVisible(false);
		   step2.setVisible(false);
		   session.setAttribute("libelle", "criteresqualifications");
		  loadApplicationState("suivi_dspaoo");
	 }
}