package sn.ssi.kermel.web.dspaoo.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class ValidationAttributionprovisoireController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CONFIRMVALIDER="CONFIRMVALIDER";
     String page=null;
     Session session = getHttpSession();
    SygDossiers dossier=new SygDossiers();
     String login,naturevalidation,autorite;
    private Listbox lstListe;
   	private int gestion=-1;
	private Intbox intgestion;
	public Textbox txtDenomination;
	 
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
		
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		
		lstListe.setItemRenderer(this);
		
			
	}


	public void onCreate(CreateEvent event) {
		
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygDossiers> dossiers = BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).ValidationAttributionprovisoire(activePage,byPage,  autorite, UIConstants.DOSSIERAVALIDER, gestion);
			 SimpleListModel listModel = new SimpleListModel(dossiers);
			 lstListe.setModel(listModel);
			 pgPagination.setTotalSize(BeanLocator.defaultLookup( DossiersAppelsOffresSession.class).countValidationAttributionprovisoire( autorite, UIConstants.DOSSIERAVALIDER, gestion));
		} 
		else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
			Listcell button = (Listcell) event.getTarget();
			String toDo = (String) button.getAttribute(UIConstants.TODO);
			String nature = (String) button.getAttribute("naturevalidation");
			dossier=(SygDossiers) button.getAttribute("dossiers");
			if (toDo.equalsIgnoreCase("valider"))
			{
			final String uri = "/passationsmarches/dspaoo/formvalidation.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"650px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.validatioattribution"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ValidationFormController.PARAM_WINDOW_CODE, dossier.getDosID());
			data.put(ValidationFormController.PARAM_WINDOW_MODE, nature);
			data.put(ValidationFormController.PARAM_WINDOW_ATTRIBUTION, 1);
			showPopupWindow(uri, data, display);
			}
			
		}

	
	}

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygDossiers dossiers = (SygDossiers) data;
		item.setValue(dossiers.getDosID());
		
		Listcell cellAutorite = new Listcell(dossiers.getAutorite().getDenomination());
		cellAutorite.setParent(item);
		 
		Listcell cellReference = new Listcell(dossiers.getAppel().getAporeference());
		cellReference.setParent(item);
		 
		Listcell cellObjet = new Listcell(dossiers.getAppel().getApoobjet());
		cellObjet.setParent(item);
		 
		Listcell cellModepassation = new Listcell(dossiers.getAppel().getModepassation().getLibelle());
		cellModepassation.setParent(item);
		 
		
		Listcell cellMontant = new Listcell(ToolKermel.format2Decimal(dossiers.getAppel().getApomontantestime()));
		cellMontant.setParent(item);

	
		Listcell cellImageValider = new Listcell();
	
		naturevalidation="validationattribution";
		cellImageValider.setImage("/images/ok.png");
		cellImageValider.setAttribute(UIConstants.TODO, "valider");
		cellImageValider.setAttribute("naturevalidation", naturevalidation);
		cellImageValider.setAttribute("dossiers", dossiers);
		cellImageValider.setTooltiptext(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validerdossier"));
		cellImageValider.addEventListener(Events.ON_CLICK, ValidationAttributionprovisoireController.this);
    	
		cellImageValider.setParent(item);

	}
	

	
	public void  onClick$btnchercher(){
		
		if(intgestion.getValue()==null)
		 {
			gestion=-1;
			
		 }
		else
		 {
			gestion=intgestion.getValue();
			page="0";
		 }

		if((txtDenomination.getValue().equalsIgnoreCase(Labels.getLabel("kermel.autoritecontractante.libelle")))||(txtDenomination.getValue().equals("")))
		 {
			autorite=null;
			
		 }
		else
		{
			autorite=txtDenomination.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
	

		public void onOK$intgestion(){
			onClick$btnchercher();	
		}
		public void onOK$btnchercher(){
			onClick$btnchercher();	
		}
		
		public void onOK$txtDenomination()
		{
			onClick$btnchercher();
		}

		public void onFocus$txtDenomination()
		{
			if(txtDenomination.getValue().equalsIgnoreCase(Labels.getLabel("kermel.autoritecontractante.libelle")))
				txtDenomination.setValue("");
		
		}
		public void onBlur$txtDenomination()
		{
			if(txtDenomination.getValue().equals(""))
				txtDenomination.setValue(Labels.getLabel("kermel.autoritecontractante.libelle"));
		}
}