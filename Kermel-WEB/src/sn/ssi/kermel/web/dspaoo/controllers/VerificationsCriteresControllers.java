package sn.ssi.kermel.web.dspaoo.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Row;
import org.zkoss.zul.RowRenderer;
import org.zkoss.zul.RowRendererExt;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCriteresQualificationsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygLotsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.CriteresQualificationsSoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersSouscriteresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.LotsSoumissionnairesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class VerificationsCriteresControllers extends AbstractWindow
		implements AfterCompose, EventListener,RowRenderer, RowRendererExt {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String mode, libDist, libEnqueteur, libMarche;
	int idProjet;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private final int byPage1 = 5;
	private Object selobj;
	// private Session session = getHttpSession();
	Grid GridLots;
	Paging pgPagination, pgDist, pgEnqueteur, pgMarche;
	Listbox lstDist, listEnqueteur, listMarche;
	Integer idFiche, distID, enqueteurID, marcheID;
	Textbox txtnumero, txtstatu, txtDrs, txtDist, anneemoi, txtRechercherDist,
			txtRechercherEnqueteur, txtObservation;
	Label nbre, lblMarche, lblEnqueteur, lblDate, lblRegion, lblDepartement,
			lblCampagne, lblSup1, lblSup2;
	Datebox datecree;
	Intbox nbretotalfs, nbretotalconsmois;
	Bandbox bdservice, bdDist, bdFmnsist, bdEnqueteur, bdMarche;
	Button btnGenerer, btnRechercherDist, btnRechercherMarcher,
			btnRechercherEnqueteur;
	Menuitem menuPreviousStep1;
	// Prodistrict district;
	// Proregionmedical regMed;
	private Div step1;
	private Iframe iframePrint;
	private boolean estGenere = false;
	private String datejour = UtilVue.getInstance().formateLaDate(new Date());
	private int annee = Integer.parseInt(ToolKermel.getAnneeCourante()),nombreexamen=0,nbreexamen=0;
	private String mois = datejour.substring(3, 5);
	Session session = getHttpSession();
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	SygDossiers dossier=new SygDossiers();
    SygAutoriteContractante autorite;
	List<SygCriteresQualificationsSoumissionnaires> criteressoum = new ArrayList<SygCriteresQualificationsSoumissionnaires>();
	private String raisonsocial=null,libellelot=null,libellecritere;
	private Textbox txtSoumissionnaire,txtLot,txtCritere;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
	


	}

	public void onCreate(final CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);

		if(dossier!=null)
		{
			createLigneLots(dossier);
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			
		}
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {


			GridLots.setRowRenderer(this);

			GridLots.setRowRenderer(this);

		    List<SygCriteresQualificationsSoumissionnaires> valLignes = new ArrayList<SygCriteresQualificationsSoumissionnaires>();
		    List<SygPlisouvertures> plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,-1,-1,1, -1, -1, -1, raisonsocial, -1, null, null);
		    List<SygLotsSoumissionnaires> lots = new ArrayList<SygLotsSoumissionnaires>();
		    List<SygCriteresQualificationsSoumissionnaires> vals = new ArrayList<SygCriteresQualificationsSoumissionnaires>();
			
		    
		  for (SygPlisouvertures pli : plis) {
			  nombreexamen=0;
			  SygCriteresQualificationsSoumissionnaires categ=new SygCriteresQualificationsSoumissionnaires();
			 
				 
		      categ.setLibelle(pli.getRetrait().getNomSoumissionnaire());
		      lots = BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).find(0, -1, dossier, pli, null,-1,null,-1,-1, -1, libellelot);
		   	   if(lots .size()!=0)   
			     valLignes.add(categ);	
	 		       for (SygLotsSoumissionnaires  lot : lots ) {
	 		    	  nbreexamen=0;
	 		    	  SygCriteresQualificationsSoumissionnaires categlot=new SygCriteresQualificationsSoumissionnaires();
	 		    	  categlot.setLibellelot(lot.getLot().getLibelle()); 
          	    	    vals = BeanLocator.defaultLookup(CriteresQualificationsSoumissionnairesSession.class).find(0, -1, dossier, pli, null,-1, lot.getLot(), libellecritere);
          	    	     if(vals.size()!=0)   
     				      valLignes.add(categlot);
          	    	      for (SygCriteresQualificationsSoumissionnaires valLigne : vals) {
			 		         valLignes.add(valLigne);
			 		           if(valLigne.getConforme()==1)
				 		          nbreexamen=nbreexamen+1;
				                }
          	    	    if(nbreexamen==vals.size())
          	    	    {  
          	    	    	nombreexamen=nombreexamen+1;
          	    	    	lot.setPlilcritereQualification(UIConstants.MOINSDISANTQUALIFIE);
          	    	    }
      	 		    	 
      		 		    else
      		 		    	lot.setPlilcritereQualification(0);
      		 		    BeanLocator.defaultLookup(LotsSoumissionnairesSession.class).update(lot);
			        }
	 		     
	 		      if(nombreexamen==lots.size())
	 		    	  pli.setCritereQualification(UIConstants.MOINSDISANTQUALIFIE);
		 		   else
		 		     pli.setCritereQualification(0);
		 		   BeanLocator.defaultLookup(RegistrededepotSession.class).update(pli);
			  }
		  GridLots.setModel(new ListModelList(valLignes));					
			
		     	
		} 
		
								

	

		

	}


	

	 
	   @Override
		public Row newRow(Grid grid) {
			// Create EditableRow instead of Row(default)
			Row row = new EditableRow();
			row.applyProperties();
			return row;
		}

		@Override
		public Component newCell(Row row) {
			return null;// Default Cell
		}

		@Override
		public int getControls() {
			return RowRendererExt.DETACH_ON_RENDER; // Default Value
		}

		@Override
		public void render(Row row, Object data, int index) throws Exception {
			final SygCriteresQualificationsSoumissionnaires critere = (SygCriteresQualificationsSoumissionnaires) data;
			
			final EditableRow editRow = (EditableRow) row;
			
			if (critere.getLibelle() != null||critere.getLibellelot() != null) {
				if (critere.getLibellelot() == null) {
					final EditableDiv libelle =	new EditableDiv(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial")+":  " +critere.getLibelle(),false);
					libelle.setHeight("30px");
		 			libelle.setStyle("color:#000;");
					libelle.txb.setReadonly(true);
					libelle.setParent(editRow);	
					
						
					final EditableDiv etat =	new EditableDiv(" ",false);
					etat.setAlign("center");
					etat.txb.setReadonly(true);
					etat.setParent(editRow);	
					
					final EditableDiv action =	new EditableDiv(" ",false);
					action.setAlign("center");
					action.txb.setReadonly(true);
					action.setParent(editRow);	
				}
				else
				{
					final EditableDiv libelle =	new EditableDiv(Labels.getLabel("kermel.plansdepassation.proceduresmarches.travauxdrp.lotslot")+":  " +critere.getLibellelot(),false);
					libelle.setHeight("30px");
		 			libelle.setStyle("margin-left:10px;color:#000;");
					libelle.txb.setReadonly(true);
					libelle.setParent(editRow);	
					
						
					final EditableDiv etat =	new EditableDiv(" ",false);
					etat.setAlign("center");
					etat.txb.setReadonly(true);
					etat.setParent(editRow);	
					
					final EditableDiv action =	new EditableDiv(" ",false);
					action.setAlign("center");
					action.txb.setReadonly(true);
					action.setParent(editRow);	
				}
				
			}
			else
			{
				
			
			final EditableDiv libelle =	new EditableDiv(critere.getCritere().getCritere().getLibelle(),false);
			libelle.setAlign("left");
			libelle.setStyle("margin-left:20px;color:#000;");
			//libelle.setStyle("font-weight:bold;color:green");
			libelle.txb.setReadonly(true);
			libelle.setParent(editRow);	
			
				
			String etat="";
			if(critere.getConforme()==1)
				etat="oui";
			else
				etat="non";
			final EditableCheckDiv lotsoumis =	new EditableCheckDiv(etat,false);
			lotsoumis.setAlign("center");
			lotsoumis.setStyle("font-weight:bold;color:green");
			if(etat.equals("oui"))
				lotsoumis.checkbox.setChecked(true);
			else
				lotsoumis.checkbox.setChecked(false);
			lotsoumis.setParent(editRow);	
		   	
	   	
			final Div ctrlDiv = new Div();
			ctrlDiv.setParent(editRow);
			final Button editBtn = new Button(null, "/images/pencil-small.png");
			editBtn.setMold("os");
			editBtn.setHeight("20px");
			editBtn.setWidth("30px");
//			if(dossier.getDosLotDivisible().equals("NON"))
//				editBtn.setDisabled(true);
	
			editBtn.setParent(ctrlDiv);
			// Button listener - control the editable of row
			editBtn.addEventListener(Events.ON_CLICK, new EventListener() {
				public void onEvent(Event event) throws Exception {
					final Button submitBtn = (Button) new Button(null, "/images/tick-small.png");
					final Button cancelBtn = (Button) new Button(null, "/images/cross-small.png");

					submitBtn.setMold("os");
					submitBtn.setHeight("20px");
					submitBtn.setWidth("30px");
					submitBtn.setTooltiptext("Valider la saisie");
					submitBtn.addEventListener(Events.ON_CLICK, new EventListener() {
						public void onEvent(Event event) throws Exception {

							editRow.toggleEditable(true);


							if(lotsoumis.checkbox.isChecked()==true)
								critere.setConforme(1);
							else
								critere.setConforme(0);
							BeanLocator.defaultLookup(CriteresQualificationsSoumissionnairesSession.class).update(critere);
							submitBtn.detach();
							cancelBtn.detach();
							editBtn.setParent(ctrlDiv);
							session.setAttribute("libelle", "vercriteresqualification");
	 						loadApplicationState("suivi_dspaoo");
						}
					});
					lotsoumis.addEventListener(Events.ON_OK, new EventListener() {
	 					public void onEvent(Event event) throws Exception {

	 						editRow.toggleEditable(true);
	 						
	 						if(lotsoumis.checkbox.isChecked()==true)
								critere.setConforme(1);
							else
								critere.setConforme(0);
							BeanLocator.defaultLookup(CriteresQualificationsSoumissionnairesSession.class).update(critere);
	 						submitBtn.detach();
	 						cancelBtn.detach();
	 						editBtn.setParent(ctrlDiv);
	 						session.setAttribute("libelle", "vercriteresqualification");
	 						loadApplicationState("suivi_dspaoo");
	 					}
	 				});
					cancelBtn.setMold("os");
					cancelBtn.setHeight("20px");
					cancelBtn.setWidth("30px");
					cancelBtn.setTooltiptext("Annuler la saisie");
					cancelBtn.addEventListener(Events.ON_CLICK, new EventListener() {
						public void onEvent(Event event) throws Exception {
							editRow.toggleEditable(false);
							submitBtn.detach();
							cancelBtn.detach();
							editBtn.setParent(ctrlDiv);
						}
					});
					submitBtn.setParent(ctrlDiv);
					cancelBtn.setParent(ctrlDiv);
					editRow.toggleEditable(true);
					editBtn.detach();
				}
			});
			}
			
		}
	

		
	     private void createLigneLots(SygDossiers dossier) {

			    
			List<SygDossierssouscriteres> criteres = BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(0,-1,dossier, null, null);
			 List<SygPlisouvertures> plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,-1,-1,1, -1, -1, -1, null, -1, null, null);
			
			for (SygPlisouvertures pli : plis) {
				
				for (int i = 0; i < criteres.size(); i++) {
					criteressoum=BeanLocator.defaultLookup(CriteresQualificationsSoumissionnairesSession.class).find(0, -1, dossier, pli, criteres.get(i),-1, null, null);
					
					if(criteressoum.size()==0)
					{
						SygCriteresQualificationsSoumissionnaires critere = new SygCriteresQualificationsSoumissionnaires();
					
						critere.setDossier(dossier);
						critere.setPlis(pli);
						critere.setCritere(criteres.get(i));
						critere.setConforme(0);
						critere.setLot(criteres.get(i).getLot());
						BeanLocator.defaultLookup(CriteresQualificationsSoumissionnairesSession.class).save(critere);
					}
				}
			
				
			   }
			
			}
	     
	     public void  onClick$btnRechercher(){
		 		if((txtSoumissionnaire.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial")))||(txtSoumissionnaire.getValue().equals("")))
		 		 {
		 			raisonsocial=null;
		 		 }
		 		else
		 		 {
		 			raisonsocial=txtSoumissionnaire.getValue();
		 			
		 		 }
		 		
		 		if((txtLot.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.travauxdrp.lotslot")))||(txtLot.getValue().equals("")))
		 		 {
		 			libellelot=null;
		 		 }
		 		else
		 		 {
		 			libellelot=txtLot.getValue();
		 			
		 		 }
		 		
		 		if((txtCritere.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.criteres")))||(txtCritere.getValue().equals("")))
		 		 {
		 			libellecritere=null;
		 		 }
		 		else
		 		 {
		 			libellecritere=txtCritere.getValue();
		 			
		 		 }
		 		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		 		}
		 	
		 	
		 	 public void onFocus$txtSoumissionnaire(){
		 			if(txtSoumissionnaire.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial"))){
		 				txtSoumissionnaire.setValue("");
		 			}		 
		 			}
		 		public void onBlur$txtSoumissionnaire(){
		 			if(txtSoumissionnaire.getValue().equalsIgnoreCase("")){
		 				txtSoumissionnaire.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial"));
		 			}		 
		 			}
		 		public void onOK$txtSoumissionnaire(){
		 			onClick$btnRechercher();	
		 		}
		 		 public void onFocus$txtLot(){
			 			if(txtLot.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.travauxdrp.lotslot"))){
			 				txtLot.setValue("");
			 			}		 
			 			}
			 		public void onBlur$txtLot(){
			 			if(txtLot.getValue().equalsIgnoreCase("")){
			 				txtLot.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.travauxdrp.lotslot"));
			 			}		 
			 			}
			 		public void onOK$txtLot(){
			 			onClick$btnRechercher();	
			 		}
		 		public void onOK$btnRechercher(){
		 			onClick$btnRechercher();	
		 		}
		 		
		 		 public void onFocus$txtCritere(){
			 			if(txtCritere.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.criteres"))){
			 				txtCritere.setValue("");
			 			}		 
			 			}
			 		public void onBlur$txtCritere(){
			 			if(txtCritere.getValue().equalsIgnoreCase("")){
			 				txtCritere.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.criteres"));
			 			}		 
			 			}
			 		public void onOK$txtCritere(){
			 			onClick$btnRechercher();	
			 		}
	     
}
