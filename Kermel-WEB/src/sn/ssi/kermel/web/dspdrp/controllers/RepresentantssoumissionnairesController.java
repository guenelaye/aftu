package sn.ssi.kermel.web.dspdrp.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygPresenceouverture;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.Presence0uvertureSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class RepresentantssoumissionnairesController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstStructure;
	private Paging pgPagination,pgStructure;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libellemonnaie=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherStructure;
    SygBailleurs bailleur=null;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idrepresentant=null;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	SygPresenceouverture representant=new SygPresenceouverture();
	SygPlisouvertures structure=new SygPlisouvertures();
	private Bandbox bdStructure;
	private Div step0,step1;
	private Menuitem menuAjouter,menuModifier,menuSupprimer;
	private Textbox txtNom,txtPrenom,txtTelephone,txtmail,txtInfos;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
    	lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
		addEventListener(ApplicationEvents.ON_PLIS, this);
		pgStructure.setPageSize(byPage);
		pgStructure.addForward("onPaging", this, ApplicationEvents.ON_PLIS);
		lstStructure.setItemRenderer(new StructuresRenderer());
    
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,-1);
		if(dossier==null)
		{
			menuAjouter.setDisabled(true);
			menuModifier.setDisabled(true);
			menuSupprimer.setDisabled(true);
		}
		else
		{
			if(dossier.getDateRemiseDossierTechnique()!=null)
			{
				menuAjouter.setDisabled(true);
				menuModifier.setDisabled(true);
				menuSupprimer.setDisabled(true);
			}
				
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			Events.postEvent(ApplicationEvents.ON_PLIS, this, null);
		}
		
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygPresenceouverture> representants = BeanLocator.defaultLookup(Presence0uvertureSession.class).find(activePage,byPageBandbox,dossier,null,null,0);
			 lstListe.setModel(new SimpleListModel(representants));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(Presence0uvertureSession.class).count(dossier,null,null,0));
		} 
	
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = ((SygPresenceouverture)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue()).getId();
				BeanLocator.defaultLookup(Presence0uvertureSession.class).delete(codes);
			}
			session.setAttribute("libelle", "representantssoumissionnaires");
			loadApplicationState("suivi_dsp_drp");
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_PLIS)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgStructure.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgStructure.getActivePage() * byPageBandbox;
				pgStructure.setPageSize(byPageBandbox);
			}
			List<SygPlisouvertures> structures = BeanLocator.defaultLookup(RegistrededepotSession.class).find(activePage, byPageBandbox,dossier,null,null,null,-1,-1,-1, -1, -1, -1, null, -1, null, null);
			lstStructure.setModel(new SimpleListModel(structures));
			pgStructure.setTotalSize(BeanLocator.defaultLookup(RegistrededepotSession.class).count(dossier,null,null,null,-1,-1,1, -1, -1, -1,null, -1, null, null));
		}
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygPresenceouverture representants = (SygPresenceouverture) data;
		item.setValue(representants);

		 Listcell cellStructure = new Listcell(representants.getPlis().getRetrait().getNomSoumissionnaire());
		 cellStructure.setParent(item);
		 
		 Listcell cellPrenom = new Listcell(representants.getPrenomrepresentant());
		 cellPrenom.setParent(item);
		 
		 Listcell cellNom = new Listcell(representants.getNomrepresentant());
		 cellNom.setParent(item);
		 
		 Listcell cellTel = new Listcell(representants.getTelephone());
		 cellTel.setParent(item);
		 
		 Listcell cellEmail = new Listcell(representants.getEmail());
		 cellEmail.setParent(item);
	
		 Listcell cellInfos = new Listcell(representants.getSupplementaire());
		 cellInfos.setParent(item);
	}

	public void onClick$menuSupprimer()
	{
		if (lstListe.getSelectedItem() == null)
			
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		

		HashMap<String, String> display = new HashMap<String, String>(); // permet
		display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
		display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
		display.put(MessageBoxController.DSP_HEIGHT, "250px");
		display.put(MessageBoxController.DSP_WIDTH, "47%");

		HashMap<String, Object> map = new HashMap<String, Object>(); // permet
		map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
		showMessageBox(display, map);
		
	}
	public void onClick$menuModifier()
	{
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		representant=(SygPresenceouverture) lstListe.getSelectedItem().getValue();
		idrepresentant=representant.getId();
		structure=representant.getPlis();
		bdStructure.setValue(structure.getRetrait().getNomSoumissionnaire());
		txtNom.setValue(representant.getNomrepresentant());
		txtPrenom.setValue(representant.getPrenomrepresentant());
		txtTelephone.setValue(representant.getTelephone());
		txtmail.setValue(representant.getEmail());
		txtInfos.setValue(representant.getSupplementaire());
		step0.setVisible(false);
		step1.setVisible(true);
	}
	
	///////////Nature///////// 
	public void onSelect$lstStructure(){
		structure= (SygPlisouvertures) lstStructure.getSelectedItem().getValue();
		bdStructure.setValue(structure.getRetrait().getNomSoumissionnaire());
		bdStructure.close();
	
	}
	
	public class StructuresRenderer implements ListitemRenderer{
	
	
	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygPlisouvertures structures = (SygPlisouvertures) data;
		item.setValue(structures);
	
		Listcell cellLibelle = new Listcell(structures.getRetrait().getNomSoumissionnaire());
		cellLibelle.setParent(item);
	
	}
	}
	public void onFocus$txtRechercherStructure(){
	if(txtRechercherStructure.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.presencerepresentantsoum.structurerepresente"))){
		txtRechercherStructure.setValue("");
	}		 
	}
	
	public void  onClick$btnRechercherStructure(){
	if(txtRechercherStructure.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.presencerepresentantsoum.structurerepresente")) || txtRechercherStructure.getValue().equals("")){
		libellemonnaie = null;
		page=null;
	}else{
		libellemonnaie = txtRechercherStructure.getValue();
		page="0";
	}
	Events.postEvent(ApplicationEvents.ON_PLIS, this, page);
	}

	

	private boolean checkFieldConstraints() {
		
		try {
		
			if(bdStructure.getValue().equals(""))
		     {
               errorComponent = bdStructure;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.presencerepresentantsoum.structurerepresente")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtNom.getValue().equals(""))
		     {
              errorComponent = txtNom;
              errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.presencerepresentantsoum.nomrepresentant")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtPrenom.getValue().equals(""))
		     {
             errorComponent = txtPrenom;
             errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.presencerepresentantsoum.prenom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtTelephone.getValue().equals(""))
		     {
            errorComponent = txtTelephone;
            errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.presencerepresentantsoum.telephone")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtmail.getValue().equals(""))
		     {
           errorComponent = txtmail;
           errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.presencerepresentantsoum.mail")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
          if (ToolKermel.ControlValidateEmail(txtmail.getValue())) {
				
			} else {
				errorComponent = txtmail;
                errorMsg = Labels.getLabel("kermel.referentiel.personne.email.incorrect");
 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
 				lbStatusBar.setValue(errorMsg);
 				throw new WrongValueException(errorComponent,errorMsg);
			}
			return true;
			
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	public void  onClick$menuFermer(){
		step0.setVisible(true);
		step1.setVisible(false);
	}
	public void  onClick$menuAjouter(){
		step0.setVisible(false);
		step1.setVisible(true);
		idrepresentant=null;
		bdStructure.setValue("");
		
	}
	public void  onClick$menuValider(){
		if(checkFieldConstraints())
		{
			
			representant.setDossier(dossier);
			representant.setPlis(structure);
			representant.setAppel(appel);
			representant.setNomrepresentant(txtNom.getValue());
			representant.setPrenomrepresentant(txtPrenom.getValue());
			representant.setTelephone(txtTelephone.getValue());
			representant.setEmail(txtmail.getValue());
			representant.setSupplementaire(txtInfos.getValue());
			representant.setEtapePI(0);
			if(idrepresentant==null)
				BeanLocator.defaultLookup(Presence0uvertureSession.class).save(representant);
			else
				BeanLocator.defaultLookup(Presence0uvertureSession.class).update(representant);
			session.setAttribute("libelle", "representantssoumissionnaires");
			loadApplicationState("suivi_dsp_drp");
			
		}
	}
}