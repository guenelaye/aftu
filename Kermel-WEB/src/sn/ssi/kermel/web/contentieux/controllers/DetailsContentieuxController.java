package sn.ssi.kermel.web.contentieux.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.contentieux.ejb.ContentieuxSession;
import sn.ssi.kermel.be.entity.SygContentieux;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class DetailsContentieuxController extends AbstractWindow implements AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private Long code;
	private SygContentieux contentieux = new SygContentieux();
	List<SygContentieux> contentieuxs = new ArrayList<SygContentieux>();
	UtilVue utilVue = UtilVue.getInstance();
	private Label lbldaterecours, lblheure, lblObjet, lblRsociale, lbltxtTel, lblFax, lblEmail, lblAdresse, lblbp, lblMotif, lblnumdecision, lbldatedecision;
	Session session = getHttpSession();
	
	

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		code = (Long) session.getAttribute("numcontentieux");
		contentieux = BeanLocator.defaultLookup(ContentieuxSession.class).findById(code);
		contentieuxs = BeanLocator.defaultLookup(ContentieuxSession.class).findRech(0, -1, code, null, null);
		
		lbldaterecours.setValue(contentieux.getDaterecours().toString());
		lblheure.setValue(contentieux.getHeure().toString());
		lblObjet.setValue(contentieux.getObjet());
		lblRsociale.setValue(contentieux.getRaisonSocial());
		
		
		
		lblAdresse.setValue(contentieux.getAdresse());
		lblMotif.setValue(contentieux.getMotifs());
		lblEmail.setValue(contentieux.getEmail());
		
		 if(contentieux.getBp()!=null)
		  lblbp.setValue(contentieux.getBp().toString());
		 else
          lblbp.setValue("---");
		  
		 if(contentieux.getNumerodecision()!=null)
		   lblnumdecision.setValue(contentieux.getNumerodecision().toString());
		 else
		   lblnumdecision.setValue("---");
			  
		 if(contentieux.getDatedecision()!=null)
		   lbldatedecision.setValue(contentieux.getDatedecision().toString());
		  else
			  lbldatedecision.setValue("---");
		
		 if(contentieux.getTelephone()!="")
		   lbltxtTel.setValue(contentieux.getTelephone());
		else
			lblFax.setValue("---");
		
		
		 if(contentieux.getFax()!="")
		   lblFax.setValue(contentieux.getFax());
		  else
			lblFax.setValue("---");
		
	}

	public void onCreate(CreateEvent createEvent) {

	}

	@Override
	public void onEvent(Event event) throws Exception {

	}

}