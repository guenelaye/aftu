package sn.ssi.kermel.web.contentieux.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;

import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class SuiviContentieuxFormController extends AbstractWindow implements AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String PARAM_WINDOW_CODE = "CODE";
	
	private Long code;
	UtilVue utilVue = UtilVue.getInstance();
	private Include pgActes;
	Session session = getHttpSession();

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

	}

	public void onCreate(CreateEvent createEvent) {

		code = (Long) session.getAttribute("numcontentieux");
		session.setAttribute("numcontentieux", code);
		pgActes.setSrc("/contentieux/dossiersuivi.zul");

	}

	@Override
	public void onEvent(Event event) throws Exception {

	}

	public void onClick$onClose() {
		loadApplicationState("contentieux");
	}
}