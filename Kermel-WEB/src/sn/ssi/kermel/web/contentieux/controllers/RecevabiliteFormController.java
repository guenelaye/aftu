package sn.ssi.kermel.web.contentieux.controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Longbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.contentieux.ejb.ContentieuxSession;
import sn.ssi.kermel.be.contentieux.ejb.PieceJointeContentieuxSession;
import sn.ssi.kermel.be.entity.SygContentieux;
import sn.ssi.kermel.be.entity.SygPieceJointeContentieux;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;


@SuppressWarnings("serial")
public class RecevabiliteFormController extends AbstractWindow implements AfterCompose,EventListener {

	
	
	private Long idpjointeAC,idpjointe;
	

	
	UtilVue utilVue = UtilVue.getInstance();
	
	private Component errorComponent;
	private String errorMsg;
	 private Label lbStatusBar;
	 private static final String ERROR_MSG_STYLE = "color:red";
	 private Datebox daterecours;
		
		private Textbox txtCommentaire;
		private Longbox lgnumero;
		
		private Date datestatut,datejour;
		private String Statut;
		
		private Radiogroup radioDecision;
		private Textbox txtVersionElectronique1;
	// paramétre contentieux
	Long codecontentieux = null;
	private SygContentieux contentieux;
	Session session = getHttpSession();

	 private static final String CONFIRMPUBLIER = "CONFIRMPUBLIER";
	
	//div plaignante
	private SygPieceJointeContentieux pjointe = new SygPieceJointeContentieux();
	private SygPieceJointeContentieux pjointes = new SygPieceJointeContentieux();
	private SygPieceJointeContentieux pjointesAC = new SygPieceJointeContentieux();
	private String nomFichier;
	private Datebox dtdate,dtdatecourrier;
	private Textbox txtLibelles, txtVersionElectronique,txtref;
	private final String cheminDossier = UIConstants.PATH_PJCONT;
	
	//div AC
	//private Label lblDenomination,lblResponsable,lblAdresse,lblTel;
	private SygPieceJointeContentieux pjointeAC = new SygPieceJointeContentieux();
	private Datebox dtdateAC,dtdatecourrierAC;
	private Textbox txtLibellesAC, txtVersionElectroniqueAC,txtrefAC;
	
	//div DNCMP
	private SygPieceJointeContentieux pjointeDNCMP = new SygPieceJointeContentieux();
	private SygPieceJointeContentieux pjointesDNCMP = new SygPieceJointeContentieux();
	private Datebox dtdateDNCMP,dtdatecourrierDNCMP;
	private Textbox txtLibellesDNCMP, txtVersionElectroniqueDNCMP,txtrefDNCMP;
	private Long idpjointeDNCMP;
	

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		codecontentieux = (Long) session.getAttribute("numcontentieux");
		session.setAttribute("numcontentieux", codecontentieux);
		contentieux = BeanLocator.defaultLookup(ContentieuxSession.class).findById(codecontentieux);
		
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	}
	
	@Override
	public void onEvent(Event event) throws Exception {

		  if(event.getName().equals(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			 String confirmer = (String) ((HashMap<String, Object>) event.getData()).get(CONFIRMPUBLIER);
			 if (confirmer != null && confirmer.equalsIgnoreCase("Publication_Confirmer")) 
			 {
				 
				 //contentieux.setTraitement(Labels.getLabel("kermel.contentieux.recevabilite.publier"));
				 contentieux.setTraitement("PUB");
				 BeanLocator.defaultLookup(ContentieuxSession.class).update(contentieux);
				 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			 }
		 }
	}

	
	public void onCreate(CreateEvent event) {
	
           //div decision
			//recevabilites = BeanLocator.defaultLookup(RecevabiliteContentieuxSession.class).findCont(codecontentieux);
			if(contentieux!=null){
				//recevabilite=recevabilites;
			//idrecevabilite=recevabilites.getId();
			
			lgnumero.setValue(contentieux.getRecvNumero());
		    daterecours.setValue(contentieux.getRecvDate());
		    txtVersionElectronique1.setValue(contentieux.getRecvFichier());
		    txtCommentaire.setValue(contentieux.getRecvCommentaire());
		    
		    if(contentieux.getRecvDecision()!=null){
		    if (contentieux.getRecvDecision().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.Decision.recevable"))) {
				radioDecision.getItemAtIndex(0).setChecked(true);
			} else {
				radioDecision.getItemAtIndex(1).setChecked(true);
			}
		    }
			}
			//div pj
			pjointes = BeanLocator.defaultLookup(PieceJointeContentieuxSession.class).findCont(codecontentieux,"Plaignant",null);
			if(pjointes!=null){
				pjointe=pjointes;
			idpjointe=pjointes.getId();
			dtdate.setValue(pjointes.getDate());
			txtLibelles.setValue(pjointes.getLibelle());
			txtVersionElectronique.setValue(pjointes.getFichier());
			dtdatecourrier.setValue(pjointes.getDatecourrier());
			txtref.setValue(pjointes.getReference());
			
			
			}
		
		//div AC
			
			pjointesAC = BeanLocator.defaultLookup(PieceJointeContentieuxSession.class).findCont(codecontentieux,"AC",null);
			if(pjointesAC!=null){
				pjointeAC=pjointesAC;
				idpjointeAC=pjointesAC.getId();
			dtdateAC.setValue(pjointesAC.getDate());
			txtLibellesAC.setValue(pjointesAC.getLibelle());
			txtVersionElectroniqueAC.setValue(pjointesAC.getFichier());
			dtdatecourrierAC.setValue(pjointesAC.getDatecourrier());
			txtrefAC.setValue(pjointesAC.getReference());
			
			
			}
			
			
          //div DNCMP
			
			pjointesDNCMP = BeanLocator.defaultLookup(PieceJointeContentieuxSession.class).findCont(codecontentieux,"DNCMP",null);
			if(pjointesDNCMP!=null){
				pjointeDNCMP=pjointesDNCMP;
				idpjointeDNCMP=pjointesDNCMP.getId();
			dtdateDNCMP.setValue(pjointesDNCMP.getDate());
			txtLibellesDNCMP.setValue(pjointesDNCMP.getLibelle());
			txtVersionElectroniqueDNCMP.setValue(pjointesDNCMP.getFichier());
			dtdatecourrierDNCMP.setValue(pjointesDNCMP.getDatecourrier());
			txtrefDNCMP.setValue(pjointesDNCMP.getReference());
			
			
			}
			
//		lblDenomination.setValue(contentieux.getAutoritecontractante().getDenomination());
//		lblResponsable.setValue(contentieux.getAutoritecontractante().getResponsable());
//		lblAdresse.setValue(contentieux.getAutoritecontractante().getAdresse());
//		lblTel.setValue(contentieux.getAutoritecontractante().getTelephone());
		
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			if(lgnumero.getValue().equals(""))
		     {
             errorComponent = lgnumero;
             errorMsg = Labels.getLabel("kermel.contentieux.Prenom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if (daterecours.getValue() == null) {

				errorComponent = daterecours;
				errorMsg = Labels.getLabel("cciad.conteneur.tac.demande.daterecours") + ": " + Labels.getLabel("cciad.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);

			}
			
			if(txtVersionElectronique1.getValue().equals(""))
		     {
         errorComponent = txtVersionElectronique1;
         errorMsg = Labels.getLabel("kermel.contentieux.pj.Fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			
			
			
			
			/* Controle date inferieure a date du jour */

			if (daterecours.getValue() != null) {
				datejour = new Date();
				if ((daterecours.getValue()).after((datejour))) {
				errorComponent = daterecours;
					errorMsg = Labels.getLabel("kermel.contentieux.Controledate")+": "+ UtilVue.getInstance().formateLaDate(datejour);
					lbStatusBar.setStyle(ERROR_MSG_STYLE);
					lbStatusBar.setValue(errorMsg);
					throw new WrongValueException(errorComponent, errorMsg);				}
			}
		
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}


public void onClick$btnChoixFichier1() {

	if (ToolKermel.isWindows())
		nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
	else
		nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

	txtVersionElectronique1.setValue(nomFichier);
}
	
	public void onClick$bcvalider() {

		if(checkFieldConstraints())
		{

			if (((String) radioDecision.getSelectedItem().getValue()).equalsIgnoreCase(Labels.getLabel("kermel.contentieux.Decision.recevable"))) {
				contentieux.setRecvDecision(Labels.getLabel("kermel.contentieux.Decision.recevable"));
			} else {

				contentieux.setRecvDecision(Labels.getLabel("kermel.contentieux.Decision.irrecevable"));
			}
		      contentieux.setRecvNumero(lgnumero.getValue());
		      contentieux.setRecvDate(daterecours.getValue());
		      contentieux.setRecvFichier(txtVersionElectronique1.getValue());
		      contentieux.setRecvCommentaire(txtCommentaire.getValue());

		datestatut = new Date();
		contentieux.setDateStatut(datestatut);
		//contentieux.setTraitement(Labels.getLabel("kermel.contentieux.recevabilite.valider"));
		contentieux.setTraitement("VAL");
		//if(idrecevabilite==null){
			
			BeanLocator.defaultLookup(ContentieuxSession.class).update(contentieux);
		    //  }else{
		    	  //BeanLocator.defaultLookup(RecevabiliteContentieuxSession.class).update(recevabilite);
		     // }
		
		//recevabilites = BeanLocator.defaultLookup(RecevabiliteContentieuxSession.class).findCont(codecontentieux);
		//idrecevabilite=recevabilites.getId();
		//recevabilite=recevabilites;
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		loadApplicationState("suivi_contentieux");
		
	} 
	}

	public void onClick$menuFermer() {
		detach();
	}
	
	public void onClick$bcpublier() throws InterruptedException {
		Statut=contentieux.getTraitement();
		if (Statut.equals("VAL")){
		HashMap<String, String> display = new HashMap<String, String>(); // permet
		 display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.plansdepassation.publier"));
		 display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.contentieux.recevabilite.publier.titre"));
		 display.put(MessageBoxController.DSP_HEIGHT, "150px");
		 display.put(MessageBoxController.DSP_WIDTH, "100px");
        HashMap<String, Object> map = new HashMap<String, Object>(); // permet
        map.put(CONFIRMPUBLIER, "Publication_Confirmer");
		 showMessageBox(display, map);
		
		}else{
			   Messagebox.show(Labels.getLabel("kermel.contentieux.recevabilite.statut.publier.controle"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		      }
		
	}

	//div plaignante
	public void onClick$btnChoixFichier() {

		if (ToolKermel.isWindows())
			nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
		else
			nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

		txtVersionElectronique.setValue(nomFichier);
	}
	
	
private boolean checkFieldConstraints2() {
		
		try {
		
			if(dtdate.getValue()==null)
		     {
               errorComponent = dtdate;
               errorMsg = Labels.getLabel("kermel.contentieux.pj.Date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			
			if(dtdatecourrier.getValue()==null)
		     {
             errorComponent = dtdatecourrier;
             errorMsg = Labels.getLabel("kermel.contentieux.pj.Datecourrier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		
			if(txtVersionElectronique.getValue().equals(""))
		     {
          errorComponent = txtVersionElectronique;
          errorMsg = Labels.getLabel("kermel.contentieux.pj.Fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(txtLibelles.getValue().equals(""))
		     {
         errorComponent = txtLibelles;
         errorMsg = Labels.getLabel("kermel.contentieux.pj.Libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}


public void onClick$bcvalider2() {


	if(checkFieldConstraints2())
	{
		pjointe.setLibelle(txtLibelles.getValue());
		pjointe.setFichier(txtVersionElectronique.getValue());
		pjointe.setDate(dtdate.getValue());
		pjointe.setDatecourrier(dtdatecourrier.getValue());
		pjointe.setReference(txtref.getValue());
		pjointe.setRecevable("Plaignant");

		pjointe.setContentieux(contentieux);
		if (idpjointe==null){ 
			BeanLocator.defaultLookup(PieceJointeContentieuxSession.class).save(pjointe);
		}
        else {
        BeanLocator.defaultLookup(PieceJointeContentieuxSession.class).update(pjointe);
        }
		pjointes = BeanLocator.defaultLookup(PieceJointeContentieuxSession.class).findCont(codecontentieux,"Plaignant",null);
		idpjointe=pjointes.getId();
		pjointe=pjointes;

	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	
} 
}

//div AC
public void onClick$btnChoixFichierAC() {

	if (ToolKermel.isWindows())
		nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
	else
		nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

	txtVersionElectroniqueAC.setValue(nomFichier);
}

private boolean checkFieldConstraintsAC() {
	
	try {
		
		
		if(dtdateAC.getValue()==null)
	     {
          errorComponent = dtdateAC;
          errorMsg = Labels.getLabel("kermel.contentieux.pj.Date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		   lbStatusBar.setStyle(ERROR_MSG_STYLE);
		   lbStatusBar.setValue(errorMsg);
		  throw new WrongValueException (errorComponent, errorMsg);
	     }
	
		if(dtdatecourrierAC.getValue()==null)
	     {
           errorComponent = dtdatecourrierAC;
           errorMsg = Labels.getLabel("kermel.contentieux.pj.Datecourrier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		   lbStatusBar.setStyle(ERROR_MSG_STYLE);
		   lbStatusBar.setValue(errorMsg);
		  throw new WrongValueException (errorComponent, errorMsg);
	     }
	
		if(txtVersionElectroniqueAC.getValue().equals(""))
	     {
      errorComponent = txtVersionElectroniqueAC;
      errorMsg = Labels.getLabel("kermel.contentieux.pj.Fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		   lbStatusBar.setStyle(ERROR_MSG_STYLE);
		   lbStatusBar.setValue(errorMsg);
		  throw new WrongValueException (errorComponent, errorMsg);
	     }
		
		if(txtLibellesAC.getValue().equals(""))
	     {
    errorComponent = txtLibellesAC;
    errorMsg = Labels.getLabel("kermel.contentieux.pj.Libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		   lbStatusBar.setStyle(ERROR_MSG_STYLE);
		   lbStatusBar.setValue(errorMsg);
		  throw new WrongValueException (errorComponent, errorMsg);
	     }
	
		return true;
			
	}
	catch (Exception e) {
		errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
		+ " [checkFieldConstraints]";
		errorComponent = null;
		return false;

		
	}
	
}


public void onClick$bcvaliderAC() {


	if(checkFieldConstraintsAC())
	{
		pjointeAC.setLibelle(txtLibellesAC.getValue());
		pjointeAC.setFichier(txtVersionElectroniqueAC.getValue());
		pjointeAC.setDate(dtdateAC.getValue());
		pjointeAC.setDatecourrier(dtdatecourrierAC.getValue());
		pjointeAC.setReference(txtrefAC.getValue());
		pjointeAC.setRecevable("AC");

		pjointeAC.setContentieux(contentieux);
		if (idpjointeAC==null){ 
			BeanLocator.defaultLookup(PieceJointeContentieuxSession.class).save(pjointeAC);
		}
        else {
        BeanLocator.defaultLookup(PieceJointeContentieuxSession.class).update(pjointeAC);
        }
		pjointesAC = BeanLocator.defaultLookup(PieceJointeContentieuxSession.class).findCont(codecontentieux,"AC",null);
		idpjointeAC=pjointesAC.getId();
		pjointeAC=pjointesAC;

	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	
   } 
}


//div DNCMP************************************************************
public void onClick$btnChoixFichierDNCMP() {

	if (ToolKermel.isWindows())
		nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
	else
		nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

	txtVersionElectroniqueDNCMP.setValue(nomFichier);
}

private boolean checkFieldConstraintsDNCMP() {
	
	try {
		
		
		if(dtdateDNCMP.getValue()==null)
	     {
          errorComponent = dtdateDNCMP;
          errorMsg = Labels.getLabel("kermel.contentieux.pj.Date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		   lbStatusBar.setStyle(ERROR_MSG_STYLE);
		   lbStatusBar.setValue(errorMsg);
		  throw new WrongValueException (errorComponent, errorMsg);
	     }
	
		if(dtdatecourrierDNCMP.getValue()==null)
	     {
           errorComponent = dtdatecourrierDNCMP;
           errorMsg = Labels.getLabel("kermel.contentieux.pj.Datecourrier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		   lbStatusBar.setStyle(ERROR_MSG_STYLE);
		   lbStatusBar.setValue(errorMsg);
		  throw new WrongValueException (errorComponent, errorMsg);
	     }
	
		if(txtVersionElectroniqueDNCMP.getValue().equals(""))
	     {
      errorComponent = txtVersionElectroniqueDNCMP;
      errorMsg = Labels.getLabel("kermel.contentieux.pj.Fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		   lbStatusBar.setStyle(ERROR_MSG_STYLE);
		   lbStatusBar.setValue(errorMsg);
		  throw new WrongValueException (errorComponent, errorMsg);
	     }
		
		if(txtLibellesDNCMP.getValue().equals(""))
	     {
    errorComponent = txtLibellesDNCMP;
    errorMsg = Labels.getLabel("kermel.contentieux.pj.Libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		   lbStatusBar.setStyle(ERROR_MSG_STYLE);
		   lbStatusBar.setValue(errorMsg);
		  throw new WrongValueException (errorComponent, errorMsg);
	     }
	
		return true;
			
	}
	catch (Exception e) {
		errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
		+ " [checkFieldConstraints]";
		errorComponent = null;
		return false;

		
	}
	
}


public void onClick$bcvaliderDNCMP() {


	if(checkFieldConstraintsDNCMP())
	{
		pjointeDNCMP.setLibelle(txtLibellesDNCMP.getValue());
		pjointeDNCMP.setFichier(txtVersionElectroniqueDNCMP.getValue());
		pjointeDNCMP.setDate(dtdateDNCMP.getValue());
		pjointeDNCMP.setDatecourrier(dtdatecourrierDNCMP.getValue());
		pjointeDNCMP.setReference(txtrefDNCMP.getValue());
		pjointeDNCMP.setRecevable("DNCMP");

		pjointeDNCMP.setContentieux(contentieux);
		if (idpjointeDNCMP==null){ 
			BeanLocator.defaultLookup(PieceJointeContentieuxSession.class).save(pjointeDNCMP);
		}
        else {
        BeanLocator.defaultLookup(PieceJointeContentieuxSession.class).update(pjointeDNCMP);
        }
		pjointesDNCMP = BeanLocator.defaultLookup(PieceJointeContentieuxSession.class).findCont(codecontentieux,"DNCMP",null);
		idpjointeDNCMP=pjointesDNCMP.getId();
		pjointeDNCMP=pjointesDNCMP;

	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	
   } 
}

}
