package sn.ssi.kermel.web.contentieux.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Longbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timebox;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.contentieux.ejb.ContentieuxSession;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygContentieux;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteContractanteSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeAutoriteContractanteSession;
import sn.ssi.kermel.be.workflow.ejb.WorkflowSession;
import sn.ssi.kermel.be.workflow.entity.SysState;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;

//@SuppressWarnings("serial")
public class ContentieuxFormController extends AbstractWindow implements AfterCompose, EventListener, ListitemRenderer {

	private static final long serialVersionUID = 3449680167389488155L;
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String WINDOW_PARAM_MODE = "MODE";

	private String mode;
	private Long code;
	private SygContentieux contentieux = new SygContentieux();
	List<SygContentieux> contentieuxs = new ArrayList<SygContentieux>();

	private Label lbStatusBar;
	//private Textbox txtNumero;
	
	//Type Autorite Contractante
	private SygTypeAutoriteContractante typeautorite = new SygTypeAutoriteContractante();
	private Listbox lstTypeAC;
	private Paging pgAutorite;
	
    //Autorite Contractante
	private SygAutoriteContractante autorite = new SygAutoriteContractante();
	private Listbox lstAutorite;
	private Paging pgTypeAC;
	//private Bandbox bdAutorite;
	
	//Recher Ac
	public Textbox txtDenomination;
	private String denomination=null;
	
	//Recher Objet Marche
	public Textbox txtrObjet;
	private String objet=null;
	
	//Procedure
	private SygAppelsOffres appeloffre= new SygAppelsOffres();
	private Listbox lstProcedure;
	private Paging pgProcedure;
	//private Bandbox bdProcedure;
	//Recher Procedure
  //  private int annee;
	
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	//private String libelleclient = null;
	private SysState state = new SysState();

	// private static final String READY_MSG_STYLE = "";
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Div step0, step1, step2,step01,stepPJ;
	private Datebox daterecours,datedecision,datecourier;
	private Timebox heure;
	private Textbox txtPrenom,txtNom,txtRsociale,txtTel,txtFax,txtEmail,txtMotif,txtObjet,txtAdresse,txtRefCourrier;
	private Longbox lgbp,lgnumdecision;
	
	private Date datestatut,date;

	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE, activePage;

	private String libelle;
	private String numenregistrement;
	//Information generale
	private Label lblDenomination, lblRefAppelOffre, lblNom,lblTypeAC,lblDenomination1,lblTypeAC2;
	List<Long> appels = new ArrayList<Long>();
	// /Decision 
//	private SygDecision decision = new SygDecision();
//	private Textbox txtRechercherDecision;
//	private Bandbox bdDecision;
//	private Paging pgDecision;
//	private Listbox lstDecision;
//	private String LibelleDecision = null;

	// Bouton recherche
//	private Textbox recherch, rechernom, rechermatricule;
//	private String RechPrenom = null;
//	private String RechNom = null;
//	private String RechMatricule = null;
	
	 private Iframe iframPJ;
	 Session session = getHttpSession();

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		// champ Decision  du formulaire
//		addEventListener(ApplicationEvents.ON_DECISION, this);
//		lstDecision.setItemRenderer(new decisionRenderer());
//		pgDecision.setPageSize(byPageBandbox);
//		pgDecision.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_DECISION);
//		Events.postEvent(ApplicationEvents.ON_DECISION, this, null);
		
		//Type AC
		addEventListener(ApplicationEvents.ON_TYPE_AUTORITES, this);
		lstTypeAC.setItemRenderer(new TypeAutoriteRenderer());
		pgTypeAC.setPageSize(byPageBandbox);
		pgTypeAC.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_TYPE_AUTORITES);
		Events.postEvent(ApplicationEvents.ON_TYPE_AUTORITES, this, null);
		
            
		//AC
		addEventListener(ApplicationEvents.ON_AUTORITES, this);
		lstAutorite.setItemRenderer(new AutoriteRenderer());
		pgAutorite.setPageSize(byPageBandbox);
		pgAutorite.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTORITES);
		//Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);
		
		//PRocedure
		addEventListener(ApplicationEvents.ON_PROCEDURE, this);
		lstProcedure.setItemRenderer(new ProcedureRenderer());
		pgProcedure.setPageSize(byPageBandbox);
		pgProcedure.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_PROCEDURE);
		

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		appels=BeanLocator.defaultLookup(AppelsOffresSession.class).findRech();
		
		
		
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_TYPE_AUTORITES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgTypeAC.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_LIST_BY_PAGES;
				activePage = pgTypeAC.getActivePage() * byPage;
				pgTypeAC.setPageSize(byPage);
			}
			List<SygTypeAutoriteContractante> typeautorite = BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).find(activePage, byPage, null,libelle);
			lstTypeAC.setModel(new SimpleListModel(typeautorite));
			pgTypeAC.setTotalSize(BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).count(null,libelle));
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTORITES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgAutorite.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_LIST_BY_PAGES;
				activePage = pgAutorite.getActivePage() * byPage;
				pgAutorite.setPageSize(byPage);
			}
			List<SygAutoriteContractante> autorite = BeanLocator.defaultLookup(AutoriteContractanteSession.class).find(activePage, byPage, denomination,null,typeautorite,null,null);
			lstAutorite.setModel(new SimpleListModel(autorite));
			pgAutorite.setTotalSize(BeanLocator.defaultLookup(AutoriteContractanteSession.class).count(denomination,null,typeautorite,null,null));
		}
		//Procedure
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_PROCEDURE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgProcedure.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_LIST_BY_PAGES;
				activePage = pgProcedure.getActivePage() * byPage;
				pgProcedure.setPageSize(byPage);
			}
			List<SygAppelsOffres> procedure = BeanLocator.defaultLookup(AppelsOffresSession.class).findRech(activePage, byPage, autorite,objet);
			lstProcedure.setModel(new SimpleListModel(procedure));
			pgProcedure.setTotalSize(BeanLocator.defaultLookup(AppelsOffresSession.class).countRech(autorite,objet));
		}
		// champ 
//		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DECISION)) {
//			List<SygDecision> decision = BeanLocator.defaultLookup(DecisionSession.class).find(
//					pgDecision.getActivePage() * byPageBandbox, byPageBandbox, null, LibelleDecision);
//			SimpleListModel listModel = new SimpleListModel(decision);
//			lstDecision.setModel(listModel);
//			pgDecision.setTotalSize(BeanLocator.defaultLookup(DecisionSession.class).count(null, LibelleDecision));
//		}

		else if (event.getName().equalsIgnoreCase(Events.ON_CLICK)) {
			String libelleimage = (String) (((Listcell) event.getTarget()).getAttribute("libelleimage"));
			if (libelleimage == "supconteneur") {
				HashMap<String, String> display = new HashMap<String, String>();
				display.put(MessageBoxController.DSP_MESSAGE, " Etes vous sur de vouloir supprimer?");
				display.put(MessageBoxController.DSP_TITLE, "Supprimer le dossier");
				display.put(MessageBoxController.DSP_HEIGHT, "150px");
				display.put(MessageBoxController.DSP_WIDTH, "100px");
				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				showMessageBox(display, map);
			}
		}
	}

	// //////////Decision
//	public void onSelect$lstDecision() {
//		bdDecision.setValue((String) lstDecision.getSelectedItem().getAttribute(UIConstants.ATTRIBUTE_LIBELLE));
//		bdDecision.close();
//	}
//
//	public class decisionRenderer implements ListitemRenderer {
//
//		@Override
//		public void render(Listitem item, Object data, int index)  throws Exception {
//			SygDecision decision = (SygDecision) data;
//			item.setValue(decision);
//			item.setAttribute(UIConstants.ATTRIBUTE_LIBELLE, decision.getLibelle());
//
//			Listcell cellLibelle = new Listcell("");
//			if (decision.getLibelle() != null) {
//				cellLibelle.setLabel(decision.getLibelle());
//			}
//			cellLibelle.setParent(item);
//			//				
//
//		}
//	}

//	public void onFocus$txtRechercherDecision() {
//		if (txtRechercherDecision.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.Decision"))) {
//			txtRechercherDecision.setValue("");
//		}
//	}
//
//	public void onBlur$txtRechercherDecision() {
//		if (txtRechercherDecision.getValue().equalsIgnoreCase("")) {
//			txtRechercherDecision.setValue(Labels.getLabel("kermel.contentieux.Decision"));
//		}
//	}

//	public void onClick$btnRechercherDecision() {
//		if (txtRechercherDecision.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.Decision")) || txtRechercherDecision.getValue().equals("")) {
//			LibelleDecision = null;
//		} else {
//			LibelleDecision = txtRechercherDecision.getValue();
//		}
//		Events.postEvent(ApplicationEvents.ON_DECISION, this, null);
//	}

	// ////////////////fin ////////////////////////////////

	// /////////////////
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent createEvent) {
		Map<String, Object> windowParams = (Map<String, Object>) createEvent.getArg();
		mode = (String) windowParams.get(WINDOW_PARAM_MODE);
		if (mode == null) {
			
		} else {
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				code = (Long) windowParams.get(PARAM_WINDOW_CODE);
				contentieux = BeanLocator.defaultLookup(ContentieuxSession.class).findById(code);
				contentieuxs = BeanLocator.defaultLookup(ContentieuxSession.class).find(0, -1, null, null);
				
				//txtPrenom.setValue(contentieux.getPrenom());
				txtObjet.setValue(contentieux.getObjet());
				txtTel.setValue(contentieux.getTelephone());
				lgbp.setValue(contentieux.getBp());
				//txtNom.setValue(contentieux.getNom());
				txtRsociale.setValue(contentieux.getRaisonSocial());
				txtFax.setValue(contentieux.getFax());
				txtEmail.setValue(contentieux.getEmail());
				txtAdresse.setValue(contentieux.getAdresse());
				
				daterecours.setValue(contentieux.getDaterecours());
				heure.setValue(contentieux.getHeure());
				txtMotif.setValue(contentieux.getMotifs());
				datecourier.setValue(contentieux.getDatecourrier());
				txtRefCourrier.setValue(contentieux.getRefcourrier());
				//bdDecision.setValue(contentieux.getDecision().getLibelle());
				autorite = contentieuxs.get(0).getAutoritecontractante();
				typeautorite=autorite.getType();
				appeloffre = contentieuxs.get(0).getAppeloffre();
				
				lgnumdecision.setValue(contentieux.getNumerodecision());
				datedecision.setValue(contentieux.getDatedecision());
				
				

				
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

			}
		}

	}

	private boolean checkFieldConstraints() {

		try {
			
//			if(txtPrenom.getValue().equals(""))
//		     {
//              errorComponent = txtPrenom;
//              errorMsg = Labels.getLabel("kermel.contentieux.Prenom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
//				lbStatusBar.setStyle(ERROR_MSG_STYLE);
//				lbStatusBar.setValue(errorMsg);
//				throw new WrongValueException (errorComponent, errorMsg);
//		     }
			
			if (datecourier.getValue() == null) {

				errorComponent = datecourier;
				errorMsg = Labels.getLabel("kermel.contentieux.datecourrier") + ": " + Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);

			}
			
			if(txtRefCourrier.getValue().equals(""))
		     {
            errorComponent = txtRefCourrier;
            errorMsg = Labels.getLabel("kermel.contentieux.reference")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(txtObjet.getValue().equals(""))
		     {
             errorComponent = txtObjet;
             errorMsg = Labels.getLabel("kermel.contentieux.Objet")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
//			if(txtNom.getValue().equals(""))
//		     {
//             errorComponent = txtNom;
//             errorMsg = Labels.getLabel("kermel.contentieux.Nom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
//				lbStatusBar.setStyle(ERROR_MSG_STYLE);
//				lbStatusBar.setValue(errorMsg);
//				throw new WrongValueException (errorComponent, errorMsg);
//		     }
			
			
			if (daterecours.getValue() == null) {

				errorComponent = daterecours;
				errorMsg = Labels.getLabel("kermel.contentieux.libelledate") + ": " + Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);

			}
			
			if (heure.getValue() == null) {

				errorComponent = heure;
				errorMsg = Labels.getLabel("kermel.contentieux.libelleheure") + ": " + Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);

			}
			
			if(txtRsociale.getValue().equals(""))
		     {
             errorComponent = txtRsociale;
             errorMsg = Labels.getLabel("kermel.contentieux.RaisonSociale")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			
            if(ToolKermel.ControlValidateEmail(txtEmail.getValue())) {
               
           } else {
                   errorComponent = txtEmail;
                   errorMsg = Labels.getLabel("kermel.contentieux.email.incorrect");
                    lbStatusBar.setStyle(ERROR_MSG_STYLE);
                    lbStatusBar.setValue(errorMsg);
                    throw new WrongValueException(errorComponent,errorMsg);
           }
			
            
            if(txtAdresse.getValue().equals(""))
		     {
           errorComponent = txtAdresse;
           errorMsg = Labels.getLabel("kermel.contentieux.adresse")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			
			if(txtMotif.getValue().equals(""))
		     {
           errorComponent = txtMotif;
           errorMsg = Labels.getLabel("kermel.contentieux.Motif")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
		
			
			
			 
			
//			if(bdDecision.getValue().equals(""))
//		     {
//             errorComponent = bdDecision;
//             errorMsg = Labels.getLabel("kermel.contentieux.Decision")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
//				lbStatusBar.setStyle(ERROR_MSG_STYLE);
//				lbStatusBar.setValue(errorMsg);
//				throw new WrongValueException (errorComponent, errorMsg);
//		     }
			


			/* Controle datecourier inferieure � daterecours */

			if (datecourier.getValue() != null) {
				
				date = daterecours.getValue();
				if ((datecourier.getValue()).after((date))) {
				errorComponent = datecourier;
					errorMsg = Labels.getLabel("kermel.contentieux.Controledate")+": "+ UtilVue.getInstance().formateLaDate(date);
					lbStatusBar.setStyle(ERROR_MSG_STYLE);
					lbStatusBar.setValue(errorMsg);
					throw new WrongValueException(errorComponent, errorMsg);				}
			}
			return true;

		} catch (Exception e) {
			errorMsg = Labels.getLabel("cciad.erreur.erreurinconnue") + ": " + e.toString() + " [checkFieldConstraints]";
			errorComponent = null;
			return false;

		}

	}
	//Step01**********************************************************************************
	public void onClick$menuCancelStep01() {
		detach();
	}

	public void onClick$menuNextStep01() {
		if (lstTypeAC.getSelectedItem() == null) {
			throw new WrongValueException(lstTypeAC, Labels.getLabel("cciad.error.select.item"));
		} else {
			typeautorite = (SygTypeAutoriteContractante) lstTypeAC.getSelectedItem().getValue();

			step01.setVisible(false);
			step0.setVisible(true);
			step1.setVisible(false);
			step2.setVisible(false);
			stepPJ.setVisible(false);
			
			Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);
			
		}
	}

	
	//Step0**************************************************************************
	public void onClick$menuCancelStep0() {
		detach();
	}

	public void onClick$menuNextStep0() {
		if (lstTypeAC.getSelectedItem() == null){
			throw new WrongValueException(lstTypeAC, Labels.getLabel("cciad.error.select.item"));
		}
		else if (lstAutorite.getSelectedItem() == null) {
			throw new WrongValueException(lstAutorite, Labels.getLabel("cciad.error.select.item"));
		} else {
			typeautorite = (SygTypeAutoriteContractante) lstTypeAC.getSelectedItem().getValue();
			autorite = (SygAutoriteContractante) lstAutorite.getSelectedItem().getValue();
			
			lblDenomination1.setValue(autorite.getDenomination());
			lblTypeAC.setValue(autorite.getType().getLibelle());
			
			step01.setVisible(false);
			step0.setVisible(false);
			step1.setVisible(true);
			step2.setVisible(false);
			stepPJ.setVisible(false);
			if(appels.size()>0)
			  Events.postEvent(ApplicationEvents.ON_PROCEDURE, this, null);
			
		}
	}

	
	public void onClick$menuPreviousStep0() {
		
		step01.setVisible(true);
		step0.setVisible(false);
		step1.setVisible(false);
		step2.setVisible(false);
		stepPJ.setVisible(false);
		
	}
	
	//Step1**********************************************************************************
	
	public void onClick$menuCancelStep1() {
		detach();
	}
	
	public void onClick$menuNextStep1() {
		if (lstAutorite.getSelectedItem() == null){
			throw new WrongValueException(lstAutorite, Labels.getLabel("cciad.error.select.item"));
		}else if(lstProcedure.getSelectedItem() == null){
			throw new WrongValueException(lstProcedure, Labels.getLabel("cciad.error.select.item"));
		} else {
			autorite = (SygAutoriteContractante) lstAutorite.getSelectedItem().getValue();
			appeloffre=(SygAppelsOffres) lstProcedure.getSelectedItem().getValue();
			lblDenomination.setValue(autorite.getDenomination());
			lblRefAppelOffre.setValue(appeloffre.getAporeference());
			lblTypeAC2.setValue(autorite.getType().getLibelle());
			step0.setVisible(false);
			step1.setVisible(false);
			step2.setVisible(true);
			stepPJ.setVisible(false);
			
			
			
		}
	}
	
	public void onClick$menuPreviousStep1() {
		
		step01.setVisible(false);
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
		stepPJ.setVisible(false);
		
	}
	
	
//Step2*****************************************************************
	public void onClick$menuNextStep2() {
		if (checkFieldConstraints()) {

//			if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
//				decision = (SygDecision) lstDecision.getSelectedItem().getValue();
//
//			} 
//			else {
//				if (bdDecision.getValue().equals(contentieux.getDecision().getLibelle())) {
//					decision = contentieux.getDecision();
//				} else {
//					decision = (SygDecision) lstDecision.getSelectedItem().getValue();
//				}
//			}
			
			 numenregistrement=BeanLocator.defaultLookup(ContentieuxSession.class).getGeneratedCode(BeConstants.PARAM_NUMCONT);
			
			state = BeanLocator.defaultLookup(WorkflowSession.class).findToState("SAISIECONT");
			contentieux.setState(state);
			    //contentieux.setPrenom(txtPrenom.getValue());
				//contentieux.setNom(txtNom.getValue());
				contentieux.setRaisonSocial(txtRsociale.getValue());
				contentieux.setEmail(txtEmail.getValue());
				contentieux.setTelephone(txtTel.getValue());
				contentieux.setFax(txtFax.getValue());
				contentieux.setMotifs(txtMotif.getValue());
				contentieux.setObjet(txtObjet.getValue());
				contentieux.setBp(lgbp.getValue());
				contentieux.setAdresse(txtAdresse.getValue());
				
			
			//contentieux.setDecision(decision);

			contentieux.setAutoritecontractante(autorite);
			contentieux.setAppeloffre(appeloffre);
			contentieux.setDaterecours(daterecours.getValue());
			 contentieux.setHeure(heure.getValue());
			 contentieux.setNumero(numenregistrement);
			 contentieux.setDatecourrier(datecourier.getValue());
			 contentieux.setRefcourrier(txtRefCourrier.getValue());
			
			   contentieux.setNumerodecision(lgnumdecision.getValue());
				contentieux.setDatedecision(datedecision.getValue()); 
			 
			datestatut = new Date();
			contentieux.setDateStatut(datestatut);
			contentieux.setTraitement("Saisie");

			if (mode == null) {

				contentieux=BeanLocator.defaultLookup(ContentieuxSession.class).save(contentieux);
				mode = UIConstants.MODE_EDIT;
			} else {
				if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {

					BeanLocator.defaultLookup(ContentieuxSession.class).update(contentieux);
				} else {

					contentieux=BeanLocator.defaultLookup(ContentieuxSession.class).save(contentieux);
					mode = UIConstants.MODE_EDIT;
				}
			}
			
			session.setAttribute("numcontentieux", contentieux.getId());
			step1.setVisible(false);
			step2.setVisible(false);
			stepPJ.setVisible(true);
			
			final String uri ="/contentieux/dossiers/piecejointe.zul";
			iframPJ.setWidth("100%");
			iframPJ.setHeight("100%");
			iframPJ.setSrc(uri);
			//loadApplicationState("contentieux");
			//detach();
			
		}
	}

	
	public void onClick$menuCancelStep2() {
		loadApplicationState("contentieux");
		detach();
	}


	
	public void onClick$menuPreviousStep2() {
		
		step01.setVisible(false);
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
		stepPJ.setVisible(false);
		
	}
	
        ///////////*******************stepPJ stepPJ stepPJ ***********************************////
public void onClick$menuPreviousStepPJ() {
		
		step01.setVisible(false);
		step0.setVisible(false);
		step1.setVisible(false);
		step2.setVisible(true);
		stepPJ.setVisible(false);
		
	}


  public void onClick$menuCancelSStepPJ() {
	loadApplicationState("contentieux");
	  detach();
  }

	// ///////////////Type Autorite////////////////////////////////////////////////////

	public class TypeAutoriteRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypeAutoriteContractante typeautorite = (SygTypeAutoriteContractante) data;
			item.setValue(typeautorite);

			Listcell cellLibelle = new Listcell(typeautorite.getLibelle());
			cellLibelle.setParent(item);




			if (mode != null) {
				if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
					if (typeautorite.getId().equals(contentieuxs.get(0).getAutoritecontractante().getType().getId())) {
						item.setSelected(true);
					}

				}
			}

		}
	}

	// ////////AC AC///////////////////////////////////////////////////////////

	public class AutoriteRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygAutoriteContractante autorite = (SygAutoriteContractante) data;
			item.setValue(autorite);

			Listcell cellDenomination = new Listcell(autorite.getDenomination());
			cellDenomination.setParent(item);

//			Listcell cellAdresse = new Listcell(autorite.getAdresse());
//			cellAdresse.setParent(item);
//
//			Listcell cellTel = new Listcell(autorite.getTelephone());
//			cellTel.setParent(item);
//			
//			Listcell cellFax = new Listcell(autorite.getFax());
//			cellFax.setParent(item);
//			
//			Listcell cellEmail = new Listcell(autorite.getEmail());
//			cellEmail.setParent(item);



			if (mode != null) {
				if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
					if (autorite.getId().equals(contentieuxs.get(0).getAutoritecontractante().getId())) {
						item.setSelected(true);
					}

				}
			}

		}
	}
	
	//Procedure

	public class ProcedureRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygAppelsOffres appeloffre = (SygAppelsOffres) data;
			item.setValue(appeloffre);

			Listcell cellReference = new Listcell(appeloffre.getAporeference());
			cellReference.setParent(item);

			Listcell cellObjet = new Listcell(appeloffre.getApoobjet());
			cellObjet.setParent(item);

			Listcell cellModePassation = new Listcell(appeloffre.getModepassation().getLibelle());
			cellModePassation.setParent(item);
			
//			Listcell cellModeSelection = new Listcell(appeloffre.getModeselection().getLibelle());
//			cellModeSelection.setParent(item);
//			
//			Listcell cellDateCreation = new Listcell(UtilVue.getInstance().formateLaDate(appeloffre.getApodatecreation()));
//			cellDateCreation.setParent(item);
			
			Listcell cellMontant = new Listcell(ToolKermel.format2Decimal(appeloffre.getApomontantestime()));
			cellMontant.setParent(item);


			if (mode != null) {
				if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
					if (appeloffre.getApoid().equals(contentieuxs.get(0).getAppeloffre().getApoid())) {
						item.setSelected(true);
					}

				}
			}

		}
	}

//////////////*******************Denomination*******//////////////////////

	// function du bouton recherche par Denomination
	public void onFocus$txtDenomination() {
		if (txtDenomination.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.denomination")))
			txtDenomination.setValue("");

	}

	public void onBlur$txtDenomination() {
		if (txtDenomination.getValue().equals(""))
			txtDenomination.setValue(Labels.getLabel("kermel.referentiel.common.denomination"));
	}

	public void onOK$txtDenomination() {
		onClick$bchercherAC();
	}

	
	public void onClick$bchercherAC() {

		if (txtDenomination.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.denomination")) || txtDenomination.getValue().equals("")) {
			denomination = null;
		} else {
			denomination = txtDenomination.getValue();
		}

		Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);

	}

	public void onOK$bchercherAC() {
		onClick$bchercherAC();
	}

	//////////////*******************Denomination*******//////////////////////
	
	
//////////////*******************Objet Marche*******//////////////////////

	// function du bouton recherche par Objet
	public void onFocus$txtrObjet() {
		if (txtrObjet.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.Objet")))
			txtrObjet.setValue("");

	}

	public void onBlur$txtrObjet() {
		if (txtrObjet.getValue().equals(""))
			txtrObjet.setValue(Labels.getLabel("kermel.contentieux.Objet"));
	}

	public void onOK$txtrObjet() {
		onClick$bchercherAC();
	}

	
	public void onClick$bchercherAO() {

		if (txtrObjet.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.Objet")) || txtrObjet.getValue().equals("")) {
			objet = null;
		} else {
			objet = txtrObjet.getValue();
		}
		if(appels.size()>0)
		Events.postEvent(ApplicationEvents.ON_PROCEDURE, this, null);

	}

	public void onOK$bchercherAO() {
		onClick$bchercherAO();
	}

	//////////////*******************Objet Marche*******//////////////////////
	
	


	// //////////////////////////////////////////////////

	// /liste autorite
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygAutoriteContractante autorite = (SygAutoriteContractante) data;
		item.setValue(autorite.getId());

	}

}