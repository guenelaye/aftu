package sn.ssi.kermel.web.contentieux.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.contentieux.ejb.ContentieuxSession;
import sn.ssi.kermel.be.entity.SygContentieux;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class DossierContentieuxFormController extends AbstractWindow implements AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String PARAM_WINDOW_CODE = "CODE";
	
	private Long code;
	private SygContentieux contentieux = new SygContentieux();
	List<SygContentieux> contentieuxs = new ArrayList<SygContentieux>();
	UtilVue utilVue = UtilVue.getInstance();
	private Label lblAutorite, lblAppeloffre, lblDateStatus, lblObjet, lblDaterecours,lblPrenom,lblNom;
	Session session = getHttpSession();
	//private DossierMenu monDossierMenu;
	private Tab TAB_DECISION,TAB_RECEVABILITE,TAB_RAPPORT;
	private Include incDETAILS,incRECEVABILITE,incRAPPORT;
	private String suivi;
	

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		suivi=(String) session.getAttribute("suivi");
		
		
		code = (Long) session.getAttribute("numcontentieux");
		contentieux = BeanLocator.defaultLookup(ContentieuxSession.class).findById(code);
		contentieuxs = BeanLocator.defaultLookup(ContentieuxSession.class).findRech(0, -1, code, null, null);
		//monDossierMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		//monDossierMenu.setId(Long.toString(contentieux.getId()));
//		monDossierMenu.setState(contentieuxs.get(0).getState().getCode());
		//monDossierMenu.afterCompose();

	}

	public void onCreate(CreateEvent createEvent) {

		
		lblAutorite.setValue(contentieuxs.get(0).getAutoritecontractante().getDenomination());
		lblAppeloffre.setValue(contentieuxs.get(0).getAppeloffre().getAporeference());
		lblObjet.setValue(contentieux.getObjet());
		lblDateStatus.setValue(UtilVue.getInstance().formateLaDate(contentieux.getDateStatut()));
		
		 if(contentieuxs.get(0).getRecvDecision()!=null){
		   if(contentieuxs.get(0).getRecvDecision().equals("Irrecevable")){
			   TAB_DECISION.setDisabled(true);
			   TAB_RECEVABILITE.setSelected(true);
			   incRECEVABILITE.setSrc("/contentieux/dossiers/recevabilite.zul");
			   
		   }
		   else{
			   TAB_RECEVABILITE.setSelected(true);
			   TAB_DECISION.setDisabled(false);
			   incRECEVABILITE.setSrc("/contentieux/dossiers/recevabilite.zul");
		   }
		 }
		 else
		 {
			 TAB_DECISION.setDisabled(true);
		 }
		 if((suivi!=null)){
			  if(suivi.equals("OngletRapport")) {
				  TAB_RAPPORT.setSelected(true);
				  incRAPPORT.setSrc("/contentieux/dossiers/rapport.zul");
			  }
		 }
		 incDETAILS.setSrc("/contentieux/dossiers/details.zul");
		//lblDaterecours.setValue(UtilVue.getInstance().formateLaDate(contentieux.getDaterecours()));
		//lblObjet.setValue(contentieux.getObjet());
//		lblPrenom.setValue(contentieux.getPrenom());
//		lblNom.setValue(contentieux.getNom());
		
		
	}

	@Override
	public void onEvent(Event event) throws Exception {

	}

}