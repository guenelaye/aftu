package sn.ssi.kermel.web.contentieux.controllers;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.contentieux.ejb.ContentieuxSession;
import sn.ssi.kermel.be.entity.SygContentieux;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;




@SuppressWarnings("serial")
public class RapportController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Datebox txtdaterapport;
	
	
	private Textbox txtVersionElectronique;
	
	
	Long code;
	
	UtilVue utilVue = UtilVue.getInstance();
	private String nomFichier;
	private final String cheminDossier = UIConstants.PATH_PJ;
	
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Label lbStatusBar;

	
	Session session = getHttpSession();
	
	private Div step0,step1;
	
	
	
		Long codecontentieux = null;
		private SygContentieux contentieux = new SygContentieux();
		
		private Iframe idIframe;
		private String extension,images;
		private Image image;
		
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		
		
		
		
		codecontentieux = (Long) session.getAttribute("numcontentieux");
		session.setAttribute("numcontentieux", codecontentieux);
		contentieux = BeanLocator.defaultLookup(ContentieuxSession.class).findById(codecontentieux);

		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	
	}
	
	@Override
	public void onEvent(Event event) throws Exception {
		// TODO Auto-generated method stub

		
	}
	
	public void onCreate(CreateEvent event) {
		if(contentieux!=null){
		
		txtdaterapport.setValue(contentieux.getDaterapport());
		txtVersionElectronique.setValue(contentieux.getFichierrapport());
		
		
		if(contentieux.getFichierrapport()!="" && contentieux.getFichierrapport()!=null)
		{
			extension=contentieux.getFichierrapport().substring(contentieux.getFichierrapport().length()-3,  contentieux.getFichierrapport().length());
			 if(extension.equalsIgnoreCase("pdf"))
				 images="/images/icone_pdf.png";
			 else  
				 images="/images/word.jpg";
			 
			image.setVisible(true);
			image.setSrc(images);
			nomFichier=contentieux.getFichierrapport();
		}
		
	    
		}
			
	}
	
	private boolean checkFieldConstraints() {

		try {
			
			if (txtdaterapport.getValue() == null) {

				errorComponent = txtdaterapport;
				errorMsg = Labels.getLabel("kermel.contentieux.dossiers.rapport.daterapport") + ": " + Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);

			}
			
			

			
			if(txtVersionElectronique.getValue().equals(""))
		     {
        errorComponent = txtVersionElectronique;
        errorMsg = Labels.getLabel("kermel.referentiel.common.fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		

			return true;

		} catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString() + " [checkFieldConstraints]";
			errorComponent = null;
			return false;

		}

	}
	
	public void onOK() {
		if (checkFieldConstraints()) {
			
			contentieux.setDaterapport(txtdaterapport.getValue());
			contentieux.setFichierrapport(txtVersionElectronique.getValue());
			
			
		  BeanLocator.defaultLookup(ContentieuxSession.class).update(contentieux);
	
	       Executions.getCurrent().setAttribute("numcontentieux",codecontentieux);
	       session.setAttribute("suivi","OngletRapport");    
	       loadApplicationState("suivi_contentieux");
	
		
		}
	}
	
	
	public void onClick$menuFermerstep1() {
		step0.setVisible(true);
		step1.setVisible(false);
	}
	
	
	
	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	  public void onClick$btnChoixFichier() {
			//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
				if (ToolKermel.isWindows())
					nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
				else
					nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

				txtVersionElectronique.setValue(nomFichier);
			}
	  
	  public void onClick$image() {
			step0.setVisible(false);
			step1.setVisible(true);
			String filepath = cheminDossier +  nomFichier;
			File f = new File(filepath.replaceAll("\\\\", "/"));

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(f, null, null);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (mymedia != null)
				idIframe.setContent(mymedia);
			else
				idIframe.setSrc("");

			idIframe.setHeight("600px");
			idIframe.setWidth("100%");
		}

		public org.zkoss.util.media.AMedia fetchFile(File file) {

			org.zkoss.util.media.AMedia mymedia = null;
			try {
				mymedia = new AMedia(file, null, null);
				return mymedia;
			} catch (Exception e) {

				e.printStackTrace();
				return null;
			}

		}

		

		

}