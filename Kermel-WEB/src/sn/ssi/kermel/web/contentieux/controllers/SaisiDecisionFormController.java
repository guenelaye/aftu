package sn.ssi.kermel.web.contentieux.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.contentieux.ejb.DecisionsContentieuxSession;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDecisionsContentieux;
import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.entity.SygTypeDecision;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteContractanteSession;
import sn.ssi.kermel.be.referentiel.ejb.ModepassationSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeAutoriteContractanteSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeDecisionSession;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

//@SuppressWarnings("serial")
public class SaisiDecisionFormController extends AbstractWindow implements AfterCompose, EventListener, ListitemRenderer {

	private static final long serialVersionUID = 3449680167389488155L;
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String WINDOW_PARAM_MODE = "MODE";

	private String mode;
	private Long code;
	private SygDecisionsContentieux contdecision = new SygDecisionsContentieux();
	List<SygDecisionsContentieux> contdecisions = new ArrayList<SygDecisionsContentieux>();

	private Label lbStatusBar;
	//private Textbox txtNumero;
	
	//Type Autorite Contractante
	private SygTypeAutoriteContractante typeautorite = new SygTypeAutoriteContractante();
	private Listbox lstTypeAC;
	private Paging pgAutorite;
	
    //Autorite Contractante
	private SygAutoriteContractante autorite = new SygAutoriteContractante();
	private Listbox lstAutorite;
	private Paging pgTypeAC;
	//private Bandbox bdAutorite;
	
	//Recher Ac
	public Textbox txtDenomination;
	private String denomination=null;
	
	//Procedure
//	private SygAppelsOffres appeloffre= new SygAppelsOffres();
//	private Listbox lstProcedure;
//	private Paging pgProcedure;
	//private Bandbox bdProcedure;
	//Recher Procedure
  //  private int annee;
	
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	//private String libelleclient = null;
	//private SysState state = new SysState();

	// private static final String READY_MSG_STYLE = "";
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Div step0, step1, step2,step01;
	private Datebox daterecours,datedecision;
	
	private Textbox txtnumdecision,txtRsociale,txtObjet,txtVersionElectronique;
	private final String cheminDossier = UIConstants.PATH_PJCONT;
	private String nomFichier;
	UtilVue utilVue = UtilVue.getInstance();
	
	private Date datejour;

	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE, activePage;

	private String libelle;
	
	//Information generale
	private Label lblDenomination,lblTypeAC;

	// /Decision 
	private SygTypeDecision typedecision = new SygTypeDecision();
	private Textbox txtRechercherDecision;
	private Bandbox bdDecision;
	private Paging pgDecision;
	private Listbox lstDecision;
	private String LibelleDecision = null;
	
	// /Type marche 
	private SygTypesmarches typemarche = new SygTypesmarches();
	private Textbox txtRechercherTypemarche;
	private Bandbox bdTypemarche;
	private Paging pgTypemarche;
	private Listbox lstTypemarche;
	private String LibelleTypemarche = null;
	
	
	// /Mode de passation
	private SygModepassation modepassation = new SygModepassation();
	private Textbox txtRechercherModepassation;
	private Bandbox bdModepassation;
	private Paging pgModepassation;
	private Listbox lstModepassation;
	private String LibelleModepassation = null;
	
	

	// Bouton recherche
//	private Textbox recherch, rechernom, rechermatricule;
//	private String RechPrenom = null;
//	private String RechNom = null;
//	private String RechMatricule = null;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		// champ Decision  du formulaire
		addEventListener(ApplicationEvents.ON_DECISION, this);
		lstDecision.setItemRenderer(new decisionRenderer());
		pgDecision.setPageSize(byPageBandbox);
		pgDecision.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_DECISION);
		Events.postEvent(ApplicationEvents.ON_DECISION, this, null);
		
		// champ type marche  du formulaire
		addEventListener(ApplicationEvents.ON_TYPE_MARCHES, this);
		lstTypemarche.setItemRenderer(new TypemarcheRenderer());
		pgTypemarche.setPageSize(byPageBandbox);
		pgTypemarche.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_TYPE_MARCHES);
		Events.postEvent(ApplicationEvents.ON_TYPE_MARCHES, this, null);
		
		
		// champ Mode passation  du formulaire
		addEventListener(ApplicationEvents.ON_MODE_PASSATIONS, this);
		lstModepassation.setItemRenderer(new ModepassationRenderer());
		pgModepassation.setPageSize(byPageBandbox);
		pgModepassation.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODE_PASSATIONS);
		Events.postEvent(ApplicationEvents.ON_MODE_PASSATIONS, this, null);
		
		//Type AC
		addEventListener(ApplicationEvents.ON_TYPE_AUTORITES, this);
		lstTypeAC.setItemRenderer(new TypeAutoriteRenderer());
		pgTypeAC.setPageSize(byPageBandbox);
		pgTypeAC.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_TYPE_AUTORITES);
		Events.postEvent(ApplicationEvents.ON_TYPE_AUTORITES, this, null);
		
            
		//AC
		addEventListener(ApplicationEvents.ON_AUTORITES, this);
		lstAutorite.setItemRenderer(new AutoriteRenderer());
		pgAutorite.setPageSize(byPageBandbox);
		pgAutorite.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTORITES);
		//Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);
		
		//PRocedure
//		addEventListener(ApplicationEvents.ON_PROCEDURE, this);
//		lstProcedure.setItemRenderer(new ProcedureRenderer());
//		pgProcedure.setPageSize(byPageBandbox);
//		pgProcedure.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_PROCEDURE);
//		

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	}

	@Override
	public void onEvent(Event event) throws Exception {
		
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_TYPE_AUTORITES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgTypeAC.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_LIST_BY_PAGES;
				activePage = pgTypeAC.getActivePage() * byPage;
				pgTypeAC.setPageSize(byPage);
			}
			List<SygTypeAutoriteContractante> typeautorite = BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).find(activePage, byPage, null,libelle);
			lstTypeAC.setModel(new SimpleListModel(typeautorite));
			pgTypeAC.setTotalSize(BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).count(null,libelle));
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTORITES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgAutorite.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_LIST_BY_PAGES;
				activePage = pgAutorite.getActivePage() * byPage;
				pgAutorite.setPageSize(byPage);
			}
			List<SygAutoriteContractante> autorite = BeanLocator.defaultLookup(AutoriteContractanteSession.class).find(activePage, byPage, denomination,null,typeautorite,null,null);
			lstAutorite.setModel(new SimpleListModel(autorite));
			pgAutorite.setTotalSize(BeanLocator.defaultLookup(AutoriteContractanteSession.class).count(denomination,null,typeautorite,null,null));
		}
		//Procedure
//		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_PROCEDURE)) {
//			if (event.getData() != null) {
//				activePage = Integer.parseInt((String) event.getData());
//				byPage = -1;
//				pgProcedure.setPageSize(1000);
//			} else {
//				byPage = UIConstants.DSP_LIST_BY_PAGES;
//				activePage = pgProcedure.getActivePage() * byPage;
//				pgProcedure.setPageSize(byPage);
//			}
//			List<SygAppelsOffres> procedure = BeanLocator.defaultLookup(AppelsOffresSession.class).findRech(activePage, byPage,null, autorite);
//			lstProcedure.setModel(new SimpleListModel(procedure));
//			pgProcedure.setTotalSize(BeanLocator.defaultLookup(AppelsOffresSession.class).countRech(null,autorite));
//		}
		// champ decision
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DECISION)) {
			List<SygTypeDecision> typedecision = BeanLocator.defaultLookup(TypeDecisionSession.class).find(
					pgDecision.getActivePage() * byPageBandbox, byPageBandbox, null, LibelleDecision);
			SimpleListModel listModel = new SimpleListModel(typedecision);
			lstDecision.setModel(listModel);
			pgDecision.setTotalSize(BeanLocator.defaultLookup(TypeDecisionSession.class).count(null, LibelleDecision));
		}
		
		// champ type marche
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_TYPE_MARCHES)) {
			List<SygTypesmarches> typemarche = BeanLocator.defaultLookup(TypesmarchesSession.class).find(
					pgTypemarche.getActivePage() * byPageBandbox, byPageBandbox, null, LibelleTypemarche, null);
			SimpleListModel listModel = new SimpleListModel(typemarche);
			lstTypemarche.setModel(listModel);
			pgTypemarche.setTotalSize(BeanLocator.defaultLookup(TypesmarchesSession.class).count(null, LibelleTypemarche,null));
		}
		// champ Mode passation
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODE_PASSATIONS)) {
			List<SygModepassation> modepassation = BeanLocator.defaultLookup(ModepassationSession.class).find(
					pgModepassation.getActivePage() * byPageBandbox, byPageBandbox, null, LibelleModepassation);
			SimpleListModel listModel = new SimpleListModel(modepassation);
			lstModepassation.setModel(listModel);
			pgModepassation.setTotalSize(BeanLocator.defaultLookup(ModepassationSession.class).count(null, LibelleModepassation));
		}
		else if (event.getName().equalsIgnoreCase(Events.ON_CLICK)) {
			String libelleimage = (String) (((Listcell) event.getTarget()).getAttribute("libelleimage"));
			if (libelleimage == "supconteneur") {
				HashMap<String, String> display = new HashMap<String, String>();
				display.put(MessageBoxController.DSP_MESSAGE, " Etes vous sur de vouloir supprimer?");
				display.put(MessageBoxController.DSP_TITLE, "Supprimer le dossier");
				display.put(MessageBoxController.DSP_HEIGHT, "150px");
				display.put(MessageBoxController.DSP_WIDTH, "100px");
				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				showMessageBox(display, map);
			}
		}
	}

	// //////////Decision
	public void onSelect$lstDecision() {
		bdDecision.setValue((String) lstDecision.getSelectedItem().getAttribute(UIConstants.ATTRIBUTE_LIBELLE));
		bdDecision.close();
	}

	public class decisionRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypeDecision typedecision = (SygTypeDecision) data;
			item.setValue(typedecision);
			item.setAttribute(UIConstants.ATTRIBUTE_LIBELLE, typedecision.getLibelle());

			Listcell cellLibelle = new Listcell("");
			if (typedecision.getLibelle() != null) {
				cellLibelle.setLabel(typedecision.getLibelle());
			}
			cellLibelle.setParent(item);
			//				

		}
	}

	public void onFocus$txtRechercherDecision() {
		if (txtRechercherDecision.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.Decision"))) {
			txtRechercherDecision.setValue("");
		}
	}

	public void onBlur$txtRechercherDecision() {
		if (txtRechercherDecision.getValue().equalsIgnoreCase("")) {
			txtRechercherDecision.setValue(Labels.getLabel("kermel.contentieux.Decision"));
		}
	}

	public void onClick$btnRechercherDecision() {
		if (txtRechercherDecision.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.Decision")) || txtRechercherDecision.getValue().equals("")) {
			LibelleDecision = null;
		} else {
			LibelleDecision = txtRechercherDecision.getValue();
		}
		Events.postEvent(ApplicationEvents.ON_DECISION, this, null);
	}

	// ////////////////fin ////////////////////////////////
	
	// //////////Type marche
	public void onSelect$lstTypemarche() {
		bdTypemarche.setValue((String) lstTypemarche.getSelectedItem().getAttribute(UIConstants.ATTRIBUTE_LIBELLE));
		bdTypemarche.close();
	}

	public class TypemarcheRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypesmarches typemarche = (SygTypesmarches) data;
			item.setValue(typemarche);
			item.setAttribute(UIConstants.ATTRIBUTE_LIBELLE, typemarche.getLibelle());

			Listcell cellLibelle = new Listcell("");
			if (typemarche.getLibelle() != null) {
				cellLibelle.setLabel(typemarche.getLibelle());
			}
			cellLibelle.setParent(item);
			//				

		}
	}

	public void onFocus$txtRechercherTypemarche() {
		if (txtRechercherTypemarche.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.typemarche"))) {
			txtRechercherTypemarche.setValue("");
		}
	}

	public void onBlur$txtRechercherTypemarche() {
		if (txtRechercherTypemarche.getValue().equalsIgnoreCase("")) {
			txtRechercherTypemarche.setValue(Labels.getLabel("kermel.contentieux.typemarche"));
		}
	}

	public void onClick$btnRechercherTypemarche() {
		if (txtRechercherTypemarche.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.typemarche")) || txtRechercherDecision.getValue().equals("")) {
			LibelleTypemarche = null;
		} else {
			LibelleTypemarche = txtRechercherTypemarche.getValue();
		}
		Events.postEvent(ApplicationEvents.ON_TYPE_MARCHES, this, null);
	}

	// ////////////////fin ////////////////////////////////
	
	// //////////Mode de passation
	public void onSelect$lstModepassation() {
		bdModepassation.setValue((String) lstModepassation.getSelectedItem().getAttribute(UIConstants.ATTRIBUTE_LIBELLE));
		bdModepassation.close();
	}

	public class ModepassationRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygModepassation modepassation = (SygModepassation) data;
			item.setValue(modepassation);
			item.setAttribute(UIConstants.ATTRIBUTE_LIBELLE, modepassation.getLibelle());

			Listcell cellLibelle = new Listcell("");
			if (modepassation.getLibelle() != null) {
				cellLibelle.setLabel(modepassation.getLibelle());
			}
			cellLibelle.setParent(item);
			//				

		}
	}

	public void onFocus$txtRechercherModepassation() {
		if (txtRechercherModepassation.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.modepassation"))) {
			txtRechercherModepassation.setValue("");
		}
	}

	public void onBlur$txtRechercherModepassation() {
		if (txtRechercherModepassation.getValue().equalsIgnoreCase("")) {
			txtRechercherModepassation.setValue(Labels.getLabel("kermel.contentieux.modepassation"));
		}
	}

	public void onClick$btnRechercherModepassation() {
		if (txtRechercherModepassation.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.modepassation")) || txtRechercherDecision.getValue().equals("")) {
			LibelleModepassation = null;
		} else {
			LibelleModepassation = txtRechercherModepassation.getValue();
		}
		Events.postEvent(ApplicationEvents.ON_MODE_PASSATIONS, this, null);
	}

	// ////////////////fin ////////////////////////////////

	// /////////////////
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent createEvent) {
		Map<String, Object> windowParams = (Map<String, Object>) createEvent.getArg();
		mode = (String) windowParams.get(WINDOW_PARAM_MODE);
		if (mode == null) {
			
		} else {
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				code = (Long) windowParams.get(PARAM_WINDOW_CODE);
				contdecision = BeanLocator.defaultLookup(DecisionsContentieuxSession.class).findById(code);
				contdecisions = BeanLocator.defaultLookup(DecisionsContentieuxSession.class).find(0, -1, null, null);
				
				
				txtnumdecision.setValue(contdecision.getDecNumero());
				txtRsociale.setValue(contdecision.getDecAuteurrecour());
				bdDecision.setValue(contdecision.getTypedcision().getLibelle());
				bdTypemarche.setValue(contdecision.getTypemarche().getLibelle());
				daterecours.setValue(contdecision.getDecDateRecour());
				txtObjet.setValue(contdecision.getDecObjet());
				datedecision.setValue(contdecision.getDecDate());
				bdModepassation.setValue(contdecision.getModepassation().getLibelle());
				 txtVersionElectronique.setValue(contdecision.getDecFichier());
				
				autorite = contdecisions.get(0).getAutoritecontractante();
				typeautorite=autorite.getType();
				
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

			}
		}

	}

	private boolean checkFieldConstraints() {

		try {
			
			if(txtnumdecision.getValue().equals(""))
		     {
                errorComponent = txtnumdecision;
                errorMsg = Labels.getLabel("kermel.contentieux.numerodecision")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			
			if (datedecision.getValue() == null) {

				errorComponent = datedecision;
				errorMsg = Labels.getLabel("kermel.contentieux.datedecision") + ": " + Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);

			}
			
			if(bdDecision.getValue().equals(""))
		     {
            errorComponent = bdDecision;
            errorMsg = Labels.getLabel("kermel.contentieux.Decision")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }

			if(txtObjet.getValue().equals(""))
		     {
             errorComponent = txtObjet;
             errorMsg = Labels.getLabel("kermel.contentieux.Objet")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			

			if(txtRsociale.getValue().equals(""))
		     {
             errorComponent = txtRsociale;
             errorMsg = Labels.getLabel("kermel.contentieux.RaisonSociale")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			
			
			if(bdTypemarche.getValue().equals(""))
		     {
            errorComponent = bdTypemarche;
            errorMsg = Labels.getLabel("kermel.contentieux.typemarche")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			
			if(bdModepassation.getValue().equals(""))
		     {
            errorComponent = bdModepassation;
            errorMsg = Labels.getLabel("kermel.contentieux.modepassation")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(txtVersionElectronique.getValue().equals(""))
		     {
             errorComponent = txtVersionElectronique;
             errorMsg = Labels.getLabel("kermel.contentieux.pj.Fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }

			/* Controle date inferieure a date du jour */

			if (daterecours.getValue() != null) {
				datejour = new Date();
				if ((daterecours.getValue()).after((datejour))) {
				errorComponent = daterecours;
					errorMsg = Labels.getLabel("kermel.contentieux.Controledate")+": "+ UtilVue.getInstance().formateLaDate(datejour);
					lbStatusBar.setStyle(ERROR_MSG_STYLE);
					lbStatusBar.setValue(errorMsg);
					throw new WrongValueException(errorComponent, errorMsg);				}
			}
			return true;

		} catch (Exception e) {
			errorMsg = Labels.getLabel("cciad.erreur.erreurinconnue") + ": " + e.toString() + " [checkFieldConstraints]";
			errorComponent = null;
			return false;

		}

	}
	
	////Fichier 
	public void onClick$btnChoixFichier() {

		if (ToolKermel.isWindows())
			nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Dec_cont", cheminDossier.replaceAll("/", "\\\\"));
		else
			nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Dec_cont", cheminDossier.replaceAll("\\\\", "/"));

		txtVersionElectronique.setValue(nomFichier);
	}
	//Step01**********************************************************************************
	public void onClick$menuCancelStep01() {
		detach();
	}

	public void onClick$menuNextStep01() {
		if (lstTypeAC.getSelectedItem() == null) {
			throw new WrongValueException(lstTypeAC, Labels.getLabel("cciad.error.select.item"));
		} else {
			typeautorite = (SygTypeAutoriteContractante) lstTypeAC.getSelectedItem().getValue();

			step01.setVisible(false);
			step0.setVisible(true);
			//step1.setVisible(false);
			step2.setVisible(false);
			
			Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);
			
		}
	}

	
	//Step0**************************************************************************
	public void onClick$menuCancelStep0() {
		detach();
	}

	public void onClick$menuNextStep0() {
		if (lstTypeAC.getSelectedItem() == null){
			throw new WrongValueException(lstTypeAC, Labels.getLabel("cciad.error.select.item"));
		}
		else if (lstAutorite.getSelectedItem() == null) {
			throw new WrongValueException(lstAutorite, Labels.getLabel("cciad.error.select.item"));
		} else {
			typeautorite = (SygTypeAutoriteContractante) lstTypeAC.getSelectedItem().getValue();
			autorite = (SygAutoriteContractante) lstAutorite.getSelectedItem().getValue();
			
			lblDenomination.setValue(autorite.getDenomination());
			lblTypeAC.setValue(autorite.getType().getLibelle());
			
			step01.setVisible(false);
			step0.setVisible(false);
			//step1.setVisible(true);
			step2.setVisible(true);
			
			//Events.postEvent(ApplicationEvents.ON_PROCEDURE, this, null);
			
		}
	}

	
	public void onClick$menuPreviousStep0() {
		
		step01.setVisible(true);
		step0.setVisible(false);
		//step1.setVisible(false);
		step2.setVisible(false);
		
	}
	

	
//Step2*****************************************************************
	public void onClick$menuNextStep2() {
		if (checkFieldConstraints()) {

			//decision
			if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				typedecision = (SygTypeDecision) lstDecision.getSelectedItem().getValue();

			} 
			else {
				if (bdDecision.getValue().equals(contdecision.getTypedcision().getLibelle())) {
					typedecision = contdecision.getTypedcision();
				} else {
					typedecision = (SygTypeDecision) lstDecision.getSelectedItem().getValue();
				}
			}
			
			//Type marche
			if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				typemarche = (SygTypesmarches) lstTypemarche.getSelectedItem().getValue();

			} 
			else {
				if (bdTypemarche.getValue().equals(contdecision.getTypemarche().getLibelle())) {
					typemarche = contdecision.getTypemarche();
				} else {
					typemarche = (SygTypesmarches) lstTypemarche.getSelectedItem().getValue();
				}
			}
			
			//Mode de passation
			if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				modepassation = (SygModepassation) lstModepassation.getSelectedItem().getValue();

			} 
			else {
				if (bdModepassation.getValue().equals(contdecision.getModepassation().getLibelle())) {
					modepassation = contdecision.getModepassation();
				} else {
					modepassation = (SygModepassation) lstModepassation.getSelectedItem().getValue();
				}
			}
			
			 //numenregistrement="CONT_"+BeanLocator.defaultLookup(ContentieuxSession.class).getGeneratedCode(BeConstants.PARAM_NUMRECU);
			
			
			   
			
			contdecision.setDecAuteurrecour(txtRsociale.getValue());
			contdecision.setDecObjet(txtObjet.getValue());
			contdecision.setDecNumero(txtnumdecision.getValue());
			
				
			
			contdecision.setTypedcision(typedecision);
			contdecision.setTypemarche(typemarche);
			contdecision.setModepassation(modepassation);
			contdecision.setAutoritecontractante(autorite);
			
			
			contdecision.setDecDateRecour(daterecours.getValue());
			
			
			
			contdecision.setDecDate(datedecision.getValue()); 
			contdecision.setDecFichier(txtVersionElectronique.getValue());
			 
			//datestatut = new Date();
			//contdecision.setDateStatut(datestatut);
			contdecision.setDecStatut("Saisie");

			if (mode == null) {

				BeanLocator.defaultLookup(DecisionsContentieuxSession.class).save(contdecision);
				mode = UIConstants.MODE_EDIT;
			} else {
				if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {

					BeanLocator.defaultLookup(DecisionsContentieuxSession.class).update(contdecision);
				} else {

					BeanLocator.defaultLookup(DecisionsContentieuxSession.class).save(contdecision);
					mode = UIConstants.MODE_EDIT;
				}
			}
			//step1.setVisible(false);
			step2.setVisible(false);
			loadApplicationState("saisi_decision");
			detach();
			
		}
	}



	// ///////////////Type Autorite////////////////////////////////////////////////////

	public class TypeAutoriteRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypeAutoriteContractante typeautorite = (SygTypeAutoriteContractante) data;
			item.setValue(typeautorite);

			Listcell cellLibelle = new Listcell(typeautorite.getLibelle());
			cellLibelle.setParent(item);




			if (mode != null) {
				if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
					if (typeautorite.getId().equals(contdecisions.get(0).getAutoritecontractante().getType().getId())) {
						item.setSelected(true);
					}

				}
			}

		}
	}

	// ////////AC AC///////////////////////////////////////////////////////////

	public class AutoriteRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygAutoriteContractante autorite = (SygAutoriteContractante) data;
			item.setValue(autorite);

			Listcell cellDenomination = new Listcell(autorite.getDenomination());
			cellDenomination.setParent(item);

//			Listcell cellAdresse = new Listcell(autorite.getAdresse());
//			cellAdresse.setParent(item);
//
//			Listcell cellTel = new Listcell(autorite.getTelephone());
//			cellTel.setParent(item);
//			
//			Listcell cellFax = new Listcell(autorite.getFax());
//			cellFax.setParent(item);
//			
//			Listcell cellEmail = new Listcell(autorite.getEmail());
//			cellEmail.setParent(item);



			if (mode != null) {
				if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
					if (autorite.getId().equals(contdecisions.get(0).getAutoritecontractante().getId())) {
						item.setSelected(true);
					}

				}
			}

		}
	}
	
	

//////////////*******************Denomination*******//////////////////////

	// function du bouton recherche par Denomination
	public void onFocus$txtDenomination() {
		if (txtDenomination.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.denomination")))
			txtDenomination.setValue("");

	}

	public void onBlur$txtDenomination() {
		if (txtDenomination.getValue().equals(""))
			txtDenomination.setValue(Labels.getLabel("kermel.referentiel.common.denomination"));
	}

	public void onOK$txtDenomination() {
		onClick$bchercherAC();
	}

	
	public void onClick$bchercherAC() {

		if (txtDenomination.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.denomination")) || txtDenomination.getValue().equals("")) {
			denomination = null;
		} else {
			denomination = txtDenomination.getValue();
		}

		Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);

	}

	public void onOK$bchercherAC() {
		onClick$bchercherAC();
	}

	//////////////*******************Denomination*******//////////////////////
	
	
	
	public void onClick$menuCancelStep2() {
		loadApplicationState("saisi_decision");
		detach();
	}


	
	public void onClick$menuPreviousStep2() {
		
		step01.setVisible(false);
		step0.setVisible(true);
		//step1.setVisible(true);
		step2.setVisible(false);
		
	}

	// //////////////////////////////////////////////////

	// /liste autorite
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygAutoriteContractante autorite = (SygAutoriteContractante) data;
		item.setValue(autorite.getId());

	}

}