package sn.ssi.kermel.web.contentieux.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.contentieux.ejb.ContentieuxSession;
import sn.ssi.kermel.be.entity.SygContentieux;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;


@SuppressWarnings("serial")
public class ListContentieuxController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtNom,txtPrenom;
    String nom=null,page=null,prenom=null;
    private Listheader lshNom,lshPrenom,lshRsocial;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_CONTENTIEUX, MOD_CONTENTIEUX, SUPP_CONTENTIEUX, WSUIVI_CONTENTIEUX;
    private String login;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_CONTENTIEUX);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_CONTENTIEUX != null) { ADD_CONTENTIEUX.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_CONTENTIEUX != null) { MOD_CONTENTIEUX.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUPP_CONTENTIEUX != null) { SUPP_CONTENTIEUX.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		if (WSUIVI_CONTENTIEUX != null) { WSUIVI_CONTENTIEUX.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_SUIVI); }
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_SUIVI, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		lshNom.setSortAscending(new FieldComparator("raisonSocial", false));
		lshNom.setSortDescending(new FieldComparator("raisonSocial", true));
		
//		lshPrenom.setSortAscending(new FieldComparator("prenom", false));
//		lshPrenom.setSortDescending(new FieldComparator("prenom", true));
//		
//		lshRsocial.setSortAscending(new FieldComparator("raisonSocial", false));
//		lshRsocial.setSortDescending(new FieldComparator("raisonSocial", true));
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	login = ((String) getHttpSession().getAttribute("user"));
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygContentieux> contentieux = BeanLocator.defaultLookup(ContentieuxSession.class).findRech(activePage,byPage,null,nom,null);
			 SimpleListModel listModel = new SimpleListModel(contentieux);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup(ContentieuxSession.class).countRech(null,nom,null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/contentieux/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"655px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ContentieuxFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			final String uri = "/contentieux/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"655px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ContentieuxFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(ContentieuxFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
				
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(ContentieuxSession.class).delete(codes);
				BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_CONTENTIEUX", Labels.getLabel("kermel.common.contentieux.suppression")+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
		
		// ///////Bouton suivi

			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_SUIVI)) {

				if (lstListe.getSelectedItem() == null) {
					throw new WrongValueException(lstListe, "Veillez selectionnez un element svp!");
				} else {
					if (lstListe.getSelectedCount() > 1) {
						throw new WrongValueException(lstListe, "Veillez selectionnez un seul element svp!");
					} else {
						
						session.setAttribute("numcontentieux", lstListe.getSelectedItem().getValue());
						loadApplicationState("suivi_contentieux");

					}
				}
			}
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygContentieux contentieux = (SygContentieux) data;
		item.setValue(contentieux.getId());

//		 Listcell cellPrenom = new Listcell(contentieux.getPrenom());
//		 cellPrenom.setParent(item);
//		 
		 Listcell cellNumero= new Listcell(contentieux.getNumero());
		 cellNumero.setParent(item);
		 
		 Listcell cellDaterecours = new
		 Listcell(UtilVue.getInstance().formateLaDate(contentieux.getDaterecours())+" "+UtilVue.getInstance().formateLHeure(contentieux.getHeure()));
		 cellDaterecours.setParent(item);
		 
		 Listcell cellRaisonSociale = new Listcell(contentieux.getRaisonSocial());
		 cellRaisonSociale.setParent(item);
		 
		 Listcell cellObjet = new Listcell(contentieux.getObjet());
		 cellObjet.setParent(item);
		 
		 Listcell cellAC = new Listcell(contentieux.getAutoritecontractante().getDenomination()+"("+contentieux.getAutoritecontractante().getSigle()+")");
		 cellAC.setParent(item);
//		 
//		 Listcell cellAppelOffre = new Listcell(contentieux.getAppeloffre().getAporeference());
//		 cellAppelOffre.setParent(item);
		 
//		 Listcell cellDecision = new Listcell(contentieux.getDecision().getLibelle());
//		 cellDecision.setParent(item);
		 
//		 Listcell cellTelephone = new Listcell(contentieux.getTelephone());
//		 cellTelephone.setParent(item);
//		 
//		 Listcell cellFax = new Listcell(contentieux.getFax());
//		 cellFax.setParent(item);
//		 
//		 Listcell cellEmail = new Listcell(contentieux.getEmail());
//		 cellEmail.setParent(item);
		 
		
		 

		 
		

	}
	public void onClick$bchercher()
	{
		
		if((txtNom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.NomRaisonSociale")))||(txtNom.getValue().equals("")))
		 {
			nom=null;
			
		 }
		else
		{
			nom=txtNom.getValue();
			page="0";
		}
		
//		if((txtPrenom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.Prenom")))||(txtPrenom.getValue().equals("")))
//		 {
//			prenom=null;
//		
//		 }
//		else
//		{
//			prenom=txtPrenom.getValue();
//			page="0";
//		}
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	//Nom
	public void onFocus$txtNom()
	{
		if(txtNom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.NomRaisonSociale")))
			txtNom.setValue("");
		
	}
	
	public void onBlur$txtNom()
	{
		if(txtNom.getValue().equals(""))
			txtNom.setValue(Labels.getLabel("kermel.contentieux.NomRaisonSociale"));
	}

	public void onOK$txtNom()
	{
		onClick$bchercher();
	}
	
	//Prenom
	
//	public void onFocus$txtPrenom()
//	{
//		if(txtPrenom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.Prenom")))
//			txtPrenom.setValue("");
//		
//	}
//	
//	public void onBlur$txtPrenom()
//	{
//		if(txtPrenom.getValue().equals(""))
//			txtPrenom.setValue(Labels.getLabel("kermel.contentieux.Prenom"));
//	}
//
//	public void onOK$txtPrenom()
//	{
//		onClick$bchercher();
//	}
//	
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
}