package sn.ssi.kermel.web.referentiel.suivieformation;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygGroupeFormation;
import sn.ssi.kermel.be.entity.SygMembresGroupes;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.referentiel.ejb.GroupeFormationSession;
import sn.ssi.kermel.be.session.ParticipantsSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

public class ListMembresGroupesController extends AbstractWindow implements
		AfterCompose, EventListener, ListitemRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Listheader lhlprenom, lhlnom;
	private Paging pg;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE = "CURRENT_MODULE";
	private Listbox list_participant;
	private String prenom, nom;
	private Textbox txtPrenom, txtNom;
	private Long idformation;
	private SygProformation formation;
	private SygGroupeFormation groupe;
    private Label lblGroupe;
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);


		list_participant.setItemRenderer(this);
		
			
		pg.setPageSize(byPage);

		pg.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);

		
		
	}
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		groupe = (SygGroupeFormation) getHttpSession().getAttribute("membresgroupe");
		lblGroupe.setValue(groupe.getLibelle());

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			List<SygMembresGroupes> listparticipant = BeanLocator.defaultLookup(GroupeFormationSession.class).findMembres(pg.getActivePage() * byPage, byPage,groupe);
			SimpleListModel listModel = new SimpleListModel(listparticipant);
			list_participant.setModel(listModel);
			pg.setTotalSize(BeanLocator.defaultLookup(GroupeFormationSession.class).countMembres(groupe));
			getHttpSession().setAttribute("nbre_participants", BeanLocator.defaultLookup(ParticipantsSession.class).count(prenom, nom, formation,groupe));
		}
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String url = "/groupedeformation/formmembre.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels
					.getLabel("kermel.nouveau.participant"));
			display.put(DSP_HEIGHT, "500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormParticipantsController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);

			showPopupWindow(url, data, display);
		}

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list_participant.getSelectedItem() == null)
				throw new WrongValueException(list_participant, Labels
						.getLabel("kermel.formation.message.selection"));

			final String url = "/groupedeformation/formmembre.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT, "500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels
					.getLabel("kermel.formation.participant.modif.tire"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormParticipantsController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(FormParticipantsController.PARAM_WIDOW_CODE,
					list_participant.getSelectedItem().getValue());

			showPopupWindow(url, data, display);
		}

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)) {
			if (list_participant.getSelectedItem() == null)
				throw new WrongValueException(list_participant, Labels
						.getLabel("kermel.formation.message.selection"));

			HashMap<String, String> display = new HashMap<String, String>();

			display.put(MessageBoxController.DSP_MESSAGE, Labels
					.getLabel("kermel.formation.message.suppression"));
			display.put(MessageBoxController.DSP_TITLE, Labels
					.getLabel("kermel.formation.titre.suppression"));
			display.put(MessageBoxController.DSP_HEIGHT, "250px");
			display.put(MessageBoxController.DSP_WIDTH, "47%");

			HashMap<String, Object> map = new HashMap<String, Object>();

			map.put(CURRENT_MODULE, list_participant.getSelectedItem()
					.getValue());
			showMessageBox(display, map);

		}

		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {
			for (int i = 0; i < list_participant.getSelectedCount(); i++) {
				Integer codes = (Integer) ((Listitem) list_participant.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(ParticipantsSession.class).delete(codes);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}

	}

	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygMembresGroupes participant = (SygMembresGroupes) data;
		item.setValue(participant.getId());

		if (participant.getPrenom() != null) {
			Listcell cellPrenom = new Listcell(participant.getPrenom());
			cellPrenom.setParent(item);
		} else {
			Listcell cellPrenom = new Listcell("");
			cellPrenom.setParent(item);
		}

		if (participant.getNom() != null) {
			Listcell cellNom = new Listcell(participant.getNom());
			cellNom.setParent(item);
		} else {
			Listcell cellNom = new Listcell("");
			cellNom.setParent(item);
		}

		if (participant.getTelephone() != null) {
			Listcell cellTel = new Listcell(participant.getTelephone());
			cellTel.setParent(item);
		} else {
			Listcell cellTel = new Listcell("");
			cellTel.setParent(item);
		}

		if (participant.getMobile() != null) {
			Listcell cellMob = new Listcell(participant.getMobile());
			cellMob.setParent(item);
		} else {
			Listcell cellMob = new Listcell("");
			cellMob.setParent(item);
		}

		if (participant.getEmail() != null) {
			Listcell cellMail = new Listcell(participant.getEmail());
			cellMail.setParent(item);
		} else {
			Listcell cellMail = new Listcell("");
			cellMail.setParent(item);
		}
		
		if (participant.getAdresse() != null) {
			Listcell cellAdresse = new Listcell(participant.getAdresse());
			cellAdresse.setParent(item);
		} else {
			Listcell cellAdresse = new Listcell("");
			cellAdresse.setParent(item);
		}
	}

	public void onClick$bchercher() {
		onOK();
	}

	public void onOK() {
		prenom = txtPrenom.getValue();
		nom = txtNom.getValue();

		if (!prenom.equalsIgnoreCase(Labels
				.getLabel("kermel.formation.participant.prenom"))) {
			if (nom.equalsIgnoreCase(Labels
					.getLabel("kermel.formation.participant.nom"))) {
				nom = "";
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

		} else if (!nom.equalsIgnoreCase(Labels
				.getLabel("kermel.formation.participant.nom"))) {
			if (prenom.equalsIgnoreCase(Labels
					.getLabel("kermel.formation.participant.prenom"))) {
				prenom = "";
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		} else
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

	}

	public void onFocus$txtPrenom() {
		if (txtPrenom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.formation.participant.prenom")))
			txtPrenom.setValue("");
		else
			prenom = txtPrenom.getValue();
	}

	public void onFocus$txtNom() {
		if (txtNom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.formation.participant.nom")))
			txtNom.setValue("");
		else
			nom = txtNom.getValue();
	}

	public void onBlur$txtPrenom() {
		if (txtPrenom.getValue().equals(""))
			txtPrenom.setValue(Labels.getLabel("kermel.formation.participant.prenom"));
	}

	public void onBlur$txtNom() {
		if (txtNom.getValue().equals(""))
			txtNom.setValue(Labels.getLabel("kermel.formation.participant.nom"));
	}
	
	public void onClick$menuFermer() {
		loadApplicationState("groupedeformation");
	}

}
