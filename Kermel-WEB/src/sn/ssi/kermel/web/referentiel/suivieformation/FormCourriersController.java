package sn.ssi.kermel.web.referentiel.suivieformation;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.EaCourriers;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.be.session.CourriersSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class FormCourriersController extends AbstractWindow implements
		EventListener, AfterCompose {

	private static final long serialVersionUID = 1L;
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,nomFichier;
	public static final String WINDOW_PARAM_MODE = "MODE";
	Integer code;
	private EaCourriers courrier = new EaCourriers();
	private Datebox txtDatesaisie, txtDatereception, txtdatecourrier;
	private Textbox txtReferenceCourrier, txtOrigine, txtAutres,
			txtCourrierEnReference, txtObjet,txtVersionElectronique;
	private SygProformation formation;
	private Button btnChoixFichier;
	private org.zkoss.util.media.Media nomPiece;
	private final String cheminDossier = UIConstants.PATH_PJ;
	private Label lbStatusBar;
	UtilVue utilVue = UtilVue.getInstance();
	   Session session = getHttpSession();
	   private Long idformation;
	   
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {

		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(PARAM_WINDOW_MODE);
		idformation = (Long) getHttpSession().getAttribute("idformation");
		formation = BeanLocator.defaultLookup(ProformationSession.class).findById(idformation);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Integer) map.get(PARAM_WIDOW_CODE);
			courrier = BeanLocator.defaultLookup(CourriersSession.class)
					.findByCode(code);
			txtDatesaisie.setValue(courrier.getDatesaisie());
			txtDatereception.setValue(courrier.getDatereception());
			txtdatecourrier.setValue(courrier.getDatecourrier());
			txtReferenceCourrier.setValue(courrier.getReferencecourrier());
			txtOrigine.setValue(courrier.getOrigine());
			txtAutres.setValue(courrier.getAutres());
			txtCourrierEnReference.setValue(courrier.getCourrierenreference());
			txtObjet.setValue(courrier.getObjet());
			txtVersionElectronique.setValue(courrier.getVelectroniq());
			  
		} else if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			
			
			txtDatesaisie.setValue(new Date());
		}
		
		
		 	
	}

	public void onOK() {

		if(txtDatesaisie.getValue()==null || "".equals(txtDatesaisie.getValue())){
			lbStatusBar.setValue(Labels.getLabel("kermel.rensigner")+" "+Labels.getLabel("kermel.formation.courrier.datesaisie"));
			throw new WrongValueException(txtDatesaisie, Labels.getLabel("kermel.ChamNull"));
		}
		if(txtDatereception.getValue()==null || "".equals(txtDatereception.getValue())){
			lbStatusBar.setValue(Labels.getLabel("kermel.rensigner")+" "+Labels.getLabel("kermel.formation.courrier.datereception"));
			throw new WrongValueException(txtDatereception, Labels.getLabel("kermel.ChamNull"));
		}
		if(txtdatecourrier.getValue()==null || "".equals(txtdatecourrier.getValue())){
			lbStatusBar.setValue(Labels.getLabel("kermel.rensigner")+" "+Labels.getLabel("kermel.formation.courrier.datecourrier"));
			throw new WrongValueException(txtdatecourrier, Labels.getLabel("kermel.ChamNull"));
		}
		if(txtReferenceCourrier.getValue()==null || "".equals(txtReferenceCourrier.getValue())){
			lbStatusBar.setValue(Labels.getLabel("kermel.rensigner")+" "+Labels.getLabel("kermel.formation.courrier.referencecourrier"));
			throw new WrongValueException(txtReferenceCourrier, Labels.getLabel("kermel.ChamNull"));
		}
		if(txtOrigine.getValue()==null || "".equals(txtOrigine.getValue())){
			lbStatusBar.setValue(Labels.getLabel("kermel.rensigner")+" "+Labels.getLabel("kermel.formation.courrier.origine"));
			throw new WrongValueException(txtOrigine, Labels.getLabel("kermel.ChamNull"));
		}
		if(txtCourrierEnReference.getValue()==null || "".equals(txtCourrierEnReference.getValue())){
			lbStatusBar.setValue(Labels.getLabel("kermel.rensigner")+" "+Labels.getLabel("kermel.formation.courrier.courrierenreference"));
			throw new WrongValueException(txtCourrierEnReference, Labels.getLabel("kermel.ChamNull"));
		}
		courrier.setDatesaisie(txtDatesaisie.getValue());
		courrier.setDatereception(txtDatereception.getValue());
		courrier.setDatecourrier(txtdatecourrier.getValue());
		courrier.setReferencecourrier(txtReferenceCourrier.getValue());
		courrier.setOrigine(txtOrigine.getValue());
		courrier.setAutres(txtAutres.getValue());
		courrier.setCourrierenreference(txtCourrierEnReference.getValue());
		courrier.setObjet(txtObjet.getValue());
		courrier.setFormation(formation);
		courrier.setVelectroniq(txtVersionElectronique.getValue());
		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			BeanLocator.defaultLookup(CourriersSession.class).save(courrier);

		} else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			BeanLocator.defaultLookup(CourriersSession.class).update(courrier);
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();

	}
	
	public void onClick$btnChoixFichier() {
		//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
			if (ToolKermel.isWindows())
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectronique.setValue(nomFichier);
		}
	
}
