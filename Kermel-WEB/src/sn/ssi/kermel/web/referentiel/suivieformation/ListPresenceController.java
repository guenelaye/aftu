package sn.ssi.kermel.web.referentiel.suivieformation;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygFPresenceParcipant;
import sn.ssi.kermel.be.entity.SygFichePresence;
import sn.ssi.kermel.be.entity.SygGroupeFormation;
import sn.ssi.kermel.be.entity.SygParticipantsFormation;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.referentiel.ejb.FPresenceParticipantSession;
import sn.ssi.kermel.be.referentiel.ejb.FichePresenceSession;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



@SuppressWarnings("serial")
public class ListPresenceController extends AbstractWindow  implements EventListener, AfterCompose,ListitemRenderer
 {

    private Listbox list;
	private Paging pg;

	public static final String PARAM_WINDOW_CODE = "CODE";
	private SygFPresenceParcipant fparticipant;
	private SygFichePresence fiche ;
    private SygParticipantsFormation participant;
    private SygGroupeFormation groupe;
    
    private  int byPage = UIConstants.DSP_GRID_ROW_BY_PAGE8;

	public static final String CURRENT_MODULE="CURRENT_MODULE";
	public static final String WINDOW_PARAM_MODE = "MODE";
	private String page=null;
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";

	private Date datefiche,dateDebuR,dateFinR;
	Long idFparticipant;
    private Long idformation;
    private SygProformation formation;
    Session session = getHttpSession();
    List<SygFPresenceParcipant>  listpart= new ArrayList<SygFPresenceParcipant>();
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
	
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
		list.setItemRenderer(this);
		pg.setPageSize(byPage);
		pg.addForward("onPaging", this,ApplicationEvents.ON_MODEL_CHANGE);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

		
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
	
		idformation = (Long) getHttpSession().getAttribute("idformation");
		formation = BeanLocator.defaultLookup(ProformationSession.class).findById(idformation);
	}

	/**
	 * Permet de gerer les evenements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			
			List<SygFichePresence> fiche = BeanLocator.defaultLookup(
					FichePresenceSession.class).findAllby(
					pg.getActivePage() * byPage, byPage, null, formation);
			SimpleListModel listModel = new SimpleListModel(fiche);
			list.setModel(listModel);
			pg.setTotalSize(BeanLocator.defaultLookup(
					FichePresenceSession.class).count(null, formation));
		}
	
 		   else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
			Listcell button = (Listcell) event.getTarget();
			String toDo = (String) button.getAttribute(UIConstants.TODO);
			Long id = (Long) list.getSelectedItem().getValue();
			if (toDo.equalsIgnoreCase("liste"))
			 {	
				final String url = "/referentiel/proformation/suivie/affichelistePresence.zul";

				final HashMap<String, String> display = new HashMap<String, String>();
				display.put(DSP_HEIGHT,"80%");
				display.put(DSP_WIDTH, "80%");
				display.put(DSP_TITLE, Labels.getLabel("kermel.formation.fichepresenceliste.liste"));
				final HashMap<String, Object> data = new HashMap<String, Object>();
//     	        data.put(AfficherListePresController.PARAM_WIDOW_CODE, id);
				session.setAttribute("id", id);
				showPopupWindow(url, data, display);
				
			 }
 		}
		
		
		}
	
		
    public void onClick$saisi() {
		
   	final String url = "/referentiel/proformation/suivie/ajoutFichePres.zul";

		final HashMap<String, String> display = new HashMap<String, String>();
		display.put(DSP_TITLE, Labels.getLabel("kermel.formation.fichepresence.saisirlist"));
		display.put(DSP_HEIGHT, "70%");
		display.put(DSP_WIDTH, "60%");

		final HashMap<String, Object> data = new HashMap<String, Object>();
		data.put(AjouFichePresenceController.PARAM_WIDOW_CODE,
				list.getSelectedItem().getValue());
		showPopupWindow(url, data, display);
    }
    

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
     SygFichePresence fiche= (SygFichePresence) data;
     item.setValue(fiche.getId());
     
		Listcell  cellJr,cellDat,cellF,cellP,cellA;
     if(fiche.getJour()!=null) {
    	 
			cellJr = new Listcell(fiche.getJour());
			cellJr.setParent(item);
		}
		else {
			cellJr = new Listcell("");
			cellJr.setParent(item);
		}
	
		
		if(fiche.getDateFiche()!=null) {
			cellDat = new Listcell(UtilVue.getInstance().formateLaDate(fiche.getDateFiche()));
			cellDat.setParent(item);
		}
		else {
			cellDat = new Listcell("");
			cellDat.setParent(item);
		}
		
		if(fiche.getNpresent()!=null) {
	    	 
			cellP = new Listcell(fiche.getNpresent().toString());
			cellP.setParent(item);
		}
		else {
			cellP = new Listcell("");
			cellP.setParent(item);
		}
		
		if(fiche.getNabs()!=null) {
	    	 
			cellA = new Listcell(fiche.getNabs().toString());
			cellA.setParent(item);
		}
		else {
			cellA = new Listcell("");
			cellA.setParent(item);
		}
		
		listpart= BeanLocator.defaultLookup(FPresenceParticipantSession.class).findAffListe(0, byPage, null, fiche);
		  	
			if(listpart.size() >0){					
			
			cellF = new Listcell();
			cellF.setImage("/images/liste.png");
			cellF.setAttribute(UIConstants.TODO, "liste");
	       // cellF.setAttribute("ficheAffiche",fiche);
			cellF.setTooltiptext("LISTE PARTICIPANT");
			cellF.addEventListener(Events.ON_CLICK, ListPresenceController.this);
			cellF.setParent(item);
		
	       }else
	       {
	    	   cellF = new Listcell(); 
	    	   cellF.setParent(item);
	       }
	}
	
}	
