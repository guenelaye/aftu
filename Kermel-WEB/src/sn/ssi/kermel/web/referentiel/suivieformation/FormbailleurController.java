package sn.ssi.kermel.web.referentiel.suivieformation;


import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.referentiel.ejb.BailleursSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class FormbailleurController extends AbstractWindow implements
		EventListener, AfterCompose {
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtNom;

	
	Long code;
	private	SygBailleurs bailleur=new SygBailleurs();

	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private String Lib;
	private String page = null;


	@Override
	public void onEvent(Event event) throws Exception {
		
	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
	
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WIDOW_CODE);
			bailleur = BeanLocator.defaultLookup(BailleursSession.class)
					.findById(code);
			
			
			txtNom.setValue(bailleur.getLibelle());
	
		}
	}

	public void onOK() {
		if(txtNom.getValue()==null || "".equals(txtNom.getValue())){
			throw new WrongValueException(txtNom, Labels.getLabel("kermel.ChamNull"));
		}
		
		
		bailleur.setLibelle(txtNom.getValue()); 
	
	
		
		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			BeanLocator.defaultLookup(BailleursSession.class).save(bailleur);

		} 
		else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			BeanLocator.defaultLookup(BailleursSession.class).update(bailleur);
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();

	}
	
	
}
