package sn.ssi.kermel.web.referentiel.suivieformation;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.EaPiecesJointes;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.be.session.PiecesJointesSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class FormPiecesJointesController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * akndiaye
	 */
	private static final long serialVersionUID = 1L;
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode;
	public static final String WINDOW_PARAM_MODE = "MODE";
	Integer code;
	private EaPiecesJointes piecejointe = new EaPiecesJointes();
	private Textbox txtLibelle, txtNomFichier, txtFormat, txttaille;
	private Datebox txtDatemisenligne;
	private Checkbox chkRapport;

	private Textbox txtFE;
	private Button btnChoixFichier;
	private org.zkoss.util.media.Media nomPiece;
	private final String cheminDossier = UIConstants.PATH_PJ;
	String dgrCode, pijcode;
	private Long idformation;
	private SygProformation formation;
	private Label lbStatusBar;
	private String errorMsg;
	private Component errorComponent;
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) {
			Button button = (Button) event.getTarget();
			String toDo = (String) button.getAttribute(UIConstants.TODO);

			if (toDo.equalsIgnoreCase("JOIN")) {

				if (ToolKermel.isWindows())
					nomPiece = FileLoader.uploadPieceDossier1(UtilVue.getInstance().formateLaDate(
									Calendar.getInstance().getTime()), "Pj",
							cheminDossier.replaceAll("/", "\\\\"));
				else
					nomPiece = FileLoader.uploadPieceDossier1(UtilVue.getInstance().formateLaDate(
									Calendar.getInstance().getTime()), "Pj",
							cheminDossier.replaceAll("\\\\", "/"));

				if (nomPiece != null) {
					btnChoixFichier.setAttribute(UIConstants.TODO, "DELETE");// pour

				} else

					throw new WrongValueException(btnChoixFichier, Labels.getLabel("kermel.selection.fichier"));

				dgrCode = UtilVue.getInstance().formateLaDate(
						Calendar.getInstance().getTime());
				pijcode = "Pj";
			} else if (toDo.equalsIgnoreCase("DELETE")) {

				if (ToolKermel.isWindows()) {
					nomPiece = FileLoader.uploadPieceDossier1(UtilVue.getInstance().formateLaDate(
									Calendar.getInstance().getTime()), "Pj",
							cheminDossier.replaceAll("/", "\\\\"));
					// txtFormat.setValue(nomPiece.substring(nomPiece.length()-3));
					// txtNomFichier.setValue(nomPiece);

				} else
					nomPiece = FileLoader.uploadPieceDossier1(UtilVue.getInstance().formateLaDate(
									Calendar.getInstance().getTime()), "Pj",
							cheminDossier.replaceAll("\\\\", "/"));

				if (nomPiece != null) {
					btnChoixFichier.setAttribute(UIConstants.TODO, "JOIN");// pour
				}
			}

		}

		txtFE.setValue(dgrCode + "_" + pijcode + "_" + nomPiece.getName());
		txtNomFichier.setValue(dgrCode+ "_"+ pijcode+ "_"+ nomPiece.getName().substring(0,
						nomPiece.getName().length() - 4));
		txtFormat.setValue(nomPiece.getFormat());
		txttaille.setValue(String.valueOf(nomPiece.getByteData().length / 1024));
	}

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		idformation = (Long) getHttpSession().getAttribute("idformation");

		formation = BeanLocator.defaultLookup(ProformationSession.class).findById(idformation);

	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {

		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(PARAM_WINDOW_MODE);
		btnChoixFichier.setAttribute(UIConstants.TODO, "JOIN");// Pour tenir

		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Integer) map.get(PARAM_WIDOW_CODE);
			piecejointe = BeanLocator.defaultLookup(PiecesJointesSession.class)
					.findByCode(code);
			txtLibelle.setValue(piecejointe.getLibelle());
			txtFE.setValue(piecejointe.getFichier());
			txtNomFichier.setValue(piecejointe.getNomfichier());
			txtFormat.setValue(piecejointe.getFormat());
			txttaille.setValue(piecejointe.getTaille());
			txtDatemisenligne.setValue(piecejointe.getDatemiseenligne());
			if (piecejointe.getRapport() == true) {
				chkRapport.setChecked(true);
			} else if (piecejointe.getRapport() == false) {
				chkRapport.setChecked(false);
			}
		}
		else
			piecejointe = new EaPiecesJointes();
		
		btnChoixFichier.addEventListener(Events.ON_CLICK, this);
		txtDatemisenligne.setValue(new Date());
	}

	public void onOK() {

		if (checkFieldConstraints()) {
		
		piecejointe.setLibelle(txtLibelle.getValue());
		piecejointe.setFichier(txtFE.getValue());
		piecejointe.setNomfichier(txtNomFichier.getValue());
		piecejointe.setFormat(txtFormat.getValue());
		piecejointe.setTaille(txttaille.getValue());
		piecejointe.setDatemiseenligne(new Date());
		piecejointe.setFormation(formation);
		if (chkRapport.isChecked()) {
			piecejointe.setRapport(true);
		} else {
			piecejointe.setRapport(false);
		}

		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			BeanLocator.defaultLookup(PiecesJointesSession.class).save(piecejointe);

		} else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			BeanLocator.defaultLookup(PiecesJointesSession.class).update(piecejointe);
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();
		} else {
			throw new WrongValueException(errorComponent, errorMsg);
		}
	}
	private boolean checkFieldConstraints() {
		try {

			if (txtLibelle.getValue() == null
					|| txtLibelle.getValue().trim().equalsIgnoreCase("")) {
				errorComponent = txtLibelle;
				errorMsg = "Veuillez renseigner le Libelle";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}

			if (txtFE.getValue() == null
					|| txtFE.getValue().trim().equalsIgnoreCase("")) {
				errorComponent = txtFE;
				errorMsg = "Veuillez renseigner le Fichier";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}
		

			if (txtDatemisenligne.getValue() == null) {
				errorComponent = txtDatemisenligne;
				errorMsg = "Veuillez renseigner la Date";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}

					
			return true;
		} catch (Exception e) {

			lbStatusBar.setValue(errorMsg);
			// lbStatusBar.setStyle(UIConstants.STYLE_STATUSBAR_ERROR);
			e.printStackTrace();
			throw new WrongValueException(errorComponent, errorMsg);
		}

	}
}
