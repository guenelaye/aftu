package sn.ssi.kermel.web.referentiel.suivieformation;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.EaCourriers;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.be.session.CourriersSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class ListCourriersController extends AbstractWindow implements
		AfterCompose, EventListener, ListitemRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Listheader lhlobjet, lhlds, lhldr, lhldc, lhlorigine;
	private Listbox list_courrier;
	private Paging pg;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private String objet, rc;
	private Textbox txtObjet, txtRC;
	private SygProformation formation;
	private Long idformation;
	public static final String CURRENT_MODULE = "CURRENT_MODULE";

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		idformation = (Long) getHttpSession().getAttribute("idformation");

		formation = BeanLocator.defaultLookup(ProformationSession.class).findById(idformation);

		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lhlds.setSortAscending(new FieldComparator("datesaisie", false));
		lhlds.setSortDescending(new FieldComparator("datesaisie", true));

		lhldr.setSortAscending(new FieldComparator("datereception", false));
		lhldr.setSortDescending(new FieldComparator("datereception", true));

		lhldc.setSortAscending(new FieldComparator("datecourrier", false));
		lhldc.setSortDescending(new FieldComparator("datecourrier", true));

		lhlorigine.setSortAscending(new FieldComparator("origine", false));
		lhlorigine.setSortDescending(new FieldComparator("origine", true));

		lhlobjet.setSortAscending(new FieldComparator("objet", false));
		lhlobjet.setSortDescending(new FieldComparator("objet", true));

		list_courrier.setItemRenderer(this);

		pg.setPageSize(byPage);

		pg.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
		Events.postEvent(ApplicationEvents.ON_CLICK, this, null);
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			List<EaCourriers> listcourrier = BeanLocator.defaultLookup(
					CourriersSession.class).find(pg.getActivePage() * byPage,
					byPage, objet, rc, formation);
			SimpleListModel listModel = new SimpleListModel(listcourrier);
			list_courrier.setModel(listModel);
			pg.setTotalSize(BeanLocator.defaultLookup(CourriersSession.class)
					.count(objet, rc, formation));
		}

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String url = "/referentiel/proformation/suivie/formcourriers.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display
					.put(DSP_TITLE, Labels
							.getLabel("kermel.nouveau.courrier"));
			display.put(DSP_HEIGHT, "500px");
			display.put(DSP_WIDTH, "60%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormCourriersController.WINDOW_PARAM_MODE,
					UIConstants.MODE_NEW);

			showPopupWindow(url, data, display);
		}

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list_courrier.getSelectedItem() == null)
				throw new WrongValueException(list_courrier, Labels
						.getLabel("kermel.formation.message.selection"));

			final String url = "/referentiel/proformation/suivie/formcourriers.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT, "500px");
			display.put(DSP_WIDTH, "60%");
			display.put(DSP_TITLE, Labels
					.getLabel("kermel.formation.courrier.modif.tire"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormCourriersController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(FormCourriersController.PARAM_WIDOW_CODE, list_courrier
					.getSelectedItem().getValue());

			showPopupWindow(url, data, display);

		}

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)) {
			if (list_courrier.getSelectedItem() == null)
				throw new WrongValueException(list_courrier, Labels
						.getLabel("kermel.formation.message.selection"));

			HashMap<String, String> display = new HashMap<String, String>();

			display.put(MessageBoxController.DSP_MESSAGE, Labels
					.getLabel("kermel.formation.message.suppression"));
			display.put(MessageBoxController.DSP_TITLE, Labels
					.getLabel("kermel.formation.titre.suppression"));
			display.put(MessageBoxController.DSP_HEIGHT, "250px");
			display.put(MessageBoxController.DSP_WIDTH, "47%");

			HashMap<String, Object> map = new HashMap<String, Object>();

			map.put(CURRENT_MODULE, list_courrier.getSelectedItem().getValue());
			showMessageBox(display, map);

		}

		else if (event.getName().equalsIgnoreCase(
				ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {
			for (int i = 0; i < list_courrier.getSelectedCount(); i++) {
				Integer codes = (Integer) ((Listitem) list_courrier
						.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(CourriersSession.class).delete(codes);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) {

			String uri = null;

			HashMap<String, String> display = null;
			HashMap<String, Object> data = null;

			uri = "/referentiel/proformation/suivie/doc.zul";

			data = new HashMap<String, Object>();
			display = new HashMap<String, String>();

			String nomFichier = (String) list_courrier.getSelectedItem()
					.getAttribute("fichier");
 
			data.put(DownloaDocsController.NOM_FICHIER, nomFichier);

			display.put(DSP_TITLE, Labels
					.getLabel("ecoagris.titre.for.fichier"));
			display.put(DSP_WIDTH, "90%");
			display.put(DSP_HEIGHT, "600px");

			showPopupWindow(uri, data, display);

		}
	}

	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		EaCourriers courrier = (EaCourriers) data;
		item.setValue(courrier.getId());
		item.setAttribute("fichier", courrier.getVelectroniq());
		
		if (courrier.getDatesaisie() != null) {
			Listcell cellDS = new Listcell(UtilVue.getInstance().formateLaDate(courrier
					.getDatesaisie()));
			cellDS.setParent(item);
		} else {
			Listcell cellDS = new Listcell("");
			cellDS.setParent(item);
		}

		if (courrier.getDatereception() != null) {
			Listcell cellDR = new Listcell(UtilVue.getInstance().formateLaDate(courrier
					.getDatereception()));
			cellDR.setParent(item);
		} else {
			Listcell cellDR = new Listcell("");
			cellDR.setParent(item);
		}

		if (courrier.getDatecourrier() != null) {
			Listcell cellDC = new Listcell(UtilVue.getInstance().formateLaDate(courrier
					.getDatecourrier()));
			cellDC.setParent(item);
		} else {
			Listcell cellDC = new Listcell("");
			cellDC.setParent(item);
		}

		if (courrier.getReferencecourrier() != null) {
			Listcell cellRefC = new Listcell(courrier.getReferencecourrier());
			cellRefC.setParent(item);
		} else {
			Listcell cellRefC = new Listcell("");
			cellRefC.setParent(item);
		}

		if (courrier.getOrigine() != null) {
			Listcell cellOrigine = new Listcell(courrier.getOrigine());
			cellOrigine.setParent(item);
		} else {
			Listcell cellOrigine = new Listcell("");
			cellOrigine.setParent(item);
		}

		if (courrier.getAutres() != null) {
			Listcell cellAutres = new Listcell(courrier.getAutres());
			cellAutres.setParent(item);
		} else {
			Listcell cellAutres = new Listcell("");
			cellAutres.setParent(item);
		}

		if (courrier.getCourrierenreference() != null) {
			Listcell cellCR = new Listcell(courrier.getCourrierenreference());
			cellCR.setParent(item);
		} else {
			Listcell cellCR = new Listcell("");
			cellCR.setParent(item);
		}

		if (courrier.getObjet() != null) {
			Listcell cellObjet = new Listcell(courrier.getObjet());
			cellObjet.setParent(item);
		} else {
			Listcell cellObjet = new Listcell("");
			cellObjet.setParent(item);
		}

		if (courrier.getVelectroniq() != null) {
			Listcell cellFichier = new Listcell("");

			cellFichier.setImage("/images/PaperClip-16x16.png");
			cellFichier.setAttribute(UIConstants.TODO, "VIEW");
			cellFichier.addEventListener(Events.ON_CLICK, this);
			cellFichier.setAttribute("fichier", courrier.getVelectroniq());
			cellFichier.setParent(item);
		} else {
			Listcell cellFichier = new Listcell("");
			cellFichier.setParent(item);
		}
	}
	
	public void onClick$bchercher() {
		onOK();
	}

	public void onOK() {
		objet = txtObjet.getValue();
		rc = txtRC.getValue();

		if (!objet.equalsIgnoreCase(Labels
				.getLabel("kermel.formation.courrier.objet"))) {
			if (rc.equalsIgnoreCase(Labels
					.getLabel("kermel.formation.courrier.referencecourrier"))) {
				rc = "";
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

		} else if (!rc.equalsIgnoreCase(Labels
				.getLabel("kermel.formation.courrier.referencecourrier"))) {
			if (objet.equalsIgnoreCase(Labels
					.getLabel("kermel.formation.courrier.objet"))) {
				objet = "";
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		} else
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

	}

	public void onFocus$txtObjet() {
		if (txtObjet.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.formation.courrier.objet")))
			txtObjet.setValue("");
		else
			objet = txtObjet.getValue();
	}

	public void onFocus$txtRC() {
		if (txtRC.getValue().equalsIgnoreCase(Labels.getLabel("kermel.formation.courrier.referencecourrier")))
			txtRC.setValue("");
		else
			rc = txtRC.getValue();
	}

	public void onBlur$txtObjet() {
		if (txtObjet.getValue().equals(""))
			txtObjet.setValue(Labels.getLabel("kermel.formation.courrier.objet"));
	}

	public void onBlur$txtRC() {
		if (txtRC.getValue().equals(""))
			txtRC.setValue(Labels.getLabel("kermel.formation.courrier.referencecourrier"));
	}

}
