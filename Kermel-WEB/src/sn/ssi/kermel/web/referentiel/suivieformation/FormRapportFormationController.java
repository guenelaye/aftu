package sn.ssi.kermel.web.referentiel.suivieformation;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.entity.SygRapportFormation;
import sn.ssi.kermel.be.entity.SygTypeRapportFormation;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.be.referentiel.ejb.RapportFormationSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeRapportFormationSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class FormRapportFormationController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle;
	Long code;
	private	SygRapportFormation rapport=new SygRapportFormation();
	private SygTypeRapportFormation type;
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	
	private Textbox txtLibelles, txtVersionElectronique,txtDescription;
	private Datebox dtdate;
	private String nomFichier;
	private org.zkoss.util.media.Media nomPiece;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	private SygProformation formation;
	private Bandbox bType;
	private Paging pgType;
	private Textbox txtRechercherType;
	private Listbox lstType;
	private String libType,page=null;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE, activePage;
	

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
		lstType.setItemRenderer(new TypeFormationRenderer());
		pgType.setPageSize(byPageBandbox);
		pgType.addForward(ApplicationEvents.ON_PAGING, this,
				ApplicationEvents.ON_AUTRE_ACTION);
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);
		

		
		Long idformation = (Long) getHttpSession().getAttribute("idformation");
		formation = BeanLocator.defaultLookup(ProformationSession.class).findById(idformation);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
	
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			rapport = BeanLocator.defaultLookup(RapportFormationSession.class).findById(code);
			dtdate.setValue(rapport.getDate());
			txtLibelles.setValue(rapport.getLibelle());
			txtVersionElectronique.setValue(rapport.getFichier());
			txtDescription.setValue(rapport.getDescription());
			type = rapport.getType();
			if(type!=null)
			bType.setValue(rapport.getType().getLibelle());
			txtVersionElectronique.setValue(rapport.getFichier());
			  
		}
		
		
	}

	@Override
	public void onEvent(Event event) throws Exception {
		// TODO Auto-generated method stub
 if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION))
		if (event.getData() != null) {

			activePage = Integer.parseInt((String) event.getData());
			byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
			pgType.setPageSize(byPageBandbox);
		} else {
			byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
			activePage = pgType.getActivePage() * byPageBandbox;
			pgType.setPageSize(byPageBandbox);

		}
			List<SygTypeRapportFormation> type = BeanLocator
					.defaultLookup(TypeRapportFormationSession.class).find(
							activePage, byPageBandbox, null, libType);
			lstType.setModel(new SimpleListModel(type));
			pgType.setTotalSize(BeanLocator.defaultLookup(
					TypeRapportFormationSession.class).count(null,
							libType));
			
			
		
	}



	
private boolean checkFieldConstraints() {
		
		try {
		
			if(txtLibelles.getValue().equals(""))
		     {
               errorComponent = txtLibelle;
               errorMsg = Labels.getLabel("kermel.referentiel.rapport.Libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(dtdate.getValue()==null)
		     {
              errorComponent = dtdate;
              errorMsg = Labels.getLabel("kermel.referentiel.rapport.Date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtVersionElectronique.getValue().equals(""))
		     {
              errorComponent = txtVersionElectronique;
              errorMsg = Labels.getLabel("kermel.referentiel.rapport.Fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}





public void onOK() {
	if(checkFieldConstraints())
	{
		rapport.setLibelle(txtLibelles.getValue()); 
		rapport.setFichier(txtVersionElectronique.getValue());
		rapport.setDate(dtdate.getValue());
		rapport.setDescription(txtDescription.getValue());
		rapport.setType(type);
		rapport.setFormation(formation);
		rapport.setStatut(Labels.getLabel("kermel.referentiel.rapport.statut.nopublier"));
		
	  	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			BeanLocator.defaultLookup(RapportFormationSession.class).save(rapport);
			
		} 
		else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			BeanLocator.defaultLookup(RapportFormationSession.class).update(rapport);
			
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();
	}
}

//Type rapport
public void onSelect$lstType() {
	type = (SygTypeRapportFormation) lstType.getSelectedItem().getValue();
	bType.setValue(type.getLibelle());

	bType.close();
}

public void onBlur$txtRechercherType() {
	if (txtRechercherType.getValue().equals(null))
		txtRechercherType.setValue(Labels
				.getLabel("kermel.referentiel.nat.form.rechercher"));
}

public void onFocus$txtRechercherType() {
	if (txtRechercherType.getValue().equalsIgnoreCase(
			Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
		txtRechercherType.setValue("");
	}
}

public void onClick$txtRechercherType() {
	if (txtRechercherType.getValue().equalsIgnoreCase(
			Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
		libType = null;
		page = null;
	} else {
		libType = txtRechercherType.getValue();
		page = "0";
	}

	Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
}

public void onClick$btnRechercherType() {
	if (txtRechercherType.getValue().equalsIgnoreCase(
			Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
		libType = null;
		page = null;
	} else {
		libType = txtRechercherType.getValue();
		page = "0";
	}

	Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
}

public class TypeFormationRenderer implements ListitemRenderer {
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygTypeRapportFormation type = (SygTypeRapportFormation) data;
		item.setValue(type);

		Listcell cellType = new Listcell(type.getLibelle());
		cellType.setParent(item);

	}
}

public void onClick$btnChoixFichier() {
	//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
		if (ToolKermel.isWindows())
			nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
		else
			nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

		txtVersionElectronique.setValue(nomFichier);
	}
}
