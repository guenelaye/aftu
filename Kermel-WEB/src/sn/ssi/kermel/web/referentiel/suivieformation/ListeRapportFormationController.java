package sn.ssi.kermel.web.referentiel.suivieformation;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.entity.SygRapportFormation;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.be.referentiel.ejb.RapportFormationSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;



@SuppressWarnings("serial")
public class ListeRapportFormationController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox list;
	private Paging pgrapport;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null;
    private Listheader lshLibelle;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_RAPFORMATION, MOD_RAPFORMATION, SUPP_RAPFORMATION;
    private String login;
    


    private String extension,images;
	private String statut,imagestatut;
	private Long idrapport;
	 private Div step0, step1;
	private final String cheminDossier = UIConstants.PATH_RAPARMP;
	private SygRapportFormation rapport;
	private SygProformation formation;
	private static final String CONFIRMPUBLIER = "CONFIRMPUBLIER";
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_RAPFORMATION);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_RAPFORMATION != null) { ADD_RAPFORMATION.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_RAPFORMATION != null) { MOD_RAPFORMATION.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUPP_RAPFORMATION != null) { SUPP_RAPFORMATION.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		lshLibelle.setSortAscending(new FieldComparator("libelle", false));
		lshLibelle.setSortDescending(new FieldComparator("libelle", true));
		list.setItemRenderer(this);
		pgrapport.setPageSize(byPage);
		pgrapport.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	
    	Long idformation = (Long) getHttpSession().getAttribute("idformation");

		formation = BeanLocator.defaultLookup(ProformationSession.class).findById(idformation);
    
		Events.postEvent(ApplicationEvents.ON_CLICK, this, null);
	}

	
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgrapport.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgrapport.getActivePage() * byPage;
				pgrapport.setPageSize(byPage);
			}
			 List<SygRapportFormation> rapport = BeanLocator.defaultLookup(RapportFormationSession.class).find(activePage,byPage,libelle,null,null,formation,null);
			 SimpleListModel listModel = new SimpleListModel(rapport);
			 list.setModel(listModel);
			 pgrapport.setTotalSize(BeanLocator.defaultLookup( RapportFormationSession.class).count(null,null,formation,null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri =  "/referentiel/proformation/suivie/formRapport.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.formation.rapport.nouv"));
			display.put(DSP_HEIGHT,"80%");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormRapportFormationController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null)
				
				throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				
			final String uri = "/referentiel/proformation/suivie/formRapport.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"80%");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.formation.rapport.mod"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormRapportFormationController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(FormRapportFormationController.PARAM_WINDOW_CODE, list .getSelectedItem().getValue());
			
			showPopupWindow(uri, data, display);
			
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				
				         {
					   HashMap<String, String> display = new HashMap<String, String>(); // permet
						display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
						display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
						display.put(MessageBoxController.DSP_HEIGHT, "250px");
						display.put(MessageBoxController.DSP_WIDTH, "47%");

						HashMap<String, Object> map = new HashMap<String, Object>(); // permet
						map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
						showMessageBox(display, map);
				       
				         }
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
					 
				for (int i = 0; i < list.getSelectedCount(); i++) {
				    Long codes = (Long)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					BeanLocator.defaultLookup(RapportFormationSession.class).delete(codes);
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) {

				String uri = null;

				HashMap<String, String> display = null;
				HashMap<String, Object> data = null;

				uri = "/formateur/doc.zul";

				data = new HashMap<String, Object>();
				display = new HashMap<String, String>();

				String nomFichier = (String) list.getSelectedItem()
						.getAttribute("fichier");

				data.put(DownloaDocsController.NOM_FICHIER, nomFichier);

				display.put(DSP_TITLE, Labels
						.getLabel("ecoagris.titre.for.fichier"));
				display.put(DSP_WIDTH, "90%");
				display.put(DSP_HEIGHT, "600px");

				showPopupWindow(uri, data, display);

			}
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygRapportFormation rapport = (SygRapportFormation) data;
		item.setValue(rapport.getId());
		item.setAttribute("fichier", rapport.getFichier());
			 
		 Listcell cellLibelle = new Listcell(rapport.getLibelle());
		 cellLibelle.setParent(item);
		 
		 Listcell cellDate = new Listcell(UtilVue.getInstance().formateLaDate(rapport.getDate()));
		 cellDate.setParent(item);
		 
		 Listcell cellType = new Listcell(rapport.getType().getLibelle());
		 cellType.setParent(item);

		 if (rapport.getFichier() != null) {
				Listcell cellFichier = new Listcell("");

				cellFichier.setImage("/images/PaperClip-16x16.png");
				cellFichier.setAttribute(UIConstants.TODO, "VIEW");
				cellFichier.addEventListener(Events.ON_CLICK, this);
				cellFichier.setAttribute("fichier", rapport.getFichier());
				cellFichier.setParent(item);
			} else {
				Listcell cellFichier = new Listcell("");
				cellFichier.setParent(item);
			}
	}
	
	public void onClick$bchercher()
	{
		
		if((txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.rapport.Libelle")))||(txtLibelle.getValue().equals("")))
		 {
			libelle=null;
			page=null;
		 }
		else
		{
			libelle=txtLibelle.getValue();
			page="0";
		}
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	
	public void onFocus$txtLibelle()
	{
		if(txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.rapport.Libelle")))
			txtLibelle.setValue("");
		
	}
	
	public void onBlur$txtLibelle()
	{
		if(txtLibelle.getValue().equals(""))
			txtLibelle.setValue(Labels.getLabel("kermel.referentiel.rapport.Libelle"));
	}

	public void onOK$txtLibelle()
	{
		onClick$bchercher();
	}
	

	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
	
}