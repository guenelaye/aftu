package sn.ssi.kermel.web.referentiel.suivieformation;

import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabbox;

public class TabFormation extends Tabbox {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void onCreate() {
	}

	// Pour le chargement la demande
	public void onSelect() {

		Tab item = getSelectedTab();
		if ((item != null) && item.getId().equals("TAB_DONNREEL")) {
			Include inc = (Include) this.getFellowIfAny("incDonneeR");
			inc.setSrc("/referentiel/proformation/suivie/formdetails.zul");
		}
		if ((item != null) && item.getId().equals("TAB_DONNREEL")) {
			Include inc = (Include) this.getFellowIfAny("incDonneeR");
			inc.setSrc("/referentiel/proformation/suivie/formDonneReel.zul");
		}
		if ((item != null) && item.getId().equals("TAB_PARTICIPANTS")) {
			Include inc = (Include) this.getFellowIfAny("incParticipant");
			inc.setSrc("/referentiel/proformation/suivie/listparticipants.zul");
		}
		if ((item != null) && item.getId().equals("TAB_PRESENCE")) {
			Include inc = (Include) this.getFellowIfAny("incPresence");
			inc.setSrc("/referentiel/proformation/suivie/listPresence.zul");
		}
		if ((item != null) && item.getId().equals("TAB_RAPPORT")) {
			Include inc = (Include) this.getFellowIfAny("incRapport");
			inc.setSrc("/referentiel/proformation/suivie/listRapport.zul");
		}
		if ((item != null) && item.getId().equals("TAB_BAILLEUR")) {
			Include inc = (Include) this.getFellowIfAny("incBail");
			inc.setSrc("/referentiel/proformation/suivie/formationbailleur.zul");
		}
		if ((item != null) && item.getId().equals("TAB_COURRIERS")) {
			Include inc = (Include) this.getFellowIfAny("incCourrier");
			inc.setSrc("/referentiel/proformation/suivie/liscourriers.zul");
		}
		if ((item != null) && item.getId().equals("TAB_PIECESJOINTES")) {
			Include inc = (Include) this.getFellowIfAny("incPieceJointe");
			inc.setSrc("/referentiel/proformation/suivie/lispiecesjointes.zul");
		}
	}
}