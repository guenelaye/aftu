package sn.ssi.kermel.web.referentiel.suivieformation;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.EaPiecesJointes;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.be.session.PiecesJointesSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class ListPiecesJointesController extends AbstractWindow implements
		AfterCompose, EventListener, ListitemRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Listheader lhllibelle, lhlnomfichier, lhlformat, lhltaille,
			lhldate;
	private Paging pg;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE = "CURRENT_MODULE";
	private Listbox list_piecesjointes;
	private String libelle, nomfichier;
	private Textbox txtLibelle, txtNF;
	private Long idformation;
	private SygProformation formation;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		idformation = (Long) getHttpSession().getAttribute("idformation");

		formation = BeanLocator.defaultLookup(ProformationSession.class).findById(idformation);

		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lhllibelle.setSortAscending(new FieldComparator("libelle", false));
		lhllibelle.setSortDescending(new FieldComparator("libelle", true));

		lhlnomfichier
				.setSortAscending(new FieldComparator("nomfichier", false));
		lhlnomfichier
				.setSortDescending(new FieldComparator("nomfichier", true));

		lhlformat.setSortAscending(new FieldComparator("format", false));
		lhlformat.setSortDescending(new FieldComparator("format", true));

		lhltaille.setSortAscending(new FieldComparator("taille", false));
		lhltaille.setSortDescending(new FieldComparator("taille", true));

		lhldate.setSortAscending(new FieldComparator("datemiseenligne", false));
		lhldate.setSortDescending(new FieldComparator("datemiseenligne", true));

		list_piecesjointes.setItemRenderer(this);

		pg.setPageSize(byPage);

		pg.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		Events.postEvent(ApplicationEvents.ON_CLICK, this, null);
	}

	@Override
	public void onEvent(final Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			List<EaPiecesJointes> listpj = BeanLocator.defaultLookup(
					PiecesJointesSession.class).find(
					pg.getActivePage() * byPage, byPage, libelle, nomfichier, formation);
			SimpleListModel listModel = new SimpleListModel(listpj);
			list_piecesjointes.setModel(listModel);
			pg.setTotalSize(BeanLocator.defaultLookup(
					PiecesJointesSession.class).count(libelle, nomfichier, formation));
		}

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String url = "/referentiel/proformation/suivie/formpiecesjointes.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.nouvelle.pj"));
			display.put(DSP_HEIGHT, "70%");
			display.put(DSP_WIDTH, "60%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormPiecesJointesController.WINDOW_PARAM_MODE,
					UIConstants.MODE_NEW);

			showPopupWindow(url, data, display);
		}

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list_piecesjointes.getSelectedItem() == null)
				throw new WrongValueException(list_piecesjointes, Labels
						.getLabel("kermel.formation.message.selection"));

			final String url = "/referentiel/proformation/suivie/formpiecesjointes.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT, "70%");
			display.put(DSP_WIDTH, "60%");
			display.put(DSP_TITLE, Labels
					.getLabel("kermel.formation.pj.modif.tire"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormPiecesJointesController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(FormPiecesJointesController.PARAM_WIDOW_CODE,
					list_piecesjointes.getSelectedItem().getValue());

			showPopupWindow(url, data, display);
		}

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)) {
			if (list_piecesjointes.getSelectedItem() == null)
				throw new WrongValueException(list_piecesjointes, Labels
						.getLabel("kermel.formation.message.selection"));

			HashMap<String, String> display = new HashMap<String, String>();

			display.put(MessageBoxController.DSP_MESSAGE, Labels
					.getLabel("kermel.formation.message.suppression"));
			display.put(MessageBoxController.DSP_TITLE, Labels
					.getLabel("kermel.formation.titre.suppression"));
			display.put(MessageBoxController.DSP_HEIGHT, "250px");
			display.put(MessageBoxController.DSP_WIDTH, "47%");

			HashMap<String, Object> map = new HashMap<String, Object>();

			map.put(CURRENT_MODULE, list_piecesjointes.getSelectedItem()
					.getValue());
			showMessageBox(display, map);

		}

		else if (event.getName().equalsIgnoreCase(
				ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {
			for (int i = 0; i < list_piecesjointes.getSelectedCount(); i++) {
				Integer codes = (Integer) ((Listitem) list_piecesjointes
						.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(PiecesJointesSession.class).delete(
						codes);
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}

		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) {

			String uri = null;

			HashMap<String, String> display = null;
			HashMap<String, Object> data = null;

			uri = "/referentiel/proformation/suivie/doc.zul";

			data = new HashMap<String, Object>();
			display = new HashMap<String, String>();

			String nomFichier = (String) list_piecesjointes.getSelectedItem()
					.getAttribute("fichier");

			data.put(DownloaDocsController.NOM_FICHIER, nomFichier);

			display.put(DSP_TITLE, Labels
					.getLabel("ecoagris.titre.for.fichier"));
			display.put(DSP_WIDTH, "90%");
			display.put(DSP_HEIGHT, "600px");

			showPopupWindow(uri, data, display);

		}

	}

	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		EaPiecesJointes piecejointe = (EaPiecesJointes) data;
		item.setValue(piecejointe.getId());

		item.setAttribute("fichier", piecejointe.getFichier());

		if (piecejointe.getLibelle() != null) {
			Listcell cellLibelle = new Listcell(piecejointe.getLibelle());
			cellLibelle.setParent(item);
		} else {
			Listcell cellLibelle = new Listcell("");
			cellLibelle.setParent(item);
		}

		if (piecejointe.getNomfichier() != null) {
			Listcell cellNomfichier = new Listcell(piecejointe.getNomfichier());
			cellNomfichier.setParent(item);
		} else {
			Listcell cellNomfichier = new Listcell("");
			cellNomfichier.setParent(item);
		}
		
		Listcell cellFormat = new Listcell("");
		  if (piecejointe.getFormat() != null) {
              if ((piecejointe.getFormat().equalsIgnoreCase("doc")) || (piecejointe.getFormat().equalsIgnoreCase("docx")))
            	  cellFormat.setImage("/images/word.jpg");
              else if (piecejointe.getFormat().equalsIgnoreCase("pdf"))
            	  cellFormat.setImage("/images/icone_pdf.png");
              else if (piecejointe.getFormat().equalsIgnoreCase("xlsx"))
            	  cellFormat.setImage("/images/excel.png");
		  }
//		if (piecejointe.getFormat() != null) {
//			Listcell cellFormat = new Listcell(piecejointe.getFormat());
//			cellFormat.setParent(item);
//		} else {
//			Listcell cellFormat = new Listcell("");
//			cellFormat.setParent(item);
//		}
		  
		cellFormat.setParent(item);
		
		if (piecejointe.getTaille() != null) {
			Listcell cellTaille = new Listcell(piecejointe.getTaille());
			cellTaille.setParent(item);
		} else {
			Listcell cellTaille = new Listcell("");
			cellTaille.setParent(item);
		}

		if (piecejointe.getDatemiseenligne() != null) {
			Listcell cellDatemiseenligne = new Listcell(UtilVue.getInstance()
					.formateLaDate(piecejointe.getDatemiseenligne()));
			cellDatemiseenligne.setParent(item);
		} else {
			Listcell cellDatemiseenligne = new Listcell("");
			cellDatemiseenligne.setParent(item);
		}

		if (piecejointe.getFichier() != null) {
			Listcell cellFichier = new Listcell("");

			cellFichier.setImage("/images/PaperClip-16x16.png");
			cellFichier.setAttribute(UIConstants.TODO, "VIEW");
			cellFichier.addEventListener(Events.ON_CLICK, this);
			cellFichier.setAttribute("fichier", piecejointe.getFichier());
			cellFichier.setParent(item);
		} else {
			Listcell cellFichier = new Listcell("");
			cellFichier.setParent(item);
		}

	}

	public void onClick$bchercher() {
		onOK();
	}

	public void onOK() {
		libelle = txtLibelle.getValue();
		nomfichier = txtNF.getValue();

		if (!libelle.equalsIgnoreCase(Labels
				.getLabel("kermel.formation.pj.libelle"))) {
			if (nomfichier.equalsIgnoreCase(Labels
					.getLabel("kermel.formation.pj.nomfichier"))) {
				nomfichier = "";
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

		} else if (!nomfichier.equalsIgnoreCase(Labels
				.getLabel("kermel.formation.pj.nomfichier"))) {
			if (libelle.equalsIgnoreCase(Labels
					.getLabel("kermel.formation.pj.libelle"))) {
				libelle = "";
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		} else
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

	}

	public void onFocus$txtLibelle() {
		if (txtLibelle.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.formation.pj.libelle")))
			txtLibelle.setValue("");
		else
			libelle = txtLibelle.getValue();
	}

	public void onFocus$txtNF() {
		if (txtNF.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.formation.pj.nomfichier")))
			txtNF.setValue("");
		else
			nomfichier = txtNF.getValue();
	}

	public void onBlur$txtLibelle() {
		if (txtLibelle.getValue().equals(""))
			txtLibelle.setValue(Labels
					.getLabel("kermel.formation.pj.libelle"));
	}

	public void onBlur$txtNF() {
		if (txtNF.getValue().equals(""))
			txtNF.setValue(Labels.getLabel("kermel.formation.pj.nomfichier"));
	}

}
