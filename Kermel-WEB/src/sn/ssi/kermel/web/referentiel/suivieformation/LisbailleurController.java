package sn.ssi.kermel.web.referentiel.suivieformation;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.referentiel.ejb.BailleursSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class LisbailleurController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox list;
	private Paging pg;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    private Textbox txtNombail;
    private Listheader lshNom;
    private String nomBail;
    private SygBailleurs bailleur=new SygBailleurs();
    private KermelSousMenu monSousMenu;
	private Menuitem ADD_BAILLEUR, MOD_BAILLEUR, SUPP_BAILLEUR;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_BAILLEUR);
		monSousMenu.afterCompose();
	
		Components.wireFellows(this, this);
		if (ADD_BAILLEUR != null) {
			ADD_BAILLEUR.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
		}
		if (MOD_BAILLEUR != null) {
			MOD_BAILLEUR.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
		}
		if (SUPP_BAILLEUR != null) {
			SUPP_BAILLEUR.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
		}
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		lshNom.setSortAscending(new FieldComparator("nom", false));
		lshNom.setSortDescending(new FieldComparator("nom", true));
		list.setItemRenderer(this);

		pg.setPageSize(byPage);
		
		pg.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 List<SygBailleurs> bailleur = BeanLocator.defaultLookup(BailleursSession.class).find(pg.getActivePage()*byPage,byPage,nomBail,null);
			 SimpleListModel listModel = new SimpleListModel(bailleur);
			list.setModel(listModel);
			pg.setTotalSize(BeanLocator.defaultLookup(BailleursSession.class).count(nomBail,null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/proformation/suivie/formBail.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE,Labels.getLabel("kermel.formation.bail.nou"));
			display.put(DSP_HEIGHT,"50%");
			display.put(DSP_WIDTH, "50%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormbailleurController.WINDOW_PARAM_MODE,
					UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) 
				throw new WrongValueException(list, Labels.getLabel("kermel.formation.message.selection"));
				
			final String uri = "/proformation/suivie/formBail.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"50%");
			display.put(DSP_WIDTH, "50%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.formation.bail.mod"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormbailleurController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(FormbailleurController.PARAM_WIDOW_CODE, list
					.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.formation.message.selection"));
				

				HashMap<String, String> display = new HashMap<String, String>();
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.formation.message.suppression"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.formation.titre.suppression"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < list.getSelectedCount(); i++) {
					Long codes = (Long)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					System.out.println(codes);
				BeanLocator.defaultLookup(BailleursSession.class).delete(codes);
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	

	}
	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygBailleurs bailleur = (SygBailleurs) data;
		item.setValue(bailleur.getId());
		
		Listcell cellNom= new Listcell(bailleur.getLibelle());
		cellNom.setParent(item);
		
		Listcell cellAuto= new Listcell(bailleur.getAutorite().getDenomination());
		cellAuto.setParent(item);
		
	}
	
	//Recherche
	public void onClick$bchercher()
	{
		onOK();
	}
	
	public void onOK()
	{
		nomBail = txtNombail.getValue();
		
		if((nomBail.equalsIgnoreCase(Labels.getLabel("kermel.formation.bail.nom"))) || "".equals(nomBail))
			nomBail="";
			
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	
	public void onFocus$txtNombail()
	{
		if(txtNombail.getValue().equalsIgnoreCase(Labels.getLabel("kermel.formation.bail.nom")))
			txtNombail.setValue("");
		else
			 nomBail=txtNombail.getValue();
	}
	
	public void onBlur$txtNombail()
	{
		if(txtNombail.getValue().equals(""))
			txtNombail.setValue(Labels.getLabel("kermel.formation.bail.nom"));
	}
}