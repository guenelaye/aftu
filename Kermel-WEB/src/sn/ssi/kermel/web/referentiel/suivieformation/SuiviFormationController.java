package sn.ssi.kermel.web.referentiel.suivieformation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.EaPiecesJointes;
import sn.ssi.kermel.be.entity.SygMois;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.be.session.MoisSession;
import sn.ssi.kermel.be.session.PiecesJointesSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;
import sn.ssi.kermel.web.referentiel.controllers.FormationFormController;


@SuppressWarnings("serial")
public class SuiviFormationController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	//step1
	private Listbox list;
	private Paging pgProformation;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private String sygmlibelle,sygmreference;
	private Listheader lshreferentiel,lshlibelle,lshdatedebut,lshdatefin;
	private Textbox txtlibelle,txtreference;
	Long code;
	private String anne;
	private Listbox listAnne;
	private Bandbox cbannee;
	private ArrayList<String> listValeursAnnees;
	UtilVue utilVue = UtilVue.getInstance();
	private String selAnne;
	 private KermelSousMenu monSousMenu;
	 private Menuitem SUIVI_FORM;

	 private SygMois moislist=null;
	 private Integer idmois;
	 Session session = getHttpSession(); 
	private Div step1,step2;

	//step2
	private Label lblRef,lblLib,lblGroup,lblLieu,lblCible,lblDateDebu,lblDateFin,lblmodul,lblNbp,lblBud,
	               lblform,lblLogis,lblCoor;

	private Tabpanel tabDonnee,tabParticipants,tabPresence,tabCourriers, tabPiecesJointes;

	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String WINDOW_PARAM_MODE = "MODE";
	private SygProformation formation;
	private Long idformation;
	private EaPiecesJointes pj;

	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_FORMATIONSUIVI);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this);
	
		if (SUIVI_FORM != null) {
			SUIVI_FORM.addForward(ApplicationEvents.ON_CLICK, this,
					ApplicationEvents.ON_AUTRE_ACTION);
		}
	
		
		/*
		 * On indique qula fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
		lshreferentiel.setSortAscending(new FieldComparator("forRef", false));
		lshreferentiel.setSortDescending(new FieldComparator("forRef", true));
	
		lshlibelle.setSortAscending(new FieldComparator("forLibelle", false));
		lshlibelle.setSortDescending(new FieldComparator("forLibelle", true));
        
        lshdatedebut.setSortAscending(new FieldComparator("forDateDebut", false));
        lshdatedebut.setSortDescending(new FieldComparator("forDateDebut", true));
	
        lshdatefin.setSortAscending(new FieldComparator("forDateFin", false));
        lshdatefin.setSortDescending(new FieldComparator("forDateFin", true));
 
		list.setItemRenderer(this);

		pgProformation.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pgProformation.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
		  step1.setVisible(true);
	      step2.setVisible(false);
	  
	      
		idmois = (Integer) getHttpSession().getAttribute("mois");
		if(idmois!=null)
		moislist =BeanLocator.defaultLookup(MoisSession.class) .findByCode(idmois);
		
		//suivi
		tabDonnee.addEventListener(Events.ON_CLICK, this);
        tabPresence.addEventListener(Events.ON_CLICK, this);
		tabParticipants.addEventListener(Events.ON_CLICK, this);
		tabCourriers.addEventListener(Events.ON_CLICK, this);
		tabPiecesJointes.addEventListener(Events.ON_CLICK, this);

	} 

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		
		anne = UtilVue.getInstance().anneecourant(new Date());
		listValeursAnnees = new ArrayList<String>();
		for (int i = 2007; i <= Integer.parseInt(anne) ; i++) {
			listValeursAnnees.add(i+"");
		}
		listAnne.setModel(new SimpleListModel(listValeursAnnees));
		Map<String, Object> map = (Map<String, Object>) event.getArg();


	}
	


	public void onClick$menufermer() {
       step1.setVisible(true);
       step2.setVisible(false);
	}
			
	public void onClick$rapport() {
		String uri = null;

		HashMap<String, String> display = null;
		HashMap<String, Object> data = null;
		
		idformation = (Long)list.getSelectedItem().getValue();
		formation = BeanLocator.defaultLookup(ProformationSession.class).findById(idformation);
		
		uri = "/referentiel/proformation/suivie/doc.zul";

		data = new HashMap<String, Object>();
		display = new HashMap<String, String>();

		pj = BeanLocator.defaultLookup(PiecesJointesSession.class).find(formation);
		String nomFichier = ((EaPiecesJointes) pj).getFichier();

		data.put(DownloaDocsController.NOM_FICHIER, nomFichier);

		display.put(DSP_TITLE, Labels.getLabel("kermel.titre.for.fichier"));
		display.put(DSP_WIDTH, "90%");
		display.put(DSP_HEIGHT, "600px");

		showPopupWindow(uri, data, display);

	}
	public void onSelect$listAnne(){
   		cbannee.setValue(listAnne.getSelectedItem().getValue().toString());
   		selAnne=listAnne.getSelectedItem().getValue().toString();
   		cbannee.close();
   	}
	

	public Date stringToDate (String sVal) {

	        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
	        
	        try {
	            return df.parse(sVal);
	        } catch (ParseException e) {
	            e.printStackTrace();
	            return null;
	        }
	    }

	
	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			
			 List<SygProformation> formation = BeanLocator.defaultLookup(ProformationSession.class).find(pgProformation.getActivePage()*byPage,byPage,null,null,null,sygmlibelle,sygmreference,anne,null,moislist,-1);
			
			SimpleListModel listModel = new SimpleListModel(formation);
			list.setModel(listModel);
			pgProformation.setTotalSize(BeanLocator.defaultLookup(
					ProformationSession.class).count(null,null,null,sygmlibelle,sygmreference,anne,null,moislist,1));
			
			
		} 
		
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)) {
				 if (list.getSelectedItem() == null) {
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
					
				 }
				 step1.setVisible(false);
				 step2.setVisible(true);
				 
				idformation = (Long)list.getSelectedItem().getValue();
				session.setAttribute("idformation", idformation);
				formation = BeanLocator.defaultLookup(ProformationSession.class).findById(idformation);
				//recupation
				if( formation.getForDateDebut()!=null)
				 lblDateDebu.setValue(UtilVue.getInstance().formateLaDate(formation.getForDateDebut()));
				if( formation.getForDateFin()!=null)
				 lblDateFin.setValue(UtilVue.getInstance().formateLaDate(formation.getForDateFin()));
				if(formation.getModule()!=null)
					 lblmodul.setValue(formation.getModule().getLibelle());
				
			
                
				Include inc = (Include) this.getFellowIfAny("incDETAILS");
				inc.setSrc("/referentiel/proformation/suivie/formdetails.zul");
				
                 
			} 
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) 
			{
			
			}

	}

	public class PieceRenderer implements ListitemRenderer {
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			EaPiecesJointes piecejointe = (EaPiecesJointes) data;
			item.setValue(piecejointe.getId());

			item.setAttribute("fichier", piecejointe.getFichier());

			if (piecejointe.getFichier() != null) {
				Listcell cellFichier = new Listcell("");

				cellFichier.setImage("/images/PaperClip-16x16.png");
				cellFichier.setAttribute(UIConstants.TODO, "VIEW");
				cellFichier.addEventListener(Events.ON_CLICK, (EventListener) new PieceRenderer());
				cellFichier.setAttribute("fichier", piecejointe.getFichier());
				cellFichier.setParent(item);
			} else {
				Listcell cellFichier = new Listcell("");
				cellFichier.setParent(item);
			}
		}
	}
	
	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygProformation formation = (SygProformation) data;
		item.setValue(formation.getId());

		Listcell cellreference = new Listcell(formation.getForRef());
		cellreference.setParent(item);
		Listcell celllibelle = new Listcell(formation.getForLibelle());
		celllibelle.setParent(item);
		Listcell celleDatedebut = new Listcell(UtilVue.getInstance().formateLaDate(formation.getForDateDebut()));
		celleDatedebut.setParent(item);
		Listcell cellDatefin = new Listcell(UtilVue.getInstance().formateLaDate(formation.getForDateFin()));
		cellDatefin.setParent(item);	 
	}
	
	public void onClick$iddetails()
	{
		if(list.getSelectedItem()==null)
		{
			throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
		}
		else
		{
			final String uri = "/referentiel/proformation/formdetails.zul";
			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouvelleformation"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormationFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(FormationFormController.PARAM_WIDOW_CODE, list
					.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		}
	}
	 
	  public void onOK$cbannee() {
		onClick$btnRechercher();
	     }
	  public void onOK$txtreference() {
		onClick$btnRechercher();
	     }
	  public void onOK$txtlibelle() {
		onClick$btnRechercher();
	      }
	
	public void onClick$btnRechercher() {
		
		anne = cbannee.getValue();

		if (txtreference.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.courrier.reference")) || txtreference.getValue().equals("")) {
			sygmreference = null;
		} else {
			sygmreference  = txtreference.getValue();
		}	
		
		if (txtlibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.devises.libelle")) || txtlibelle.getValue().equals("")) {
			sygmlibelle = null;
		} else {
			sygmlibelle  = txtlibelle.getValue();
		}	

 
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	

		public void onFocus$txtreference() {
				if (txtreference.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.courrier.reference"))) {
					txtreference.setValue("");

				}
		}
			
			public void onFocus$txtlibelle() {
				if (txtlibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.devises.libelle"))) {
					txtlibelle.setValue("");

				}
		}
				
				public void onBlur$txtreference() {
					if (txtreference.getValue().equalsIgnoreCase("")) {
						txtreference.setValue(Labels.getLabel("kermel.referentiel.courrier.reference"));
					}	
			}
				public void onBlur$txtlibelle() {
					if (txtlibelle.getValue().equalsIgnoreCase("")) {
						txtlibelle.setValue(Labels.getLabel("kermel.referentiel.devises.libelle"));
					}	
			}
				

}
