package sn.ssi.kermel.web.referentiel.suivieformation;

import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygFormationAutorite;
import sn.ssi.kermel.be.entity.SygGroupeFormation;
import sn.ssi.kermel.be.entity.SygParticipantsFormation;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteFormationSession;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.be.session.ParticipantsSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
public class FormParticipantsController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * akndiaye
	 */
	private static final long serialVersionUID = 1L;
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode;
	public static final String WINDOW_PARAM_MODE = "MODE";
	Integer code;
	private SygParticipantsFormation participant = new SygParticipantsFormation();
	private Textbox txtPrenom, txtNom, txtTelephone, txtMobile, txtMail, txtAdresse;
	private Long idformation;
	private SygProformation formation;
	private SygFormationAutorite fauto;
	private Label lbStatusBar;
	private SygGroupeFormation groupe;
    private String page=null;
	private String libelleautorite = null;

//	private SygTypeAutoriteContractante typeautorite;
//	private SygAutoriteContractante autorite;
    private SygFormationAutorite autorite;
	private Div step0,step1,step2;
	private Textbox txtRechercherAutorite;
	private Listbox lstAutorite,list;
	private Paging pgAutorite,pg;
	private Label entete1, entete2, entete3;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE, activePage;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
//		addEventListener(ApplicationEvents.ON_TYPE_AUTORITES, this);
//		lstAutorite.setItemRenderer(new TypeAutoritesRenderer());
//		pgAutorite.setPageSize(byPage);
//		pgAutorite.addForward(ApplicationEvents.ON_PAGING, this,
//				ApplicationEvents.ON_TYPE_AUTORITES);
		
		addEventListener(ApplicationEvents.ON_AUTORITES, this);
		list.setItemRenderer(new AutoritesRenderer());
		pg.setPageSize(byPage);
		pg.addForward(ApplicationEvents.ON_PAGING, this,
				ApplicationEvents.ON_AUTORITES);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		Events.postEvent(ApplicationEvents.ON_TYPE_AUTORITES, this, null);
		
		idformation = (Long) getHttpSession().getAttribute("idformation");
		formation = BeanLocator.defaultLookup(ProformationSession.class).findById(idformation);
		
		//	groupe= formation.getGroupe();
			Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);
	}

//	public void onClick$menuNext() {
//		if (lstAutorite.getSelectedItem() == null)
//			throw new WrongValueException(lstAutorite, Labels
//					.getLabel("kermel.error.select.item"));
//
//		typeautorite = (SygTypeAutoriteContractante) lstAutorite
//				.getSelectedItem().getValue();
//		entete1.setValue(typeautorite.getLibelle());
//		step0.setVisible(false);
//		step1.setVisible(true);
//		Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);
//
//	}

	public void onClick$menuNext1() {
		if (list.getSelectedItem() == null)
			throw new WrongValueException(list, Labels
					.getLabel("kermel.error.select.item"));

		autorite = (SygFormationAutorite) list.getSelectedItem().getValue();
		entete2.setValue(autorite.getType().getLibelle());
      	entete3.setValue(autorite.getAutorite().getDenomination());
		step1.setVisible(false);
		step2.setVisible(true);
//		Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);

	}

	public void onClick$menuPrevious() {
		if (list.getSelectedItem() == null)
			throw new WrongValueException(list, Labels
					.getLabel("kermel.error.select.item"));

		autorite = (SygFormationAutorite) list
				.getSelectedItem().getValue();
		step1.setVisible(true);
		step2.setVisible(false);
//		Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);

	}

	
//	public void onClick$btnRechercherAutorite()
//	{
//		if (txtRechercherAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.typeAutoritecontractante")) || txtRechercherAutorite.getValue().equals("")) {
//			libelleautorite = null;
//		} else
//		{
//			
//		libelleautorite = txtRechercherAutorite.getValue();
//		
//		}
//		Events.postEvent(ApplicationEvents.ON_TYPE_AUTORITES, this, null);
//		
//		}
//	
//	public void onFocus$txtRechercherAutorite()
//	{
//		if(txtRechercherAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.autoritecontractante.libelle")))
//			txtRechercherAutorite.setValue("");
//		else
//			libelleautorite=txtRechercherAutorite.getValue();
//	}
//	
//	
//	public void onBlur$txtRechercherAutorite()
//	{
//		if(txtRechercherAutorite.getValue().equals(""))
//			txtRechercherAutorite.setValue(Labels.getLabel("kermel.autoritecontractante.libelle"));
//	}
	
	public void onClick$menuPrevious1() {
		step1.setVisible(true);
		step2.setVisible(false);
		

	}

	public void onClick$menuFermer() {
		detach();
	}

	public void onClick$menuFermer1() {
		detach();
	}

//	public class TypeAutoritesRenderer implements ListitemRenderer {
//
//		@Override
//		public void render(Listitem item, Object data, int index)  throws Exception {
//			SygTypeAutoriteContractante autorites = (SygTypeAutoriteContractante) data;
//			item.setValue(autorites);
//
//			Listcell cellLibelle = new Listcell(autorites.getLibelle());
//			cellLibelle.setParent(item);
//			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
//				if (autorites.getId().equals(
//						fauto.getAutorite().getType().getId()))
//					item.setSelected(true);
//
//			}
//
//		}
//	}

	public class AutoritesRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygFormationAutorite autorites = (SygFormationAutorite) data;
			item.setValue(autorites);

			Listcell cellCode = new Listcell(autorites.getAutorite().getSigle());
			cellCode.setParent(item);

			Listcell cellLibelle = new Listcell(autorites.getAutorite().getDenomination());
			cellLibelle.setParent(item);

			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				if (autorites.getAutorite().getId().equals(participant.getAutorite().getId()))
					item.setSelected(true);

			}
		}
	}
	@Override
	public void onEvent(Event event) throws Exception {
		


		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTORITES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pg.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pg.getActivePage() * byPage;
				pg.setPageSize(byPage);
			}
			List<SygFormationAutorite> autorites = BeanLocator .defaultLookup(AutoriteFormationSession.class).find(activePage, byPage, formation, null);
			list.setModel(new SimpleListModel(autorites));
			pg.setTotalSize(BeanLocator.defaultLookup( AutoriteFormationSession.class).count( formation, null));
		
	}
}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {

		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(PARAM_WINDOW_MODE);

		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Integer) map.get(PARAM_WIDOW_CODE);
			participant = BeanLocator.defaultLookup(ParticipantsSession.class)
					.findByCode(code);
			entete2.setValue(participant.getTypeautorite().getLibelle());
			entete3.setValue(participant.getAutorite().getDenomination());
			txtPrenom.setValue(participant.getPrenom());
			txtNom.setValue(participant.getNom());
			txtTelephone.setValue(participant.getTelephone());
			txtMobile.setValue(participant.getMobile());
			txtMail.setValue(participant.getEmail());
			txtAdresse.setValue(participant.getAdresse());
			Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION1, this, null);
			
		} 

	}

	 public static boolean isValid(String email) {
			if (email != null && email.trim().length() > 0)
				return email
						.matches("^[a-zA-Z0-9\\.\\-\\_]+@([a-zA-Z0-9\\-\\_\\.]+\\.)+([a-zA-Z]{2,4})$");
			return false;
		}
	 
	public void onClick$menuValider() {
		
		if(txtPrenom.getValue()==null || "".equals(txtPrenom.getValue())){
			lbStatusBar.setValue(Labels.getLabel("kermel.rensigner")+" "+Labels.getLabel("kermel.formation.participant.prenom"));
			throw new WrongValueException(txtPrenom, Labels.getLabel("kermel.ChamNull"));
			
		}
		if(txtNom.getValue()==null || "".equals(txtNom.getValue())){
			lbStatusBar.setValue(Labels.getLabel("kermel.rensigner")+" "+Labels.getLabel("kermel.formation.participant.nom"));
			throw new WrongValueException(txtNom, Labels.getLabel("kermel.ChamNull"));
		}
		if(txtTelephone.getValue()==null || "".equals(txtTelephone.getValue())){
			lbStatusBar.setValue(Labels.getLabel("kermel.rensigner")+" "+Labels.getLabel("kermel.formation.participant.telephone"));
			throw new WrongValueException(txtTelephone, Labels.getLabel("kermel.ChamNull"));
		}
		if(txtMobile.getValue()==null || "".equals(txtMobile.getValue())){
			lbStatusBar.setValue(Labels.getLabel("kermel.rensigner")+" "+Labels.getLabel("kermel.formation.participant.mobile"));
			throw new WrongValueException(txtMobile, Labels.getLabel("kermel.ChamNull"));
		}
		if(txtAdresse.getValue()==null || "".equals(txtAdresse.getValue())){
			lbStatusBar.setValue(Labels.getLabel("kermel.rensigner")+" "+Labels.getLabel("kermel.formation.participant.adresse"));
			throw new WrongValueException(txtAdresse, Labels.getLabel("kermel.ChamNull"));
		}
		if(txtMail.getValue()==null || "".equals(txtMail.getValue())){
			lbStatusBar.setValue(Labels.getLabel("kermel.rensigner")+" "+Labels.getLabel("kermel.formation.participant.mail"));
			throw new WrongValueException(txtMail, Labels.getLabel("kermel.ChamNull"));
		}
		participant.setPrenom(txtPrenom.getValue());
		participant.setNom(txtNom.getValue());
		participant.setTelephone(txtTelephone.getValue());
		participant.setMobile(txtMobile.getValue());
		if (isValid(txtMail.getValue())) {
		participant.setEmail(txtMail.getValue());
		}
		else{
			throw new WrongValueException(txtMail, Labels.getLabel("kermel.mailexception"));
		}
		participant.setAdresse(txtAdresse.getValue());
		participant.setGroupe(groupe);
	
		autorite = (SygFormationAutorite) list.getSelectedItem().getValue();
		participant.setTypeautorite(autorite.getAutorite().getType());
		participant.setAutorite(autorite.getAutorite());
		participant.setFormation(formation);

		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			BeanLocator.defaultLookup(ParticipantsSession.class).save(
					participant);

		}
		 else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
		 BeanLocator.defaultLookup(ParticipantsSession.class).update(
		 participant);
		 }
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();
		
	}

	
}
