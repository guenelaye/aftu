package sn.ssi.kermel.web.referentiel.suivieformation;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygFPresenceParcipant;
import sn.ssi.kermel.be.entity.SygFichePresence;
import sn.ssi.kermel.be.entity.SygParticipantsFormation;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.referentiel.ejb.FPresenceParticipantSession;
import sn.ssi.kermel.be.referentiel.ejb.FichePresenceSession;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.be.session.ParticipantsSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class AjouFichePresenceController extends AbstractWindow implements
		AfterCompose, EventListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Listheader lhlprenom;
	private Paging pg,pgPart;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE = "CURRENT_MODULE";
	private Listbox list,list_part;
	private Long idformation;
	private SygProformation formation;
	private SygFichePresence fiche;
	private SygParticipantsFormation participant;
	private SygFPresenceParcipant fpart=new SygFPresenceParcipant();
	//private SygGroupeFormation groupe;
    private Label entete1;
    private Div step1,step2;
	private String mode,nom,prenom;
	private int  activePage;
	private int byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGESS;
    Session session = getHttpSession();
    private Label titreparticipant;
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	public static final String WINDOW_PARAM_MODE = "MODE";
	List<SygFPresenceParcipant>  listpart= new ArrayList<SygFPresenceParcipant>();
	Long code;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		idformation = (Long) getHttpSession().getAttribute("idformation");

		formation = BeanLocator.defaultLookup(ProformationSession.class).findById(idformation);
		//recuperer le groupe
		//groupe= formation.getGroupe();
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);

		lhlprenom.setSortAscending(new FieldComparator("nom", false));
		lhlprenom.setSortDescending(new FieldComparator("nom", true));


		
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
		list_part.setItemRenderer(new ParticipantRenderer());
		pgPart.setPageSize(byPage);
		pgPart.addForward(ApplicationEvents.ON_PAGING, this,
				ApplicationEvents.ON_AUTRE_ACTION);
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {

		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(PARAM_WINDOW_MODE);

			code = (Long) map.get(PARAM_WIDOW_CODE);
			fiche = BeanLocator.defaultLookup(FichePresenceSession.class)
					.findByCode(code);
            entete1.setValue(fiche.getJour());
          //  titreparticipant.setValue(Labels.getLabel("kermel.formation.fichepresence.part")+" "+groupe.getLibelle());
           
            listpart = BeanLocator.defaultLookup(FPresenceParticipantSession.class)
			.findAffListe(0,byPage,null,fiche);
            
            session.setAttribute("fiche",fiche);
	}

	
   public void onClick$menuValider() {

	   if (list_part.getSelectedItem() == null)
			throw new WrongValueException(list_part, Labels
					.getLabel("kermel.error.select.item"));
	   
	     fiche = (SygFichePresence) getHttpSession().getAttribute("fiche");
        listpart= BeanLocator.defaultLookup(FPresenceParticipantSession.class).findAffListe(0, -1, null, fiche);

      
        	//pr supprimer les donne de la liste
        	for (int j = 0; j <listpart.size(); j++) 
			{				
				BeanLocator.defaultLookup(FPresenceParticipantSession.class).delete(listpart.get(j).getId());
			}	
        	
        	//pr inserer les nouvelles donnees
		   for (int j = 0; j <list_part.getSelectedItems().size(); j++)
		      {
			  				   
				   Integer codes = (Integer)((Listitem) list_part.getSelectedItems().toArray()[j]).getValue();
				   participant = BeanLocator.defaultLookup(ParticipantsSession.class).findByCode(codes);
				   SygFPresenceParcipant fpart=new SygFPresenceParcipant();
				   fpart.setPaticipant(participant);
				   fpart.setFiche(fiche);
				   BeanLocator.defaultLookup(FPresenceParticipantSession.class).save(fpart);				
		      } 
		   fiche = (SygFichePresence) getHttpSession().getAttribute("fiche");
		   SygFPresenceParcipant ficheparticipant= BeanLocator.defaultLookup(FPresenceParticipantSession.class).findByFiche(fiche);
		   //stocker le ngre de present et d absen dans la table intermedaire
		   Integer nbreparticipant =  (Integer) getHttpSession().getAttribute("nbre_participants");
           Integer nbreabs = nbreparticipant - list_part.getSelectedItems().size();
           ficheparticipant.setNbrepresent(list_part.getSelectedItems().size());		    
           ficheparticipant.setNbreabsent(nbreabs);
		   BeanLocator.defaultLookup(FPresenceParticipantSession.class).update(ficheparticipant);				
	         //copier les donne dans la table fiche
		   if(ficheparticipant.getNbrepresent()!=null)
		   fiche.setNpresent(ficheparticipant.getNbrepresent()) ;
		   if(ficheparticipant.getNbreabsent()!=null)
	       fiche.setNabs(ficheparticipant.getNbreabsent());
	       BeanLocator.defaultLookup(FichePresenceSession.class).update(fiche);	
//		}else
//		 {
//			
//			 for (int j = 0; j <list_part.getSelectedItems().size(); j++)
//		      {
//			  				   
//				   Integer codes = (Integer)((Listitem) list_part.getSelectedItems().toArray()[j]).getValue();
//				   participant = BeanLocator.defaultLookup(ParticipantsSession.class).findByCode(codes);
//				   SygFPresenceParcipant fpart=new SygFPresenceParcipant();
//				   fpart.setPaticipant(participant);
//				   fpart.setFiche(fiche);
//				   BeanLocator.defaultLookup(FPresenceParticipantSession.class).save(fpart);				
//		      } 
//		   fiche = (SygFichePresence) getHttpSession().getAttribute("fiche");
//		   SygFPresenceParcipant ficheparticipant= BeanLocator.defaultLookup(FPresenceParticipantSession.class).findByFiche(fiche);
//		   //stocker le ngre de present et d absen dans la table intermedaire
//		   Integer nbreparticipant =  (Integer) getHttpSession().getAttribute("nbre_participants");
//          Integer nbreabs = nbreparticipant - list_part.getSelectedItems().size();
//          ficheparticipant.setNbrepresent(list_part.getSelectedItems().size());		    
//          ficheparticipant.setNbreabsent(nbreabs);
//		   BeanLocator.defaultLookup(FPresenceParticipantSession.class).update(ficheparticipant);				
//	         //copier les donne dans la table fiche
//		   if(ficheparticipant.getNbrepresent()!=null)
//		   fiche.setNpresent(ficheparticipant.getNbrepresent()) ;
//		   if(ficheparticipant.getNbreabsent()!=null)
//	       fiche.setNabs(ficheparticipant.getNbreabsent());
//	       BeanLocator.defaultLookup(FichePresenceSession.class).update(fiche);		      
//		   }

    	   Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		   detach();
	  
		
	}
   
	public class ParticipantRenderer implements ListitemRenderer {
		
	
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygParticipantsFormation participant = (SygParticipantsFormation) data;
			item.setValue(participant.getId());

			Listcell cellPrenom = new Listcell(participant.getPrenom());
			cellPrenom.setParent(item);

			Listcell cellNom = new Listcell(participant.getNom());
			cellNom.setParent(item);
			
			for (SygFPresenceParcipant fpart : listpart)			
				if(participant.getId().equals(fpart.getPaticipant().getId())){					
					
						item.setSelected(true);

				}
		}
	}
	@Override
	public void onEvent(final Event event) throws Exception {
	
		
		 if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)) {
				if (event.getData() != null) {
					activePage = Integer.parseInt((String) event.getData());
					byPageBandbox = -1;
					pgPart.setPageSize(1000);
				} else {
					byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGESS;
					activePage = pgPart.getActivePage() * byPageBandbox;
					pgPart.setPageSize(byPageBandbox);
				}
				List<SygParticipantsFormation> listparticipant = BeanLocator.defaultLookup(
						ParticipantsSession.class).find(
						pgPart.getActivePage() * byPageBandbox, byPageBandbox, prenom, nom,formation,null,null);
				SimpleListModel listModel = new SimpleListModel(listparticipant);
				list_part.setModel(listModel);
				pgPart.setTotalSize(BeanLocator.defaultLookup(ParticipantsSession.class).count(prenom, nom, formation,null));
				getHttpSession().setAttribute("nbre_participants", BeanLocator.defaultLookup(ParticipantsSession.class).count(prenom, nom, formation,null));

			}

	}

	
//	@Override
//	public void render(Listitem item, Object data, int index)  throws Exception {
//		SygFichePresence fiche = (SygFichePresence) data;
//		item.setValue(fiche.getId());
//		
//		if (fiche.getJour() != null) {
//			Listcell celljr = new Listcell(fiche.getJour());
//			celljr.setParent(item);
//		} else {
//			Listcell celljr = new Listcell("");
//			celljr.setParent(item);
//		}
//		if (fiche.getDateFiche() != null) {
//			Listcell cellDate = new Listcell(UtilVue.getInstance()
//					.formateLaDate(fiche.getDateFiche()));
//			cellDate.setParent(item);
//		} else {
//			Listcell cellDate = new Listcell("");
//			cellDate.setParent(item);
//		}
//		
//	}

}
