package sn.ssi.kermel.web.referentiel.suivieformation;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Longbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygFormationBail;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.referentiel.ejb.BailleurFormationSession;
import sn.ssi.kermel.be.referentiel.ejb.BailleursSession;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;



@SuppressWarnings("serial")
public class ListFormationBailleurController  extends AbstractWindow  implements EventListener, AfterCompose, ListitemRenderer {

    private Listbox list,lstbail;
	private Paging pgList,pgbail;
	private Textbox txtRechercherbail,txtNum;
	private Datebox dateAccor;
	private Bandbox bdbail;
	public static final String PARAM_WINDOW_CODE = "CODE";
	private SygFormationBail fbailleur;
	private SygProformation formation ;
    private SygBailleurs bail;
    
    private  int byPage = UIConstants.DSP_GRID_ROW_BY_PAGE;
	private int byPageBandBox = UIConstants.DSP_BANDBOXS_BY_PAGES;
	private int activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	public static final String WINDOW_PARAM_MODE = "MODE";
	private String page=null;
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private Long sommeTrouve=0L;
	private Div div1;
	private Long montant;
	private Longbox lgmontant;
	private String bailLib;
	private String num;
	private Date dateac;
    Integer idFbailleur;
    private Integer id=null;
    private Long idformation;
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
	
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
		
		idformation = (Long) getHttpSession().getAttribute("idformation");
		formation = BeanLocator.defaultLookup(ProformationSession.class).findById(idformation);

		
		list.setItemRenderer(this);
		pgList.setPageSize(byPage);
		pgList.addForward("onPaging", this,ApplicationEvents.ON_MODEL_CHANGE);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
		lstbail.setItemRenderer(new BailRenderer());
		pgbail.setPageSize(byPageBandBox);
		pgbail.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTRE_ACTION);
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);

		
	}

	/**
	 * Permet de gerer les evenements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			
		    		List<SygFormationBail> fbailleur = BeanLocator.defaultLookup(BailleurFormationSession.class).find(pgList.getActivePage()*byPage,byPage,formation,null);
					SimpleListModel listModel = new SimpleListModel(fbailleur);
					list.setModel(listModel);
					pgList.setTotalSize(BeanLocator.defaultLookup(BailleurFormationSession.class).count(formation,null));
					  if (fbailleur.size()==0)
					   {
						  sommeTrouve=0L;
					
			           }else 
			        	   sommeTrouve=BeanLocator.defaultLookup(BailleurFormationSession.class).getMontantTotal();					  
		}
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
					throw new WrongValueException(list, Labels.getLabel("kermel.formation.message.selection"));
				HashMap<String, String> display = new HashMap<String, String>(); 
			
				// permet de fixer les dimensions du popup
			
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.formation.message.suppression"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.formation.titre.suppression"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// permet de passer des parametres au popup
				
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
				
		}
			
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){		

				BeanLocator.defaultLookup(BailleurFormationSession.class).delete(id);
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
				initilisation();
			   }
				
 		   else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
			Listcell button = (Listcell) event.getTarget();
			String toDo = (String) button.getAttribute(UIConstants.TODO);
			id = ((SygFormationBail) button.getAttribute("fbailleur")).getId();
			if (toDo.equalsIgnoreCase("delete"))
			 {
				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE, Labels.getLabel("kermel.formation.message.suppression"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.formation.titre.suppression"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");
	         	HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, id);
				showMessageBox(display, map);
			 }
 		}
		
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)){
			if (event.getData() != null)
			   {
				 
				activePage = Integer.parseInt((String) event.getData());
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
				pgbail.setPageSize(byPageBandBox);	
			       } 
			    else {
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgbail.getActivePage() * byPageBandBox;
				pgbail.setPageSize(byPageBandBox);
			       
			      }
			      
				 List<SygBailleurs> baill = BeanLocator.defaultLookup(BailleursSession.class).find(pgbail.getActivePage() * byPageBandBox, byPageBandBox,bailLib,null);
				 lstbail.setModel(new SimpleListModel(baill));
				 pgbail.setTotalSize(BeanLocator.defaultLookup(BailleursSession.class).count(bailLib,null));
				}	  
		
		}
	
		

    public void onClick$ajout() {
		
		div1.setVisible(true);
		
		Long ValMax=formation.getBudg();
	
		if (list.getSelectedItem() != null)
			
		{	 if (sommeTrouve>=ValMax)
						throw new WrongValueException(list, Labels.getLabel("kermel.projet.bail.somm"));	
	    } 
    }
    
   public void onClick$modif()  {
		if (list.getSelectedItem() == null) 
			throw new WrongValueException(list, Labels.getLabel("kermel.formation.message.selection"));
		
			 fbailleur =  BeanLocator.defaultLookup(BailleurFormationSession.class).findByCode((Integer)list.getSelectedItem().getValue());
			 div1.setVisible(true);
		    
		     bail=BeanLocator.defaultLookup(BailleursSession.class).findById(fbailleur.getBailleur().getId());
		    bdbail.setValue(bail.getLibelle());
		    txtNum.setValue(fbailleur.getNum());
		    dateAccor.setValue(fbailleur.getDateAccor());
		    lgmontant.setValue(fbailleur.getMontant());
		
 }
   
   
    public void onClick$menuValider() {
    	
    	
		    	
		    	if(bdbail.getValue()==null || "".equals(bdbail.getValue())){
					throw new WrongValueException(bdbail, Labels.getLabel("kermel.ChamNull"));
				}
				if(lgmontant.getValue()==null || "".equals(lgmontant.getValue())){
					throw new WrongValueException(lgmontant, Labels.getLabel("kermel.ChamNull"));
				}
			
				           if(fbailleur!=null && fbailleur.getId()!=null)
			    			 {
				        	   	
				        	   	
				        	   	Long monm = fbailleur.getMontant();
				        	   	sommeTrouve-= monm;
				        	   	Long montantAjouter=lgmontant.getValue();
				               	montantAjouter += sommeTrouve;
				                Long ValMax = formation.getBudg();
				                
				        		    	if(montantAjouter>ValMax){
				        		    		throw new WrongValueException(list, Labels.getLabel("kermel.projet.bail.somm"));
				        		    	}
				        		    	setFBailleur();
					    		BeanLocator.defaultLookup(BailleurFormationSession.class).update(fbailleur);			    			 
                             }else
			    			  {
                            	 
                            	 	fbailleur = new SygFormationBail();
                            	 	
    				        	   	Long montantAjouter=lgmontant.getValue();
    				               	montantAjouter += sommeTrouve;
    				                Long ValMax = formation.getBudg();
    				                
    				        		    	if(montantAjouter>ValMax){
    				        		    		throw new WrongValueException(list, Labels.getLabel("kermel.projet.bail.somm"));
    				        		    	}

    				        		    	setFBailleur();
					    			BeanLocator.defaultLookup(BailleurFormationSession.class).save(fbailleur);			    			 
                               }
			    		   
			    		    Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			    		    initilisation();
			    	 	    		    
    }

    
    
 public void initilisation(){
	    		   
	  lgmontant.setValue(new Long(0));
	  bdbail.setValue("");
	  txtNum.setValue(null);
	  dateAccor.setValue(null);
	  fbailleur = null;
}
	
 
 
 
//bailleur
	public void onSelect$lstbail(){
		 bail = (SygBailleurs) lstbail.getSelectedItem().getValue();
		 bdbail.setValue(bail.getLibelle());
		
		 bdbail.close();
	}
	
	 public void onBlur$txtRechercherbail()
	 {
	 if(txtRechercherbail.getValue().equals(null))
		 txtRechercherbail.setValue(Labels.getLabel("recherche.fiche.rech"));
	 }
		 
	public void onFocus$txtRechercherbail(){
		 if(txtRechercherbail.getValue().equalsIgnoreCase(Labels.getLabel("recherche.fiche.rech"))){
			 txtRechercherbail.setValue("");
		 }		 
	}
	
	public void  onClick$txtRechercherbail(){
		if(txtRechercherbail.getValue().equalsIgnoreCase(Labels.getLabel("recherche.fiche.rech")))
		{
			bailLib = null;
			page = null;
		}
		else
		{
			bailLib = txtRechercherbail.getValue();
			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}
	
	public void  onClick$btnRechercherbail(){
		if(txtRechercherbail.getValue().equalsIgnoreCase(Labels.getLabel("recherche.fiche.rech")))
		{
			bailLib = null;
			page = null;
		}
		else
		{
			bailLib = txtRechercherbail.getValue();
			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	

	public class BailRenderer implements ListitemRenderer {
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygBailleurs bailleur= (SygBailleurs) data;
			item.setValue(bailleur);
			
			Listcell cellDip = new Listcell(bailleur.getLibelle());
			cellDip.setParent(item);

		}
	}
	

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		 SygFormationBail fbailleur = (SygFormationBail) data;
		 item.setValue(fbailleur.getId());
		 
		Listcell  cellNom,cellMont,cellDat,cellNum,cellSup;
	
		
		if(fbailleur.getNum()!=null) {
			cellNum = new Listcell(fbailleur.getNum());
			cellNum.setParent(item);
		}
		else {
			cellNum = new Listcell("");
			cellNum.setParent(item);
		}
	
		
		if(fbailleur.getBailleur()!=null) {
			cellNom = new Listcell(fbailleur.getBailleur().getLibelle());
			cellNom.setParent(item);
		}
		else {
			cellNom = new Listcell("");
			cellNom.setParent(item);
		}
	
		if(fbailleur.getDateAccor()!=null) {
			cellDat = new Listcell(UtilVue.getInstance().formateLaDate(fbailleur.getDateAccor()));
			cellDat.setParent(item);
		}
		else {
			cellDat = new Listcell("");
			cellDat.setParent(item);
		}
		if(fbailleur.getMontant()!=null) {
			cellMont = new Listcell(fbailleur.getMontant().toString());
			cellMont.setParent(item);
		}
		else {
			cellMont = new Listcell("");
			cellMont.setParent(item);
		}
	 
		
	        cellSup = new Listcell();
			cellSup.setImage("/images/delete.png");
			cellSup.setAttribute(UIConstants.TODO, "delete");
			cellSup.setAttribute("fbailleur",fbailleur);
			cellSup.setTooltiptext("Supprimer Conteneur");
			cellSup.addEventListener(Events.ON_CLICK, ListFormationBailleurController.this);
			cellSup.setParent(item);
		
	}
	
	private void setFBailleur(){
		idformation = (Long) getHttpSession().getAttribute("idformation");
		formation = BeanLocator.defaultLookup(ProformationSession.class).findById(idformation);

		fbailleur.setBailleur(bail);
		fbailleur.setNum(txtNum.getValue());
		fbailleur.setDateAccor(dateAccor.getValue());
		fbailleur.setMontant(lgmontant.getValue());
		fbailleur.setFormation(formation);
	
	}
}
	
