package sn.ssi.kermel.web.referentiel.suivieformation;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class FormDetailsFormationController extends AbstractWindow implements AfterCompose{

	/**
	 * 
	 */
	private Long idformation;
	Session session = getHttpSession(); 
	private SygProformation formation;
	private Label lblRef,lblLib,lblGroup,lblLieu,lblCible,lblNbp,lblBud,
    lblform,lblLogis,lblCoor;
	 
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		

	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {

		idformation=(Long) session.getAttribute("idformation");
		formation = BeanLocator.defaultLookup(ProformationSession.class).findById(idformation);
		//recupation
		if(formation.getForRef()!=null)
		 lblRef.setValue(formation.getForRef());
		if(formation.getForLibelle()!=null)
		 lblLib.setValue(formation.getForLibelle());
		if(formation.getForLieu()!=null)
		 lblLieu.setValue(formation.getForLieu());
		if(formation.getForCible()!=null)
		 lblCible.setValue(formation.getForCible());
		if(formation.getEffectif()!=null)
		 lblNbp.setValue(formation.getEffectif().toString());
		if(formation.getBudg()!=null)
		lblBud.setValue(formation.getBudg().toString());
		
		
		if(formation.getFormateur()!=null)
		 lblform.setValue(formation.getFormateur().getPrenom()+"  "+formation.getFormateur().getNom());
	       if(formation.getCoordonnateur()!=null)
         lblCoor.setValue(formation.getCoordonnateur().getPrenom()+" "+formation.getCoordonnateur().getNom());
        if(formation.getLogisticien()!=null)
         lblLogis.setValue(formation.getLogisticien().getPrenom()+" "+formation.getLogisticien().getNom());

	}
	
	
}