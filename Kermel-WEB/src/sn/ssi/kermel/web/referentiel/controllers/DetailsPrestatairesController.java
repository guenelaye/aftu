package sn.ssi.kermel.web.referentiel.controllers;


import java.util.Map;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygPrestataire;
import sn.ssi.kermel.be.referentiel.ejb.PrestataireSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class DetailsPrestatairesController extends AbstractWindow implements
		 AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String login;
	Long code;
	SygPrestataire prestataire=new SygPrestataire();
	private Long idplan;
    private Label lblidentifiant,lblraisonsociale,lbladresse,lbllibelle,lblpays,lblmail,lblcommentaire,lblnom,lblprenom,lbltelephone,lblmails,lblnatureprestataire;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private String mode;
	public static final String PARAM_WIDOW_CODE = "CODE";
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		

		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
			code = (Long) map.get(PARAM_WIDOW_CODE);
			prestataire = BeanLocator.defaultLookup(PrestataireSession.class).findById(code);
			//lblidentifiant.setValue(prestataire.getIdentifiant());
			lblraisonsociale.setValue(prestataire.getRaisonsociale());
			lbladresse.setValue(prestataire.getAdresse());
			lblmail.setValue(prestataire.getMail());
			lblcommentaire.setValue(prestataire.getCommentaire());
			lblpays.setValue(prestataire.getPays().getLibelle());
			lblnom.setValue(prestataire.getNom());
			lblprenom.setValue(prestataire.getPrenom());
			lbltelephone.setValue(prestataire.getTelephone());
			lblmails.setValue(prestataire.getMails());
			lblnatureprestataire.setValue(prestataire.getNatureprestataire());

		}
			
}
	