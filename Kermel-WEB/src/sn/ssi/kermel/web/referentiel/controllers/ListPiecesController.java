package sn.ssi.kermel.web.referentiel.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPieces;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.PiecesSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;



@SuppressWarnings("serial")
public class ListPiecesController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox list;
	private Paging pg;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private String sygmlibelle,sygmcodepiece;
	private Listheader libelle,description,codepiece,localisation;
	private Textbox txtlibelle,txtdsecription,txtcodepiece,txtlocalisation;
	Long code;
	private Menuitem ADD_PIECES, MOD_PIECES, SUPP_PIECES;
	 private KermelSousMenu monSousMenu;
	 private Utilisateur infoscompte;
		private SygAutoriteContractante autorite=null;

	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_PIECES);
		monSousMenu.afterCompose();
		Components.wireFellows(this, this); // mais le wireFellows precedent
		Components.wireFellows(this, this);
		if (ADD_PIECES != null)
			ADD_PIECES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
		if (MOD_PIECES != null)
			MOD_PIECES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
		if (SUPP_PIECES != null)
			SUPP_PIECES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
		libelle.setSortAscending(new FieldComparator("libelle", false));
		libelle.setSortDescending(new FieldComparator("libelle", true));
		
		description.setSortAscending(new FieldComparator("description", false));
		description.setSortDescending(new FieldComparator("description", true));
		
		codepiece.setSortAscending(new FieldComparator("codepiece", false));	
		codepiece.setSortDescending(new FieldComparator("codepiece", true));
		
		localisation.setSortAscending(new FieldComparator("localisation", false));	
		localisation.setSortDescending(new FieldComparator("localisation", true));
	
 
		list.setItemRenderer(this);

		pg.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pg.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		
	}

	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 List<SygPieces> piece = BeanLocator.defaultLookup(PiecesSession.class).find(pg.getActivePage()*byPage,byPage,sygmcodepiece,sygmlibelle,autorite);
			 SimpleListModel listModel = new SimpleListModel(piece);
			list.setModel(listModel);
			pg.setTotalSize(BeanLocator.defaultLookup( PiecesSession.class).count(sygmcodepiece,sygmlibelle,autorite));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/referentiel/pieces/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(PiecesFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/referentiel/pieces/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(PiecesFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(PiecesFormController.PARAM_WIDOW_CODE, list
					.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				// de
				// fixer
				// les
				// dimenisons
				// du
				// popup
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// de
				// passer
				// des
				// parametres
				// au
				// popup
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < list.getSelectedCount(); i++) {
					Long codes = (Long)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					System.out.println(codes);
				BeanLocator.defaultLookup(PiecesSession.class).delete(codes);
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygPieces pieces = (SygPieces) data;
		item.setValue(pieces.getId());

		Listcell cellCodepiece = new Listcell(pieces.getCodepiece());
		cellCodepiece.setParent(item);
		Listcell celllibelle = new Listcell(pieces.getLibelle());
		celllibelle.setParent(item);
		Listcell celldescription = new Listcell(pieces.getDescription());
		celldescription.setParent(item);
		Listcell celllocalisation = new Listcell(pieces.getLocalisation());
		celllocalisation.setParent(item);
		
		 if(pieces.getLocalisation().equals("n"))
			   celllocalisation.setLabel(Labels.getLabel("kermel.common.form.locales"));
		 else  if(pieces.getLocalisation().equals("i"))
			   celllocalisation.setLabel(Labels.getLabel("kermel.common.form.etrangeres"));
		 else
		    celllocalisation.setLabel(Labels.getLabel("kermel.common.form.lesdeux"));
		 celllocalisation.setParent(item);
		 
	}
	
	public void onOK$txtcodepiece() {
		onClick$btnRechercher();
	}
	public void onOK$txtlibelle() {
		onClick$btnRechercher();
	}
	
	public void onClick$btnRechercher() {
		if (txtcodepiece.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.codepiece")) || txtcodepiece.getValue().equals("")) {
			sygmcodepiece = null;
		} else {
			sygmcodepiece  = txtcodepiece.getValue();
		}	
		
		if (txtlibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.libelle")) || txtlibelle.getValue().equals("")) {
			sygmlibelle = null;
		} else {
			sygmlibelle  = txtlibelle.getValue();
		}	
		

 
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	

		public void onFocus$txtcodepiece() {
			if (txtcodepiece.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.codepiece"))) {
				txtcodepiece.setValue("");
				

			}
		}
			
			public void onFocus$txtlibelle() {
				if (txtlibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.libelle"))) {
					txtlibelle.setValue("");

				}
		}
			

			public void onBlur$txtcodepiece() {
				if (txtcodepiece.getValue().equalsIgnoreCase("")) {
					txtcodepiece.setValue(Labels.getLabel("kermel.common.form.codepiece"));
				}
			}
				public void onBlur$txtlibelle() {
					if (txtlibelle.getValue().equalsIgnoreCase("")) {
						txtlibelle.setValue(Labels.getLabel("kermel.referentiel.common.libelle"));
					}	
			}
				

}