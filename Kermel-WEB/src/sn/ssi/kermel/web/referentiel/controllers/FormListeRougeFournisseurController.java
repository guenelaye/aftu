package sn.ssi.kermel.web.referentiel.controllers;


import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygCategori;
import sn.ssi.kermel.be.entity.SygDecision;
import sn.ssi.kermel.be.entity.SygListeRougeF;
import sn.ssi.kermel.be.entity.TypeCategorie;
import sn.ssi.kermel.be.referentiel.ejb.DecisionSession;
import sn.ssi.kermel.be.referentielprix.ejb.ListeRougeFournisseurSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class FormListeRougeFournisseurController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_STATE = "STATE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle;
	Long code,typecat,id;
	private	SygListeRougeF listerouge=new SygListeRougeF();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";

	TypeCategorie type;
	SygCategori idcategori;
	Session session = getHttpSession();
	private Textbox txtRef ,txtRaisonsociale ,txtMotif ,txtCommentaire,txtNinea;
	private Datebox dtdebut ,dtfin;
	
	private Bandbox bdDecision;
	private Paging pgDecision;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE,activePage;
	private String libellecategorie = null,page=null;
	private Listbox lstDecision;
	private SygDecision decision ;

	

	@Override
	public void onEvent(Event event) throws Exception {

		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgDecision.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgDecision.getActivePage() * byPageBandbox;
				pgDecision.setPageSize(byPageBandbox);
			}
			List<SygDecision> listerouge = BeanLocator.defaultLookup(DecisionSession.class).findAll();
			lstDecision.setModel(new SimpleListModel(listerouge));
			pgDecision.setTotalSize(BeanLocator.defaultLookup(DecisionSession.class).count(null,null));
		
		}


	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);

		lstDecision.setItemRenderer(new DecisionRenderer());
		pgDecision.setPageSize(byPageBandbox);
		pgDecision.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
    	
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			listerouge = BeanLocator.defaultLookup(ListeRougeFournisseurSession.class).findById(code);
			txtRef.setValue(listerouge.getReference());
			txtRaisonsociale.setValue(listerouge.getRaisonsociale());
			txtRef.setValue(listerouge.getReference());
			txtNinea.setValue(listerouge.getNinea());
			txtMotif.setValue(listerouge.getMotif());
			txtCommentaire.setValue(listerouge.getCommentaire());
			dtdebut.setValue(listerouge.getDatedebut());
			dtfin.setValue(listerouge.getDatfin());
			
			if(listerouge.getDecision()!=null)
			{
			  decision=listerouge.getDecision();
			  bdDecision.setValue(decision.getLibelle());
			}
			
		}
	}

	public void onOK() {
		//if(checkFieldConstraints())
	   //	{
			listerouge.setReference(txtRef.getValue());
			listerouge.setRaisonsociale(txtRaisonsociale.getValue());
			listerouge.setNinea(txtNinea.getValue());
			listerouge.setMotif(txtMotif.getValue());
			listerouge.setCommentaire(txtCommentaire.getValue());
			listerouge.setDatedebut(dtdebut.getValue());
			listerouge.setDatfin(dtfin.getValue());
			listerouge.setStatut("no");
			if(decision !=null)
				listerouge.setDecision(decision);	
			
	     	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(ListeRougeFournisseurSession.class).save(listerouge);
				//BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_CAT", Labels.getLabel("kermel.referentiel.common.bureaucmp.ajouter")+" :" + new Date(), login);
				
	
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(ListeRougeFournisseurSession.class).update(listerouge);
				//BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_CAT", Labels.getLabel("kermel.referentiel.common.bureaucmp.modifier")+" :" + new Date(), login);
				
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		//}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			if(txtLibelle.getValue().equals(""))
		     {
               errorComponent = txtLibelle;
               errorMsg = Labels.getLabel("kermel.referentiel.banque.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}


    public class DecisionRenderer implements ListitemRenderer{
	
	
	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygDecision decision = (SygDecision) data;
		item.setValue(decision);
		
		 Listcell cellLibelle = new Listcell(decision.getLibelle());
		 cellLibelle.setParent(item);

	}
}



    public void onSelect$lstDecision(){
    	decision= (SygDecision) lstDecision.getSelectedItem().getValue();
		bdDecision.setValue(decision.getLibelle());
		bdDecision.close();
		
		}
	
}
