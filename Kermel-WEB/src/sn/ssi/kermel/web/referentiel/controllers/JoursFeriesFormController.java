package sn.ssi.kermel.web.referentiel.controllers;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygJoursferies;
import sn.ssi.kermel.be.referentiel.ejb.JoursFeriesSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class JoursFeriesFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtDescription;
	Long code;
	private	SygJoursferies jour=new SygJoursferies();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	List<SygJoursferies> jours = new ArrayList<SygJoursferies>();
	private Datebox dtdate;
	
	@Override
	public void onEvent(Event event) throws Exception {


	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			jour = BeanLocator.defaultLookup(JoursFeriesSession.class).findById(code);
			txtDescription.setValue(jour.getDescription());
			dtdate.setValue(jour.getDate());
		}
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			jour.setDescription(txtDescription.getValue());
			jour.setDate(dtdate.getValue());
		  	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(JoursFeriesSession.class).save(jour);
				BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_JOURSFERIES", Labels.getLabel("kermel.referentiel.common.joursferies.ajouter")+" :" + new Date(), login);
				
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(JoursFeriesSession.class).update(jour);
				BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_JOURSFERIES", Labels.getLabel("kermel.referentiel.common.joursferies.modifier")+" :" + new Date(), login);
				
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			
			

			if(dtdate.getValue()==null)
		     {
               errorComponent = dtdate;
               errorMsg = Labels.getLabel("kermel.referentiel.devises.date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			jours=BeanLocator.defaultLookup(JoursFeriesSession.class).find(0, -1, null, dtdate.getValue());
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT))
			{
			  if(jours.size()>1)
			     {
	                errorComponent = dtdate;
					errorMsg = Labels.getLabel("kermel.referentiel.date.existe")+": "+UtilVue.getInstance().formateLaDate(dtdate.getValue());
					lbStatusBar.setStyle(ERROR_MSG_STYLE);
					lbStatusBar.setValue(errorMsg);
					throw new WrongValueException (errorComponent, errorMsg);
			    }
			}
			else
			{
				  if(jours.size()>0)
				  {
					errorComponent = dtdate;
					errorMsg = Labels.getLabel("kermel.referentiel.date.existe")+" :"+UtilVue.getInstance().formateLaDate(dtdate.getValue());
					lbStatusBar.setStyle(ERROR_MSG_STYLE);
					lbStatusBar.setValue(errorMsg);
					throw new WrongValueException (errorComponent, errorMsg);
				  }
			}

			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	

}
