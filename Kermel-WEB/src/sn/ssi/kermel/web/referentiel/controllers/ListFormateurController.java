package sn.ssi.kermel.web.referentiel.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygFormateur;
import sn.ssi.kermel.be.entity.SygSpecialite;
import sn.ssi.kermel.be.entity.SygTypeFormateur;
import sn.ssi.kermel.be.referentiel.ejb.FormateurSession;
import sn.ssi.kermel.be.referentiel.ejb.SpecialiteSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeFormateurSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class ListFormateurController extends AbstractWindow implements EventListener, AfterCompose, ListitemRenderer {

	private Listbox list,lstSpec;
	private Paging pg,pgSpec;
	public static final String PARAM_WINDOW_CODE = "CODE";

	private Bandbox bSpec;
	private Listheader lshPrenom;
	private String spec,nom,prenom;
	private Textbox txtPrenom,txtNom,txtRechercherSpec;
	private SygFormateur formateur =new SygFormateur();
	private SygSpecialite specialite;
	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private int activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	public static final String WINDOW_PARAM_MODE = "MODE";
	private String page=null;
	Session session = getHttpSession();
    Long idType;
    private SygTypeFormateur type;
	
	private KermelSousMenu monSousMenu;
	private Menuitem ADD_FORMATEUR, MOD_FORMATEUR, DEL_FORMATEUR,DETAIL_FORMATEUR;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_FORMATEUR);
		monSousMenu.afterCompose();
		Components.wireFellows(this, this);
		if (ADD_FORMATEUR != null) {
			ADD_FORMATEUR.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
		}
		if (MOD_FORMATEUR != null) {
			MOD_FORMATEUR.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
		}
		if (DEL_FORMATEUR != null) {
			DEL_FORMATEUR.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
		}
		if (DETAIL_FORMATEUR != null) {
			DETAIL_FORMATEUR.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DETAILS);
		}
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	
		Events.postEvent(ApplicationEvents.ON_CLICK, this, null);
		
		lshPrenom.setSortAscending(new FieldComparator("prenom", false));
		lshPrenom.setSortDescending(new FieldComparator("prenom", true));
		list.setItemRenderer(this);
		pg.setPageSize(byPage);
		pg.addForward("onPaging", this,ApplicationEvents.ON_MODEL_CHANGE);
		

   		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
		lstSpec.setItemRenderer(new SpecRenderer());
		pgSpec.setPageSize(byPageBandBox);
		pgSpec.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTRE_ACTION);
   		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);

   		idType=(Long) session.getAttribute("type");
		type=BeanLocator.defaultLookup(TypeFormateurSession.class).findById(idType);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	/**
	 * Permet de gerer les evenements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet evenement survient, les evenements de la liste sont mis �
		 * jour de meme que l'element de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
 			 List<SygFormateur> formateur = BeanLocator.defaultLookup(FormateurSession.class).findAllby(pg.getActivePage()*byPage,byPage, nom, prenom,null, specialite,type);

 			 SimpleListModel listModel = new SimpleListModel(formateur);
			list.setModel(listModel);
			pg.setTotalSize(BeanLocator.defaultLookup(FormateurSession.class).count(nom, prenom,null, specialite,type));

		} 
	
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/formateur/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.formateur.ajout"));
			display.put(DSP_HEIGHT,"600px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormFormateurController.WINDOW_PARAM_MODE,UIConstants.MODE_NEW);
			
			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				throw new WrongValueException(list, Labels.getLabel("kermel.referentiel.nat.message.selection"));
				
			}
			final String uri ="/formateur/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"600px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.formateur.modif"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormFormateurController.WINDOW_PARAM_MODE,UIConstants.MODE_EDIT);
		    data.put(FormFormateurController.PARAM_WIDOW_CODE, list.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
					throw new WrongValueException(list, Labels.getLabel("kermel.referentiel.nat.message.selection"));
				HashMap<String, String> display = new HashMap<String, String>(); 
			
				// permet de fixer les dimensions du popup
			
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.referentiel.caractere.message.suppression"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.referentiel.caractere.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// permet de passer des parametres au popup
				
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){		
			for (int i = 0; i < list.getSelectedCount(); i++) {
				formateur = (SygFormateur)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
				//System.out.println(pop);
				BeanLocator.defaultLookup(FormateurSession.class).delete(formateur.getId());
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
				
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DETAILS)) {
				 if (list.getSelectedItem() == null) {
					throw new WrongValueException(list, Labels.getLabel("kermel.referentiel.nat.message.selection"));
					
				 }
				final String uri ="/formateur/detail.zul";

				final HashMap<String, String> display = new HashMap<String, String>();
				display.put(DSP_HEIGHT,"80%");
				display.put(DSP_WIDTH, "80%");
				display.put(DSP_TITLE, Labels.getLabel("kermel.formateur.detail"));
				final HashMap<String, Object> data = new HashMap<String, Object>();
			    data.put(DetailFormateurController.PARAM_WIDOW_CODE, list.getSelectedItem().getValue());

				showPopupWindow(uri, data, display); 
	            
			} 

		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)){
			if (event.getData() != null)
			   {
				 
				activePage = Integer.parseInt((String) event.getData());
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
				pgSpec.setPageSize(byPageBandBox);	
			       } 
			    else {
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgSpec.getActivePage() * byPageBandBox;
				pgSpec.setPageSize(byPageBandBox);
			       
			      }
			
				  List<SygSpecialite> specialite = BeanLocator.defaultLookup(SpecialiteSession.class).find(activePage, byPageBandBox,null,spec);
				   lstSpec.setModel(new SimpleListModel(specialite));
				   pgSpec.setTotalSize(BeanLocator.defaultLookup(SpecialiteSession.class).count(null,spec));
				        
		}	
        else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) {
			
			String uri = null;
			
			HashMap<String, String> display = null;
			HashMap<String, Object> data = null;

			uri = "/formateur/doc.zul" ;

			data = new HashMap<String, Object>();
			display = new HashMap<String, String>();

			String nomFichier = (String) list.getSelectedItem().getAttribute("cv");

			data.put(DownloaDocsController.NOM_FICHIER, nomFichier);

			display.put(DSP_TITLE, "Visualisation du fichier");
			display.put(DSP_WIDTH, "90%");
			display.put(DSP_HEIGHT, "600px");

			showPopupWindow(uri, data, display);
		
		}

	}
	
	

	/**
	 * Definit comment un element de la liste est affiche.
	 */
	
	
	public void onSelect$lstSpec(){
		 specialite = (SygSpecialite) lstSpec.getSelectedItem().getValue();
		 bSpec.setValue(specialite.getLibelle());
		
		 bSpec.close();
	}
	
	 public void onBlur$txtRechercherSpec()
	 {
	 if(txtRechercherSpec.getValue().equals(null))
		 txtRechercherSpec.setValue(Labels.getLabel("kermel.referentiel.nat.form.rechercher"));
	 }
		 
	public void onFocus$txtRechercherSpec(){
		 if(txtRechercherSpec.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.nat.form.rechercher"))){
			 txtRechercherSpec.setValue("");
		 }		 
	}
	
	public void  onClick$txtRechercherSpec(){
		if(txtRechercherSpec.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.nat.form.rechercher")))
		{
			spec = null;
			page = null;
		}
		else
		{
			spec = txtRechercherSpec.getValue();
			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}
	
	public void  onClick$btnRechercherSpec(){
		if(txtRechercherSpec.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.nat.form.rechercher")))
		{
			spec = null;
			page = null;
		}
		else
		{
			spec = txtRechercherSpec.getValue();
			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	

	public class SpecRenderer implements ListitemRenderer {
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygSpecialite sp= (SygSpecialite) data;
			item.setValue(sp);
			
			Listcell cellSp = new Listcell(sp.getLibelle());
			cellSp.setParent(item);

		}
	}
	
	public void onClick$bchercher()
	{
		onOK();
	}
	
	public void onOK()
	{
		nom = txtNom.getValue();
		prenom=txtPrenom.getValue();
		spec=bSpec.getValue();
		
		if(!prenom.equalsIgnoreCase(Labels.getLabel("kermel.formateur.pnom")))
		{ if(nom.equalsIgnoreCase(Labels.getLabel("kermel.formateur.nom")))
			nom="";
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	    }

		if(!nom.equalsIgnoreCase(Labels.getLabel("kermel.formateur.nom")))
		{ if(prenom.equalsIgnoreCase(Labels.getLabel("kermel.formateur.pnom")))
			prenom="";
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	    }
		
	  if(nom.equalsIgnoreCase(Labels.getLabel("kermel.formateur.nom"))&& prenom.equalsIgnoreCase(Labels.getLabel("kermel.formateur.pnom")))
			  {
		       nom="";
		       prenom="";
		      }
	  Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
	
	public void onFocus$txtPrenom()
	{
		if(txtPrenom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.formateur.pnom")))
			txtPrenom.setValue("");
		else
			prenom=txtPrenom.getValue();
	}
	
	
	public void onBlur$txtPrenom()
	{
		if(txtPrenom.getValue().equals(""))
			txtPrenom.setValue(Labels.getLabel("kermel.formateur.pnom"));
	}
	
	public void onFocus$txtNom()
	{
		if(txtNom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.formateur.nom")))
			txtNom.setValue("");
		else
			nom=txtNom.getValue();
	}
	
	
	public void onBlur$txtNom()
	{
		if(txtNom.getValue().equals(""))
			txtNom.setValue(Labels.getLabel("kermel.formateur.nom"));
	}
	
	

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		 SygFormateur formateur = (SygFormateur) data;
		 item.setValue(formateur);
		 item.setAttribute("cv", formateur.getCv());
		 
		Listcell cellNin,cellNom,cellPnom, cellAdd,cellTel,cellMail,cellDatenaiss,cellLieunaiss,cellSpec,
		cellCom,cellSexe,cellCv;
		
		
		if(formateur.getNin()!=null) {
			cellNin = new Listcell(formateur.getNin().toString());
			cellNin.setParent(item);
		}
		else {
			cellNin = new Listcell("");
			cellNin.setParent(item);
		}
		if(formateur.getPrenom()!=null) {
			cellPnom = new Listcell(formateur.getPrenom());
			cellPnom.setParent(item);
		}
		else {
			cellPnom = new Listcell("");
			cellPnom.setParent(item);
		}
		
		if(formateur.getNom()!=null) {
			cellNom = new Listcell(formateur.getNom());
			cellNom.setParent(item);
		}
		else {
			cellNom = new Listcell("");
			cellNom.setParent(item);
		}
		
		if(formateur.getDateNaiss()!=null) {
			cellDatenaiss = new Listcell(UtilVue.getInstance().formateLaDate(formateur.getDateNaiss()));
			cellDatenaiss.setParent(item);
		}
		else {
			cellDatenaiss = new Listcell("");
			cellDatenaiss.setParent(item);
		}
		
//		if(formateur.getLieuNaiss()!=null) {
//			cellLieunaiss = new Listcell(formateur.getLieuNaiss());
//			cellLieunaiss.setParent(item);
//		}
//		else {
//			cellLieunaiss = new Listcell("");
//			cellLieunaiss.setParent(item);
//		}
		
//		if(formateur.getAddress()!=null) {
//			cellAdd = new Listcell(formateur.getAddress());
//			cellAdd.setParent(item);
//		}
//		else {
//			cellAdd = new Listcell("");
//			cellAdd.setParent(item);
//		}
//		if(formateur.getEmail()!=null) {
//			cellMail = new Listcell(formateur.getEmail());
//			cellMail.setParent(item);
//		}
//		else {
//			cellMail = new Listcell("");
//			cellMail.setParent(item);
//		}
//		
//		if(formateur.getTel()!=null) {
//			cellTel = new Listcell(formateur.getTel().toString());
//			cellTel.setParent(item);
//		}
//		else {
//			cellTel = new Listcell("");
//			cellTel.setParent(item);
//		}
//		if(formateur.getCommentaire()!=null) {
//			cellCom = new Listcell(formateur.getCommentaire());
//			cellCom.setParent(item);
//		}
//		else {
//			cellCom = new Listcell("");
//			cellCom.setParent(item);
//		}
		
		if(formateur.getSpecialite()!=null) {
			cellSpec = new Listcell(formateur.getSpecialite().getLibelle());
			cellSpec.setParent(item);
		}
		else {
			cellSpec = new Listcell("");
			cellSpec.setParent(item);
		}
		
	
		//sexe
		String img = "";
	          if(formateur.getSexe().equals("Homme")) {
	                img = "/images/male.png";  
	        }else {
	                img = "/images/female.png";
	        }
	        if(formateur.getSexe()!=null) {
				cellSexe = new Listcell(null,img);
				cellSexe.setParent(item);
			}
			else {
				cellSexe = new Listcell("");
				cellSexe.setParent(item);
			}
	        
	    	if(formateur.getCv()!=null && !"".equals(formateur.getCv())) {
				cellCv = new Listcell("");
				
				cellCv.setImage("/images/PaperClip-16x16.png");
				cellCv.setAttribute(UIConstants.TODO, "VIEW");
				cellCv.addEventListener(Events.ON_CLICK, this);
				cellCv.setAttribute("cv", formateur.getCv());
				cellCv.setParent(item);
			}
			else {
				cellCv = new Listcell("");
				cellCv.setParent(item);
			}	
	}  
}
