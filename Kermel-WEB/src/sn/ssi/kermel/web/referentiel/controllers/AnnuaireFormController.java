package sn.ssi.kermel.web.referentiel.controllers;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAnnuaire;
import sn.ssi.kermel.be.entity.SygService;
import sn.ssi.kermel.be.referentiel.ejb.AnnuaireSession;
import sn.ssi.kermel.be.referentiel.ejb.ServiceSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class AnnuaireFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_STATE = "STATE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtNom, txtPrenom,txtFonction,txtTelephone,txtEmail;
	Long code;
	private	SygAnnuaire annuaire=new SygAnnuaire();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg,login;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Bandbox bdService;
	private Paging pgService;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE,activePage;
	private String libelleservice = null,callBack,page=null;
	private Listbox lstService;
	private Textbox txtRechercherService,txtService;
	SygService service=null;
	private String codemodification,codeajout,libellemodification,libelleajout;
	
	@Override
	public void onEvent(Event event) throws Exception {
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgService.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgService.getActivePage() * byPageBandbox;
				pgService.setPageSize(byPageBandbox);
			}
			List<SygService> service = BeanLocator.defaultLookup(ServiceSession.class).find(activePage, byPageBandbox,null,libelleservice,null, null);
			lstService.setModel(new SimpleListModel(service));
			pgService.setTotalSize(BeanLocator.defaultLookup(ServiceSession.class).count(null,libelleservice,null, null));
		}

	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		lstService.setItemRenderer(new ServicesRenderer());
		pgService.setPageSize(byPageBandbox);
		pgService.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		callBack= (String) map.get(PARAM_WINDOW_STATE);
		if(callBack.equals(UIConstants.ANNUAIRE_ARMP))
		{
		   
		   codeajout="ADD_ANNUAIREARMP";
		   libelleajout=Labels.getLabel("kermel.annuaire.armp.ajouter");
		   codemodification="MOD_ANNUAIREARMP";
		   libellemodification=Labels.getLabel("kermel.annuaire.armp.modifier");
		}
		else
		{
			
			codeajout="ADD_ANNUAIREDCMP";
			libelleajout=Labels.getLabel("kermel.annuaire.dcmp.ajouter");
			codemodification="MOD_ANNUAIREDCMP";
			libellemodification=Labels.getLabel("kermel.annuaire.dcmp.modifier");
		}
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			annuaire = BeanLocator.defaultLookup(AnnuaireSession.class).findById(code);
			txtPrenom.setValue(annuaire.getPrenom());
			txtNom.setValue(annuaire.getNom());
			txtFonction.setValue(annuaire.getFonction());
			service=annuaire.getService();
			txtService.setValue(service.getCodification());
			bdService.setValue(service.getLibelle());
			txtEmail.setValue(annuaire.getEmail());
			txtTelephone.setValue(annuaire.getTelephone());
		}
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			annuaire.setPrenom(txtPrenom.getValue());
			annuaire.setNom(txtNom.getValue());
			annuaire.setFonction(txtFonction.getValue());
			annuaire.setService(service);
			annuaire.setOrgane(callBack);
			annuaire.setEmail(txtEmail.getValue());
			annuaire.setTelephone(txtTelephone.getValue());
	     	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(AnnuaireSession.class).save(annuaire);
				BeanLocator.defaultLookup(JournalSession.class).logAction(codeajout,libelleajout+" :" + new Date(), login);
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(AnnuaireSession.class).update(annuaire);
				BeanLocator.defaultLookup(JournalSession.class).logAction(codemodification,libellemodification+" :" + new Date(), login);
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			if(txtPrenom.getValue().equals(""))
		     {
               errorComponent = txtPrenom;
               errorMsg = Labels.getLabel("kermel.common.form.prenom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			

			if(txtNom.getValue().equals(""))
		     {
               errorComponent = txtNom;
               errorMsg = Labels.getLabel("kermel.common.form.nom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtFonction.getValue().equals(""))
		     {
              errorComponent = txtFonction;
              errorMsg = Labels.getLabel("kermel.common.form.fonction")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }

			if(service==null)
		     {
             errorComponent = bdService;
             errorMsg = Labels.getLabel("kermel.common.form.service")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }

			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	

		///////////Service///////// 
		public void onSelect$lstService(){
		service= (SygService) lstService.getSelectedItem().getValue();
		bdService.setValue(service.getLibelle());
		txtService.setValue(service.getCodification());
		bdService.close();
		
		}
		
		public class ServicesRenderer implements ListitemRenderer{
			
		
		
			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
				SygService service = (SygService) data;
				item.setValue(service);
				
				Listcell cellCode = new Listcell("");
				if (service.getCodification()!=null){
					cellCode.setLabel(service.getCodification());
				}
				cellCode.setParent(item);
			
				Listcell cellLibelle = new Listcell("");
				if (service.getLibelle()!=null){
					cellLibelle.setLabel(service.getLibelle());
				}
				cellLibelle.setParent(item);
		
			}
		}
		public void onFocus$txtRechercherService(){
		if(txtRechercherService.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.service"))){
			txtRechercherService.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherService(){
		if(txtRechercherService.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.service")) || txtRechercherService.getValue().equals("")){
			libelleservice = null;
			page=null;
		}else{
			libelleservice = txtRechercherService.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
		
		 public void onOK$txtService() {
			 List<SygService> services = BeanLocator.defaultLookup(ServiceSession.class).find(0, -1, txtService.getValue(),null,null, null);
				
				if (services.size() > 0) {
					service=(SygService)services.get(0);
					txtService.setValue(service.getCodification());
					bdService.setValue(service.getLibelle());
					
				}
				else {
					bdService.setValue(null);
					bdService.open();
					txtService.setFocus(true);
				}
				
			}

}
