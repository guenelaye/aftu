package sn.ssi.kermel.web.referentiel.controllers;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCalendrierPaiement;
import sn.ssi.kermel.be.entity.SygCatFournisseur;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDemandePaiement;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.ContratSession;
import sn.ssi.kermel.be.session.CalendrierPaiementSession;
import sn.ssi.kermel.be.session.DemandePaiementSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class DemandePaiementEditerFormController extends AbstractWindow implements
		EventListener, AfterCompose,ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtNom, txtAdresse, txtEmail, txtTel, txtFax;
	Long code;
	private	SygDemandePaiement demande=new SygDemandePaiement();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Bandbox bdCategorie;
	private Paging pgCategorie;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE,activePage;
	private String libellecategorie = null, page=null;
	private Listbox lstCategorie;
	private Textbox txtRechercherCategorie,txtRef,txtlivrable;
	SygCatFournisseur categorie=null;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private Datebox  datePaiement,dateCourrier;
	private Doublebox  montant;
	Session session = getHttpSession();
	private SygContrats contrat;
	private Paging pgLignes,pgProduit;
	private Listbox  listeLignes;
	private final int byPage = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Div  div0,div1;
    private SygCalendrierPaiement calendreier;
    private Menuitem  menuSuivant,menuValider,menuFermer;
    List<SygCalendrierPaiement> produits = new ArrayList<SygCalendrierPaiement>();
	

	@Override
	public void onEvent(Event event) throws Exception {
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)){
			
		 System.out.println(" #######################################"+(Long) session.getAttribute("demande"));
        
		   produits = BeanLocator.defaultLookup(CalendrierPaiementSession.class).findCalen(demande.getCalendrier().getCalenpaId());
				
			listeLignes.setModel(new SimpleListModel(produits));
    		pgLignes.setTotalSize(BeanLocator.defaultLookup(CalendrierPaiementSession.class).count(null,null));
		
		}
			

	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
		/*
		lstCategorie.setItemRenderer(new CategoriesRenderer());
		pgCategorie.setPageSize(byPageBandbox);
		pgCategorie.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		 */
		
		pgLignes.setPageSize(byPage);
		pgLignes.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		listeLignes.setItemRenderer(this);
		
		 Long idcontrat = (Long) session.getAttribute("contrat");
		   contrat=BeanLocator.defaultLookup(ContratSession.class).findById(idcontrat);
		
		  
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			demande = BeanLocator.defaultLookup(DemandePaiementSession.class).findById(code);
			datePaiement.setValue(demande.getDatepaiement());
			txtRef.setValue(demande.getRef());
			dateCourrier.setValue(demande.getDateCourrier());
			montant.setValue(demande.getMontantpaye());
			txtlivrable.setValue(demande.getLivrable());
		// produits.add(BeanLocator.defaultLookup(CalendrierPaiementSession.class).findById(demande.getCalendrier().getCalenpaId()));
			Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);	
		}
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			demande.setDatepaiement(datePaiement.getValue());
			demande.setRef(txtRef.getValue());
			demande.setDateCourrier(dateCourrier.getValue());
			demande.setMontantpaye(montant.getValue());
			demande.setLivrable(txtlivrable.getValue());
			demande.setContrat(contrat);
			demande.setCalendrier(calendreier);
			
			if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)){ 
			
			}
	
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) 
				BeanLocator.defaultLookup(DemandePaiementSession.class).update(demande);
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
	public void onSuivant() {
		
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) 
			calendreier=BeanLocator.defaultLookup(CalendrierPaiementSession.class).findById(demande.getCalendrier().getCalenpaId());
		else
			calendreier = (SygCalendrierPaiement) listeLignes.getSelectedItem().getValue();
		
		div0.setVisible(false);
		div1.setVisible(true);
		menuSuivant.setDisabled(true);
		menuValider.setDisabled(false);
		montant.setValue(calendreier.getMontantpaye());
		txtlivrable.setValue(calendreier.getLivrable());
		
		
	}

	
private boolean checkFieldConstraints() {
		
		try {		

			if(datePaiement.getValue()==null)
		     {
				errorComponent = datePaiement;
              	errorMsg = Labels.getLabel("kermel.common.form.nom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(txtRef.getValue().equals(""))
		     {
				errorComponent = txtRef;
             	errorMsg = Labels.getLabel("kermel.importateur.adresse")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }

			if(dateCourrier.getValue()==null)
		     {
				errorComponent = dateCourrier;
             	errorMsg = Labels.getLabel("kermel.referentiel.common.code.categorie")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }

			
			if(montant.getValue()==null)
		     {
				errorComponent = montant;
            	errorMsg = Labels.getLabel("kermel.referentiel.common.code.categorie")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }

			if(txtlivrable.getValue().equals(""))
		     {
				errorComponent = montant;
           	errorMsg = Labels.getLabel("kermel.referentiel.common.code.categorie")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }

			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	

		///////////Categorie///////// 
		public void onSelect$lstCategorie(){
			categorie= (SygCatFournisseur) lstCategorie.getSelectedItem().getValue();
			bdCategorie.setValue(categorie.getLibelle());
			bdCategorie.close();
		}
		
		
		public class CategoriesRenderer implements ListitemRenderer{
			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
				SygCatFournisseur categorie = (SygCatFournisseur) data;
				item.setValue(categorie);
				
				Listcell cellLibelle = new Listcell("");
				if (categorie.getLibelle()!=null){
					cellLibelle.setLabel(categorie.getLibelle());
				}
				cellLibelle.setParent(item);
		
			}
		}
		
		public void onFocus$txtRechercherCategorie(){
		if(txtRechercherCategorie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.code.categorie"))){
			txtRechercherCategorie.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherCategorie(){
		if(txtRechercherCategorie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.code.categorie")) || txtRechercherCategorie.getValue().equals("")){
			libellecategorie = null;
			page=null;
		}
		else{
			libellecategorie = txtRechercherCategorie.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}


		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
				// TODO Auto-generated method stub
			
			SygCalendrierPaiement produits = (SygCalendrierPaiement) data;
			item.setValue(produits);
			item.setSelected(true);
//			String codeprod = produit.getProduitPK().getCprod()+" "+produit.getProduitPK().getPrecsen()+" "+produit.getProduitPK().getPrecuemoa();
			Listcell cellProduit = new Listcell("");
			//if(produits.getDatepaiement()!=null)
			 cellProduit = new Listcell(ToolKermel.dateToString(produits.getDatepaiement()));					
			cellProduit.setParent(item);
			
			Listcell cellPrecum = new Listcell(produits.getLivrable());					
			cellPrecum.setParent(item);
			
			Listcell cellPrecsen = new Listcell(String.valueOf(produits.getTaux())+"%");					
			cellPrecsen.setParent(item);
			
			Listcell cellesp = new Listcell(ToolKermel.formatDecimal(produits.getMontantpaye()));					
			cellesp.setParent(item);
			
			Listcell cellValeur = new Listcell(produits.getCommentaire());
			cellValeur.setParent(item);
			
			Listcell cellEcheance = new Listcell("", "/images/cancel.png");
			 SimpleDateFormat inputDf = new SimpleDateFormat("yyyy-MM-dd");
			 Date maDateAvecFormat=new Date();
			 String datejour=inputDf.format(maDateAvecFormat);
			 
			if((produits.getDatepaiement().getTime()- new Date().getTime())>=0)
			 cellEcheance = new Listcell("", "/images/ok.png");
				  
			//System.out.println(ToolKermel.DifferenceDate(produits.getDatepaiement(),new Date(),"jj")+"######################"+produits.getDatepaiement()+"########"+inputDf.parse(datejour));
			cellEcheance.setParent(item);
			
			Listcell cellPayer = new Listcell("", "/images/cancel.png");
			if ((produits.getPayer()!=null)&&(produits.getPayer().equalsIgnoreCase("oui")))
			     cellPayer = new Listcell("", "/images/ok.png");
			cellPayer.setParent(item);
			
			
		
		}
}
