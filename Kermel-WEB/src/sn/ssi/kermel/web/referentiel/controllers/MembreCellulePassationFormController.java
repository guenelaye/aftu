package sn.ssi.kermel.web.referentiel.controllers;


import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygMembreCellulePassation;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.MembreCellulePassationSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class MembreCellulePassationFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtNom, txtPrenom, txtEmail, txtTel, txtFonction;
	private Combobox cbService;
	Long code;
	private	SygMembreCellulePassation membre = new SygMembreCellulePassation();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	
	
	@Override
	public void onEvent(Event event) throws Exception {
		
	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			membre = BeanLocator.defaultLookup(MembreCellulePassationSession.class).findById(code);
			
			txtNom.setValue(membre.getNom());
			txtPrenom.setValue(membre.getPrenom());
			txtFonction.setValue(membre.getFonction());
			txtTel.setValue(membre.getTel());
			txtEmail.setValue(membre.getEmail());
			cbService.setValue(membre.getService());
		}
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			membre.setNom(txtNom.getValue());
			membre.setPrenom(txtPrenom.getValue());
			membre.setFonction(txtFonction.getValue());
			membre.setTel(txtTel.getValue());
			membre.setEmail(txtEmail.getValue());
			membre.setService(cbService.getValue());
			membre.setAutorite(autorite);
			if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) 
				BeanLocator.defaultLookup(MembreCellulePassationSession.class).save(membre);
	
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) 
				BeanLocator.defaultLookup(MembreCellulePassationSession.class).update(membre);
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {		

			if(txtNom.getValue().equals(""))
		     {
				errorComponent = txtNom;
              	errorMsg = Labels.getLabel("kermel.common.form.nom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(txtPrenom.getValue().equals(""))
		     {
				errorComponent = txtPrenom;
             	errorMsg = Labels.getLabel("kermel.common.form.prenom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	
}
