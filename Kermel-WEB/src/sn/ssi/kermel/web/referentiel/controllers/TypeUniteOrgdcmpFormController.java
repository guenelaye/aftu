package sn.ssi.kermel.web.referentiel.controllers;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygTypeUniteOrgdcmp;
import sn.ssi.kermel.be.referentiel.ejb.TypeUniteOrgdcmpSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class TypeUniteOrgdcmpFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle;
	Long code;
	private	SygTypeUniteOrgdcmp typeuniteorg=new SygTypeUniteOrgdcmp();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg,login;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Combobox comNiveau;
	private int niveau,filtrniveau;
	// /Type decision
	private SygTypeUniteOrgdcmp direction = null;
	private Textbox txtRechercherType;
	private Bandbox bdType;
	private Paging pgType;
	private Listbox lstType;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private String LibelleType = null;

	@Override
	public void onEvent(Event event) throws Exception {

		// champ direction du formulaire
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_TYPEUNITEORG)) {
			List<SygTypeUniteOrgdcmp> direction = BeanLocator.defaultLookup(TypeUniteOrgdcmpSession.class).findDirection(
					pgType.getActivePage() * byPageBandbox, byPageBandbox, null, LibelleType,filtrniveau);
			SimpleListModel listModel = new SimpleListModel(direction);
			lstType.setModel(listModel);
			pgType.setTotalSize(BeanLocator.defaultLookup(TypeUniteOrgdcmpSession.class).countDirection(null, LibelleType,filtrniveau));
		}
	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		// champ Type decision du formulaire
		addEventListener(ApplicationEvents.ON_TYPEUNITEORG, this);
		lstType.setItemRenderer(new typeuniteorgRenderer());
		pgType.setPageSize(byPageBandbox);
		pgType.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_TYPEUNITEORG);
		
	}
	
	

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			typeuniteorg = BeanLocator.defaultLookup(TypeUniteOrgdcmpSession.class).findById(code);
			txtLibelle.setValue(typeuniteorg.getLibelle());
			String niveaux=Integer.valueOf(typeuniteorg.getNiveau()).toString();
			comNiveau.setValue(niveaux);
//			  if(niveaux.equalsIgnoreCase("1")){
//				
//				comNiveau.setSelectedIndex(1);
//				}else if(niveaux.equalsIgnoreCase("2")){
//					comNiveau.setSelectedIndex(2);
//				}else if(niveaux.equalsIgnoreCase("3")){
//					comNiveau.setSelectedIndex(3);
//				}else if(niveaux.equalsIgnoreCase("4")){
//					comNiveau.setSelectedIndex(4);
//				}else if(niveaux.equalsIgnoreCase("5")){
//					comNiveau.setSelectedIndex(5);
//				}
			
			if(typeuniteorg.getTypeuniteorg()!=null)
			{
				direction=typeuniteorg.getTypeuniteorg();
				bdType.setValue(direction.getLibelle());
			}
			
			
		}
	}
	
	public void onSelect$comNiveau() {
		
		if(comNiveau.getSelectedItem().getLabel().equalsIgnoreCase("1")){
		//niveau=Integer.parseInt(comNiveau.getValue());
			niveau=1; filtrniveau=0;
		}else if(comNiveau.getSelectedItem().getLabel().equalsIgnoreCase("2")){
			niveau=2; filtrniveau=1;
		}else if(comNiveau.getSelectedItem().getLabel().equalsIgnoreCase("3")){
			niveau=3; filtrniveau=2;
		}else if(comNiveau.getSelectedItem().getLabel().equalsIgnoreCase("4")){
			niveau=4; filtrniveau=3;
		}else if(comNiveau.getSelectedItem().getLabel().equalsIgnoreCase("5")){
			niveau=5;filtrniveau=4;
		}
		bdType.setValue("");
		Events.postEvent(ApplicationEvents.ON_TYPEUNITEORG, this, null);
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			
//			if(direction!=null)
//			  typeuniteorg.setNiveau(1);
//			else
//				 typeuniteorg.setNiveau(0);
			if(niveau!=0)
			typeuniteorg.setNiveau(niveau);
			else
				typeuniteorg.setNiveau(0);
			
			typeuniteorg.setTypeuniteorg(direction);
			typeuniteorg.setLibelle(txtLibelle.getValue());  
		  	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(TypeUniteOrgdcmpSession.class).save(typeuniteorg);
				BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_TYPEUNITEORGDCMP", Labels.getLabel("kermel.referentiel.common.typeuniteorgcmp.ajouter")+" :" + new Date(), login);
				
	
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(TypeUniteOrgdcmpSession.class).update(typeuniteorg);
				BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_TYPEUNITEORGDCMP", Labels.getLabel("kermel.referentiel.common.typeuniteorgcmp.modifier")+" :" + new Date(), login);
				
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			

			if(txtLibelle.getValue().equals(""))
		     {
               errorComponent = txtLibelle;
               errorMsg = Labels.getLabel("kermel.referentiel.banque.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			
			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}


////////////type decision
public void onSelect$lstType() {
	direction = (SygTypeUniteOrgdcmp) lstType.getSelectedItem().getValue();
	bdType.setValue((String) lstType.getSelectedItem().getAttribute(UIConstants.ATTRIBUTE_LIBELLE));
	bdType.close();
}

public class typeuniteorgRenderer implements ListitemRenderer {

	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygTypeUniteOrgdcmp type = (SygTypeUniteOrgdcmp) data;
		item.setValue(type);
		item.setAttribute(UIConstants.ATTRIBUTE_LIBELLE, type.getLibelle());

		Listcell cellLibelle = new Listcell("");
		if (type.getLibelle() != null) {
			cellLibelle.setLabel(type.getLibelle());
		}
		cellLibelle.setParent(item);
		//				

	}

	
}

public void onFocus$txtRechercherType() {
	if (txtRechercherType.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.typeuniteorg.direction"))) {
		txtRechercherType.setValue("");
	}
}

public void onBlur$txtRechercherType() {
	if (txtRechercherType.getValue().equalsIgnoreCase("")) {
		txtRechercherType.setValue(Labels.getLabel("kermel.referentiel.typeuniteorg.direction"));
	}
}

public void onClick$btnRechercherType() {
	if (txtRechercherType.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.typeuniteorg.direction")) || txtRechercherType.getValue().equals("")) {
		LibelleType = null;
	} else {
		LibelleType = txtRechercherType.getValue();
	}
	Events.postEvent(ApplicationEvents.ON_TYPEUNITEORG, this, null);
}

// ////////////////fin ////////////////////////////////
	
@Override
public void render(Listitem arg0, Object arg1, int index) throws Exception {
	// TODO Auto-generated method stub
	
}
}
