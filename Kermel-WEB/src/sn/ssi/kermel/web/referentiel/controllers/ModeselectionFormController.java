package sn.ssi.kermel.web.referentiel.controllers;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygModeselection;
import sn.ssi.kermel.be.referentiel.ejb.ModeselectionSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class ModeselectionFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle,txtCode,txtDescriptions;
	Long code;
	private	SygModeselection modeselection=new SygModeselection();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	List<SygModeselection> modeselections = new ArrayList<SygModeselection>();
	
	@Override
	public void onEvent(Event event) throws Exception {


	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			modeselection = BeanLocator.defaultLookup(ModeselectionSession.class).findById(code);
			txtCode.setValue(modeselection.getCode());
			txtLibelle.setValue(modeselection.getLibelle());
			txtDescriptions.setValue(modeselection.getDescription());
			
		}
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			modeselection.setCode(txtCode.getValue());
			modeselection.setLibelle(txtLibelle.getValue());
			modeselection.setDescription(txtDescriptions.getValue()); 
		  	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(ModeselectionSession.class).save(modeselection);
				BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_MODESELECTION", Labels.getLabel("kermel.referentiel.common.modeselection.ajouter")+" :" + new Date(), login);
				
	
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(ModeselectionSession.class).update(modeselection);
				BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_MODESELECTION", Labels.getLabel("kermel.referentiel.common.modeselection.modifier")+" :" + new Date(), login);
				
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			
			

			if(txtCode.getValue().equals(""))
		     {
               errorComponent = txtCode;
               errorMsg = Labels.getLabel("kermel.bureau.douane.code")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			modeselections=BeanLocator.defaultLookup(ModeselectionSession.class).find(txtCode.getValue());
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT))
			{
				if(!modeselection.getCode().equals(txtCode.getValue()))
				{
					if(modeselections.size()>0)
					  {
			              errorComponent = txtCode;
						  errorMsg =Labels.getLabel("kermel.bureau.douane.code")+" "+ Labels.getLabel("kermel.referentiel.existe")+": "+txtCode.getValue();
						  lbStatusBar.setStyle(ERROR_MSG_STYLE);
						  lbStatusBar.setValue(errorMsg);
						 throw new WrongValueException (errorComponent, errorMsg);
					 }
				}
			
			}
			else
			{
				  if(modeselections.size()>0)
				  {
					errorComponent = txtCode;
					errorMsg = Labels.getLabel("kermel.bureau.douane.code")+" "+Labels.getLabel("kermel.referentiel.existe")+" :"+txtCode.getValue();
					lbStatusBar.setStyle(ERROR_MSG_STYLE);
					lbStatusBar.setValue(errorMsg);
					throw new WrongValueException (errorComponent, errorMsg);
				  }
			}
			if(txtLibelle.getValue().equals(""))
		     {
             errorComponent = txtLibelle;
             errorMsg = Labels.getLabel("kermel.referentiel.banque.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	

}
