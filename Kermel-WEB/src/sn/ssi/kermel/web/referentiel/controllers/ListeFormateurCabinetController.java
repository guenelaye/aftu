package sn.ssi.kermel.web.referentiel.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygFormateur;
import sn.ssi.kermel.be.entity.SygSpecialite;
import sn.ssi.kermel.be.entity.SygTypeFormateur;
import sn.ssi.kermel.be.referentiel.ejb.FormateurSession;
import sn.ssi.kermel.be.referentiel.ejb.SpecialiteSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeFormateurSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class ListeFormateurCabinetController extends AbstractWindow implements EventListener, AfterCompose, ListitemRenderer {

	private Listbox list,lstSpec;
	private Paging pg,pgSpec;
	public static final String PARAM_WINDOW_CODE = "CODE";

	private Bandbox bSpec;
	private Listheader lshRaison;
	private String spec,raison;
	private Textbox txtRaison,txtRechercherSpec;
	private SygFormateur formateur =new SygFormateur();
	private SygSpecialite specialite;
	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private int activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	public static final String WINDOW_PARAM_MODE = "MODE";
	private String page=null;
	Session session = getHttpSession();
    Long idType;
    private SygTypeFormateur type;
	
	private KermelSousMenu monSousMenu;
	private Menuitem ADD_FORMATEUR, MOD_FORMATEUR, DEL_FORMATEUR,DETAIL_FORMATEUR;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_FORMATEUR);
		monSousMenu.afterCompose();
		Components.wireFellows(this, this);
		if (ADD_FORMATEUR != null) {
			ADD_FORMATEUR.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
		}
		if (MOD_FORMATEUR != null) {
			MOD_FORMATEUR.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
		}
		if (DEL_FORMATEUR != null) {
			DEL_FORMATEUR.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
		}
		if (DETAIL_FORMATEUR != null) {
			DETAIL_FORMATEUR.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DETAILS);
		}
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	
		Events.postEvent(ApplicationEvents.ON_CLICK, this, null);
		
		lshRaison.setSortAscending(new FieldComparator("societe", false));
		lshRaison.setSortDescending(new FieldComparator("societe", true));
		list.setItemRenderer(this);
		pg.setPageSize(byPage);
		pg.addForward("onPaging", this,ApplicationEvents.ON_MODEL_CHANGE);
		

   		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
		lstSpec.setItemRenderer(new SpecRenderer());
		pgSpec.setPageSize(byPageBandBox);
		pgSpec.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTRE_ACTION);
   		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);

   		idType=(Long) session.getAttribute("type");
		type=BeanLocator.defaultLookup(TypeFormateurSession.class).findById(idType);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	/**
	 * Permet de gerer les evenements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet evenement survient, les evenements de la liste sont mis �
		 * jour de meme que l'element de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
 			 List<SygFormateur> formateur = BeanLocator.defaultLookup(FormateurSession.class).findRaison(pg.getActivePage()*byPage,byPage,raison,specialite,type);

 			 SimpleListModel listModel = new SimpleListModel(formateur);
			list.setModel(listModel);
			pg.setTotalSize(BeanLocator.defaultLookup(FormateurSession.class).count(null, null,null, specialite,type));

		} 
	
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/formateur/formfcabinet.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.formateur.ajout"));
			display.put(DSP_HEIGHT,"85%");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormCabineController.WINDOW_PARAM_MODE,UIConstants.MODE_NEW);
			
			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				throw new WrongValueException(list, Labels.getLabel("kermel.referentiel.nat.message.selection"));
				
			}
			final String uri ="/formateur/formfcabinet.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"85%");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.formateur.modif"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormCabineController.WINDOW_PARAM_MODE,UIConstants.MODE_EDIT);
		    data.put(FormCabineController.PARAM_WIDOW_CODE, list.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
					throw new WrongValueException(list, Labels.getLabel("kermel.referentiel.nat.message.selection"));
				HashMap<String, String> display = new HashMap<String, String>(); 
			
				// permet de fixer les dimensions du popup
			
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.referentiel.caractere.message.suppression"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.referentiel.caractere.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// permet de passer des parametres au popup
				
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){		
			for (int i = 0; i < list.getSelectedCount(); i++) {
				formateur = (SygFormateur)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
				//System.out.println(pop);
				BeanLocator.defaultLookup(FormateurSession.class).delete(formateur.getId());
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
				
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DETAILS)) {
				 if (list.getSelectedItem() == null) {
					throw new WrongValueException(list, Labels.getLabel("kermel.referentiel.nat.message.selection"));
					
				 }
				final String uri ="/formateur/detailCabinet.zul";

				final HashMap<String, String> display = new HashMap<String, String>();
				display.put(DSP_HEIGHT,"80%");
				display.put(DSP_WIDTH, "80%");
				display.put(DSP_TITLE, Labels.getLabel("kermel.formateur.detail"));
				final HashMap<String, Object> data = new HashMap<String, Object>();
			    data.put(DetailCabinetController.PARAM_WINDOW_CODE, list.getSelectedItem().getValue());

				showPopupWindow(uri, data, display); 
	            
			} 

		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)){
			if (event.getData() != null)
			   {
				 
				activePage = Integer.parseInt((String) event.getData());
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
				pgSpec.setPageSize(byPageBandBox);	
			       } 
			    else {
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgSpec.getActivePage() * byPageBandBox;
				pgSpec.setPageSize(byPageBandBox);
			       
			      }
			
				  List<SygSpecialite> specialite = BeanLocator.defaultLookup(SpecialiteSession.class).find(activePage, byPageBandBox,null,spec);
				   lstSpec.setModel(new SimpleListModel(specialite));
				   pgSpec.setTotalSize(BeanLocator.defaultLookup(SpecialiteSession.class).count(null,spec));
				        
		}	
       

	}
	
	

	/**
	 * Definit comment un element de la liste est affiche.
	 */
	
	
	public void onSelect$lstSpec(){
		 specialite = (SygSpecialite) lstSpec.getSelectedItem().getValue();
		 bSpec.setValue(specialite.getLibelle());
		
		 bSpec.close();
	}
	
	 public void onBlur$txtRechercherSpec()
	 {
	 if(txtRechercherSpec.getValue().equals(null))
		 txtRechercherSpec.setValue(Labels.getLabel("kermel.referentiel.nat.form.rechercher"));
	 }
		 
	public void onFocus$txtRechercherSpec(){
		 if(txtRechercherSpec.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.nat.form.rechercher"))){
			 txtRechercherSpec.setValue("");
		 }		 
	}
	
	public void  onClick$txtRechercherSpec(){
		if(txtRechercherSpec.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.nat.form.rechercher")))
		{
			spec = null;
			page = null;
		}
		else
		{
			spec = txtRechercherSpec.getValue();
			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}
	
	public void  onClick$btnRechercherSpec(){
		if(txtRechercherSpec.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.nat.form.rechercher")))
		{
			spec = null;
			page = null;
		}
		else
		{
			spec = txtRechercherSpec.getValue();
			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	

	public class SpecRenderer implements ListitemRenderer {
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygSpecialite sp= (SygSpecialite) data;
			item.setValue(sp);
			
			Listcell cellSp = new Listcell(sp.getLibelle());
			cellSp.setParent(item);

		}
	}
	
	public void onClick$bchercher()
	{
		onOK();
	}
	
	public void onOK()
	{
		raison= txtRaison.getValue();
		spec=bSpec.getValue();

	  if(raison.equalsIgnoreCase(Labels.getLabel("kermel.formateur.raisonsocial")))
			  {
		  raison="";
		     
		      }
	  Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
	
	public void onFocus$txtRaison()
	{
		if(txtRaison.getValue().equalsIgnoreCase(Labels.getLabel("kermel.formateur.raisonsocial")))
			txtRaison.setValue("");
		else
			raison=txtRaison.getValue();
	}
	
	
	public void onBlur$txtRaison()
	{
		if(txtRaison.getValue().equals(""))
			txtRaison.setValue(Labels.getLabel("kermel.formateur.raisonsocial"));
	}
	
	

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		 SygFormateur formateur = (SygFormateur) data;
		 item.setValue(formateur);
		 
		Listcell cellRais,cellAdd,cellTel,cellMail,cellSpec;
		
		
		if(formateur.getSociete()!=null) {
			cellRais = new Listcell(formateur.getSociete());
			cellRais.setParent(item);
		}
		else {
			cellRais = new Listcell("");
			cellRais.setParent(item);
		}
		
		if(formateur.getAdsociete()!=null) {
			cellAdd = new Listcell(formateur.getAdsociete());
			cellAdd.setParent(item);
		}
		else {
			cellAdd = new Listcell("");
			cellAdd.setParent(item);
		}
		
		
		if(formateur.getMailsociete()!=null) {
			cellMail = new Listcell(formateur.getMailsociete());
			cellMail.setParent(item);
		}
		else {
			cellMail = new Listcell("");
			cellMail.setParent(item);
		}
		
		if(formateur.getTelsociete()!=null) {
			cellTel = new Listcell(formateur.getTelsociete().toString());
			cellTel.setParent(item);
		}
		else {
			cellTel = new Listcell("");
			cellTel.setParent(item);
		}
		
		if(formateur.getSpecialite()!=null) {
			cellSpec = new Listcell(formateur.getSpecialite().getLibelle());
			cellSpec.setParent(item);
		}
		else {
			cellSpec = new Listcell("");
			cellSpec.setParent(item);
		}
		
	
	
	}  
}
