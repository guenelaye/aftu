package sn.ssi.kermel.web.referentiel.controllers;


import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygSpecialite;
import sn.ssi.kermel.be.referentiel.ejb.SpecialiteSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


public class FormSpecialiteController extends AbstractWindow implements
EventListener, AfterCompose{

    public static final String PARAM_WINDOW_CODE = "CODE";

	private Textbox txtLib,txtDesc,txtCod;
	private SygSpecialite spec =new SygSpecialite();
	
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String WINDOW_PARAM_MODE = "MODE";

    private String mode;
    Long code;
    Integer id;
    

   @Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
   		}
   	
   
	/**
	 * Permet de gerer les evenements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		
	}
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(WINDOW_PARAM_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			spec = (SygSpecialite) map.get(PARAM_WINDOW_CODE);

		
			txtCod.setValue(spec.getCode());
			txtLib.setValue(spec.getLibelle());
			txtDesc.setValue(spec.getDescription());
			
			} 
		}
	
	
	public void onClick$menuValider() {

		
		if(txtCod.getValue()==null || "".equals(txtCod.getValue())){
			throw new WrongValueException(txtCod, Labels.getLabel("kermel.ChamNull"));
		}
		
		if(txtLib.getValue()==null || "".equals(txtLib.getValue())){
			throw new WrongValueException(txtLib, Labels.getLabel("kermel.ChamNull"));
		}
		
		spec.setCode(txtCod.getValue());
		spec.setLibelle(txtLib.getValue());
		spec.setDescription(txtDesc.getValue());
	
		
		 if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)){
		BeanLocator.defaultLookup(SpecialiteSession.class).save(spec);
		    }
		    
		 else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)){
			BeanLocator.defaultLookup(SpecialiteSession.class).update(spec);

		}
		 loadApplicationState("list_specialite");
		 detach();
	}	

 public void  onClick$menuFermer(){
		
		 loadApplicationState("list_specialite");
		 detach();
		
	}
}
