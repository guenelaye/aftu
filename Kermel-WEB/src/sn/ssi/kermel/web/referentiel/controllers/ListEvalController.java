package sn.ssi.kermel.web.referentiel.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygEvaluateur;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteContractanteSession;
import sn.ssi.kermel.be.referentiel.ejb.EvaluateurSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class ListEvalController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstEval;
	private Paging pgPagination,pgAuto;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtNom,txtPrenom,txtFction;
    String nom=null,prenom=null,fonction=null,telephone=null,email=null,commentaire=null,page=null;
    
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_EVA, MOD_EVA, SUP_EVA;
    private String login;
    
    private	SygEvaluateur eval;
  private Textbox txtRechercherAuto;
	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private Listbox lstAuto;
	private String AutoDeno;
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode;
	Integer code;
	private SygAutoriteContractante autorite = null;
	private Utilisateur infoscompte;
	
	
	@Override
	public void afterCompose() {
		// TODO Auto-generated method stub
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_GESTIONEVAL);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent

		if (ADD_EVA != null) {
			ADD_EVA.addForward(ApplicationEvents.ON_CLICK, this,
					ApplicationEvents.ON_ADD);
		}
		addEventListener(ApplicationEvents.ON_ADD, this);

		if (MOD_EVA != null) {
			MOD_EVA.addForward(ApplicationEvents.ON_CLICK, this,
					ApplicationEvents.ON_EDIT);
		}
		if (SUP_EVA != null) {
			SUP_EVA.addForward(ApplicationEvents.ON_CLICK, this,
					ApplicationEvents.ON_DELETE);
		}

		// affichage des mess si on n a pas selectionner une ligne
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		lstEval.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		
		
		

	}
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygEvaluateur> eval = BeanLocator.defaultLookup(EvaluateurSession.class).find(activePage,byPage, nom,prenom,
					 autorite, telephone, email, fonction,commentaire);
			 SimpleListModel listModel = new SimpleListModel(eval);
			 lstEval.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup(EvaluateurSession.class).count(nom,prenom,autorite, telephone, 
					email, fonction,commentaire));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/referentiel/evaluateur/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(EvalFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstEval.getSelectedItem() == null)
				
				throw new WrongValueException(lstEval, Labels.getLabel("kermel.error.select.item"));
			final String uri = "/referentiel/evaluateur/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(EvalFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(TypeServiceFormController.PARAM_WINDOW_CODE, lstEval .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstEval.getSelectedItem() == null)
				
					throw new WrongValueException(lstEval, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.referentiel.eval.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, lstEval.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstEval.getSelectedCount(); i++) {
				Integer codes = (Integer)((Listitem) lstEval.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(EvaluateurSession.class).delete(codes);
				
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)){
				if (event.getData() != null)
				   {
					 
					activePage = Integer.parseInt((String) event.getData());
					byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
					pgAuto.setPageSize(byPageBandBox);	
				       } 
				    else {
					byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
					activePage = pgAuto.getActivePage() * byPageBandBox;
					pgAuto.setPageSize(byPageBandBox);
				       
				      }
				     List<SygAutoriteContractante> autorite = BeanLocator.defaultLookup(AutoriteContractanteSession.class).find(activePage, byPageBandBox, AutoDeno, null, null, null, null);
				     lstAuto.setModel(new SimpleListModel(autorite));
				     pgAuto.setTotalSize(BeanLocator.defaultLookup(AutoriteContractanteSession.class).count(AutoDeno,null,null, null, null));
				      
				    
			}
			
	}

	
	 public void onClick$bchercher()
	 {
		 onOk();
	 }
	 
	public void onOk()
	{
		nom=txtNom.getValue();
		prenom=txtPrenom.getValue();
		fonction=txtFction.getValue();
		
		
		if(!nom.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.eval.nom")))
		{
			if(prenom.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.eval.prenom")))
				prenom="";
			if(fonction.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.eval.fction")))
				fonction="";
		
		    Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this,page);
		}
		
		if(!prenom.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.eval.prenom")))
		{
			if(nom.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.eval.nom")))
				nom="";
			if(fonction.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.eval.fction")))
				fonction="";
			
		    Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
		
		if(!fonction.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.eval.fction")))
		{
			if(nom.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.eval.nom")))
				nom="";
			if(prenom.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.eval.prenom")))
		       prenom="";
			
		    Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
		

		if(nom.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.eval.nom")) && prenom.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.eval.prenom"))
				&& fonction.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.eval.fction")))
		{
			nom=""; 
			prenom="";
			fonction="";
		    AutoDeno=null;
		}
//		bautorite.setValue(Labels.getLabel("kermel.referentiel.eval.autorite"));
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	
	}
		

	
	
	//nom
	public void onFocus$txtNom()
	{
		if(txtNom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.eval.nom")))
			txtNom.setValue("");
		
	}
	public void onBlur$txtNom()
	{
		if(txtNom.getValue().equals(""))
			txtNom.setValue(Labels.getLabel("kermel.referentiel.eval.nom"));
	}

	public void onOK$txtNom()
	{
		onOk();
	}
	
	//prenom
	public void onFocus$txtPrenom()
	{
		if(txtPrenom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.eval.prenom")))
			txtPrenom.setValue("");
		
	}
	public void onBlur$txtPrenom()
	{
		if(txtPrenom.getValue().equals(""))
			txtPrenom.setValue(Labels.getLabel("kermel.referentiel.eval.prenom"));
	}

	public void onOK$txtPrenom()
	{
		onOk();
	}
	
	//fonction
	public void onFocus$txtFction()
	{
		if(txtFction.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.eval.fction")))
			txtFction.setValue("");
		
	}
	public void onBlur$txtFction()
	{
		if(txtFction.getValue().equals(""))
			txtFction.setValue(Labels.getLabel("kermel.referentiel.eval.fction"));
	}

	public void onOK$txtFction()
	{
		onOk();
	}

	
	
	 public void onBlur$txtRechercherAuto()
	 {
	 if(txtRechercherAuto.getValue().equals(null))
		 txtRechercherAuto.setValue(Labels.getLabel("kermel.referentiel.eval.form.rech"));
	 }
		 
	public void onFocus$txtRechercherAuto(){
		 if(txtRechercherAuto.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.eval.form.rech"))){
			 txtRechercherAuto.setValue("");
		 }		 
	}
	
	public void  onClick$txtRechercherAuto(){
		if(txtRechercherAuto.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.eval.form.rech")))
		{
			AutoDeno = null;
			page = null;
		}
		else
		{
			AutoDeno = txtRechercherAuto.getValue();
			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}
	
	public void  onClick$btnRechercherAuto(){
		if(txtRechercherAuto.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.eval.form.rech")))
		{
			AutoDeno = null;
			page = null;
		}
		else
		{
			AutoDeno = txtRechercherAuto.getValue();
			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	

	public class AutoRenderer implements ListitemRenderer {
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygAutoriteContractante autorite = (SygAutoriteContractante) data;
			item.setValue(autorite);
			
			Listcell cellDeno = new Listcell(autorite.getDenomination());
			cellDeno.setParent(item);

		}
	}
	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygEvaluateur eval = (SygEvaluateur) data;
		item.setValue(eval.getId());
		
		Listcell cellNom = new Listcell(eval.getNom());
		cellNom.setParent(item);
		Listcell cellPrenom = new Listcell(eval.getPrenom());
		cellPrenom.setParent(item);
	
		Listcell cellTel = new Listcell(eval.getTelephone());
		cellTel.setParent(item);
		Listcell cellEmail = new Listcell(eval.getEmail());
		cellEmail.setParent(item);
		Listcell cellFct = new Listcell(eval.getFonction());
		cellFct.setParent(item);
		Listcell cellCom = new Listcell(eval.getCommentaire());
		cellCom.setParent(item);
	}
}
