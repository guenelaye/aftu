package sn.ssi.kermel.web.referentiel.controllers;


import java.util.Map;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygCoordonateur;
import sn.ssi.kermel.be.entity.SygFormateur;
import sn.ssi.kermel.be.entity.SygLogisticien;
import sn.ssi.kermel.be.entity.SygModule;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class DetailsFormController extends AbstractWindow implements
		 AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	Long code;
	SygProformation formation=new SygProformation();
    private Label lblreference,lbllibelle,lbllieu,lblcible,lblmodule,lbleffectif,lblformateur,lblbudg,lblcommentaire,lblautorite,lblDateDebut,lblDateFin,lbltypeautorite;
    private Label lbllog,lblcoor;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	public static final String PARAM_WIDOW_CODE = "CODE";
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
			code = (Long) map.get(PARAM_WIDOW_CODE);
			formation = BeanLocator.defaultLookup(ProformationSession.class).findById(code);
			
//			SygAutoriteContractante autorite2 = (formation.getAutorite());
			SygModule module=(formation.getModule());
			SygFormateur formateur=(formation.getFormateur());
			//SygGroupeFormation groupe =formation.getGroupe();
			SygLogisticien logisticien= formation.getLogisticien();
			SygCoordonateur coordonnateur= formation.getCoordonnateur();
			
			if(formation.getForRef()!=null)
		     lblreference.setValue(formation.getForRef());
			if(formation.getForLibelle()!=null)
		     lbllibelle.setValue(formation.getForLibelle());
			if(formation.getForLieu()!=null)
		     lbllieu.setValue(formation.getForLieu());
			if(formation.getForCible()!=null)
		     lblcible.setValue(formation.getForCible());
		    if(module!=null)
		     lblmodule.setValue(module.getLibelle());
		    if(formation.getForDateDebut()!=null)
		     lblDateDebut.setValue(UtilVue.getInstance().formateLaDate(formation.getForDateDebut()));
		    if(formation.getForDateFin()!=null)
		     lblDateFin.setValue(UtilVue.getInstance().formateLaDate(formation.getForDateFin()));
		    if(formation.getBudg()!=null)
		     lblbudg.setValue(formation.getBudg().toString());
		    if(formation.getEffectif()!=null)
		     lbleffectif.setValue(formation.getEffectif().toString());
//		    if(formation.getGroupe()!=null)
//		      lblgroup.setValue(groupe.getLibelle());	
		    if(formation.getFormateur()!=null)
		     lblformateur.setValue(formateur.getPrenom()+" "+formateur.getNom());
		    if(formation.getCoordonnateur()!=null)
			  lblcoor.setValue(coordonnateur.getPrenom()+" "+coordonnateur.getNom());
		    if(formation.getLogisticien()!=null)
		     lbllog.setValue(logisticien.getPrenom()+" "+logisticien.getNom());
//		    if (autorite2 != null)
//				lblautorite.setValue(autorite2.getDenomination());
//			    lbltypeautorite.setValue(autorite2.getType().getLibelle());  
		    lblcommentaire.setValue(formation.getForCommentaire());
		   	
		}
			
}
	