package sn.ssi.kermel.web.referentiel.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAnnuaire;
import sn.ssi.kermel.be.referentiel.ejb.AnnuaireSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class ListAnnuaireController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtPrenom,txtNom,txtFonction,txtService;
    String nom=null,prenom=null,callBack,fonction=null,service=null,page=null;
    private Listheader lshPrenom,lshNom,lshFonction,lshService;
    Session session = getHttpSession();
    private Label lbltitre;
    String login;
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_ANNUAIREARMP, MOD_ANNUAIREARMP, SUPP_ANNUAIREARMP;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.ANNUAIREARMP);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_ANNUAIREARMP != null) { ADD_ANNUAIREARMP.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_ANNUAIREARMP != null) { MOD_ANNUAIREARMP.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUPP_ANNUAIREARMP != null) { SUPP_ANNUAIREARMP.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lshPrenom.setSortAscending(new FieldComparator("prenom", false));
		lshPrenom.setSortDescending(new FieldComparator("prenom", true));
		lshNom.setSortAscending(new FieldComparator("nom", false));
		lshNom.setSortDescending(new FieldComparator("nom", true));
		lshFonction.setSortAscending(new FieldComparator("fonction", false));
		lshFonction.setSortDescending(new FieldComparator("fonction", true));
		lshService.setSortAscending(new FieldComparator("service.libelle", false));
		lshService.setSortDescending(new FieldComparator("service.libelle", true));
		
		callBack=UIConstants.ANNUAIRE_ARMP;
		lbltitre.setValue(Labels.getLabel("kermel.annuaire.libelle.armp"));
			
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	login = ((String) getHttpSession().getAttribute("user"));
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygAnnuaire> annuaires = BeanLocator.defaultLookup(AnnuaireSession.class).find(activePage,byPage,prenom,nom,fonction,service,null,callBack);
			 SimpleListModel listModel = new SimpleListModel(annuaires);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup( AnnuaireSession.class).count(prenom,nom,fonction,service,null,callBack));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/referentiel/annuaire/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(AnnuaireFormController.PARAM_WINDOW_STATE, callBack);
			data.put(AnnuaireFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			final String uri = "/referentiel/annuaire/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(AnnuaireFormController.PARAM_WINDOW_STATE, callBack);
			data.put(AnnuaireFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(AnnuaireFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
				
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(AnnuaireSession.class).delete(codes);
				BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_ANNUAIREARMP", Labels.getLabel("kermel.annuaire.armp.suppression")+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygAnnuaire annuaire = (SygAnnuaire) data;
		item.setValue(annuaire.getId());

		 Listcell cellPrenom = new Listcell(annuaire.getPrenom());
		 cellPrenom.setParent(item);
		 
		 Listcell cellNom = new Listcell(annuaire.getNom());
		 cellNom.setParent(item);
		 
		 Listcell cellTelephone = new Listcell(annuaire.getTelephone());
		 cellTelephone.setParent(item);
		 
		 Listcell cellEmail = new Listcell(annuaire.getEmail());
		 cellEmail.setParent(item);
		 
		 Listcell cellFonction = new Listcell(annuaire.getFonction());
		 cellFonction.setParent(item);
		 
		 Listcell cellService = new Listcell(annuaire.getService().getLibelle());
		 cellService.setParent(item);


	}
	public void onClick$bchercher()
	{
		
		
		if((txtPrenom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.prenom")))||(txtPrenom.getValue().equals("")))
		 {
			prenom=null;
			page=null;
		 }
		else
		{
			prenom=txtPrenom.getValue();
			page="0";
		}
		if((txtNom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.nom")))||(txtNom.getValue().equals("")))
		 {
			nom=null;
			page=null;
		 }
		else
		{
			nom=txtNom.getValue();
			page="0";
		}
		if((txtFonction.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.fonction")))||(txtFonction.getValue().equals("")))
		 {
			fonction=null;
			page=null;
		 }
		else
		{
			fonction=txtFonction.getValue();
			page="0";
		}
		if((txtService.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.service")))||(txtService.getValue().equals("")))
		 {
			service=null;
			page=null;
		 }
		else
		{
			service=txtService.getValue();
			page="0";
		}
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	
	public void onFocus$txtPrenom()
	{
		if(txtPrenom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.prenom")))
			txtPrenom.setValue("");
		
	}
	public void onBlur$txtPrenom()
	{
		if(txtPrenom.getValue().equals(""))
			txtPrenom.setValue(Labels.getLabel("kermel.common.form.prenom"));
	}
	
	public void onOK$txtPrenom()
	{
		onClick$bchercher();
	}
	public void onBlur$txtNom()
	{
		if(txtNom.getValue().equals(""))
			txtNom.setValue(Labels.getLabel("kermel.common.form.nom"));
	}
	public void onFocus$txtNom()
	{
		if(txtNom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.nom")))
			txtNom.setValue("");
	
	}
	public void onOK$txtCode()
	{
		onClick$bchercher();
	}
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
	public void onFocus$txtFonction()
	{
		if(txtFonction.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.fonction")))
			txtFonction.setValue("");
		
	}
	public void onBlur$txtFonction()
	{
		if(txtFonction.getValue().equals(""))
			txtFonction.setValue(Labels.getLabel("kermel.common.form.fonction"));
	}
	
	public void onOK$txtFonction()
	{
		onClick$bchercher();
	}
	public void onFocus$txtService()
	{
		if(txtService.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.service")))
			txtService.setValue("");
		
	}
	public void onBlur$txtService()
	{
		if(txtService.getValue().equals(""))
			txtService.setValue(Labels.getLabel("kermel.common.form.service"));
	}
	
	public void onOK$txtService()
	{
		onClick$bchercher();
	}
}