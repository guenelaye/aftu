package sn.ssi.kermel.web.referentiel.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Include;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.West;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygCategorieLegislation;
import sn.ssi.kermel.be.referentiel.ejb.CategorieLegislationSession;
import sn.ssi.kermel.be.referentiel.ejb.FichierLegislationSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class TreeCategorieLegislationController extends AbstractWindow implements
		AfterCompose, IdSpace,EventListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Textbox txtSearch;
	private Include content;
	private Combobox listCategory;
	private Tree tree;
	private Treechildren child = new Treechildren();
	private Session session = getHttpSession();
	private SygCategorieLegislation categorie = null;
		private String openWest;
	private West wp;
	private String libelle;
	
	@SuppressWarnings("unchecked")
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);

	   /*	region = (SiapDecoupage1) Executions.getCurrent().getAttribute(
				UIConstants.REGION);
		*/
		openWest = (String) Executions.getCurrent().getAttribute("openWest");
		wp.setOpen(true);
		

		txtSearch.setValue("Recherche");
		initTree();
		if (categorie != null) {
			//System.out.println("gggggggggggggggggggggggggggg##########################"+categorie.getLibelle());
			wp.setOpen(true);
		}

	}

	private void initTree() {
		child.setParent(tree);
		treeProcedureBuilder();
		content.setSrc("/legislation/list.zul");
		content.setHeight("600px");
	}

	public void onSelect$tree() {
		tree.isInvalidated();
		Object selobj = tree.getSelectedItem().getValue();

		session.setAttribute("selObj", selobj);
		content.invalidate();
		if (selobj instanceof SygCategorieLegislation) {

			List<String> status = new ArrayList<String>();
			categorie = (SygCategorieLegislation) selobj;
			session.setAttribute("categorie",categorie);
			content.setSrc("/legislation/list.zul");

		} 
		
			
			
	}

	@SuppressWarnings("unchecked")
	public void onSelect$listCategory() {
		// txtSearch.setDisabled(true);
		//
		// txtSearch.setDisabled(false);
		// txtSearch.setValue("");
		//
		// List<String> status = new ArrayList<String>();
		// status.add(UIConstants.PUBLISHED);
		//
		// if(listCategory.getSelectedItem().getValue().toString().equalsIgnoreCase(UIConstants.PROCEDURE)){
		//
		// if(session.getAttribute("category_mode") != null &&
		// !session.getAttribute("category_mode").equals(UIConstants.PROCESS)){
		// removeTreeChildren();
		// //treeProcedureBuilder();
		// chargerRegion();
		// }
		//
		// session.setAttribute("category_mode", UIConstants.PROCEDURE);
		//
		// } else
		// if(listCategory.getSelectedItem().getValue().toString().equalsIgnoreCase(UIConstants.ETAPE)){
		// session.setAttribute("category_mode", UIConstants.ETAPE);
		//
		// treeEtapeBuilder();
		//
		// } else
		// if(listCategory.getSelectedItem().getValue().toString().equalsIgnoreCase(UIConstants.TACHE)){
		// session.setAttribute("category_mode", UIConstants.TACHE);
		//
		// treeTacheBuilder();
		//
		// } else
		// if(listCategory.getSelectedItem().getValue().toString().equalsIgnoreCase(UIConstants.OPERATION)){
		// session.setAttribute("category_mode", UIConstants.OPERATION);
		//
		// treeOperationBuilder();
		//
		// }

	}

	// public void onOK$txtSearch(){
	// search();
	// }

	public void onClick$search_img() {
		//search();
		treeProcedureBuilder();
	}

	@SuppressWarnings("unchecked")
	public void search() {
		//
		// List<String> status = new ArrayList<String>();
		// status.add(UIConstants.PUBLISHED);
		//
		// removeTreeChildren();
		//
		// if(listCategory.getSelectedItem().getValue().toString().equalsIgnoreCase(UIConstants.PROCEDURE)){
		// session.setAttribute("category_mode", UIConstants.PROCEDURE);
		//
		// //treeProcedureBuilder();
		// chargerRegion();
		//
		// } else
		// if(listCategory.getSelectedItem().getValue().toString().equalsIgnoreCase(UIConstants.ETAPE)){
		// session.setAttribute("category_mode", UIConstants.ETAPE);
		//
		// treeEtapeBuilder();
		//
		// } else
		// if(listCategory.getSelectedItem().getValue().toString().equalsIgnoreCase(UIConstants.TACHE)){
		// session.setAttribute("category_mode", UIConstants.TACHE);
		//
		// treeTacheBuilder();
		//
		// } else
		// if(listCategory.getSelectedItem().getValue().toString().equalsIgnoreCase(UIConstants.OPERATION)){
		// session.setAttribute("category_mode", UIConstants.OPERATION);
		//
		// treeOperationBuilder();
		//
		// }

	}

	public void treeBuilder() {
		// String key = null;
		//
		// if(!"".equals(txtSearch.getValue()) &&
		// !txtSearch.getValue().equals("Recherche"))
		// key = txtSearch.getValue();
		//
		// List<String> status = new ArrayList<String>();
		// status.add(UIConstants.PUBLISHED);
		//
		// List<OhProcess> listProcess =
		// BeanLocator.defaultLookup(ProcessSession.class).findProcessesByStatut(key,
		// status);
		//
		// List<OhProcedure> listProcedure =
		// BeanLocator.defaultLookup(ProcedureSession.class).findAll(null,
		// status);
		//
		// if(listProcedure.size() > 0){
		//
		// for (OhProcedure ohProcedure : listProcedure) {
		// Treeitem procedureItem = new Treeitem();
		//
		// procedureItem.setLabel(ohProcedure.getProLibelle());
		// procedureItem.setTooltiptext(ohProcedure.getProLibelle());
		//
		// procedureItem.setImage("g");
		// procedureItem.setValue(ohProcedure);
		// procedureItem.setOpen(false);
		//
		// procedureItem.setParent(child);
		//
		// List <OhEtape> listEtapes =
		// BeanLocator.defaultLookup(ProcedureSession.class).findEtapesByProcedure(ohProcedure.getProID(),
		// null, status);
		//
		// if(listEtapes.size() > 0){
		//
		// Treechildren procedureTreechildren = new Treechildren();
		// for (OhEtape ohEtape : listEtapes) {
		// Treeitem etapeItem = new Treeitem();
		//
		// etapeItem.setLabel(ohEtape.getEtaLibelle());
		// etapeItem.setTooltiptext(ohEtape.getEtaLibelle());
		//
		//
		// etapeItem.setValue(ohEtape);
		// etapeItem.setOpen(false);
		//
		// etapeItem.setParent(procedureTreechildren);
		//
		// List <OhTache> listTaches =
		// BeanLocator.defaultLookup(EtapeSession.class).findTachesByEtape(ohEtape.getEtaID(),
		// null, status);
		// if(listTaches.size() > 0){
		// Treechildren etapeTreechildren = new Treechildren();
		// for (OhTache ohTache : listTaches) {
		// Treeitem tacheItem = new Treeitem();
		//
		// tacheItem.setLabel(ohTache.getTacLibelle());
		// tacheItem.setTooltiptext(ohTache.getTacLibelle());
		//
		// tacheItem.setImage("g");
		// tacheItem.setValue(ohTache);
		// tacheItem.setOpen(false);
		//
		// tacheItem.setParent(etapeTreechildren);
		//
		// List <OhOperation> listOperations =
		// BeanLocator.defaultLookup(TacheSession.class).findOperationsByTache(ohTache.getTacID(),
		// null, status, null);
		// if(listOperations.size() > 0){
		// Treechildren tacheTreechildren = new Treechildren();
		//
		// for (OhOperation ohOperation : listOperations) {
		// Treeitem operationItem = new Treeitem();
		//
		// operationItem.setLabel(ohOperation.getOpLibelle());
		// operationItem.setTooltiptext(ohOperation.getOpLibelle());
		//
		// operationItem.setImage("g");
		// operationItem.setValue(ohOperation);
		// operationItem.setOpen(false);
		//
		// operationItem.setParent(tacheTreechildren);
		// }
		// tacheTreechildren.setParent(tacheItem);
		// }
		// }
		// etapeTreechildren.setParent(etapeItem);
		// }
		// }
		// procedureTreechildren.setParent(procedureItem);
		// }
		// }
		// }
	}

	public void treeProcedureBuilder() {

		 String key = null;
		/*
		 if(!"".equals(txtSearch.getValue()) &&
		 !txtSearch.getValue().equals("Recherche"))
		 key = txtSearch.getValue();
		  */
		/*
		 List<String> status = new ArrayList<String>();
		 status.add(UIConstants.PUBLISHED);
		   */
		 List<SygCategorieLegislation> listProcedure = BeanLocator.defaultLookup(
					CategorieLegislationSession.class).find(0, -1,null, libelle);
			System.out.println("==============================ok");
		int i=0;	
		 for (SygCategorieLegislation ohProcedure : listProcedure) {
		    i++;
		 Treeitem procedureItem = new Treeitem();
		 
		int nb=BeanLocator.defaultLookup( FichierLegislationSession.class).countALL(ohProcedure,null);

		 procedureItem.setLabel(ohProcedure.getLibelle()+" ("+nb+")");
		 procedureItem.setTooltiptext(ohProcedure.getLibelle());
		
	   	 procedureItem.setImage("/images/arrow.png");
		 procedureItem.setValue(ohProcedure);
		 procedureItem.setOpen(false);
		
		 procedureItem.setParent(child);
		 if(i==1){
			 if((SygCategorieLegislation)session.getAttribute("categorie")!=null){} else{
		 session.setAttribute("categorie",ohProcedure);
			 }
		 }
			
		
				 
		 List<SygCategorieLegislation> listEtapes = new ArrayList<SygCategorieLegislation>();
			/* BeanLocator.defaultLookup(
					DSession.class)
					.findbyRegion(0, -1, ohProcedure.getId()); */
			
		 if(listEtapes.size() > 0){
		 Treechildren procedureTreechildren = new Treechildren();
		 for (SygCategorieLegislation ohEtape : listEtapes) {
		 Treeitem etapeItem = new Treeitem();
		
		 etapeItem.setLabel(ohEtape.getLibelle());
		 etapeItem.setTooltiptext(ohEtape.getLibelle());
		
		etapeItem.setImage("/images/arrow.png");
		
		 etapeItem.setValue(ohEtape);
		 etapeItem.setOpen(false);
		
		 etapeItem.setParent(procedureTreechildren);
		
			
		 List <SygCategorieLegislation> listTaches = new ArrayList<SygCategorieLegislation>();
		 //BeanLocator.defaultLookup(PostePluvioSession.class).findbyDepartement(ohEtape.getId());
		//	System.out.println("==============================ok"+listTaches.size());
			
		 if(listTaches.size() > 0){
		 Treechildren etapeTreechildren = new Treechildren();
		 for (SygCategorieLegislation ohTache : listTaches) {
		 Treeitem tacheItem = new Treeitem();
		
		 tacheItem.setLabel(ohTache.getLibelle());
		 tacheItem.setTooltiptext(ohTache.getLibelle());
		
		// tacheItem.setImage("g");
		 tacheItem.setValue(ohTache);
		 tacheItem.setOpen(false);
		
		 tacheItem.setParent(etapeTreechildren);
		
		 List <SygCategorieLegislation> listOperations = new ArrayList<SygCategorieLegislation>();
		// BeanLocator.defaultLookup(PostePluvioSession.class).findAll();
		 if(listOperations.size() > 0){
		 Treechildren tacheTreechildren = new Treechildren();
		   
		/* for (SiapPostePluvio ohOperation : listOperations) {
		 Treeitem operationItem = new Treeitem();
		
		 operationItem.setLabel(ohOperation.getLibelle());
		 operationItem.setTooltiptext(ohOperation.getLibelle());
		
		// operationItem.setImage("g");
		 operationItem.setValue(ohOperation);
		
		 operationItem.setParent(tacheTreechildren);
		 }  */   
		  
		// tacheTreechildren.setParent(tacheItem);
		   }    
		 }
		 etapeTreechildren.setParent(etapeItem);
		 }
		 }
		 procedureTreechildren.setParent(procedureItem);
		 }
		 }
	}

	public void treeEtapeBuilder() {
		// String key = null;
		//
		// if(!"".equals(txtSearch.getValue()) &&
		// !txtSearch.getValue().equals("Recherche"))
		// key = txtSearch.getValue();
		//
		// List<String> status = new ArrayList<String>();
		// status.add(UIConstants.PUBLISHED);
		//
		// removeTreeChildren();
		//
		// List <OhEtape> listEtapes =
		// BeanLocator.defaultLookup(EtapeSession.class).findAll(key, status);
		//
		// if(listEtapes.size() > 0){
		//
		// for (OhEtape ohEtape : listEtapes) {
		// Treeitem etapeItem = new Treeitem();
		//
		// etapeItem.setLabel(ohEtape.getEtaLibelle());
		// etapeItem.setTooltiptext(ohEtape.getEtaLibelle());
		//
		// etapeItem.setImage("g");
		//
		// etapeItem.setValue(ohEtape);
		// etapeItem.setOpen(false);
		//
		// etapeItem.setParent(child);
		//
		// List <OhTache> listTaches =
		// BeanLocator.defaultLookup(EtapeSession.class).findTachesByEtape(ohEtape.getEtaID(),
		// null, status);
		// if(listTaches.size() > 0){
		// Treechildren etapeTreechildren = new Treechildren();
		// for (OhTache ohTache : listTaches) {
		// Treeitem tacheItem = new Treeitem();
		//
		// tacheItem.setLabel(ohTache.getTacLibelle());
		// tacheItem.setTooltiptext(ohTache.getTacLibelle());
		//
		// tacheItem.setImage("g");
		// tacheItem.setValue(ohTache);
		// tacheItem.setOpen(false);
		//
		// tacheItem.setParent(etapeTreechildren);
		//
		// List <OhOperation> listOperations =
		// BeanLocator.defaultLookup(TacheSession.class).findOperationsByTache(ohTache.getTacID(),
		// null, status, null);
		// if(listOperations.size() > 0){
		// Treechildren tacheTreechildren = new Treechildren();
		//
		// for (OhOperation ohOperation : listOperations) {
		// Treeitem operationItem = new Treeitem();
		//
		// operationItem.setLabel(ohOperation.getOpLibelle());
		// operationItem.setTooltiptext(ohOperation.getOpLibelle());
		//
		// operationItem.setImage("g");
		// operationItem.setValue(ohOperation);
		//
		// operationItem.setParent(tacheTreechildren);
		// }
		// tacheTreechildren.setParent(tacheItem);
		// }
		// }
		// etapeTreechildren.setParent(etapeItem);
		// }
		// }
		// }
	}

	public void treeTacheBuilder() {
		// String key = null;
		//
		// if(!"".equals(txtSearch.getValue()) &&
		// !txtSearch.getValue().equals("Recherche"))
		// key = txtSearch.getValue();
		//
		// List<String> status = new ArrayList<String>();
		// status.add(UIConstants.PUBLISHED);
		//
		// removeTreeChildren();
		//
		// List <OhTache> listTaches =
		// BeanLocator.defaultLookup(TacheSession.class).findAll(key, status);
		// if(listTaches.size() > 0){
		//
		// for (OhTache ohTache : listTaches) {
		// Treeitem tacheItem = new Treeitem();
		//
		// tacheItem.setLabel(ohTache.getTacLibelle());
		// tacheItem.setTooltiptext(ohTache.getTacLibelle());
		//
		// tacheItem.setImage("g");
		// tacheItem.setValue(ohTache);
		// tacheItem.setOpen(false);
		//
		// tacheItem.setParent(child);
		//
		// List <OhOperation> listOperations =
		// BeanLocator.defaultLookup(TacheSession.class).findOperationsByTache(ohTache.getTacID(),
		// null, status, null);
		// if(listOperations.size() > 0){
		// Treechildren tacheTreechildren = new Treechildren();
		//
		// for (OhOperation ohOperation : listOperations) {
		// Treeitem operationItem = new Treeitem();
		//
		// operationItem.setLabel(ohOperation.getOpLibelle());
		// operationItem.setTooltiptext(ohOperation.getOpLibelle());
		//
		// operationItem.setImage("g");
		// operationItem.setValue(ohOperation);
		//
		// operationItem.setParent(tacheTreechildren);
		// }
		// tacheTreechildren.setParent(tacheItem);
		// }
		// }
		// }
	}

	public void treeOperationBuilder() {
		// String key = null;
		//
		// if(!"".equals(txtSearch.getValue()) &&
		// !txtSearch.getValue().equals("Recherche"))
		// key = txtSearch.getValue();
		//
		// List<String> status = new ArrayList<String>();
		// status.add(UIConstants.PUBLISHED);
		//
		// removeTreeChildren();
		//
		// List <OhOperation> listOperations =
		// BeanLocator.defaultLookup(OperationSession.class).findAll(key,
		// status, null);
		// if(listOperations.size() > 0){
		//
		// for (OhOperation ohOperation : listOperations) {
		// Treeitem operationItem = new Treeitem();
		//
		// operationItem.setLabel(ohOperation.getOpLibelle());
		// operationItem.setTooltiptext(ohOperation.getOpLibelle());
		//
		//
		// operationItem.setValue(ohOperation);
		//
		//
		// operationItem.setParent(child);
		// }
		// }
	}

	public void removeTreeChildren() {
		while (child.getItemCount() > 0) {
			child.removeChild(child.getFirstChild());
		}
	}

	
	

	public void chargerTache(Long etaID, List<String> status, String index) {
		// try {
		// List <OhTache> listTaches =
		// BeanLocator.defaultLookup(EtapeSession.class).findTachesByEtape(etaID,
		// null, status);
		// if(listTaches.size() > 0){
		// Treechildren etapeTreechildren = new Treechildren();
		// for (OhTache ohTache : listTaches) {
		// Treeitem tacheItem = new Treeitem();
		// tacheItem.setId("t@ch_"+ohTache.getTacID());
		// tacheItem.setLabel(ohTache.getTacLibelle());
		// tacheItem.setTooltiptext(ohTache.getTacLibelle());
		//
		//
		// tacheItem.setValue(ohTache);
		// tacheItem.setOpen(false);
		//
		// tacheItem.setParent(etapeTreechildren);
		// }
		// etapeTreechildren.setParent((Component) child.getFellowIfAny(index));
		// }
		// } catch (Exception e) {
		// // afterCompose();
		// }
		//
	}

	public void chargerOperation(Long tacID, List<String> status, String idc) {
		// try {
		// List <OhOperation> listOperations =
		// BeanLocator.defaultLookup(TacheSession.class).findOperationsByTache(tacID,
		// null, status, null);
		// if(listOperations.size() > 0){
		// Treechildren tacheTreechildren = new Treechildren();
		//
		// for (OhOperation ohOperation : listOperations) {
		// Treeitem operationItem = new Treeitem();
		//
		// operationItem.setLabel(ohOperation.getOpLibelle());
		// operationItem.setTooltiptext(ohOperation.getOpLibelle());
		//
		//
		// operationItem.setValue(ohOperation);
		//
		// operationItem.setParent(tacheTreechildren);
		// }
		// tacheTreechildren.setParent((Component)child.getFellowIfAny(idc));
		// }
		// } catch (Exception e) {
		// //afterCompose();
		// }
	}

	public void onOpen$tree() {

		System.out.println(tree.getSelectedItem().getId());
	}

	public void onFocus$txtSearch() {
		if (txtSearch.getValue().equalsIgnoreCase("Recherche")) {
			txtSearch.setValue(null);
		}
		
	}

	public void onBlur$txtSearch() {
		if (txtSearch.getValue().equalsIgnoreCase("")
				|| (txtSearch.getValue().equalsIgnoreCase(null))) {
			txtSearch.setValue("Recherche");
		}
	}

	public void onOK$txtSearch() {
		if (txtSearch.getValue() != null
				&& !txtSearch.getValue().trim().equals("")) {
			libelle = txtSearch.getValue();
		} else {
			libelle = null;
		}
		chargerSearch();
		//treeProcedureBuilder();
	}

	public void chargerSearch() {
		removeTreeChildren();
	//	chargerRegion();

	}

	public void onClick$onAdd()
	{
		System.out.println("BOOOOOOOOOOOOOOOOOOOOOOOOOOOOOMMMMMMMMMMMMMMMMMMMM");
		Events.postEvent(ApplicationEvents.ON_ADD, this, null);
	}

	@Override
	public void onEvent(Event event) throws Exception {
		// TODO Auto-generated method stub
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			
			loadApplicationState("list_texte_reglementaire");
		}else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/legislation/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, "Nouvelle Categorie");
			display.put(DSP_HEIGHT,"300px");
			display.put(DSP_WIDTH, "50%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(CodeMarcheFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		
	}
	
	//public void 
	

	
	
}