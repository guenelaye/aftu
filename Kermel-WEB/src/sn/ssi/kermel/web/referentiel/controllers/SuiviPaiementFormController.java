package sn.ssi.kermel.web.referentiel.controllers;


import java.util.Date;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Doublebox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCalendrierPaiement;
import sn.ssi.kermel.be.entity.SygCatFournisseur;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDemandePaiement;
import sn.ssi.kermel.be.entity.SygPenaliteRetard;
import sn.ssi.kermel.be.entity.SygSuiviPaiement;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.ContratSession;
import sn.ssi.kermel.be.session.CalendrierPaiementSession;
import sn.ssi.kermel.be.session.DemandePaiementSession;
import sn.ssi.kermel.be.session.PenaliteRetardSession;
import sn.ssi.kermel.be.session.SuiviPaiementSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class SuiviPaiementFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtNom, txtAdresse, txtEmail, txtTel, txtFax;
	Long code;
	private	SygDemandePaiement demande=new SygDemandePaiement();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Bandbox bdCategorie;
	private Paging pgCategorie;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE,activePage;
	private String libellecategorie = null, page=null;
	private Listbox lstCategorie;
	private Textbox txtRechercherCategorie,txtRef,txtlivrable;
	SygCatFournisseur categorie=null;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private Datebox  datePaiement,dateCourrier;
	private Doublebox  montant;
	Session session = getHttpSession();
	private SygContrats contrat;
	private Radio  rdoui,rdnon;
	private Datebox datepenalisation;
	private Textbox txtObservation;
	private Intbox  tauxpenalite;
	private Label lbltaux,lbldatepenalite,lblObservation,somme,pc;
	private SygSuiviPaiement  suivi= new SygSuiviPaiement();
	private SygPenaliteRetard  penalite= new  SygPenaliteRetard();


	@Override
	public void onEvent(Event event) throws Exception {
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)){
//			if (event.getData() != null) {
//				activePage = Integer.parseInt((String) event.getData());
//				byPageBandbox = -1;
//				pgCategorie.setPageSize(1000);
//			} else {
//				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
//				activePage = pgCategorie.getActivePage() * byPageBandbox;
//				pgCategorie.setPageSize(byPageBandbox);
//			}
			/*List<SygCatFournisseur> categorie = BeanLocator.defaultLookup(CatFourSession.class).find(activePage, byPageBandbox,libellecategorie);
			lstCategorie.setModel(new SimpleListModel(categorie));
			pgCategorie.setTotalSize(BeanLocator.defaultLookup(CatFourSession.class).count(libellecategorie));
		     */
		}

	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		/*
		lstCategorie.setItemRenderer(new CategoriesRenderer());
		pgCategorie.setPageSize(byPageBandbox);
		pgCategorie.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		 */
		 Long idcontrat = (Long) session.getAttribute("contrat");
		   contrat=BeanLocator.defaultLookup(ContratSession.class).findById(idcontrat);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			demande = BeanLocator.defaultLookup(DemandePaiementSession.class).findById(code);
			
			/*datePaiement.setValue(demande.getDatepaiement());
			txtRef.setValue(demande.getRef());
			dateCourrier.setValue(demande.getDateCourrier());
			montant.setValue(demande.getMontantpaye());
			txtlivrable.setValue(demande.getLivrable());
			
			*/
			montant.setValue(demande.getMontantpaye());
			datePaiement.setValue(new Date());
			datepenalisation.setValue(new Date());
			
			if(BeanLocator.defaultLookup(PenaliteRetardSession.class).findPenalite(demande.getDemPaId())!=null){
				penalite=BeanLocator.defaultLookup(PenaliteRetardSession.class).findPenalite(demande.getDemPaId());
				rdoui.setChecked(true);
				rdnon.setDisabled(true);
				tauxpenalite.setValue(penalite.getTaux());
				somme.setValue( "Soit - "+ToolKermel.formatDecimal(penalite.getMontantpenalite()));
				txtObservation.setValue(penalite.getObservation());
				lbldatepenalite.setVisible(true);
				datepenalisation.setVisible(true);
				lbltaux.setVisible(true);
				tauxpenalite.setVisible(true);
				lblObservation.setVisible(true);
				
				txtObservation.setVisible(true);
				somme.setVisible(true);
				pc.setVisible(true);
			}
			
			
		}
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			suivi.setDateSuivi(datePaiement.getValue());
		     suivi.setMontantpaye(montant.getValue());
			 suivi.setDemandePaiement(demande);
			
			if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)){ 
				//demande.setSuivi("non");
			//	BeanLocator.defaultLookup(DemandePaiementSession.class).save(demande);
			}
	
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
		        demande.setSuivi("oui");
		        SygCalendrierPaiement  calen= BeanLocator.defaultLookup(CalendrierPaiementSession.class).findById(demande.getCalendrier().getCalenpaId());
		        calen.setPayer("oui");
		        BeanLocator.defaultLookup(CalendrierPaiementSession.class).update(calen);
		        BeanLocator.defaultLookup(DemandePaiementSession.class).update(demande);
				suivi=BeanLocator.defaultLookup(SuiviPaiementSession.class).saveSuivi(suivi);
				
				if(rdoui.isChecked()==true){
				    
					penalite.setDatepenalite(datepenalisation.getValue());
					penalite.setTaux(tauxpenalite.getValue());
					penalite.setMontantpenalite(Double.valueOf(((tauxpenalite.getValue()* demande.getMontantpaye())/100)));
					penalite.setObservation(txtObservation.getValue());
					penalite.setDemande(demande);
			
			if(BeanLocator.defaultLookup(PenaliteRetardSession.class).findPenalite(demande.getDemPaId())!=null)
			{
			   BeanLocator.defaultLookup(PenaliteRetardSession.class).update(penalite);
			}else
			 BeanLocator.defaultLookup(PenaliteRetardSession.class).save(penalite);
				}
				
			}
			//	BeanLocator.defaultLookup(DemandePaiementSession.class).update(demande);
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {		

//			if(datePaiement.getValue()==null)
//		     {
//				errorComponent = datePaiement;
//              	errorMsg = Labels.getLabel("kermel.common.form.nom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
//				lbStatusBar.setStyle(ERROR_MSG_STYLE);
//				lbStatusBar.setValue(errorMsg);
//				throw new WrongValueException (errorComponent, errorMsg);
//		     }
//			
//			if(txtRef.getValue().equals(""))
//		     {
//				errorComponent = txtRef;
//             	errorMsg = Labels.getLabel("kermel.importateur.adresse")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
//				lbStatusBar.setStyle(ERROR_MSG_STYLE);
//				lbStatusBar.setValue(errorMsg);
//				throw new WrongValueException (errorComponent, errorMsg);
//		     }
//
//			if(dateCourrier.getValue()==null)
//		     {
//				errorComponent = dateCourrier;
//             	errorMsg = Labels.getLabel("kermel.referentiel.common.code.categorie")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
//				lbStatusBar.setStyle(ERROR_MSG_STYLE);
//				lbStatusBar.setValue(errorMsg);
//				throw new WrongValueException (errorComponent, errorMsg);
//		     }
//
//			
			if(montant.getValue()==null)
		     {
				errorComponent = montant;
            	errorMsg = Labels.getLabel("kermel.referentiel.common.code.categorie")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }

			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	

		///////////Categorie///////// 
		public void onSelect$lstCategorie(){
			categorie= (SygCatFournisseur) lstCategorie.getSelectedItem().getValue();
			bdCategorie.setValue(categorie.getLibelle());
			bdCategorie.close();
		}
		
		public void onCheck$rdoui(){
			
			//System.out.println("######################################OKOKOKOKO");
			lbldatepenalite.setVisible(true);
			datepenalisation.setVisible(true);
			lbltaux.setVisible(true);
			tauxpenalite.setVisible(true);
			lblObservation.setVisible(true);
			txtObservation.setVisible(true);
			somme.setVisible(true);
			pc.setVisible(true);
		}
		
         public void onCheck$rdnon(){
			
			//System.out.println("######################################OKOKOKOKO");
			lbldatepenalite.setVisible(false);
			datepenalisation.setVisible(false);
			lbltaux.setVisible(false);
			tauxpenalite.setVisible(false);
			lblObservation.setVisible(false);
			txtObservation.setVisible(false);
			 montant.setValue(demande.getMontantpaye());
			 tauxpenalite.setValue(null);
			 somme.setVisible(false);
			 somme.setValue("");
			 pc.setVisible(false);
			 
		}
		
         public void onBlur$tauxpenalite(){
//    		 if(txtProduit.getValue().equalsIgnoreCase(Labels.getLabel(""))){
//    			 txtProduit.setValue("");
//    		 }	
    		 if (tauxpenalite.getValue()!=null ){ 
    		// System.out.print("################################"+taux.getValue());
    		 if (tauxpenalite.getValue()>30){
    				 throw new WrongValueException(tauxpenalite, " le taux doit �tre entre  0 et 30");
    		 }else
    			 somme.setValue("soit - "+ String.valueOf( String.valueOf((tauxpenalite.getValue()* demande.getMontantpaye())/100)));
    		
    		 montant.setValue( demande.getMontantpaye()-((tauxpenalite.getValue()* demande.getMontantpaye())/100));
    				 
    		 }
    	 }
         
		
		public class CategoriesRenderer implements ListitemRenderer{
			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
				SygCatFournisseur categorie = (SygCatFournisseur) data;
				item.setValue(categorie);
				
				Listcell cellLibelle = new Listcell("");
				if (categorie.getLibelle()!=null){
					cellLibelle.setLabel(categorie.getLibelle());
				}
				cellLibelle.setParent(item);
		
			}
		}
		
		public void onFocus$txtRechercherCategorie(){
		if(txtRechercherCategorie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.code.categorie"))){
			txtRechercherCategorie.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherCategorie(){
		if(txtRechercherCategorie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.code.categorie")) || txtRechercherCategorie.getValue().equals("")){
			libellecategorie = null;
			page=null;
		}
		else{
			libellecategorie = txtRechercherCategorie.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
}
