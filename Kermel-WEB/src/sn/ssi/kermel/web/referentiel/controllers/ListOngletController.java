package sn.ssi.kermel.web.referentiel.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.session.QuestionSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


@SuppressWarnings("serial")
public class ListOngletController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtObjet,txtRef;
    String libelle=null,page=null,code=null,login;
    private Listheader lshObjet,lshRef;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem  TRAITER_REPONSE, WMODP_TYPESMARCHES;
    SygDossiers  dossier;
    private Include  includePage1,includePage2;
    private Tab   page1,page2; 
    private Label   lblRef,lblObjet,lblpublier,lblNbr;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		//dossiers
		dossier=(SygDossiers) getHttpSession().getAttribute("dossier");
		lblRef.setValue(dossier.getAppel().getAporeference());
		lblObjet.setValue(dossier.getAppel().getApoobjet());
		lblpublier.setValue(ToolKermel.dateToString(dossier.getDosDatePublication()));
		lblNbr.setValue("");
		
		
		
		
		
    	login = ((String) getHttpSession().getAttribute("user"));
    	page1.setLabel("Questions en attentes ("+  BeanLocator.defaultLookup(QuestionSession.class).find(dossier).size()+ ")");
    	page2.setLabel("Questions repondues ("+ BeanLocator.defaultLookup(QuestionSession.class).findRep(dossier).size()+")");
    	
    	
    	
    	includePage1.setSrc("/reponse/listQuestion.zul");
    	
    	includePage2.setSrc("/reponse/listReponse.zul");
	}

	 
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		
	}


	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub
		
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	
	public void onClose(){
		
		loadApplicationState("liste_reponse");
	}
}