package sn.ssi.kermel.web.referentiel.controllers;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygTypeFormateur;
import sn.ssi.kermel.be.referentiel.ejb.FormateurSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeFormateurSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


public class AccueilFormateurController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";

	private Tab TAB_FORMIND,TAB_FORMCAB;
	Session session = getHttpSession();
	private SygTypeFormateur type;
	private Long idtype;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
//		
//		idType=(Long) session.getAttribute("type");
//		type=BeanLocator.defaultLookup(TypeFormateurSession.class).findById(idType);

	}

	
	public void onCreate(CreateEvent createEvent) {
		
		TAB_FORMIND.setLabel(Labels.getLabel("kermel.common.form.formateurind")+" ("+BeanLocator.defaultLookup(FormateurSession.class).count( null, null, null,null,BeanLocator.defaultLookup(TypeFormateurSession.class).findById(1L))+")");
		TAB_FORMCAB.setLabel(Labels.getLabel("kermel.common.form.formateurcab")+" ("+BeanLocator.defaultLookup(FormateurSession.class).count( null, null, null,null,BeanLocator.defaultLookup(TypeFormateurSession.class).findById(2L))+")");
		idtype=(Long) session.getAttribute("type");
		if(idtype!=null)
		{
			if(idtype.intValue()==1)
			{
				Include inc = (Include) this.getFellowIfAny("incFORMIND");
		        inc.setSrc("/formateur/liste.zul");	
			}
			else
			{
				Include inc = (Include) this.getFellowIfAny("incFORMCAB");
		        inc.setSrc("/formateur/liste.zul");	
		        TAB_FORMCAB.setSelected(true);
			}
			
		}
		else
		{
			Include inc = (Include) this.getFellowIfAny("incFORMIND");
			session.setAttribute("type",new Long(1));
	        inc.setSrc("/formateur/liste.zul");	
		}
	
		    
	
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	

}
