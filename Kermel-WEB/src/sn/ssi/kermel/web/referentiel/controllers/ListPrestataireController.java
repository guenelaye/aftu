package sn.ssi.kermel.web.referentiel.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygPays;
import sn.ssi.kermel.be.entity.SygPrestataire;
import sn.ssi.kermel.be.referentiel.ejb.PaysSession;
import sn.ssi.kermel.be.referentiel.ejb.PrestataireSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;



@SuppressWarnings("serial")
public class ListPrestataireController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox list;
	private Paging pg;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private String sygmnumero,sygmraisonsociale,sygmidentifiant;
	private Listheader lshnumero,lshraisonsociale,lshadresse,lshmail,lshcommentaire,lshidentifiant,lshpays;
	private Textbox txtnumero,txtraisonsociale,txtadresse,txtmail,txtcommentaire,txtninea,txtidentifiant;
	private Menuitem ADD_PRESTATAIRE, MOD_PRESTATAIRE, SUP_PRESTATAIRE,WDETAIL_PRESTATAIRE;
	Long code;
	private KermelSousMenu monSousMenu;
    private Session session = getHttpSession();
	 

	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_PRESTATAIRES);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		Components.wireFellows(this, this);
		if (ADD_PRESTATAIRE != null)
			ADD_PRESTATAIRE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
		if (MOD_PRESTATAIRE != null)
			MOD_PRESTATAIRE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
		if (SUP_PRESTATAIRE != null)
			SUP_PRESTATAIRE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
		if (WDETAIL_PRESTATAIRE != null)
			WDETAIL_PRESTATAIRE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DETAILS);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	
//		lshidentifiant.setSortAscending(new FieldComparator("ninea", false));
//		lshidentifiant.setSortDescending(new FieldComparator("ninea", true));
//		lshraisonsociale.setSortAscending(new FieldComparator("raisonsociale", false));
//		lshraisonsociale.setSortDescending(new FieldComparator("raisonsociale", true));
//		lshadresse.setSortAscending(new FieldComparator("adresse", false));
//		lshadresse.setSortDescending(new FieldComparator("adresse", true));
//		lshmail.setSortAscending(new FieldComparator("mail", false));
//		lshmail.setSortDescending(new FieldComparator("mail", true));
//		lshpays.setSortAscending(new FieldComparator("idpays", false));
//		lshpays.setSortDescending(new FieldComparator("idpays", true));
//		lshcommentaire.setSortAscending(new FieldComparator("commentaire", false));
//		lshcommentaire.setSortDescending(new FieldComparator("commentaire", true));

	
 
		list.setItemRenderer(this);

		pg.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pg.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 List<SygPrestataire> prestataire = BeanLocator.defaultLookup(PrestataireSession.class).find(pg.getActivePage()*byPage,byPage,sygmnumero,sygmraisonsociale,sygmidentifiant,null,null,null);
			 SimpleListModel listModel = new SimpleListModel(prestataire);
			list.setModel(listModel);
			pg.setTotalSize(BeanLocator.defaultLookup(
				PrestataireSession.class).count(null,null,null,null,null,null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/referentiel/prestataire/formprestataire.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.repertoireauditeur"));
			display.put(DSP_HEIGHT,"600px");
			display.put(DSP_WIDTH, "90%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(PrestataireFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/referentiel/prestataire/formprestataire.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"600px");
			display.put(DSP_WIDTH, "90%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(PrestataireFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(PrestataireFormController.PARAM_WIDOW_CODE, list
					.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				// de
				// fixer
				// les
				// dimenisons
				// du
				// popup
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// de
				// passer
				// des
				// parametres
				// au
				// popup
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < list.getSelectedCount(); i++) {
					Long codes = (Long)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					System.out.println(codes);
				BeanLocator.defaultLookup(PrestataireSession.class).delete(codes);
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DETAILS)) {
	
				if(list.getSelectedItem()==null)
				{
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				}
				else
				{
					final String uri = "/referentiel/prestataire/formdetails.zul";
					final HashMap<String, String> display = new HashMap<String, String>();
					display.put(DSP_HEIGHT,"600px");
					display.put(DSP_WIDTH, "80%");
					display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.repertoireauditeur"));

					final HashMap<String, Object> data = new HashMap<String, Object>();
					data.put(PrestataireFormController.WINDOW_PARAM_MODE,
							UIConstants.MODE_EDIT);
					data.put(PrestataireFormController.PARAM_WIDOW_CODE, list
							.getSelectedItem().getValue());

					showPopupWindow(uri, data, display);
				}
            }
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygPrestataire prestataire = (SygPrestataire) data;
		item.setValue(prestataire.getIdprestataire());
//		Listcell cellidentifiant = new Listcell(prestataire.getIdentifiant());
//		cellidentifiant.setParent(item);
		Listcell cellraisonsociale = new Listcell(prestataire.getRaisonsociale());
		cellraisonsociale.setParent(item);
		Listcell celladresse = new Listcell(prestataire.getAdresse());
		celladresse.setParent(item);
		Listcell cellmail = new Listcell(prestataire.getMail());
		cellmail.setParent(item);
		
		Listcell cellpays = new Listcell(""); 
		if(prestataire.getPays()!=null){
			SygPays pays = BeanLocator.defaultLookup(PaysSession.class).findById(prestataire.getPays().getIdpays());
			if(pays!=null){
				cellpays = new Listcell(pays.getLibelle()); 
			}
			
		}
		cellpays.setParent(item);
		
		Listcell cellcommentaire = new Listcell(prestataire.getCommentaire());
		cellcommentaire.setParent(item);
	
		 
	}
	public void onOK$txtnumero() {
		onClick$btnRechercher();
	}
	public void onOK$txtidentifiant() {
		onClick$btnRechercher();
	}
	public void onOK$txtraisonsociale() {
		onClick$btnRechercher();
	}
	
	public void onClick$btnRechercher() {
		
		
		if (txtnumero.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.numero")) || txtnumero.getValue().equals("")) {
			sygmnumero = null;
		} else {
			sygmnumero  = txtnumero.getValue();
		}	
		if (txtidentifiant.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.identifiant")) || txtidentifiant.getValue().equals("")) {
			sygmidentifiant = null;
		} else {
			sygmidentifiant  = txtidentifiant.getValue();
		}

		if (txtraisonsociale.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.raisonsociale")) || txtraisonsociale.getValue().equals("")) {
			sygmraisonsociale = null;
		} else {
			sygmraisonsociale  = txtraisonsociale.getValue();
		}
		

 
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
			
			
			public void onFocus$txtnumero() {
				if (txtnumero.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.numero"))) {
					txtnumero.setValue("");

				}
		}
			public void onFocus$txtidentifiant() {
				if (txtidentifiant.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.identifiant"))) {
					txtidentifiant.setValue("");
		
				}
		  }
			public void onFocus$txtraisonsociale() {
				if (txtraisonsociale.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.raisonsociale"))) {
					txtraisonsociale.setValue("");

				}
		}
			
			public void onBlur$txtnumero() {
				if (txtnumero.getValue().equalsIgnoreCase("")) {
					txtnumero.setValue(Labels.getLabel("kermel.common.form.numero"));
				}	
		}

			public void onBlur$txtidentifiant() {
				if (txtidentifiant.getValue().equalsIgnoreCase("")) {
					txtidentifiant.setValue(Labels.getLabel("kermel.common.form.identifiant"));
				}	
		}
				public void onBlur$txtraisonsociale() {
					if (txtraisonsociale.getValue().equalsIgnoreCase("")) {
						txtraisonsociale.setValue(Labels.getLabel("kermel.referentiel.common.raisonsociale"));
					}	
			}
				
			
				

}