package sn.ssi.kermel.web.referentiel.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteContractanteSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeAutoriteContractanteSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;


@SuppressWarnings("serial")
public class AutoritesContractantesController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtDenomination,txtRechercherType;
    String denomination=null,page=null,libelle=null;
    private Listheader lshType,lshLibelle;
    Session session = getHttpSession();
    SygTypeAutoriteContractante type=null;
    private Long idtype;
    private Label lblType;
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_AUTORITEAUTC, MOD_AUTORITEAUTC, SUPP_AUTORITEAUTC;
    String login;
    private Bandbox bdType;
    private Listbox lstType;
    private Paging pgType;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_AUTORITEAUTC);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		
		if (ADD_AUTORITEAUTC != null) { ADD_AUTORITEAUTC.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_AUTORITEAUTC != null) { MOD_AUTORITEAUTC.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUPP_AUTORITEAUTC != null) { SUPP_AUTORITEAUTC.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_CLOSE, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		lshLibelle.setSortAscending(new FieldComparator("libelle", false));
		lshLibelle.setSortDescending(new FieldComparator("libelle", true));
		lshType.setSortAscending(new FieldComparator("type.libelle", false));
		lshType.setSortDescending(new FieldComparator("type.libelle", true));
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
		
		addEventListener(ApplicationEvents.ON_TYPE_AUTORITES, this);
		lstType.setItemRenderer(new TypesRenderer());
		pgType.setPageSize(byPageBandbox);
		pgType.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_TYPE_AUTORITES);
		Events.postEvent(ApplicationEvents.ON_TYPE_AUTORITES, this, null);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		login = ((String) getHttpSession().getAttribute("user"));
		
		
		
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_TYPE_AUTORITES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgType.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgType.getActivePage() * byPageBandbox;
				pgType.setPageSize(byPageBandbox);
			}
			List<SygTypeAutoriteContractante> types = BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).find(activePage, byPageBandbox, null, libelle);
			lstType.setModel(new SimpleListModel(types));
			pgType.setTotalSize(BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).count(null, libelle));
		}
		else	if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygAutoriteContractante> autorites = BeanLocator.defaultLookup(AutoriteContractanteSession.class).find(activePage,byPage,denomination,null,type, null, null);
			 SimpleListModel listModel = new SimpleListModel(autorites);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup( AutoriteContractanteSession.class).count(denomination,null,type, null, null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/referentiel/typeautorite/ajoutautorite.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"650px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(AutoriteContractanteFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			final String uri = "/referentiel/typeautorite/ajoutautorite.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"650px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(AutoriteContractanteFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(AutoriteContractanteFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
	
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
				
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(AutoriteContractanteSession.class).delete(codes);
				BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_AC", Labels.getLabel("kermel.referentiel.common.autorite.suppression")+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	
			
	}


	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygAutoriteContractante autorite = (SygAutoriteContractante) data;
		item.setValue(autorite.getId());

		 Listcell cellLibelle = new Listcell(autorite.getDenomination());
		 cellLibelle.setParent(item);
		 
		 Listcell cellType = new Listcell(autorite.getType().getLibelle());
		 cellType.setParent(item);
		 
		
		 
		

	}
	public void onClick$bchercher()
	{
		
		if((txtDenomination.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.denomination")))||(txtDenomination.getValue().equals("")))
		 {
			denomination=null;
			
		 }
		else
		{
			denomination=txtDenomination.getValue();
			page="0";
		}
		
		if(bdType.getValue().equals(""))
			type=null;
		else
		{
			page="0";
		}
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	public void onFocus$txtDenomination()
	{
		if(txtDenomination.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.denomination")))
			txtDenomination.setValue("");
	
	}
	public void onBlur$txtDenomination()
	{
		if(txtDenomination.getValue().equals(""))
			txtDenomination.setValue(Labels.getLabel("kermel.referentiel.common.denomination"));
	}
	
	
	public void onOK$txtDenomination()
	{
		onClick$bchercher();
	}
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
	
	public void onClick$menuFermer()
	{
		loadApplicationState("referentiel");
	}
	public class TypesRenderer implements ListitemRenderer{
		
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypeAutoriteContractante types = (SygTypeAutoriteContractante) data;
			item.setValue(types);
			
			Listcell cellLibelle = new Listcell(types.getLibelle());
			cellLibelle.setParent(item);
		}
	}
	
	public void onSelect$lstType(){
		type= (SygTypeAutoriteContractante) lstType.getSelectedItem().getValue();
		bdType.setValue(type.getLibelle());
		bdType.close();
	
	}
	
	public void onFocus$btnRechercherType(){
		if(txtRechercherType.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.marche.Type"))){
			txtRechercherType.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherService(){
			String page;
		if(txtRechercherType.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.marche.Type")) || txtRechercherType.getValue().equals("")){
			libelle = null;
			page=null;
		}else{
			libelle= txtRechercherType.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_TYPE_AUTORITES, this, page);
		}
}