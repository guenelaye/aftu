package sn.ssi.kermel.web.referentiel.controllers;

import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygConfigurationDatesRealisations;
import sn.ssi.kermel.be.plansdepassation.ejb.ConfigurationDatesRealisationsSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


@SuppressWarnings("serial")
public class ConfigurationDatesRealisationsTFSController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
    private Div step0,step1,step2;
    SygConfigurationDatesRealisations configuration=null;
    private Intbox dureea,dureeb,dureec,dureed,dureee,dureef,dureeastep2,dureebstep2,dureecstep2,dureedstep2;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		lstListe.setItemRenderer(this);
	 	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    
	}

	
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			
			 List<SygConfigurationDatesRealisations> configurations = BeanLocator.defaultLookup(ConfigurationDatesRealisationsSession.class).find(null,null,null,"TFS");
			 SimpleListModel listModel = new SimpleListModel(configurations);
			 lstListe.setModel(listModel);
			
		} 
		

	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygConfigurationDatesRealisations configurations = (SygConfigurationDatesRealisations) data;
		item.setValue(configurations);

			 
		 Listcell cellCode = new Listcell(configurations.getMode().getLibelle());
		 cellCode.setParent(item);
		 
		 Listcell cellExamendncmp = new Listcell(configurations.getExamendncmp());
		 cellExamendncmp.setParent(item);
		 
			 
		 Listcell cellExamenccmp = new Listcell(configurations.getExamenccmp());
		 cellExamenccmp.setParent(item);
		 
		
	}
	public void onClick$menuConfigurer()
	{
		
		if (lstListe.getSelectedItem() == null)
			
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		
		configuration=(SygConfigurationDatesRealisations) lstListe.getSelectedItem().getValue();
		if(configuration.getMode().getId().intValue()==1)
			{
				dureea.setValue(configuration.getDureea());
				dureeb.setValue(configuration.getDureeb());
				dureec.setValue(configuration.getDureec());
				dureed.setValue(configuration.getDureed());
				dureee.setValue(configuration.getDureee());
				dureef.setValue(configuration.getDureef());
				
				step0.setVisible(false);
				step1.setVisible(true);
				step2.setVisible(false);
			}
		else
		    {
			
				dureeastep2.setValue(configuration.getDureea());
				dureebstep2.setValue(configuration.getDureeb());
				dureecstep2.setValue(configuration.getDureec());
				dureedstep2.setValue(configuration.getDureed());
				
				step0.setVisible(false);
				step1.setVisible(false);
				step2.setVisible(true);
		    }
			
		
		
	}
	public void onClick$menuFermer()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
	}
	public void onClick$menuFermerstep2()
	{
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
	}
	
	public void onClick$menuValider()
	{
		configuration.setDureea(dureea.getValue());
		configuration.setDureeb(dureeb.getValue());
		configuration.setDureec(dureec.getValue());
		configuration.setDureed(dureed.getValue());
		configuration.setDureee(dureee.getValue());
		configuration.setDureef(dureef.getValue());
		
		BeanLocator.defaultLookup(ConfigurationDatesRealisationsSession.class).update(configuration);
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	public void onClick$menuValiderstep2()
	{
		configuration.setDureea(dureeastep2.getValue());
		configuration.setDureeb(dureebstep2.getValue());
		configuration.setDureec(dureecstep2.getValue());
		configuration.setDureed(dureedstep2.getValue());
		
		BeanLocator.defaultLookup(ConfigurationDatesRealisationsSession.class).update(configuration);
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
}