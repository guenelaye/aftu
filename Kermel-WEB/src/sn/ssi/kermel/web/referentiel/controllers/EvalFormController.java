package sn.ssi.kermel.web.referentiel.controllers;

import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygEvaluateur;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.EvaluateurSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class EvalFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtNom,txtPrenom,txtTel,txtMail,txtCom,txtFction;
	Integer code;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	private	SygEvaluateur eval=new SygEvaluateur();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private String page = null;
	private SygAutoriteContractante autorite = null;
	private Utilisateur infoscompte;


	@Override
	public void afterCompose() {
		// TODO Auto-generated method stub
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
	}

	@Override
	public void onEvent(Event event) throws Exception {
		// TODO Auto-generated method stub
		
			   

		
			
	}
	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Integer) map.get(PARAM_WINDOW_CODE);
			eval = BeanLocator.defaultLookup(EvaluateurSession.class).findById(code);
			txtNom.setValue(eval.getNom());
			txtPrenom.setValue(eval.getPrenom());
			txtTel.setValue(eval.getTelephone());
			txtMail.setValue(eval.getEmail());
			txtFction.setValue(eval.getFonction());
			txtCom.setValue(eval.getCommentaire());
			
		}
	}
	
	public void onOK() {
		
		eval.setNom(txtNom.getValue());
		eval.setPrenom(txtPrenom.getValue());
		eval.setTelephone(txtTel.getValue());
		eval.setEmail(txtMail.getValue());
		eval.setFonction(txtFction.getValue());
		eval.setCommentaire(txtCom.getValue());
		eval.setAutorite(autorite);
		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW))
			BeanLocator.defaultLookup(EvaluateurSession.class).save(eval);
		
		else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT))
			BeanLocator.defaultLookup(EvaluateurSession.class).update(eval);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();
	}	
	
	

}

	