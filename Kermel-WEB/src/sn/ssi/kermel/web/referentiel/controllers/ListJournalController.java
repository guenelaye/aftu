package sn.ssi.kermel.web.referentiel.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygJournal;
import sn.ssi.kermel.be.referentiel.ejb.JournalMSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;


@SuppressWarnings("serial")
public class ListJournalController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null;
    private Listheader lshLibelle;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_JOURNAL, MOD_JOURNAL, DEL_JOURNAL;
    private String login;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_JOURNAL);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_JOURNAL != null) { ADD_JOURNAL.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_JOURNAL != null) { MOD_JOURNAL.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (DEL_JOURNAL != null) { DEL_JOURNAL.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		lshLibelle.setSortAscending(new FieldComparator("libelle", false));
		lshLibelle.setSortDescending(new FieldComparator("libelle", true));
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	login = ((String) getHttpSession().getAttribute("user"));
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygJournal> types = BeanLocator.defaultLookup(JournalMSession.class).find(activePage,byPage,null,null,null,null);
			 SimpleListModel listModel = new SimpleListModel(types);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup( JournalMSession.class).count(null,null,null,null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/referentiel/journal/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.referentiel.journal.libelle"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(TypeServiceFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			final String uri = "/referentiel/journal/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(TypeServiceFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(TypeServiceFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
				
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(JournalMSession.class).delete(codes);
			//	BeanLocator.defaultLookup(JournalMSession.class).logAction("SUPP_TYPESERVICE", Labels.getLabel("kermel.referentiel.common.typeservice.suppression")+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) {

				if (lstListe.getSelectedItem() == null) {
					throw new WrongValueException(lstListe, "Veuillez s�lectionner un �l�ment svp!");
				} else {
					if (lstListe.getSelectedCount() > 1) {
						throw new WrongValueException(lstListe, "Veuillez s�lectionner un seul �l�ment svp!");
					} else {

						String uri = null;

						HashMap<String, String> display = null;
						HashMap<String, Object> data = null;

						uri = "/referentiel/journal/doc.zul";

						data = new HashMap<String, Object>();
						display = new HashMap<String, String>();
						String nomFichier;
						// if
						// (list.getSelectedItem().getAttribute("codePJ").toString().equals()){
						String codes = ((Listitem) lstListe.getSelectedItems().toArray()[0]).getValue().toString();
						// String toDo =
						// list.getSelectedItem().getAttribute(UIConstants.TODO).toString();
						// BeanLocator.defaultLookup(CarriereSession.class).delete(Long.parseLong(codes));
						// Long code = (Long) ((HashMap<String,
						// Object>)event.getData()).get(CURRENT);
						// String toDo = (String) ((HashMap<String, Object>)
						// event.getData()).get(UIConstants.TODO);

						// if (toDo != null && toDo.equalsIgnoreCase("VIEW")) {
						nomFichier = (String) BeanLocator.defaultLookup(JournalMSession.class).findById(Long.parseLong(codes)).getFichierJournal();
						// } else {
						// nomFichier = (String)
						// BeanLocator.defaultLookup(DemandeEmploiSession.class).findById(Long.parseLong(codes)).getLettremotiv();

						// }
						data.put(DownloaDocsController.NOM_FICHIER, nomFichier);

						display.put(DSP_TITLE, "Journal des march�s pubics");
						display.put(DSP_WIDTH, "70%");
						display.put(DSP_HEIGHT, "700px");

						showPopupWindow(uri, data, display);
					}
				}
			}
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygJournal type = (SygJournal) data;
		item.setValue(type.getIdJournal());

		 Listcell cellNum = new Listcell(type.getNumero());
		 cellNum.setParent(item);
		 Listcell cellLibelle = new Listcell(type.getDescription());
		 cellLibelle.setParent(item);
		 Listcell cellDatePub = new Listcell(type.getDatePub().toString());
		 cellDatePub.setParent(item);
		 
		 Listcell  cellCv = new Listcell();
			cellCv.setImage("/images/PaperClip-16x16.png");
			cellCv.setTooltiptext("Voir Piece");
			cellCv.setAttribute(UIConstants.TODO, "VIEW");
			cellCv.addEventListener(Events.ON_CLICK, this);
			session.setAttribute("nomFichier", type.getFichierJournal());
			cellCv.setAttribute("codePJ", type.getIdJournal());
			cellCv.setParent(item);
		 
		

	}
	public void onClick$bchercher()
	{
		
		if((txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.banque.libelle")))||(txtLibelle.getValue().equals("")))
		 {
			libelle=null;
			page=null;
		 }
		else
		{
			libelle=txtLibelle.getValue();
			page="0";
		}
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	
	public void onFocus$txtLibelle()
	{
		if(txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.banque.libelle")))
			txtLibelle.setValue("");
		
	}
	
	public void onBlur$txtLibelle()
	{
		if(txtLibelle.getValue().equals(""))
			txtLibelle.setValue(Labels.getLabel("kermel.referentiel.banque.libelle"));
	}

	public void onOK$txtLibelle()
	{
		onClick$bchercher();
	}
	
	
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
}