package sn.ssi.kermel.web.referentiel.controllers;


import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygFormationAutorite;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteContractanteSession;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteFormationSession;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeAutoriteContractanteSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


@SuppressWarnings("serial")
public class FormFormationAutoriteController  extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode;
	public static final String WINDOW_PARAM_MODE = "MODE";
  
	private Textbox txtRechercherAutorite;
	private Paging pg,pgAutorite;
	Long code;

	private Listbox lstAutorite, list;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE, activePage;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private Label entete1;
	
	private String libelleautorite = null;

	private SygTypeAutoriteContractante typeautorite;
	private SygAutoriteContractante autorite;
	private SygProformation formation;
	private SygFormationAutorite fauto;
	private Div step0, step1;
	private String errorMsg;
	private Component errorComponent;
	private String mod, nom, prenom, page = null,libgroupe,nlogisticien,ncoordonnateur;

	 private Integer idmois,idannee;
	 Session session = getHttpSession(); 

	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			code = (Long) map.get(PARAM_WIDOW_CODE);
			formation = BeanLocator.defaultLookup(ProformationSession.class)
					.findById(code);
		}
	
	}

	public void onOK() {
		
			
		fauto= new SygFormationAutorite();
		
		if (list.getSelectedItem() == null)
			throw new WrongValueException(list, Labels
					.getLabel("kermel.error.select.item"));

		typeautorite = (SygTypeAutoriteContractante) lstAutorite
				.getSelectedItem().getValue();
		autorite = (SygAutoriteContractante) list.getSelectedItem().getValue();
		fauto.setAutorite(autorite);
		fauto.setType(typeautorite);
		fauto.setFormation(formation);
		
		BeanLocator.defaultLookup(AutoriteFormationSession.class).save(fauto);
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION1, getOpener(), null);
		detach();

	}

	

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);



		addEventListener(ApplicationEvents.ON_TYPE_AUTORITES, this);
		lstAutorite.setItemRenderer(new TypeAutoritesRenderer());
		pgAutorite.setPageSize(byPage);
		pgAutorite.addForward(ApplicationEvents.ON_PAGING, this,
				ApplicationEvents.ON_TYPE_AUTORITES);
		addEventListener(ApplicationEvents.ON_AUTORITES, this);

		
		
		list.setItemRenderer(new AutoritesRenderer());
		pg.setPageSize(byPage);
		pg.addForward(ApplicationEvents.ON_PAGING, this,
				ApplicationEvents.ON_AUTORITES);
		
		Events.postEvent(ApplicationEvents.ON_TYPE_AUTORITES, this, null);	
		
	}

	public void onClick$menuNext() {
		if (lstAutorite.getSelectedItem() == null)
			throw new WrongValueException(lstAutorite, Labels
					.getLabel("kermel.error.select.item"));

		typeautorite = (SygTypeAutoriteContractante) lstAutorite
				.getSelectedItem().getValue();
		entete1.setValue(typeautorite.getLibelle());
		step0.setVisible(false);
		step1.setVisible(true);
		Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);

	}



	public void onClick$menuPrevious() {
		if (lstAutorite.getSelectedItem() == null)
			throw new WrongValueException(lstAutorite, Labels
					.getLabel("kermel.error.select.item"));

		typeautorite = (SygTypeAutoriteContractante) lstAutorite
				.getSelectedItem().getValue();
		step0.setVisible(true);
		step1.setVisible(false);
		Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);

	}

	
	public void onClick$btnRechercherAutorite()
	{
		if (txtRechercherAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.typeAutoritecontractante")) || txtRechercherAutorite.getValue().equals("")) {
			libelleautorite = null;
		} else
		{
			
		libelleautorite = txtRechercherAutorite.getValue();
		
		}
		Events.postEvent(ApplicationEvents.ON_TYPE_AUTORITES, this, null);
		
		}
	
	public void onFocus$txtRechercherAutorite()
	{
		if(txtRechercherAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.autoritecontractante.libelle")))
			txtRechercherAutorite.setValue("");
		else
			libelleautorite=txtRechercherAutorite.getValue();
	}
	
	
	public void onBlur$txtRechercherAutorite()
	{
		if(txtRechercherAutorite.getValue().equals(""))
			txtRechercherAutorite.setValue(Labels.getLabel("kermel.autoritecontractante.libelle"));
	}
	
	

	public void onClick$menuFermer() {
		detach();
	}

	public void onClick$menuFermer1() {
		detach();
	}

	public class TypeAutoritesRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypeAutoriteContractante autorites = (SygTypeAutoriteContractante) data;
			item.setValue(autorites);

			Listcell cellLibelle = new Listcell(autorites.getLibelle());
			cellLibelle.setParent(item);
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				if (autorites.getId().equals(
						fauto.getAutorite().getType().getId()))
					item.setSelected(true);

			}

		}
	}

	public class AutoritesRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygAutoriteContractante autorites = (SygAutoriteContractante) data;
			item.setValue(autorites);

			Listcell cellCode = new Listcell(autorites.getSigle());
			cellCode.setParent(item);

			Listcell cellLibelle = new Listcell(autorites.getDenomination());
			cellLibelle.setParent(item);

			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				if (autorites.getId().equals(fauto.getAutorite().getId()))
					item.setSelected(true);

			}
		}
	}

	@Override
	public void onEvent(Event event) throws Exception {
		// TODO Auto-generated method stub

		if (event.getName().equalsIgnoreCase(
				ApplicationEvents.ON_TYPE_AUTORITES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgAutorite.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgAutorite.getActivePage() * byPage;
				pgAutorite.setPageSize(byPage);
			}
			List<SygTypeAutoriteContractante> autorites = BeanLocator
					.defaultLookup(TypeAutoriteContractanteSession.class).find(
							activePage, byPage, null, libelleautorite);
			lstAutorite.setModel(new SimpleListModel(autorites));
			pgAutorite.setTotalSize(BeanLocator.defaultLookup(
					TypeAutoriteContractanteSession.class).count(null,
					libelleautorite));
		}

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTORITES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgAutorite.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgAutorite.getActivePage() * byPage;
				pgAutorite.setPageSize(byPage);
			}
			List<SygAutoriteContractante> autorites = BeanLocator
					.defaultLookup(AutoriteContractanteSession.class).find(
							activePage, byPage, libelleautorite, null,
							typeautorite, null, null);
			list.setModel(new SimpleListModel(autorites));
			pg.setTotalSize(BeanLocator.defaultLookup(
					AutoriteContractanteSession.class).count(libelleautorite,
					null, null, null, null));
		}
		

	}

	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub
		
	}

	
	}
