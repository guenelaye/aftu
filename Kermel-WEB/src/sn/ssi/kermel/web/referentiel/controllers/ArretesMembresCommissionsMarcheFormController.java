package sn.ssi.kermel.web.referentiel.controllers;


import java.util.Calendar;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygArretesMembresCommissionsMarches;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.MembresCommissionsMarchesSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;




@SuppressWarnings("serial")
public class ArretesMembresCommissionsMarcheFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String codelocalite,annee,gestion;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox  txtReference,txtVersionElectronique,txtReferencemembre,txtVersionElectroniquemembre;
	private SygArretesMembresCommissionsMarches arrete =new SygArretesMembresCommissionsMarches();
	private SygArretesMembresCommissionsMarches arretecommission =new SygArretesMembresCommissionsMarches();
	private SimpleListModel lst;
	Long code;
	private Checkbox cbResponsable;
	private int nombre=0;
	private String nomFichier,nomFichiermembre;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private SygAutoriteContractante autorite = null;
	private Utilisateur infoscompte;
	private Datebox dtdate,dtdatemembre;
	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		gestion = (String) map.get(PARAM_WINDOW_MODE);
		
		arretecommission= BeanLocator.defaultLookup(MembresCommissionsMarchesSession.class).findArrete(autorite, gestion);
		
		if(arretecommission!=null)
		{
			arrete=arretecommission;
			txtReference.setValue(arrete.getReference());
			dtdate.setValue(arrete.getDate());
			txtReferencemembre.setValue(arrete.getReferencemembre());
			dtdatemembre.setValue(arrete.getDatemembre());
		}
				
			
	}
		  

	public void onOK() {
		if(checkFieldConstraints())
		{
			
			arrete.setReference(txtReference.getValue());
			arrete.setDate(dtdate.getValue());
			arrete.setGestion(gestion);
			arrete.setAutorite(autorite);
			arrete.setReferencemembre(txtReferencemembre.getValue());
			arrete.setDatemembre(dtdatemembre.getValue());
			if(!txtVersionElectronique.getValue().equals(""))
				arrete.setFichier(txtVersionElectronique.getValue());
			if(!txtVersionElectroniquemembre.getValue().equals(""))
				arrete.setFichiermembre(txtVersionElectroniquemembre.getValue());
			if (arretecommission==null)
				BeanLocator.defaultLookup(MembresCommissionsMarchesSession.class).saveArrete(arrete);
	    	else
				BeanLocator.defaultLookup(MembresCommissionsMarchesSession.class).updateArrete(arrete);
		
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}

	}
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		
	}
	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onEvent(Event arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	

	
		
			
	 public void onClick$btnChoixFichier() {
				if (ToolKermel.isWindows())
					nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
				else
					nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

				txtVersionElectronique.setValue(nomFichier);
			}
		
	 public void onClick$btnChoixFichiermembre() {
			if (ToolKermel.isWindows())
				nomFichiermembre = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichiermembre = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectroniquemembre.setValue(nomFichiermembre);
		}
	 private boolean checkFieldConstraints() {
			
			try {
				if(txtReference.getValue().equals(""))
			     {
	              errorComponent = txtReference;
	              errorMsg = Labels.getLabel("kermel.referentiel.courrier.reference")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				   lbStatusBar.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar.setValue(errorMsg);
				   throw new WrongValueException (errorComponent, errorMsg);
			     }
				if(dtdate.getValue().equals(""))
			     {
	              errorComponent = dtdate;
	              errorMsg = Labels.getLabel("kermel.referentiel.devises.date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				   lbStatusBar.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar.setValue(errorMsg);
				   throw new WrongValueException (errorComponent, errorMsg);
			     }
				if(txtReferencemembre.getValue().equals(""))
			     {
	              errorComponent = txtReferencemembre;
	              errorMsg = Labels.getLabel("kermel.referentiel.courrier.reference")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				   lbStatusBar.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar.setValue(errorMsg);
				   throw new WrongValueException (errorComponent, errorMsg);
			     }
				if(dtdatemembre.getValue().equals(""))
			     {
	              errorComponent = dtdatemembre;
	              errorMsg = Labels.getLabel("kermel.referentiel.devises.date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				   lbStatusBar.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar.setValue(errorMsg);
				   throw new WrongValueException (errorComponent, errorMsg);
			     }
				return true;
					
			}
			catch (Exception e) {
				errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
				+ " [checkFieldConstraints]";
				errorComponent = null;
				return false;

				
			}
			
		}
		

}