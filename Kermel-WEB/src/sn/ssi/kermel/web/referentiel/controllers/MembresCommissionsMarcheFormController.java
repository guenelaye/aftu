package sn.ssi.kermel.web.referentiel.controllers;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.zkoss.util.media.Media;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Fileupload;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygMembresCommissionsMarches;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.MembresCommissionsMarchesSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;




@SuppressWarnings("serial")
public class MembresCommissionsMarcheFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,codelocalite,gestion;
	public static final String PARAM_WINDOW_GESTION = "GESTION";
	private Textbox txtprenom, txtnom,txtfonction,txttel,txtemail;
	private Bandbox blocalite;
	private Listbox bdlocalite,lstMembresCommissionsMarche;
	private SygMembresCommissionsMarches membre =new SygMembresCommissionsMarches();
	private SimpleListModel lst;
	private Paging pgMembresCommissionsMarche;
	private final String ON_LOCALITE = "onLocalite";
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private Combobox txttypemembre;
	private ArrayList<String> listValeursAnnees;
	Long code;
	private Checkbox cbResponsable;
	private int nombre=0;
	List<SygMembresCommissionsMarches> membres = new ArrayList<SygMembresCommissionsMarches>();
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private SygAutoriteContractante autorite = null;
	private Utilisateur infoscompte;
	private Image imagePhoto;
	private boolean photoChanged;
	private String photoName;
	private org.zkoss.image.Image img = null;
	private Label lblGestion;
	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WIDOW_CODE);
			membre = BeanLocator.defaultLookup(MembresCommissionsMarchesSession.class)
					.findById(code);
			
			txtnom.setValue(membre.getNom());
			txtprenom.setValue(membre.getPrenom());
			txttel.setValue(membre.getTel());
			txtemail.setValue(membre.getEmail());
		
			gestion=membre.getGestion();
			//cbannee.setValue(membre.getGestion());
			txtfonction.setValue(membre.getFonction());
			if(membre.getFlagpresident()==UIConstants.PARENT)
			  {
				cbResponsable.setDisabled(false) ;
				cbResponsable.setChecked(true) ;
			  }
			else
			{
				cbResponsable.setChecked(false) ;
				membres=BeanLocator.defaultLookup(MembresCommissionsMarchesSession.class).find(0,-1,null,null,autorite,membre.getGestion(),UIConstants.PARENT,-1);
				 for (int i = 0; i < membres.size(); i++) {
					 if(membres.get(i).getFlagpresident()==UIConstants.PARENT)
					 {
						 nombre=nombre+1;
					 }
				 }
				 if(nombre>0)
				 {
					 cbResponsable.setDisabled(true) ;
					 
				 }
				 else
				 {
					 cbResponsable.setDisabled(false) ;
				 }
			}
			if ((membre.getPhoto() != null) && !membre.getPhoto().trim().equals("")) {
				img = fetchImage(UIConstants.PATH_PHOTOS, membre.getPhoto());
			} else {
				img = fetchImage(UIConstants.PATH_PHOTOS, UIConstants.PHOTO_PERSO_DEFAULT);
			}	
	    }
		else
		{
			    img = fetchImage(UIConstants.PATH_PHOTOS, UIConstants.PHOTO_PERSO_DEFAULT);
		}
		imagePhoto.setContent(img);
		gestion = (String) map.get(PARAM_WINDOW_GESTION);
		lblGestion.setValue(gestion);
		  
	}
	public void onOK() {
		if(checkFieldConstraints())
		{
			membre.setNom(txtnom.getValue());
			membre.setPrenom(txtprenom.getValue());
			membre.setGestion(gestion);
		    membre.setTypemembre(txttypemembre.getValue());
			membre.setTel(txttel.getValue());
			membre.setEmail(txtemail.getValue());
			membre.setFonction(txtfonction.getValue());
			
			membre.setAutorite(autorite);
			if(cbResponsable.isChecked()==true)
			{
				membre.setFlagpresident(UIConstants.PARENT);
			}
			else
			{
				membre.setFlagpresident(UIConstants.NPARENT);
			}
			if (photoChanged && (imagePhoto.getContent() != null)) 
			{
				membre.setPhoto(photoName);
				 savePhoto();
			}
			if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(MembresCommissionsMarchesSession.class).save(membre);
	
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(MembresCommissionsMarchesSession.class).update(membre);
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}

	}
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	
	}
	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onEvent(Event arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	

	 public void onSelect$cbannee(){
		 membres=BeanLocator.defaultLookup(MembresCommissionsMarchesSession.class).find(0,-1,null,null,autorite,membre.getGestion(),UIConstants.PARENT,-1);
		 nombre=0;
		 for (int i = 0; i < membres.size(); i++) {
			 if(membres.get(i).getFlagpresident()==UIConstants.PARENT)
			 {
				 nombre=nombre+1;
			 }
		 }
		 if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			 if(membre.getFlagpresident()==UIConstants.PARENT)
			  {
				 cbResponsable.setDisabled(false) ;
				 cbResponsable.setChecked(true) ;
			  }
			 else
			 {
				 cbResponsable.setChecked(false) ;
				 if(nombre>0)
				 {
					 cbResponsable.setDisabled(true) ;
				 }
				 else
				 {
					 cbResponsable.setDisabled(false) ;
				 }
			 }
		 }
		 else
		 {
			 cbResponsable.setChecked(false) ;
			 if(nombre>0)
			 {
				 cbResponsable.setDisabled(true) ;
				
			 }
			 else
			 {
				 cbResponsable.setDisabled(false) ;
			 }
		 }
	 }
		
			
		

	 private boolean checkFieldConstraints() {
			
			try {
				if(txtnom.getValue().equals(""))
			     {
	              errorComponent = txtnom;
	              errorMsg = Labels.getLabel("kermel.common.form.nom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				   lbStatusBar.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar.setValue(errorMsg);
				   throw new WrongValueException (errorComponent, errorMsg);
			     }
				if(txtprenom.getValue().equals(""))
			     {
	              errorComponent = txtprenom;
	              errorMsg = Labels.getLabel("kermel.common.form.prenom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				   lbStatusBar.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar.setValue(errorMsg);
				   throw new WrongValueException (errorComponent, errorMsg);
			     }
				
				if(txtemail.getValue().equals(""))
			     {
	              errorComponent = txtemail;
	              errorMsg = Labels.getLabel("kermel.referentiel.personne.email")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				   lbStatusBar.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar.setValue(errorMsg);
				   throw new WrongValueException (errorComponent, errorMsg);
			     }
				if (ToolKermel.ControlValidateEmail(txtemail.getValue())) {
					
				} else {
					errorComponent = txtemail;
	                errorMsg = Labels.getLabel("kermel.referentiel.personne.email.incorrect");
	 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
	 				lbStatusBar.setValue(errorMsg);
	 				throw new WrongValueException(errorComponent,errorMsg);
				}
	 			
				if(txtfonction.getValue().equals(""))
			     {
	              errorComponent = txtfonction;
	              errorMsg = Labels.getLabel("kermel.common.form.fonction")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				   lbStatusBar.setStyle(ERROR_MSG_STYLE);
				   lbStatusBar.setValue(errorMsg);
				   throw new WrongValueException (errorComponent, errorMsg);
			     }
				
				return true;
					
			}
			catch (Exception e) {
				errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
				+ " [checkFieldConstraints]";
				errorComponent = null;
				return false;

				
			}
			
		}
		
		private Boolean savePhoto() {

			try {
				if (photoChanged && (imagePhoto.getContent() != null)) {

					System.out.println(UIConstants.PATH_PHOTOS + membre.getPhoto());
					// String filepath =
					// Sessions.getCurrent().getWebApp().getRealPath(UIConstants.PATH_AGT_PHOTOS
					// + dossagent.getAgtPhoto());
					

					String filepath = UIConstants.PATH_PHOTOS + membre.getPhoto();
					System.out.println(imagePhoto.getSrc() + " source image ################"+filepath);
					System.out.println(filepath.replaceAll("/", "\\\\"));

					// windows
					// File f = new File(filepath.replaceAll("/", "\\\\"));
					// linux
					File f = new File(filepath.replaceAll("/", "\\\\"));

					if (f.exists()) {
						f.delete();
					}
					f.createNewFile();

					InputStream inputStream = imagePhoto.getContent().getStreamData();
					OutputStream out = new FileOutputStream(f);
					byte buf[] = new byte[1024];

					int len;
					while ((len = inputStream.read(buf)) > 0) {
						out.write(buf, 0, len);
					}
					out.close();
					inputStream.close();
				}
				return true;
			} catch (Exception e) {
				return false;
			}
		}

		public org.zkoss.image.Image fetchImage(String path, String file) {
			File imgfile = null;
			// windows
			// File imgfile = new File(path.replaceAll("/", "\\\\") + file);
			// linux
			if (ToolKermel.isWindows())
				imgfile = new File(path.replaceAll("/", "\\\\") + file);
			else
				imgfile = new File(path.replaceAll("\\\\", "/") + file);// linux

			org.zkoss.image.Image img = null;
			try {
				img = new org.zkoss.image.AImage(imgfile);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}

			return (org.zkoss.image.Image) img;
		}
	 
	 public void onClick$buttonTelecharger() {

			try {

				Media media = Fileupload.get();

				if (media instanceof org.zkoss.image.Image) {

					imagePhoto.setContent((org.zkoss.image.Image) media);
					photoName = generatePhotoName() + "." + ((org.zkoss.image.Image) media).getFormat();
					System.out.println(photoName + "ff" + ((org.zkoss.image.Image) media).getFormat());
					photoChanged = true;

				} else if (media != null) {

					alert("Ok");
				}

			} catch (Exception e) {

			}

		}

		private String generatePhotoName() {

			try {
				return "photo_"
						+ txtnom.getValue().replace('\\', '-').replace('/', '-').replace(':', '-').replace('*', '-').replace('?', '-').replace('\"', '-').replace('<', '-').replace(
								'>', '-').replace('|', '-');
			} catch (Exception e) {
				return null;
			}

		}
}