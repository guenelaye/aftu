package sn.ssi.kermel.web.referentiel.controllers;

import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCatFournisseur;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygPenaliteRetard;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.ContratSession;
import sn.ssi.kermel.be.session.PenaliteRetardSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class ListPenaliteRetardController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtNom;
    String nom=null, page=null;
    private Listheader lshNom, lshCategorie;
    String login;
    private SygAutoriteContractante autorite = null;
    
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    
    private SygCatFournisseur categorie = null;
    private Bandbox bdCategorie;
	private Paging pgCategorie;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private Listbox lstCategorie;
	private Textbox txtRechercherCategorie;
	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private String categorielibelle = null;
	private Utilisateur infoscompte;
	private SygContrats contrat;
	private String status;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
			/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
			
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION1, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lshNom.setSortAscending(new FieldComparator("datepaiement", false));
		lshNom.setSortDescending(new FieldComparator("datepaiement", true));
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
		 Long idcontrat = (Long) session.getAttribute("contrat");
		   contrat=BeanLocator.defaultLookup(ContratSession.class).findById(idcontrat);
    	login = ((String) getHttpSession().getAttribute("user"));
	}

	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);
	
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)){
			
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygPenaliteRetard> penalites = BeanLocator.defaultLookup(PenaliteRetardSession.class).find(activePage,byPage,contrat.getConID(),null);
			 SimpleListModel listModel = new SimpleListModel(penalites);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup( PenaliteRetardSession.class).count(null,null));
		} 
		
		

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygPenaliteRetard penalite = (SygPenaliteRetard) data;
		item.setValue(penalite.getPenId());
	//	item.setAttribute("status", demande.getSuivi());

		
		Listcell cellDateCourrier = new Listcell(penalite.getDemande().getLivrable());
		cellDateCourrier.setParent(item);
		
		Listcell cellTaux = new Listcell(ToolKermel.format1Decimal(penalite.getTaux()));
		cellTaux.setParent(item);
		
		Listcell cellMontant = new Listcell(ToolKermel.formatDecimal(penalite.getMontantpenalite()));
		cellMontant.setParent(item);
	
	
		
			
	}
	
	///////////Categorie///////// 
	public void onSelect$lstCategorie(){
		categorie= (SygCatFournisseur) lstCategorie.getSelectedItem().getValue();
		bdCategorie.setValue(categorie.getLibelle());
		bdCategorie.close();
	}
	
	public class CategoriesRenderer implements ListitemRenderer{
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygCatFournisseur categorie = (SygCatFournisseur) data;
			item.setValue(categorie);
			
			Listcell cellLibelle = new Listcell("");
			if (categorie.getLibelle()!=null){
				cellLibelle.setLabel(categorie.getLibelle());
			}
			cellLibelle.setParent(item);
	
		}
	}
	
	public void onFocus$txtRechercherCategorie(){
		if(txtRechercherCategorie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.code.categorie"))){
			txtRechercherCategorie.setValue("");
		}		 
	}
	
	public void  onClick$btnRechercherCategorie(){
		if(txtRechercherCategorie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.code.categorie")) || txtRechercherCategorie.getValue().equals("")){
			categorielibelle = null;
			page=null;
		}
		else{
			categorielibelle = txtRechercherCategorie.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}
	
	public void onClick$bchercher()
	{
		categorielibelle = bdCategorie.getValue();
		nom = txtNom.getValue();
		if(categorielibelle.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.code.categorie")) || "".equals(categorielibelle))
			categorie = null;
		
		if(nom.equalsIgnoreCase(Labels.getLabel("kermel.common.form.nom")) || "".equals(nom))
			nom = null;
		
		bdCategorie.setValue(Labels.getLabel("kermel.referentiel.common.code.categorie"));
		txtNom.setValue(Labels.getLabel("kermel.common.form.nom"));
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	
	public void onBlur$txtNom()
	{
		if(txtNom.getValue().equals(""))
			txtNom.setValue(Labels.getLabel("kermel.common.form.nom"));
	}
	public void onFocus$txtNom()
	{
		if(txtNom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.nom")))
			txtNom.setValue("");
	
	}
	public void onOK$txtNom()
	{
		onClick$bchercher();
	}
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
	
	
	
	
	
}
