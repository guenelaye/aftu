package sn.ssi.kermel.web.referentiel.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygNatureprix;
import sn.ssi.kermel.be.referentiel.ejb.NatureprixSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;


@SuppressWarnings("serial")
public class ListNatPrixController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstNat;
	private Paging pg;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle,txtCode;
    String libelle=null,page=null,code=null,login;
    private Listheader lshLibelle;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_NAT, MOD_NAT,SUP_NAT;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_NATPRIX);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent

		if (ADD_NAT != null) {
			ADD_NAT.addForward(ApplicationEvents.ON_CLICK, this,
					ApplicationEvents.ON_ADD);
		}
		addEventListener(ApplicationEvents.ON_ADD, this);

		if (MOD_NAT != null) {
			MOD_NAT.addForward(ApplicationEvents.ON_CLICK, this,
					ApplicationEvents.ON_EDIT);
		}
		if (SUP_NAT != null) {
			SUP_NAT.addForward(ApplicationEvents.ON_CLICK, this,
					ApplicationEvents.ON_DELETE);
		}
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
		lshLibelle.setSortAscending(new FieldComparator("natLibelle", false));
		lshLibelle.setSortDescending(new FieldComparator("natLibelle", true));
		lstNat.setItemRenderer(this);
		pg.setPageSize(byPage);
		pg.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	login = ((String) getHttpSession().getAttribute("user"));
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pg.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pg.getActivePage() * byPage;
				pg.setPageSize(byPage);
			}
			 List<SygNatureprix> nat = BeanLocator.defaultLookup(NatureprixSession.class).find(activePage,byPage,code,libelle);
			 SimpleListModel listModel = new SimpleListModel(nat);
			 lstNat.setModel(listModel);
			 pg.setTotalSize(BeanLocator.defaultLookup( NatureprixSession.class).count(code,libelle));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/referentiel/NatPrix/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.nat.titre.nouveau"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormNatPrixController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstNat.getSelectedItem() == null)
				
				throw new WrongValueException(lstNat, Labels.getLabel("kermel.error.select.item"));
			final String uri = "/referentiel/NatPrix/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormNatPrixController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(FormNatPrixController.PARAM_WINDOW_CODE, lstNat .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstNat.getSelectedItem() == null)
				
					throw new WrongValueException(lstNat, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, lstNat.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstNat.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstNat.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(NatureprixSession.class).delete(codes);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	

	}
	
	
	public void onClick$bchercher()
	{
		onOK();
	}

	public void onOK()
	{
		code=txtCode.getValue();
		libelle=txtLibelle.getValue();
		
		if(!libelle.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.nat.libelle")))
		{ if(code.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.nat.code")))
			code="";
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	    }

		if(!code.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.nat.code")))
		{ if(libelle.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.nat.libelle")))
			libelle="";
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	    }
		
	  if(code.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.nat.code"))&& libelle.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.nat.libelle")))
			  {
		       code="";
		       libelle="";
		      }
	  Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
	
	
	public void onFocus$txtLibelle()
	{
		if(txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.nat.libelle")))
			txtLibelle.setValue("");
		
	}
	
	public void onBlur$txtLibelle()
	{
		if(txtLibelle.getValue().equals(""))
			txtLibelle.setValue(Labels.getLabel("kermel.referentiel.nat.libelle"));
	}

	public void onOK$txtLibelle()
	{
		onOK();
	}
	
	public void onFocus$txtCode()
	{
		if(txtCode.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.nat.code")))
			txtCode.setValue("");
		
	}
	
	public void onBlur$txtCode()
	{
		if(txtCode.getValue().equals(""))
			txtCode.setValue(Labels.getLabel("kermel.referentiel.nat.code"));
	}

	public void onOK$txtCode()
	{
		onOK();
	}
	
	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygNatureprix nat= (SygNatureprix) data;
		item.setValue(nat.getNatId());

		 Listcell cellCode = new Listcell(nat.getNatCode());
		 cellCode.setParent(item);
		 Listcell cellLibelle = new Listcell(nat.getNatLibelle());
		 cellLibelle.setParent(item);
		 	

	}
}