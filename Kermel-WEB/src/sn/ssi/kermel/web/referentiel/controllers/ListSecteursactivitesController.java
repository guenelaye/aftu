package sn.ssi.kermel.web.referentiel.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.referentiel.ejb.SecteursactivitesSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class ListSecteursactivitesController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null,login,codesuppression,libellesuppression;
    private Listheader lshLibelle,lshDivision;
    Session session = getHttpSession();
    private String reference;
    private int parent;
    private Label lbltitre;
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_SECTEURSACTIVITES, MOD_SECTEURSACTIVITES, SUPP_SECTEURSACTIVITES,ADD_CATEGORIES, MOD_CATEGORIES, SUPP_CATEGORIES;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		reference=(String) session.getAttribute("reference");
		System.out.println("*****My State*******  "+reference);
		if(reference.equals("categorie")){
			parent=UIConstants.PARENT;
			lbltitre.setValue(Labels.getLabel("kermel.referentiel.common.code.categorie"));
			monSousMenu.setFea_code(UIConstants.REF_CATEGORIES);
			monSousMenu.afterCompose();
			/* pour les menuitem eventuellement crees */
			Components.wireFellows(this, this); // mais le wireFellows precedent
			// reste indispensable
			/* reprise des forwards definis dans le .zul */
			if (ADD_CATEGORIES != null) { ADD_CATEGORIES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
			if (MOD_CATEGORIES != null) { MOD_CATEGORIES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
			if (SUPP_CATEGORIES != null) { SUPP_CATEGORIES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
			codesuppression="SUPP_CATEGORIES";
			libellesuppression=Labels.getLabel("kermel.referentiel.common.categories.suppression");
		}else {
			parent=UIConstants.NPARENT;
			lbltitre.setValue(Labels.getLabel("kermel.referentiel.common.code.secteur"));
			monSousMenu.setFea_code(UIConstants.REF_SECTEURSACTIVITES);
			monSousMenu.afterCompose();
			/* pour les menuitem eventuellement crees */
			Components.wireFellows(this, this); // mais le wireFellows precedent
			// reste indispensable
			/* reprise des forwards definis dans le .zul */
			if (ADD_SECTEURSACTIVITES != null) { ADD_SECTEURSACTIVITES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
			if (MOD_SECTEURSACTIVITES != null) { MOD_SECTEURSACTIVITES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
			if (SUPP_SECTEURSACTIVITES != null) { SUPP_SECTEURSACTIVITES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
			codesuppression="SUPP_SECTEURSACTIVITES";
			libellesuppression=Labels.getLabel("kermel.referentiel.common.secteur.suppression");
		}
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

//		lshLibelle.setSortAscending(new FieldComparator("libelle", false));
//		lshLibelle.setSortDescending(new FieldComparator("libelle", true));
//		lshDivision.setSortAscending(new FieldComparator("division.libelle", false));
//		lshDivision.setSortDescending(new FieldComparator("division.libelle", true));
		
		
		
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	login = ((String) getHttpSession().getAttribute("user"));
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygSecteursactivites> secteurs = BeanLocator.defaultLookup(SecteursactivitesSession.class).find(activePage,byPage,null,libelle,null);
			 SimpleListModel listModel = new SimpleListModel(secteurs);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup( SecteursactivitesSession.class).count(null,libelle,null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/referentiel/secteursactivites/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(SecteursactivitesFormController.PARAM_WINDOW_PARENT, parent);
			data.put(SecteursactivitesFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			final String uri = "/referentiel/secteursactivites/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(SecteursactivitesFormController.PARAM_WINDOW_PARENT, parent);
			data.put(SecteursactivitesFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(SecteursactivitesFormController.PARAM_WINDOW_CODE, ((SygSecteursactivites)lstListe .getSelectedItem().getValue()).getCode());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
				
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				categorie = (SygSecteursactivites) ((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				categories=BeanLocator.defaultLookup(SecteursactivitesSession.class).find(0,-1,null,null,categorie);
					for (int k = 0; k < categories.size(); k++) {
						BeanLocator.defaultLookup(SecteursactivitesSession.class).delete(categories.get(k).getCode());
					}
				BeanLocator.defaultLookup(SecteursactivitesSession.class).delete(categorie.getCode());
				
				BeanLocator.defaultLookup(JournalSession.class).logAction(codesuppression,libellesuppression+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	

	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygSecteursactivites secteur = (SygSecteursactivites) data;
		item.setValue(secteur);

		 Listcell cellLibelle = new Listcell("");
		 if((secteur.getCode().length()/3)==1)
			 cellLibelle.setLabel("---"+secteur.getLibelle());
		 else  if((secteur.getCode().length()/3)==2)
			 cellLibelle.setLabel("------"+secteur.getLibelle());
		 else  if((secteur.getCode().length()/3)==3)
			 cellLibelle.setLabel("---------"+secteur.getLibelle());
		 else  if((secteur.getCode().length()/3)==4)
			 cellLibelle.setLabel("------------"+secteur.getLibelle());
		 else  if((secteur.getCode().length()/3)==4)
			 cellLibelle.setLabel("---------------"+secteur.getLibelle());
		 else  if((secteur.getCode().length()/3)==6)
			 cellLibelle.setLabel("------------------"+secteur.getLibelle());
		 else  if((secteur.getCode().length()/3)==7)
			 cellLibelle.setLabel("---------------------"+secteur.getLibelle());
		 else  if((secteur.getCode().length()/3)==8)
			 cellLibelle.setLabel("------------------------"+secteur.getLibelle());
		 else  if((secteur.getCode().length()/3)==9)
			 cellLibelle.setLabel("---------------------------"+secteur.getLibelle());
		 else  if((secteur.getCode().length()/3)==10)
			 cellLibelle.setLabel("------------------------------"+secteur.getLibelle());
		 else  cellLibelle.setLabel(secteur.getLibelle());
		 cellLibelle.setParent(item);
		 
		
		


	}
	public void onClick$bchercher()
	{
		
		
		if((txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.banque.libelle")))||(txtLibelle.getValue().equals("")))
		 {
			libelle=null;
			page=null;
		 }
		else
		{
			libelle=txtLibelle.getValue();
			page="0";
		}
		
		
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	
	public void onFocus$txtLibelle()
	{
		if(txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.banque.libelle")))
			txtLibelle.setValue("");
		
	}
	public void onBlur$txtLibelle()
	{
		if(txtLibelle.getValue().equals(""))
			txtLibelle.setValue(Labels.getLabel("kermel.referentiel.banque.libelle"));
	}
	
	public void onOK$txtLibelle()
	{
		onClick$bchercher();
	}
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
	
}