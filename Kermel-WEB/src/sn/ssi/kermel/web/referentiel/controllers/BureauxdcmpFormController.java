package sn.ssi.kermel.web.referentiel.controllers;


import java.util.Date;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygBureauxdcmp;
import sn.ssi.kermel.be.entity.SygTypeUniteOrgdcmp;
import sn.ssi.kermel.be.referentiel.ejb.BureauxdcmpSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeUniteOrgdcmpSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class BureauxdcmpFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_STATE = "STATE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle;
	Long code,typeunitorgId,uniteorgId;
	private	SygBureauxdcmp bureau=new SygBureauxdcmp();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
//	private Bandbox bdDivision;
//	private Paging pgDivision;
//	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE,activePage;
//	private String libelledivision = null,page=null;
//	private Listbox lstDivision;
//	private Textbox txtRechercherDivision;
	SygTypeUniteOrgdcmp division;
	SygBureauxdcmp uniteorg;
	Session session = getHttpSession();

	@Override
	public void onEvent(Event event) throws Exception {
//		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)){
//			if (event.getData() != null) {
//				activePage = Integer.parseInt((String) event.getData());
//				byPageBandbox = -1;
//				pgDivision.setPageSize(1000);
//			} else {
//				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
//				activePage = pgDivision.getActivePage() * byPageBandbox;
//				pgDivision.setPageSize(byPageBandbox);
//			}
//			List<SygTypeUniteOrgdcmp> divisions = BeanLocator.defaultLookup(TypeUniteOrgdcmpSession.class).find(activePage, byPageBandbox,null,libelledivision, null);
//			lstDivision.setModel(new SimpleListModel(divisions));
//			pgDivision.setTotalSize(BeanLocator.defaultLookup(TypeUniteOrgdcmpSession.class).count(null,libelledivision, null));
//		}

	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
//		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
//		lstDivision.setItemRenderer(new DivisionsRenderer());
//		pgDivision.setPageSize(byPageBandbox);
//		pgDivision.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		typeunitorgId=(Long) session.getAttribute("typeunitorg");
		uniteorgId=(Long) session.getAttribute("uniteorg");
		division = BeanLocator.defaultLookup(TypeUniteOrgdcmpSession.class).findById(typeunitorgId);
		uniteorg = BeanLocator.defaultLookup(BureauxdcmpSession.class).findById(uniteorgId);
		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			bureau = BeanLocator.defaultLookup(BureauxdcmpSession.class).findById(code);
			txtLibelle.setValue(bureau.getLibelle());
			//division=bureau.getDivision();
			//bdDivision.setValue(division.getLibelle());
			
		}
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			bureau.setLibelle(txtLibelle.getValue());
			bureau.setDivision(division);
			bureau.setUniteorg(uniteorg);
			
	     	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(BureauxdcmpSession.class).save(bureau);
				BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_BUREAUXDCMP", Labels.getLabel("kermel.referentiel.common.bureaucmp.ajouter")+" :" + new Date(), login);
				
	
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(BureauxdcmpSession.class).update(bureau);
				BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_BUREAUXDCMP", Labels.getLabel("kermel.referentiel.common.bureaucmp.modifier")+" :" + new Date(), login);
				
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			if(txtLibelle.getValue().equals(""))
		     {
               errorComponent = txtLibelle;
               errorMsg = Labels.getLabel("kermel.referentiel.banque.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			

//			if(division==null)
//		     {
//             errorComponent = bdDivision;
//             errorMsg = Labels.getLabel("kermel.referentiel.common.division")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
//				lbStatusBar.setStyle(ERROR_MSG_STYLE);
//				lbStatusBar.setValue(errorMsg);
//				throw new WrongValueException (errorComponent, errorMsg);
//		     }

			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	

		///////////Service///////// 
//		public void onSelect$lstDivision(){
//		division= (SygTypeUniteOrgdcmp) lstDivision.getSelectedItem().getValue();
//		bdDivision.setValue(division.getLibelle());
//		bdDivision.close();
//		
//		}
//		
//		public class DivisionsRenderer implements ListitemRenderer{
//			
//		
//		
//			@Override
//			public void render(Listitem item, Object data, int index)  throws Exception {
//				SygTypeUniteOrgdcmp division = (SygTypeUniteOrgdcmp) data;
//				item.setValue(division);
//				
//				Listcell cellLibelle = new Listcell(division.getLibelle());
//			    cellLibelle.setParent(item);
//		
//			}
//		}
//		public void onFocus$btnRechercherDivision(){
//		if(txtRechercherDivision.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.division"))){
//			txtRechercherDivision.setValue("");
//		}		 
//		}
//		
//		public void  onClick$btnRechercherService(){
//		if(txtRechercherDivision.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.division")) || txtRechercherDivision.getValue().equals("")){
//			libelledivision = null;
//			page=null;
//		}else{
//			libelledivision = txtRechercherDivision.getValue();
//			page="0";
//		}
//		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
//		}
		
		

}
