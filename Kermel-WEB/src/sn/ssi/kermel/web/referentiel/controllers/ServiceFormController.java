package sn.ssi.kermel.web.referentiel.controllers;


import java.util.Date;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygService;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteContractanteSession;
import sn.ssi.kermel.be.referentiel.ejb.ServiceSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeAutoriteContractanteSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class ServiceFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle, txtCode,txtDescription;
	Long code;
	private	SygService service=new SygService();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygTypeAutoriteContractante type=new SygTypeAutoriteContractante();
	SygAutoriteContractante autorite=new SygAutoriteContractante();
	private Long idtype,idautorite;
	private Label lblType,lblAutorite;
	Session session = getHttpSession();
	
	
	@Override
	public void onEvent(Event event) throws Exception {

		
	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
			
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		idtype=(Long) session.getAttribute("idtype");
		idautorite=(Long) session.getAttribute("idautorite");
		type=BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).findById(idtype);
		autorite=BeanLocator.defaultLookup(AutoriteContractanteSession.class).findById(idautorite);
		lblType.setValue(type.getLibelle());
		lblAutorite.setValue(autorite.getDenomination());
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			service = BeanLocator.defaultLookup(ServiceSession.class).findById(code);
			txtCode.setValue(service.getCodification());
			txtLibelle.setValue(service.getLibelle());
			txtDescription.setValue(service.getDescription());
		
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			service.setCodification(txtCode.getValue());
			service.setLibelle(txtLibelle.getValue());  
			service.setDescription(txtDescription.getValue());
			service.setAutorite(autorite);
			
		   	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(ServiceSession.class).save(service);
				BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_SERVICEAC", Labels.getLabel("kermel.referentiel.common.service.ajouter")+" :" + new Date(), login);
				
	
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(ServiceSession.class).update(service);
				BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_SERVICEAC", Labels.getLabel("kermel.referentiel.common.service.modifier")+" :" + new Date(), login);
				
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			if(txtCode.getValue().equals(""))
		     {
               errorComponent = txtCode;
               errorMsg = Labels.getLabel("kermel.referentiel.common.autorisation.code")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			

			if(txtLibelle.getValue().equals(""))
		     {
               errorComponent = txtLibelle;
               errorMsg = Labels.getLabel("kermel.referentiel.banque.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			
			
		
			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	



	

}
