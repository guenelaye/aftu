package sn.ssi.kermel.web.referentiel.controllers;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygLogisticien;
import sn.ssi.kermel.be.referentiel.ejb.LogisticienSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;


@SuppressWarnings("serial")
public class ListLogisticienController extends AbstractWindow implements EventListener, AfterCompose, ListitemRenderer {

	private Listbox list;
	private Paging pg;
	public static final String PARAM_WINDOW_CODE = "CODE";

	private Listheader lshPrenom;
	private String nom,prenom;
	private Textbox txtPrenom,txtNom;
	private SygLogisticien logisticien;
	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private int activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	public static final String WINDOW_PARAM_MODE = "MODE";
	private String page=null;
	Session session = getHttpSession();
	
	private KermelSousMenu monSousMenu;
	private Menuitem ADD_LOGISTICIEN, MOD_LOGISTICIEN, DEL_LOGISTICIEN;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_LOGISTICIEN);
		monSousMenu.afterCompose();
		Components.wireFellows(this, this);
		if (ADD_LOGISTICIEN != null) {
			ADD_LOGISTICIEN.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
		}
		if (MOD_LOGISTICIEN != null) {
			MOD_LOGISTICIEN.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
		}
		if (DEL_LOGISTICIEN != null) {
			DEL_LOGISTICIEN.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
		}
	
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	
		Events.postEvent(ApplicationEvents.ON_CLICK, this, null);
		
		lshPrenom.setSortAscending(new FieldComparator("prenom", false));
		lshPrenom.setSortDescending(new FieldComparator("prenom", true));
		list.setItemRenderer(this);
		pg.setPageSize(byPage);
		pg.addForward("onPaging", this,ApplicationEvents.ON_MODEL_CHANGE);
	
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	/**
	 * Permet de gerer les evenements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet evenement survient, les evenements de la liste sont mis �
		 * jour de meme que l'element de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
 			 List<SygLogisticien> logisticien = BeanLocator.defaultLookup(LogisticienSession.class).findAllby(pg.getActivePage()*byPage,byPage, nom, prenom);

 			 SimpleListModel listModel = new SimpleListModel(logisticien);
			list.setModel(listModel);
			pg.setTotalSize(BeanLocator.defaultLookup(LogisticienSession.class).count(nom, prenom));

		} 
	
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/logisticienformation/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.formation.logisticien.nouv"));
			display.put(DSP_HEIGHT,"80%");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormLogisticienController.WINDOW_PARAM_MODE,UIConstants.MODE_NEW);
			
			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				throw new WrongValueException(list, Labels.getLabel("kermel.referentiel.nat.message.selection"));
				
			}
			final String uri ="/logisticienformation/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"80%");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.formation.logisticien.modif"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormLogisticienController.WINDOW_PARAM_MODE,UIConstants.MODE_EDIT);
		    data.put(FormLogisticienController.PARAM_WIDOW_CODE, list.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
					throw new WrongValueException(list, Labels.getLabel("kermel.referentiel.nat.message.selection"));
				HashMap<String, String> display = new HashMap<String, String>(); 
			
				// permet de fixer les dimensions du popup
			
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.referentiel.caractere.message.suppression"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.referentiel.caractere.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// permet de passer des parametres au popup
				
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){		
			for (int i = 0; i < list.getSelectedCount(); i++) {
				logisticien = (SygLogisticien)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
				//System.out.println(pop);
				BeanLocator.defaultLookup(LogisticienSession.class).delete(logisticien.getId());
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
			
	}
	
	
	public void onClick$bchercher()
	{
		onOK();
	}
	
	public void onOK()
	{
		nom = txtNom.getValue();
		prenom=txtPrenom.getValue();
	
		
		if(!prenom.equalsIgnoreCase(Labels.getLabel("kermel.formateur.pnom")))
		{ if(nom.equalsIgnoreCase(Labels.getLabel("kermel.formateur.nom")))
			nom="";
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	    }

		if(!nom.equalsIgnoreCase(Labels.getLabel("kermel.formateur.nom")))
		{ if(prenom.equalsIgnoreCase(Labels.getLabel("kermel.formateur.pnom")))
			prenom="";
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	    }
		
	  if(nom.equalsIgnoreCase(Labels.getLabel("kermel.formateur.nom"))&& prenom.equalsIgnoreCase(Labels.getLabel("kermel.formateur.pnom")))
			  {
		       nom="";
		       prenom="";
		      }
	  Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
	
	public void onFocus$txtPrenom()
	{
		if(txtPrenom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.formateur.pnom")))
			txtPrenom.setValue("");
		else
			prenom=txtPrenom.getValue();
	}
	
	
	public void onBlur$txtPrenom()
	{
		if(txtPrenom.getValue().equals(""))
			txtPrenom.setValue(Labels.getLabel("kermel.formateur.pnom"));
	}
	
	public void onFocus$txtNom()
	{
		if(txtNom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.formateur.nom")))
			txtNom.setValue("");
		else
			nom=txtNom.getValue();
	}
	
	
	public void onBlur$txtNom()
	{
		if(txtNom.getValue().equals(""))
			txtNom.setValue(Labels.getLabel("kermel.formateur.nom"));
	}
	
	

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		 SygLogisticien logist = (SygLogisticien) data;
		 item.setValue(logist);
		
		Listcell cellNin,cellNom,cellPnom, cellAdd,cellTel,cellMail,cellCom;
		
		
		if(logist.getNin()!=null) {
			cellNin = new Listcell(logist.getNin().toString());
			cellNin.setParent(item);
		}
		else {
			cellNin = new Listcell("");
			cellNin.setParent(item);
		}
		if(logist.getPrenom()!=null) {
			cellPnom = new Listcell(logist.getPrenom());
			cellPnom.setParent(item);
		}
		else {
			cellPnom = new Listcell("");
			cellPnom.setParent(item);
		}
		
		if(logist.getNom()!=null) {
			cellNom = new Listcell(logist.getNom());
			cellNom.setParent(item);
		}
		else {
			cellNom = new Listcell("");
			cellNom.setParent(item);
		}
		
		if(logist.getAddress()!=null) {
			cellAdd = new Listcell(logist.getAddress());
			cellAdd.setParent(item);
		}
		else {
			cellAdd = new Listcell("");
			cellAdd.setParent(item);
		}
		
		if(logist.getEmail()!=null) {
			cellMail = new Listcell(logist.getEmail());
			cellMail.setParent(item);
		}
		else {
			cellMail = new Listcell("");
			cellMail.setParent(item);
		}
		
		if(logist.getTel()!=null) {
			cellTel = new Listcell(logist.getTel().toString());
			cellTel.setParent(item);
		}
		else {
			cellTel = new Listcell("");
			cellTel.setParent(item);
		}
		
		if(logist.getCommentaire()!=null) {
			cellCom = new Listcell(logist.getCommentaire());
			cellCom.setParent(item);
		}
		else {
			cellCom = new Listcell("");
			cellCom.setParent(item);
		}
	}  
}
