package sn.ssi.kermel.web.referentiel.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCatFournisseur;
import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygMontantsSeuils;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.ModepassationSession;
import sn.ssi.kermel.be.referentiel.ejb.MontantsSeuilsSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeAutoriteContractanteSession;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class SeuilsRevueAPrioriController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtModeEngagement;
    String nom=null, page=null;
    private Listheader lshNom, lshCategorie;
    String login;
    private SygAutoriteContractante autorite = null;
    
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_SEUILSRAPRIORI, MOD_SEUILSRAPRIORI, SUPP_SEUILSRAPRIORI;
    
    private SygCatFournisseur categorie = null;
    private Bandbox bdCategorie;
	private Paging pgCategorie;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private Listbox lstCategorie;
	private Textbox txtRechercherCategorie;
	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private String categorielibelle = null;
	private Utilisateur infoscompte;
	private Bandbox bdTypeAutorite,bdTypeMarche,bdModePassation;
	private Paging pgTypeAutorite,pgTypeMarche,pgModePassation;
	private String libelletypeautorite = null, libelletypemarche = null,libellemodepassation = null,libellemodeengagement=null,examen=null;
	private Listbox lstTypeAutorite,lstTypeMarche,lstModePassation;
	private Textbox txtRechercherTypeAutorite,txtRechercherTypeMarche,txtRechercherModePassation;
	SygTypeAutoriteContractante typeautorite=null;
	SygTypesmarches typemarche=null;
	SygModepassation modepassation=null;
	private Combobox cbExamen;
	
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_SEUILSRAPRIORI);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_SEUILSRAPRIORI != null) { ADD_SEUILSRAPRIORI.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_SEUILSRAPRIORI != null) { MOD_SEUILSRAPRIORI.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUPP_SEUILSRAPRIORI != null) { SUPP_SEUILSRAPRIORI.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		
		addEventListener(ApplicationEvents.ON_TYPE_AUTORITES, this);
		lstTypeAutorite.setItemRenderer(new TypeAutoritesRenderer());
		pgTypeAutorite.setPageSize(byPageBandbox);
		pgTypeAutorite.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_TYPE_AUTORITES);
		Events.postEvent(ApplicationEvents.ON_TYPE_AUTORITES, this, null);
		
		addEventListener(ApplicationEvents.ON_TYPE_MARCHES, this);
		lstTypeMarche.setItemRenderer(new TypeMarchesRenderer());
		pgTypeMarche.setPageSize(byPageBandbox);
		pgTypeMarche.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_TYPE_MARCHES);
		Events.postEvent(ApplicationEvents.ON_TYPE_MARCHES, this, null);
		
		addEventListener(ApplicationEvents.ON_MODE_PASSATIONS, this);
		lstModePassation.setItemRenderer(new ModesPassationsRenderer());
		pgModePassation.setPageSize(byPageBandbox);
		pgModePassation.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODE_PASSATIONS);
		Events.postEvent(ApplicationEvents.ON_MODE_PASSATIONS, this, null);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lshNom.setSortAscending(new FieldComparator("nom", false));
		lshNom.setSortDescending(new FieldComparator("nom", true));
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
    	
    	login = ((String) getHttpSession().getAttribute("user"));
	}

	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {
		 if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygMontantsSeuils> seuils = BeanLocator.defaultLookup(MontantsSeuilsSession.class).find(activePage,byPage, typeautorite, typemarche, modepassation, libellemodeengagement, examen,UIConstants.TYPE_SEUILSRAPRIORI);
			 SimpleListModel listModel = new SimpleListModel(seuils);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup( MontantsSeuilsSession.class).count(typeautorite, typemarche, modepassation, libellemodeengagement, examen,UIConstants.TYPE_SEUILSRAPRIORI));
		} 
		 if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_TYPE_AUTORITES)){
				if (event.getData() != null) {
					activePage = Integer.parseInt((String) event.getData());
					byPageBandbox = -1;
					pgTypeAutorite.setPageSize(1000);
				} else {
					byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
					activePage = pgTypeAutorite.getActivePage() * byPageBandbox;
					pgTypeAutorite.setPageSize(byPageBandbox);
				}
				List<SygTypeAutoriteContractante> typeautorites = BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).find(activePage, byPageBandbox,null,libelletypeautorite);
				lstTypeAutorite.setModel(new SimpleListModel(typeautorites));
				pgTypeAutorite.setTotalSize(BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).count(null,libelletypeautorite));
			}
			else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_TYPE_MARCHES)){
				if (event.getData() != null) {
					activePage = Integer.parseInt((String) event.getData());
					byPageBandbox = -1;
					pgTypeMarche.setPageSize(1000);
				} else {
					byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
					activePage = pgTypeMarche.getActivePage() * byPageBandbox;
					pgTypeMarche.setPageSize(byPageBandbox);
				}
				List<SygTypesmarches> typemarches = BeanLocator.defaultLookup(TypesmarchesSession.class).find(activePage, byPageBandbox,null,libelletypemarche,null);
				lstTypeMarche.setModel(new SimpleListModel(typemarches));
				pgTypeMarche.setTotalSize(BeanLocator.defaultLookup(TypesmarchesSession.class).count(null,libelletypemarche,null));
			}
			else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODE_PASSATIONS)){
				if (event.getData() != null) {
					activePage = Integer.parseInt((String) event.getData());
					byPageBandbox = -1;
					pgModePassation.setPageSize(1000);
				} else {
					byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
					activePage = pgModePassation.getActivePage() * byPageBandbox;
					pgModePassation.setPageSize(byPageBandbox);
				}
				List<SygModepassation> modespassations = BeanLocator.defaultLookup(ModepassationSession.class).find(activePage, byPageBandbox,null,libellemodepassation);
				lstModePassation.setModel(new SimpleListModel(modespassations));
				pgModePassation.setTotalSize(BeanLocator.defaultLookup(ModepassationSession.class).count(null,libellemodepassation));
			}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/referentiel/montant_seuil/formrevueapriori.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(MontantsSeuilsFormController.PARAM_WINDOW_TYPE, UIConstants.TYPE_SEUILSRAPRIORI);
			data.put(MontantsSeuilsFormController.PARAM_WINDOW_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListe.getSelectedItem() == null)
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			
			final String uri = "/referentiel/montant_seuil/formrevueapriori.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(MontantsSeuilsFormController.PARAM_WINDOW_TYPE, UIConstants.TYPE_SEUILSRAPRIORI);
			data.put(MontantsSeuilsFormController.PARAM_WINDOW_MODE, UIConstants.MODE_EDIT);
			data.put(MontantsSeuilsFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				
				HashMap<String, String> display = new HashMap<String, String>();
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); 
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(MontantsSeuilsSession.class).delete(codes);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	

	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygMontantsSeuils seuils = (SygMontantsSeuils) data;
		item.setValue(seuils.getId());

		Listcell cellTypeAutorite = new Listcell(seuils.getTypeautorite().getLibelle());
		cellTypeAutorite.setParent(item);
		
		Listcell cellTypemarche = new Listcell(seuils.getTypemarche().getLibelle());
		cellTypemarche.setParent(item);
		
		Listcell cellSeuil = new Listcell("");
		if(seuils.getMontantinferieur()!=null&&seuils.getMontantsuperieur()!=null)
		{
			cellSeuil.setLabel("Sup�rieur ou �gal � "+ToolKermel.format2Decimal(seuils.getMontantinferieur())+" et  inf�rieur � "+ToolKermel.format2Decimal(seuils.getMontantsuperieur())+".");
    	}
		else
		{
			if(seuils.getMontantinferieur()!=null)
				cellSeuil.setLabel("Sup�rieur ou �gal � "+ToolKermel.format2Decimal(seuils.getMontantinferieur())+".");
			else
				cellSeuil.setLabel("Inf�rieur � "+ToolKermel.format2Decimal(seuils.getMontantsuperieur())+".");


		}
		cellSeuil.setParent(item);
		
		Listcell cellProcedures = new Listcell(seuils.getModepassation().getLibelle());
		cellProcedures.setParent(item);

		Listcell cellModeEngagement = new Listcell(seuils.getModeengagement());
		cellModeEngagement.setParent(item);
		
		Listcell cellExamen = new Listcell(seuils.getExamen());
		cellExamen.setParent(item);
	}
	
	///////////Type Autorite///////// 
	public void onSelect$lstTypeAutorite(){
		typeautorite= (SygTypeAutoriteContractante) lstTypeAutorite.getSelectedItem().getValue();
		bdTypeAutorite.setValue(typeautorite.getLibelle());
		bdTypeAutorite.close();
	}
	
	public class TypeAutoritesRenderer implements ListitemRenderer{
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypeAutoriteContractante typeautorite = (SygTypeAutoriteContractante) data;
			item.setValue(typeautorite);
			
			Listcell cellLibelle = new Listcell(typeautorite.getLibelle());
			cellLibelle.setParent(item);
	
		}
	}
	
	public void onFocus$txtRechercherTypeAutorite(){
	if(txtRechercherTypeAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.montantseuils.typeautorite"))){
		txtRechercherTypeAutorite.setValue("");
	}		 
	}
	
	public void  onClick$btnRechercherTypeAutorite(){
	if(txtRechercherTypeAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.montantseuils.typeautorite")) || txtRechercherTypeAutorite.getValue().equals("")){
		libelletypeautorite = null;
		page=null;
	}
	else{
		libelletypeautorite = txtRechercherTypeAutorite.getValue();
		page="0";
	}
	Events.postEvent(ApplicationEvents.ON_TYPE_AUTORITES, this, page);
	}
	
///////////Type Marche///////// 
	public void onSelect$lstTypeMarche(){
		typemarche= (SygTypesmarches) lstTypeMarche.getSelectedItem().getValue();
		bdTypeMarche.setValue(typemarche.getLibelle());
		bdTypeMarche.close();
	}
	
	public class TypeMarchesRenderer implements ListitemRenderer{
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypesmarches typemarche = (SygTypesmarches) data;
			item.setValue(typemarche);
			
			Listcell cellLibelle = new Listcell(typemarche.getLibelle());
			cellLibelle.setParent(item);
	
		}
	}
	
	public void onFocus$txtRechercherTypeMarche(){
	if(txtRechercherTypeMarche.getValue().equalsIgnoreCase(Labels.getLabel("kermel.montantseuils.typemarche"))){
		txtRechercherTypeMarche.setValue("");
	}		 
	}
	
	public void  onClick$btnRechercherTypeMarche(){
	if(txtRechercherTypeMarche.getValue().equalsIgnoreCase(Labels.getLabel("kermel.montantseuils.typemarche")) || txtRechercherTypeMarche.getValue().equals("")){
		libelletypemarche = null;
		page=null;
	}
	else{
		libelletypemarche = txtRechercherTypeMarche.getValue();
		page="0";
	}
	Events.postEvent(ApplicationEvents.ON_TYPE_MARCHES, this, page);
	}
	
///////////Mode Passation///////// 
	public void onSelect$lstModePassation(){
		modepassation= (SygModepassation) lstModePassation.getSelectedItem().getValue();
		bdModePassation.setValue(modepassation.getLibelle());
		bdModePassation.close();
	}
	
	public class ModesPassationsRenderer implements ListitemRenderer{
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygModepassation modes = (SygModepassation) data;
			item.setValue(modes);
			
			Listcell cellLibelle = new Listcell(modes.getLibelle());
			cellLibelle.setParent(item);
	
		}
	}
	
	public void onFocus$txtRechercherModePassation(){
	if(txtRechercherModePassation.getValue().equalsIgnoreCase(Labels.getLabel("kermel.montantseuils.modepassation"))){
		txtRechercherModePassation.setValue("");
	}		 
	}
	
	public void  onClick$btnRechercherModePassation(){
	if(txtRechercherModePassation.getValue().equalsIgnoreCase(Labels.getLabel("kermel.montantseuils.modepassation")) || txtRechercherModePassation.getValue().equals("")){
		libellemodepassation = null;
		page=null;
	}
	else{
		libellemodepassation = txtRechercherModePassation.getValue();
		page="0";
	}
	Events.postEvent(ApplicationEvents.ON_TYPE_MARCHES, this, page);
	}
	public void onClick$bchercher()
	{
		if(bdTypeAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.montantseuils.typeautorite")) || bdTypeAutorite.getValue().equals("")){
			typeautorite = null;
		}
		else{
			page="0";
		}
		if(bdTypeMarche.getValue().equalsIgnoreCase(Labels.getLabel("kermel.montantseuils.typemarche")) || bdTypeMarche.getValue().equals("")){
			typemarche = null;
		}
		else{
			page="0";
		}
		if(bdTypeAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.montantseuils.modepassation")) || bdTypeAutorite.getValue().equals("")){
			modepassation = null;
		}
		else{
			page="0";
		}
		if(txtModeEngagement.getValue().equalsIgnoreCase(Labels.getLabel("kermel.montantseuils.modeengagement")) || txtModeEngagement.getValue().equals("")){
			libellemodeengagement = null;
		}
		else{
			page="0";
			libellemodeengagement=txtModeEngagement.getValue();
		}
		if(cbExamen.getValue().equals("")){
			examen = null;
		}
		else{
			page="0";
			examen=cbExamen.getValue();
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	
	public void onBlur$txtModeEngagement()
	{
		if(txtModeEngagement.getValue().equals(""))
			txtModeEngagement.setValue(Labels.getLabel("kermel.montantseuils.modeengagement"));
	}
	public void onFocus$txtModeEngagement()
	{
		if(txtModeEngagement.getValue().equalsIgnoreCase(Labels.getLabel("kermel.montantseuils.modeengagement")))
			txtModeEngagement.setValue("");
	
	}
	public void onOK$txtModeEngagement()
	{
		onClick$bchercher();
	}
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
	
}
