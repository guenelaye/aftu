package sn.ssi.kermel.web.referentiel.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCalendrierPaiement;
import sn.ssi.kermel.be.entity.SygCatFournisseur;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDemandePaiement;
import sn.ssi.kermel.be.entity.SygPenaliteRetard;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.ContratSession;
import sn.ssi.kermel.be.session.CalendrierPaiementSession;
import sn.ssi.kermel.be.session.DemandePaiementSession;
import sn.ssi.kermel.be.session.PenaliteRetardSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class ListDemandePaiementController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtNom;
    String nom=null, page=null;
    private Listheader lshNom, lshCategorie;
    String login;
    private SygAutoriteContractante autorite = null;
    
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_DEMANDE_PAIEMENT, MOD_DEMANDE_PAIEMENT,SUP_DEMANDE_PAIEMENT, SUI_DEMANDE_PAIEMENT;
    
    private SygCatFournisseur categorie = null;
    private Bandbox bdCategorie;
	private Paging pgCategorie;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private Listbox lstCategorie;
	private Textbox txtRechercherCategorie;
	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private String categorielibelle = null;
	private Utilisateur infoscompte;
	private SygContrats contrat;
	private String status;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code("DEMANDE_PAIEMENT");
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_DEMANDE_PAIEMENT != null) { ADD_DEMANDE_PAIEMENT.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_DEMANDE_PAIEMENT != null) { MOD_DEMANDE_PAIEMENT.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUP_DEMANDE_PAIEMENT != null) { SUP_DEMANDE_PAIEMENT.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		if (SUI_DEMANDE_PAIEMENT != null) { SUI_DEMANDE_PAIEMENT.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_AUTRE_ACTION1); }
		
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION1, this);
		/*
		//recherche categorie
		lstCategorie.setItemRenderer(new CategoriesRenderer());
   		pgCategorie.setPageSize(byPageBandbox);
   		pgCategorie.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTRE_ACTION);
	
   		  */
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lshNom.setSortAscending(new FieldComparator("datepaiement", false));
		lshNom.setSortDescending(new FieldComparator("datepaiement", true));
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
		 Long idcontrat = (Long) session.getAttribute("contrat");
		   contrat=BeanLocator.defaultLookup(ContratSession.class).findById(idcontrat);
    	login = ((String) getHttpSession().getAttribute("user"));
	}

	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);
	
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)){
			
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygDemandePaiement> demandes = BeanLocator.defaultLookup(DemandePaiementSession.class).find(activePage,byPage,contrat.getConID(),null);
			 SimpleListModel listModel = new SimpleListModel(demandes);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup( DemandePaiementSession.class).count(null,null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/suivimarches/demandepaiement/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE,"Nouvelle demande de paiement");
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(DemandePaiementFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION1)) {
			if (lstListe.getSelectedItem() == null)
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			
			final String uri = "/suivimarches/demandepaiement/formSuivi.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, "Suivi demande");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(SuiviPaiementFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(SuiviPaiementFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListe.getSelectedItem() == null)
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			/* debut traitement */
			SygDemandePaiement demande=BeanLocator.defaultLookup(DemandePaiementSession.class).findById((Long)lstListe .getSelectedItem().getValue());
			if(demande.getSuivi().equalsIgnoreCase("oui"))
			
				throw new WrongValueException(lstListe,"Veuillez selectionner une demande non payer");
			
	
             /* fin  de traitement  */			
			
			final String uri = "/suivimarches/demandepaiement/formEditer.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(DemandePaiementEditerFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(DemandePaiementEditerFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());
			session.setAttribute("demande", lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				
				HashMap<String, String> display = new HashMap<String, String>();
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); 
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				SygDemandePaiement  demande= BeanLocator.defaultLookup(DemandePaiementSession.class).findById(codes);
				SygCalendrierPaiement calen= BeanLocator.defaultLookup(CalendrierPaiementSession.class).findById(demande.getCalendrier().getCalenpaId());
				   calen.setPayer(null);
				   BeanLocator.defaultLookup(CalendrierPaiementSession.class).update(calen);
			     //suppression de penalite
				SygPenaliteRetard penalite = BeanLocator.defaultLookup(PenaliteRetardSession.class).findPenalite(demande.getDemPaId()); 
				if(penalite !=null)
				BeanLocator.defaultLookup(PenaliteRetardSession.class).delete(penalite.getPenId());
				   // fin de traitement
				BeanLocator.defaultLookup(DemandePaiementSession.class).delete(codes);
			//	BeanLocator.defaultLookup(DemandePaiementSession.class).logAction("SUPP_FOURNISSEUR", Labels.getLabel("kermel.referentiel.common.fournisseur.suppression")+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygDemandePaiement demande = (SygDemandePaiement) data;
		item.setValue(demande.getDemPaId());
		item.setAttribute("status", demande.getSuivi());

		Listcell cellDate = new Listcell(String.valueOf(demande.getDatepaiement()));
		cellDate.setParent(item);
		
		Listcell cellRef = new Listcell(demande.getRef());
		cellRef.setParent(item);
		
		
		Listcell cellDateCourrier = new Listcell(ToolKermel.dateToString(demande.getDateCourrier()));
		cellDateCourrier.setParent(item);
		
		
		Listcell cellMontant = new Listcell(ToolKermel.formatDecimal(demande.getMontantpaye()));
		cellMontant.setParent(item);
		
		Listcell cellLivrable = new Listcell(demande.getLivrable());
		cellLivrable.setParent(item);
		
		Listcell cellTauxP = new Listcell(" ");
		if(BeanLocator.defaultLookup(PenaliteRetardSession.class).findPenalite(demande.getDemPaId())!=null)
			cellTauxP = new Listcell(String.valueOf(BeanLocator.defaultLookup(PenaliteRetardSession.class).findPenalite(demande.getDemPaId()).getTaux())+"%");
		cellTauxP.setParent(item);

		Listcell cellDemande = new Listcell("", "/images/cancel.png");
		if ((demande.getSuivi()!=null)&&(demande.getSuivi().equalsIgnoreCase("oui")))
			cellDemande = new Listcell("", "/images/ok.png");
		cellDemande.setParent(item);
		
		
		
	}
	
	///////////Categorie///////// 
	public void onSelect$lstCategorie(){
		categorie= (SygCatFournisseur) lstCategorie.getSelectedItem().getValue();
		bdCategorie.setValue(categorie.getLibelle());
		bdCategorie.close();
	}
	
	public class CategoriesRenderer implements ListitemRenderer{
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygCatFournisseur categorie = (SygCatFournisseur) data;
			item.setValue(categorie);
			
			Listcell cellLibelle = new Listcell("");
			if (categorie.getLibelle()!=null){
				cellLibelle.setLabel(categorie.getLibelle());
			}
			cellLibelle.setParent(item);
	
		}
	}
	
	public void onFocus$txtRechercherCategorie(){
		if(txtRechercherCategorie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.code.categorie"))){
			txtRechercherCategorie.setValue("");
		}		 
	}
	
	public void  onClick$btnRechercherCategorie(){
		if(txtRechercherCategorie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.code.categorie")) || txtRechercherCategorie.getValue().equals("")){
			categorielibelle = null;
			page=null;
		}
		else{
			categorielibelle = txtRechercherCategorie.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}
	
	public void onClick$bchercher()
	{
		categorielibelle = bdCategorie.getValue();
		nom = txtNom.getValue();
		if(categorielibelle.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.code.categorie")) || "".equals(categorielibelle))
			categorie = null;
		
		if(nom.equalsIgnoreCase(Labels.getLabel("kermel.common.form.nom")) || "".equals(nom))
			nom = null;
		
		bdCategorie.setValue(Labels.getLabel("kermel.referentiel.common.code.categorie"));
		txtNom.setValue(Labels.getLabel("kermel.common.form.nom"));
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	
	public void onBlur$txtNom()
	{
		if(txtNom.getValue().equals(""))
			txtNom.setValue(Labels.getLabel("kermel.common.form.nom"));
	}
	public void onFocus$txtNom()
	{
		if(txtNom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.nom")))
			txtNom.setValue("");
	
	}
	public void onOK$txtNom()
	{
		onClick$bchercher();
	}
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
	
	
	public void onSelect$lstListe(){
//		status=(String) lstListe.getSelectedItem().getAttribute("status");
//		
//		System.out.println("##############################OK");
//		if(status.equalsIgnoreCase("oui"))
//		{
//			if(SUI_DEMANDE_PAIEMENT!=null)
//				SUI_DEMANDE_PAIEMENT.setDisabled(true);
//			
//			/*if(WVALID_PLANPASSATION!=null)
//			WVALID_PLANPASSATION.setDisabled(true);
//			if(WWPUB_PLANPASSATION!=null)
//			WWPUB_PLANPASSATION.setDisabled(true);
//			*/
//		} else{
//			
//			if(SUI_DEMANDE_PAIEMENT!=null)
//				SUI_DEMANDE_PAIEMENT.setDisabled(false);
//			
//		}
		
	}
	
	
}
