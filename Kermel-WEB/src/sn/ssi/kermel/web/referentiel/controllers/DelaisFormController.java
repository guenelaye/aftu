package sn.ssi.kermel.web.referentiel.controllers;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygDelais;
import sn.ssi.kermel.be.referentiel.ejb.DelaisSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class DelaisFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle,txtCode,txtUnite,txtCommentaires;
	Long code;
	private	SygDelais delai=new SygDelais();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Datebox dtdebutvalide,dtfinvalide;
	private Intbox intValeur;
	List<SygDelais> delais = new ArrayList<SygDelais>();
	
	@Override
	public void onEvent(Event event) throws Exception {


	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			delai = BeanLocator.defaultLookup(DelaisSession.class).findById(code);
			txtCode.setValue(delai.getCode());
			txtLibelle.setValue(delai.getLibelle());
			intValeur.setValue(delai.getValeur());
			txtUnite.setValue(delai.getUnite());
			dtdebutvalide.setValue(delai.getDatedebutvalidite());
			dtfinvalide.setValue(delai.getDatefinvalidite());
			txtCommentaires.setValue(delai.getCommentaire());
			
		}
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			delai.setCode(txtCode.getValue());
			delai.setLibelle(txtLibelle.getValue());
			delai.setValeur(intValeur.getValue());
			delai.setUnite(txtUnite.getValue());
			delai.setDatedebutvalidite(dtdebutvalide.getValue());
			delai.setDatefinvalidite(dtfinvalide.getValue());
			delai.setCommentaire(txtCommentaires.getValue()); 
			delai.setDate(new Date());
		  	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(DelaisSession.class).save(delai);
				BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_DELAIS", Labels.getLabel("kermel.referentiel.common.delais.ajouter")+" :" + new Date(), login);
				
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(DelaisSession.class).update(delai);
				BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_DELAIS", Labels.getLabel("kermel.referentiel.common.delais.modifier")+" :" + new Date(), login);
				
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			
			

			if(txtCode.getValue().equals(""))
		     {
               errorComponent = txtCode;
               errorMsg = Labels.getLabel("kermel.bureau.douane.code")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			delais=BeanLocator.defaultLookup(DelaisSession.class).find( txtCode.getValue());
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT))
			{
				if(!delai.getCode().equalsIgnoreCase(txtCode.getValue()))
				{
					 if(delais.size()>0)
				     {
		                errorComponent = txtCode;
						errorMsg =Labels.getLabel("kermel.bureau.douane.code")+" "+ Labels.getLabel("kermel.referentiel.existe")+": "+txtCode.getValue();
						lbStatusBar.setStyle(ERROR_MSG_STYLE);
						lbStatusBar.setValue(errorMsg);
						throw new WrongValueException (errorComponent, errorMsg);
				    }
				}
			 
			}
			else
			{
				  if(delais.size()>0)
				  {
					errorComponent = txtCode;
					errorMsg = Labels.getLabel("kermel.bureau.douane.code")+" "+Labels.getLabel("kermel.referentiel.existe")+" :"+txtCode.getValue();
					lbStatusBar.setStyle(ERROR_MSG_STYLE);
					lbStatusBar.setValue(errorMsg);
					throw new WrongValueException (errorComponent, errorMsg);
				  }
			}
			if(txtLibelle.getValue().equals(""))
		     {
             errorComponent = txtLibelle;
             errorMsg = Labels.getLabel("kermel.referentiel.banque.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(intValeur.getValue()==null)
		     {
            errorComponent = intValeur;
            errorMsg = Labels.getLabel("kermel.referentiel.valeur")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtUnite.getValue().equals(""))
		     {
            errorComponent = txtUnite;
            errorMsg = Labels.getLabel("kermel.referentiel.unite")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dtdebutvalide.getValue()==null)
		     {
           errorComponent = dtdebutvalide;
           errorMsg = Labels.getLabel("kermel.referentiel.datedebutvalidite")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(dtfinvalide.getValue()==null)
		     {
          errorComponent = dtfinvalide;
          errorMsg = Labels.getLabel("kermel.referentiel.datefinvalidite")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((dtdebutvalide.getValue()).after(dtfinvalide.getValue()))
			 {
				errorComponent = dtdebutvalide;
				errorMsg =Labels.getLabel("kermel.referentiel.datedebutvalidite")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" : "+Labels.getLabel("kermel.referentiel.datefinvalidite")+": "+UtilVue.getInstance().formateLaDate(dtfinvalide.getValue());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	

}
