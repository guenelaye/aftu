package sn.ssi.kermel.web.referentiel.controllers;
import java.util.Map;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;

import sn.ssi.kermel.be.entity.SygFormateur;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class DetailCabinetController extends AbstractWindow implements
AfterCompose {

	/**
	 * 
	 */

	public static final String PARAM_WINDOW_MODE = "MODE";
	Integer code;
	SygFormateur formateur;
    private Label lblPrenom,lblNom,lblMailcontac,lblTelcontac;
    private Label lblRaison,lblAdd,lblMail,lblTel,lblSpec,lblCom;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	public static final String PARAM_WINDOW_CODE = "CODE";
    private String mode;
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		

		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
//			code = (Integer) map.get(PARAM_WINDOW_CODE);
//	
//			formateur =(SygFormateur) BeanLocator.defaultLookup(FormateurSession.class).findByCode(code);
//		
	       formateur = (SygFormateur) map.get(PARAM_WINDOW_CODE);
 
	       //societe
			if(formateur.getSociete()!=null)
		     lblRaison.setValue(formateur.getSociete());
			if(formateur.getAdsociete()!=null)
		     lblAdd.setValue(formateur.getAdsociete());
			if(formateur.getMailsociete()!=null)
		     lblMail.setValue(formateur.getMailsociete());
			if(formateur.getTelsociete()!=null)
		     lblTel.setValue(formateur.getTelsociete().toString());
			if(formateur.getSpecialite()!=null)
		     lblSpec.setValue(formateur.getSpecialite().getLibelle());
			if(formateur.getCommentaire()!=null)
		     lblCom.setValue(formateur.getCommentaire());
			//personne contact
			if(formateur.getPrenom()!=null)
		     lblPrenom.setValue(formateur.getPrenom());
			if(formateur.getNom()!=null)
		     lblNom.setValue(formateur.getNom());
			if(formateur.getEmail()!=null)
		     lblMailcontac.setValue(formateur.getEmail());
			if(formateur.getTel()!=null)
		    lblTelcontac.setValue(formateur.getTel().toString());
			
      }

	

	public void  onClick$menuFermer(){
		 detach();
		
	}

}
