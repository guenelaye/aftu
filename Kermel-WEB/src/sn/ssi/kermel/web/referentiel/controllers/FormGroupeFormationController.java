package sn.ssi.kermel.web.referentiel.controllers;

import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygGroupeFormation;
import sn.ssi.kermel.be.referentiel.ejb.GroupeFormationSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class FormGroupeFormationController extends AbstractWindow implements
EventListener, AfterCompose{

    public static final String PARAM_WINDOW_CODE = "CODE";

	private Textbox txtLib,txtDesc,txtCod;
	private SygGroupeFormation groupe =new SygGroupeFormation();
	
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String WINDOW_PARAM_MODE = "MODE";

    private String mode;
    Long code;
    Integer id;
    

   @Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
   		}
   	
   
	/**
	 * Permet de gerer les evenements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		
	}
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(WINDOW_PARAM_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			groupe = (SygGroupeFormation) map.get(PARAM_WINDOW_CODE);

		
			txtCod.setValue(groupe.getCode());
			txtLib.setValue(groupe.getLibelle());
			txtDesc.setValue(groupe.getDescription());
			
			} 
		}
	
	
	public void onClick$menuValider() {

		
		if(txtCod.getValue()==null || "".equals(txtCod.getValue())){
			throw new WrongValueException(txtCod, Labels.getLabel("kermel.ChamNull"));
		}
		
		if(txtLib.getValue()==null || "".equals(txtLib.getValue())){
			throw new WrongValueException(txtLib, Labels.getLabel("kermel.ChamNull"));
		}
		
		groupe.setCode(txtCod.getValue());
		groupe.setLibelle(txtLib.getValue());
		groupe.setDescription(txtDesc.getValue());
	
		
		 if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)){
		BeanLocator.defaultLookup(GroupeFormationSession.class).save(groupe);
		    }
		    
		 else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)){
			BeanLocator.defaultLookup(GroupeFormationSession.class).update(groupe);

		}
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		 detach();
	}	

 public void  onClick$menuFermer(){
	
		 detach();
		
	}
}

