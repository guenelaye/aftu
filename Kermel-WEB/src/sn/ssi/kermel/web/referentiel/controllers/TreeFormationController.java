package sn.ssi.kermel.web.referentiel.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Include;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.West;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAnnee;
import sn.ssi.kermel.be.entity.SygMois;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.be.session.AnneeSession;
import sn.ssi.kermel.be.session.MoisSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class TreeFormationController extends AbstractWindow implements
		AfterCompose, IdSpace {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Textbox txtSearch;
	private Include content;
	private Combobox listCategory;
	private Tree tree;
	private Treechildren child = new Treechildren();
    Session session = getHttpSession();
	private SygAnnee region = null;
	private SygMois departement = null;
	private SygProformation formation=null;
	private String openWest;
	private West wp;
	private String libelle=null;
	
	private Combobox combo;
	private Button bchercher;
	Date date = new Date();

	@SuppressWarnings("unchecked")
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		region = (SygAnnee) Executions.getCurrent().getAttribute("annee");
		openWest = (String) Executions.getCurrent().getAttribute("openWest");
		wp.setOpen(true);


		txtSearch.setValue("Recherche");
		initTree();
		if (region != null) {
			chargerSearch();
			wp.setOpen(true);
			onSelect$tree();
		}

	}

	private void initTree() {
		child.setParent(tree);
		treeProcedureBuilder();
		content.setSrc("/referentiel/proformation/list.zul");
		String test = ToolKermel.StringDateToStringSql("11-08-2010");
		System.out.println("beeeeeeeeeeeeeennnnn  :    "+test);
		Date date = ToolKermel.stringToDateSQL(test);
		System.out.println("    ");
		System.out.println("                  date  :    "+date);
		
	}
	

	public void onSelect$tree() {
		tree.isInvalidated();
		Object selobj = tree.getSelectedItem().getValue();

		session.setAttribute("selObj", selobj);
		
		content.setSrc("/referentiel/proformation/page.zul");
		if (selobj instanceof SygAnnee) {

			List<String> status = new ArrayList<String>();
	        region = (SygAnnee) selobj;
			session.setAttribute("annee",region.getId());
			tree.getSelectedItem().setOpen(true);
//			content.setSrc("/referentiel/proformation/list.zul?pageId="	+ "p__@" + tree.getSelectedItem().getId());
//			content.setMode("defer");
		
		} else if (selobj instanceof SygMois) {
			List<String> status = new ArrayList<String>();
			departement = (SygMois) selobj;
			session.setAttribute("mois",departement.getId());
			session.setAttribute("annee",tree.getSelectedItem().getAttribute("annee"));
			content.setSrc("/referentiel/proformation/list.zul?pageId="	+ "p__@" + tree.getSelectedItem().getId());
			content.setMode("defer");			

			
		}
		
		
		
	}

	@SuppressWarnings("unchecked")
	public void onSelect$listCategory() {
		

	}

	public void onClick$search_img() {
		search();
	}

	@SuppressWarnings("unchecked")
	public void search() {
		

	}

	
	
	public void treeBuilder() {
		
	}

	public void treeProcedureBuilder() {
		
		 String key = null;
		
		 List<SygAnnee> annees = BeanLocator.defaultLookup(AnneeSession.class).find(0, -1, libelle);
		 System.out.println("==============================ok");
			
		 for (SygAnnee annee : annees) {
		
			 Treeitem procedureItem = new Treeitem();
			
			 procedureItem.setLabel(annee.getLibelle());
			 procedureItem.setTooltiptext(annee.getLibelle());
			
			 procedureItem.setImage("/images/puce.png");
			 procedureItem.setValue(annee);
			 
			 procedureItem.setOpen(true);
			
			 procedureItem.setParent(child);
			
					 
			 List<SygMois> listesmois = BeanLocator.defaultLookup(MoisSession.class).find(0, -1, null, null, annee);
				
				
			 if(listesmois.size() > 0){
			
				 Treechildren procedureTreechildren = new Treechildren();
				 for (SygMois mois : listesmois) {
					 Treeitem etapeItem = new Treeitem();
				
					 etapeItem.setLabel(mois.getLibelle()+" ("+ BeanLocator.defaultLookup(ProformationSession.class).count( null, mois,annee,-1)+")");
					 etapeItem.setTooltiptext(mois.getLibelle());
					
					 etapeItem.setImage("/images/puce.png");
				
					 etapeItem.setValue(mois);
					 etapeItem.setAttribute("annee", annee.getId());
					 etapeItem.setOpen(false);
					
					 etapeItem.setParent(procedureTreechildren);
					
				
				 }
				 procedureTreechildren.setParent(procedureItem);
			 }
		 }
		 

	}

	public void treeEtapeBuilder() {
		
	}

	public void treeTacheBuilder() {
		
	}

	public void treeOperationBuilder() {
		
	}

	public void removeTreeChildren() {
		while (child.getItemCount() > 0) {
			child.removeChild(child.getFirstChild());
		}
	}



	public void onFocus$txtSearch() {
		if (txtSearch.getValue().equalsIgnoreCase("Recherche")) {
			txtSearch.setValue(null);
		}
	}

	public void onBlur$txtSearch() {
		if (txtSearch.getValue().equalsIgnoreCase("")
				|| (txtSearch.getValue().equalsIgnoreCase(null))) {
			txtSearch.setValue("Recherche");
		}
	}

	public void onOK$txtSearch() {
		
		onClick$btnRechercher();
	}

	
	public void onClick$btnRechercher() {
		if (txtSearch.getValue() != null
				&& !txtSearch.getValue().trim().equals("")) {
			libelle = txtSearch.getValue();
		} else {
			libelle = null;
		}
		chargerSearch();
	}
	
	public void chargerSearch() {
		removeTreeChildren();
		treeProcedureBuilder();

	}
}