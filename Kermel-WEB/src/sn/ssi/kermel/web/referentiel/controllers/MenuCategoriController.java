package sn.ssi.kermel.web.referentiel.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.West;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygCategori;
import sn.ssi.kermel.be.referentiel.ejb.CategoriSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class MenuCategoriController extends AbstractWindow implements
		AfterCompose, IdSpace {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private Include content;
	
	private Tree tree;
	private Treechildren child = new Treechildren();
	private Session session = getHttpSession();
	private SygCategori categori;
	
	private String openWest;
	private West wp;

	List<SygCategori> listCategori = new ArrayList<SygCategori>();
	
	

	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		listCategori = BeanLocator.defaultLookup(CategoriSession.class).findRech(0, -1, null,null,Long.parseLong("1"));
	
		
		openWest = (String) Executions.getCurrent().getAttribute("openWest");
		wp.setOpen(true);

		initTree();


	}

	private void initTree() {
		child.setParent(tree);
		treeProcedureBuilder();

	    	content.setSrc("/referentiel/categorie/liste.zul");
		
	}

	public void onSelect$tree() {
		tree.isInvalidated();
		Object selobj = tree.getSelectedItem().getValue();

		session.setAttribute("selObj", selobj);
		content.invalidate();
		
		if (selobj instanceof SygCategori) {

			List<String> status = new ArrayList<String>();
			categori = (SygCategori) selobj;
			
			
			//if(direction.getDivision().getId()!=5){
			 System.out.println(tree.getSelectedItem().getId());
			 content.setSrc("/referentiel/categorie/liste.zul");
		      //}
		} 

	}

	public void treeProcedureBuilder() {

		String key = null;
		
			System.out.println("==============================ok");
			
			 for (SygCategori cat : listCategori) {
		
				
				 Treeitem debutItem = new Treeitem();
				
				 debutItem.setLabel(cat.getLibelle());
				 debutItem.setTooltiptext(cat.getLibelle());
				 debutItem.setValue(cat);
				 debutItem.setOpen(false);
				
				 debutItem.setParent(child);
		
				 
		 List<SygCategori> listNivo2 = BeanLocator.defaultLookup(
				 CategoriSession.class).findRech(0, -1,null,Long.parseLong("2"),cat.getId());
						
					 if(listNivo2.size() > 0){
					 Treechildren procedureTreechildren = new Treechildren();
					 for (SygCategori service : listNivo2) {
					 Treeitem etapeItem = new Treeitem();
					
					 etapeItem.setLabel(service.getLibelle());
					 etapeItem.setTooltiptext(service.getLibelle());
					 etapeItem.setValue(service);
					 etapeItem.setOpen(false);
					
					 etapeItem.setParent(procedureTreechildren);
					
			
		 List <SygCategori> listNivo3 =BeanLocator.defaultLookup(CategoriSession.class)
		 .findRech(0, -1,null,Long.parseLong("3"),service.getId());
			System.out.println("==============================ok"+listNivo3.size());
			
		 if(listNivo3.size() > 0){
		 Treechildren etapeTreechildren = new Treechildren();
		 for (SygCategori autreservice : listNivo3) {
		 Treeitem tacheItem = new Treeitem();
		
		 tacheItem.setLabel(autreservice.getLibelle());
		 tacheItem.setTooltiptext(autreservice.getLibelle());
		 tacheItem.setValue(autreservice);
		 tacheItem.setOpen(false);
		
		 tacheItem.setParent(etapeTreechildren);
	  
		 }
		 etapeTreechildren.setParent(etapeItem);
		 }
		 }
		 procedureTreechildren.setParent(debutItem);
		 }
	   }
	
	}


	public void removeTreeChildren() {
		while (child.getItemCount() > 0) {
			child.removeChild(child.getFirstChild());
		}
	}


	public void onOpen$tree() {

		System.out.println(tree.getSelectedItem().getId());
	}


	
}
