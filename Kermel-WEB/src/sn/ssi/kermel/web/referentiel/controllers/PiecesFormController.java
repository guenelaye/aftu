package sn.ssi.kermel.web.referentiel.controllers;


import java.util.ArrayList;
import java.util.Map;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPieces;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.PiecesSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;




@SuppressWarnings("serial")
public class PiecesFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,codelocalite,annee;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtcodepiece,txtlibelle,txtdescription,txtlocalisation;
	private SygPieces piece =new SygPieces();
	private SimpleListModel lst;
	private Paging pgPiece;
	private final String ON_LOCALITE = "onLocalite";
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private ArrayList<String> listValeursAnnees;
	Long code;
	private Radio Locale,etrangeres,lesdeux;
	private SygAutoriteContractante autorite = null;
	private Utilisateur infoscompte;

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WIDOW_CODE);
			piece = BeanLocator.defaultLookup(PiecesSession.class) .findById(code);
			
			txtcodepiece.setValue(piece.getCodepiece());
			txtlibelle.setValue(piece.getLibelle());
			txtdescription.setValue(piece.getDescription());
			
			if(piece.getLocalisation().equals("n"))
		   Locale.setSelected(true);
			
			if(piece.getLocalisation().equals("i"))
				etrangeres.setSelected(true);
			
			if(piece.getLocalisation().equals("ni"))
				lesdeux.setSelected(true);
			
		}
			
	}
	public void onOK() {
		
		piece.setCodepiece(txtcodepiece.getValue());
		piece.setLibelle(txtlibelle.getValue());
		piece.setDescription(txtdescription.getValue());
		//piece.setAutorite(autorite);
		if (Locale.isChecked()== true)
			piece.setLocalisation("n");
		
		if (etrangeres.isChecked()== true)
			piece.setLocalisation("i");
		
		if (lesdeux.isChecked()== true)
			piece.setLocalisation("ni");
		
		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			BeanLocator.defaultLookup(PiecesSession.class).save(piece);

		} 
		else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			BeanLocator.defaultLookup(PiecesSession.class).update(piece);
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();

	}
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	
	}
	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onEvent(Event arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

}