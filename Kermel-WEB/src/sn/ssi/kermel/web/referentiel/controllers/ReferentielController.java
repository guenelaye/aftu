package sn.ssi.kermel.web.referentiel.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Listcell;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SysProfilAction;
import sn.ssi.kermel.be.security.ProfilsSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


public class ReferentielController extends AbstractWindow implements
		AfterCompose, EventListener {

	public static final String PARAM_PROFESSEUR_CODE = "CODE";

	private Include idreferentiel;
	Session session = getHttpSession();
	private String reference,profil;
	List<SysProfilAction> actions = new ArrayList<SysProfilAction>();
	private Listcell typeautorite,typeservice,polesdcmp,fonctions,joursferies,garantie,piecesrecus,categorie,secteurs,delais,typesmarches,modepassation,modeselection,critere,
	catFour,eval,monnai,natprix,fournisseur,divisionsmarches,sourcesfinancement,commissionsmarches,
	logisticienformation,groupedeformation,typeRapport,coordinateurs;
	private short allow=1;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		profil=(String) getHttpSession().getAttribute("profil");
		actions=BeanLocator.defaultLookup(ProfilsSession.class).ListeDroits(profil);
		reference=(String) session.getAttribute("reference");
		
		for (int i = 0; i < actions.size(); i++) {
			
			if((actions.get(i).getSysAction().getActCode().equals("ADD_TYPEAC"))||(actions.get(i).getSysAction().getActCode().equals("MOD_TYPEAC"))
					||(actions.get(i).getSysAction().getActCode().equals("SUPP_TYPEAC"))||(actions.get(i).getSysAction().getActCode().equals("WDET_TYPEAC")))
			{
				typeautorite.setVisible(true);
			}
			
			if((actions.get(i).getSysAction().getActCode().equals("ADD_TYPESERVICE"))||(actions.get(i).getSysAction().getActCode().equals("MOD_TYPESERVICE"))
					||(actions.get(i).getSysAction().getActCode().equals("SUPP_TYPESERVICE")))
			{
				typeservice.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_POLEDCMP"))||(actions.get(i).getSysAction().getActCode().equals("MOD_POLEDCMP"))
					||(actions.get(i).getSysAction().getActCode().equals("SUPP_POLEDCMP")))
			{
				polesdcmp.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_FONCTIONS"))||(actions.get(i).getSysAction().getActCode().equals("MOD_FONCTIONS"))
					||(actions.get(i).getSysAction().getActCode().equals("SUPP_FONCTIONS")))
			{
				fonctions.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_JOURSFERIES"))||(actions.get(i).getSysAction().getActCode().equals("MOD_JOURSFERIES"))
					||(actions.get(i).getSysAction().getActCode().equals("SUPP_JOURSFERIES")))
			{
				joursferies.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_GARANTIS"))||(actions.get(i).getSysAction().getActCode().equals("MOD_GARANTIS"))
					||(actions.get(i).getSysAction().getActCode().equals("SUPP_GARANTIS")))
			{
				garantie.setVisible(true);
				piecesrecus.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_CATEGORIES"))||(actions.get(i).getSysAction().getActCode().equals("MOD_CATEGORIES"))
					||(actions.get(i).getSysAction().getActCode().equals("SUPP_CATEGORIES")))
			{
				categorie.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_SECTEURSACTIVITES"))||(actions.get(i).getSysAction().getActCode().equals("MOD_SECTEURSACTIVITES"))
					||(actions.get(i).getSysAction().getActCode().equals("SUPP_SECTEURSACTIVITES")))
			{
				secteurs.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_DELAIS"))||(actions.get(i).getSysAction().getActCode().equals("MOD_DELAIS"))
					||(actions.get(i).getSysAction().getActCode().equals("SUPP_DELAIS")))
			{
				delais.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_TYPESMARCHES"))||(actions.get(i).getSysAction().getActCode().equals("MOD_TYPESMARCHES"))
					||(actions.get(i).getSysAction().getActCode().equals("SUPP_TYPESMARCHES"))||(actions.get(i).getSysAction().getActCode().equals("WMODS_TYPESMARCHES")))
			{
				typesmarches.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_MODEPASSATION"))||(actions.get(i).getSysAction().getActCode().equals("MOD_MODEPASSATION"))
					||(actions.get(i).getSysAction().getActCode().equals("SUPP_MODEPASSATION")))
			{
				modepassation.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_MODESELECTION"))||(actions.get(i).getSysAction().getActCode().equals("MOD_MODESELECTION"))
					||(actions.get(i).getSysAction().getActCode().equals("SUPP_MODESELECTION")))
			{
				modeselection.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_CRITERE"))||(actions.get(i).getSysAction().getActCode().equals("MOD_CRITERE"))
					||(actions.get(i).getSysAction().getActCode().equals("DEL_CRITERE")))
			{
				critere.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_EVA"))||(actions.get(i).getSysAction().getActCode().equals("MOD_EVA"))
					||(actions.get(i).getSysAction().getActCode().equals("SUP_EVA")))
			{
				eval.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_MONNAI"))||(actions.get(i).getSysAction().getActCode().equals("MOD_MONNAI"))
					||(actions.get(i).getSysAction().getActCode().equals("DEL_MONNAI")))
			{
				monnai.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_NAT"))||(actions.get(i).getSysAction().getActCode().equals("MOD_NAT"))
					||(actions.get(i).getSysAction().getActCode().equals("SUP_NAT")))
			{
				natprix.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_FOURNISSEUR"))||(actions.get(i).getSysAction().getActCode().equals("MOD_FOURNISSEUR"))
					||(actions.get(i).getSysAction().getActCode().equals("SUPP_FOURNISSEUR")))
			{
				fournisseur.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_MEMBRE"))||(actions.get(i).getSysAction().getActCode().equals("MOD_MEMBRE"))
					||(actions.get(i).getSysAction().getActCode().equals("SUPP_MEMBRE")))
			{
				divisionsmarches.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_BAILLEUR"))||(actions.get(i).getSysAction().getActCode().equals("MOD_BAILLEUR"))
					||(actions.get(i).getSysAction().getActCode().equals("SUPP_BAILLEUR")))
			{
				sourcesfinancement.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_COMMISSIONSMARCHES"))||(actions.get(i).getSysAction().getActCode().equals("MOD_COMMISSIONSMARCHES"))
					||(actions.get(i).getSysAction().getActCode().equals("SUPP_COMMISSIONSMARCHES")))
			{
				commissionsmarches.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_CATEGORIE"))||(actions.get(i).getSysAction().getActCode().equals("MOD_CATEGORIE"))
					||(actions.get(i).getSysAction().getActCode().equals("SUPP_CATEGORIE")))
			{
				catFour.setVisible(true);
			}
			if((actions.get(i).getSysAction().getActCode().equals("ADD_COORDINNATEUR"))||(actions.get(i).getSysAction().getActCode().equals("MOD_COORDINNATEUR"))
					||(actions.get(i).getSysAction().getActCode().equals("DEL_COORDINNATEUR")))
			{
				coordinateurs.setVisible(true);
			}
			 if((actions.get(i).getSysAction().getActCode().equals("ADD_LOGISTICIEN"))||(actions.get(i).getSysAction().getActCode().equals("MOD_LOGISTICIEN"))
						||(actions.get(i).getSysAction().getActCode().equals("DEL_LOGISTICIEN")))
				{
				 logisticienformation.setVisible(true);
				}
			 if((actions.get(i).getSysAction().getActCode().equals("ADD_GROUPEFORM"))||(actions.get(i).getSysAction().getActCode().equals("MOD_GROUPEFORM"))
						||(actions.get(i).getSysAction().getActCode().equals("DEL_GROUPEFORM")))
				{
				 groupedeformation.setVisible(true);
				}
			 if((actions.get(i).getSysAction().getActCode().equals("ADD_TYPERAP"))||(actions.get(i).getSysAction().getActCode().equals("MOD_TYPERAP"))
						||(actions.get(i).getSysAction().getActCode().equals("DEL_TYPERAP")))
				{
				 typeRapport.setVisible(true);
				}
			 
		}
		if(reference!=null)
		{
			  
				 if((reference.equals("garantie"))||(reference.equals("piecesrecus")))
					{
						
						 idreferentiel.setSrc("/referentiel/piecesrecus/list.zul");
					}
				 else
				    {
					   
					 if((reference.equals("categorie"))||(reference.equals("secteurs")))
						{
							
						 idreferentiel.setSrc("/referentiel/secteursactivites/list.zul");
						}
					 else
					    {
						   
						 if(reference.equals("typesmarches"))
							{
								
							 idreferentiel.setSrc("/referentiel/typesmarches/list.zul");
							}
						 else
						    {
							   
							   idreferentiel.setSrc("/referentiel/typeautorite/list.zul");
						    }
					    }
				    }
			  
		}
		else
		{
			if(typeautorite.isVisible()==true)
			   idreferentiel.setSrc("/referentiel/typeautorite/list.zul");	
		}
		
		
		
		
		  
	}
	
	public void onClick$eval() {
		idreferentiel.setSrc("/referentiel/evaluateur/list.zul");
	}
	public void onClick$typeautorite() {
		idreferentiel.setSrc("/referentiel/typeautorite/list.zul");
	}
	
	public void onClick$critere() {
		idreferentiel.setSrc("/referentiel/critere/list.zul");
	}
	
	public void onClick$catFour() {
		idreferentiel.setSrc("/referentiel/catFournisseur/list.zul");
	}
	
	
	public void onClick$typeservice() {
		idreferentiel.setSrc("/referentiel/typeservice/list.zul");
	}
	public void onClick$polesdcmp() {
		idreferentiel.setSrc("/referentiel/poledcmp/list.zul");
	}
	
	public void onClick$natprix() {
		idreferentiel.setSrc("/referentiel/NatPrix/list.zul");
	}
	
	
	public void onClick$joursferies() {
		
		idreferentiel.setSrc("/referentiel/joursferies/list.zul");
	}
   public void onClick$fonctions() {
		
		idreferentiel.setSrc("/referentiel/fonctions/list.zul");
	}
	public void onClick$garantie() {
		session.setAttribute("reference","garantie");
		loadApplicationState("referentiel");
	}
	
	public void onClick$piecesrecus() {
		session.setAttribute("reference","piecesrecus");
		loadApplicationState("referentiel");
	}
	
	
	 public void onClick$categorie() {
			
		    session.setAttribute("reference","categorie");
			loadApplicationState("referentiel");
		}
	 
	 public void onClick$secteurs() {
		    session.setAttribute("reference","secteurs");
			loadApplicationState("referentiel");
			
		}
	 
	 public void onClick$delais() {
			
			idreferentiel.setSrc("/referentiel/delais/list.zul");
		}
	 public void onClick$typesmarches() {
			
			idreferentiel.setSrc("/referentiel/typesmarches/list.zul");
		}
	 public void onClick$modepassation() {
			
			idreferentiel.setSrc("/referentiel/modepassation/list.zul");
		}
	 public void onClick$modeselection() {
			
			idreferentiel.setSrc("/referentiel/modeselection/list.zul");
		}
	 public void onClick$monnai() {
			
			idreferentiel.setSrc("/referentiel/MonnaiOffre/list.zul");
		}
	 public void onClick$divisionsmarches() {
			
			idreferentiel.setSrc("/referentiel/divisionmarche/list.zul");
		}
	 public void onClick$sourcesfinancement() {
			
			idreferentiel.setSrc("/referentiel/sourcefinancement/list.zul");
		}
	
	 public void onClick$fournisseur() {
			
			idreferentiel.setSrc("/referentiel/fournisseur/list.zul");
		}
	 public void onClick$MembresCommissionMarche() {
			
			idreferentiel.setSrc("/referentiel/commissionsmarches/list.zul");
		}
	 public void onClick$piece() {
			
			idreferentiel.setSrc("/referentiel/pieces/list.zul");
		}
	 public void onClick$commissionsmarches() {
			
			idreferentiel.setSrc("/referentiel/commissionsmarches/list.zul");
		}
	 
	 public void onClick$evaluateur() {
			
			idreferentiel.setSrc("/referentiel/evaluateur/list.zul");
		}
	 public void onClick$coordinateurs() {
			
			idreferentiel.setSrc("/coordonnateurformation/liste.zul");
		}
	 public void onClick$logisticienformation() {
			
			idreferentiel.setSrc("/logisticienformation/liste.zul");
		}
	 public void onClick$groupedeformation() {
			
			idreferentiel.setSrc("/groupedeformation/listegroup.zul");
		}
	 public void onClick$typeRapport() {
			
			idreferentiel.setSrc("/typeRapport/liste.zul");
		}
	 
	
	 public void onClick$delaisdatesrealisations() {
			
			idreferentiel.setSrc("/referentiel/delaisdatesrealisations/form.zu");
		}
	@Override
	public void onEvent(Event arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}


	
	

}
