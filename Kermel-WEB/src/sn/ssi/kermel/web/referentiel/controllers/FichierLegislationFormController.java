package sn.ssi.kermel.web.referentiel.controllers;


import java.util.Calendar;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygCategorieLegislation;
import sn.ssi.kermel.be.entity.SygFichierLegislation;
import sn.ssi.kermel.be.referentiel.ejb.FichierLegislationSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class FichierLegislationFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle;
	Long code;
	private	SygFichierLegislation fichier=new SygFichierLegislation();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	
	private Textbox txtLibelles, txtVersionElectronique,txtDescription;
	private Datebox dtdate;
	private String nomFichier;
	private final String cheminDossier = UIConstants.PATH_ESPACELEGISLATION;
	UtilVue utilVue = UtilVue.getInstance();
	  Session session = getHttpSession();
	
	@Override
	public void onEvent(Event event) throws Exception {


	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			fichier = BeanLocator.defaultLookup(FichierLegislationSession.class).findById(code);
			dtdate.setValue(fichier.getDate());
			txtLibelles.setValue(fichier.getLibelle());
			txtVersionElectronique.setValue(fichier.getFichier());
			txtDescription.setValue(fichier.getDescription());
		
			
		}
	}

	public void onClick$btnChoixFichier() {

		if (ToolKermel.isWindows())
			nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "rap", cheminDossier.replaceAll("/", "\\\\"));
		else
			nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "rap", cheminDossier.replaceAll("\\\\", "/"));

		txtVersionElectronique.setValue(nomFichier);
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			if(txtLibelles.getValue().equals(""))
		     {
               errorComponent = txtLibelle;
               errorMsg = Labels.getLabel("kermel.referentiel.rapport.Libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(dtdate.getValue()==null)
		     {
              errorComponent = dtdate;
              errorMsg = Labels.getLabel("kermel.referentiel.rapport.Date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtVersionElectronique.getValue().equals(""))
		     {
              errorComponent = txtVersionElectronique;
              errorMsg = Labels.getLabel("kermel.referentiel.rapport.Fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}





public void onOK() {
	if(checkFieldConstraints())
	{
		fichier.setLibelle(txtLibelles.getValue()); 
		fichier.setFichier(txtVersionElectronique.getValue());
		fichier.setDate(dtdate.getValue());
		fichier.setDescription(txtDescription.getValue());
		fichier.setStatut(Labels.getLabel("kermel.referentiel.rapport.statut.nopublier"));
		fichier.setCategorie((SygCategorieLegislation)session.getAttribute("categorie"));
		
	  	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			BeanLocator.defaultLookup(FichierLegislationSession.class).save(fichier);
			//BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_RAPARMP", Labels.getLabel("kermel.referentiel.common.rapport.ajouter")+" :" + new Date(), login);
			
		} 
		else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			BeanLocator.defaultLookup(FichierLegislationSession.class).update(fichier);
			//BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_RAPARMP", Labels.getLabel("kermel.referentiel.common.rapport.modifier")+" :" + new Date(), login);
			
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		loadApplicationState("list_texte_reglementaire");
		detach();
	}
}
}
