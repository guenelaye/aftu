package sn.ssi.kermel.web.referentiel.controllers;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.media.Media;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Fileupload;
import org.zkoss.zul.Image;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPoledcmp;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteContractanteSession;
import sn.ssi.kermel.be.referentiel.ejb.PoledcmpSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeAutoriteContractanteSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class ACFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 *  
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtSigle, txtDenomination,txtAdresse,txtTelephone,txtFax,txtEmail,txtSiteWeb,txtResponsable,txtCodeTypeAutorite,txtRechercherTypeAutorite;
	Long code;
	private	SygAutoriteContractante autorite=new SygAutoriteContractante();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Intbox intordre;
	private Bandbox bdPole;
	private Paging pgPole,pgTypeAutorite;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE,activePage;
	private String libellepole = null,page=null,codetype=null,libelletype=null;
	private Listbox lstPole,lstTypeAutorite;
	private Textbox txtRechercherPole;
	SygPoledcmp pole=null;
	SygTypeAutoriteContractante type=new SygTypeAutoriteContractante();
	private Long idtype;
    private Label lblType;
    Session session = getHttpSession();
	private Image imagePhoto;
	private boolean photoChanged;
	private String photoName;
	private org.zkoss.image.Image img = null;
	List<SygAutoriteContractante> autorites = new ArrayList<SygAutoriteContractante>();
	private Div step0,step1;
	
	@Override
	public void onEvent(Event event) throws Exception {
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgPole.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPole.getActivePage() * byPageBandbox;
				pgPole.setPageSize(byPageBandbox);
			}
			List<SygPoledcmp> poles = BeanLocator.defaultLookup(PoledcmpSession.class).find(activePage, byPageBandbox,null,libellepole);
			lstPole.setModel(new SimpleListModel(poles));
			pgPole.setTotalSize(BeanLocator.defaultLookup(PoledcmpSession.class).count(null,libellepole));
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_TYPES)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgTypeAutorite.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgTypeAutorite.getActivePage() * byPageBandbox;
				pgTypeAutorite.setPageSize(byPageBandbox);
			}
			List<SygTypeAutoriteContractante> types = BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).find(activePage, byPageBandbox, codetype, libelletype);
			lstTypeAutorite.setModel(new SimpleListModel(types));
			pgTypeAutorite.setTotalSize(BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).count(codetype, libelletype));
		}
	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		

		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		lstPole.setItemRenderer(new PolesRenderer());
		pgPole.setPageSize(byPageBandbox);
		pgPole.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
		addEventListener(ApplicationEvents.ON_TYPES, this);
		lstTypeAutorite.setItemRenderer(new TypesAutorites());
		pgTypeAutorite.setPageSize(byPageBandbox);
		pgTypeAutorite.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_TYPES);
		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			autorite = BeanLocator.defaultLookup(AutoriteContractanteSession.class).findById(code);
			txtDenomination.setValue(autorite.getDenomination());
			txtSigle.setValue(autorite.getSigle());
			if(autorite.getPole()!=null)
			{
			    pole=autorite.getPole();
			    bdPole.setValue(pole.getLibelle());
			}
			txtAdresse.setValue(autorite.getAdresse());
			txtTelephone.setValue(autorite.getTelephone());
			txtFax.setValue(autorite.getFax());
			txtEmail.setValue(autorite.getEmail());
			txtSiteWeb.setValue(autorite.getUrlsiteweb());
			txtResponsable.setValue(autorite.getResponsable());
			intordre.setValue(autorite.getOrdre());
			type=autorite.getType();
			if ((autorite.getLogo() != null) && !autorite.getLogo().trim().equals("")) {
				img = fetchImage(UIConstants.PATH_PHOTOS, autorite.getLogo());
			} else {
				img = fetchImage(UIConstants.PATH_PHOTOS, UIConstants.PHOTO_LOGO_DEFAULT);
			}
		}
		else
		{
			    img = fetchImage(UIConstants.PATH_PHOTOS, UIConstants.PHOTO_LOGO_DEFAULT);
		}
		imagePhoto.setContent(img);
		Events.postEvent(ApplicationEvents.ON_TYPES, this, null);
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			autorite.setDenomination(txtDenomination.getValue());
			autorite.setSigle(txtSigle.getValue());  
			autorite.setAdresse(txtAdresse.getValue());
			autorite.setOrdre(intordre.getValue());
			autorite.setTelephone(txtTelephone.getValue());
			autorite.setFax(txtFax.getValue());
			autorite.setEmail(txtEmail.getValue());
			autorite.setUrlsiteweb(txtSiteWeb.getValue());
			autorite.setResponsable(txtResponsable.getValue());
			autorite.setType(type);
			//autorite.setCertificat("personnal_nyal.cer");
			if (photoChanged && (imagePhoto.getContent() != null)) 
			{
				autorite.setLogo(photoName);
				 savePhoto();
			}
			if(pole!=null)
				autorite.setPole(pole);
	     	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(AutoriteContractanteSession.class).save(autorite);
				BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_AC", Labels.getLabel("kermel.referentiel.common.autorite.ajouter")+" :" + new Date(), login);
				
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(AutoriteContractanteSession.class).update(autorite);
				BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_AC", Labels.getLabel("kermel.referentiel.common.autorite.modifier")+" :" + new Date(), login);
				
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			if(txtDenomination.getValue().equals(""))
		     {
               errorComponent = txtDenomination;
               errorMsg = Labels.getLabel("kermel.referentiel.common.denomination")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			

			if(txtSigle.getValue().equals(""))
		     {
               errorComponent = txtSigle;
               errorMsg = Labels.getLabel("kermel.referentiel.common.sigle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
//			if(txtAdresse.getValue().equals(""))
//		     {
//              errorComponent = txtAdresse;
//              errorMsg = Labels.getLabel("kermel.courriers.adresse")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
//				lbStatusBar.setStyle(ERROR_MSG_STYLE);
//				lbStatusBar.setValue(errorMsg);
//				throw new WrongValueException (errorComponent, errorMsg);
//		     }
//			if(txtTelephone.getValue().equals(""))
//		     {
//             errorComponent = txtTelephone;
//             errorMsg = Labels.getLabel("kermel.referentiel.personne.telephone")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
//				lbStatusBar.setStyle(ERROR_MSG_STYLE);
//				lbStatusBar.setValue(errorMsg);
//				throw new WrongValueException (errorComponent, errorMsg);
//		     }
//			if(txtEmail.getValue().equals(""))
//		     {
//            errorComponent = txtEmail;
//            errorMsg = Labels.getLabel("kermel.referentiel.personne.email")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
//				lbStatusBar.setStyle(ERROR_MSG_STYLE);
//				lbStatusBar.setValue(errorMsg);
//				throw new WrongValueException (errorComponent, errorMsg);
//		     }
//			if (ToolKermel.ControlValidateEmail(txtEmail.getValue())) {
//				
//			} else {
//				errorComponent = txtEmail;
//                errorMsg = Labels.getLabel("kermel.referentiel.personne.email.incorrect");
// 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
// 				lbStatusBar.setValue(errorMsg);
// 				throw new WrongValueException(errorComponent,errorMsg);
//			}
// 			
//			if(intordre.getValue()==null)
//		     {
//               errorComponent = intordre;
//               errorMsg = Labels.getLabel("kermel.referentiel.common.ordre")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
//				lbStatusBar.setStyle(ERROR_MSG_STYLE);
//				lbStatusBar.setValue(errorMsg);
//				throw new WrongValueException (errorComponent, errorMsg);
//		     }
//			autorites=BeanLocator.defaultLookup(AutoriteContractanteSession.class).find(intordre.getValue());
//			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT))
//			{
//				if(autorite.getOrdre()!=intordre.getValue())
//				{
//					 if(autorites.size()>0)
//				     {
//		                errorComponent = intordre;
//						errorMsg = Labels.getLabel("kermel.referentiel.common.ordre")+" "+Labels.getLabel("kermel.referentiel.date.existe")+": "+intordre.getValue();
//						lbStatusBar.setStyle(ERROR_MSG_STYLE);
//						lbStatusBar.setValue(errorMsg);
//						throw new WrongValueException (errorComponent, errorMsg);
//				    }
//				}
//			 
//			}
//			else
//			{
//				  if(autorites.size()>0)
//				  {
//					errorComponent = intordre;
//					errorMsg =Labels.getLabel("kermel.referentiel.common.ordre")+" "+ Labels.getLabel("kermel.referentiel.existe")+" :"+intordre.getValue();
//					lbStatusBar.setStyle(ERROR_MSG_STYLE);
//					lbStatusBar.setValue(errorMsg);
//					throw new WrongValueException (errorComponent, errorMsg);
//				  }
//			}
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	

	///////////Pole///////// 
	public void onSelect$lstPole(){
	pole= (SygPoledcmp) lstPole.getSelectedItem().getValue();
	bdPole.setValue(pole.getLibelle());
	bdPole.close();
	
	}

public class PolesRenderer implements ListitemRenderer{
	


	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygPoledcmp pole = (SygPoledcmp) data;
		item.setValue(pole);
		
		
		Listcell cellLibelle = new Listcell("");
		if (pole.getLibelle()!=null){
			cellLibelle.setLabel(pole.getLibelle());
		}
		cellLibelle.setParent(item);

	}
}
	public void onFocus$txtRechercherPole(){
	if(txtRechercherPole.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.pole"))){
		txtRechercherPole.setValue("");
	}		 
	}
	
	public void  onClick$btnRechercherPole(){
	if(txtRechercherPole.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.pole")) || txtRechercherPole.getValue().equals("")){
		libellepole = null;
		page=null;
	}else{
		libellepole = txtRechercherPole.getValue();
		page="0";
	}
	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	private Boolean savePhoto() {

		try {
			if (photoChanged && (imagePhoto.getContent() != null)) {

				System.out.println(UIConstants.PATH_PHOTOS + autorite.getLogo());
				// String filepath =
				// Sessions.getCurrent().getWebApp().getRealPath(UIConstants.PATH_AGT_PHOTOS
				// + dossagent.getAgtPhoto());
				

				String filepath = UIConstants.PATH_PHOTOS + autorite.getLogo();
				System.out.println(imagePhoto.getSrc() + " source image ################"+filepath);
				System.out.println(filepath.replaceAll("/", "\\\\"));

				// windows
				// File f = new File(filepath.replaceAll("/", "\\\\"));
				// linux
				File f = new File(filepath.replaceAll("/", "\\\\"));

				if (f.exists()) {
					f.delete();
				}
				f.createNewFile();

				InputStream inputStream = imagePhoto.getContent().getStreamData();
				OutputStream out = new FileOutputStream(f);
				byte buf[] = new byte[1024];

				int len;
				while ((len = inputStream.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				out.close();
				inputStream.close();
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	public org.zkoss.image.Image fetchImage(String path, String file) {
		File imgfile = null;
		// windows
		// File imgfile = new File(path.replaceAll("/", "\\\\") + file);
		// linux
		if (ToolKermel.isWindows())
			imgfile = new File(path.replaceAll("/", "\\\\") + file);
		else
			imgfile = new File(path.replaceAll("\\\\", "/") + file);// linux

		org.zkoss.image.Image img = null;
		try {
			img = new org.zkoss.image.AImage(imgfile);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return (org.zkoss.image.Image) img;
	}

	public void onClick$buttonTelecharger() {

		try {

			Media media = Fileupload.get();

			if (media instanceof org.zkoss.image.Image) {

				imagePhoto.setContent((org.zkoss.image.Image) media);
				photoName = generatePhotoName() + "." + ((org.zkoss.image.Image) media).getFormat();
				System.out.println(photoName + "ff" + ((org.zkoss.image.Image) media).getFormat());
				photoChanged = true;

			} else if (media != null) {

				alert("Ok");
			}

		} catch (Exception e) {

		}

	}

	private String generatePhotoName() {

		try {
			return "photo_"
					+ txtDenomination.getValue().replace('\\', '-').replace('/', '-').replace(':', '-').replace('*', '-').replace('?', '-').replace('\"', '-').replace('<', '-').replace(
							'>', '-').replace('|', '-');
		} catch (Exception e) {
			return null;
		}

	}
	
	
	 public class TypesAutorites implements ListitemRenderer{
			
			
			
			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
				SygTypeAutoriteContractante types = (SygTypeAutoriteContractante) data;
				item.setValue(types);
				
				Listcell cellCode = new Listcell(types.getCode());
				cellCode.setParent(item);
			
				Listcell cellLibelle = new Listcell(types.getLibelle());
				cellLibelle.setParent(item);
				if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
					if(autorite.getType().getId().intValue()==types.getId().intValue());
					 item.setSelected(true);
				}
			}
		}
	 
	 public void onBlur$txtCodeTypeAutorite(){
			if(txtCodeTypeAutorite.getValue().equalsIgnoreCase("")){
				txtCodeTypeAutorite.setValue(Labels.getLabel("kermel.referentiel.common.autorisation.code"));
			}		 
		}


		public void onFocus$txtCodeTypeAutorite(){
			if(txtCodeTypeAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.autorisation.code"))){
				txtCodeTypeAutorite.setValue("");
			}		 
		}

		public void onOK$txtCodeTypeAutorite(){
			onClick$btnRechercherTypeAutorite();
			 
		}

		 public void onBlur$txtRechercherTypeAutorite(){
				if(txtRechercherTypeAutorite.getValue().equalsIgnoreCase("")){
					txtRechercherTypeAutorite.setValue(Labels.getLabel("kermel.autoritecontractante.libelle"));
				}		 
			}

	
	public void onFocus$txtRechercherTypeAutorite(){
		if(txtRechercherTypeAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.autoritecontractante.libelle"))){
			txtRechercherTypeAutorite.setValue("");
		}		 
	}
		
		public void  onClick$btnRechercherTypeAutorite(){
			String page=null;
			if(txtRechercherTypeAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.autoritecontractante.libelle")) || txtRechercherTypeAutorite.getValue().equals(""))
			   {
				  libelletype = null;
			   }
			else
			   {
				libelletype= txtRechercherTypeAutorite.getValue();
				  page="0";
			   }
			if(txtCodeTypeAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.autorisation.code")) || txtCodeTypeAutorite.getValue().equals(""))
			   {
				  codetype = null;
			   }
			else
			   {
				codetype= txtCodeTypeAutorite.getValue();
				  page="0";
			   }
		     Events.postEvent(ApplicationEvents.ON_TYPES, this, page);
		}
		
		public void onClick$menuNext(){
			if (lstTypeAutorite.getSelectedItem() == null)
				throw new WrongValueException(lstTypeAutorite, Labels.getLabel("kermel.error.select.item"));
			
			type=(SygTypeAutoriteContractante) lstTypeAutorite.getSelectedItem().getValue();
			lblType.setValue(type.getLibelle());
			step0.setVisible(false);
			step1.setVisible(true);
			
		}
		public void onClick$menuPrevious(){
			step0.setVisible(true);
			step1.setVisible(false);
		}
		public void onClick$menuCancel(){
			detach();
		}
		
		public void onClick$menuFermer(){
			detach();
		}
		
}
