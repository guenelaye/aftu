package sn.ssi.kermel.web.referentiel.controllers;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygDecision;
import sn.ssi.kermel.be.entity.SygTypeDecision;
import sn.ssi.kermel.be.referentiel.ejb.DecisionSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeDecisionSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class DecisionFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle;
	Long code;
	private	SygDecision decision=new SygDecision();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	
	
	// /Type decision
	private SygTypeDecision typedecision = new SygTypeDecision();
	private Textbox txtRechercherType;
	private Bandbox bdType;
	private Paging pgType;
	private Listbox lstType;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private String LibelleType = null;

	@Override
	public void onEvent(Event event) throws Exception {

		// champ typedecision du formulaire
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_TYPEDECISION)) {
			List<SygTypeDecision> typedecision = BeanLocator.defaultLookup(TypeDecisionSession.class).find(
					pgType.getActivePage() * byPageBandbox, byPageBandbox, null, LibelleType);
			SimpleListModel listModel = new SimpleListModel(typedecision);
			lstType.setModel(listModel);
			pgType.setTotalSize(BeanLocator.defaultLookup(TypeDecisionSession.class).count(null, LibelleType));
		}
		
	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		// champ Type decision du formulaire
		addEventListener(ApplicationEvents.ON_TYPEDECISION, this);
		lstType.setItemRenderer(new typedecisionRenderer());
		pgType.setPageSize(byPageBandbox);
		pgType.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_TYPEDECISION);
		Events.postEvent(ApplicationEvents.ON_TYPEDECISION, this, null);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			decision = BeanLocator.defaultLookup(DecisionSession.class).findById(code);
			txtLibelle.setValue(decision.getLibelle());
			bdType.setValue(decision.getTypedecision().getLibelle());
		
			
		}
	}
	

	public void onOK() {
		if(checkFieldConstraints())
		{
			
			if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				typedecision = (SygTypeDecision) lstType.getSelectedItem().getValue();

			} else {
				if (bdType.getValue().equals(decision.getTypedecision().getLibelle())) {
					typedecision = decision.getTypedecision();
				} else {
					typedecision = (SygTypeDecision) lstType.getSelectedItem().getValue();
				}
			}
			decision.setTypedecision(typedecision);
			decision.setLibelle(txtLibelle.getValue());  
		  	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(DecisionSession.class).save(decision);
				BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_DECISION", Labels.getLabel("kermel.referentiel.common.tdecision.ajouter")+" :" + new Date(), login);
				
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(DecisionSession.class).update(decision);
				BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_DECISION", Labels.getLabel("kermel.referentiel.common.decision.modifier")+" :" + new Date(), login);
				
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			if(txtLibelle.getValue().equals(""))
		     {
               errorComponent = txtLibelle;
               errorMsg = Labels.getLabel("kermel.referentiel.decision.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(bdType.getValue().equals(""))
		     {
              errorComponent = bdType;
              errorMsg = Labels.getLabel("kermel.referentiel.decision.typedecision")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			

			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	

////////////type decision
public void onSelect$lstType() {
	bdType.setValue((String) lstType.getSelectedItem().getAttribute(UIConstants.ATTRIBUTE_LIBELLE));
	bdType.close();
}

public class typedecisionRenderer implements ListitemRenderer {

	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygTypeDecision type = (SygTypeDecision) data;
		item.setValue(type);
		item.setAttribute(UIConstants.ATTRIBUTE_LIBELLE, type.getLibelle());

		Listcell cellLibelle = new Listcell("");
		if (type.getLibelle() != null) {
			cellLibelle.setLabel(type.getLibelle());
		}
		cellLibelle.setParent(item);
		//				

	}

	
}

public void onFocus$txtRechercherType() {
	if (txtRechercherType.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.decision.typedecision"))) {
		txtRechercherType.setValue("");
	}
}

public void onBlur$txtRechercherType() {
	if (txtRechercherType.getValue().equalsIgnoreCase("")) {
		txtRechercherType.setValue(Labels.getLabel("kermel.referentiel.decision.typedecision"));
	}
}

public void onClick$btnRechercherType() {
	if (txtRechercherType.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.decision.typedecision")) || txtRechercherType.getValue().equals("")) {
		LibelleType = null;
	} else {
		LibelleType = txtRechercherType.getValue();
	}
	Events.postEvent(ApplicationEvents.ON_TYPEDECISION, this, null);
}

// ////////////////fin ////////////////////////////////

@Override
public void render(Listitem arg0, Object arg1, int index) throws Exception {
	// TODO Auto-generated method stub
	
}

}
