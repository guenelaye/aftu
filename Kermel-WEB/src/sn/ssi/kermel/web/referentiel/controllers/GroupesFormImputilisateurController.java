package sn.ssi.kermel.web.referentiel.controllers;


import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
//import sn.ssi.ecourrier.be.entity.EcoContactsCarnet;
//import sn.ssi.ecourrier.be.entity.EcoDossiers;
//import sn.ssi.ecourrier.be.entity.EcoGroupesCarnet;
import sn.ssi.kermel.be.entity.EcoGroupesImputation;
import sn.ssi.kermel.be.entity.EcoGroupesImputilisateur;
import sn.ssi.kermel.be.entity.Utilisateur;
//import sn.ssi.kermel.be.carnet.ejb.ContactsCarnetSession;
//import sn.ssi.kermel.be.carnet.ejb.GroupesCarnetSession;
//import sn.ssi.kermel.be.referentiel.ejb.DossiersSession;
import sn.ssi.kermel.be.referentiel.ejb.GroupeImputationSession;
import sn.ssi.kermel.be.referentiel.ejb.GroupeImpututilisateurSession;
import sn.ssi.kermel.be.security.UtilisateurSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class GroupesFormImputilisateurController extends AbstractWindow implements
		EventListener, AfterCompose,ListitemRenderer  {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtraison,txtAdresse, txtEmail,txtfax,txtdescriptions,txtgroupeimputation,txtutilisateur;
	private Intbox txtTel;
	Long code;
	//private	EcoContactsCarnet contact = new EcoContactsCarnet();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	//List<EcoContactsCarnet> contacts = new ArrayList<EcoContactsCarnet>();
	
	private Bandbox bandgroupeimputation,bandutilisateur;
	private Textbox txtRechercherGroupe,txtlibelle,txtDescription,txtRecherchergroupeimputation,txtRechercherutilisateur;
	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private Paging pgGroupe,pg;
	private Listbox listutilisateur,listgroupeimputation;
	private int activePage;
	//private EcoDossiers dossiers= new EcoDossiers();
	private EcoGroupesImputilisateur GroupesImputilisateur = new EcoGroupesImputilisateur();
	private String groupeLibelle;
	private String page = null;
	private EcoGroupesImputation GroupesImputation; 
	private Utilisateur utilisateur;
	private Component errorcomponent;
	private String errorMessage;
	//private Label lbStatusBar;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		listgroupeimputation.setItemRenderer(new GroupeRenderer());
		pg.setPageSize(byPageBandBox);
		pg.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
		listutilisateur.setItemRenderer(this);
		pg.setPageSize(byPageBandBox);
		pg.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		
//		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
//   		lstGroupe.setItemRenderer(new GroupeRenderer());
//   		pgGroupe.setPageSize(byPageBandBox);
//   		pgGroupe.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTRE_ACTION);
//   		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			//contact = BeanLocator.defaultLookup(ContactsCarnetSession.class).findById(code);
			GroupesImputation = BeanLocator.defaultLookup(GroupeImputationSession.class).findById(GroupesImputilisateur.getGroupesImputation().getId());
			bandgroupeimputation.setValue(GroupesImputation.getLibelle());
			
			utilisateur = BeanLocator.defaultLookup(UtilisateurSession.class).findByCode(GroupesImputilisateur.getUtilisateur().getId());
			bandutilisateur.setValue(GroupesImputation.getLibelle());
			
			txtlibelle.setValue(GroupesImputilisateur.getLibelle());
			txtdescriptions.setValue(GroupesImputilisateur.getDescription());
//			txtutilisateur.setValue(GroupesImputilisateur.getUtilisateur().getNom());
//			txtgroupeimputation.setValue(GroupesImputilisateur.getGroupesImputation().getLibelle());
			
			
			
			
			//bdGroupe.setValue(contact.getGroupe().getLibelle());
		}
	}
	
//	@Override
//	public void onEvent(Event event) throws Exception {
//		// TODO Auto-generated method stub
//		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)){
//			if (event.getData() != null) {
//				activePage = Integer.parseInt((String) event.getData());
//				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
//				pgGroupe.setPageSize(byPageBandBox);
//			} 
//			else {
//				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
//				activePage = pgGroupe.getActivePage() * byPageBandBox;
//				pgGroupe.setPageSize(byPageBandBox);
//			}
//			List<EcoGroupesCarnet> grpe = BeanLocator.defaultLookup(GroupesCarnetSession.class).find(activePage, byPageBandBox, null, groupeLibelle);
//			lstGroupe.setModel(new SimpleListModel(grpe));
//			pgGroupe.setTotalSize(BeanLocator.defaultLookup(GroupesCarnetSession.class).count(null, groupeLibelle));
//		}
//	}

	public void onOK() {
		if(checkFieldConstraints())
		{
//			if (lstGroupe.getSelectedItem() == null)
//				throw new WrongValueException(bdGroupe, Labels.getLabel("ecourrier.erreur.champobligatoire"));
			
			GroupesImputilisateur.setLibelle(txtlibelle.getValue());
			GroupesImputilisateur.setDescription(txtdescriptions.getValue());
//			GroupesImputilisateur.setGroupesImputation(txtgroupeimputation.getValue());
//			GroupesImputilisateur.setUtilisateur(txtutilisateur.getValue());
			
			if (listutilisateur.getSelectedItem() == null) {
				errorMessage = "Veuillez selectionner un utilisateur!";
				errorcomponent = bandutilisateur;
				throw new WrongValueException(errorcomponent, errorMessage);
	
			}
			GroupesImputilisateur.setUtilisateur((Utilisateur) listutilisateur.getSelectedItem().getValue());
			
			
			if (listgroupeimputation.getSelectedItem() == null) {
				errorMessage = "Veuillez selectionner un groupe!";
				errorcomponent = bandgroupeimputation;
				throw new WrongValueException(errorcomponent, errorMessage);
	
			}
			GroupesImputilisateur.setGroupesImputation((EcoGroupesImputation) listgroupeimputation.getSelectedItem().getValue());
			
			
			
		  	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(GroupeImpututilisateurSession.class).save(GroupesImputilisateur);
				//BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_CONTACTPRIVE", Labels.getLabel("ecourrier.referentiel.common.groupe.ajouter")+" :" + UtilVue.getInstance().formateLaDate(new Date()), login);
				
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(GroupeImpututilisateurSession.class).update(GroupesImputilisateur);
				//BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_CONTACTPRIVE", Labels.getLabel("ecourrier.referentiel.common.groupe.modifier")+" :" + UtilVue.getInstance().formateLaDate(new Date()), login);
				
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}
	
	//Groupe
		public void onSelect$listgroupeimputation(){
			GroupesImputation = (EcoGroupesImputation) listgroupeimputation.getSelectedItem().getValue();
			bandgroupeimputation.setValue(GroupesImputation.getLibelle());
			
			bandgroupeimputation.close();
		}
		
		public void onSelect$listutilisateur(){
			utilisateur = (Utilisateur) listutilisateur.getSelectedItem().getValue();
			bandutilisateur.setValue(utilisateur.getNom());
			
			bandutilisateur.close();
		}
		
		public class GroupeRenderer implements ListitemRenderer {

			@Override
			public void render(Listitem item, Object data, int index) 
					throws Exception {
				EcoGroupesImputation GroupesImputation = (EcoGroupesImputation) data;
				item.setValue(GroupesImputation);
				
				Listcell cellLibelle = new Listcell(GroupesImputation.getLibelle());
				cellLibelle.setParent(item);				
			}
			
		}
		
		
		public class UtilisateursRenderer implements ListitemRenderer {

			@Override
			public void render(Listitem item, Object data, int index) 
					throws Exception {
				Utilisateur utilisateur = (Utilisateur) data;
				item.setValue(utilisateur);
				
				Listcell cellnom = new Listcell(utilisateur.getNom());
				cellnom.setParent(item);				
			}
			
		}
		
		
		
		public void onFocus$txtRecherchergroupeimputation(){
			 if(txtRecherchergroupeimputation.getValue().equalsIgnoreCase(Labels.getLabel("ecourrier.common.form.rechercher"))){
				 txtRecherchergroupeimputation.setValue("");
			 }		 
		}
		
		
		public void onFocus$txtRechercherutilisateur(){
			 if(txtRechercherutilisateur.getValue().equalsIgnoreCase(Labels.getLabel("ecourrier.common.form.rechercher"))){
				 txtRechercherutilisateur.setValue("");
			 }		 
		}
		
		public void  onClick$btnRecherchergroupeimputation(){
			if(txtRecherchergroupeimputation.getValue().equalsIgnoreCase(Labels.getLabel("ecourrier.common.form.rechercher")))
			{
				groupeLibelle = null;
				page = null;
			}
			else
			{
				groupeLibelle = txtRecherchergroupeimputation.getValue();
				page="0";
			}
				
			Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
		}

		public void  onClick$bandutilisateur(){
			if(txtRechercherutilisateur.getValue().equalsIgnoreCase(Labels.getLabel("ecourrier.common.form.rechercher")))
			{
				groupeLibelle = null;
				page = null;
			}
			else
			{
				groupeLibelle = txtRechercherutilisateur.getValue();
				page="0";
			}
				
			Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
		}

	//contr�le erreurs
	private boolean checkFieldConstraints() {
		
		try {
		
			if(txtlibelle.getValue().equals(""))
		     {
               errorComponent = txtlibelle;
               errorMsg = Labels.getLabel("ecourrier.referentiel.common.libelle")+": "+Labels.getLabel("ecourrier.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }

			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("ecourrier.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

	@Override
	public void onEvent(Event arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub
		
	}
	

}
