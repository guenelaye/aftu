package sn.ssi.kermel.web.referentiel.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygSpecialite;
import sn.ssi.kermel.be.referentiel.ejb.SpecialiteSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class ListSpecialiteController extends AbstractWindow implements EventListener, AfterCompose, ListitemRenderer {

	private Listbox list;
	private Paging pg;
	public static final String PARAM_WINDOW_CODE = "CODE";

	private Listheader lshLib;
	
	private Textbox txtLib,txtCode;
	private SygSpecialite spec =new SygSpecialite();

	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private int activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	public static final String WINDOW_PARAM_MODE = "MODE";
	private String page=null,lib,code;
	Session session = getHttpSession();

	
	private KermelSousMenu monSousMenu;
	private Menuitem ADD_SPECIALITE, MOD_SPECIALITE, DEL_SPECIALITE;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_SPECIALITE);
		monSousMenu.afterCompose();
		Components.wireFellows(this, this);
		if (ADD_SPECIALITE != null) {
			ADD_SPECIALITE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
		}
		if (MOD_SPECIALITE != null) {
			MOD_SPECIALITE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
		}
		if (DEL_SPECIALITE != null) {
			DEL_SPECIALITE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
		}
	
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	
		
		lshLib.setSortAscending(new FieldComparator("libelle", false));
		lshLib.setSortDescending(new FieldComparator("libelle", true));
		list.setItemRenderer(this);
		pg.setPageSize(byPage);
		pg.addForward("onPaging", this,ApplicationEvents.ON_MODEL_CHANGE);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

   	
	}

	/**
	 * Permet de gerer les evenements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet evenement survient, les evenements de la liste sont mis �
		 * jour de meme que l'element de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
 			 List<SygSpecialite> spec = BeanLocator.defaultLookup(SpecialiteSession.class).find(pg.getActivePage()*byPage,byPage,code,lib);

 			 SimpleListModel listModel = new SimpleListModel(spec);
			list.setModel(listModel);
			pg.setTotalSize(BeanLocator.defaultLookup(SpecialiteSession.class).count(code,lib));
		} 
	
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/specialite/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.spec.ajout"));
			display.put(DSP_HEIGHT,"80%");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormSpecialiteController.WINDOW_PARAM_MODE,UIConstants.MODE_NEW);
			
			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				throw new WrongValueException(list, Labels.getLabel("kermel.referentiel.nat.message.selection"));
				
			}
			final String uri ="/specialite/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"80%");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.spec.modif"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormSpecialiteController.WINDOW_PARAM_MODE,UIConstants.MODE_EDIT);
		    data.put(FormSpecialiteController.PARAM_WIDOW_CODE, list.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
					throw new WrongValueException(list, Labels.getLabel("kermel.referentiel.nat.message.selection"));
				HashMap<String, String> display = new HashMap<String, String>(); 
			
				// permet de fixer les dimensions du popup
			
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.referentiel.caractere.message.suppression"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.referentiel.caractere.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// permet de passer des parametres au popup
				
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){		
			for (int i = 0; i < list.getSelectedCount(); i++) {
				spec = (SygSpecialite)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
				//System.out.println(pop);
				BeanLocator.defaultLookup(SpecialiteSession.class).delete(spec.getId());
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
		
	}
	
	

	/**
	 * Definit comment un element de la liste est affiche.
	 */
	
	
	public void onClick$bchercher()
	{
		onOK();
	}
	
	public void onOK()
	{
		lib = txtLib.getValue();
		code=txtCode.getValue();
		
		if(!lib.equalsIgnoreCase(Labels.getLabel("kermel.module.lib")))
		{ if(code.equalsIgnoreCase(Labels.getLabel("kermel.module.code")))
			code="";
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	    }

		if(!code.equalsIgnoreCase(Labels.getLabel("kermel.module.code")))
		{ if(lib.equalsIgnoreCase(Labels.getLabel("kermel.module.lib")))
			lib="";
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	    }
		
	  if(code.equalsIgnoreCase(Labels.getLabel("kermel.module.code"))&& lib.equalsIgnoreCase(Labels.getLabel("kermel.module.lib")))
			  {
		       code="";
		       lib="";
		      }
	  Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
	
	public void onFocus$txtCode()
	{
		if(txtCode.getValue().equalsIgnoreCase(Labels.getLabel("kermel.module.code")))
			txtCode.setValue("");
		else
			code=txtCode.getValue();
	}
	
	
	public void onBlur$txtCode()
	{
		if(txtCode.getValue().equals(""))
			txtCode.setValue(Labels.getLabel("kermel.module.code"));
	}
	public void onFocus$txtLib()
	{
		if(txtLib.getValue().equalsIgnoreCase(Labels.getLabel("kermel.module.lib")))
			txtLib.setValue("");
		else
			lib=txtLib.getValue();
	}
	
	
	public void onBlur$txtLib()
	{
		if(txtLib.getValue().equals(""))
			txtLib.setValue(Labels.getLabel("kermel.module.lib"));
	}
	
	

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		 SygSpecialite spec = (SygSpecialite) data;
		item.setValue(spec);
		
		Listcell cellLib,cellCd,cellDesc, cellDom;
		
		
		if(spec.getCode()!=null) {
			cellCd = new Listcell(spec.getCode());
			cellCd.setParent(item);
		}
		else {
			cellCd = new Listcell("");
			cellCd.setParent(item);
		}
		if(spec.getLibelle()!=null) {
			cellLib = new Listcell(spec.getLibelle());
			cellLib.setParent(item);
		}
		else {
			cellLib = new Listcell("");
			cellLib.setParent(item);
		}
		
		if(spec.getDescription()!=null) {
			cellDesc = new Listcell(spec.getDescription());
			cellDesc.setParent(item);
		}
		else {
			cellDesc = new Listcell("");
			cellDesc.setParent(item);
		}	
		
	}  
}

