package sn.ssi.kermel.web.referentiel.controllers;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygCoordonateur;
import sn.ssi.kermel.be.entity.SygFichePresence;
import sn.ssi.kermel.be.entity.SygFormateur;
import sn.ssi.kermel.be.entity.SygLogisticien;
import sn.ssi.kermel.be.entity.SygMois;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.referentiel.ejb.CoordonnateurSession;
import sn.ssi.kermel.be.referentiel.ejb.FichePresenceSession;
import sn.ssi.kermel.be.referentiel.ejb.FormateurSession;
import sn.ssi.kermel.be.referentiel.ejb.LogisticienSession;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.be.session.MoisSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class FormDonneReelFormationController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode, codelocalite, annee;
	public static final String WINDOW_PARAM_MODE = "MODE";
	
	private Textbox txtlieuR,txtcommentaire,txtRechercherForm,txtRechercherCoor,txtRechercherLog;
	private Datebox txtDateDebutR,txtDateFinR;
	private Bandbox bForm,bCoor,bLog;
	private Listbox lstForm,lstCoor,lstLog;
	private Paging pgForm,pgCoor,pgLog;
	private SygProformation formation;
      private SygFichePresence fiche;
	private ArrayList<String> listValeursAnnees;
	Long code;

	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE, activePage;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private Label lbStatusBar;

	private String errorMsg;
	private Component errorComponent;
	private String nformateur,nlogisticien,ncoordonnateur,page = null;
	private String nom,prenom;

	private SygFormateur formateur;
	private SygLogisticien logisticien;
	private SygCoordonateur coordonnateur;

	
	 private SygMois moislist;
	 private Integer idmois;
	private Long idFormation;
	 Session session = getHttpSession(); 
	
 Long dure;
 int jour=0;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le
		 * "model" des modules est mis � jour soit apr�s une insertion,
		 * supresion, modif,...
		 */
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
		
		
		lstForm.setItemRenderer(new FormRenderer());
		pgForm.setPageSize(byPageBandbox);
		pgForm.addForward(ApplicationEvents.ON_PAGING, this,
				ApplicationEvents.ON_AUTRE_ACTION);
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);

		lstCoor.setItemRenderer(new CoorRenderer());
		pgCoor.setPageSize(byPageBandbox);
		pgCoor.addForward(ApplicationEvents.ON_PAGING, this,
				ApplicationEvents.ON_AUTRE_ACTION);
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);

		lstLog.setItemRenderer(new LogRenderer());
		pgLog.setPageSize(byPageBandbox);
		pgLog.addForward(ApplicationEvents.ON_PAGING, this,
				ApplicationEvents.ON_AUTRE_ACTION);
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);
	
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
		lbStatusBar.setValue(Labels
				.getLabel("kermel.common.form.champs.obligatoire"));
		
		idmois = (Integer) getHttpSession().getAttribute("mois");
		moislist =BeanLocator.defaultLookup(MoisSession.class)
		.findByCode(idmois);
		
		

	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {

		idFormation = (Long) getHttpSession().getAttribute("idformation");
		formation = BeanLocator.defaultLookup(ProformationSession.class).findById(idFormation);
		
		if(formation.getForLieuR()!=null)
		 txtlieuR.setValue(formation.getForLieuR());
		if(formation.getForDateDebutR()!=null)
		 txtDateDebutR.setValue(formation.getForDateDebutR());
		if(formation.getForDateFinR()!=null)
		 txtDateFinR.setValue(formation.getForDateFinR());
		if(formation.getFormateurR()!=null)
		 bForm.setValue(formation.getFormateurR().getPrenom()+" "+formation.getFormateurR().getNom());
		if(formation.getCoordonaterR()!=null)
		 bCoor.setValue(formation.getCoordonaterR().getPrenom()+" "+formation.getCoordonaterR().getNom());
		if(formation.getLogisticienR()!=null)		
		 bLog.setValue(formation.getLogisticienR().getPrenom()+" "+formation.getLogisticienR().getNom());
		if(formation.getForCommentaire()!=null)
		txtcommentaire.setValue(formation.getForCommentaire());

	}
	
	public void onClick$initialiser() {
		txtlieuR.setValue(null);
		txtDateDebutR.setValue(null);
		txtDateFinR.setValue(null);
		bForm.setValue(null);
		bCoor.setValue(null);
		bLog.setValue(null);
		txtcommentaire.setValue(null);
	}	

	
	public void onClick$menuValider() {
		idFormation = (Long) getHttpSession().getAttribute("idformation");
		formation = BeanLocator.defaultLookup(ProformationSession.class).findById(idFormation);

		setFormation();
		BeanLocator.defaultLookup(ProformationSession.class).update(formation);
	
	}

	private void setFormation() {
		if(txtlieuR.getValue()==null || "".equals(txtlieuR.getValue())){
			lbStatusBar.setValue(Labels.getLabel("kermel.rensigner")+" "+Labels.getLabel("kermel.referentiel.common.marche.Lieu"));
			throw new WrongValueException(txtlieuR, Labels.getLabel("kermel.ChamNull"));
		}
		
		if(txtDateDebutR.getValue()==null || "".equals(txtDateDebutR.getValue())){
			lbStatusBar.setValue(Labels.getLabel("kermel.rensigner")+" "+Labels.getLabel("kermel.referentiel.common.programme.PROG_DATEDEBUT"));
			throw new WrongValueException(txtDateDebutR, Labels.getLabel("kermel.ChamNull"));
		}
		if(txtDateFinR.getValue()==null || "".equals(txtDateFinR.getValue())){
			lbStatusBar.setValue(Labels.getLabel("kermel.rensigner")+" "+Labels.getLabel("kermel.referentiel.common.programme.PROG_DATEFIN"));
			throw new WrongValueException(txtDateFinR, Labels.getLabel("kermel.ChamNull"));
		}
		
		if(bForm.getValue()==null || "".equals(bForm.getValue())){
			lbStatusBar.setValue(Labels.getLabel("kermel.rensigner")+" "+Labels.getLabel("kermel.formation.form"));
			throw new WrongValueException(bForm, Labels.getLabel("kermel.ChamNull"));
		}
		
		if(bCoor.getValue()==null || "".equals(bCoor.getValue())){
			lbStatusBar.setValue(Labels.getLabel("kermel.rensigner")+" "+Labels.getLabel("kermel.titre.list.coor"));
			throw new WrongValueException(bCoor, Labels.getLabel("kermel.ChamNull"));
		}
		if(bLog.getValue()==null || "".equals(bLog.getValue())){
			lbStatusBar.setValue(Labels.getLabel("kermel.rensigner")+" "+Labels.getLabel("kermel.titre.list.logisticien"));
			throw new WrongValueException(bLog, Labels.getLabel("kermel.ChamNull"));
		}
		
		formation.setForLieuR(txtlieuR.getValue());
		formation.setForDateDebutR(txtDateDebutR.getValue());
		formation.setForDateFinR(txtDateFinR.getValue());
		
			//pour remplir directement la table Fiche de presence
		
			List<SygFichePresence> liste = BeanLocator.defaultLookup(
					FichePresenceSession.class).findAllby(0, byPage, null, formation);
			if(liste.size()>0)
			{	
				int limite=liste.size();
				for (int j = 0; j <limite; j++) 
				{				
					BeanLocator.defaultLookup(FichePresenceSession.class).delete(liste.get(j).getId());
				}	
					
					dure=ToolKermel.DifferenceDate(txtDateDebutR.getValue(), txtDateFinR.getValue(), "jj");
					for(int  i=0;i<=dure.intValue();i++)
					{	
							SygFichePresence fiche=new SygFichePresence();
							Calendar cal = Calendar.getInstance();
							cal.setTime(txtDateDebutR.getValue());
							cal.add(Calendar.DATE, i);
							fiche.setDateFiche(cal.getTime());
							jour=i+1;
							fiche.setJour("JOUR "+jour);
							fiche.setFormation(formation);
							BeanLocator.defaultLookup(FichePresenceSession.class).save(fiche);				
					}
			}
			else
			{ dure=ToolKermel.DifferenceDate(txtDateDebutR.getValue(), txtDateFinR.getValue(), "jj");
				for(int  i=0;i<=dure.intValue();i++)
				{	
						SygFichePresence fiche=new SygFichePresence();	
//						fiche.setDateFiche(ToolKermel.getCourantEch(txtDateDebutR.getValue(), i));
						Calendar cal = Calendar.getInstance();
						cal.setTime(txtDateDebutR.getValue());
						cal.add(Calendar.DATE, i);
						fiche.setDateFiche(cal.getTime());
						jour=i+1;
						fiche.setJour("JOUR "+jour);
						fiche.setFormation(formation);
						BeanLocator.defaultLookup(FichePresenceSession.class).save(fiche);				
				}
			}
		formation.setForCommentaire(txtcommentaire.getValue());
		
		formation.setFormateurR(formateur);
		formation.setCoordonaterR(coordonnateur);
		formation.setLogisticienR(logisticien);
		
		formation.setMois(moislist);
	}
	
	


	@Override
	public void onEvent(Event event) throws Exception {
		// TODO Auto-generated method stub

		
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)) {
			if (event.getData() != null) {

				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				pgForm.setPageSize(byPageBandbox);
			} else {
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgForm.getActivePage() * byPageBandbox;
				pgForm.setPageSize(byPageBandbox);

			}

			List<SygFormateur> formateur = BeanLocator.defaultLookup(
					FormateurSession.class).findAllby(activePage,
					byPageBandbox, nom, prenom, null, null,null);
			lstForm.setModel(new SimpleListModel(formateur));
			pgForm.setTotalSize(BeanLocator.defaultLookup(
					FormateurSession.class).count(nom, prenom, null, null,null));

			List<SygCoordonateur> coordonnateur = BeanLocator.defaultLookup(
					CoordonnateurSession.class).findAllby(activePage,
					byPageBandbox, nom, prenom);
			lstCoor.setModel(new SimpleListModel(coordonnateur));
			pgCoor.setTotalSize(BeanLocator.defaultLookup(
					CoordonnateurSession.class).count(nom, prenom));
			
			List<SygLogisticien> logisticien = BeanLocator.defaultLookup(
					LogisticienSession.class).findAllby(activePage,
					byPageBandbox, nom, prenom);
			lstLog.setModel(new SimpleListModel(logisticien));
			pgLog.setTotalSize(BeanLocator.defaultLookup(
					LogisticienSession.class).count(nom, prenom));


		}

	}

	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub

	}



	// Formateur
	public void onSelect$lstForm() {
		formateur = (SygFormateur) lstForm.getSelectedItem().getValue();
		bForm.setValue(formateur.getPrenom() + " " + formateur.getNom());

		bForm.close();
	}

	public void onBlur$txtRechercherForm() {
		if (txtRechercherForm.getValue().equals(null))
			txtRechercherForm.setValue(Labels
					.getLabel("kermel.referentiel.nat.form.rechercher"));
	}

	public void onFocus$txtRechercherForm() {
		if (txtRechercherForm.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			txtRechercherForm.setValue("");
		}
	}

	public void onClick$txtRechercherForm() {
		if (txtRechercherForm.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			nformateur = null;
			page = null;
		} else {
			nformateur = txtRechercherForm.getValue();
			page = "0";
		}

		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	public void onClick$btnRechercherForm() {
		if (txtRechercherForm.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			nformateur = null;
			page = null;
		} else {
			nformateur = txtRechercherForm.getValue();
			page = "0";
		}

		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	public class FormRenderer implements ListitemRenderer {
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygFormateur form = (SygFormateur) data;
			item.setValue(form);

			Listcell cellForm = new Listcell(form.getPrenom() + "  "
					+ form.getNom());
			cellForm.setParent(item);

		}
	}

	// Coordonateur
	public void onSelect$lstCoor() {
		coordonnateur = (SygCoordonateur) lstCoor.getSelectedItem().getValue();
		bCoor.setValue(coordonnateur.getPrenom() + " " + coordonnateur.getNom());

		bCoor.close();
	}

	public void onBlur$txtRechercherCoor() {
		if (txtRechercherCoor.getValue().equals(null))
			txtRechercherCoor.setValue(Labels
					.getLabel("kermel.referentiel.nat.form.rechercher"));
	}

	public void onFocus$txtRechercherCoor() {
		if (txtRechercherCoor.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			txtRechercherCoor.setValue("");
		}
	}

	public void onClick$txtRechercherCoor() {
		if (txtRechercherCoor.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			ncoordonnateur = null;
			page = null;
		} else {
			ncoordonnateur = txtRechercherCoor.getValue();
			page = "0";
		}

		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	public void onClick$btnRechercherCoor() {
		if (txtRechercherCoor.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			ncoordonnateur = null;
			page = null;
		} else {
			ncoordonnateur = txtRechercherCoor.getValue();
			page = "0";
		}

		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	public class CoorRenderer implements ListitemRenderer {
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygCoordonateur coordon = (SygCoordonateur) data;
			item.setValue(coordon);

			Listcell cellCoor = new Listcell(coordon.getPrenom() + "  "
					+ coordon.getNom());
			cellCoor.setParent(item);

		}
	}

	// Logisticien
	public void onSelect$lstLog() {
		logisticien = (SygLogisticien) lstLog.getSelectedItem().getValue();
		bLog.setValue(logisticien.getPrenom() + " " + logisticien.getNom());

		bLog.close();
	}

	public void onBlur$txtRechercherLog() {
		if (txtRechercherLog.getValue().equals(null))
			txtRechercherLog.setValue(Labels
					.getLabel("kermel.referentiel.nat.form.rechercher"));
	}

	public void onFocus$txtRechercherLog() {
		if (txtRechercherLog.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			txtRechercherLog.setValue("");
		}
	}

	public void onClick$txtRechercherLog() {
		if (txtRechercherLog.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			nlogisticien = null;
			page = null;
		} else {
			nlogisticien = txtRechercherLog.getValue();
			page = "0";
		}

		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	public void onClick$btnRechercherLog() {
		if (txtRechercherLog.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			nlogisticien = null;
			page = null;
		} else {
			nlogisticien = txtRechercherLog.getValue();
			page = "0";
		}

		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	public class LogRenderer implements ListitemRenderer {
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygLogisticien logist = (SygLogisticien) data;
			item.setValue(logist);

			Listcell cellForm = new Listcell(logist.getPrenom() + "  "
					+ logist.getNom());
			cellForm.setParent(item);

		}
	}

	
	public void onBlur$txtDateDebutR() {

		Date DateDebut = txtDateDebutR.getValue();
		Date dateFin = (Date) txtDateFinR.getValue();
		if (txtDateFinR.getValue() != null) {
			if (dateFin.compareTo(DateDebut) < 0) {
				errorMsg = "La date d�but doit etre inferieure la date de fin . Veuillez corriger cette incoh�rence.";
				errorComponent = txtDateFinR;
				throw new WrongValueException(errorComponent, errorMsg);
			}
		}

	}

	public void onBlur$txtDateFinR() {
		Date DateFin = txtDateFinR.getValue();
		Date DateDebut = (Date) txtDateDebutR.getValue();
		if (txtDateDebutR.getValue() != null) {
			if (DateDebut.compareTo(DateFin) > 0) {
				errorMsg = "La Date fin doit etre sup�rieur � la date de d�but. Veuillez corriger cette incoh�rence.";
				errorComponent = txtDateDebutR;
				throw new WrongValueException(errorComponent, errorMsg);

			}
		}
	}
}