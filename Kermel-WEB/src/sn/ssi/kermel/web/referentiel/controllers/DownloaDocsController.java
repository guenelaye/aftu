package sn.ssi.kermel.web.referentiel.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;

import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Iframe;

import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class DownloaDocsController extends AbstractWindow implements AfterCompose, EventListener {

	private Iframe idIframe;
	private final String url = UIConstants.PATH_PJ;
	private String nomFichier,chemin;
	public static String NOM_FICHIER = "NOMFICHIER";
	public static String NOM_URL = "URL";

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

	}

	@Override
	public void onEvent(Event event) throws Exception {
		// TODO Auto-generated method stub

	}

	public void onCreate(CreateEvent e) {
		Map<String, Object> windowParams =  (Map<String, Object> ) e.getArg();
		nomFichier = (String) windowParams.get(NOM_FICHIER);
		chemin = (String) windowParams.get(NOM_URL);
		System.out.println(url + nomFichier);
		
		  
		idIframe.setHeight("500px");
		idIframe.setWidth("100%");

		String filepath ;
		if(chemin!=null)
			filepath = chemin + nomFichier;
		else
			filepath = url + nomFichier;
		File f = null;
		if(ToolKermel.isWindows())
		// windows
		  f = new File(filepath.replaceAll("/", "\\\\"));
		// linux
		else
          f = new File(filepath.replaceAll("\\\\", "/"));

		org.zkoss.util.media.AMedia mymedia = null;
		try {
			mymedia = new AMedia(f, null, null);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (mymedia != null)
			idIframe.setContent(mymedia);
		else
			idIframe.setSrc("");

	}

	public org.zkoss.util.media.AMedia fetchFile(File file) {

		org.zkoss.util.media.AMedia mymedia = null;
		try {
			mymedia = new AMedia(file, null, null);
			return mymedia;
		} catch (Exception e) {

			e.printStackTrace();
			return null;
		}

	}

}
