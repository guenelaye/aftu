package sn.ssi.kermel.web.referentiel.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.West;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygBureauxdcmp;
import sn.ssi.kermel.be.referentiel.ejb.BureauxdcmpSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class TreeTypeUnitOrgController extends AbstractWindow implements
		AfterCompose, IdSpace {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private Include content;
	
	private Tree tree;
	private Treechildren child = new Treechildren();
	
	private Session session = getHttpSession();
	private SygBureauxdcmp direction,uniteorg;
	
	private String openWest;
	private West wp;

	List<SygBureauxdcmp> listUniteOrg = new ArrayList<SygBureauxdcmp>();
	
	

	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		listUniteOrg = BeanLocator.defaultLookup(BureauxdcmpSession.class).findRech(0, -1, null,Long.parseLong("1"),null);
	
		
		openWest = (String) Executions.getCurrent().getAttribute("openWest");
		wp.setOpen(true);

		initTree();


	}

	private void initTree() {
		child.setParent(tree);
	   treeOrganigrammeBuilder();
	  
//	   List<SygBureauxdcmp> listdirectiongeneral = BeanLocator.defaultLookup(
//				 BureauxdcmpSession.class).findRech(0, -1,null,null,Long.parseLong("1"));
//	    if(listdirectiongeneral.size()>0)
//	    {
	    	//direction=listdirectiongeneral.get(0)	;
	    	//Object selobj =direction;
	    	
	    	content.setSrc("/referentiel/bureaux/list.zul");
			//content.setHeight("100%");
	   // }
		
	}

	public void onSelect$tree() {
		tree.isInvalidated();
		Object selobj = tree.getSelectedItem().getValue();

		session.setAttribute("selObj", selobj);
		content.invalidate();
		
		if (selobj instanceof SygBureauxdcmp) {

			List<String> status = new ArrayList<String>();
			direction = (SygBureauxdcmp) selobj;
			
			
			//if(direction.getDivision().getId()!=5){
			 System.out.println(tree.getSelectedItem().getId());
			 content.setSrc("/referentiel/bureaux/list.zul");
		      //}
		} 

	}

	
	public void treeOrganigrammeBuilder() {

		 String key = null;
		
			System.out.println("==============================ok");
			
			 for (SygBureauxdcmp UniteOrg : listUniteOrg) {
		
		 Treeitem uniteorgItem = new Treeitem();
		
		 uniteorgItem.setLabel(UniteOrg.getLibelle());
		 uniteorgItem.setTooltiptext(UniteOrg.getLibelle());
		
	   	 //procedureItem.setImage("g");
		 uniteorgItem.setValue(UniteOrg);
		 uniteorgItem.setOpen(false);
		
		 uniteorgItem.setParent(child);
		
			//Niveau 2 :Direction	 
		 List<SygBureauxdcmp> listDirection = BeanLocator.defaultLookup(
				 BureauxdcmpSession.class).findRech(0, -1,null,Long.parseLong("2"),UniteOrg.getId());
			
		 if(listDirection.size() > 0){
		 Treechildren uniteorgTreechildren = new Treechildren();
		 for (SygBureauxdcmp direction : listDirection) {
		 Treeitem directionItem = new Treeitem();
		
		 directionItem.setLabel(direction.getLibelle());
		 directionItem.setTooltiptext(direction.getLibelle());
		
		// etapeItem.setImage("g");
		
		 directionItem.setValue(direction);
		 directionItem.setOpen(false);
		
		 directionItem.setParent(uniteorgTreechildren);
		
			//Niveau 3:Division
		 List<SygBureauxdcmp> listDivision = BeanLocator.defaultLookup(
				 BureauxdcmpSession.class).findRech(0, -1,null,Long.parseLong("3"),direction.getId());
			System.out.println("==============================ok"+listDivision.size());
			
		 if(listDivision.size() > 0){
		 Treechildren directionTreechildren = new Treechildren();
		 for (SygBureauxdcmp division : listDivision) {
		 Treeitem divisionItem = new Treeitem();
		
		 divisionItem.setLabel(division.getLibelle());
		 divisionItem.setTooltiptext(division.getLibelle());
		
		// tacheItem.setImage("g");
		 divisionItem.setValue(division);
		 divisionItem.setOpen(false);
		
		 divisionItem.setParent(directionTreechildren);
		
		 //Niveau 4:D�partement
		 
		 List<SygBureauxdcmp> listDepartement = BeanLocator.defaultLookup(
				 BureauxdcmpSession.class).findRech(0, -1,null,Long.parseLong("4"),division.getId());
		 System.out.println("==============================ok"+listDepartement.size());
		
		 if(listDepartement.size() > 0){
			 Treechildren divisionTreechildren = new Treechildren();
			 
			 for (SygBureauxdcmp departement : listDepartement) {
				 Treeitem departementItem = new Treeitem();
				
				 departementItem.setLabel(departement.getLibelle());
				 departementItem.setTooltiptext(departement.getLibelle());
				
				// tacheItem.setImage("g");
				 departementItem.setValue(departement);
				 departementItem.setOpen(false);
				
				 departementItem.setParent(divisionTreechildren);
				 
				//Niveau 5:Service
				 
				 List<SygBureauxdcmp> listService = BeanLocator.defaultLookup(
						 BureauxdcmpSession.class).findRech(0, -1,null,Long.parseLong("5"),departement.getId());
				 System.out.println("==============================ok"+listService.size());
				
				 if(listService.size() > 0){
					 Treechildren departementTreechildren = new Treechildren();
					 
//					 for (SygBureauxdcmp service : listService) {
//						 Treeitem serviceItem = new Treeitem();
//						
//						 serviceItem.setLabel(service.getLibelle());
//						 serviceItem.setTooltiptext(service.getLibelle());
//						
//						// tacheItem.setImage("g");
//						 serviceItem.setValue(service);
//						 serviceItem.setOpen(false);
//						
//						 serviceItem.setParent(departementTreechildren);
//						 
//						 
//						 
//					 }
					 //departementTreechildren.setParent(departementItem);
				   }  //Fin niveau 5 service 
				 
			 }
			 divisionTreechildren.setParent(divisionItem);
		   }  //Fin niveau 4 departement  
		
		 }
		 directionTreechildren.setParent(directionItem);
		 }//Fin niveau 3 division
		 }
		 uniteorgTreechildren.setParent(uniteorgItem);
		 }//Fin niveau 2 direction
		 }
	}
	///


	public void removeTreeChildren() {
		while (child.getItemCount() > 0) {
			child.removeChild(child.getFirstChild());
		}
	}


	public void onOpen$tree() {

		System.out.println(tree.getSelectedItem().getId());
	}


	
}