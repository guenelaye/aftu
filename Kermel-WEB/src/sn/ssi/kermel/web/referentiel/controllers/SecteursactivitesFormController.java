package sn.ssi.kermel.web.referentiel.controllers;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.referentiel.ejb.SecteursactivitesSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class SecteursactivitesFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_PARENT = "PARENT";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle,txtDescription;
	String code,codesecteur,secteurcode;
	private	SygSecteursactivites categorie=new SygSecteursactivites();
	private Label lbStatusBar,idlibelle;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Bandbox bdCategorie;
	private Paging pgCategorie;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE,activePage;
	private String libellecategorie = null,page=null;
	private Listbox lstCategorie;
	private Textbox txtRechercherCategorie;
	SygSecteursactivites categories=null;
	List<SygSecteursactivites> listescategories = new ArrayList<SygSecteursactivites>();
	SygSecteursactivites activite=null;
	private int parent;
	private String codemodification,codeajout,libellemodification,libelleajout;
	
	@Override
	public void onEvent(Event event) throws Exception {
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgCategorie.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgCategorie.getActivePage() * byPageBandbox;
				pgCategorie.setPageSize(byPageBandbox);
			}
			List<SygSecteursactivites> categories = BeanLocator.defaultLookup(SecteursactivitesSession.class).find(activePage, byPageBandbox,null,libellecategorie,null);
			lstCategorie.setModel(new SimpleListModel(categories));
			pgCategorie.setTotalSize(BeanLocator.defaultLookup(SecteursactivitesSession.class).count(null,libellecategorie,null));
		}

	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		lstCategorie.setItemRenderer(new CategoriesRenderer());
		pgCategorie.setPageSize(byPageBandbox);
		pgCategorie.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		parent=(Integer) map.get(PARAM_WINDOW_PARENT);
		if(parent==UIConstants.PARENT)
		{
		   idlibelle.setValue("*");
		   idlibelle.setStyle(ERROR_MSG_STYLE);
		   codeajout="ADD_CATEGORIES";
		   libelleajout=Labels.getLabel("kermel.referentiel.common.categories.ajouter");
		   codemodification="MOD_CATEGORIES";
		   libellemodification=Labels.getLabel("kermel.referentiel.common.categories.modifier");
		}
		else
		{
			idlibelle.setValue("");
			codeajout="ADD_SECTEURSACTIVITES";
			libelleajout=Labels.getLabel("kermel.referentiel.common.secteur.ajouter");
			codemodification="MOD_SECTEURSACTIVITES";
			libellemodification=Labels.getLabel("kermel.referentiel.common.secteur.modifier");
		}
		 
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (String) map.get(PARAM_WINDOW_CODE);
			categorie = BeanLocator.defaultLookup(SecteursactivitesSession.class).findById(code);
			txtLibelle.setValue(categorie.getLibelle());
			if(categorie.getSecteur()!=null)
			{
			  categories=categorie.getSecteur();
			  bdCategorie.setValue(categories.getLibelle());
			}
			
			txtDescription.setValue(categorie.getDescription());
		}
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			categorie.setLibelle(txtLibelle.getValue());
			categorie.setSecteur(categories);
			categorie.setCode(codesecteur);
			if(categories!=null)
			    categorie.setSecteur(categories);
			categorie.setDescription(txtDescription.getValue());
	     	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(SecteursactivitesSession.class).save(categorie,parent);
				BeanLocator.defaultLookup(JournalSession.class).logAction(codeajout,libelleajout+" :" + new Date(), login);
	
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(SecteursactivitesSession.class).update(categorie);
				BeanLocator.defaultLookup(JournalSession.class).logAction(codemodification,libellemodification+" :" + new Date(), login);
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			if(txtLibelle.getValue().equals(""))
		     {
               errorComponent = txtLibelle;
               errorMsg = Labels.getLabel("kermel.referentiel.banque.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
            if(parent==UIConstants.PARENT)
            {
				if(categories==null)
			     {
	             errorComponent = bdCategorie;
	             errorMsg = Labels.getLabel("kermel.referentiel.common.division")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
					lbStatusBar.setStyle(ERROR_MSG_STYLE);
					lbStatusBar.setValue(errorMsg);
					throw new WrongValueException (errorComponent, errorMsg);
			     }
	
			 }
            if(categories!=null)
		     {
            	listescategories=BeanLocator.defaultLookup(SecteursactivitesSession.class).find(0,-1,null,null,categories);
				secteurcode=BeanLocator.defaultLookup(SecteursactivitesSession.class).getGeneratedCode(categories.getCode(), UIConstants.PARAM_SECTEURACTIVITE, listescategories.size()+1);
				activite=BeanLocator.defaultLookup(SecteursactivitesSession.class).findById(secteurcode);
				if(activite==null)
					codesecteur=secteurcode;
				else
				    codesecteur=BeanLocator.defaultLookup(SecteursactivitesSession.class).getGeneratedCode(categories.getCode(), UIConstants.PARAM_SECTEURACTIVITE, listescategories.size()+2);
	  
		     }
            else
             {
            	
            	codesecteur=BeanLocator.defaultLookup(SecteursactivitesSession.class).getGeneratedCode("", UIConstants.PARAM_SECTEURACTIVITE, UIConstants.NPARENT);
     	       
             }
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	

		///////////Service///////// 
		public void onSelect$lstCategorie(){
		categories= (SygSecteursactivites) lstCategorie.getSelectedItem().getValue();
		bdCategorie.setValue(categories.getLibelle());
		bdCategorie.close();
		
		}
		
		public class CategoriesRenderer implements ListitemRenderer{
			
		
		
			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
				SygSecteursactivites activite = (SygSecteursactivites) data;
				item.setValue(activite);
				
				 Listcell cellLibelle = new Listcell("");
				 if((activite.getCode().length()/3)==1)
					 cellLibelle.setLabel("---"+activite.getLibelle());
				 else  if((activite.getCode().length()/3)==2)
					 cellLibelle.setLabel("------"+activite.getLibelle());
				 else  if((activite.getCode().length()/3)==3)
					 cellLibelle.setLabel("---------"+activite.getLibelle());
				 else  if((activite.getCode().length()/3)==4)
					 cellLibelle.setLabel("------------"+activite.getLibelle());
				 else  if((activite.getCode().length()/3)==4)
					 cellLibelle.setLabel("---------------"+activite.getLibelle());
				 else  if((activite.getCode().length()/3)==6)
					 cellLibelle.setLabel("------------------"+activite.getLibelle());
				 else  if((activite.getCode().length()/3)==7)
					 cellLibelle.setLabel("---------------------"+activite.getLibelle());
				 else  if((activite.getCode().length()/3)==8)
					 cellLibelle.setLabel("------------------------"+activite.getLibelle());
				 else  if((activite.getCode().length()/3)==9)
					 cellLibelle.setLabel("---------------------------"+activite.getLibelle());
				 else  if((activite.getCode().length()/3)==10)
					 cellLibelle.setLabel("------------------------------"+activite.getLibelle());
				 else  cellLibelle.setLabel(activite.getLibelle());
				 cellLibelle.setParent(item);
		
			}
		}
		public void onFocus$btnRechercherDivision(){
		if(txtRechercherCategorie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.code.categorie"))){
			txtRechercherCategorie.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherService(){
		if(txtRechercherCategorie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.code.categorie")) || txtRechercherCategorie.getValue().equals("")){
			libellecategorie = null;
			page=null;
		}else{
			libellecategorie = txtRechercherCategorie.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
		
		

}
