package sn.ssi.kermel.web.referentiel.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.EaPiecesJointes;
import sn.ssi.kermel.be.entity.SygFormationGroupe;
import sn.ssi.kermel.be.entity.SygMois;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteFormationSession;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.be.session.MoisSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;


@SuppressWarnings("serial")
public class ListeFormationController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	//pr liste formation
	private Listbox list,listgroupe;
	private Paging pgProformation,pgGroupe;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private int byPage1 = UIConstants.DSP_GRID_ROWS_BY_PAGES;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private String sygmlibelle,sygmreference;
	private Date sygmdatedebut,sygmdatefin;
	private String statut,imagestatut;
	
	private Listheader lshreferentiel,lshlibelle,lieu,cible,effectif,module,formateur,commentaire,lshdatedebut,lshdatefin;

	private Textbox txtlibelle,txtdsecription, txtdatedebut,txtdatefin,txtreference;
	Long code;
	
	private String anne;
	private Listbox listAnne;
	private Bandbox cbannee;
	private ArrayList<String> listValeursAnnees;
	UtilVue utilVue = UtilVue.getInstance();
	private String selAnne;
	 private KermelSousMenu monSousMenu;
	 private Menuitem ADD_FORM, MOD_FORM,SUP_FORM,DETAIL_FORM,ADD_AUTORITE;

	 private SygMois moislist=null;
	 private Integer idmois;
	 Session session = getHttpSession(); 
	 private Div step1,step2;

	private Long idformation;
	private int activePage;
	private SygProformation formation;

		private static final long serialVersionUID = 1L;
		public static final String PARAM_WIDOW_CODE = "CODE";
		public static final String WINDOW_PARAM_MODE = "MODE";
		 private static final String CONFIRMPUBLIER = "CONFIRMPUBLIER";
	
	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_FORMATION);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this);
		if (ADD_FORM != null) {
			ADD_FORM.addForward(ApplicationEvents.ON_CLICK, this,ApplicationEvents.ON_ADD);
		}
		
		if (MOD_FORM != null) {
			MOD_FORM.addForward(ApplicationEvents.ON_CLICK, this,
					ApplicationEvents.ON_EDIT);
		}
		if (SUP_FORM != null) {
			SUP_FORM.addForward(ApplicationEvents.ON_CLICK, this,
					ApplicationEvents.ON_DELETE);
		}
		if (DETAIL_FORM != null) {
			DETAIL_FORM.addForward(ApplicationEvents.ON_CLICK, this,
					ApplicationEvents.ON_DETAILS);
		}

		if (ADD_AUTORITE != null) {
			ADD_AUTORITE.addForward(ApplicationEvents.ON_CLICK, this,ApplicationEvents.ON_AUTRE_ACTION);
		}
		
		/*
		 * On indique qula fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
		

	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
		lshreferentiel.setSortAscending(new FieldComparator("forRef", false));
		lshreferentiel.setSortDescending(new FieldComparator("forRef", true));
	
		lshlibelle.setSortAscending(new FieldComparator("forLibelle", false));
		lshlibelle.setSortDescending(new FieldComparator("forLibelle", true));
        
        lshdatedebut.setSortAscending(new FieldComparator("forDateDebut", false));
        lshdatedebut.setSortDescending(new FieldComparator("forDateDebut", true));
	
        lshdatefin.setSortAscending(new FieldComparator("forDateFin", false));
        lshdatefin.setSortDescending(new FieldComparator("forDateFin", true));
 
		list.setItemRenderer(this);
		pgProformation.setPageSize(byPage);
		
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION1, this);
		listgroupe.setItemRenderer(new GroupesFormationRenderer());
		pgGroupe.setPageSize(byPage1);
		pgGroupe.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTRE_ACTION1);
   		
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pgProformation.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
		idmois = (Integer) getHttpSession().getAttribute("mois");
		if(idmois!=null)
		  moislist =BeanLocator.defaultLookup(MoisSession.class) .findByCode(idmois);
		
		
		
		  step1.setVisible(true);
	      step2.setVisible(false);
		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		
		anne = UtilVue.getInstance().anneecourant(new Date());
		listValeursAnnees = new ArrayList<String>();
		for (int i = 2007; i <= Integer.parseInt(anne) ; i++) {
			listValeursAnnees.add(i+"");
		}
		listAnne.setModel(new SimpleListModel(listValeursAnnees));
		Map<String, Object> map = (Map<String, Object>) event.getArg();


	}
	


	public void onSelect$listAnne(){
   		cbannee.setValue(listAnne.getSelectedItem().getValue().toString());
   		selAnne=listAnne.getSelectedItem().getValue().toString();
   		cbannee.close();
   	}
	

	public Date stringToDate (String sVal) {

	        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
	        
	        try {
	            return df.parse(sVal);
	        } catch (ParseException e) {
	            e.printStackTrace();
	            return null;
	        }
	    }

	
	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			
			 List<SygProformation> formation = BeanLocator.defaultLookup(ProformationSession.class).find(pgProformation.getActivePage()*byPage,byPage,null,null,null,sygmlibelle,sygmreference,anne,null,moislist,-1);
			
			SimpleListModel listModel = new SimpleListModel(formation);
			list.setModel(listModel);
			pgProformation.setTotalSize(BeanLocator.defaultLookup(ProformationSession.class).count(null,null,null,sygmlibelle,sygmreference,anne,null,moislist,-1));
			
			
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/referentiel/proformation/formformation.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouvelleformation"));
			display.put(DSP_HEIGHT,"600px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormationFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/referentiel/proformation/formformation.zul";
			 formation=(SygProformation)list .getSelectedItem().getValue();
			  if(formation.getPublie()==1)
          	{
         	 Messagebox.show(Labels.getLabel("kermel.formation.statut.publier.controle.modif"), "Erreur", Messagebox.OK, Messagebox.ERROR);
			
          	}
          else
              {
			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"600px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormationFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(FormationFormController.PARAM_WIDOW_CODE, formation.getId());

			showPopupWindow(uri, data, display);
              }
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				
                formation=(SygProformation)list .getSelectedItem().getValue();
                if(formation.getPublie()==1)
                	{
               	 Messagebox.show(Labels.getLabel("kermel.formation.statut.publier.controle.supprime"), "Erreur", Messagebox.OK, Messagebox.ERROR);
     			
                	}
                else
                    {
	                	HashMap<String, String> display = new HashMap<String, String>(); // permet
	    				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
	    				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
	    				display.put(MessageBoxController.DSP_HEIGHT, "250px");
	    				display.put(MessageBoxController.DSP_WIDTH, "47%");
	             	    HashMap<String, Object> map = new HashMap<String, Object>(); // permet
	    		   	    map.put(CONFIRMPUBLIER, "SUPPRIMER");
	    				showMessageBox(display, map);
    				
                   }
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				String confirmer = (String) ((HashMap<String, Object>) event.getData()).get(CONFIRMPUBLIER);
				 if (confirmer != null && confirmer.equalsIgnoreCase("Publication_Confirmer")) 
				 {
					    formation.setPublie(1);
						BeanLocator.defaultLookup(ProformationSession.class).update(formation);
						
						Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
				 }
				 else
				 {
					 if (confirmer != null && confirmer.equalsIgnoreCase("SUPPRIMER")) 
					 {
						 for (int i = 0; i < list.getSelectedCount(); i++) {
								Long codes = ((SygProformation)((Listitem) list.getSelectedItems().toArray()[i]).getValue()).getId();
								System.out.println(codes);
							BeanLocator.defaultLookup(ProformationSession.class).delete(codes);
							}
						 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
					 }
					 else
					 {
						 if (confirmer != null && confirmer.equalsIgnoreCase("SUPPRIMERGROUPE")) 
						 {
							 for (int i = 0; i < listgroupe.getSelectedCount(); i++) {
									Integer codes = ((SygFormationGroupe)((Listitem) listgroupe.getSelectedItems().toArray()[i]).getValue()).getId();
									System.out.println(codes);
								    BeanLocator.defaultLookup(AutoriteFormationSession.class).deleteGroupe(codes);
								}
							 Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION1, this, null);
						 }
					 }
				 }
				
				
			}
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DETAILS)) {
				 if (list.getSelectedItem() == null) {
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
					
				 }
				final String uri ="/referentiel/proformation/formdetails.zul";

				final HashMap<String, String> display = new HashMap<String, String>();
				display.put(DSP_HEIGHT,"85%");
				display.put(DSP_WIDTH, "80%");
				display.put(DSP_TITLE, Labels.getLabel("kermel.formateur.detail"));
				final HashMap<String, Object> data = new HashMap<String, Object>();
			
				showPopupWindow(uri, data, display); 
	            
			} 
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)) {
				 if (list.getSelectedItem() == null) {
						throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
						
					 }
				 formation = (SygProformation)list.getSelectedItem().getValue();
				 session.setAttribute("idformation", ((SygProformation)list.getSelectedItem().getValue()).getId());
				 Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION1, this, null);
					 step1.setVisible(false);
					 step2.setVisible(true);

			}
		
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION1)){
				if (event.getData() != null)
				   {
					 
					formation = (SygProformation)list.getSelectedItem().getValue();
					idformation=formation.getId();
					activePage = Integer.parseInt((String) event.getData());
					byPage1 = UIConstants.DSP_GRID_ROWS_BY_PAGES ;
					pgGroupe.setPageSize(byPage1);	
				       } 
				    else {
				    	byPage1 = UIConstants.DSP_GRID_ROWS_BY_PAGES;
					activePage = pgGroupe.getActivePage() * byPage1;
					pgGroupe.setPageSize(byPage1);
				       
				      }
				
					  List<SygFormationGroupe> groupes = BeanLocator.defaultLookup(AutoriteFormationSession.class).findGroupe(activePage, byPage1,formation,null);
					  listgroupe.setModel(new SimpleListModel(groupes));
					  pgGroupe.setTotalSize(BeanLocator.defaultLookup(AutoriteFormationSession.class).countGroupe(formation,null));
					        
			}	
		else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
			Listcell button = (Listcell) event.getTarget();
			String toDo = (String) button.getAttribute(UIConstants.TODO);
			formation = (SygProformation) button.getAttribute("formation");
			
			if (toDo.equalsIgnoreCase("publier"))
			{
				
				HashMap<String, String> display = new HashMap<String, String>(); // permet
				 display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.plansdepassation.publier"));
				 display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.referentiel.formation.publier.titre"));
				 display.put(MessageBoxController.DSP_HEIGHT, "150px");
				 display.put(MessageBoxController.DSP_WIDTH, "100px");
		        HashMap<String, Object> map = new HashMap<String, Object>(); // permet
		         map.put(CONFIRMPUBLIER, "Publication_Confirmer");
				 showMessageBox(display, map);
				
				
				
			}else if(toDo.equalsIgnoreCase("depublier")){
				
				formation.setPublie(0);
				BeanLocator.defaultLookup(ProformationSession.class).update(formation);
				
				 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
				
			}
		}
	
	}

	public class PieceRenderer implements ListitemRenderer {
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			EaPiecesJointes piecejointe = (EaPiecesJointes) data;
			item.setValue(piecejointe.getId());

			item.setAttribute("fichier", piecejointe.getFichier());

			if (piecejointe.getFichier() != null) {
				Listcell cellFichier = new Listcell("");

				cellFichier.setImage("/images/PaperClip-16x16.png");
				cellFichier.setAttribute(UIConstants.TODO, "VIEW");
				cellFichier.addEventListener(Events.ON_CLICK, (EventListener) new PieceRenderer());
				cellFichier.setAttribute("fichier", piecejointe.getFichier());
				cellFichier.setParent(item);
			} else {
				Listcell cellFichier = new Listcell("");
				cellFichier.setParent(item);
			}
		}
	}
	
	public class GroupesFormationRenderer implements ListitemRenderer {
		@Override
		public void render(final Listitem item, final Object data, int index) throws Exception {
			SygFormationGroupe gr = (SygFormationGroupe) data;
				item.setValue(gr);
				
				Listcell cellLib,cellCd,cellDesc;
				
				
				if(gr.getGroupe().getCode()!=null) {
					cellCd = new Listcell(gr.getGroupe().getCode());
					cellCd.setParent(item);
				}
				else {
					cellCd = new Listcell("");
					cellCd.setParent(item);
				}
				if(gr.getGroupe().getLibelle()!=null) {
					cellLib = new Listcell(gr.getGroupe().getLibelle());
					cellLib.setParent(item);
				}
				else {
					cellLib = new Listcell("");
					cellLib.setParent(item);
				}
				
				if(gr.getGroupe().getDescription()!=null) {
					cellDesc = new Listcell(gr.getGroupe().getDescription());
					cellDesc.setParent(item);
				}
				else {
					cellDesc = new Listcell("");
					cellDesc.setParent(item);
				}	
				
	}
}
	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygProformation formation = (SygProformation) data;
		item.setValue(formation);

		Listcell cellreference = new Listcell(formation.getForRef());
		cellreference.setParent(item);
		Listcell celllibelle = new Listcell(formation.getForLibelle());
		celllibelle.setParent(item);
		Listcell celleDatedebut = new Listcell(UtilVue.getInstance().formateLaDate(formation.getForDateDebut()));
		celleDatedebut.setParent(item);
		Listcell cellDatefin = new Listcell(UtilVue.getInstance().formateLaDate(formation.getForDateFin()));
		cellDatefin.setParent(item);
		
         Listcell cellImageStatut = new Listcell("");
		 
		 if(formation.getPublie()==0){
			 imagestatut="/images/deletemoins.png";
			 
			 cellImageStatut.setAttribute(UIConstants.TODO, "publier");
			 cellImageStatut.setTooltiptext(Labels.getLabel("kermel.referentiel.formation.publier.titre"));
		 }else{
			 imagestatut="/images/accept.png";
			 cellImageStatut.setAttribute(UIConstants.TODO, "depublier");
			 cellImageStatut.setTooltiptext(Labels.getLabel("kermel.referentiel.formation.depublier.titre"));
		 }
		
		    cellImageStatut.setImage(imagestatut);
			cellImageStatut.setAttribute("formation", formation);
			cellImageStatut.addEventListener(Events.ON_CLICK, ListeFormationController.this);
			cellImageStatut.setParent(item);
	}
	
	public void onClick$ajout()
	{
		final String uri = "/referentiel/proformation/autorite/formgroupe.zul";

		final HashMap<String, String> display = new HashMap<String, String>();
		display.put(DSP_TITLE, Labels.getLabel("kermel.referentiel.groupedeformation"));
		display.put(DSP_HEIGHT,"85%");
		display.put(DSP_WIDTH, "80%");

		final HashMap<String, Object> data = new HashMap<String, Object>();
		data.put(FormFormationAutoriteController.WINDOW_PARAM_MODE,UIConstants.MODE_NEW);
		
	//	data.put(FormFormationAutoriteController.PARAM_WIDOW_CODE, ((SygProformation)list.getSelectedItem().getValue()).getId());
		showPopupWindow(uri, data, display);
		
		}
	
	  public void onOK$cbannee() {
		onClick$btnRechercher();
	     }
	  public void onOK$txtreference() {
		onClick$btnRechercher();
	     }
	  public void onOK$txtlibelle() {
		onClick$btnRechercher();
	      }
	
	public void onClick$btnRechercher() {
		
		anne = cbannee.getValue();

		if (txtreference.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.courrier.reference")) || txtreference.getValue().equals("")) {
			sygmreference = null;
		} else {
			sygmreference  = txtreference.getValue();
		}	
		
		if (txtlibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.devises.libelle")) || txtlibelle.getValue().equals("")) {
			sygmlibelle = null;
		} else {
			sygmlibelle  = txtlibelle.getValue();
		}	

 
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	

		public void onFocus$txtreference() {
				if (txtreference.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.courrier.reference"))) {
					txtreference.setValue("");

				}
		}
			
			public void onFocus$txtlibelle() {
				if (txtlibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.devises.libelle"))) {
					txtlibelle.setValue("");

				}
		}
				
				public void onBlur$txtreference() {
					if (txtreference.getValue().equalsIgnoreCase("")) {
						txtreference.setValue(Labels.getLabel("kermel.referentiel.courrier.reference"));
					}	
			}
				public void onBlur$txtlibelle() {
					if (txtlibelle.getValue().equalsIgnoreCase("")) {
						txtlibelle.setValue(Labels.getLabel("kermel.referentiel.devises.libelle"));
					}	
			}
				
				public void onClick$menuFermer() {
					 step1.setVisible(true);
					 step2.setVisible(false);
				}
				
				public void onClick$menuSupprimer()
				{
					 if (listgroupe.getSelectedItem() == null) {
							throw new WrongValueException(listgroupe, Labels.getLabel("kermel.error.select.item"));
							
						 }
					HashMap<String, String> display = new HashMap<String, String>(); // permet
    				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
    				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
    				display.put(MessageBoxController.DSP_HEIGHT, "250px");
    				display.put(MessageBoxController.DSP_WIDTH, "47%");
             	    HashMap<String, Object> map = new HashMap<String, Object>(); // permet
    		   	    map.put(CONFIRMPUBLIER, "SUPPRIMERGROUPE");
    				showMessageBox(display, map);
					
					}
				
}