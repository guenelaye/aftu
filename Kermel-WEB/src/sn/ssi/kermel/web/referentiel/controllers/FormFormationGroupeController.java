package sn.ssi.kermel.web.referentiel.controllers;


import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygFormationAutorite;
import sn.ssi.kermel.be.entity.SygFormationGroupe;
import sn.ssi.kermel.be.entity.SygGroupeFormation;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.referentiel.ejb.AutoriteFormationSession;
import sn.ssi.kermel.be.referentiel.ejb.GroupeFormationSession;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


@SuppressWarnings("serial")
public class FormFormationGroupeController  extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode;
	public static final String WINDOW_PARAM_MODE = "MODE";
  
	private Textbox txtRechercherAutorite;
	private Paging pgGroupe;
	Long code;

	private Listbox lstGroupe;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE, activePage;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private Label entete1;
	
	private String libelleautorite = null;

	private SygTypeAutoriteContractante typeautorite;
	private SygAutoriteContractante autorite;
	private SygProformation formation;
	private SygFormationAutorite fauto;
	private SygGroupeFormation groupe;
	private Div step0, step1;
	private String errorMsg;
	private Component errorComponent;
	private String mod, nom, prenom, page = null,libgroupe,nlogisticien,ncoordonnateur;

	 private Integer idmois,idannee;
	 Session session = getHttpSession(); 
		private String libelle=null;

	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		mode = (String) map.get(PARAM_WINDOW_MODE);
		code = (Long) session.getAttribute("idformation");
		formation = BeanLocator.defaultLookup(ProformationSession.class).findById(code);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	public void onClick$menuValider() {
		
			
		fauto= new SygFormationAutorite();
		
		if (lstGroupe.getSelectedItem() == null)
			throw new WrongValueException(lstGroupe, Labels .getLabel("kermel.error.select.item"));

		for (int i = 0; i < lstGroupe.getSelectedCount(); i++) {
			SygFormationGroupe newgroupe=new SygFormationGroupe();
			groupe = (SygGroupeFormation)((Listitem) lstGroupe.getSelectedItems().toArray()[i]).getValue();
			newgroupe.setFormation(formation);
			newgroupe.setGroupe(groupe);
			BeanLocator.defaultLookup(AutoriteFormationSession.class).saveGroupe(newgroupe);
		}
		
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION1, getOpener(), null);
		detach();

	}

	

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);



		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		lstGroupe.setItemRenderer(this);
		pgGroupe.setPageSize(byPage);
		pgGroupe.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
	
		
	
		
	}

	

	
	public void onClick$btnRechercherAutorite()
	{
		if (txtRechercherAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.typeAutoritecontractante")) || txtRechercherAutorite.getValue().equals("")) {
			libelleautorite = null;
		} else
		{
			
		libelleautorite = txtRechercherAutorite.getValue();
		
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
		}
	
	public void onFocus$txtRechercherAutorite()
	{
		if(txtRechercherAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.autoritecontractante.libelle")))
			txtRechercherAutorite.setValue("");
		else
			libelleautorite=txtRechercherAutorite.getValue();
	}
	
	
	public void onBlur$txtRechercherAutorite()
	{
		if(txtRechercherAutorite.getValue().equals(""))
			txtRechercherAutorite.setValue(Labels.getLabel("kermel.autoritecontractante.libelle"));
	}
	
	

	public void onClick$menuFermer() {
		detach();
	}

	public void onClick$menuFermer1() {
		detach();
	}





	@Override
	public void onEvent(Event event) throws Exception {
		// TODO Auto-generated method stub

		if (event.getName().equalsIgnoreCase(
				ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgGroupe.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgGroupe.getActivePage() * byPage;
				pgGroupe.setPageSize(byPage);
			}
			 List<SygGroupeFormation> groupe = BeanLocator.defaultLookup(GroupeFormationSession.class).Groupes(activePage,byPage,libelle,formation);

 			 SimpleListModel listModel = new SimpleListModel(groupe);
 			lstGroupe.setModel(listModel);
			pgGroupe.setTotalSize(BeanLocator.defaultLookup(GroupeFormationSession.class).Groupes(libelle,formation).size());
			
		
		}

	

	}

	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		// TODO Auto-generated method stub
		 SygGroupeFormation gr = (SygGroupeFormation) data;
			item.setValue(gr);
			
			Listcell cellLib,cellCd,cellDesc;
			
			
			if(gr.getCode()!=null) {
				cellCd = new Listcell(gr.getCode());
				cellCd.setParent(item);
			}
			else {
				cellCd = new Listcell("");
				cellCd.setParent(item);
			}
			if(gr.getLibelle()!=null) {
				cellLib = new Listcell(gr.getLibelle());
				cellLib.setParent(item);
			}
			else {
				cellLib = new Listcell("");
				cellLib.setParent(item);
			}
			
			if(gr.getDescription()!=null) {
				cellDesc = new Listcell(gr.getDescription());
				cellDesc.setParent(item);
			}
			else {
				cellDesc = new Listcell("");
				cellDesc.setParent(item);
			}	
	}

	
	}
