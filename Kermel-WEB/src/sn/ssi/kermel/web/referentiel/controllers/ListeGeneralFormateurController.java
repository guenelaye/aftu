package sn.ssi.kermel.web.referentiel.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;

import sn.ssi.kermel.web.common.controllers.AbstractWindow;


public class ListeGeneralFormateurController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private Include pgForm;

	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		


	}

	public void onCreate(CreateEvent createEvent) {
		pgForm.setSrc("/formateur/accueil.zul");
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	
}