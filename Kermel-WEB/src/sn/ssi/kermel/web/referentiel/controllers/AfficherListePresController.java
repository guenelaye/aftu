package sn.ssi.kermel.web.referentiel.controllers;
	
	import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Iframe;

import sn.ssi.kermel.web.common.controllers.AbstractWindow;



	@SuppressWarnings("serial")
	public class AfficherListePresController extends AbstractWindow  implements  AfterCompose
	 {
		private String url = "http://localhost:8080/birt/frameset?__report=/etats/" ;
		private final String fichier = "participants.rptdesign";
		private String parametres = "&JOUR_ID=";
		private String format = "&__format=PDF";
		
		private Iframe idIframe;
	    
		@Override
		public void afterCompose() {
			Components.wireFellows(this, this);
			Components.addForwards(this, this);
			

	          
			Long idFiche = (Long) getHttpSession().getAttribute("id");
         	parametres+=idFiche;
         	System.out.println(url + fichier + parametres + format);
         	idIframe.setSrc(url + fichier + parametres + format);

		}

		

		public void onClick$menuFermer() {
			detach();
		}

}
