package sn.ssi.kermel.web.referentiel.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.PkQuestion;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.session.QuestionSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;


@SuppressWarnings("serial")
public class ListReponseController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtObjet,txtRef;
    String libelle=null,page=null,code=null,login;
    private Listheader lshObjet,lshRef;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem  TRAITER_REPONSE, WMODP_TYPESMARCHES;
    SygDossiers  dossier= null;
    
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		//dossiers
		dossier=(SygDossiers) getHttpSession().getAttribute("dossier");
		
	
				
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_MODESSELECTIONS, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	
	}

	 
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			
			dossier=(SygDossiers) getHttpSession().getAttribute("dossier");
			 List<PkQuestion> types = BeanLocator.defaultLookup(QuestionSession.class).findRep(dossier);
			 SimpleListModel listModel = new SimpleListModel(types);
			 lstListe.setModel(listModel);
			 pgPagination.setTotalSize(types.size());
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			
if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			final String uri = "/reponse/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			
			
			getHttpSession().setAttribute("reponse", lstListe.getSelectedItem().getValue());
			data.put(ReponseFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
//			final String uri = "/referentiel/typesmarches/form.zul";
//
//			final HashMap<String, String> display = new HashMap<String, String>();
//			display.put(DSP_HEIGHT,"500px");
//			display.put(DSP_WIDTH, "80%");
//			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));
//
//			final HashMap<String, Object> data = new HashMap<String, Object>();
//			data.put(TypemarcheFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
//			data.put(TypemarcheFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());
//
//			showPopupWindow(uri, data, display);
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
				
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
			
			
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
					
					PkQuestion question=(PkQuestion) ((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
					question.setRep(null);
					BeanLocator.defaultLookup(QuestionSession.class).update(question);
					
				//Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				//BeanLocator.defaultLookup(TypesmarchesSession.class).delete(codes);
				//BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_TYPESMARCHES", Labels.getLabel("kermel.referentiel.common.typemarche.suppression")+" :" + new Date(), login);
				
				}
				loadApplicationState("reponse");
				//Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODESSELECTIONS)) {
				if (lstListe.getSelectedItem() == null)
				  throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				
				session.setAttribute("idtype", lstListe.getSelectedItem().getValue());
				loadApplicationState("typesmarches_modesselections");
			} 
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DETAILS)) {
				if (lstListe.getSelectedItem() == null)
				  throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				
				session.setAttribute("idtype", lstListe.getSelectedItem().getValue());
				loadApplicationState("typesmarches_modespassations");
			} 
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		PkQuestion types = (PkQuestion) data;
		item.setValue(types);

		 Listcell cellDate = new Listcell(ToolKermel.dateToString(types.getDateEntree()));
		 cellDate.setParent(item);
			 
		 Listcell cellCode = new Listcell(types.getObjet());
		 cellCode.setParent(item);
		 
		 Listcell cellLibelle = new Listcell(types.getDescription());
		 cellLibelle.setParent(item);
		 
			 
		 Listcell cellDescription = new Listcell("");
		 cellDescription.setParent(item);
		
		 
		
	}
	public void onClick$bchercher()
	{
		
		if((txtObjet.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.banque.libelle")))||(txtObjet.getValue().equals("")))
		 {
			libelle=null;
			page=null;
		 }
		else
		{
			libelle=txtObjet.getValue();
			page="0";
		}
		if((txtRef.getValue().equalsIgnoreCase(Labels.getLabel("kermel.bureau.douane.code")))||(txtRef.getValue().equals("")))
		 {
			code=null;
			page=null;
		 }
		else
		{
			code=txtRef.getValue();
			page="0";
		}
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	
	public void onFocus$txtObjet()
	{
		if(txtObjet.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.banque.libelle")))
			txtObjet.setValue("");
		
	}
	
	public void onBlur$txtObjet()
	{
		if(txtObjet.getValue().equals(""))
			txtObjet.setValue(Labels.getLabel("kermel.referentiel.banque.libelle"));
	}

	public void onOK$txtObjet()
	{
		onClick$bchercher();
	}
	
	
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
	
	public void onFocus$txtRef()
	{
		if(txtRef.getValue().equalsIgnoreCase(Labels.getLabel("kermel.bureau.douane.code")))
			txtRef.setValue("");
		
	}
	
	public void onBlur$txtRef()
	{
		if(txtRef.getValue().equals(""))
			txtRef.setValue(Labels.getLabel("kermel.bureau.douane.code"));
	}

	public void onOK$txtRef()
	{
		onClick$bchercher();
	}
}