package sn.ssi.kermel.web.referentiel.controllers;


import java.util.Date;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygCategori;
import sn.ssi.kermel.be.entity.TypeCategorie;
import sn.ssi.kermel.be.referentiel.ejb.CategoriSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeCategoriSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class FormCategoriController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_STATE = "STATE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle;
	Long code,typecat,id;
	private	SygCategori categori=new SygCategori();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";

	TypeCategorie type;
	SygCategori idcategori;
	Session session = getHttpSession();

	@Override
	public void onEvent(Event event) throws Exception {


	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

    	typecat=(Long) session.getAttribute("typecategori");
    	id=(Long) session.getAttribute("idcategori");
    	type =BeanLocator.defaultLookup(TypeCategoriSession.class).findById(typecat);
		idcategori = BeanLocator.defaultLookup(CategoriSession.class).findById(id);
		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			categori = BeanLocator.defaultLookup(CategoriSession.class).findById(code);
			txtLibelle.setValue(categori.getLibelle());
			
		}
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			categori.setLibelle(txtLibelle.getValue());
			categori.setType(type);
			categori.setCategori(idcategori);
			
	     	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(CategoriSession.class).save(categori);
				BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_CAT", Labels.getLabel("kermel.referentiel.common.bureaucmp.ajouter")+" :" + new Date(), login);
				
	
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(CategoriSession.class).update(categori);
				BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_CAT", Labels.getLabel("kermel.referentiel.common.bureaucmp.modifier")+" :" + new Date(), login);
				
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			if(txtLibelle.getValue().equals(""))
		     {
               errorComponent = txtLibelle;
               errorMsg = Labels.getLabel("kermel.referentiel.banque.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	
}
