package sn.ssi.kermel.web.referentiel.controllers;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Longbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAnnee;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCoordonateur;
import sn.ssi.kermel.be.entity.SygFormateur;
import sn.ssi.kermel.be.entity.SygLogisticien;
import sn.ssi.kermel.be.entity.SygModule;
import sn.ssi.kermel.be.entity.SygMois;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.referentiel.ejb.CoordonnateurSession;
import sn.ssi.kermel.be.referentiel.ejb.FormateurSession;
import sn.ssi.kermel.be.referentiel.ejb.LogisticienSession;
import sn.ssi.kermel.be.referentiel.ejb.ModuleSession;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.be.session.AnneeSession;
import sn.ssi.kermel.be.session.MoisSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class FormationFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode;
	public static final String WINDOW_PARAM_MODE = "MODE";
  
	private Textbox txtreference, txtlieu, txtlibelle, txtcible,
			txtcommentaire,   txtRechercherAutorite;
	private Textbox txtRechercherForm, txtRechercherMod,txtRechercherCoor,txtRechercherLog;
	private Longbox lgBudg, lgeffectif;
	private Bandbox bForm, bMod,bCoor,bLog;
	private SygProformation formation;
	private Datebox txtDateFin, txtDateDebut;
	private Paging pg;
	Long code;
	private Paging pgAutorite, pgMod, pgForm,pgLog,pgCoor;
	private Listbox lstAutorite, list, lstMod, lstForm,lstCoor,lstLog;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE, activePage;
	private int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private Label entete1, entete2, entete3;
	private Label lbStatusBar;
	private String libelleautorite = null;

	private SygTypeAutoriteContractante typeautorite;
	private SygAutoriteContractante autorite;
	private Div step0, step1, step2;
	private String errorMsg;
	private Component errorComponent;
	private String mod, nom, prenom, page = null,libgroupe,nlogisticien,ncoordonnateur;
	private SygModule module;
	private SygFormateur formateur;
	 private SygMois moislist;
	// private SygGroupeFormation groupe;
	 private SygLogisticien logisticien;
	 private SygCoordonateur coordonnateur;
	 private Integer idmois,idannee;
	 Session session = getHttpSession(); 
	 private SygAnnee annee;
	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WIDOW_CODE);
			formation = BeanLocator.defaultLookup(ProformationSession.class)
					.findById(code);
			
//			if(formation.getTypeautorite()!=null)
//			 entete2.setValue(formation.getTypeautorite().getLibelle());
//			if(formation.getAutorite()!=null)
//			 entete3.setValue(formation.getAutorite().getDenomination());
			if(formation.getForRef()!=null)
			 txtreference.setValue(formation.getForRef());
			if(formation.getForLibelle()!=null)
			 txtlibelle.setValue(formation.getForLibelle());
			if(formation.getForLieu()!=null)
			 txtlieu.setValue(formation.getForLieu());
			if(formation.getForCible()!=null)
			 txtcible.setValue(formation.getForCible());
			if(formation.getModule()!=null)
			 bMod.setValue(formation.getModule().getLibelle());
			if(formation.getFormateur()!=null)
			 bForm.setValue(formation.getFormateur().getPrenom() + " "
					+ formation.getFormateur().getNom());
	       
	        if(formation.getLogisticien()!=null)
			 bLog.setValue(formation.getLogisticien().getPrenom()+" "+formation.getLogisticien().getNom());
			if(formation.getCoordonnateur()!=null)
	         bCoor.setValue(formation.getCoordonnateur().getPrenom()+" "+formation.getCoordonnateur().getNom());
			if(formation.getForDateDebut()!=null)
			txtDateDebut.setValue(formation.getForDateDebut());
			if(formation.getForDateFin()!=null)
			 txtDateFin.setValue(formation.getForDateFin());
			if(formation.getEffectif()!=null)
			lgeffectif.setValue(formation.getEffectif());
			if(formation.getBudg()!=null)
			lgBudg.setValue(formation.getBudg());
			txtcommentaire.setValue(formation.getForCommentaire());
		}

	}

	public void onOK() {
		if (checkFieldConstraints()) {
			if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				formation = new SygProformation();
				setFormation();
				BeanLocator.defaultLookup(ProformationSession.class).save(
						formation);

			} else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				setFormation();
				BeanLocator.defaultLookup(ProformationSession.class).update(
						formation);
			}
			Executions.getCurrent().setAttribute("annee",annee);
			
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(),null);
			loadApplicationState("ListeFormation");
			detach();
		} else {
			throw new WrongValueException(errorComponent, errorMsg);
		}

	}

	private void setFormation() {
		formation.setForRef(txtreference.getValue());
		formation.setForLibelle(txtlibelle.getValue());
		formation.setForLieu(txtlieu.getValue());
		formation.setForCible(txtcible.getValue());
		formation.setForDateDebut(txtDateDebut.getValue());
		formation.setForDateFin(txtDateFin.getValue());
		formation.setFormateur(formateur);
		//formation.setGroupe(groupe);
		formation.setCoordonnateur(coordonnateur);
		formation.setLogisticien(logisticien);
		formation.setEffectif(lgeffectif.getValue());
		formation.setModule(module);
		formation.setBudg(lgBudg.getValue());
		formation.setForCommentaire(txtcommentaire.getValue());
//		formation.setAutorite(autorite);
//		formation.setTypeautorite(typeautorite);
		formation.setMois(moislist);
		formation.setAnne(annee);
		formation.setPublie(0);
	}

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le
		 * "model" des modules est mis � jour soit apr�s une insertion,
		 * supresion, modif,...
		 */
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
		lstMod.setItemRenderer(new ModRenderer());
		pgMod.setPageSize(byPageBandbox);
		pgMod.addForward(ApplicationEvents.ON_PAGING, this,
				ApplicationEvents.ON_AUTRE_ACTION);
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);

		lstForm.setItemRenderer(new FormRenderer());
		pgForm.setPageSize(byPageBandbox);
		pgForm.addForward(ApplicationEvents.ON_PAGING, this,
				ApplicationEvents.ON_AUTRE_ACTION);
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);

//		addEventListener(ApplicationEvents.ON_TYPE_AUTORITES, this);
//		lstAutorite.setItemRenderer(new TypeAutoritesRenderer());
//		pgAutorite.setPageSize(byPage);
//		pgAutorite.addForward(ApplicationEvents.ON_PAGING, this,
//				ApplicationEvents.ON_TYPE_AUTORITES);
//		addEventListener(ApplicationEvents.ON_AUTORITES, this);

		lstCoor.setItemRenderer(new CoorRenderer());
		pgCoor.setPageSize(byPageBandbox);
		pgCoor.addForward(ApplicationEvents.ON_PAGING, this,
				ApplicationEvents.ON_AUTRE_ACTION);
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);

		lstLog.setItemRenderer(new LogRenderer());
		pgLog.setPageSize(byPageBandbox);
		pgLog.addForward(ApplicationEvents.ON_PAGING, this,
				ApplicationEvents.ON_AUTRE_ACTION);
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);
		
		
		
//		list.setItemRenderer(new AutoritesRenderer());
//		pg.setPageSize(byPage);
//		pg.addForward(ApplicationEvents.ON_PAGING, this,
//				ApplicationEvents.ON_AUTORITES);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);

		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		Events.postEvent(ApplicationEvents.ON_TYPE_AUTORITES, this, null);
		lbStatusBar.setValue(Labels
				.getLabel("kermel.common.form.champs.obligatoire"));
		
		idmois = (Integer) getHttpSession().getAttribute("mois");
		moislist =BeanLocator.defaultLookup(MoisSession.class).findByCode(idmois);
		idannee = (Integer) getHttpSession().getAttribute("annee");
		annee=BeanLocator.defaultLookup(AnneeSession.class).findByCode(idannee);
		
	}

//	public void onClick$menuNext() {
//		if (lstAutorite.getSelectedItem() == null)
//			throw new WrongValueException(lstAutorite, Labels
//					.getLabel("kermel.error.select.item"));
//
//		typeautorite = (SygTypeAutoriteContractante) lstAutorite
//				.getSelectedItem().getValue();
//		entete1.setValue(typeautorite.getLibelle());
//		step0.setVisible(false);
//		step1.setVisible(true);
//		Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);
//
//	}
//
//	public void onClick$menuNext1() {
//		if (list.getSelectedItem() == null)
//			throw new WrongValueException(list, Labels
//					.getLabel("kermel.error.select.item"));
//
//		typeautorite = (SygTypeAutoriteContractante) lstAutorite
//				.getSelectedItem().getValue();
//		autorite = (SygAutoriteContractante) list.getSelectedItem().getValue();
//		entete2.setValue(typeautorite.getLibelle());
//		entete3.setValue(autorite.getDenomination());
//		step1.setVisible(false);
//		step2.setVisible(true);
//		Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);
//
//	}
//
//	public void onClick$menuPrevious() {
//		if (lstAutorite.getSelectedItem() == null)
//			throw new WrongValueException(lstAutorite, Labels
//					.getLabel("kermel.error.select.item"));
//
//		typeautorite = (SygTypeAutoriteContractante) lstAutorite
//				.getSelectedItem().getValue();
//		step0.setVisible(true);
//		step1.setVisible(false);
//		Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);
//
//	}
//
//	
//	public void onClick$btnRechercherAutorite()
//	{
//		if (txtRechercherAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.typeAutoritecontractante")) || txtRechercherAutorite.getValue().equals("")) {
//			libelleautorite = null;
//		} else
//		{
//			
//		libelleautorite = txtRechercherAutorite.getValue();
//		
//		}
//		Events.postEvent(ApplicationEvents.ON_TYPE_AUTORITES, this, null);
//		
//		}
//	
//	public void onFocus$txtRechercherAutorite()
//	{
//		if(txtRechercherAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.autoritecontractante.libelle")))
//			txtRechercherAutorite.setValue("");
//		else
//			libelleautorite=txtRechercherAutorite.getValue();
//	}
//	
//	
//	public void onBlur$txtRechercherAutorite()
//	{
//		if(txtRechercherAutorite.getValue().equals(""))
//			txtRechercherAutorite.setValue(Labels.getLabel("kermel.autoritecontractante.libelle"));
//	}
//	
//	public void onClick$menuPrevious1() {
//		if (lstAutorite.getSelectedItem() == null)
//			throw new WrongValueException(lstAutorite, Labels
//					.getLabel("kermel.error.select.item"));
//
//		typeautorite = (SygTypeAutoriteContractante) lstAutorite
//				.getSelectedItem().getValue();
//		step1.setVisible(true);
//		step2.setVisible(false);
//		Events.postEvent(ApplicationEvents.ON_AUTORITES, this, null);
//
//	}

	public void onClick$menuFermer() {
		detach();
	}

	public void onClick$menuFermer1() {
		detach();
	}

//	public class TypeAutoritesRenderer implements ListitemRenderer {
//
//		@Override
//		public void render(Listitem item, Object data, int index)  throws Exception {
//			SygTypeAutoriteContractante autorites = (SygTypeAutoriteContractante) data;
//			item.setValue(autorites);
//
//			Listcell cellLibelle = new Listcell(autorites.getLibelle());
//			cellLibelle.setParent(item);
//			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
//				if (autorites.getId().equals(
//						formation.getAutorite().getType().getId()))
//					item.setSelected(true);
//
//			}
//
//		}
//	}
//
//	public class AutoritesRenderer implements ListitemRenderer {
//
//		@Override
//		public void render(Listitem item, Object data, int index)  throws Exception {
//			SygAutoriteContractante autorites = (SygAutoriteContractante) data;
//			item.setValue(autorites);
//
//			Listcell cellCode = new Listcell(autorites.getSigle());
//			cellCode.setParent(item);
//
//			Listcell cellLibelle = new Listcell(autorites.getDenomination());
//			cellLibelle.setParent(item);
//
//			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
//				if (autorites.getId().equals(formation.getAutorite().getId()))
//					item.setSelected(true);
//
//			}
//		}
//	}

	@Override
	public void onEvent(Event event) throws Exception {
		// TODO Auto-generated method stub

//		if (event.getName().equalsIgnoreCase(
//				ApplicationEvents.ON_TYPE_AUTORITES)) {
//			if (event.getData() != null) {
//				activePage = Integer.parseInt((String) event.getData());
//				byPage = -1;
//				pgAutorite.setPageSize(1000);
//			} else {
//				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
//				activePage = pgAutorite.getActivePage() * byPage;
//				pgAutorite.setPageSize(byPage);
//			}
//			List<SygTypeAutoriteContractante> autorites = BeanLocator
//					.defaultLookup(TypeAutoriteContractanteSession.class).find(
//							activePage, byPage, null, libelleautorite);
//			lstAutorite.setModel(new SimpleListModel(autorites));
//			pgAutorite.setTotalSize(BeanLocator.defaultLookup(
//					TypeAutoriteContractanteSession.class).count(null,
//					libelleautorite));
//		}
//
//		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTORITES)) {
//			if (event.getData() != null) {
//				activePage = Integer.parseInt((String) event.getData());
//				byPage = -1;
//				pgAutorite.setPageSize(1000);
//			} else {
//				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
//				activePage = pgAutorite.getActivePage() * byPage;
//				pgAutorite.setPageSize(byPage);
//			}
//			List<SygAutoriteContractante> autorites = BeanLocator
//					.defaultLookup(AutoriteContractanteSession.class).find(
//							activePage, byPage, libelleautorite, null,
//							typeautorite, null);
//			list.setModel(new SimpleListModel(autorites));
//			pg.setTotalSize(BeanLocator.defaultLookup(
//					AutoriteContractanteSession.class).count(libelleautorite,
//					null, null, null));
//		}
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)) {// pr
																					// step2
			if (event.getData() != null) {

				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				pgMod.setPageSize(byPageBandbox);
			} else {
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgMod.getActivePage() * byPageBandbox;
				pgMod.setPageSize(byPageBandbox);

			}

			List<SygModule> module = BeanLocator.defaultLookup(
					ModuleSession.class).find(activePage, byPageBandbox, null,
					mod);
			lstMod.setModel(new SimpleListModel(module));
			pgMod.setTotalSize(BeanLocator.defaultLookup(ModuleSession.class)
					.count(null, mod));

			List<SygFormateur> formateur = BeanLocator.defaultLookup(
					FormateurSession.class).findAllby(activePage,
					byPageBandbox, nom, prenom, null, null,null);
			lstForm.setModel(new SimpleListModel(formateur));
			pgForm.setTotalSize(BeanLocator.defaultLookup(
					FormateurSession.class).count(nom, prenom, null, null,null));

			
			List<SygCoordonateur> coordonnateur = BeanLocator.defaultLookup(
					CoordonnateurSession.class).findAllby(activePage,
					byPageBandbox, nom, prenom);
			lstCoor.setModel(new SimpleListModel(coordonnateur));
			pgCoor.setTotalSize(BeanLocator.defaultLookup(
					CoordonnateurSession.class).count(nom, prenom));
			
			List<SygLogisticien> logisticien = BeanLocator.defaultLookup(
					LogisticienSession.class).findAllby(activePage,
					byPageBandbox, nom, prenom);
			lstLog.setModel(new SimpleListModel(logisticien));
			pgLog.setTotalSize(BeanLocator.defaultLookup(
					LogisticienSession.class).count(nom, prenom));

			
			
		}

	}

	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub

	}

	// module
	public void onSelect$lstMod() {
		module = (SygModule) lstMod.getSelectedItem().getValue();
		bMod.setValue(module.getLibelle());

		bMod.close();
	}

	public void onBlur$txtRechercherMod() {
		if (txtRechercherMod.getValue().equals(null))
			txtRechercherMod.setValue(Labels
					.getLabel("kermel.referentiel.nat.form.rechercher"));
	}

	public void onFocus$txtRechercherMod() {
		if (txtRechercherMod.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			txtRechercherMod.setValue("");
		}
	}

	public void onClick$txtRechercherMod() {
		if (txtRechercherMod.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			mod = null;
			page = null;
		} else {
			mod = txtRechercherMod.getValue();
			page = "0";
		}

		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	public void onClick$btnRechercherMod() {
		if (txtRechercherMod.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			mod = null;
			page = null;
		} else {
			mod = txtRechercherMod.getValue();
			page = "0";
		}

		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	public class ModRenderer implements ListitemRenderer {
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygModule mdule = (SygModule) data;
			item.setValue(mdule);

			Listcell cellMod = new Listcell(mdule.getLibelle());
			cellMod.setParent(item);

		}
	}



	// Formateur
	public void onSelect$lstForm() {
		formateur = (SygFormateur) lstForm.getSelectedItem().getValue();
		bForm.setValue(formateur.getPrenom() + " " + formateur.getNom());

		bForm.close();
	}

	public void onBlur$txtRechercherForm() {
		if (txtRechercherForm.getValue().equals(null))
			txtRechercherForm.setValue(Labels
					.getLabel("kermel.referentiel.nat.form.rechercher"));
	}

	public void onFocus$txtRechercherForm() {
		if (txtRechercherForm.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			txtRechercherForm.setValue("");
		}
	}

	public void onClick$txtRechercherForm() {
		if (txtRechercherForm.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			nom = null;
			page = null;
		} else {
			nom = txtRechercherForm.getValue();
			page = "0";
		}

		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	public void onClick$btnRechercherForm() {
		if (txtRechercherForm.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			nom = null;
			page = null;
		} else {
			nom = txtRechercherForm.getValue();
			page = "0";
		}

		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	public class FormRenderer implements ListitemRenderer {
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygFormateur form = (SygFormateur) data;
			item.setValue(form);

			Listcell cellForm = new Listcell(form.getPrenom() + "  "
					+ form.getNom());
			cellForm.setParent(item);

		}
	}

	// Coordonateur
	public void onSelect$lstCoor() {
		coordonnateur = (SygCoordonateur) lstCoor.getSelectedItem().getValue();
		bCoor.setValue(coordonnateur.getPrenom() + " " + coordonnateur.getNom());

		bCoor.close();
	}

	public void onBlur$txtRechercherCoor() {
		if (txtRechercherCoor.getValue().equals(null))
			txtRechercherCoor.setValue(Labels
					.getLabel("kermel.referentiel.nat.form.rechercher"));
	}

	public void onFocus$txtRechercherCoor() {
		if (txtRechercherCoor.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			txtRechercherCoor.setValue("");
		}
	}

	public void onClick$txtRechercherCoor() {
		if (txtRechercherCoor.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			ncoordonnateur = null;
			page = null;
		} else {
			ncoordonnateur = txtRechercherCoor.getValue();
			page = "0";
		}

		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	public void onClick$btnRechercherCoor() {
		if (txtRechercherCoor.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			ncoordonnateur = null;
			page = null;
		} else {
			ncoordonnateur = txtRechercherCoor.getValue();
			page = "0";
		}

		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	public class CoorRenderer implements ListitemRenderer {
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygCoordonateur coordon = (SygCoordonateur) data;
			item.setValue(coordon);

			Listcell cellCoor = new Listcell(coordon.getPrenom() + "  "
					+ coordon.getNom());
			cellCoor.setParent(item);

		}
	}

	// Logisticien
	public void onSelect$lstLog() {
		logisticien = (SygLogisticien) lstLog.getSelectedItem().getValue();
		bLog.setValue(logisticien.getPrenom() + " " + logisticien.getNom());

		bLog.close();
	}

	public void onBlur$txtRechercherLog() {
		if (txtRechercherLog.getValue().equals(null))
			txtRechercherLog.setValue(Labels
					.getLabel("kermel.referentiel.nat.form.rechercher"));
	}

	public void onFocus$txtRechercherLog() {
		if (txtRechercherLog.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			txtRechercherLog.setValue("");
		}
	}

	public void onClick$txtRechercherLog() {
		if (txtRechercherLog.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			nlogisticien = null;
			page = null;
		} else {
			nlogisticien = txtRechercherLog.getValue();
			page = "0";
		}

		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	public void onClick$btnRechercherLog() {
		if (txtRechercherLog.getValue().equalsIgnoreCase(
				Labels.getLabel("kermel.referentiel.nat.form.rechercher"))) {
			nlogisticien = null;
			page = null;
		} else {
			nlogisticien = txtRechercherLog.getValue();
			page = "0";
		}

		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	public class LogRenderer implements ListitemRenderer {
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygLogisticien logist = (SygLogisticien) data;
			item.setValue(logist);

			Listcell cellForm = new Listcell(logist.getPrenom() + "  "
					+ logist.getNom());
			cellForm.setParent(item);

		}
	}

	private boolean checkFieldConstraints() {
		try {

			if (txtreference.getValue() == null
					|| txtreference.getValue().trim().equalsIgnoreCase("")) {
				errorComponent = txtreference;
				errorMsg = "Veuillez renseigner le champ R�f�rence";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}

			if (txtlibelle.getValue() == null
					|| txtlibelle.getValue().trim().equalsIgnoreCase("")) {
				errorComponent = txtlibelle;
				errorMsg = "Veuillez renseigner le champ Libelle";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}
			if (txtlieu.getValue() == null
					|| txtlieu.getValue().trim().equalsIgnoreCase("")) {
				errorComponent = txtlieu;
				errorMsg = "Veuillez renseigner le champ Lieu";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}

			if (txtcible.getValue() == null
					|| txtcible.getValue().trim().equalsIgnoreCase("")) {
				errorComponent = txtcible;
				errorMsg = "Veuillez renseigner le champ Cible";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}

			if (txtDateDebut.getValue() == null) {
				errorComponent = txtDateDebut;
				errorMsg = "Veuillez renseigner le champ Date de Debut";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}

			if (txtDateFin.getValue() == null) {
				errorComponent = txtDateFin;
				errorMsg = "Veuillez renseigner le champ Date de Fin";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}

			if (lgeffectif.getValue() == null) {
				errorComponent = lgeffectif;
				errorMsg = "Veuillez renseigner le champ Effectif";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}
			
			

			 if (bMod.getValue() == null ||
					 bMod.getValue().trim().equalsIgnoreCase("")) {
			 errorComponent = bMod;
			 errorMsg = "Veuillez renseigner le champ module";
			 lbStatusBar.setValue(errorMsg);
			 lbStatusBar.setStyle("color:red");
			 return false;
			 }
							
			

				
			 if (txtRechercherCoor.getValue() == null ||
			  txtRechercherCoor.getValue().trim().equalsIgnoreCase("")) {
			 errorComponent = txtRechercherCoor;
			 errorMsg = "Veuillez renseigner le champ coordonnateur";
			 lbStatusBar.setValue(errorMsg);
			 lbStatusBar.setStyle("color:red");
			 return false;
			 }
				
			 if (txtRechercherLog.getValue() == null ||
			 txtRechercherLog.getValue().trim().equalsIgnoreCase("")) {
			 errorComponent = txtRechercherLog;
			 errorMsg = "Veuillez renseigner le champ logisticien";
			 lbStatusBar.setValue(errorMsg);
			 lbStatusBar.setStyle("color:red");
			 return false;
			 }
			 
			
			return true;
		} catch (Exception e) {

			lbStatusBar.setValue(errorMsg);
			// lbStatusBar.setStyle(UIConstants.STYLE_STATUSBAR_ERROR);
			e.printStackTrace();
			throw new WrongValueException(errorComponent, errorMsg);
		}

	}

	public void onBlur$txtDateDebut() {

		Date DateDebut = txtDateDebut.getValue();
		Date dateFin = (Date) txtDateFin.getValue();
		if (txtDateFin.getValue() != null) {
			if (dateFin.compareTo(DateDebut) < 0) {
				errorMsg = "La date d�but doit etre inferieure la date de fin . Veuillez corriger cette incoh�rence.";
				errorComponent = txtDateFin;
				throw new WrongValueException(errorComponent, errorMsg);
			}
		}

	}

	public void onBlur$txtDateFin() {
		Date DateFin = txtDateFin.getValue();
		Date DateDebut = (Date) txtDateDebut.getValue();
		if (txtDateDebut.getValue() != null) {
			if (DateDebut.compareTo(DateFin) > 0) {
				errorMsg = "La Date fin doit etre sup�rieur � la date de d�but. Veuillez corriger cette incoh�rence.";
				errorComponent = txtDateDebut;
				throw new WrongValueException(errorComponent, errorMsg);

			}
		}
	}
}