package sn.ssi.kermel.web.referentiel.controllers;


import java.util.Calendar;
import java.util.Map;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Longbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygJournal;
import sn.ssi.kermel.be.referentiel.ejb.JournalMSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;




@SuppressWarnings("serial")
public class JournalFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,codelocalite,annee,nomFichier,nomPiece,nomPiece1;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtNumero,txtDescription,txtfichier,txtVersionElectronique;
	//private Intbox txtNumero;
	private Longbox numero;
	private Intbox txttraiter,txtpublier,txtpoubelle;
	private SygJournal journal =new SygJournal();
	private SimpleListModel lst;
	private Paging pgjournal;
	private Datebox datePublication;
	private final String ON_LOCALITE = "onLocalite";
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	Long code;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	private Button btnChoixFichier;
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WIDOW_CODE);
			journal = BeanLocator.defaultLookup(JournalMSession.class)
					.findById(code);
			
			txtNumero.setValue(journal.getNumero());
			txtDescription.setValue(journal.getDescription());
			//txtfichier.setValue(journal.getFichierJournal());
			datePublication.setValue(journal.getDatePub());
	
		}else if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			btnChoixFichier.setAttribute(UIConstants.TODO, "JOIN");// Pour tenir
		
		}
		btnChoixFichier.addEventListener(Events.ON_CLICK, this);
			
	     }
	public void onOK() {
		
		journal.setNumero(txtNumero.getValue().toString());
		journal.setDescription(txtDescription.getValue());
		journal.setFichierJournal(txtVersionElectronique.getValue());
		journal.setDatePub(datePublication.getValue());
	//	journal.setOrigine( UIConstants.journalPOUBELLE);
	//	journal.setPoubelle(UIConstants.PARENT);
		
		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			BeanLocator.defaultLookup(JournalMSession.class).save(journal);

		} 
		else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			BeanLocator.defaultLookup(JournalMSession.class).update(journal);
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();

	}
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	
	}
	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub
	
		
	}
	@Override
	public void onEvent(Event event) throws Exception {
		// TODO Auto-generated method stub
	

		txtVersionElectronique.setValue(nomPiece);
	//	txtlettre.setValue(nomPiece1);
		System.out.println("========================================="+txtVersionElectronique.getValue());
	}

	public void onClick$btnChoixFichier() {
		//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
			if (ToolKermel.isWindows())
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectronique.setValue(nomFichier);
		}
	

}