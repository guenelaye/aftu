package sn.ssi.kermel.web.referentiel.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygDecision;
import sn.ssi.kermel.be.entity.SygTypeDecision;
import sn.ssi.kermel.be.referentiel.ejb.DecisionSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeDecisionSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;


@SuppressWarnings("serial")
public class ListDecisionController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null;
    private Listheader lshLibelle;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_DECISION, MOD_DECISION, SUPP_DECISION;
    private String login;
    
 // /Type decision
	private SygTypeDecision typedecision = new SygTypeDecision();
	private Textbox txtRechercherType;
	private Bandbox bdType;
	private Paging pgType;
	private Listbox lstType;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private String LibelleType = null;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_DECISION);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_DECISION != null) { ADD_DECISION.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_DECISION != null) { MOD_DECISION.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUPP_DECISION != null) { SUPP_DECISION.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		lshLibelle.setSortAscending(new FieldComparator("libelle", false));
		lshLibelle.setSortDescending(new FieldComparator("libelle", true));
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	login = ((String) getHttpSession().getAttribute("user"));
    	
    	
    	// champ Type decision du formulaire
		addEventListener(ApplicationEvents.ON_TYPEDECISION, this);
		lstType.setItemRenderer(new typedecisionRenderer());
		pgType.setPageSize(byPageBandbox);
		pgType.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_TYPEDECISION);
		Events.postEvent(ApplicationEvents.ON_TYPEDECISION, this, null);
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygDecision> types = BeanLocator.defaultLookup(DecisionSession.class).findRech(activePage,byPage,null,libelle,LibelleType);
			 SimpleListModel listModel = new SimpleListModel(types);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup( DecisionSession.class).countRech(null,libelle,LibelleType));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/referentiel/decision/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"300px");
			display.put(DSP_WIDTH, "40%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(DecisionFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			final String uri = "/referentiel/decision/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"300px");
			display.put(DSP_WIDTH, "40%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(DecisionFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(DecisionFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
				
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(DecisionSession.class).delete(codes);
				BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_DECISION", Labels.getLabel("kermel.referentiel.common.decision.suppression")+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	
		// champ typedecision du formulaire
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_TYPEDECISION)) {
			List<SygTypeDecision> typedecision = BeanLocator.defaultLookup(TypeDecisionSession.class).find(
					pgType.getActivePage() * byPageBandbox, byPageBandbox, null, LibelleType);
			SimpleListModel listModel = new SimpleListModel(typedecision);
			lstType.setModel(listModel);
			pgType.setTotalSize(BeanLocator.defaultLookup(TypeDecisionSession.class).count(null, LibelleType));
		}
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygDecision decision = (SygDecision) data;
		item.setValue(decision.getId());

			 
		 Listcell cellLibelle = new Listcell(decision.getLibelle());
		 cellLibelle.setParent(item);
		 
		 Listcell cellTypeDecision= new Listcell(decision.getTypedecision().getLibelle());
		 cellTypeDecision.setParent(item);
		 
		

	}
	public void onClick$bchercher()
	{
		
		if((txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.typedecision.libelle")))||(txtLibelle.getValue().equals("")))
		 {
			libelle=null;
			
		 }
		else
		{
			libelle=txtLibelle.getValue();
			page="0";
		}
		
		
		if((bdType.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.decision.typedecision")))||(bdType.getValue().equals("")))
		 {
			LibelleType=null;
			
		 }
		else
		{
			LibelleType = bdType.getValue();
			page="0";
		}
		
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	
	public void onFocus$txtLibelle()
	{
		if(txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.decision.libelle")))
			txtLibelle.setValue("");
		
	}
	
	public void onBlur$txtLibelle()
	{
		if(txtLibelle.getValue().equals(""))
			txtLibelle.setValue(Labels.getLabel("kermel.referentiel.decision.libelle"));
	}

	public void onOK$txtLibelle()
	{
		onClick$bchercher();
	}
	
	/////type decision
	public void onFocus$bdType()
	{
		if(bdType.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.decision.typedecision")))
			bdType.setValue("");
		
	}
	
	public void onBlur$bdType()
	{
		if(bdType.getValue().equals(""))
			bdType.setValue(Labels.getLabel("kermel.referentiel.decision.typedecision"));
	}

	public void onOK$bdType()
	{
		onClick$bchercher();
	}
	
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
	
////////////type decision
	public void onSelect$lstType() {
		bdType.setValue((String) lstType.getSelectedItem().getAttribute(UIConstants.ATTRIBUTE_LIBELLE));
		bdType.close();
	}

	public class typedecisionRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypeDecision type = (SygTypeDecision) data;
			item.setValue(type);
			item.setAttribute(UIConstants.ATTRIBUTE_LIBELLE, type.getLibelle());

			Listcell cellLibelle = new Listcell("");
			if (type.getLibelle() != null) {
				cellLibelle.setLabel(type.getLibelle());
			}
			cellLibelle.setParent(item);
			//				

		}

		
	}

	public void onFocus$txtRechercherType() {
		if (txtRechercherType.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.decision.typedecision"))) {
			txtRechercherType.setValue("");
		}
	}

	public void onBlur$txtRechercherType() {
		if (txtRechercherType.getValue().equalsIgnoreCase("")) {
			txtRechercherType.setValue(Labels.getLabel("kermel.referentiel.decision.typedecision"));
		}
	}

	public void onClick$btnRechercherType() {
		if (txtRechercherType.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.decision.typedecision")) || txtRechercherType.getValue().equals("")) {
			LibelleType = null;
		} else {
			LibelleType = txtRechercherType.getValue();
		}
		Events.postEvent(ApplicationEvents.ON_TYPEDECISION, this, null);
	}

	// ////////////////fin ////////////////////////////////
}