package sn.ssi.kermel.web.referentiel.controllers;

import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Longbox;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygCoordonateur;
import sn.ssi.kermel.be.referentiel.ejb.CoordonnateurSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;


public class FormCoordonnateurController  extends AbstractWindow implements
EventListener, AfterCompose{

	public static final String PARAM_WINDOW_CODE = "CODE";

	private Textbox txtPnom,txtNom,txtAdd,txtMail,txtCom;
	private Longbox lgNinc,lgTel;
	private SygCoordonateur coordonnateur= new SygCoordonateur() ;
	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGES;
	private int activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String WINDOW_PARAM_MODE = "MODE";
	private String page=null;
    private String mode;
    Long code;
    Integer id;
	
	Session session = getHttpSession();
   @Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
   }

  
   
	/**
	 * Permet de gerer les evenements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

	
	}
	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(WINDOW_PARAM_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			coordonnateur = (SygCoordonateur) map.get(PARAM_WINDOW_CODE);
	
			lgNinc.setValue(coordonnateur.getNin());
			txtPnom.setValue(coordonnateur.getPrenom());
			txtNom.setValue(coordonnateur.getNom());
			txtAdd.setValue(coordonnateur.getAddress());
			lgTel.setValue(coordonnateur.getTel());
			txtCom.setValue(coordonnateur.getCommentaire());
			txtMail.setValue(coordonnateur.getEmail());
		}
	}
	

	
	 public static boolean isValid(String email) {
			if (email != null && email.trim().length() > 0)
				return email
						.matches("^[a-zA-Z0-9\\.\\-\\_]+@([a-zA-Z0-9\\-\\_\\.]+\\.)+([a-zA-Z]{2,4})$");
			return false;
		}
	 
	public void onClick$menuValider() {

	
		if(lgNinc.getValue()==null || "".equals(lgNinc.getValue())){
			throw new WrongValueException(lgNinc, Labels.getLabel("kermel.ChamNull"));
		}
		if(txtPnom.getValue()==null || "".equals(txtPnom.getValue())){
			throw new WrongValueException(txtPnom, Labels.getLabel("kermel.ChamNull"));
		}
		if(txtNom.getValue()==null || "".equals(txtNom.getValue())){
			throw new WrongValueException(txtNom, Labels.getLabel("kermel.ChamNull"));
		}
	
		if(txtAdd.getValue()==null || "".equals(txtAdd.getValue())){
			throw new WrongValueException(txtAdd, Labels.getLabel("kermel.ChamNull"));
		}
		if(lgTel.getValue()==null || "".equals(lgTel.getValue())){
			throw new WrongValueException(lgTel, Labels.getLabel("kermel.ChamNull"));
		}
		if(txtMail.getValue()==null || "".equals(txtMail.getValue())){
			throw new WrongValueException(txtMail, Labels.getLabel("kermel.ChamNull"));
		}
		
       
    	coordonnateur.setNin(lgNinc.getValue());
		coordonnateur.setPrenom(txtPnom.getValue());
		coordonnateur.setNom(txtNom.getValue());
		coordonnateur.setAddress(txtAdd.getValue());
		coordonnateur.setTel(lgTel.getValue());
		coordonnateur.setCommentaire(txtCom.getValue());
		if (isValid(txtMail.getValue())) {
			coordonnateur.setEmail(txtMail.getValue());
		}
		else{
			throw new WrongValueException(txtMail, Labels.getLabel("kermel.mailexception"));

		}
		
		 if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)){
		BeanLocator.defaultLookup(CoordonnateurSession.class).save(coordonnateur);
		    }
		    
		 else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)){
			BeanLocator.defaultLookup(CoordonnateurSession.class).update(coordonnateur);

		}
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			
		 detach();
	}	

	public void  onClick$menuFermer(){

		 detach();
		
	}


	
}

