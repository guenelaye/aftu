package sn.ssi.kermel.web.referentiel.controllers;


import java.util.Date;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygModeselection;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.be.entity.SygTypesmarchesmodesselections;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesSession;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesmodesselectionsSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class TypesmarchescriteresFormController extends AbstractWindow implements
		EventListener, AfterCompose,ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	Long code;
	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null;
    Session session = getHttpSession();
    SygTypesmarches type=new SygTypesmarches();
    SygModeselection modes=new SygModeselection();
    private Long idplan,idrealisation;
    private Label lblInfos,lblRealisation;
    private Menuitem menuValider;
    private Long idtype;
    private Label lblType;
	


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		login = ((String) getHttpSession().getAttribute("user"));
		idtype=(Long) session.getAttribute("idtype");
		type=BeanLocator.defaultLookup(TypesmarchesSession.class).findById(idtype);
		lblType.setValue(type.getLibelle());
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	@Override
	public void onEvent(Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			
			 List<SygModeselection> modes = BeanLocator.defaultLookup(TypesmarchesmodesselectionsSession.class).ListesModes(pgPagination.getActivePage() * byPage,byPage,libelle,type.getId());
			 SimpleListModel listModel = new SimpleListModel(modes);
			 lstListe.setModel(listModel);
			 pgPagination.setTotalSize(BeanLocator.defaultLookup( TypesmarchesmodesselectionsSession.class).ListesModes(libelle,type.getId()).size());
		} 
	}
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygModeselection modes = (SygModeselection) data;
		item.setValue(modes);

		 
		 Listcell cellLibelle = new Listcell(modes.getLibelle());
		 cellLibelle.setParent(item);
	

		 
	}
	
	public void onClick$menuValider()
	{	
		if (lstListe.getSelectedItem() == null)
			
			throw new WrongValueException(menuValider, Labels.getLabel("kermel.error.select.item"));
		for (int i = 0; i < lstListe.getSelectedCount(); i++) {
			 SygTypesmarchesmodesselections criteres=new SygTypesmarchesmodesselections();
			 modes= (SygModeselection)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
			 criteres.setType(type);
			 criteres.setMode(modes);
			 BeanLocator.defaultLookup(TypesmarchesmodesselectionsSession.class).save(criteres);
			 BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_TYPESMARCHESMS", Labels.getLabel("kermel.referentiel.common.typesmarches.modesselections.ajouter")+" :" + new Date(), login);
				
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();
		
	}
	public void onClick$menuFermer()
	{
		detach();
	}
	
	public void onClick$bchercher()
	{
		
		if((txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.libelle")))||(txtLibelle.getValue().equals("")))
		 {
			libelle=null;
			page=null;
		 }
		else
		{
			libelle=txtLibelle.getValue();
			page="0";
		}
		
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	public void onFocus$txtLibelle()
	{
		if(txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.libelle")))
			txtLibelle.setValue("");
	
	}
	public void onBlur$txtLibelle()
	{
		if(txtLibelle.getValue().equals(""))
			txtLibelle.setValue(Labels.getLabel("kermel.referentiel.common.libelle"));
	}
	
	public void onOK$txtLibelle()
	{
		onClick$bchercher();
	}
	
	
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}

}
