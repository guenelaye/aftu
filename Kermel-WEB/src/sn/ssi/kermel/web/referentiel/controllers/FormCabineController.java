package sn.ssi.kermel.web.referentiel.controllers;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Longbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygFormateur;
import sn.ssi.kermel.be.entity.SygSpecialite;
import sn.ssi.kermel.be.entity.SygTypeFormateur;
import sn.ssi.kermel.be.referentiel.ejb.FormateurSession;
import sn.ssi.kermel.be.referentiel.ejb.SpecialiteSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeFormateurSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class FormCabineController  extends AbstractWindow implements
EventListener, AfterCompose{

	private Listbox lstSpec;
	private Paging pgSpec;
	public static final String PARAM_WINDOW_CODE = "CODE";

	private Bandbox bSpec;
	private String spec, nomPiece;
	private Textbox txtRs,txtAdd,txtMailRs,txtCom,txtRechercherSpec,txtPnom,txtNom,txtMail;
	private Longbox lgTelRs,lgTel;
	private SygFormateur formateur =new SygFormateur();
	private SygSpecialite specialite;
	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGES;
	private int activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String WINDOW_PARAM_MODE = "MODE";
	private String page=null;
    private String mode;
    Long code;
    Integer id;
    Long idType;
    private SygTypeFormateur type;
	Session session = getHttpSession();
   @Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		
	   addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
		lstSpec.setItemRenderer(new SpecRenderer());
		pgSpec.setPageSize(byPageBandBox);
		pgSpec.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTRE_ACTION);
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);

		idType=(Long) session.getAttribute("type");
		type=BeanLocator.defaultLookup(TypeFormateurSession.class).findById(idType);
   		}
   		

  
   
	/**
	 * Permet de gerer les evenements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

	
		
		 if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)){//pr step2
			if (event.getData() != null)
			   {
				 
				activePage = Integer.parseInt((String) event.getData());
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
				pgSpec.setPageSize(byPageBandBox);	
			       } 
			    else {
				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgSpec.getActivePage() * byPageBandBox;
				pgSpec.setPageSize(byPageBandBox);
			       
			      }
			      
			  List<SygSpecialite> specialite = BeanLocator.defaultLookup(SpecialiteSession.class).find(activePage, byPageBandBox,null,spec);
			   lstSpec.setModel(new SimpleListModel(specialite));
			   pgSpec.setTotalSize(BeanLocator.defaultLookup(SpecialiteSession.class).count(null,spec));
			        
				}
		
	}
	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(WINDOW_PARAM_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			formateur = (SygFormateur) map.get(PARAM_WINDOW_CODE);
	
			//societe
			txtRs.setValue(formateur.getSociete());
			txtAdd.setValue(formateur.getAdsociete());
			txtMailRs.setValue(formateur.getMailsociete());
			lgTelRs.setValue(formateur.getTelsociete());
			txtCom.setValue(formateur.getCommentaire());
			bSpec.setValue(formateur.getSpecialite().getLibelle());
			 specialite = formateur.getSpecialite();
			
			 //personne contact
			txtPnom.setValue(formateur.getPrenom());
			txtNom.setValue(formateur.getNom());
			lgTel.setValue(formateur.getTel());
			txtMail.setValue(formateur.getEmail()); 

		} 	
	}
	
	
	 public static boolean isValid(String email) {
			if (email != null && email.trim().length() > 0)
				return email
						.matches("^[a-zA-Z0-9\\.\\-\\_]+@([a-zA-Z0-9\\-\\_\\.]+\\.)+([a-zA-Z]{2,4})$");
			return false;
		}
	 
	public void onClick$menuValider() {

		if(txtRs.getValue()==null || "".equals(txtRs.getValue())){
			throw new WrongValueException(txtRs, Labels.getLabel("kermel.ChamNull"));
		}
		if(txtAdd.getValue()==null || "".equals(txtAdd.getValue())){
			throw new WrongValueException(txtAdd, Labels.getLabel("kermel.ChamNull"));
		}
		if(txtMailRs.getValue()==null || "".equals(txtMailRs.getValue())){
			throw new WrongValueException(txtMailRs, Labels.getLabel("kermel.ChamNull"));
		}
		if(lgTelRs.getValue()==null || "".equals(lgTelRs.getValue())){
			throw new WrongValueException(lgTelRs, Labels.getLabel("kermel.ChamNull"));
		}
		if(bSpec.getValue()==null || "".equals(bSpec.getValue())){
			throw new WrongValueException(bSpec, Labels.getLabel("kermel.ChamNull"));
		}
		
     
       //societe
		formateur.setSociete(txtRs.getValue());
		formateur.setAdsociete(txtAdd.getValue());
		if (isValid(txtMailRs.getValue())) {
		formateur.setMailsociete(txtMailRs.getValue());
		}
		else{
			throw new WrongValueException(txtMailRs, Labels.getLabel("kermel.mailexception"));

		}
		formateur.setTelsociete(lgTelRs.getValue());
		formateur.setCommentaire(txtCom.getValue());
		
		  //personne contact
		formateur.setPrenom(txtPnom.getValue());
		formateur.setNom(txtNom.getValue());
		formateur.setTel(lgTel.getValue());
		if(txtMail.getValue()!= null)
		 {
			if (isValid(txtMail.getValue())) {
			formateur.setEmail(txtMail.getValue());
			}
			else{
				throw new WrongValueException(txtMail, Labels.getLabel("kermel.mailexception"));
			}
		}

		formateur.setSpecialite(specialite);
		formateur.setType(type);
		
		 if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)){
		BeanLocator.defaultLookup(FormateurSession.class).save(formateur);
		    }
		    
		 else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)){
			BeanLocator.defaultLookup(FormateurSession.class).update(formateur);

		}
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		 detach();
	}	

	public void  onClick$menuFermer(){
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();
		
	}

	public void onSelect$lstSpec(){
		 specialite = (SygSpecialite) lstSpec.getSelectedItem().getValue();
		 bSpec.setValue(specialite.getLibelle());
		
		 bSpec.close();
	}
	
	 public void onBlur$txtRechercherSpec()
	 {
	 if(txtRechercherSpec.getValue().equals(null))
		 txtRechercherSpec.setValue(Labels.getLabel("kermel.referentiel.nat.form.rechercher"));
	 }
		 
	public void onFocus$txtRechercherSpec(){
		 if(txtRechercherSpec.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.nat.form.rechercher"))){
			 txtRechercherSpec.setValue("");
		 }		 
	}
	
	public void  onClick$txtRechercherSpec(){
		if(txtRechercherSpec.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.nat.form.rechercher")))
		{
			spec = null;
			page = null;
		}
		else
		{
			spec = txtRechercherSpec.getValue();
			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}
	
	public void  onClick$btnRechercherSpec(){
		if(txtRechercherSpec.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.nat.form.rechercher")))
		{
			spec = null;
			page = null;
		}
		else
		{
			spec = txtRechercherSpec.getValue();
			page="0";
		}
			
		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
	}

	

	public class SpecRenderer implements ListitemRenderer {
		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygSpecialite sp= (SygSpecialite) data;
			item.setValue(sp);
			
			Listcell cellSp = new Listcell(sp.getLibelle());
			cellSp.setParent(item);

		}
	}
	
	
}
