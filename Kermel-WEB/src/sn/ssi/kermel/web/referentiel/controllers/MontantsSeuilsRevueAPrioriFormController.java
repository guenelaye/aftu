package sn.ssi.kermel.web.referentiel.controllers;


import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygMontantsSeuils;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.be.entity.SygTypesmarchesmodespassations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.MontantsSeuilsSession;
import sn.ssi.kermel.be.referentiel.ejb.TypeAutoriteContractanteSession;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesSession;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesmodespassationsSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class MontantsSeuilsRevueAPrioriFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	public static final String PARAM_WINDOW_TYPE = "TYPE";
	private String mode,type;
	Long code;
	private	SygMontantsSeuils seuil=new SygMontantsSeuils();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Bandbox bdTypeAutorite,bdTypeMarche,bdModePassation;
	private Paging pgTypeAutorite,pgTypeMarche,pgModePassation;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE,activePage;
	private String libelletypeautorite = null, page=null,libelletypemarche = null,libellemodepassatio = null;
	private Listbox lstTypeAutorite,lstTypeMarche,lstModePassation;
	private Textbox txtRechercherTypeAutorite,txtRechercherTypeMarche,txtRechercherModePassation,txtModeEngagement;
	SygTypeAutoriteContractante typeautorite=null;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygTypesmarches typemarche=null;
	SygModepassation modepassation=null;
	private Decimalbox dcmontantinferieur,dcmontantsuperieur;
	private Combobox cbExamen;
	private Label inferieur,superieur;

	


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_TYPE_AUTORITES, this);
		lstTypeAutorite.setItemRenderer(new TypeAutoritesRenderer());
		pgTypeAutorite.setPageSize(byPageBandbox);
		pgTypeAutorite.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_TYPE_AUTORITES);
		Events.postEvent(ApplicationEvents.ON_TYPE_AUTORITES, this, null);
		
		addEventListener(ApplicationEvents.ON_TYPE_MARCHES, this);
		lstTypeMarche.setItemRenderer(new TypeMarchesRenderer());
		pgTypeMarche.setPageSize(byPageBandbox);
		pgTypeMarche.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_TYPE_MARCHES);
		Events.postEvent(ApplicationEvents.ON_TYPE_MARCHES, this, null);
		
		addEventListener(ApplicationEvents.ON_MODE_PASSATIONS, this);
		lstModePassation.setItemRenderer(new ModesPassationsRenderer());
		pgModePassation.setPageSize(byPageBandbox);
		pgModePassation.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODE_PASSATIONS);
		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		mode = (String) map.get(PARAM_WINDOW_MODE);
		type = (String) map.get(PARAM_WINDOW_TYPE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			seuil = BeanLocator.defaultLookup(MontantsSeuilsSession.class).findById(code);
			typeautorite=seuil.getTypeautorite();
			bdTypeAutorite.setValue(typeautorite.getLibelle());
			typemarche=seuil.getTypemarche();	
			bdTypeMarche.setValue(typemarche.getLibelle());
			if(seuil.getMontantinferieur()!=null)
			dcmontantinferieur.setValue(seuil.getMontantinferieur());
			if(seuil.getMontantsuperieur()!=null)
			dcmontantsuperieur.setValue(seuil.getMontantsuperieur());
			modepassation=seuil.getModepassation();
			bdModePassation.setValue(modepassation.getLibelle());
			txtModeEngagement.setValue(seuil.getModeengagement());
			cbExamen.setValue(seuil.getExamen());
			Events.postEvent(ApplicationEvents.ON_MODE_PASSATIONS, this, null);
		}
		inferieur.setValue(">=");
		superieur.setValue("=<");
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_TYPE_AUTORITES)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgTypeAutorite.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgTypeAutorite.getActivePage() * byPageBandbox;
				pgTypeAutorite.setPageSize(byPageBandbox);
			}
			List<SygTypeAutoriteContractante> typeautorites = BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).find(activePage, byPageBandbox,null,libelletypeautorite);
			lstTypeAutorite.setModel(new SimpleListModel(typeautorites));
			pgTypeAutorite.setTotalSize(BeanLocator.defaultLookup(TypeAutoriteContractanteSession.class).count(null,libelletypeautorite));
		}
		else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_TYPE_MARCHES)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgTypeMarche.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgTypeMarche.getActivePage() * byPageBandbox;
				pgTypeMarche.setPageSize(byPageBandbox);
			}
			List<SygTypesmarches> typemarches = BeanLocator.defaultLookup(TypesmarchesSession.class).find(activePage, byPageBandbox,null,libelletypemarche,null);
			lstTypeMarche.setModel(new SimpleListModel(typemarches));
			pgTypeMarche.setTotalSize(BeanLocator.defaultLookup(TypesmarchesSession.class).count(null,libelletypemarche,null));
		}
		else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODE_PASSATIONS)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgModePassation.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgModePassation.getActivePage() * byPageBandbox;
				pgModePassation.setPageSize(byPageBandbox);
			}
			List<SygTypesmarchesmodespassations> modes = BeanLocator.defaultLookup(TypesmarchesmodespassationsSession.class).find(activePage, byPageBandbox,libellemodepassatio,typemarche,null);
			lstModePassation.setModel(new SimpleListModel(modes));
			pgModePassation.setTotalSize(BeanLocator.defaultLookup(TypesmarchesmodespassationsSession.class).count(libellemodepassatio,typemarche,null));
			
			
		}
	}
	public void onOK() {
		if(checkFieldConstraints())
		{
			seuil.setTypeautorite(typeautorite);
			seuil.setTypemarche(typemarche)	;	
			if(dcmontantinferieur.getValue()!=null)
			seuil.setMontantinferieur(dcmontantinferieur.getValue());
			if(dcmontantsuperieur.getValue()!=null)
			seuil.setMontantsuperieur(dcmontantsuperieur.getValue());
			seuil.setModepassation(modepassation);
			seuil.setModeengagement(txtModeEngagement.getValue());
			seuil.setExamen(cbExamen.getValue());
			seuil.setType(type);
			if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) 
				BeanLocator.defaultLookup(MontantsSeuilsSession.class).save(seuil);
	
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) 
				BeanLocator.defaultLookup(MontantsSeuilsSession.class).update(seuil);
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {		

			if(bdTypeAutorite.getValue().equals(""))
		     {
				errorComponent = bdTypeAutorite;
              	errorMsg = Labels.getLabel("kermel.montantseuils.typeautorite")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(bdTypeMarche.getValue().equals(""))
		     {
				errorComponent = bdTypeMarche;
             	errorMsg = Labels.getLabel("kermel.montantseuils.typemarche")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }

			if((dcmontantinferieur.getValue()==null)&&(dcmontantsuperieur.getValue()==null))
		     {
				errorComponent = dcmontantinferieur;
             	errorMsg = Labels.getLabel("kermel.montantseuils.seuils")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(bdModePassation.getValue().equals(""))
		     {
				errorComponent = bdModePassation;
            	errorMsg = Labels.getLabel("kermel.montantseuils.modepassation")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtModeEngagement.getValue().equals(""))
		     {
				errorComponent = txtModeEngagement;
           	    errorMsg = Labels.getLabel("kermel.montantseuils.modeengagement")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(cbExamen.getValue().equals(""))
		     {
				errorComponent = cbExamen;
          	    errorMsg = Labels.getLabel("kermel.montantseuils.examen")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	

		///////////Type Autorite///////// 
		public void onSelect$lstTypeAutorite(){
			typeautorite= (SygTypeAutoriteContractante) lstTypeAutorite.getSelectedItem().getValue();
			bdTypeAutorite.setValue(typeautorite.getLibelle());
			bdTypeAutorite.close();
		}
		
		public class TypeAutoritesRenderer implements ListitemRenderer{
			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
				SygTypeAutoriteContractante typeautorite = (SygTypeAutoriteContractante) data;
				item.setValue(typeautorite);
				
				Listcell cellLibelle = new Listcell(typeautorite.getLibelle());
				cellLibelle.setParent(item);
		
			}
		}
		
		public void onFocus$txtRechercherTypeAutorite(){
		if(txtRechercherTypeAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.montantseuils.typeautorite"))){
			txtRechercherTypeAutorite.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherTypeAutorite(){
		if(txtRechercherTypeAutorite.getValue().equalsIgnoreCase(Labels.getLabel("kermel.montantseuils.typeautorite")) || txtRechercherTypeAutorite.getValue().equals("")){
			libelletypeautorite = null;
			page=null;
		}
		else{
			libelletypeautorite = txtRechercherTypeAutorite.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_TYPE_AUTORITES, this, page);
		}
		
///////////Type Marche///////// 
		public void onSelect$lstTypeMarche(){
			typemarche= (SygTypesmarches) lstTypeMarche.getSelectedItem().getValue();
			bdTypeMarche.setValue(typemarche.getLibelle());
			Events.postEvent(ApplicationEvents.ON_MODE_PASSATIONS, this, null);
			bdTypeMarche.close();
		}
		
		public class TypeMarchesRenderer implements ListitemRenderer{
			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
				SygTypesmarches typemarche = (SygTypesmarches) data;
				item.setValue(typemarche);
				
				Listcell cellLibelle = new Listcell(typemarche.getLibelle());
				cellLibelle.setParent(item);
		
			}
		}
		
		public void onFocus$txtRechercherTypeMarche(){
		if(txtRechercherTypeMarche.getValue().equalsIgnoreCase(Labels.getLabel("kermel.montantseuils.typemarche"))){
			txtRechercherTypeMarche.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherTypeMarche(){
		if(txtRechercherTypeMarche.getValue().equalsIgnoreCase(Labels.getLabel("kermel.montantseuils.typemarche")) || txtRechercherTypeMarche.getValue().equals("")){
			libelletypemarche = null;
			page=null;
		}
		else{
			libelletypemarche = txtRechercherTypeMarche.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_TYPE_MARCHES, this, page);
		}
		
///////////Mode Passation///////// 
		public void onSelect$lstModePassation(){
			modepassation= (SygModepassation) lstModePassation.getSelectedItem().getValue();
			bdModePassation.setValue(modepassation.getLibelle());
			bdModePassation.close();
		}
		
		public class ModesPassationsRenderer implements ListitemRenderer{
			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
				SygTypesmarchesmodespassations modes = (SygTypesmarchesmodespassations) data;
				item.setValue(modes.getMode());
				
				Listcell cellLibelle = new Listcell(modes.getMode().getLibelle());
				cellLibelle.setParent(item);
		
			}
		}
		
		public void onFocus$txtRechercherModePassation(){
		if(txtRechercherModePassation.getValue().equalsIgnoreCase(Labels.getLabel("kermel.montantseuils.modepassation"))){
			txtRechercherModePassation.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherModePassation(){
		if(txtRechercherModePassation.getValue().equalsIgnoreCase(Labels.getLabel("kermel.montantseuils.modepassation")) || txtRechercherModePassation.getValue().equals("")){
			libellemodepassatio = null;
			page=null;
		}
		else{
			libellemodepassatio = txtRechercherModePassation.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_TYPE_MARCHES, this, page);
		}
}
