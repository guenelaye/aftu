package sn.ssi.kermel.web.referentiel.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygBureauxdcmp;
import sn.ssi.kermel.be.referentiel.ejb.BureauxdcmpSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class ListBureauxdcmpController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle,txtDivision;
    String libelle=null,division=null,page=null;
    private Listheader lshLibelle;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_BUREAUXDCMP, MOD_BUREAUXDCMP, SUPP_BUREAUXDCMP;
    private String login;
    private Long typeunitorg,uniteorg;
    private SygBureauxdcmp directionservice;
    
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_BUREAUXDCMP);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_BUREAUXDCMP != null) { ADD_BUREAUXDCMP.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_BUREAUXDCMP != null) { MOD_BUREAUXDCMP.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUPP_BUREAUXDCMP != null) { SUPP_BUREAUXDCMP.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lshLibelle.setSortAscending(new FieldComparator("libelle", false));
		lshLibelle.setSortDescending(new FieldComparator("libelle", true));
//		lshDivision.setSortAscending(new FieldComparator("division.libelle", false));
//		lshDivision.setSortDescending(new FieldComparator("division.libelle", true));
		
		
		
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	login = ((String) getHttpSession().getAttribute("user"));
    	
    	Object selobj = session.getAttribute("selObj");
    	directionservice = (SygBureauxdcmp) selobj;
    	if (directionservice!=null){
    		typeunitorg=directionservice.getDivision().getId();
    		
    		//if(typeunitorg!=1){
    		//uniteorg=directionservice.getUniteorg().getId();
    		//typeunitorg=directionservice.getId();
    	     uniteorg=directionservice.getId();
    	    // typeunitorg=null;
//    		}else{
//    			uniteorg=null;
  //  		}
    	     typeunitorg=typeunitorg+1;
    	
    	}
    	else{
    		typeunitorg=Long.parseLong("2");
    		uniteorg=Long.parseLong("1");
    		}
		
    	session.setAttribute("typeunitorg", typeunitorg);
    	session.setAttribute("uniteorg", uniteorg);
	}

	
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygBureauxdcmp> bureaux = BeanLocator.defaultLookup(BureauxdcmpSession.class).findRech(activePage,byPage,libelle,null,uniteorg);
			 SimpleListModel listModel = new SimpleListModel(bureaux);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup( BureauxdcmpSession.class).countRech(libelle,null,uniteorg));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/referentiel/bureaux/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"300px");
			display.put(DSP_WIDTH, "50%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(BureauxdcmpFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			final String uri = "/referentiel/bureaux/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"300px");
			display.put(DSP_WIDTH, "50%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(BureauxdcmpFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(BureauxdcmpFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
				
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(BureauxdcmpSession.class).delete(codes);
				BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_BUREAUXDCMP", Labels.getLabel("kermel.referentiel.common.bureaucmp.suppression")+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygBureauxdcmp bureau = (SygBureauxdcmp) data;
		item.setValue(bureau.getId());

		 Listcell cellLibelle = new Listcell(bureau.getLibelle());
		 cellLibelle.setParent(item);
		 
//		 Listcell cellDivision = new Listcell(bureau.getDivision().getLibelle());
//		 cellDivision.setParent(item);
		 
		


	}
	public void onClick$bchercher()
	{
		
		
		if((txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.banque.libelle")))||(txtLibelle.getValue().equals("")))
		 {
			libelle=null;
			page=null;
		 }
		else
		{
			libelle=txtLibelle.getValue();
			page="0";
		}
		if((txtDivision.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.division")))||(txtDivision.getValue().equals("")))
		 {
			division=null;
			page=null;
		 }
		else
		{
			division=txtDivision.getValue();
			page="0";
		}
		
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	
	public void onFocus$txtLibelle()
	{
		if(txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.banque.libelle")))
			txtLibelle.setValue("");
		
	}
	public void onBlur$txtLibelle()
	{
		if(txtLibelle.getValue().equals(""))
			txtLibelle.setValue(Labels.getLabel("kermel.referentiel.banque.libelle"));
	}
	
	public void onOK$txtLibelle()
	{
		onClick$bchercher();
	}
	public void onBlur$txtDivision()
	{
		if(txtDivision.getValue().equals(""))
			txtDivision.setValue(Labels.getLabel("kermel.referentiel.common.division"));
	}
	public void onFocus$txtDivision()
	{
		if(txtDivision.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.division")))
			txtDivision.setValue("");
	
	}
	public void onOK$txtDivision()
	{
		onClick$bchercher();
	}
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
	
}