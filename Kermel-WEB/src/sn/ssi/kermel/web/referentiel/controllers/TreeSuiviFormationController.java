package sn.ssi.kermel.web.referentiel.controllers;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Include;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treechildren;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.West;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAnnee;
import sn.ssi.kermel.be.entity.SygMois;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.referentiel.ejb.ProformationSession;
import sn.ssi.kermel.be.session.AnneeSession;
import sn.ssi.kermel.be.session.MoisSession;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class TreeSuiviFormationController extends AbstractWindow implements
		AfterCompose, IdSpace {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Textbox txtSearch;
	private Include content;
	private Combobox listCategory;
	private Tree tree;
	private Treechildren child = new Treechildren();
    Session session = getHttpSession();
	private SygAnnee region = null;
	private SygMois departement = null;
	private SygProformation formation=null;
	private String openWest;
	private West wp;
	private String libelle;
	
	private Combobox combo;
	private Button bchercher;
	Date date = new Date();

	@SuppressWarnings("unchecked")
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		region = (SygAnnee) Executions.getCurrent().getAttribute("annee");
		openWest = (String) Executions.getCurrent().getAttribute("openWest");
		wp.setOpen(true);
//		campagne = BeanLocator.defaultLookup(CampagneAgricolSession.class).findByCode((Integer) getHttpSession().getAttribute(UIConstants.CAMPAGNE));


		txtSearch.setValue("Recherche");
		initTree();
		if (region != null) {
			wp.setOpen(true);
		}

	}

	private void initTree() {
		child.setParent(tree);
		treeProcedureBuilder();
		content.setSrc("/referentiel/proformation/suivie/listesuivi.zul");
		String test = ToolKermel.StringDateToStringSql("11-08-2010");
		System.out.println("beeeeeeeeeeeeeennnnn  :    "+test);
		Date date = ToolKermel.stringToDateSQL(test);
		System.out.println("    ");
		System.out.println("                  date  :    "+date);
		
	}
	

	public void onSelect$tree() {
		tree.isInvalidated();
		Object selobj = tree.getSelectedItem().getValue();

		session.setAttribute("selObj", selobj);
		
		content.setSrc("/referentiel/proformation/suivie/pagesuivi.zul");
		if (selobj instanceof SygAnnee) {

			List<String> status = new ArrayList<String>();
	        region = (SygAnnee) selobj;
			session.setAttribute("annee",region.getId());
			
//			content.setSrc("/referentiel/proformation/page.zul");
			tree.getSelectedItem().setOpen(true);
//			content.setSrc("/referentiel/proformation/suivie/listesuivi.zul?pageId="	+ "p__@" + tree.getSelectedItem().getId());
//			content.setMode("defer");
		
		} else if (selobj instanceof SygMois) {
			List<String> status = new ArrayList<String>();
			departement = (SygMois) selobj;
			session.setAttribute("mois",departement.getId());
			session.setAttribute("annee",tree.getSelectedItem().getAttribute("annee"));
//			content.setSrc("/referentiel/proformation/page.zul");
			content.setSrc("/referentiel/proformation/suivie/listesuivi.zul?pageId="	+ "p__@" + tree.getSelectedItem().getId());
			content.setMode("defer");			

			
		}
		 else if (selobj instanceof SygProformation) {
				List<String> status = new ArrayList<String>();
				formation = (SygProformation) selobj;
				session.setAttribute("formation",formation);
				session.setAttribute("dateDebut",formation.getForDateDebut());
				
				content.setSrc("/referentiel/proformation/suivie/listesuivi.zul?pageId="	+ "p__@" + tree.getSelectedItem().getId());
				content.setMode("defer");			

				
			}
		
		
	}

	@SuppressWarnings("unchecked")
	public void onSelect$listCategory() {
		

	}

	public void onClick$search_img() {
		search();
	}

	@SuppressWarnings("unchecked")
	public void search() {
		

	}

	public void recherche() {
		if (combo.getValue().equals("Régions") || combo.getValue().equals("Départements") || combo.getValue().equals("Marchés")) {
			bchercher.setVisible(false);
			txtSearch.setVisible(true);
			combo.setVisible(false);
			
		}
	}
	
	public void onClick$bchercher() {
		recherche();
	}
	public void treeBuilder() {
		
	}

	public void treeProcedureBuilder() {
		
		 String key = null;
		
		 List<SygAnnee> listProcedure = BeanLocator.defaultLookup(AnneeSession.class).find(0, -1, libelle);
		 System.out.println("==============================ok");
			
		 for (SygAnnee ohProcedure : listProcedure) {
		
			 Treeitem procedureItem = new Treeitem();
			
			 procedureItem.setLabel(ohProcedure.getLibelle());
			 procedureItem.setTooltiptext(ohProcedure.getLibelle());
			
			 procedureItem.setImage("/images/puce.png");
			 procedureItem.setValue(ohProcedure);
			 procedureItem.setOpen(false);
			
			 procedureItem.setParent(child);
			
					 
			 List<SygMois> listEtapes = BeanLocator.defaultLookup(MoisSession.class).find(0, -1, null, null, ohProcedure);
				
				
			 if(listEtapes.size() > 0){
			
				 Treechildren procedureTreechildren = new Treechildren();
				 for (SygMois ohEtape : listEtapes) {
					 Treeitem etapeItem = new Treeitem();
				
					 etapeItem.setLabel(ohEtape.getLibelle()+" ("+ BeanLocator.defaultLookup(
								ProformationSession.class).count(null, ohEtape,ohProcedure,-1)+")");
					 etapeItem.setTooltiptext(ohEtape.getLibelle());
					
					 etapeItem.setImage("/images/puce.png");
				
					 etapeItem.setValue(ohEtape);
					 etapeItem.setAttribute("annee", ohProcedure.getId());
					 etapeItem.setOpen(false);
					
					 etapeItem.setParent(procedureTreechildren);
					
					
				 }
				 procedureTreechildren.setParent(procedureItem);
			 }
		 }
		 

	}

	public void treeEtapeBuilder() {
		
	}

	public void treeTacheBuilder() {
		
	}

	public void treeOperationBuilder() {
		
	}

	public void removeTreeChildren() {
		while (child.getItemCount() > 0) {
			child.removeChild(child.getFirstChild());
		}
	}


	


	public void onOpen$tree() {

		System.out.println(tree.getSelectedItem().getId());
	}

	public void onFocus$txtSearch() {
		if (txtSearch.getValue().equalsIgnoreCase("Recherche")) {
			txtSearch.setValue(null);
		}
	}

	public void onBlur$txtSearch() {
		if (txtSearch.getValue().equalsIgnoreCase("")
				|| (txtSearch.getValue().equalsIgnoreCase(null))) {
			txtSearch.setValue("Recherche");
		}
	}

	public void onOK$txtSearch() {
		onClick$btnRechercher();
	}



	public void chargerAll() {
		List<SygAnnee> listRegion = BeanLocator.defaultLookup(AnneeSession.class).find(0, -1, libelle);
		int i = 0;
		for (SygAnnee region : listRegion) {

			Treeitem procedureItem = new Treeitem();
			procedureItem.setAttribute("index", i);
			procedureItem.setLabel(region.getLibelle());
			procedureItem.setTooltiptext(region.getLibelle());

			procedureItem.setValue(region);
			procedureItem.setOpen(false);

			procedureItem.setParent(child);

			System.out.println(procedureItem.getId());
			if ((this.region != null)
					&& (region.getLibelle().equalsIgnoreCase(this.region.getLibelle()))) {
				procedureItem.setSelected(true);
				procedureItem.setOpen(true);
				tree.setSelectedItem(procedureItem);

				onSelect$tree();
			}
			i++;
			List<SygMois> listDepartement = BeanLocator.defaultLookup(MoisSession.class).find(0, -1, region.getId());
			if (listDepartement.size() > 0) {

				Treechildren procedureTreechildren = new Treechildren();
				for (SygMois departement : listDepartement) {
					Treeitem etapeItem = new Treeitem();
					etapeItem.setAttribute("index", i);
					etapeItem.setId(departement.getCode().toString() + i);
					etapeItem.setTooltiptext(departement.getLibelle());

					etapeItem.setValue(departement);
					etapeItem.setOpen(false);

					etapeItem.setParent(procedureTreechildren);
					if (this.departement != null
							&& (departement.getId() == this.departement.getId())) {
						etapeItem.setSelected(true);
						etapeItem.setOpen(true);
						tree.setSelectedItem(etapeItem);
						onSelect$tree();
					}
					i++;
				}
				procedureTreechildren.setParent(procedureItem);
			}
		}
	}


	public void onClick$btnRechercher() {
		if (txtSearch.getValue() != null
				&& !txtSearch.getValue().trim().equals("")) {
			libelle = txtSearch.getValue();
		} else {
			libelle = null;
		}
		chargerSearch();
	}
	
	public void chargerSearch() {
		removeTreeChildren();
		treeProcedureBuilder();

	}
	
}