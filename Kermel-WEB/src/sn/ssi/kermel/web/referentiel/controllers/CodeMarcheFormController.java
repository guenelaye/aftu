package sn.ssi.kermel.web.referentiel.controllers;

import java.util.Calendar;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygCodeMarche;
import sn.ssi.kermel.be.referentiel.ejb.CodeMarcheSession;
import sn.ssi.kermel.web.accueil.controllers.UIUtils;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class CodeMarcheFormController extends AbstractWindow implements EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode, login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle;
	Long code;
	private SygCodeMarche codemarche = new SygCodeMarche();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";

	private Textbox txtLibelles, txtVersionElectronique, txtDescription;
	private Datebox dtdate;
	private String nomFichier;
	private final String cheminDossier = UIConstants.PATH_CODEMARCHE;
	UtilVue utilVue = UtilVue.getInstance();
	private Button btnChoixFichier;

	@Override
	public void onEvent(Event event) throws Exception {

	}

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			codemarche = BeanLocator.defaultLookup(CodeMarcheSession.class).findById(code);
			dtdate.setValue(codemarche.getDate());
			txtLibelles.setValue(codemarche.getLibelle());
			txtVersionElectronique.setValue(codemarche.getFichier());
			txtDescription.setValue(codemarche.getDescription());

		}
	}
	
	
	public void onUpload(UploadEvent event) {

		try {
			Messagebox.show((String) Executions.getCurrent().getAttribute(
					"FICHIER"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onClick$btnChoixFichier(Event aEvent) {
		UIUtils.uploadFile(btnChoixFichier);
	}
	
//	public void onClick$btnChoixFichier() {
//
//		if (ToolKermel.isWindows())
//			nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "rap",
//					cheminDossier.replaceAll("/", "\\\\"));
//		else
//			nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "rap",
//					cheminDossier.replaceAll("\\\\", "/"));
//
//		txtVersionElectronique.setValue(nomFichier);
//	}
	
	private boolean checkFieldConstraints() {

		try {

			if (txtLibelles.getValue().equals("")) {
				errorComponent = txtLibelle;
				errorMsg = Labels.getLabel("kermel.referentiel.rapport.Libelle") + ": "
						+ Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);
			}

			if (dtdate.getValue() == null) {
				errorComponent = dtdate;
				errorMsg = Labels.getLabel("kermel.referentiel.rapport.Date") + ": "
						+ Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);
			}
			if (txtVersionElectronique.getValue().equals("")) {
				errorComponent = txtVersionElectronique;
				errorMsg = Labels.getLabel("kermel.referentiel.rapport.Fichier") + ": "
						+ Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);
			}

			return true;

		} catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
					+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

		}

	}

	public void onOK() {
		if (checkFieldConstraints()) {
			codemarche.setLibelle(txtLibelles.getValue());
			codemarche.setFichier(txtVersionElectronique.getValue());
			codemarche.setDate(dtdate.getValue());
			codemarche.setDescription(txtDescription.getValue());
			codemarche.setStatut(Labels.getLabel("kermel.referentiel.rapport.statut.nopublier"));

			if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(CodeMarcheSession.class).save(codemarche);

			} else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(CodeMarcheSession.class).update(codemarche);
			}

			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}
	
	public void onUpload$btnChoixFichier(Event aEvent)
			throws InterruptedException {
		String format = null;
		String taillefinal = null;
		String name = null;
		double taille = new Double(0);

		final UploadEvent event = ((UploadEvent) ((ForwardEvent) aEvent)
				.getOrigin());
		name = event.getMedia().getName();

		format = event.getMedia().getFormat();
		double taill = ((event.getMedia().getByteData().length));
		taille = (taill / 1024);
		String cheminDossier;
		cheminDossier = UIConstants.path+"\\alldocs\\codemarche";
		if (ToolKermel.isWindows()) {
			nomFichier = FileLoader.uploadPieceJointe(UtilVue.getInstance()
					.formateLaDate3(Calendar.getInstance().getTime()), "Pj",
					cheminDossier.replaceAll("/", "\\\\"), event.getMedia());
		} else {
			nomFichier = FileLoader.uploadPieceJointe(UtilVue.getInstance()
					.formateLaDate3(Calendar.getInstance().getTime()), "Pj",
					cheminDossier.replaceAll("\\\\", "/"), event.getMedia());
		}
		txtVersionElectronique.setValue(nomFichier);
	}
}
