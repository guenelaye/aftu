package sn.ssi.kermel.web.referentiel.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygCategori;
import sn.ssi.kermel.be.entity.SygListeRougeF;
import sn.ssi.kermel.be.referentielprix.ejb.ListeRougeFournisseurSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class ListeRougeFournisseurController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,division=null,page=null;
    private Listheader lshLibelle;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_LISTE_ROUGE_F, MOD_LISTE_ROUGE_F, SUP_LISTE_ROUGE_F,PUB_LISTE_ROUGE_F;
    private String login;
    private Long typecategori,idcategori;
    private SygCategori categori;
    private String statut,imagestatut;
    private Long idrapport;
    private SygListeRougeF  listRouge;
    
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.FEA_LISTE_ROUGE_FOURNISSEUR);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_LISTE_ROUGE_F != null) { ADD_LISTE_ROUGE_F.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_LISTE_ROUGE_F != null) { MOD_LISTE_ROUGE_F.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUP_LISTE_ROUGE_F != null) { SUP_LISTE_ROUGE_F.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		if (PUB_LISTE_ROUGE_F != null) { PUB_LISTE_ROUGE_F.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_AUTRE_ACTION); }
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		//lshLibelle.setSortAscending(new FieldComparator("libelle", false));
		//lshLibelle.setSortDescending(new FieldComparator("libelle", true));
//		lshDivision.setSortAscending(new FieldComparator("division.libelle", false));
//		lshDivision.setSortDescending(new FieldComparator("division.libelle", true));
		
		
		
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	login = ((String) getHttpSession().getAttribute("user"));
    	
    	/*
    	Object selobj = session.getAttribute("selObj");
    	categori = (SygCategori) selobj;
    	if (categori!=null){
    		typecategori=categori.getType().getId();
    		
    		idcategori=categori.getId();
   
    		typecategori=typecategori+1;
    	
    	}
    	else{
    		typecategori=Long.parseLong("2");
    		idcategori=Long.parseLong("1");
    		}
		
    	session.setAttribute("typecategori", typecategori);
    	session.setAttribute("idcategori", idcategori);
       
    	*/
	}

	
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygListeRougeF> listeRouge = BeanLocator.defaultLookup(ListeRougeFournisseurSession.class).findRech(activePage,byPage,null,null,null);
			 SimpleListModel listModel = new SimpleListModel(listeRouge);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup( ListeRougeFournisseurSession.class).countRech(null,null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/listerougefournisseur/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, "Nouveau");
			display.put(DSP_HEIGHT,"600px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormCategoriController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			final String uri = "/listerougefournisseur/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"90%");
			display.put(DSP_WIDTH, "70%");
			display.put(DSP_TITLE, "Editer");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormListeRougeFournisseurController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(FormListeRougeFournisseurController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
				
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(ListeRougeFournisseurSession.class).delete(codes);
				//BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_CAT", Labels.getLabel("kermel.referentiel.common.bureaucmp.suppression")+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
		     
			else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
				Listcell button = (Listcell) event.getTarget();
				String toDo = (String) button.getAttribute(UIConstants.TODO);
				idrapport = (Long) button.getAttribute("idrapport");
				listRouge=BeanLocator.defaultLookup(ListeRougeFournisseurSession.class).findById(idrapport);
				
				if (toDo.equalsIgnoreCase("publier"))
				{
					/*
					HashMap<String, String> display = new HashMap<String, String>(); // permet
					 display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.plansdepassation.publier"));
					 display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.referentiel.rapport.publier.titre"));
					 display.put(MessageBoxController.DSP_HEIGHT, "150px");
					 display.put(MessageBoxController.DSP_WIDTH, "100px");
			        HashMap<String, Object> map = new HashMap<String, Object>(); // permet
			         map.put(CONFIRMPUBLIER, "Publication_Confirmer");
					 showMessageBox(display, map);
					*/
					listRouge.setStatut("yes");
					BeanLocator.defaultLookup(ListeRougeFournisseurSession.class).update(listRouge);
					
					 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);		
					
				}else if(toDo.equalsIgnoreCase("d�publier")){
					
					listRouge.setStatut(Labels.getLabel("kermel.referentiel.rapport.statut.nopublier"));
					//code.setDatepublication(null);
					BeanLocator.defaultLookup(ListeRougeFournisseurSession.class).update(listRouge);
					
					
					 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
					
				}
			}
	
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygListeRougeF  fournisseur = (SygListeRougeF) data;
		item.setValue(fournisseur.getId());
		 statut=fournisseur.getStatut();
		 Listcell cellRef = new Listcell(fournisseur.getReference());
		 cellRef.setParent(item);
		 
		 Listcell cellRaison = new Listcell(fournisseur.getRaisonsociale());
		 cellRaison.setParent(item);
		 
		 Listcell cellDecision = new Listcell("");
		 if(fournisseur.getDecision()!=null)
		 cellDecision = new Listcell(fournisseur.getDecision().getLibelle());
		 cellDecision.setParent(item);
		 
		 Listcell cellDate1 = new Listcell("");
		 if(fournisseur.getDatedebut()!=null)
		  cellDate1 = new Listcell(fournisseur.getDatedebut().toString());
		 cellDate1.setParent(item);

		 Listcell cellDate2 = new Listcell("");
		 if(fournisseur.getDatfin()!=null)
		 cellDate2 = new Listcell(fournisseur.getDatfin().toString());
		 cellDate2.setParent(item);
		 
		 Listcell cellMotif = new Listcell(fournisseur.getMotif());
		 cellMotif.setParent(item);
		 
		 
        Listcell cellImageStatut = new Listcell("");
		 
		 if(statut.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.rapport.statut.nopublier"))){
			 imagestatut="/images/deletemoins.png";
			 
			 cellImageStatut.setAttribute(UIConstants.TODO, "publier");
			 cellImageStatut.setTooltiptext("Publier Rapport");
		 }else{
			 imagestatut="/images/accept.png";
			 cellImageStatut.setAttribute(UIConstants.TODO, "d�publier");
			 cellImageStatut.setTooltiptext("D�publier Rapport");
		 }
		
		    cellImageStatut.setImage(imagestatut);
			cellImageStatut.setAttribute("idrapport", fournisseur.getId());
			cellImageStatut.addEventListener(Events.ON_CLICK, ListeRougeFournisseurController.this);
			cellImageStatut.setParent(item);
		 


	}
	public void onClick$bchercher()
	{
		
		
		if((txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.banque.libelle")))||(txtLibelle.getValue().equals("")))
		 {
			libelle=null;
			page=null;
		 }
		else
		{
			libelle=txtLibelle.getValue();
			page="0";
		}
	
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	
	public void onFocus$txtLibelle()
	{
		if(txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.banque.libelle")))
			txtLibelle.setValue("");
		
	}
	public void onBlur$txtLibelle()
	{
		if(txtLibelle.getValue().equals(""))
			txtLibelle.setValue(Labels.getLabel("kermel.referentiel.banque.libelle"));
	}
	
	public void onOK$txtLibelle()
	{
		onClick$bchercher();
	}
	
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
	
}