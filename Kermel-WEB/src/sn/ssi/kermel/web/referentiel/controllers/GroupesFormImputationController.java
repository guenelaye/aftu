package sn.ssi.kermel.web.referentiel.controllers;


import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
//import sn.ssi.kermel.be.entity.EcoContactsCarnet;
//import sn.ssi.ecourrier.be.entity.EcoDossiers;
//import sn.ssi.ecourrier.be.entity.EcoGroupesCarnet;
import sn.ssi.kermel.be.entity.EcoGroupesImputation;
//import sn.ssi.kermel.be.carnet.ejb.ContactsCarnetSession;
//import sn.ssi.kermel.be.carnet.ejb.GroupesCarnetSession;
//import sn.ssi.kermel.be.referentiel.ejb.DossiersSession;
import sn.ssi.kermel.be.referentiel.ejb.GroupeImputationSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class GroupesFormImputationController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtraison,txtAdresse, txtEmail,txtfax,txtdescriptions;
	private Intbox txtTel;
	Long code;
	//private	EcoContactsCarnet contact = new EcoContactsCarnet();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	//List<EcoContactsCarnet> contacts = new ArrayList<EcoContactsCarnet>();
	
	private Bandbox bdGroupe;
	private Textbox txtRechercherGroupe,txtlibelle,txtDescription;
	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
	private Paging pgGroupe;
	private Listbox lstGroupe;
	private int activePage;
	//private EcoDossiers dossiers= new EcoDossiers();
	private EcoGroupesImputation GroupesImputation = new EcoGroupesImputation();
	private String groupeLibelle;
	private String page = null;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
//		addEventListener(ApplicationEvents.ON_AUTRE_ACTION, this);
//   		lstGroupe.setItemRenderer(new GroupeRenderer());
//   		pgGroupe.setPageSize(byPageBandBox);
//   		pgGroupe.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_AUTRE_ACTION);
//   		Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, null);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			GroupesImputation = BeanLocator.defaultLookup(GroupeImputationSession.class).findById(code);
			
			txtlibelle.setValue(GroupesImputation.getLibelle());
			txtdescriptions.setValue(GroupesImputation.getDescription());
			
			
			
			
			//bdGroupe.setValue(contact.getGroupe().getLibelle());
		}
	}
	
//	@Override
//	public void onEvent(Event event) throws Exception {
//		// TODO Auto-generated method stub
//		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_AUTRE_ACTION)){
//			if (event.getData() != null) {
//				activePage = Integer.parseInt((String) event.getData());
//				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
//				pgGroupe.setPageSize(byPageBandBox);
//			} 
//			else {
//				byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
//				activePage = pgGroupe.getActivePage() * byPageBandBox;
//				pgGroupe.setPageSize(byPageBandBox);
//			}
//			List<EcoGroupesCarnet> grpe = BeanLocator.defaultLookup(GroupesCarnetSession.class).find(activePage, byPageBandBox, null, groupeLibelle);
//			lstGroupe.setModel(new SimpleListModel(grpe));
//			pgGroupe.setTotalSize(BeanLocator.defaultLookup(GroupesCarnetSession.class).count(null, groupeLibelle));
//		}
//	}

	public void onOK() {
		if(checkFieldConstraints())
		{
//			if (lstGroupe.getSelectedItem() == null)
//				throw new WrongValueException(bdGroupe, Labels.getLabel("ecourrier.erreur.champobligatoire"));
			
			GroupesImputation.setLibelle(txtlibelle.getValue());
			GroupesImputation.setDescription(txtdescriptions.getValue());
			
			
			
			//contact.setGroupe(groupe);
			//contact.setType("prive"); //a dynamiser
		  	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(GroupeImputationSession.class).save(GroupesImputation);
				//BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_CONTACTPRIVE", Labels.getLabel("ecourrier.referentiel.common.groupe.ajouter")+" :" + UtilVue.getInstance().formateLaDate(new Date()), login);
				
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(GroupeImputationSession.class).update(GroupesImputation);
				//BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_CONTACTPRIVE", Labels.getLabel("ecourrier.referentiel.common.groupe.modifier")+" :" + UtilVue.getInstance().formateLaDate(new Date()), login);
				
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}
	
	//Groupe
//		public void onSelect$lstGroupe(){
//			 groupe = (EcoGroupesCarnet) lstGroupe.getSelectedItem().getValue();
//			 bdGroupe.setValue(groupe.getLibelle());
//			
//			 bdGroupe.close();
//		}
		
//		public class GroupeRenderer implements ListitemRenderer {
//
//			@Override
//			public void render(Listitem item, Object data, int arg2)
//					throws Exception {
//				EcoGroupesCarnet grpe = (EcoGroupesCarnet) data;
//				item.setValue(grpe);
//				
//				Listcell cellLibelle = new Listcell(grpe.getLibelle());
//				cellLibelle.setParent(item);				
//			}
//			
//		}
		
		
//		public void onFocus$txtRechercherGroupe(){
//			 if(txtRechercherGroupe.getValue().equalsIgnoreCase(Labels.getLabel("ecourrier.common.form.rechercher"))){
//				 txtRechercherGroupe.setValue("");
//			 }		 
//		}
//		
//		public void  onClick$btnRechercherGroupe(){
//			if(txtRechercherGroupe.getValue().equalsIgnoreCase(Labels.getLabel("ecourrier.common.form.rechercher")))
//			{
//				groupeLibelle = null;
//				page = null;
//			}
//			else
//			{
//				groupeLibelle = txtRechercherGroupe.getValue();
//				page="0";
//			}
//				
//			Events.postEvent(ApplicationEvents.ON_AUTRE_ACTION, this, page);
//		}


	//contr�le erreurs
	private boolean checkFieldConstraints() {
		
		try {
		
			if(txtlibelle.getValue().equals(""))
		     {
               errorComponent = txtlibelle;
               errorMsg = Labels.getLabel("ecourrier.referentiel.common.libelle")+": "+Labels.getLabel("ecourrier.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }

			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("ecourrier.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

	@Override
	public void onEvent(Event arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}
	

}
