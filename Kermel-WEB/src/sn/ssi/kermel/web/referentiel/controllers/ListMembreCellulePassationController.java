package sn.ssi.kermel.web.referentiel.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
//import sn.ssi.kermel.be.entity.SygCatFournisseur;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygMembreCellulePassation;
import sn.ssi.kermel.be.entity.Utilisateur;
//import sn.ssi.kermel.be.referentiel.ejb.CatFourSession;
import sn.ssi.kermel.be.referentiel.ejb.MembreCellulePassationSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class ListMembreCellulePassationController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtNom, txtPrenom;
    String nom=null, prenom=null, page=null;
    private Listheader lshNom, lshPrenom;
    String login;
    private SygAutoriteContractante autorite = null;
    
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_MEMBRE, MOD_MEMBRE, SUPP_MEMBRE;
    private Utilisateur infoscompte;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_MEMBRES);
		monSousMenu.afterCompose();
		Components.wireFellows(this, this);
		if (ADD_MEMBRE != null) { ADD_MEMBRE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_MEMBRE != null) { MOD_MEMBRE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUPP_MEMBRE != null) { SUPP_MEMBRE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

		lshNom.setSortAscending(new FieldComparator("nom", false));
		lshNom.setSortDescending(new FieldComparator("nom", true));
		lshPrenom.setSortAscending(new FieldComparator("prenom", false));
		lshPrenom.setSortDescending(new FieldComparator("prenom", true));
				
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
    	
    	login = ((String) getHttpSession().getAttribute("user"));
	}

	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygMembreCellulePassation> membres = BeanLocator.defaultLookup(MembreCellulePassationSession.class).find(activePage,byPage,nom,prenom,autorite);
			 SimpleListModel listModel = new SimpleListModel(membres);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup( MembreCellulePassationSession.class).count(nom,prenom,autorite));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/referentiel/divisionmarche/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(AnnuaireFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListe.getSelectedItem() == null)
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			
			final String uri = "/referentiel/divisionmarche/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(AnnuaireFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(AnnuaireFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				
				HashMap<String, String> display = new HashMap<String, String>();
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); 
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(MembreCellulePassationSession.class).delete(codes);
				BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_MEMBRE", Labels.getLabel("kermel.referentiel.common.membre.suppression")+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygMembreCellulePassation membre = (SygMembreCellulePassation) data;
		item.setValue(membre.getId());

		Listcell cellNom = new Listcell(membre.getNom());
		cellNom.setParent(item);
		
		Listcell cellPrenom = new Listcell(membre.getPrenom());
		cellPrenom.setParent(item);
		
		Listcell cellFonction = new Listcell(membre.getFonction());
		cellFonction.setParent(item);
		
		Listcell cellTel = new Listcell(membre.getTel());
		cellTel.setParent(item);
		
		Listcell cellEmail = new Listcell(membre.getEmail());
		cellEmail.setParent(item);
		
		Listcell cellService = new Listcell(membre.getService());
		cellService.setParent(item);
	}
	
	public void onClick$bchercher()
	{
		prenom = txtPrenom.getValue();
		nom = txtNom.getValue();
		if(prenom.equalsIgnoreCase(Labels.getLabel("kermel.common.form.prenom")) || "".equals(prenom))
			prenom = null;
		
		if(nom.equalsIgnoreCase(Labels.getLabel("kermel.common.form.nom")) || "".equals(nom))
			nom = null;
		
		txtPrenom.setValue(Labels.getLabel("kermel.common.form.prenom"));
		txtNom.setValue(Labels.getLabel("kermel.common.form.nom"));
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	
	public void onBlur$txtNom()
	{
		if(txtNom.getValue().equals(""))
			txtNom.setValue(Labels.getLabel("kermel.common.form.nom"));
	}
	public void onFocus$txtNom()
	{
		if(txtNom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.nom")))
			txtNom.setValue("");
	
	}
	public void onOK$txtNom()
	{
		onClick$bchercher();
	}
	
	public void onBlur$txtPrenom()
	{
		if(txtPrenom.getValue().equals(""))
			txtPrenom.setValue(Labels.getLabel("kermel.common.form.prenom"));
	}
	public void onFocus$txtPrenom()
	{
		if(txtPrenom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.prenom")))
			txtPrenom.setValue("");
	
	}
	public void onOK$txtPrenom()
	{
		onClick$bchercher();
	}
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
}