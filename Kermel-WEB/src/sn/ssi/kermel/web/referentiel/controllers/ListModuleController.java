
	package sn.ssi.kermel.web.referentiel.controllers;

	import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygModule;
import sn.ssi.kermel.be.referentiel.ejb.ModuleSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

	@SuppressWarnings("serial")
	public class ListModuleController  extends AbstractWindow implements EventListener, AfterCompose, ListitemRenderer {

		private Listbox list;
		private Paging pg;
		public static final String PARAM_WINDOW_CODE = "CODE";

		private Listheader lshLib;
		
		private Textbox txtLib,txtCode;
		private SygModule module =new SygModule();

		private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
	    private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
		private int activePage;
		public static final String CURRENT_MODULE="CURRENT_MODULE";
		public static final String WINDOW_PARAM_MODE = "MODE";
		private String page=null,lib,code;
		Session session = getHttpSession();

		
		private KermelSousMenu monSousMenu;
		private Menuitem ADD_MODULE, MOD_MODULE, DEL_MODULE;
	    
		@Override
		public void afterCompose() {
			Components.wireFellows(this, this);
			Components.addForwards(this, this);
			
			monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
			monSousMenu.setFea_code(UIConstants.REF_MODULE);
			monSousMenu.afterCompose();
			Components.wireFellows(this, this);
			if (ADD_MODULE != null) {
				ADD_MODULE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
			}
			if (MOD_MODULE != null) {
				MOD_MODULE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
			}
			if (DEL_MODULE != null) {
				DEL_MODULE.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
			}
		
			
			addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
			addEventListener(ApplicationEvents.ON_ADD, this);
			addEventListener(ApplicationEvents.ON_EDIT, this);
			addEventListener(ApplicationEvents.ON_DELETE, this);
			addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
			Events.postEvent(ApplicationEvents.ON_CLICK, this, null);
			
			lshLib.setSortAscending(new FieldComparator("libelle", false));
			lshLib.setSortDescending(new FieldComparator("libelle", true));
			list.setItemRenderer(this);
			pg.setPageSize(byPage);
			pg.addForward("onPaging", this,ApplicationEvents.ON_MODEL_CHANGE);
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

	   	
		}

		/**
		 * Permet de gerer les evenements survenus sur la page correspondante.
		 */
		@SuppressWarnings("unchecked")
		@Override
		public void onEvent(final Event event) throws Exception {

			/*
			 * Lorsque cet evenement survient, les evenements de la liste sont mis �
			 * jour de meme que l'element de pagination
			 */
			if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
	 			 List<SygModule> modul = BeanLocator.defaultLookup(ModuleSession.class).find(pg.getActivePage()*byPage,byPage,code,lib);

	 			 SimpleListModel listModel = new SimpleListModel(modul);
				list.setModel(listModel);
				pg.setTotalSize(BeanLocator.defaultLookup(ModuleSession.class).count(code,lib));
			} 
		
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
				final String uri = "/module/form.zul";

				final HashMap<String, String> display = new HashMap<String, String>();
				display.put(DSP_TITLE, Labels.getLabel("kermel.module.ajout"));
				display.put(DSP_HEIGHT,"85%");
				display.put(DSP_WIDTH, "80%");

				final HashMap<String, Object> data = new HashMap<String, Object>();
				data.put(FormModuleController.WINDOW_PARAM_MODE,UIConstants.MODE_NEW);
				
				showPopupWindow(uri, data, display);
			} 
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
				if (list.getSelectedItem() == null) {
					throw new WrongValueException(list, Labels.getLabel("kermel.referentiel.nat.message.selection"));
					
				}
				final String uri ="/module/form.zul";

				final HashMap<String, String> display = new HashMap<String, String>();
				display.put(DSP_HEIGHT,"85%");
				display.put(DSP_WIDTH, "80%");
				display.put(DSP_TITLE, Labels.getLabel("kermel.module.modif"));

				final HashMap<String, Object> data = new HashMap<String, Object>();
				data.put(FormModuleController.WINDOW_PARAM_MODE,UIConstants.MODE_EDIT);
			    data.put(FormModuleController.PARAM_WIDOW_CODE, list.getSelectedItem().getValue());

				showPopupWindow(uri, data, display);
			} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
					if (list.getSelectedItem() == null)
						throw new WrongValueException(list, Labels.getLabel("kermel.referentiel.nat.message.selection"));
					HashMap<String, String> display = new HashMap<String, String>(); 
				
					// permet de fixer les dimensions du popup
				
					display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.referentiel.caractere.message.suppression"));
					display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.referentiel.caractere.form.supprimer"));
					display.put(MessageBoxController.DSP_HEIGHT, "250px");
					display.put(MessageBoxController.DSP_WIDTH, "47%");

					HashMap<String, Object> map = new HashMap<String, Object>(); // permet
					// permet de passer des parametres au popup
					
					map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
					showMessageBox(display, map);
			}
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){		
				for (int i = 0; i < list.getSelectedCount(); i++) {
					module = (SygModule)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					//System.out.println(pop);
					BeanLocator.defaultLookup(ModuleSession.class).delete(module.getId());
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) {
				
				String uri = null;
				
				HashMap<String, String> display = null;
				HashMap<String, Object> data = null;

				uri = "/formateur/doc.zul" ;

				data = new HashMap<String, Object>();
				display = new HashMap<String, String>();

				String nomFichier = (String) list.getSelectedItem().getAttribute("cv");

				data.put(DownloaDocsController.NOM_FICHIER, nomFichier);

				display.put(DSP_TITLE, "Visualisation du fichier");
				display.put(DSP_WIDTH, "90%");
				display.put(DSP_HEIGHT, "600px");

				showPopupWindow(uri, data, display);
			
			}

		}
		
		

		/**
		 * Definit comment un element de la liste est affiche.
		 */
		
		
		public void onClick$bchercher()
		{
			onOK();
		}
		
		public void onOK()
		{
			lib = txtLib.getValue();
			code=txtCode.getValue();
			
			if(!lib.equalsIgnoreCase(Labels.getLabel("kermel.module.lib")))
			{ if(code.equalsIgnoreCase(Labels.getLabel("kermel.module.code")))
				code="";
			 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		    }

			if(!code.equalsIgnoreCase(Labels.getLabel("kermel.module.code")))
			{ if(lib.equalsIgnoreCase(Labels.getLabel("kermel.module.lib")))
				lib="";
			 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		    }
			
		  if(code.equalsIgnoreCase(Labels.getLabel("kermel.module.code"))&& lib.equalsIgnoreCase(Labels.getLabel("kermel.module.lib")))
				  {
			       code="";
			       lib="";
			      }
		  Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
			}
		
		public void onFocus$txtCode()
		{
			if(txtCode.getValue().equalsIgnoreCase(Labels.getLabel("kermel.module.code")))
				txtCode.setValue("");
			else
				code=txtCode.getValue();
		}
		
		
		public void onBlur$txtCode()
		{
			if(txtCode.getValue().equals(""))
				txtCode.setValue(Labels.getLabel("kermel.module.code"));
		}
		public void onFocus$txtLib()
		{
			if(txtLib.getValue().equalsIgnoreCase(Labels.getLabel("kermel.module.lib")))
				txtLib.setValue("");
			else
				lib=txtLib.getValue();
		}
		
		
		public void onBlur$txtLib()
		{
			if(txtLib.getValue().equals(""))
				txtLib.setValue(Labels.getLabel("kermel.module.lib"));
		}
		
		

		@Override
		public void render(final Listitem item, final Object data, int index) throws Exception {
			 SygModule module = (SygModule) data;
			item.setValue(module);
			
			Listcell cellLib,cellCd,cellCom,cellCibl,cellVolum,cellPre,cellObj,cellProg;
			
			
			if(module.getCode()!=null) {
				cellCd = new Listcell(module.getCode());
				cellCd.setParent(item);
			}
			else {
				cellCd = new Listcell("");
				cellCd.setParent(item);
			}
			if(module.getLibelle()!=null) {
				cellLib = new Listcell(module.getLibelle());
				cellLib.setParent(item);
			}
			else {
				cellLib = new Listcell("");
				cellLib.setParent(item);
			}
			if(module.getCible()!=null) {
				cellCibl = new Listcell(module.getCible());
				cellCibl.setParent(item);
			}
			else {
				cellCibl = new Listcell("");
				cellCibl.setParent(item);
			}
			
			if(module.getVolumhoraire()!=null) {
				cellVolum = new Listcell(module.getVolumhoraire().toString());
				cellVolum.setParent(item);
			}
			else {
				cellVolum = new Listcell("");
				cellVolum.setParent(item);
			}
			
			if(module.getObject()!=null) {
				cellObj= new Listcell(module.getObject());
				cellObj.setParent(item);
			}
			else {
				cellObj = new Listcell("");
				cellObj.setParent(item);
			}
			if(module.getPrerequi()!=null) {
				cellPre= new Listcell(module.getPrerequi());
				cellPre.setParent(item);
			}
			else {
				cellPre = new Listcell("");
				cellPre.setParent(item);
			}
		
			 
		if(module.getProg()!=null && !"".equals(module.getProg())) {
			cellProg = new Listcell("");
			
			cellProg.setImage("/images/PaperClip-16x16.png");
			cellProg.setAttribute(UIConstants.TODO, "VIEW");
			cellProg.addEventListener(Events.ON_CLICK, this);
			cellProg.setAttribute("cv", module.getProg());
			cellProg.setParent(item);
		}
		else {
			cellProg = new Listcell("");
			cellProg.setParent(item);
		}	
	}
}


