package sn.ssi.kermel.web.referentiel.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygNatureprix;
import sn.ssi.kermel.be.referentiel.ejb.NatureprixSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class FormNatPrixController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle,txtCode;
	Long code;
	private	SygNatureprix nat=new SygNatureprix();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	List<SygNatureprix> nature = new ArrayList<SygNatureprix>();
	
	@Override
	public void onEvent(Event event) throws Exception {


	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			nat = BeanLocator.defaultLookup(NatureprixSession.class).findById(code);
			txtCode.setValue(nat.getNatCode());
			txtLibelle.setValue(nat.getNatLibelle());
		}
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
		     nat.setNatCode(txtCode.getValue());
			 nat.setNatLibelle(txtLibelle.getValue());
		  	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(NatureprixSession.class).save(nat);
				
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(NatureprixSession.class).update(nat);
						
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		

			if(txtCode.getValue().equals(""))
		     {
               errorComponent = txtCode;
               errorMsg = Labels.getLabel("kermel.referentiel.nat.code")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			nature=BeanLocator.defaultLookup(NatureprixSession.class).find(txtCode.getValue());
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT))
			{
				if(!nat.getNatCode().equalsIgnoreCase(txtCode.getValue()))
				{
					  if(nature.size()>0)
					     {
			                errorComponent = txtCode;
							errorMsg =Labels.getLabel("kermel.referentiel.nat.code")+" "+ Labels.getLabel("kermel.referentiel.existe")+": "+txtCode.getValue();
							lbStatusBar.setStyle(ERROR_MSG_STYLE);
							lbStatusBar.setValue(errorMsg);
							throw new WrongValueException (errorComponent, errorMsg);
					    }
				}
			
			}
			else
			{
				  if(nature.size()>0)
				  {
					errorComponent = txtCode;
					errorMsg = Labels.getLabel("kermel.referentiel.nat.code")+" "+Labels.getLabel("kermel.referentiel.existe")+" :"+txtCode.getValue();
					lbStatusBar.setStyle(ERROR_MSG_STYLE);
					lbStatusBar.setValue(errorMsg);
					throw new WrongValueException (errorComponent, errorMsg);
				  }
			}
			if(txtLibelle.getValue().equals(""))
		     {
             errorComponent = txtLibelle;
             errorMsg = Labels.getLabel("kermel.referentiel.nat.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;
	
		}
		
	}
}