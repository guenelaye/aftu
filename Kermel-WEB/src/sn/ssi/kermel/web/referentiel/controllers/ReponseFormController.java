package sn.ssi.kermel.web.referentiel.controllers;


import java.io.File;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.PkQuestion;
import sn.ssi.kermel.be.entity.PkTabReponse;
import sn.ssi.kermel.be.session.QuestionSession;
import sn.ssi.kermel.be.session.TabReponseSession;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class ReponseFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle,txtCode,txtReponse;
	Long code;
	private	PkTabReponse  reponse=new PkTabReponse();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	PkTabReponse  tabquestion;
	
	
	private Datebox   dateSaisie;
    public Textbox txtVersionElectronique;
    private String nomFichier;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	
	
	@Override
	public void onEvent(Event event) throws Exception {


	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			
			
		}else{
			
		     tabquestion= ((PkQuestion) getHttpSession().getAttribute("reponse")).getRep();
			dateSaisie.setValue(tabquestion.getDateEntree());
			txtReponse.setValue(tabquestion.getReponse());
			
			dateSaisie.setDisabled(true);
			txtReponse.setDisabled(true);
			//txtVersionElectronique.setValue(prestataire.getFacture());
			
		}
	}

	public void onOK() {
		
		     reponse.setDateEntree(dateSaisie.getValue());
		     reponse.setReponse(txtReponse.getValue());
		     reponse.setNomfichier(txtVersionElectronique.getValue());
		
		  	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
		  		tabquestion.setDateEntree(dateSaisie.getValue());
		  		tabquestion.setReponse(txtReponse.getValue());
		  		
			BeanLocator.defaultLookup(TabReponseSession.class).update(tabquestion);
				
				
		
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				
				reponse=BeanLocator.defaultLookup(TabReponseSession.class).saveTabReponse(reponse);

				List<PkQuestion>list= (List<PkQuestion>) getHttpSession().getAttribute("list");
				
				
				for (int i = 0; i < list.size(); i++) {
					list.get(i).setRep(reponse);
					BeanLocator.defaultLookup(QuestionSession.class).update(list.get(i));
				}
						
			}
	
			//Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			loadApplicationState("reponse");
			//detach();
		}

	

	public void onClick$btnChoixFichier() {
		//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
			if (ToolKermel.isWindows())
				nomFichier = FileLoader.uploadPieceDossierAO(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichier = FileLoader.uploadPieceDossierAO(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectronique.setValue(nomFichier);
		}
	
	public org.zkoss.util.media.AMedia fetchFile(File file) {

		org.zkoss.util.media.AMedia mymedia = null;
		try {
			mymedia = new AMedia(file, null, null);
			return mymedia;
		} catch (Exception e) {

			e.printStackTrace();
			return null;
		}

	}
	

}
