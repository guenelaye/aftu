package sn.ssi.kermel.web.referentiel.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCritere;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.CritereSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

public class CritereListeController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstCarac;
	private Paging pg;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null;
    private Listheader lshLibelle;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_CRITERE, MOD_CRITERE, DEL_CRITERE;
    private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_GESTIONCRITERE);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent

		if (ADD_CRITERE != null) {
			ADD_CRITERE.addForward(ApplicationEvents.ON_CLICK, this,
					ApplicationEvents.ON_ADD);
		}
		addEventListener(ApplicationEvents.ON_ADD, this);

		if (MOD_CRITERE != null) {
			MOD_CRITERE.addForward(ApplicationEvents.ON_CLICK, this,
					ApplicationEvents.ON_EDIT);
		}
		if (DEL_CRITERE != null) {
			DEL_CRITERE.addForward(ApplicationEvents.ON_CLICK, this,
					ApplicationEvents.ON_DELETE);
		}
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		lshLibelle.setSortAscending(new FieldComparator("libelle", false));
		lshLibelle.setSortDescending(new FieldComparator("libelle", true));
		lstCarac.setItemRenderer(this);
		pg.setPageSize(byPage);
		pg.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
	}

	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pg.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pg.getActivePage() * byPage;
				pg.setPageSize(byPage);
			}
			 List<SygCritere> critere = BeanLocator.defaultLookup(CritereSession.class).find(pg.getActivePage()*byPage, byPage, null, libelle,autorite);
			 SimpleListModel listModel = new SimpleListModel(critere);
			 lstCarac.setModel(listModel);
			pg.setTotalSize(BeanLocator.defaultLookup( CritereSession.class).count(null,libelle,autorite));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/referentiel/critere/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(CritereFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstCarac.getSelectedItem() == null)
				
				throw new WrongValueException(lstCarac, Labels.getLabel("kermel.referentiel.critere.message.selection"));
			final String uri = "/referentiel/critere/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(CritereFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(CritereFormController.PARAM_WINDOW_CODE, lstCarac.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstCarac.getSelectedItem() == null)
				
					throw new WrongValueException(lstCarac, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(DSP_HEIGHT,"500px");
				display.put(DSP_WIDTH, "80%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, lstCarac.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstCarac.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstCarac.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(CritereSession.class).delete(codes);
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	

	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygCritere critere = (SygCritere) data;
		item.setValue(critere.getId());

			 
		 Listcell cellLibelle = new Listcell(critere.getLibelle());
		 cellLibelle.setParent(item);
		 
		
	}
	public void onClick$bchercher()
	{
		onOK();
	}
	
	public void onOK()
	{
		libelle = txtLibelle.getValue();
		
		if((libelle.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.critere.libelle")))||(libelle.equals("")))
		 {
			libelle=null;
			page=null;
		 }
		else
		{
			libelle=txtLibelle.getValue();
			page="0";
		}
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	
	public void onFocus$txtLibelle()
	{
		if(txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.critere.libelle")))
			txtLibelle.setValue("");
		else
			 libelle=txtLibelle.getValue();
	}
	
	
	public void onBlur$txtLibelle()
	{
		if(txtLibelle.getValue().equals(""))
			txtLibelle.setValue(Labels.getLabel("kermel.referentiel.critere.libelle"));
	}

}
