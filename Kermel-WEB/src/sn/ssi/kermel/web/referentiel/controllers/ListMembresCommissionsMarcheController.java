package sn.ssi.kermel.web.referentiel.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Div;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygArretesMembresCommissionsMarches;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygMembresCommissionsMarches;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.MembresCommissionsMarchesSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;



@SuppressWarnings("serial")
public class ListMembresCommissionsMarcheController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox list;
	private Paging pg;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private String sygmnom,sygmprenom,sygmgestion,extension,images,extensionmem,imagesmem;
	private Listheader nom, prenom, fonction,tel, email;
	private Textbox txtnom,txtprenom;
	Long code;
	private KermelSousMenu monSousMenu;
	private Menuitem ADD_COMMISSIONSMARCHES, MOD_COMMISSIONSMARCHES, SUPP_COMMISSIONSMARCHES;
	private Combobox cbannee;
	List<SygMembresCommissionsMarches> responsablemembre = new ArrayList<SygMembresCommissionsMarches>();
	private Label lblNom,lblPrenom,lblTelephone,lblEmail,lblFonction,lbltitre,lbldeuxpoints,lblValue,lblReference,lblReferences,lblDate,lblDates,
	lbltitres,lbldeuxpointss,imagess;
	private ArrayList<String> listValeursAnnees;
	private Image image,imagedeux;
	private Div step0,step1;
	private Iframe idIframe;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	private SygAutoriteContractante autorite = null;
	private Utilisateur infoscompte;
	private Button btnModifier;
	private SygArretesMembresCommissionsMarches arrete =null;

	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_COMMISSIONSMARCHES);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_COMMISSIONSMARCHES != null) { ADD_COMMISSIONSMARCHES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_COMMISSIONSMARCHES != null) { MOD_COMMISSIONSMARCHES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUPP_COMMISSIONSMARCHES != null) { SUPP_COMMISSIONSMARCHES.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
			
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
		nom.setSortAscending(new FieldComparator("nom", false));
		nom.setSortDescending(new FieldComparator("nom", true));
		
		prenom.setSortAscending(new FieldComparator("prenom", false));
		prenom.setSortDescending(new FieldComparator("prenom", true));
		
			 
	    tel.setSortAscending(new FieldComparator("tel", false));
	    tel.setSortDescending(new FieldComparator("tel", true));
	    
		email.setSortAscending(new FieldComparator("email", false));
		email.setSortDescending(new FieldComparator("email", true));
		
	    fonction.setSortAscending(new FieldComparator("fonction", false));
	    fonction.setSortDescending(new FieldComparator("fonction", true));
 
		list.setItemRenderer(this);

		pg.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pg.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

	
	}
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		sygmgestion = UtilVue.getInstance().anneecourant(new Date());
		
		listValeursAnnees = new ArrayList<String>();
		for (int i = 2007; i <= Integer.parseInt(sygmgestion)+1 ; i++) {
			listValeursAnnees.add(i+"");
		}
		cbannee.setModel(new SimpleListModel(listValeursAnnees));
		infosResponsable();
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	
	 public void infosResponsable() {
		 arrete= BeanLocator.defaultLookup(MembresCommissionsMarchesSession.class).findArrete(autorite, sygmgestion);
		 if(arrete!=null)
			{
			   lblReference.setValue(arrete.getReference());
			   lblDate.setValue(UtilVue.getInstance().formateLaDate(arrete.getDate()));
			   lblReferences.setValue(arrete.getReferencemembre());
			   lblDates.setValue(UtilVue.getInstance().formateLaDate(arrete.getDatemembre()));
			   if(arrete.getFichier()!=null)
				{
					extension=arrete.getFichier().substring(arrete.getFichier().length()-3,arrete.getFichier().length());
					 if(extension.equalsIgnoreCase("pdf"))
						 images="/images/icone_pdf.png";
					 else  
						 images="/images/word.jpg";
					 
					image.setVisible(true);
					image.setSrc(images);
					lbldeuxpoints.setValue(":");
					lbltitre.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.procesverbalouverture.versionfichier"));

				}
			   else
			   {
				   image.setVisible(false);
					lbldeuxpoints.setValue("");
					lbltitre.setValue("");
			   }
			 
			   if(arrete.getFichiermembre()!=null)
				{
					extension=arrete.getFichiermembre().substring(arrete.getFichiermembre().length()-3,arrete.getFichiermembre().length());
					 if(extension.equalsIgnoreCase("pdf"))
						 imagesmem="/images/icone_pdf.png";
					 else  
						 imagesmem="/images/word.jpg";
					 
					imagedeux.setVisible(true);
					imagedeux.setSrc(imagesmem);
					lbldeuxpoints.setValue(":");
					lbltitres.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.procesverbalouverture.versionfichier"));

				}
			   else
			   {
				   imagedeux.setVisible(false);
					lbldeuxpoints.setValue("");
					lbltitres.setValue("");
			   }
			  
			}
		 else
		 {
			   lblReference.setValue("");
			   lblDate.setValue("");
			   lblReferences.setValue("");
			   lblDates.setValue("");
			    image.setVisible(false);
				lbldeuxpoints.setValue("");
				lbltitre.setValue("");

			    imagedeux.setVisible(false);
				lbldeuxpoints.setValue("");
				lbltitres.setValue("");
		 }
				
		
		 
		 responsablemembre=BeanLocator.defaultLookup(MembresCommissionsMarchesSession.class).find(0,-1,null,null,autorite,sygmgestion,UIConstants.PARENT,-1);
			if(responsablemembre.size()>0)
			{
				lblNom.setValue(responsablemembre.get(0).getNom());
				lblPrenom.setValue(responsablemembre.get(0).getPrenom());
				lblTelephone.setValue(responsablemembre.get(0).getTel());
				lblEmail.setValue(responsablemembre.get(0).getEmail());
				lblFonction.setValue(responsablemembre.get(0).getFonction());
			
				
				btnModifier.setDisabled(false);
							
			}
			else
			{
				lblNom.setValue("");
				lblPrenom.setValue("");
				lblTelephone.setValue("");
				lblEmail.setValue("");
				lblFonction.setValue("");
				
				
				btnModifier.setDisabled(true);
			}
			lblValue.setValue(Labels.getLabel("kermel.common.form.membrescommissionsmarche")+" "+sygmgestion);
	 }
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 List<SygMembresCommissionsMarches> marche = BeanLocator.defaultLookup(MembresCommissionsMarchesSession.class).find(pg.getActivePage()*byPage,byPage,sygmnom,sygmprenom,autorite,sygmgestion,UIConstants.NPARENT,-1);
			 list.setModel(new SimpleListModel(marche));
			 pg.setTotalSize(BeanLocator.defaultLookup( MembresCommissionsMarchesSession.class).count(sygmnom,sygmprenom,autorite,sygmgestion,UIConstants.NPARENT,-1));
			 infosResponsable();
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			 if(arrete==null)
				 throw new WrongValueException(ADD_COMMISSIONSMARCHES, Labels.getLabel("kermel.common.form.membrescommissionsmarche.arretes")); 
			 responsablemembre=BeanLocator.defaultLookup(MembresCommissionsMarchesSession.class).find(0,-1,null,null,autorite,sygmgestion,-1,-1);
			 if (responsablemembre.size()==6)
					throw new WrongValueException(ADD_COMMISSIONSMARCHES, Labels.getLabel("kermel.common.form.membrescommissionsmarche.nombre"));
			final String uri = "/referentiel/commissionsmarches/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(MembresCommissionsMarcheFormController.PARAM_WINDOW_GESTION, sygmgestion);
			data.put(MembresCommissionsMarcheFormController.PARAM_WINDOW_MODE,UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null)
				throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
			final String uri = "/referentiel/commissionsmarches/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(MembresCommissionsMarcheFormController.PARAM_WINDOW_GESTION, sygmgestion);
			data.put(MembresCommissionsMarcheFormController.PARAM_WINDOW_MODE, UIConstants.MODE_EDIT);
			data.put(MembresCommissionsMarcheFormController.PARAM_WIDOW_CODE, list .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
	
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				// de
				// fixer
				// les
				// dimenisons
				// du
				// popup
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// de
				// passer
				// des
				// parametres
				// au
				// popup
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < list.getSelectedCount(); i++) {
					Long codes = (Long)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					System.out.println(codes);
				BeanLocator.defaultLookup(MembresCommissionsMarchesSession.class).delete(codes);
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) {
				Listcell button = (Listcell) event.getTarget();
				String nomfichier=(String) button.getAttribute("fichier");
				step0.setVisible(false);
				step1.setVisible(true);
				String filepath = cheminDossier +  nomfichier;
				File f = new File(filepath.replaceAll("\\\\", "/"));

				org.zkoss.util.media.AMedia mymedia = null;
				try {
					mymedia = new AMedia(f, null, null);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				if (mymedia != null)
					idIframe.setContent(mymedia);
				else
					idIframe.setSrc("");

				idIframe.setHeight("600px");
				idIframe.setWidth("100%");
			}

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygMembresCommissionsMarches membre = (SygMembresCommissionsMarches) data;
		item.setValue(membre.getId());

		Listcell cellNom = new Listcell(membre.getNom());
		cellNom.setParent(item);
		Listcell cellPrenom = new Listcell(membre.getPrenom());
		cellPrenom.setParent(item);
	
		Listcell cellTel = new Listcell(membre.getTel());
		cellTel.setParent(item);
		Listcell cellEmail = new Listcell(membre.getEmail());
		cellEmail.setParent(item);
		Listcell cellFonction = new Listcell(membre.getFonction());
		cellFonction.setParent(item);
		
		Listcell cellArrete = new Listcell(membre.getArrete());
		cellArrete.setParent(item);


		Listcell cellImage = new Listcell();
				
		if(membre.getFichierarrete()!=null)
		{
			extensionmem=membre.getFichierarrete().substring(membre.getFichierarrete().length()-3, membre.getFichierarrete().length());
			 if(extensionmem.equals("pdf"))
				 imagesmem="/images/icone_pdf.png";
			 else  
				 imagesmem="/images/word.jpg";
			 cellImage.setImage(imagesmem);
			 cellImage.setAttribute(UIConstants.TODO, "visualiser");
			 cellImage.setAttribute("fichier", membre.getFichierarrete());
			//cellImageSupprime.setTooltiptext("Supprimer r�alisation");
			 cellImage.addEventListener(Events.ON_CLICK, ListMembresCommissionsMarcheController.this);
			
		}
		cellImage.setParent(item);
	}
	
	public void onOK$txtnom() {
		onClick$btnRechercher();
	}
	public void onOK$txtprenom() {
		onClick$btnRechercher();
	}
	public void onOK$txtgestion() {
		onClick$btnRechercher();
	}
	
	public void onClick$btnRechercher() {
		if (txtnom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.nom")) || txtnom.getValue().equals("")) {
			sygmnom = null;
		} else {
			sygmnom = txtnom.getValue();
		}	
		
		if (txtprenom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.prenom")) || txtprenom.getValue().equals("")) {
			sygmprenom = null;
		} else {
			sygmprenom = txtprenom.getValue();
		}
		
		if((cbannee.getSelectedItem()==null)||(cbannee.getSelectedItem().getValue().toString().equals("")))
		{
			sygmgestion = UtilVue.getInstance().anneecourant(new Date());
		}
		else
		{
			sygmgestion = cbannee.getSelectedItem().getValue().toString();
		}
 
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	

		public void onFocus$txtnom() {
			if (txtnom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.nom"))) {
				txtnom.setValue("");

			}
		}
			
			public void onFocus$txtprenom() {
				if (txtprenom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.common.form.prenom"))) {
					txtprenom.setValue("");

				}
		}
			
			

			public void onBlur$txtnom() {
				if (txtnom.getValue().equalsIgnoreCase("")) {
					txtnom.setValue(Labels.getLabel("kermel.common.form.nom"));
				}
			}
				public void onBlur$txtprenom() {
					if (txtprenom.getValue().equalsIgnoreCase("")) {
						txtprenom.setValue(Labels.getLabel("kermel.common.form.prenom"));
					}	
			}
				
				public void onClick$image() {
					step0.setVisible(false);
					step1.setVisible(true);
					String filepath = cheminDossier +  responsablemembre.get(0).getFichierarrete();
					File f = new File(filepath.replaceAll("\\\\", "/"));

					org.zkoss.util.media.AMedia mymedia = null;
					try {
						mymedia = new AMedia(f, null, null);
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					if (mymedia != null)
						idIframe.setContent(mymedia);
					else
						idIframe.setSrc("");

					idIframe.setHeight("600px");
					idIframe.setWidth("100%");
				}
				
				public org.zkoss.util.media.AMedia fetchFile(File file) {

					org.zkoss.util.media.AMedia mymedia = null;
					try {
						mymedia = new AMedia(file, null, null);
						return mymedia;
					} catch (Exception e) {

						e.printStackTrace();
						return null;
					}

				}
				
				public void onClick$menuFermer() {
					step1.setVisible(false);
					step0.setVisible(true);
				}

				public void onClick$btnModifier() {
					
					final String uri = "/referentiel/commissionsmarches/form.zul";

					final HashMap<String, String> display = new HashMap<String, String>();
					display.put(DSP_HEIGHT,"500px");
					display.put(DSP_WIDTH, "80%");
					display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

					final HashMap<String, Object> data = new HashMap<String, Object>();
					data.put(ArretesMembresCommissionsMarcheFormController.WINDOW_PARAM_MODE,
							UIConstants.MODE_EDIT);
					data.put(MembresCommissionsMarcheFormController.PARAM_WIDOW_CODE, responsablemembre.get(0).getId());

					showPopupWindow(uri, data, display);
				}
				
              public void onClick$btnArrete() {
					
					final String uri = "/referentiel/commissionsmarches/formarrete.zul";

					final HashMap<String, String> display = new HashMap<String, String>();
					display.put(DSP_HEIGHT,"500px");
					display.put(DSP_WIDTH, "80%");
					display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.membrescommissionsmarche.referencesjuridiques"));

					final HashMap<String, Object> data = new HashMap<String, Object>();
					data.put(ArretesMembresCommissionsMarcheFormController.WINDOW_PARAM_MODE, sygmgestion);
			
					showPopupWindow(uri, data, display);
				}
              
             
}