package sn.ssi.kermel.web.referentiel.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygCoordonateur;
import sn.ssi.kermel.be.referentiel.ejb.CoordonnateurSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;


@SuppressWarnings("serial")
public class ListCoordonnateurController extends AbstractWindow implements EventListener, AfterCompose, ListitemRenderer {

	private Listbox list;
	private Paging pg;
	public static final String PARAM_WINDOW_CODE = "CODE";

	private Listheader lshPrenom;
	private String nom,prenom;
	private Textbox txtPrenom,txtNom;
	private SygCoordonateur coordonnateur;
	private int byPageBandBox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	private int activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	public static final String WINDOW_PARAM_MODE = "MODE";
	private String page=null;
	Session session = getHttpSession();
	
	private KermelSousMenu monSousMenu;
	private Menuitem ADD_COORDINNATEUR, MOD_COORDINNATEUR, DEL_COORDINNATEUR;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_COORDONNATEUR);
		monSousMenu.afterCompose();
		Components.wireFellows(this, this);
		if (ADD_COORDINNATEUR != null) {
			ADD_COORDINNATEUR.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
		}
		if (MOD_COORDINNATEUR != null) {
			MOD_COORDINNATEUR.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
		}
		if (DEL_COORDINNATEUR != null) {
			DEL_COORDINNATEUR.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
		}
	
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	
		Events.postEvent(ApplicationEvents.ON_CLICK, this, null);
		
		lshPrenom.setSortAscending(new FieldComparator("prenom", false));
		lshPrenom.setSortDescending(new FieldComparator("prenom", true));
		list.setItemRenderer(this);
		pg.setPageSize(byPage);
		pg.addForward("onPaging", this,ApplicationEvents.ON_MODEL_CHANGE);
	
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	/**
	 * Permet de gerer les evenements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet evenement survient, les evenements de la liste sont mis �
		 * jour de meme que l'element de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
 			 List<SygCoordonateur> coord = BeanLocator.defaultLookup(CoordonnateurSession.class).findAllby(pg.getActivePage()*byPage,byPage, nom, prenom);

 			 SimpleListModel listModel = new SimpleListModel(coord);
			list.setModel(listModel);
			pg.setTotalSize(BeanLocator.defaultLookup(CoordonnateurSession.class).count(nom, prenom));

		} 
	
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/coordonnateurformation/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.formation.coordonateur.nouv"));
			display.put(DSP_HEIGHT,"80%");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormCoordonnateurController.WINDOW_PARAM_MODE,UIConstants.MODE_NEW);
			
			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				throw new WrongValueException(list, Labels.getLabel("kermel.referentiel.nat.message.selection"));
				
			}
			final String uri ="/coordonnateurformation/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"80%");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.formation.coordonateur.modif"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FormCoordonnateurController.WINDOW_PARAM_MODE,UIConstants.MODE_EDIT);
		    data.put(FormCoordonnateurController.PARAM_WIDOW_CODE, list.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
					throw new WrongValueException(list, Labels.getLabel("kermel.referentiel.nat.message.selection"));
				HashMap<String, String> display = new HashMap<String, String>(); 
			
				// permet de fixer les dimensions du popup
			
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.referentiel.caractere.message.suppression"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.referentiel.caractere.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// permet de passer des parametres au popup
				
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){		
			for (int i = 0; i < list.getSelectedCount(); i++) {
				coordonnateur = (SygCoordonateur)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
				//System.out.println(pop);
				BeanLocator.defaultLookup(CoordonnateurSession.class).delete(coordonnateur.getId());
			}
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
			
	}
	
	
	public void onClick$bchercher()
	{
		onOK();
	}
	
	public void onOK()
	{
		nom = txtNom.getValue();
		prenom=txtPrenom.getValue();
	
		
		if(!prenom.equalsIgnoreCase(Labels.getLabel("kermel.formateur.pnom")))
		{ if(nom.equalsIgnoreCase(Labels.getLabel("kermel.formateur.nom")))
			nom="";
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	    }

		if(!nom.equalsIgnoreCase(Labels.getLabel("kermel.formateur.nom")))
		{ if(prenom.equalsIgnoreCase(Labels.getLabel("kermel.formateur.pnom")))
			prenom="";
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	    }
		
	  if(nom.equalsIgnoreCase(Labels.getLabel("kermel.formateur.nom"))&& prenom.equalsIgnoreCase(Labels.getLabel("kermel.formateur.pnom")))
			  {
		       nom="";
		       prenom="";
		      }
	  Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
	
	public void onFocus$txtPrenom()
	{
		if(txtPrenom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.formateur.pnom")))
			txtPrenom.setValue("");
		else
			prenom=txtPrenom.getValue();
	}
	
	
	public void onBlur$txtPrenom()
	{
		if(txtPrenom.getValue().equals(""))
			txtPrenom.setValue(Labels.getLabel("kermel.formateur.pnom"));
	}
	
	public void onFocus$txtNom()
	{
		if(txtNom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.formateur.nom")))
			txtNom.setValue("");
		else
			nom=txtNom.getValue();
	}
	
	
	public void onBlur$txtNom()
	{
		if(txtNom.getValue().equals(""))
			txtNom.setValue(Labels.getLabel("kermel.formateur.nom"));
	}
	
	

	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		 SygCoordonateur coord = (SygCoordonateur) data;
		 item.setValue(coord);
		
		Listcell cellNin,cellNom,cellPnom, cellAdd,cellTel,cellMail,cellCom;
		
		
		if(coord.getNin()!=null) {
			cellNin = new Listcell(coord.getNin().toString());
			cellNin.setParent(item);
		}
		else {
			cellNin = new Listcell("");
			cellNin.setParent(item);
		}
		if(coord.getPrenom()!=null) {
			cellPnom = new Listcell(coord.getPrenom());
			cellPnom.setParent(item);
		}
		else {
			cellPnom = new Listcell("");
			cellPnom.setParent(item);
		}
		
		if(coord.getNom()!=null) {
			cellNom = new Listcell(coord.getNom());
			cellNom.setParent(item);
		}
		else {
			cellNom = new Listcell("");
			cellNom.setParent(item);
		}
		
		if(coord.getAddress()!=null) {
			cellAdd = new Listcell(coord.getAddress());
			cellAdd.setParent(item);
		}
		else {
			cellAdd = new Listcell("");
			cellAdd.setParent(item);
		}
		
		if(coord.getEmail()!=null) {
			cellMail = new Listcell(coord.getEmail());
			cellMail.setParent(item);
		}
		else {
			cellMail = new Listcell("");
			cellMail.setParent(item);
		}
		
		if(coord.getTel()!=null) {
			cellTel = new Listcell(coord.getTel().toString());
			cellTel.setParent(item);
		}
		else {
			cellTel = new Listcell("");
			cellTel.setParent(item);
		}
		
		if(coord.getCommentaire()!=null) {
			cellCom = new Listcell(coord.getCommentaire());
			cellCom.setParent(item);
		}
		else {
			cellCom = new Listcell("");
			cellCom.setParent(item);
		}
	}  
}
