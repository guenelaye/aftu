package sn.ssi.kermel.web.referentiel.controllers;


import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCatFournisseur;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygPays;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.CatFourSession;
import sn.ssi.kermel.be.referentiel.ejb.FournisseurSession;
import sn.ssi.kermel.be.referentiel.ejb.PaysSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class FournisseurFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,fichier;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_FICHIER = "FICHIER";
	private Textbox txtNom, txtAdresse, txtEmail, txtTel, txtFax;
	Long code;
	private	SygFournisseur fournisseur=new SygFournisseur();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	private Bandbox bdCategorie;
	private Paging pgCategorie;
	private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE,activePage;
	private String libellecategorie = null, page=null;
	private Listbox lstCategorie;
	private Textbox txtRechercherCategorie;
	SygCatFournisseur categorie=null;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	private Listbox lstPays;
	private Paging pgPays;
	private Bandbox bdPays;
	private String libellepays=null;
	SygPays pays=new SygPays();
    public Textbox txtRechercherPays;

	@Override
	public void onEvent(Event event) throws Exception {
		if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgCategorie.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgCategorie.getActivePage() * byPageBandbox;
				pgCategorie.setPageSize(byPageBandbox);
			}
			List<SygCatFournisseur> categorie = BeanLocator.defaultLookup(CatFourSession.class).find(activePage, byPageBandbox,libellecategorie);
			lstCategorie.setModel(new SimpleListModel(categorie));
			pgCategorie.setTotalSize(BeanLocator.defaultLookup(CatFourSession.class).count(libellecategorie));
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_PAYS)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgPays.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgPays.getActivePage() * byPageBandbox;
				pgPays.setPageSize(byPageBandbox);
			}
			List<SygPays> pays = BeanLocator.defaultLookup(PaysSession.class).find(activePage, byPageBandbox,libellepays,null);
			lstPays.setModel(new SimpleListModel(pays));
			pgPays.setTotalSize(BeanLocator.defaultLookup(PaysSession.class).count(libellepays,null));
		}
	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		lstCategorie.setItemRenderer(new CategoriesRenderer());
		pgCategorie.setPageSize(byPageBandbox);
		pgCategorie.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_MODEL_CHANGE);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
		addEventListener(ApplicationEvents.ON_PAYS, this);
		pgPays.setPageSize(byPageBandbox);
		pgPays.addForward("onPaging", this, ApplicationEvents.ON_PAYS);
		lstPays.setItemRenderer(new PaysRenderer());
		Events.postEvent(ApplicationEvents.ON_PAYS, this, null);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		mode = (String) map.get(PARAM_WINDOW_MODE);
		fichier=(String) map.get(WINDOW_PARAM_FICHIER);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			fournisseur = BeanLocator.defaultLookup(FournisseurSession.class).findById(code);
			txtNom.setValue(fournisseur.getNom());
			txtTel.setValue(fournisseur.getTel());
			txtFax.setValue(fournisseur.getFax());
			txtEmail.setValue(fournisseur.getEmail());
			txtAdresse.setValue(fournisseur.getAdresse());
			categorie=fournisseur.getCategorie();
			bdCategorie.setValue(categorie.getLibelle());
			pays=fournisseur.getPays();
			bdPays.setValue(pays.getLibelle());
		}
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			fournisseur.setNom(txtNom.getValue());
			fournisseur.setAdresse(txtAdresse.getValue());
			fournisseur.setTel(txtTel.getValue());
			fournisseur.setFax(txtFax.getValue());
			fournisseur.setEmail(txtEmail.getValue());
			fournisseur.setCategorie(categorie);
			//fournisseur.setAutorite(autorite);
			fournisseur.setPays(pays);
			if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) 
				fournisseur=	BeanLocator.defaultLookup(FournisseurSession.class).save(fournisseur);
	
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) 
				BeanLocator.defaultLookup(FournisseurSession.class).update(fournisseur);
	
			if(fichier.equals("FOURNISSEUR"))
			  Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			else
			{
				Executions.getCurrent().setAttribute("fournisseur", fournisseur);
				 Events.postEvent(ApplicationEvents.ON_FOURNISSEUR, getOpener(), -1000);
			}
			  
				
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {		

			if(txtNom.getValue().equals(""))
		     {
				errorComponent = txtNom;
              	errorMsg = Labels.getLabel("kermel.common.form.nom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(bdPays.getValue().equals(""))
		     {
            errorComponent = bdPays;
            errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.lectureoffres.pays")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtAdresse.getValue().equals(""))
		     {
				errorComponent = txtAdresse;
             	errorMsg = Labels.getLabel("kermel.importateur.adresse")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }

			if(categorie==null)
		     {
				errorComponent = bdCategorie;
             	errorMsg = Labels.getLabel("kermel.referentiel.common.code.categorie")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }

			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	

		///////////Categorie///////// 
		public void onSelect$lstCategorie(){
			categorie= (SygCatFournisseur) lstCategorie.getSelectedItem().getValue();
			bdCategorie.setValue(categorie.getLibelle());
			bdCategorie.close();
		}
		
		public class CategoriesRenderer implements ListitemRenderer{
			@Override
			public void render(Listitem item, Object data, int index)  throws Exception {
				SygCatFournisseur categorie = (SygCatFournisseur) data;
				item.setValue(categorie);
				
				Listcell cellLibelle = new Listcell("");
				if (categorie.getLibelle()!=null){
					cellLibelle.setLabel(categorie.getLibelle());
				}
				cellLibelle.setParent(item);
		
			}
		}
		
		public void onFocus$txtRechercherCategorie(){
		if(txtRechercherCategorie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.code.categorie"))){
			txtRechercherCategorie.setValue("");
		}		 
		}
		
		public void  onClick$btnRechercherCategorie(){
		if(txtRechercherCategorie.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.code.categorie")) || txtRechercherCategorie.getValue().equals("")){
			libellecategorie = null;
			page=null;
		}
		else{
			libellecategorie = txtRechercherCategorie.getValue();
			page="0";
		}
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
		}
		
///////////Pays///////// 
	public void onSelect$lstPays(){
		pays= (SygPays) lstPays.getSelectedItem().getValue();
	    bdPays.setValue(pays.getLibelle());
	    bdPays.close();
	
	}
	
	public class PaysRenderer implements ListitemRenderer{
	
	
	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygPays pays = (SygPays) data;
		item.setValue(pays);
		
		Listcell cellLibelle = new Listcell(pays.getLibelle());
		cellLibelle.setParent(item);
		
		
		}
	}
	public void onFocus$txtRechercherPays(){
	if(txtRechercherPays.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.lectureoffres.pays"))){
		txtRechercherPays.setValue("");
	}		 
	}
	
	public void  onOK$btnRechercherPays(){
		onClick$btnRechercherPays();
	}
	public void  onOK$txtRechercherPays(){
		onClick$btnRechercherPays();
	}
	public void  onClick$btnRechercherPays(){
	if(txtRechercherPays.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.lectureoffres.pays")) || txtRechercherPays.getValue().equals("")){
	libellepays = null;
	page=null;
	}else{
		libellepays = txtRechercherPays.getValue();
	page="0";
	}
	Events.postEvent(ApplicationEvents.ON_PAYS, this, page);
	}
	
}
