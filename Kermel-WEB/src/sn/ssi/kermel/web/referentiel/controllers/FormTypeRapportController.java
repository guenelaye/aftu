package sn.ssi.kermel.web.referentiel.controllers;

import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygTypeRapportFormation;
import sn.ssi.kermel.be.referentiel.ejb.TypeRapportFormationSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

public class FormTypeRapportController extends AbstractWindow implements
EventListener, AfterCompose{

    public static final String PARAM_WINDOW_CODE = "CODE";

	private Textbox txtLib,txtDesc,txtCod;
	private SygTypeRapportFormation type =new SygTypeRapportFormation();
	
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String WINDOW_PARAM_MODE = "MODE";

    private String mode;
    Long code;
    Integer id;
    

   @Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
   		}
   	
   
	/**
	 * Permet de gerer les evenements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		
	}
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(WINDOW_PARAM_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
         type = (SygTypeRapportFormation) map.get(PARAM_WINDOW_CODE);

		
			txtCod.setValue(type.getCode());
			txtLib.setValue(type.getLibelle());
			txtDesc.setValue(type.getDescription());
			
			} 
		}
	
	
	public void onClick$menuValider() {

		
		if(txtCod.getValue()==null || "".equals(txtCod.getValue())){
			throw new WrongValueException(txtCod, Labels.getLabel("kermel.ChamNull"));
		}
		
		if(txtLib.getValue()==null || "".equals(txtLib.getValue())){
			throw new WrongValueException(txtLib, Labels.getLabel("kermel.ChamNull"));
		}
		
		type.setCode(txtCod.getValue());
		type.setLibelle(txtLib.getValue());
		type.setDescription(txtDesc.getValue());
	
		
		 if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)){
		BeanLocator.defaultLookup(TypeRapportFormationSession.class).save(type);
		    }
		    
		 else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)){
			BeanLocator.defaultLookup(TypeRapportFormationSession.class).update(type);

		}
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		 detach();
	}	

 public void  onClick$menuFermer(){
	
		 detach();
}
}