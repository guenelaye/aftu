package sn.ssi.kermel.web.referentiel.controllers;
import java.util.Calendar;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Button;
import org.zkoss.zul.Longbox;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygModule;
import sn.ssi.kermel.be.referentiel.ejb.ModuleSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

public class FormModuleController extends AbstractWindow implements
EventListener, AfterCompose{

    public static final String PARAM_WINDOW_CODE = "CODE";

	private Textbox txtLib,txtCom,txtCod,txtVersionElectronique,txtCibl,txtObj,txtPre;
	private Longbox lgVol;
	private SygModule module =new SygModule();
	
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String PARAM_WIDOW_CODE = "CODE";

    private String mode,nomPiece;
	private Button btnChoixFichier;
	private final String cheminDossier = UIConstants.PATH_PJ;
    Long code;
    Integer id;
    

   @Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
   		}
   	
   
	/**
	 * Permet de gerer les evenements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {
		
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) {
			Button button = (Button) event.getTarget();
			String toDo = (String) button.getAttribute(UIConstants.TODO);

			if (toDo.equalsIgnoreCase("JOIN")) {
				
				if (ToolKermel.isWindows())
					nomPiece = FileLoader.uploadPieceDossier(UtilVue.getInstance().formateLaDate(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
				else
					nomPiece = FileLoader.uploadPieceDossier(UtilVue.getInstance().formateLaDate(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

				if (nomPiece != null) {
					btnChoixFichier.setAttribute(UIConstants.TODO, "DELETE");// pour

					
				} else
					
					 throw new WrongValueException(btnChoixFichier, Labels.getLabel("kermel.fichiernull"));
			} else if (toDo.equalsIgnoreCase("DELETE")) {
				
				if (ToolKermel.isWindows())
					nomPiece = FileLoader.uploadPieceDossier(UtilVue.getInstance().formateLaDate(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
				else
					nomPiece = FileLoader.uploadPieceDossier(UtilVue.getInstance().formateLaDate(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

				if (nomPiece != null) {
					btnChoixFichier.setAttribute(UIConstants.TODO, "JOIN");// pour		
				}
			}

		}
		txtVersionElectronique.setValue(nomPiece);
	
}

		
	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(WINDOW_PARAM_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			module = (SygModule) map.get(PARAM_WINDOW_CODE);

		
			txtCod.setValue(module.getCode());
			txtLib.setValue(module.getLibelle());
			txtCibl.setValue(module.getCible());
			txtObj.setValue(module.getObject());
			lgVol.setValue(module.getVolumhoraire());
			txtPre.setValue(module.getPrerequi());
			txtCom.setValue(module.getCommentaire());
			txtVersionElectronique.setValue(module.getProg());
           } else if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)){
			
			btnChoixFichier.setAttribute(UIConstants.TODO, "JOIN");// Pour tenir
		}
		
		btnChoixFichier.addEventListener(Events.ON_CLICK, this);
		 	
	}
	
	
	
	
	
	public void onClick$menuValider() {

		
		if(txtCod.getValue()==null || "".equals(txtCod.getValue())){
			throw new WrongValueException(txtCod, Labels.getLabel("kermel.ChamNull"));
		}
		
		if(txtLib.getValue()==null || "".equals(txtLib.getValue())){
			throw new WrongValueException(txtLib, Labels.getLabel("kermel.ChamNull"));
		}
		if(lgVol.getValue()==null || "".equals(lgVol.getValue())){
			throw new WrongValueException(lgVol, Labels.getLabel("kermel.ChamNull"));
		}
		if(txtCibl.getValue()==null || "".equals(txtCibl.getValue())){
			throw new WrongValueException(txtCibl, Labels.getLabel("kermel.ChamNull"));
		}
		if(txtObj.getValue()==null || "".equals(txtObj.getValue())){
			throw new WrongValueException(txtObj, Labels.getLabel("kermel.ChamNull"));
		}
		if(txtVersionElectronique.getValue()==null || "".equals(txtVersionElectronique.getValue())){
			throw new WrongValueException(txtVersionElectronique, Labels.getLabel("kermel.ChamNull"));
		}
		
		module.setCode(txtCod.getValue());
		module.setLibelle(txtLib.getValue());
		module.setCommentaire(txtCom.getValue());
	   module.setCible(txtCibl.getValue());
	   module.setObject(txtObj.getValue());
	   module.setPrerequi(txtPre.getValue());
	   module.setVolumhoraire(lgVol.getValue());
	   module.setProg(txtVersionElectronique.getValue());
		
		 if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)){
		BeanLocator.defaultLookup(ModuleSession.class).save(module);
		    }
		    
		 else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)){
			BeanLocator.defaultLookup(ModuleSession.class).update(module);

		}
		 loadApplicationState("list_module");
		 detach();
	}	

	public void  onClick$menuFermer(){
		 loadApplicationState("list_module");
		 detach();
		
	}
}
