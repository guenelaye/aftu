package sn.ssi.kermel.web.denonciations.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.denonciation.ejb.CourrierDenonciationsSession;
import sn.ssi.kermel.be.denonciation.ejb.DenonciationSession;
import sn.ssi.kermel.be.entity.SygCourrierDenonciations;
import sn.ssi.kermel.be.entity.SygDenonciation;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;


@SuppressWarnings("serial")
public class ListDenonciationCourrierSuivitController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null;
    private Listheader lshLibelle;
    Session session = getHttpSession();
     private String login;
    private Div step0, step1,step2;

    //
    Long numdenonimation = null;
	private SygDenonciation denonciation;
    
	
	private SygCourrierDenonciations courrier = new SygCourrierDenonciations();
	private Textbox txtLibelles, txtVersionElectronique;
	private final String cheminDossier = UIConstants.PATH_PJCONT;
	private String nomFichier;
	UtilVue utilVue = UtilVue.getInstance();
	private Datebox dtdate;
	private Component errorComponent;
	private String errorMsg;
	 private Label lbStatusBar;
	 private static final String ERROR_MSG_STYLE = "color:red";
	 
	
		private Long idpiecejointe=null;
		
		private String extension,images;
		private Iframe idIframe;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		lshLibelle.setSortAscending(new FieldComparator("libelle", false));
		lshLibelle.setSortDescending(new FieldComparator("libelle", true));
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    
    	login = ((String) getHttpSession().getAttribute("user"));
    	
    	///
    	
    	
	}
	
	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		numdenonimation = (Long) session.getAttribute("numdenonimation");
		denonciation = BeanLocator.defaultLookup(DenonciationSession.class) .findById(numdenonimation);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygCourrierDenonciations> courriers = BeanLocator.defaultLookup(CourrierDenonciationsSession.class).findRech(activePage,byPage,numdenonimation,libelle,null);
			 SimpleListModel listModel = new SimpleListModel(courriers);
			 lstListe.setModel(listModel);
			 pgPagination.setTotalSize(BeanLocator.defaultLookup( CourrierDenonciationsSession.class).countRech(numdenonimation,libelle,null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {

			step0.setVisible(false);
			step1.setVisible(true);
			
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			courrier = (SygCourrierDenonciations) lstListe.getSelectedItem().getValue();
			idpiecejointe=courrier.getId();
			dtdate.setValue(courrier.getDate());
			txtLibelles.setValue(courrier.getLibelle());
			//txtVersionElectronique.setValue(courrier.getFichier());
			
			step0.setVisible(false);
			step1.setVisible(true);
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
				
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				BeanLocator.defaultLookup(CourrierDenonciationsSession.class).delete(codes);
				BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_PJCONT", Labels.getLabel("kermel.referentiel.common.decision.suppression")+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) {
				Listcell button = (Listcell) event.getTarget();
				String nomfichier=(String) button.getAttribute("fichier");
				step0.setVisible(false);
				step1.setVisible(false);
				step2.setVisible(true);
				String filepath = cheminDossier +  nomfichier;
				File f = new File(filepath.replaceAll("\\\\", "/"));

				org.zkoss.util.media.AMedia mymedia = null;
				try {
					mymedia = new AMedia(f, null, null);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				if (mymedia != null)
					idIframe.setContent(mymedia);
				else
					idIframe.setSrc("");

				idIframe.setHeight("600px");
				idIframe.setWidth("100%");
			}
		
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygCourrierDenonciations courrier = (SygCourrierDenonciations) data;
		item.setValue(courrier);

			 
		 Listcell cellLibelle = new Listcell(courrier.getLibelle());
		 cellLibelle.setParent(item);
		 
		 Listcell cellDate = new Listcell(UtilVue.getInstance().formateLaDate(courrier.getDate()));
		 cellDate.setParent(item);
		 
		 extension=courrier.getFichier().substring(courrier.getFichier().length()-3,  courrier.getFichier().length());
		 if(extension.equalsIgnoreCase("pdf"))
			 images="/images/icone_pdf.png";
		 else  
			 images="/images/word.jpg";
			
		 Listcell image = new Listcell("");
		 image.setImage(images);
		 image.setAttribute("fichier", courrier.getFichier());
		 image.setTooltiptext("Visualiser fichier");
		 image.addEventListener(Events.ON_CLICK, ListDenonciationCourrierSuivitController.this);
		 image.setParent(item);

	}
	public void onClick$bchercher()
	{
		
		if((txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.pj.Libelle")))||(txtLibelle.getValue().equals("")))
		 {
			libelle=null;
			
		 }
		else
		{
			libelle=txtLibelle.getValue();
			page="0";
		}
		
		
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	
	public void onFocus$txtLibelle()
	{
		if(txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.pj.Libelle")))
			txtLibelle.setValue("");
		
	}
	
	public void onBlur$txtLibelle()
	{
		if(txtLibelle.getValue().equals(""))
			txtLibelle.setValue(Labels.getLabel("kermel.contentieux.pj.Libelle"));
	}

	public void onOK$txtLibelle()
	{
		onClick$bchercher();
	}
	
	
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
	
	
	///
	


	public void onClick$btnChoixFichier() {

		if (ToolKermel.isWindows())
			nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
		else
			nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

		txtVersionElectronique.setValue(nomFichier);
	}
	
	
private boolean checkFieldConstraints() {
		
		try {
		
			if(dtdate.getValue()==null)
		     {
               errorComponent = dtdate;
               errorMsg = Labels.getLabel("kermel.contentieux.pj.Date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		
			if(txtVersionElectronique.getValue().equals(""))
		     {
          errorComponent = txtVersionElectronique;
          errorMsg = Labels.getLabel("kermel.contentieux.pj.Fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(txtLibelles.getValue().equals(""))
		     {
         errorComponent = txtLibelles;
         errorMsg = Labels.getLabel("kermel.contentieux.pj.Libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

	public void onOK() {
		
		if(checkFieldConstraints())
		{
			courrier.setLibelle(txtLibelles.getValue());
			courrier.setFichier(txtVersionElectronique.getValue());
			courrier.setDate(dtdate.getValue());

			courrier.setDenonciation(denonciation);

			if (idpiecejointe==null) 
				BeanLocator.defaultLookup(CourrierDenonciationsSession.class).save(courrier);
            else 
            BeanLocator.defaultLookup(CourrierDenonciationsSession.class).update(courrier);
			
		
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			step0.setVisible(true);
			step1.setVisible(false);
			effacer();
		} 
	}

	public void effacer(){
		idpiecejointe=null;
		txtLibelles.setValue("");
		txtVersionElectronique.setValue("");
		dtdate.setValue(null);
	}
	
	public void onClick$menuFermer() {
		
		step0.setVisible(true);
		step1.setVisible(false);
	}
	
public void onClick$menuFermer2() {
		
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
	}

}

