package sn.ssi.kermel.web.denonciations.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tabpanel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.denonciation.ejb.DenonciationSession;
import sn.ssi.kermel.be.entity.SygDenonciation;
import sn.ssi.kermel.web.common.components.DossierMenu;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



public class DossiersNumVertFormController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private Long code;
	UtilVue utilVue = UtilVue.getInstance();
	private Label lbldate,lblResume;
	Session session = getHttpSession();
	private DossierMenu monDossierMenu;
	private Tabpanel tabCourriersDenonciationSuivit;
	private Include incCourriersDenonciationSuivit;
	private SygDenonciation denonciation =new SygDenonciation();

	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		tabCourriersDenonciationSuivit.addEventListener(Events.ON_CLICK, this);
		code = (Long) session.getAttribute("numdenonimation");
		System.out.println(code.toString()+"hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh");
		denonciation = BeanLocator.defaultLookup(DenonciationSession.class) .findById(code);
		lblResume.setValue(denonciation.getObjet());
		
		lbldate.setValue(UtilVue.getInstance().formateLaDate(denonciation.getDateappel()));
	   monDossierMenu.setProfil((String) getHttpSession().getAttribute("profil"));
//		monDossierMenu.setDosCode(audit.getIdaudit());
//        monDossierMenu.setState(denonimation.getState().getCode());
        monDossierMenu.afterCompose();

	}

	
	public void onCreate(CreateEvent createEvent) {
		
	
		
		 Include inc = (Include) this.getFellowIfAny("incDecision");
         inc.setSrc("/denonciation/decision.zul");
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	
}