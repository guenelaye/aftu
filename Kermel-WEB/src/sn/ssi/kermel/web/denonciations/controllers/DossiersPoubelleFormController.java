package sn.ssi.kermel.web.denonciations.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabpanel;

import sn.ssi.kermel.be.entity.SygDenonciation;
import sn.ssi.kermel.web.common.components.DossierMenu;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



public class DossiersPoubelleFormController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private Long code;
	UtilVue utilVue = UtilVue.getInstance();
	private Label lblobjet,lbldenonciation,lbldatedenonciation,lblfichier;
	Session session = getHttpSession();
	private DossierMenu monDossierMenu;
	private Tabpanel tabCourriersDenonciationp;
	private Include incCourriersDenonciation;
	private SygDenonciation denonciation =new SygDenonciation();
	private Tab TAB_Portail;
	private String LibelleTab;
	private Tab TAB_Courriersp,TAB_Portailp,TAB_NumVertp;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		


	}

	
	public void onCreate(CreateEvent createEvent) {
		
		LibelleTab = (String) session.getAttribute("LibelleTab");
		
		if(LibelleTab!=null)
		{
			if(LibelleTab.equals("POUCOUR"))
			  {
				TAB_Courriersp.setSelected(true);
				Include inc = (Include) this.getFellowIfAny("incCourriersp");
				inc.setSrc("/denonciation/listdenonciationpoubellecourrier.zul");		
			  }
			else if(LibelleTab.equals("POUPORT"))
			  {
				TAB_Portailp.setSelected(true);
				Include inc = (Include) this.getFellowIfAny("incPortailp");
				inc.setSrc("/denonciation/listdenonciationpoubelle.zul");		
			  }
			else if(LibelleTab.equals("POUNUMVERT"))
			  {
				TAB_NumVertp.setSelected(true);
				Include inc = (Include) this.getFellowIfAny("incNumVertp");
				inc.setSrc("/denonciation/listdenonciationpoubellenumvert.zul");		
			  }
			else
			{
				 Include inc = (Include) this.getFellowIfAny("incCourriersp");
			      inc.setSrc("/denonciation/listdenonciationpoubellecourrier.zul");
			}
		}
		else
		{
			 Include inc = (Include) this.getFellowIfAny("incCourriersp");
		      inc.setSrc("/denonciation/listdenonciationpoubellecourrier.zul");
		}
		
		
	
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	
}