package sn.ssi.kermel.web.denonciations.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.denonciation.ejb.DenonciationSession;
import sn.ssi.kermel.be.entity.SygDenonciation;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;



@SuppressWarnings("serial")
public class ListSuiviDenonciationCourrierController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox list;
	private Paging pgdenonciation;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private Date sygmdatedenonciation;
	private Datebox datedenonciation;
	private Listheader lshfichier,lshtraiter,lshpublier,lshpoubelle,lshdenonciation,lshobjet,lshdatedenonciation;
	private Textbox txtobjet,txtdenonciation,txtfichier;
	private Intbox txttraiter,txtpublier,txtpoubelle;
	Long code;
	private Menuitem ADD_SUIVIDENONCIATION, MOD_SUIVIDENONCIATION, SUPP_SUIVIDENONCIATION;
	 private KermelSousMenu monSousMenu;

	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.SUIVIDENONCIATION );
		monSousMenu.afterCompose();
		Components.wireFellows(this, this); // mais le wireFellows precedent
		Components.wireFellows(this, this);
		if (ADD_SUIVIDENONCIATION != null)
			ADD_SUIVIDENONCIATION.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
		if (MOD_SUIVIDENONCIATION != null)
			MOD_SUIVIDENONCIATION.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
		if (SUPP_SUIVIDENONCIATION != null)
			SUPP_SUIVIDENONCIATION.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
		lshdatedenonciation.setSortAscending(new FieldComparator("datedenonciation", false));
		lshdatedenonciation.setSortDescending(new FieldComparator("datedenonciation", true));
		
		list.setItemRenderer(this);

		pgdenonciation.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pgdenonciation.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 List<SygDenonciation> denonciation = BeanLocator.defaultLookup(DenonciationSession.class).find(pgdenonciation.getActivePage()*byPage,byPage, sygmdatedenonciation, null, null, UIConstants.PARENT, UIConstants.DENONCIATIONPORTAIL);
			 SimpleListModel listModel = new SimpleListModel(denonciation);
			list.setModel(listModel);
			pgdenonciation.setTotalSize(BeanLocator.defaultLookup(DenonciationSession.class).count(sygmdatedenonciation, null, null, UIConstants.PARENT, UIConstants.DENONCIATIONPORTAIL));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/denonciation/formsuividenonciation.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(DenonciationFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/denonciation/formsuividenonciation.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(DenonciationFormController.PARAM_WIDOW_CODE,
					UIConstants.MODE_EDIT);
			data.put(DenonciationFormController.PARAM_WIDOW_CODE, list
					.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				// de
				// fixer
				// les
				// dimenisons
				// du
				// popup
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// de
				// passer
				// des
				// parametres
				// au
				// popup
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < list.getSelectedCount(); i++) {
					Long codes = (Long)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					System.out.println(codes);
				BeanLocator.defaultLookup(DenonciationSession.class).delete(codes);
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygDenonciation denonciation = (SygDenonciation) data;
		item.setValue(denonciation.getID());

		Listcell cellobjet = new Listcell(denonciation.getObjet());
		cellobjet.setParent(item);
		Listcell celldenonciation = new Listcell(denonciation.getDenonciation());
		celldenonciation.setParent(item);
		Listcell celldatedenonciation = new Listcell(UtilVue.getInstance().formateLaDate2(denonciation.getDatedenonciation()));
		celldatedenonciation.setParent(item);
		Listcell cellfichier = new Listcell(denonciation.getFichier().toString());
		cellfichier.setParent(item);
		 
	}
	
	public void onOK$datedenonciation() {
		onClick$btnRechercher();
	}
	
	public void onClick$btnRechercher() {
		if (datedenonciation.getValue() == null) {
			sygmdatedenonciation = null;
		} else {
			sygmdatedenonciation  = datedenonciation.getValue();
		}	

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
}