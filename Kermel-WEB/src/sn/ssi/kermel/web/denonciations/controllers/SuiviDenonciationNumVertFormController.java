package sn.ssi.kermel.web.denonciations.controllers;


import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;

import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.UtilVue;



public class SuiviDenonciationNumVertFormController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code;
	UtilVue utilVue = UtilVue.getInstance();
	private Include pgdenonciation;
	Session session = getHttpSession();


	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		


	}

	public void onCreate(CreateEvent createEvent) {
		
		  	pgdenonciation.setSrc("/denonciation/dossiernumvert.zul");
	
		
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	
	
	public void onClick$onClose(){
		session.setAttribute("LibelleTab","NUMVERT");
		loadApplicationState("denonciations");
	}
}
