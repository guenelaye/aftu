package sn.ssi.kermel.web.denonciations.controllers;


import java.util.Calendar;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.denonciation.ejb.DenonciationSession;
import sn.ssi.kermel.be.entity.SygDenonciation;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;




@SuppressWarnings("serial")
public class NouvellenonciationFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,codelocalite,annee,nomFichier;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtobjet,txtdenonciation,txtfichier,txtVersionElectronique,txtnomappellant,txtadresse,txtreference;
	private Intbox txttraiter,txtpublier,txtpoubelle,txtnumvert;
	private SygDenonciation denonciation =new SygDenonciation();
	private SimpleListModel lst;
	private Paging pgdenonciation;
	private Datebox datedenonciation,dateappel,datecourrier,datereception;
	private final String ON_LOCALITE = "onLocalite";
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	Long code;
	private Div step0, step1, step2;
	private Component errorComponent;
	private Label lbStatusBar;
	private String errorMsg;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode!=null)
		{
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WIDOW_CODE);
			denonciation = BeanLocator.defaultLookup(DenonciationSession.class)
					.findById(code);
			
//			txtobjet.setValue(denonciation.getObjet());
//			txtdenonciation.setValue(denonciation.getDenonciation());
//			txtfichier.setValue(denonciation.getFichier());
//			datedenonciation.setValue(denonciation.getDatedenonciation());
			
			
			txtreference.setValue(denonciation.getReference());
			txtobjet.setValue(denonciation.getObjet());
			datecourrier.setValue(denonciation.getDatecourrier());
			datereception.setValue(denonciation.getDatereception());
			
			
			txtnumvert.setValue(denonciation.getNumerovert());
			txtnomappellant.setValue(denonciation.getNomappellant());
			dateappel.setValue(denonciation.getDateappel());
			txtadresse.setValue(denonciation.getAdresse());
	
		}
		}	
	}
	public void onOK() {
      if (checkFieldConstraints()){
		denonciation.setReference(txtreference.getValue());
		denonciation.setObjet(txtobjet.getValue());
		denonciation.setDatecourrier(datecourrier.getValue());
		denonciation.setDatereception(datereception.getValue());
		denonciation.setFichier(nomFichier);
		denonciation.setOrigine( UIConstants.DENONCIATIONCOURRIER);
		denonciation.setPoubelle(UIConstants.PARENT);
		
		denonciation.setNomappellant(txtnomappellant.getValue());
		denonciation.setNumerovert(txtnumvert.getValue());
		denonciation.setDateappel(dateappel.getValue());
		denonciation.setAdresse(txtadresse.getValue());
		denonciation.setOrigine( UIConstants.DENONCIATIONNUMEROVERT);
		denonciation.setPoubelle(UIConstants.PARENT);
		
		if (mode==null||mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			BeanLocator.defaultLookup(DenonciationSession.class).save(denonciation);

		} 
		else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			BeanLocator.defaultLookup(DenonciationSession.class).update(denonciation);
		}

		//Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}else{
			throw new WrongValueException(errorComponent, errorMsg);
		}
		loadApplicationState("denonciations");
		detach();
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();
		
	}
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	
	}
	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onEvent(Event arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}
	public void onClick$btnChoixFichier() {
		//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
			if (ToolKermel.isWindows())
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectronique.setValue(nomFichier);
		}

	public void onCheck$courrier() {

		step1.setVisible(false);
		step0.setVisible(true);

     }
		public void onCheck$numerovert() {

			step1.setVisible(true);
			step0.setVisible(false);   
				}

		public void onClick$menuPrevious() {
			
			step0.setVisible(true);
			step1.setVisible(false);
		}
		private boolean checkFieldConstraints()
		{
			try {
				
				if (txtreference.getValue() == null || txtreference.getValue().trim().equalsIgnoreCase("")) {
					errorComponent = txtreference;
					errorMsg = "Veuillez renseigner la r�f�rence";
					lbStatusBar.setValue(errorMsg);
					lbStatusBar.setStyle("color:red");
					return false;
				}

				if (txtobjet.getValue() == null || txtobjet.getValue().trim().equalsIgnoreCase("")) {
					errorComponent = txtobjet;
					errorMsg = "Veuillez renseigner le champ objet";
					lbStatusBar.setValue(errorMsg);
					lbStatusBar.setStyle("color:red");
					return false;
				}
				if (datecourrier.getValue() == null ) {
					errorComponent = datecourrier;
					errorMsg = "Veuillez renseigner le champ Date Courrier";
					lbStatusBar.setValue(errorMsg);
					lbStatusBar.setStyle("color:red");
					return false;
				}
				if (datereception.getValue() == null ) {
					errorComponent = datereception;
					errorMsg = "Veuillez renseigner le champ Date Reception";
					lbStatusBar.setValue(errorMsg);
					lbStatusBar.setStyle("color:red");
					return false;
				}
				
				if (datereception.getValue() == null) {
					errorComponent = datereception;
					errorMsg = "Veuillez renseigner le champ Date Reception";
					lbStatusBar.setValue(errorMsg);
					lbStatusBar.setStyle("color:red");
					return false;
				}
				if (dateappel.getValue() == null ) {
					errorComponent = dateappel;
					errorMsg = "Veuillez renseigner la date";
					lbStatusBar.setValue(errorMsg);
					lbStatusBar.setStyle("color:red");
					return false;
				}

				if (txtobjet.getValue() == null || txtobjet.getValue().trim().equalsIgnoreCase("")) {
					errorComponent = txtobjet;
					errorMsg = "Veuillez renseigner le champ objet";
					lbStatusBar.setValue(errorMsg);
					lbStatusBar.setStyle("color:red");
					return false;
				}
				return true;
			}catch (Exception e) {

				lbStatusBar.setValue(errorMsg);
				// lbStatusBar.setStyle(UIConstants.STYLE_STATUSBAR_ERROR);
				e.printStackTrace();
				throw new WrongValueException(errorComponent, errorMsg);
			}
	}
  }