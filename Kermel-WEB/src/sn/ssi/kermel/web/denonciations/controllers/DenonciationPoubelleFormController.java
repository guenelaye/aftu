package sn.ssi.kermel.web.denonciations.controllers;


import java.util.Calendar;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.denonciation.ejb.DenonciationSession;
import sn.ssi.kermel.be.entity.SygDenonciation;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;




@SuppressWarnings("serial")
public class DenonciationPoubelleFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,codelocalite,annee,nomFichier;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtobjet,txtdenonciation,txtfichier,txtVersionElectronique;
	private Intbox txttraiter,txtpublier,txtpoubelle;
	private SygDenonciation denonciation =new SygDenonciation();
	private SimpleListModel lst;
	private Paging pgdenonciation;
	private Datebox datedenonciation;
	private final String ON_LOCALITE = "onLocalite";
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	Long code;
	private String errorMsg;
	private Component errorComponent;
	private Label lbStatusBar;
	private final String cheminDossier = UIConstants.PATH_PJ;
	UtilVue utilVue = UtilVue.getInstance();
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WIDOW_CODE);
			denonciation = BeanLocator.defaultLookup(DenonciationSession.class)
					.findById(code);
			
			txtobjet.setValue(denonciation.getObjet());
			txtdenonciation.setValue(denonciation.getDenonciation());
			txtfichier.setValue(denonciation.getFichier());
			datedenonciation.setValue(denonciation.getDatedenonciation());
	
		}
			
	}
	public void onOK() {
		if (checkFieldConstraints()){
		denonciation.setObjet(txtobjet.getValue());
		denonciation.setDenonciation(txtdenonciation.getValue());
		denonciation.setFichier(txtfichier.getValue());
		denonciation.setDatedenonciation(datedenonciation.getValue());
		denonciation.setOrigine( UIConstants.DENONCIATIONPOUBELLE);
		denonciation.setPoubelle(UIConstants.NPARENT);
		
		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			BeanLocator.defaultLookup(DenonciationSession.class).save(denonciation);

		} 
		else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			BeanLocator.defaultLookup(DenonciationSession.class).update(denonciation);
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();
	}else{
		throw new WrongValueException(errorComponent, errorMsg);
	}
	}
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		/*
		 * On indique que la fen�tre est notifi�e � chaque fois que le "model"
		 * des modules est mis � jour soit apr�s une insertion, supresion,
		 * modif,...
		 */
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);

	
	}
	@Override
	public void render(Listitem arg0, Object arg1, int index) throws Exception {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onEvent(Event arg0) throws Exception {
		// TODO Auto-generated method stub
		
	}
	public void onClick$btnChoixFichier() {
		//	String cheminDossier1 = cheminDossier.replaceAll("/", "\\\\");
			if (ToolKermel.isWindows())
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
			else
				nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

			txtVersionElectronique.setValue(nomFichier);
		}
	private boolean checkFieldConstraints()
	{
		try {
	    if (txtobjet.getValue() == null || txtobjet.getValue().trim().equalsIgnoreCase("")) {
				errorComponent = txtobjet;
				errorMsg = "Veuillez renseigner le champ objet";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}
			if (txtdenonciation.getValue() == null || txtdenonciation.getValue().trim().equalsIgnoreCase("")) {
				errorComponent = txtdenonciation;
				errorMsg = "Veuillez renseigner la denonciation";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}
			
			if (datedenonciation.getValue() == null) {
				errorComponent = datedenonciation;
				errorMsg = "Veuillez renseigner la Date de Denonciation";
				lbStatusBar.setValue(errorMsg);
				lbStatusBar.setStyle("color:red");
				return false;
			}
			return true;
		}catch (Exception e) {

			lbStatusBar.setValue(errorMsg);
			// lbStatusBar.setStyle(UIConstants.STYLE_STATUSBAR_ERROR);
			e.printStackTrace();
			throw new WrongValueException(errorComponent, errorMsg);
		}
}

}