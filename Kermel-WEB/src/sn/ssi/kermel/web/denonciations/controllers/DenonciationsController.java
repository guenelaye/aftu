package sn.ssi.kermel.web.denonciations.controllers;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.web.common.controllers.AbstractWindow;




public class DenonciationsController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
    private Tab TAB_Portail,TAB_CourriersDenonciation,TAB_NumVert,TAB_Poubelle;
	Session session = getHttpSession();
	
	private String LibelleTab;


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		LibelleTab = (String) session.getAttribute("LibelleTab");
		
		if(LibelleTab!=null)
		{
			if(LibelleTab.equals("PORTAIL"))
			  {
				TAB_Portail.setSelected(true);
				Include inc = (Include) this.getFellowIfAny("incPortail");
				inc.setSrc("/denonciation/listdenonciation.zul");		
			  }
			else if(LibelleTab.equals("COURRIER"))
			  {
				TAB_CourriersDenonciation.setSelected(true);
				Include inc = (Include) this.getFellowIfAny("incCourriersDenonciation");
				inc.setSrc("//denonciation/listdenonciationcourrier.zul");		
			  }
			else if(LibelleTab.equals("NUMVERT"))
			  {
				TAB_NumVert.setSelected(true);
				Include inc = (Include) this.getFellowIfAny("incNumVert");
				inc.setSrc("/denonciation/listdenonciationnumvert.zul");		
			  }
			else if((LibelleTab.equals("POUCOUR"))||(LibelleTab.equals("POUPORT"))||(LibelleTab.equals("POUNUMVERT")))
			  {
				TAB_Poubelle.setSelected(true);
				Include inc = (Include) this.getFellowIfAny("incPoubelle");
				inc.setSrc("/denonciation/dossierpoubelle.zul");		
			  }
			else
			{
				 Include inc = (Include) this.getFellowIfAny("incPortail");
				    inc.setSrc("/denonciation/listdenonciation.zul");	
			}
		}
		else
		{
			 Include inc = (Include) this.getFellowIfAny("incPortail");
			    inc.setSrc("/denonciation/listdenonciation.zul");	
		}
		
	
	}

	@Override
	public void onEvent(Event event) throws Exception {
		
	}

	
}