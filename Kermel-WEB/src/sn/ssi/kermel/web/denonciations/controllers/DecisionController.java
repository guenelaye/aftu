package sn.ssi.kermel.web.denonciations.controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.denonciation.ejb.DecisionDenonciationSession;
import sn.ssi.kermel.be.denonciation.ejb.DenonciationSession;
import sn.ssi.kermel.be.denonciation.ejb.PieceJointeDenonciationSession;
import sn.ssi.kermel.be.denonciation.ejb.TypeDecisionDenonciationSession;
import sn.ssi.kermel.be.entity.SygDecisionDenonciation;
import sn.ssi.kermel.be.entity.SygDenonciation;
import sn.ssi.kermel.be.entity.SygPieceJointeDenonciation;
import sn.ssi.kermel.be.entity.SygTypeDecisionDenonciation;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class DecisionController extends AbstractWindow implements AfterCompose,EventListener {

	
	
	private Long idpjointeAC,idpjointe;
	

	
	UtilVue utilVue = UtilVue.getInstance();
	private Long code,iddecisioncont;
	private SygDecisionDenonciation decisiondenon= new SygDecisionDenonciation();
	private SygDecisionDenonciation decisiondenons = new SygDecisionDenonciation();
	
	
	private Component errorComponent;
	private String errorMsg;
	 private Label lbStatusBar;
	 private static final String ERROR_MSG_STYLE = "color:red";
	 private Datebox daterecours;
		
		private Textbox txtCommentaire;
		
		
		private Date datestatut,datejour;
		private String Statut;
		
		//private Radiogroup radioDecision;
		private Textbox txtVersionElectronique1;
		
		// /Decision 
		private SygTypeDecisionDenonciation typedecision = new SygTypeDecisionDenonciation();
		private Textbox txtRechercherDecision;
		private Bandbox bdDecision;
		private Paging pgDecision;
		private Listbox lstDecision;
		private String LibelleDecision = null;
		private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
	 
	
		// paramétre denonciation
		Long codedenonciation = null;
		private SygDenonciation denonciation;
		Session session = getHttpSession();

	 private static final String CONFIRMPUBLIER = "CONFIRMPUBLIER";
	
	//div plaignante
	 private SygPieceJointeDenonciation pjointe = new SygPieceJointeDenonciation();
		private SygPieceJointeDenonciation pjointes = new SygPieceJointeDenonciation();
		private SygPieceJointeDenonciation pjointesAC = new SygPieceJointeDenonciation();
		private String nomFichier;
		private Datebox dtdate,dtdatecourrier;
		private Textbox txtLibelles, txtVersionElectronique,txtref,txtnumero;
		private final String cheminDossier = UIConstants.PATH_PJCONT;
	
	//div AC
	private Datebox dtdateAC,dtdatecourrierAC;
	private Textbox txtLibellesAC, txtVersionElectroniqueAC,txtrefAC;

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		// champ Decision  du formulaire
		addEventListener(ApplicationEvents.ON_DECISION, this);
		lstDecision.setItemRenderer(new decisionRenderer());
		pgDecision.setPageSize(byPageBandbox);
		pgDecision.addForward(ApplicationEvents.ON_PAGING, this, ApplicationEvents.ON_DECISION);
		Events.postEvent(ApplicationEvents.ON_DECISION, this, null);

		codedenonciation = (Long) session.getAttribute("numdenonimation");
		session.setAttribute("numdenonimation", codedenonciation);
		denonciation = BeanLocator.defaultLookup(DenonciationSession.class).findById(codedenonciation);
		
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
	}
	
	@Override
	public void onEvent(Event event) throws Exception {

		  if(event.getName().equals(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			 String confirmer = (String) ((HashMap<String, Object>) event.getData()).get(CONFIRMPUBLIER);
			 if (confirmer != null && confirmer.equalsIgnoreCase("Publication_Confirmer")) 
			 {
				 
				// contentieux.setTraitement(Labels.getLabel("kermel.contentieux.recevabilite.publier"));
				 decisiondenon.setDecStatut("PUB");
				 BeanLocator.defaultLookup(DenonciationSession.class).update(denonciation);
				 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			 }
		 }// champ decision
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DECISION)) {
				List<SygTypeDecisionDenonciation> typedecision = BeanLocator.defaultLookup(TypeDecisionDenonciationSession.class).find(
						pgDecision.getActivePage() * byPageBandbox, byPageBandbox, null, LibelleDecision);
				SimpleListModel listModel = new SimpleListModel(typedecision);
				lstDecision.setModel(listModel);
				pgDecision.setTotalSize(BeanLocator.defaultLookup(TypeDecisionDenonciationSession.class).count(null, LibelleDecision));
			}
	}

	
	public void onCreate(CreateEvent event) {
	
		 //div decision
		decisiondenons = BeanLocator.defaultLookup(DenonciationSession.class).findden(codedenonciation);
			//if(contentieux!=null){
			if(decisiondenons!=null){
				decisiondenon=decisiondenons;
				iddecisioncont=decisiondenons.getId();
			
			txtnumero.setValue(decisiondenons.getDecNumero());
		    daterecours.setValue(decisiondenons.getDecDate());
		    txtVersionElectronique1.setValue(decisiondenons.getDecFichier());
		    txtCommentaire.setValue(decisiondenons.getDecCommentaire());
		    typedecision=decisiondenon.getTypedcision();
		    if(typedecision!=null)
		    bdDecision.setValue(typedecision.getLibelletypedecision());
			}
		
		
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			
			if(bdDecision.getValue().equals(""))
		     {
           errorComponent = bdDecision;
           errorMsg = Labels.getLabel("kermel.contentieux.Decision")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(txtnumero.getValue().equals(""))
		     {
             errorComponent = txtnumero;
             errorMsg = Labels.getLabel("kermel.contentieux.Prenom")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if (daterecours.getValue() == null) {

				errorComponent = daterecours;
				errorMsg = Labels.getLabel("cciad.conteneur.tac.demande.daterecours") + ": " + Labels.getLabel("cciad.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException(errorComponent, errorMsg);

			}
			
			if(txtVersionElectronique1.getValue().equals(""))
		     {
         errorComponent = txtVersionElectronique1;
         errorMsg = Labels.getLabel("kermel.contentieux.pj.Fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			
			
			
			
			/* Controle date inferieure a date du jour */

			if (daterecours.getValue() != null) {
				datejour = new Date();
				if ((daterecours.getValue()).after((datejour))) {
				errorComponent = daterecours;
					errorMsg = Labels.getLabel("kermel.contentieux.Controledate")+": "+ UtilVue.getInstance().formateLaDate(datejour);
					lbStatusBar.setStyle(ERROR_MSG_STYLE);
					lbStatusBar.setValue(errorMsg);
					throw new WrongValueException(errorComponent, errorMsg);				}
			}
		
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}


public void onClick$btnChoixFichier1() {

	if (ToolKermel.isWindows())
		nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
	else
		nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

	txtVersionElectronique1.setValue(nomFichier);
}
	
	public void onClick$bcvalider() {

		if(checkFieldConstraints())
		{

			decisiondenon.setTypedcision(typedecision);
			decisiondenon.setDecNumero(txtnumero.getValue());
			decisiondenon.setDecDate(daterecours.getValue());
			decisiondenon.setDecFichier(txtVersionElectronique1.getValue());
			decisiondenon.setDecCommentaire(txtCommentaire.getValue());
			decisiondenon.setDenonciation(denonciation);
				

			decisiondenon.setDecStatut("VAL");

		if(decisiondenon==null){
			
			decisiondenon=BeanLocator.defaultLookup(DecisionDenonciationSession.class).save(decisiondenon);
	      }else{
	    	  BeanLocator.defaultLookup(DecisionDenonciationSession.class).update(decisiondenon);
	      }
		
	//	decisiondenons = BeanLocator.defaultLookup(DecisionDenonciationSession.class).findCont(codedenonciation);
		iddecisioncont=decisiondenons.getId();
		//decisiondenon=decisiondenons;
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		
	} 
	}

	public void onClick$menuFermer() {
		detach();
	}
	
	public void onClick$bcpublier() throws InterruptedException {
		Statut=decisiondenon.getDecStatut();
		if (Statut.equals("VAL")){
		HashMap<String, String> display = new HashMap<String, String>(); // permet
		 display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.plansdepassation.publier"));
		 display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.contentieux.recevabilite.publier.titre"));
		 display.put(MessageBoxController.DSP_HEIGHT, "150px");
		 display.put(MessageBoxController.DSP_WIDTH, "100px");
        HashMap<String, Object> map = new HashMap<String, Object>(); // permet
        map.put(CONFIRMPUBLIER, "Publication_Confirmer");
		 showMessageBox(display, map);
		
		}else{
			   Messagebox.show(Labels.getLabel("kermel.contentieux.recevabilite.statut.publier.controle"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		      }
		
	}
	
	
	// //////////Decision
	public void onSelect$lstDecision() {
		typedecision=(SygTypeDecisionDenonciation) lstDecision.getSelectedItem().getValue();
		bdDecision.setValue(typedecision.getLibelletypedecision());
		bdDecision.close();
	}

	public class decisionRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data, int index)  throws Exception {
			SygTypeDecisionDenonciation typedecision = (SygTypeDecisionDenonciation) data;
			item.setValue(typedecision);
			item.setAttribute(UIConstants.ATTRIBUTE_LIBELLE, typedecision.getLibelletypedecision());

			Listcell cellLibelle = new Listcell("");
			if (typedecision.getLibelletypedecision() != null) {
				cellLibelle.setLabel(typedecision.getLibelletypedecision());
			}
			cellLibelle.setParent(item);
			//				

		}
	}

	public void onFocus$txtRechercherDecision() {
		if (txtRechercherDecision.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.Decision"))) {
			txtRechercherDecision.setValue("");
		}
	}

	public void onBlur$txtRechercherDecision() {
		if (txtRechercherDecision.getValue().equalsIgnoreCase("")) {
			txtRechercherDecision.setValue(Labels.getLabel("kermel.contentieux.Decision"));
		}
	}

	public void onClick$btnRechercherDecision() {
		if (txtRechercherDecision.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.Decision")) || txtRechercherDecision.getValue().equals("")) {
			LibelleDecision = null;
		} else {
			LibelleDecision = txtRechercherDecision.getValue();
		}
		Events.postEvent(ApplicationEvents.ON_DECISION, this, null);
	}

	// ////////////////fin ////////////////////////////////

	//div plaignant***********************************************div plaignant***********************
	public void onClick$btnChoixFichier() {

		if (ToolKermel.isWindows())
			nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
		else
			nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

		txtVersionElectronique.setValue(nomFichier);
	}
	
	
private boolean checkFieldConstraints2() {
		
		try {
		
			if(dtdate.getValue()==null)
		     {
               errorComponent = dtdate;
               errorMsg = Labels.getLabel("kermel.contentieux.pj.Date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(dtdatecourrier.getValue()==null)
		     {
              errorComponent = dtdatecourrier;
              errorMsg = Labels.getLabel("kermel.contentieux.pj.Datecourrier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		
			if(txtVersionElectronique.getValue().equals(""))
		     {
          errorComponent = txtVersionElectronique;
          errorMsg = Labels.getLabel("kermel.contentieux.pj.Fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			
			if(txtLibelles.getValue().equals(""))
		     {
        errorComponent = txtLibelles;
        errorMsg = Labels.getLabel("kermel.contentieux.pj.Libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}


public void onClick$bcvalider2() {


	if(checkFieldConstraints2())
	{
		pjointe.setLibelle(txtLibelles.getValue());
		pjointe.setFichier(txtVersionElectronique.getValue());
		pjointe.setDate(dtdate.getValue());
		pjointe.setDatecourrier(dtdatecourrier.getValue());
		pjointe.setReference(txtref.getValue());
		pjointe.setDecision("PlaignantD");

		if (idpjointe==null){ 
			pjointes=BeanLocator.defaultLookup(PieceJointeDenonciationSession.class).save(pjointe);
		}
        else {
        BeanLocator.defaultLookup(PieceJointeDenonciationSession.class).update(pjointe);
        }
	//	pjointes = BeanLocator.defaultLookup(PieceJointeDenonciationSession.class).findden(codedenonciation,null,"PlaignantD");
		idpjointe=pjointes.getId();
	//	pjointe=pjointes;


	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	
} 
}
//div AC*******************************************************div AC********************
public void onClick$btnChoixFichierAC() {

	if (ToolKermel.isWindows())
		nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
	else
		nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

	txtVersionElectroniqueAC.setValue(nomFichier);
}

private boolean checkFieldConstraintsAC() {
	
	try {
	
		if(dtdateAC.getValue()==null)
	     {
           errorComponent = dtdateAC;
           errorMsg = Labels.getLabel("kermel.contentieux.pj.Date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		   lbStatusBar.setStyle(ERROR_MSG_STYLE);
		   lbStatusBar.setValue(errorMsg);
		  throw new WrongValueException (errorComponent, errorMsg);
	     }
		
		
		if(dtdatecourrierAC.getValue()==null)
	     {
          errorComponent = dtdatecourrierAC;
          errorMsg = Labels.getLabel("kermel.contentieux.pj.Datecourrier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		   lbStatusBar.setStyle(ERROR_MSG_STYLE);
		   lbStatusBar.setValue(errorMsg);
		  throw new WrongValueException (errorComponent, errorMsg);
	     }
	
		if(txtVersionElectroniqueAC.getValue().equals(""))
	     {
      errorComponent = txtVersionElectroniqueAC;
      errorMsg = Labels.getLabel("kermel.contentieux.pj.Fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		   lbStatusBar.setStyle(ERROR_MSG_STYLE);
		   lbStatusBar.setValue(errorMsg);
		  throw new WrongValueException (errorComponent, errorMsg);
	     }
		
		if(txtLibellesAC.getValue().equals(""))
	     {
   errorComponent = txtLibellesAC;
   errorMsg = Labels.getLabel("kermel.contentieux.pj.Libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
		   lbStatusBar.setStyle(ERROR_MSG_STYLE);
		   lbStatusBar.setValue(errorMsg);
		  throw new WrongValueException (errorComponent, errorMsg);
	     }
	
		return true;
			
	}
	catch (Exception e) {
		errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
		+ " [checkFieldConstraints]";
		errorComponent = null;
		return false;

		
	}
	
}


public void onClick$bcvaliderAC() {


	if(checkFieldConstraintsAC())
	{
		pjointe.setLibelle(txtLibellesAC.getValue());
		pjointe.setFichier(txtVersionElectroniqueAC.getValue());
		pjointe.setDate(dtdateAC.getValue());
		pjointe.setDatecourrier(dtdatecourrierAC.getValue());
		pjointe.setReference(txtrefAC.getValue());
		pjointe.setRecevable("ACD");

		if (idpjointe==null){ 
			pjointes=	BeanLocator.defaultLookup(PieceJointeDenonciationSession.class).save(pjointe);
		}
        else {
        BeanLocator.defaultLookup(PieceJointeDenonciationSession.class).update(pjointe);
        }
		//pjointes = BeanLocator.defaultLookup(PieceJointeDenonciationSession.class).findden(codedenonciation,null,"PlaignantD");
		idpjointe=pjointes.getId();
		//pjointe=pjointes;


	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	
   } 
}

}
