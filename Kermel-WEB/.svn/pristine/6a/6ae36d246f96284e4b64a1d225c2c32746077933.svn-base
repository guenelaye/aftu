package sn.ssi.kermel.web.denonciations.controllers;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.denonciation.ejb.DecisionDenonciationSession;
import sn.ssi.kermel.be.entity.SygDecisionDenonciation;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;


@SuppressWarnings("serial")
public class ListSaisiDecisionController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtNom,txtPrenom;
    String nom=null,page=null,prenom=null;
    private Listheader lshNom;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_CONT_DEC, MOD_CONT_DEC, SUPP_CONT_DEC;
    private String login;
    private String statut,imagestatut;
    private static final String CONFIRMPUBLIER = "CONFIRMPUBLIER";
    private Long iddecision;
    private SygDecisionDenonciation decisiondenon;
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_CONT_DEC);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_CONT_DEC != null) { ADD_CONT_DEC.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_CONT_DEC != null) { MOD_CONT_DEC.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUPP_CONT_DEC != null) { SUPP_CONT_DEC.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		lshNom.setSortAscending(new FieldComparator("decAuteurrecour", false));
		lshNom.setSortDescending(new FieldComparator("decAuteurrecour", true));
		
	
		
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	login = ((String) getHttpSession().getAttribute("user"));
	}

	
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygDecisionDenonciation> decisiondenon = BeanLocator.defaultLookup(DecisionDenonciationSession.class).find(activePage,byPage,null,null);
			 SimpleListModel listModel = new SimpleListModel(decisiondenon);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup(DecisionDenonciationSession.class).count(null,null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/denonciation/formsaisidecision.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"600px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
		//	data.put(SaisiDecisionFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			
                 String statut = (String)lstListe.getSelectedItem().getAttribute("statut");
			
			 if(statut.equalsIgnoreCase("Saisie")){
			
			final String uri = "/denonciation/formsaisidecision.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"600px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
//			data.put(SaisiDecisionFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
//			data.put(SaisiDecisionFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
			
			 }else{
					
				   Messagebox.show(Labels.getLabel("kermel.contentieux.publication.controle.modif"), "Erreur", Messagebox.OK, Messagebox.ERROR);
			      }
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
				
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				
				String statut = (String)lstListe.getSelectedItem().getAttribute("statut");
				 if(statut.equalsIgnoreCase("Saisie")){
				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				 }else{
						
					   Messagebox.show(Labels.getLabel("kermel.contentieux.publication.controle.suprimmer"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				    }
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				String confirmer = (String) ((HashMap<String, Object>) event.getData()).get(CONFIRMPUBLIER);
				if (confirmer != null && confirmer.equalsIgnoreCase("Publication_Confirmer")) 
				 {
					 
					decisiondenon.setDecStatut("Publi�");
					//decisioncont.setDatepublication(new Date());
						BeanLocator.defaultLookup(DecisionDenonciationSession.class).update(decisiondenon);
					
					 
				 }else{
				
				   for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				   Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
				   BeanLocator.defaultLookup(DecisionDenonciationSession.class).delete(codes);
				   BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_CONT_DEC", Labels.getLabel("kermel.common.contentieux.suppression")+" :" + new Date(), login);
				  }
			  }
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
			else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
				Listcell button = (Listcell) event.getTarget();
				String toDo = (String) button.getAttribute(UIConstants.TODO);
				iddecision = (Long) button.getAttribute("iddecision");
				decisiondenon=BeanLocator.defaultLookup(DecisionDenonciationSession.class).findById(iddecision);
				
				if (toDo.equalsIgnoreCase("publier"))
				{
					
					HashMap<String, String> display = new HashMap<String, String>(); // permet
					 display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.plansdepassation.publier"));
					 display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.contentieux.publication"));
					 display.put(MessageBoxController.DSP_HEIGHT, "150px");
					 display.put(MessageBoxController.DSP_WIDTH, "100px");
			        HashMap<String, Object> map = new HashMap<String, Object>(); // permet
			        map.put(CONFIRMPUBLIER, "Publication_Confirmer");
					 showMessageBox(display, map);
					
					
					
				}else if(toDo.equalsIgnoreCase("d�publier")){
					
					decisiondenon.setDecStatut("Saisie");
					//decisioncont.setDatepublication(null);
					BeanLocator.defaultLookup(DecisionDenonciationSession.class).update(decisiondenon);
					
					 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
					
				}

			}

		
		
	

	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygDecisionDenonciation decisiondenon = (SygDecisionDenonciation) data;
		item.setValue(decisiondenon.getId());

		
		Listcell cellDaterecours = new
		 Listcell(UtilVue.getInstance().formateLaDate(decisiondenon.getDecDate()));
		 cellDaterecours.setParent(item);
		 
		 Listcell cellObjet = new Listcell(decisiondenon.getDecObjet());
		 cellObjet.setParent(item);
		 
		 Listcell cellRaisonSociale = new Listcell(decisiondenon.getDecAuteurrecour());
		 cellRaisonSociale.setParent(item);
		 
		 Listcell cellTelephone = new Listcell(decisiondenon.getDecStatut());
		 cellTelephone.setParent(item);
		 
//		 Listcell cellAC = new Listcell(decisioncont.getAutoritecontractante().getDenomination()+"("+decisioncont.getAutoritecontractante().getSigle()+")");
//		 cellAC.setParent(item);
		 
		 
		 
		 Listcell cellDecision = new Listcell(decisiondenon.getTypedcision().getLibelletypedecision());
		 cellDecision.setParent(item);
		 
		   statut=decisiondenon.getDecStatut();
		   item.setAttribute("statut", statut);
           Listcell cellImageStatut = new Listcell("");
		 
		 if(statut.equalsIgnoreCase("Saisie")){
			 imagestatut="/images/deletemoins.png";
			 
			 cellImageStatut.setAttribute(UIConstants.TODO, "publier");
			 cellImageStatut.setTooltiptext("Publier");
		 }else{
			 imagestatut="/images/accept.png";
			 cellImageStatut.setAttribute(UIConstants.TODO, "d�publier");
			 cellImageStatut.setTooltiptext("D�publier");
		 }
		
		    cellImageStatut.setImage(imagestatut);
			cellImageStatut.setAttribute("iddecision", decisiondenon.getId());
			cellImageStatut.addEventListener(Events.ON_CLICK, ListSaisiDecisionController.this);
			cellImageStatut.setParent(item);
		 
		

	}
	public void onClick$bchercher()
	{
		
		if((txtNom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.requerant")))||(txtNom.getValue().equals("")))
		 {
			nom=null;
			
		 }
		else
		{
			nom=txtNom.getValue();
			page="0";
		}
		
		
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	//Nom
	public void onFocus$txtNom()
	{
		if(txtNom.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.requerant")))
			txtNom.setValue("");
		
	}
	
	public void onBlur$txtNom()
	{
		if(txtNom.getValue().equals(""))
			txtNom.setValue(Labels.getLabel("kermel.contentieux.requerant"));
	}

	public void onOK$txtNom()
	{
		onClick$bchercher();
	}
	
	
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
}