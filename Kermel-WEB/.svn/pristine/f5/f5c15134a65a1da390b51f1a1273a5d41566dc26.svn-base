package sn.ssi.kermel.web.traitementdossier.controllers;


import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygTypesDossiers;
import sn.ssi.kermel.be.traitementdossier.ejb.TypesDossiersSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



@SuppressWarnings("serial")
public class TypesDossiersFormController extends AbstractWindow implements
		 AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WIDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtcode,txtlibelle,txtdescription;
	private SygTypesDossiers dossiers =new SygTypesDossiers ();
	private SimpleListModel lst;
	private final String ON_LOCALITE = "onLocalite";
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	Long code;
	private Intbox txtdelaitraitement,delairevue,delaiversioncorriges;
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();

		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WIDOW_CODE);
			dossiers = BeanLocator.defaultLookup(TypesDossiersSession.class).findById(code);

			txtcode.setValue(dossiers.getCode());
			txtlibelle.setValue(dossiers.getLibelle());
			txtdelaitraitement.setValue(dossiers.getDelaitraitement());
			txtdescription.setValue(dossiers.getDescription());
			
			delairevue.setValue(dossiers.getDelairevue());
			delaiversioncorriges.setValue(dossiers.getDelaicorrigee());
			}
			
		}
			
	
	public void onOK() {
		
		dossiers.setCode(txtcode.getValue());
		dossiers.setLibelle(txtlibelle.getValue());
		dossiers.setDelaitraitement(txtdelaitraitement.getValue());
		dossiers.setDescription(txtdescription.getValue());
		dossiers.setDelairevue(delairevue.getValue());
		dossiers.setDelaicorrigee(delaiversioncorriges.getValue());
		if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
			BeanLocator.defaultLookup(TypesDossiersSession.class).save(dossiers);

		} 
		else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			BeanLocator.defaultLookup(TypesDossiersSession.class).update(dossiers);
		}

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();

	}
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

	

	
	}
	
private boolean checkFieldConstraints() {
		
		try {
		
			if(txtcode.getValue().equals(""))
		     {
               errorComponent = txtcode;
               errorMsg = Labels.getLabel("kermel.common.form.code")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtlibelle.getValue().equals(""))
		     {
              errorComponent = txtlibelle;
              errorMsg = Labels.getLabel("kermel.common.form.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			if(txtdelaitraitement.getValue()==null)
		     {
              errorComponent = txtdelaitraitement;
              errorMsg = Labels.getLabel("kermel.common.form.delais.revue.details")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(delairevue.getValue()==null)
		     {
             errorComponent = delairevue;
             errorMsg = Labels.getLabel("archivage.common.form.delais.corrige.details")+": "+Labels.getLabel("archivage.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(delaiversioncorriges.getValue()==null)
		     {
             errorComponent = delaiversioncorriges;
             errorMsg = Labels.getLabel("kermel.common.form.delais.corrige.details")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("Saemap.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
}