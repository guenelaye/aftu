package sn.ssi.kermel.web.referentiel.controllers;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.be.referentiel.ejb.TypesmarchesSession;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class TypemarcheFormController extends AbstractWindow implements
		EventListener, AfterCompose {

	/**
	 * 
	 */
	public static final String PARAM_WINDOW_CODE = "CODE";
	public static final String PARAM_WINDOW_MODE = "MODE";
	private String mode,login;
	public static final String WINDOW_PARAM_MODE = "MODE";
	private Textbox txtLibelle,txtCode,txtDescriptions;
	Long code;
	private	SygTypesmarches type=new SygTypesmarches();
	private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg;
	private static final String ERROR_MSG_STYLE = "color:red";
	List<SygTypesmarches> types = new ArrayList<SygTypesmarches>();
	
	@Override
	public void onEvent(Event event) throws Exception {


	}


	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
	}

	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		Map<String, Object> map = (Map<String, Object>) event.getArg();
		login = ((String) getHttpSession().getAttribute("user"));
		mode = (String) map.get(PARAM_WINDOW_MODE);
		if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
			code = (Long) map.get(PARAM_WINDOW_CODE);
			type = BeanLocator.defaultLookup(TypesmarchesSession.class).findById(code);
			txtCode.setValue(type.getCode());
			txtLibelle.setValue(type.getLibelle());
			txtDescriptions.setValue(type.getDescription());
			
		}
	}

	public void onOK() {
		if(checkFieldConstraints())
		{
			type.setCode(txtCode.getValue());
			type.setLibelle(txtLibelle.getValue());
			type.setDescription(txtDescriptions.getValue()); 
		  	if (mode.equalsIgnoreCase(UIConstants.MODE_NEW)) {
				BeanLocator.defaultLookup(TypesmarchesSession.class).save(type);
				BeanLocator.defaultLookup(JournalSession.class).logAction("ADD_TYPESMARCHES", Labels.getLabel("kermel.referentiel.common.typemarche.ajouter")+" :" + new Date(), login);
				
			} 
			else if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT)) {
				BeanLocator.defaultLookup(TypesmarchesSession.class).update(type);
				BeanLocator.defaultLookup(JournalSession.class).logAction("MOD_TYPESMARCHES", Labels.getLabel("kermel.referentiel.common.typemarche.modifier")+" :" + new Date(), login);
				
			}
	
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
			detach();
		}
	}

	
private boolean checkFieldConstraints() {
		
		try {
		
			
			

			if(txtCode.getValue().equals(""))
		     {
               errorComponent = txtCode;
               errorMsg = Labels.getLabel("kermel.bureau.douane.code")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			types=BeanLocator.defaultLookup(TypesmarchesSession.class).find(txtCode.getValue());
			if (mode.equalsIgnoreCase(UIConstants.MODE_EDIT))
			{
				 if(!type.getCode().equalsIgnoreCase(txtCode.getValue()))
				 {
					 if(types.size()>0)
				     {
		                errorComponent = txtCode;
						errorMsg =Labels.getLabel("kermel.bureau.douane.code")+" "+ Labels.getLabel("kermel.referentiel.existe")+": "+txtCode.getValue();
						lbStatusBar.setStyle(ERROR_MSG_STYLE);
						lbStatusBar.setValue(errorMsg);
						throw new WrongValueException (errorComponent, errorMsg);
				    }
				 }
			 
			}
			else
			{
				  if(types.size()>0)
				  {
					errorComponent = txtCode;
					errorMsg = Labels.getLabel("kermel.bureau.douane.code")+" "+Labels.getLabel("kermel.referentiel.existe")+" :"+txtCode.getValue();
					lbStatusBar.setStyle(ERROR_MSG_STYLE);
					lbStatusBar.setValue(errorMsg);
					throw new WrongValueException (errorComponent, errorMsg);
				  }
			}
			if(txtLibelle.getValue().equals(""))
		     {
             errorComponent = txtLibelle;
             errorMsg = Labels.getLabel("kermel.referentiel.banque.libelle")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
		     }
			
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	

}
