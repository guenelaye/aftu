package sn.ssi.kermel.web.aoodeuxetapes.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygMonnaieoffre;
import sn.ssi.kermel.be.entity.SygPays;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrederetraitdaoSession;
import sn.ssi.kermel.be.referentiel.ejb.PaysSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;

@SuppressWarnings("serial")
public class RegistreRetraitsDAOController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe,lstPays;
	private Paging pgPagination,pgPays;
	private Bandbox bdPays;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle,txtRechercherPays;
    String libellemonnaie=null,page=null,login,codesuppression,libellesuppression;
    Session session = getHttpSession();
    List<SygSecteursactivites> categories = new ArrayList<SygSecteursactivites>();
    SygSecteursactivites categorie=null;
    private int byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
    private Textbox txtRechercherMonnaie;
    SygBailleurs bailleur=null;
    private Label lbStatusBar;
	private Component errorComponent;
	private String errorMsg,libellepays=null;
	private static final String ERROR_MSG_STYLE = "color:red";
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygRealisations realisation=new SygRealisations();
	private Long idretrait=null;
	List<SygRealisationsBailleurs> realisations = new ArrayList<SygRealisationsBailleurs>();
	SygRealisationsBailleurs sources=new SygRealisationsBailleurs();
	SygDossiers dossier=new SygDossiers();
	SygRetraitregistredao retrait=new SygRetraitregistredao();
	SygMonnaieoffre monnaie=new SygMonnaieoffre();
	private Bandbox bdMonnaie;
	private Decimalbox dcTaux;
	private Div step0,step1,step2;
	private Menuitem menuAjouter,menuModifier,menuSupprimer;
	private Datebox dtdateretrait;
	private Textbox txtRaisonsocial,txtTelephone,txtFax,txtEmail;
	private Decimalbox dcMontant;
	private Radio rdespece,rdcheque,rdautres;
	private Radiogroup rdpodepaiement;
	private Label lblNbrplis,lblMontant,lblPartARMP;
	private BigDecimal montant=new BigDecimal(0);
	private BigDecimal montantarmp=new BigDecimal(0);
	SygPays pays=new SygPays();
	private Iframe idIframe;
	public static final String EDIT_URL = "http://"+UIConstants.IP_SSI+":"+UIConstants.PORT_SSI+"/EtatsKermel/OuverturePlisPDFServlet";
	private Menuitem menuEditer;
	List<SygRetraitregistredao> retraits = new ArrayList<SygRetraitregistredao>();
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
    	lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
		addEventListener(ApplicationEvents.ON_PAYS, this);
		pgPays.setPageSize(byPageBandbox);
		pgPays.addForward("onPaging", this, ApplicationEvents.ON_PAYS);
		lstPays.setItemRenderer(new PaysRenderer());
		Events.postEvent(ApplicationEvents.ON_PAYS, this, null);
    
	}


	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		realisation=appel.getRealisation();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,1);
		if(dossier==null)
		{
			menuAjouter.setDisabled(true);
			menuModifier.setDisabled(true);
			menuSupprimer.setDisabled(true);
		}
		else
		{
			if(appel.getApoDatepvouverturepli()!=null)
			 {
				menuAjouter.setDisabled(true);
				menuModifier.setDisabled(true);
				menuSupprimer.setDisabled(true);
			 }
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			Events.postEvent(ApplicationEvents.ON_MONNAIES, this, null);
			dcMontant.setValue(dossier.getDosmontantdao());
		}
		
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 retraits = BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).find(activePage,byPageBandbox,dossier);
			 lstListe.setModel(new SimpleListModel(retraits));
			 pgPagination.setTotalSize(BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).count(dossier));
			 lblNbrplis.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.totalpliretire")+": "+BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).find(0,-1,dossier).size());
			 montant=new BigDecimal(0);
			 for (int i = 0; i < retraits.size(); i++) {
				 montant=montant.add(retraits.get(i).getMontantverse());
			 }
			 if(retraits.size()>0)
			 {
				 montantarmp=montant.divide(new BigDecimal(2));
				 lblMontant.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.totalmontantverse")+": "+ToolKermel.format2Decimal(montant));
				 lblPartARMP.setValue(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.codepartarmp")+": "+ToolKermel.format2Decimal(montantarmp));
				 appel.setApomontantversement(montant);	
				 BeanLocator.defaultLookup(AppelsOffresSession.class).update(appel);
			 }
			
		} 
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_PAYS)){
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPageBandbox = -1;
				pgPays.setPageSize(1000);
			} else {
				byPageBandbox = UIConstants.DSP_BANDBOX_BY_PAGE;
				activePage = pgPays.getActivePage() * byPageBandbox;
				pgPays.setPageSize(byPageBandbox);
			}
			List<SygPays> pays = BeanLocator.defaultLookup(PaysSession.class).find(activePage, byPageBandbox,libellepays,null);
			lstPays.setModel(new SimpleListModel(pays));
			pgPays.setTotalSize(BeanLocator.defaultLookup(PaysSession.class).count(libellepays,null));
		}
		else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
			for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes = ((SygRetraitregistredao)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue()).getId();
				BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).delete(codes);
			}
			session.setAttribute("libelle", "registreretraitdao");
			loadApplicationState("aoo_deuxetapes");
		}
		
	}

	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		
		SygRetraitregistredao retraits = (SygRetraitregistredao) data;
		item.setValue(retraits);

		 Listcell cellDate = new Listcell(UtilVue.getInstance().formateLaDate2(retraits.getDateRetrait()));
		 cellDate.setParent(item);
		 
		 Listcell cellRaisonsocial = new Listcell(retraits.getNomSoumissionnaire());
		 cellRaisonsocial.setParent(item);
		 
		 Listcell cellTelephone = new Listcell(retraits.getTelephone());
		 cellTelephone.setParent(item);
		 
		 Listcell cellMontant = new Listcell(ToolKermel.format2Decimal(retraits.getMontantverse()));
		 cellMontant.setParent(item);
	}

	public void onClick$menuSupprimer()
	{
		if (lstListe.getSelectedItem() == null)
			
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		

		HashMap<String, String> display = new HashMap<String, String>(); // permet
		display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
		display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
		display.put(MessageBoxController.DSP_HEIGHT, "250px");
		display.put(MessageBoxController.DSP_WIDTH, "47%");

		HashMap<String, Object> map = new HashMap<String, Object>(); // permet
		map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
		showMessageBox(display, map);
		
	}
	public void onClick$menuModifier()
	{
		if (lstListe.getSelectedItem() == null)
			throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
		retrait=(SygRetraitregistredao) lstListe.getSelectedItem().getValue();
		idretrait=retrait.getId();
		dtdateretrait.setValue(retrait.getDateRetrait());
		txtRaisonsocial.setValue(retrait.getNomSoumissionnaire());
		txtTelephone.setValue(retrait.getTelephone());
		txtFax.setValue(retrait.getFax());
		txtEmail.setValue(retrait.getEmail());
		dcMontant.setValue(retrait.getMontantverse());
		pays=retrait.getPays();
		bdPays.setValue(pays.getLibelle());
		if(retrait.getModepaiement().equals("0"))
			rdespece.setChecked(true);
		else if(retrait.getModepaiement().equals("1"))
			rdcheque.setChecked(true);
		else
			rdautres.setChecked(true);
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
	}
	

	

	private boolean checkFieldConstraints() {
		
		try {
		
			if(dtdateretrait.getValue()==null)
		     {
               errorComponent = dtdateretrait;
               errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.dateretrait")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((dtdateretrait.getValue()).before(dossier.getDosDatePublication()))
			 {
				errorComponent = dtdateretrait;
				errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.dateretrait")+" "+Labels.getLabel("kermel.referentiel.date.posterieure")+" "+Labels.getLabel("kermel.common.form.Datepublication")+": "+UtilVue.getInstance().formateLaDate(dossier.getDosDatePublication());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			if((dtdateretrait.getValue()).after(dossier.getDosDateOuvertueDesplis()))
			 {
				errorComponent = dtdateretrait;
				errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.dateretrait")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.dossiers.infos.dateouvertureplis")+": "+UtilVue.getInstance().formateLaDate(dossier.getDosDateOuvertueDesplis());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			if((dtdateretrait.getValue()).after(new Date() ))
			 {
				errorComponent = dtdateretrait;
				errorMsg =Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.dateretrait")+" "+Labels.getLabel("kermel.referentiel.date.inferieure")+" "+Labels.getLabel("kermel.referentiel.date.dujour")+": "+UtilVue.getInstance().formateLaDate(new Date());
				lbStatusBar.setStyle(ERROR_MSG_STYLE);
				lbStatusBar.setValue(errorMsg);
				throw new WrongValueException (errorComponent, errorMsg);
			  }
			if(txtRaisonsocial.getValue().equals(""))
		     {
              errorComponent = txtRaisonsocial;
              errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.raisonsocial")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(bdPays.getValue().equals(""))
		     {
             errorComponent = bdPays;
             errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.lectureoffres.pays")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtTelephone.getValue().equals(""))
		     {
             errorComponent = txtTelephone;
             errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.telephone")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if(txtEmail.getValue().equals(""))
		     {
             errorComponent = txtEmail;
             errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.email")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if (ToolKermel.ControlValidateEmail(txtEmail.getValue())) {
				
			} else {
				errorComponent = txtEmail;
                errorMsg = Labels.getLabel("kermel.referentiel.personne.email.incorrect");
 				lbStatusBar.setStyle(ERROR_MSG_STYLE);
 				lbStatusBar.setValue(errorMsg);
 				throw new WrongValueException(errorComponent,errorMsg);
			}
			if(dcMontant.getValue()==null)
		     {
            errorComponent = dcMontant;
            errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.montant")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			if((rdespece.isChecked()==false)&&(rdcheque.isChecked()==false)&&(rdautres.isChecked()==false))
		     {
           errorComponent = rdpodepaiement;
           errorMsg = Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.registreretrait.modepaiement")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
			return true;
			
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}
	public void  onClick$menuFermer(){
		effacerform();
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
	}
	public void  onClick$menuAjouter(){
		Events.postEvent(ApplicationEvents.ON_PAYS, this, null);
		step0.setVisible(false);
		step1.setVisible(true);
		step2.setVisible(false);
		
		
	}
	public void  effacerform(){
		dtdateretrait.setValue(null);
		txtRaisonsocial.setValue("");
		txtTelephone.setValue("");
		txtFax.setValue("");
		txtEmail.setValue("");
		idretrait=null;
		rdespece.setChecked(false);
		rdcheque.setChecked(false);
		rdautres.setChecked(false);
	}
	
	public void  onClick$menuValider(){
		if(checkFieldConstraints())
		{
			retrait.setDateRetrait(dtdateretrait.getValue());
			retrait.setNomSoumissionnaire(txtRaisonsocial.getValue());
			retrait.setTelephone(txtTelephone.getValue());
			retrait.setFax(txtFax.getValue());
			retrait.setEmail(txtEmail.getValue());
			retrait.setMontantverse(dcMontant.getValue());
			retrait.setDossier(dossier);
			retrait.setPays(pays);
			if(rdespece.isChecked()==true)
				retrait.setModepaiement("0");
			else  if(rdcheque.isChecked()==true)
			retrait.setModepaiement("1");
			else
			retrait.setModepaiement("2");
			
			if(idretrait==null)
				BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).save(retrait);
			else
				BeanLocator.defaultLookup(RegistrederetraitdaoSession.class).update(retrait);
			session.setAttribute("libelle", "registreretraitdao");
			loadApplicationState("aoo_deuxetapes");
			
		}
	}
	
///////////Pays///////// 
	public void onSelect$lstPays(){
		pays= (SygPays) lstPays.getSelectedItem().getValue();
	bdPays.setValue(pays.getLibelle());
	bdPays.close();
	
	}
	
	public class PaysRenderer implements ListitemRenderer{
	
	
	
	@Override
	public void render(Listitem item, Object data, int index)  throws Exception {
		SygPays pays = (SygPays) data;
		item.setValue(pays);
		
		Listcell cellLibelle = new Listcell(pays.getLibelle());
		cellLibelle.setParent(item);
		
		
		}
	}
	public void onFocus$txtRechercherPays(){
	if(txtRechercherPays.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.lectureoffres.pays"))){
		txtRechercherPays.setValue("");
	}		 
	}
	
	public void  onOK$btnRechercherPays(){
		onClick$btnRechercherPays();
	}
	public void  onOK$txtRechercherPays(){
		onClick$btnRechercherPays();
	}
	public void  onClick$btnRechercherPays(){
	if(txtRechercherPays.getValue().equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.ouvertureplis.lectureoffres.pays")) || txtRechercherPays.getValue().equals("")){
	libellepays = null;
	page=null;
	}else{
		libellepays = txtRechercherPays.getValue();
	page="0";
	}
	Events.postEvent(ApplicationEvents.ON_PAYS, this, page);
	}
	
	public void  onClick$menuFermerstep2(){
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
	}
	
	 public void onClick$menuEditer(){
		 if(retraits.size()>0)
		 {
			idIframe.setSrc(EDIT_URL + "?code="+dossier.getDosID()+ "&libelle=registreretraits");
			step0.setVisible(false);
			step1.setVisible(false);
			step2.setVisible(true);
		 }
		 else
		 {
			 throw new WrongValueException(menuEditer, Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.saisirregistreretrait")); 
		 }
	}
}