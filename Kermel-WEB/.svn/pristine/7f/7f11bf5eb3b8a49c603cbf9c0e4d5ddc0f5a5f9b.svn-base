package sn.ssi.kermel.web.plansdepassation.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabpanel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.plansdepassation.ejb.PlansdepassationSession;
import sn.ssi.kermel.be.plansdepassation.ejb.RealisationsSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class ListMisesAJourRealisationsController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code;
	private Tabpanel tabDetailsfecp;
	private Include incDetailsfecp;
	private Tab TAB_REALTRAV,TAB_REALSPI,TAB_REALSSERV,TAB_REALSFOURN,TAB_REALSDSP;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblAnnee,lblNumero;
	String login,status,type;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	List<SygRealisations> realisations = new ArrayList<SygRealisations>();
	private KermelSousMenu monSousMenu;
	private Menuitem  WZREAL_FERMER, WSOUM_PLAN, WVALID_PLAN, WWPUB_PLAN;
	    
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		String code = Executions.getCurrent().getParameter("code");
		if(code==null){
	  	monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REALSPLANS);
		monSousMenu.afterCompose();
			/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent


		if (WZREAL_FERMER != null) { WZREAL_FERMER.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_CLOSE); }
		if (WSOUM_PLAN != null) { WSOUM_PLAN.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_SOUMETTRE); }
		if (WVALID_PLAN != null) { WVALID_PLAN.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_VALIDATIONS); }
		if (WWPUB_PLAN != null) { WWPUB_PLAN.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_PUBLIER); }
		}
		addEventListener(ApplicationEvents.ON_CLOSE, this);
		addEventListener(ApplicationEvents.ON_SOUMETTRE, this);
		addEventListener(ApplicationEvents.ON_VALIDATIONS, this);
		addEventListener(ApplicationEvents.ON_PUBLIER, this);
	}

	
	public void onCreate(CreateEvent createEvent) {
		login = ((String) getHttpSession().getAttribute("user"));
		idplan=(Long) session.getAttribute("idplan");
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		type=infoscompte.getType();
		plan=BeanLocator.defaultLookup(PlansdepassationSession.class).findById(idplan);
		status=plan.getStatus();
		realisations=BeanLocator.defaultLookup(RealisationsSession.class).find(0,-1,null,null,plan, null);
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
		{
			autorite=infoscompte.getAutorite();	
		}
		else
		{
			autorite=null;	  
		}
		lblAnnee.setValue(plan.getAnnee()+"");
		lblNumero.setValue(plan.getNumplan());
		TAB_REALTRAV.setLabel(Labels.getLabel("kermel.plansdepassation.realisation.type.travaux")+" ("+BeanLocator.defaultLookup( RealisationsSession.class).nombrerealisationsMAJ(plan,UIConstants.PARAM_TMTRAVAUX,"U"+plan.getVersion())+")");
		TAB_REALSFOURN.setLabel(Labels.getLabel("kermel.plansdepassation.realisation.type.fournitures")+" ("+BeanLocator.defaultLookup( RealisationsSession.class).nombrerealisationsMAJ(plan,UIConstants.PARAM_TMFOURNITURES,"U"+plan.getVersion())+")");
		TAB_REALSPI.setLabel(Labels.getLabel("kermel.plansdepassation.realisation.type.pi")+" ("+BeanLocator.defaultLookup( RealisationsSession.class).nombrerealisationsMAJ(plan,UIConstants.PARAM_TMPI,"U"+plan.getVersion())+")");
		TAB_REALSSERV.setLabel(Labels.getLabel("kermel.plansdepassation.realisation.type.services")+" ("+BeanLocator.defaultLookup( RealisationsSession.class).nombrerealisationsMAJ(plan,UIConstants.PARAM_TMSERVICES,"U"+plan.getVersion())+")");
		TAB_REALSDSP.setLabel(Labels.getLabel("kermel.plansdepassation.realisation.type.dsp")+" ("+BeanLocator.defaultLookup( RealisationsSession.class).nombrerealisationsMAJ(plan,UIConstants.PARAM_TMDSP,"U"+plan.getVersion())+")");
			
		LibelleTab = (String) session.getAttribute("LibelleTab");
		    if(LibelleTab!=null)
		    {
		    	 if(LibelleTab.equals("REALTF"))
				  {
		    		 TAB_REALTRAV.setSelected(true);
					Include inc = (Include) this.getFellowIfAny("incREALTRAV");
					inc.setSrc("/plansdepassation/listmajrealisationstravaux.zul");		
				  }
				else
				{
					 if(LibelleTab.equals("REALPI"))
					  {
						 TAB_REALSPI.setSelected(true);
						Include inc = (Include) this.getFellowIfAny("incREALSPI");
						inc.setSrc("/plansdepassation/listmajrealisationspi.zul");	
					  }
					else
					{
						if(LibelleTab.equals("REALSERVICES"))
						  {
							TAB_REALSSERV.setSelected(true);
							Include inc = (Include) this.getFellowIfAny("incREALSSERV");
							inc.setSrc("/plansdepassation/listmajrealisationsserv.zul");	
						  }
						else
						{
							if(LibelleTab.equals("REALSFOURN"))
							  {
								TAB_REALSFOURN.setSelected(true);
								Include inc = (Include) this.getFellowIfAny("incREALSFOURN");
								inc.setSrc("/plansdepassation/listmajrealisationsfournitures.zul");	
							  }
							else
							{
								if(LibelleTab.equals("REALSDSP"))
								  {
									TAB_REALSDSP.setSelected(true);
									Include inc = (Include) this.getFellowIfAny("incREALSDSP");
									inc.setSrc("/plansdepassation/listmajrealisationsdsp.zul");	
								  }
								else
								{
									Include inc = (Include) this.getFellowIfAny("incREALTRAV");
									inc.setSrc("/plansdepassation/listmajrealisationstravaux.zul");	
								}
								
							}
						}
					}
				}
		    }
		    else
		    {
		    	 Include inc = (Include) this.getFellowIfAny("incREALTRAV");
		    	inc.setSrc("/plansdepassation/listmajrealisationstravaux.zul");	
		    }
		   
		   // Infos();
	}

	@Override
	public void onEvent(Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_SOUMETTRE)) {
			if(realisations.size()>0)
			{
				final String uri = "/plansdepassation/formsousmissionvalidation.zul";

				final HashMap<String, String> display = new HashMap<String, String>();
				display.put(DSP_HEIGHT,"650px");
				display.put(DSP_WIDTH, "80%");
				display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.soumettre.validation"));

				final HashMap<String, Object> data = new HashMap<String, Object>();
				data.put(SoumissionValidationFormController.PARAM_WINDOW_MODE, "realisations");
				data.put(SoumissionValidationFormController.PARAM_WINDOW_CODE, plan.getIDinfoplan());

				showPopupWindow(uri, data, display);
			}
			else
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.realisation.saisir"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_VALIDATIONS)) {
			final String uri = "/plansdepassation/formvalidation.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"650px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.validation"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(ValidationFormController.PARAM_WINDOW_MODE, "realisations_maj");
			data.put(ValidationFormController.PARAM_WINDOW_CODE, idplan);

			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_PUBLIER)) {
			final String uri = "/plansdepassation/formpublication.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"650px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.plansdepassation.proceduresmarches.prestationintellectuelle.publication"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(PublicationFormController.PARAM_WINDOW_MODE, "maj_plans_avalider");
			data.put(PublicationFormController.PARAM_WINDOW_CODE, idplan);

			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLOSE)) {
			if((type.equals(UIConstants.USERS_TYPES_DCMP)))
				 loadApplicationState("maj_plans_avalider");
			else
			{
				
				loadApplicationState("details_maj_plan");
			}
				
			
		}
	}

	
	public void Infos(){
		
		if((status.equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.saisie")))||(status.equalsIgnoreCase(Labels.getLabel("kermel.plansdepassation.statut.rejeter"))))
		{
			if(WSOUM_PLAN!=null)
			   WSOUM_PLAN.setDisabled(false);
			if(WVALID_PLAN!=null)
			WVALID_PLAN.setDisabled(true);
			if(WWPUB_PLAN!=null)
			WWPUB_PLAN.setDisabled(true);
		}
		else
		{
			if(status.equals(Labels.getLabel("kermel.plansdepassation.statut.miseenvalidation")))
			{
				if(WVALID_PLAN!=null)
				WVALID_PLAN.setDisabled(false);
				if(WWPUB_PLAN!=null)
				WWPUB_PLAN.setDisabled(true);
			}
			else
				
			{
				if(status.equals(Labels.getLabel("kermel.plansdepassation.statut.valider")))
				{
					   if(WVALID_PLAN!=null)
						WVALID_PLAN.setDisabled(true);
						if(WWPUB_PLAN!=null)
						WWPUB_PLAN.setDisabled(false);
				}
				else
				{
					if(status.equals(Labels.getLabel("kermel.plansdepassation.statut.publier")))
					{
						   if(WVALID_PLAN!=null)
							WVALID_PLAN.setDisabled(true);
							if(WWPUB_PLAN!=null)
							WWPUB_PLAN.setDisabled(true);
					}
				}
				
			}
			if(WSOUM_PLAN!=null)
			WSOUM_PLAN.setDisabled(true);
			
		}
	}
	
}