package sn.ssi.kermel.web.alertes.controllers;

import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.alertes.AlertesSession;
import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SysAlerte;
import sn.ssi.kermel.be.workflow.entity.SysState;
import sn.ssi.kermel.be.workflow.manager.ejb.WorkflowManagerSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;

@SuppressWarnings("serial")
public class AlertesFormController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_ALERTE_ID = "ALERTE_ID";

	private Listbox lstStates;
	@SuppressWarnings("unused")
	private Listbox lstMode;
	String mode;
	long alerteId;
	SysAlerte alerte;

	private Textbox txtDescription, txtCode, txtContenu, txtDelai;

	private void initUI() {
		if (mode.equalsIgnoreCase("EDIT")) {
			txtCode.setValue(alerte.getNom());
			txtCode.setDisabled(true);
			txtDescription.setValue(alerte.getDesc());
			txtContenu.setValue(alerte.getContenu());
			txtDelai.setValue(String.valueOf(alerte.getDelai()));
		}
	}

	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		lstStates.setItemRenderer(this);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);

	}

	@SuppressWarnings("unchecked")
	public void onCreate(final CreateEvent createEvent) {
		final Map windowParams = createEvent.getArg();
		mode = (String) windowParams.get(WINDOW_PARAM_MODE);
		if (mode.equalsIgnoreCase("EDIT")) {
			alerteId = (Long) windowParams.get(WINDOW_PARAM_ALERTE_ID);
			alerte = BeanLocator.defaultLookup(AlertesSession.class).findById(
					alerteId);
		}
		initUI();
	}

	@Override
	public void onEvent(final Event event) throws Exception {
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			final List<SysState> states = BeanLocator.defaultLookup(
					WorkflowManagerSession.class).findStates();
			final SimpleListModel listModel = new SimpleListModel(states);
			lstStates.setModel(listModel);
		}
	}

	public void onOK() {
		final SysAlerte alerte = new SysAlerte();
		alerte.setId(alerteId);
		alerte.setNom(txtCode.getValue());
		alerte.setDesc(txtDescription.getValue());
		alerte.setDelai(Integer.parseInt(txtDelai.getValue()));
		alerte.setContenu(txtContenu.getValue());

		final String staCode = lstStates.getSelectedItem().getValue()
				.toString();
		final SysState state = BeanLocator.defaultLookup(
				WorkflowManagerSession.class).findState(staCode);
		alerte.setSysState(state);

		BeanLocator.defaultLookup(AlertesSession.class).createAlerte(alerte);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, getOpener(), null);
		detach();
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		final SysState state = (SysState) data;
		item.setValue(state.getCode());
		if (alerte != null)
			if (state.getCode().equals(alerte.getSysState().getCode())) {
				item.setSelected(true);
			}
		final Listcell cellLibelle = new Listcell(state.getLibelle());
		cellLibelle.setParent(item);

	}

}