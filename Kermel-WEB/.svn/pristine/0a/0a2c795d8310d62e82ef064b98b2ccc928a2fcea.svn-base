package sn.ssi.kermel.web.referentiel.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Div;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygCategorieLegislation;
import sn.ssi.kermel.be.entity.SygFichierLegislation;
import sn.ssi.kermel.be.referentiel.ejb.FichierLegislationSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.UtilVue;


@SuppressWarnings("serial")
public class ListFichierLegislationController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null;
    private Listheader lshLibelle;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_FICHIER_LEGISLATION, MOD_FICHIER_LEGISLATION, SUP_FICHIER_LEGISLATION;
    private String login;
    
    private String extension,images;
	private Iframe idIframe;
	private String statut,imagestatut;
	private Long idrapport;
	private final String cheminDossier = UIConstants.PATH_ESPACELEGISLATION;
	 private Div step0, step1;
	 private SygFichierLegislation fiche;
	 private static final String CONFIRMPUBLIER = "CONFIRMPUBLIER";
	 private Label   titre;
	 
    
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code("FICHIER_LEGISLATION");
		monSousMenu.afterCompose();
		if(session.getAttribute("categorie")!=null)
		titre.setValue(((SygCategorieLegislation)session.getAttribute("categorie")).getLibelle());
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_FICHIER_LEGISLATION != null) { ADD_FICHIER_LEGISLATION.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_FICHIER_LEGISLATION != null) { MOD_FICHIER_LEGISLATION.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUP_FICHIER_LEGISLATION != null) { SUP_FICHIER_LEGISLATION.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		lshLibelle.setSortAscending(new FieldComparator("libelle", false));
		lshLibelle.setSortDescending(new FieldComparator("libelle", true));
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
    	login = ((String) getHttpSession().getAttribute("user"));
	}

	
	
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygFichierLegislation> types = BeanLocator.defaultLookup(FichierLegislationSession.class).findAll(activePage,byPage,(SygCategorieLegislation)session.getAttribute("categorie"),libelle);
			 SimpleListModel listModel = new SimpleListModel(types);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup( FichierLegislationSession.class).countALL((SygCategorieLegislation)session.getAttribute("categorie"),libelle));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/legislation/formLegislation.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.nouveau"));
			display.put(DSP_HEIGHT,"400px");
			display.put(DSP_WIDTH, "50%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(CodeMarcheFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);
    		showPopupWindow(uri, data, display);
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			
			String statut = (String)lstListe.getSelectedItem().getAttribute("statut");
			
			if(statut.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.rapport.statut.nopublier"))){
				
			final String uri = "/legislation/formLegislation.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"400px");
			display.put(DSP_WIDTH, "50%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(CodeMarcheFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(CodeMarcheFormController.PARAM_WINDOW_CODE, lstListe .getSelectedItem().getValue());
			
			showPopupWindow(uri, data, display);
			
			   }else{
					
				   Messagebox.show(Labels.getLabel("kermel.referentiel.rapport.statut.publier.controle.modif"), "Erreur", Messagebox.OK, Messagebox.ERROR);
			      }
			
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
				
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				
				String statut = (String)lstListe.getSelectedItem().getAttribute("statut");
				
				   if(statut.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.rapport.statut.nopublier"))){
					   HashMap<String, String> display = new HashMap<String, String>(); // permet
						display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
						display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
						display.put(MessageBoxController.DSP_HEIGHT, "250px");
						display.put(MessageBoxController.DSP_WIDTH, "47%");

						HashMap<String, Object> map = new HashMap<String, Object>(); // permet
						map.put(CURRENT_MODULE, "Suppression_Confirmer");
						showMessageBox(display, map);
				   }else{
					
					   Messagebox.show(Labels.getLabel("kermel.referentiel.rapport.statut.publier.controle"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				      }
				
				
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				String confirmer = (String) ((HashMap<String, Object>) event.getData()).get(CONFIRMPUBLIER);
				 if (confirmer != null && confirmer.equalsIgnoreCase("Publication_Confirmer")) 
				 {
					 
					  fiche.setStatut(Labels.getLabel("kermel.referentiel.rapport.statut.publier"));
						fiche.setDatepublication(new Date());
						BeanLocator.defaultLookup(FichierLegislationSession.class).update(fiche);
					
					 
				 }else{
					 
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				    Long codes = (Long)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue();
					BeanLocator.defaultLookup(FichierLegislationSession.class).delete(codes);
				   // BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_CODEMarche", Labels.getLabel("kermel.referentiel.common.rapport.suppression")+" :" + new Date(), login);
				
				    }
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
				   loadApplicationState("list_texte_reglementaire");
			}
			else	if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)){
				Listcell button = (Listcell) event.getTarget();
				String toDo = (String) button.getAttribute(UIConstants.TODO);
				idrapport = (Long) button.getAttribute("idrapport");
				fiche=BeanLocator.defaultLookup(FichierLegislationSession.class).findById(idrapport);
				
				if (toDo.equalsIgnoreCase("publier"))
				{
					
					HashMap<String, String> display = new HashMap<String, String>(); // permet
					 display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.plansdepassation.publier"));
					 display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.referentiel.rapport.publier.titre"));
					 display.put(MessageBoxController.DSP_HEIGHT, "150px");
					 display.put(MessageBoxController.DSP_WIDTH, "100px");
			        HashMap<String, Object> map = new HashMap<String, Object>(); // permet
			        map.put(CONFIRMPUBLIER, "Publication_Confirmer");
					 showMessageBox(display, map);
					
					
					
				}else if(toDo.equalsIgnoreCase("d�publier")){
					
					fiche.setStatut(Labels.getLabel("kermel.referentiel.rapport.statut.nopublier"));
					fiche.setDatepublication(null);
					BeanLocator.defaultLookup(FichierLegislationSession.class).update(fiche);
					
					 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
					
				}else if (toDo.equalsIgnoreCase("telecharger")){
					
					String nomfichier=(String) fiche.getFichier();
					step0.setVisible(false);
					step1.setVisible(true);
					String filepath = cheminDossier +  nomfichier;
					File f = new File(filepath.replaceAll("\\\\", "/"));

					org.zkoss.util.media.AMedia mymedia = null;
					try {
						mymedia = new AMedia(f, null, null);
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					if (mymedia != null)
						idIframe.setContent(mymedia);
					else
						idIframe.setSrc("");

					idIframe.setHeight("600px");
					idIframe.setWidth("100%");
					
				}
			}


	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygFichierLegislation rapportarmp = (SygFichierLegislation) data;
		item.setValue(rapportarmp.getId());
		 statut=rapportarmp.getStatut();
          item.setAttribute("statut", statut);
			 
		 Listcell cellLibelle = new Listcell(rapportarmp.getLibelle());
		 cellLibelle.setParent(item);
		 
		 Listcell cellDate = new Listcell(UtilVue.getInstance().formateLaDate(rapportarmp.getDate()));
		 cellDate.setParent(item);
		 
		 extension=rapportarmp.getFichier().substring(rapportarmp.getFichier().length()-3,  rapportarmp.getFichier().length());
		 if(extension.equalsIgnoreCase("pdf"))
			 images="/images/icone_pdf.png";
		 else  
			 images="/images/word.jpg";
			
		 Listcell image = new Listcell("");
		 image.setImage(images);
		 image.setAttribute(UIConstants.TODO, "telecharger");
		 image.setAttribute("idrapport", rapportarmp.getId());
		 image.setTooltiptext("Visualiser fichier");
		 image.addEventListener(Events.ON_CLICK, ListFichierLegislationController.this);
		 image.setParent(item);
		 
		 
		 //Listcell cellDatePublication = new Listcell(UtilVue.getInstance().formateLaDate(rapportarmp.getDatepublication()));
		 //cellDatePublication.setParent(item);
		 
		 Listcell cellDatePublication = new Listcell("");
			if (rapportarmp.getDatepublication()!=null){
				cellDatePublication.setLabel(UtilVue.getInstance().formateLaDate(rapportarmp.getDatepublication()));
			}
			cellDatePublication.setParent(item);
		 
		
		 Listcell cellImageStatut = new Listcell("");
		 
		 if(statut!=null&&statut.equalsIgnoreCase(Labels.getLabel("kermel.referentiel.rapport.statut.nopublier"))){
			 imagestatut="/images/deletemoins.png";
			 
			 cellImageStatut.setAttribute(UIConstants.TODO, "publier");
			 cellImageStatut.setTooltiptext("Publier Rapport");
		 }else{
			 imagestatut="/images/accept.png";
			 cellImageStatut.setAttribute(UIConstants.TODO, "d�publier");
			 cellImageStatut.setTooltiptext("D�publier Rapport");
		 }
		
		    cellImageStatut.setImage(imagestatut);
			cellImageStatut.setAttribute("idrapport", rapportarmp.getId());
			cellImageStatut.addEventListener(Events.ON_CLICK, ListFichierLegislationController.this);
			cellImageStatut.setParent(item);
		

	}
	public void onClick$bchercher()
	{
		
		if((txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.rapport.Libelle")))||(txtLibelle.getValue().equals("")))
		 {
			libelle=null;
			page=null;
		 }
		else
		{
			libelle=txtLibelle.getValue();
			page="0";
		}
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	
	public void onFocus$txtLibelle()
	{
		if(txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.rapport.Libelle")))
			txtLibelle.setValue("");
		
	}
	
	public void onBlur$txtLibelle()
	{
		if(txtLibelle.getValue().equals(""))
			txtLibelle.setValue(Labels.getLabel("kermel.referentiel.rapport.Libelle"));
	}

	public void onOK$txtLibelle()
	{
		onClick$bchercher();
	}
	
	
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
	
public void onClick$menuFermer2() {
		
		step0.setVisible(true);
		step1.setVisible(false);
	}
}