package sn.ssi.kermel.web.denonciations.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.FieldComparator;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.denonciation.ejb.DenonciationSession;
import sn.ssi.kermel.be.denonciation.ejb.PieceJointeDenonciationSession;
import sn.ssi.kermel.be.entity.SygDenonciation;
import sn.ssi.kermel.be.entity.SygPieceJointeDenonciation;
import sn.ssi.kermel.be.security.JournalSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;
import sn.ssi.kermel.web.core.utils.FileLoader;
import sn.ssi.kermel.web.core.utils.UtilVue;


@SuppressWarnings("serial")
public class PieceJointeController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstListe;
	private Paging pgPagination;
	private  int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE,activePage;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
    public Textbox txtLibelle;
    String libelle=null,page=null;
    private Listheader lshLibelle;
    Session session = getHttpSession();
    private KermelSousMenu monSousMenu;
    private Menuitem ADD_PJDENONCIATION, MOD_PJDENONCIATION, SUPP_PJDENONCIATION;
    private String login;
    private Div step0, step1,step2;

    //
    Long code = null;
	private SygDenonciation denonciation;
    
	
	private SygPieceJointeDenonciation pjointe = new SygPieceJointeDenonciation();
	private Textbox txtLibelles, txtVersionElectronique;
	private final String cheminDossier = UIConstants.PATH_PJCONT;
	private String nomFichier;
	UtilVue utilVue = UtilVue.getInstance();
	private Datebox dtdate;
	private Component errorComponent;
	private String errorMsg;
	 private Label lbStatusBar;
	 private static final String ERROR_MSG_STYLE = "color:red";
	 
	
		private Long idpiecejointe=null;
		
		private String extension,images;
		private Iframe idIframe;
	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.REF_PJDENONCIATION);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_PJDENONCIATION != null) { ADD_PJDENONCIATION.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_PJDENONCIATION != null) { MOD_PJDENONCIATION.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (SUPP_PJDENONCIATION != null) { SUPP_PJDENONCIATION.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		lshLibelle.setSortAscending(new FieldComparator("libelle", false));
		lshLibelle.setSortDescending(new FieldComparator("libelle", true));
		lstListe.setItemRenderer(this);
		pgPagination.setPageSize(byPage);
		pgPagination.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);
    	
    	login = ((String) getHttpSession().getAttribute("user"));
    	
    	///
    	
    	
	}
	
	
	@SuppressWarnings("unchecked")
	public void onCreate(CreateEvent event) {
		code = (Long) session.getAttribute("numdenonimation");
		session.setAttribute("numdenonimation", code);
		denonciation = BeanLocator.defaultLookup(DenonciationSession.class).findById(code);
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			if (event.getData() != null) {
				activePage = Integer.parseInt((String) event.getData());
				byPage = -1;
				pgPagination.setPageSize(1000);
			} else {
				byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
				activePage = pgPagination.getActivePage() * byPage;
				pgPagination.setPageSize(byPage);
			}
			 List<SygPieceJointeDenonciation> pjointes = BeanLocator.defaultLookup(PieceJointeDenonciationSession.class).findRech(activePage,byPage,code,libelle,null);
			 SimpleListModel listModel = new SimpleListModel(pjointes);
			 lstListe.setModel(listModel);
			pgPagination.setTotalSize(BeanLocator.defaultLookup( PieceJointeDenonciationSession.class).countRech(code,libelle,null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {

			step0.setVisible(false);
			step1.setVisible(true);
			
		}
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstListe.getSelectedItem() == null)
				
				throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
			pjointe = (SygPieceJointeDenonciation) lstListe.getSelectedItem().getValue();
			idpiecejointe=pjointe.getId();
			dtdate.setValue(pjointe.getDate());
			txtLibelles.setValue(pjointe.getLibelle());
			txtVersionElectronique.setValue(pjointe.getFichier());
			
			step0.setVisible(false);
			step1.setVisible(true);
		} 
		
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (lstListe.getSelectedItem() == null)
				
					throw new WrongValueException(lstListe, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				map.put(CURRENT_MODULE, lstListe.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < lstListe.getSelectedCount(); i++) {
				Long codes =( (SygPieceJointeDenonciation)((Listitem) lstListe.getSelectedItems().toArray()[i]).getValue()).getId();
				BeanLocator.defaultLookup(PieceJointeDenonciationSession.class).delete(codes);
				BeanLocator.defaultLookup(JournalSession.class).logAction("SUPP_PJCONT", Labels.getLabel("kermel.referentiel.common.decision.suppression")+" :" + new Date(), login);
				
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
			else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_CLICK)) {
				Listcell button = (Listcell) event.getTarget();
				String nomfichier=(String) button.getAttribute("fichier");
				step0.setVisible(false);
				step1.setVisible(false);
				step2.setVisible(true);
				String filepath = cheminDossier +  nomfichier;
				File f = new File(filepath.replaceAll("\\\\", "/"));

				org.zkoss.util.media.AMedia mymedia = null;
				try {
					mymedia = new AMedia(f, null, null);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				if (mymedia != null)
					idIframe.setContent(mymedia);
				else
					idIframe.setSrc("");

				idIframe.setHeight("600px");
				idIframe.setWidth("100%");
			}
		
	}

	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		SygPieceJointeDenonciation pjointe = (SygPieceJointeDenonciation) data;
		item.setValue(pjointe);

			 
		 Listcell cellLibelle = new Listcell(pjointe.getLibelle());
		 cellLibelle.setParent(item);
		 
		 Listcell cellDate = new Listcell("");
		 if(pjointe.getDate()!=null)
			 cellDate.setLabel(UtilVue.getInstance().formateLaDate(pjointe.getDate()));
		 cellDate.setParent(item);
		 
		 extension=pjointe.getFichier().substring(pjointe.getFichier().length()-3,  pjointe.getFichier().length());
		 if(extension.equalsIgnoreCase("pdf"))
			 images="/images/icone_pdf.png";
		 else  
			 images="/images/word.jpg";
			
		 Listcell image = new Listcell("");
		 image.setImage(images);
		 image.setAttribute("fichier", pjointe.getFichier());
		 image.setTooltiptext("Visualiser fichier");
		 image.addEventListener(Events.ON_CLICK, PieceJointeController.this);
		 image.setParent(item);

	}
	public void onClick$bchercher()
	{
		
		if((txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.pj.Libelle")))||(txtLibelle.getValue().equals("")))
		 {
			libelle=null;
			
		 }
		else
		{
			libelle=txtLibelle.getValue();
			page="0";
		}
		
		
		 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, page);
	}
	
	
	
	public void onFocus$txtLibelle()
	{
		if(txtLibelle.getValue().equalsIgnoreCase(Labels.getLabel("kermel.contentieux.pj.Libelle")))
			txtLibelle.setValue("");
		
	}
	
	public void onBlur$txtLibelle()
	{
		if(txtLibelle.getValue().equals(""))
			txtLibelle.setValue(Labels.getLabel("kermel.contentieux.pj.Libelle"));
	}

	public void onOK$txtLibelle()
	{
		onClick$bchercher();
	}
	
	
	
	public void onOK$bchercher()
	{
		onClick$bchercher();
	}
	
	
	///
	


	public void onClick$btnChoixFichier() {

		if (ToolKermel.isWindows())
			nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("/", "\\\\"));
		else
			nomFichier = FileLoader.uploadPieceDossier(utilVue.formateLaDate3(Calendar.getInstance().getTime()), "Pj", cheminDossier.replaceAll("\\\\", "/"));

		txtVersionElectronique.setValue(nomFichier);
	}
	
	
private boolean checkFieldConstraints() {
		
		try {
		
			if(dtdate.getValue()==null)
		     {
               errorComponent = dtdate;
               errorMsg = Labels.getLabel("kermel.contentieux.pj.Date")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
//		
			if(txtVersionElectronique.getValue().equals(""))
		     {
          errorComponent = txtVersionElectronique;
          errorMsg = Labels.getLabel("kermel.contentieux.pj.Fichier")+": "+Labels.getLabel("kermel.erreur.champobligatoire");
			   lbStatusBar.setStyle(ERROR_MSG_STYLE);
			   lbStatusBar.setValue(errorMsg);
			  throw new WrongValueException (errorComponent, errorMsg);
		     }
		
			return true;
				
		}
		catch (Exception e) {
			errorMsg = Labels.getLabel("kermel.erreur.erreurinconnue") + ": " + e.toString()
			+ " [checkFieldConstraints]";
			errorComponent = null;
			return false;

			
		}
		
	}

	public void onOK() {
		
		if(checkFieldConstraints())
		{
			pjointe.setLibelle(txtLibelles.getValue());
			pjointe.setFichier(txtVersionElectronique.getValue());
			pjointe.setDate(dtdate.getValue());

			pjointe.setDenonciation(denonciation);

			if (idpiecejointe==null) 
				BeanLocator.defaultLookup(PieceJointeDenonciationSession.class).save(pjointe);
            else 
            BeanLocator.defaultLookup(PieceJointeDenonciationSession.class).update(pjointe);
			
		
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			step0.setVisible(true);
			step1.setVisible(false);
			effacer();
		} 
	}

	public void effacer(){
		idpiecejointe=null;
		txtLibelles.setValue("");
		txtVersionElectronique.setValue("");
		dtdate.setValue(null);
	}
	
	public void onClick$menuFermer() {
		
		step0.setVisible(true);
		step1.setVisible(false);
	}
	
public void onClick$menuFermer2() {
		
		step0.setVisible(true);
		step1.setVisible(false);
		step2.setVisible(false);
	}

}

