package sn.ssi.kermel.web.prestationintellectuelle.controllers;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Tab;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDevise;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierspieces;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.passationsmarches.ejb.AppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DeviseSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersAppelsOffresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersPiecesSession;
import sn.ssi.kermel.be.passationsmarches.ejb.DossiersSouscriteresSession;
import sn.ssi.kermel.be.passationsmarches.ejb.RegistrededepotSession;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;



/**

 * @author Adama samb
 * @since mercredi 12 Janvier 2011, 09:00
 
 */
public class TraitementsDossiersDPController extends AbstractWindow implements
		AfterCompose, EventListener {

	private static final long serialVersionUID = -2016441067149357242L;
	public static final String WINDOW_PARAM_MODE = "MODE";
	public static final String WINDOW_PARAM_CODE = "CODE";
	private String code,avalider="oui";
	private Tab TAB_infosgenerales,TAB_garanties,TAB_piecesadministratives,TAB_criteresqualifications,TAB_devise,TAB_financement;
	private String LibelleTab;
	Session session = getHttpSession();
    private Long idplan,idrealisation;
	private Label lblInfos;
	String login;
	SygPlansdepassation plan=new SygPlansdepassation();
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;
	SygAppelsOffres appel=new SygAppelsOffres();
	private Long idappel;
	SygDossiers dossier=new SygDossiers();
	List<SygDossierssouscriteres> criteres = new ArrayList<SygDossierssouscriteres>();
	List<SygDevise> devises = new ArrayList<SygDevise>();
	List<SygDossierspieces> piecesadministratives = new ArrayList<SygDossierspieces>();
	List<SygPlisouvertures> plis = new ArrayList<SygPlisouvertures>();

	
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		
	

	}

	
	public void onCreate(CreateEvent createEvent) {
		idappel=(Long) session.getAttribute("idappel");
		appel=BeanLocator.defaultLookup(AppelsOffresSession.class).findById(idappel);
		autorite=appel.getAutorite();
		dossier=BeanLocator.defaultLookup(DossiersAppelsOffresSession.class).Dossier(null, appel,2);
		if(dossier!=null)
		{
			devises = BeanLocator.defaultLookup(DeviseSession.class).find(0,-1,dossier);
			piecesadministratives= BeanLocator.defaultLookup(DossiersPiecesSession.class).find(0,-1,dossier);
			criteres = BeanLocator.defaultLookup(DossiersSouscriteresSession.class).find(0,-1,dossier, null, null);
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		}
			
	
	}

	@Override
	public void onEvent(Event event) throws Exception {
	
	}

	
	
	public void onClick$demandeproposition() {
		session.setAttribute("libelle", "infogeneralesdp");
		loadApplicationState("procedure_pi");
	}
	
	public void onClick$miseenvalidation() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		}
		else
		{
			if(devises.size()==0)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirdevises"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			}
			else
			{
				if(piecesadministratives.size()==0)
				{
					 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisirpieces"), "Erreur", Messagebox.OK, Messagebox.ERROR);
						
				}
				else
				{
					if(criteres.size()==0)
					{
						 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.validationdossier.saisircriteres"), "Erreur", Messagebox.OK, Messagebox.ERROR);
							
					}
					else
					{
						session.setAttribute("libelle", "validationdossierdp");
						loadApplicationState("procedure_pi");
					}
				}
			}
			
			
		}
		
	}
	
	public void onClick$registredepot() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		}
		else
		{
			if(dossier.getDosDateValidation()==null)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.validerdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
			}
			else
			{
				session.setAttribute("libelle", "plisouvertures");
				loadApplicationState("procedure_pi");
			}
		}
		
	}
	
	public void onClick$ouvertureplis() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
		}
		else
		{
			plis = BeanLocator.defaultLookup(RegistrededepotSession.class).find(0,-1,dossier,null,null,null,-1,-1,-1, -1, -1,-1, null, -1, null, null);
			if(plis.size()==0)
			{
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.ouvertureplis.saisirregistredepot"), "Erreur", Messagebox.OK, Messagebox.ERROR);
			}
			else
			{
				session.setAttribute("libelle", "ouvertureplis");
				loadApplicationState("procedure_pi");
			}
		}
		
	}
	public void onClick$evaluationattribution() {
		session.setAttribute("libelle", "evaluationattribution");
		loadApplicationState("procedure_pi");
	}
	public void onClick$signaturemarche() {
		session.setAttribute("libelle", "souscriptiondumarche");
		loadApplicationState("procedure_pi");
	}
	
	public void onClick$approbationmarche() {
		session.setAttribute("libelle", "approbationmarche");
		loadApplicationState("procedure_pi");
	}
	public void onClick$notificationmarche() {
		session.setAttribute("libelle", "notificationmarche");
		loadApplicationState("procedure_pi");
	}
	public void onClick$pubattribdefinitive() {
		session.setAttribute("libelle", "publicationattributiondefinitive");
		loadApplicationState("procedure_pi");
	}
	public void onClick$pubattriprovisoire() {
		session.setAttribute("libelle", "publicationattributionprovisoire");
		loadApplicationState("procedure_pi");
	}
	public void onClick$mevalidation() {
		session.setAttribute("libelle", "soumissionpourvalidationattributionprovisoire");
		loadApplicationState("procedure_pi");
	}
	public void onClick$attribution() {
		session.setAttribute("libelle", "attributionprovisoire");
		loadApplicationState("procedure_pi");
	}
	public void onClick$resultatnegociation() {
		session.setAttribute("libelle", "resultatnegociation");
		loadApplicationState("procedure_pi");
	}
	public void onClick$ouvertureoffrefinancieres() {
		session.setAttribute("libelle", "ouvertureoffrefinancieres");
		loadApplicationState("procedure_pi");
	}
	
	public void onClick$immatriculation() throws InterruptedException {
		if(dossier==null)
		{
			 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.saisirdossier"), "Erreur", Messagebox.OK, Messagebox.ERROR);
				
		}
		else
		{
			 if(dossier.getDosDatePublicationDefinitive()==null)
			 {
				 Messagebox.show(Labels.getLabel("kermel.plansdepassation.proceduresmarches.preparation.publierattrribution"), "Erreur", Messagebox.OK, Messagebox.ERROR);
					
			 }
			 else
			 {
		        session.setAttribute("libelle", "immatriculation");
		        loadApplicationState("procedure_pi");
			 }
		}
	}
}