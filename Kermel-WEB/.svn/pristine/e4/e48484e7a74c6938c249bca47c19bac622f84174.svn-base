package sn.ssi.kermel.web.securite.controleaccess.utilisateurs.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.security.UtilisateurSession;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;

@SuppressWarnings("serial")
public class ListUtilisateursController extends AbstractWindow implements EventListener, AfterCompose, ListitemRenderer {

	private Listbox lstUtilisateurs;
	private Paging pgUtilisateurs;

	private KermelSousMenu monSousMenu;
	private Menuitem ADD_UTILISATEUR, MOD_UTILISATEUR, DEL_UTILISATEUR;/*
																			 * ,
																			 * HLP_TDG
																			 */;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_UTILISATEUR = "CURRENT_UTILISATEUR";
	private String user,typeuser;
	private Utilisateur infoscompte;
	private SygAutoriteContractante autorite=null;

	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);

		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.GESTION_UTILISATEURARMP);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		// reste indispensable
		/* reprise des forwards definis dans le .zul */
		if (ADD_UTILISATEUR != null) { ADD_UTILISATEUR.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD); }
		if (MOD_UTILISATEUR != null) { MOD_UTILISATEUR.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT); }
		if (DEL_UTILISATEUR != null) { DEL_UTILISATEUR.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE); }
		
			
		// if (HELP_USER != null)
		// HELP_USER.addForward(ApplicationEvents.ON_CLICK, this,
		// ApplicationEvents.ON_);
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_DETAILS, this);
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
		addEventListener(ApplicationEvents.ON_ADDUSERDNCMP, this);
		addEventListener(ApplicationEvents.ON_EDITUSERDNCMP, this);
		
		addEventListener(ApplicationEvents.ON_ADDUSERAC, this);
		addEventListener(ApplicationEvents.ON_EDITUSERAC, this);


		lstUtilisateurs.setItemRenderer(this);

		pgUtilisateurs.setPageSize(byPage);

		pgUtilisateurs.addForward("onPaging", this, ApplicationEvents.ON_MODEL_CHANGE);

		
	}

	@SuppressWarnings("unchecked")
	public void onCreate(final CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		typeuser=UIConstants.USERS_TYPES_ARMP;
		user=null;
		if(infoscompte.getType().equals(UIConstants.USERS_TYPES_AC))
			autorite=infoscompte.getAutorite();
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			final List<Utilisateur> Utilisateurs = BeanLocator.defaultLookup(UtilisateurSession.class).findUtilisateurs(pgUtilisateurs.getActivePage() * byPage, byPage, typeuser, autorite,user);
			final SimpleListModel listModel = new SimpleListModel(Utilisateurs);
			lstUtilisateurs.setModel(listModel);
			pgUtilisateurs.setTotalSize(BeanLocator.defaultLookup(UtilisateurSession.class).countAllUtilisateurs(typeuser, autorite,user));
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/securite/controleaccess/utilisateurs/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_HEIGHT, "650px");
			display.put(DSP_TITLE, Labels.getLabel("kermel.securite.users.nouveau"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(UtilisateurFormController.WINDOW_PARAM_MODE, UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (lstUtilisateurs.getSelectedItem() == null)
				throw new WrongValueException(MOD_UTILISATEUR, Labels.getLabel("kermel.error.select.item"));
			
			final String uri = "/securite/controleaccess/utilisateurs/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_HEIGHT, "650px");
			display.put(DSP_TITLE, Labels.getLabel("kermel.securite.users.modifier"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(UtilisateurFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(UtilisateurFormController.WINDOW_PARAM_USER_ID, lstUtilisateurs.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
	
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)) {
			if (lstUtilisateurs.getSelectedItem() == null)
				throw new WrongValueException(DEL_UTILISATEUR, Labels.getLabel("kermel.error.select.item"));
			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
			display.put(MessageBoxController.DSP_TITLE,Labels.getLabel("kermel.common.form.supprimer"));
			final HashMap<String, Object> data = new HashMap<String, Object>();      
			data.put(CURRENT_UTILISATEUR, lstUtilisateurs.getSelectedItem().getValue());

			showMessageBox(display, data);
		} else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)) {
			// int id ;
			final Long id = (Long) ((HashMap<String, Object>) event.getData()).get(CURRENT_UTILISATEUR);
			BeanLocator.defaultLookup(UtilisateurSession.class).deleteUtilisateur(id);
			Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
		} else if (event.getName().equalsIgnoreCase(Events.ON_DOUBLE_CLICK)) {
			final Listitem listitem = (Listitem) event.getTarget();
			final String uri = "/securite/controleaccess/utilisateurs/form.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_HEIGHT, "600px");
			display.put(DSP_TITLE,Labels.getLabel("kermel.securite.users.modifier"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(UtilisateurFormController.WINDOW_PARAM_MODE, UIConstants.MODE_EDIT);
			data.put(UtilisateurFormController.WINDOW_PARAM_USER_ID, listitem.getValue());

			showPopupWindow(uri, data, display);
		}

	}
	
	@Override
	public void render(final Listitem item, final Object data, int index) throws Exception {
		final Utilisateur utilisateur = (Utilisateur) data;
		item.setValue(utilisateur.getId());
		item.setAttribute("type", utilisateur.getType());
		item.addEventListener(Events.ON_DOUBLE_CLICK, this);

		final Listcell cellLogin = new Listcell(utilisateur.getLogin());
		cellLogin.setParent(item);
		Listcell cellProfil = new Listcell("");
		if (utilisateur.getSysProfil().getPfLibelle() != null) {
			cellProfil = new Listcell(utilisateur.getSysProfil().getPfLibelle());
		}
		cellProfil.setParent(item);

		Listcell cellPrenom = new Listcell("");
		if (utilisateur.getPrenom() != null) {
			cellPrenom = new Listcell(utilisateur.getPrenom());
		}
		cellPrenom.setParent(item);

		Listcell cellNom = new Listcell();
		if (utilisateur.getNom() != null) {
			cellNom = new Listcell(utilisateur.getNom());
		}
		cellNom.setParent(item);
		
		 Listcell cellImage = new Listcell();
		 if (utilisateur.getActif()==true) 
		 {
			 cellImage.setImage("/images/tick.png");
			 cellImage.setTooltiptext(Labels.getLabel("kermel.utilisateur.etat.actif")); 
		 }
		 else
		 {
			 cellImage.setImage("/images/publish_x.png");
			 cellImage.setTooltiptext(Labels.getLabel("kermel.utilisateur.etat.nonactif")); 
		 }
		
		 cellImage.setParent(item);
		
		
	}

}