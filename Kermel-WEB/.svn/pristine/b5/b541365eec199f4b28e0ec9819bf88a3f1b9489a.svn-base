package sn.ssi.kermel.web.fichdossiertype.controllers;

import java.util.HashMap;
import java.util.List;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.ext.AfterCompose;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Paging;
import org.zkoss.zul.SimpleListModel;
import org.zkoss.zul.Textbox;

import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.dossiertype.ejb.FichDossierTypeSession;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygFichDossierType;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.web.common.components.KermelSousMenu;
import sn.ssi.kermel.web.common.constants.ApplicationEvents;
import sn.ssi.kermel.web.common.constants.UIConstants;
import sn.ssi.kermel.web.common.controllers.AbstractWindow;
import sn.ssi.kermel.web.common.controllers.MessageBoxController;



@SuppressWarnings("serial")
public class ListFichDossierTypeGeneralController extends AbstractWindow implements
		EventListener, AfterCompose, ListitemRenderer {

	private Listbox list;
	private Paging pgfiche;
	private final int byPage = UIConstants.DSP_GRID_ROWS_BY_PAGE;
	public static final String CURRENT_MODULE="CURRENT_MODULE";
	private String sygmlibelletype;
	private Listheader libelledossiers,Typedossier;
	private Textbox txtlibelletype,txtTypedossier;
	Long code;
	public String extension,images;
	private Menuitem ADD_FICHEDOSSIER, MOD_FICHEDOSSIER, SUPP_FICHEDOSSIER,WPUBLIER_FICHEDOSSIER,WDEPUBLIER_FICHEDOSSIER;
	 private KermelSousMenu monSousMenu;
	 private Integer Publier;
	 private Utilisateur infoscompte;
		private SygAutoriteContractante autorite=null;
	/**
	 * Permet d'intervenir juste apr�s la composition de l'arbre des composants
	 * pour effectuer certaines initialisations.
	 */
	@Override
	public void afterCompose() {
		Components.wireFellows(this, this);
		Components.addForwards(this, this);
		monSousMenu.setProfil((String) getHttpSession().getAttribute("profil"));
		monSousMenu.setFea_code(UIConstants.FICHEDOSSIER);
		monSousMenu.afterCompose();
		/* pour les menuitem eventuellement crees */
		Components.wireFellows(this, this); // mais le wireFellows precedent
		Components.wireFellows(this, this);
		if (ADD_FICHEDOSSIER != null)
			ADD_FICHEDOSSIER.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_ADD);
		if (MOD_FICHEDOSSIER != null)
			MOD_FICHEDOSSIER.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_EDIT);
		if (SUPP_FICHEDOSSIER != null)
			SUPP_FICHEDOSSIER.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DELETE);
		if (WPUBLIER_FICHEDOSSIER != null)
			WPUBLIER_FICHEDOSSIER.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_PUBLIER);
		
		if (WDEPUBLIER_FICHEDOSSIER != null){
			WDEPUBLIER_FICHEDOSSIER.addForward(ApplicationEvents.ON_CLICK, this, ApplicationEvents.ON_DEPUBLIER);
			WDEPUBLIER_FICHEDOSSIER.setVisible(false);
		}
		
		addEventListener(ApplicationEvents.ON_MODEL_CHANGE, this);
		addEventListener(ApplicationEvents.ON_ADD, this);
		addEventListener(ApplicationEvents.ON_EDIT, this);
		addEventListener(ApplicationEvents.ON_DELETE, this);
		addEventListener(ApplicationEvents.ON_PUBLIER, this);
		addEventListener(ApplicationEvents.ON_DEPUBLIER, this);
	
		addEventListener(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM, this);
		
 
		list.setItemRenderer(this);

		pgfiche.setPageSize(byPage);
		/*
		 * Apr�s une pagination, le mod�le de liste est mis � jour.
		 */
		pgfiche.addForward("onPaging", this,
				ApplicationEvents.ON_MODEL_CHANGE);

		
	}

	public void onCreate(CreateEvent createEvent) {
		infoscompte=(Utilisateur) getHttpSession().getAttribute("infoscompte");
		if(infoscompte.getAutorite()!=null)
			autorite=infoscompte.getAutorite();
		else
			autorite=null;
		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	/**
	 * Permet de g�rer les �v�nements survenus sur la page correspondante.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(final Event event) throws Exception {

		/*
		 * Lorsque cet �v�nement survient, les �l�ments de la liste sont mis �
		 * jour de m�me que l'�l�ment de pagination
		 */
		
		if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_MODEL_CHANGE)) {
			 List<SygFichDossierType> fichdossiertype = BeanLocator.defaultLookup(FichDossierTypeSession.class).find(pgfiche.getActivePage()*byPage,byPage,null,null,sygmlibelletype,null);
			 SimpleListModel listModel = new SimpleListModel(fichdossiertype);
			list.setModel(listModel);
			pgfiche.setTotalSize(BeanLocator.defaultLookup(FichDossierTypeSession.class).count(null,null,sygmlibelletype,null));
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_ADD)) {
			final String uri = "/fichdossiertype/formfichdossiertype.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.FichDossierType"));
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FichDossierTypeFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_NEW);

			showPopupWindow(uri, data, display);
		} 
		else if (event.getName().equalsIgnoreCase(ApplicationEvents.ON_EDIT)) {
			if (list.getSelectedItem() == null) {
				alert("");
				return;
			}
			final String uri = "/fichdossiertype/formfichdossiertype.zul";

			final HashMap<String, String> display = new HashMap<String, String>();
			display.put(DSP_HEIGHT,"500px");
			display.put(DSP_WIDTH, "80%");
			display.put(DSP_TITLE, Labels.getLabel("kermel.common.form.editer"));

			final HashMap<String, Object> data = new HashMap<String, Object>();
			data.put(FichDossierTypeFormController.WINDOW_PARAM_MODE,
					UIConstants.MODE_EDIT);
			data.put(FichDossierTypeFormController.PARAM_WIDOW_CODE, list
					.getSelectedItem().getValue());

			showPopupWindow(uri, data, display);
		} 
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DELETE)){
				if (list.getSelectedItem() == null)
				
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				

				HashMap<String, String> display = new HashMap<String, String>(); // permet
				// de
				// fixer
				// les
				// dimenisons
				// du
				// popup
				display.put(MessageBoxController.DSP_MESSAGE,  Labels.getLabel("kermel.common.form.question.supprimer"));
				display.put(MessageBoxController.DSP_TITLE, Labels.getLabel("kermel.common.form.supprimer"));
				display.put(MessageBoxController.DSP_HEIGHT, "250px");
				display.put(MessageBoxController.DSP_WIDTH, "47%");

				HashMap<String, Object> map = new HashMap<String, Object>(); // permet
				// de
				// passer
				// des
				// parametres
				// au
				// popup
				map.put(CURRENT_MODULE, list.getSelectedItem().getValue());
				showMessageBox(display, map);
				
				
			}else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_MESSAGE_BOX_CONFIRM)){
				
				for (int i = 0; i < list.getSelectedCount(); i++) {
					Integer codes = (Integer)((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					System.out.println(codes);
				BeanLocator.defaultLookup(FichDossierTypeSession.class).delete(codes);
				}
				Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_PUBLIER)){
				if (list.getSelectedItem()==null) 
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				 for (int i = 0; i < list.getSelectedCount(); i++) {
					 Long code=(Long) ((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					 SygFichDossierType	fichdossiertype= BeanLocator.defaultLookup(FichDossierTypeSession.class).findById(code);
					 fichdossiertype.setPublier("non");
					 //fichdossiertype.setDatepublication(new Date());
			      	   BeanLocator.defaultLookup(FichDossierTypeSession.class).update(fichdossiertype);
			      	   
				 }
				 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
			else if(event.getName().equalsIgnoreCase(ApplicationEvents.ON_DEPUBLIER)){
				if (list.getSelectedItem()==null) 
					throw new WrongValueException(list, Labels.getLabel("kermel.error.select.item"));
				 for (int i = 0; i < list.getSelectedCount(); i++) {
					 Long code=(Long) ((Listitem) list.getSelectedItems().toArray()[i]).getValue();
					 SygFichDossierType	fichdossiertype= BeanLocator.defaultLookup(FichDossierTypeSession.class).findById(code);
					 fichdossiertype.setPublier("yes");
					// fichdossiertype.setDatepublication(null);
			      	   BeanLocator.defaultLookup(FichDossierTypeSession.class).update(fichdossiertype);
			      	   
				 }
				 Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
			}
		
	}
	/**
	 * D�finit comment un �l�ment de la liste est affich�.
	 */
	@Override
	public void render( Listitem item,  Object data, int index) throws Exception {
		SygFichDossierType fichdossiertype = (SygFichDossierType) data;
		item.setValue(fichdossiertype.getID());
		item.setAttribute("Publier", fichdossiertype.getPublier());
		Listcell cellLibellefichierdossiertype = new Listcell(fichdossiertype.getLibellefichierdossiertype());
		cellLibellefichierdossiertype.setParent(item);
//		Listcell cellannee = new Listcell(fichdossiertype.get);
//		cellannee.setParent(item);
//		Listcell celldatepublication = new Listcell("");
//		if(avi.getDatepublication()!=null)
//		celldatepublication.setLabel(UtilVue.getInstance().formateLaDate2(avi.getDatepublication()));
//		celldatepublication.setParent(item);
		
		
		
		  Listcell cellimage = new Listcell();
			
			if (fichdossiertype.getNomfichierdossiertype()!=null)  
			extension=fichdossiertype.getNomfichierdossiertype().substring(fichdossiertype.getNomfichierdossiertype().length()-3,  fichdossiertype.getNomfichierdossiertype().length());
			else extension="";
			 if(extension.equalsIgnoreCase("pdf"))
				 images="/images/icone_pdf.png";
			 else  
				 images="/images/word.jpg";

			 cellimage.setImage(images);
			 cellimage.setParent(item);
//		Listcell cellfichier_avis= new Listcell(avi.getFichier_Avis());
//		cellfichier_avis.setParent(item);
		 
	}
	
	public void onOK$txtnumero() {
		onClick$btnRechercher();
	}
	public void onClick$btnRechercher() {
		
		
		if (txtlibelletype.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.libelletype")) || txtlibelletype.getValue().equals("")) {
			sygmlibelletype = null;
		} else {
			sygmlibelletype  = txtlibelletype.getValue();
		}	

		Events.postEvent(ApplicationEvents.ON_MODEL_CHANGE, this, null);
	}
	public void onFocus$txtnumero() {
				if (txtlibelletype.getValue().equalsIgnoreCase(Labels.getLabel("kermel.referentiel.common.libelletype"))) {
					txtlibelletype.setValue("");

				}
		}
				public void onBlur$txtlibelletype() {
					if (txtlibelletype.getValue().equalsIgnoreCase("")) {
						txtlibelletype.setValue(Labels.getLabel("kermel.referentiel.common.libelletype"));
					}	
			}
				public void onSelect$list(){
					Publier=Integer.parseInt(list.getSelectedItem().getAttribute("Publier").toString());
					if(Publier.equals ("non"))
						
					{
						SUPP_FICHEDOSSIER.setVisible(true);
						WPUBLIER_FICHEDOSSIER.setVisible(true);
						WDEPUBLIER_FICHEDOSSIER.setVisible(false);
					}
					else 
					{
						SUPP_FICHEDOSSIER.setVisible(false);
						WPUBLIER_FICHEDOSSIER.setVisible(false);
						WDEPUBLIER_FICHEDOSSIER.setVisible(true);
					}
					}
					
			  
}
