Ext.ns('Ext.ux');

Ext.ux.TrainingHud = {
  targetEl: "training-list",
  baseUrl: "/products/_training_callout/",
  productId: 0,
  success: function(response, options) {
    console.log(response);
    if(response.status == 200) {
      Ext.Element.get(this.targetEl).insertHtml("afterBegin", response.responseText);
    }
  },
  failure: function(response, options) {
    // stub
  },
  request: function() {
    if(Ext.Element.get(this.targetEl)) {
      Ext.Ajax.request({
        url: this.baseUrl + this.productId,
        success: this.success,
        failure: this.failure,
        scope: this
      });
    }
  }
};