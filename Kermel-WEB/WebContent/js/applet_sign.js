var HOSTNAME = "http://localhost:8080/PkermelV2";
var HOSTNAME_HTTPS = "https://localhost:8443/PkermelV2";
/** ***************** debut LP7********************* */
var URL_JNLP_LP7WEB = HOSTNAME + "/signature/applet/jnlp/LP7Web.jnlp";
var context;
var file;
var step = 0;
var mode = "";

var lastFilepathAdded;
var lastFilename;

function startAppletLoader(filePath, fileName) {
	lastFilepathAdded = filePath;
	lastFilename = fileName;
	$('input[name=save_type]').change(function() {
		if ($('input[name=save_type]:checked').val() == 'local') {
			$('#localSaveDiv').show();
		} else {
			$('#localSaveDiv').hide();
		}
	});

	$('#sppActivated')
			.change(
					function() {
						$('#zip_code').attr("disabled",
								!$('#sppActivated').is(':checked'));
						$('#city').attr("disabled",
								!$('#sppActivated').is(':checked'));
						$('#state_or_province').attr("disabled",
								!$('#sppActivated').is(':checked'));
						$('#country').attr("disabled",
								!$('#sppActivated').is(':checked'));
					});

	$('#cti_activated').change(
			function() {
				$('#commitment_type').attr("disabled",
						!$('#cti_activated').is(':checked'));
			});

	$('#signing_time_activated').change(
			function() {
				$('#signing_time_local').attr("disabled",
						!$('#signing_time_activated').is(':checked'));
				$('#custom_time').attr(
						"disabled",
						(!$('#signing_time_activated').is(':checked') || $(
								'#signing_time_local').is(':checked')));
			});

	$('#signing_time_local').change(
			function() {
				$('#custom_time').attr("disabled",
						$('#signing_time_local').is(':checked'));
			});

	$('#timestamp_activated').change(
			function() {
				$('#timestamp_url').attr("disabled",
						!$('#timestamp_activated').is(':checked'));
				$('#recovery_activated').attr("disabled",
						!$('#timestamp_activated').is(':checked'));
				$('#recovery_attempts_interval').attr(
						"disabled",
						!($('#timestamp_activated').is(':checked') && $(
								'#recovery_activated').is(':checked')));
				$('#recovery_attempts_before_abandon').attr(
						"disabled",
						!($('#timestamp_activated').is(':checked') && $(
								'#recovery_activated').is(':checked')));
			});

	$('#recovery_activated').change(
			function() {
				$('#recovery_attempts_interval').attr(
						"disabled",
						!($('#timestamp_activated').is(':checked') && $(
								'#recovery_activated').is(':checked')));
				$('#recovery_attempts_before_abandon').attr(
						"disabled",
						!($('#timestamp_activated').is(':checked') && $(
								'#recovery_activated').is(':checked')));
			});

	$('#signer_roles_activated').change(
			function() {
				$('#signer_roles').attr("disabled",
						!$('#signer_roles_activated').is(':checked'));
			});

	$('input[name=policy_type]')
			.change(
					function() {
						$('#policy_oid')
								.attr(
										"disabled",
										($('input[name=policy_type]:checked')
												.val() != 'explicit'));
						$('#policy_url')
								.attr(
										"disabled",
										($('input[name=policy_type]:checked')
												.val() != 'explicit'));
					});

	$('#pdfSignatureVisible').change(
			function() {
				$('#pdfSignatureUseField').attr("disabled",
						!$('#pdfSignatureVisible').is(':checked'));
				$('#pdfSignatureField').attr("disabled",
						!$('#pdfSignatureVisible').is(':checked'));
				$('input[name=pdfSignatureImage]').attr("disabled",
						!$('#pdfSignatureVisible').is(':checked'));
				$('#pdfSignatureImageFile').attr("disabled",
						!$('#pdfSignatureVisible').is(':checked'));
				$('#pdfSignatureNameVisible').attr("disabled",
						!$('#pdfSignatureVisible').is(':checked'));
				$('#pdfSignatureDateVisible').attr("disabled",
						!$('#pdfSignatureVisible').is(':checked'));
				$('#pdfSignatureLocationVisible').attr("disabled",
						!$('#pdfSignatureVisible').is(':checked'));
				$('#pdfSignatureReasonVisible').attr("disabled",
						!$('#pdfSignatureVisible').is(':checked'));
				$('#pdfSignatureIdVisible').attr("disabled",
						!$('#pdfSignatureVisible').is(':checked'));
				$('#pdfSignatureLabelVisible').attr("disabled",
						!$('#pdfSignatureVisible').is(':checked'));
				$('input[name=pdfSignaturePage]').attr(
						"disabled",
						(!$('#pdfSignatureVisible').is(':checked') || $(
								'#pdfSignatureUseField').is(':checked')));
				$('#pdfSignaturePageIndex').attr(
						"disabled",
						(!$('#pdfSignatureVisible').is(':checked') || $(
								'#pdfSignatureUseField').is(':checked')));
				$('input[name=pdfSignaturePosition]').attr(
						"disabled",
						(!$('#pdfSignatureVisible').is(':checked') || $(
								'#pdfSignatureUseField').is(':checked')));
				$('#pdfSignaturePositionRelativeY').attr(
						"disabled",
						(!$('#pdfSignatureVisible').is(':checked') || $(
								'#pdfSignatureUseField').is(':checked')));
				$('#pdfSignaturePositionRelativeX').attr(
						"disabled",
						(!$('#pdfSignatureVisible').is(':checked') || $(
								'#pdfSignatureUseField').is(':checked')));
				$('#pdfSignaturePositionAbsoluteX').attr(
						"disabled",
						(!$('#pdfSignatureVisible').is(':checked') || $(
								'#pdfSignatureUseField').is(':checked')));
				$('#pdfSignaturePositionAbsoluteY').attr(
						"disabled",
						(!$('#pdfSignatureVisible').is(':checked') || $(
								'#pdfSignatureUseField').is(':checked')));
				$('#pdfSignaturePositionWidth').attr(
						"disabled",
						(!$('#pdfSignatureVisible').is(':checked') || $(
								'#pdfSignatureUseField').is(':checked')));
				$('#pdfSignaturePositionHeight').attr(
						"disabled",
						(!$('#pdfSignatureVisible').is(':checked') || $(
								'#pdfSignatureUseField').is(':checked')));
			});

	$('#pdfSignatureUseField').change(
			function() {
				$('input[name=pdfSignaturePage]').attr("disabled",
						$('#pdfSignatureUseField').is(':checked'));
				$('#pdfSignaturePageIndex').attr("disabled",
						$('#pdfSignatureUseField').is(':checked'));
				$('input[name=pdfSignaturePosition]').attr("disabled",
						$('#pdfSignatureUseField').is(':checked'));
				$('#pdfSignaturePositionRelativeY').attr("disabled",
						$('#pdfSignatureUseField').is(':checked'));
				$('#pdfSignaturePositionRelativeX').attr("disabled",
						$('#pdfSignatureUseField').is(':checked'));
				$('#pdfSignaturePositionAbsoluteX').attr("disabled",
						$('#pdfSignatureUseField').is(':checked'));
				$('#pdfSignaturePositionAbsoluteY').attr("disabled",
						$('#pdfSignatureUseField').is(':checked'));
				$('#pdfSignaturePositionWidth').attr("disabled",
						$('#pdfSignatureUseField').is(':checked'));
				$('#pdfSignaturePositionHeight').attr("disabled",
						$('#pdfSignatureUseField').is(':checked'));
			});

	$('#insertPdfImage').change(
			function() {
				$('#pdfImageFilename').attr("disabled",
						!$('#insertPdfImage').is(':checked'));
				$('input[name=pdfImagePosition]').attr("disabled",
						!$('#insertPdfImage').is(':checked'));
				$('#pdfImagePositionRelativeY').attr("disabled",
						!$('#insertPdfImage').is(':checked'));
				$('#pdfImagePositionRelativeX').attr("disabled",
						!$('#insertPdfImage').is(':checked'));
				$('#pdfImagePositionAbsoluteX').attr("disabled",
						!$('#insertPdfImage').is(':checked'));
				$('#pdfImagePositionAbsoluteY').attr("disabled",
						!$('#insertPdfImage').is(':checked'));
				$('input[name=pdfImagePage]').attr("disabled",
						!$('#insertPdfImage').is(':checked'));
				$('#pdfImageSelectionPage').attr("disabled",
						!$('#insertPdfImage').is(':checked'));
				$('#pdfImagePagePeriod').attr("disabled",
						!$('#insertPdfImage').is(':checked'));
				$('#pdfImagePageStart').attr("disabled",
						!$('#insertPdfImage').is(':checked'));
			});

	$('input[name=signerOCSPSource]').change(
			function() {
				$('#signerOCSPURL')
						.attr(
								"disabled",
								($('input[name=signerOCSPSource]:checked')
										.val() != 'server'));
			});

	$('input[name=signerCRLSource]')
			.change(
					function() {
						$('#signerCRLURL')
								.attr(
										"disabled",
										($(
												'input[name=signerCRLSource]:checked')
												.val() != 'server'));
					});

	$('input[name=parentsOCSPSource]').change(
			function() {
				$('#parentsOCSPURL')
						.attr(
								"disabled",
								($('input[name=parentsOCSPSource]:checked')
										.val() != 'server'));
			});

	$('input[name=parentsARLSource]').change(
			function() {
				$('#parentsARLURL')
						.attr(
								"disabled",
								($('input[name=parentsARLSource]:checked')
										.val() != 'server'));
			});

	$('#msofficeVisibleSignature').change(
			function() {
				$('#msofficeFieldIndex').attr("disabled",
						!$('#msofficeVisibleSignature').is(':checked'));
				$('#msOfficeVisibleImageSource').attr("disabled",
						!$('#msofficeVisibleSignature').is(':checked'));
				$('#msOfficeVisibleImage').attr("disabled",
						!$('#msofficeVisibleSignature').is(':checked'));
			});

	// Javascript API to load LPAppletLoader and LP7Web applets
	var jsAppletLoader = new LPAppletLoader(URL_JNLP_LP7WEB);
	jsAppletLoader.setRequiredJavaPluginVersion("1.5+");
	jsAppletLoader.setAppletObjectId("appletLoaderId");
	jsAppletLoader.setWidth(300);
	jsAppletLoader.setHeight(70);
	jsAppletLoader.setTimeout(60000);
	jsAppletLoader.setSubAppletHeight(25);
	jsAppletLoader.setSubAppletWidth(400);
	jsAppletLoader.setLPAppletLoaderPath(HOSTNAME + "/signature/applet");
	jsAppletLoader
			.setMessageNoJavaPlugin("<p><strong>Le plugin Java n'est pas install\E9 ou activ\E9.<br />Veuillez v\E9rifier que le plugin Java est install\E9 et activ\E9 dans votre navigateur.</strong></p>");
	jsAppletLoader
			.setMessageJavaPluginDownloadLink("T\E9l\E9chargez la derni\E8re version du plugin Java.");
	jsAppletLoader
			.setMessageInvalidJavaPluginVersion("<p><strong>Ce navigateur ne poss\E8de pas un plugin Java suffisamment r\E9cent.</strong></p>");
	jsAppletLoader
			.setMessageExcludeJavaPlugin("<p><strong>Le plugin Java que vous utilisez n'est pas compatible.</strong></p>");
	jsAppletLoader.setInitializingMessage("appletLoaderInitializingSubApplet");
	jsAppletLoader
			.setDownloadingMessage("T\E9l\E9chargement de l'applet de signature...");
	jsAppletLoader.setLoadingMessage("Chargement de l'applet de signature...");
	jsAppletLoader.setCheckConfiguration(true);
	jsAppletLoader.launch("div_applet");
}

function appletLoaderLoaded() {
	// LPAppletLoader applet
	var appletLoader = document.getElementById('appletLoaderId');

	appletLoader.addParameter('cookie', 'PHPSESSID=hkrpcmo1lqk2mu58aj0bfu4av3');
	appletLoader.addParameter('ui_language', 'fr');
	appletLoader.addParameter('browser', navigator.userAgent);
	appletLoader.addParameter('keystore_types', 'pkcs11');
	appletLoader.addParameter('truststore_types', 'os');
	appletLoader.addParameter('_context_key_caching', true);
	appletLoader.addParameter('use_browser_keystore', 'false');
	appletLoader.addParameter('certificates_subject_dn_format', 'CN,SOURCE');
	appletLoader.addParameter('pkcs11_use_modules_properties_files', 'false');
	appletLoader.addParameter('pkcs11_modules', 'aetpkss1.dll,gclib.dll');
	appletLoader.addParameter('pkcs11_modules_activated', 'false');
	appletLoader.addParameter('network_connection_timeout', '8000');
	appletLoader.addParameter('network_so_timeout', '8000');
	appletLoader.addParameter('progress_bar_download_signed_content_visible',
			'true');
	appletLoader.addParameter('progress_bar_download_signed_content_details',
			true);
	appletLoader.addParameter('progress_bar_post_signature_result_visible',
			'true');
	appletLoader.addParameter('progress_bar_post_sign_result_details', true);
	appletLoader.addParameter('progress_bar_download_signature_policy_visible',
			'true');
	appletLoader.addParameter('keyusage_column_visible', 'true');
	appletLoader.addParameter('validity_column_visible', 'true');

	appletLoader.launch();
}

function appletLoaderNotLoaded(errorCode) {
	var message = "appletLoaderNotLoaded"
	if (errorCode == LPAppletLoader.ErrorCode.NO_JAVA_PLUGIN) {
		message = '<font color="red">Aucun plugin Java install\E9</font>';
	} else if (errorCode == LPAppletLoader.ErrorCode.INVALID_JAVA_PLUGIN) {
		message = '<font color="red">Votre version du plugin Java n\'est pas suffisamment r\E9cent.</font>';
	} else if (errorCode == LPAppletLoader.ErrorCode.APPLET_LOADING_TIMEOUT) {
		message = '<font color="red">T\E9l\E9chargement de l\'applet impossible (Timeout). V\E9rifier votre connexion.</font>';
	} else if (errorCode == LPAppletLoader.ErrorCode.APPLET_LOADING_INIT_ERROR) {
		message = '<font color="red">Probl\E8me \E0 l\'initialisation de l\'applet.</font>';
	} else {
		// Undetermined error
	}
	document.getElementById("message").innerHTML = "<font color='red'>"
			+ message + "</font>";
}

function subAppletLoaded() {

	createContext();
	addFile();

	$('.sign_button').removeAttr('disabled');
	$('#div_subapplet_buttons').show();
}

function subAppletNotLoaded(errorCode) {
	// Minimize the LPAppletLoader applet
	document.getElementById("appletLoaderId").width = '1px';
	document.getElementById("appletLoaderId").height = '1px';

	var message = "ERREUR: Impossible de charger l'applet LP7Web. ";
	if (errorCode == 1) {
		message += "Probl\E8me \E0 l'initialisation.";
	} else if (errorCode == 2) {
		message += "Fichier JNLP invalide.";
	} else if (errorCode == 5) {
		message += "Probl\E8me de chargement.";
	} else if (errorCode == 6) {
		message += "T\E9l\E9chargement de l'archive \E9chou\E9.";
	} else {
		message += "Code d'erreur: " + errorCode;
	}
	document.getElementById("message").innerHTML = "<font color='red'>"
			+ message + "</font>";
}

function refreshCertificateList() {
	var subApplet = document.getElementById('appletLoaderId').getSubApplet();
	subApplet.refreshCertificateList();

}

function viewCertDetails() {
	var subApplet = document.getElementById('appletLoaderId').getSubApplet();
	subApplet.viewSelectedCertificateDetails();
}

function createContext() {
	var subApplet = document.getElementById('appletLoaderId').getSubApplet();
	subApplet.createSignatureContext();
}

var lastSignatureContextAdded;

var lastMimeTypeAdded;
function addFile() {

	lastSignatureContextAdded = context;

	lastMimeTypeAdded = "application/pdf";

	var subApplet = document.getElementById('appletLoaderId').getSubApplet();

	subApplet.addSignedContentFromURI(lastSignatureContextAdded,
			lastFilepathAdded, lastFilename, lastMimeTypeAdded, null);

}

function download() {
	var subApplet = document.getElementById('appletLoaderId').getSubApplet();
	subApplet.downloadDeferredSignedContents(null, null);
}

function sign(modeSign) {
	mode = modeSign;
	
	$('#message').html(
			'<font color="green">Signature du document en cours ...</font>');

	var contextToSign = context;
	var fileToSign = file;

	var subApplet = document.getElementById('appletLoaderId').getSubApplet();

	subApplet.generateSignature(contextToSign, file, null, false);

}

function generateSignature() {

	var entryName = lpWebEncryptApplet.getFileEntryName(currentZipID,
			selectedIndex);
	lpWebEncryptApplet
			.exportFile('callbackExportFile', currentZipID, entryName);

}

function saveSignatureResult(result, fileName) {
	step++;
	$('#message')
			.html(
					'<font color="green">Enregistrement de la signature en cours...</font>');

	var subApplet = document.getElementById('appletLoaderId').getSubApplet();

	subApplet
			.postSignatureResult(result,
					"https://localhost:8443/Kermel-WEB/jsp/recept.jsp?file="
							+ fileName, fileName, true);

}

function callbackCheckConfiguration(errorCode) {
	var message = "ERREUR: Impossible de charger l'applet LP7Web. ";
	if (errorCode == 10) {
		message += "Votre r\E9pertoire personnel est invalide.";
	} else if (errorCode == 11) {
		message += "Votre r\E9pertoire personnel est prot\E9g\E9 en lecture.";
	} else if (errorCode == 12) {
		message += "Votre r\E9pertoire personnel est prot\E9g\E9 en \E9criture.";
	} else if (errorCode == 13) {
		message += "Le r\E9pertoire temporaire est invalide.";
	} else if (errorCode == 14) {
		message += "Le r\E9pertoire temporaire est prot\E9g\E9 en lecture.";
	} else if (errorCode == 15) {
		message += "Le r\E9pertoire temporaire est prot\E9g\E9 en \E9criture.";
	} else if (errorCode == 16) {
		message += "La version de Java utilis\E9 est incompatible avec cette version de LP7Web.";
	} else if (errorCode == 17) {
		message = "";
	} else if (errorCode == 18) {
		message += "Votre proxy n'est pas joignable.";
	} else if (errorCode == 19) {
		message += "Probl\E8me de connexion, v\E9rifier votre connexion..";
	}
	document.getElementById("message").innerHTML = "<font color='red'>"
			+ message + "</font>";
}

function callbackLP7Web(source, resultCode, result) {
	if (resultCode == 0) {
		if (source == 'createSignatureContext') {
			context = result;

		} else if (source == 'addSignedContentFromLocalFile'
				|| source == 'addSignedContentFromURI'
				|| source == 'addDeferredSignedContentFromURI') {
			file = result;

		} else if (source == 'generateSignature') {
			$('#message').html(
					'<font color="green">Signature généré avec succès</font>');
			nomFichier = lastFilename;
			saveSignatureResult(result, nomFichier);

		} else if (source == 'saveSignatureResult'
				|| source == 'postSignatureResult') {

			$('#message').html(
			'<font color="green">Signature enregistrée</font>');
			
			if(mode === "PR") {
				$('.aftersign_button').click();
				}
				else if(mode === "SEC") {
					$('.aftersign_button_sec').click();
				}
		}
	} else if (resultCode != 4) {
		if (source == 'allTimestampAttemptsFailed') {
			alert("allTimestampAttemptsFailed");
			return;
		} else if (source == 'timestampAttemptFailed') {
			alert("timestampAttemptFailed");
			return;
		}

		var msg = '<font color="red">';
		if (resultCode == 100) {
			msg += 'Mot de passe invalide.';
		} else if (resultCode == 101) {
			msg += 'Acc\E8s a votre certificat impossible.';
		} else if (resultCode == 102) {
			msg += 'Chaine de certification invalide.';
		} else if (resultCode == 103) {
			msg += 'Chaine de certification expir\E9e.';
		} else if (resultCode == 104) {
			msg += 'Chaine de certification pas encore valide.';
		} else if (resultCode == 106) {
			msg += 'Certificat r\E9voqu\E9.';
		} else if (resultCode == 107) {
			msg += 'Impossible de v\E9rifier le statut de r\E9vocation du certificat.';
		} else {
			msg += 'Error when call method ' + source + ': resultCode='
					+ resultCode;
		}
		msg += '</font>';
		$('#message').html(msg);

		if (source != 'postStackTrace') {
			var subApplet = document.getElementById('appletLoaderId')
					.getSubApplet();
			subApplet.postStackTrace("http://localhost/lp7web/stacktrace.php ",
					null);
		}
	}
}