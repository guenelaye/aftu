﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html
	xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>


<script src="./js/jquery-191.min.js" type="text/javascript"></script>
<script src="./js/LPAppletLoader.js" type="text/javascript"></script>

<script type="text/javascript">

	var HOSTNAME = "http://localhost:8080/lpwebdecrypt";
	HOSTNAME_PKERMEL = "http://localhost:8080/PkermelV2";
	var URL_JNLP_LPWEBDECRYPT = HOSTNAME_PKERMEL
			+ "/decryptage/applet/jnlp/LPWebDecrypt.jnlp";
	var lpWebDecryptApplet;
	var decryptSessionID;
	function startAppletLoader() {
		// Javascript API to load LPAppletLoader and LPWebDecrypt applets
		//  alert(HOSTNAME);
		$('#decph').show();
		var jsAppletLoader = new LPAppletLoader(URL_JNLP_LPWEBDECRYPT);
		jsAppletLoader.setRequiredJavaPluginVersion("1.4.2+");
		jsAppletLoader.setAppletObjectId("appletId");
		jsAppletLoader.setWidth(300);
		jsAppletLoader.setHeight(70);
		jsAppletLoader.setSubAppletHeight(70);
		jsAppletLoader.setSubAppletWidth(300);
		jsAppletLoader.setTimeout(0);
		jsAppletLoader.setLPAppletLoaderPath(HOSTNAME_PKERMEL + "/applet");
		jsAppletLoader
				.setMessageNoJavaPlugin("<p><strong>Le plugin Java n'est pas installé ou activé.<br />Veuillez vérifier que le plugin Java est installé et activé dans votre navigateur.</strong></p>");
		jsAppletLoader
				.setMessageJavaPluginDownloadLink("Téléchargez la dernière version du plugin Java.");
		jsAppletLoader
				.setMessageInvalidJavaPluginVersion("<p><strong>Ce navigateur ne possède pas un plugin Java suffisamment écent.</strong></p>");
		jsAppletLoader
				.setDownloadingMessage("Téléchargement de l'applet de décryptage...");
		jsAppletLoader
				.setLoadingMessage("Lancement de l'applet de décryptage...");
		jsAppletLoader.setBackgroundColor("#FFFFFF");
		jsAppletLoader.launch("div_applet");

	}

	function appletLoaderLoaded() {
		// LPAppletLoader applet
		var appletLoader = document.getElementById('appletId');
		appletLoader.addParameter('background_color', '#FFFFFF');
		appletLoader.addParameter('ui_mode', 'internal');
		appletLoader.addParameter('browser_name', navigator.appName);
 		appletLoader.addParameter('use_browser_keystore', 'true');
		//appletLoader.addParameter('keystore_types','lp'); 
 		appletLoader.addParameter('keystore_types', 'browser,os');
		appletLoader.addParameter('hide.certificates.on.start', true);

		appletLoader.launch();
	}

	function appletLoaderNotLoaded(errorCode) {
		if (errorCode == LPAppletLoader.ErrorCode.NO_JAVA_PLUGIN) {

		} else if (errorCode == LPAppletLoader.ErrorCode.INVALID_JAVA_PLUGIN) {

		} else if (errorCode == LPAppletLoader.ErrorCode.APPLET_LOADING_TIMEOUT) {

		} else {
			// Undetermined error
		}

		var message = "ERREUR: Impossible de charger l'applet LPAppletLoader "
		document.getElementById("div_applet_error").innerHTML = "<font color='red'>"
				+ message + "</font>";
	}

	function subAppletLoaded() {
		document.getElementById('decryptBtn').disabled = false;
		createSession();

	}

	function subAppletNotLoaded(errorCode) {
		var message = "ERREUR: Impossible de charger l'applet LPWebDecrypt"
		document.getElementById("div_applet_error").innerHTML = "<font color='red'>"
				+ message + "</font>";
	}

	function createSession() {
		var applet = document.getElementById('appletId').getSubApplet();
		var cheminxmlov = document.getElementById('cheminxmlov').value;
		var cheminov = document.getElementById('cheminov').value;
		
		applet.createSessionFromURI('callbackCreateDecryptSession',
				cheminxmlov, cheminov);
		
		//applet.createSessionFromFile('callbackCreateDecryptSession',cheminxmlov,cheminov);

	}
	
	function addp12() {		
		var applet = document.getElementById('appletId').getSubApplet();
		
		applet.addp12("${privateCertPath}","${privateCertPass}");		
	}

	function callbackCreateDecryptSession(resultCode, sessionID) {
	
		if (resultCode == 0) {
			decryptSessionID = sessionID;
			addp12();
			

		} else if (resultCode == 3) {
			// OPERATION CANCELED
		} else {
			alert("Erreur " + resultCode);
		}

	}

	function decrypt() {
		var applet = document.getElementById('appletId').getSubApplet();
		applet.decrypt('callbackDecrypt', decryptSessionID, null);
	}

	function callbackDecrypt(resultCode, decryptID) {
		if (resultCode == 0) {
			var applet = document.getElementById('appletId').getSubApplet();
			//applet.saveDecipherResultToLocalFile('callbackSaveToLocalFile', decryptSessionID, decryptID, null);
			applet.postDecipherResult('callbackPostVerServeur',
					decryptSessionID, decryptID, decryptID, HOSTNAME_PKERMEL
							+ "/server2.jsp?zipName=" + decryptID, null);
			document.getElementById('pli').value = decryptID;
			//document.getElementById("zipName").value = filePath;

		} else if (resultCode == 3) {
			// OPERATION CANCELED
		} else {
			alert("Erreur " + resultCode);
		}
	}

	function callbackSaveToLocalFile(resultCode, filePath, fileSize) {
		if (resultCode == 0) {
			alert("Decipher file saved with success in the following file: "
					+ filePath);

			document.getElementById("chemin").value = filePath;
			document.getElementById("frm").submit();

		} else if (resultCode == 3) {
			// OPERATION CANCELED
		} else {
			alert("Erreur " + resultCode);
		}
	}

	function callbackPostVerServeur(resultCode) {
		if (resultCode == 0) {
			//alert("Déchiffrement effectué avec succès ! ");

			document.getElementById("frm").submit();

		} else if (resultCode == 3) {
			// OPERATION CANCELED
		} else {
			alert("Erreur " + resultCode);
		}
	}

	function begin() {
		alert("Début de décryptage");
	}

	function advancement(pourcentage, total) {
		document.getElementById('progress').style.width = ((200 * pourcentage) / total)
				+ "px";
	}

	function end() {
		alert("Fin du décryptage");
	}
</script>

<link type="text/css" rel="stylesheet" href="./assets/metro/css/metro.css" />
<link type="text/css" rel="stylesheet" href="./assets/metro/css/metro-icons.css" />
<link type="text/css" rel="stylesheet" href="./assets/metro/css/metro-responsive.css" />
<link type="text/css" rel="stylesheet" href="./assets/metro/css/metro-schemes.css" />
<link type="text/css" rel="stylesheet" href="./assets/metro/css/docs.css" />

		


</head>

<body onload='startAppletLoader()'>





<form id="decph" action="./DecryptServlet?id=${id}"  style="display: none;">
	<input type="hidden" id="cheminxmlov"
		value="${encryptedXmlPath}" />
	<input type="hidden" id="cheminov"
		value="${encryptedDataPath}" />


	<div id="div_applet"></div> 
	
				
	<button class="command-button success" type="button" id="decryptBtn"
										onclick="javascript:decrypt();" disabled="disabled">
										<span class="icon mif-file-zip"></span>Lancer l'ouverture du pli <small>cette action est irréversible</small>
									</button>			



	<div id='waitDiv' style='display: none;'>
		<img src='images/wait.gif' width='200px' height='20px'>
	</div>


	<div id='filter' style='display: block'></div>
</form>

	<form id="frm" name="frm" action="./AfterDecryptServlet" method="post">
		<input type="hidden" id="chemin" name="chemin" />
		<input type="hidden" id="pli" name="pli" />
		<input type="hidden" id="zipName" name="zipName" />
		<input type="hidden" name="id" value="${id}" />
	</form>

	
</body>
</html>
