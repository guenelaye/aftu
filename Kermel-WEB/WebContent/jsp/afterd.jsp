<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link type="text/css" rel="stylesheet"
	href="./assets/metro/css/metro.css" />
<link type="text/css" rel="stylesheet"
	href="./assets/metro/css/metro-icons.css" />
<link type="text/css" rel="stylesheet"
	href="./assets/metro/css/metro-responsive.css" />
<link type="text/css" rel="stylesheet"
	href="./assets/metro/css/metro-schemes.css" />
<link type="text/css" rel="stylesheet"
	href="./assets/metro/css/docs.css" />
</head>
<body>
	<div class="notify success">
		<span class="notify-closer"></span> <span class="notify-title">Succes!
		</span> <span class="notify-text">${message}</span>
	</div>
	<div class="notify info">
		<span class="notify-closer"></span> <span class="notify-title">Info</span>
		<span class="notify-text">Cliquez sur le boutton suivant pour
			continuer</span>
	</div>



</body>
</html>