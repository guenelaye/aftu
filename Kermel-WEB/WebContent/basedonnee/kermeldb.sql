-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- G�n�r� le : Mer 12 Juin 2013 � 18:59
-- Version du serveur: 5.1.53
-- Version de PHP: 5.3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de donn�es: `kermeldb`
--

-- --------------------------------------------------------

--
-- Structure de la table `ea_courriers`
--

CREATE TABLE IF NOT EXISTS `ea_courriers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autres` varchar(255) DEFAULT NULL,
  `courrierenreference` varchar(255) DEFAULT NULL,
  `datecourrier` date DEFAULT NULL,
  `datereception` date DEFAULT NULL,
  `datesaisie` date DEFAULT NULL,
  `objet` varchar(255) DEFAULT NULL,
  `origine` varchar(50) DEFAULT NULL,
  `referencecourrier` varchar(50) DEFAULT NULL,
  `formation` bigint(20) NOT NULL,
  `courrier_electroniq` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK72519D838AB88E5B` (`formation`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `ea_courriers`
--

INSERT INTO `ea_courriers` (`id`, `autres`, `courrierenreference`, `datecourrier`, `datereception`, `datesaisie`, `objet`, `origine`, `referencecourrier`, `formation`, `courrier_electroniq`) VALUES
(3, 'aa', 'aaa', '2013-05-08', '2013-05-08', '2013-05-08', 'aa', 'aaa', 'aa', 1, '3050508052013_Pj_recu.pdf'),
(4, 'dd', 'dd', '2013-05-11', '2013-05-11', '2013-05-11', 'dd', 'dd', 'dd', 3, '32491011052013_Pj_recu.pdf');

-- --------------------------------------------------------

--
-- Structure de la table `ea_participants`
--

CREATE TABLE IF NOT EXISTS `ea_participants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adresse` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `nom` varchar(100) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `autorite_contra` bigint(20) NOT NULL,
  `formation` bigint(20) NOT NULL,
  `groupe_id` int(11) DEFAULT NULL,
  `Typeautorite_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1539B26356BD84F9` (`groupe_id`),
  KEY `FK1539B2638AB88E5B` (`formation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `ea_participants`
--


-- --------------------------------------------------------

--
-- Structure de la table `ea_piecesjointes`
--

CREATE TABLE IF NOT EXISTS `ea_piecesjointes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datemiseenligne` date DEFAULT NULL,
  `fichier` varchar(255) DEFAULT NULL,
  `format` varchar(50) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `nomfichier` varchar(255) DEFAULT NULL,
  `rapport` bit(1) DEFAULT NULL,
  `taille` varchar(10) DEFAULT NULL,
  `formation` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK67AF5D708AB88E5B` (`formation`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `ea_piecesjointes`
--

INSERT INTO `ea_piecesjointes` (`id`, `datemiseenligne`, `fichier`, `format`, `libelle`, `nomfichier`, `rapport`, `taille`, `formation`) VALUES
(3, '2013-05-08', '08-05-2013_Pj_Document.rtf', 'doc', 'dd', '08-05-2013_Pj_Document', b'1', '0', 1),
(4, '2013-05-11', '11-05-2013_Pj_recu.pdf', 'pdf', 'dd', '11-05-2013_Pj_recu', b'0', '13', 3);

-- --------------------------------------------------------

--
-- Structure de la table `eco_caracterescourriers`
--

CREATE TABLE IF NOT EXISTS `eco_caracterescourriers` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `ICONE` varchar(50) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `eco_caracterescourriers`
--


-- --------------------------------------------------------

--
-- Structure de la table `eco_groupes`
--

CREATE TABLE IF NOT EXISTS `eco_groupes` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(50) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `GroupesImputation` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK1E5A6CBF984D7F38` (`GroupesImputation`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `eco_groupes`
--


-- --------------------------------------------------------

--
-- Structure de la table `eco_groupesimputation`
--

CREATE TABLE IF NOT EXISTS `eco_groupesimputation` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `eco_groupesimputation`
--

INSERT INTO `eco_groupesimputation` (`ID`, `description`, `LIBELLE`) VALUES
(1, 'Groupe A', 'Groupe A'),
(2, 'Groupe B', 'Groupe B'),
(3, 'Groupe C', 'Groupe C');

-- --------------------------------------------------------

--
-- Structure de la table `eco_groupesimputilisateur`
--

CREATE TABLE IF NOT EXISTS `eco_groupesimputilisateur` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `GroupesImputation` bigint(20) DEFAULT NULL,
  `Utilisateur` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKF1B6B6984D7F38` (`GroupesImputation`),
  KEY `FKF1B6B6C955852B` (`Utilisateur`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `eco_groupesimputilisateur`
--

INSERT INTO `eco_groupesimputilisateur` (`ID`, `description`, `LIBELLE`, `GroupesImputation`, `Utilisateur`) VALUES
(1, NULL, NULL, 1, 4);

-- --------------------------------------------------------

--
-- Structure de la table `eco_modereception`
--

CREATE TABLE IF NOT EXISTS `eco_modereception` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `eco_modereception`
--


-- --------------------------------------------------------

--
-- Structure de la table `eco_modetraitement`
--

CREATE TABLE IF NOT EXISTS `eco_modetraitement` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `ICONE` varchar(50) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `eco_modetraitement`
--


-- --------------------------------------------------------

--
-- Structure de la table `eco_typescourriers`
--

CREATE TABLE IF NOT EXISTS `eco_typescourriers` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(50) DEFAULT NULL,
  `DELAI` int(11) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `eco_typescourriers`
--


-- --------------------------------------------------------

--
-- Structure de la table `jos_banner`
--

CREATE TABLE IF NOT EXISTS `jos_banner` (
  `bid` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` varchar(30) NOT NULL DEFAULT 'banner',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `imageurl` varchar(100) NOT NULL DEFAULT '',
  `clickurl` varchar(200) NOT NULL DEFAULT '',
  `date` datetime DEFAULT NULL,
  `showBanner` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor` varchar(50) DEFAULT NULL,
  `custombannercode` text,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tags` text NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`bid`),
  KEY `viewbanner` (`showBanner`),
  KEY `idx_banner_catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `jos_banner`
--

INSERT INTO `jos_banner` (`bid`, `cid`, `type`, `name`, `alias`, `imptotal`, `impmade`, `clicks`, `imageurl`, `clickurl`, `date`, `showBanner`, `checked_out`, `checked_out_time`, `editor`, `custombannercode`, `catid`, `description`, `sticky`, `ordering`, `publish_up`, `publish_down`, `tags`, `params`) VALUES
(1, 1, 'banner', 'OSM 1', 'osm-1', 0, 43, 0, 'osmbanner1.png', 'http://www.opensourcematters.org', '2004-07-07 15:31:29', 1, 0, '0000-00-00 00:00:00', '', '', 13, '', 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(2, 1, 'banner', 'OSM 2', 'osm-2', 0, 49, 0, 'osmbanner2.png', 'http://www.opensourcematters.org', '2004-07-07 15:31:29', 1, 0, '0000-00-00 00:00:00', '', '', 13, '', 0, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(3, 1, '', 'Joomla!', 'joomla', 0, 846, 0, '', 'http://www.joomla.org', '2006-05-29 14:21:28', 1, 0, '0000-00-00 00:00:00', '', '<a href="{CLICKURL}" target="_blank">{NAME}</a>\r\n<br/>\r\nJoomla! The most popular and widely used Open Source CMS Project in the world.', 14, '', 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(4, 1, '', 'JoomlaCode', 'joomlacode', 0, 846, 0, '', 'http://joomlacode.org', '2006-05-29 14:19:26', 1, 0, '0000-00-00 00:00:00', '', '<a href="{CLICKURL}" target="_blank">{NAME}</a>\r\n<br/>\r\nJoomlaCode, development and distribution made easy.', 14, '', 0, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(5, 1, '', 'Joomla! Extensions', 'joomla-extensions', 0, 841, 0, '', 'http://extensions.joomla.org', '2006-05-29 14:23:21', 1, 0, '0000-00-00 00:00:00', '', '<a href="{CLICKURL}" target="_blank">{NAME}</a>\r\n<br/>\r\nJoomla! Components, Modules, Plugins and Languages by the bucket load.', 14, '', 0, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(6, 1, '', 'Joomla! Shop', 'joomla-shop', 0, 841, 0, '', 'http://shop.joomla.org', '2006-05-29 14:23:21', 1, 0, '0000-00-00 00:00:00', '', '<a href="{CLICKURL}" target="_blank">{NAME}</a>\r\n<br/>\r\nFor all your Joomla! merchandise.', 14, '', 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(7, 1, '', 'Joomla! Promo Shop', 'joomla-promo-shop', 0, 10, 1, 'shop-ad.jpg', 'http://shop.joomla.org', '2007-09-19 17:26:24', 1, 0, '0000-00-00 00:00:00', '', '', 33, '', 0, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(8, 1, '', 'Joomla! Promo Books', 'joomla-promo-books', 0, 12, 0, 'shop-ad-books.jpg', 'http://shop.joomla.org/amazoncom-bookstores.html', '2007-09-19 17:28:01', 1, 0, '0000-00-00 00:00:00', '', '', 33, '', 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `jos_bannerclient`
--

CREATE TABLE IF NOT EXISTS `jos_bannerclient` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contact` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `extrainfo` text NOT NULL,
  `checked_out` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out_time` time DEFAULT NULL,
  `editor` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `jos_bannerclient`
--

INSERT INTO `jos_bannerclient` (`cid`, `name`, `contact`, `email`, `extrainfo`, `checked_out`, `checked_out_time`, `editor`) VALUES
(1, 'Open Source Matters', 'Administrator', 'admin@opensourcematters.org', '', 0, '00:00:00', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `jos_bannertrack`
--

CREATE TABLE IF NOT EXISTS `jos_bannertrack` (
  `track_date` date NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `jos_bannertrack`
--


-- --------------------------------------------------------

--
-- Structure de la table `jos_categories`
--

CREATE TABLE IF NOT EXISTS `jos_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  `section` varchar(50) NOT NULL DEFAULT '',
  `image_position` varchar(30) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor` varchar(50) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cat_idx` (`section`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Contenu de la table `jos_categories`
--

INSERT INTO `jos_categories` (`id`, `parent_id`, `title`, `name`, `alias`, `image`, `section`, `image_position`, `description`, `published`, `checked_out`, `checked_out_time`, `editor`, `ordering`, `access`, `count`, `params`) VALUES
(1, 0, 'Latest', '', 'latest-news', 'taking_notes.jpg', '1', 'left', 'The latest news from the Joomla! Team', 1, 0, '0000-00-00 00:00:00', '', 1, 0, 1, ''),
(2, 0, 'Joomla! Specific Links', '', 'joomla-specific-links', 'clock.jpg', 'com_weblinks', 'left', 'A selection of links that are all related to the Joomla! Project.', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(3, 0, 'Newsflash', '', 'newsflash', '', '1', 'left', '', 1, 0, '0000-00-00 00:00:00', '', 2, 0, 0, ''),
(4, 0, 'Joomla!', '', 'joomla', '', 'com_newsfeeds', 'left', '', 1, 62, '2012-11-27 10:04:14', NULL, 2, 0, 0, ''),
(5, 0, 'Free and Open Source Software', '', 'free-and-open-source-software', '', 'com_newsfeeds', 'left', 'Read the latest news about free and open source software from some of its leading advocates.', 1, 0, '0000-00-00 00:00:00', NULL, 3, 0, 0, ''),
(6, 0, 'Related Projects', '', 'related-projects', '', 'com_newsfeeds', 'left', 'Joomla builds on and collaborates with many other free and open source projects. Keep up with the latest news from some of them.', 1, 0, '0000-00-00 00:00:00', NULL, 4, 0, 0, ''),
(12, 0, 'Contacts', '', 'contacts', '', 'com_contact_details', 'left', 'Contact Details for this Web site', 1, 0, '0000-00-00 00:00:00', NULL, 0, 0, 0, ''),
(13, 0, 'Joomla', '', 'joomla', '', 'com_banner', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 0, 0, 0, ''),
(14, 0, 'Text Ads', '', 'text-ads', '', 'com_banner', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 0, 0, 0, ''),
(15, 0, 'Features', '', 'features', '', 'com_content', 'left', '', 0, 0, '0000-00-00 00:00:00', NULL, 6, 0, 0, ''),
(17, 0, 'Benefits', '', 'benefits', '', 'com_content', 'left', '', 0, 0, '0000-00-00 00:00:00', NULL, 4, 0, 0, ''),
(18, 0, 'Platforms', '', 'platforms', '', 'com_content', 'left', '', 0, 0, '0000-00-00 00:00:00', NULL, 3, 0, 0, ''),
(19, 0, 'Other Resources', '', 'other-resources', '', 'com_weblinks', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 2, 0, 0, ''),
(29, 0, 'The CMS', '', 'the-cms', '', '4', 'left', 'Information about the software behind Joomla!<br />', 1, 0, '0000-00-00 00:00:00', NULL, 2, 0, 0, ''),
(28, 0, 'Current Users', '', 'current-users', '', '3', 'left', 'Questions that users migrating to Joomla! 1.5 are likely to raise<br />', 1, 0, '0000-00-00 00:00:00', NULL, 2, 0, 0, ''),
(25, 0, 'The Project', '', 'the-project', '', '4', 'left', 'General facts about Joomla!<br />', 1, 65, '2007-06-28 14:50:15', NULL, 1, 0, 0, ''),
(27, 0, 'New to Joomla!', '', 'new-to-joomla', '', '3', 'left', 'Questions for new users of Joomla!', 1, 0, '0000-00-00 00:00:00', NULL, 3, 0, 0, ''),
(30, 0, 'The Community', '', 'the-community', '', '4', 'left', 'About the millions of Joomla! users and Web sites<br />', 1, 0, '0000-00-00 00:00:00', NULL, 3, 0, 0, ''),
(31, 0, 'General', '', 'general', '', '3', 'left', 'General questions about the Joomla! CMS', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(32, 0, 'Languages', '', 'languages', '', '3', 'left', 'Questions related to localisation and languages', 1, 0, '0000-00-00 00:00:00', NULL, 4, 0, 0, ''),
(33, 0, 'Joomla! Promo', '', 'joomla-promo', '', 'com_banner', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(34, 0, 'Demo ', '', 'demo', '', '5', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, '');

-- --------------------------------------------------------

--
-- Structure de la table `jos_components`
--

CREATE TABLE IF NOT EXISTS `jos_components` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `link` varchar(255) NOT NULL DEFAULT '',
  `menuid` int(11) unsigned NOT NULL DEFAULT '0',
  `parent` int(11) unsigned NOT NULL DEFAULT '0',
  `admin_menu_link` varchar(255) NOT NULL DEFAULT '',
  `admin_menu_alt` varchar(255) NOT NULL DEFAULT '',
  `option` varchar(50) NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `admin_menu_img` varchar(255) NOT NULL DEFAULT '',
  `iscore` tinyint(4) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent_option` (`parent`,`option`(32))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- Contenu de la table `jos_components`
--

INSERT INTO `jos_components` (`id`, `name`, `link`, `menuid`, `parent`, `admin_menu_link`, `admin_menu_alt`, `option`, `ordering`, `admin_menu_img`, `iscore`, `params`, `enabled`) VALUES
(1, 'Banners', '', 0, 0, '', 'Banner Management', 'com_banners', 0, 'js/ThemeOffice/component.png', 0, 'track_impressions=0\ntrack_clicks=0\ntag_prefix=\n\n', 1),
(2, 'Banners', '', 0, 1, 'option=com_banners', 'Active Banners', 'com_banners', 1, 'js/ThemeOffice/edit.png', 0, '', 1),
(3, 'Clients', '', 0, 1, 'option=com_banners&c=client', 'Manage Clients', 'com_banners', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(4, 'Web Links', 'option=com_weblinks', 0, 0, '', 'Manage Weblinks', 'com_weblinks', 0, 'js/ThemeOffice/component.png', 0, 'show_comp_description=1\ncomp_description=\nshow_link_hits=1\nshow_link_description=1\nshow_other_cats=1\nshow_headings=1\nshow_page_title=1\nlink_target=0\nlink_icons=\n\n', 1),
(5, 'Links', '', 0, 4, 'option=com_weblinks', 'View existing weblinks', 'com_weblinks', 1, 'js/ThemeOffice/edit.png', 0, '', 1),
(6, 'Categories', '', 0, 4, 'option=com_categories&section=com_weblinks', 'Manage weblink categories', '', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(7, 'Contacts', 'option=com_contact', 0, 0, '', 'Edit contact details', 'com_contact', 0, 'js/ThemeOffice/component.png', 1, 'contact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_fax=\nicon_misc=\nshow_headings=1\nshow_position=1\nshow_email=0\nshow_telephone=1\nshow_mobile=1\nshow_fax=1\nbannedEmail=\nbannedSubject=\nbannedText=\nsession=1\ncustomReply=0\n\n', 1),
(8, 'Contacts', '', 0, 7, 'option=com_contact', 'Edit contact details', 'com_contact', 0, 'js/ThemeOffice/edit.png', 1, '', 1),
(9, 'Categories', '', 0, 7, 'option=com_categories&section=com_contact_details', 'Manage contact categories', '', 2, 'js/ThemeOffice/categories.png', 1, 'contact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_fax=\nicon_misc=\nshow_headings=1\nshow_position=1\nshow_email=0\nshow_telephone=1\nshow_mobile=1\nshow_fax=1\nbannedEmail=\nbannedSubject=\nbannedText=\nsession=1\ncustomReply=0\n\n', 1),
(10, 'Polls', 'option=com_poll', 0, 0, 'option=com_poll', 'Manage Polls', 'com_poll', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(11, 'News Feeds', 'option=com_newsfeeds', 0, 0, '', 'News Feeds Management', 'com_newsfeeds', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(12, 'Feeds', '', 0, 11, 'option=com_newsfeeds', 'Manage News Feeds', 'com_newsfeeds', 1, 'js/ThemeOffice/edit.png', 0, 'show_headings=1\nshow_name=1\nshow_articles=1\nshow_link=1\nshow_cat_description=1\nshow_cat_items=1\nshow_feed_image=1\nshow_feed_description=1\nshow_item_description=1\nfeed_word_count=0\n\n', 1),
(13, 'Categories', '', 0, 11, 'option=com_categories&section=com_newsfeeds', 'Manage Categories', '', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(14, 'User', 'option=com_user', 0, 0, '', '', 'com_user', 0, '', 1, '', 1),
(15, 'Search', 'option=com_search', 0, 0, 'option=com_search', 'Search Statistics', 'com_search', 0, 'js/ThemeOffice/component.png', 1, 'enabled=0\n\n', 1),
(16, 'Categories', '', 0, 1, 'option=com_categories&section=com_banner', 'Categories', '', 3, '', 1, '', 1),
(17, 'Wrapper', 'option=com_wrapper', 0, 0, '', 'Wrapper', 'com_wrapper', 0, '', 1, '', 1),
(18, 'Mail To', '', 0, 0, '', '', 'com_mailto', 0, '', 1, '', 1),
(19, 'Media Manager', '', 0, 0, 'option=com_media', 'Media Manager', 'com_media', 0, '', 1, 'upload_extensions=bmp,csv,doc,epg,gif,ico,jpg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,EPG,GIF,ICO,JPG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS\nupload_maxsize=10000000\nfile_path=images\nimage_path=images/stories\nrestrict_uploads=1\nallowed_media_usergroup=3\ncheck_mime=1\nimage_extensions=bmp,gif,jpg,png\nignore_extensions=\nupload_mime=image/jpeg,image/gif,image/png,image/bmp,application/x-shockwave-flash,application/msword,application/excel,application/pdf,application/powerpoint,text/plain,application/x-zip\nupload_mime_illegal=text/html\nenable_flash=0\n\n', 1),
(20, 'Articles', 'option=com_content', 0, 0, '', '', 'com_content', 0, '', 1, 'show_noauth=0\nshow_title=1\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\nfeed_summary=0\n\n', 1),
(21, 'Configuration Manager', '', 0, 0, '', 'Configuration', 'com_config', 0, '', 1, '', 1),
(22, 'Installation Manager', '', 0, 0, '', 'Installer', 'com_installer', 0, '', 1, '', 1),
(23, 'Language Manager', '', 0, 0, '', 'Languages', 'com_languages', 0, '', 1, 'administrator=fr-FR\nsite=fr-FR', 1),
(24, 'Mass mail', '', 0, 0, '', 'Mass Mail', 'com_massmail', 0, '', 1, 'mailSubjectPrefix=\nmailBodySuffix=\n\n', 1),
(25, 'Menu Editor', '', 0, 0, '', 'Menu Editor', 'com_menus', 0, '', 1, '', 1),
(27, 'Messaging', '', 0, 0, '', 'Messages', 'com_messages', 0, '', 1, '', 1),
(28, 'Modules Manager', '', 0, 0, '', 'Modules', 'com_modules', 0, '', 1, '', 1),
(29, 'Plugin Manager', '', 0, 0, '', 'Plugins', 'com_plugins', 0, '', 1, '', 1),
(30, 'Template Manager', '', 0, 0, '', 'Templates', 'com_templates', 0, '', 1, '', 1),
(31, 'User Manager', '', 0, 0, '', 'Users', 'com_users', 0, '', 1, 'allowUserRegistration=1\nnew_usertype=Registered\nuseractivation=1\nfrontend_userparams=1\n\n', 1),
(32, 'Cache Manager', '', 0, 0, '', 'Cache', 'com_cache', 0, '', 1, '', 1),
(33, 'Control Panel', '', 0, 0, '', 'Control Panel', 'com_cpanel', 0, '', 1, '', 1),
(42, 'Gavick PhotoSlide GK2', 'option=com_gk2_photoslide', 0, 0, 'option=com_gk2_photoslide', 'Gavick PhotoSlide GK2', 'com_gk2_photoslide', 0, 'components/com_gk2_photoslide/interface/images/com_logo_gk2.png', 0, '', 1),
(35, 'Gavick Tabs Manager GK2', 'option=com_gk2_tabs_manager', 0, 0, 'option=com_gk2_tabs_manager', 'Gavick Tabs Manager GK2', 'com_gk2_tabs_manager', 0, 'components/com_gk2_tabs_manager/interface/images/com_logo_gk2.png', 0, '', 1),
(36, 'JCE', 'option=com_jce', 0, 0, 'option=com_jce', 'JCE', 'com_jce', 0, 'components/com_jce/img/logo.png', 0, '', 1),
(37, 'JCE MENU CPANEL', '', 0, 36, 'option=com_jce', 'JCE MENU CPANEL', 'com_jce', 0, 'templates/khepri/images/menu/icon-16-cpanel.png', 0, '', 1),
(38, 'JCE MENU CONFIG', '', 0, 36, 'option=com_jce&type=config', 'JCE MENU CONFIG', 'com_jce', 1, 'templates/khepri/images/menu/icon-16-config.png', 0, '', 1),
(39, 'JCE MENU GROUPS', '', 0, 36, 'option=com_jce&type=group', 'JCE MENU GROUPS', 'com_jce', 2, 'templates/khepri/images/menu/icon-16-user.png', 0, '', 1),
(40, 'JCE MENU PLUGINS', '', 0, 36, 'option=com_jce&type=plugin', 'JCE MENU PLUGINS', 'com_jce', 3, 'templates/khepri/images/menu/icon-16-plugin.png', 0, '', 1),
(41, 'JCE MENU INSTALL', '', 0, 36, 'option=com_jce&type=install', 'JCE MENU INSTALL', 'com_jce', 4, 'templates/khepri/images/menu/icon-16-install.png', 0, '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `jos_contact_details`
--

CREATE TABLE IF NOT EXISTS `jos_contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `imagepos` varchar(20) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `jos_contact_details`
--

INSERT INTO `jos_contact_details` (`id`, `name`, `alias`, `con_position`, `address`, `suburb`, `state`, `country`, `postcode`, `telephone`, `fax`, `misc`, `image`, `imagepos`, `email_to`, `default_con`, `published`, `checked_out`, `checked_out_time`, `ordering`, `params`, `user_id`, `catid`, `access`, `mobile`, `webpage`) VALUES
(1, 'March�s Public Togo', 'marches-public-togo', 'Position', 'Togo', 'Lom�', 'State', 'Togo', '', '+222 000 000 00 0', '+222 000 0 00 00 0', 'March� Public Niger', '', 'top', 'infos@togomp.com', 0, 1, 0, '0000-00-00 00:00:00', 1, 'show_name=1\nshow_position=1\nshow_email=0\nshow_street_address=1\nshow_suburb=1\nshow_state=1\nshow_postcode=1\nshow_country=1\nshow_telephone=1\nshow_mobile=1\nshow_fax=1\nshow_webpage=1\nshow_misc=1\nshow_image=1\nallow_vcard=0\ncontact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_mobile=\nicon_fax=\nicon_misc=\nshow_email_form=1\nemail_description=1\nshow_email_copy=1\nbanned_email=\nbanned_subject=\nbanned_text=', 0, 12, 0, '', '');

-- --------------------------------------------------------

--
-- Structure de la table `jos_content`
--

CREATE TABLE IF NOT EXISTS `jos_content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `title_alias` varchar(255) NOT NULL DEFAULT '',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `sectionid` int(11) unsigned NOT NULL DEFAULT '0',
  `mask` int(11) unsigned NOT NULL DEFAULT '0',
  `catid` int(11) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` text NOT NULL,
  `version` int(11) unsigned NOT NULL DEFAULT '1',
  `parentid` int(11) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(11) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `metadata` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_section` (`sectionid`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=62 ;

--
-- Contenu de la table `jos_content`
--

INSERT INTO `jos_content` (`id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`) VALUES
(1, 'Atelier national de lancement du site web r�gional des march�s publics au Togo', 'atelier-national-de-lancement-du-site-web-regional-des-marches-publics-au-togo', '', '<div align="left">\r\n<table border="0" style="color: #666666; text-align: left; font-family: Arial, Helvetica, sans-serif; font-size: 11px; width: 837px; height: 1130px;">\r\n<tbody>\r\n<tr>\r\n<td style="font-family: Arial, Helvetica, sans-serif; padding: 0px 6px;"><span style="font-size: small; outline-width: 0px;"><span style="font-size: small; outline-width: 0px;">Du mercredi&nbsp;9 au vendredi 11 mai&nbsp;2012, s�est tenu � l''h�tel&nbsp;GAWEYE � Niamey, un atelier de formation sur le Syst�me d�Information R�gional des March�s Publics (SIRMP), sous la pr�sidence de M. TAHIROU Abdou, Directeur des Finances Publiques et de la Fiscalit� Int�rieure de la Commission de l�Union Economique et Mon�taire Ouest Africain (UEMOA).</span></span>\r\n<p style="outline-width: 0px; text-align: justify;"><span style="font-size: small; outline-width: 0px;">Cet atelier a r�uni autour d�une m�me table, les points focaux des march�s publics des minist�res, des collectivit�s d�concentr�es, de l�organe de r�gulation, de l�organe de contr�le et des repr�sentants du Projet de R�forme des March�s Publics (PRMP) de la commission de l�UEMOA.&nbsp;<br /></span></p>\r\n<p style="outline-width: 0px;"><span style="font-size: small; outline-width: 0px;">L�atelier s�est d�roul� en deux grandes phases d�velopp�es ci-dessous :</span></p>\r\n<ul style="margin-right: 12px; margin-left: 12px; list-style-image: none; padding-right: 6px; padding-bottom: 1px; padding-left: 6px; outline-width: 0px; font-family: Tahoma, Geneva, sans-serif;">\r\n<li style="padding-top: 0px; padding-bottom: 0px; background-image: url(''http://176.31.126.94/nigermp3/templates/sportline/images/strelica_green.gif''); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; outline-width: 0px; background-position: 0px 50%; background-repeat: no-repeat no-repeat;"><span style="font-size: small; outline-width: 0px;">la c�r�monie d�ouverture</span></li>\r\n<li style="padding-top: 0px; padding-bottom: 0px; background-image: url(''http://176.31.126.94/nigermp3/templates/sportline/images/strelica_green.gif''); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; outline-width: 0px; background-position: 0px 50%; background-repeat: no-repeat no-repeat;"><span style="font-size: small; outline-width: 0px;">les travaux de l�atelier.</span></li>\r\n</ul>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" style="font-family: Arial, Helvetica, sans-serif; padding: 0px 6px;">&nbsp;\r\n<table border="0" style="width: 866px; height: 793px;">\r\n<tbody>\r\n<tr>\r\n<td colspan="2" style="font-family: Arial, Helvetica, sans-serif; padding: 0px 6px;">\r\n<p style="outline-width: 0px;"><span style="font-size: small; outline-width: 0px;"><strong style="font-size: 13px; outline-width: 0px;">La c�r�monie d�ouverture</strong>&nbsp;de l�Atelier a �t� pr�sid�e M. MAIDAGI Abdou,&nbsp;Directeur de Cabinet&nbsp;du Minist�re de l''Economie et des Finances, repr�sentant le Ministre de l�Economie et des Finances, en pr�sence de M. MADOU Mahamadou,&nbsp;Secr�taire Ex�cutif&nbsp;de l''Agence&nbsp;de R�gulation des March�s Publics&nbsp;(ARMP), Mme&nbsp;RABO Fatchima&nbsp;Directrice G�n�rale&nbsp;du Contr�le des March�s Publics et de M.&nbsp;DIOP Abdoulaye&nbsp;Repr�sentant R�sident de la Commission de l''UEMOA au Niger.</span></p>\r\n<p style="outline-width: 0px; text-align: justify;"><span style="font-size: small; outline-width: 0px;">Deux allocutions ont meubl� cette &nbsp;c�r�monie. La premi�re a �t� faite par M. Abdou TAHIROU qui a d�abord indiqu� qu�une vaste r�forme des march�s publics de l�espace UEMOA a commenc� depuis les ann�es 2000, avec l�adoption du code de transparence dans les finances publiques suivi plus tard par deux directives sur les march�s publics et les d�l�gations de service public. Il a poursuivi en indiquant que dans la m�me logique, il a �t� mis en place un site internet pour la vulgarisation des informations ayant trait au domaine des march�s publics dans l�espace UEMOA. Pour finir, M. TAHIROU a indiqu� que cet atelier a pour but de pr�senter le Syst�me d�Information R�gional des March�s Publics (SIRMP) et de former les points focaux � l�utilisation de cet outil r�gional.</span></p>\r\n<p style="outline-width: 0px; text-align: justify;"><span style="font-size: small; outline-width: 0px;">Prenant la parole pour la seconde allocution, M.&nbsp;MAIDAGI Abdou a remerci� tous les acteurs de la commande publique pour leur pr�sence � cet atelier.&nbsp;Selon lui, ce petit noyau que constitue les points focaux, devra � l�issue ce s�minaire de formation, participer � v�hiculer l�information sur les march�s publics, gage de bonne gouvernance sur le plan communautaire. Il a ensuite d�clar�, au nom du Ministre de l�Economie et des Finances, ouvert, l�Atelier de formation sur le SIRMP.</span></p>\r\n<p style="outline-width: 0px; text-align: justify;"><span style="font-size: small; outline-width: 0px;"><strong style="font-size: 13px; outline-width: 0px;">Les travaux de l�atelier</strong>&nbsp;ont effectivement d�but� sous la pr�sidence de M. TAHIROU Abdou. Le programme suivant a �t� d�roul� durant les trois jours de travaux par M. KOGOE Jean-Baptiste, Expert Informaticien du PRMP:</span></p>\r\n<ul style="margin-right: 12px; margin-left: 12px; list-style-image: none; padding-right: 6px; padding-bottom: 1px; padding-left: 6px; outline-width: 0px; font-family: Tahoma, Geneva, sans-serif;">\r\n<li style="padding-top: 0px; padding-bottom: 0px; background-image: url(''http://176.31.126.94/nigermp3/templates/sportline/images/strelica_green.gif''); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; outline-width: 0px; background-position: 0px 50%; background-repeat: no-repeat no-repeat;"><span style="font-size: small; outline-width: 0px;">premi�re journ�e : pr�sentation du syst�me r�gional des marches publics et du site web r�gional</span></li>\r\n<li style="padding-top: 0px; padding-bottom: 0px; background-image: url(''http://176.31.126.94/nigermp3/templates/sportline/images/strelica_green.gif''); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; outline-width: 0px; background-position: 0px 50%; background-repeat: no-repeat no-repeat;"><span style="font-size: small; outline-width: 0px;">deuxi�me journ�e : pr�sentation des profils</span></li>\r\n<li style="padding-top: 0px; padding-bottom: 0px; background-image: url(''http://176.31.126.94/nigermp3/templates/sportline/images/strelica_green.gif''); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; outline-width: 0px; background-position: 0px 50%; background-repeat: no-repeat no-repeat;"><span style="font-size: small; outline-width: 0px;">troisi�me journ�e : saisie des plans de passation</span></li>\r\n</ul>\r\n<p style="outline-width: 0px;"><span style="font-size: small; outline-width: 0px;"><strong style="font-size: 13px; outline-width: 0px;">Au terme de l�Atelier de formation</strong>, le Pr�sident de s�ance, M. TAHIROU Abdou a remerci� tous les participants pour leur assiduit� et leur disponibilit� durant ces trois (3) jours de formation. Il a annonc� que la mise en place du SIRMP n�est qu�une �tape dans le vaste chantier de r�forme des finances publiques de l�espace communautaire et que la r�forme ne touche pas seulement les march�s publics mais tous les pans des finances publiques. Il a termin� son propos en souhaitant que les acteurs form�s au cours de cet atelier puissent &nbsp;gagner le challenge de la bonne gouvernance � travers la mise en �uvre de la transparence dans les march�s publics dans l�espace communautaire. Une mise en �uvre qui se traduit par une bonne et fr�quente alimentation du portail r�gional.</span></p>\r\n<p style="outline-width: 0px;"><span style="font-size: small; outline-width: 0px;">L''atelier a en outre �t� tr�s b�nifique aux participants car il leur a permi :</span></p>\r\n<p style="outline-width: 0px;"><span style="font-size: small; outline-width: 0px;">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; une ma�trise des nouvelles r�gles de publication des avis d�appels d�offres dans l�espace&nbsp;&nbsp;&nbsp;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UEMOA ;</span></p>\r\n<p style="outline-width: 0px;"><span style="font-size: small; outline-width: 0px;">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; une meilleure connaissance du site web r�gional des march�s publics ;</span></p>\r\n<p style="outline-width: 0px;"><span style="font-size: small; outline-width: 0px;">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; une ma�trise des proc�dures et m�thodes de publication des informations sur le site web&nbsp;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; r�gional des march�s&nbsp;publics ;</span></p>\r\n<p style="outline-width: 0px;"><span style="font-size: small; outline-width: 0px;">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; une meilleure visibilit� sur les futures �volutions du syst�me d�information r�gional des march�s&nbsp;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;publics ;&nbsp;&nbsp;</span></p>\r\n<p style="outline-width: 0px;"><span style="font-size: small; outline-width: 0px;">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; une ma�trise des crit�res de surveillance multilat�rale des march�s publics dans l�espace&nbsp;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UEMOA ;</span></p>\r\n<p style="outline-width: 0px;"><span style="font-size: small; outline-width: 0px;">-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; la saisie des donn�es 2012 relatives aux plans de passation des march�s, aux avis d�appels&nbsp;&nbsp;&nbsp;&nbsp;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d�offres, aux avis&nbsp;d�attributions, aux contentieux, aux statistiques, aux textes r�glementaires,&nbsp;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; aux programmes de formation, � la&nbsp;liste noire, etc.</span></p>\r\n<p style="outline-width: 0px;"><span style="font-size: small; outline-width: 0px;">&nbsp;Les adresses du site web r�gional des march�s publics sont :&nbsp;<a href="http://www.marchespublics-uemoa.net/undefined/" style="color: #000000;"><span style="font-size: small; outline-width: 0px;">www.marchespublics-uemoa.net</span></a><span style="font-size: small; outline-width: 0px;">,&nbsp;<a href="http://www.marchespublics-uemoa.org/" style="color: #000000;"><span style="font-size: small; outline-width: 0px;">www.marchespublics-uemoa.org</span></a><span style="font-size: small; outline-width: 0px;">&nbsp;et<a href="http://www.marchespublics.uemoa.int/" style="color: #000000;"><span style="font-size: small; outline-width: 0px;">www.marchespublics.uemoa.int</span></a></span></span></span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>', '', 1, 1, 0, 1, '2008-08-12 10:00:00', 62, '', '2012-11-27 13:04:05', 62, 0, '0000-00-00 00:00:00', '2006-01-03 01:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 37, 0, 2, '', '', 0, 102, 'robots=\nauthor='),
(2, 'Newsflash 1', 'newsflash-1', '', '<p>Joomla! makes it easy to launch a Web site of any kind. Whether you want a brochure site or you are building a large online community, Joomla! allows you to deploy a new site in minutes and add extra functionality as you need it. The hundreds of available Extensions will help to expand your site and allow you to deliver new services that extend your reach into the Internet.</p>', '', 1, 1, 0, 3, '2008-08-10 06:30:34', 62, '', '2008-08-10 06:30:34', 62, 0, '0000-00-00 00:00:00', '2004-08-09 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 6, '', '', 0, 1, 'robots=\nauthor='),
(3, 'Newsflash 2', 'newsflash-2', '', '<p>The one thing about a Web site, it always changes! Joomla! makes it easy to add Articles, content, images, videos, and more. Site administrators can edit and manage content ''in-context'' by clicking the ''Edit'' link. Webmasters can also edit content through a graphical Control Panel that gives you complete control over your site.</p>', '', 1, 1, 0, 3, '2008-08-09 22:30:34', 62, '', '2008-08-09 22:30:34', 62, 0, '0000-00-00 00:00:00', '2004-08-09 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 7, '', '', 0, 2, 'robots=\nauthor='),
(4, 'Newsflash 3', 'newsflash-3', '', '<p>With a library of thousands of free <a href="http://extensions.joomla.org" target="_blank" title="The Joomla! Extensions Directory">Extensions</a>, you can add what you need as your site grows. Don''t wait, look through the <a href="http://extensions.joomla.org/" target="_blank" title="Joomla! Extensions">Joomla! Extensions</a>  library today. </p>', '', 1, 1, 0, 3, '2008-08-10 06:30:34', 62, '', '2008-08-10 06:30:34', 62, 0, '0000-00-00 00:00:00', '2004-08-09 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 8, '', '', 0, 1, 'robots=\nauthor='),
(5, 'Joomla! License Guidelines', 'joomla-license-guidelines', 'joomla-license-guidelines', '<p>This Web site is powered by <a href="http://joomla.org/" target="_blank" title="Joomla!">Joomla!</a> The software and default templates on which it runs are Copyright 2005-2008 <a href="http://www.opensourcematters.org/" target="_blank" title="Open Source Matters">Open Source Matters</a>. The sample content distributed with Joomla! is licensed under the <a href="http://docs.joomla.org/JEDL" target="_blank" title="Joomla! Electronic Document License">Joomla! Electronic Documentation License.</a> All data entered into this Web site and templates added after installation, are copyrighted by their respective copyright owners.</p> <p>If you want to distribute, copy, or modify Joomla!, you are welcome to do so under the terms of the <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0.html#SEC1" target="_blank" title="GNU General Public License"> GNU General Public License</a>. If you are unfamiliar with this license, you might want to read <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0.html#SEC4" target="_blank" title="How To Apply These Terms To Your Program">''How To Apply These Terms To Your Program''</a> and the <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0-faq.html" target="_blank" title="GNU General Public License FAQ">''GNU General Public License FAQ''</a>.</p> <p>The Joomla! licence has always been GPL.</p>', '', 1, 4, 0, 25, '2008-08-20 10:11:07', 62, '', '2008-08-20 10:11:07', 62, 0, '0000-00-00 00:00:00', '2004-08-19 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 3, '', '', 0, 116, 'robots=\nauthor='),
(6, 'We are Volunteers', 'we-are-volunteers', '', '<p>The Joomla Core Team and Working Group members are volunteer developers, designers, administrators and managers who have worked together to take Joomla! to new heights in its relatively short life. Joomla! has some wonderfully talented people taking Open Source concepts to the forefront of industry standards.  Joomla! 1.5 is a major leap forward and represents the most exciting Joomla! release in the history of the project. </p>', '', 0, 1, 0, 1, '2007-07-07 09:54:06', 62, '', '2007-07-07 09:54:06', 62, 0, '0000-00-00 00:00:00', '2004-07-06 22:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 10, 0, 5, '', '', 0, 67, 'robots=\nauthor='),
(9, 'Millions of Smiles', 'millions-of-smiles', '', '<p>The Joomla! team has millions of good reasons to be smiling about the Joomla! 1.5. In its current incarnation, it''s had millions of downloads, taking it to an unprecedented level of popularity.  The new code base is almost an entire re-factor of the old code base.  The user experience is still extremely slick but for developers the API is a dream.  A proper framework for real PHP architects seeking the best of the best.</p><p>If you''re a former Mambo User or a 1.0 series Joomla! User, 1.5 is the future of CMSs for a number of reasons.  It''s more powerful, more flexible, more secure, and intuitive.  Our developers and interface designers have worked countless hours to make this the most exciting release in the content management system sphere.</p><p>Go on ... get your FREE copy of Joomla! today and spread the word about this benchmark project. </p>', '', 0, 1, 0, 1, '2007-07-07 09:54:06', 62, '', '2007-07-07 09:54:06', 62, 0, '0000-00-00 00:00:00', '2004-07-06 22:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 5, 0, 6, '', '', 0, 25, 'robots=\nauthor='),
(10, 'How do I localise Joomla! to my language?', 'how-do-i-localise-joomla-to-my-language', '', '<h4>General<br /></h4><p>In Joomla! 1.5 all User interfaces can be localised. This includes the installation, the Back-end Control Panel and the Front-end Site.</p><p>The core release of Joomla! 1.5 is shipped with  language choices in the installation but, other than English (the default), languages for the Site and Administration interfaces need to be added after installation. Links to such language packs exist below.</p>', '<p>Translation Teams for Joomla! 1.5 may have also released fully localised installation packages where site, administrator and sample data are in the local language. These localised releases can be found in the specific team projects on the <a href="http://extensions.joomla.org/component/option,com_mtree/task,listcats/cat_id,1837/Itemid,35/" target="_blank" title="JED">Joomla! Extensions Directory</a>.</p><h4>How do I install language packs?</h4><ul><li>First download both the admin and the site language packs that you require.</li><li>Install each pack separately using the Extensions-&gt;Install/Uninstall Menu selection and then the package file upload facility.</li><li>Go to the Language Manager and be sure to select Site or Admin in the sub-menu. Then select the appropriate language and make it the default one using the Toolbar button.</li></ul><h4>How do I select languages?</h4><ul><li>Default languages can be independently set for Site and for Administrator</li><li>In addition, users can define their preferred language for each Site and Administrator. This takes affect after logging in.</li><li>While logging in to the Administrator Back-end, a language can also be selected for the particular session.</li></ul><h4>Where can I find Language Packs and Localised Releases?</h4><p><em>Please note that Joomla! 1.5 is new and language packs for this version may have not been released at this time.</em> </p><ul><li><a href="http://joomlacode.org/gf/project/jtranslation/" target="_blank" title="Accredited Translations">The Joomla! Accredited Translations Project</a>  - This is a joint repository for language packs that were developed by teams that are members of the Joomla! Translations Working Group.</li><li><a href="http://extensions.joomla.org/component/option,com_mtree/task,listcats/cat_id,1837/Itemid,35/" target="_blank" title="Translations">The Joomla! Extensions Site - Translations</a>  </li><li><a href="http://community.joomla.org/translations.html" target="_blank" title="Translation Work Group Teams">List of Translation Teams and Translation Partner Sites for Joomla! 1.5</a> </li></ul>', 1, 3, 0, 32, '2008-07-30 14:06:37', 62, '', '2008-07-30 14:06:37', 62, 0, '0000-00-00 00:00:00', '2006-09-29 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 9, 0, 5, '', '', 0, 10, 'robots=\nauthor='),
(11, 'How do I upgrade to Joomla! 1.5 ?', 'how-do-i-upgrade-to-joomla-15', '', '<p>Joomla! 1.5 does not provide an upgrade path from earlier versions. Converting an older site to a Joomla! 1.5 site requires creation of a new empty site using Joomla! 1.5 and then populating the new site with the content from the old site. This migration of content is not a one-to-one process and involves conversions and modifications to the content dump.</p> <p>There are two ways to perform the migration:</p>', ' <div id="post_content-107"><li>An automated method of migration has been provided which uses a migrator Component to create the migration dump out of the old site (Mambo 4.5.x up to Joomla! 1.0.x) and a smart import facility in the Joomla! 1.5 Installation that performs required conversions and modifications during the installation process.</li> <li>Migration can be performed manually. This involves exporting the required tables, manually performing required conversions and modifications and then importing the content to the new site after it is installed.</li>  <p><!--more--></p> <h2><strong> Automated migration</strong></h2>  <p>This is a two phased process using two tools. The first tool is a migration Component named <font face="courier new,courier">com_migrator</font>. This Component has been contributed by Harald Baer and is based on his <strong>eBackup </strong>Component. The migrator needs to be installed on the old site and when activated it prepares the required export dump of the old site''s data. The second tool is built into the Joomla! 1.5 installation process. The exported content dump is loaded to the new site and all conversions and modification are performed on-the-fly.</p> <h3><u> Step 1 - Using com_migrator to export data from old site:</u></h3> <li>Install the <font face="courier new,courier">com_migrator</font> Component on the <u><strong>old</strong></u> site. It can be found at the <a href="http://joomlacode.org/gf/project/pasamioprojects/frs/" target="_blank" title="JoomlaCode">JoomlaCode developers forge</a>.</li> <li>Select the Component in the Component Menu of the Control Panel.</li> <li>Click on the <strong>Dump it</strong> icon. Three exported <em>gzipped </em>export scripts will be created. The first is a complete backup of the old site. The second is the migration content of all core elements which will be imported to the new site. The third is a backup of all 3PD Component tables.</li> <li>Click on the download icon of the particular exports files needed and store locally.</li> <li> export sets can be created.</li> <li>The exported data is not modified in anyway and the original encoding is preserved. This makes the <font face="courier new,courier">com_migrator</font> tool a recommended tool to use for manual migration as well.</li> <h3><u> Step 2 - Using the migration facility to import and convert data during Joomla! 1.5 installation:</u></h3><p>Note: This function requires the use of the <em><font face="courier new,courier">iconv </font></em>function in PHP to convert encodings. If <em><font face="courier new,courier">iconv </font></em>is not found a warning will be provided.</p> <li>In step 6 - Configuration select the ''Load Migration Script'' option in the ''Load Sample Data, Restore or Migrate Backed Up Content'' section of the page.</li> <li>Enter the table prefix used in the content dump. For example: ''jos_'' or ''site2_'' are acceptable values.</li> <li>Select the encoding of the dumped content in the dropdown list. This should be the encoding used on the pages of the old site. (As defined in the _ISO variable in the language file or as seen in the browser page info/encoding/source)</li> <li>Browse the local host and select the migration export and click on <strong>Upload and Execute</strong></li> <li>A success message should appear or alternately a listing of database errors</li> <li>Complete the other required fields in the Configuration step such as Site Name and Admin details and advance to the final step of installation. (Admin details will be ignored as the imported data will take priority. Please remember admin name and password from the old site)</li> <p><u><br /></u></p></div>', 1, 3, 0, 28, '2008-07-30 20:27:52', 62, '', '2008-07-30 20:27:52', 62, 0, '0000-00-00 00:00:00', '2006-09-29 12:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 10, 0, 3, '', '', 0, 15, 'robots=\nauthor='),
(12, 'Why does Joomla! 1.5 use UTF-8 encoding?', 'why-does-joomla-15-use-utf-8-encoding', '', '<p>Well... how about never needing to mess with encoding settings again?</p><p>Ever needed to display several languages on one page or site and something always came up in Giberish?</p><p>With utf-8 (a variant of Unicode) glyphs (character forms) of basically all languages can be displayed with one single encoding setting. </p>', '', 1, 3, 0, 31, '2008-08-05 01:11:29', 62, '', '2008-08-05 01:11:29', 62, 0, '0000-00-00 00:00:00', '2006-10-03 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 8, 0, 8, '', '', 0, 29, 'robots=\nauthor='),
(13, 'What happened to the locale setting?', 'what-happened-to-the-locale-setting', '', 'This is now defined in the Language [<em>lang</em>].xml file in the Language metadata settings. If you are having locale problems such as dates do not appear in your language for example, you might want to check/edit the entries in the locale tag. Note that  locale strings can be set and the host will usually accept the first one recognised.', '', 1, 3, 0, 28, '2008-08-06 16:47:35', 62, '', '2008-08-06 16:47:35', 62, 0, '0000-00-00 00:00:00', '2006-10-05 14:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 2, '', '', 0, 11, 'robots=\nauthor='),
(14, 'What is the FTP layer for?', 'what-is-the-ftp-layer-for', '', '<p>The FTP Layer allows file operations (such as installing Extensions or updating the main configuration file) without having to make all the folders and files writable. This has been an issue on Linux and other Unix based platforms in respect of file permissions. This makes the site admin''s life a lot easier and increases security of the site.</p><p>You can check the write status of relevent folders by going to ''''Help-&gt;System Info" and then in the sub-menu to "Directory Permissions". With the FTP Layer enabled even if all directories are red, Joomla! will operate smoothly.</p><p>NOTE: the FTP layer is not required on a Windows host/server. </p>', '', 1, 3, 0, 31, '2008-08-06 21:27:49', 62, '', '2008-08-06 21:27:49', 62, 0, '0000-00-00 00:00:00', '2006-10-05 16:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=', 6, 0, 6, '', '', 0, 25, 'robots=\nauthor='),
(15, 'Can Joomla! 1.5 operate with PHP Safe Mode On?', 'can-joomla-15-operate-with-php-safe-mode-on', '', '<p>Yes it can! This is a significant security improvement.</p><p>The <em>safe mode</em> limits PHP to be able to perform actions only on files/folders who''s owner is the same as PHP is currently using (this is usually ''apache''). As files normally are created either by the Joomla! application or by FTP access, the combination of PHP file actions and the FTP Layer allows Joomla! to operate in PHP Safe Mode.</p>', '', 1, 3, 0, 31, '2008-08-06 19:28:35', 62, '', '2008-08-06 19:28:35', 62, 0, '0000-00-00 00:00:00', '2006-10-05 14:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 4, '', '', 0, 8, 'robots=\nauthor='),
(16, 'Only one edit window! How do I create "Read more..."?', 'only-one-edit-window-how-do-i-create-read-more', '', '<p>This is now implemented by inserting a <strong>Read more...</strong> tag (the button is located below the editor area) a dotted line appears in the edited text showing the split location for the <em>Read more....</em> A new Plugin takes care of the rest.</p><p>It is worth mentioning that this does not have a negative effect on migrated data from older sites. The new implementation is fully backward compatible.</p>', '', 1, 3, 0, 28, '2008-08-06 19:29:28', 62, '', '2008-08-06 19:29:28', 62, 0, '0000-00-00 00:00:00', '2006-10-05 14:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 4, '', '', 0, 22, 'robots=\nauthor='),
(17, 'My MySQL database does not support UTF-8. Do I have a problem?', 'my-mysql-database-does-not-support-utf-8-do-i-have-a-problem', '', 'No you don''t. Versions of MySQL lower than 4.1 do not have built in UTF-8 support. However, Joomla! 1.5 has made provisions for backward compatibility and is able to use UTF-8 on older databases. Let the installer take care of all the settings and there is no need to make any changes to the database (charset, collation, or any other).', '', 1, 3, 0, 31, '2008-08-07 09:30:37', 62, '', '2008-08-07 09:30:37', 62, 0, '0000-00-00 00:00:00', '2006-10-05 20:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 10, 0, 7, '', '', 0, 13, 'robots=\nauthor='),
(18, 'Joomla! Features', 'joomla-features', '', '<h4><font color="#ff6600">Joomla! features:</font></h4> <ul><li>Completely database driven site engines </li><li>News, products, or services sections fully editable and manageable</li><li>Topics sections can be added to by contributing Authors </li><li>Fully customisable layouts including <em>left</em>, <em>center</em>, and <em>right </em>Menu boxes </li><li>Browser upload of images to your own library for use anywhere in the site </li><li>Dynamic Forum/Poll/Voting booth for on-the-spot results </li><li>Runs on Linux, FreeBSD, MacOSX server, Solaris, and AIX', '  </li></ul> <h4>Extensive Administration:</h4> <ul><li>Change order of objects including news, FAQs, Articles etc. </li><li>Random Newsflash generator </li><li>Remote Author submission Module for News, Articles, FAQs, and Links </li><li>Object hierarchy - as many Sections, departments, divisions, and pages as you want </li><li>Image library - store all your PNGs, PDFs, DOCs, XLSs, GIFs, and JPEGs online for easy use </li><li>Automatic Path-Finder. Place a picture and let Joomla! fix the link </li><li>News Feed Manager. Easily integrate news feeds into your Web site.</li><li>E-mail a friend and Print format available for every story and Article </li><li>In-line Text editor similar to any basic word processor software </li><li>User editable look and feel </li><li>Polls/Surveys - Now put a different one on each page </li><li>Custom Page Modules. Download custom page Modules to spice up your site </li><li>Template Manager. Download Templates and implement them in seconds </li><li>Layout preview. See how it looks before going live </li><li>Banner Manager. Make money out of your site.</li></ul>', 1, 4, 0, 29, '2008-08-08 23:32:45', 62, '', '2008-08-08 23:32:45', 62, 0, '0000-00-00 00:00:00', '2006-10-07 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 11, 0, 4, '', '', 0, 59, 'robots=\nauthor='),
(19, 'Joomla! Overview', 'joomla-overview', '', '<p>If you''re new to Web publishing systems, you''ll find that Joomla! delivers sophisticated solutions to your online needs. It can deliver a robust enterprise-level Web site, empowered by endless extensibility for your bespoke publishing needs. Moreover, it is often the system of choice for small business or home users who want a professional looking site that''s simple to deploy and use. <em>We do content right</em>.<br /> </p><p>So what''s the catch? How much does this system cost?</p><p> Well, there''s good news ... and more good news! Joomla! 1.5 is free, it is released under an Open Source license - the GNU/General Public License v 2.0. Had you invested in a mainstream, commercial alternative, there''d be nothing but moths left in your wallet and to add new functionality would probably mean taking out a second mortgage each time you wanted something adding!</p><p>Joomla! changes all that ... <br />Joomla! is different from the normal models for content management software. For a start, it''s not complicated. Joomla! has been developed for everybody, and anybody can develop it further. It is designed to work (primarily) with other Open Source, free, software such as PHP, MySQL, and Apache. </p><p>It is easy to install and administer, and is reliable. </p><p>Joomla! doesn''t even require the user or administrator of the system to know HTML to operate it once it''s up and running.</p><p>To get the perfect Web site with all the functionality that you require for your particular application may take additional time and effort, but with the Joomla! Community support that is available and the many Third Party Developers actively creating and releasing new Extensions for the 1.5 platform on an almost daily basis, there is likely to be something out there to meet your needs. Or you could develop your own Extensions and make these available to the rest of the community. </p>', '', 1, 4, 0, 29, '2008-08-09 07:49:20', 62, '', '2008-08-09 07:49:20', 62, 0, '0000-00-00 00:00:00', '2006-10-07 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 13, 0, 2, '', '', 0, 216, 'robots=\nauthor='),
(20, 'Support and Documentation', 'support-and-documentation', '', '<h1>Support </h1><p>Support for the Joomla! CMS can be found on several places. The best place to start would be the <a href="http://docs.joomla.org/" target="_blank" title="Joomla! Official Documentation Wiki">Joomla! Official Documentation Wiki</a>. Here you can help yourself to the information that is regularly published and updated as Joomla! develops. There is much more to come too!</p> <p>Of course you should not forget the Help System of the CMS itself. On the <em>topmenu </em>in the Back-end Control panel you find the Help button which will provide you with lots of explanation on features.</p> <p>Another great place would of course be the <a href="http://forum.joomla.org/" target="_blank" title="Forum">Forum</a> . On the Joomla! Forum you can find help and support from Community members as well as from Joomla! Core members and Working Group members. The forum contains a lot of information, FAQ''s, just about anything you are looking for in terms of support.</p> <p>Two other resources for Support are the <a href="http://developer.joomla.org/" target="_blank" title="Joomla! Developer Site">Joomla! Developer Site</a> and the <a href="http://extensions.joomla.org/" target="_blank" title="Joomla! Extensions Directory">Joomla! Extensions Directory</a> (JED). The Joomla! Developer Site provides lots of technical information for the experienced Developer as well as those new to Joomla! and development work in general. The JED whilst not a support site in the strictest sense has many of the Extensions that you will need as you develop your own Web site.</p> <p>The Joomla! Developers and Bug Squad members are regularly posting their blog reports about several topics such as programming techniques and security issues.</p> <h1>Documentation</h1> <p>Joomla! Documentation can of course be found on the <a href="http://docs.joomla.org/" target="_blank" title="Joomla! Official Documentation Wiki">Joomla! Official Documentation Wiki</a>. You can find information for beginners, installation, upgrade, Frequently Asked Questions, developer topics, and a lot more. The Documentation Team helps oversee the wiki but you are invited to contribute content, as well.</p> <p>There are also books written about Joomla! You can find a listing of these books in the <a href="http://shop.joomla.org/" target="_blank" title="Joomla! Shop">Joomla! Shop</a>.</p>', '', 1, 4, 0, 25, '2008-08-09 08:33:57', 62, '', '2008-08-09 08:33:57', 62, 0, '0000-00-00 00:00:00', '2006-10-07 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 13, 0, 2, '', '', 0, 6, 'robots=\nauthor='),
(21, 'Joomla! Facts', 'joomla-facts', '', '<p>Here are some interesting facts about Joomla!</p><ul><li><span>Over 210,000 active registered Users on the <a href="http://forum.joomla.org" target="_blank" title="Joomla Forums">Official Joomla! community forum</a> and more on the many international community sites.</span><ul><li><span>over 1,000,000 posts in over 200,000 topics</span></li><li>over 1,200 posts per day</li><li>growing at 150 new participants each day!</li></ul></li><li><span>1168 Projects on the JoomlaCode (<a href="http://joomlacode.org/" target="_blank" title="JoomlaCode">joomlacode.org</a> ). All for open source addons by third party developers.</span><ul><li><span>Well over 6,000,000 downloads of Joomla! since the migration to JoomlaCode in March 2007.<br /></span></li></ul></li><li><span>Nearly 4,000 extensions for Joomla! have been registered on the <a href="http://extensions.joomla.org" target="_blank" title="http://extensions.joomla.org">Joomla! Extension Directory</a>  </span></li><li><span>Joomla.org exceeds 2 TB of traffic per month!</span></li></ul>', '', 1, 4, 0, 30, '2008-08-09 16:46:37', 62, '', '2008-08-09 16:46:37', 62, 0, '0000-00-00 00:00:00', '2006-10-07 14:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 13, 0, 1, '', '', 0, 50, 'robots=\nauthor='),
(22, 'What''s New in 1.5?', 'whats-new-in-15', '', '<p>As with previous releases, Joomla! provides a unified and easy-to-use framework for delivering content for Web sites of all kinds. To support the changing nature of the Internet and emerging Web technologies, Joomla! required substantial restructuring of its core functionality and we also used this effort to simplify many challenges within the current user interface. Joomla! 1.5 has many new features.</p>', '<p style="margin-bottom: 0in">In Joomla! 1.5, you''ll notice: </p>    <ul><li>     <p style="margin-bottom: 0in">       Substantially improved usability, manageability, and scalability far beyond the original Mambo foundations</p>   </li><li>     <p style="margin-bottom: 0in"> Expanded accessibility to support internationalisation, double-byte characters and right-to-left support for Arabic, Farsi, and Hebrew languages among others</p>   </li><li>     <p style="margin-bottom: 0in"> Extended integration of external applications through Web services and remote authentication such as the Lightweight Directory Access Protocol (LDAP)</p>   </li><li>     <p style="margin-bottom: 0in"> Enhanced content delivery, template and presentation capabilities to support accessibility standards and content delivery to any destination</p>   </li><li>     <p style="margin-bottom: 0in">       A more sustainable and flexible framework for Component and Extension developers</p>   </li><li>     <p style="margin-bottom: 0in">Backward compatibility with previous releases of Components, Templates, Modules, and other Extensions</p></li></ul>', 1, 4, 0, 29, '2008-08-11 22:13:58', 62, '', '2008-08-11 22:13:58', 62, 0, '0000-00-00 00:00:00', '2006-10-10 18:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 10, 0, 1, '', '', 0, 123, 'robots=\nauthor='),
(23, 'Platforms and Open Standards', 'platforms-and-open-standards', '', '<p class="MsoNormal">Joomla! runs on any platform including Windows, most flavours of Linux, several Unix versions, and the Apple OS/X platform.  Joomla! depends on PHP and the MySQL database to deliver dynamic content.  </p>            <p class="MsoNormal">The minimum requirements are:</p>      <ul><li>Apache 1.x, 2.x and higher</li><li>PHP 4.3 and higher</li><li>MySQL 3.23 and higher</li></ul>It will also run on alternative server platforms such as Windows IIS - provided they support PHP and MySQL - but these require additional configuration in order for the Joomla! core package to be successful installed and operated.', '', 1, 4, 0, 25, '2008-08-11 04:22:14', 62, '', '2008-08-11 04:22:14', 62, 0, '0000-00-00 00:00:00', '2006-10-10 08:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 4, '', '', 0, 11, 'robots=\nauthor='),
(24, 'Content Layouts', 'content-layouts', '', '<p>Joomla! provides plenty of flexibility when displaying your Web content. Whether you are using Joomla! for a blog site, news or a Web site for a company, you''ll find one or more content styles to showcase your information. You can also change the style of content dynamically depending on your preferences. Joomla! calls how a page is laid out a <strong>layout</strong>. Use the guide below to understand which layouts are available and how you might use them. </p> <h2>Content </h2> <p>Joomla! makes it extremely easy to add and display content. All content  is placed where your mainbody tag in your template is located. There are three main types of layouts available in Joomla! and all of them can be customised via parameters. The display and parameters are set in the Menu Item used to display the content your working on. You create these layouts by creating a Menu Item and choosing how you want the content to display.</p> <h3>Blog Layout<br /> </h3> <p>Blog layout will show a listing of all Articles of the selected blog type (Section or Category) in the mainbody position of your template. It will give you the standard title, and Intro of each Article in that particular Category and/or Section. You can customise this layout via the use of the Preferences and Parameters, (See Article Parameters) this is done from the Menu not the Section Manager!</p> <h3>Blog Archive Layout<br /> </h3> <p>A Blog Archive layout will give you a similar output of Articles as the normal Blog Display but will add, at the top, two drop down lists for month and year plus a search button to allow Users to search for all Archived Articles from a specific month and year.</p> <h3>List Layout<br /> </h3> <p>Table layout will simply give you a <em>tabular </em>list<em> </em>of all the titles in that particular Section or Category. No Intro text will be displayed just the titles. You can set how many titles will be displayed in this table by Parameters. The table layout will also provide a filter Section so that Users can reorder, filter, and set how many titles are listed on a single page (up to 50)</p> <h2>Wrapper</h2> <p>Wrappers allow you to place stand alone applications and Third Party Web sites inside your Joomla! site. The content within a Wrapper appears within the primary content area defined by the "mainbody" tag and allows you to display their content as a part of your own site. A Wrapper will place an IFRAME into the content Section of your Web site and wrap your standard template navigation around it so it appears in the same way an Article would.</p> <h2>Content Parameters</h2> <p>The parameters for each layout type can be found on the right hand side of the editor boxes in the Menu Item configuration screen. The parameters available depend largely on what kind of layout you are configuring.</p>', '', 1, 4, 0, 29, '2008-08-12 22:33:10', 62, '', '2008-08-12 22:33:10', 62, 0, '0000-00-00 00:00:00', '2006-10-11 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 11, 0, 5, '', '', 0, 70, 'robots=\nauthor='),
(25, 'What are the requirements to run Joomla! 1.5?', 'what-are-the-requirements-to-run-joomla-15', '', '<p>Joomla! runs on the PHP pre-processor. PHP comes in many flavours, for a lot of operating systems. Beside PHP you will need a Web server. Joomla! is optimized for the Apache Web server, but it can run on different Web servers like Microsoft IIS it just requires additional configuration of PHP and MySQL. Joomla! also depends on a database, for this currently you can only use MySQL. </p>Many people know from their own experience that it''s not easy to install an Apache Web server and it gets harder if you want to add MySQL, PHP and Perl. XAMPP, WAMP, and MAMP are easy to install distributions containing Apache, MySQL, PHP and Perl for the Windows, Mac OSX and Linux operating systems. These packages are for localhost installations on non-public servers only.<br />The minimum version requirements are:<br /><ul><li>Apache 1.x or 2.x</li><li>PHP 4.3 or up</li><li>MySQL 3.23 or up</li></ul>For the latest minimum requirements details, see <a href="http://www.joomla.org/about-joomla/technical-requirements.html" target="_blank" title="Joomla! Technical Requirements">Joomla! Technical Requirements</a>.', '', 1, 3, 0, 31, '2008-08-11 00:42:31', 62, '', '2008-08-11 00:42:31', 62, 0, '0000-00-00 00:00:00', '2006-10-10 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 5, '', '', 0, 29, 'robots=\nauthor=');
INSERT INTO `jos_content` (`id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`) VALUES
(26, 'Extensions', 'extensions', '', '<p>Out of the box, Joomla! does a great job of managing the content needed to make your Web site sing. But for many people, the true power of Joomla! lies in the application framework that makes it possible for developers all around the world to create powerful add-ons that are called <strong>Extensions</strong>. An Extension is used to add capabilities to Joomla! that do not exist in the base core code. Here are just some examples of the hundreds of available Extensions:</p> <ul>   <li>Dynamic form builders</li>   <li>Business or organisational directories</li>   <li>Document management</li>   <li>Image and multimedia galleries</li>   <li>E-commerce and shopping cart engines</li>   <li>Forums and chat software</li>   <li>Calendars</li>   <li>E-mail newsletters</li>   <li>Data collection and reporting tools</li>   <li>Banner advertising systems</li>   <li>Paid subscription services</li>   <li>and many, many, more</li> </ul> <p>You can find more examples over at our ever growing <a href="http://extensions.joomla.org" target="_blank" title="Joomla! Extensions Directory">Joomla! Extensions Directory</a>. Prepare to be amazed at the amount of exciting work produced by our active developer community!</p><p>A useful guide to the Extension site can be found at:<br /><a href="http://extensions.joomla.org/content/view/15/63/" target="_blank" title="Guide to the Joomla! Extension site">http://extensions.joomla.org/content/view/15/63/</a> </p> <h3>Types of Extensions </h3><p>There are five types of extensions:</p> <ul>   <li>Components</li>   <li>Modules</li>   <li>Templates</li>   <li>Plugins</li>   <li>Languages</li> </ul> <p>You can read more about the specifics of these using the links in the Article Index - a Table of Contents (yet another useful feature of Joomla!) - at the top right or by clicking on the <strong>Next </strong>link below.<br /> </p> <hr title="Components" class="system-pagebreak" /> <h3><img src="images/stories/ext_com.png" border="0" alt="Component - Joomla! Extension Directory" title="Component - Joomla! Extension Directory" width="17" height="17" /> Components</h3> <p>A Component is the largest and most complex of the Extension types.  Components are like mini-applications that render the main body of the  page. An analogy that might make the relationship easier to understand  would be that Joomla! is a book and all the Components are chapters in  the book. The core Article Component (<font face="courier new,courier">com_content</font>), for example, is the  mini-application that handles all core Article rendering just as the  core registration Component (<font face="courier new,courier">com_user</font>) is the mini-application  that handles User registration.</p> <p>Many of Joomla!''s core features are provided by the use of default Components such as:</p> <ul>   <li>Contacts</li>   <li>Front Page</li>   <li>News Feeds</li>   <li>Banners</li>   <li>Mass Mail</li>   <li>Polls</li></ul><p>A Component will manage data, set displays, provide functions, and in general can perform any operation that does not fall under the general functions of the core code.</p> <p>Components work hand in hand with Modules and Plugins to provide a rich variety of content display and functionality aside from the standard Article and content display. They make it possible to completely transform Joomla! and greatly expand its capabilities.</p>  <hr title="Modules" class="system-pagebreak" /> <h3><img src="images/stories/ext_mod.png" border="0" alt="Module - Joomla! Extension Directory" title="Module - Joomla! Extension Directory" width="17" height="17" /> Modules</h3> <p>A more lightweight and flexible Extension used for page rendering is a Module. Modules are used for small bits of the page that are generally  less complex and able to be seen across different Components. To  continue in our book analogy, a Module can be looked at as a footnote  or header block, or perhaps an image/caption block that can be rendered  on a particular page. Obviously you can have a footnote on any page but  not all pages will have them. Footnotes also might appear regardless of  which chapter you are reading. Simlarly Modules can be rendered  regardless of which Component you have loaded.</p> <p>Modules are like little mini-applets that can be placed anywhere on your site. They work in conjunction with Components in some cases and in others are complete stand alone snippets of code used to display some data from the database such as Articles (Newsflash) Modules are usually used to output data but they can also be interactive form items to input data for example the Login Module or Polls.</p> <p>Modules can be assigned to Module positions which are defined in your Template and in the back-end using the Module Manager and editing the Module Position settings. For example, "left" and "right" are common for a 3 column layout. </p> <h4>Displaying Modules</h4> <p>Each Module is assigned to a Module position on your site. If you wish it to display in two different locations you must copy the Module and assign the copy to display at the new location. You can also set which Menu Items (and thus pages) a Module will display on, you can select all Menu Items or you can pick and choose by holding down the control key and selecting  locations one by one in the Modules [Edit] screen</p> <p>Note: Your Main Menu is a Module! When you create a new Menu in the Menu Manager you are actually copying the Main Menu Module (<font face="courier new,courier">mod_mainmenu</font>) code and giving it the name of your new Menu. When you copy a Module you do not copy all of its parameters, you simply allow Joomla! to use the same code with two separate settings.</p> <h4>Newsflash Example</h4> <p>Newsflash is a Module which will display Articles from your site in an assignable Module position. It can be used and configured to display one Category, all Categories, or to randomly choose Articles to highlight to Users. It will display as much of an Article as you set, and will show a <em>Read more...</em> link to take the User to the full Article.</p> <p>The Newsflash Component is particularly useful for things like Site News or to show the latest Article added to your Web site.</p>  <hr title="Plugins" class="system-pagebreak" /> <h3><img src="images/stories/ext_plugin.png" border="0" alt="Plugin - Joomla! Extension Directory" title="Plugin - Joomla! Extension Directory" width="17" height="17" /> Plugins</h3> <p>One  of the more advanced Extensions for Joomla! is the Plugin. In previous  versions of Joomla! Plugins were known as Mambots. Aside from changing their name their  functionality has been expanded. A Plugin is a section of code that  runs when a pre-defined event happens within Joomla!. Editors are Plugins, for example, that execute when the Joomla! event <font face="courier new,courier">onGetEditorArea</font> occurs. Using a Plugin allows a developer to change  the way their code behaves depending upon which Plugins are installed  to react to an event.</p>  <hr title="Languages" class="system-pagebreak" /> <h3><img src="images/stories/ext_lang.png" border="0" alt="Language - Joomla! Extensions Directory" title="Language - Joomla! Extensions Directory" width="17" height="17" /> Languages</h3> <p>New  to Joomla! 1.5 and perhaps the most basic and critical Extension is a Language. Joomla! is released with  Installation Languages but the base Site and Administrator are packaged in just the one Language <strong>en-GB</strong> - being English with GB spelling for example. To include all the translations currently available would bloat the core package and make it unmanageable for uploading purposes. The Language files enable all the User interfaces both Front-end and Back-end to be presented in the local preferred language. Note these packs do not have any impact on the actual content such as Articles. </p> <p>More information on languages is available from the <br />   <a href="http://community.joomla.org/translations.html" target="_blank" title="Joomla! Translation Teams">http://community.joomla.org/translations.html</a></p>', '', 1, 4, 0, 29, '2008-08-11 06:00:00', 62, '', '2008-08-11 06:00:00', 62, 0, '0000-00-00 00:00:00', '2006-10-10 22:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 24, 0, 3, 'About Joomla!, General, Extensions', '', 0, 134, 'robots=\nauthor='),
(27, 'The Joomla! Community', 'the-joomla-community', '', '<p><strong>Got a question? </strong>With more than 210,000 members, the Joomla! Discussion Forums at <a href="http://forum.joomla.org/" target="_blank" title="Forums">forum.joomla.org</a> are a great resource for both new and experienced users. Ask your toughest questions the community is waiting to see what you''ll do with your Joomla! site.</p><p><strong>Do you want to show off your new Joomla! Web site?</strong> Visit the <a href="http://forum.joomla.org/viewforum.php?f=514" target="_blank" title="Site Showcase">Site Showcase</a> section of our forum.</p><p><strong>Do you want to contribute?</strong></p><p>If you think working with Joomla is fun, wait until you start working on it. We''re passionate about helping Joomla users become contributors. There are many ways you can help Joomla''s development:</p><ul>	<li>Submit news about Joomla. We syndicate Joomla-related news on <a href="http://news.joomla.org" target="_blank" title="JoomlaConnect">JoomlaConnect<sup>TM</sup></a>. If you have Joomla news that you would like to share with the community, find out how to get connected <a href="http://community.joomla.org/connect.html" target="_blank" title="JoomlaConnect">here</a>.</li>	<li>Report bugs and request features in our <a href="http://joomlacode.org/gf/project/joomla/tracker/" target="_blank" title="Joomla! developement trackers">trackers</a>. Please read <a href="http://docs.joomla.org/Filing_bugs_and_issues" target="_blank" title="Reporting Bugs">Reporting Bugs</a>, for details on how we like our bug reports served up</li><li>Submit patches for new and/or fixed behaviour. Please read <a href="http://docs.joomla.org/Patch_submission_guidelines" target="_blank" title="Submitting Patches">Submitting Patches</a>, for details on how to submit a patch.</li><li>Join the <a href="http://forum.joomla.org/viewforum.php?f=509" target="_blank" title="Joomla! development forums">developer forums</a> and share your ideas for how to improve Joomla. We''re always open to suggestions, although we''re likely to be sceptical of large-scale suggestions without some code to back it up.</li><li>Join any of the <a href="http://www.joomla.org/about-joomla/the-project/working-groups.html" target="_blank" title="Joomla! working groups">Joomla Working Groups</a> and bring your personal expertise to the Joomla community. </li></ul><p>These are just a few ways you can contribute. See <a href="http://www.joomla.org/about-joomla/contribute-to-joomla.html" target="_blank" title="Contribute">Contribute to Joomla</a> for many more ways.</p>', '', 1, 4, 0, 30, '2008-08-12 16:50:48', 62, '', '2008-08-12 16:50:48', 62, 0, '0000-00-00 00:00:00', '2006-10-11 02:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 12, 0, 2, '', '', 0, 52, 'robots=\nauthor='),
(28, 'How do I install Joomla! 1.5?', 'how-do-i-install-joomla-15', '', '<p>Installing of Joomla! 1.5 is pretty easy. We assume you have set-up your Web site, and it is accessible with your browser.<br /><br />Download Joomla! 1.5, unzip it and upload/copy the files into the directory you Web site points to, fire up your browser and enter your Web site address and the installation will start.  </p><p>For full details on the installation processes check out the <a href="http://help.joomla.org/content/category/48/268/302" target="_blank" title="Joomla! 1.5 Installation Manual">Installation Manual</a> on the <a href="http://help.joomla.org" target="_blank" title="Joomla! Help Site">Joomla! Help Site</a> where you will also find download instructions for a PDF version too. </p>', '', 1, 3, 0, 31, '2008-08-11 01:10:59', 62, '', '2008-08-11 01:10:59', 62, 0, '0000-00-00 00:00:00', '2006-10-10 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 5, 0, 3, '', '', 0, 43, 'robots=\nauthor='),
(29, 'What is the purpose of the collation selection in the installation screen?', 'what-is-the-purpose-of-the-collation-selection-in-the-installation-screen', '', 'The collation option determines the way ordering in the database is done. In languages that use special characters, for instance the German umlaut, the database collation determines the sorting order. If you don''t know which collation you need, select the "utf8_general_ci" as most languages use this. The other collations listed are exceptions in regards to the general collation. If your language is not listed in the list of collations it most likely means that "utf8_general_ci is suitable.', '', 1, 3, 0, 32, '2008-08-11 03:11:38', 62, '', '2008-08-11 03:11:38', 62, 0, '0000-00-00 00:00:00', '2006-10-10 08:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=', 4, 0, 4, '', '', 0, 6, 'robots=\nauthor='),
(30, 'What languages are supported by Joomla! 1.5?', 'what-languages-are-supported-by-joomla-15', '', 'Within the Installer you will find a wide collection of languages. The installer currently supports the following languages: Arabic, Bulgarian, Bengali, Czech, Danish, German, Greek, English, Spanish, Finnish, French, Hebrew, Devanagari(India), Croatian(Croatia), Magyar (Hungary), Italian, Malay, Norwegian bokmal, Dutch, Portuguese(Brasil), Portugues(Portugal), Romanian, Russian, Serbian, Svenska, Thai and more are being added all the time.<br />By default the English language is installed for the Back and Front-ends. You can download additional language files from the <a href="http://extensions.joomla.org" target="_blank" title="Joomla! Extensions Directory">Joomla!Extensions Directory</a>. ', '', 1, 3, 0, 32, '2008-08-11 01:12:18', 62, '', '2008-08-11 01:12:18', 62, 0, '0000-00-00 00:00:00', '2006-10-10 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 5, 0, 2, '', '', 0, 8, 'robots=\nauthor='),
(31, 'Is it useful to install the sample data?', 'is-it-useful-to-install-the-sample-data', '', 'Well you are reading it right now! This depends on what you want to achieve. If you are new to Joomla! and have no clue how it all fits together, just install the sample data. If you don''t like the English sample data because you - for instance - speak Chinese, then leave it out.', '', 1, 3, 0, 27, '2008-08-11 09:12:55', 62, '', '2008-08-11 09:12:55', 62, 0, '0000-00-00 00:00:00', '2006-10-10 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 3, '', '', 0, 4, 'robots=\nauthor='),
(32, 'Where is the Static Content Item?', 'where-is-the-static-content', '', '<p>In Joomla! versions prior to 1.5 there were separate processes for creating a Static Content Item and normal Content Items. The processes have been combined now and whilst both content types are still around they are renamed as Articles for Content Items and Uncategorized Articles for Static Content Items. </p><p>If you want to create a static item, create a new Article in the same way as for standard content and rather than relating this to a particular Section and Category just select <span style="font-style: italic">Uncategorized</span> as the option in the Section and Category drop down lists.</p>', '', 1, 3, 0, 28, '2008-08-10 23:13:33', 62, '', '2008-08-10 23:13:33', 62, 0, '0000-00-00 00:00:00', '2006-10-10 04:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 6, '', '', 0, 7, 'robots=\nauthor='),
(33, 'What is an Uncategorised Article?', 'what-is-uncategorised-article', '', 'Most Articles will be assigned to a Section and Category. In many cases, you might not know where you want it to appear so put the Article in the <em>Uncategorized </em>Section/Category. The Articles marked as <em>Uncategorized </em>are handled as static content.', '', 1, 3, 0, 31, '2008-08-11 15:14:11', 62, '', '2008-08-11 15:14:11', 62, 0, '0000-00-00 00:00:00', '2006-10-10 12:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 8, 0, 2, '', '', 0, 10, 'robots=\nauthor='),
(34, 'Does the PDF icon render pictures and special characters?', 'does-the-pdf-icon-render-pictures-and-special-characters', '', 'Yes! Prior to Joomla! 1.5, only the text values of an Article and only for ISO-8859-1 encoding was allowed in the PDF rendition. With the new PDF library in place, the complete Article including images is rendered and applied to the PDF. The PDF generator also handles the UTF-8 texts and can handle any character sets from any language. The appropriate fonts must be installed but this is done automatically during a language pack installation.', '', 1, 3, 0, 32, '2008-08-11 17:14:57', 62, '', '2008-08-11 17:14:57', 62, 0, '0000-00-00 00:00:00', '2006-10-10 14:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 3, '', '', 0, 6, 'robots=\nauthor='),
(35, 'Is it possible to change A Menu Item''s Type?', 'is-it-possible-to-change-the-types-of-menu-entries', '', '<p>You indeed can change the Menu Item''s Type to whatever you want, even after they have been created. </p><p>If, for instance, you want to change the Blog Section of a Menu link, go to the Control Panel-&gt;Menus Menu-&gt;[menuname]-&gt;Menu Item Manager and edit the Menu Item. Select the <strong>Change Type</strong> button and choose the new style of Menu Item Type from the available list. Thereafter, alter the Details and Parameters to reconfigure the display for the new selection  as you require it.</p>', '', 1, 3, 0, 31, '2008-08-10 23:15:36', 62, '', '2008-08-10 23:15:36', 62, 0, '0000-00-00 00:00:00', '2006-10-10 04:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 1, '', '', 0, 18, 'robots=\nauthor='),
(36, 'Where did the Installers go?', 'where-did-the-installer-go', '', 'The improved Installer can be found under the Extensions Menu. With versions prior to Joomla! 1.5 you needed to select a specific Extension type when you wanted to install it and use the Installer associated with it, with Joomla! 1.5 you just select the Extension you want to upload, and click on install. The Installer will do all the hard work for you.', '', 1, 3, 0, 28, '2008-08-10 23:16:20', 62, '', '2008-08-10 23:16:20', 62, 0, '0000-00-00 00:00:00', '2006-10-10 04:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 1, '', '', 0, 7, 'robots=\nauthor='),
(37, 'Where did the Mambots go?', 'where-did-the-mambots-go', '', '<p>Mambots have been renamed as Plugins. </p><p>Mambots were introduced in Mambo and offered possibilities to add plug-in logic to your site mainly for the purpose of manipulating content. In Joomla! 1.5, Plugins will now have much broader capabilities than Mambots. Plugins are able to extend functionality at the framework layer as well.</p>', '', 1, 3, 0, 28, '2008-08-11 09:17:00', 62, '', '2008-08-11 09:17:00', 62, 0, '0000-00-00 00:00:00', '2006-10-10 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 5, '', '', 0, 6, 'robots=\nauthor='),
(38, 'I installed with my own language, but the Back-end is still in English', 'i-installed-with-my-own-language-but-the-back-end-is-still-in-english', '', '<p>A lot of different languages are available for the Back-end, but by default this language may not be installed. If you want a translated Back-end, get your language pack and install it using the Extension Installer. After this, go to the Extensions Menu, select Language Manager and make your language the default one. Your Back-end will be translated immediately.</p><p>Users who have access rights to the Back-end may choose the language they prefer in their Personal Details parameters. This is of also true for the Front-end language.</p><p> A good place to find where to download your languages and localised versions of Joomla! is <a href="http://extensions.joomla.org/index.php?option=com_mtree&task=listcats&cat_id=1837&Itemid=35" target="_blank" title="Translations for Joomla!">Translations for Joomla!</a> on JED.</p>', '', 1, 3, 0, 32, '2008-08-11 17:18:14', 62, '', '2008-08-11 17:18:14', 62, 0, '0000-00-00 00:00:00', '2006-10-10 14:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 1, '', '', 0, 7, 'robots=\nauthor='),
(39, 'How do I remove an Article?', 'how-do-i-remove-an-article', '', '<p>To completely remove an Article, select the Articles that you want to delete and move them to the Trash. Next, open the Article Trash in the Content Menu and select the Articles you want to delete. After deleting an Article, it is no longer available as it has been deleted from the database and it is not possible to undo this operation.  </p>', '', 1, 3, 0, 27, '2008-08-11 09:19:01', 62, '', '2008-08-11 09:19:01', 62, 0, '0000-00-00 00:00:00', '2006-10-10 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 2, '', '', 0, 10, 'robots=\nauthor='),
(40, 'What is the difference between Archiving and Trashing an Article? ', 'what-is-the-difference-between-archiving-and-trashing-an-article', '', '<p>When you <em>Archive </em>an Article, the content is put into a state which removes it from your site as published content. The Article is still available from within the Control Panel and can be <em>retrieved </em>for editing or republishing purposes. Trashed Articles are just one step from being permanently deleted but are still available until you Remove them from the Trash Manager. You should use Archive if you consider an Article important, but not current. Trash should be used when you want to delete the content entirely from your site and from future search results.  </p>', '', 1, 3, 0, 27, '2008-08-11 05:19:43', 62, '', '2008-08-11 05:19:43', 62, 0, '0000-00-00 00:00:00', '2006-10-10 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 8, 0, 1, '', '', 0, 5, 'robots=\nauthor='),
(41, 'Newsflash 5', 'newsflash-5', '', 'Joomla! 1.5 - ''Experience the Freedom''!. It has never been easier to create your own dynamic Web site. Manage all your content from the best CMS admin interface and in virtually any language you speak.', '', 1, 1, 0, 3, '2008-08-12 00:17:31', 62, '', '2008-08-12 00:17:31', 62, 0, '0000-00-00 00:00:00', '2006-10-11 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 5, 0, 5, '', '', 0, 5, 'robots=\nauthor='),
(42, 'Newsflash 4', 'newsflash-4', '', 'Yesterday all servers in the U.S. went out on strike in a bid to get more RAM and better CPUs. A spokes person said that the need for better RAM was due to some fool increasing the front-side bus speed. In future, buses will be told to slow down in residential motherboards.', '', 1, 1, 0, 3, '2008-08-12 00:25:50', 62, '', '2008-08-12 00:25:50', 62, 0, '0000-00-00 00:00:00', '2006-10-11 06:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 5, 0, 3, '', '', 0, 8, 'robots=\nauthor='),
(43, 'Example Pages and Menu Links', 'example-pages-and-menu-links', '', '<p>This page is an example of content that is <em>Uncategorized</em>; that is, it does not belong to any Section or Category. You will see there is a new Menu in the left column. It shows links to the same content presented in 4 different page layouts.</p><ul><li>Section Blog</li><li>Section Table</li><li> Blog Category</li><li>Category Table</li></ul><p>Follow the links in the <strong>Example Pages</strong> Menu to see some of the options available to you to present all the different types of content included within the default installation of Joomla!.</p><p>This includes Components and individual Articles. These links or Menu Item Types (to give them their proper name) are all controlled from within the <strong><font face="courier new,courier">Menu Manager-&gt;[menuname]-&gt;Menu Items Manager</font></strong>. </p>', '', 1, 0, 0, 0, '2008-08-12 09:26:52', 62, '', '2008-08-12 09:26:52', 62, 0, '0000-00-00 00:00:00', '2006-10-11 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 2, 'Uncategorized, Uncategorized, Example Pages and Menu Links', '', 0, 45, 'robots=\nauthor='),
(44, 'Joomla! Security Strike Team', 'joomla-security-strike-team', '', '<p>The Joomla! Project has assembled a top-notch team of experts to form the new Joomla! Security Strike Team. This new team will solely focus on investigating and resolving security issues. Instead of working in relative secrecy, the JSST will have a strong public-facing presence at the <a href="http://developer.joomla.org/security.html" target="_blank" title="Joomla! Security Center">Joomla! Security Center</a>.</p>', '<p>The new JSST will call the new <a href="http://developer.joomla.org/security.html" target="_blank" title="Joomla! Security Center">Joomla! Security Center</a> their home base. The Security Center provides a public presence for <a href="http://developer.joomla.org/security/news.html" target="_blank" title="Joomla! Security News">security issues</a> and a platform for the JSST to <a href="http://developer.joomla.org/security/articles-tutorials.html" target="_blank" title="Joomla! Security Articles">help the general public better understand security</a> and how it relates to Joomla!. The Security Center also offers users a clearer understanding of how security issues are handled. There''s also a <a href="http://feeds.joomla.org/JoomlaSecurityNews" target="_blank" title="Joomla! Security News Feed">news feed</a>, which provides subscribers an up-to-the-minute notification of security issues as they arise.</p>', 0, 1, 0, 1, '2007-07-07 09:54:06', 62, '', '2007-07-07 09:54:06', 62, 0, '0000-00-00 00:00:00', '2004-07-06 22:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 4, '', '', 0, 7, 'robots=\nauthor='),
(45, 'D�cret N�2011-686/PRN/PM, portant code des march�s publics et des d�l�gations de service publics', 'decret-nd2011-686prnpm-portant-code-des-marches-publics-et-des-delegations-de-service-publics', '', '<h3>Article premier</h3>\r\n<p>En application du Code des Obligations de l�Administration et de la loi n�&nbsp;90-07 du 26 juin 1990 relative � l�organisation et au contr�le des entreprises du secteur parapublic et au contr�le des personnes morales de droit priv� b�n�ficiant du concours financier de la puissance publique, le pr�sent d�cret fixe les r�gles r�gissant la passation, l�ex�cution et le contr�le des march�s conclus par les personnes morales mentionn�es � l�article 2, pour r�pondre � leurs besoins en mati�re de r�alisation de travaux et d�achat de fournitures ou de services ainsi que la passation et le contr�le des contrats portant participation � l�ex�cution d�un service public</p>\r\n<h3>Article 2</h3>\r\n<p>1. Les dispositions du pr�sent d�cret s�appliquent aux march�s conclus par les autorit�s contractantes suivantes&nbsp;:</p>\r\n<p>a) l�Etat, les collectivit�s locales, y compris leurs services d�centralis�s et les organisations ou agences non dot�es de la personnalit� morale, plac�es sous leur autorit�&nbsp;;</p>\r\n<p>b) les �tablissements publics&nbsp;;</p>\r\n<p>c) les agences ou organismes, personnes morales de droit public ou priv�, autres que les �tablissements publics, soci�t�s nationales ou soci�t�s anonymes � participation publique majoritaire, dont l�activit� est financ�e majoritairement par l�Etat ou une collectivit� locale et s�exerce essentiellement dans le cadre d�activit�s d�int�r�t g�n�ral&nbsp;;</p>\r\n<p>d) les soci�t�s nationales et les soci�t�s anonymes � participation publique majoritaire r�gies par la loi n�&nbsp;90-07 du 26 juin 1990 susvis�e&nbsp;;</p>\r\n<p>e) les associations form�es par les personnes vis�es au paragraphe a) � d) ci-dessus.</p>\r\n<p>2. Les march�s pass�s par une personne morale de droit public ou priv� pour le compte d�une autorit� contractante sont soumis aux r�gles qui s�appliquent, conform�ment au pr�sent d�cret, aux march�s pass�s directement par ladite autorit� contractante. La d�l�gation des t�ches relatives � la passation de march�s concernant la r�alisation d�ouvrages ou de projets doit �tre effectu�e dans les conditions stipul�es aux articles 31 � 34 du pr�sent d�cret.</p>', '', 1, 1, 0, 1, '2007-07-07 09:54:06', 62, '', '2012-11-27 10:26:35', 62, 0, '0000-00-00 00:00:00', '2004-07-06 22:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 3, 0, 3, '', '', 0, 6, 'robots=\nauthor='),
(46, 'Typography', 'typography', '', '<p><span class="color-6">This page presents most of typographical aspects of VIYO template. Make your readers happy with great Typography and User Experience!</span> </p><p>&nbsp;</p><p class="info">This is a sample info message. Use <strong>&lt;p class="info"&gt;Your info message goes here!&lt;/p&gt;</strong>.</p><p class="tips">This is a sample tips message. Use <strong>&lt;p class="tips"&gt;Your tips goes here!&lt;/p&gt;</strong>.</p><p class="warning">This is a sample error message. Use <strong>&lt;p class="warning"&gt;Your error message goes here!&lt;/p&gt;</strong>.</p><p>&nbsp;</p><p class="audio">This is a sample audio message. Use <strong>&lt;p class="audio"&gt;Your audio message goes here!&lt;/p&gt;</strong>.</p><p class="webcam">This is a sample webcam message. Use <strong>&lt;p class="webcam"&gt;Your webcam goes here!&lt;/p&gt;</strong>.</p><p class="email">This is a sample email message. Use <strong>&lt;p class="email"&gt;Your email message goes here!&lt;/p&gt;</strong>.</p><p class="credit">This is a sample creditcart message. Use <strong>&lt;p class="creditcart "&gt;Your creditcart message goes here!&lt;/p&gt;</strong>.</p><p class="feed">This is a sample feed message. Use <strong>&lt;p class="feed"&gt;Your feed goes here!&lt;/p&gt;</strong>.</p><p class="help">This is a sample email message. Use <strong>&lt;p class="help"&gt;Your help message goes here!&lt;/p&gt;</strong>.</p><p>&nbsp;</p><p>This is a <span class="highlight-1">highlight phrase</span>. Use <strong>&lt;span class="highlight-1"&gt;Your highlight phrase goes here!&lt;/span&gt;</strong>. </p><p>This is a <span class="highlight-2">highlight phrase</span>. Use <strong>&lt;span class="highlight-2"&gt;Your highlight phrase goes here!&lt;/span&gt;</strong>. </p><p>This is a <span class="highlight-3">highlight phrase</span>. Use <strong>&lt;span class="highlight-3"&gt;Your highlight phrase goes here!&lt;/span&gt;</strong>. </p><p>This is a <span class="highlight-4">highlight phrase</span>. Use <strong>&lt;span class="highlight-4"&gt;Your highlight phrase goes here!&lt;/span&gt;</strong>. </p><p>&nbsp;</p><p> Below is a sample of <strong>&lt;pre&gt;</strong> or <strong>&lt;div class="code"&gt;</strong></p><pre>#wrapper {<br />	position: relative;<br />	float: left;<br />	display: block;<br />}<br /></pre><p>&nbsp;</p><p><span class="clear">This is a sample pin note. Use <strong>&lt;span class="clear"&gt;</strong>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer urna. Aenean tristique. Fusce a neque. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. <strong>&lt;/span&gt;</strong></span> </p><p><span class="clear-1">This is a sample pin note. Use <strong>&lt;span class="clear-1"&gt;</strong>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer urna. Aenean tristique. Fusce a neque. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. <strong>&lt;/span&gt;</strong></span> </p><p><span class="clear-2">This is a sample pin note. Use <strong>&lt;span class="clear-2"&gt;</strong>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer urna. Aenean tristique. Fusce a neque. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. <strong>&lt;/span&gt;</strong></span></p><p><span class="color">This is a sample pin note. Use <strong>&lt;span class="color"&gt;</strong>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer urna. Aenean tristique. Fusce a neque. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. <strong>&lt;/span&gt;</strong></span> </p><p><span class="color-1">This is a sample pin note. Use <strong>&lt;span class="color-1"&gt;</strong>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer urna. Aenean tristique. Fusce a neque. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. <strong>&lt;/span&gt;</strong></span> </p><p><span class="color-2">This is a sample pin note. Use <strong>&lt;span class="color-2"&gt;</strong>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer urna. Aenean tristique. Fusce a neque. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. <strong>&lt;/span&gt;</strong></span> </p><p><span class="color-3">This is a sample pin note. Use <strong>&lt;span class="color-3"&gt;</strong>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer urna. Aenean tristique. Fusce a neque. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. <strong>&lt;/span&gt;</strong></span> </p><p><span class="color-4">This is a sample pin note. Use <strong>&lt;span class="color-4"&gt;</strong>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer urna. Aenean tristique. Fusce a neque. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. <strong>&lt;/span&gt;</strong></span> </p><p><span class="color-5">This is a sample pin note. Use <strong>&lt;span class="color-5"&gt;</strong>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer urna. Aenean tristique. Fusce a neque. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. <strong>&lt;/span&gt;</strong></span></p><p><span class="color-6">This is a sample pin note. Use <strong>&lt;span class="color-6"&gt;</strong>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer urna. Aenean tristique. Fusce a neque. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. <strong>&lt;/span&gt;</strong></span></p><p><span class="color-7">This is a sample pin note. Use <strong>&lt;span class="color-7"&gt;</strong>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer urna. Aenean tristique. Fusce a neque. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. <strong>&lt;/span&gt;</strong></span></p>', '', 1, 4, 0, 25, '2009-02-20 16:58:19', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2009-02-20 16:58:19', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 1, '', '', 0, 266, 'robots=\nauthor='),
(47, 'U2 - Get On Your Boots ', 'u2-get-on-your-boots-', '', '<p align="justify"><img class="caption" src="images/stories/demo/u2_news.jpg" border="0" alt="U2 - Get On Your Boots " title="U2 - Get On Your Boots " align="right" />Through a combination of zealous righteousness and post-punk experimentalism,  U2 became one of the most popular rock and roll bands of the ''80s. Equally  known for their sweeping sound as for their grandiose statements about politics  and religion, they were rock''n''roll crusaders during an era of synthesized  pop and heavy metal. The Edge provided the group with a signature sound by  creating sweeping sonic landscapes with his heavily processed, echoed guitars.  Though the Edge''s style wasn''t conventional, the rhythm section of Larry </p>', '<p align="justify">Mullen,  Jr. and Adam Clayton played the songs as driving hard rock, giving the band a  forceful, powerful edge that was designed for arenas. And their lead singer, Bono, was a  frontman who had a knack of grand gestures that played better in stadiums than  small clubs. It''s no accident that footage of Bono parading with a white flag with  "Sunday Bloody Sunday" blaring in the background became the defining moment of  U2''s early career -- there rarely was a band that believed so deeply in rock''s  potential for revolution as U2, and there rarely was a band that didn''t care if  they appeared foolish in the process.</p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel ipsum a  est luctus congue. Phasellus cursus euismod nibh. Etiam et massa in dui semper  eleifend. Ut adipiscing adipiscing tellus. In egestas. Integer sagittis neque  sed lacus. Aenean eu lectus. Suspendisse eleifend rhoncus tortor. Proin  vulputate. Donec sit amet diam vitae metus blandit malesuada. <div align="justify"> </div><p align="justify">Nulla consectetur lobortis est. Mauris fermentum. Ut tempor, enim sit amet  congue tristique, nunc est consequat risus, vitae sodales orci nisi sit amet  turpis. Aenean ullamcorper, ligula quis malesuada semper, enim odio blandit  lectus, quis faucibus tortor nunc sed tellus. Vestibulum ante ipsum primis in  faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum fringilla  pulvinar nisl. Donec vitae metus. Nunc vitae nulla quis magna dapibus interdum.  Nulla consectetur. Aenean ipsum. Phasellus bibendum tincidunt ante. Mauris  tortor. Donec viverra imperdiet magna. In at neque at dolor blandit tristique.  Sed sodales leo et orci. Curabitur ut odio vel ipsum consequat bibendum. Aliquam  consequat malesuada sapien. Quisque eros. Maecenas consectetur arcu mollis nisl.  </p><div align="justify"> </div><p align="justify">Curabitur ultricies. Sed tortor risus, suscipit id, mattis id, eleifend ac,  leo. Nulla facilisi. Nulla consectetur nisi ac nisl. Donec nec augue sit amet  nisi euismod bibendum. Phasellus feugiat, nisi in sollicitudin hendrerit, magna  nisi iaculis lorem, id dignissim mi lectus id elit. Fusce vel felis. Quisque  lacinia. Vestibulum vitae quam. Ut hendrerit dolor vel mauris. Cras nec augue  vel neque commodo blandit. Nunc id nisl. Donec in ligula in ipsum feugiat  imperdiet. </p><p>&nbsp;</p><p>&nbsp;</p>', 1, 5, 0, 34, '2009-02-25 17:58:42', 62, '', '2009-02-28 06:15:49', 62, 0, '0000-00-00 00:00:00', '2009-02-25 17:58:42', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 8, 0, 11, '', '', 0, 56, 'robots=\nauthor='),
(48, 'Director Tim Burton approached Pearl Jam ', 'director-tim-burton-approached-pearl-jam-', '', '<p><img class="caption" src="images/stories/demo/pearl_jam_news.png" border="0" alt="Pearl Jam are preparing new tour" title="Pearl Jam are preparing new tour" align="left" /><span class="content">Wartime, for everything else that''s wrong with it, brings  out the best in Pearl Jam: the power-chord brawn, contrary righteousness and  metallic-KO songwriting sense. The band''s second and third albums, 1993''s  bluntly titled <em>Vs</em>. and 1994''s <em>Vitalogy</em>, are as good as modern  rock-in-opposition gets: shotgun guitars, incendiary bass and drums, and Eddie  Vedder''s scalded-dog howl, all discharged in backs-to-the-wall fury and  union.This album, Pearl Jam''s first studio release in four years and their best  in ten, is more of that top electric combat. </span></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel ipsum a  est luctus congue. Phasellus cursus euismod nibh. Etiam et massa in dui semper  eleifend. Ut adipiscing adipiscing tellus. In egestas. Integer sagittis neque  sed lacus. Aenean eu lectus. Suspendisse eleifend rhoncus tortor. Proin  vulputate. Donec sit amet diam vitae metus blandit malesuada. </p> <p>Nulla consectetur lobortis est. Mauris fermentum. Ut tempor, enim sit amet  congue tristique, nunc est consequat risus, vitae sodales orci nisi sit amet  turpis. Aenean ullamcorper, ligula quis malesuada semper, enim odio blandit  lectus, quis faucibus tortor nunc sed tellus. Vestibulum ante ipsum primis in  faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum fringilla  pulvinar nisl. Donec vitae metus. Nunc vitae nulla quis magna dapibus interdum.  Nulla consectetur. Aenean ipsum. Phasellus bibendum tincidunt ante. Mauris  tortor. Donec viverra imperdiet magna. In at neque at dolor blandit tristique.  Sed sodales leo et orci. Curabitur ut odio vel ipsum consequat bibendum. Aliquam  consequat malesuada sapien. Quisque eros. Maecenas consectetur arcu mollis nisl.  </p> <p>Curabitur ultricies. Sed tortor risus, suscipit id, mattis id, eleifend ac,  leo. Nulla facilisi. Nulla consectetur nisi ac nisl. Donec nec augue sit amet  nisi euismod bibendum. Phasellus feugiat, nisi in sollicitudin hendrerit, magna  nisi iaculis lorem, id dignissim mi lectus id elit. Fusce vel felis. Quisque  lacinia. Vestibulum vitae quam. Ut hendrerit dolor vel mauris. Cras nec augue  vel neque commodo blandit. Nunc id nisl. Donec in ligula in ipsum feugiat  imperdiet. </p>', '', 1, 5, 0, 34, '2009-02-24 00:00:00', 62, '', '2009-02-26 03:52:37', 62, 0, '0000-00-00 00:00:00', '2009-02-25 19:01:27', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 3, 0, 10, '', '', 0, 2, 'robots=\nauthor=');
INSERT INTO `jos_content` (`id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`) VALUES
(49, 'After Midnight Project release EP', 'after-midnight-project-release-ep', '', '<p align="justify"><img class="caption" src="images/stories/demo/project_news.jpg" border="0" alt="After Midnight Project have released an EP" title="After Midnight Project have released an EP" align="right" />With a combination of pop rock, industrial, and a pinch of nu metal, After  Midnight Project have released an EP that will definitely turn heads. The  question is, which heads will it turn? Certain songs on <em>The Becoming</em>  sound as if they would fit in on an old school Warped Tour lineup with the likes  of Pennywise and Bad Religion. While those songs have a more seasoned punk rock  sound, there are certain tracks such as �Digital Crush� and title track �The  Becoming� that sound as if they could be on your typical party radio station.  This could be looked upon as a good or bad thing depending on the listener�s  musical taste. After Midnight Project have definitely gained national attention  by earning a spot on the prestigious �Top Four Unsigned Bands in the Nation�  list in <em>Alternative Press</em>. The first song on <em>The Becoming</em>, �Take  Me Home,� is also featured on the Xbox 360 game <em>Prey</em>. While those are two  things that any band would be ecstatic about, it is obvious that the music they  are making is going to appeal to your average Red Jumpsuit Apparatus fan who  bought their album because they heard �Face Down� on the radio and couldn�t get  it out of their head. If you can get over that mountain or anthill (depending on  how important this fact is), then you could be in for quite a fun listen.</p><p align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel ipsum a  est luctus congue. Phasellus cursus euismod nibh. Etiam et massa in dui semper  eleifend. Ut adipiscing adipiscing tellus. In egestas. Integer sagittis neque  sed lacus. Aenean eu lectus. Suspendisse eleifend rhoncus tortor. Proin  vulputate. Donec sit amet diam vitae metus blandit malesuada. </p><div align="justify"> </div><p align="justify">Nulla consectetur lobortis est. Mauris fermentum. Ut tempor, enim sit amet  congue tristique, nunc est consequat risus, vitae sodales orci nisi sit amet  turpis. Aenean ullamcorper, ligula quis malesuada semper, enim odio blandit  lectus, quis faucibus tortor nunc sed tellus. Vestibulum ante ipsum primis in  faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum fringilla  pulvinar nisl. Donec vitae metus. Nunc vitae nulla quis magna dapibus interdum.  Nulla consectetur. Aenean ipsum. Phasellus bibendum tincidunt ante. Mauris  tortor. Donec viverra imperdiet magna. In at neque at dolor blandit tristique.  Sed sodales leo et orci. Curabitur ut odio vel ipsum consequat bibendum. Aliquam  consequat malesuada sapien. Quisque eros. Maecenas consectetur arcu mollis nisl.  </p><div align="justify"> </div><p align="justify">Curabitur ultricies. Sed tortor risus, suscipit id, mattis id, eleifend ac,  leo. Nulla facilisi. Nulla consectetur nisi ac nisl. Donec nec augue sit amet  nisi euismod bibendum. Phasellus feugiat, nisi in sollicitudin hendrerit, magna  nisi iaculis lorem, id dignissim mi lectus id elit. Fusce vel felis. Quisque  lacinia. Vestibulum vitae quam. Ut hendrerit dolor vel mauris. Cras nec augue  vel neque commodo blandit. Nunc id nisl. Donec in ligula in ipsum feugiat  imperdiet. </p><p>&nbsp;</p>', '', 1, 5, 0, 34, '2009-02-24 00:00:00', 62, '', '2009-02-28 06:39:17', 62, 0, '0000-00-00 00:00:00', '2009-02-25 19:08:54', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 3, 0, 9, '', '', 0, 6, 'robots=\nauthor='),
(50, 'Madonna Tops Moneymakers', 'madonna-tops-moneymakers', '', '<p align="justify"><img class="caption" src="images/stories/demo/madonna_news_big.png" border="0" alt="Madonna Tops Moneymakers List" title="Madonna Tops Moneymakers List" align="right" /><span class="cat-story-text-font">There''s no  stopping Maddona. The 50  year-old pop star is the highest earning star in the music business bringing in  a massive $242 million in 2008 alone. Billboard just released their  Moneymakers list of 2008 and Madge came out on top earning a grand total of  $242,176,466. Rockers Bon Jovi came second in the poll, followed by  Bruce Springsteen, The Police and Celine Dion, all of whom toured last year.</span></p><p align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis rhoncus. Nunc  nibh sapien, aliquam vitae, lobortis viverra, iaculis non, enim. Vivamus at  nisl. Aliquam nulla nulla, viverra eget, pellentesque vel, sollicitudin  interdum, nulla. Donec augue tellus, luctus in, auctor a, egestas a, libero.  Vestibulum arcu tellus, aliquet eget, faucibus vehicula, adipiscing at, massa.  Suspendisse potenti. Praesent ornare luctus urna. Aliquam dolor quam, feugiat  et, dictum ac, faucibus a, orci. Donec leo mi, facilisis non, luctus id,  ultrices vitae, augue. Suspendisse potenti. Pellentesque a tellus. Vivamus justo  mauris, molestie eget, euismod nec, iaculis vel, lorem. Cras malesuada velit eu  mauris. Donec cursus pede a quam. Nam eu felis id velit dapibus lacinia. </p><div align="justify"> </div><p align="justify">Praesent velit. Vestibulum lorem tellus, elementum vitae, tempor id,  ultricies et, erat. Quisque condimentum sapien a ligula. Nullam eros nulla,  adipiscing vel, dignissim at, lacinia at, mi. Nam sit amet turpis. Sed id nunc.  Mauris ut nunc quis nisl condimentum commodo. Fusce vel dolor in augue vulputate  blandit. Proin pharetra lacinia sapien. Suspendisse ut erat. Morbi lorem quam,  laoreet eget, consequat ut, cursus vel, tellus. Praesent interdum tellus et  sapien. Maecenas porttitor justo ut mauris. Phasellus vehicula congue urna. </p><div align="justify"> </div><p align="justify">Integer tempus. Donec vitae lectus eget urna lobortis porta. Fusce tortor  diam, viverra in, commodo vel, tincidunt nec, odio. Vivamus convallis. Class  aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos  himenaeos. Sed at magna at sem sollicitudin lobortis. Suspendisse facilisis.  Cras iaculis, lorem a porttitor scelerisque, nulla urna ultricies tortor, at  molestie odio felis sed nisl. Vivamus varius est ut lorem. Sed sit amet massa.  Proin quis tortor id quam tempor ullamcorper. Etiam est magna, malesuada in,  consequat eu, pharetra nec, ligula. Aliquam a velit. Aliquam ac felis. </p><div align="justify"> </div><p align="justify">Cras blandit mauris sed justo. Etiam accumsan, ante vel posuere elementum,  risus urna tempor lacus, eget dapibus nulla ante sed dui. Integer vitae dui. In  ultricies nisi sit amet nisi. Cum sociis natoque penatibus et magnis dis  parturient montes, nascetur ridiculus mus. Maecenas vehicula est sed neque. Sed  ultrices ante. Donec vestibulum. Vestibulum tristique. Donec bibendum. Nulla  semper dui sit amet enim. Vivamus odio tellus, pharetra eget, laoreet eget,  consequat at, justo. Proin facilisis risus eget orci. Proin orci tellus,  scelerisque at, gravida at, pulvinar vel, dui. </p><div align="justify"> </div><p align="justify">Proin faucibus. Etiam mi sem, lobortis nec, scelerisque hendrerit, commodo  in, enim. Phasellus adipiscing tellus vitae odio. Nulla elit quam, ornare ac,  dictum sed, condimentum vel, est. Etiam fermentum, odio id semper placerat, enim  mi sagittis sapien, sed dictum libero orci vel purus. Nam aliquet massa. Mauris  mattis dui non arcu. Ut luctus, ante non imperdiet malesuada, nulla purus  adipiscing orci, in tincidunt tellus nulla non ligula. Pellentesque rhoncus,  quam et euismod sodales, massa justo ultricies erat, in fringilla dolor purus at  neque. Nam consequat, ipsum nec tincidunt dignissim, dui leo consectetur eros,  in rhoncus leo elit id mauris. In eu libero et orci elementum pulvinar. Mauris  ac diam. Duis risus tellus, placerat at, facilisis ac, tristique id, sem. Sed  venenatis lacinia augue. Aenean quis purus. </p><p>&nbsp;</p>', '', 1, 5, 0, 34, '2009-02-26 02:10:33', 62, '', '2009-02-26 02:16:53', 62, 0, '0000-00-00 00:00:00', '2009-02-26 02:10:33', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 8, '', '', 0, 37, 'robots=\nauthor='),
(51, 'The B-52''s as a New Wave', 'the-b-52s-as-a-new-wave', '', '<p align="justify"><img class="caption" src="images/stories/demo/b52_news.jpg" border="0" alt="The B-52''s originated as a New Wave " title="The B-52''s originated as a New Wave " align="right" />The B-52''s originated as a New Wave rock band formed in Athens, Georgia, United States, in 1976. The band''s name comes from a particular beehive hairdo resembling the nose cone of the aircraft of the same name. During their early years, wigs of that style were often worn by the band''s female singers Cindy Wilson and Kate Pierson.[1] The correct name for the band has long been "The B-52''s", but in 2008 they dropped the apostrophe, with their official website and Funplex album and single covers reading "The B-52s". However, the file tags on the digital releases of both of these retain the apostrophe. Both spellings could now be considered correct.</p><p align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis rhoncus. Nunc  nibh sapien, aliquam vitae, lobortis viverra, iaculis non, enim. Vivamus at  nisl. Aliquam nulla nulla, viverra eget, pellentesque vel, sollicitudin  interdum, nulla. Donec augue tellus, luctus in, auctor a, egestas a, libero.  Vestibulum arcu tellus, aliquet eget, faucibus vehicula, adipiscing at, massa.  Suspendisse potenti. Praesent ornare luctus urna. Aliquam dolor quam, feugiat  et, dictum ac, faucibus a, orci. Donec leo mi, facilisis non, luctus id,  ultrices vitae, augue. Suspendisse potenti. Pellentesque a tellus. Vivamus justo  mauris, molestie eget, euismod nec, iaculis vel, lorem. Cras malesuada velit eu  mauris. Donec cursus pede a quam. Nam eu felis id velit dapibus lacinia. </p><div align="justify"> </div><p align="justify">Praesent velit. Vestibulum lorem tellus, elementum vitae, tempor id,  ultricies et, erat. Quisque condimentum sapien a ligula. Nullam eros nulla,  adipiscing vel, dignissim at, lacinia at, mi. Nam sit amet turpis. Sed id nunc.  Mauris ut nunc quis nisl condimentum commodo. Fusce vel dolor in augue vulputate  blandit. Proin pharetra lacinia sapien. Suspendisse ut erat. Morbi lorem quam,  laoreet eget, consequat ut, cursus vel, tellus. Praesent interdum tellus et  sapien. Maecenas porttitor justo ut mauris. Phasellus vehicula congue urna. </p><div align="justify"> </div><p align="justify">Integer tempus. Donec vitae lectus eget urna lobortis porta. Fusce tortor  diam, viverra in, commodo vel, tincidunt nec, odio. Vivamus convallis. Class  aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos  himenaeos. Sed at magna at sem sollicitudin lobortis. Suspendisse facilisis.  Cras iaculis, lorem a porttitor scelerisque, nulla urna ultricies tortor, at  molestie odio felis sed nisl. Vivamus varius est ut lorem. Sed sit amet massa.  Proin quis tortor id quam tempor ullamcorper. Etiam est magna, malesuada in,  consequat eu, pharetra nec, ligula. Aliquam a velit. Aliquam ac felis. </p><div align="justify"> </div><p align="justify">Cras blandit mauris sed justo. Etiam accumsan, ante vel posuere elementum,  risus urna tempor lacus, eget </p><p align="justify">&nbsp;</p>', '', 1, 5, 0, 34, '2009-02-26 00:00:00', 62, '', '2009-02-28 01:54:34', 62, 0, '0000-00-00 00:00:00', '2009-02-26 02:37:46', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 3, 0, 7, '', '', 0, 33, 'robots=\nauthor='),
(52, 'Shakira for Bollywood', 'shakira-for-bollywood', '', '<p align="justify"><img class="caption" src="images/stories/demo/shakira.png" border="0" alt="Shakira to hip-shake for Bollywood" title="Shakira to hip-shake for Bollywood" align="right" /><span class="storyfirstpara">If things go as  planned, pop sensation Shakira would sing and shake her booty for a Bollywood  flick. </span><span class="storybody">Film producer Surya Pratap is in  final stages of negotiations with Shakira for a song in his movie �The Final  Call� which is based on the theme of father-son relationship. The producer has  already approached Sanjay Dutt for the lead role and the actor is learnt to have expressed his  interest but is yet to give his final nod. Similarly, Shakira has been in  talks to croon a song and she has reportedly agreed. Only legalities on paper  need to be done for her to be an official part of the film.</span></p><p align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis rhoncus. Nunc  nibh sapien, aliquam vitae, lobortis viverra, iaculis non, enim. Vivamus at  nisl. Aliquam nulla nulla, viverra eget, pellentesque vel, sollicitudin  interdum, nulla. Donec augue tellus, luctus in, auctor a, egestas a, libero.  Vestibulum arcu tellus, aliquet eget, faucibus vehicula, adipiscing at, massa.  Suspendisse potenti. Praesent ornare luctus urna. Aliquam dolor quam, feugiat  et, dictum ac, faucibus a, orci. Donec leo mi, facilisis non, luctus id,  ultrices vitae, augue. Suspendisse potenti. Pellentesque a tellus. Vivamus justo  mauris, molestie eget, euismod nec, iaculis vel, lorem. Cras malesuada velit eu  mauris. Donec cursus pede a quam. Nam eu felis id velit dapibus lacinia. </p><div align="justify"> </div><p align="justify">Praesent velit. Vestibulum lorem tellus, elementum vitae, tempor id,  ultricies et, erat. Quisque condimentum sapien a ligula. Nullam eros nulla,  adipiscing vel, dignissim at, lacinia at, mi. Nam sit amet turpis. Sed id nunc.  Mauris ut nunc quis nisl condimentum commodo. Fusce vel dolor in augue vulputate  blandit. Proin pharetra lacinia sapien. Suspendisse ut erat. Morbi lorem quam,  laoreet eget, consequat ut, cursus vel, tellus. Praesent interdum tellus et  sapien. Maecenas porttitor justo ut mauris. Phasellus vehicula congue urna. </p><div align="justify"> </div><p align="justify">Integer tempus. Donec vitae lectus eget urna lobortis porta. Fusce tortor  diam, viverra in, commodo vel, tincidunt nec, odio. Vivamus convallis. Class  aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos  himenaeos. Sed at magna at sem sollicitudin lobortis. Suspendisse facilisis.  Cras iaculis, lorem a porttitor scelerisque, nulla urna ultricies tortor, at  molestie odio felis sed nisl. Vivamus varius est ut lorem. Sed sit amet massa.  Proin quis tortor id quam tempor ullamcorper. Etiam est magna, malesuada in,  consequat eu, pharetra nec, ligula. Aliquam a velit. Aliquam ac felis. </p><div align="justify"> </div><p align="justify">Cras blandit mauris sed justo. Etiam accumsan, ante vel posuere elementum,  risus urna tempor lacus, eget dapibus nulla ante sed dui. Integer vitae dui. In  ultricies nisi sit amet nisi. Cum sociis natoque penatibus et magnis dis  parturient montes, nascetur ridiculus mus. Maecenas vehicula est sed neque. Sed  ultrices ante. Donec vestibulum. Vestibulum tristique. Donec bibendum. Nulla  semper dui sit amet enim. Vivamus odio tellus, pharetra eget, laoreet eget,  consequat at, justo. Proin facilisis risus eget orci. Proin orci tellus,  scelerisque at, gravida at, pulvinar vel, dui. </p><p>&nbsp;</p>', '', 1, 5, 0, 34, '2009-02-26 03:03:58', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2009-02-26 03:03:58', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 6, '', '', 0, 119, 'robots=\nauthor='),
(53, 'TOURnews! We have Tournews!!', 'tournews-we-have-tournews', '', '<p align="justify"><img class="caption" src="images/stories/demo/stones_news.png" border="0" alt="TOURnews! We have Tournews!!" title="TOURnews! We have Tournews!!" align="left" />During the 60''s and the early 70''s, the Stones were something more than a  rock-and-roll band. Albums like ''''Beggars Banquet,'''' ''''Let It Bleed'''' and  ''''Exile on Main Street'''' articulated some of the thoughts and aspirations of an  entire generation, and even the Rolling Stones'' troubles - the murder at their  free concert in Altamont, their drug arrests - seemed to mirror the troubles  their generation of young fans was experiencing. Whatever the Rolling Stones  did, they mattered. They weren''t just a musical group, they were news. </p><div align="justify"> </div><p align="justify">The Rolling Stones'' remarkable endurance over the decades shows that rock and  roll has grown up. It has its own tradition now, and the Stones embody that  tradition; their music is rooted in traditional verities, in rhythm-and-blues  and soul music, and it has remained relevant while more ephemeral pop fashions  have come and gone.</p><p align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis rhoncus. Nunc  nibh sapien, aliquam vitae, lobortis viverra, iaculis non, enim. Vivamus at  nisl. Aliquam nulla nulla, viverra eget, pellentesque vel, sollicitudin  interdum, nulla. Donec augue tellus, luctus in, auctor a, egestas a, libero.  Vestibulum arcu tellus, aliquet eget, faucibus vehicula, adipiscing at, massa.  Suspendisse potenti. Praesent ornare luctus urna. Aliquam dolor quam, feugiat  et, dictum ac, faucibus a, orci. Donec leo mi, facilisis non, luctus id,  ultrices vitae, augue. Suspendisse potenti. Pellentesque a tellus. Vivamus justo  mauris, molestie eget, euismod nec, iaculis vel, lorem. Cras malesuada velit eu  mauris. Donec cursus pede a quam. Nam eu felis id velit dapibus lacinia. </p><div align="justify"> </div><p align="justify">Praesent velit. Vestibulum lorem tellus, elementum vitae, tempor id,  ultricies et, erat. Quisque condimentum sapien a ligula. Nullam eros nulla,  adipiscing vel, dignissim at, lacinia at, mi. Nam sit amet turpis. Sed id nunc.  Mauris ut nunc quis nisl condimentum commodo. Fusce vel dolor in augue vulputate  blandit. Proin pharetra lacinia sapien. Suspendisse ut erat. Morbi lorem quam,  laoreet eget, consequat ut, cursus vel, tellus. Praesent interdum tellus et  sapien. Maecenas porttitor justo ut mauris. Phasellus vehicula congue urna. </p><div align="justify"> </div><p align="justify">Integer tempus. Donec vitae lectus eget urna lobortis porta. Fusce tortor  diam, viverra in, commodo vel, tincidunt nec, odio. Vivamus convallis. Class  aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos  himenaeos. Sed at magna at sem sollicitudin lobortis. Suspendisse facilisis.  Cras iaculis, lorem a porttitor scelerisque, nulla urna ultricies tortor, at  molestie odio felis sed nisl. Vivamus varius est ut lorem. Sed sit amet massa.  Proin quis tortor id quam tempor ullamcorper. Etiam est magna, malesuada in,  consequat eu, pharetra nec, ligula. Aliquam a velit. Aliquam ac felis. </p><div align="justify"> </div><p align="justify">Cras blandit mauris sed justo. Etiam accumsan, ante vel posuere elementum,  risus urna tempor lacus, eget dapibus nulla ante sed dui. Integer vitae dui. In  ultricies nisi sit amet nisi. Cum sociis natoque penatibus et magnis dis  parturient montes, nascetur ridiculus mus. Maecenas vehicula est sed neque. Sed  ultrices ante. Donec vestibulum. Vestibulum tristique. Donec bibendum. Nulla  semper dui sit amet enim. Vivamus odio tellus, pharetra eget, laoreet eget,  consequat at, justo. Proin facilisis risus eget orci. Proin orci tellus,  scelerisque at, gravida at, pulvinar vel, dui. </p><p align="justify">&nbsp;</p>', '', 1, 5, 0, 34, '2009-02-26 03:30:31', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2009-02-26 03:30:31', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 5, '', '', 0, 99, 'robots=\nauthor='),
(54, 'Barry Adamsom and Nick Cave', 'barry-adamsom-and-nick-cave', '', '<p align="justify"><img class="caption" src="images/stories/demo/barryadamson.png" border="0" alt="Barry Adamsom will have Nick Cave in same stage" title="Barry Adamsom will have Nick Cave in same stage" align="right" />Nick Cave will be the special  guest next Thursday (April 8) at the first night of the <strong>Mini-Meltdown  Festival</strong> at London SE1 <strong>Royal Festival Hall Purcell  Rooms</strong>, the three day event organised by <strong>Mute Records</strong>  boss <strong>Daniel Miller</strong>. <strong>Nick</strong> will join  former <strong>Bad Seeds</strong> bandmate <strong>Barry Adamson</strong>  onstage as well as introducing the event. </p><p align="justify">Also playing with  <strong>Barry</strong> on Thursday are <strong>The Mute Allstars</strong> -  <strong>Pan Sonic</strong>, <strong>Simon Fisher Turner</strong>, <strong>Andrei  Samsonov</strong> and <strong>Add N To (X)</strong> - who will all be onstage  together and separately at various times performing a live soundtrack to a film  called <strong>A Page Of Madness</strong> by Japanese director  <strong>Kunagassa</strong>, an extraordinary black and white film made in  1926.</p><p align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis rhoncus. Nunc  nibh sapien, aliquam vitae, lobortis viverra, iaculis non, enim. Vivamus at  nisl. Aliquam nulla nulla, viverra eget, pellentesque vel, sollicitudin  interdum, nulla. Donec augue tellus, luctus in, auctor a, egestas a, libero.  Vestibulum arcu tellus, aliquet eget, faucibus vehicula, adipiscing at, massa.  Suspendisse potenti. Praesent ornare luctus urna. Aliquam dolor quam, feugiat  et, dictum ac, faucibus a, orci. Donec leo mi, facilisis non, luctus id,  ultrices vitae, augue. Suspendisse potenti. Pellentesque a tellus. Vivamus justo  mauris, molestie eget, euismod nec, iaculis vel, lorem. Cras malesuada velit eu  mauris. Donec cursus pede a quam. Nam eu felis id velit dapibus lacinia. </p><div align="justify"> </div><p align="justify">Praesent velit. Vestibulum lorem tellus, elementum vitae, tempor id,  ultricies et, erat. Quisque condimentum sapien a ligula. Nullam eros nulla,  adipiscing vel, dignissim at, lacinia at, mi. Nam sit amet turpis. Sed id nunc.  Mauris ut nunc quis nisl condimentum commodo. Fusce vel dolor in augue vulputate  blandit. Proin pharetra lacinia sapien. Suspendisse ut erat. Morbi lorem quam,  laoreet eget, consequat ut, cursus vel, tellus. Praesent interdum tellus et  sapien. Maecenas porttitor justo ut mauris. Phasellus vehicula congue urna. </p><div align="justify"> </div><p align="justify">Integer tempus. Donec vitae lectus eget urna lobortis porta. Fusce tortor  diam, viverra in, commodo vel, tincidunt nec, odio. Vivamus convallis. Class  aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos  himenaeos. Sed at magna at sem sollicitudin lobortis. Suspendisse facilisis.  Cras iaculis, lorem a porttitor scelerisque, nulla urna ultricies tortor, at  molestie odio felis sed nisl. Vivamus varius est ut lorem. Sed sit amet massa.  Proin quis tortor id quam tempor ullamcorper. Etiam est magna, malesuada in,  consequat eu, pharetra nec, ligula. Aliquam a velit. Aliquam ac felis. </p><div align="justify"> </div><p align="justify">Cras blandit mauris sed justo. Etiam accumsan, ante vel posuere elementum,  risus urna tempor lacus, eget dapibus nulla ante sed dui. Integer vitae dui. In  ultricies nisi sit amet nisi. Cum sociis natoque penatibus et magnis dis  parturient montes, nascetur ridiculus mus. Maecenas vehicula est sed neque. Sed  ultrices ante. Donec vestibulum. Vestibulum tristique. Donec bibendum. Nulla  semper dui sit amet enim. Vivamus odio tellus, pharetra eget, laoreet eget,  consequat at, justo. Proin facilisis risus eget orci. Proin orci tellus,  scelerisque at, gravida at, pulvinar vel, dui. </p><p>&nbsp;</p>', '', 1, 5, 0, 34, '2009-02-25 00:00:00', 62, '', '2009-02-26 03:52:15', 62, 0, '0000-00-00 00:00:00', '2009-02-26 03:43:24', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 3, 0, 4, '', '', 0, 79, 'robots=\nauthor='),
(55, 'Myslovitz Major Performance through the Night', 'myslovitz-major-performance', '', '<p align="justify"><img class="caption" src="images/stories/demo/myslovitz.jpg" border="0" alt="Myslovitz - The Famous Polish Band" title="Myslovitz - The Famous Polish Band" align="left" />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eu erat  faucibus ante malesuada cursus. Aenean rhoncus fermentum nulla. Vestibulum quis  sem id elit vestibulum euismod. Nam mattis mollis odio. Fusce eu nisi.  Suspendisse viverra metus et turpis. Curabitur rhoncus felis et enim. Phasellus  id dui et pede condimentum faucibus. Sed varius. Ut ut dui. Class aptent taciti  sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque  viverra mi et sem pharetra blandit. Donec dolor dolor, faucibus quis, porta sit  amet, egestas non, leo. Nulla lacus nisi, mattis quis, consequat vehicula,  viverra vitae, arcu. Phasellus fermentum, quam interdum dictum dictum, dolor  diam sodales sapien, ut gravida tortor arcu in lorem. Suspendisse potenti.  Vestibulum condimentum, nibh non semper euismod, mauris nulla eleifend ligula,  non semper eros sapien consectetur est. Curabitur lobortis vestibulum ante. </p><div align="justify"> </div><p align="justify">Duis vestibulum cursus arcu. Fusce leo lectus, viverra a, sagittis lobortis,  varius ut, lectus. Pellentesque lectus. Integer pharetra magna placerat urna.  Etiam sollicitudin lorem at mauris. Curabitur a elit eget metus sollicitudin  ultrices. Proin mollis purus nec dui. Aenean laoreet, enim id scelerisque  bibendum, massa nulla mollis erat, at placerat velit lacus vitae nunc. Aliquam  condimentum gravida velit. Ut luctus sollicitudin massa. In varius purus sit  amet massa. Aliquam erat volutpat. </p><div align="justify"> </div><p align="justify">Fusce adipiscing. Nunc sit amet eros. Nulla facilisi. Maecenas facilisis sem  tempor velit. In hac habitasse platea dictumst. Vivamus tincidunt, sapien et  pharetra ullamcorper, tellus tortor ultricies massa, ut iaculis diam velit a  turpis. Fusce ac purus. Aenean varius arcu sit amet mi. Vestibulum fringilla  metus ut tellus. Maecenas ullamcorper consequat est. Donec nunc est, dapibus  tristique, aliquam ac, pellentesque id, sem. Etiam dui. </p><div align="justify"> </div><p align="justify">Donec vestibulum laoreet ligula. Mauris imperdiet sem a neque. Phasellus non  lorem. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur  ridiculus mus. Aliquam tempus posuere nisl. Praesent gravida nunc quis purus.  Donec vulputate ligula vitae odio. Nullam vitae urna ut nunc facilisis laoreet.  In hac habitasse platea dictumst. Maecenas at lorem. Nam nisl mauris, pulvinar  a, sollicitudin vel, adipiscing ut, libero. </p><div align="justify"> </div><p align="justify">Nulla congue pede at lacus. Fusce nisl diam, hendrerit volutpat, molestie et,  eleifend at, sapien. In hac habitasse platea dictumst. Nunc lacinia, nibh a  fringilla vestibulum, erat sapien consectetur lectus, quis mollis nisl turpis et  mi. Nullam eleifend turpis vel odio. Duis feugiat. In quis lorem. Etiam quis  tellus et justo dignissim placerat. Curabitur nec sem ut ante cursus  scelerisque. Ut vehicula pretium ipsum. Nam at nisi ut lorem facilisis pulvinar.  Cras sapien libero, gravida et, egestas nec, convallis id, felis. Vivamus  adipiscing tellus nec eros. Maecenas neque. Aenean at eros sed leo rutrum  adipiscing. Curabitur varius. Etiam molestie urna sit amet ante. Curabitur et  justo. Duis auctor dolor. Suspendisse diam nulla, tincidunt et, laoreet sit  amet, tempus ut, nisi. </p>', '', 1, 5, 0, 34, '2009-02-28 02:05:32', 62, '', '2009-02-28 02:10:51', 62, 0, '0000-00-00 00:00:00', '2009-02-28 02:05:32', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 3, 0, 3, '', '', 0, 28, 'robots=\nauthor='),
(56, 'Metallica To Be Inducted Into Hall of Fame', 'metallica-to-be-inducted-into-hall-of-fame', '', '<div align="justify"><img class="caption" src="images/stories/demo/metallica.jpg" border="0" alt="Metallica To Be Inducted Into Hall of Fame" title="Metallica To Be Inducted Into Hall of Fame" align="left" />Wow . . . what a great way to start off 2009! We are beyond proud to let you  know that we will indeed be inducted into the Rock and Roll Hall of Fame in a  ceremony held in Cleveland on April 4, 2009. As a refresher, it has to have been  25 years since your first record was released to be eligible and while it  certainly doesn''t feel like it''s been that long, we hear that''s the case!  <br /><br />So here''s the other cool part. For the first time in the 24 years that  the Hall of Fame has been around, tickets to the ceremony will be available to  YOU! The induction ceremony will be held at Public Hall in Cleveland, and if  you''re a member of the Hall of Fame and Museum, you can buy tickets in a  pre-sale on January 22 and 23. There is a walk-up sale on Saturday, January 24  (that means you have to find your way to the box office at the Hall of Fame that  day), and finally they will be available through all Ticketmaster outlets  starting on Monday, January 26. Met Club members should check www.metclub.com  for info about a special ticket sale for them.</div><div align="justify"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eu erat  faucibus ante malesuada cursus. Aenean rhoncus fermentum nulla. Vestibulum quis  sem id elit vestibulum euismod. Nam mattis mollis odio. Fusce eu nisi.  Suspendisse viverra metus et turpis. Curabitur rhoncus felis et enim. Phasellus  id dui et pede condimentum faucibus. Sed varius. Ut ut dui. Class aptent taciti  sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque  viverra mi et sem pharetra blandit. Donec dolor dolor, faucibus quis, porta sit  amet, egestas non, leo. Nulla lacus nisi, mattis quis, consequat vehicula,  viverra vitae, arcu. Phasellus fermentum, quam interdum dictum dictum, dolor  diam sodales sapien, ut gravida tortor arcu in lorem. Suspendisse potenti.  Vestibulum condimentum, nibh non semper euismod, mauris nulla eleifend ligula,  non semper eros sapien consectetur est. Curabitur lobortis vestibulum ante. </p> <p>Duis vestibulum cursus arcu. Fusce leo lectus, viverra a, sagittis lobortis,  varius ut, lectus. Pellentesque lectus. Integer pharetra magna placerat urna.  Etiam sollicitudin lorem at mauris. Curabitur a elit eget metus sollicitudin  ultrices. Proin mollis purus nec dui. Aenean laoreet, enim id scelerisque  bibendum, massa nulla mollis erat, at placerat velit lacus vitae nunc. Aliquam  condimentum gravida velit. Ut luctus sollicitudin massa. In varius purus sit  amet massa. Aliquam erat volutpat. </p> <p>Fusce adipiscing. Nunc sit amet eros. Nulla facilisi. Maecenas facilisis sem  tempor velit. In hac habitasse platea dictumst. Vivamus tincidunt, sapien et  pharetra ullamcorper, tellus tortor ultricies massa, ut iaculis diam velit a  turpis. Fusce ac purus. Aenean varius arcu sit amet mi. Vestibulum fringilla  metus ut tellus. Maecenas ullamcorper consequat est. Donec nunc est, dapibus  tristique, aliquam ac, pellentesque id, sem. Etiam dui. </p> <p>Donec vestibulum laoreet ligula. Mauris imperdiet sem a neque. Phasellus non  lorem. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur  ridiculus mus. Aliquam tempus posuere nisl. Praesent gravida nunc quis purus.  Donec vulputate ligula vitae odio. Nullam vitae urna ut nunc facilisis laoreet.  In hac habitasse platea dictumst. Maecenas at lorem. Nam nisl mauris, pulvinar  a, sollicitudin vel, adipiscing ut, libero. </p></div>', '', 1, 5, 0, 34, '2009-02-28 04:47:04', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2009-02-28 04:47:04', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 2, '', '', 0, 78, 'robots=\nauthor='),
(57, 'Modules Positions', 'modules-positions', '', '<p><span class="color-6">MUSICTUBE supports full width configurations for use with galleries or forums, make sure you do not publish any modules in right and the ?component will fill the entire width.</span></p><p>&nbsp;</p><div style="text-align: center"><img src="images/stories/demo/modulepositions.png" border="0" width="850" /></div> <br /><p>&nbsp;</p>', '', 1, 0, 0, 0, '2009-02-28 05:25:10', 62, '', '2009-02-28 13:08:06', 62, 0, '0000-00-00 00:00:00', '2009-02-28 05:25:10', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 5, 0, 1, '', '', 0, 325, 'robots=\nauthor='),
(58, 'Josh Stone on the Spotlight', 'josh-stone-on-the-spotlight', '', '<img class="caption" src="images/stories/demo/josh_plug2.jpg" border="0" alt="Josh Stone Spotlight" title="Josh Stone Spotlight" align="left" /><span class="content">Joss Stone is a nineteen-year-old Brit with a huge voice  that packs grit and diva-tude beyond its years. There are a couple of moments on  Stone''s third album when she clobbers a melody with enough showy vocal oomph to  make even Christina Aguilera fans squirm. But for the most part, Stone employs  her remarkable instrument with focus and nuance on <em>Introducing</em>, and the  result is an album full of solid pop-wise R&B. Warm acoustic funk grooves  and strong melodies are all over classic-soul-infused cuts like "Baby Baby Baby"  and big, bright stuff like "Put Your Hands on Me." Getting cameos from Common  and Lauryn Hill on an album like this is like inviting Barbra Streisand to sing  at the Democratic National Convention: pretty frigging obvious. But those two  songs are pretty good, too.</span> <div align="justify"><p align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam eu erat  faucibus ante malesuada cursus. Aenean rhoncus fermentum nulla. Vestibulum quis  sem id elit vestibulum euismod. Nam mattis mollis odio. Fusce eu nisi.  Suspendisse viverra metus et turpis. Curabitur rhoncus felis et enim. Phasellus  id dui et pede condimentum faucibus. Sed varius. Ut ut dui. Class aptent taciti  sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque  viverra mi et sem pharetra blandit. Donec dolor dolor, faucibus quis, porta sit  amet, egestas non, leo. Nulla lacus nisi, mattis quis, consequat vehicula,  viverra vitae, arcu. Phasellus fermentum, quam interdum dictum dictum, dolor  diam sodales sapien, ut gravida tortor arcu in lorem. Suspendisse potenti.  Vestibulum condimentum, nibh non semper euismod, mauris nulla eleifend ligula,  non semper eros sapien consectetur est. Curabitur lobortis vestibulum ante. </p><div align="justify"> </div><p align="justify">Duis vestibulum cursus arcu. Fusce leo lectus, viverra a, sagittis lobortis,  varius ut, lectus. Pellentesque lectus. Integer pharetra magna placerat urna.  Etiam sollicitudin lorem at mauris. Curabitur a elit eget metus sollicitudin  ultrices. Proin mollis purus nec dui. Aenean laoreet, enim id scelerisque  bibendum, massa nulla mollis erat, at placerat velit lacus vitae nunc. Aliquam  condimentum gravida velit. Ut luctus sollicitudin massa. In varius purus sit  amet massa. Aliquam erat volutpat. </p><div align="justify"> </div><p align="justify">Fusce adipiscing. Nunc sit amet eros. Nulla facilisi. Maecenas facilisis sem  tempor velit. In hac habitasse platea dictumst. Vivamus tincidunt, sapien et  pharetra ullamcorper, tellus tortor ultricies massa, ut iaculis diam velit a  turpis. Fusce ac purus. Aenean varius arcu sit amet mi. Vestibulum fringilla  metus ut tellus. Maecenas ullamcorper consequat est. Donec nunc est, dapibus  tristique, aliquam ac, pellentesque id, sem. Etiam dui. </p><div align="justify"> </div><p align="justify">Donec vestibulum laoreet ligula. Mauris imperdiet sem a neque. Phasellus non  lorem. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur  ridiculus mus. Aliquam tempus posuere nisl. Praesent gravida nunc quis purus.  Donec vulputate ligula vitae odio. Nullam vitae urna ut nunc facilisis laoreet.  In hac habitasse platea dictumst. Maecenas at lorem. Nam nisl mauris, pulvinar  a, sollicitudin vel, adipiscing ut, libero. </p><div align="justify"> </div><p align="justify">Nulla congue pede at lacus. Fusce nisl diam, hendrerit volutpat, molestie et,  eleifend at, sapien. In hac habitasse platea dictumst. Nunc lacinia, nibh a  fringilla vestibulum, erat sapien consectetur lectus, quis mollis nisl turpis et  mi. Nullam eleifend turpis vel odio. Duis feugiat. In quis lorem. Etiam quis  tellus et justo dignissim placerat. Curabitur nec sem ut ante cursus  scelerisque. Ut vehicula pretium ipsum. Nam at nisi ut lorem facilisis pulvinar.  Cras sapien libero, gravida et, egestas nec, convallis id, felis. Vivamus  adipiscing tellus nec eros. Maecenas neque. Aenean at eros sed leo rutrum  adipiscing. Curabitur varius. Etiam molestie urna sit amet ante. Curabitur et  justo. Duis auctor dolor. Suspendisse diam nulla, tincidunt et, laoreet sit  amet, tempus ut, nisi. </p><p align="justify">Nulla congue pede at lacus. Fusce nisl diam, hendrerit volutpat, molestie et, eleifend at, sapien. In hac habitasse platea dictumst. Nunc lacinia, nibh a fringilla vestibulum, erat sapien consectetur lectus, quis mollis nisl turpis et mi. Nullam eleifend turpis vel odio. Duis feugiat. In quis lorem. Etiam quis tellus et justo dignissim placerat. Curabitur nec sem ut ante cursus scelerisque. Ut vehicula pretium ipsum. Nam at nisi ut lorem facilisis pulvinar. Cras sapien libero, gravida et, egestas nec, convallis id, felis. Vivamus adipiscing tellus nec eros. Maecenas neque. Aenean at eros sed leo rutrum adipiscing. Curabitur varius. Etiam molestie urna sit amet ante. Curabitur et justo. Duis auctor dolor. Suspendisse diam nulla, tincidunt et, laoreet sit amet, tempus ut, nisi. </p> </div>', '', 1, 5, 0, 34, '2009-02-28 06:19:00', 62, '', '2009-02-28 06:37:26', 62, 0, '0000-00-00 00:00:00', '2009-02-28 06:19:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 1, '', '', 0, 72, 'robots=\nauthor='),
(59, 'ARMP', 'armp', '', '<p><img src="images/stories/Logo_ARMP.png" width="50" height="63" alt="Logo_ARMP" style="color: #339966; font-size: 19px; line-height: 18px;" />&nbsp; &nbsp; <span style="font-size: 36pt;"><sup><span style="color: #339966; line-height: 18px;">ARMP</span></sup></span></p>\r\n<p></p>\r\n<p><span style="color: #339966;"><span style="font-size: 14pt;"><br /></span></span></p>\r\n<p></p>\r\n<p><span style="color: #339966;"><span style="font-size: 14pt;">Contenu � compl�ter...</span></span></p>', '', 1, 1, 0, 3, '2012-11-27 13:04:10', 62, '', '2012-11-28 12:19:03', 62, 0, '0000-00-00 00:00:00', '2012-11-27 13:04:10', '0000-00-00 00:00:00', '', '', 'show_title=0\nlink_titles=\nshow_intro=0\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=0\nshow_create_date=0\nshow_modify_date=0\nshow_pdf_icon=0\nshow_print_icon=0\nshow_email_icon=0\nlanguage=\nkeyref=\nreadmore=', 14, 0, 4, '', '', 0, 23, 'robots=\nauthor='),
(60, 'DNCMP', 'dncmp', '', '<p></p>\r\n<p></p>\r\n<p><span style="color: #339966; font-size: 36pt;">DNCMP</span></p>\r\n<p><span style="color: #339966; font-size: 36pt;"><br /></span></p>\r\n<p></p>\r\n<p></p>\r\n<p><span style="font-size: 18pt;"><span color="#339966" size="2" style="color: #339966;"><span style="line-height: 13px;">Contenu � compl�ter....</span></span></span></p>\r\n<p></p>\r\n<p><span style="font-size: 18pt;"><span color="#339966" size="2" style="color: #339966; font-size: x-small;"><span style="line-height: 13px;"><br /></span></span></span></p>\r\n<p></p>\r\n<p></p>\r\n<p></p>\r\n<p></p>\r\n<p></p>\r\n<p></p>\r\n<p></p>\r\n<p></p>\r\n<p></p>\r\n<p></p>\r\n<p></p>\r\n<p></p>\r\n<p></p>\r\n<p></p>\r\n<p><span style="font-size: 18pt;"><span color="#339966" size="2" style="color: #339966; font-size: x-small;"><span style="line-height: 13px;"><br /></span></span></span></p>', '', 1, 1, 0, 3, '2012-11-28 12:19:21', 62, '', '2012-11-28 12:35:55', 62, 0, '0000-00-00 00:00:00', '2012-11-28 12:19:21', '0000-00-00 00:00:00', '', '', 'show_title=0\nlink_titles=\nshow_intro=0\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=0\nshow_create_date=0\nshow_modify_date=0\nshow_pdf_icon=0\nshow_print_icon=0\nshow_email_icon=0\nlanguage=\nkeyref=\nreadmore=', 9, 0, 2, '', '', 0, 11, 'robots=\nauthor='),
(61, 'VERT', 'vert', '', '<p><img src="images/stories/numero-vert.png" /></p>\r\n<p><span style="color: #339966; font-size: 36pt;"><img src="images/stories/n-vert.jpg" width="210" height="50" alt="n-vert" style="vertical-align: top;" />&nbsp; &nbsp;Num�ro vert</span></p>\r\n<p></p>\r\n<p><span style="color: #000000; font-size: 14pt;">Avec le num�ro vert vous avez la possibilit� d''appeller gratuitement en cas de besoin.</span></p>\r\n<p></p>\r\n<p></p>\r\n<p><span style="color: #339966; font-size: 48px; line-height: 48px;"><br /></span></p>\r\n<p></p>\r\n<p></p>\r\n<p><span style="color: #000000; font-size: 18pt;"><img src="images/stories/armpcentre.jpg" width="170" height="85" alt="armpcentre" />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span><span style="color: #339966; font-size: 48px; line-height: 48px;">Centre d''appel</span></p>\r\n<p></p>\r\n<p><span style="font-size: 14pt; line-height: 48px;">Un centre d''appel fonctionnel 24h/24 est mis � la disposition des clients</span></p>\r\n<p><span style="color: #000000; font-size: 18pt;"><br /></span></p>', '', 1, 1, 0, 3, '2012-11-28 12:59:11', 62, '', '2012-12-04 08:41:27', 62, 62, '2012-12-04 08:41:29', '2012-11-28 12:59:11', '0000-00-00 00:00:00', '', '', 'show_title=0\nlink_titles=\nshow_intro=0\nshow_section=0\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=0\nshow_modify_date=0\nshow_pdf_icon=0\nshow_print_icon=0\nshow_email_icon=0\nlanguage=\nkeyref=\nreadmore=', 4, 0, 1, '', '', 0, 17, 'robots=\nauthor=');

-- --------------------------------------------------------

--
-- Structure de la table `jos_content_frontpage`
--

CREATE TABLE IF NOT EXISTS `jos_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `jos_content_frontpage`
--

INSERT INTO `jos_content_frontpage` (`content_id`, `ordering`) VALUES
(47, 1);

-- --------------------------------------------------------

--
-- Structure de la table `jos_content_rating`
--

CREATE TABLE IF NOT EXISTS `jos_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(11) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(11) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `jos_content_rating`
--


-- --------------------------------------------------------

--
-- Structure de la table `jos_core_acl_aro`
--

CREATE TABLE IF NOT EXISTS `jos_core_acl_aro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_value` varchar(240) NOT NULL DEFAULT '0',
  `value` varchar(240) NOT NULL DEFAULT '',
  `order_value` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `jos_section_value_value_aro` (`section_value`(100),`value`(100)),
  KEY `jos_gacl_hidden_aro` (`hidden`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `jos_core_acl_aro`
--

INSERT INTO `jos_core_acl_aro` (`id`, `section_value`, `value`, `order_value`, `name`, `hidden`) VALUES
(10, 'users', '62', 0, 'Administrator', 0),
(11, 'users', '63', 0, 'khadim', 0),
(12, 'users', '64', 0, 'sarr', 0),
(13, 'users', '65', 0, 'Rama', 0);

-- --------------------------------------------------------

--
-- Structure de la table `jos_core_acl_aro_groups`
--

CREATE TABLE IF NOT EXISTS `jos_core_acl_aro_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `jos_gacl_parent_id_aro_groups` (`parent_id`),
  KEY `jos_gacl_lft_rgt_aro_groups` (`lft`,`rgt`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Contenu de la table `jos_core_acl_aro_groups`
--

INSERT INTO `jos_core_acl_aro_groups` (`id`, `parent_id`, `name`, `lft`, `rgt`, `value`) VALUES
(17, 0, 'ROOT', 1, 22, 'ROOT'),
(28, 17, 'USERS', 2, 21, 'USERS'),
(29, 28, 'Public Frontend', 3, 12, 'Public Frontend'),
(18, 29, 'Registered', 4, 11, 'Registered'),
(19, 18, 'Author', 5, 10, 'Author'),
(20, 19, 'Editor', 6, 9, 'Editor'),
(21, 20, 'Publisher', 7, 8, 'Publisher'),
(30, 28, 'Public Backend', 13, 20, 'Public Backend'),
(23, 30, 'Manager', 14, 19, 'Manager'),
(24, 23, 'Administrator', 15, 18, 'Administrator'),
(25, 24, 'Super Administrator', 16, 17, 'Super Administrator');

-- --------------------------------------------------------

--
-- Structure de la table `jos_core_acl_aro_map`
--

CREATE TABLE IF NOT EXISTS `jos_core_acl_aro_map` (
  `acl_id` int(11) NOT NULL DEFAULT '0',
  `section_value` varchar(230) NOT NULL DEFAULT '0',
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`acl_id`,`section_value`,`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `jos_core_acl_aro_map`
--


-- --------------------------------------------------------

--
-- Structure de la table `jos_core_acl_aro_sections`
--

CREATE TABLE IF NOT EXISTS `jos_core_acl_aro_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(230) NOT NULL DEFAULT '',
  `order_value` int(11) NOT NULL DEFAULT '0',
  `name` varchar(230) NOT NULL DEFAULT '',
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `jos_gacl_value_aro_sections` (`value`),
  KEY `jos_gacl_hidden_aro_sections` (`hidden`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `jos_core_acl_aro_sections`
--

INSERT INTO `jos_core_acl_aro_sections` (`id`, `value`, `order_value`, `name`, `hidden`) VALUES
(10, 'users', 1, 'Users', 0);

-- --------------------------------------------------------

--
-- Structure de la table `jos_core_acl_groups_aro_map`
--

CREATE TABLE IF NOT EXISTS `jos_core_acl_groups_aro_map` (
  `group_id` int(11) NOT NULL DEFAULT '0',
  `section_value` varchar(240) NOT NULL DEFAULT '',
  `aro_id` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `group_id_aro_id_groups_aro_map` (`group_id`,`section_value`,`aro_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `jos_core_acl_groups_aro_map`
--

INSERT INTO `jos_core_acl_groups_aro_map` (`group_id`, `section_value`, `aro_id`) VALUES
(18, '', 11),
(18, '', 12),
(18, '', 13),
(25, '', 10);

-- --------------------------------------------------------

--
-- Structure de la table `jos_core_log_items`
--

CREATE TABLE IF NOT EXISTS `jos_core_log_items` (
  `time_stamp` date NOT NULL DEFAULT '0000-00-00',
  `item_table` varchar(50) NOT NULL DEFAULT '',
  `item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `jos_core_log_items`
--


-- --------------------------------------------------------

--
-- Structure de la table `jos_core_log_searches`
--

CREATE TABLE IF NOT EXISTS `jos_core_log_searches` (
  `search_term` varchar(128) NOT NULL DEFAULT '',
  `hits` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `jos_core_log_searches`
--


-- --------------------------------------------------------

--
-- Structure de la table `jos_gk2_photoslide_extensions`
--

CREATE TABLE IF NOT EXISTS `jos_gk2_photoslide_extensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `status` int(11) NOT NULL,
  `type` varchar(128) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `phpclassfile` varchar(255) NOT NULL,
  `version` varchar(128) NOT NULL,
  `author` varchar(128) NOT NULL,
  `desc` mediumtext NOT NULL,
  `storage` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `jos_gk2_photoslide_extensions`
--


-- --------------------------------------------------------

--
-- Structure de la table `jos_gk2_photoslide_groups`
--

CREATE TABLE IF NOT EXISTS `jos_gk2_photoslide_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `plugin` varchar(255) NOT NULL,
  `mediumThumbX` int(11) NOT NULL DEFAULT '0',
  `mediumThumbY` int(11) NOT NULL DEFAULT '0',
  `smallThumbX` int(11) NOT NULL DEFAULT '0',
  `smallThumbY` int(11) NOT NULL DEFAULT '0',
  `bgcolor` varchar(7) NOT NULL DEFAULT '0',
  `titlecolor` varchar(7) NOT NULL DEFAULT '0',
  `textcolor` varchar(7) NOT NULL DEFAULT '0',
  `linkcolor` varchar(7) NOT NULL DEFAULT '0',
  `hlinkcolor` varchar(7) NOT NULL DEFAULT '0',
  `quality` int(3) NOT NULL DEFAULT '75',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `jos_gk2_photoslide_groups`
--

INSERT INTO `jos_gk2_photoslide_groups` (`id`, `name`, `plugin`, `mediumThumbX`, `mediumThumbY`, `smallThumbX`, `smallThumbY`, `bgcolor`, `titlecolor`, `textcolor`, `linkcolor`, `hlinkcolor`, `quality`) VALUES
(1, 'Togo slide', 'GK News Image III', 554, 290, 90, 47, '#000000', '#FFFFFF', '#AAAAAA', '#CCCCCC', '#EEEEEE', 85);

-- --------------------------------------------------------

--
-- Structure de la table `jos_gk2_photoslide_plugins`
--

CREATE TABLE IF NOT EXISTS `jos_gk2_photoslide_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `status` int(11) NOT NULL,
  `type` varchar(128) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `phpclassfile` varchar(255) NOT NULL,
  `version` varchar(128) NOT NULL,
  `author` varchar(128) NOT NULL,
  `desc` mediumtext NOT NULL,
  `gfields` mediumtext NOT NULL,
  `sfields` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `jos_gk2_photoslide_plugins`
--

INSERT INTO `jos_gk2_photoslide_plugins` (`id`, `name`, `status`, `type`, `filename`, `phpclassfile`, `version`, `author`, `desc`, `gfields`, `sfields`) VALUES
(1, 'GK News Image I', 1, 'module', 'plg_gk_news_image_1.xml', 'plg_gk_news_image_1.php', '2.0', 'GavickPro', 'XML file for module Gavick News Image I', 'mediumThumbX,mediumThumbY,smallThumbX,smallThumbY,bgcolor,titlecolor,textcolor,linkcolor,hlinkcolor,quality', 'title,text,linktype,linkvalue,article,wordcount,stretch'),
(2, 'GK News Image III', 1, 'module', 'plg_gk_news_image_3.xml', 'plg_gk_news_image_3.php', '2.0', 'GavickPro', 'XML file for module Gavick News Image III', 'mediumThumbX,mediumThumbY,smallThumbX,smallThumbY,bgcolor,titlecolor,textcolor,linkcolor,hlinkcolor,quality', 'title,text,linktype,linkvalue,article,wordcount,stretch');

-- --------------------------------------------------------

--
-- Structure de la table `jos_gk2_photoslide_slides`
--

CREATE TABLE IF NOT EXISTS `jos_gk2_photoslide_slides` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `published` int(1) NOT NULL,
  `access` int(1) NOT NULL,
  `file` varchar(255) NOT NULL,
  `order` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `text` varchar(255) NOT NULL DEFAULT '0',
  `linktype` int(1) NOT NULL DEFAULT '0',
  `linkvalue` varchar(255) NOT NULL DEFAULT '0',
  `article` int(11) NOT NULL DEFAULT '0',
  `wordcount` int(4) NOT NULL DEFAULT '0',
  `stretch` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `jos_gk2_photoslide_slides`
--

INSERT INTO `jos_gk2_photoslide_slides` (`id`, `group_id`, `name`, `published`, `access`, `file`, `order`, `title`, `text`, `linktype`, `linkvalue`, `article`, `wordcount`, `stretch`) VALUES
(1, 1, 'Avis d''attribution', 1, 0, '172015avis_attribution.jpg', 4, 'Avis d''attribution', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 1, '', 43, 50, 0),
(2, 1, 'Contentieux', 1, 0, '891867contentieux.jpg', 3, 'Contentieux', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 1, '', 43, 50, 0),
(3, 1, 'Avis d''appel d''offres', 1, 0, '254138avis_appel_offres.jpg', 2, 'Avis d''appel d''offres', '', 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 43, 50, 0),
(4, 1, 'Plans de passation', 1, 0, '871377plan_passation.jpg', 1, 'Plans de passation', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', 1, '', 43, 50, 0);

-- --------------------------------------------------------

--
-- Structure de la table `jos_gk2_tabs_manager_extensions`
--

CREATE TABLE IF NOT EXISTS `jos_gk2_tabs_manager_extensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `status` int(11) NOT NULL,
  `type` varchar(128) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `phpclassfile` varchar(255) NOT NULL,
  `version` varchar(128) NOT NULL,
  `author` varchar(128) NOT NULL,
  `desc` mediumtext NOT NULL,
  `storage` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `jos_gk2_tabs_manager_extensions`
--


-- --------------------------------------------------------

--
-- Structure de la table `jos_gk2_tabs_manager_groups`
--

CREATE TABLE IF NOT EXISTS `jos_gk2_tabs_manager_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `plugin` varchar(255) NOT NULL,
  `desc` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `jos_gk2_tabs_manager_groups`
--

INSERT INTO `jos_gk2_tabs_manager_groups` (`id`, `name`, `plugin`, `desc`) VALUES
(1, 'Demo TabArts white', 'Tab Arts GK2', 'Demo content for white TabArts module'),
(2, 'Demo TabArts black', 'Tab Arts GK2', 'Demo content for black TabArts module'),
(3, 'Demo TabsMix', 'Tab Mix GK1', 'Demo TabMix module');

-- --------------------------------------------------------

--
-- Structure de la table `jos_gk2_tabs_manager_plugins`
--

CREATE TABLE IF NOT EXISTS `jos_gk2_tabs_manager_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `status` int(11) NOT NULL,
  `type` varchar(128) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `phpclassfile` varchar(255) NOT NULL,
  `version` varchar(128) NOT NULL,
  `author` varchar(128) NOT NULL,
  `desc` mediumtext NOT NULL,
  `gfields` mediumtext NOT NULL,
  `sfields` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `jos_gk2_tabs_manager_plugins`
--

INSERT INTO `jos_gk2_tabs_manager_plugins` (`id`, `name`, `status`, `type`, `filename`, `phpclassfile`, `version`, `author`, `desc`, `gfields`, `sfields`) VALUES
(1, 'Tab Arts GK2', 1, 'module', 'plg_tab_arts_gk2.xml', 'plg_tab_arts_gk2.php', '2.0', 'GavickPro', 'XML file for module Tab Arts GK2', 'desc', 'art'),
(2, 'Tab Mix GK1', 1, 'module', 'plg_tab_mix_gk1.xml', 'plg_tab_mix_gk1.php', '1.0', 'GavickPro', 'XML file for module Tab Mix GK1', 'desc', 'type,position,art,content');

-- --------------------------------------------------------

--
-- Structure de la table `jos_gk2_tabs_manager_tabs`
--

CREATE TABLE IF NOT EXISTS `jos_gk2_tabs_manager_tabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `published` int(1) NOT NULL,
  `access` int(1) NOT NULL,
  `order` int(11) NOT NULL,
  `art` int(11) NOT NULL DEFAULT '0',
  `type` int(1) NOT NULL DEFAULT '0',
  `position` varchar(255) NOT NULL DEFAULT '',
  `content` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `jos_gk2_tabs_manager_tabs`
--

INSERT INTO `jos_gk2_tabs_manager_tabs` (`id`, `group_id`, `name`, `published`, `access`, `order`, `art`, `type`, `position`, `content`) VALUES
(1, 1, 'Tab 1', 1, 0, 1, 51, 0, '', ''),
(2, 1, 'Tab 2', 1, 0, 2, 47, 0, '', ''),
(3, 1, 'Tab 3', 1, 0, 3, 49, 0, '', ''),
(4, 1, 'Tab 4', 1, 0, 4, 55, 0, '', ''),
(5, 2, 'Tab 1', 1, 0, 1, 47, 0, '', ''),
(6, 2, 'Tab 2', 1, 0, 2, 49, 0, '', ''),
(7, 2, 'Tab 3', 1, 0, 3, 51, 0, '', ''),
(8, 2, 'Tab 4', 1, 0, 4, 55, 0, '', ''),
(9, 3, 'Video', 1, 0, 1, 43, 2, 'userarea', '&lt;object width=&quot;280&quot; height=&quot;165&quot;&gt;&lt;param name=&quot;allowfullscreen&quot; value=&quot;true&quot; /&gt;&lt;param name=&quot;allowscriptaccess&quot; value=&quot;always&quot; /&gt;&lt;param name=&quot;movie&quot; value=&quot;http://vimeo.com/moogaloop.swf?clip_id=2396020&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=41BCEA&amp;fullscreen=1&quot; /&gt;&lt;embed src=&quot;http://vimeo.com/moogaloop.swf?clip_id=2396020&amp;server=vimeo.com&amp;show_title=0&amp;show_byline=0&amp;show_portrait=0&amp;color=41BCEA&amp;fullscreen=1&quot; type=&quot;application/x-shockwave-flash&quot; allowfullscreen=&quot;true&quot; allowscriptaccess=&quot;always&quot; wmode=&quot;transparent&quot; width=&quot;280&quot; height=&quot;165&quot;&gt;&lt;/embed&gt;&lt;/object&gt;');

-- --------------------------------------------------------

--
-- Structure de la table `jos_groups`
--

CREATE TABLE IF NOT EXISTS `jos_groups` (
  `id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `jos_groups`
--

INSERT INTO `jos_groups` (`id`, `name`) VALUES
(0, 'Public'),
(1, 'Registered'),
(2, 'Special');

-- --------------------------------------------------------

--
-- Structure de la table `jos_jce_groups`
--

CREATE TABLE IF NOT EXISTS `jos_jce_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `users` text NOT NULL,
  `types` varchar(255) NOT NULL,
  `components` text NOT NULL,
  `rows` text NOT NULL,
  `plugins` varchar(255) NOT NULL,
  `published` tinyint(3) NOT NULL,
  `ordering` int(11) NOT NULL,
  `checked_out` tinyint(3) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `jos_jce_groups`
--

INSERT INTO `jos_jce_groups` (`id`, `name`, `description`, `users`, `types`, `components`, `rows`, `plugins`, `published`, `ordering`, `checked_out`, `checked_out_time`, `params`) VALUES
(1, 'Default', 'Default group for all users with edit access', '', '19,20,21,23,24,25', '', '7,15,10,11,18,14,5,8,16,6,17,13,12,9;31,22,30,34,21,24,29,25,19,27,46,20,26,23;45,35,36,37,41,38,40,42,43,39,44;55,52,56,51,48,53,47,49,50', '1,2,3,4,5,19,20,35,36,37,38,39,40,47,48,49,50,51,52,53,55,56', 1, 1, 0, '0000-00-00 00:00:00', ''),
(2, 'Front End', 'Sample Group for Authors, Editors, Publishers', '', '19,20,21', '', '7,15,18,14,5,8,16,6,27,17,13,12,9,26;30,45,34,41,24,48,29,25,19,46,20,42,43,49;55,52,31,56,37,51,38,40,53,47,23,44,50', '1,3,5,19,20,37,38,40,47,48,49,50,51,52,53,55,56', 0, 2, 0, '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Structure de la table `jos_jce_plugins`
--

CREATE TABLE IF NOT EXISTS `jos_jce_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `layout` varchar(255) NOT NULL,
  `row` int(11) NOT NULL,
  `ordering` int(11) NOT NULL,
  `published` tinyint(3) NOT NULL,
  `editable` tinyint(3) NOT NULL,
  `iscore` tinyint(3) NOT NULL,
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `plugin` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=57 ;

--
-- Contenu de la table `jos_jce_plugins`
--

INSERT INTO `jos_jce_plugins` (`id`, `title`, `name`, `type`, `icon`, `layout`, `row`, `ordering`, `published`, `editable`, `iscore`, `checked_out`, `checked_out_time`) VALUES
(1, 'Context Menu', 'contextmenu', 'plugin', '', '', 0, 0, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(2, 'File Browser', 'browser', 'plugin', '', '', 0, 0, 1, 1, 1, 0, '0000-00-00 00:00:00'),
(3, 'Inline Popups', 'inlinepopups', 'plugin', '', '', 0, 0, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(4, 'Media Support', 'media', 'plugin', '', '', 0, 0, 1, 1, 1, 0, '0000-00-00 00:00:00'),
(5, 'Help', 'help', 'plugin', 'help', 'help', 1, 1, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(6, 'New Document', 'newdocument', 'command', 'newdocument', 'newdocument', 1, 2, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(7, 'Bold', 'bold', 'command', 'bold', 'bold', 1, 3, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(8, 'Italic', 'italic', 'command', 'italic', 'italic', 1, 4, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(9, 'Underline', 'underline', 'command', 'underline', 'underline', 1, 5, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(10, 'Font Select', 'fontselect', 'command', 'fontselect', 'fontselect', 1, 6, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(11, 'Font Size Select', 'fontsizeselect', 'command', 'fontsizeselect', 'fontsizeselect', 1, 7, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(12, 'Style Select', 'styleselect', 'command', 'styleselect', 'styleselect', 1, 8, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(13, 'StrikeThrough', 'strikethrough', 'command', 'strikethrough', 'strikethrough', 1, 9, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(14, 'Justify Full', 'full', 'command', 'justifyfull', 'justifyfull', 1, 10, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(15, 'Justify Center', 'center', 'command', 'justifycenter', 'justifycenter', 1, 11, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(16, 'Justify Left', 'left', 'command', 'justifyleft', 'justifyleft', 1, 12, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(17, 'Justify Right', 'right', 'command', 'justifyright', 'justifyright', 1, 13, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(18, 'Format Select', 'formatselect', 'command', 'formatselect', 'formatselect', 1, 14, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(19, 'Paste', 'paste', 'plugin', 'cut,copy,paste', 'paste', 2, 1, 1, 1, 1, 0, '0000-00-00 00:00:00'),
(20, 'Search Replace', 'searchreplace', 'plugin', 'search,replace', 'searchreplace', 2, 2, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(21, 'Font ForeColour', 'forecolor', 'command', 'forecolor', 'forecolor', 2, 3, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(22, 'Font BackColour', 'backcolor', 'command', 'backcolor', 'backcolor', 2, 4, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(23, 'Unlink', 'unlink', 'command', 'unlink', 'unlink', 2, 5, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(24, 'Indent', 'indent', 'command', 'indent', 'indent', 2, 6, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(25, 'Outdent', 'outdent', 'command', 'outdent', 'outdent', 2, 7, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(26, 'Undo', 'undo', 'command', 'undo', 'undo', 2, 8, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(27, 'Redo', 'redo', 'command', 'redo', 'redo', 2, 9, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(28, 'HTML', 'html', 'command', 'code', 'code', 2, 10, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(29, 'Numbered List', 'numlist', 'command', 'numlist', 'numlist', 2, 11, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(30, 'Bullet List', 'bullist', 'command', 'bullist', 'bullist', 2, 12, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(31, 'Anchor', 'anchor', 'command', 'anchor', 'anchor', 2, 13, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(32, 'Image', 'image', 'command', 'image', 'image', 2, 14, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(33, 'Link', 'link', 'command', 'link', 'link', 2, 15, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(34, 'Code Cleanup', 'cleanup', 'command', 'cleanup', 'cleanup', 2, 16, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(35, 'Directionality', 'directionality', 'plugin', 'ltr,rtl', 'directionality', 3, 1, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(36, 'Emotions', 'emotions', 'plugin', 'emotions', 'emotions', 3, 2, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(37, 'Fullscreen', 'fullscreen', 'plugin', 'fullscreen', 'fullscreen', 3, 3, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(38, 'Preview', 'preview', 'plugin', 'preview', 'preview', 3, 4, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(39, 'Tables', 'table', 'plugin', 'tablecontrols', 'buttons', 3, 5, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(40, 'Print', 'print', 'plugin', 'print', 'print', 3, 6, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(41, 'Horizontal Rule', 'hr', 'command', 'hr', 'hr', 3, 7, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(42, 'Subscript', 'sub', 'command', 'sub', 'sub', 3, 8, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(43, 'Superscript', 'sup', 'command', 'sup', 'sup', 3, 9, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(44, 'Visual Aid', 'visualaid', 'command', 'visualaid', 'visualaid', 3, 10, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(45, 'Character Map', 'charmap', 'command', 'charmap', 'charmap', 3, 11, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(46, 'Remove Format', 'removeformat', 'command', 'removeformat', 'removeformat', 2, 1, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(47, 'Styles', 'style', 'plugin', 'styleprops', 'style', 4, 1, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(48, 'Non-Breaking', 'nonbreaking', 'plugin', 'nonbreaking', 'nonbreaking', 4, 2, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(49, 'Visual Characters', 'visualchars', 'plugin', 'visualchars', 'visualchars', 4, 3, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(50, 'XHTML Xtras', 'xhtmlxtras', 'plugin', 'cite,abbr,acronym,del,ins,attribs', 'xhtmlxtras', 4, 4, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(51, 'Image Manager', 'imgmanager', 'plugin', 'imgmanager', 'imgmanager', 4, 5, 1, 1, 1, 0, '0000-00-00 00:00:00'),
(52, 'Advanced Link', 'advlink', 'plugin', 'advlink', 'advlink', 4, 6, 1, 1, 1, 0, '0000-00-00 00:00:00'),
(53, 'Spell Checker', 'spellchecker', 'plugin', 'spellchecker', 'spellchecker', 4, 7, 1, 1, 1, 0, '0000-00-00 00:00:00'),
(54, 'Layers', 'layer', 'plugin', 'insertlayer,moveforward,movebackward,absolute', 'layer', 4, 8, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(55, 'Advanced Code Editor', 'advcode', 'plugin', 'advcode', 'advcode', 4, 9, 1, 0, 1, 0, '0000-00-00 00:00:00'),
(56, 'Article Breaks', 'article', 'plugin', 'readmore,pagebreak', 'article', 4, 10, 1, 0, 1, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `jos_menu`
--

CREATE TABLE IF NOT EXISTS `jos_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menutype` varchar(75) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `link` text,
  `type` varchar(50) NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `parent` int(11) unsigned NOT NULL DEFAULT '0',
  `componentid` int(11) unsigned NOT NULL DEFAULT '0',
  `sublevel` int(11) DEFAULT '0',
  `ordering` int(11) DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pollid` int(11) NOT NULL DEFAULT '0',
  `browserNav` tinyint(4) DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `utaccess` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `lft` int(11) unsigned NOT NULL DEFAULT '0',
  `rgt` int(11) unsigned NOT NULL DEFAULT '0',
  `home` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `componentid` (`componentid`,`menutype`,`published`,`access`),
  KEY `menutype` (`menutype`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=80 ;

--
-- Contenu de la table `jos_menu`
--

INSERT INTO `jos_menu` (`id`, `menutype`, `name`, `alias`, `link`, `type`, `published`, `parent`, `componentid`, `sublevel`, `ordering`, `checked_out`, `checked_out_time`, `pollid`, `browserNav`, `access`, `utaccess`, `params`, `lft`, `rgt`, `home`) VALUES
(1, 'mainmenu', 'Accueil', 'accueil', 'index.php?option=com_content&view=frontpage', 'component', 1, 0, 20, 0, 23, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'num_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=4\norderby_pri=\norderby_sec=front\nmulti_column_order=1\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=0\nshow_title=1\nlink_titles=1\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=0\nshow_create_date=0\nshow_modify_date=0\nshow_item_navigation=0\nshow_readmore=0\nshow_vote=0\nshow_icons=0\nshow_pdf_icon=0\nshow_print_icon=0\nshow_email_icon=0\nshow_hits=0\nfeed_summary=\npage_title=Portail des March�s Publics du Togo\nshow_page_title=0\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 1),
(2, 'mainmenu', 'Joomla! License', 'joomla-license', 'index.php?option=com_content&view=article&id=5', 'component', -2, 0, 20, 1, 15, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=0\nshow_title=\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(41, 'mainmenu', 'FAQ', 'faq', 'index.php?option=com_content&view=section&id=3', 'component', -2, 0, 20, 0, 16, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_page_title=1\nshow_description=0\nshow_description_image=0\nshow_categories=1\nshow_empty_categories=0\nshow_cat_num_articles=1\nshow_category_description=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\norderby=\nshow_noauth=0\nshow_title=1\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1', 0, 0, 0),
(11, 'othermenu', 'Joomla! Home', 'joomla-home', 'http://www.joomla.org', 'url', 1, 0, 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'menu_image=-1\n\n', 0, 0, 0),
(12, 'othermenu', 'Joomla! Forums', 'joomla-forums', 'http://forum.joomla.org', 'url', 1, 0, 0, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'menu_image=-1\n\n', 0, 0, 0),
(13, 'othermenu', 'Joomla! Documentation', 'joomla-documentation', 'http://docs.joomla.org', 'url', 1, 0, 0, 0, 3, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'menu_image=-1\n\n', 0, 0, 0),
(14, 'othermenu', 'Joomla! Community', 'joomla-community', 'http://community.joomla.org', 'url', 1, 0, 0, 0, 4, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'menu_image=-1\n\n', 0, 0, 0),
(15, 'othermenu', 'Joomla! Magazine', 'joomla-community-magazine', 'http://community.joomla.org/magazine.html', 'url', 1, 0, 0, 0, 5, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'menu_image=-1\n\n', 0, 0, 0),
(16, 'othermenu', 'OSM Home', 'osm-home', 'http://www.opensourcematters.org', 'url', 1, 0, 0, 0, 6, 0, '0000-00-00 00:00:00', 0, 0, 0, 6, 'menu_image=-1\n\n', 0, 0, 0),
(17, 'othermenu', 'Administrator', 'administrator', 'administrator/', 'url', 1, 0, 0, 0, 7, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'menu_image=-1\n\n', 0, 0, 0),
(18, 'topmenu', 'News', 'news', 'index.php?option=com_newsfeeds&view=newsfeed&id=1&feedid=1', 'component', -2, 0, 11, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'show_page_title=1\npage_title=News\npageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_headings=1\nshow_name=1\nshow_articles=1\nshow_link=1\nshow_other_cats=1\nshow_cat_description=1\nshow_cat_items=1\nshow_feed_image=1\nshow_feed_description=1\nshow_item_description=1\nfeed_word_count=0\n\n', 0, 0, 0),
(20, 'usermenu', 'Your Details', 'your-details', 'index.php?option=com_user&view=user&task=edit', 'component', 1, 0, 14, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 1, 3, '', 0, 0, 0),
(24, 'usermenu', 'Logout', 'logout', 'index.php?option=com_user&view=login', 'component', 1, 0, 14, 0, 4, 0, '0000-00-00 00:00:00', 0, 0, 1, 3, '', 0, 0, 0),
(38, 'keyconcepts', 'Content Layouts', 'content-layouts', 'index.php?option=com_content&view=article&id=24', 'component', 1, 0, 20, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'pageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(27, 'mainmenu', 'Joomla! Overview', 'joomla-overview', 'index.php?option=com_content&view=article&id=19', 'component', -2, 0, 20, 0, 17, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'pageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(28, 'topmenu', 'About Joomla!', 'about-joomla', 'index.php?option=com_content&view=article&id=25', 'component', -2, 0, 20, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'pageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(29, 'topmenu', 'Features', 'features', 'index.php?option=com_content&view=article&id=22', 'component', -2, 0, 20, 0, 3, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'pageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(30, 'topmenu', 'The Community', 'the-community', 'index.php?option=com_content&view=article&id=27', 'component', -2, 0, 20, 0, 4, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'pageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(34, 'mainmenu', 'What''s New in 1.5?', 'what-is-new-in-1-5', 'index.php?option=com_content&view=article&id=22', 'component', -2, 0, 20, 1, 18, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'pageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_noauth=0\nshow_title=1\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(40, 'keyconcepts', 'Extensions', 'extensions', 'index.php?option=com_content&view=article&id=26', 'component', 1, 0, 20, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'pageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(37, 'mainmenu', 'More about Joomla!', 'more-about-joomla', 'index.php?option=com_content&view=section&id=4', 'component', -2, 0, 20, 1, 19, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_description=0\nshow_description_image=0\nshow_categories=1\nshow_empty_categories=0\nshow_cat_num_articles=1\nshow_category_description=1\norderby=\norderby_sec=\nshow_feed_link=1\nshow_noauth=0\nshow_title=1\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(43, 'keyconcepts', 'Example Pages', 'example-pages', 'index.php?option=com_content&view=article&id=43', 'component', 1, 0, 20, 0, 3, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'pageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(44, 'ExamplePages', 'Section Blog', 'section-blog', 'index.php?option=com_content&view=section&layout=blog&id=3', 'component', 1, 0, 20, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_page_title=1\npage_title=Example of Section Blog layout (FAQ section)\nshow_description=0\nshow_description_image=0\nnum_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=4\nshow_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\norderby_pri=\norderby_sec=\nshow_pagination=2\nshow_pagination_results=1\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(45, 'ExamplePages', 'Section Table', 'section-table', 'index.php?option=com_content&view=section&id=3', 'component', 1, 0, 20, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_page_title=1\npage_title=Example of Table Blog layout (FAQ section)\nshow_description=0\nshow_description_image=0\nshow_categories=1\nshow_empty_categories=0\nshow_cat_num_articles=1\nshow_category_description=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\norderby=\nshow_noauth=0\nshow_title=1\nnlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(46, 'ExamplePages', 'Category Blog', 'categoryblog', 'index.php?option=com_content&view=category&layout=blog&id=31', 'component', 1, 0, 20, 0, 3, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_page_title=1\npage_title=Example of Category Blog layout (FAQs/General category)\nshow_description=0\nshow_description_image=0\nnum_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=4\nshow_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\norderby_pri=\norderby_sec=\nshow_pagination=2\nshow_pagination_results=1\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(47, 'ExamplePages', 'Category Table', 'category-table', 'index.php?option=com_content&view=category&id=32', 'component', 1, 0, 20, 0, 4, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_page_title=1\npage_title=Example of Category Table layout (FAQs/Languages category)\nshow_headings=1\nshow_date=0\ndate_format=\nfilter=1\nfilter_type=title\npageclass_sfx=\nmenu_image=-1\nsecure=0\norderby_sec=\nshow_pagination=1\nshow_pagination_limit=1\nshow_noauth=0\nshow_title=1\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(48, 'mainmenu', 'Web Links', 'web-links', 'index.php?option=com_weblinks&view=categories', 'component', -2, 0, 4, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'page_title=Weblinks\nimage=-1\nimage_align=right\npageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_comp_description=1\ncomp_description=\nshow_link_hits=1\nshow_link_description=1\nshow_other_cats=1\nshow_headings=1\nshow_page_title=1\nlink_target=0\nlink_icons=\n\n', 0, 0, 0),
(49, 'mainmenu', 'News Feeds', 'news-feeds', 'index.php?option=com_newsfeeds&view=categories', 'component', -2, 0, 11, 0, 20, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_page_title=1\npage_title=Newsfeeds\nshow_comp_description=1\ncomp_description=\nimage=-1\nimage_align=right\npageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_headings=1\nshow_name=1\nshow_articles=1\nshow_link=1\nshow_other_cats=1\nshow_cat_description=1\nshow_cat_items=1\nshow_feed_image=1\nshow_feed_description=1\nshow_item_description=1\nfeed_word_count=0\n\n', 0, 0, 0),
(50, 'mainmenu', 'The News', 'the-news', 'index.php?option=com_content&view=category&layout=blog&id=1', 'component', -2, 0, 20, 0, 21, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_page_title=1\npage_title=The News\nshow_description=0\nshow_description_image=0\nnum_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=4\nshow_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\norderby_pri=\norderby_sec=\nshow_pagination=2\nshow_pagination_results=1\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(51, 'usermenu', 'Submit an Article', 'submit-an-article', 'index.php?option=com_content&view=article&layout=form', 'component', 1, 0, 20, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 2, 0, '', 0, 0, 0),
(52, 'usermenu', 'Submit a Web Link', 'submit-a-web-link', 'index.php?option=com_weblinks&view=weblink&layout=form', 'component', 1, 0, 4, 0, 3, 0, '0000-00-00 00:00:00', 0, 0, 2, 0, '', 0, 0, 0),
(53, 'mainmenu', 'Typography', 'typography', 'index.php?option=com_content&view=article&id=46', 'component', -2, 0, 20, 0, 22, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=0\nshow_title=0\nlink_titles=0\nshow_intro=0\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=0\nshow_create_date=0\nshow_modify_date=0\nshow_item_navigation=0\nshow_readmore=0\nshow_vote=0\nshow_icons=0\nshow_pdf_icon=0\nshow_print_icon=0\nshow_email_icon=0\nshow_hits=0\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(54, 'mainmenu', 'Dummy item', 'dummy-item', '#', 'url', -2, 0, 0, 2, 14, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(55, 'mainmenu', 'Dummy item', 'dummy-item', '#', 'url', -2, 0, 0, 2, 13, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(56, 'mainmenu', 'Dummy item', 'dummy-item', '#', 'url', -2, 0, 0, 2, 12, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(57, 'mainmenu', 'Dummy item', 'dummy-item', '#', 'url', -2, 0, 0, 3, 3, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(58, 'mainmenu', 'Dummy item', 'dummy-item', '#', 'url', -2, 0, 0, 3, 4, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(59, 'mainmenu', 'Dummy item', 'dummy-item', '#', 'url', -2, 0, 0, 3, 5, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(60, 'mainmenu', 'Dummy item', 'dummy-item', '#', 'url', -2, 0, 0, 3, 6, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(61, 'mainmenu', 'Dummy item', 'dummy-item', '#', 'url', -2, 0, 0, 3, 7, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(62, 'mainmenu', 'Dummy item', 'dummy-item', '#', 'url', -2, 0, 0, 3, 8, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(63, 'mainmenu', 'Dummy item', 'dummy-item', '#', 'url', -2, 0, 0, 3, 9, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(64, 'mainmenu', 'Dummy item', 'dummy-item', '#', 'url', -2, 0, 0, 4, 10, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(65, 'mainmenu', 'Dummy item', 'dummy-item', '#', 'url', -2, 0, 0, 4, 11, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(66, 'mainmenu', 'Modules Positions', 'modules-positions', 'index.php?option=com_content&view=article&id=57', 'component', -2, 0, 20, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=0\nshow_title=0\nlink_titles=0\nshow_intro=0\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=0\nshow_create_date=0\nshow_modify_date=0\nshow_item_navigation=0\nshow_readmore=0\nshow_vote=0\nshow_icons=0\nshow_pdf_icon=0\nshow_print_icon=0\nshow_email_icon=0\nshow_hits=0\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(67, 'mainmenu', 'Plan de passation', 'plan-de-passation', 'index.php?option=com_plandepassation&view=list&Itemid=90&lang=fr', 'url', 1, 0, 0, 0, 24, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(68, 'mainmenu', 'Avis G�n�raux', 'avis-generaux', 'index.php?option=com_avisgeneraux&view=list&Itemid=91&lang=fr', 'url', 1, 0, 0, 0, 25, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(69, 'mainmenu', 'Appel d''offres', 'appel-doffres', 'index.php?option=com_appeloffres&view=listSectAct&Itemid=89&lang=fr', 'url', 1, 0, 0, 0, 26, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(70, 'mainmenu', 'Avis d''attribution', 'avis-dattribution', 'index.php?option=com_resultats&view=list&Itemid=91&lang=fr', 'url', 1, 0, 0, 0, 27, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(71, 'mainmenu', 'Contentieux', 'contentieux', 'index.php?option=com_contentieux&view=list&Itemid=93&lang=fr', 'url', 1, 0, 0, 0, 28, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(72, 'mainmenu', 'Statistiques', 'statistiques', 'index.php?option=com_statistiques&view=list&stat=1&Itemid=96&lang=fr', 'url', 1, 0, 0, 0, 29, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(73, 'Publications', 'Journal des march�s publics', 'journal-des-marches-publics', '#', 'url', 1, 75, 0, 1, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(74, 'Publications', 'Recueil des textes r�glementaires', 'recueil-des-textes-reglementaires', '#', 'url', 1, 75, 0, 1, 2, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(75, 'Publications', 'Publications', 'publications', '#', 'url', 1, 0, 0, 0, 3, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(76, 'topmenu', 'ARMP', 'armp', '#', 'url', 1, 0, 0, 0, 5, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(77, 'topmenu', 'DNCMP', 'dncmp', '#', 'url', 1, 0, 0, 0, 6, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(78, 'topmenu', 'SYGMAP', 'sygmap', '#', 'url', 1, 0, 0, 0, 7, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\n\n', 0, 0, 0),
(79, 'mainmenu', 'Contact Togo Mp', 'contact-togo-mp', 'index.php?option=com_contact&view=contact&id=1', 'component', -2, 0, 7, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_contact_list=0\nshow_category_crumb=0\ncontact_icons=\nicon_address=\nicon_email=\nicon_telephone=\nicon_mobile=\nicon_fax=\nicon_misc=\nshow_headings=\nshow_position=\nshow_email=\nshow_telephone=\nshow_mobile=\nshow_fax=\nallow_vcard=\nbanned_email=\nbanned_subject=\nbanned_text=\nvalidate_session=\ncustom_reply=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `jos_menu_types`
--

CREATE TABLE IF NOT EXISTS `jos_menu_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menutype` varchar(75) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `menutype` (`menutype`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `jos_menu_types`
--

INSERT INTO `jos_menu_types` (`id`, `menutype`, `title`, `description`) VALUES
(1, 'mainmenu', 'Main Menu', 'The main menu for the site'),
(2, 'usermenu', 'User Menu', 'A Menu for logged in Users'),
(3, 'topmenu', 'Top Menu', 'Top level navigation'),
(4, 'othermenu', 'Resources', 'Additional links'),
(5, 'ExamplePages', 'Example Pages', 'Example Pages'),
(6, 'keyconcepts', 'Key Concepts', 'This describes some critical information for new Users.'),
(7, 'Publications', 'Publications', '');

-- --------------------------------------------------------

--
-- Structure de la table `jos_messages`
--

CREATE TABLE IF NOT EXISTS `jos_messages` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `folder_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` int(11) NOT NULL DEFAULT '0',
  `priority` int(1) unsigned NOT NULL DEFAULT '0',
  `subject` text NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `useridto_state` (`user_id_to`,`state`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `jos_messages`
--


-- --------------------------------------------------------

--
-- Structure de la table `jos_messages_cfg`
--

CREATE TABLE IF NOT EXISTS `jos_messages_cfg` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) NOT NULL DEFAULT '',
  `cfg_value` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `jos_messages_cfg`
--


-- --------------------------------------------------------

--
-- Structure de la table `jos_migration_backlinks`
--

CREATE TABLE IF NOT EXISTS `jos_migration_backlinks` (
  `itemid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `url` text NOT NULL,
  `sefurl` text NOT NULL,
  `newurl` text NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `jos_migration_backlinks`
--


-- --------------------------------------------------------

--
-- Structure de la table `jos_modules`
--

CREATE TABLE IF NOT EXISTS `jos_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) DEFAULT NULL,
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  `numnews` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `iscore` tinyint(4) NOT NULL DEFAULT '0',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `control` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`,`access`),
  KEY `newsfeeds` (`module`,`published`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=81 ;

--
-- Contenu de la table `jos_modules`
--

INSERT INTO `jos_modules` (`id`, `title`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `published`, `module`, `numnews`, `access`, `showtitle`, `params`, `iscore`, `client_id`, `control`) VALUES
(1, 'Main Menu', '', 1, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_mainmenu', 0, 0, 1, 'menutype=mainmenu\nmoduleclass_sfx=_menu\n', 1, 0, ''),
(2, 'Login', '', 1, 'login', 0, '0000-00-00 00:00:00', 1, 'mod_login', 0, 0, 1, '', 1, 1, ''),
(3, 'Popular', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_popular', 0, 2, 1, '', 0, 1, ''),
(4, 'Recent added Articles', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_latest', 0, 2, 1, 'ordering=c_dsc\nuser_id=0\ncache=0\n\n', 0, 1, ''),
(5, 'Menu Stats', '', 5, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_stats', 0, 2, 1, '', 0, 1, ''),
(6, 'Unread Messages', '', 1, 'header', 0, '0000-00-00 00:00:00', 1, 'mod_unread', 0, 2, 1, '', 1, 1, ''),
(7, 'Online Users', '', 2, 'header', 0, '0000-00-00 00:00:00', 1, 'mod_online', 0, 2, 1, '', 1, 1, ''),
(8, 'Toolbar', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', 1, 'mod_toolbar', 0, 2, 1, '', 1, 1, ''),
(9, 'Quick Icons', '', 1, 'icon', 0, '0000-00-00 00:00:00', 1, 'mod_quickicon', 0, 2, 1, '', 1, 1, ''),
(10, 'Logged in Users', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_logged', 0, 2, 1, '', 0, 1, ''),
(11, 'Footer', '', 0, 'footer', 0, '0000-00-00 00:00:00', 1, 'mod_footer', 0, 0, 1, '', 1, 1, ''),
(12, 'Admin Menu', '', 1, 'menu', 0, '0000-00-00 00:00:00', 1, 'mod_menu', 0, 2, 1, '', 0, 1, ''),
(13, 'Admin SubMenu', '', 1, 'submenu', 0, '0000-00-00 00:00:00', 1, 'mod_submenu', 0, 2, 1, '', 0, 1, ''),
(14, 'User Status', '', 1, 'status', 0, '0000-00-00 00:00:00', 1, 'mod_status', 0, 2, 1, '', 0, 1, ''),
(15, 'Title', '', 1, 'title', 0, '0000-00-00 00:00:00', 1, 'mod_title', 0, 2, 1, '', 0, 1, ''),
(16, 'Polls', '', 1, 'user9', 0, '0000-00-00 00:00:00', 0, 'mod_poll', 0, 0, 1, 'id=14\nmoduleclass_sfx=\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(17, 'User Menu', '', 4, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_mainmenu', 0, 1, 1, 'menutype=usermenu\nmoduleclass_sfx=_menu\ncache=1', 1, 0, ''),
(18, 'Login Form', '', 7, 'userarea', 62, '2009-02-28 13:40:33', 1, 'mod_login', 0, 0, 1, 'cache=0\nmoduleclass_sfx=\npretext=\nposttext=\nlogin=\nlogout=\ngreeting=1\nname=0\nusesecure=0\n\n', 1, 0, ''),
(19, 'Latest News', '', 1, 'user10', 0, '0000-00-00 00:00:00', 0, 'mod_latestnews', 0, 0, 1, 'count=5\nordering=c_dsc\nuser_id=0\nshow_front=1\nsecid=\ncatid=\nmoduleclass_sfx=\ncache=1\ncache_time=900\n\n', 1, 0, ''),
(20, 'Statistics', '', 6, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_stats', 0, 0, 1, 'serverinfo=1\nsiteinfo=1\ncounter=1\nincrease=0\nmoduleclass_sfx=', 0, 0, ''),
(21, 'Who''s Online', '', 5, 'column', 0, '0000-00-00 00:00:00', 0, 'mod_whosonline', 0, 0, 1, 'cache=0\nshowmode=0\nmoduleclass_sfx=\n\n', 0, 0, ''),
(22, 'Popular News', '', 1, 'user7', 0, '0000-00-00 00:00:00', 0, 'mod_mostread', 0, 0, 1, 'moduleclass_sfx=\nshow_front=1\ncount=5\ncatid=\nsecid=\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(23, 'Archive', '', 8, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_archive', 0, 0, 1, 'cache=1', 1, 0, ''),
(24, 'Sections News', '', 1, 'user8', 0, '0000-00-00 00:00:00', 0, 'mod_sections', 0, 0, 1, 'count=5\nmoduleclass_sfx=\ncache=1\ncache_time=900\n\n', 1, 0, ''),
(25, 'Newsflash', '', 4, 'top', 0, '0000-00-00 00:00:00', 0, 'mod_newsflash', 0, 0, 1, 'catid=3\nlayout=default\nimage=0\nlink_titles=\nshowLastSeparator=1\nreadmore=0\nitem_title=0\nitems=\nmoduleclass_sfx=_color\ncache=0\ncache_time=900\n\n', 0, 0, ''),
(26, 'Related Items', '', 2, 'user8', 0, '0000-00-00 00:00:00', 0, 'mod_related_items', 0, 0, 1, 'showDate=0\nmoduleclass_sfx=\ncache=0\ncache_time=900\n\n', 0, 0, ''),
(27, 'Search', '', 0, 'search', 62, '2012-12-11 17:28:05', 1, 'mod_search', 0, 0, 0, 'moduleclass_sfx=\nwidth=20\ntext=Rechercher un avis\nbutton=1\nbutton_pos=right\nimagebutton=\nbutton_text=\nset_itemid=\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(28, 'Random Image', '', 3, 'column', 0, '0000-00-00 00:00:00', 0, 'mod_random_image', 0, 0, 1, 'type=jpg\nfolder=\nlink=\nwidth=\nheight=\nmoduleclass_sfx=\ncache=0\ncache_time=900\n\n', 0, 0, ''),
(29, 'Top Menu', '', 1, 'user3', 0, '0000-00-00 00:00:00', 0, 'mod_mainmenu', 0, 0, 1, 'menutype=topmenu\nmenu_style=list_flat\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\nshow_whitespace=0\ncache=1\ntag_id=\nclass_sfx=-nav\nmoduleclass_sfx=\nmaxdepth=10\nmenu_images=0\nmenu_images_align=0\nmenu_images_link=0\nexpand_menu=0\nactivate_parent=0\nfull_active_id=0\nindent_image=0\nindent_image1=-1\nindent_image2=-1\nindent_image3=-1\nindent_image4=-1\nindent_image5=-1\nindent_image6=-1\nspacer=\nend_spacer=\n\n', 1, 0, ''),
(30, 'Banners', '', 1, 'footer', 0, '0000-00-00 00:00:00', 1, 'mod_banners', 0, 0, 0, 'target=1\ncount=1\ncid=1\ncatid=33\ntag_search=0\nordering=random\nheader_text=\nfooter_text=\nmoduleclass_sfx=\ncache=1\ncache_time=15\n\n', 1, 0, ''),
(31, 'Resources', '', 2, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_mainmenu', 0, 0, 1, 'menutype=othermenu\nmenu_style=list\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\nshow_whitespace=0\ncache=1\ntag_id=\nclass_sfx=\nmoduleclass_sfx=_menu\nmaxdepth=10\nmenu_images=0\nmenu_images_align=0\nexpand_menu=0\nactivate_parent=0\nfull_active_id=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\n\n', 0, 0, ''),
(32, 'Wrapper', '', 9, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_wrapper', 0, 0, 1, '', 0, 0, ''),
(33, 'Footer', '', 2, 'footer', 62, '2012-12-10 18:27:50', 1, 'mod_footer', 0, 0, 0, 'cache=1\n\n', 1, 0, ''),
(34, 'Feed Display', '', 2, 'user9', 0, '0000-00-00 00:00:00', 0, 'mod_feed', 0, 0, 1, 'moduleclass_sfx=\nrssurl=\nrssrtl=0\nrsstitle=1\nrssdesc=1\nrssimage=1\nrssitems=3\nrssitemdesc=1\nword_count=0\ncache=0\ncache_time=15\n\n', 1, 0, ''),
(35, 'Breadcrumbs', '', 1, 'breadcrumbs', 0, '0000-00-00 00:00:00', 0, 'mod_breadcrumbs', 0, 0, 1, 'showHome=1\nhomeText=Home\nshowLast=1\nseparator=\nmoduleclass_sfx=\ncache=0\n\n', 1, 0, ''),
(36, 'Syndication', '', 1, 'syndicate', 62, '2012-11-27 10:01:15', 1, 'mod_syndicate', 0, 0, 0, '', 1, 0, ''),
(38, 'Advertisement', '', 7, 'column', 0, '0000-00-00 00:00:00', 0, 'mod_banners', 0, 0, 1, 'target=1\ncount=4\ncid=0\ncatid=14\ntag_search=0\nordering=0\nheader_text=Featured Links:\nfooter_text=<a href="http://www.joomla.org">Ads by Joomla!</a>\nmoduleclass_sfx=_text\ncache=0\ncache_time=900\n\n', 0, 0, ''),
(39, 'Example Pages', '', 5, 'left', 62, '2012-11-27 10:00:40', 1, 'mod_mainmenu', 0, 0, 1, 'cache=1\nclass_sfx=\nmoduleclass_sfx=_menu\nmenutype=ExamplePages\nmenu_style=list_flat\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nfull_active_id=0\nmenu_images=0\nmenu_images_align=0\nexpand_menu=0\nactivate_parent=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\nwindow_open=\n\n', 0, 0, ''),
(40, 'Key Concepts', '', 3, 'left', 62, '2012-11-27 10:00:28', 1, 'mod_mainmenu', 0, 0, 1, 'cache=1\nclass_sfx=\nmoduleclass_sfx=_menu\nmenutype=keyconcepts\nmenu_style=list\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nfull_active_id=0\nmenu_images=0\nmenu_images_align=0\nexpand_menu=0\nactivate_parent=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\nwindow_open=\n\n', 0, 0, ''),
(41, 'Welcome to Joomla!', '<div style="padding: 5px">  <p>   Congratulations on choosing Joomla! as your content management system. To   help you get started, check out these excellent resources for securing your   server and pointers to documentation and other helpful resources. </p> <p>   <strong>Security</strong><br /> </p> <p>   On the Internet, security is always a concern. For that reason, you are   encouraged to subscribe to the   <a href="http://feedburner.google.com/fb/a/mailverify?uri=JoomlaSecurityNews" target="_blank">Joomla!   Security Announcements</a> for the latest information on new Joomla! releases,   emailed to you automatically. </p> <p>   If this is one of your first Web sites, security considerations may   seem complicated and intimidating. There are three simple steps that go a long   way towards securing a Web site: (1) regular backups; (2) prompt updates to the   <a href="http://www.joomla.org/download.html" target="_blank">latest Joomla! release;</a> and (3) a <a href="http://docs.joomla.org/Security_Checklist_2_-_Hosting_and_Server_Setup" target="_blank" title="good Web host">good Web host</a>. There are many other important security considerations that you can learn about by reading the <a href="http://docs.joomla.org/Category:Security_Checklist" target="_blank" title="Joomla! Security Checklist">Joomla! Security Checklist</a>. </p> <p>If you believe your Web site was attacked, or you think you have discovered a security issue in Joomla!, please do not post it in the Joomla! forums. Publishing this information could put other Web sites at risk. Instead, report possible security vulnerabilities to the <a href="http://developer.joomla.org/security/contact-the-team.html" target="_blank" title="Joomla! Security Task Force">Joomla! Security Task Force</a>.</p><p><strong>Learning Joomla!</strong> </p> <p>   A good place to start learning Joomla! is the   "<a href="http://docs.joomla.org/beginners" target="_blank">Absolute Beginner''s   Guide to Joomla!.</a>" There, you will find a Quick Start to Joomla!   <a href="http://help.joomla.org/ghop/feb2008/task048/joomla_15_quickstart.pdf" target="_blank">guide</a>   and <a href="http://help.joomla.org/ghop/feb2008/task167/index.html" target="_blank">video</a>,   amongst many other tutorials. The   <a href="http://community.joomla.org/magazine/view-all-issues.html" target="_blank">Joomla!   Community Magazine</a> also has   <a href="http://community.joomla.org/magazine/article/522-introductory-learning-joomla-using-sample-data.html" target="_blank">articles   for new learners</a> and experienced users, alike. A great place to look for   answers is the   <a href="http://docs.joomla.org/Category:FAQ" target="_blank">Frequently Asked   Questions (FAQ)</a>. If you are stuck on a particular screen in the   Administrator (which is where you are now), try clicking the Help toolbar   button to get assistance specific to that page. </p> <p>   If you still have questions, please feel free to use the   <a href="http://forum.joomla.org/" target="_blank">Joomla! Forums.</a> The forums   are an incredibly valuable resource for all levels of Joomla! users. Before   you post a question, though, use the forum search (located at the top of each   forum page) to see if the question has been asked and answered. </p> <p>   <strong>Getting Involved</strong> </p> <p>   <a name="twjs" title="twjs"></a> If you want to help make Joomla! better, consider getting   involved. There are   <a href="http://www.joomla.org/about-joomla/contribute-to-joomla.html" target="_blank">many ways   you can make a positive difference.</a> Have fun using Joomla!.</p></div>', 0, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 2, 1, 'moduleclass_sfx=\n\n', 1, 1, ''),
(42, 'Joomla! Security Newsfeed', '', 6, 'cpanel', 62, '2008-10-25 20:15:17', 1, 'mod_feed', 0, 0, 1, 'cache=1\ncache_time=15\nmoduleclass_sfx=\nrssurl=http://feeds.joomla.org/JoomlaSecurityNews\nrssrtl=0\nrsstitle=1\nrssdesc=0\nrssimage=1\nrssitems=1\nrssitemdesc=1\nword_count=0\n\n', 0, 1, ''),
(43, 'Gavick News Image VI', '', 0, 'header1', 0, '0000-00-00 00:00:00', 0, 'mod_gk_news_image_6', 0, 0, 0, 'moduleclass_sfx=\nmodule_id=news_image_6_1\nmodule_bg=\nstart_h=120\nshow_text_block=0\ntext_block_opacity=0.45\ntext_block_background=1\ntext_block_bgcolor=#000000\nclean_xhtml=1\nreadmore_button=1\nreadmore_text=Read more\ntitle_link=1\ntitle_limit=30\ntext_limit=60\ntabsbar_width=400\ntabs_amount=10\ntabs_per_page=4\ntabs_margin=2\ntabs_position=right\ntabsbar_margin=12\nanimation_slide_speed=1000\nanimation_interval=5000\nautoanimation=1\nanimation_slide_type=0\nanimation_text_type=0\npreloading=1\nclean_code=1\nuseMoo=2\nuseScript=2\ncompress_js=0\n\n', 0, 0, ''),
(44, 'Banner 1', '<img src="images/demo/musictube_banner1.png" border="0" alt="Banner 1" />', 1, 'header2', 0, '0000-00-00 00:00:00', 0, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(45, 'Banner 2', '<img src="images/demo/musictube_banner2.png" border="0" alt="Banner 2" />', 2, 'header2', 0, '0000-00-00 00:00:00', 0, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(66, 'Gavick News Image I', '', 10, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_gk_news_image_1', 0, 0, 1, 'module_id=newsimage1\nwrapper_border_color=#000\nshow_text_block=1\ntext_block_width=200\ntext_block_margin=50\ntext_block_opacity=0.45\ntext_block_background=1\ntext_block_bgcolor=#000000\ntext_block_position=1\ntext_block_height=100\nclean_xhtml=1\nreadmore_button=1\noutter_interface_width=24\noutter_readmore=1\nreadmore_text=Read more\ntitle_link=1\nprev_button=1\nnext_button=1\nplay_button=1\ninterface_x=20\ninterface_y=20\nslidelinks=1\nthumbnail_bar=1\nthumbnail_margin=4\nthumbnail_border=1\nthumbnail_border_color_inactive=#000000\nthumbnail_border_color=#FFFFFF\nthumbnail_bar_position=1\nshow_ticks=1\ntick_x=20\ntick_y=20\npreloading=1\nanimation_slide_speed=1000\nanimation_interval=5000\nautoanimation=1\n', 0, 0, ''),
(47, 'Banner top', '<img src="images/demo/musictube_banner_top.png" border="0" alt="Banner top" />', 1, 'banner1', 0, '0000-00-00 00:00:00', 0, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(48, 'TabArts I', '', 0, 'column', 0, '0000-00-00 00:00:00', 0, 'mod_tabarts_gk2', 0, 0, 0, 'moduleclass_sfx=_color nopad\nmodule_id=tabarts1\nmoduleHeight=329px\nmoduleWidth=0\ntabsGroupID=2\nnews_content_header_pos=1\nnews_content_image_pos=3\nnews_content_text_pos=4\nnews_content_info_pos=0\nnews_content_readmore_pos=1\nnews_readmore_text=Read more...\nnews_header_link=1\nnews_image_link=1\nnews_text_link=0\nnews_author=1\nnews_cats=1\nnews_date=1\nnews_header_order=2\nnews_image_order=1\nnews_text_order=3\nnews_info_order=4\nnews_limit_type=1\nnews_limit=250\nclean_xhtml=1\nnews_content_timezone=0\nimg_height=123px\nimg_width=262px\ndate_format=D, d M Y\nusername=0\nactivator=click\nanimation=0\nanimationType=2\nanimationSpeed=500\nanimationInterval=5000\nanimationFun=Fx.Transitions.Circ.easeOut\nbuttons=0\nstyleCSS=style1\nstyleType=0\nstyleFile=\nstyleSuffix=\nclean_code=1\nuseMoo=2\nuseScript=2\ncompress_js=1\n\n', 0, 0, ''),
(49, 'TabArts II', '', 6, 'column', 0, '0000-00-00 00:00:00', 0, 'mod_tabarts_gk2', 0, 0, 1, 'moduleclass_sfx= nopad\nmodule_id=tabarts2\nmoduleHeight=315px\nmoduleWidth=0\ntabsGroupID=1\nnews_content_header_pos=1\nnews_content_image_pos=3\nnews_content_text_pos=1\nnews_content_info_pos=0\nnews_content_readmore_pos=1\nnews_readmore_text=Read more...\nnews_header_link=1\nnews_image_link=1\nnews_text_link=0\nnews_author=1\nnews_cats=1\nnews_date=1\nnews_header_order=2\nnews_image_order=1\nnews_text_order=3\nnews_info_order=4\nnews_limit_type=0\nnews_limit=40\nclean_xhtml=1\nnews_content_timezone=0\nimg_height=123px\nimg_width=262px\ndate_format=D, d M Y\nusername=0\nactivator=click\nanimation=0\nanimationType=1\nanimationSpeed=500\nanimationInterval=5000\nanimationFun=Fx.Transitions.Circ.easeOut\nbuttons=0\nstyleCSS=style1\nstyleType=0\nstyleFile=\nstyleSuffix=\nclean_code=1\nuseMoo=2\nuseScript=2\ncompress_js=1\n\n', 0, 0, ''),
(50, 'Popular News', '', 1, 'advert1', 0, '0000-00-00 00:00:00', 0, 'mod_news_show_gk3', 0, 0, 1, 'moduleclass_sfx=\nmodule_unique_id=newsshow1\nwidth=0\ntd_padding=0\nnews_type=0\nsection=0\ncategory=0\nsections=\ncategories=\nids=51,54,49,48\nnews_amount=4\nnews_sort_value=created\nnews_sort_order=DESC\nnews_frontpage=1\nnews_column=1\nnews_rows=4\nunauthorized=0\nonly_frontpage=0\nshow_list=0\nrounded=0\nlist_style=0\nnews_content_header_pos=1\nnews_content_image_pos=1\nnews_content_text_pos=4\nnews_content_info_pos=1\nnews_content_readmore_pos=0\nnews_readmore_text=READMORE\nnews_header_link=1\nnews_image_link=1\nnews_text_link=0\nnews_author=0\nnews_cats=0\nnews_date=0\nnews_more_in=0\nnews_header_order=2\nnews_image_order=1\nnews_text_order=3\nnews_info_order=4\nhead_nofollow=1\nimage_nofollow=1\ntext_nofollow=1\ninfo_nofollow=1\nlist_nofollow=1\nreadmore_nofollow=1\nhead_target=1\nimage_target=1\ntext_target=1\ninfo_target=1\nlist_target=1\nreadmore_target=1\nnews_limit_type=1\nnews_limit=115\ntitle_limit=50\nclean_xhtml=1\nnews_content_timezone=0\nplugin_support=1\nonly_plugin=1\nimg_height=0\nimg_width=0\nimg_margin=3px 6px 6px 0px\ndate_format=D, d M Y\nusername=0\nstartposition=0\npanel=0\nparse_plugins=0\nmootools_version=1_11\nuseMoo=2\nuseScript=2\ncompress_js=1\n\n', 0, 0, ''),
(51, 'Breaking news', '', 1, 'advert3', 0, '0000-00-00 00:00:00', 0, 'mod_news_show_gk3', 0, 0, 1, 'moduleclass_sfx=\nmodule_unique_id=newsshow9\nwidth=0\ntd_padding=0\nnews_type=0\nsection=5\ncategory=34\nsections=\ncategories=\nids=\nnews_amount=6\nnews_sort_value=created\nnews_sort_order=DESC\nnews_frontpage=1\nnews_column=1\nnews_rows=6\nunauthorized=0\nonly_frontpage=0\nshow_list=0\nrounded=0\nlist_style=0\nnews_content_header_pos=1\nnews_content_image_pos=0\nnews_content_text_pos=1\nnews_content_info_pos=1\nnews_content_readmore_pos=0\nnews_readmore_text=READMORE\nnews_header_link=1\nnews_image_link=1\nnews_text_link=0\nnews_author=0\nnews_cats=0\nnews_date=0\nnews_more_in=0\nnews_header_order=2\nnews_image_order=1\nnews_text_order=3\nnews_info_order=4\nhead_nofollow=1\nimage_nofollow=1\ntext_nofollow=1\ninfo_nofollow=1\nlist_nofollow=1\nreadmore_nofollow=1\nhead_target=1\nimage_target=1\ntext_target=1\ninfo_target=1\nlist_target=1\nreadmore_target=1\nnews_limit_type=1\nnews_limit=50\ntitle_limit=40\nclean_xhtml=1\nnews_content_timezone=0\nplugin_support=1\nonly_plugin=1\nimg_height=0\nimg_width=0\nimg_margin=3px 6px 6px 6px\ndate_format=D, d M Y\nusername=0\nstartposition=0\npanel=0\nparse_plugins=0\nmootools_version=1_11\nuseMoo=2\nuseScript=2\ncompress_js=1\n\n', 0, 0, ''),
(59, 'Breaking news', '', 2, 'advert3', 0, '0000-00-00 00:00:00', 0, 'mod_news_pro_gk1', 0, 0, 1, 'moduleclass_sfx= nopad\nmodule_unique_id=newspro4\nwidth_module=1\nwidth_links=313\ntd_padding=5px\nsection=5\ncategory=0\nsections=\ncategories=\nids=\nmode=standard\nnews_full_pages=1\nnews_short_pages=1\nnews_column=1\nnews_rows=1\nlinks_amount=5\nnews_sort_value=created\nnews_sort_order=DESC\nnews_frontpage=1\nunauthorized=0\nonly_frontpage=0\nstartposition=0\nshow_list=1\nlist_position=right\nnews_readmore_text=Read more...\nnews_header_link=1\nnews_image_link=0\nnews_text_link=0\nnews_author=1\nnews_cats=1\nnews_date=1\nnews_more_in=0\ndate_format=D, d M Y\nusername=0\nnews_content_header_pos=1\nnews_content_image_pos=0\nnews_content_text_pos=1\nnews_content_info_pos=0\nnews_content_readmore_pos=2\nnews_header_order=1\nnews_image_order=2\nnews_text_order=3\nnews_info_order=4\nhead_nofollow=1\nimage_nofollow=1\ntext_nofollow=1\ninfo_nofollow=1\nlist_nofollow=1\nreadmore_nofollow=1\nhead_target=1\nimage_target=1\ntext_target=1\ninfo_target=1\nlist_target=1\nreadmore_target=1\nnews_limit_type=0\nnews_limit=30\ntitle_limit=30\nlist_title_limit=40\nlist_text_limit=50\nclean_xhtml=1\nnews_content_timezone=0\nplugin_support=0\nonly_plugin=0\nimg_height=142px\nimg_width=300px\nimg_margin=3px\nparse_plugins=0\nuseMoo=2\nuseScript=2\ncompress_js=1\n\n', 0, 0, ''),
(52, 'Top Stars News', '', 1, 'advert2', 0, '0000-00-00 00:00:00', 0, 'mod_news_show_gk3', 0, 0, 1, 'moduleclass_sfx=\nmodule_unique_id=newsshow2\nwidth=0\ntd_padding=0\nnews_type=0\nsection=0\ncategory=0\nsections=\ncategories=\nids=47,50,53,52\nnews_amount=4\nnews_sort_value=created\nnews_sort_order=DESC\nnews_frontpage=1\nnews_column=1\nnews_rows=4\nunauthorized=0\nonly_frontpage=0\nshow_list=0\nrounded=0\nlist_style=0\nnews_content_header_pos=1\nnews_content_image_pos=1\nnews_content_text_pos=1\nnews_content_info_pos=1\nnews_content_readmore_pos=0\nnews_readmore_text=READMORE\nnews_header_link=1\nnews_image_link=1\nnews_text_link=0\nnews_author=0\nnews_cats=0\nnews_date=0\nnews_more_in=0\nnews_header_order=2\nnews_image_order=1\nnews_text_order=3\nnews_info_order=4\nhead_nofollow=1\nimage_nofollow=1\ntext_nofollow=1\ninfo_nofollow=1\nlist_nofollow=1\nreadmore_nofollow=1\nhead_target=1\nimage_target=1\ntext_target=1\ninfo_target=1\nlist_target=1\nreadmore_target=1\nnews_limit_type=1\nnews_limit=70\ntitle_limit=30\nclean_xhtml=1\nnews_content_timezone=0\nplugin_support=1\nonly_plugin=1\nimg_height=0\nimg_width=0\nimg_margin=3px 6px 6px 6px\ndate_format=D, d M Y\nusername=0\nstartposition=0\npanel=0\nparse_plugins=0\nmootools_version=1_11\nuseMoo=2\nuseScript=2\ncompress_js=1\n\n', 0, 0, ''),
(53, 'Special Video Clip', '', 2, 'column', 0, '0000-00-00 00:00:00', 0, 'mod_gk_embed_media', 0, 0, 1, 'moduleclass_sfx=_color\nmodule_unique_id=gk_embed_media\nurl=\nwidth=0\nheight=0\ntype=other\nyt_related=1\nyt_border=1\nyt_style=0\nyt_color1=0x000000\nyt_color2=0x000000\ngv_fs=1\nveoh_fs=1\nveoh_bg=#000000\nveoh_autoplay=1\ndm_size=1\ndm_fs=1\ndm_bg=000000\ndm_glow=000000\ndm_fg=FFFFFF\ndm_special=000000\ndm_autoplay=1\ndm_related=1\nrev_fs=1\nvid_fs=1\nvid_autoplay=1\nvid_player=1\nother=<object width="295" height="176"><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=2396020&server=vimeo.com&show_title=0&show_byline=0&show_portrait=0&color=41BCEA&fullscreen=1" /><embed src="http://vimeo.com/moogaloop.swf?clip_id=2396020&server=vimeo.com&show_title=0&show_byline=0&show_portrait=0&color=41BCEA&fullscreen=1" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" wmode="transparent" width="295" height="176"></embed></object>\n\n', 0, 0, ''),
(55, 'Popular News', '', 1, 'user4', 0, '0000-00-00 00:00:00', 0, 'mod_news_show_gk3', 0, 0, 1, 'moduleclass_sfx=\nmodule_unique_id=newsshow4\nwidth=0\ntd_padding=0\nnews_type=0\nsection=0\ncategory=0\nsections=\ncategories=\nids=47,50,53,52\nnews_amount=4\nnews_sort_value=ordering\nnews_sort_order=DESC\nnews_frontpage=1\nnews_column=1\nnews_rows=4\nunauthorized=0\nonly_frontpage=0\nshow_list=0\nrounded=0\nlist_style=0\nnews_content_header_pos=1\nnews_content_image_pos=1\nnews_content_text_pos=1\nnews_content_info_pos=1\nnews_content_readmore_pos=0\nnews_readmore_text=READMORE\nnews_header_link=1\nnews_image_link=1\nnews_text_link=0\nnews_author=0\nnews_cats=0\nnews_date=0\nnews_more_in=0\nnews_header_order=2\nnews_image_order=1\nnews_text_order=3\nnews_info_order=4\nhead_nofollow=1\nimage_nofollow=1\ntext_nofollow=1\ninfo_nofollow=1\nlist_nofollow=1\nreadmore_nofollow=1\nhead_target=1\nimage_target=1\ntext_target=1\ninfo_target=1\nlist_target=1\nreadmore_target=1\nnews_limit_type=1\nnews_limit=70\ntitle_limit=30\nclean_xhtml=1\nnews_content_timezone=0\nplugin_support=1\nonly_plugin=1\nimg_height=0\nimg_width=0\nimg_margin=3px 6px 6px 6px\ndate_format=D, d M Y\nusername=0\nstartposition=0\npanel=0\nparse_plugins=0\nmootools_version=1_11\nuseMoo=2\nuseScript=2\ncompress_js=1\n\n', 0, 0, ''),
(65, 'Top Stars News', '', 4, 'column', 0, '0000-00-00 00:00:00', 0, 'mod_news_show_gk3', 0, 0, 1, 'moduleclass_sfx=\nmodule_unique_id=newsshow5\nwidth=0\ntd_padding=0\nnews_type=0\nsection=0\ncategory=0\nsections=\ncategories=\nids=52,53,50,47\nnews_amount=3\nnews_sort_value=ordering\nnews_sort_order=DESC\nnews_frontpage=1\nnews_column=1\nnews_rows=4\nunauthorized=0\nonly_frontpage=0\nshow_list=0\nrounded=0\nlist_style=0\nnews_content_header_pos=1\nnews_content_image_pos=1\nnews_content_text_pos=1\nnews_content_info_pos=1\nnews_content_readmore_pos=0\nnews_readmore_text=READMORE\nnews_header_link=1\nnews_image_link=1\nnews_text_link=0\nnews_author=0\nnews_cats=0\nnews_date=0\nnews_more_in=0\nnews_header_order=2\nnews_image_order=1\nnews_text_order=3\nnews_info_order=4\nhead_nofollow=1\nimage_nofollow=1\ntext_nofollow=1\ninfo_nofollow=1\nlist_nofollow=1\nreadmore_nofollow=1\nhead_target=1\nimage_target=1\ntext_target=1\ninfo_target=1\nlist_target=1\nreadmore_target=1\nnews_limit_type=1\nnews_limit=70\ntitle_limit=30\nclean_xhtml=1\nnews_content_timezone=0\nplugin_support=1\nonly_plugin=1\nimg_height=0\nimg_width=0\nimg_margin=3px 6px 6px 6px\ndate_format=D, d M Y\nusername=0\nstartposition=0\npanel=0\nparse_plugins=0\nmootools_version=1_11\nuseMoo=2\nuseScript=2\ncompress_js=1\n\n', 0, 0, ''),
(58, 'SpotLight News', '', 2, 'user4', 0, '0000-00-00 00:00:00', 0, 'mod_news_pro_gk1', 0, 0, 1, 'moduleclass_sfx=_color nopad\nmodule_unique_id=newspro2\nwidth_module=620\nwidth_links=1\ntd_padding=10px\nsection=5\ncategory=0\nsections=\ncategories=\nids=\nmode=standard\nnews_full_pages=2\nnews_short_pages=2\nnews_column=2\nnews_rows=2\nlinks_amount=4\nnews_sort_value=hits\nnews_sort_order=DESC\nnews_frontpage=1\nunauthorized=0\nonly_frontpage=0\nstartposition=0\nshow_list=0\nlist_position=left\nnews_readmore_text=Read more...\nnews_header_link=1\nnews_image_link=1\nnews_text_link=0\nnews_author=1\nnews_cats=1\nnews_date=1\nnews_more_in=1\ndate_format=D, d M Y\nusername=0\nnews_content_header_pos=1\nnews_content_image_pos=1\nnews_content_text_pos=4\nnews_content_info_pos=0\nnews_content_readmore_pos=4\nnews_header_order=1\nnews_image_order=2\nnews_text_order=3\nnews_info_order=4\nhead_nofollow=1\nimage_nofollow=1\ntext_nofollow=1\ninfo_nofollow=1\nlist_nofollow=1\nreadmore_nofollow=1\nhead_target=1\nimage_target=1\ntext_target=1\ninfo_target=1\nlist_target=1\nreadmore_target=1\nnews_limit_type=1\nnews_limit=120\ntitle_limit=30\nlist_title_limit=15\nlist_text_limit=30\nclean_xhtml=1\nnews_content_timezone=0\nplugin_support=1\nonly_plugin=0\nimg_height=53px\nimg_width=113px\nimg_margin=8px\nparse_plugins=0\nuseMoo=2\nuseScript=2\ncompress_js=0\n\n', 0, 0, ''),
(56, 'Popular News', '', 1, 'bottom', 62, '2009-02-28 12:16:00', 0, 'mod_news_show_gk3', 0, 0, 1, 'moduleclass_sfx=_color\nmodule_unique_id=newsshow5\nwidth=0\ntd_padding=0\nnews_type=0\nsection=0\ncategory=0\nsections=\ncategories=\nids=47,50,53,52\nnews_amount=4\nnews_sort_value=ordering\nnews_sort_order=ASC\nnews_frontpage=1\nnews_column=1\nnews_rows=4\nunauthorized=0\nonly_frontpage=0\nshow_list=0\nrounded=0\nlist_style=0\nnews_content_header_pos=1\nnews_content_image_pos=2\nnews_content_text_pos=1\nnews_content_info_pos=1\nnews_content_readmore_pos=0\nnews_readmore_text=READMORE\nnews_header_link=1\nnews_image_link=1\nnews_text_link=0\nnews_author=0\nnews_cats=0\nnews_date=0\nnews_more_in=0\nnews_header_order=2\nnews_image_order=1\nnews_text_order=3\nnews_info_order=4\nhead_nofollow=1\nimage_nofollow=1\ntext_nofollow=1\ninfo_nofollow=1\nlist_nofollow=1\nreadmore_nofollow=1\nhead_target=1\nimage_target=1\ntext_target=1\ninfo_target=1\nlist_target=1\nreadmore_target=1\nnews_limit_type=1\nnews_limit=200\ntitle_limit=30\nclean_xhtml=1\nnews_content_timezone=0\nplugin_support=1\nonly_plugin=1\nimg_height=0\nimg_width=0\nimg_margin=3px 6px 6px 6px\ndate_format=D, d M Y\nusername=0\nstartposition=0\npanel=0\nparse_plugins=0\nmootools_version=1_11\nuseMoo=2\nuseScript=2\ncompress_js=1\n\n', 0, 0, ''),
(57, 'News Pro GK1', '', 1, 'top', 0, '0000-00-00 00:00:00', 0, 'mod_news_pro_gk1', 0, 0, 1, 'moduleclass_sfx= nopad\nmodule_unique_id=newspro1\nwidth_module=305\nwidth_links=315\ntd_padding=0 10px 10px 10px\nsection=5\ncategory=0\nsections=\ncategories=\nids=\nmode=standard\nnews_full_pages=3\nnews_short_pages=3\nnews_column=1\nnews_rows=1\nlinks_amount=5\nnews_sort_value=created\nnews_sort_order=DESC\nnews_frontpage=1\nunauthorized=0\nonly_frontpage=0\nstartposition=0\nshow_list=1\nlist_position=right\nnews_readmore_text=Read more...\nnews_header_link=1\nnews_image_link=1\nnews_text_link=0\nnews_author=1\nnews_cats=0\nnews_date=1\nnews_more_in=0\ndate_format=D, d M Y\nusername=0\nnews_content_header_pos=1\nnews_content_image_pos=3\nnews_content_text_pos=1\nnews_content_info_pos=0\nnews_content_readmore_pos=2\nnews_header_order=1\nnews_image_order=2\nnews_text_order=3\nnews_info_order=4\nhead_nofollow=1\nimage_nofollow=1\ntext_nofollow=1\ninfo_nofollow=1\nlist_nofollow=1\nreadmore_nofollow=1\nhead_target=1\nimage_target=1\ntext_target=1\ninfo_target=1\nlist_target=1\nreadmore_target=1\nnews_limit_type=0\nnews_limit=30\ntitle_limit=40\nlist_title_limit=30\nlist_text_limit=40\nclean_xhtml=1\nnews_content_timezone=0\nplugin_support=0\nonly_plugin=0\nimg_height=132px\nimg_width=280px\nimg_margin=3px\nparse_plugins=0\nuseMoo=2\nuseScript=2\ncompress_js=1\n\n', 0, 0, ''),
(60, 'Copy of SpotLight News', '', 2, 'bottom', 0, '0000-00-00 00:00:00', 0, 'mod_news_pro_gk1', 0, 0, 0, 'moduleclass_sfx= nopad\nmodule_unique_id=newspro3\nwidth_module=420\nwidth_links=200\ntd_padding=10px\nsection=5\ncategory=0\nsections=\ncategories=\nids=\nmode=standard\nnews_full_pages=2\nnews_short_pages=2\nnews_column=1\nnews_rows=1\nlinks_amount=3\nnews_sort_value=created\nnews_sort_order=DESC\nnews_frontpage=1\nunauthorized=0\nonly_frontpage=0\nstartposition=2\nshow_list=1\nlist_position=left\nnews_readmore_text=Read more...\nnews_header_link=1\nnews_image_link=1\nnews_text_link=0\nnews_author=1\nnews_cats=1\nnews_date=1\nnews_more_in=1\ndate_format=D, d M Y\nusername=0\nnews_content_header_pos=1\nnews_content_image_pos=1\nnews_content_text_pos=4\nnews_content_info_pos=0\nnews_content_readmore_pos=2\nnews_header_order=1\nnews_image_order=2\nnews_text_order=3\nnews_info_order=4\nhead_nofollow=1\nimage_nofollow=1\ntext_nofollow=1\ninfo_nofollow=1\nlist_nofollow=1\nreadmore_nofollow=1\nhead_target=1\nimage_target=1\ntext_target=1\ninfo_target=1\nlist_target=1\nreadmore_target=1\nnews_limit_type=0\nnews_limit=80\ntitle_limit=40\nlist_title_limit=20\nlist_text_limit=30\nclean_xhtml=1\nnews_content_timezone=0\nplugin_support=0\nonly_plugin=0\nimg_height=53px\nimg_width=113px\nimg_margin=3px\nparse_plugins=0\nuseMoo=2\nuseScript=2\ncompress_js=1\n\n', 0, 0, ''),
(64, 'Sections News', '', 3, 'user9', 0, '0000-00-00 00:00:00', 0, 'mod_sections', 0, 0, 1, 'count=5\nmoduleclass_sfx=\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(62, 'Sections News', '', 2, 'user10', 0, '0000-00-00 00:00:00', 0, 'mod_sections', 0, 0, 1, 'count=5\nmoduleclass_sfx=\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(63, 'Popular News', '', 1, 'user11', 0, '0000-00-00 00:00:00', 0, 'mod_mostread', 0, 0, 1, 'moduleclass_sfx=\nshow_front=1\ncount=5\ncatid=\nsecid=\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(67, 'Gavick News Image III', '', 0, 'header1', 62, '2012-11-27 10:00:04', 1, 'mod_gk_news_image_3', 0, 0, 0, 'moduleclass_sfx=\nmodule_id=news_image_6_1\ngroup_id=1\nmodule_bg=\nshow_text_block=1\ntext_block_opacity=0.45\ntext_block_height=120\ntext_block_background=1\ntext_block_bgcolor=#000000\nclean_xhtml=1\nreadmore_button=1\nreadmore_text=Lire la suite...\ntitle_link=1\ntabs_col=4\ntabs_row=4\ntabs_margin=5\ntabs_padding=3\ntabs_border=2\nanimation_slide_speed=2000\nanimation_interval=7000\nautoanimation=1\nanimation_slide_type=0\nanimation_text_type=0\ntabs_position=left\ntooltips=1\ntooltips_anim=1\nclickable_slides=1\npreloading=1\nclean_code=1\nuseMoo=2\nuseScript=2\ncompress_js=1\n\n', 0, 0, ''),
(68, 'Ressources', '<table style="; width: 100%;" border="0" cellpadding="0" cellspacing="0">\r\n<tbody>\r\n<tr style="height: 23px;">\r\n<td style="width: 565px height:140px; background-image: url(''images/bb.png'');" colspan="2"><span style="color: #ff3300;"><strong><span style="color: #ff6600; font-size: 10pt;"><strong>RESSOURCES </strong></span><br /></strong></span></td>\r\n<td style="background-image: url(''images/bb.png'');"><span style="color: #ff3300;"><strong><span style="color: #ff6600; font-size: 10pt;"><span style="font-family: tahoma,arial,helvetica,sans-serif;"><strong>LIENS UTILES</strong></span></span></strong></span></td>\r\n<td style="background-image: url(''images/bb.png'');"><span style="color: #ff3300;"><strong><span style="color: #ff6600; font-size: 10pt;"><span style="font-family: tahoma,arial,helvetica,sans-serif;"><strong>SIGMAP</strong></span></span></strong></span></td>\r\n</tr>\r\n<tr>\r\n<td valign="top"><span color="#008000" size="3" style="color: #008000; font-size: small;"><span class="Apple-style-span" style="font-size: 13px;"><a href="index.php?option=com_code"><img src="images/add1.gif" alt="point" />&nbsp;Code des march�s<br /></a><a href="index.php?option=com_programme"><img src="images/add1.gif" alt="point" />&nbsp;Programme de formation<br /></a><a href="index.php?option=com_legislationvisualisation"><img src="images/add1.gif" alt="point" />&nbsp;Espace l�gislation</a><br /> <a href="index.php?option=com_vmarket"><img src="images/add1.gif" alt="point" />&nbsp;Dossiers types</a><br /></span></span></td>\r\n<td valign="top"><span color="#008000" size="3" style="color: #008000; font-size: small;"><span class="Apple-style-span" style="font-size: 13px;"> <a href="index.php?option=com_documents&amp;idcat=1&amp;Itemid=119"><img src="images/add1.gif" alt="point" /></a><a href="index.php?option=com_rapportarmp">&nbsp;Rapports ARMP</a><br /><a href="index.php?option=com_rapportdcmp"><img src="images/add1.gif" alt="point" />&nbsp;Rapports DNCMP</a><br /><img src="images/add1.gif" alt="point" />&nbsp;<a href="index.php?option=com_audit">Rapports d''audit</a><br /><img src="images/add1.gif" alt="point" />&nbsp;<a href="index.php?option=com_documents">Autres documents</a></span></span></td>\r\n<td valign="top"><span style="font-size: 10pt;"><span style="font-size: small; color: #008000;"><a href="index.php?option=com_annuaire&amp;page=0&amp;Itemid=188"><img src="images/add1.gif" alt="point" />&nbsp;Annuaire D.N.C.M.P</a></span></span><br /><span style="font-size: small; color: #008000;"><img src="images/add1.gif" alt="point" />&nbsp;<a href="index.php?option=com_annuaire&amp;page=1&amp;Itemid=188">Annuaire A.R.M.P</a></span><br /><span style="font-size: small; color: #008000;"><img src="images/add1.gif" alt="point" />&nbsp;<a href="index.php?option=com_annuaire&amp;page=2&amp;Itemid=188">Annuaire Autorit�s contractantes</a></span><br /><span style="font-size: small; color: #008000;"><img src="images/add1.gif" alt="point" />&nbsp;<a href="index.php?option=com_annuaire&amp;page=4&amp;Itemid=188">Auditeurs et Experts Comptables</a></span><br /> <span style="font-size: small; color: #008000;"><img src="images/add1.gif" alt="point" />&nbsp;<a href="http://www.marchespublics-uemoa.net/" target="_blank">Portail r�gional des march�s publics</a></span><br /> <span style="font-size: small; color: #008000;"><img src="images/add1.gif" alt="point" />&nbsp;<a href="http://armp-togo.com/" target="_blank">ARMP</a></span><br /> <span color="#008000"><span color="#008000" style="color: #008000;"><br /></span></span></td>\r\n<td valign="top"><span style="font-size: 10pt;"><span style="font-size: small; color: #008000;"><a href="http://localhost:8080/Sygmap-WEB" target="_blank"><img src="images/add1.gif" alt="point" /> Syst�me des march�s publics</a></span></span><br /> <span style="font-size: small; color: #008000;"><img src="images/add1.gif" alt="point" />&nbsp;<a href="http://finances.gouv.tg/" target="_blank">Minist�re de l�Economie et des Finances</a></span><br /> <span style="font-size: small; color: #008000;"><img src="images/add1.gif" alt="point" />&nbsp;<a href="http://www.afdb.org/fr/" target="_blank">BAD</a></span><br /> <span style="font-size: small; color: #008000;"><img src="images/add1.gif" alt="point" />&nbsp;<a href="http://www.banquemondiale.org/" target="_blank">Banque mondiale</a></span><br /> <span style="font-size: small; color: #008000;"><img src="images/add1.gif" alt="point" />&nbsp;<a href="http://www.tg.undp.org/undptogo/index.htm" target="_blank">PNUD</a></span><br /><span color="#008000"><span color="#008000" style="color: #008000;"><a href="index.php?option=com_content&amp;view=article&amp;id=61:vert&amp;catid=3:newsflash"><img style="vertical-align: middle;" alt="n-vert" src="images/stories/n-vert.jpg" height="50" width="210" /></a><br /></span></span></td>\r\n</tr>\r\n</tbody>\r\n</table>', 5, 'bottom', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(69, 'Consultations', '<table style="background-color: #ffffff; width: 100%;" border="0">\r\n<tbody>\r\n<tr>\r\n<td style="background-color: #00802b; background-image: url(''images/stories/ressources.png''); text-align: center;"><span style="color: #ffffff;"><strong>CONSULTATIONS</strong></span></td>\r\n<td style="background-color: #00802b; background-image: url(''images/stories/ressources.png''); text-align: center;"><span style="color: #ffffff;"><strong>ALERTES</strong></span></td>\r\n<td style="background-color: #00802b; background-image: url(''images/stories/ressources.png''); text-align: center;"><span style="color: #ffffff;"><strong>LISTE ROUGE</strong></span></td>\r\n<td style="background-color: #00802b; background-image: url(''images/stories/ressources.png''); text-align: center;"><span style="color: #ffffff;"><strong>DENONCIATIONS</strong></span></td>\r\n<td style="background-color: #00802b; background-image: url(''images/stories/ressources.png''); text-align: center;"><span style="color: #ffffff;"><strong>STATISTIQUES</strong></span></td>\r\n</tr>\r\n<tr>\r\n<td style="background-color: #f7f7f7;" valign="top"><span style="color: #339966;"><img alt="consultations" src="images/stories/consultations.jpg" height="94" width="175" /></span><br /><br /><img alt="fleche" src="images/stories/fleche.png" height="8" width="8" /><a href="#"><span style="font-size: 10pt; color: #ff6600;">Plan de passation</span></a><br /><span style="font-size: 10pt;"><img alt="fleche" src="images/stories/fleche.png" height="8" width="8" /><a href="#"><span style="color: #ff6600;">Avis g�n�raux</span></a></span><br /><span style="font-size: 10pt;"><img alt="fleche" src="images/stories/fleche.png" height="8" width="8" /><a href="#"><span style="color: #ff6600;">Appels d''offres</span></a></span><br /><span style="font-size: 10pt;"><img alt="fleche" src="images/stories/fleche.png" height="8" width="8" /><a href="#"><span style="color: #ff6600;">Avis d''attributions</span></a></span><br /><span style="font-size: 10pt;"><img alt="fleche" src="images/stories/fleche.png" height="8" width="8" /><a href="#"><span style="color: #ff6600;">Arr�t�s de r�siliation</span></a></span><br /><span style="font-size: 10pt;"><img alt="fleche" src="images/stories/fleche.png" height="8" width="8" /><a href="#"><span style="color: #ff6600;">Contentieux</span></a></span></td>\r\n<td style="background-color: #f7f7f7;" valign="top">\r\n<p><span style="color: #008000;"><strong><img alt="alerte" src="images/stories/alerte.jpg" height="94" width="175" /></strong></span><br /><span style="font-size: 10pt;"><span style="color: #000000;">Soyez automatiquement </span><span style="color: #000000;">alert�s</span></span><span style="color: #008000;"><strong><span style="color: #000000;"><a href="#"><span style="color: #ff6600; font-size: 10pt;"><br />Abonnez vous</span></a></span><br /></strong></span></p>\r\n</td>\r\n<td style="background-color: #f7f7f7;" valign="top">\r\n<p><strong><span style="color: #339966;"><img alt="listerouge" src="images/stories/listerouge.jpg" height="94" width="175" /></span></strong><br /><span style="font-size: 10pt;"><span style="color: #000000;">Pour consulter la liste <br /></span><span style="color: #000000;">des entreprises exclues <br />du</span><span style="color: #000000;"> droit � concourir pour<br /></span><span style="color: #000000;">l''obtention de march�s</span><span style="color: #000000;"> <br />publics</span> <a href="#"><span style="color: #ff6600;">cliquez ici</span></a></span></p>\r\n<p><strong><span style="color: #339966;"><br /></span></strong></p>\r\n</td>\r\n<td style="background-color: #f7f7f7;" valign="top">\r\n<p><strong><span style="color: #339966;"><img alt="denociations" src="images/stories/denociations.jpg" height="94" width="175" /></span></strong><br /><span style="color: #000000; font-size: 10pt;">Pour faire un d�nonciation</span><br /><span style="font-size: 10pt;"><span style="color: #000000;">anonyme</span><a href="#"><span style="color: #ff6600;"> cliquez ici</span></a></span></p>\r\n<p><strong><span style="color: #339966;"><br /></span></strong></p>\r\n</td>\r\n<td style="background-color: #f7f7f7;" valign="top">\r\n<p style="text-align: justify;"><strong><span style="color: #339966;"><img alt="statistiques" src="images/stories/statistiques.jpg" height="94" width="175" /></span></strong><br /><span style="color: #000000; font-size: 10pt;">Pour consulter les </span><br /><span style="font-size: 10pt;"><span style="color: #000000;">statistiques</span><a href="#"><span style="color: #ff6600;"> cliquez ici</span></a></span></p>\r\n<p><strong><span style="color: #339966;"><br /></span></strong></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', 2, 'top', 0, '0000-00-00 00:00:00', 0, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(70, 'Gavick News Highlighter GK1', '', 0, 'breadcrumbs', 0, '0000-00-00 00:00:00', 1, 'mod_gk_news_highlighter', 0, 0, 0, 'moduleclass_sfx=\nmodule_id=news-highlight-1\nmoduleHeight=28\nmoduleWidth=565\ninterfaceWidth=120\nbgcolor=#af0e2d\nbordercolor=#E9E9A1\nlinkcolor=\nhlinkcolor=\ntextleft_color=#fbcb00\ntextleft_style=bold\nintrotext=ACTUALITES\nextra_divs=1\narrows=1\nsection=1\ncategory=1\nsections=\ncategories=\nids=\nnews_amount=\nnews_sort_value=created\nnews_sort_order=DESC\nnews_frontpage=1\nunauthorized=0\nonly_frontpage=0\nstartposition=0\nlinks=1\nshow_title=1\nshow_description=1\ntitle_length=50\ndesc_length=30\nanimationType=3\nanimationSpeed=250\nanimationInterval=5000\nanimationFun=Fx.Transitions.linear\nmouseover=1\nclean_code=1\nuseMoo=2\nuseScript=2\ncompress_js=1\n\n', 0, 0, ''),
(71, 'Numero vert', '<a href="#"><img src="images/stories/numero-vert.jpg" alt="" align="left" /></a><br />', 0, 'top', 0, '0000-00-00 00:00:00', 0, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(72, 'Espace entreprise', '<p><span style="color: #008000;"><strong>Espace Entreprise</strong></span></p>\r\n<img style="margin-right: 4px; float: left;" src="images/stories/espace-entreprise.jpg" />\r\n<p style="text-align: justify;"><span style="color: #000000;"><span style="font-size: x-small;">Cet espace offre des services tels que les alertes e-mails et le rappel principalement destin�s aux entreprises ou consultants individuels d�sirants d��tre inform�s sur les march�s publics du Togo.<br /><a href="#"><img style="float: right;" src="images/stories/connecter.png" /></a><a href="#"><img src="images/stories/inscrire.png" alt="" align="right" /></a><br /></span></span></p>', 0, 'top', 0, '0000-00-00 00:00:00', 0, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(73, 'Accordion Menu Lite', '', 0, 'top', 0, '0000-00-00 00:00:00', 0, 'mod_accordionmenu_lite', 0, 0, 0, 'menutype=Publications\nmoduleclass_sfx=\nremind=0\nparentlink=0\n\n', 0, 0, ''),
(74, 'Recherche avanc�e', '<p><a href="index.php?option=com_rechavanceeppm&amp;lang=fr"><strong><span style="color: #ccffcc;">Recherche avanc�e</span></strong></a></p>', 0, 'search', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(75, 'Menu footer', '<span style="color: #ccffcc;"><a href="#">ACCUEIL</a><strong><span style="color: #800000;"> |<a href="index.php?option=com_rss&amp;view=list&amp;Itemid=108&amp;lang=fr"><img src="images/stories/rss.png" alt="rss" height="10" width="10" /></a></span></strong><a href="index.php?option=com_rss&amp;view=list&amp;Itemid=108&amp;lang=fr"> FLUX RSS</a> <strong><span style="color: #800000;">|</span></strong> <a href="index.php?option=com_contact&amp;view=contact&amp;catid=12:contacts&amp;id=1-marches-public-togo">CONTACTS</a> <strong><span style="color: #800000;">|</span></strong><a href="#"> <span style="color: #ccffcc;"></span></a><a href="index.php?option=com_mentionlegale&amp;view=list&amp;Itemid=93&amp;lang=fr">MENTIONS LEGALES</a></span><br />', 2, 'user7', 62, '2012-12-10 18:28:24', 1, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(76, 'Lexique des march�s publics', '<img src="images/stories/lexique.png" alt="" align="left" /><span style="color: #993300;"><a href="#"><span style="font-size: 12pt;"><strong>Lexique des march�s publics</strong></span></a><br /></span>\r\n<p></p>\r\n<p style="text-align: justify;"><span style="color: #993300;"></span><span style="color: #000000; font-family: Arial,Helvetica,sans-serif; font-size: 11px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; background-color: #ffffff; float: none;">Tous les termes et le vocabulaire employ�s dans le code des march�s publics ou dans les proc�dures d''appels d''offres de march�s publics sont expliqu�s ici.</span></p>\r\n<p><span style="color: #993300;"><br /></span></p>', 0, 'top', 0, '0000-00-00 00:00:00', 0, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(78, 'Liste rouge', '<table style="background-color: #ffffff; width: 100%; margin: auto 5px;" border="0" cellpading="20" cellspacing="20">\r\n<tbody>\r\n<tr>\r\n<td width="185" style="background-color: #ffffff; background-image: url(''images/apple_box.png''); background-position: 0 0; background-repeat: repeat-x; border: 0 solid #FFFFFF; border-radius: 0 0 0 0; bottom: 0; box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.6);">\r\n<table style="padding: 0px 10px 0px 0px;">\r\n<tbody>\r\n<tr>\r\n<td class="titre_2" style="color: #993333; margin-top: 20px; display: block; font: 20px/1.2em ''Overlock'',''sans-serif'';"><span>Statistiques</span></td>\r\n</tr>\r\n<tr>\r\n<td class="description">\r\n<p>Cette espace permet de consulter les statistiques p�riodiques et les statistiques g�n�rales de toutes les rubriques du portail des march�s publics du Togo</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="font: normal normal normal 16px/1.3em ''Open Sans'', sans-serif;" class="clicici"><a href="index.php?option=com_statistiques&amp;view=list&amp;stat=1&amp;Itemid=96&amp;lang=fr">cliquez ici</a></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n<td width="25"></td>\r\n<td width="185" style="background-color: #ffffff; background-image: url(''images/apple_box.png''); background-position: 0 0; background-repeat: repeat-x; border: 0 solid #FFFFFF; border-radius: 0 0 0 0; bottom: 0; box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.6);">\r\n<table style="padding: 0px 10px 0px 0px;">\r\n<tbody>\r\n<tr>\r\n<td class="titre_2" style="color: #993333; margin-top: 20px; display: block; font: 20px/1.2em ''Overlock'',''sans-serif'';"><span>Lexique</span></td>\r\n</tr>\r\n<tr>\r\n<td class="description">\r\n<p>Dans cet espace tous les termes et le vocabulaire employ�s dans le code des march�s publics ou dans les proc�dures d''appels d''offres de march�s publics sont expliqu�s</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="font: normal normal normal 16px/1.3em ''Open Sans'', sans-serif;" class="clicici"><a href="index.php?option=com_dictionnaires&amp;lang=fr">cliquez ici</a></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n<td width="25"></td>\r\n<td width="185" style="background-color: #ffffff; background-image: url(''images/apple_box.png''); background-position: 0 0; background-repeat: repeat-x; border: 0 solid #FFFFFF; border-radius: 0 0 0 0; bottom: 0; box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.6);">\r\n<table style="padding: 0px 10px 0px 0px;">\r\n<tbody>\r\n<tr>\r\n<td class="titre_2" style="color: #993333; margin-top: 20px; display: block; font: 20px/1.2em ''Overlock'',''sans-serif'';"><span>Publication</span></td>\r\n</tr>\r\n<tr>\r\n<td class="description">\r\n<p>Dans cet rubrique vous avez la possibilit� de consulter le recueil des textes r�glementaires et journal des march�s publics. Veuillez suivre le lien pour consulter.</p>\r\n<p></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="font: normal normal normal 16px/1.3em ''Open Sans'', sans-serif;" class="clicici"><a href="index.php?option=com_reglementaire&amp;view=list&amp;Itemid=92&amp;lang=fr">cliquez ici</a></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n<td width="25"></td>\r\n<td width="185" style="background-color: #ffffff; background-image: url(''images/apple_box.png''); background-position: 0 0; background-repeat: repeat-x; border: 0 solid #FFFFFF; border-radius: 0 0 0 0; bottom: 0; box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.6);">\r\n<table style="padding: 0px 10px 0px 0px;">\r\n<tbody>\r\n<tr>\r\n<td class="titre_2" style="color: #993333; margin-top: 20px; display: block; font: 20px/1.2em ''Overlock'',''sans-serif'';"><span>R�ferentiel des prix</span></td>\r\n</tr>\r\n<tr>\r\n<td class="description">\r\n<p>Cet espace vous permet de consulter tous les prix de r�ference qui ont �t� �tablis sur le march� du Togo. Veuillez suivre le lien pour consulter ces derniers.</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style="font: normal normal normal 16px/1.3em ''Open Sans'', sans-serif;" class="clicici"><a href="index.php?option=com_refprix&amp;view=list&amp;Itemid=94&amp;lang=fr">cliquez ici</a></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', 4, 'bottom', 62, '2012-12-10 12:26:58', 1, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, '');
INSERT INTO `jos_modules` (`id`, `title`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `published`, `module`, `numnews`, `access`, `showtitle`, `params`, `iscore`, `client_id`, `control`) VALUES
(77, 'Consultations', '<table class="conteneur" cellpadding="10" cellspacing="10">\r\n<tbody>\r\n<tr style="height: 94;">\r\n<td style="background-color: #ffffff; border: 8px solid #F0F0F0; border-radius: 0 0 0 0;" nowrap="nowrap" bgcolor="#F0F0F0" valign="top">\r\n<table style="vertical-align: top; height: 342%;">\r\n<tbody>\r\n<tr>\r\n<td valign="top" width="173"><img src="images/stories/consultations.jpg" height="94" width="175" /></td>\r\n</tr>\r\n<tr>\r\n<td style="background-color: #152635; border: 0 solid #152635; border-radius: 0 0 0 0; display: block; width: 175px; height: 30px;">\r\n<h4 style="position: relative; top: -8px; text-transform: uppercase; font-size: 18px/1.2em; font-family: ''Anton'',''sans-serif''; text-align: center; color: #ffffff;">Num�ro vert</h4>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td class="pointu" style="text-align: center; position: relative; top: -2px;"><img src="images/stories/flechette.png" /></td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p><span class="font_9"> Cet espace vous permet de<br /> consulter le num�ro vert de <br /> l''autorit� de r�gulation des <br />march�s publics du Togo.<br /> Veuillez cliquer sur le lien<br /> ci apr�s pour plus d''informations. </span></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><span class="suite"><a href="index.php?option=com_plandepassation&amp;view=list">Cliquez ici</a></span></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n<td></td>\r\n<td style="background-color: #ffffff; border: 8px solid #F0F0F0; border-radius: 0 0 0 0;" nowrap="nowrap" bgcolor="#F0F0F0" valign="top">\r\n<table style="vertical-align: top; height: 342%;">\r\n<tbody>\r\n<tr>\r\n<td colspan="2" valign="top" width="173"><img alt="alerte" src="images/stories/espace-entreprise-togo.jpg" height="94" width="175" /></td>\r\n</tr>\r\n<tr>\r\n<td style="background-color: #152635; border: 0 solid #152635; border-radius: 0 0 0 0; display: block; width: 175px; height: 30px;">\r\n<h4 style="position: relative; top: -8px; text-transform: uppercase; font-size: 18px/1.2em; font-family: ''Anton'',''sans-serif''; text-align: center; color: #ffffff;">ESPACE ENTREPRISE</h4>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td class="pointu" style="text-align: center; position: relative; top: -2px;"><img src="images/stories/flechette.png" /></td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p><span class="font_9">Cet espace offre des services tels <br />que les alertes e-mails et le <br />rappel principalement destin�s <br />aux entreprises ou consultants <br />individuels d�sirants d��tre inform�s <br />sur les march�s publics du Togo.<br /></span></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><span class="suite"><a href="index.php?option=com_user&amp;view=login&amp;lang=fr">Cliquez ici</a></span></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n<td></td>\r\n<td style="background-color: #ffffff; border: 8px solid #F0F0F0; border-radius: 0 0 0 0;" nowrap="nowrap" bgcolor="#F0F0F0" valign="top">\r\n<table style="vertical-align: top; height: 342%;">\r\n<tbody>\r\n<tr>\r\n<td valign="top" width="173"><img alt="listerouge" src="images/stories/denociations.jpg" height="94" width="175" /></td>\r\n</tr>\r\n<tr>\r\n<td style="background-color: #152635; border: 0 solid #152635; border-radius: 0 0 0 0; display: block; width: 175px; height: 30px;">\r\n<h4 style="position: relative; top: -8px; text-transform: uppercase; font-size: 18px/1.2em; font-family: ''Anton'',''sans-serif''; text-align: center; color: #ffffff;">LISTE ROUGE</h4>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td class="pointu" style="text-align: center; position: relative; top: -2px;"><img src="images/stories/flechette.png" /></td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p><span class="font_9">Cet espace vous permet de<br />consulter la liste des structures&nbsp;<br />qui ont �t� mis sur liste rouge<br />car ayant eu recours � des&nbsp;<br />m�thodes frauduleuses&nbsp;<br />sur les march�s publics du Togo.</span></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><span class="suite"><a href="index.php?option=com_listerouge&amp;view=list&amp;lang=fr">Cliquez ici</a></span></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n<td></td>\r\n<td style="background-color: #ffffff; border: 8px solid #F0F0F0; border-radius: 0 0 0 0;" nowrap="nowrap" bgcolor="#F0F0F0" valign="top">\r\n<table style="vertical-align: top; height: 342%;">\r\n<tbody>\r\n<tr>\r\n<td valign="top" width="173"><img src="images/stories/denonciation.jpg" height="94" width="175" /></td>\r\n</tr>\r\n<tr>\r\n<td style="background-color: #152635; border: 0 solid #152635; border-radius: 0 0 0 0; display: block; width: 175px; height: 30px;">\r\n<h4 style="position: relative; top: -8px; text-transform: uppercase; font-size: 18px/1.2em; font-family: ''Anton'',''sans-serif''; text-align: center; color: #ffffff;">DENONCIATIONS</h4>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td class="pointu" style="text-align: center; position: relative; top: -2px;"><img src="images/stories/flechette.png" /></td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p><span class="font_9">Cet espace vous permet de<br />faire des d�nonciations<br />par rapport � des d�marches<br />frauduleuses auxquels vous<br />avez assist� sur le portail<br />des march�s publics du Togo.</span></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><span class="suite"><a href="index.php?option=com_denonciations&amp;lang=fr">Cliquez ici</a></span></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', 3, 'bottom', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(79, 'Menu top', '', 2, 'banner1', 0, '0000-00-00 00:00:00', 0, 'mod_mainmenu', 0, 0, 0, 'menutype=topmenu\nmenu_style=horiz_flat\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\nshow_whitespace=1\ncache=1\ntag_id=\nclass_sfx=\nmoduleclass_sfx=\nmaxdepth=10\nmenu_images=0\nmenu_images_align=0\nmenu_images_link=0\nexpand_menu=0\nactivate_parent=0\nfull_active_id=1\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\n\n', 0, 0, ''),
(80, 'Menutop', '<p style="text-align: justify;"><span style="color: #800000;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=59:armp&amp;catid=3:newsflash">ARMP</a> | <a href="index.php?option=com_content&amp;view=article&amp;id=60:dcmp&amp;catid=3:newsflash">DNCMP </a>| <a href="http://localhost:8080/Sygmap-WEB" target="_blank">SIGMAP</a></strong></span></p>', 0, 'menutop', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, '');

-- --------------------------------------------------------

--
-- Structure de la table `jos_modules_menu`
--

CREATE TABLE IF NOT EXISTS `jos_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`moduleid`,`menuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `jos_modules_menu`
--

INSERT INTO `jos_modules_menu` (`moduleid`, `menuid`) VALUES
(1, 0),
(16, 1),
(17, 0),
(18, 0),
(19, 0),
(21, 1),
(22, 0),
(24, 0),
(25, 0),
(26, 0),
(27, 0),
(28, 1),
(29, 0),
(30, 0),
(31, 1),
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(38, 1),
(39, 43),
(39, 44),
(39, 45),
(39, 46),
(39, 47),
(40, 0),
(43, 1),
(44, 1),
(45, 1),
(47, 0),
(48, 1),
(49, 41),
(49, 48),
(49, 49),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(62, 0),
(63, 0),
(64, 0),
(65, 1),
(66, 0),
(67, 1),
(68, 0),
(69, 0),
(70, 0),
(71, 0),
(72, 0),
(73, 0),
(74, 0),
(75, 0),
(76, 1),
(77, 1),
(78, 1),
(79, 0),
(80, 0);

-- --------------------------------------------------------

--
-- Structure de la table `jos_newsfeeds`
--

CREATE TABLE IF NOT EXISTS `jos_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `link` text NOT NULL,
  `filename` varchar(200) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(11) unsigned NOT NULL DEFAULT '1',
  `cache_time` int(11) unsigned NOT NULL DEFAULT '3600',
  `checked_out` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `published` (`published`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `jos_newsfeeds`
--

INSERT INTO `jos_newsfeeds` (`catid`, `id`, `name`, `alias`, `link`, `filename`, `published`, `numarticles`, `cache_time`, `checked_out`, `checked_out_time`, `ordering`, `rtl`) VALUES
(4, 1, 'Joomla! Announcements', 'joomla-official-news', 'http://feeds.joomla.org/JoomlaAnnouncements', '', 1, 5, 3600, 0, '0000-00-00 00:00:00', 1, 0),
(4, 2, 'Joomla! Core Team Blog', 'joomla-core-team-blog', 'http://feeds.joomla.org/JoomlaCommunityCoreTeamBlog', '', 1, 5, 3600, 0, '0000-00-00 00:00:00', 2, 0),
(4, 3, 'Joomla! Community Magazine', 'joomla-community-magazine', 'http://feeds.joomla.org/JoomlaMagazine', '', 1, 20, 3600, 0, '0000-00-00 00:00:00', 3, 0),
(4, 4, 'Joomla! Developer News', 'joomla-developer-news', 'http://feeds.joomla.org/JoomlaDeveloper', '', 1, 5, 3600, 0, '0000-00-00 00:00:00', 4, 0),
(4, 5, 'Joomla! Security News', 'joomla-security-news', 'http://feeds.joomla.org/JoomlaSecurityNews', '', 1, 5, 3600, 0, '0000-00-00 00:00:00', 5, 0),
(5, 6, 'Free Software Foundation Blogs', 'free-software-foundation-blogs', 'http://www.fsf.org/blogs/RSS', NULL, 1, 5, 3600, 0, '0000-00-00 00:00:00', 4, 0),
(5, 7, 'Free Software Foundation', 'free-software-foundation', 'http://www.fsf.org/news/RSS', NULL, 1, 5, 3600, 62, '2008-09-14 00:24:25', 3, 0),
(5, 8, 'Software Freedom Law Center Blog', 'software-freedom-law-center-blog', 'http://www.softwarefreedom.org/feeds/blog/', NULL, 1, 5, 3600, 0, '0000-00-00 00:00:00', 2, 0),
(5, 9, 'Software Freedom Law Center News', 'software-freedom-law-center', 'http://www.softwarefreedom.org/feeds/news/', NULL, 1, 5, 3600, 62, '2012-11-27 10:04:43', 1, 0),
(5, 10, 'Open Source Initiative Blog', 'open-source-initiative-blog', 'http://www.opensource.org/blog/feed', NULL, 1, 5, 3600, 0, '0000-00-00 00:00:00', 5, 0),
(6, 11, 'PHP News and Announcements', 'php-news-and-announcements', 'http://www.php.net/feed.atom', NULL, 1, 5, 3600, 62, '2008-09-14 00:25:37', 1, 0),
(6, 12, 'Planet MySQL', 'planet-mysql', 'http://www.planetmysql.org/rss20.xml', NULL, 1, 5, 3600, 62, '2008-09-14 00:25:51', 2, 0),
(6, 13, 'Linux Foundation Announcements', 'linux-foundation-announcements', 'http://www.linuxfoundation.org/press/rss20.xml', NULL, 1, 5, 3600, 62, '2008-09-14 00:26:11', 3, 0),
(6, 14, 'Mootools Blog', 'mootools-blog', 'http://feeds.feedburner.com/mootools-blog', NULL, 1, 5, 3600, 62, '2008-09-14 00:26:51', 4, 0);

-- --------------------------------------------------------

--
-- Structure de la table `jos_plugins`
--

CREATE TABLE IF NOT EXISTS `jos_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `element` varchar(100) NOT NULL DEFAULT '',
  `folder` varchar(100) NOT NULL DEFAULT '',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(3) NOT NULL DEFAULT '0',
  `iscore` tinyint(3) NOT NULL DEFAULT '0',
  `client_id` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_folder` (`published`,`client_id`,`access`,`folder`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Contenu de la table `jos_plugins`
--

INSERT INTO `jos_plugins` (`id`, `name`, `element`, `folder`, `access`, `ordering`, `published`, `iscore`, `client_id`, `checked_out`, `checked_out_time`, `params`) VALUES
(1, 'Authentication - Joomla', 'joomla', 'authentication', 0, 1, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(2, 'Authentication - LDAP', 'ldap', 'authentication', 0, 2, 0, 1, 0, 0, '0000-00-00 00:00:00', 'host=\nport=389\nuse_ldapV3=0\nnegotiate_tls=0\nno_referrals=0\nauth_method=bind\nbase_dn=\nsearch_string=\nusers_dn=\nusername=\npassword=\nldap_fullname=fullName\nldap_email=mail\nldap_uid=uid\n\n'),
(3, 'Authentication - GMail', 'gmail', 'authentication', 0, 4, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(4, 'Authentication - OpenID', 'openid', 'authentication', 0, 3, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(5, 'User - Joomla!', 'joomla', 'user', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'autoregister=1\n\n'),
(6, 'Search - Content', 'content', 'search', 0, 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\nsearch_content=1\nsearch_uncategorised=1\nsearch_archived=1\n\n'),
(7, 'Search - Contacts', 'contacts', 'search', 0, 3, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(8, 'Search - Categories', 'categories', 'search', 0, 4, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(9, 'Search - Sections', 'sections', 'search', 0, 5, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(10, 'Search - Newsfeeds', 'newsfeeds', 'search', 0, 6, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(11, 'Search - Weblinks', 'weblinks', 'search', 0, 2, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(12, 'Content - Pagebreak', 'pagebreak', 'content', 0, 10000, 1, 1, 0, 0, '0000-00-00 00:00:00', 'enabled=1\ntitle=1\nmultipage_toc=1\nshowall=1\n\n'),
(13, 'Content - Rating', 'vote', 'content', 0, 4, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(14, 'Content - Email Cloaking', 'emailcloak', 'content', 0, 5, 1, 0, 0, 0, '0000-00-00 00:00:00', 'mode=1\n\n'),
(15, 'Content - Code Hightlighter (GeSHi)', 'geshi', 'content', 0, 5, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(16, 'Content - Load Module', 'loadmodule', 'content', 0, 6, 1, 0, 0, 0, '0000-00-00 00:00:00', 'enabled=1\nstyle=0\n\n'),
(17, 'Content - Page Navigation', 'pagenavigation', 'content', 0, 2, 1, 1, 0, 0, '0000-00-00 00:00:00', 'position=1\n\n'),
(18, 'Editor - No Editor', 'none', 'editors', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(19, 'Editor - TinyMCE 2.0', 'tinymce', 'editors', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 'theme=advanced\ncleanup=1\ncleanup_startup=0\nautosave=0\ncompressed=0\nrelative_urls=1\ntext_direction=ltr\nlang_mode=0\nlang_code=en\ninvalid_elements=applet\ncontent_css=1\ncontent_css_custom=\nnewlines=0\ntoolbar=top\nhr=1\nsmilies=1\ntable=1\nstyle=1\nlayer=1\nxhtmlxtras=0\ntemplate=0\ndirectionality=1\nfullscreen=1\nhtml_height=550\nhtml_width=750\npreview=1\ninsertdate=1\nformat_date=%Y-%m-%d\ninserttime=1\nformat_time=%H:%M:%S\n\n'),
(20, 'Editor - XStandard Lite 2.0', 'xstandard', 'editors', 0, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(21, 'Editor Button - Image', 'image', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(22, 'Editor Button - Pagebreak', 'pagebreak', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(23, 'Editor Button - Readmore', 'readmore', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(24, 'XML-RPC - Joomla', 'joomla', 'xmlrpc', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(25, 'XML-RPC - Blogger API', 'blogger', 'xmlrpc', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', 'catid=1\nsectionid=0\n\n'),
(27, 'System - SEF', 'sef', 'system', 0, 1, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(28, 'System - Debug', 'debug', 'system', 0, 2, 1, 0, 0, 0, '0000-00-00 00:00:00', 'queries=1\nmemory=1\nlangauge=1\n\n'),
(29, 'System - Legacy', 'legacy', 'system', 0, 3, 0, 1, 0, 0, '0000-00-00 00:00:00', 'route=0\n\n'),
(30, 'System - Cache', 'cache', 'system', 0, 4, 0, 1, 0, 0, '0000-00-00 00:00:00', 'browsercache=0\ncachetime=15\n\n'),
(31, 'System - Log', 'log', 'system', 0, 5, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(32, 'System - Remember Me', 'remember', 'system', 0, 6, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(33, 'System - Backlink', 'backlink', 'system', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(34, 'System - Mootools Upgrade', 'mtupgrade', 'system', 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(35, 'Editor - JCE', 'jce', 'editors', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Structure de la table `jos_polls`
--

CREATE TABLE IF NOT EXISTS `jos_polls` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `voters` int(9) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '0',
  `lag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `jos_polls`
--

INSERT INTO `jos_polls` (`id`, `title`, `alias`, `voters`, `checked_out`, `checked_out_time`, `published`, `access`, `lag`) VALUES
(14, 'Joomla! is used for?', 'joomla-is-used-for', 11, 0, '0000-00-00 00:00:00', 1, 0, 86400);

-- --------------------------------------------------------

--
-- Structure de la table `jos_poll_data`
--

CREATE TABLE IF NOT EXISTS `jos_poll_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pollid` int(11) NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  `hits` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pollid` (`pollid`,`text`(1))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `jos_poll_data`
--

INSERT INTO `jos_poll_data` (`id`, `pollid`, `text`, `hits`) VALUES
(1, 14, 'Community Sites', 2),
(2, 14, 'Public Brand Sites', 3),
(3, 14, 'eCommerce', 1),
(4, 14, 'Blogs', 0),
(5, 14, 'Intranets', 0),
(6, 14, 'Photo and Media Sites', 2),
(7, 14, 'All of the Above!', 3),
(8, 14, '', 0),
(9, 14, '', 0),
(10, 14, '', 0),
(11, 14, '', 0),
(12, 14, '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `jos_poll_date`
--

CREATE TABLE IF NOT EXISTS `jos_poll_date` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `vote_id` int(11) NOT NULL DEFAULT '0',
  `poll_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `poll_id` (`poll_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `jos_poll_date`
--

INSERT INTO `jos_poll_date` (`id`, `date`, `vote_id`, `poll_id`) VALUES
(1, '2006-10-09 13:01:58', 1, 14),
(2, '2006-10-10 15:19:43', 7, 14),
(3, '2006-10-11 11:08:16', 7, 14),
(4, '2006-10-11 15:02:26', 2, 14),
(5, '2006-10-11 15:43:03', 7, 14),
(6, '2006-10-11 15:43:38', 7, 14),
(7, '2006-10-12 00:51:13', 2, 14),
(8, '2007-05-10 19:12:29', 3, 14),
(9, '2007-05-14 14:18:00', 6, 14),
(10, '2007-06-10 15:20:29', 6, 14),
(11, '2007-07-03 12:37:53', 2, 14);

-- --------------------------------------------------------

--
-- Structure de la table `jos_poll_menu`
--

CREATE TABLE IF NOT EXISTS `jos_poll_menu` (
  `pollid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pollid`,`menuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `jos_poll_menu`
--


-- --------------------------------------------------------

--
-- Structure de la table `jos_sections`
--

CREATE TABLE IF NOT EXISTS `jos_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` text NOT NULL,
  `scope` varchar(50) NOT NULL DEFAULT '',
  `image_position` varchar(30) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_scope` (`scope`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `jos_sections`
--

INSERT INTO `jos_sections` (`id`, `title`, `name`, `alias`, `image`, `scope`, `image_position`, `description`, `published`, `checked_out`, `checked_out_time`, `ordering`, `access`, `count`, `params`) VALUES
(1, 'News', '', 'news', 'articles.jpg', 'content', 'right', 'Select a news topic from the list below, then select a news article to read.', 1, 0, '0000-00-00 00:00:00', 3, 0, 2, ''),
(3, 'FAQs', '', 'faqs', 'key.jpg', 'content', 'left', 'From the list below choose one of our FAQs topics, then select an FAQ to read. If you have a question which is not in this section, please contact us.', 1, 0, '0000-00-00 00:00:00', 5, 0, 23, ''),
(4, 'About Joomla!', '', 'about-joomla', '', 'content', 'left', '', 1, 0, '0000-00-00 00:00:00', 2, 0, 14, ''),
(5, 'Demo', '', 'demo', '', 'content', 'left', '', 1, 0, '0000-00-00 00:00:00', 6, 0, 1, '');

-- --------------------------------------------------------

--
-- Structure de la table `jos_session`
--

CREATE TABLE IF NOT EXISTS `jos_session` (
  `username` varchar(150) DEFAULT '',
  `time` varchar(14) DEFAULT '',
  `session_id` varchar(200) NOT NULL DEFAULT '0',
  `guest` tinyint(4) DEFAULT '1',
  `userid` int(11) DEFAULT '0',
  `usertype` varchar(50) DEFAULT '',
  `gid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `client_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `data` longtext,
  PRIMARY KEY (`session_id`(64)),
  KEY `whosonline` (`guest`,`usertype`),
  KEY `userid` (`userid`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `jos_session`
--

INSERT INTO `jos_session` (`username`, `time`, `session_id`, `guest`, `userid`, `usertype`, `gid`, `client_id`, `data`) VALUES
('', '1370852548', '4b0712f3c17feda61380bbe6f33b2e83', 1, 0, '', 0, 0, '__default|a:7:{s:22:"session.client.browser";s:72:"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0";s:15:"session.counter";i:7;s:8:"registry";O:9:"JRegistry":3:{s:17:"_defaultNameSpace";s:7:"session";s:9:"_registry";a:1:{s:7:"session";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:4:"user";O:5:"JUser":19:{s:2:"id";i:0;s:4:"name";N;s:8:"username";N;s:5:"email";N;s:8:"password";N;s:14:"password_clear";s:0:"";s:8:"usertype";N;s:5:"block";N;s:9:"sendEmail";i:0;s:3:"gid";i:0;s:12:"registerDate";N;s:13:"lastvisitDate";N;s:10:"activation";N;s:6:"params";N;s:3:"aid";i:0;s:5:"guest";i:1;s:7:"_params";O:10:"JParameter":7:{s:4:"_raw";s:0:"";s:4:"_xml";N;s:9:"_elements";a:0:{}s:12:"_elementPath";a:1:{i:0;s:58:"C:\\wamp\\www\\togomp\\libraries\\joomla\\html\\parameter\\element";}s:17:"_defaultNameSpace";s:8:"_default";s:9:"_registry";a:1:{s:8:"_default";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:9:"_errorMsg";N;s:7:"_errors";a:0:{}}s:19:"session.timer.start";i:1370852501;s:18:"session.timer.last";i:1370852537;s:17:"session.timer.now";i:1370852548;}');

-- --------------------------------------------------------

--
-- Structure de la table `jos_stats_agents`
--

CREATE TABLE IF NOT EXISTS `jos_stats_agents` (
  `agent` varchar(255) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `jos_stats_agents`
--


-- --------------------------------------------------------

--
-- Structure de la table `jos_templates_menu`
--

CREATE TABLE IF NOT EXISTS `jos_templates_menu` (
  `template` varchar(255) NOT NULL DEFAULT '',
  `menuid` int(11) NOT NULL DEFAULT '0',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menuid`,`client_id`,`template`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `jos_templates_menu`
--

INSERT INTO `jos_templates_menu` (`template`, `menuid`, `client_id`) VALUES
('gk_musictube', 0, 0),
('khepri', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `jos_users`
--

CREATE TABLE IF NOT EXISTS `jos_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `usertype` varchar(25) NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `gid` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `address` varchar(500) DEFAULT '',
  `bureau` varchar(500) DEFAULT '',
  `cellulaire` varchar(500) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `usertype` (`usertype`),
  KEY `idx_name` (`name`),
  KEY `gid_block` (`gid`,`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Contenu de la table `jos_users`
--

INSERT INTO `jos_users` (`id`, `name`, `username`, `email`, `password`, `usertype`, `block`, `sendEmail`, `gid`, `registerDate`, `lastvisitDate`, `activation`, `params`, `address`, `bureau`, `cellulaire`) VALUES
(62, 'Administrator', 'admin', 'eee@eee.pl', 'e7247759c1633c0f9f1485f3690294a9', 'Super Administrator', 0, 1, 25, '2009-02-10 08:38:46', '2013-05-24 17:04:04', '', 'admin_language=\nlanguage=\neditor=\nhelpsite=\ntimezone=0\n\n', '', '', ''),
(63, 'khadim', 'khadimsarr3322@yahoo.fr', 'khadimsarr3322@yahoo.fr', 'b9226e9dd66aed68cc9ddd42f0c475b6:iv9oDdwm3A0yy7topTwdslIMzHTGZOET', 'Consultant individuel', 1, 0, 18, '2012-11-26 15:13:13', '0000-00-00 00:00:00', '2702dd2db4e4ddaf20cd6008f5e9072f', '\n', '', '', ''),
(64, 'sarr', 'sarr@gmail.com', 'sarr@gmail.com', '33f7f013ce3e2d8c6a751731f7e7e0bf:JDjj3AqqxRlRAGj6J3RJOYUcPBMnXuma', 'Consultant individuel', 0, 0, 18, '2012-11-26 15:17:10', '2012-11-26 16:43:37', 'e051dc05d5385e495e89985f573d4977', '\n', '', '', ''),
(65, 'Rama', 'sy@yahoo.fr', 'sy@yahoo.fr', '2c3037ae2469d27a905af0f8dff483c7:pcsAZxzDivbwdCfGZSBTe3ZcEFFZCIAB', 'Consultant individuel', 0, 0, 18, '2012-11-26 16:58:17', '2012-11-28 10:45:04', '6c17dad85000726de7bbdd9d8669126e', '\n', 'Dakar', '338785412', '775874123');

-- --------------------------------------------------------

--
-- Structure de la table `jos_weblinks`
--

CREATE TABLE IF NOT EXISTS `jos_weblinks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `sid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(250) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`,`published`,`archived`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `jos_weblinks`
--

INSERT INTO `jos_weblinks` (`id`, `catid`, `sid`, `title`, `alias`, `url`, `description`, `date`, `hits`, `published`, `checked_out`, `checked_out_time`, `ordering`, `archived`, `approved`, `params`) VALUES
(1, 2, 0, 'Joomla!', 'joomla', 'http://www.joomla.org', 'Home of Joomla!', '2005-02-14 15:19:02', 3, 1, 0, '0000-00-00 00:00:00', 1, 0, 1, 'target=0'),
(2, 2, 0, 'php.net', 'php', 'http://www.php.net', 'The language that Joomla! is developed in', '2004-07-07 11:33:24', 6, 1, 0, '0000-00-00 00:00:00', 3, 0, 1, ''),
(3, 2, 0, 'MySQL', 'mysql', 'http://www.mysql.com', 'The database that Joomla! uses', '2004-07-07 10:18:31', 1, 1, 0, '0000-00-00 00:00:00', 5, 0, 1, ''),
(4, 2, 0, 'OpenSourceMatters', 'opensourcematters', 'http://www.opensourcematters.org', 'Home of OSM', '2005-02-14 15:19:02', 11, 1, 0, '0000-00-00 00:00:00', 2, 0, 1, 'target=0'),
(5, 2, 0, 'Joomla! - Forums', 'joomla-forums', 'http://forum.joomla.org', 'Joomla! Forums', '2005-02-14 15:19:02', 4, 1, 0, '0000-00-00 00:00:00', 4, 0, 1, 'target=0'),
(6, 2, 0, 'Ohloh Tracking of Joomla!', 'ohloh-tracking-of-joomla', 'http://www.ohloh.net/projects/20', 'Objective reports from Ohloh about Joomla''s development activity. Joomla! has some star developers with serious kudos.', '2007-07-19 09:28:31', 1, 1, 0, '0000-00-00 00:00:00', 6, 0, 1, 'target=0\n\n');

-- --------------------------------------------------------

--
-- Structure de la table `pmbbailleurs`
--

CREATE TABLE IF NOT EXISTS `pmbbailleurs` (
  `IDBAILLEURS` bigint(20) NOT NULL AUTO_INCREMENT,
  `LIBBAILLEURS` varchar(255) DEFAULT NULL,
  `typebailleurs` varchar(255) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IDBAILLEURS`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2855 ;

--
-- Contenu de la table `pmbbailleurs`
--

INSERT INTO `pmbbailleurs` (`IDBAILLEURS`, `LIBBAILLEURS`, `typebailleurs`, `Autorite_ID`) VALUES
(10, 'État : budget consolidé d''investissement( BCI)', 'Resources Internes', 2),
(11, 'Etat : budget de fonctionnement', 'Resources Internes', 2),
(15, 'Etat : Comptes spéciaux du trésor', 'Resources Internes', 2),
(820, ' État Sénégal: budget consolidé d''investissement gestion 2010', 'Resources Internes', 2),
(1277, 'BUDGET DE FONCTIONNEMENT ETAT 2010', 'Resources Internes', 2),
(1454, 'PNDL/CR', 'Financement extérieure', 2),
(1468, 'État du Sénégal: budget de fonctionnement, gestion 2010', 'Resources Internes', 2),
(2149, 'fonds GAVI-RSS', 'Financement extérieure', 2),
(2491, 'BCI 2011', 'Financement extérieure', 2),
(2695, 'Banque Islamique de Développement', 'Financement extérieure', 2),
(2768, 'BCI 2011', '1', 2),
(2853, 'sdd', '0', 2),
(2854, 'Banque Islamique de D�veloppement', 'Financement ext�rieure', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_annuaire`
--

CREATE TABLE IF NOT EXISTS `pmb_annuaire` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `fonction` varchar(255) DEFAULT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `Organe` varchar(50) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  `Service_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK262DEE097B75F0D4` (`Service_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_annuaire`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_appelsoffres`
--

CREATE TABLE IF NOT EXISTS `pmb_appelsoffres` (
  `apoID` bigint(20) NOT NULL AUTO_INCREMENT,
  `apoCommentaireAuto` varchar(255) DEFAULT NULL,
  `apoCommentaireMiseAuto` varchar(255) DEFAULT NULL,
  `apoDateAuto` date DEFAULT NULL,
  `apoDatecreation` date DEFAULT NULL,
  `apoDateMiseAuto` date DEFAULT NULL,
  `apoDateRejet` date DEFAULT NULL,
  `apoEtatValidation` int(11) DEFAULT NULL,
  `apoFichierMiseAuto` varchar(255) DEFAULT NULL,
  `apoFichierPV` varchar(100) DEFAULT NULL,
  `apoFichierValidation` varchar(255) DEFAULT NULL,
  `apoMontantestime` decimal(19,2) DEFAULT NULL,
  `apoNumeropretcredit` varchar(40) DEFAULT NULL,
  `apoObjet` varchar(255) DEFAULT NULL,
  `apoProjet` varchar(255) DEFAULT NULL,
  `apoReference` varchar(50) DEFAULT NULL,
  `ResponsableDCMP` int(11) DEFAULT NULL,
  `dateAffectationDossier` date DEFAULT NULL,
  `dateStopProcedure` date DEFAULT NULL,
  `etatseuil` int(11) DEFAULT NULL,
  `motifStopProcedure` varchar(255) DEFAULT NULL,
  `NumeroMarche` varchar(100) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  `Categorie_ID` varchar(255) DEFAULT NULL,
  `Modepassation_ID` bigint(20) DEFAULT NULL,
  `Modeselection` bigint(20) DEFAULT NULL,
  `Plan_ID` bigint(20) DEFAULT NULL,
  `Typemarche_ID` bigint(20) DEFAULT NULL,
  `apoDatepvouverturepli` date DEFAULT NULL,
  `apoStatut` varchar(10) DEFAULT NULL,
  `apoDateVersement` date DEFAULT NULL,
  `apomontantversement` decimal(19,2) DEFAULT NULL,
  `apoNbreDAO` int(11) DEFAULT NULL,
  PRIMARY KEY (`apoID`),
  KEY `FKB2E5FB763E345B81` (`Categorie_ID`),
  KEY `FKB2E5FB76296FFA9C` (`Typemarche_ID`),
  KEY `FKB2E5FB765FC4E24C` (`Modeselection`),
  KEY `FKB2E5FB7697DC1D4` (`Modepassation_ID`),
  KEY `FKB2E5FB764B9DC270` (`Autorite_ID`),
  KEY `FKB2E5FB7650D62C5D` (`Plan_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Contenu de la table `pmb_appelsoffres`
--

INSERT INTO `pmb_appelsoffres` (`apoID`, `apoCommentaireAuto`, `apoCommentaireMiseAuto`, `apoDateAuto`, `apoDatecreation`, `apoDateMiseAuto`, `apoDateRejet`, `apoEtatValidation`, `apoFichierMiseAuto`, `apoFichierPV`, `apoFichierValidation`, `apoMontantestime`, `apoNumeropretcredit`, `apoObjet`, `apoProjet`, `apoReference`, `ResponsableDCMP`, `dateAffectationDossier`, `dateStopProcedure`, `etatseuil`, `motifStopProcedure`, `NumeroMarche`, `Autorite_ID`, `Categorie_ID`, `Modepassation_ID`, `Modeselection`, `Plan_ID`, `Typemarche_ID`, `apoDatepvouverturepli`, `apoStatut`, `apoDateVersement`, `apomontantversement`, `apoNbreDAO`) VALUES
(1, NULL, NULL, NULL, '2013-04-28', NULL, NULL, 0, NULL, NULL, NULL, '500000000.00', NULL, 'Construction de route', NULL, 'T_DAGE_0001', 0, NULL, NULL, 0, NULL, NULL, 6, '4', 1, 2, 1, 3, '2013-04-29', 'OUI', '2013-05-13', '300000.00', 0),
(2, NULL, NULL, NULL, '2013-05-04', NULL, NULL, 0, NULL, NULL, NULL, '40000000000.00', NULL, 'Appel d''Offres Ouvert', NULL, 'T_DAGE_0012', 0, NULL, NULL, 0, NULL, NULL, 6, '4', 5, 2, 12, 3, NULL, 'OUI', '2013-05-13', '0.00', 0),
(3, NULL, NULL, NULL, '2013-05-04', NULL, NULL, 0, NULL, NULL, NULL, '500000000.00', NULL, 'Demande de Renseignement et de Prix', NULL, 'T_DAGE_0002', 0, NULL, NULL, 0, NULL, NULL, 6, '4', 34, 2, 2, 3, '2013-05-04', 'OUI', '2013-05-13', '0.00', 0),
(4, NULL, NULL, NULL, '2013-05-04', NULL, NULL, 0, NULL, NULL, NULL, '500000000.00', NULL, 'Appel d''Offres Ouvert en deux etapes', NULL, 'T_DAGE_0004', 0, NULL, NULL, 0, NULL, NULL, 6, '4', 3, 2, 4, 3, NULL, 'OUI', '2013-05-13', '0.00', 0),
(5, NULL, NULL, NULL, '2013-05-04', NULL, NULL, 0, NULL, NULL, NULL, '40000000.00', NULL, 'Demande renseignement prix', NULL, 'PI_DAGE_0014', 0, NULL, NULL, 0, NULL, NULL, 6, '4', 34, 1, 14, 4, '2013-05-04', 'OUI', '2013-05-13', '0.00', 0),
(6, NULL, NULL, NULL, '2013-05-04', NULL, NULL, 0, NULL, NULL, NULL, '6000000000.00', NULL, 'Manifestation d''interet', NULL, 'PI_DAGE_0013', 0, NULL, NULL, 0, NULL, NULL, 6, '4', 42, 1, 13, 4, '2013-05-04', 'OUI', '2013-05-13', '0.00', 0),
(7, NULL, NULL, NULL, '2013-05-05', NULL, NULL, 0, NULL, NULL, NULL, '5000000000.00', NULL, 'Appel d''offre ouvert', NULL, 'F_DAGE_0015', 0, NULL, NULL, 0, NULL, NULL, 6, '4', 1, 2, 15, 1, '2013-05-05', 'OUI', '2013-05-13', '1200000.00', 0),
(8, NULL, NULL, NULL, '2013-05-05', NULL, NULL, 0, NULL, NULL, NULL, '500000000.00', NULL, 'DRP', NULL, 'F_DAGE_0016', 0, NULL, NULL, 0, NULL, NULL, 6, '4', 34, 2, 16, 1, '2013-05-05', 'OUI', '2013-05-13', '0.00', 0),
(9, NULL, NULL, NULL, '2013-05-05', NULL, NULL, 0, NULL, NULL, NULL, '400000000.00', NULL, 'Appel d''offre ouvert', NULL, 'S_DAGE_0019', 0, NULL, NULL, 0, NULL, NULL, 6, '4', 1, 2, 19, 2, '2013-05-05', 'OUI', '2013-05-13', '15000000.00', 0),
(10, NULL, NULL, NULL, '2013-05-05', NULL, NULL, 0, NULL, NULL, NULL, '50000000.00', NULL, 'DRP', NULL, 'S_DAGE_0020', 0, NULL, NULL, 0, NULL, NULL, 6, '4', 34, 2, 20, 2, '2013-05-05', 'OUI', '2013-05-13', '0.00', 0),
(11, NULL, NULL, NULL, '2013-05-06', NULL, NULL, 0, NULL, NULL, NULL, '500000000.00', NULL, 'Travaux', NULL, 'T_DAGE_0026', 0, NULL, NULL, 0, NULL, NULL, 902, '4', 1, 2, 26, 3, '2013-05-07', 'NON', '2013-05-07', '100000.00', 0),
(12, NULL, NULL, NULL, '2013-05-08', NULL, NULL, 0, NULL, NULL, NULL, '12000000.00', NULL, 'DSP', NULL, 'T_DAGE_0029', 0, NULL, NULL, 0, NULL, NULL, 902, '4', 34, 2, 29, 3, NULL, 'NON', NULL, '0.00', 0),
(13, NULL, NULL, NULL, '2013-05-08', NULL, NULL, 0, NULL, NULL, NULL, '600000000.00', NULL, 'ddd', NULL, 'T_DAGE_0047', 0, NULL, NULL, 0, NULL, NULL, 902, '4', 1, 2, 47, 3, '2013-05-08', 'NON', NULL, '200000.00', 0),
(14, NULL, NULL, NULL, '2013-05-08', NULL, NULL, 0, NULL, NULL, NULL, '500000000.00', NULL, 'PI', NULL, 'PI_DAGE_0034', 0, NULL, NULL, 0, NULL, NULL, 902, '4', 42, 1, 34, 4, NULL, 'NON', NULL, '0.00', 0),
(15, NULL, NULL, NULL, '2013-05-08', NULL, NULL, 0, NULL, NULL, NULL, '500000000.00', NULL, 'Construction du si�ge de la DNCMP', NULL, 'T_DNCMP_0038', 0, NULL, NULL, 0, NULL, NULL, 9, '4', 1, 2, 38, 3, '2013-05-09', 'OUI', '2013-05-13', '200000.00', 0),
(16, NULL, NULL, NULL, '2013-05-09', NULL, NULL, 0, NULL, NULL, NULL, '700000000.00', NULL, 'PI', NULL, 'PI_DAGE_0048', 0, NULL, NULL, 0, NULL, NULL, 902, '4', 42, 1, 48, 4, '2013-05-10', 'NON', NULL, '0.00', 0),
(17, NULL, NULL, NULL, '2013-05-10', NULL, NULL, 0, NULL, NULL, NULL, '800000000.00', NULL, 'Etude d''impact environnemental', NULL, 'PI_DER_0051', 0, NULL, NULL, 0, NULL, NULL, 905, '2', 42, 1, 52, 4, NULL, 'OUI', '2013-05-13', '0.00', 0),
(18, NULL, NULL, NULL, '2013-05-10', NULL, NULL, 0, NULL, NULL, NULL, '200000000.00', NULL, 'AOO', NULL, 'F_DAGE_0031', 0, NULL, NULL, 0, NULL, NULL, 902, '4', 5, 2, 31, 1, NULL, 'NON', NULL, '0.00', 0),
(19, NULL, NULL, NULL, '2013-05-10', NULL, NULL, 0, NULL, NULL, NULL, '500000000.00', NULL, 'Travaux d''entretien de la route', NULL, 'T_DER_0049', 0, NULL, NULL, 0, NULL, NULL, 905, '4', 5, 2, 49, 3, NULL, 'OUI', '2013-05-13', '0.00', 0);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_arretes`
--

CREATE TABLE IF NOT EXISTS `pmb_arretes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `arretNum` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `arretObjet` text COLLATE latin1_general_ci,
  `Autorite_Id` int(11) DEFAULT NULL,
  `arreteTitulaire` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `arretPub` int(2) DEFAULT '0',
  `arretText` text COLLATE latin1_general_ci,
  `arreteFichier` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_arretes`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_arretesmembrescommissionmarche`
--

CREATE TABLE IF NOT EXISTS `pmb_arretesmembrescommissionmarche` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `datemembre` date DEFAULT NULL,
  `fichier` varchar(255) DEFAULT NULL,
  `fichiermembre` varchar(255) DEFAULT NULL,
  `gestion` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `referencemembre` varchar(255) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `pmb_arretesmembrescommissionmarche`
--

INSERT INTO `pmb_arretesmembrescommissionmarche` (`ID`, `date`, `datemembre`, `fichier`, `fichiermembre`, `gestion`, `reference`, `referencemembre`, `Autorite_ID`) VALUES
(1, '2013-04-28', '2013-04-28', '18111028042013_Pj_02-ECBU.pdf', '53111028042013_Pj_02-ECBU.pdf', '2013', '324', 'dd', 6),
(2, '2013-05-04', '2013-05-04', '5032104052013_Pj_adresse.rtf', '5732104052013_Pj_adresse.rtf', '2014', 'drrr', 'rrr', 6),
(3, '2013-05-07', '2013-05-07', '2101707052013_Pj_recu.pdf', '0701707052013_Pj_modele.doc', '2013', 'ss', 'sss', 902),
(4, '2013-05-07', '2013-05-07', '5342407052013_Pj_recu.pdf', '5942407052013_Pj_recu.pdf', '2013', '32', 'REF', 9),
(5, '2013-05-03', '2013-05-10', '31551010052013_Pj_table.rtf', '46551010052013_Pj_recu.pdf', '2013', '235-', '567', 905);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_attributions`
--

CREATE TABLE IF NOT EXISTS `pmb_attributions` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `AttributaireProvisoire` varchar(255) DEFAULT NULL,
  `Commentaire` varchar(255) DEFAULT NULL,
  `commentairedefinitif` varchar(255) DEFAULT NULL,
  `DatePublicationDefinitive` date DEFAULT NULL,
  `DatePublicationProvisoire` date DEFAULT NULL,
  `JoursExecution` int(11) DEFAULT NULL,
  `MoisExecution` int(11) DEFAULT NULL,
  `MontantMarche` decimal(19,2) DEFAULT NULL,
  `montantdefinitif` decimal(19,2) DEFAULT NULL,
  `nomFichierDef` varchar(255) DEFAULT NULL,
  `ReferenceAvisGeneral` varchar(50) DEFAULT NULL,
  `ReferencePlandePassation` varchar(255) DEFAULT NULL,
  `SemaineExecution` int(11) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `Pliouverture_ID` bigint(20) DEFAULT NULL,
  `dateattribution` date DEFAULT NULL,
  `Lot_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKAF2E67CE21E224C0` (`Dossiers_ID`),
  KEY `FKAF2E67CE57A27964` (`Pliouverture_ID`),
  KEY `FKAF2E67CEB5246B11` (`Lot_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Contenu de la table `pmb_attributions`
--

INSERT INTO `pmb_attributions` (`ID`, `AttributaireProvisoire`, `Commentaire`, `commentairedefinitif`, `DatePublicationDefinitive`, `DatePublicationProvisoire`, `JoursExecution`, `MoisExecution`, `MontantMarche`, `montantdefinitif`, `nomFichierDef`, `ReferenceAvisGeneral`, `ReferencePlandePassation`, `SemaineExecution`, `Dossiers_ID`, `Pliouverture_ID`, `dateattribution`, `Lot_ID`) VALUES
(1, '2SI(solutions et strat�gies informatique)', '', NULL, '2013-04-29', '2013-04-29', 20, 24, '450000000.00', '450000000.00', NULL, NULL, NULL, 3, 1, 1, '2013-04-29', 1),
(2, '2SI(solutions et strat�gies informatique)', '', NULL, '2013-05-04', '2013-05-04', 5, 24, '500000000.00', '500000000.00', NULL, NULL, NULL, 3, 3, 4, '2013-05-04', 2),
(4, NULL, '', NULL, '2013-05-04', '2013-05-04', 15, 12, '6000000000.00', '6000000000.00', NULL, NULL, NULL, 3, 5, 8, '2013-05-04', NULL),
(5, NULL, '', NULL, '2013-05-04', '2013-05-04', 10, 12, '40000000.00', '40000000.00', NULL, NULL, NULL, 3, 9, 10, '2013-05-04', NULL),
(6, '2SI(solutions et strat�gies informatique)', '', NULL, '2013-05-05', '2013-05-05', 2, 2, '5000000000.00', '5000000000.00', NULL, NULL, NULL, 2, 10, 12, '2013-05-05', 3),
(7, '2SI(solutions et strat�gies informatique)', '', NULL, '2013-05-05', '2013-05-05', 2, 3, '500000000.00', '500000000.00', NULL, NULL, NULL, 2, 11, 15, '2013-05-05', 4),
(8, '2SI(solutions et strat�gies informatique)', '', NULL, '2013-05-05', '2013-05-05', 2, 2, '400000000.00', '400000000.00', NULL, NULL, NULL, 2, 12, 18, '2013-05-05', 5),
(9, '2SI(solutions et strat�gies informatique)', '', NULL, '2013-05-05', '2013-05-05', 2, 2, '50000000.00', '50000000.00', NULL, NULL, NULL, 2, 13, 21, '2013-05-05', 7),
(10, 'Entreprise  TFC (Travaux-Fournitures-con)', '', NULL, '2013-05-05', '2013-05-05', 2, 3, '9000000.00', '9000000.00', NULL, NULL, NULL, 2, 13, 23, '2013-05-05', 6),
(11, '2SI(solutions et strat�gies informatique)', 'dd', NULL, NULL, NULL, 2, 12, '10000000.00', '10000000.00', NULL, NULL, NULL, 3, 14, 24, '2013-05-08', 8),
(12, '2SI(solutions et strat�gies informatique)', 'ddd', NULL, '2013-05-08', '2013-05-08', 2, 12, '600000000.00', '600000000.00', NULL, NULL, NULL, 3, 15, 27, '2013-05-08', 9),
(13, '2SI(solutions et strat�gies informatique)', '', NULL, '2013-05-09', '2013-05-09', 2, 12, '200000000.00', '200000000.00', NULL, NULL, NULL, 3, 18, 34, '2013-05-09', 10),
(14, 'Neurotech', '', NULL, '2013-05-09', '2013-05-09', 4, 12, '95000000.00', '95000000.00', NULL, NULL, NULL, 3, 18, 36, '2013-05-09', 11),
(15, NULL, '', NULL, NULL, NULL, 2, 12, '500000000.00', '500000000.00', NULL, NULL, NULL, 3, 20, 43, '2013-05-10', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_audit`
--

CREATE TABLE IF NOT EXISTS `pmb_audit` (
  `idaudit` bigint(20) NOT NULL AUTO_INCREMENT,
  `datestatut` date DEFAULT NULL,
  `gestion` int(11) DEFAULT NULL,
  `libelleaudit` varchar(255) DEFAULT NULL,
  `statut` varchar(255) DEFAULT NULL,
  `STA_CODE` varchar(200) DEFAULT NULL,
  `nombreautorite` int(11) DEFAULT NULL,
  `nombreprestataire` int(11) DEFAULT NULL,
  PRIMARY KEY (`idaudit`),
  KEY `FK9D5921F805BFDC` (`STA_CODE`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `pmb_audit`
--

INSERT INTO `pmb_audit` (`idaudit`, `datestatut`, `gestion`, `libelleaudit`, `statut`, `STA_CODE`, `nombreautorite`, `nombreprestataire`) VALUES
(1, '2013-05-03', 2012, 'Audit 2012', 'Saisi', 'INIAUDIT_STATE', 5, 2),
(2, '2013-05-03', 2012, 'ddd', 'Saisi', 'INIAUDIT_STATE', 3, 1),
(3, '2013-05-08', 2012, 'Formation\n', 'Saisi', 'INIAUDIT_STATE', 3, 1),
(4, '2013-05-12', 2011, 'Audit 2011', 'Saisi', 'INIAUDIT_STATE', 5, 2),
(5, '2013-05-13', 2012, 'Audits 2011', 'Saisi', 'INIAUDIT_STATE', 4, 2),
(6, '2013-05-25', 2012, 'Audit 2012', 'Saisi', 'INIAUDIT_STATE', 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_auditpiecejointe`
--

CREATE TABLE IF NOT EXISTS `pmb_auditpiecejointe` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Date` date DEFAULT NULL,
  `Fichier` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `reference` varchar(50) DEFAULT NULL,
  `audit` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK94B205E8DF43B570` (`audit`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `pmb_auditpiecejointe`
--

INSERT INTO `pmb_auditpiecejointe` (`id`, `Date`, `Fichier`, `libelle`, `reference`, `audit`) VALUES
(1, '2013-05-01', '3822708052013_Pj_table.rtf', NULL, NULL, NULL),
(2, '2013-05-11', '39171011052013_Pj_recu.pdf', NULL, NULL, 4);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_auditsprestac`
--

CREATE TABLE IF NOT EXISTS `pmb_auditsprestac` (
  `idpaac` bigint(20) NOT NULL AUTO_INCREMENT,
  `datedebutprevu` date DEFAULT NULL,
  `datedebutreel` date DEFAULT NULL,
  `dateenvoirapprov` date DEFAULT NULL,
  `datefinprevu` date DEFAULT NULL,
  `datefinreel` date DEFAULT NULL,
  `datepublicrapfin` date DEFAULT NULL,
  `daterapportfinal` date DEFAULT NULL,
  `daterapportprov` date DEFAULT NULL,
  `daterecepoberservation` date DEFAULT NULL,
  `nomfichierobs` varchar(255) DEFAULT NULL,
  `nomfichierrapfin` varchar(255) DEFAULT NULL,
  `nomfichierrapprov` varchar(255) DEFAULT NULL,
  `audit` bigint(20) DEFAULT NULL,
  `id` bigint(20) DEFAULT NULL,
  `prestataire` bigint(20) DEFAULT NULL,
  `CourrierAudits` bigint(20) DEFAULT NULL,
  `autoriteID` bigint(20) DEFAULT NULL,
  `ContratsPrestataires` bigint(20) DEFAULT NULL,
  `nomfichier2obs` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idpaac`),
  KEY `FK92357FB4A2096A91` (`prestataire`),
  KEY `FK92357FB4DF43B570` (`audit`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Contenu de la table `pmb_auditsprestac`
--

INSERT INTO `pmb_auditsprestac` (`idpaac`, `datedebutprevu`, `datedebutreel`, `dateenvoirapprov`, `datefinprevu`, `datefinreel`, `datepublicrapfin`, `daterapportfinal`, `daterapportprov`, `daterecepoberservation`, `nomfichierobs`, `nomfichierrapfin`, `nomfichierrapprov`, `audit`, `id`, `prestataire`, `CourrierAudits`, `autoriteID`, `ContratsPrestataires`, `nomfichier2obs`) VALUES
(1, '2013-05-03', '2013-05-04', '2013-05-10', '2013-05-10', '2013-05-10', '2013-05-03', '2013-05-03', '2013-05-03', '2013-05-03', '31261003052013_Pj_adresse.rtf', '07341003052013_Pj_adresse.rtf', '2153903052013_Pj_adresse.rtf', 1, NULL, NULL, NULL, 9, 1, NULL),
(9, '2013-05-04', '2013-05-04', '2013-05-04', '2013-05-10', '2013-05-11', '2013-05-04', '2013-05-04', '2013-05-04', '2013-05-04', '4741904052013_Pj_adresse.rtf', '2148904052013_Pj_adresse.rtf', '1041904052013_Pj_adresse.rtf', 1, NULL, NULL, NULL, 5, 2, NULL),
(10, '2013-05-04', '2013-05-04', '2013-05-04', '2013-05-04', '2013-05-04', '2013-05-04', '2013-05-04', '2013-05-04', '2013-05-04', '21101004052013_Pj_adresse.rtf', '46041004052013_Pj_adresse.rtf', '07101004052013_Pj_adresse.rtf', 1, NULL, NULL, NULL, 6, 2, NULL),
(11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 911, NULL, NULL),
(12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 912, 1, NULL),
(13, '2013-05-08', '2013-05-09', '2013-05-08', '2013-05-09', '2013-05-11', '2013-05-08', '2013-05-08', '2013-05-08', '2013-05-08', '2133708052013_Pj_recu.pdf', '3433708052013_Pj_recu.pdf', '0733708052013_Pj_recu.pdf', 3, NULL, NULL, NULL, 1, 3, NULL),
(14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, 3, 3, NULL),
(15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, 4, 3, NULL),
(16, '2013-05-11', '2013-05-12', '2013-05-11', '2013-05-18', '2013-05-18', '2013-05-11', '2013-05-18', '2013-05-11', '2013-05-11', '00151011052013_Pj_recu.pdf', '17171011052013_Pj_recu.pdf', '41141011052013_Pj_recu.pdf', 4, NULL, NULL, NULL, 5, 7, NULL),
(17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, 4, 5, NULL),
(18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, 6, 5, NULL),
(19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, 940, 5, NULL),
(20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, 908, 7, NULL),
(21, '2013-05-13', '2013-05-14', '2013-05-13', '2013-05-29', '2013-05-30', '2013-05-13', '2013-05-13', '2013-05-13', '2013-05-13', '4152513052013_Pj_recu.pdf', '1654513052013_Pj_recu.pdf', '0651513052013_Pj_table.rtf', 5, NULL, NULL, NULL, 905, 8, NULL),
(22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, 1, 8, NULL),
(23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, 4, 9, NULL),
(24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, 908, 9, NULL),
(25, '2013-05-26', '2013-05-26', '2013-05-26', '2013-05-26', '2013-05-26', '2013-05-26', '2013-05-26', '2013-05-19', '2013-05-26', '2407026052013_Pj_recu.pdf', '4007026052013_Pj_recu.pdf', '3706026052013_Pj_indicateur.rtf', 6, NULL, NULL, NULL, 1, 11, ''),
(26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, 6, 10, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_autoritecontractante`
--

CREATE TABLE IF NOT EXISTS `pmb_autoritecontractante` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `odreAutoriteContractante` int(11) DEFAULT '0',
  `denomination` tinytext COLLATE latin1_general_ci,
  `responsable` tinytext COLLATE latin1_general_ci,
  `photoResponsable` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `adresse` tinytext COLLATE latin1_general_ci,
  `telephone` varchar(15) COLLATE latin1_general_ci DEFAULT '',
  `fax` varchar(50) COLLATE latin1_general_ci DEFAULT '',
  `email` varchar(100) COLLATE latin1_general_ci DEFAULT '',
  `urlsiteweb` tinytext COLLATE latin1_general_ci,
  `typeautorite_ID` int(10) DEFAULT '0',
  `sigle` tinytext COLLATE latin1_general_ci,
  `logo` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `compteurMarche` int(11) DEFAULT '0' COMMENT 'umerotation du marche ',
  `compteurAvis` int(11) DEFAULT '0' COMMENT 'numerotation des avis',
  `valeurdelaicomplementpieces` int(5) DEFAULT '0' COMMENT 'pour completer pieces admin',
  `unitedelaicomplementpieces` varchar(20) COLLATE latin1_general_ci DEFAULT '',
  `flagTache` int(1) DEFAULT '0',
  `polId` bigint(20) DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `FK3F4838014FFE3A50` (`typeautorite_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=941 ;

--
-- Contenu de la table `pmb_autoritecontractante`
--

INSERT INTO `pmb_autoritecontractante` (`ID`, `odreAutoriteContractante`, `denomination`, `responsable`, `photoResponsable`, `adresse`, `telephone`, `fax`, `email`, `urlsiteweb`, `typeautorite_ID`, `sigle`, `logo`, `compteurMarche`, `compteurAvis`, `valeurdelaicomplementpieces`, `unitedelaicomplementpieces`, `flagTache`, `polId`) VALUES
(1, 1, 'Minist�re de l�Eau, de l�Assainissement et de l�Hydraulique Villageoise', '', NULL, '', '', '', '', '', 1, 'MEAHV', '', 1, 3, 0, '', 0, 1),
(3, 2, 'Minist�re d�l�gu� aupr�s du Minist�re de l�Agriculture, de l�Elevage et de la P�che, charg� des Infrastructures Rurales', 'Ibrahima CISSOKHO', NULL, 'Rue 110 x G zone B Dakar', '338593848', '338250774', 'minsports@hotmail.fr', 'www.sports.gouv.sn', 1, 'MDMAEPIR', '', 0, 1, 0, 'jour', 1, 1),
(4, 3, 'Minist�re de l�Agriculture, de l�Elevage et de la P�che ', '', NULL, 'Bulding administratif 8e �tage pi�ce 825\r\nBP 4041 Dakar.\r\n', 'T�l : 33 849 73', '', 'mfacelmarche@yahoo.fr', '', 1, 'MAEP', '', 0, 0, 0, 'jour', 1, 1),
(5, 4, 'Minist�re des Arts et Culture', '', '', '', '', '', '', '', 1, 'MAC', '', 0, 10, 0, '', 0, 1),
(6, 5, 'Minist�re de la Fonction Publique et des R�formes Administratives ', 'Monsieur Mamadou DIA', '463717303472619473938a', 'Boulevard Martin luther king X Dardanelles ', '338423592', '338423591', 'dageme@orange.sn', 'education.gouv.sn', 1, 'MFPRA', '42158080046b9fb800f428.png', 1, 54, 3, 'jour', 1, 1),
(7, 6, 'Pr�sidence de la R�publique', '', NULL, 'AA', '33 7676878', '', 'dcmp@minfinances.sn', '', 1, 'PR', '', 0, 1, 0, '', 0, 1),
(8, 7, 'Premier Minist�re', 'Diakaria DIAW', '11387515954b83c4842dbe0.doc', 'Building administratif, 1er �tage, avenue LS Senghor', '33 829 62 01', '33 842 32 46', 'mfpetop@gouv.sn', 'mfpetop@gouv.sn', 1, 'PM', '12719968904b83c4842cc30.doc', 0, 2, 5, 'jour', 1, 1),
(9, 8, 'Minist�re de l��conomie et des finances', '', NULL, 'aa', '11', '', 'alpha@alpha.sn', '', 1, 'MEF', '', 0, 0, 0, '', 0, 1),
(11, 9, 'Minist�re du Commerce et de la Promotion du Secteur Priv�', 'Ibrahima Diallo', NULL, 'Si�ge du Minist�re de la Sant� et de la Pr�vention, Direction de la Sant�, Rue Aim� C�saire, Fann R�sidence, Dakar', '33 869 42 97', '33 869 42 06', 'ibdiallo76@yahoo.fr', 'www.minsante@gouv.sn', 1, 'MCPSP', '', 0, 24, 0, 'jour', 1, 1),
(12, 10, 'Minist�re de l�Enseignement Technique et de la Formation  Professionnelle ', 'Amadou B�ye NDIAYE', NULL, 'Hann Mariste 2', '338691541', '', 'beyendiaye@gmail.com', '', 1, 'METFP', '', 0, 0, 0, 'jour', 1, 1),
(13, 11, 'Minist�re du D�veloppement � la Base, de l�Artisanat, de la Jeunesse et de l�Emploi des Jeunes', 'MME BINETA GUEYE TRAORE', NULL, '91 SOTRAC MERMOZ/ROUTE DE OUAKAM', '338651975', '', 'mepn@environnement.gouv.sn', 'www.environnement.gouv.sn', 1, 'MDBAJEJ', '', 1, 2, 0, 'jour', 1, 1),
(14, 12, 'Minist�re de la Promotion de la Femme ', 'Biram SARR', NULL, '28, avenue L�opold S�dar Senghor Dakar', '338220871', '338232582', 'sageculture@yahoo.fr', 'http://www.culture.gouv.sn', 1, 'MPF', '', 0, 0, 0, 'jour', 1, 1),
(15, 13, 'Minist�re de l�Action Sociale et de la Solidarit� Nationale', '', NULL, 'aa', '33 7676878', '', 'dcmp@minfinances.sn', '', 1, 'MASSN', '', 0, 0, 0, '', 0, 1),
(18, 14, 'Minist�re des Enseignements Primaire et Secondaire et de l�Alphab�tisation', '', NULL, 'Building Administratif', '338491840', '', 'karimdiouf80@yahoo.fr', '', 1, 'MEPSA', '', 0, 2, 0, 'jour', 1, 1),
(20, 15, 'Minist�re des Droits de l�Homme et de la Consolidation de la D�mocratie et de la Formation Civique', '', NULL, 'EEE', '333', 'AZEAEZ', 'dcmp@minfinances.sn', '', 1, 'MDHCDFC', '', 0, 0, 0, 'jour', 1, 1),
(24, 16, 'Minist�re des Sports et Loisirs ', '', NULL, '', '', '', '', '', 1, 'MSL', '', 0, 0, 0, '', 0, 1),
(26, 17, 'Minist�re de la S�curit� et de la Protection Civile', '', '', 'Point E', 'Point E', 'Point E', 'boubacar.mbodj@gmail.com', '', 1, 'MSPC', '', 0, 0, 0, '', 0, 1),
(29, 18, 'Minist�re du Tourisme', '', NULL, '', '', '', '', '', 1, 'MT', '', 4, 20, 0, '', 0, 1),
(2, 19, 'Minist�re de la Justice charg�e des relations avec les Institutions de la R�publique', 'Adama Mboup', '14537650174a7052f5159c7.png', 'Cellule de passation de march�s \r\nrue Carde - Immeuble Peytavin \r\n(8�me �tage ) BP 4017 - Dakar-S�n�gal', '+221338213985', '+221338213984', 'cpmarches@minfinances.sn', 'http://www.finances.gouv.sn/', 1, 'MJCRIR', '14537650174a7052f5159c7.png', 2503, 21, 0, 'jour', 1, 1),
(413, 20, 'Minist�re aupr�s du Pr�sident de la R�publique, charg� de la Planification du D�veloppement et de l�Am�nagement du Territoire', '', NULL, 'aaa', '4444', '', 'dcmp@minfinances.sn', '', 1, 'MCPDAT', '', 0, 0, 0, '', 0, 1),
(89, 21, 'Minist�re de l�Administration Territoriale, de la D�centralisation et des Collectivit�s Locales ', 'fatou gaye sarr', NULL, 'Building Administratif 3� Etage DAKAR/SENEGAL', '33 8233101/3384', '338233268', 'dage_agri@yahoo.fr', '', 1, 'MATDCL', '', 0, 2, 0, 'jour', 1, 1),
(90, 22, 'Minist�re de Travail, de l�Emploi et de la S�curit� Sociale', '', NULL, 'Point E', '338542131', '338542131', 'boubacar.mbodj@gmail.com', '', 1, 'MTESS', '', 0, 0, 0, '', 0, 1),
(92, 23, 'Minist�re de l�enseignement sup�rieur et de la recherche', '', NULL, 'Point E', '338541255', '338541255', 'boubacar.mbodj@gmail.com', '', 1, 'MESR', '', 0, 0, 0, 'jour', 1, 1),
(93, 24, 'Minist�re de l�Industrie, de la Zone Franche et des Innovation Technologiques', '', NULL, 'Point E', '338541255', '', 'boubacar.mbodj@gmail.com', '', 1, 'MIZFIT', '', 0, 0, 0, 'jour', 1, 1),
(94, 25, 'Minist�re des Affaires Etrang�res et de la Coop�ration ', 'Oumar GAYE', '17448697849c2219acba58.doc', '23 rue du Docteur Calmette', '338217736', '338229414', 'gayomar66@yahoo.fr', 'www.senex.sn', 1, 'MAEC', '17448697849c2219acba58.doc', 0, 1, 2, 'jour', 1, 1),
(95, 26, 'Minist�re de la sant� ', 'Monsieur Boubacar BA, Secr�taire g�n�ral, Coordonnateur de la Cellule de passation des march�s', NULL, 'Building administratif, quatri�me �tage, Dakar', '33 849 50 74 ', '33 823 87 20', 'boubacar.ba@yahoo.fr', '', 1, 'MS', '', 0, 0, 0, 'jour', 1, 1),
(902, 30, 'Minist�re de la communication', '', NULL, '', '', '', '', '', 1, 'MC', '', 0, 0, 0, '', 0, 1),
(96, 27, 'Minist�re de la d�fense et des anciens combattants', 'Jean Pierre MENDY', NULL, '122,bis Avenue Andr� Peytavin BP 4037 Dakar R�publique du S�n�gal', '33 889 57 17', '33 822 55 94', 'jpierremendy@yahoo.fr', '', 1, 'MDAC', '', 0, 1, 0, 'jour', 1, 1),
(97, 28, 'Minist�re des Transports', 'Malick DIALLO ', NULL, 'Bulding Administratif 5�me Etage', '338229597', '338229597', 'bambatuba2003@yahoo.fr', '', 1, 'MTP', '', 0, 0, 0, 'jour', 1, 1),
(719, 29, 'Minist�re de l�urbanisme et de l�habitat', '', '', 'Point', '338245121', '', 'boubacar.mbodj@gmail.com', '', 1, 'MUH', '', 0, 1, 0, 'jour', 1, 1),
(454, 31, 'Minist�re des mines et de l��nergie', '', NULL, '', '', '', '', '', 1, 'MME', '', 0, 0, 0, '', 0, 1),
(904, 31, 'Minist�re de l�environnement des ressources foresti�res', '', '', '', '', '', '', '', 1, 'MERF', '', 0, 0, 0, '', 0, 1),
(905, 33, 'Minist�re des travaux publics', '', '', '', '', '', '', '', 1, 'MTP', '', 0, 0, 0, '', 0, 1),
(906, 34, 'Minist�re des postes et t�l�communications', '', '', '', '', '', '', '', 1, 'MPT', '', 0, 0, 0, '', 0, 1),
(908, 35, 'Port autonome de Lom�', '', NULL, '', '', '', '', '', 2, 'PAL', NULL, 0, 0, 0, '', 0, NULL),
(909, 36, 'Soci�t� A�roportuaire de Lom�-Tokoin ', '', NULL, '', '', '', '', '', 2, 'SALT', NULL, 0, 0, 0, '', 0, NULL),
(910, 37, 'Nouvelle Soci�t� Cotonni�re du Togo ', '', NULL, '', '', '', '', '', 2, 'NSCT', NULL, 0, 0, 0, '', 0, NULL),
(911, 38, 'Soci�t� nouvelles des phosphates du Togo ', '', NULL, '', '', '', '', '', 2, 'SNPT', NULL, 0, 0, 0, '', 0, NULL),
(912, 39, 'LONATO', '', NULL, '', '', '', '', '', 2, 'LONATO', NULL, 0, 0, 0, '', 0, NULL),
(913, 40, 'TOGO TELECOM', '', NULL, '', '', '', '', '', 2, 'TOGOTELECOM', NULL, 0, 0, 0, '', 0, NULL),
(914, 41, 'CEET', '', NULL, '', '', '', '', '', 2, 'CEET', NULL, 0, 0, 0, '', 0, NULL),
(915, 42, 'Chambre de Commerce et d�Industrie du Togo ', '', NULL, '', '', '', '', '', 2, 'CCIT', NULL, 0, 0, 0, '', 0, NULL),
(916, 43, 'CAMEG', '', NULL, '', '', '', '', '', 2, 'CAMEG', NULL, 0, 0, 0, '', 0, NULL),
(917, 44, 'TOGO CELLULAIRE ', '', NULL, '', '', '', '', '', 2, 'TOGOCEL', NULL, 0, 0, 0, '', 0, NULL),
(918, 45, 'Soci�t� d�administration de la zone franche ', '', NULL, '', '', '', '', '', 2, 'SAZF', NULL, 0, 0, 0, '', 0, NULL),
(919, 46, 'Togolaise des Eaux', '', NULL, '', '', '', '', '', 2, 'TOGEAU', NULL, 0, 0, 0, '', 0, NULL),
(920, 47, 'Cour supr�me ', '', NULL, '', '', '', '', '', 3, 'CSUP', NULL, 0, 0, 0, '', 0, NULL),
(921, 48, 'Autorit� de R�gulation des March�s Publics', '', NULL, '', '', '', '', '', 3, 'ARMP', NULL, 0, 0, 0, '', 0, NULL),
(922, 49, 'Autorit� de R�glementation du Secteurs des Postes et T�l�communications ', '', NULL, '', '', '', '', '', 3, 'ARSPT', NULL, 0, 0, 0, '', 0, NULL),
(923, 50, 'Agence Nationale de Gestion de l�Environnement ', '', NULL, '', '', '', '', '', 3, 'ANGE', NULL, 0, 0, 0, '', 0, NULL),
(924, 51, 'Haute Autorit� de l�Audiovisuel et de la Communication ', '', NULL, '', '', '', '', '', 3, 'HAAC', NULL, 0, 0, 0, '', 0, NULL),
(925, 52, 'Autorit� de r�glementation du secteur de l��lectricit�', '', NULL, '', '', '', '', '', 3, 'ARSE', NULL, 0, 0, 0, '', 0, NULL),
(926, 53, 'Cour des comptes', '', NULL, '', '', '', '', '', 3, 'COURSUP', NULL, 0, 0, 0, '', 0, NULL),
(927, 54, 'Cour Constitutionnelle', '', NULL, '', '', '', '', '', 3, 'COURCONST', NULL, 0, 0, 0, '', 0, NULL),
(928, 55, 'Universit� de Lom�', '', NULL, '', '', '', '', '', 3, 'UNIVLOME', NULL, 0, 0, 0, '', 0, NULL),
(929, 56, 'Caisse de Retraite du Togo ', '', NULL, '', '', '', '', '', 3, 'CRT', NULL, 0, 0, 0, '', 0, NULL),
(930, 57, 'D�l�gation sp�ciale de la commune de Lom�', '', NULL, '', '', '', '', '', 3, 'DSCL', NULL, 0, 0, 0, '', 0, NULL),
(931, 58, 'Universit� de Kara', '', NULL, '', '', '', '', '', 3, 'UNIVKARA', NULL, 0, 0, 0, '', 0, NULL),
(932, 59, 'Agence Nationale Pour l�Emploi', '', NULL, '', '', '', '', '', 3, 'AGPE', NULL, 0, 0, 0, '', 0, NULL),
(933, 60, 'Conseil National des Chargeurs du Togo', '', NULL, '', '', '', '', '', 3, 'CNCT', NULL, 0, 0, 0, '', 0, NULL),
(934, 61, 'Pr�fecture du Golfe', '', NULL, '', '', '', '', '', 3, 'PREFGOLFE', NULL, 0, 0, 0, '', 0, NULL),
(935, 62, 'Direction G�n�rale de l�Office du Baccalaur�at', '', NULL, '', '', '', '', '', 3, 'DGOB', NULL, 0, 0, 0, '', 0, NULL),
(936, 63, 'D�l�gation Sp�ciale de la Pr�fecture du Golfe', '', NULL, '', '', '', '', '', 3, 'DSPG', NULL, 0, 0, 0, '', 0, NULL),
(937, 64, 'Etablissement Publics d�Administration des March�s de Lom�', '', NULL, '', '', '', '', '', 3, 'EPAML', NULL, 0, 0, 0, '', 0, NULL),
(939, 300, 'Assembl�e nationale', '', NULL, 'Lom�', '6789', '', 'assemble@gmail.com', '', 5, 'AN', NULL, 0, 0, 0, '', 0, NULL),
(940, 288, 'Cour des comptes', '', NULL, 'Lom�', '67890', '', 'asamb@ssi.sn', '', 5, 'CC', NULL, 0, 0, 0, '', 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_avisattributions`
--

CREATE TABLE IF NOT EXISTS `pmb_avisattributions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `attriObjet` text COLLATE latin1_general_ci,
  `attriType` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `attritexte` longtext COLLATE latin1_general_ci,
  `attriRef` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `Autorite_Id` int(11) DEFAULT NULL,
  `attriPub` int(2) DEFAULT '0',
  `attriDate` date DEFAULT NULL,
  `IDDOSSIER` int(11) DEFAULT NULL,
  `attriRaisonsocial` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `attrifichier` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK86E1846D3475D85E` (`IDDOSSIER`),
  KEY `FK86E1846D4B9DC270` (`Autorite_Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=16 ;

--
-- Contenu de la table `pmb_avisattributions`
--

INSERT INTO `pmb_avisattributions` (`ID`, `attriObjet`, `attriType`, `attritexte`, `attriRef`, `Autorite_Id`, `attriPub`, `attriDate`, `IDDOSSIER`, `attriRaisonsocial`, `attrifichier`) VALUES
(1, 'Construction de route', 'provisoire', NULL, 'T_DAGE_0001', 6, 1, '2013-04-29', 1, '2SI(solutions et strat�gies informatique)', '3437829042013_Pj_adresse.rtf'),
(4, 'Manifestation d''interet', 'provisoire', NULL, 'PI_DAGE_0013', 6, 1, '2013-05-04', 5, '2SI(solutions et strat�gies informatique)', '1631304052013_Pj_adresse.rtf'),
(3, 'Demande de Renseignement et de Prix', 'provisoire', NULL, 'T_DAGE_0002', 6, 1, '2013-05-04', 3, '2SI(solutions et strat�gies informatique)', '52111104052013_Pj_adresse.rtf'),
(5, 'Demande renseignement prix', 'provisoire', NULL, 'PI_DAGE_0014', 6, 0, '2013-05-04', 9, 'A.T.I multim�dia', '54581004052013_Pj_adresse.rtf'),
(6, 'Appel d''offre ouvert', 'provisoire', NULL, 'F_DAGE_0015', 6, 1, '2013-05-05', 10, '2SI(solutions et strat�gies informatique)', '2942905052013_Pj_adresse.rtf'),
(7, 'DRP', 'provisoire', NULL, 'F_DAGE_0016', 6, 1, '2013-05-05', 11, '2SI(solutions et strat�gies informatique)', '42011005052013_Pj_adresse.rtf'),
(8, 'Appel d''offre ouvert', 'provisoire', NULL, 'S_DAGE_0019', 6, 1, '2013-05-05', 12, '2SI(solutions et strat�gies informatique)', '2245205052013_Pj_adresse.rtf'),
(9, 'DRP', 'provisoire', NULL, 'S_DAGE_0020', 6, 1, '2013-05-05', 13, '2SI(solutions et strat�gies informatique)', '3728305052013_Pj_adresse.rtf'),
(10, 'DRP', 'provisoire', NULL, 'S_DAGE_0020', 6, 1, '2013-05-05', 13, 'Entreprise  TFC (Travaux-Fournitures-con)', '3527305052013_Pj_adresse.rtf'),
(11, 'Travaux', 'provisoire', NULL, 'T_DAGE_0026', 902, 0, '2013-05-08', 14, '2SI(solutions et strat�gies informatique)', '4506808052013_Pj_recu.pdf'),
(12, 'ddd', 'provisoire', NULL, 'T_DAGE_0047', 902, 1, '2013-05-08', 15, '2SI(solutions et strat�gies informatique)', '0510908052013_Pj_recu.pdf'),
(13, 'Construction du si�ge de la DNCMP', 'provisoire', NULL, 'T_DNCMP_0038', 9, 1, '2013-05-09', 18, '2SI(solutions et strat�gies informatique)', '0457009052013_Pj_recu.pdf'),
(14, 'Construction du si�ge de la DNCMP', 'provisoire', NULL, 'T_DNCMP_0038', 9, 1, '2013-05-09', 18, 'Neurotech', '0457009052013_Pj_recu.pdf'),
(15, 'PI', 'provisoire', NULL, 'PI_DAGE_0048', 902, 0, '2013-05-10', 20, '2SI(solutions et strat�gies informatique)', '5037910052013_Pj_modele.doc');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_avisgeneral`
--

CREATE TABLE IF NOT EXISTS `pmb_avisgeneral` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `annee` varchar(255) DEFAULT NULL,
  `fichier_Avis` varchar(255) DEFAULT NULL,
  `Numero` varchar(50) DEFAULT NULL,
  `NumeroTelephone` varchar(255) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  `Adressecomplete` varchar(255) DEFAULT NULL,
  `BorderauAvis` varchar(255) DEFAULT NULL,
  `commentaireMiseEnvalidation` varchar(255) DEFAULT NULL,
  `commentaireValidation` varchar(255) DEFAULT NULL,
  `Commentaires` varchar(255) DEFAULT NULL,
  `DateAuPlusTard` date DEFAULT NULL,
  `dateCreation` date DEFAULT NULL,
  `dateMiseEnvalidation` date DEFAULT NULL,
  `dateRejet` date DEFAULT NULL,
  `dateValidation` date DEFAULT NULL,
  `Datepublication` date DEFAULT NULL,
  `etatValidAvis` varchar(255) DEFAULT NULL,
  `Etatmarches` int(11) DEFAULT NULL,
  `fichier_validation` varchar(255) DEFAULT NULL,
  `lastVersionValid` int(11) DEFAULT NULL,
  `motif` varchar(255) DEFAULT NULL,
  `NomAgentResponsable` varchar(255) DEFAULT NULL,
  `NomService` varchar(255) DEFAULT NULL,
  `NumeroTelecopieur` varchar(255) DEFAULT NULL,
  `PrenomResponsable` varchar(255) DEFAULT NULL,
  `texteAvisGeneral` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `InfoPlan_ID` bigint(20) DEFAULT NULL,
  `Etat` int(11) DEFAULT NULL,
  `contenu` varchar(50) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `publier` int(11) DEFAULT NULL,
  `resume` varchar(50) DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `soumelectron` int(3) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `pmb_avisgeneral`
--

INSERT INTO `pmb_avisgeneral` (`ID`, `annee`, `fichier_Avis`, `Numero`, `NumeroTelephone`, `Autorite_ID`, `Adressecomplete`, `BorderauAvis`, `commentaireMiseEnvalidation`, `commentaireValidation`, `Commentaires`, `DateAuPlusTard`, `dateCreation`, `dateMiseEnvalidation`, `dateRejet`, `dateValidation`, `Datepublication`, `etatValidAvis`, `Etatmarches`, `fichier_validation`, `lastVersionValid`, `motif`, `NomAgentResponsable`, `NomService`, `NumeroTelecopieur`, `PrenomResponsable`, `texteAvisGeneral`, `version`, `InfoPlan_ID`, `Etat`, `contenu`, `Date`, `publier`, `resume`, `Title`, `soumelectron`) VALUES
(4, '2013', '2824503052013_Pj_adresse.rtf', 'AG_MFPRA _ 2013', NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0),
(5, '2013', '5860608052013_Pj_recu.pdf', 'AG_MC _ 2013', NULL, 902, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0),
(6, '2013', '1941908052013_Pj_recu.pdf', 'AG_MEF _ 2013', NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-06-11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1),
(7, '2013', '1753908052013_Pj_Avis General.pdf', 'AG_CC _ 2013', NULL, 940, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_avisgeneral1`
--

CREATE TABLE IF NOT EXISTS `pmb_avisgeneral1` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `annee` varchar(255) DEFAULT NULL,
  `fichier_Avis` varchar(255) DEFAULT NULL,
  `Numero` varchar(50) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  `Etat` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_avisgeneral1`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_banque`
--

CREATE TABLE IF NOT EXISTS `pmb_banque` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  `sigle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `pmb_banque`
--

INSERT INTO `pmb_banque` (`id`, `libelle`, `sigle`) VALUES
(1, 'Banque Centrale des Etats de l''Afrique de l''Ouest', 'BCEAO'),
(2, 'Banque Internationale pour le Commerce et l''Industrie du Sénégal', 'BICIS');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_calendrierpaiement`
--

CREATE TABLE IF NOT EXISTS `pmb_calendrierpaiement` (
  `calID` bigint(20) NOT NULL AUTO_INCREMENT,
  `commentaire` varchar(255) DEFAULT NULL,
  `datepaiement` date DEFAULT NULL,
  `echeanceexpirer` varchar(255) DEFAULT NULL,
  `livable` varchar(255) DEFAULT NULL,
  `montant` double DEFAULT NULL,
  `payer` varchar(255) DEFAULT NULL,
  `taux` int(11) DEFAULT NULL,
  `contrat` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`calID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `pmb_calendrierpaiement`
--

INSERT INTO `pmb_calendrierpaiement` (`calID`, `commentaire`, `datepaiement`, `echeanceexpirer`, `livable`, `montant`, `payer`, `taux`, `contrat`) VALUES
(1, 'ddd', '2013-05-01', NULL, 'dd', 54000000, 'non', 12, 1);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_categoriedocument`
--

CREATE TABLE IF NOT EXISTS `pmb_categoriedocument` (
  `IDcatDoc` int(11) NOT NULL AUTO_INCREMENT,
  `niveauDoc` int(11) NOT NULL DEFAULT '0' COMMENT 'pour connaitre ki est le pere',
  `libelleCatDoc` longtext COLLATE latin1_general_ci NOT NULL,
  `nbTotalFichiersDoc` int(15) NOT NULL DEFAULT '0' COMMENT 'pour connaitre le nombre de fichiers',
  `flagdernierniveauDoc` smallint(6) NOT NULL DEFAULT '0' COMMENT 'pour connaitre le dernier niveau',
  `descriptionDocument` tinytext COLLATE latin1_general_ci NOT NULL,
  `Autorite_ID` int(10) NOT NULL DEFAULT '0',
  `IDdoc` bigint(20) DEFAULT NULL,
  `catDoc_ID` int(11) DEFAULT NULL,
  `libelleDoc` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `nomFichierDoc` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `Publier` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`IDcatDoc`),
  KEY `FKCD248F984B9DC270` (`Autorite_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='table catégories de documents' AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_categoriedocument`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_categorielegislation`
--

CREATE TABLE IF NOT EXISTS `pmb_categorielegislation` (
  `IDLEGISLATION` int(11) NOT NULL AUTO_INCREMENT,
  `niveau_LEGISLATION` int(11) NOT NULL DEFAULT '0' COMMENT 'pour connaitre ki est le pere',
  `liblegislation` longtext COLLATE latin1_general_ci NOT NULL,
  `nbtotalfichiers` int(15) NOT NULL DEFAULT '0' COMMENT 'pour connaitre le nombre de fichiers',
  `flagdernierniveau` smallint(6) NOT NULL DEFAULT '0' COMMENT 'pour connaitre le dernier niveau',
  `descriptionCategorieLegislation` tinytext COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`IDLEGISLATION`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='table catégories de législations' AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_categorielegislation`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_categorielegislations`
--

CREATE TABLE IF NOT EXISTS `pmb_categorielegislations` (
  `IDLEGISLATION` bigint(20) NOT NULL AUTO_INCREMENT,
  `descriptionCategorieLegislation` varchar(255) DEFAULT NULL,
  `liblegislation` varchar(255) DEFAULT NULL,
  `niveau_LEGISLATION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IDLEGISLATION`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `pmb_categorielegislations`
--

INSERT INTO `pmb_categorielegislations` (`IDLEGISLATION`, `descriptionCategorieLegislation`, `liblegislation`, `niveau_LEGISLATION`) VALUES
(1, 'Agr�ment pour cautionnement bancaire', 'Agr�ment pour cautionnement bancaire', NULL),
(2, 'Circulaires', 'Circulaires', NULL),
(3, 'Directives', 'Directives', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_categories`
--

CREATE TABLE IF NOT EXISTS `pmb_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `pmb_categories`
--

INSERT INTO `pmb_categories` (`id`, `code`, `commentaire`, `designation`) VALUES
(1, 'MB', '', 'Mobilier de Bureau'),
(2, 'EB', '', 'Equipement de Bureau'),
(3, 'MFB', '', 'Mat�riels et Fournitures de Bureau'),
(4, 'MIA', '', 'MATERIELS\nINFORMATIQUES ET\nACCESSOIRES'),
(5, 'MR', 'Mat�riel roulant', 'Mat�riel roulant');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_cloture_recours`
--

CREATE TABLE IF NOT EXISTS `pmb_cloture_recours` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Commentaire` varchar(50) DEFAULT NULL,
  `datecloture` date DEFAULT NULL,
  `Issuerecoursgracieux` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_cloture_recours`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_codemarche`
--

CREATE TABLE IF NOT EXISTS `pmb_codemarche` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `codeDateRapport` date DEFAULT NULL,
  `codeDatePublication` date DEFAULT NULL,
  `codeDescription` varchar(255) DEFAULT NULL,
  `codeNomfichier` varchar(255) DEFAULT NULL,
  `codeLibelle` varchar(255) DEFAULT NULL,
  `codePublier` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `pmb_codemarche`
--

INSERT INTO `pmb_codemarche` (`ID`, `codeDateRapport`, `codeDatePublication`, `codeDescription`, `codeNomfichier`, `codeLibelle`, `codePublier`) VALUES
(1, '2013-05-04', '2013-05-04', '', '3910004052013_rap_adresse.rtf', '04052013', 'yes');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_conf_dates_real`
--

CREATE TABLE IF NOT EXISTS `pmb_conf_dates_real` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateavisccmpdncmp` varchar(50) DEFAULT NULL,
  `dateavisccmpdncmpami` varchar(50) DEFAULT NULL,
  `dateavisccmpdncmpdp` varchar(50) DEFAULT NULL,
  `dateavisccmpdncmpdppt` varchar(50) DEFAULT NULL,
  `dateavisptfservcontrole` varchar(50) DEFAULT NULL,
  `datefinevaluation` varchar(50) DEFAULT NULL,
  `datefinevaluationpf` varchar(50) DEFAULT NULL,
  `dateinvitationsoumission` varchar(50) DEFAULT NULL,
  `datelancementmanifestation` varchar(50) DEFAULT NULL,
  `datenegociation` varchar(50) DEFAULT NULL,
  `datenonobjectionptf` varchar(50) DEFAULT NULL,
  `datenonobjectionptfappel` varchar(50) DEFAULT NULL,
  `datenonobjectionptfpt` varchar(50) DEFAULT NULL,
  `dateouverture` varchar(50) DEFAULT NULL,
  `dateouverturedp` varchar(50) DEFAULT NULL,
  `dateouverturemanifestation` varchar(50) DEFAULT NULL,
  `dateouvertureplis` varchar(50) DEFAULT NULL,
  `datepreparationdaodcrbc` varchar(50) DEFAULT NULL,
  `datepreparationdp` varchar(50) DEFAULT NULL,
  `datepreparationtdrami` varchar(50) DEFAULT NULL,
  `dateprevisionnellesignaturecontrat` varchar(50) DEFAULT NULL,
  `datereceptionavisccmpdncmp` varchar(50) DEFAULT NULL,
  `datereceptionavisccmpdncmpappel` varchar(50) DEFAULT NULL,
  `dureea` int(11) DEFAULT NULL,
  `dureeb` int(11) DEFAULT NULL,
  `dureec` int(11) DEFAULT NULL,
  `dureed` int(11) DEFAULT NULL,
  `dureee` int(11) DEFAULT NULL,
  `dureef` int(11) DEFAULT NULL,
  `dureeg` int(11) DEFAULT NULL,
  `dureeh` int(11) DEFAULT NULL,
  `dureei` int(11) DEFAULT NULL,
  `dureej` int(11) DEFAULT NULL,
  `dureek` int(11) DEFAULT NULL,
  `dureel` int(11) DEFAULT NULL,
  `dureem` int(11) DEFAULT NULL,
  `dureen` int(11) DEFAULT NULL,
  `dureeo` int(11) DEFAULT NULL,
  `dureep` int(11) DEFAULT NULL,
  `dureeq` int(11) DEFAULT NULL,
  `dureer` int(11) DEFAULT NULL,
  `durees` int(11) DEFAULT NULL,
  `dureet` int(11) DEFAULT NULL,
  `dureeu` int(11) DEFAULT NULL,
  `dureev` int(11) DEFAULT NULL,
  `dureew` int(11) DEFAULT NULL,
  `dureex` int(11) DEFAULT NULL,
  `examenccmp` varchar(5) DEFAULT NULL,
  `examendncmp` varchar(5) DEFAULT NULL,
  `MODE_ID` bigint(20) DEFAULT NULL,
  `type` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `pmb_conf_dates_real`
--

INSERT INTO `pmb_conf_dates_real` (`id`, `dateavisccmpdncmp`, `dateavisccmpdncmpami`, `dateavisccmpdncmpdp`, `dateavisccmpdncmpdppt`, `dateavisptfservcontrole`, `datefinevaluation`, `datefinevaluationpf`, `dateinvitationsoumission`, `datelancementmanifestation`, `datenegociation`, `datenonobjectionptf`, `datenonobjectionptfappel`, `datenonobjectionptfpt`, `dateouverture`, `dateouverturedp`, `dateouverturemanifestation`, `dateouvertureplis`, `datepreparationdaodcrbc`, `datepreparationdp`, `datepreparationtdrami`, `dateprevisionnellesignaturecontrat`, `datereceptionavisccmpdncmp`, `datereceptionavisccmpdncmpappel`, `dureea`, `dureeb`, `dureec`, `dureed`, `dureee`, `dureef`, `dureeg`, `dureeh`, `dureei`, `dureej`, `dureek`, `dureel`, `dureem`, `dureen`, `dureeo`, `dureep`, `dureeq`, `dureer`, `durees`, `dureet`, `dureeu`, `dureev`, `dureew`, `dureex`, `examenccmp`, `examendncmp`, `MODE_ID`, `type`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 7, 30, 30, 7, 21, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'oui', 'non', 1, 'TFS'),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15, 7, 30, 30, 15, 21, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'non', 'oui', 1, 'TFS'),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 7, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'non', 'non', 34, 'TFS'),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 7, 30, 30, 7, 7, 30, 21, 7, 7, 9, 7, 7, 21, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'oui', 'non', 42, 'PI'),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 15, 7, 30, 30, 15, 7, 30, 21, 15, 7, 9, 15, 7, 21, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'non', 'oui', 42, 'PI'),
(6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 7, 4, 5, 3, 7, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'non', 'non', 34, 'PI');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_contractsprestataires`
--

CREATE TABLE IF NOT EXISTS `pmb_contractsprestataires` (
  `idcontract` bigint(20) NOT NULL AUTO_INCREMENT,
  `datedebutcontract` date DEFAULT NULL,
  `datefincontract` date DEFAULT NULL,
  `numcontract` varchar(255) DEFAULT NULL,
  `idaudit` bigint(20) DEFAULT NULL,
  `idprestataire` bigint(20) DEFAULT NULL,
  `datePaiement` date DEFAULT NULL,
  `montantHT` bigint(20) DEFAULT NULL,
  `montantVerse` bigint(20) DEFAULT NULL,
  `Statut` varchar(20) DEFAULT NULL,
  `datesignaturecontrat` date DEFAULT NULL,
  PRIMARY KEY (`idcontract`),
  KEY `FK6531D7C83A25A6E3` (`idprestataire`),
  KEY `FK6531D7C83C56A135` (`idaudit`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `pmb_contractsprestataires`
--

INSERT INTO `pmb_contractsprestataires` (`idcontract`, `datedebutcontract`, `datefincontract`, `numcontract`, `idaudit`, `idprestataire`, `datePaiement`, `montantHT`, `montantVerse`, `Statut`, `datesignaturecontrat`) VALUES
(1, '2013-05-03', '2013-05-11', '333', 1, 1, NULL, NULL, NULL, NULL, NULL),
(2, '2013-05-03', '2013-05-10', 'ddd', 1, 2, NULL, NULL, NULL, NULL, NULL),
(3, '2013-05-08', '2013-05-29', 'zzz', 3, 1, NULL, NULL, NULL, NULL, NULL),
(5, '2013-05-11', '2013-05-31', 'zzzz', 4, 3, NULL, NULL, NULL, NULL, NULL),
(7, '2013-05-11', '2013-05-25', 'drt', 4, 1, NULL, NULL, NULL, NULL, NULL),
(8, '2013-05-13', '2013-06-13', '4567', 5, 4, NULL, NULL, NULL, NULL, NULL),
(9, '2013-05-07', '2013-06-13', '6789', 5, 3, NULL, NULL, NULL, NULL, NULL),
(10, '2013-05-25', '2013-05-25', 'ERFD', 6, 2, NULL, NULL, NULL, NULL, '2013-05-31'),
(11, '2013-05-26', '2013-05-26', 'ERT', 6, 3, NULL, NULL, NULL, NULL, '2013-05-31');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_contrats`
--

CREATE TABLE IF NOT EXISTS `pmb_contrats` (
  `conID` bigint(20) NOT NULL AUTO_INCREMENT,
  `conCommentApprobation` varchar(255) DEFAULT NULL,
  `conCommentNotification` varchar(255) DEFAULT NULL,
  `conCommentOrdreDemarrage` varchar(255) DEFAULT NULL,
  `conCommentRecepDefinitive` varchar(255) DEFAULT NULL,
  `conCommentRecepProvisoire` varchar(255) DEFAULT NULL,
  `conCommentSignature` varchar(255) DEFAULT NULL,
  `conDateApprobation` date DEFAULT NULL,
  `conDateNotification` date DEFAULT NULL,
  `conDateOrdreDemarrage` date DEFAULT NULL,
  `conDateRecepDefinitive` date DEFAULT NULL,
  `conDateRecepProvisoire` date DEFAULT NULL,
  `conDateSignature` date DEFAULT NULL,
  `conFichierOrdreDemarrage` varchar(255) DEFAULT NULL,
  `conFichierRecepDefinitive` varchar(255) DEFAULT NULL,
  `conFichierRecepProvisoire` varchar(255) DEFAULT NULL,
  `conRefApprobation` varchar(255) DEFAULT NULL,
  `conRefNotification` varchar(255) DEFAULT NULL,
  `conRefSignature` varchar(255) DEFAULT NULL,
  `concommentaireAttributiondefinitive` varchar(255) DEFAULT NULL,
  `concommentaireAttributionprovisoire` varchar(255) DEFAULT NULL,
  `condateAttributiondefinitive` date DEFAULT NULL,
  `condateAttributionprovisoire` date DEFAULT NULL,
  `conrefAttributiondefinitive` varchar(255) DEFAULT NULL,
  `conrefAttributionprovisoire` varchar(255) DEFAULT NULL,
  `montant` decimal(19,2) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `Pli_ID` bigint(20) DEFAULT NULL,
  `condatepaiement` date DEFAULT NULL,
  `constatus` varchar(10) DEFAULT NULL,
  `montantverse` decimal(19,2) DEFAULT NULL,
  `concommentairesdmdmat` varchar(255) DEFAULT NULL,
  `concommentairesmatriculation` varchar(255) DEFAULT NULL,
  `datedemandeimmatriculation` date DEFAULT NULL,
  `dateimmatriculation` date DEFAULT NULL,
  `immatriculation` int(11) DEFAULT NULL,
  `nummatriculation` varchar(50) DEFAULT NULL,
  `LOT` bigint(20) DEFAULT NULL,
  `fournisseurs_ID` bigint(20) DEFAULT NULL,
  `Modepassation_ID` bigint(20) DEFAULT NULL,
  `Service_ID` bigint(20) DEFAULT NULL,
  `Typemarche_ID` bigint(20) DEFAULT NULL,
  `concsituation` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`conID`),
  KEY `FK2AD0081A21E224C0` (`Dossiers_ID`),
  KEY `FK2AD0081A2B7B217B` (`Pli_ID`),
  KEY `FK2AD0081AF6884D99` (`LOT`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `pmb_contrats`
--

INSERT INTO `pmb_contrats` (`conID`, `conCommentApprobation`, `conCommentNotification`, `conCommentOrdreDemarrage`, `conCommentRecepDefinitive`, `conCommentRecepProvisoire`, `conCommentSignature`, `conDateApprobation`, `conDateNotification`, `conDateOrdreDemarrage`, `conDateRecepDefinitive`, `conDateRecepProvisoire`, `conDateSignature`, `conFichierOrdreDemarrage`, `conFichierRecepDefinitive`, `conFichierRecepProvisoire`, `conRefApprobation`, `conRefNotification`, `conRefSignature`, `concommentaireAttributiondefinitive`, `concommentaireAttributionprovisoire`, `condateAttributiondefinitive`, `condateAttributionprovisoire`, `conrefAttributiondefinitive`, `conrefAttributionprovisoire`, `montant`, `Autorite_ID`, `Dossiers_ID`, `Pli_ID`, `condatepaiement`, `constatus`, `montantverse`, `concommentairesdmdmat`, `concommentairesmatriculation`, `datedemandeimmatriculation`, `dateimmatriculation`, `immatriculation`, `nummatriculation`, `LOT`, `fournisseurs_ID`, `Modepassation_ID`, `Service_ID`, `Typemarche_ID`, `concsituation`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-09', NULL, '2013-04-29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '450000000.00', 6, 1, 1, NULL, 'NON', '6750000.00', 'ccc', '', '2013-04-29', '2013-04-29', 1, '2013040001', 1, 1, 1, 18, 3, 'En cours'),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-09', NULL, '2013-05-04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '40000000000.00', 6, 2, NULL, NULL, 'NON', '600000000.00', NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, 5, 18, 3, 'En cours'),
(3, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '2013-05-04', '2013-05-09', NULL, '2013-05-04', '43211104052013_Pj_adresse.rtf', NULL, '53211104052013_Pj_adresse.rtf', NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-04', NULL, NULL, '500000000.00', 6, 3, 4, NULL, 'NON', '7500000.00', '', '', '2013-05-04', '2013-05-04', 1, '2013050002', 2, 1, 34, 18, 3, 'En cours'),
(4, NULL, NULL, NULL, '', '', NULL, NULL, NULL, '2013-05-12', '2013-05-10', NULL, '2013-05-12', '3853812052013_Pj_recu.pdf', '0154812052013_Pj_recu.pdf', '4953812052013_Pj_recu.pdf', NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-12', NULL, NULL, '6000000000.00', 6, 5, 8, NULL, 'NON', '90000000.00', 'ras', '', '2013-05-04', '2013-05-04', 1, '2013050003', NULL, 1, 42, 18, 4, 'En cours'),
(5, NULL, NULL, NULL, '', '', NULL, NULL, NULL, '2013-05-12', '2013-05-10', NULL, '2013-05-12', '5152812052013_Pj_recu.pdf', '1553812052013_Pj_recu.pdf', '0253812052013_Pj_recu.pdf', NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-12', NULL, NULL, '40000000.00', 6, 9, 11, NULL, 'NON', '600000.00', '', '', '2013-05-05', '2013-05-05', 1, '2013050004', NULL, 1, 34, 18, 4, 'En cours'),
(6, NULL, NULL, NULL, '', '', NULL, NULL, NULL, '2013-05-12', '2013-05-10', NULL, '2013-05-12', '2648812052013_Pj_recu.pdf', '0949812052013_Pj_recu.pdf', '4748812052013_Pj_recu.pdf', NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-12', NULL, NULL, '5000000000.00', 6, 10, 12, NULL, 'NON', '75000000.00', '', '', '2013-05-05', '2013-05-05', 1, '2013050005', 3, 1, 1, 18, 1, 'En cours'),
(7, NULL, NULL, NULL, '', '', NULL, NULL, NULL, '2013-05-12', '2013-05-10', NULL, '2013-05-12', '1550812052013_Pj_recu.pdf', '3550812052013_Pj_recu.pdf', '2550812052013_Pj_recu.pdf', NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-12', NULL, NULL, '0.00', 6, 11, 15, NULL, 'NON', '0.00', '', '', '2013-05-05', '2013-05-05', 1, '2013050006', 4, 1, 34, 18, 1, 'En cours'),
(8, NULL, NULL, NULL, '', '', NULL, NULL, NULL, '2013-05-12', '2013-05-10', NULL, '2013-05-12', '0151812052013_Pj_recu.pdf', '2351812052013_Pj_recu.pdf', '1151812052013_Pj_recu.pdf', NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-12', NULL, NULL, '400000000.00', 6, 12, 18, NULL, 'NON', '6000000.00', '', '', '2013-05-05', '2013-05-05', 1, '2013050007', 5, 1, 1, 18, 2, 'En cours'),
(9, NULL, NULL, NULL, '', '', NULL, NULL, NULL, '2013-05-12', '2013-05-10', NULL, '2013-05-12', '4751812052013_Pj_recu.pdf', '1152812052013_Pj_recu.pdf', '5851812052013_Pj_recu.pdf', NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-12', NULL, NULL, '0.00', 6, 13, 21, NULL, 'NON', '0.00', '', '', '2013-05-05', '2013-05-05', 1, '2013050008', 7, 1, 34, 18, 2, 'En cours'),
(10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-09', NULL, '2013-05-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.00', 6, 13, 23, '2013-05-12', 'OUI', '0.00', NULL, '', NULL, '2013-05-05', 1, '2013050009', 6, 3, 34, 18, 2, 'En cours'),
(11, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '2013-05-12', '2013-05-09', NULL, '2013-05-08', '0039812052013_Pj_recu.pdf', NULL, '1339812052013_Pj_recu.pdf', NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-12', NULL, NULL, '600000000.00', 902, 15, 27, NULL, 'NON', '9000000.00', 'ddd', '', '2013-05-08', '2013-05-08', 1, '2013050010', 9, 1, 1, 19, 3, 'En cours'),
(12, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '2013-05-12', '2013-05-09', NULL, '2013-05-09', '3840812052013_Pj_recu.pdf', NULL, '4940812052013_Pj_recu.pdf', NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-12', NULL, NULL, '200000000.00', 9, 18, 34, NULL, 'NON', '3000000.00', 'ras', '', '2013-05-09', '2013-05-09', 1, '2013050011', 10, 1, 1, 20, 3, 'En cours'),
(13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-09', NULL, '2013-05-09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '95000000.00', 9, 18, 36, NULL, 'NON', '1425000.00', NULL, '', NULL, '2013-05-09', 1, '2013050012', 11, 5, 1, 20, 3, 'En cours');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_courriercontentieux`
--

CREATE TABLE IF NOT EXISTS `pmb_courriercontentieux` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Date` date DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `contentieuxID` bigint(20) DEFAULT NULL,
  `Fichier` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKB04DF4B1EA7AB2C5` (`contentieuxID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `pmb_courriercontentieux`
--

INSERT INTO `pmb_courriercontentieux` (`id`, `Date`, `libelle`, `contentieuxID`, `Fichier`) VALUES
(1, '2013-05-07', 'sss', 1, '4819107052013_Pj_recu.pdf'),
(2, '2013-05-01', 'sss', 1, '58451108052013_Pj_adresse.rtf'),
(3, '2013-05-11', 'dd', 2, '3313911052013_Pj_recu.pdf');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_courrierdenonciations`
--

CREATE TABLE IF NOT EXISTS `pmb_courrierdenonciations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Date` date DEFAULT NULL,
  `Fichier` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `denonciationID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `pmb_courrierdenonciations`
--

INSERT INTO `pmb_courrierdenonciations` (`id`, `Date`, `Fichier`, `libelle`, `denonciationID`) VALUES
(1, '2013-05-09', '2605009052013_Pj_adresse.rtf', 'dd', 5),
(2, '2013-05-09', '0858609052013_Pj_recu.pdf', 'ttt', 5);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_courriers_ac`
--

CREATE TABLE IF NOT EXISTS `pmb_courriers_ac` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `courrierautre` varchar(100) DEFAULT NULL,
  `datecourrier` date DEFAULT NULL,
  `datereception` date DEFAULT NULL,
  `datesaisie` date DEFAULT NULL,
  `objetcourrier` varchar(200) DEFAULT NULL,
  `reference` varchar(50) DEFAULT NULL,
  `autoritecontractante` bigint(20) DEFAULT NULL,
  `modereception` bigint(20) DEFAULT NULL,
  `typecourrier` bigint(20) DEFAULT NULL,
  `courrierRef` varchar(255) DEFAULT NULL,
  `natureCourrier_idnaturecourrier` bigint(20) DEFAULT NULL,
  `amplitaire` varchar(255) DEFAULT NULL,
  `modetraitement_idmodetraitement` bigint(20) DEFAULT NULL,
  `dossierCourrier_id` bigint(20) DEFAULT NULL,
  `numenregistrement` varchar(100) DEFAULT NULL,
  `typesDossiers_Id` bigint(20) DEFAULT NULL,
  `nature` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKC8346D95F7EC5CB2` (`modereception`),
  KEY `FKC8346D95FF9E8AB8` (`modetraitement_idmodetraitement`),
  KEY `FKC8346D9512168014` (`dossierCourrier_id`),
  KEY `FKC8346D95578FE0B4` (`natureCourrier_idnaturecourrier`),
  KEY `FKC8346D9539D8E5E7` (`typecourrier`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `pmb_courriers_ac`
--

INSERT INTO `pmb_courriers_ac` (`id`, `courrierautre`, `datecourrier`, `datereception`, `datesaisie`, `objetcourrier`, `reference`, `autoritecontractante`, `modereception`, `typecourrier`, `courrierRef`, `natureCourrier_idnaturecourrier`, `amplitaire`, `modetraitement_idmodetraitement`, `dossierCourrier_id`, `numenregistrement`, `typesDossiers_Id`, `nature`) VALUES
(3, 'ARRIVE', '2013-05-24', '2013-05-24', '2013-05-24', 'ss', 'ss', 932, 1, 1, NULL, 2, 'www', NULL, NULL, '2013/002', 6, 'ARRIVE'),
(4, NULL, '2013-05-25', '2013-05-25', '2013-05-25', 'ssss', 'sss', 923, 1, 1, NULL, 2, '', NULL, NULL, '2013/001', 5, 'DEPART');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_courriers_audits`
--

CREATE TABLE IF NOT EXISTS `pmb_courriers_audits` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `courrierCommentaire` varchar(100) DEFAULT NULL,
  `datecourrier` date DEFAULT NULL,
  `datereception` date DEFAULT NULL,
  `datesaisie` date DEFAULT NULL,
  `objetcourrier` varchar(200) DEFAULT NULL,
  `courrierOrigine` varchar(100) DEFAULT NULL,
  `reference` varchar(50) DEFAULT NULL,
  `AutoriteAuditsPres` bigint(20) DEFAULT NULL,
  `fichier` varchar(255) DEFAULT NULL,
  `audit` bigint(20) DEFAULT NULL,
  `Datepubrapport` date DEFAULT NULL,
  `fichier2` varchar(255) DEFAULT NULL,
  `Statut` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `pmb_courriers_audits`
--

INSERT INTO `pmb_courriers_audits` (`id`, `courrierCommentaire`, `datecourrier`, `datereception`, `datesaisie`, `objetcourrier`, `courrierOrigine`, `reference`, `AutoriteAuditsPres`, `fichier`, `audit`, `Datepubrapport`, `fichier2`, `Statut`) VALUES
(2, NULL, '2013-05-08', '2013-05-08', NULL, 'dd', NULL, 'dd', NULL, '3318608052013_Pj_recu.pdf', NULL, NULL, NULL, NULL),
(3, NULL, '2013-05-08', NULL, NULL, NULL, 'RapportProvisoire', 'www', 13, NULL, NULL, NULL, NULL, NULL),
(4, NULL, '2013-05-08', NULL, NULL, NULL, 'Observation', 'wwww', 13, NULL, NULL, NULL, NULL, NULL),
(5, NULL, '2013-05-11', NULL, NULL, NULL, 'RapportProvisoire', 'ddd', 16, NULL, 4, NULL, NULL, NULL),
(6, NULL, '2013-05-11', NULL, NULL, NULL, 'Observation', 'ssssss', 16, NULL, 4, NULL, NULL, NULL),
(7, NULL, '2013-05-11', '2013-05-11', NULL, 'dd', NULL, 'd', NULL, '40231011052013_Pj_recu.pdf', 4, NULL, NULL, NULL),
(8, NULL, '2013-05-13', NULL, NULL, NULL, 'RapportProvisoire', '233', 21, '0651513052013_Pj_table.rtf', 5, NULL, NULL, NULL),
(9, NULL, '2013-05-13', NULL, NULL, NULL, 'Observation', '234E', 21, '4152513052013_Pj_recu.pdf', 5, NULL, NULL, NULL),
(10, NULL, '2013-05-26', NULL, NULL, NULL, 'RapportProvisoire', 'dff', 25, '3706026052013_Pj_indicateur.rtf', 6, NULL, NULL, NULL),
(11, NULL, '2013-05-26', NULL, NULL, NULL, 'Observation', 'vgf', 25, '2407026052013_Pj_recu.pdf', 6, NULL, '', NULL),
(12, NULL, '2013-05-26', NULL, NULL, NULL, 'RapportDefinitive', NULL, 25, '4007026052013_Pj_recu.pdf', 6, '2013-05-26', NULL, 'PUB'),
(13, NULL, '2013-05-26', NULL, NULL, NULL, 'RapportGlobal', NULL, NULL, '2908026052013_Pj_recu.pdf', 6, '2013-05-26', NULL, 'PUB');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_courriers_evenement`
--

CREATE TABLE IF NOT EXISTS `pmb_courriers_evenement` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `courrierCommentaire` varchar(100) DEFAULT NULL,
  `datecourrier` date DEFAULT NULL,
  `datereception` date DEFAULT NULL,
  `datesaisie` date DEFAULT NULL,
  `objetcourrier` varchar(200) DEFAULT NULL,
  `courrierOrigine` varchar(100) DEFAULT NULL,
  `reference` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_courriers_evenement`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_critereanalyse`
--

CREATE TABLE IF NOT EXISTS `pmb_critereanalyse` (
  `idcritereanalyse` bigint(20) NOT NULL AUTO_INCREMENT,
  `codecritereanalyse` varchar(10) DEFAULT NULL,
  `descriptionanalyse` varchar(500) DEFAULT NULL,
  `libellecritereanalyse` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idcritereanalyse`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_critereanalyse`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_criteresoumissions`
--

CREATE TABLE IF NOT EXISTS `pmb_criteresoumissions` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `conforme` int(11) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `CRITERE` bigint(20) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `Pli_ID` bigint(20) DEFAULT NULL,
  `libellelot` varchar(255) DEFAULT NULL,
  `lot` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK7FC6D7C21E224C0` (`Dossiers_ID`),
  KEY `FK7FC6D7C4E4BAA3B` (`CRITERE`),
  KEY `FK7FC6D7C2B7B217B` (`Pli_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Contenu de la table `pmb_criteresoumissions`
--

INSERT INTO `pmb_criteresoumissions` (`ID`, `conforme`, `libelle`, `CRITERE`, `Dossiers_ID`, `Pli_ID`, `libellelot`, `lot`) VALUES
(1, 0, NULL, 1, 1, 1, NULL, 1),
(2, 0, NULL, 1, 1, 2, NULL, 1),
(3, 0, NULL, 1, 1, 3, NULL, 1),
(4, 0, NULL, 2, 3, 4, NULL, 2),
(5, 0, NULL, 2, 3, 5, NULL, 2),
(6, 0, NULL, 6, 10, 12, NULL, 3),
(7, 0, NULL, 6, 10, 13, NULL, 3),
(8, 0, NULL, 6, 10, 14, NULL, 3),
(9, 0, NULL, 7, 11, 15, NULL, 4),
(10, 0, NULL, 7, 11, 16, NULL, 4),
(11, 0, NULL, 7, 11, 17, NULL, 4),
(12, 0, NULL, 8, 12, 18, NULL, 5),
(13, 0, NULL, 8, 12, 19, NULL, 5),
(14, 0, NULL, 8, 12, 20, NULL, 5),
(15, 0, NULL, 9, 13, 21, NULL, 6),
(16, 0, NULL, 10, 13, 21, NULL, 7),
(17, 0, NULL, 9, 13, 22, NULL, 6),
(18, 0, NULL, 10, 13, 22, NULL, 7),
(19, 0, NULL, 9, 13, 23, NULL, 6),
(20, 0, NULL, 10, 13, 23, NULL, 7),
(21, 0, NULL, 11, 14, 24, NULL, 8),
(22, 0, NULL, 11, 14, 25, NULL, 8),
(23, 0, NULL, 11, 14, 26, NULL, 8),
(24, 0, NULL, 12, 15, 27, NULL, 9),
(25, 0, NULL, 12, 15, 28, NULL, 9),
(26, 1, NULL, 15, 18, 34, NULL, 10),
(27, 1, NULL, 17, 18, 34, NULL, 11),
(28, 1, NULL, 16, 18, 34, NULL, 10),
(29, 1, NULL, 18, 18, 34, NULL, 11),
(30, 1, NULL, 15, 18, 35, NULL, 10),
(31, 1, NULL, 17, 18, 35, NULL, 11),
(32, 1, NULL, 16, 18, 35, NULL, 10),
(33, 1, NULL, 18, 18, 35, NULL, 11),
(34, 1, NULL, 15, 18, 36, NULL, 10),
(35, 1, NULL, 17, 18, 36, NULL, 11),
(36, 1, NULL, 16, 18, 36, NULL, 10),
(37, 1, NULL, 18, 18, 36, NULL, 11),
(38, 1, NULL, 15, 18, 37, NULL, 10),
(39, 1, NULL, 17, 18, 37, NULL, 11),
(40, 1, NULL, 16, 18, 37, NULL, 10),
(41, 1, NULL, 18, 18, 37, NULL, 11);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_decision`
--

CREATE TABLE IF NOT EXISTS `pmb_decision` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  `typedecisionID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6E5324D64172CAED` (`typedecisionID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `pmb_decision`
--

INSERT INTO `pmb_decision` (`id`, `libelle`, `typedecisionID`) VALUES
(1, 'Contestation de l’attribution du marché de fourniture', 1),
(3, 'Suspension de la procédure de passation du marché', 5);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_decisioncontentieux`
--

CREATE TABLE IF NOT EXISTS `pmb_decisioncontentieux` (
  `decId` bigint(20) NOT NULL AUTO_INCREMENT,
  `decAuteurrecour` varchar(255) DEFAULT NULL,
  `decCommentaire` varchar(255) DEFAULT NULL,
  `decDate` date DEFAULT NULL,
  `decDateRecour` date DEFAULT NULL,
  `decDecision` varchar(50) DEFAULT NULL,
  `decFichier` varchar(255) DEFAULT NULL,
  `decNumero` varchar(50) DEFAULT NULL,
  `decObjet` varchar(255) DEFAULT NULL,
  `decStatut` varchar(10) DEFAULT NULL,
  `aut_autId` bigint(20) DEFAULT NULL,
  `contentieuxID` bigint(20) DEFAULT NULL,
  `modepassation` bigint(20) DEFAULT NULL,
  `decisionID` bigint(20) DEFAULT NULL,
  `typemarche` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`decId`),
  KEY `FKC2662E22BB47723C` (`modepassation`),
  KEY `FKC2662E22EA7AB2C5` (`contentieuxID`),
  KEY `FKC2662E2211127C33` (`decisionID`),
  KEY `FKC2662E229C2E6DA` (`typemarche`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `pmb_decisioncontentieux`
--

INSERT INTO `pmb_decisioncontentieux` (`decId`, `decAuteurrecour`, `decCommentaire`, `decDate`, `decDateRecour`, `decDecision`, `decFichier`, `decNumero`, `decObjet`, `decStatut`, `aut_autId`, `contentieuxID`, `modepassation`, `decisionID`, `typemarche`) VALUES
(1, NULL, 'ss', '2013-05-07', NULL, NULL, '2316107052013_Pj_recu.pdf', '222', NULL, 'VAL', 6, 1, NULL, 2, NULL),
(2, NULL, 'fff', '2013-05-11', NULL, NULL, '3843811052013_Pj_recu.pdf', 'fru', NULL, 'VAL', 9, 2, NULL, 4, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_decisiondenonciation`
--

CREATE TABLE IF NOT EXISTS `pmb_decisiondenonciation` (
  `decId` bigint(20) NOT NULL AUTO_INCREMENT,
  `decAuteurrecour` varchar(255) DEFAULT NULL,
  `decCommentaire` varchar(255) DEFAULT NULL,
  `decDate` date DEFAULT NULL,
  `decDateRecour` date DEFAULT NULL,
  `decDecision` varchar(50) DEFAULT NULL,
  `decFichier` varchar(255) DEFAULT NULL,
  `decNumero` varchar(50) DEFAULT NULL,
  `decObjet` varchar(255) DEFAULT NULL,
  `decStatut` varchar(10) DEFAULT NULL,
  `aut_autId` bigint(20) DEFAULT NULL,
  `denonciationID` bigint(20) DEFAULT NULL,
  `modepassation` bigint(20) DEFAULT NULL,
  `decisionID` bigint(20) DEFAULT NULL,
  `typemarche` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`decId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `pmb_decisiondenonciation`
--

INSERT INTO `pmb_decisiondenonciation` (`decId`, `decAuteurrecour`, `decCommentaire`, `decDate`, `decDateRecour`, `decDecision`, `decFichier`, `decNumero`, `decObjet`, `decStatut`, `aut_autId`, `denonciationID`, `modepassation`, `decisionID`, `typemarche`) VALUES
(1, 'Z', 'ZZZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, NULL, 'drr', '2013-05-08', NULL, NULL, '3249908052013_Pj_adresse.rtf', 'sss', NULL, 'VAL', NULL, 5, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_decisionslocaux`
--

CREATE TABLE IF NOT EXISTS `pmb_decisionslocaux` (
  `dlocId` bigint(20) NOT NULL AUTO_INCREMENT,
  `dlocBP` bigint(20) DEFAULT NULL,
  `dlocdaterecours` date DEFAULT NULL,
  `dlocEmail` varchar(50) DEFAULT NULL,
  `dlocFax` varchar(50) DEFAULT NULL,
  `dlocHeure` time DEFAULT NULL,
  `dlocMotifs` varchar(255) DEFAULT NULL,
  `dlocObjet` varchar(255) DEFAULT NULL,
  `dlocTelephone` varchar(50) DEFAULT NULL,
  `appeloffreID` bigint(20) DEFAULT NULL,
  `aut_autId` bigint(20) DEFAULT NULL,
  `dlocDatestatut` date DEFAULT NULL,
  `dlocTraitement` varchar(8) DEFAULT NULL,
  `statCODE` varchar(200) DEFAULT NULL,
  `recvCommentaire` varchar(255) DEFAULT NULL,
  `recvDate` date DEFAULT NULL,
  `recvDecision` varchar(50) DEFAULT NULL,
  `recvFichier` varchar(255) DEFAULT NULL,
  `recvNumero` bigint(20) DEFAULT NULL,
  `dlocPublier` char(10) DEFAULT NULL,
  `dlocDatecreation` date DEFAULT NULL,
  `dlocReference` varchar(255) DEFAULT NULL,
  `dlocDateDecision` date DEFAULT NULL,
  `dlocNumero` varchar(255) DEFAULT NULL,
  `dlocNumeroDecision` bigint(20) DEFAULT NULL,
  `dlocNomRaisonSocial` varchar(255) DEFAULT NULL,
  `dlocReferenceCourrier` varchar(255) DEFAULT NULL,
  `dlocAdresse` varchar(255) DEFAULT NULL,
  `dlocNom` varchar(255) DEFAULT NULL,
  `dlocPrenom` varchar(255) DEFAULT NULL,
  `dlocRaisonSocial` varchar(255) DEFAULT NULL,
  `dlocdatecourrier` date DEFAULT NULL,
  PRIMARY KEY (`dlocId`),
  KEY `FK60DA8C2114FFAC9` (`aut_autId`),
  KEY `FK60DA8C2190154997` (`appeloffreID`),
  KEY `FK60DA8C21F92DAD71` (`statCODE`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `pmb_decisionslocaux`
--

INSERT INTO `pmb_decisionslocaux` (`dlocId`, `dlocBP`, `dlocdaterecours`, `dlocEmail`, `dlocFax`, `dlocHeure`, `dlocMotifs`, `dlocObjet`, `dlocTelephone`, `appeloffreID`, `aut_autId`, `dlocDatestatut`, `dlocTraitement`, `statCODE`, `recvCommentaire`, `recvDate`, `recvDecision`, `recvFichier`, `recvNumero`, `dlocPublier`, `dlocDatecreation`, `dlocReference`, `dlocDateDecision`, `dlocNumero`, `dlocNumeroDecision`, `dlocNomRaisonSocial`, `dlocReferenceCourrier`, `dlocAdresse`, `dlocNom`, `dlocPrenom`, `dlocRaisonSocial`, `dlocdatecourrier`) VALUES
(1, NULL, '2013-05-07', 'dd@yahoo.fr', 'dd', '12:00:00', 'dd', 'sss', 'dd', 1, 6, '2013-05-07', 'PUB', 'SAISIECONT_STATE', '', '2013-05-07', 'Recevable', '4403107052013_Pj_modele.doc', 222, NULL, NULL, NULL, '2013-05-07', '2013001', 1, 'dd', 'sss', 'dd', NULL, NULL, NULL, '2013-05-07'),
(2, NULL, '2013-05-11', 'test@ssi.sn', '', '14:00:00', 'test', 'Test', '', 15, 9, '2013-05-11', 'PUB', 'SAISIECONT_STATE', 'raq', '2013-05-11', 'Recevable', '2926811052013_Pj_recu.pdf', 5487, NULL, NULL, NULL, NULL, '2013002', NULL, 'test', '5678', 'test', NULL, NULL, NULL, '2013-05-11');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_demandepaiement`
--

CREATE TABLE IF NOT EXISTS `pmb_demandepaiement` (
  `demID` bigint(20) NOT NULL AUTO_INCREMENT,
  `datecourrier` date DEFAULT NULL,
  `datepaiement` date DEFAULT NULL,
  `livable` varchar(255) DEFAULT NULL,
  `montant` double DEFAULT NULL,
  `refcourrier` varchar(255) DEFAULT NULL,
  `suivi` varchar(255) DEFAULT NULL,
  `calendrier` bigint(20) DEFAULT NULL,
  `contrat` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`demID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `pmb_demandepaiement`
--

INSERT INTO `pmb_demandepaiement` (`demID`, `datecourrier`, `datepaiement`, `livable`, `montant`, `refcourrier`, `suivi`, `calendrier`, `contrat`) VALUES
(1, '2013-05-01', '2013-05-01', 'dd', 54000000, 'sss', 'non', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_demandeprocedurederogatoire`
--

CREATE TABLE IF NOT EXISTS `pmb_demandeprocedurederogatoire` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `datedemande` datetime DEFAULT NULL,
  `datestatut` datetime DEFAULT NULL,
  `motif` varchar(10) DEFAULT NULL,
  `piecejointe` varchar(10) DEFAULT NULL,
  `statut` varchar(10) DEFAULT NULL,
  `demande` bigint(20) DEFAULT NULL,
  `realisation` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_demandeprocedurederogatoire`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_denonciation`
--

CREATE TABLE IF NOT EXISTS `pmb_denonciation` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `addresse` varchar(255) DEFAULT NULL,
  `dateappel` date DEFAULT NULL,
  `datecourrier` date DEFAULT NULL,
  `datedenonciation` date DEFAULT NULL,
  `datereception` date DEFAULT NULL,
  `denonciation` varchar(255) DEFAULT NULL,
  `fichier` varchar(255) DEFAULT NULL,
  `nomappellant` varchar(255) DEFAULT NULL,
  `numerovert` int(11) DEFAULT NULL,
  `objet` varchar(255) DEFAULT NULL,
  `origine` varchar(255) DEFAULT NULL,
  `poubelle` int(11) DEFAULT NULL,
  `publier` int(11) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `traiter` int(11) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `dlocDatestatut` date DEFAULT NULL,
  `recvCommentaire` varchar(255) DEFAULT NULL,
  `recvDate` date DEFAULT NULL,
  `recvDecision` varchar(50) DEFAULT NULL,
  `recvFichier` varchar(255) DEFAULT NULL,
  `recvNumero` bigint(20) DEFAULT NULL,
  `dlocTraitement` varchar(8) DEFAULT NULL,
  `statCODE` varchar(200) DEFAULT NULL,
  `appeloffreID` bigint(20) DEFAULT NULL,
  `aut_autId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKBD220A1DF92DAD71` (`statCODE`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `pmb_denonciation`
--

INSERT INTO `pmb_denonciation` (`ID`, `addresse`, `dateappel`, `datecourrier`, `datedenonciation`, `datereception`, `denonciation`, `fichier`, `nomappellant`, `numerovert`, `objet`, `origine`, `poubelle`, `publier`, `reference`, `traiter`, `adresse`, `dlocDatestatut`, `recvCommentaire`, `recvDate`, `recvDecision`, `recvFichier`, `recvNumero`, `dlocTraitement`, `statCODE`, `appeloffreID`, `aut_autId`) VALUES
(2, NULL, NULL, '2013-05-02', NULL, '2013-05-02', NULL, '32521102052013_Pj_adresse.rtf', NULL, NULL, 'EE', 'COURRIER', 1, NULL, 'EE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, NULL, '2013-05-02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sss', 'NUMEROVERT', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, NULL, NULL, '2013-05-07', NULL, '2013-04-30', NULL, '09171007052013_Pj_recu.pdf', NULL, NULL, 'dd', 'COURRIER', 1, NULL, 'dd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, NULL, NULL, NULL, '2013-05-08', NULL, 'cc', '161006539518a05eede1d8', NULL, NULL, 'cc', 'PORTAIL', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, NULL, NULL, NULL, '2013-05-13', NULL, 'D�nonciation', '92469029051912ca9b02a6.pdf', NULL, NULL, '09876', 'PORTAIL', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, NULL, NULL, NULL, '2013-05-13', NULL, '6789DE', '86866428251912d1b77fe4.pdf', NULL, NULL, '6789DE', 'PORTAIL', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, NULL, NULL, NULL, '2013-05-24', NULL, 'rrrrrrrrrrrrrrrr', '716033035519f94adc4c56', NULL, NULL, 'rrrrrrrrrrrrrrrrrr', 'PORTAIL', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_derogatoire`
--

CREATE TABLE IF NOT EXISTS `pmb_derogatoire` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `commentairedcmp` varchar(255) DEFAULT NULL,
  `DateMiseValidation` date DEFAULT NULL,
  `DateValidation` date DEFAULT NULL,
  `fichier` varchar(255) DEFAULT NULL,
  `fichierdcmp` varchar(255) DEFAULT NULL,
  `modepassation` bigint(20) DEFAULT NULL,
  `numAut` varchar(255) DEFAULT NULL,
  `requete` varchar(255) DEFAULT NULL,
  `validationAOR` int(11) DEFAULT NULL,
  `validationED` int(11) DEFAULT NULL,
  `validationPU` int(11) DEFAULT NULL,
  `autorite` bigint(20) DEFAULT NULL,
  `modepassationCible` bigint(20) DEFAULT NULL,
  `rea_Id` bigint(20) DEFAULT NULL,
  `user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `pmb_derogatoire`
--

INSERT INTO `pmb_derogatoire` (`id`, `commentairedcmp`, `DateMiseValidation`, `DateValidation`, `fichier`, `fichierdcmp`, `modepassation`, `numAut`, `requete`, `validationAOR`, `validationED`, `validationPU`, `autorite`, `modepassationCible`, `rea_Id`, `user`) VALUES
(1, 'March� par Entente Directe', '2013-05-03', '2013-05-03', '02511103052013_Pj_adresse.rtf', '49531103052013_Pj_adresse.rtf', 1, NULL, 'Demande d''Entente Directe', 0, 2, 0, 6, 5, 12, 17),
(2, 'dd', '2013-05-10', '2013-05-10', '1404310052013_Pj_recu.pdf', '4607310052013_Pj_recu.pdf', 1, NULL, 'Entente directe', 0, 2, 0, 902, 5, 31, 18),
(3, '', '2013-05-10', '2013-05-10', '4817410052013_Pj_recu.pdf', '', 1, NULL, 'Demande d''entente directe', 0, 2, 0, 905, 5, 49, 21);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_devise`
--

CREATE TABLE IF NOT EXISTS `pmb_devise` (
  `devId` bigint(20) NOT NULL AUTO_INCREMENT,
  `devTauxConversion` decimal(19,2) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `monCode` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`devId`),
  KEY `FK17530BC021E224C0` (`Dossiers_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `pmb_devise`
--

INSERT INTO `pmb_devise` (`devId`, `devTauxConversion`, `Dossiers_ID`, `monCode`) VALUES
(1, '1.00', 1, 1),
(2, '1.00', 3, 1),
(3, '1.00', 5, 1),
(4, '1.00', 9, 1),
(5, '1.00', 10, 1),
(6, '1.00', 11, 1),
(7, '1.00', 12, 1),
(8, '1.00', 13, 1),
(9, '1.00', 14, 1),
(10, '1.00', 15, 1),
(11, '1.00', 17, 1),
(12, '1.00', 18, 1),
(13, '1.00', 20, 1),
(14, '1.00', 22, 1);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_documents`
--

CREATE TABLE IF NOT EXISTS `pmb_documents` (
  `idDocument` bigint(20) NOT NULL AUTO_INCREMENT,
  `Idlot` int(11) DEFAULT NULL,
  `textpvouverture` varchar(255) DEFAULT NULL,
  `TypeDocument` varchar(255) DEFAULT NULL,
  `Appelsoffres_ID` bigint(20) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `NomFichier` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `objet` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `PLIS` bigint(20) DEFAULT NULL,
  `heure` date DEFAULT NULL,
  PRIMARY KEY (`idDocument`),
  KEY `FK7F00FCFE21E224C0` (`Dossiers_ID`),
  KEY `FK7F00FCFE25D5960` (`Appelsoffres_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=145 ;

--
-- Contenu de la table `pmb_documents`
--

INSERT INTO `pmb_documents` (`idDocument`, `Idlot`, `textpvouverture`, `TypeDocument`, `Appelsoffres_ID`, `Dossiers_ID`, `NomFichier`, `libelle`, `date`, `objet`, `reference`, `PLIS`, `heure`) VALUES
(1, NULL, NULL, 'ajoutdossier', 1, 1, '0510628042013_Pj_02-ECBU.pdf', 'Dossier d''appel d''offres', '2013-04-28', NULL, NULL, NULL, '2013-04-28'),
(2, NULL, NULL, 'mavdossier', 1, 1, '1117628042013_Pj_02-ECBU.pdf', 'Mise en validation DAO', '2013-04-28', NULL, NULL, NULL, '2013-04-28'),
(3, NULL, NULL, 'validationdao', 1, 1, '1318628042013_Pj_02-ECBU.pdf', 'Validation DAO', '2013-04-28', NULL, NULL, NULL, '2013-04-28'),
(4, NULL, NULL, 'publicationdao', 1, 1, '1819628042013_Pj_02-ECBU.pdf', 'Mise en publication DAO', '2013-04-28', NULL, NULL, NULL, '2013-04-28'),
(5, NULL, NULL, 'pmb_pvouverture', 1, 1, '3919829042013_Pj_exemple.rtf', 'PV d''ouverture', '2013-04-29', NULL, NULL, NULL, '2013-04-29'),
(6, NULL, NULL, 'pv_evaluation', 1, 1, '4525829042013_Pj_exemple.rtf', 'PV d''�valuation', NULL, NULL, NULL, NULL, NULL),
(7, 1, NULL, 'pmb_attributions', 1, 1, '1931829042013_Pj_adresse.rtf', NULL, '2013-04-29', NULL, NULL, NULL, '2013-04-29'),
(8, NULL, NULL, 'mavattprov', 1, 1, '5633829042013_Pj_adresse.rtf', 'Mise en validation Attribution provisoire', '2013-04-29', NULL, NULL, NULL, '2013-04-29'),
(9, NULL, NULL, 'validationdao', 1, 1, '3834829042013_Pj_exemple.rtf', 'Validation DAO', '2013-04-29', NULL, NULL, NULL, '2013-04-29'),
(10, NULL, NULL, 'pmb_attributionsp', 1, 1, '3437829042013_Pj_adresse.rtf', 'PV d''attribution ( provisoir et d�finitif )', '2013-04-29', NULL, NULL, NULL, '2013-04-29'),
(11, NULL, NULL, 'mavattprov', 1, 1, '2139829042013_Pj_adresse.rtf', 'Mise en validation Attribution provisoire', '2013-04-29', NULL, NULL, NULL, '2013-04-29'),
(12, NULL, NULL, 'validationdao', 1, 1, '1540829042013_Pj_adresse.rtf', 'Validation DAO', '2013-04-29', NULL, NULL, NULL, '2013-04-29'),
(13, NULL, NULL, 'pmb_attributionsd', 1, 1, '3841829042013_Pj_adresse.rtf', 'Avis attribution d�finitive', '2013-04-29', NULL, NULL, NULL, '2013-04-29'),
(14, NULL, NULL, 'mavdossier', 2, 2, '1714104052013_Pj_adresse.rtf', 'Mise en validation DAO', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(15, NULL, NULL, 'validationdao', 2, 2, '0717104052013_Pj_adresse.rtf', 'Validation DAO', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(16, NULL, NULL, 'pmb_pvouverture', 3, 3, '13421004052013_Pj_adresse.rtf', 'PV d''ouverture', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(17, 2, NULL, 'pmb_attributions', 3, 3, '52111104052013_Pj_adresse.rtf', 'PV d''attribution ( provisoir et d�finitif )', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(18, NULL, NULL, 'pmb_attributionsp', 3, 3, '22481004052013_Pj_adresse.rtf', 'PV d''attribution ( provisoir et d�finitif )', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(19, NULL, NULL, 'ajoutdossier', 6, 4, '0049004052013_Pj_adresse.rtf', 'Dossier d''appel d''offres', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(20, NULL, NULL, 'mavdossier', 6, 4, '4649004052013_Pj_adresse.rtf', 'Mise en validation DAO', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(21, NULL, NULL, 'validationdao', 6, 4, '2050004052013_Pj_adresse.rtf', 'Validation DAO', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(22, NULL, NULL, 'publicationdao', 6, 4, '5850004052013_Pj_adresse.rtf', 'Mise en publication DAO', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(23, NULL, NULL, 'notificationcandidat', 6, 4, '3727104052013_Pj_adresse.rtf', NULL, '2013-05-04', 'ddd', 'dd', 6, '2013-05-04'),
(24, NULL, NULL, 'notificationcandidat', 6, 4, '5427104052013_Pj_adresse.rtf', NULL, '2013-05-04', 'ddd', 'ddd', 7, '2013-05-04'),
(25, NULL, NULL, 'mavdossier', 6, 5, '0330104052013_Pj_adresse.rtf', 'Mise en validation DAO', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(26, NULL, NULL, 'validationdao', 6, 5, '5930104052013_Pj_adresse.rtf', 'Validation DAO', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(27, NULL, NULL, 'pmb_pvouverture', 6, 5, '5930104052013_Pj_adresse.rtf', 'PV d''ouverture', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(28, NULL, NULL, 'pv_evaluation', 6, 5, '0015304052013_Pj_adresse.rtf', 'PV d''�valuation', NULL, NULL, NULL, NULL, NULL),
(29, NULL, NULL, 'pv_resultatnegociation', 6, 5, '5722304052013_Pj_adresse.rtf', NULL, '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(30, NULL, NULL, 'pmb_attributions', 6, 5, '1631304052013_Pj_adresse.rtf', 'PV d''attribution ( provisoir et d�finitif )', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(31, NULL, NULL, 'mavattprov', 6, 5, '1352304052013_Pj_adresse.rtf', 'Mise en validation Attribution provisoire', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(32, NULL, NULL, 'validationdao', 6, 5, '5852304052013_Pj_motdepasse.rtf', 'Validation DAO', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(33, NULL, NULL, 'publicationattprov', 6, 5, '1554304052013_Pj_adresse.rtf', 'Mise en publication Attribution provisoire', NULL, NULL, NULL, NULL, NULL),
(34, NULL, NULL, 'signaturemarche', 6, 5, '0658304052013_Pj_adresse.rtf', 'Signature du march�', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(38, NULL, NULL, 'ajoutdossier', 5, 9, '5414804052013_Pj_adresse.rtf', 'Dossier d''appel d''offres', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(39, NULL, NULL, 'lettreinvitation', 5, 9, '1920804052013_Pj_adresse.rtf', 'Lettre d''invitation', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(40, NULL, NULL, 'pmb_pvouverture', 5, 9, '3659804052013_Pj_adresse.rtf', 'PV d''ouverture', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(41, NULL, NULL, 'pv_evaluation', 5, 9, '0829904052013_Pj_adresse.rtf', 'PV d''�valuation', NULL, NULL, NULL, NULL, NULL),
(42, NULL, NULL, 'pmb_attributions', 5, 9, '54581004052013_Pj_adresse.rtf', 'PV d''attribution ( provisoir et d�finitif )', '2013-05-04', NULL, NULL, NULL, '2013-05-04'),
(43, NULL, NULL, 'ajoutdossier', 7, 10, '1814905052013_Pj_adresse.rtf', 'Dossier d''appel d''offres', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(44, NULL, NULL, 'mavdossier', 7, 10, '5816905052013_Pj_adresse.rtf', 'Mise en validation DAO', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(45, NULL, NULL, 'validationdao', 7, 10, '3417905052013_Pj_adresse.rtf', 'Validation DAO', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(46, NULL, NULL, 'publicationdao', 7, 10, '3019905052013_Pj_adresse.rtf', 'Mise en publication DAO', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(47, NULL, NULL, 'pmb_pvouverture', 7, 10, '2737905052013_Pj_adresse.rtf', 'PV d''ouverture', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(48, NULL, NULL, 'pv_evaluation', 7, 10, '4339905052013_Pj_adresse.rtf', 'PV d''�valuation', NULL, NULL, NULL, NULL, NULL),
(49, 3, NULL, 'pmb_attributions', 7, 10, '4340905052013_Pj_adresse.rtf', NULL, '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(50, NULL, NULL, 'mavattprov', 7, 10, '1741905052013_Pj_adresse.rtf', 'Mise en validation Attribution provisoire', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(51, NULL, NULL, 'validationdao', 7, 10, '5641905052013_Pj_adresse.rtf', 'Validation DAO', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(52, NULL, NULL, 'pmb_attributionsp', 7, 10, '2942905052013_Pj_adresse.rtf', 'PV d''attribution ( provisoir et d�finitif )', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(53, NULL, NULL, 'mavattprov', 7, 10, '0644905052013_Pj_adresse.rtf', 'Mise en validation Attribution provisoire', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(54, NULL, NULL, 'validationdao', 7, 10, '5844905052013_Pj_adresse.rtf', 'Validation DAO', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(55, NULL, NULL, 'pmb_attributionsd', 7, 10, '0746905052013_Pj_adresse.rtf', 'Avis attribution d�finitive', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(56, NULL, NULL, 'pmb_pvouverture', 8, 11, '5658905052013_Pj_adresse.rtf', 'PV d''ouverture', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(57, 4, NULL, 'pmb_attributions', 8, 11, '19011005052013_Pj_adresse.rtf', 'PV d''attribution ( provisoir et d�finitif )', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(58, NULL, NULL, 'pmb_attributionsp', 8, 11, '42011005052013_Pj_adresse.rtf', 'PV d''attribution ( provisoir et d�finitif )', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(59, NULL, NULL, 'ajoutdossier', 9, 12, '0628205052013_Pj_adresse.rtf', 'Dossier d''appel d''offres', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(60, NULL, NULL, 'mavdossier', 9, 12, '3730205052013_Pj_adresse.rtf', 'Mise en validation DAO', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(61, NULL, NULL, 'validationdao', 9, 12, '1931205052013_Pj_adresse.rtf', 'Validation DAO', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(62, NULL, NULL, 'publicationdao', 9, 12, '3932205052013_Pj_adresse.rtf', 'Mise en publication DAO', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(63, NULL, NULL, 'pmb_pvouverture', 9, 12, '0840205052013_Pj_adresse.rtf', 'PV d''ouverture', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(64, NULL, NULL, 'pv_evaluation', 9, 12, '0443205052013_Pj_adresse.rtf', 'PV d''�valuation', NULL, NULL, NULL, NULL, NULL),
(65, 5, NULL, 'pmb_attributions', 9, 12, '3743205052013_Pj_adresse.rtf', NULL, '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(66, NULL, NULL, 'mavattprov', 9, 12, '0044205052013_Pj_adresse.rtf', 'Mise en validation Attribution provisoire', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(67, NULL, NULL, 'validationdao', 9, 12, '2744205052013_Pj_adresse.rtf', 'Validation DAO', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(68, NULL, NULL, 'pmb_attributionsp', 9, 12, '2245205052013_Pj_adresse.rtf', 'PV d''attribution ( provisoir et d�finitif )', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(69, NULL, NULL, 'mavattprov', 9, 12, '5346205052013_Pj_adresse.rtf', 'Mise en validation Attribution provisoire', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(70, NULL, NULL, 'validationdao', 9, 12, '5447205052013_Pj_adresse.rtf', 'Validation DAO', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(71, NULL, NULL, 'pmb_attributionsd', 9, 12, '0849205052013_Pj_adresse.rtf', 'Avis attribution d�finitive', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(72, NULL, NULL, 'pmb_pvouverture', 10, 13, '3307305052013_Pj_adresse.rtf', 'PV d''ouverture', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(73, 7, NULL, 'pmb_attributions', 10, 13, '2510305052013_Pj_adresse.rtf', 'PV d''attribution ( provisoir et d�finitif )', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(74, 6, NULL, 'pmb_attributions', 10, 13, '3527305052013_Pj_adresse.rtf', 'PV d''attribution ( provisoir et d�finitif )', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(75, NULL, NULL, 'pmb_attributionsp', 10, 13, '3728305052013_Pj_adresse.rtf', 'PV d''attribution ( provisoir et d�finitif )', '2013-05-05', NULL, NULL, NULL, '2013-05-05'),
(76, NULL, NULL, 'ajoutdossier', 11, 14, '2043906052013_Pj_recu.pdf', 'Dossier d''appel d''offres', '2013-05-06', NULL, NULL, NULL, '2013-05-06'),
(77, NULL, NULL, 'publicationdao', 11, 14, '0348906052013_Pj_recu.pdf', 'Mise en publication DAO', '2013-05-06', NULL, NULL, NULL, '2013-05-06'),
(78, NULL, NULL, 'pmb_pvouverture', 11, 14, '3027907052013_Pj_recu.pdf', 'PV d''ouverture', '2013-05-07', NULL, NULL, NULL, '2013-05-07'),
(79, NULL, NULL, 'pv_evaluation', 11, 14, '0006808052013_Pj_recu.pdf', 'PV d''�valuation', NULL, NULL, NULL, NULL, NULL),
(80, 8, NULL, 'pmb_attributions', 11, 14, '4506808052013_Pj_recu.pdf', NULL, '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(81, NULL, NULL, 'mavattprov', 11, 14, '0707808052013_Pj_recu.pdf', 'Mise en validation Attribution provisoire', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(82, NULL, NULL, 'validationdao', 11, 14, '0412808052013_Pj_recu.pdf', 'Rejet DAO', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(83, NULL, NULL, 'ajoutdossier', 13, 15, '2327808052013_Pj_recu.pdf', 'Dossier d''appel d''offres', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(84, NULL, NULL, 'mavdossier', 13, 15, '1029808052013_Pj_recu.pdf', 'Mise en validation DAO', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(85, NULL, NULL, 'validationdao', 13, 15, '4229808052013_Pj_recu.pdf', 'Validation DAO', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(86, NULL, NULL, 'publicationdao', 13, 15, '4458808052013_Pj_recu.pdf', 'Mise en publication DAO', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(87, NULL, NULL, 'pmb_pvouverture', 13, 15, '5805908052013_Pj_recu.pdf', 'PV d''ouverture', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(88, NULL, NULL, 'pv_evaluation', 13, 15, '4607908052013_Pj_recu.pdf', 'PV d''�valuation', NULL, NULL, NULL, NULL, NULL),
(89, 9, NULL, 'pmb_attributions', 13, 15, '1908908052013_Pj_recu.pdf', NULL, '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(90, NULL, NULL, 'mavattprov', 13, 15, '5008908052013_Pj_recu.pdf', 'Mise en validation Attribution provisoire', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(91, NULL, NULL, 'validationdao', 13, 15, '2309908052013_Pj_recu.pdf', 'Validation DAO', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(92, NULL, NULL, 'pmb_attributionsp', 13, 15, '0510908052013_Pj_recu.pdf', 'PV d''attribution ( provisoir et d�finitif )', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(93, NULL, NULL, 'mavattprov', 13, 15, '1011908052013_Pj_recu.pdf', 'Mise en validation Attribution provisoire', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(94, NULL, NULL, 'validationdao', 13, 15, '0512908052013_Pj_recu.pdf', 'Validation DAO', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(95, NULL, NULL, 'pmb_attributionsd', 13, 15, '1313908052013_Pj_recu.pdf', 'Avis attribution d�finitive', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(96, NULL, NULL, 'ajoutdossier', 14, 16, '0416908052013_Pj_recu.pdf', 'Dossier d''appel d''offres', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(97, NULL, NULL, 'publicationdao', 14, 16, '5316908052013_Pj_recu.pdf', 'Mise en publication DAO', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(98, NULL, NULL, 'notificationcandidat', 14, 16, '5721908052013_Pj_Document.rtf', NULL, '2013-05-08', 'sss', 'sss', 30, '2013-05-08'),
(99, NULL, NULL, 'notificationcandidat', 14, 16, '2622908052013_Pj_recu.pdf', NULL, '2013-05-08', 'sss', '  ssss', 29, '2013-05-08'),
(100, NULL, NULL, 'mavdossier', 14, 16, '5927908052013_Pj_recu.pdf', 'Mise en validation DAO', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(101, NULL, NULL, 'validationdao', 14, 16, '3728908052013_Pj_recu.pdf', 'Validation DAO', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(102, NULL, NULL, 'mavdossier', 14, 17, '5733908052013_Pj_recu.pdf', 'Mise en validation DAO', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(103, NULL, NULL, 'validationdao', 14, 17, '3134908052013_Pj_recu.pdf', 'Validation DAO', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(104, NULL, NULL, 'ajoutdossier', 15, 18, '35321008052013_Pj_adresse.rtf', 'Dossier d''appel d''offres', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(105, NULL, NULL, 'mavdossier', 15, 18, '01461008052013_Pj_adresse.rtf', 'Mise en validation DAO', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(106, NULL, NULL, 'validationdao', 15, 18, NULL, 'Rejet DAO', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(107, NULL, NULL, 'mavdossier', 15, 18, '49531008052013_Pj_adresse.rtf', 'Mise en validation DAO', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(108, NULL, NULL, 'validationdao', 15, 18, '22541008052013_Pj_adresse.rtf', 'Validation DAO', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(109, NULL, NULL, 'publicationdao', 15, 18, '51031108052013_Pj_adresse.rtf', 'Mise en publication DAO', '2013-05-08', NULL, NULL, NULL, '2013-05-08'),
(110, NULL, NULL, 'pmb_pvouverture', 15, 18, '02271009052013_Pj_recu.pdf', 'PV d''ouverture', '2013-05-09', NULL, NULL, NULL, '2013-05-09'),
(111, NULL, NULL, 'pv_evaluation', 15, 18, '4708009052013_Pj_recu.pdf', 'PV d''�valuation', NULL, NULL, NULL, NULL, NULL),
(112, 10, NULL, 'pmb_attributions', 15, 18, '3624009052013_Pj_recu.pdf', NULL, '2013-05-09', NULL, NULL, NULL, '2013-05-09'),
(113, 11, NULL, 'pmb_attributions', 15, 18, '2825009052013_Pj_recu.pdf', NULL, '2013-05-09', NULL, NULL, NULL, '2013-05-09'),
(114, NULL, NULL, 'mavattprov', 15, 18, '0726009052013_Pj_recu.pdf', 'Mise en validation Attribution provisoire', '2013-05-09', NULL, NULL, NULL, '2013-05-09'),
(115, NULL, NULL, 'validationdao', 15, 18, '3052009052013_Pj_recu.pdf', 'Rejet DAO', '2013-05-09', NULL, NULL, NULL, '2013-05-09'),
(116, NULL, NULL, 'mavattprov', 15, 18, '2754009052013_Pj_recu.pdf', 'Mise en validation Attribution provisoire', '2013-05-09', NULL, NULL, NULL, '2013-05-09'),
(117, NULL, NULL, 'validationdao', 15, 18, '0456009052013_Pj_recu.pdf', 'Validation DAO', '2013-05-09', NULL, NULL, NULL, '2013-05-09'),
(118, NULL, NULL, 'pmb_attributionsp', 15, 18, '0457009052013_Pj_recu.pdf', 'PV d''attribution ( provisoir et d�finitif )', '2013-05-09', NULL, NULL, NULL, '2013-05-09'),
(119, NULL, NULL, 'mavattprov', 15, 18, '5210109052013_Pj_recu.pdf', 'Mise en validation Attribution provisoire', '2013-05-09', NULL, NULL, NULL, '2013-05-09'),
(120, NULL, NULL, 'validationdao', 15, 18, '2625109052013_Pj_recu.pdf', 'Validation DAO', '2013-05-09', NULL, NULL, NULL, '2013-05-09'),
(121, NULL, NULL, 'pmb_attributionsd', 15, 18, '4427109052013_Pj_recu.pdf', 'Avis attribution d�finitive', '2013-05-09', NULL, NULL, NULL, '2013-05-09'),
(122, NULL, NULL, 'ajoutdossier', 16, 19, '2011509052013_Pj_recu.pdf', 'Dossier d''appel d''offres', '2013-05-09', NULL, NULL, NULL, '2013-05-09'),
(123, NULL, NULL, 'mavdossier', 16, 19, '4619509052013_Pj_recu.pdf', 'Mise en validation DAO', '2013-05-09', NULL, NULL, NULL, '2013-05-09'),
(124, NULL, NULL, 'validationdao', 16, 19, '48151009052013_Pj_recu.pdf', 'Validation DAO', '2013-05-09', NULL, NULL, NULL, '2013-05-09'),
(125, NULL, NULL, 'publicationdao', 16, 19, '47211009052013_Pj_recu.pdf', 'Mise en publication DAO', '2013-05-09', NULL, NULL, NULL, '2013-05-09'),
(126, NULL, NULL, 'notificationcandidat', 16, 19, '5443410052013_Pj_modele.doc', NULL, '2013-05-10', 'dd', 'ddd', 39, '2013-05-10'),
(127, NULL, NULL, 'notificationcandidat', 16, 19, '3344410052013_Pj_recu.pdf', NULL, '2013-05-10', 'ddd', 'ddd', 38, '2013-05-10'),
(128, NULL, NULL, 'mavdossier', 16, 20, '3547510052013_Pj_recu.pdf', 'Mise en validation DAO', '2013-05-10', NULL, NULL, NULL, '2013-05-10'),
(129, NULL, NULL, 'validationdao', 16, 20, '4651510052013_Pj_recu.pdf', 'Validation DAO', '2013-05-10', NULL, NULL, NULL, '2013-05-10'),
(130, NULL, NULL, 'pmb_pvouverture', 16, 20, '1325610052013_Pj_recu.pdf', 'PV d''ouverture', '2013-05-10', NULL, NULL, NULL, '2013-05-10'),
(131, NULL, NULL, 'pv_evaluation', 16, 20, '3322910052013_Pj_recu.pdf', 'PV d''�valuation', NULL, NULL, NULL, NULL, NULL),
(132, NULL, NULL, 'pv_resultatnegociation', 16, 20, '2935910052013_Pj_Document.rtf', NULL, '2013-05-10', NULL, NULL, NULL, '2013-05-10'),
(133, NULL, NULL, 'pmb_attributions', 16, 20, '5037910052013_Pj_modele.doc', 'PV d''attribution ( provisoir et d�finitif )', '2013-05-10', NULL, NULL, NULL, '2013-05-10'),
(134, NULL, NULL, 'mavattprov', 16, 20, '1438910052013_Pj_recu.pdf', 'Mise en validation Attribution provisoire', '2013-05-10', NULL, NULL, NULL, '2013-05-10'),
(135, NULL, NULL, 'validationdao', 16, 20, '0446910052013_Pj_recu.pdf', 'Validation DAO', '2013-05-10', NULL, NULL, NULL, '2013-05-10'),
(136, NULL, NULL, 'ajoutdossier', 17, 21, '03521110052013_Pj_recu.pdf', 'Dossier d''appel d''offres', '2013-05-10', NULL, NULL, NULL, '2013-05-10'),
(137, NULL, NULL, 'mavdossier', 17, 21, '49541110052013_Pj_recu.pdf', 'Mise en validation DAO', '2013-05-10', NULL, NULL, NULL, '2013-05-10'),
(138, NULL, NULL, 'validationdao', 17, 21, '37561110052013_Pj_recu.pdf', 'Validation DAO', '2013-05-10', NULL, NULL, NULL, '2013-05-10'),
(139, NULL, NULL, 'publicationdao', 17, 21, '42571110052013_Pj_recu.pdf', 'Mise en publication DAO', '2013-05-10', NULL, NULL, NULL, '2013-05-10'),
(140, NULL, NULL, 'notificationcandidat', 17, 21, '2911010052013_Pj_recu.pdf', NULL, '2013-05-10', 'ddd', 'dd', 44, '2013-05-10'),
(141, NULL, NULL, 'notificationcandidat', 17, 21, '4711010052013_Pj_recu.pdf', NULL, '2013-05-10', 'ddd', 'ddd', 45, '2013-05-10'),
(142, NULL, NULL, 'notificationcandidat', 17, 21, '0312010052013_Pj_recu.pdf', NULL, '2013-05-10', 'sss', 'ddd', 46, '2013-05-10'),
(143, NULL, NULL, 'mavdossier', 17, 22, '0920010052013_Pj_recu.pdf', 'Mise en validation DAO', '2013-05-10', NULL, NULL, NULL, '2013-05-10'),
(144, NULL, NULL, 'validationdao', 17, 22, '4920010052013_Pj_recu.pdf', 'Validation DAO', '2013-05-10', NULL, NULL, NULL, '2013-05-10');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_dossiercommissioncellules`
--

CREATE TABLE IF NOT EXISTS `pmb_dossiercommissioncellules` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `commissionmarche_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKFA31C2321E224C0` (`Dossiers_ID`),
  KEY `FKFA31C2366462C0B` (`commissionmarche_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_dossiercommissioncellules`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_dossiercommissionmarche`
--

CREATE TABLE IF NOT EXISTS `pmb_dossiercommissionmarche` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `etapePI` int(11) DEFAULT NULL,
  `flagpresenceevaluation` int(11) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `commissionmarche_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKDD7517BE21E224C0` (`Dossiers_ID`),
  KEY `FKDD7517BE260F6E03` (`commissionmarche_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Contenu de la table `pmb_dossiercommissionmarche`
--

INSERT INTO `pmb_dossiercommissionmarche` (`ID`, `etapePI`, `flagpresenceevaluation`, `Dossiers_ID`, `commissionmarche_ID`) VALUES
(1, 0, 1, 1, 1),
(2, 0, 1, 3, 1),
(3, 0, 1, 5, 2),
(4, 1, 1, 5, 2),
(5, 0, 1, 9, 2),
(6, 1, 1, 9, 2),
(7, 0, 1, 10, 1),
(8, 0, 1, 11, 1),
(9, 0, 1, 12, 1),
(10, 0, 1, 13, 1),
(11, 0, 1, 14, 3),
(12, 0, 1, 15, 3),
(13, 0, 1, 17, 3),
(14, 0, 1, 18, 5),
(15, 0, 1, 20, 3),
(16, 1, 1, 20, 3),
(17, 0, 1, 22, 6),
(18, 0, 1, 22, 7);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_dossierpiece`
--

CREATE TABLE IF NOT EXISTS `pmb_dossierpiece` (
  `dospiID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `Piece_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`dospiID`),
  KEY `FK17227AFD21E224C0` (`Dossiers_ID`),
  KEY `FK17227AFD4CEAE4D7` (`Piece_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Contenu de la table `pmb_dossierpiece`
--

INSERT INTO `pmb_dossierpiece` (`dospiID`, `Dossiers_ID`, `Piece_ID`) VALUES
(1, 1, 2),
(2, 1, 1),
(3, 3, 2),
(4, 5, 2),
(5, 9, 2),
(6, 10, 2),
(7, 11, 2),
(8, 11, 1),
(9, 12, 2),
(10, 12, 1),
(11, 13, 2),
(12, 13, 1),
(13, 14, 4),
(14, 14, 3),
(15, 15, 4),
(16, 17, 4),
(17, 18, 5),
(18, 18, 6),
(19, 20, 4),
(20, 22, 7);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_dossiers`
--

CREATE TABLE IF NOT EXISTS `pmb_dossiers` (
  `dosID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Autorite_ID` int(255) DEFAULT NULL,
  `commentaireDelaiTechniquecouverture` varchar(255) DEFAULT NULL,
  `dateLimiteDossierTechnique` date DEFAULT NULL,
  `dateRemiseDossierTechnique` date DEFAULT NULL,
  `dosAchevement` varchar(255) DEFAULT NULL,
  `dosAdresseRetrait` varchar(255) DEFAULT NULL,
  `dosAllotissement` varchar(100) DEFAULT NULL,
  `dosBonDeCommandeDrp` varchar(255) DEFAULT NULL,
  `dosBordereau` varchar(255) DEFAULT NULL,
  `dosBordereauPrequalif` varchar(255) DEFAULT NULL,
  `dosBudgetAnomalie` varchar(255) DEFAULT NULL,
  `dosCommentApprobation` varchar(255) DEFAULT NULL,
  `dosCommentNotification` varchar(255) DEFAULT NULL,
  `dosCommentOrdreDemarrage` varchar(255) DEFAULT NULL,
  `dosCommentRecepDefinitive` varchar(255) DEFAULT NULL,
  `dosCommentRecepProvisoire` varchar(255) DEFAULT NULL,
  `dosCommentSignature` varchar(255) DEFAULT NULL,
  `dosCommentaireAttestationcredit` varchar(255) DEFAULT NULL,
  `dosCommentaireAttributionDefinitive` varchar(255) DEFAULT NULL,
  `dosCommentaireAttributionProvisoire` varchar(255) DEFAULT NULL,
  `dosCommentaireEnvoiDp` varchar(255) DEFAULT NULL,
  `dosCommentaireLettreInvitation` varchar(255) DEFAULT NULL,
  `dosCommentaireMiseValidation` varchar(255) DEFAULT NULL,
  `dosCommentaireMiseValidationPrequalif` varchar(255) DEFAULT NULL,
  `dosCommentairePublication` varchar(255) DEFAULT NULL,
  `dosCommentairePublicationProvisoire` varchar(255) DEFAULT NULL,
  `dosCommentaireValidation` varchar(255) DEFAULT NULL,
  `dosCommentaireValidationPrequalif` varchar(255) DEFAULT NULL,
  `dosCommentairesPublicationDefinitive` varchar(255) DEFAULT NULL,
  `dosConditionAcquistion` varchar(255) DEFAULT NULL,
  `dosDateApprobation` date DEFAULT NULL,
  `dosDateAttributionDefinitive` date DEFAULT NULL,
  `dosDateAttributionProvisoire` date DEFAULT NULL,
  `dosDateBonDeCommandeDrp` date DEFAULT NULL,
  `dosDateCreation` date DEFAULT NULL,
  `dosDateDebutRetrait` date DEFAULT NULL,
  `dosDateEnvoiDp` date DEFAULT NULL,
  `dosDateLimiteDepot` date DEFAULT NULL,
  `dosDateMiseValidation` date DEFAULT NULL,
  `dosDateMiseValidationPrequalif` date DEFAULT NULL,
  `dosDateMiseValidationSignature` date DEFAULT NULL,
  `dosDateNotification` date DEFAULT NULL,
  `dosDateOrdreDemarrage` date DEFAULT NULL,
  `dosDateOuvertueDesplis` date DEFAULT NULL,
  `dosDatePublication` date DEFAULT NULL,
  `dosDatePublicationDefinitive` date DEFAULT NULL,
  `dosDatePublicationProvisoire` date DEFAULT NULL,
  `dosDateRecepDefinitive` date DEFAULT NULL,
  `dosDateRecepProvisoire` date DEFAULT NULL,
  `dosDateRejet` date DEFAULT NULL,
  `dosDateRejetPrequalif` date DEFAULT NULL,
  `dosDateRejetSignature` date DEFAULT NULL,
  `dosDateSignature` date DEFAULT NULL,
  `dosDateValidation` date DEFAULT NULL,
  `dosDateValidationPrequalif` date DEFAULT NULL,
  `dosDateValidationSignature` date DEFAULT NULL,
  `dosDescriptif` varchar(255) DEFAULT NULL,
  `dosDownloadFichier` varchar(255) DEFAULT NULL,
  `dosEtatValidation` int(11) DEFAULT NULL,
  `dosEtatValidationPrequalif` int(11) DEFAULT NULL,
  `dosFichier` varchar(255) DEFAULT NULL,
  `dosFichierBonDeCommandeDrp` varchar(255) DEFAULT NULL,
  `dosFichierEvaluation` varchar(255) DEFAULT NULL,
  `dosFichierMiseValidationPrequalif` varchar(255) DEFAULT NULL,
  `dosFichierOrdreDemarrage` varchar(255) DEFAULT NULL,
  `dosFichierRecepDefinitive` varchar(255) DEFAULT NULL,
  `dosFichierRecepProvisoire` varchar(255) DEFAULT NULL,
  `dosFichierValidation` varchar(255) DEFAULT NULL,
  `dosFichierValidationPrequalif` varchar(255) DEFAULT NULL,
  `dosFonctionResp` varchar(255) DEFAULT NULL,
  `dosHeureOuvertureDesPlis` time DEFAULT NULL,
  `dosHeurelimitedepot` time DEFAULT NULL,
  `dosLettreInvitationDrp` varchar(255) DEFAULT NULL,
  `dosLieuDepotDossier` varchar(255) DEFAULT NULL,
  `dosLieuOuvertureDesPlis` varchar(255) DEFAULT NULL,
  `dosLotDivisible` varchar(3) DEFAULT NULL,
  `dosMailResp` varchar(255) DEFAULT NULL,
  `dosNatureMarche` int(11) DEFAULT NULL,
  `dosNomREsp` varchar(255) DEFAULT NULL,
  `dosNombreLots` int(11) DEFAULT NULL,
  `dosNoteEliminatoireAmi` int(11) DEFAULT NULL,
  `dosObjetTextAvisAttributiondefinitif` varchar(255) DEFAULT NULL,
  `dosObjetTextAvisAttributionprovisoire` varchar(255) DEFAULT NULL,
  `dosPrenomResp` varchar(255) DEFAULT NULL,
  `dosRefApprobation` varchar(255) DEFAULT NULL,
  `dosRefAttributionDefinitive` varchar(255) DEFAULT NULL,
  `dosRefAttributionProvisoire` varchar(255) DEFAULT NULL,
  `dosRefNotification` varchar(255) DEFAULT NULL,
  `dosRefSignature` varchar(255) DEFAULT NULL,
  `dosReference` varchar(100) DEFAULT NULL,
  `dosSoumission` int(11) DEFAULT NULL,
  `dosTelResp` varchar(255) DEFAULT NULL,
  `dosTextAvisAttributiondefinitive` varchar(255) DEFAULT NULL,
  `dosTextAvisAttributionprovisoire` varchar(255) DEFAULT NULL,
  `dosdateValidAttestationcredit` date DEFAULT NULL,
  `dosdateValidAttestationcreditA` date DEFAULT NULL,
  `dosfichierSignature` varchar(255) DEFAULT NULL,
  `etatPrequalif` int(11) DEFAULT NULL,
  `ponderationfinanciere` decimal(19,2) DEFAULT NULL,
  `ponderationtechnique` decimal(19,2) DEFAULT NULL,
  `texte_html` varchar(255) DEFAULT NULL,
  `unitedelaicomplementpieces` varchar(255) DEFAULT NULL,
  `valeurdelaicomplementpieces` int(11) DEFAULT NULL,
  `Appelsoffres_ID` bigint(20) DEFAULT NULL,
  `Plan_ID` bigint(20) DEFAULT NULL,
  `nat_natCode` bigint(20) DEFAULT NULL,
  `commentaireDelaiTechnique` varchar(255) DEFAULT NULL,
  `dosdatenonobjectionptf` date DEFAULT NULL,
  `dosmontantdao` int(11) DEFAULT NULL,
  `autorite` tinyblob,
  `dosIncidents` varchar(255) DEFAULT NULL,
  `dosDateMiseValidationattribution` date DEFAULT NULL,
  `dosdateinvitation` date DEFAULT NULL,
  `dosponderationfinanciere` int(11) DEFAULT NULL,
  `dosponderationtechnique` int(11) DEFAULT NULL,
  `dosmontantgarantie` decimal(19,2) DEFAULT NULL,
  `dosmontant` decimal(19,2) DEFAULT NULL,
  `doscommentairesdemandepublication` varchar(255) DEFAULT NULL,
  `dosdatedemandepublication` date DEFAULT NULL,
  `dosfichierdemandepublication` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`dosID`),
  KEY `FK9B27500250D62C5D` (`Plan_ID`),
  KEY `FK9B27500225D5960` (`Appelsoffres_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Contenu de la table `pmb_dossiers`
--

INSERT INTO `pmb_dossiers` (`dosID`, `Autorite_ID`, `commentaireDelaiTechniquecouverture`, `dateLimiteDossierTechnique`, `dateRemiseDossierTechnique`, `dosAchevement`, `dosAdresseRetrait`, `dosAllotissement`, `dosBonDeCommandeDrp`, `dosBordereau`, `dosBordereauPrequalif`, `dosBudgetAnomalie`, `dosCommentApprobation`, `dosCommentNotification`, `dosCommentOrdreDemarrage`, `dosCommentRecepDefinitive`, `dosCommentRecepProvisoire`, `dosCommentSignature`, `dosCommentaireAttestationcredit`, `dosCommentaireAttributionDefinitive`, `dosCommentaireAttributionProvisoire`, `dosCommentaireEnvoiDp`, `dosCommentaireLettreInvitation`, `dosCommentaireMiseValidation`, `dosCommentaireMiseValidationPrequalif`, `dosCommentairePublication`, `dosCommentairePublicationProvisoire`, `dosCommentaireValidation`, `dosCommentaireValidationPrequalif`, `dosCommentairesPublicationDefinitive`, `dosConditionAcquistion`, `dosDateApprobation`, `dosDateAttributionDefinitive`, `dosDateAttributionProvisoire`, `dosDateBonDeCommandeDrp`, `dosDateCreation`, `dosDateDebutRetrait`, `dosDateEnvoiDp`, `dosDateLimiteDepot`, `dosDateMiseValidation`, `dosDateMiseValidationPrequalif`, `dosDateMiseValidationSignature`, `dosDateNotification`, `dosDateOrdreDemarrage`, `dosDateOuvertueDesplis`, `dosDatePublication`, `dosDatePublicationDefinitive`, `dosDatePublicationProvisoire`, `dosDateRecepDefinitive`, `dosDateRecepProvisoire`, `dosDateRejet`, `dosDateRejetPrequalif`, `dosDateRejetSignature`, `dosDateSignature`, `dosDateValidation`, `dosDateValidationPrequalif`, `dosDateValidationSignature`, `dosDescriptif`, `dosDownloadFichier`, `dosEtatValidation`, `dosEtatValidationPrequalif`, `dosFichier`, `dosFichierBonDeCommandeDrp`, `dosFichierEvaluation`, `dosFichierMiseValidationPrequalif`, `dosFichierOrdreDemarrage`, `dosFichierRecepDefinitive`, `dosFichierRecepProvisoire`, `dosFichierValidation`, `dosFichierValidationPrequalif`, `dosFonctionResp`, `dosHeureOuvertureDesPlis`, `dosHeurelimitedepot`, `dosLettreInvitationDrp`, `dosLieuDepotDossier`, `dosLieuOuvertureDesPlis`, `dosLotDivisible`, `dosMailResp`, `dosNatureMarche`, `dosNomREsp`, `dosNombreLots`, `dosNoteEliminatoireAmi`, `dosObjetTextAvisAttributiondefinitif`, `dosObjetTextAvisAttributionprovisoire`, `dosPrenomResp`, `dosRefApprobation`, `dosRefAttributionDefinitive`, `dosRefAttributionProvisoire`, `dosRefNotification`, `dosRefSignature`, `dosReference`, `dosSoumission`, `dosTelResp`, `dosTextAvisAttributiondefinitive`, `dosTextAvisAttributionprovisoire`, `dosdateValidAttestationcredit`, `dosdateValidAttestationcreditA`, `dosfichierSignature`, `etatPrequalif`, `ponderationfinanciere`, `ponderationtechnique`, `texte_html`, `unitedelaicomplementpieces`, `valeurdelaicomplementpieces`, `Appelsoffres_ID`, `Plan_ID`, `nat_natCode`, `commentaireDelaiTechnique`, `dosdatenonobjectionptf`, `dosmontantdao`, `autorite`, `dosIncidents`, `dosDateMiseValidationattribution`, `dosdateinvitation`, `dosponderationfinanciere`, `dosponderationtechnique`, `dosmontantgarantie`, `dosmontant`, `doscommentairesdemandepublication`, `dosdatedemandepublication`, `dosfichierdemandepublication`) VALUES
(1, 6, NULL, '2013-04-29', '2013-04-29', '1958801052013_Pj_adresse.rtf', NULL, NULL, NULL, 'ddd', NULL, NULL, '', 'ccc', '', '', '', NULL, NULL, 'ccc', '', NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, NULL, '2013-04-29', '2013-04-29', '2013-04-29', NULL, '2013-04-28', NULL, NULL, '2013-04-28', '2013-04-28', NULL, '2013-04-29', '2013-04-29', '2013-05-01', '2013-04-28', '2013-04-28', '2013-04-29', '2013-04-29', '2013-05-01', '2013-05-01', NULL, NULL, NULL, '2013-04-29', '2013-04-28', '2013-04-29', '2013-04-29', NULL, NULL, 1, 0, '0510628042013_Pj_02-ECBU.pdf', NULL, NULL, '2139829042013_Pj_adresse.rtf', '5056801052013_Pj_adresse.rtf', '1358801052013_Pj_adresse.rtf', '5957801052013_Pj_adresse.rtf', '1540829042013_Pj_adresse.rtf', NULL, NULL, '20:00:00', '14:00:00', NULL, 'Dakar', 'Dakar', 'NON', NULL, 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'cc', NULL, 'T_DAGE_0001', 0, NULL, NULL, NULL, NULL, NULL, '2538829042013_Pj_adresse.rtf', 0, NULL, NULL, '1819628042013_Pj_02-ECBU.pdf', NULL, 0, 1, 1, 2, '', NULL, 100000, NULL, 'ras', '2013-04-29', NULL, 0, 0, '25000000.00', '500000000.00', '', '2013-04-28', '5218628042013_Pj_02-ECBU.pdf'),
(2, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'www', NULL, NULL, '', '', NULL, NULL, NULL, 'ff', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, NULL, NULL, '2013-05-04', '2013-05-04', NULL, NULL, '2013-05-04', NULL, NULL, NULL, '2013-05-04', NULL, NULL, '2013-05-04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-04', '2013-05-04', NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, '1714104052013_Pj_adresse.rtf', NULL, NULL, NULL, '0717104052013_Pj_adresse.rtf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '23S', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, '0713104052013_Pj_adresse.rtf', 0, NULL, NULL, NULL, NULL, 0, 2, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL),
(3, 6, NULL, NULL, NULL, '15221104052013_Pj_adresse.rtf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ss', '', '', '', NULL, NULL, NULL, 'ccc', NULL, 'zzz', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-04', NULL, NULL, '2013-05-04', NULL, NULL, '2013-05-08', NULL, NULL, '2013-05-04', '2013-05-04', '2013-05-04', NULL, NULL, '2013-05-04', '2013-05-04', '2013-05-04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '1512904052013_Pj_adresse.rtf', NULL, NULL, NULL, '43211104052013_Pj_adresse.rtf', '08221104052013_Pj_adresse.rtf', '53211104052013_Pj_adresse.rtf', NULL, NULL, NULL, '18:00:00', NULL, '09221004052013_Pj_adresse.rtf', NULL, 'Dakar', 'NON', NULL, 0, NULL, 1, 60, NULL, NULL, NULL, NULL, NULL, NULL, 'ss', NULL, 'T_DAGE_0002', 2, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 3, 2, 2, NULL, NULL, NULL, NULL, 'ras', NULL, '2013-05-04', 0, 0, NULL, NULL, NULL, NULL, NULL),
(4, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ddd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ffff', NULL, 'fff', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-04', NULL, NULL, '2013-05-04', '2013-05-04', NULL, NULL, '2013-05-04', NULL, NULL, '2013-05-04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-04', NULL, NULL, NULL, NULL, 1, 0, '0049004052013_Pj_adresse.rtf', NULL, NULL, '4649004052013_Pj_adresse.rtf', NULL, NULL, NULL, '2050004052013_Pj_adresse.rtf', NULL, NULL, NULL, '12:00:00', NULL, 'Dakar', NULL, NULL, NULL, 0, NULL, 0, 70, NULL, '5850004052013_Pj_adresse.rtf', NULL, NULL, NULL, NULL, NULL, NULL, 'PI_DAGE_0013', 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 6, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL),
(5, 6, NULL, '2013-05-04', '2013-05-04', '0754812052013_Pj_recu.pdf', NULL, NULL, NULL, 'ff', NULL, NULL, 'sss', 'ddd', '', '', '', NULL, NULL, 'ras', '', NULL, NULL, 'fff', NULL, NULL, NULL, 'ddd', NULL, NULL, NULL, '2013-05-04', '2013-05-04', '2013-05-04', NULL, '2013-05-04', NULL, NULL, '2013-05-04', '2013-05-04', NULL, NULL, '2013-05-04', '2013-05-12', '2013-05-04', NULL, '2013-05-04', '2013-05-04', '2013-05-12', '2013-05-12', NULL, NULL, NULL, '2013-05-04', '2013-05-04', '2013-05-04', NULL, NULL, NULL, 1, 0, '3028104052013_Pj_adresse.rtf', NULL, NULL, '1352304052013_Pj_adresse.rtf', '3853812052013_Pj_recu.pdf', '0154812052013_Pj_recu.pdf', '4953812052013_Pj_recu.pdf', '5852304052013_Pj_motdepasse.rtf', NULL, NULL, '15:00:00', '14:00:00', NULL, 'Dakar', 'Dakar', NULL, NULL, 0, NULL, 0, 70, NULL, NULL, NULL, NULL, NULL, NULL, 'dd', NULL, 'PI_DAGE_0013', 0, NULL, NULL, NULL, NULL, NULL, '0658304052013_Pj_adresse.rtf', 2, NULL, NULL, '1554304052013_Pj_adresse.rtf', NULL, 0, 6, 13, NULL, '', NULL, NULL, NULL, 'ras', '2013-05-04', NULL, 30, 70, NULL, NULL, NULL, NULL, NULL),
(9, 6, NULL, '2013-05-04', '2013-05-04', '2153812052013_Pj_recu.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, 'ss', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-04', NULL, NULL, '2013-05-04', NULL, NULL, '2013-05-08', NULL, NULL, '2013-05-04', '2013-05-12', '2013-05-04', NULL, NULL, NULL, '2013-05-12', '2013-05-12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '5414804052013_Pj_adresse.rtf', NULL, NULL, NULL, '5152812052013_Pj_recu.pdf', '1553812052013_Pj_recu.pdf', '0253812052013_Pj_recu.pdf', NULL, NULL, NULL, '19:00:00', NULL, '1920804052013_Pj_adresse.rtf', NULL, 'Dakar', NULL, NULL, 0, NULL, 0, 70, NULL, NULL, NULL, NULL, NULL, NULL, 'sss', NULL, 'PI_DAGE_0014', 2, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 5, 14, 2, '', NULL, NULL, NULL, 'ras', NULL, '2013-05-04', 30, 70, NULL, NULL, NULL, NULL, NULL),
(10, 6, NULL, '2013-05-05', '2013-05-04', '1549812052013_Pj_recu.pdf', NULL, NULL, NULL, 'ss', NULL, NULL, '', '', '', '', '', NULL, NULL, 'sss', '', NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, NULL, '2013-05-05', '2013-05-05', '2013-05-05', NULL, '2013-05-05', NULL, NULL, '2013-05-05', '2013-05-05', NULL, '2013-05-05', '2013-05-05', '2013-05-12', '2013-05-05', '2013-05-05', '2013-05-05', '2013-05-05', '2013-05-12', '2013-05-12', NULL, NULL, NULL, '2013-05-05', '2013-05-05', '2013-05-05', '2013-05-05', NULL, NULL, 1, 0, '1814905052013_Pj_adresse.rtf', NULL, NULL, '0644905052013_Pj_adresse.rtf', '2648812052013_Pj_recu.pdf', '0949812052013_Pj_recu.pdf', '4748812052013_Pj_recu.pdf', '5844905052013_Pj_adresse.rtf', NULL, NULL, '20:00:00', '12:00:00', NULL, 'Dakar', 'Dakar', 'NON', NULL, 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'sss', NULL, 'F_DAGE_0015', 0, NULL, NULL, NULL, NULL, NULL, '1143905052013_Pj_adresse.rtf', 0, NULL, NULL, '3019905052013_Pj_adresse.rtf', NULL, 0, 7, 15, 2, '', NULL, 400000, NULL, 'ras', '2013-05-05', NULL, 0, 0, '750000000.00', '5000000000.00', '', '2013-05-05', '0618905052013_Pj_adresse.rtf'),
(11, 6, NULL, NULL, NULL, '4150812052013_Pj_recu.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ss', '', '', '', NULL, NULL, NULL, '', NULL, 'sss', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-05', NULL, NULL, '2013-05-05', NULL, NULL, '2013-05-08', NULL, NULL, '2013-05-05', '2013-05-12', '2013-05-05', NULL, NULL, '2013-05-05', '2013-05-12', '2013-05-12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2948905052013_Pj_adresse.rtf', NULL, NULL, NULL, '1550812052013_Pj_recu.pdf', '3550812052013_Pj_recu.pdf', '2550812052013_Pj_recu.pdf', NULL, NULL, NULL, '12:00:00', NULL, '0352905052013_Pj_adresse.rtf', NULL, 'Dakar', 'NON', NULL, 0, NULL, 1, 70, NULL, NULL, NULL, NULL, NULL, NULL, 'ss', NULL, 'F_DAGE_0016', 3, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 8, 16, 2, NULL, NULL, NULL, NULL, 'ras', NULL, '2013-05-05', 0, 0, NULL, NULL, NULL, NULL, NULL),
(12, 6, NULL, '2013-05-05', '2013-05-05', '2951812052013_Pj_recu.pdf', NULL, NULL, NULL, 'ss', NULL, NULL, '', '', '', '', '', NULL, NULL, 'sss', '', NULL, NULL, 'ss', NULL, '', NULL, '', NULL, NULL, NULL, '2013-05-05', '2013-05-05', '2013-05-05', NULL, '2013-05-05', NULL, NULL, '2013-05-05', '2013-05-05', NULL, '2013-05-05', '2013-05-05', '2013-05-12', '2013-05-05', '2013-05-05', '2013-05-05', '2013-05-05', '2013-05-12', '2013-05-12', NULL, NULL, NULL, '2013-05-05', '2013-05-05', '2013-05-05', '2013-05-05', NULL, NULL, 1, 0, '0628205052013_Pj_adresse.rtf', NULL, NULL, '5346205052013_Pj_adresse.rtf', '0151812052013_Pj_recu.pdf', '2351812052013_Pj_recu.pdf', '1151812052013_Pj_recu.pdf', '5447205052013_Pj_adresse.rtf', NULL, NULL, '18:00:00', '12:00:00', NULL, 'Dakar', 'Dakar', 'NON', NULL, 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'sss', NULL, 'S_DAGE_0019', 0, NULL, NULL, NULL, NULL, NULL, '3146205052013_Pj_adresse.rtf', 0, NULL, NULL, '3932205052013_Pj_adresse.rtf', NULL, 0, 9, 19, 2, '', NULL, 5000000, NULL, 'ras', '2013-05-05', NULL, 0, 0, '20000000.00', '400000000.00', '', '2013-05-05', '0032205052013_Pj_adresse.rtf'),
(13, 6, NULL, NULL, NULL, '1852812052013_Pj_recu.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sss', '', '', '', NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-05', '2013-05-05', NULL, NULL, '2013-05-05', NULL, NULL, '2013-05-08', NULL, NULL, '2013-05-05', '2013-05-12', '2013-05-05', NULL, NULL, '2013-05-05', '2013-05-12', '2013-05-12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '3251205052013_Pj_adresse.rtf', NULL, NULL, NULL, '4751812052013_Pj_recu.pdf', '1152812052013_Pj_recu.pdf', '5851812052013_Pj_recu.pdf', NULL, NULL, NULL, '19:00:00', NULL, '2660205052013_Pj_adresse.rtf', NULL, 'Dakar', 'OUI', NULL, 0, NULL, 2, 70, NULL, NULL, NULL, NULL, NULL, NULL, 's', NULL, 'S_DAGE_0020', 3, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, 10, 20, 2, NULL, NULL, NULL, NULL, 'ras', NULL, '2013-05-04', 0, 0, NULL, NULL, NULL, NULL, NULL),
(14, 902, NULL, '2013-05-08', '2013-05-08', NULL, NULL, NULL, NULL, 'ss', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sss', NULL, '', NULL, 'xxxx', NULL, NULL, NULL, NULL, NULL, '2013-05-08', NULL, '2013-05-06', NULL, NULL, '2013-05-06', '2013-05-08', NULL, NULL, NULL, NULL, '2013-05-06', '2013-05-06', NULL, NULL, NULL, NULL, '2013-05-08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2043906052013_Pj_recu.pdf', NULL, NULL, '0707808052013_Pj_recu.pdf', NULL, NULL, NULL, '0412808052013_Pj_recu.pdf', NULL, NULL, '23:00:00', '14:00:00', NULL, 'Dakar', 'Dakar', 'NON', NULL, 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'T_DAGE_0026', 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, '0348906052013_Pj_recu.pdf', NULL, 0, 11, 26, 2, 'ddd', NULL, 70000, NULL, 'ras', NULL, NULL, 0, 0, '0.00', '10000000.00', NULL, NULL, NULL),
(15, 902, NULL, '2013-05-08', '2013-05-08', '3039812052013_Pj_recu.pdf', NULL, NULL, NULL, 'cc', NULL, NULL, '', '', '', '', '', NULL, NULL, 'xxx', 'xxx', NULL, NULL, 'ccc', NULL, '', NULL, 'xxx', NULL, NULL, NULL, '2013-05-08', '2013-05-08', '2013-05-08', NULL, '2013-05-08', NULL, NULL, '2013-05-08', '2013-05-08', NULL, '2013-05-08', '2013-05-08', '2013-05-12', '2013-05-08', '2013-05-08', '2013-05-08', '2013-05-08', '2013-05-12', '2013-05-12', NULL, NULL, NULL, '2013-05-08', '2013-05-08', '2013-05-08', '2013-05-08', NULL, NULL, 1, 0, '2327808052013_Pj_recu.pdf', NULL, NULL, '1011908052013_Pj_recu.pdf', '0039812052013_Pj_recu.pdf', '2439812052013_Pj_recu.pdf', '1339812052013_Pj_recu.pdf', '0512908052013_Pj_recu.pdf', NULL, NULL, '20:00:00', '12:00:00', NULL, 'Dakar', 'Dakar', 'NON', NULL, 0, NULL, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'xxx', NULL, 'T_DAGE_0047', 0, NULL, NULL, NULL, NULL, NULL, '5310908052013_Pj_recu.pdf', 0, NULL, NULL, '4458808052013_Pj_recu.pdf', NULL, 0, 13, 47, 2, '', NULL, 100000, NULL, 'ras', '2013-05-08', NULL, 0, 0, '18000000.00', '600000000.00', '', '2013-05-08', '3058808052013_Pj_recu.pdf'),
(16, 902, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sss', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ddd', NULL, 'ss', NULL, 'xxx', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-08', NULL, NULL, '2013-05-08', '2013-05-08', NULL, NULL, '2013-05-08', NULL, NULL, '2013-05-08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-08', NULL, NULL, NULL, NULL, 1, 0, '0416908052013_Pj_recu.pdf', NULL, NULL, '5927908052013_Pj_recu.pdf', NULL, NULL, NULL, '3728908052013_Pj_recu.pdf', NULL, NULL, NULL, '12:00:00', NULL, 'Dakar', NULL, NULL, NULL, 0, NULL, 0, 70, NULL, '5316908052013_Pj_recu.pdf', NULL, NULL, NULL, NULL, NULL, NULL, 'PI_DAGE_0034', 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 14, 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL),
(17, 902, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sss', NULL, NULL, NULL, 'ww', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-08', NULL, NULL, '2013-05-08', '2013-05-08', NULL, NULL, NULL, NULL, '2013-05-08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-08', NULL, NULL, NULL, NULL, 1, 0, '5029908052013_Pj_recu.pdf', NULL, NULL, '5733908052013_Pj_recu.pdf', NULL, NULL, NULL, '3134908052013_Pj_recu.pdf', NULL, NULL, '12:00:00', '12:00:00', NULL, 'Dakar', 'Dakar', NULL, NULL, 0, NULL, 0, 70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PI_DAGE_0034', 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, 0, 14, 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30, 70, NULL, NULL, NULL, NULL, NULL),
(18, 9, NULL, '2013-05-09', '2013-05-09', '1143812052013_Pj_recu.pdf', NULL, NULL, NULL, 'dfdf', NULL, NULL, '', '', '', 'ss', '', NULL, NULL, 'ras', '', NULL, NULL, '', NULL, '', NULL, '', NULL, NULL, NULL, '2013-05-09', '2013-05-09', '2013-05-09', NULL, '2013-05-08', NULL, NULL, '2013-05-08', '2013-05-08', NULL, '2013-05-09', '2013-05-09', '2013-05-12', '2013-05-08', '2013-05-08', '2013-05-09', '2013-05-09', '2013-05-12', '2013-05-12', '2013-05-09', NULL, NULL, '2013-05-09', '2013-05-08', '2013-05-09', '2013-05-09', NULL, NULL, 1, 0, '35321008052013_Pj_adresse.rtf', NULL, NULL, '5210109052013_Pj_recu.pdf', '3840812052013_Pj_recu.pdf', '0443812052013_Pj_recu.pdf', '4940812052013_Pj_recu.pdf', '2625109052013_Pj_recu.pdf', NULL, NULL, '18:00:00', '23:30:00', NULL, 'Lom�', 'Dakar', 'OUI', NULL, 0, NULL, 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'sss', NULL, 'T_DNCMP_0038', 0, NULL, NULL, NULL, NULL, NULL, '3210109052013_Pj_recu.pdf', 0, NULL, NULL, '51031108052013_Pj_adresse.rtf', NULL, 0, 15, 38, 2, '', NULL, 100000, NULL, NULL, '2013-05-09', NULL, 0, 0, '5000000.00', '500000000.00', '', '2013-05-08', '41011108052013_Pj_adresse.rtf'),
(19, 902, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ddd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ras', NULL, 'dd', NULL, 'ras', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-09', NULL, NULL, '2013-05-09', '2013-05-09', NULL, NULL, '2013-05-03', NULL, NULL, '2013-05-09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-09', NULL, NULL, NULL, NULL, 1, 0, '2011509052013_Pj_recu.pdf', NULL, NULL, '4619509052013_Pj_recu.pdf', NULL, NULL, NULL, '48151009052013_Pj_recu.pdf', NULL, NULL, NULL, '23:30:00', NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 70, NULL, '47211009052013_Pj_recu.pdf', NULL, NULL, NULL, NULL, NULL, NULL, 'PI_DAGE_0048', 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 16, 48, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL),
(20, 902, NULL, '2013-05-10', '2013-05-10', NULL, NULL, NULL, NULL, 'ss', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ss', NULL, NULL, NULL, 'ras', NULL, NULL, NULL, NULL, NULL, '2013-05-10', NULL, '2013-05-10', NULL, NULL, '2013-05-10', '2013-05-10', NULL, NULL, NULL, NULL, '2013-05-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-10', '2013-05-10', NULL, NULL, NULL, 1, 0, '2534510052013_Pj_table.rtf', NULL, NULL, '1438910052013_Pj_recu.pdf', NULL, NULL, NULL, '0446910052013_Pj_recu.pdf', NULL, NULL, '14:00:00', '13:00:00', NULL, 'Dakar', 'Dakar', NULL, NULL, 0, NULL, 0, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PI_DAGE_0048', 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, 0, 16, 48, NULL, '', NULL, NULL, NULL, 'ras', '2013-05-10', NULL, 30, 70, NULL, NULL, NULL, NULL, NULL),
(21, 905, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sss', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'M', NULL, 'REt', NULL, 'valid�', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-10', NULL, NULL, '2013-05-10', '2013-05-10', NULL, NULL, '2013-05-10', NULL, NULL, '2013-05-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-10', NULL, NULL, NULL, NULL, 1, 0, '03521110052013_Pj_recu.pdf', NULL, NULL, '49541110052013_Pj_recu.pdf', NULL, NULL, NULL, '37561110052013_Pj_recu.pdf', NULL, NULL, NULL, '12:00:00', NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 70, NULL, '42571110052013_Pj_recu.pdf', NULL, NULL, NULL, NULL, NULL, NULL, 'PI_DER_0051', 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 17, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL),
(22, 905, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-10', NULL, NULL, '2013-05-10', '2013-05-10', NULL, NULL, NULL, NULL, '2013-05-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-10', NULL, NULL, NULL, NULL, 1, 0, '4812010052013_Pj_recu.pdf', NULL, NULL, '0920010052013_Pj_recu.pdf', NULL, NULL, NULL, '4920010052013_Pj_recu.pdf', NULL, NULL, '14:00:00', '13:00:00', NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, 70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'PI_DER_0051', 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, 0, 17, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 30, 70, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_dossiersouscritere`
--

CREATE TABLE IF NOT EXISTS `pmb_dossiersouscritere` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `note` decimal(19,2) DEFAULT NULL,
  `Souscritere_ID` bigint(20) DEFAULT NULL,
  `Dossier_ID` bigint(20) DEFAULT NULL,
  `LOT` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK4EE1EED33F6CC61D` (`Dossier_ID`),
  KEY `FK4EE1EED3B74B3DAE` (`Souscritere_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Contenu de la table `pmb_dossiersouscritere`
--

INSERT INTO `pmb_dossiersouscritere` (`ID`, `note`, `Souscritere_ID`, `Dossier_ID`, `LOT`) VALUES
(1, NULL, 1, 1, 1),
(2, NULL, 1, 3, 2),
(3, '100.00', 1, 4, NULL),
(4, '100.00', 1, 5, NULL),
(5, '100.00', 1, 9, NULL),
(6, NULL, 1, 10, 3),
(7, NULL, 1, 11, 4),
(8, NULL, 1, 12, 5),
(9, NULL, 1, 13, 6),
(10, NULL, 1, 13, 7),
(11, NULL, 3, 14, 8),
(12, NULL, 3, 15, 9),
(13, '100.00', 3, 16, NULL),
(14, '100.00', 3, 17, NULL),
(15, NULL, 4, 18, 10),
(16, NULL, 5, 18, 10),
(17, NULL, 4, 18, 11),
(18, NULL, 5, 18, 11),
(19, '100.00', 3, 19, NULL),
(20, '50.00', 3, 20, NULL),
(21, '50.00', 6, 20, NULL),
(22, '30.00', 7, 21, NULL),
(23, '50.00', 8, 21, NULL),
(24, '20.00', 9, 21, NULL),
(25, '30.00', 7, 22, NULL),
(26, '50.00', 8, 22, NULL),
(27, '20.00', 9, 22, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_dossiersouscritereevaluateur`
--

CREATE TABLE IF NOT EXISTS `pmb_dossiersouscritereevaluateur` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(11) DEFAULT NULL,
  `note` decimal(19,2) DEFAULT NULL,
  `Souscritere_ID` bigint(20) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `Evaluateur_ID` int(11) DEFAULT NULL,
  `Pli_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK737BBC0921E224C0` (`Dossiers_ID`),
  KEY `FK737BBC092B7B217B` (`Pli_ID`),
  KEY `FK737BBC092D514C80` (`Evaluateur_ID`),
  KEY `FK737BBC09B74B3DAE` (`Souscritere_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Contenu de la table `pmb_dossiersouscritereevaluateur`
--

INSERT INTO `pmb_dossiersouscritereevaluateur` (`ID`, `libelle`, `note`, `Souscritere_ID`, `Dossiers_ID`, `Evaluateur_ID`, `Pli_ID`) VALUES
(1, NULL, '98.00', 1, 4, NULL, 6),
(2, NULL, '78.00', 1, 4, NULL, 7),
(3, NULL, '90.00', 1, 5, NULL, 8),
(4, NULL, '78.00', 1, 5, NULL, 9),
(5, NULL, '90.00', 1, 9, NULL, 10),
(6, NULL, '78.00', 1, 9, NULL, 11),
(7, NULL, '80.00', 3, 16, NULL, 29),
(8, NULL, '90.00', 3, 16, NULL, 30),
(9, NULL, '60.00', 3, 16, NULL, 31),
(10, NULL, '75.00', 3, 19, NULL, 38),
(11, NULL, '89.00', 3, 19, NULL, 39),
(12, NULL, '69.00', 3, 19, NULL, 40),
(13, NULL, '45.00', 3, 20, NULL, 42),
(14, NULL, '35.00', 6, 20, NULL, 42),
(15, NULL, '49.00', 3, 20, NULL, 43),
(16, NULL, '45.00', 6, 20, NULL, 43),
(17, NULL, '25.00', 7, 21, NULL, 44),
(18, NULL, '45.00', 8, 21, NULL, 44),
(19, NULL, '15.00', 9, 21, NULL, 44),
(20, NULL, '23.00', 7, 21, NULL, 45),
(21, NULL, '43.00', 8, 21, NULL, 45),
(22, NULL, '13.00', 9, 21, NULL, 45),
(23, NULL, '23.00', 7, 21, NULL, 46),
(24, NULL, '50.00', 8, 21, NULL, 46),
(25, NULL, '0.00', 9, 21, NULL, 46),
(26, NULL, '0.00', 7, 21, NULL, 47),
(27, NULL, '45.00', 8, 21, NULL, 47),
(28, NULL, '0.00', 9, 21, NULL, 47);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_dossiertype`
--

CREATE TABLE IF NOT EXISTS `pmb_dossiertype` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `libelledossiertype` tinytext COLLATE latin1_general_ci NOT NULL,
  `descriptiondossiertype` text COLLATE latin1_general_ci NOT NULL,
  `NBFICHIER` int(9) NOT NULL DEFAULT '0',
  `niveau_dossiertype` int(11) NOT NULL DEFAULT '0',
  `DateCreation` date DEFAULT NULL,
  `NumDossier` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `Typedossier` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_dossiertype`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_dossiertypes`
--

CREATE TABLE IF NOT EXISTS `pmb_dossiertypes` (
  `Iddossiers` bigint(20) NOT NULL AUTO_INCREMENT,
  `libelledossiers` varchar(10) DEFAULT NULL,
  `Typedossier` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`Iddossiers`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_dossiertypes`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_evaluateurs`
--

CREATE TABLE IF NOT EXISTS `pmb_evaluateurs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `commentaire` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fonction` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `pmb_evaluateurs`
--

INSERT INTO `pmb_evaluateurs` (`ID`, `commentaire`, `email`, `fonction`, `nom`, `prenom`, `telephone`, `Autorite_ID`) VALUES
(1, '', '', '', 'Adama', 'Samb', '37788', 6),
(2, '', '', '', 'ss', 'ss', 'ss', 902),
(3, '', 'asamb@ssi.sn', '', 'Samb', 'Adama', '6788', 9);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_evaluateursdossier`
--

CREATE TABLE IF NOT EXISTS `pmb_evaluateursdossier` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `commission` varchar(50) DEFAULT NULL,
  `Datelimite` date DEFAULT NULL,
  `Dateremise` date DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `Evaluateur_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKEB51CE6821E224C0` (`Dossiers_ID`),
  KEY `FKEB51CE682D514C80` (`Evaluateur_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `pmb_evaluateursdossier`
--

INSERT INTO `pmb_evaluateursdossier` (`ID`, `commission`, `Datelimite`, `Dateremise`, `Dossiers_ID`, `Evaluateur_ID`) VALUES
(1, NULL, NULL, NULL, 1, 1),
(2, NULL, NULL, NULL, 3, 1),
(3, NULL, NULL, NULL, 5, 1),
(4, NULL, NULL, NULL, 9, 1),
(5, NULL, NULL, NULL, 10, 1),
(6, NULL, NULL, NULL, 11, 1),
(7, NULL, NULL, NULL, 12, 1),
(8, NULL, NULL, NULL, 13, 1),
(9, NULL, NULL, NULL, 14, 2),
(10, NULL, NULL, NULL, 15, 2),
(11, NULL, NULL, NULL, 18, 3),
(12, NULL, NULL, NULL, 20, 2);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_evenement`
--

CREATE TABLE IF NOT EXISTS `pmb_evenement` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `intitule` varchar(50) DEFAULT NULL,
  `lieu` varchar(50) DEFAULT NULL,
  `participant` varchar(50) DEFAULT NULL,
  `courrierevenement` bigint(20) DEFAULT NULL,
  `dossiers` bigint(20) DEFAULT NULL,
  `piecejointeevenement` bigint(20) DEFAULT NULL,
  `typeevenement` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_evenement`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_familles`
--

CREATE TABLE IF NOT EXISTS `pmb_familles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `categories` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `pmb_familles`
--

INSERT INTO `pmb_familles` (`id`, `code`, `commentaire`, `designation`, `categories`) VALUES
(1, 'MIR', '', 'Miroir', 1),
(2, 'ARM', '', 'Armoire', 1),
(3, 'BUR', '', 'Bureau', 1),
(4, 'CHA', '', 'Chaise', 1),
(5, 'PDG', '', 'Pages de garde', 4),
(6, 's', '', 'ss', 2),
(7, 'VH', '', 'V�hicules utilitaires', 5);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_fichdossiertype`
--

CREATE TABLE IF NOT EXISTS `pmb_fichdossiertype` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `libellefichierdossiertype` text COLLATE latin1_general_ci NOT NULL,
  `nomfichierdossiertype` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'nom du fichier sur le serveur',
  `publier` varchar(5) COLLATE latin1_general_ci NOT NULL DEFAULT 'no' COMMENT 'publier ou non',
  `Dossiertype_ID` int(11) NOT NULL DEFAULT '0' COMMENT 'clÃ© de catÃ©gorielÃ©gislation',
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='table des fichiers de lÃ©gislation' AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_fichdossiertype`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_fichformulaire`
--

CREATE TABLE IF NOT EXISTS `pmb_fichformulaire` (
  `IDFICHIERLEGISLATION` int(11) NOT NULL AUTO_INCREMENT,
  `libfichierlegislation` tinytext COLLATE latin1_general_ci NOT NULL,
  `nomfichier` tinytext COLLATE latin1_general_ci NOT NULL COMMENT 'nom du fichier sur le serveur',
  `publier` enum('yes','no') COLLATE latin1_general_ci NOT NULL DEFAULT 'no' COMMENT 'afficher ou non',
  `ID_LEGISLATION` int(11) NOT NULL DEFAULT '0' COMMENT 'clé de catégorielégislation',
  PRIMARY KEY (`IDFICHIERLEGISLATION`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='table des fichiers de législation' AUTO_INCREMENT=90 ;

--
-- Contenu de la table `pmb_fichformulaire`
--

INSERT INTO `pmb_fichformulaire` (`IDFICHIERLEGISLATION`, `libfichierlegislation`, `nomfichier`, `publier`, `ID_LEGISLATION`) VALUES
(58, 'formulaire', '7300548114555ad4cb34ae.pdf', 'no', 3),
(80, 'Modèle Avis général de passation de marchés ', '2051967046492578dc0c815.doc', 'yes', 1),
(82, 'Modèle de publication des avis d''appel d''offre pour les marchés de fourniture et de travaux', '88674904049476a24158bb.pdf', 'yes', 1),
(62, 'Modèle d''avis d''attribution', '108798184845a2176f03d0d.doc', 'no', 1),
(63, 'test fichier', '131859122545a8f41031979.xls', 'no', 4),
(64, 'Modèle de garantie bancaire d''exécution', '14724891184682f4cf0b71e.doc', 'no', 1),
(65, 'Modèle de garantie bancaire restitution avance forfaitaire', '13965338754682f4e644aa5.doc', 'no', 1),
(66, 'Modèle d''engagement Charte éthique', '20748025724682f50094c62.doc', 'no', 1),
(67, 'Modèle formulaire marché fournitures', '2251824014682f51daba98.doc', 'no', 1),
(68, 'Modèle formulaire marché travaux', '21354329364682f532a037d.doc', 'no', 1),
(69, 'Modèle garantie bancaire de soumission', '11227937914682f5417de2c.doc', 'no', 1),
(70, 'Modèle indicatif d''avis d''attribution', '20194081334682f555c28cd.doc', 'no', 1),
(71, 'Modèle lettre notification attribution marché fournitures', '4350432384682f561b34aa.doc', 'no', 1),
(72, 'Modèle lettre notification attribution marché travaux', '17706555454682f57281b35.doc', 'no', 1),
(73, 'Modèle PV examen rapport évaluation offres financières par CM', '7957739474682f587bebc5.doc', 'no', 1),
(74, 'Modèle PV ouverture des plis', '8478262324682f5978d250.doc', 'no', 1),
(75, 'Modèle rapport évaluation offres financières consultants', '683974904682f5a91e84b.doc', 'no', 1),
(81, 'Modèle de transmission des statistiques mensuelles', '170750650549257965da69a.doc', 'no', 1),
(79, 'Plan de passation des marchés ', '17196299694925781f5072f.doc', 'yes', 1),
(83, 'Modèle de publication des avis d''attribution provisoire et définitive', '81657952149476aa642620.pdf', 'yes', 1),
(84, 'Formulaire d''inscription', '1368525919496eff42aa21a.pdf', 'yes', 7),
(85, 'FORMATION REALISEE EN 2008', '180401544249703ce58ee08.pdf', 'yes', 7),
(87, 'Formulaire d''évaluation du Code des marchés publics', '20561675254a8a4895e8cc8.doc', 'yes', 1),
(88, 'Modèle de rapport trimestriel des Cellules de passation des marchés', '294855634a8d4aa4e7d6d.doc', 'yes', 1),
(89, 'tester', '10912309994fe1f648a7d8f.pdf', 'yes', 1);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_fichierdocument`
--

CREATE TABLE IF NOT EXISTS `pmb_fichierdocument` (
  `IDdoc` int(11) NOT NULL AUTO_INCREMENT,
  `libelleDoc` text COLLATE latin1_general_ci NOT NULL,
  `nomFichierDoc` text COLLATE latin1_general_ci NOT NULL COMMENT 'nom du fichier sur le serveur',
  `publier` varchar(5) COLLATE latin1_general_ci NOT NULL DEFAULT 'no' COMMENT 'afficher ou non',
  `catDoc_ID` int(11) NOT NULL DEFAULT '0' COMMENT 'clÃ© de catÃ©goriedocument',
  `Autorite_ID` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IDdoc`),
  KEY `FKBDA84A4F4B9DC270` (`Autorite_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='table des Documents' AUTO_INCREMENT=51 ;

--
-- Contenu de la table `pmb_fichierdocument`
--

INSERT INTO `pmb_fichierdocument` (`IDdoc`, `libelleDoc`, `nomFichierDoc`, `publier`, `catDoc_ID`, `Autorite_ID`) VALUES
(9, 'RAPPORT TRIMESTRIEL ARMP Mai-Juillet 2008 ', '32320426149156a335b99f.pdf', 'yes', 1, 0),
(3, 'jf,f,h', '96098219245a773071e84c.xls', 'no', 2, 0),
(4, 'ytykkyk', '143014338545a7a7ecd1cf2.xls', 'no', 3, 0),
(5, 'fichier du rapport annuel dcmp de l''année en cours', '69999285545acc6e413130.doc', 'no', 4, 0),
(6, 'RAPPORT TRIMESTRIEL ARMP Février-Avril 2008', '940318594483a99f949fb7.pdf', 'yes', 1, 0),
(7, 'ARRETE INTERMINISTERIEL N° 000093 DU 14 JANVIER\r\n2004 RELATIF A LA QUALIFICATION ET A LA\r\nCLASSIFICATION DES ENTREPRISES, ENTREPRENEURS ET\r\nARTISANS DE BATIMENTS ET DES TRAVAUX PUBLICS', '16572550864896edd997b06.pdf', 'no', 5, 0),
(8, 'criteres de classement', '5827658094896edfb87c56.pdf', 'no', 5, 0),
(13, 'RAPPORT TRIMESTRIEL ARMP Août-Octobre 2008 ', '3eme rapport trimestriel 2008.pdf', 'yes', 1, 0),
(14, 'Résolution N° 08/09 relative �  l''adoption des cahiers des clauses administratives générales (CCAG) applicables aux marchés de travaux, fournitures et services', 'resoluton008.pdf', 'yes', 7, 0),
(12, 'Evaluation du système des marchés publics du Sénégal selon les indicateurs de l''OCDE', 'RAPPORTREVUEOCDEMPSENEGAL2008.pdf', 'yes', 1, 0),
(15, 'Résolution N° 09/09 relative �  l''adoption des "Conditions générales du Marché" applicables aux Demandes de propositions types des marchés de prestations intellectuelles rémunérés au temps passé ou au forfait dont le coût estimatif est supérieur ou égal �  35 millions de francs F CFA', 'resoluton009.pdf', 'yes', 7, 0),
(16, 'Résolution N° 10/09 relative �  l''adoption des "Conditions générales du Marché" applicables aux Demandes de propositions type pour les marchés de prestations intellectuelles rémunérés au temps passé ou au forfait, et dont le coût estimatif est inférieur �  35 millions de francs F CFA.', 'resoluton010.pdf', 'yes', 7, 0),
(17, 'Résolution N° 11/09 relative �  l''adoption du dossier type de marchés de travaux de moins de 35 millions de francs CFA applicables aux communautés rurales et communes dont le budget annuel initial est inférieur �  300 millions de francs CFA.', 'resoluton011.pdf', 'yes', 7, 0),
(18, 'Résolution N° 12/09 relative �  l''adoption du dossier type de marché de fournitures et de services estimés �  moins de 25 millions de francs cfa applicable aux communautés rurales et communes dont le budget annuel initial est inférieur �  300 millions de francs cfa.', 'resoluton012.pdf', 'yes', 7, 0),
(19, 'Résolution N° 13/09 relative �  l''adoption du dossier type de Demande de propositions pour les marchés de prestations intellectuelles rémunérés au temps passé ou au forfait, et dont le coût estimatif est inférieur �  35 millions de francs F CFA', 'resoluton013.pdf', 'yes', 7, 0),
(20, 'Résolution N° 15/09 relative �  l''adoption du modèle de rapport d''évaluation des offres et recommandation pour l''attribution des marchés de travaux et fournitures.', 'resoluton015.pdf', 'yes', 7, 0),
(21, 'Résolution N° 16/09 relative �  l''adoption du dossier type de pré sélection applicable aux marchés de prestations intellectuelles.', 'resoluton016.pdf', 'yes', 7, 0),
(22, 'Résolution N° 14/09 relative �  l''adoption du dossier type d''appel d''offres de travaux de moins de 35 millions de francs cfa applicable aux communautés rurales et aux communes dont le budget annuel initial est inférieur �  300 millions de francs cfa', 'resoluton014.pdf', 'yes', 7, 0),
(23, 'Résolutions N° 17/09 relative �  l''adoption du dossier type de Demande de propositions pour les marchés de prestations intellectuelles rémunérés au temps passé ou au forfait, et dont le coût estimatif est supérieur �  35 millions de francs F CFA', 'resoluton017.pdf', 'yes', 7, 0),
(24, 'Résolution N° 18/09 relative �  l''adoption du dossier type d''appel d''offres applicable aux marchés de travaux de taille moyenne dont le coût estimatif est compris entre 35 millions et 200 millions de francs cfa', 'resoluton018.pdf', 'yes', 7, 0),
(25, 'Résolution N° 19/09 relative �  l''adoption du dossier type d''appel d''offres applicable aux marchés de fournitures', 'resoluton019.pdf', 'yes', 7, 0),
(26, 'Résolution N° 20/09 relative �  l''adoption du dossier type d''appel d''offres applicable aux marchés de services', 'resoluton020.pdf', 'yes', 7, 0),
(27, 'Résolution N° 21/09 relative �  l''adoption du dossier type de pré qualification applicable �  la passation des marchés de travaux importants ou complexes, de fournitures de matériels devant être fabriqués sur commande, ou de services spécialisés.', 'resoluton021.pdf', 'yes', 7, 0),
(28, 'LISTE DES PERSONNES AYANT SUIVI LA FORMATION DES FORMATEURS (session 2008)', 'LISTE_FORMATEURS.pdf', 'yes', 8, 0),
(29, 'Résoluion n° 22/09 relative �  l''adoption du Plan d''action Post revue du Système national des Marchés publics', 'résolution022.pdf', 'yes', 7, 0),
(30, 'Résolution n°23/09 relative �  l''adoption du manuel de classement et d''archivage des documents de passation des marchés', 'resolution023.pdf', 'yes', 7, 0),
(31, 'R2SOLUTION n°24/09 relative �  l''adoption du mandat habilitant l''ARTP �  collecter au nom et pour le compte de l''ARMP, les redevances dues par les sociétés délégataires de service public intervenant dans le domaine régulé par elle', 'résolution024.pdf', 'no', 7, 0),
(32, 'Rapport annuel 2008', 'Rapport Annuel 2008.pdf', 'yes', 1, 0),
(33, 'RAPPORT TRIMESTRIEL ARMP Janvier-Mars 2009 ', 'Rapport trim N°5 Janv mars 09.pdf', 'yes', 1, 0),
(34, 'RAPPORT TRIMESTRIEL ARMP Avril-Juin 2009', 'rapport trim n°6 Avril-Juin.pdf', 'yes', 1, 0),
(35, 'Demande d''avis n°001 sur la mis en place d''une commission des marchés au sein de l''Agence Nationale pour l''Emploi des Jeunes', 'Avis n°001.pdf', 'yes', 9, 0),
(36, 'Demande d’avis n°001bis sur la mis en place d’une commission des marchés au sein de l’Agence Nationale pour l’Emploi des Jeunes', 'Avis n° 001bis.pdf', 'yes', 9, 0),
(37, 'Demande d''avis n°002 sur la mis en place d''une commission des marchés au sein du Fond National de Promotion de la Jeunesse', 'Avis n° 002.pdf', 'yes', 9, 0),
(38, 'Avis n° 003/ARMP/CRD du 18 septembre 2008 du Comité de Règlement des Différends statuant en commission de litiges sur la demande de régularisation du marché n° 3072/HPD/MAT/CM du 17/12/2007 relatif �  la réhabilitation et l’extension des services médicaux de l’hôpital Principal de Dakar', 'AVIS N° 003.pdf', 'yes', 9, 0),
(39, 'Avis n° 004/ARMP/CRD du 29 septembre 2008 du Comité de Règlement des Différends statuant en commission de litiges sur la demande de dérogation aux dispositions du code des marchés introduite par la Direction de la Gestion du Patrimoine Bâti en vue de la finalisation des marchés relatifs aux travaux d’entretien et de réhabilitation de bâtiments administratifs', 'AVIS N° 004.pdf', 'yes', 9, 0),
(40, 'Avis n° 005/ARMP/CRD du 20 Octobre 2008 du Comité de Règlement des Différends statuant en commission de litiges sur la demande de la DAP sur la conduite �  tenir suite �  la relance sans succès de l''AO initialement déclaré infructueux ', 'AVIS N° 005 - SUR REQUETE DE L\\''ADM PENITENTIAIRE.pdf', 'yes', 9, 0),
(41, 'Avis n° 006/ARMP/CRD du 20 Octobre 2008 du Comité de Règlement des Différends statuant en commission de litiges sur la demande du cabinet Performances SARL relative �  la limitation aux seules ONG, de la participation �  la manifestation d''intérêt lancée par le Ministère des Mines, de l''Industrie et des PME', 'AVIS N° 006 -  AFF PERFORMANCES C MIN DES _.pdf', 'yes', 9, 0),
(42, 'Avis n° 007/ARMP/CRD du 28 Octobre 2008 du Comité de Règlement des Différends statuant en commission de litiges sur la demande de précision de l''hôpital principal de Dakar relativement �  la procédure d''acquisition de matériels par crédit bail prévue �  l''article 4 du code des marchés publics', 'AVIS N° 007 -  SUR DEMANDE HPD.pdf', 'yes', 9, 0),
(43, 'ARRET N°10 du 25/09/08 - Etat du Sénégal Contre ARMP,CRD,AATR', 'arrété_CF.pdf', 'yes', 10, 0),
(44, 'Rapport trimestriel de l’Autorité de Régulation des Marchés Publics (ARMP) avril - juin2010', 'Rapport trimestriel avril - juin 2010.pdf', 'yes', 1, 0),
(45, 'libelle', 'Document1.pdf', 'yes', 4, 0),
(46, 'teste dcmp', 'Document1.pdf', 'yes', 4, 0),
(47, 'tester dcmp', 'Document1.pdf', 'yes', 4, 0),
(48, 'armptester', 'Document1.pdf', 'yes', 1, 0),
(49, 'resolutionARMP', 'Document1.pdf', 'yes', 7, 0),
(50, 'rtrerr', 'Document1.pdf', 'yes', 11, 0);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_fichierlegislation`
--

CREATE TABLE IF NOT EXISTS `pmb_fichierlegislation` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `codeDateRapport` date DEFAULT NULL,
  `codeDatePublication` date DEFAULT NULL,
  `codeDescription` varchar(255) DEFAULT NULL,
  `codeNomfichier` varchar(255) DEFAULT NULL,
  `codeLibelle` varchar(255) DEFAULT NULL,
  `codePublier` varchar(255) DEFAULT NULL,
  `categorie` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Contenu de la table `pmb_fichierlegislation`
--

INSERT INTO `pmb_fichierlegislation` (`ID`, `codeDateRapport`, `codeDatePublication`, `codeDescription`, `codeNomfichier`, `codeLibelle`, `codePublier`, `categorie`) VALUES
(1, '2013-01-08', '2013-01-09', 'tester', '0550608012013_rap_Au Sénégal.docx', '	Arrêté N° 107/CAB/PM/ARMP du 10 Mai 2010 Fixant les délais de publicité et de réception des offres des Marchés Publics', 'yes', 2),
(2, '2013-01-09', '2013-01-09', 'tester', '0452608012013_rap_Diagramme_1.pdf', 'Arrêté fixant le taux de la redevance de régulation des marchés publics au titre des années 2008 et 2009', 'yes', 2),
(3, '2013-01-08', '2013-01-09', 'tester', '4319708012013_rap_Doc1.docx', 'METHODOLOGIE DE CONDUITE DES AUDITS DES MARCHES PUBLICS LANCES PAR LES AUTORITES CONTRACTANTES', 'yes', 6),
(4, '2013-01-08', '2013-04-13', 'tester', '2520708012013_rap_Doc2.docx', 'Liste des Établissements financiers agréés �  cautionner des marchés publics', 'yes', 1),
(5, '2013-01-09', '2013-01-09', 'tester', '11291009012013_rap_Doc1.docx', 'Arrêté 28.02.2011 x 1946 portant agrément de UBA �  garantir les candidats au marchés publics', 'yes', 1),
(6, '2013-01-08', '2013-04-13', 'tester', '4144013042013_rap_modele.doc', 'Arrêté 05.01.2011 x 417 portant agrément de Askia Assurances �  garantir les candidats au marchés publics', 'yes', 1),
(7, '2013-01-09', '2013-05-04', 'decision', '28331009012013_rap_Doc1.docx', 'Décisions ARMP n° 1 & 2 fixant les délais impartis �  la DCMP pour l''examen des dossiers et la Composition du Comité de Règlement des Différents', 'yes', 3),
(8, '2013-01-09', '2013-01-09', 'decret', '33361009012013_rap_Doc2.docx', 'Décret modifiant l''article 63 du décret 2007-545 du 25 avril 2007 portant code des marchés publics', 'yes', 5),
(9, '2013-01-09', '2013-01-09', 'tester', '19431009012013_rap_demande permission.docx', 'Code des obligations de l''administration (COA)', 'yes', 7),
(10, '2013-01-09', '2013-01-09', 'testr', '03461009012013_rap_Doc2.docx', 'Code des marchés publics', 'yes', 7),
(11, '2013-01-09', '2013-01-09', 'teste', '30581009012013_rap_Doc2.docx', 'Circulaire pour la mise en place des cellules de passation de marchés des autorités contractantes', 'yes', 8),
(12, '2013-01-09', '2013-01-09', 'tester juste ', '45011109012013_rap_Document1.pdf', 'Circulaire portant Entrée en vigueur du nouveau Code des Marchés publics', 'yes', 8),
(17, '2013-01-10', '2013-01-10', 'test', '5125110012013_rap_Doc1.docx', 'tester', 'yes', 9),
(18, '2013-04-14', '2013-04-14', 'aaaa', '2404914042013_rap_02-ECBU.pdf', 'aa', 'yes', 11),
(19, '2013-04-16', NULL, 'dddd', '1212916042013_rap_02-ECBU.pdf', 'dd', 'no', 11),
(20, '2013-04-17', NULL, 'ddd', '3239517042013_rap_02-ECBU.pdf', 'ddd', 'no', 1),
(21, '2013-05-13', NULL, 'sss', '3311513052013_rap_recu.pdf', 'sss', 'no', 1),
(22, '2013-05-13', NULL, 'www', '2012513052013_rap_recu.pdf', 'www', 'no', 3);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_fichiersaudits`
--

CREATE TABLE IF NOT EXISTS `pmb_fichiersaudits` (
  `idfichiersaudits` bigint(20) NOT NULL AUTO_INCREMENT,
  `libellefichiersaudits` varchar(255) DEFAULT NULL,
  `nomfichierpdf` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idfichiersaudits`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_fichiersaudits`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_fichlegislation`
--

CREATE TABLE IF NOT EXISTS `pmb_fichlegislation` (
  `IDFICHIERLEGISLATION` int(11) NOT NULL AUTO_INCREMENT,
  `libfichierlegislation` text COLLATE latin1_general_ci NOT NULL,
  `nomfichier` varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'nom du fichier sur le serveur',
  `publier` varchar(5) COLLATE latin1_general_ci NOT NULL DEFAULT 'no' COMMENT 'afficher ou non',
  `ID_LEGISLATION` int(11) NOT NULL DEFAULT '0' COMMENT 'clÃ© de catÃ©gorielÃ©gislation',
  PRIMARY KEY (`IDFICHIERLEGISLATION`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='table des fichiers de lÃ©gislation' AUTO_INCREMENT=275 ;

--
-- Contenu de la table `pmb_fichlegislation`
--

INSERT INTO `pmb_fichlegislation` (`IDFICHIERLEGISLATION`, `libfichierlegislation`, `nomfichier`, `publier`, `ID_LEGISLATION`) VALUES
(58, 'Decret portant reorganisation du bureau', '2985886444545d60a9c674.pdf', 'no', 315),
(59, 'Directive N° 04/2005/CM/UEMOA du 09/12/2005 portant procédures de passation, d''exécution et de règle', '30989574045a1fe85baebb.pdf', 'no', 315),
(60, 'Directive N°05/2005/CM/UEMOA du 09/12/2005 portant contrôle et régulation des marchés publics et des', '747102345a1fefca037e.pdf', 'no', 315),
(61, 'Code des obligations de l''administration (COA)', '142831423445a1ff2aaba99.doc', 'no', 315),
(62, 'Code des marchés publics (CMP)', '60310556745a1ff4a40d9c.doc', 'no', 315),
(63, 'Code des obligations de l''administration (COA )', '206773175145a204a657bd3.doc', 'no', 321),
(64, 'Code des marchés publics (CMP)', '154172022845a2055dc28cf.doc', 'no', 321),
(65, 'Directive N°04/2005/CM/UEMOA du 09/12/2005 portant procédures de passation, d''exécution et de règlem', '19353993045a2090f501c4.pdf', 'no', 320),
(66, 'Directive N°05/2005/CM/UEMOA du 09/12/2005 portant contrôle et régulation des marchés publics et des', '162188764445a209916ad00.pdf', 'no', 320),
(67, 'Décret portant organisation et fonctionnement de l''Autorité de Régulation des MArchés publics(ARMP)', '191282606045a20a4266ff8.doc', 'no', 323),
(68, 'Directive N°04/2005/CM/UEMOA du 09/12/2005 portant procédures de passation, d''exécution et de règlement', '177752340945a21141c28cf.pdf', 'yes', 325),
(70, 'Code des obligations de l''administration (COA)', '110808470645a212727de2c.doc', 'yes', 326),
(71, 'Code des marchés publics', '202665289445a212851e84b.doc', 'yes', 326),
(72, 'yyykykyk', '126868099945a7daf2d9705.xls', 'no', 316),
(73, 'ykyk', '197964911145a7db152625e.pdf', 'no', 328),
(74, 'glpp', '73634163245a7de709c675.doc', 'no', 328),
(75, 'dgkdg', '127612652245a8e6b0baebc.xls', 'no', 3),
(76, 'fyl', '161904960745a8e6d100004.pdf', 'no', 3),
(77, 'fkkkkkk', '155301360945a8ec1a7de2c.xls', 'no', 4),
(78, 'fichier', '84018095945a8ee4dca2df.xls', 'no', 4),
(79, 'llljmjmjkmm', '161217532545a8f05f3d093.xls', 'no', 1),
(80, 'fichier de test', '189514843045a8f094dd40e.xls', 'no', 316),
(81, 'fichier de test', '91392321845a8f0fc9c675.xls', 'no', 4),
(82, 'gffgngfn', '156655462345a8f159e1115.xls', 'no', 4),
(83, 'gngcghncg', '129737491345a8f2818583f.xls', 'no', 4),
(84, 'hhjdhrh', '95399333845a8f34ea7d90.xls', 'no', 4),
(85, 'truru', '167843762345a8f3a47a123.xls', 'no', 4),
(221, 'Arrêté 26.04.2011 x 04219 portant agrément de la CNCAS �  garantir les candidats au marchés publics ', '8524734634dba977a00001.pdf', 'yes', 334),
(87, 'DIRECTIVE N°05/2005/CM/UEMOA PORTANT CONTROLE ET REGULATION DES MARCHES PUBLICS ET DES DELEGATIONS DE SERVICE PUBLIC DANS L''UNION ECONOMIQUE ET MONETAIRE OUEST AFRICAINE', 'directive_05_2005_uemoa.pdf', 'yes', 325),
(88, 'DÉCISION N° 01/2000/CM/UEMOA PORTANT ADOPTION DU DOCUMENT DE CONCEPTION', '2000dfznaolgumfr8.pdf', 'no', 332),
(110, 'Les procédures de passation', '82660348247fefed7da4db.pdf', 'yes', 333),
(89, 'ARRETE PRIS EN APPLICATION DE L’ARTICLE 35 DU CODE DES\r\nMARCHES PUBLICS RELATIF AUX CELLULES DE PASSATION\r\nDE MARCHES DES AUTORITES CONTRACTANTES', '175223755847b31936ab992.pdf', 'no', 331),
(90, 'ARRETE PRIS EN APPLICATION DE L’ARTICLE 112 DU CODE DES\r\nMARCHES PUBLICS FIXANT LES SEUILS A PARTIR DESQUELS\r\nIL EST REQUIS UNE GARANTIE DE BONNE EXECUTION', '86868845247b3195b656f7.pdf', 'no', 331),
(274, 'tttt', '1734363445507d573003d0c.docx', 'yes', 330),
(263, 'tester', '6668788625065739a1e84b.pdf', 'no', 341),
(273, 'frfrfrfrrfrfr', '19813239555075c461e8b28.pdf', 'yes', 331),
(258, 'session de formation de la 2ème promotion', '9159464506482d19c680.pdf', 'yes', 337),
(272, 'jiojiojoj', '14472624935075b7543938a.pdf', 'no', 331),
(270, 'hhhhhhhhh', '806394142507598b181b35.pdf', 'no', 331),
(94, 'ARRETE PRIS EN APPLICATION DE L’ARTICLE 77-3 DU CODE DES\r\nMARCHES PUBLICS RELATIF AUX COMMANDES POUVANT ETRE\r\nDISPENSEES DE FORME ECRITE ET DONNER LIEU A\r\nREGLEMENT SUR MEMOIRES OU FACTURES', '96619480447b319ee9cb96.pdf', 'yes', 331),
(95, 'ARRETE PRIS EN APPLICATION DE L’ARTICLE 111 DU CODE DES\r\nMARCHES PUBLICS FIXANT LES SEUILS EN DESSOUS DESQUELS\r\nIL N’EST PAS REQUIS DE GARANTIE DE SOUMISSION', '169701871147b31a0be133b.pdf', 'yes', 331),
(96, 'Circulaire pour la mise en place des cellules de\r\npassation de marchés des autorités contractantes', '162359416747b31a3681ac2.pdf', 'yes', 329),
(97, 'Circulaire portant Entrée en vigueur du nouveau Code des Marchés publics', '32675499547b31a5380d07.pdf', 'yes', 329),
(106, 'Saisine de l''ARMP', '22857647647fb5e507890c.pdf', 'yes', 333),
(107, 'Décisions ARMP n° 1 & 2  fixant les délais impartis �  la DCMP pour l''examen des dossiers et la Composition du Comité de Règlement des Différents', '173973854647fb67eac5902.pdf', 'yes', 332),
(114, 'Délais impartis �  la DCMP pour la revue a priori', '1361914328483448a5bfe43.pdf', 'yes', 333),
(137, 'Seuils et Délais', '20822212894874d7436455b.pdf', 'yes', 333),
(109, 'Décision n°1 ARMP', '12482656547fb69ec15316.pdf', 'yes', 329),
(255, 'tester legislation', '18135349264ff2e62bec830.pdf', 'yes', 331),
(256, 'code_du _jour', '557908565004448907a15.pdf', 'yes', 326),
(257, 'yuyuy_', '14404822765062de917de2c.pdf', 'yes', 335),
(131, 'Constitution des dossiers soumis �  la DCMP', '1922581353484fb56784d95.pdf', 'yes', 333),
(254, 'Arrêté N° 107/CAB/PM/ARMP du 10 Mai 2010 Fixant les délais de publicité et de réception des offres des Marchés Publics', '20605332534fdb2c8413131.pdf', 'yes', 331),
(248, 'Arrêté 31.03.2011 x 03460 portant agrément de la Banque des Institutions Mutualisées de l''Afrique de l''Ouest (BIMAO) �  garantir les candidats au marchés publics ', '', 'no', 334),
(133, 'DECISION N° 010/ARMP/CRMP/CRD EN DATE DU 11 JUIN 20 08 DU COMITE DE REGLEMENT DES DIFFERENDS STATUANT EN COMMISSION DES LITIGES SUR LA DEMANDE D’ENQUÊTE SOLLICITEE PAR Me IBRAHIMA DIAWARA CONTRE LE REFUS DE LA SICAP DE LUI COMMUNIQUER LES PROCES VERBAUX D’OUVERTURE DES PLIS ET D’ATTRIBUTION', '291349538485cfe29cdd97.pdf', 'yes', 332),
(134, 'DECISION N° 012/ARMP/CRMP/CRD EN DATE DU 11 JUIN 20 08 DU COMITE DE REGLEMENT DES DIFFERENDS STATUANT EN COMMISSION DES LITIGES SUR L’ATTRIBUTION DU MARCHE\r\nD’ACQUISITION DE FOURNITURES DE BUREAU ET CONSOMMABLES INFORMATIQUES DU MINISTERE DE L’ENERGIE', '2146420769485cfe79aee31.pdf', 'yes', 332),
(135, 'DECISION N° 013/ARMP/CRMP/CRD EN DATE DU 11 JUIN 20 08 DU COMITE DE REGLEMENT DES DIFFERENDS STATUANT EN COMMISSION DES LITIGES SUR LE REJET PAR LA D.C.M.P. DE LA PROPOSITION D’AVENANT POUR LA REALISATION D’UNE DEUXIEME CENTRALE DE 125 MW', '1150393281485cff09442e1.pdf', 'yes', 332),
(148, 'Immatriculation des marchés : Composition des dossiers', '158735977492c29f70ca1b.doc', 'yes', 333),
(136, 'DECISION N° 011/ARMP/CRMP/CRD EN DATE DU 11 JUIN 20 08 DU COMITE DE REGLEMENT DES DIFFERENDS STATUANT EN COMMISSION LITIGES SUR L’AVIS NEGATIF EMIS PAR LA DCMP A LA DEMANDE D’AUTORISATION DU MINISTERE DE L’INTERIEUR DE PASSER PAR ENTENTE DIRECTE LE MARCHE DE L’EXTENSION DU RESEAU DE PRODUCTION DES ASSEPORTS NUMERISES\r\n', '16299410194874c1166a5b4.pdf', 'yes', 332),
(229, 'Arrêté 06.05.2010 x 04196 portant agrément de la ICBS �  garantir les candidats au marchés publics ', '13422346374bf0eb7ee984e.pdf', 'yes', 334),
(141, 'DECISION N° 015/ARMP/CRMP/CRD DU 10 JUILLET 2008 DU COMITE DE REGLEMENT DES DIFFERENDS STATUANT EN COMMISSION LITIGES SUR L’AVIS DEFAVORABLE DE LA DCMP A LA DEMANDE DE LA DGID DE RENOUVELER PAR AVENANT DU MARCHE DE GARDIENNAGE DE SES LOCAUX', '176619889648806a9a1b609.pdf', 'yes', 332),
(151, 'Décret modifiant l''article 63 du décret 2007-545 du 25 avril 2007 portant code des marchés publics ', '9151114644946612de7915.pdf', 'yes', 330),
(150, 'Arrêté fixant le taux de la redevance de régulation des marchés publics au titre des années 2008 et 2009', '165751448449465f1235597.pdf', 'yes', 331),
(152, 'Décision fixant le délai imparti �  la Direction centrale des Marchés publics pour examiner les dossiers relevant de catastrophe naturelle ou technologique et nécessitant une intervention immédiate', '1669915854494669fceaacf.pdf', 'yes', 332),
(153, 'Projet de manuel de procédure du code des marché', '13899410749468401d3a3a.doc', 'yes', 333),
(154, 'Décision n° 000004/CRMP fixant les modèle de publication des avis d''appel d''offre pour les marchés de fourniture et de travaux', '38314721949476bfb706e4.pdf', 'yes', 332),
(155, 'Décision n° 000003/CRMP fixant les modèles de publication des avis d''attribution provisoire et définitive', '86443078249476cf41f4d7.pdf', 'yes', 332),
(157, 'METHODOLOGIE DE CONDUITE DES AUDITS DES MARCHES PUBLICS LANCES PAR LES AUTORITES CONTRACTANTES', '530070027497aef749835d.pdf', 'yes', 333),
(220, 'Arrêté 02.03.2011 x 02025 portant agrément de la SONAC �  garantir les candidats au marchés publics ', '6876955764d7a0ec8d59f9.pdf', 'yes', 334),
(212, 'Arrêté 20.04.2011*004128 portant agrément de la BSIC Sénégal �  garantir les candidats au marchés publics ', '3270618614dba970403d0a.pdf', 'yes', 334),
(211, 'Arrêté n°28.02.2011*1945 portant agrément de la BOA �  garantir les candidats au marchés publics ', '1947166594d762d3b35680.pdf', 'yes', 334),
(179, 'Décret portant nomination de membres du Conseil de Régulation des Marchés Publics', '7538905244a28e4b251354.pdf', 'yes', 330),
(216, '', '', 'yes', 0),
(207, 'Arrêté 13.04.2011 x 03880 portant agrément de la SGBS �  garantir les candidats au marchés publics ', '18165725964db977006ea06.pdf', 'yes', 334),
(238, 'Liste des Établissements financiers agréés �  cautionner des marchés publics ', '11066465674dba943a29f64.xls', 'yes', 334),
(167, 'Décret portant organisation et fonctionnement de l''Autorité de Régulation des Marchés publics', '190881649449d609445b7c7.doc', 'yes', 330),
(169, 'MANUEL DE CLASSEMENT ET D’ARCHIVAGE DES DOCUMENTS DE PASSATION DES MARCHES ', '27749350249e7273fc622f.doc', 'yes', 333),
(170, 'Circulaire pour la mise en oeuvre de la procédure de demande de renseignements et de prix (DRP) prévue par les dispositions de l''article 77 du decret n° 2007-545 du 25 avril portant Code des Marchés Publics', '155662554849e85993b55a0.pdf', 'yes', 329),
(174, 'METHODOLOGIE DE SELECTION DES AUTORITES CONTRACTANTES A AUDITER', '6841844184a0d85f7d95d2.pdf', 'yes', 333),
(226, 'Arrêté 22.04.2010 * 003787 portant agrément de la BIS �  garantir les candidats au marchés publics ', '21300691054bde56f279cbb.pdf', 'yes', 334),
(227, 'Arrêté 06.04.2011 * 003630 portant agrément de la BICIS �  garantir les candidats au marchés publics ', '19329667774db83420bebc3.pdf', 'yes', 334),
(181, 'Décret fixant les modalités de recrutement, le statut et les pouvoirs des agents de l''ARMP chargés des enquêtes', '14880260824a28f7c7bcc43.pdf', 'yes', 330),
(223, 'Arrêté pris en application de l''article 78 du decrét n° 2007-545 du 25 avril 2007 portant Code des Marchés Publics, relatif aux procédures applicables aux marchés passés par les communautés rurales et certaines communes', '6268114844bd955b0bb488.pdf', 'yes', 331),
(231, 'Redevance de régulation 2010', '366160384c206c5e244ae.pdf', 'yes', 331),
(232, 'Arrêté 31.03.2011 x 03460 portant agrément de la Banque des Institutions Mutualisées de l''Afrique de l''Ouest (BIMAO) �  garantir les candidats au marchés publics ', '1809777164db83222f1b81.pdf', 'yes', 334),
(224, 'Arrêté 22.04.2010 x 03787 portant agrément de la Banque Islamique du Sénégal(BIS) �  garantir les candidats au marchés publics ', '11898916774bd957a858543.pdf', 'yes', 334),
(203, 'Arrêté 28.02.2011 x 1946 portant agrément de UBA �  garantir les candidats au marchés publics ', '978264984d762c9cc65d5.pdf', 'yes', 334),
(204, 'Arrêté 05.01.2011 x 417 portant agrément de Askia Assurances �  garantir les candidats au marchés publics ', '5531480814d3954ac9c672.pdf', 'yes', 334),
(213, 'Arrêté 12.04.2011*003812 portant agrément du Crédit du Sénégal �  garantir les candidats au marchés publics ', '14754399044db833542625b.pdf', 'yes', 334),
(214, 'Arrêté 04.03.2011*002183 portant agrément de Ecobank Sénégal �  garantir les candidats au marchés publics ', '8887635594d7a0e608d24f.pdf', 'yes', 334),
(234, 'Arrêté 23.08.2010 x 07480 portant agrément de la CBAO groupe Attijariwafa �  garantir les candidats au marchés publics ', '14679196634c90b54c18688.pdf', 'yes', 334),
(235, 'Arrêté 03.09.2010 x 07851 portant agrément de la BRS �  garantir les candidats au marchés publics ', '3847250804c90b5fd2067b.pdf', 'yes', 334),
(239, 'Arrêté 18.03.2010 * 02487 portant agrément de la BRM �  garantir les candidats au marchés publics ', '1360662074cd2a7a2b34a9.pdf', 'yes', 334),
(242, 'Décret n° 2010 - 1396 modifiant le décret n° 2007-546 du 25 avril 2007 portant organisation et fonctionnement de l''ARMP', '4495484554cea68c366ff4.pdf', 'yes', 330),
(243, 'Décret n°2001-362 du 04 mai 2001 relatif aux procédures d''exécution et d''aménagement des sanctions pénales', '6541099654d30846090f58.doc', 'yes', 330),
(244, 'Arrêté 09.09.2010 * 08006 portant agrément du CMS �  garantir les candidats au marchés publics ', '7660638394d395649e1114.pdf', 'yes', 334),
(245, 'DECRET n° 2011-04 du 6 janvier 2011 modifiant et complétant le décret n° 2007 - 545 du 25 avril 2007 portant Code des Marchés Publics', '10255593774d400e01f0538.pdf', 'no', 330),
(246, 'DECRET n° 2011-04 du 6 janvier 2011 modifiant et complétant le decret n° 2007-545 du 25 avril 2007 portant Code des Marchés publics', '10846360344d4010cc35680.pdf', 'no', 330),
(247, 'Arrêté 01.04.2011 x 03558 portant agrément de la BRS �  garantir les candidats au marchés publics ', '19374966054db833d07a121.pdf', 'yes', 334);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_formulaire`
--

CREATE TABLE IF NOT EXISTS `pmb_formulaire` (
  `IDLEGISLATION` int(11) NOT NULL AUTO_INCREMENT,
  `niveau_LEGISLATION` int(11) NOT NULL DEFAULT '0' COMMENT 'pour connaitre ki est le pere',
  `liblegislation` longtext COLLATE latin1_general_ci NOT NULL,
  `nbtotalfichiers` int(15) NOT NULL DEFAULT '0' COMMENT 'pour connaitre le nombre de fichiers',
  `flagdernierniveau` smallint(6) NOT NULL DEFAULT '0' COMMENT 'pour connaitre le dernier niveau',
  `descriptionCategorieLegislation` tinytext COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`IDLEGISLATION`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=8 ;

--
-- Contenu de la table `pmb_formulaire`
--

INSERT INTO `pmb_formulaire` (`IDLEGISLATION`, `niveau_LEGISLATION`, `liblegislation`, `nbtotalfichiers`, `flagdernierniveau`, `descriptionCategorieLegislation`) VALUES
(1, 0, 'Modèles Etat du Togo', 6, 0, 'Modèles Etat du Togo'),
(5, 0, 'Modèles Banque Mondiale', 0, 0, 'Modèles Banque Mondiale'),
(6, 0, 'Modèle BAD', 0, 0, 'Modèle BAD'),
(7, 0, 'Formations', 2, 0, '');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_fournisseurs`
--

CREATE TABLE IF NOT EXISTS `pmb_fournisseurs` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `adresse` varchar(255) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `fax` varchar(15) DEFAULT NULL,
  `raisonSociale` varchar(255) DEFAULT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  `categoriefournisseurs_id` bigint(20) DEFAULT NULL,
  `ORIGINE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK57B091ECC845D8E2` (`categoriefournisseurs_id`),
  KEY `FK57B091EC4B9DC270` (`Autorite_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `pmb_fournisseurs`
--

INSERT INTO `pmb_fournisseurs` (`ID`, `adresse`, `email`, `fax`, `raisonSociale`, `telephone`, `Autorite_ID`, `categoriefournisseurs_id`, `ORIGINE`) VALUES
(1, 'Point E', 'ssi@ssi.sn', '', '2SI(solutions et strat�gies informatique)', '76890', 6, 1, 170),
(2, 'DAKAR-PONTY', 'ati@sentoo.sn', '', 'A.T.I multim�dia', '(221)553-22-22', 6, 1, 193),
(3, 'France', 'tfc@@yahoo.fr', '', 'Entreprise  TFC (Travaux-Fournitures-con)', '', 6, 1, 210),
(4, 'Dakar', '', '', 'Solid', '', 6, 1, 170),
(5, 'Dakar', 'neurotech@yahoo.fr', '', 'Neurotech', '6789654', 6, 1, 170);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_garantie`
--

CREATE TABLE IF NOT EXISTS `pmb_garantie` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `lotID` int(11) DEFAULT NULL,
  `pourcentage` decimal(19,2) DEFAULT NULL,
  `ac` bigint(20) DEFAULT NULL,
  `dossier` bigint(20) DEFAULT NULL,
  `garantieID` bigint(20) DEFAULT NULL,
  `montant` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKEB839665226E1D09` (`garantieID`),
  KEY `FKEB839665D06EC9D9` (`dossier`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `pmb_garantie`
--

INSERT INTO `pmb_garantie` (`ID`, `lotID`, `pourcentage`, `ac`, `dossier`, `garantieID`, `montant`) VALUES
(1, 0, '5.00', NULL, 1, 1, '25000000.00'),
(2, 0, '15.00', NULL, 10, 1, '750000000.00'),
(3, 0, '5.00', NULL, 12, 1, '20000000.00'),
(4, 0, '5.00', NULL, 14, 1, '500000.00'),
(5, 0, '3.00', NULL, 15, 1, '18000000.00'),
(6, 0, '1.00', NULL, 18, 1, '5000000.00');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_garantiesoumissions`
--

CREATE TABLE IF NOT EXISTS `pmb_garantiesoumissions` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `fournie` varchar(3) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `GARANTIE` bigint(20) DEFAULT NULL,
  `Pli_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK3E85AD7B21E224C0` (`Dossiers_ID`),
  KEY `FK3E85AD7B2B7B217B` (`Pli_ID`),
  KEY `FK3E85AD7B9A83BBB5` (`GARANTIE`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Contenu de la table `pmb_garantiesoumissions`
--

INSERT INTO `pmb_garantiesoumissions` (`ID`, `fournie`, `libelle`, `Dossiers_ID`, `GARANTIE`, `Pli_ID`) VALUES
(1, 'oui', NULL, 1, 1, 1),
(2, 'oui', NULL, 1, 1, 2),
(3, 'oui', NULL, 1, 1, 3),
(4, 'oui', NULL, 10, 2, 12),
(5, 'oui', NULL, 10, 2, 13),
(6, 'oui', NULL, 10, 2, 14),
(7, 'oui', NULL, 12, 3, 18),
(8, 'oui', NULL, 12, 3, 19),
(9, 'oui', NULL, 12, 3, 20),
(10, 'oui', NULL, 14, 4, 24),
(11, 'oui', NULL, 14, 4, 25),
(12, 'oui', NULL, 14, 4, 26),
(13, 'oui', NULL, 15, 5, 27),
(14, 'oui', NULL, 15, 5, 28),
(15, 'oui', NULL, 18, 6, 34),
(16, 'oui', NULL, 18, 6, 35),
(17, 'oui', NULL, 18, 6, 36),
(18, 'oui', NULL, 18, 6, 37);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_glossaire`
--

CREATE TABLE IF NOT EXISTS `pmb_glossaire` (
  `glossId` bigint(20) NOT NULL AUTO_INCREMENT,
  `glossTerme` varchar(250) DEFAULT '',
  `glossDefinition` text,
  `tletter` char(1) DEFAULT NULL,
  PRIMARY KEY (`glossId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Contenu de la table `pmb_glossaire`
--

INSERT INTO `pmb_glossaire` (`glossId`, `glossTerme`, `glossDefinition`, `tletter`) VALUES
(1, 'UEMOA', 'Union Economique et Monétaire Ouest Africaine', 'U'),
(45, 'Délégation de service public', 'Le contrat par lequel une des personnes morales\r\nde droit public ou de droit privé visées aux articles 4 et 5 de la Directive 04/2005/CM/UEMOA confie la gestion d’un service public relevant de sa compétence �  un délégataire dont la rémunération est liée ou substantiellement assurée par les résultats de l’exploitation du service. Au sens de la Directive 04/2005/CM/UEMOA, les délégations de services publics comprennent les régies intéressées, les affermages, (l’opération de réseau) ainsi que les concessions de service public, qu’elles incluent ou non l’exécution d’un ouvrage\r\n', 'D'),
(44, 'Délégataire', 'La personne morale de droit privé ou de droit public signataire d’une convention de délégation de service public et �  laquelle l’autorité délégante confie, conformément aux dispositions de la Directive 04/2005/CM/UEMOA, l’exploitation d’un service public avec ou sans prestations complémentaires\r\n', 'D'),
(43, 'Concession de service public', 'Le mode de gestion d''un service public dans le\r\ncadre duquel un opérateur privé ou public, le concessionnaire, est sélectionné conformément aux dispositions de la Directive 04/2005/CM/UEMOA. Elle se caractérise par le mode  de  rémunération  de  l''opérateur  �   qui  est  reconnu  le  droit  d''exploiter l''ouvrage �  titre onéreux pendant une durée déterminée\r\n', 'C'),
(40, 'Autorité délégante', 'L’autorité contractante ci-dessus définie, cocontractante\r\nd’une convention de délégation de service public\r\n', 'A'),
(41, 'Candidat', 'La personne physique ou morale qui manifeste un intérêt �  participer ou qui est retenue par une autorité contractante pour participer �  une procédure de passation de marchés\r\n', 'C'),
(42, 'Candidature', 'Acte par lequel le candidat manifeste un intérêt �  participer, sans que cet acte ne l’engage ni ne lui impose d’obligations vis-� -vis de l’autorité contractante.\r\n', 'C'),
(18, 'Cahier des charges', 'Cahier qui détaille l''ensemble des aspects contractuels d''un marché ( délais, pénalités, conditions générales...) Ce document est utilisé par défaut. Lors d''un marché le CCAG peut être complété par des clauses particulières qui fixe des modalités spécifiques pour le marché en cours, il s''agit alors du CCAP. Ce document n''est jamais présent dans le DCE.', 'C'),
(19, 'CCTP', 'Le CCTP vient apporter des conditions particulières particulières �  celles posé par défaut par le CCTG. Ce cahier se trouve dans le DCE. Lorsqu''il existe peu de conditions techniques spécifiques pour le marché en cours, les clauses sont rédigées dans le CCAP.\r\n', 'C'),
(20, 'Certificat électronique', 'Il permet d''accéder aux plates formes de dématérialisation des marchés publics. Ce certificat est utilisé pour garantir la sécurité lorsqu''une entreprise transfert son dossier de candidature �  une administration par Internet. Le certificat permet également d''authentifier une entreprise lorsqu''elle retire un DCE.\r\nDésormais, il est possible de joindre �  sa réponse électronique, une copie physique du dossier de candidature (par clef USB, papier, CD-ROM ...). Lorsque l''on transmet un document physique en plus de sa réponse électronique, ce document est appelé copie de sauvegarde.\r\nLes certificats sont les mêmes que ceux utilisé pour la procédure de télé-TVA.', 'C'),
(21, 'Co-traitant', 'Entreprise qui participe �  un groupement regroupant plusieurs autres entreprises afin de répondre �  un marché. Il est fréquent de trouver des PME co-traitantes avec de grandes entreprises. ', 'C'),
(22, 'Code des Marchés Publics (CMP)', 'Tout comme le code de la route réglemente le comportement des automobilistes, le CMP réglemente le cadre dans lequel les administrations doivent émettre les appels d''offres et borne le cadre dans lequel les entreprises peuvent y répondre.\r\nLes parties prenantes dans un appel d''offres (administrations, entreprises, sous traitant...) sont tenues de respecter les différentes modalités du CMP.\r\nLe CMP n''a pas de valeur contractuel. ', 'C'),
(23, 'Commission d''appel d''offres (CAO) ', 'La Commission d''Appel d''Offres (CAO) rassemblant des menbres �  voix délibératives. Ses fonctions sont les suivantes :\r\n\r\n    * d''analyser les dossiers envoyés par les entreprises\r\n    * a le pouvoir de déclarer un marché infructueux\r\n    * d''attribuer le marché �  l''entreprise présentant l''offre économiquement la plus avantageuse\r\n    * a le pouvoir de déclarer une candidature nulle\r\n\r\nVoir la vidéo expliquant le fonctionnement d''une CAO présentée par Bernard Labiste, Maire de la commune du Haillan et président de la Commission d''Appel d''Offres (CAO)\r\n', 'C'),
(24, 'Concours', 'Procédure d''achat public utilisé par l''administration lorsqu''elle n''est pas encore en mesure de décrire avec une précision suffisante les aspectes techniques et financiers d''un ouvrage, d''un matériel �  réaliser car l''objet du marché nécessite des études importantes. La procédure de concours peut être passée selon une procédure restreinte ou par procédure ouverte.\r\n', 'C'),
(38, 'Attributaire', 'Le soumissionnaire dont l’offre a été retenue avant l’approbation du Marchés\r\n', 'A'),
(39, 'Autorité contractante', 'La personne morale de droit public ou de droit privé\r\nvisée aux articles 4 et 5 de la Directive  04/2005/CM/UEMOA, signataire d’un marché public.\r\n', 'A'),
(26, 'DCE (Dossier de Candidature des Entreprises)', 'Le Dossier de Candidature des Entreprises est constitué par l''ensemble des pièces du dossier d''appel d''offres. ', 'D'),
(33, 'Accord-cadre  ', 'L’accord conclu entre une ou plusieurs autorités contractantes ayant pour objet d''établir les termes régissant les marchés �  passer au cours d''une période donnée, notamment en ce qui concerne les prix et, le cas échéant, les quantités envisagées.', 'A'),
(35, 'Affermage', 'Le  contrat  par  lequel  l’autorité  contractante  charge  le  fermier, personne publique ou privée, de l’exploitation d’ouvrages qu’elle a acquis préalablement afin que celui-ci assure la fourniture d’un service public, le fermier ne réalisant pas les investissements initiaux\r\n', 'A'),
(46, 'Entreprise communautaire ', 'L’entreprise dont le siège social est situé dans un\r\nEtat membre de l’UEMOA.\r\n', 'E'),
(47, 'Maître d’ouvrage ', 'La personne morale de droit public ou de droit privé visée aux articles 4 et 5 de la Directive 04/2005/CM/UEMOA qui est le propriétaire final de l’ouvrage ou de l’équipement technique, objet du marché\r\n', 'M'),
(48, 'Maître d’ouvrage délégué  ', 'La personne morale de droit public ou de droit privé\r\nqui est le délégataire du maître d’ouvrage dans l’exécution de ses missions\r\n', 'M'),
(49, 'Marché  public', 'Le  contrat  écrit  conclu  �   titre  onéreux  par  une  autorité\r\ncontractante pour répondre �  ses besoins en matière de travaux, de fo urnitures ou de services au sens de la présente Directive\r\n', 'M'),
(50, 'Marché public de  fournitures', 'le marché qui a pour objet l’achat, le crédit-bail,\r\nla location ou la location-vente avec ou sans option d’achat de biens de toute nature y compris des matières premières, produits, équipements et objets sous forme  solide,  liquide  ou  gazeuse,  ainsi  que  les  services  accessoires  �   la fourniture de ces biens\r\n', 'M'),
(51, 'Marché public de  services', 'Le marché qui n’est ni un marché de travaux ni un\r\nmarché de fournitures. Il comprend également le marché de prestations intellectuelles, c''est-� -dire le marché de services dont l’élément prédominant n’est pas physiquement quantifiable\r\n', 'M'),
(52, 'Marché public de  travaux', 'le marché qui a pour objet soit, l’exécution, soit,\r\nconjointement, la conception et l’exécution de travaux ou d’un ouvrage\r\n', 'M'),
(53, 'Marché public  de type mixte', 'Le marché relevant d’une des trois catégories\r\nmentionnées ci-dessus qui peut comporter, �  titre accessoire, des éléments relevant d’une autre catégorie. Les procédures de passation et d’exécution des marchés publics devront prendre compte les spécificités applicables pour chaque type d’acquisition.\r\n', 'M'),
(54, 'Moyen  électronique', 'Le moyen utilisant des équipements électroniques de\r\ntraitement (y compris la compression numérique) et de stockage de données, et utilisant la diffusion, l''acheminement et la réception par fils, par radio, par moyens optiques ou par d''autres moyens électromagnétiques\r\n', 'M'),
(55, 'Offre', 'L''ensemble des éléments techniques et financiers inclus dans le dossier de soumission\r\n', 'O'),
(56, 'Organisme de droit public', 'l’organisme,\r\na)  créé pour satisfaire spécifiquement des besoins d''intérêt général ayant un caractère autre qu''industriel ou commercial ;\r\nb)  doté de la personnalité juridique, et\r\nc)  dont soit l''activité est financée majoritairement par l''État, les collectivités territoriales  ou  d''autres  organismes  de  droit  public,  soit  la  gestion  est\r\nsoumise �  un contrôle par ces derniers, soit l''organe d''administration, de direction ou de surveillance est composé de membres dont plus de la moitié  sont  désignés par l''État, les collectivités territoriales ou d''autres organismes de droit public\r\n', 'O'),
(57, 'Ouvrage', 'Le résultat d’un ensemble de travaux de bâtiment ou de génie civil destiné �  remplir par lui-même une fonction économique ou technique. Il peut comprendre  notamment des opérations de construction, de reconstruction, de démolition, de réparation ou rénovation, tel que la préparation du chantier, les travaux de terrassement, l’érection, la construction, l’installation d’équipement ou de matériel, la décoration et la finition ainsi que les services accessoires aux travaux  si  la  valeur  de  ces  services  ne  dépasse  pas  celle  des  travaux  eux- mêmes\r\n', 'O'),
(58, 'Personne  responsable  du  marché', 'Le  représentant  dûment  mandaté  par l’autorité contractante pour la représenter dans la passation et dans l’exécution du marché\r\n', 'P'),
(59, 'Régie  intéressée', ':  Le  contrat  par  lequel  l’autorité  contractante  finance  elle-même l’établissement d’un service, mais en confie la gestion �  une personne privée ou publique qui est rémunérée par l’autorité contracta nte tout en étant intéressée aux résultats que ce soit au regard des économies réalisées, des gains de productivité ou de l’amélioration de la qualité du service\r\n', 'R'),
(60, 'Soumissionnaire', 'La personne physique ou morale qui participe �  un appel\r\nd’offres en soumettant un acte d’engagement et les éléments constitutifs de son offre\r\n', 'S'),
(61, 'Soumission', 'L''acte d’engagement écrit au terme duquel un soumissionnaire fait connaître ses conditions et s''engage �  respecter les cahiers des charges applicables', 'S'),
(62, 'Titulaire', 'La personne physique ou morale, attributaire, dont le marché conclu avec  l’autorité  contractante,  conformément  �   la  Directive 04/2005/CM/UEMOA,  a  été approuvé.\r\n', 'T'),
(63, 'Way', 'chemin', 'W');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_grillesanalyses`
--

CREATE TABLE IF NOT EXISTS `pmb_grillesanalyses` (
  `idgrillesanalyses` bigint(20) NOT NULL AUTO_INCREMENT,
  `idcritereanalyse` bigint(20) DEFAULT NULL,
  `Id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idgrillesanalyses`),
  KEY `FKA0AE70327896BF76` (`Id`),
  KEY `FKA0AE703270F4102B` (`idcritereanalyse`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_grillesanalyses`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_historiqueappeloffres_ac`
--

CREATE TABLE IF NOT EXISTS `pmb_historiqueappeloffres_ac` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `daofichier` varchar(255) DEFAULT NULL,
  `histo_commentaire` varchar(255) DEFAULT NULL,
  `histo_datevalidation` datetime DEFAULT NULL,
  `histo_fichiervalidation` varchar(255) DEFAULT NULL,
  `histo_validation` varchar(255) DEFAULT NULL,
  `histoac_ac` int(11) NOT NULL,
  `histoac_attribution` int(11) DEFAULT NULL,
  `histoac_borderau` varchar(255) DEFAULT NULL,
  `histoac_commentaire` varchar(255) DEFAULT NULL,
  `histoac_datecreationdossier` datetime DEFAULT NULL,
  `histoac_datevalidation` date DEFAULT NULL,
  `histoac_fichiervalidation` varchar(255) DEFAULT NULL,
  `histoac_validation` varchar(255) DEFAULT NULL,
  `histo_ac` bigint(20) DEFAULT NULL,
  `dosID` bigint(20) DEFAULT NULL,
  `user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKE6412EBF6840AB71` (`dosID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Contenu de la table `pmb_historiqueappeloffres_ac`
--

INSERT INTO `pmb_historiqueappeloffres_ac` (`ID`, `daofichier`, `histo_commentaire`, `histo_datevalidation`, `histo_fichiervalidation`, `histo_validation`, `histoac_ac`, `histoac_attribution`, `histoac_borderau`, `histoac_commentaire`, `histoac_datecreationdossier`, `histoac_datevalidation`, `histoac_fichiervalidation`, `histoac_validation`, `histo_ac`, `dosID`, `user`) VALUES
(1, '1117628042013_Pj_02-ECBU.pdf', '', '2013-04-28 00:00:00', '1318628042013_Pj_02-ECBU.pdf', '1', 0, 0, 'sss', '', '2013-04-28 00:00:00', '2013-04-28', NULL, NULL, 6, 1, 17),
(2, '5633829042013_Pj_adresse.rtf', '', '2013-04-29 00:00:00', '3834829042013_Pj_exemple.rtf', '1', 0, 1, 'ddd', '', '2013-04-28 00:00:00', '2013-04-29', NULL, NULL, 6, 1, 17),
(3, '2139829042013_Pj_adresse.rtf', '', '2013-04-29 00:00:00', '1540829042013_Pj_adresse.rtf', '1', 0, 2, 'ddd', '', '2013-04-28 00:00:00', '2013-04-29', NULL, NULL, 6, 1, 17),
(4, '1714104052013_Pj_adresse.rtf', '', '2013-05-04 00:00:00', '0717104052013_Pj_adresse.rtf', '1', 0, 0, 'www', '', '2013-05-04 00:00:00', '2013-05-04', NULL, NULL, 6, 2, 17),
(5, '4649004052013_Pj_adresse.rtf', '', '2013-05-04 00:00:00', '2050004052013_Pj_adresse.rtf', '1', 0, 0, NULL, 'ffff', '2013-05-04 00:00:00', '2013-05-04', NULL, NULL, 6, 4, 17),
(6, '0330104052013_Pj_adresse.rtf', '', '2013-05-04 00:00:00', '5930104052013_Pj_adresse.rtf', '1', 0, 0, NULL, '', '2013-05-04 00:00:00', '2013-05-04', NULL, NULL, 6, 5, 17),
(7, '1352304052013_Pj_adresse.rtf', 'ddd', '2013-05-04 00:00:00', '5852304052013_Pj_motdepasse.rtf', '1', 0, 1, 'ff', 'fff', '2013-05-04 00:00:00', '2013-05-04', NULL, NULL, 6, 5, 17),
(8, '5816905052013_Pj_adresse.rtf', '', '2013-05-05 00:00:00', '3417905052013_Pj_adresse.rtf', '1', 0, 0, 'fgg', '', '2013-05-05 00:00:00', '2013-05-05', NULL, NULL, 6, 10, 17),
(9, '1741905052013_Pj_adresse.rtf', '', '2013-05-05 00:00:00', '5641905052013_Pj_adresse.rtf', '1', 0, 1, 'ss', '', '2013-05-05 00:00:00', '2013-05-05', NULL, NULL, 6, 10, 17),
(10, '0644905052013_Pj_adresse.rtf', '', '2013-05-05 00:00:00', '5844905052013_Pj_adresse.rtf', '1', 0, 2, 'ss', '', '2013-05-05 00:00:00', '2013-05-05', NULL, NULL, 6, 10, 17),
(11, '3730205052013_Pj_adresse.rtf', '', '2013-05-05 00:00:00', '1931205052013_Pj_adresse.rtf', '1', 0, 0, 'ss', 'ss', '2013-05-05 00:00:00', '2013-05-05', NULL, NULL, 6, 12, 17),
(12, '0044205052013_Pj_adresse.rtf', '', '2013-05-05 00:00:00', '2744205052013_Pj_adresse.rtf', '1', 0, 1, 'ss', 'ss', '2013-05-05 00:00:00', '2013-05-05', NULL, NULL, 6, 12, 17),
(13, '5346205052013_Pj_adresse.rtf', '', '2013-05-05 00:00:00', '5447205052013_Pj_adresse.rtf', '1', 0, 2, 'ss', 'ss', '2013-05-05 00:00:00', '2013-05-05', NULL, NULL, 6, 12, 17),
(14, '0707808052013_Pj_recu.pdf', 'xxxx', '2013-05-08 00:00:00', '0412808052013_Pj_recu.pdf', '2', 0, 1, 'ss', 'sss', '2013-05-06 00:00:00', '2013-05-08', NULL, NULL, 902, 14, 18),
(15, '1029808052013_Pj_recu.pdf', 'xxx', '2013-05-08 00:00:00', '4229808052013_Pj_recu.pdf', '1', 0, 0, 'ss', 'sss', '2013-05-08 00:00:00', '2013-05-08', NULL, NULL, 902, 15, 18),
(16, '5008908052013_Pj_recu.pdf', 'ras', '2013-05-08 00:00:00', '2309908052013_Pj_recu.pdf', '1', 0, 1, 'cc', 'ccc', '2013-05-08 00:00:00', '2013-05-08', NULL, NULL, 902, 15, 18),
(17, '1011908052013_Pj_recu.pdf', 'xxx', '2013-05-08 00:00:00', '0512908052013_Pj_recu.pdf', '1', 0, 2, 'cc', 'ccc', '2013-05-08 00:00:00', '2013-05-08', NULL, NULL, 902, 15, 18),
(18, '5927908052013_Pj_recu.pdf', 'xxx', '2013-05-08 00:00:00', '3728908052013_Pj_recu.pdf', '1', 0, 0, NULL, 'ddd', '2013-05-08 00:00:00', '2013-05-08', NULL, NULL, 902, 16, 18),
(19, '5733908052013_Pj_recu.pdf', 'ww', '2013-05-08 00:00:00', '3134908052013_Pj_recu.pdf', '1', 0, 0, NULL, 'sss', '2013-05-08 00:00:00', '2013-05-08', NULL, NULL, 902, 17, 18),
(20, '01461008052013_Pj_adresse.rtf', 'Rejet�e', '2013-05-08 00:00:00', '3134908052013_Pj_recu.pdf', '2', 0, 0, '564', 'Soumission pour validation', '2013-05-08 00:00:00', '2013-05-08', NULL, NULL, 9, 18, 19),
(21, '49531008052013_Pj_adresse.rtf', 'ras', '2013-05-08 00:00:00', '22541008052013_Pj_adresse.rtf', '1', 0, 0, 'ddd', 'ddd', '2013-05-08 00:00:00', '2013-05-08', NULL, NULL, 9, 18, 19),
(22, '0726009052013_Pj_recu.pdf', 'rejet�', '2013-05-09 00:00:00', '3052009052013_Pj_recu.pdf', '2', 0, 1, 'dfdf', '', '2013-05-08 00:00:00', '2013-05-09', NULL, NULL, 9, 18, 19),
(23, '2754009052013_Pj_recu.pdf', '', '2013-05-09 00:00:00', '0456009052013_Pj_recu.pdf', '1', 0, 1, 'dfdf', '', '2013-05-08 00:00:00', '2013-05-09', NULL, NULL, 9, 18, 19),
(24, '5210109052013_Pj_recu.pdf', '', '2013-05-09 00:00:00', '2625109052013_Pj_recu.pdf', '1', 0, 2, 'dfdf', '', '2013-05-08 00:00:00', '2013-05-09', NULL, NULL, 9, 18, 19),
(25, '4619509052013_Pj_recu.pdf', 'ras', '2013-05-09 00:00:00', '48151009052013_Pj_recu.pdf', '1', 0, 0, NULL, 'ras', '2013-05-09 00:00:00', '2013-05-09', NULL, NULL, 902, 19, 18),
(26, '3547510052013_Pj_recu.pdf', '', '2013-05-10 00:00:00', '4651510052013_Pj_recu.pdf', '1', 0, 0, NULL, 'ras', '2013-05-10 00:00:00', '2013-05-10', NULL, NULL, 902, 20, 18),
(27, '1438910052013_Pj_recu.pdf', 'ras', '2013-05-10 00:00:00', '0446910052013_Pj_recu.pdf', '1', 0, 1, 'ss', 'ss', '2013-05-10 00:00:00', '2013-05-10', NULL, NULL, 902, 20, 18),
(28, '49541110052013_Pj_recu.pdf', 'valid�', '2013-05-10 00:00:00', '37561110052013_Pj_recu.pdf', '1', 0, 0, NULL, 'M', '2013-05-10 00:00:00', '2013-05-10', NULL, NULL, 905, 21, 21),
(29, '0920010052013_Pj_recu.pdf', '', '2013-05-10 00:00:00', '4920010052013_Pj_recu.pdf', '1', 0, 0, NULL, '', '2013-05-10 00:00:00', '2013-05-10', NULL, NULL, 905, 22, 21);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_historiquedossier`
--

CREATE TABLE IF NOT EXISTS `pmb_historiquedossier` (
  `hisID` bigint(20) NOT NULL AUTO_INCREMENT,
  `hisBudgetAnomalie` varchar(255) DEFAULT NULL,
  `hisCommentaireRejet` varchar(255) DEFAULT NULL,
  `hisDateRejet` date DEFAULT NULL,
  `hisFichierRejeter` varchar(255) DEFAULT NULL,
  `Appelsoffres_ID` bigint(20) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`hisID`),
  KEY `FKBB3AF0B421E224C0` (`Dossiers_ID`),
  KEY `FKBB3AF0B425D5960` (`Appelsoffres_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_historiquedossier`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_historiqueplan`
--

CREATE TABLE IF NOT EXISTS `pmb_historiqueplan` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `commentaireMiseValidation` varchar(255) DEFAULT NULL,
  `CommentaireRejet` varchar(255) DEFAULT NULL,
  `histoac_datevalidation` date DEFAULT NULL,
  `dateRejet` datetime DEFAULT NULL,
  `etat` varchar(255) DEFAULT NULL,
  `fichierMiseValidation` varchar(255) DEFAULT NULL,
  `fichierRejet` varchar(255) DEFAULT NULL,
  `Plan_ID` bigint(20) DEFAULT NULL,
  `dateMiseEnValidation` date DEFAULT NULL,
  `user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Contenu de la table `pmb_historiqueplan`
--

INSERT INTO `pmb_historiqueplan` (`ID`, `commentaireMiseValidation`, `CommentaireRejet`, `histoac_datevalidation`, `dateRejet`, `etat`, `fichierMiseValidation`, `fichierRejet`, `Plan_ID`, `dateMiseEnValidation`, `user`) VALUES
(1, '', '', NULL, '2013-04-28 00:00:00', 'OUI', '', '', 9, '2013-04-28', 17),
(2, '', '', NULL, '2013-05-03 00:00:00', 'OUI', '', '', 11, '2013-05-03', 17),
(3, '', '', NULL, '2013-05-06 00:00:00', 'NON', '', '', 12, '2013-05-06', 18),
(4, '', 'ddd', NULL, '2013-05-06 00:00:00', 'OUI', '3311606052013_Pj_adresse.rtf', '5027606052013_Pj_recu.pdf', 12, '2013-05-06', 18),
(17, '', 'dd', NULL, '2013-05-06 00:00:00', 'NON', '4604906052013_Pj_recu.pdf', '3506906052013_Pj_recu.pdf', 13, '2013-05-06', 18),
(18, '', 'ss', NULL, '2013-05-06 00:00:00', 'OUI', '5107906052013_Pj_modele.docx', '2509906052013_Pj_recu.pdf', 13, '2013-05-06', 18),
(19, '', 'Rejet�', NULL, '2013-05-07 00:00:00', 'NON', '5141507052013_Pj_recu.pdf', '1343507052013_Pj_recu.pdf', 14, '2013-05-07', 19),
(20, '', 'Valid�', NULL, '2013-05-07 00:00:00', 'OUI', '2044507052013_Pj_table.rtf', '4544507052013_Pj_modele.doc', 14, '2013-05-07', 19),
(21, '', '', NULL, '2013-05-07 00:00:00', 'OUI', '5814607052013_Pj_recu.pdf', '4616607052013_Pj_recu.pdf', 15, '2013-05-07', 19),
(22, '', '', NULL, '2013-05-10 00:00:00', 'OUI', '22291110052013_Pj_recu.pdf', '11411110052013_Pj_recu.pdf', 16, '2013-05-10', 21),
(23, 'dd', 'zz', NULL, '2013-05-10 00:00:00', 'OUI', '30461110052013_Pj_recu.pdf', '32471110052013_Pj_recu.pdf', 17, '2013-05-10', 21),
(24, 'sss', 'sss', NULL, '2013-05-27 00:00:00', 'OUI', '', '', 22, '2013-05-27', 19),
(25, '', '', NULL, '2013-06-02 00:00:00', 'OUI', '3150902062013_Pj_recu.pdf', '', 27, '2013-06-02', 19);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_issuerecours`
--

CREATE TABLE IF NOT EXISTS `pmb_issuerecours` (
  `issID` bigint(20) NOT NULL AUTO_INCREMENT,
  `issLibelle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`issID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_issuerecours`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_journal`
--

CREATE TABLE IF NOT EXISTS `pmb_journal` (
  `idJournal` bigint(20) NOT NULL AUTO_INCREMENT,
  `numero` varchar(50) DEFAULT NULL,
  `datePub` date DEFAULT NULL,
  `fichierJournal` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idJournal`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `pmb_journal`
--

INSERT INTO `pmb_journal` (`idJournal`, `numero`, `datePub`, `fichierJournal`, `description`) VALUES
(1, 's', '2013-05-07', '', 's');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_listenoire`
--

CREATE TABLE IF NOT EXISTS `pmb_listenoire` (
  `LisID` int(10) NOT NULL AUTO_INCREMENT,
  `LisRaisonSociale` tinytext COLLATE latin1_general_ci NOT NULL,
  `LisTypeEntreprise` varchar(40) COLLATE latin1_general_ci NOT NULL,
  `LisNinea` tinytext COLLATE latin1_general_ci NOT NULL,
  `LisRefDecision` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `LisdateFinSanction` date NOT NULL DEFAULT '0000-00-00',
  `LisMotif` tinytext COLLATE latin1_general_ci NOT NULL,
  `LisCommentaire` tinytext COLLATE latin1_general_ci NOT NULL,
  `LisdateDebutSanction` date NOT NULL DEFAULT '0000-00-00',
  `LisDecisionId` int(11) NOT NULL,
  PRIMARY KEY (`LisID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci ROW_FORMAT=DYNAMIC COMMENT='Liste des entreprises mise en liste noire' AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_listenoire`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_lots`
--

CREATE TABLE IF NOT EXISTS `pmb_lots` (
  `lotID` bigint(20) NOT NULL AUTO_INCREMENT,
  `lotCommentaire` varchar(255) DEFAULT NULL,
  `lotLibelle` varchar(255) DEFAULT NULL,
  `lotMontantCaution` decimal(19,2) DEFAULT NULL,
  `lotNumero` varchar(255) DEFAULT NULL,
  `Contrat_ID` bigint(20) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`lotID`),
  KEY `FKA533493C21E224C0` (`Dossiers_ID`),
  KEY `FKA533493C1770904D` (`Contrat_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `pmb_lots`
--

INSERT INTO `pmb_lots` (`lotID`, `lotCommentaire`, `lotLibelle`, `lotMontantCaution`, `lotNumero`, `Contrat_ID`, `Dossiers_ID`) VALUES
(1, 'Lot 1', 'Lot 1', NULL, '1', NULL, 1),
(2, '', 'Lot 1', '400000000.00', '1', NULL, 3),
(3, '', 'Lot 1', NULL, '1', NULL, 10),
(4, '', 'Lot 1', '500000000.00', '1', NULL, 11),
(5, '', 'Lot 1', NULL, '1', NULL, 12),
(6, '', 'Lot 1', '10000000.00', '1', NULL, 13),
(7, '', 'Lot 2', '50000000.00', '2', NULL, 13),
(8, '', 'Lot 1', NULL, '1', NULL, 14),
(9, '', 'Lot 1', NULL, '1', NULL, 15),
(10, '', 'Lot', NULL, '1', NULL, 18),
(11, '', 'Lot 2', NULL, '2', NULL, 18);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_marchefournisseurs`
--

CREATE TABLE IF NOT EXISTS `pmb_marchefournisseurs` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Appelsoffres_ID` bigint(20) DEFAULT NULL,
  `Fournisseurs_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKF2D1F1EEF35C9F63` (`Fournisseurs_ID`),
  KEY `FKF2D1F1EE25D5960` (`Appelsoffres_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `pmb_marchefournisseurs`
--

INSERT INTO `pmb_marchefournisseurs` (`ID`, `Appelsoffres_ID`, `Fournisseurs_ID`) VALUES
(1, 3, 1),
(2, 3, 2),
(4, 5, 1),
(5, 5, 2),
(6, 8, 1),
(7, 8, 2),
(8, 8, 3),
(9, 10, 1),
(10, 10, 2),
(11, 10, 3);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_membrecellulepassation`
--

CREATE TABLE IF NOT EXISTS `pmb_membrecellulepassation` (
  `celID` bigint(20) NOT NULL AUTO_INCREMENT,
  `celEmail` varchar(255) DEFAULT NULL,
  `celFonction` varchar(255) DEFAULT NULL,
  `celNom` varchar(255) DEFAULT NULL,
  `celPrenom` varchar(255) DEFAULT NULL,
  `celSelectionne` int(11) DEFAULT NULL,
  `celService` varchar(255) DEFAULT NULL,
  `celTel` varchar(255) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`celID`),
  KEY `FK23FCE2924B9DC270` (`Autorite_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_membrecellulepassation`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_membrescommissionmarche`
--

CREATE TABLE IF NOT EXISTS `pmb_membrescommissionmarche` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `actif` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `etapePI` int(11) DEFAULT NULL,
  `flagpresident` int(11) DEFAULT NULL,
  `fonction` varchar(255) DEFAULT NULL,
  `gestion` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `statut` varchar(30) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  `typemembre` varchar(255) DEFAULT NULL,
  `arrete` varchar(50) DEFAULT NULL,
  `fichierarrete` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `pmb_membrescommissionmarche`
--

INSERT INTO `pmb_membrescommissionmarche` (`ID`, `actif`, `email`, `etapePI`, `flagpresident`, `fonction`, `gestion`, `nom`, `prenom`, `statut`, `telephone`, `Autorite_ID`, `typemembre`, `arrete`, `fichierarrete`, `photo`) VALUES
(1, 0, 'aliou.diop@yahoo.fr', 0, 1, 'Responsable', '2013', 'Diop', 'Aliou', NULL, '', 6, 'Type membre', NULL, NULL, 'photo_Diop.png'),
(2, 0, 'ds@ssi.sn', 0, 0, 'sss', '2014', 'sd', 'sd', NULL, 'ss', 6, 'Type membre', NULL, NULL, NULL),
(3, 0, 'sss@yahoo.fr', 0, 0, 'ddd', '2013', 'sss', 'ss', NULL, '456', 902, 'Type membre', NULL, NULL, NULL),
(4, 0, 'asamn@ssi.sn', 0, 1, 'Test', NULL, 'Samb', 'Adama', NULL, '', 9, 'Type membre', NULL, NULL, NULL),
(5, 0, 'asamb@ssi.sn', 0, 0, 'test', '2013', 'Samb', 'Adama', NULL, '', 9, 'Type membre', NULL, NULL, NULL),
(6, 0, 'asamb@ssi.sn', 0, 0, 'Developpeur', '2013', 'Samb', 'Adama', NULL, '', 905, 'Type membre', NULL, NULL, NULL),
(7, 0, 'asamb@ssi.sn', 0, 0, 'Test', '2013', 'Diop', 'Ibou', NULL, '', 905, 'Type membre', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_modeledocument`
--

CREATE TABLE IF NOT EXISTS `pmb_modeledocument` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) DEFAULT NULL,
  `description` varchar(10) DEFAULT NULL,
  `fichier` varchar(254) DEFAULT NULL,
  `libelle` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_modeledocument`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_modereception`
--

CREATE TABLE IF NOT EXISTS `pmb_modereception` (
  `idmodereception` bigint(20) NOT NULL AUTO_INCREMENT,
  `codemodereception` varchar(10) DEFAULT NULL,
  `descriptionmodereception` varchar(10) DEFAULT NULL,
  `libellemodereception` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idmodereception`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `pmb_modereception`
--

INSERT INTO `pmb_modereception` (`idmodereception`, `codemodereception`, `descriptionmodereception`, `libellemodereception`) VALUES
(1, 'CR', 'Coursier', 'Coursier'),
(3, 'MAIL', 'MAIL', 'MAIL');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_modetraitement`
--

CREATE TABLE IF NOT EXISTS `pmb_modetraitement` (
  `idmodetraitement` bigint(20) NOT NULL AUTO_INCREMENT,
  `codemodetraitement` varchar(10) DEFAULT NULL,
  `libellemodetraitement` varchar(255) DEFAULT NULL,
  `descriptionmodetraitement` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idmodetraitement`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `pmb_modetraitement`
--

INSERT INTO `pmb_modetraitement` (`idmodetraitement`, `codemodetraitement`, `libellemodetraitement`, `descriptionmodetraitement`) VALUES
(2, 'NM', 'Normal', 'Normal'),
(3, 'UR', 'Urgent', 'Urgent'),
(4, 'TR', 'Tr�s urgent', 'Tr�s urgent');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_monnaieoffre`
--

CREATE TABLE IF NOT EXISTS `pmb_monnaieoffre` (
  `monId` bigint(10) NOT NULL AUTO_INCREMENT,
  `monCode` char(5) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `monLibelle` varchar(150) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`monId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `pmb_monnaieoffre`
--

INSERT INTO `pmb_monnaieoffre` (`monId`, `monCode`, `monLibelle`) VALUES
(1, 'CFA', 'CFA'),
(2, 'EUROS', 'EUROS'),
(3, 'DOLLA', 'DOLLARS');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_montantsseuils`
--

CREATE TABLE IF NOT EXISTS `pmb_montantsseuils` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `examen` varchar(40) DEFAULT NULL,
  `modeengagement` varchar(255) DEFAULT NULL,
  `montantinferieur` decimal(19,2) DEFAULT NULL,
  `montantsuperieur` decimal(19,2) DEFAULT NULL,
  `MODEPASSATION` bigint(20) DEFAULT NULL,
  `TYPEAUTORITE` bigint(20) DEFAULT NULL,
  `TYPEMARCHE` bigint(20) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK4B52693BBB47723C` (`MODEPASSATION`),
  KEY `FK4B52693B73B9D2D8` (`TYPEAUTORITE`),
  KEY `FK4B52693B9C2E6DA` (`TYPEMARCHE`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Contenu de la table `pmb_montantsseuils`
--

INSERT INTO `pmb_montantsseuils` (`ID`, `examen`, `modeengagement`, `montantinferieur`, `montantsuperieur`, `MODEPASSATION`, `TYPEAUTORITE`, `TYPEMARCHE`, `type`) VALUES
(1, NULL, NULL, '5000000.00', '30000000.00', 42, 1, 4, 'SEUILSPASSATION'),
(2, 'DNCMP', 'March�', '50000000.00', NULL, 1, 1, 3, 'SEUILSRAPRIORI'),
(3, 'DNCMP', 'March�', '25000000.00', NULL, 1, 1, 1, 'SEUILSRAPRIORI'),
(4, 'DNCMP', 'March�', '50000000.00', NULL, 42, 1, 4, 'SEUILSRAPRIORI'),
(5, NULL, NULL, '500000000.00', NULL, 1, 1, 3, 'SEUILSCOMMUNAUTAIRE'),
(6, NULL, NULL, '25000000.00', NULL, 42, 1, 4, 'SEUILSCOMMUNAUTAIRE'),
(7, 'DNCMP', 'Appel d�offres', '50000000.00', NULL, 1, 1, 2, 'SEUILSRAPRIORI'),
(8, 'DNCMP', 'Appel d�offres', '50000000.00', NULL, 1, 1, 5, 'SEUILSRAPRIORI'),
(9, 'DNCMP', 'AP', '50000000.00', NULL, 1, 2, 3, 'SEUILSRAPRIORI'),
(10, 'DNCMP', 'AOO', '50000000.00', NULL, 1, 2, 2, 'SEUILSRAPRIORI'),
(11, 'DNCMP', 'MI', '25000000.00', NULL, 42, 2, 4, 'SEUILSRAPRIORI'),
(12, 'DNCMP', 'AOO', '50000000.00', NULL, 1, 2, 1, 'SEUILSRAPRIORI'),
(13, 'DNCMP', 'AOO', '50000000.00', NULL, 1, 2, 5, 'SEUILSRAPRIORI'),
(14, 'DNCMP', 'AOO', '50000000.00', NULL, 1, 3, 3, 'SEUILSRAPRIORI'),
(15, 'DNCMP', 'AOO', '50000000.00', NULL, 1, 3, 2, 'SEUILSRAPRIORI'),
(16, 'DNCMP', 'AOO', '25000000.00', NULL, 42, 3, 4, 'SEUILSRAPRIORI'),
(17, 'DNCMP', 'AOO', '50000000.00', NULL, 1, 3, 1, 'SEUILSRAPRIORI'),
(18, 'DNCMP', 'AOO', '50000000.00', NULL, 1, 3, 5, 'SEUILSRAPRIORI'),
(19, 'DNCMP', 'AOO', '50000000.00', NULL, 1, 5, 3, 'SEUILSRAPRIORI'),
(20, 'DNCMP', 'AOO', '50000000.00', NULL, 1, 5, 2, 'SEUILSRAPRIORI'),
(21, 'DNCMP', 'AOO', '50000000.00', NULL, 1, 5, 1, 'SEUILSRAPRIORI'),
(22, 'DNCMP', 'AOO', '50000000.00', NULL, 1, 5, 5, 'SEUILSRAPRIORI'),
(23, 'DNCMP', 'MI', '25000000.00', NULL, 42, 5, 4, 'SEUILSRAPRIORI'),
(24, 'DNCMP', 'AOO', '50000000.00', NULL, 1, 6, 3, 'SEUILSRAPRIORI'),
(25, 'DNCMP', 'AOO', '50000000.00', NULL, 1, 6, 2, 'SEUILSRAPRIORI'),
(26, 'DNCMP', 'AOO', '50000000.00', NULL, 1, 6, 1, 'SEUILSRAPRIORI'),
(27, 'DNCMP', 'AOO', '50000000.00', NULL, 1, 6, 5, 'SEUILSRAPRIORI'),
(28, 'DNCMP', 'MI', '25000000.00', NULL, 42, 6, 4, 'SEUILSRAPRIORI');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_naturecourrier`
--

CREATE TABLE IF NOT EXISTS `pmb_naturecourrier` (
  `idnaturecourrier` bigint(20) NOT NULL AUTO_INCREMENT,
  `codenaturecourrier` varchar(10) DEFAULT NULL,
  `descriptionnaturecourrier` varchar(255) DEFAULT NULL,
  `libellenaturecourrier` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idnaturecourrier`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `pmb_naturecourrier`
--

INSERT INTO `pmb_naturecourrier` (`idnaturecourrier`, `codenaturecourrier`, `descriptionnaturecourrier`, `libellenaturecourrier`) VALUES
(1, 'CF', 'Confidentiel', 'Confidentiel'),
(2, 'OD', 'Ordinaire', 'Ordinaire'),
(3, 'SC', 'Secret', 'Secret');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_natureprix`
--

CREATE TABLE IF NOT EXISTS `pmb_natureprix` (
  `natId` bigint(10) NOT NULL AUTO_INCREMENT,
  `natCode` char(10) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `natLibelle` varchar(155) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`natId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `pmb_natureprix`
--

INSERT INTO `pmb_natureprix` (`natId`, `natCode`, `natLibelle`) VALUES
(1, 'TTC', 'TTC: Toute Taxe Comprise'),
(2, 'HT', 'HT: Hors Taxe'),
(3, 'HD', 'Hors Douane');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_observateursindependants`
--

CREATE TABLE IF NOT EXISTS `pmb_observateursindependants` (
  `IDpresenceouverture` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateconvocation` date DEFAULT NULL,
  `presence` varchar(255) DEFAULT NULL,
  `qualite` varchar(255) DEFAULT NULL,
  `representant` varchar(255) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `etape` int(11) DEFAULT NULL,
  PRIMARY KEY (`IDpresenceouverture`),
  KEY `FK7D074F8421E224C0` (`Dossiers_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `pmb_observateursindependants`
--

INSERT INTO `pmb_observateursindependants` (`IDpresenceouverture`, `dateconvocation`, `presence`, `qualite`, `representant`, `Dossiers_ID`, `etape`) VALUES
(1, NULL, NULL, 'Qualit�', 'Ama', 1, 0),
(2, NULL, NULL, 'dd', 'dd', 3, 0),
(3, NULL, NULL, 'ff', 'ff', 5, 0),
(4, NULL, NULL, 'sss', 's', 9, 0),
(5, NULL, NULL, 'ss', 'ss', 10, 0),
(6, NULL, NULL, 'ss', 'ss', 11, 0),
(7, NULL, NULL, 'xx', 'xx', 12, 0),
(8, NULL, NULL, 'ff', 'ff', 13, 0),
(9, NULL, NULL, 'sss', 'ss', 14, 0),
(10, NULL, NULL, 'qq', 'qq', 15, 0),
(12, NULL, NULL, 'dd', 'dd', 20, 0),
(13, NULL, NULL, 'Qualit�', 'Diop', 22, 0);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_operationpaiement`
--

CREATE TABLE IF NOT EXISTS `pmb_operationpaiement` (
  `opeCode` varchar(255) NOT NULL,
  `opeCommentaires` varchar(255) DEFAULT NULL,
  `opeDateRecu` date DEFAULT NULL,
  `opeMontantTotal` decimal(19,2) DEFAULT NULL,
  `opeRecude` varchar(255) DEFAULT NULL,
  `opMontantTotalVerse` decimal(19,2) DEFAULT NULL,
  `fournisseurID` bigint(20) DEFAULT NULL,
  `opmontantTotalVerseenlettre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`opeCode`),
  KEY `FK47DDCC58BB2F0857` (`fournisseurID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `pmb_operationpaiement`
--

INSERT INTO `pmb_operationpaiement` (`opeCode`, `opeCommentaires`, `opeDateRecu`, `opeMontantTotal`, `opeRecude`, `opMontantTotalVerse`, `fournisseurID`, `opmontantTotalVerseenlettre`) VALUES
('F_3201305003', '', '2013-05-12', '0.00', 'Entreprise  TFC (Travaux-Fournitures-con)', '0.00', 3, 'z�ro');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_operationrecouvrement`
--

CREATE TABLE IF NOT EXISTS `pmb_operationrecouvrement` (
  `opeCode` varchar(255) NOT NULL,
  `opeCommentaires` varchar(255) DEFAULT NULL,
  `opeDateRecu` date DEFAULT NULL,
  `opeMontantTotal` decimal(19,2) DEFAULT NULL,
  `opeRecude` varchar(255) DEFAULT NULL,
  `opMontantTotalVerse` decimal(19,2) DEFAULT NULL,
  `autoriteID` bigint(20) DEFAULT NULL,
  `opmontantTotalEnLettre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`opeCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `pmb_operationrecouvrement`
--

INSERT INTO `pmb_operationrecouvrement` (`opeCode`, `opeCommentaires`, `opeDateRecu`, `opeMontantTotal`, `opeRecude`, `opMontantTotalVerse`, `autoriteID`, `opmontantTotalEnLettre`) VALUES
('R6201305001', '', '2013-05-11', '16500000.00', 'Minist�re de la Fonction Publique et des R�formes Administratives ', NULL, 6, 'seize millions cinq cents mille'),
('R902201305001', '', '2013-05-07', '10000000.00', 'Minist�re de la communication', NULL, 902, 'dix millions'),
('R905201305001', '', '2013-05-13', '0.00', 'Minist�re des travaux publics', NULL, 905, 'z�ro'),
('R9201305001', '', '2013-05-12', '400000.00', 'Minist�re de l��conomie et des finances', NULL, 9, 'quatre cents mille');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_paiement`
--

CREATE TABLE IF NOT EXISTS `pmb_paiement` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `banque` varchar(100) DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  `modepaiement` varchar(10) DEFAULT NULL,
  `numCheque` varchar(100) DEFAULT NULL,
  `contratsId` bigint(20) DEFAULT NULL,
  `fournisseurId` bigint(20) DEFAULT NULL,
  `operationID` varchar(50) DEFAULT NULL,
  `banqueID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK834A3EA5EF4092C5` (`banqueID`),
  KEY `FK834A3EA51770DB61` (`contratsId`),
  KEY `FK834A3EA5BB2F0857` (`fournisseurId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `pmb_paiement`
--

INSERT INTO `pmb_paiement` (`id`, `banque`, `commentaire`, `modepaiement`, `numCheque`, `contratsId`, `fournisseurId`, `operationID`, `banqueID`) VALUES
(1, NULL, '', 'Ch�que', 'sss', 10, 3, 'F_3201305003', 1);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_penaliteretard`
--

CREATE TABLE IF NOT EXISTS `pmb_penaliteretard` (
  `penid` bigint(20) NOT NULL AUTO_INCREMENT,
  `datepenalite` date DEFAULT NULL,
  `montant` double DEFAULT NULL,
  `observation` varchar(255) DEFAULT NULL,
  `taux` int(11) DEFAULT NULL,
  `demane` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`penid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_penaliteretard`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_piece`
--

CREATE TABLE IF NOT EXISTS `pmb_piece` (
  `IDPIECE` bigint(20) NOT NULL AUTO_INCREMENT,
  `codepiece` varchar(10) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `localisation` varchar(3) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IDPIECE`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `pmb_piece`
--

INSERT INTO `pmb_piece` (`IDPIECE`, `codepiece`, `description`, `libelle`, `localisation`, `type`, `Autorite_ID`) VALUES
(1, '1', 'Attestation de non faillite', 'Attestation de non faillite', 'n', 0, 6),
(2, '2', 'Inscription au registre de commerce et du crédit mobilier ou registre des métiers', 'Inscription au registre de commerce et du crédit mobilier ou registre des métiers', 'n', 0, 6),
(3, 'ANF', 'Attestation de non faillite', 'Attestation de non faillite', 'n', 0, 902),
(4, 'IRCMRM', 'Inscription au registre de commerce et du cr�dit mobilier ou registre des m�tiers', 'Inscription au registre de commerce et du cr�dit mobilier ou registre des m�tiers', 'n', 0, 902),
(5, '1', 'Quitus Fiscal', 'Quitus Fiscal', 'n', 0, 9),
(6, '2', 'Attestation de non faillite', 'Attestation de non faillite', 'ni', 0, 9),
(7, '1', 'Quitus fiscal', 'Quitus fiscal', 'n', 0, 905);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_piecesplisouvertures`
--

CREATE TABLE IF NOT EXISTS `pmb_piecesplisouvertures` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `etat` varchar(10) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `piece_ID` bigint(20) DEFAULT NULL,
  `pliouverture_ID` bigint(20) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK849675C721E224C0` (`Dossiers_ID`),
  KEY `FK849675C74CEAE4D7` (`piece_ID`),
  KEY `FK849675C757A27964` (`pliouverture_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Contenu de la table `pmb_piecesplisouvertures`
--

INSERT INTO `pmb_piecesplisouvertures` (`ID`, `etat`, `Dossiers_ID`, `piece_ID`, `pliouverture_ID`, `libelle`) VALUES
(1, 'F', 1, 1, 1, NULL),
(2, 'F', 1, 2, 1, NULL),
(3, 'F', 1, 1, 2, NULL),
(4, 'F', 1, 2, 2, NULL),
(5, 'F', 1, 1, 3, NULL),
(6, 'F', 1, 2, 3, NULL),
(7, 'F', 3, 2, 4, NULL),
(8, 'F', 3, 2, 5, NULL),
(9, 'F', 5, 2, 8, NULL),
(10, 'F', 5, 2, 9, NULL),
(11, 'F', 9, 2, 10, NULL),
(12, 'F', 9, 2, 11, NULL),
(13, 'F', 10, 2, 12, NULL),
(14, 'F', 10, 2, 13, NULL),
(15, 'F', 10, 2, 14, NULL),
(16, 'F', 11, 1, 15, NULL),
(17, 'F', 11, 2, 15, NULL),
(18, 'F', 11, 1, 16, NULL),
(19, 'F', 11, 2, 16, NULL),
(20, 'F', 11, 1, 17, NULL),
(21, 'F', 11, 2, 17, NULL),
(22, 'F', 12, 1, 18, NULL),
(23, 'F', 12, 2, 18, NULL),
(24, 'F', 12, 1, 19, NULL),
(25, 'F', 12, 2, 19, NULL),
(26, 'F', 12, 1, 20, NULL),
(27, 'F', 12, 2, 20, NULL),
(28, 'F', 13, 1, 21, NULL),
(29, 'F', 13, 2, 21, NULL),
(30, 'F', 13, 1, 22, NULL),
(31, 'F', 13, 2, 22, NULL),
(32, 'F', 13, 1, 23, NULL),
(33, 'F', 13, 2, 23, NULL),
(34, 'F', 14, 3, 24, NULL),
(35, 'F', 14, 4, 24, NULL),
(36, 'F', 14, 3, 25, NULL),
(37, 'F', 14, 4, 25, NULL),
(38, 'F', 14, 3, 26, NULL),
(39, 'F', 14, 4, 26, NULL),
(40, 'F', 15, 4, 27, NULL),
(41, 'F', 15, 4, 28, NULL),
(42, 'F', 18, 6, 34, NULL),
(43, 'F', 18, 5, 34, NULL),
(44, 'F', 18, 6, 35, NULL),
(45, 'F', 18, 5, 35, NULL),
(46, 'F', 18, 6, 36, NULL),
(47, 'F', 18, 5, 36, NULL),
(48, 'F', 18, 6, 37, NULL),
(49, 'F', 18, 5, 37, NULL),
(50, 'F', 20, 4, 42, NULL),
(51, 'F', 20, 4, 43, NULL),
(52, 'NF', 22, 7, 48, NULL),
(53, 'NF', 22, 7, 49, NULL),
(54, 'NF', 22, 7, 50, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_piecesrequises`
--

CREATE TABLE IF NOT EXISTS `pmb_piecesrequises` (
  `idpiecesrequises` bigint(20) NOT NULL AUTO_INCREMENT,
  `Id` bigint(20) DEFAULT NULL,
  `IDPIECE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idpiecesrequises`),
  KEY `FK30FFBF2E7896BF76` (`Id`),
  KEY `FK30FFBF2ED96B837E` (`IDPIECE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_piecesrequises`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_pjaudit`
--

CREATE TABLE IF NOT EXISTS `pmb_pjaudit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Date` date DEFAULT NULL,
  `Datecourrier` date DEFAULT NULL,
  `decision` varchar(5) DEFAULT NULL,
  `Fichier` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `recevable` varchar(3) DEFAULT NULL,
  `reference` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_pjaudit`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_pjcontentieux`
--

CREATE TABLE IF NOT EXISTS `pmb_pjcontentieux` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Fichier` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `contentieuxID` bigint(20) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `recevable` varchar(50) DEFAULT NULL,
  `decision` varchar(50) DEFAULT NULL,
  `Datecourrier` date DEFAULT NULL,
  `reference` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `pmb_pjcontentieux`
--

INSERT INTO `pmb_pjcontentieux` (`id`, `Fichier`, `libelle`, `contentieuxID`, `Date`, `recevable`, `decision`, `Datecourrier`, `reference`) VALUES
(2, '1811107052013_Pj_indicateur.rtf', 'ddddd', 1, '2013-05-07', 'AC', NULL, '2013-05-07', 'ddd'),
(3, '0317107052013_Pj_recu.pdf', 'sss', 1, '2013-05-07', 'ACD', 'PlaignantD', '2013-05-07', 'sss'),
(4, '1517107052013_Pj_recu.pdf', 'ss', 1, '2013-05-07', 'ACD', 'PlaignantD', '2013-05-07', 'sss'),
(5, '5639811052013_Pj_recu.pdf', 'sss', 2, '2013-05-11', 'Plaignant', NULL, '2013-05-11', 'sss'),
(6, '1040811052013_Pj_recu.pdf', 'ssssss', 2, '2013-05-11', 'AC', NULL, '2013-05-11', 'sssssss'),
(7, '0144811052013_Pj_recu.pdf', 'fff', 2, '2013-05-11', NULL, 'PlaignantD', '2013-05-11', 'fff'),
(8, '2244811052013_Pj_recu.pdf', 'ccc', 2, '2013-05-11', 'ACD', 'PlaignantD', '2013-05-11', 'fff'),
(9, '5332613052013_Pj_recu.pdf', 'DAO', 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_pjdenonciation`
--

CREATE TABLE IF NOT EXISTS `pmb_pjdenonciation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Date` date DEFAULT NULL,
  `Datecourrier` date DEFAULT NULL,
  `decision` varchar(50) DEFAULT NULL,
  `Fichier` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `recevable` varchar(20) DEFAULT NULL,
  `reference` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6F10DA97ECB9AC44` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `pmb_pjdenonciation`
--

INSERT INTO `pmb_pjdenonciation` (`id`, `Date`, `Datecourrier`, `decision`, `Fichier`, `libelle`, `recevable`, `reference`) VALUES
(2, '2013-05-08', '2013-05-08', 'PlaignantD', '58431008052013_Pj_adresse.rtf', 'dddd', NULL, 'dd'),
(3, '2013-05-08', '2013-05-08', 'PlaignantD', '03481008052013_Pj_adresse.rtf', 'ggg', 'ACD', 'ggg'),
(4, '2013-05-08', '2013-05-08', 'PlaignantD', '03481008052013_Pj_adresse.rtf', 'ggg', 'ACD', 'ggg');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_pjevenement`
--

CREATE TABLE IF NOT EXISTS `pmb_pjevenement` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Date` date DEFAULT NULL,
  `Datecourrier` date DEFAULT NULL,
  `Fichier` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `reference` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_pjevenement`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_plansdepassation`
--

CREATE TABLE IF NOT EXISTS `pmb_plansdepassation` (
  `IDinfoplan` bigint(20) NOT NULL AUTO_INCREMENT,
  `annee` int(11) DEFAULT NULL,
  `BorderauPlan` varchar(20) DEFAULT NULL,
  `budcontrole` varchar(3) DEFAULT NULL,
  `budgetanomalie` varchar(3) DEFAULT NULL,
  `commentaireMiseValidation` varchar(255) DEFAULT NULL,
  `commentaireValidation` varchar(255) DEFAULT NULL,
  `DateAuPlusTard` date DEFAULT NULL,
  `dateMiseEnValidation` date DEFAULT NULL,
  `dateRejet` date DEFAULT NULL,
  `dateValidation` date DEFAULT NULL,
  `Datecreation` date DEFAULT NULL,
  `datepublication` date DEFAULT NULL,
  `etatValidPlan` varchar(3) DEFAULT NULL,
  `fichierMiseValidation` varchar(255) DEFAULT NULL,
  `fichier_validation` varchar(255) DEFAULT NULL,
  `lastVersionValid` int(11) DEFAULT NULL,
  `motif` varchar(255) DEFAULT NULL,
  `Nomresp` varchar(20) DEFAULT NULL,
  `NumeroDernierRealisation` int(11) DEFAULT NULL,
  `Numplan` varchar(20) DEFAULT NULL,
  `prenomResp` varchar(20) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  `commentaires` varchar(255) DEFAULT NULL,
  `referenceMiseValidation` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `commentairePublication` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `infoplanID` bigint(20) DEFAULT NULL,
  `referenceValidation` varchar(255) DEFAULT NULL,
  `datedebut` date DEFAULT NULL,
  `datefin` date DEFAULT NULL,
  PRIMARY KEY (`IDinfoplan`),
  KEY `FK8789E8734B9DC270` (`Autorite_ID`),
  KEY `FK8789E873484933B1` (`infoplanID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Contenu de la table `pmb_plansdepassation`
--

INSERT INTO `pmb_plansdepassation` (`IDinfoplan`, `annee`, `BorderauPlan`, `budcontrole`, `budgetanomalie`, `commentaireMiseValidation`, `commentaireValidation`, `DateAuPlusTard`, `dateMiseEnValidation`, `dateRejet`, `dateValidation`, `Datecreation`, `datepublication`, `etatValidPlan`, `fichierMiseValidation`, `fichier_validation`, `lastVersionValid`, `motif`, `Nomresp`, `NumeroDernierRealisation`, `Numplan`, `prenomResp`, `Autorite_ID`, `commentaires`, `referenceMiseValidation`, `status`, `commentairePublication`, `version`, `infoplanID`, `referenceValidation`, `datedebut`, `datefin`) VALUES
(9, 2013, NULL, NULL, NULL, '', '', NULL, '2013-04-28', NULL, '2013-04-28', '2013-04-28', '2013-04-28', NULL, '', '', 0, NULL, NULL, 0, 'MFPRA_2013_1', NULL, 6, '', '', 'PUB', '', 1, NULL, '', '2013-01-01', '2013-12-31'),
(10, 2013, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-01', NULL, NULL, NULL, NULL, 0, 'Mise � jour', NULL, 0, 'MFPRA_2013_2', NULL, 6, NULL, NULL, 'SAISIE', NULL, 2, 9, NULL, NULL, NULL),
(11, 2014, NULL, NULL, NULL, '', '', NULL, '2013-05-03', NULL, '2013-05-03', '2013-05-03', '2013-05-03', NULL, '', '', 0, NULL, NULL, 0, 'MFPRA_2014_1', NULL, 6, '', '', 'PUB', '', 1, NULL, '', '2014-01-01', '2014-12-31'),
(12, 2013, NULL, NULL, NULL, '', 'ddd', NULL, '2013-05-06', '2013-05-06', '2013-05-06', '2013-05-06', '2013-05-06', NULL, '3311606052013_Pj_adresse.rtf', '5027606052013_Pj_recu.pdf', 0, NULL, NULL, 0, 'MC_2013_1', NULL, 902, '', 'ssss', 'SAISIE', '', 1, NULL, 'dd', '2013-01-01', '2013-12-31'),
(13, 2013, NULL, NULL, NULL, '', 'dd', NULL, '2013-05-06', '2013-05-06', '2013-05-06', '2013-05-06', '2013-05-06', NULL, '5107906052013_Pj_modele.docx', '4408906052013_Pj_recu.pdf', 0, 'Mise � jour\n', NULL, 0, 'MC_2013_2', NULL, 902, NULL, 'xx', 'PUB', '', 2, 12, 'dd', NULL, NULL),
(14, 2013, NULL, NULL, NULL, '', 'Valid�', NULL, '2013-05-07', '2013-05-07', '2013-05-07', '2013-05-07', '2013-05-07', NULL, '2044507052013_Pj_table.rtf', '4544507052013_Pj_modele.doc', 0, NULL, NULL, 0, 'MEF_2013_1', NULL, 9, '', 'www', 'PUB', '', 1, NULL, 'xxxx', '2013-01-01', '2013-12-31'),
(15, 2013, NULL, NULL, NULL, '', '', NULL, '2013-05-07', NULL, '2013-05-07', '2013-05-07', '2013-05-07', NULL, '5814607052013_Pj_recu.pdf', '4616607052013_Pj_recu.pdf', 0, 'Nouveau cr�dit', NULL, 0, 'MEF_2013_2', NULL, 9, NULL, 'dd', 'PUB', '', 2, 14, 'ddd', NULL, NULL),
(16, 2013, NULL, NULL, NULL, '', '', NULL, '2013-05-10', NULL, '2013-05-10', '2013-05-10', '2013-05-10', NULL, '22291110052013_Pj_recu.pdf', '11411110052013_Pj_recu.pdf', 0, NULL, NULL, 0, 'MTP_2013_1', NULL, 905, '', '4567E', 'PUB', '', 1, NULL, '456', '2013-01-01', '2013-12-31'),
(17, 2013, NULL, NULL, NULL, 'dd', 'zz', NULL, '2013-05-10', NULL, '2013-05-10', '2013-05-10', '2013-05-10', NULL, '30461110052013_Pj_recu.pdf', '32471110052013_Pj_recu.pdf', 0, 'Mise � jour', NULL, 0, 'MTP_2013_2', NULL, 905, NULL, 'ddd', 'PUB', '', 2, 16, 'zz', NULL, NULL),
(18, 2013, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-11', NULL, NULL, NULL, NULL, 0, 'Mise � jour', NULL, 0, 'MC_2013_3', NULL, 902, NULL, NULL, 'SAISIE', NULL, 3, 12, NULL, NULL, NULL),
(22, 2013, NULL, NULL, NULL, 'sss', 'sss', NULL, '2013-05-27', NULL, '2013-05-27', '2013-05-26', '2013-05-27', NULL, '', '', 0, 'MAJ', NULL, 0, 'MEF_2013_3', NULL, 9, NULL, 'sss', 'PUB', '', 3, 14, 's', NULL, NULL),
(26, 2013, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-05-28', NULL, NULL, NULL, NULL, 0, 'maj', NULL, 0, 'MEF_2013_4', NULL, 9, NULL, NULL, 'SAISIE', NULL, 4, 14, NULL, NULL, NULL),
(30, 2014, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-06-11', NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 'MEF_2014_1', NULL, 9, '', NULL, 'SAISIE', NULL, 1, NULL, NULL, '2014-01-01', '2014-12-31');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_plilot`
--

CREATE TABLE IF NOT EXISTS `pmb_plilot` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `lotrecu` varchar(3) DEFAULT NULL,
  `lotsoumis` varchar(3) DEFAULT NULL,
  `plilEtatExamenPreliminaire` int(11) DEFAULT NULL,
  `plilEtatPreselection` int(11) DEFAULT NULL,
  `plilValide` int(11) DEFAULT NULL,
  `pliladresseMail` varchar(255) DEFAULT NULL,
  `plilattributaireProvisoire` int(11) DEFAULT NULL,
  `plilclassementechnique` int(11) DEFAULT NULL,
  `plilclassementgeneral` int(11) DEFAULT NULL,
  `plilcommentaire` varchar(255) DEFAULT NULL,
  `plilcritereQualification` int(11) DEFAULT NULL,
  `plildateDepot` date DEFAULT NULL,
  `plilibelle` varchar(255) DEFAULT NULL,
  `plilmontantoffert` decimal(19,2) DEFAULT NULL,
  `plilnumero` varchar(255) DEFAULT NULL,
  `plilrabais` int(11) DEFAULT NULL,
  `plilraisonsociale` varchar(255) DEFAULT NULL,
  `plilscorefinal` decimal(19,2) DEFAULT NULL,
  `plilscorefinancier` decimal(19,2) DEFAULT NULL,
  `plilscorefinancierpondere` decimal(19,2) DEFAULT NULL,
  `plilscoretechnique` decimal(19,2) DEFAULT NULL,
  `plilscoretechniquepondere` decimal(19,2) DEFAULT NULL,
  `plilsrixevalue` decimal(19,2) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `Lot_ID` bigint(20) DEFAULT NULL,
  `Pli_ID` bigint(20) DEFAULT NULL,
  `plillettreSoumission` int(11) DEFAULT NULL,
  `plilmontantdefinitif` decimal(19,2) DEFAULT NULL,
  `plilnumeros` varchar(255) DEFAULT NULL,
  `pliloffreFinanciere` int(11) DEFAULT NULL,
  `pliloffreTechnique` int(11) DEFAULT NULL,
  `plilmonCode` bigint(20) DEFAULT NULL,
  `plilnatCode` bigint(20) DEFAULT NULL,
  `plilpays` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK2C29F9BE21E224C0` (`Dossiers_ID`),
  KEY `FK2C29F9BEB5246B11` (`Lot_ID`),
  KEY `FK2C29F9BE2B7B217B` (`Pli_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Contenu de la table `pmb_plilot`
--

INSERT INTO `pmb_plilot` (`ID`, `lotrecu`, `lotsoumis`, `plilEtatExamenPreliminaire`, `plilEtatPreselection`, `plilValide`, `pliladresseMail`, `plilattributaireProvisoire`, `plilclassementechnique`, `plilclassementgeneral`, `plilcommentaire`, `plilcritereQualification`, `plildateDepot`, `plilibelle`, `plilmontantoffert`, `plilnumero`, `plilrabais`, `plilraisonsociale`, `plilscorefinal`, `plilscorefinancier`, `plilscorefinancierpondere`, `plilscoretechnique`, `plilscoretechniquepondere`, `plilsrixevalue`, `Dossiers_ID`, `Lot_ID`, `Pli_ID`, `plillettreSoumission`, `plilmontantdefinitif`, `plilnumeros`, `pliloffreFinanciere`, `pliloffreTechnique`, `plilmonCode`, `plilnatCode`, `plilpays`) VALUES
(1, 'non', 'oui', 1, 1, 1, 'ssi@ssi.sn', 1, 0, 0, NULL, 2, NULL, NULL, '450000000.00', '1', 0, '2SI(solutions et strat�gies informatique)', '0.00', '0.00', '0.00', '0.00', '0.00', '450000000.00', 1, 1, 1, 0, '450000000.00', NULL, 0, 0, 1, 2, 42),
(2, 'non', 'oui', 1, 1, 1, 'ati@sentoo.sn', 0, 0, 0, NULL, 2, NULL, NULL, '500000000.00', '2', 0, 'A.T.I multim�dia', '0.00', '0.00', '0.00', '0.00', '0.00', '500000000.00', 1, 1, 2, 0, '500000000.00', NULL, 0, 0, 1, 2, 210),
(3, 'non', 'oui', 1, 1, 1, 'tfc@yahoo.fr', 0, 0, 0, NULL, 2, NULL, NULL, '500000000.00', '3', 0, 'Entreprise  TFC (Travaux-Fournitures-con)', '0.00', '0.00', '0.00', '0.00', '0.00', '500000000.00', 1, 1, 3, 0, '500000000.00', NULL, 0, 0, 1, 2, 170),
(4, 'non', 'oui', 1, 1, 1, 'ssi@ssi.sn', 1, 0, 0, NULL, 2, NULL, NULL, '500000000.00', '1', 0, '2SI(solutions et strat�gies informatique)', '0.00', '0.00', '0.00', '0.00', '0.00', '500000000.00', 3, 2, 4, 0, '500000000.00', NULL, 0, 0, 1, 2, 170),
(5, 'non', 'oui', 1, 1, 1, 'ati@sentoo.sn', 0, 0, 0, NULL, 2, NULL, NULL, '510000000.00', '2', 0, 'A.T.I multim�dia', '0.00', '0.00', '0.00', '0.00', '0.00', '510000000.00', 3, 2, 5, 0, '510000000.00', NULL, 0, 0, 1, 2, 193),
(6, 'non', 'oui', 1, 1, 1, 'ssi@ssi.sn', 1, 0, 0, NULL, 2, NULL, NULL, '5000000000.00', '1', 0, '2SI(solutions et strat�gies informatique)', '0.00', '0.00', '0.00', '0.00', '0.00', '5000000000.00', 10, 3, 12, 0, '5000000000.00', NULL, 0, 0, 1, 2, 170),
(7, 'non', 'oui', 1, 1, 1, 'tfc@yahoo.fr', 0, 0, 0, NULL, 2, NULL, NULL, '7000000000.00', '2', 0, 'Entreprise  TFC (Travaux-Fournitures-con)', '0.00', '0.00', '0.00', '0.00', '0.00', '7000000000.00', 10, 3, 13, 0, '7000000000.00', NULL, 0, 0, 1, 2, 210),
(8, 'non', 'oui', 1, 1, 1, 'neurotech@yahoo.fr', 0, 0, 0, NULL, 2, NULL, NULL, '6000000000.00', '3', 0, 'Neurotech', '0.00', '0.00', '0.00', '0.00', '0.00', '6000000000.00', 10, 3, 14, 0, '6000000000.00', NULL, 0, 0, 1, 2, 170),
(9, 'non', 'oui', 1, 1, 1, 'ssi@ssi.sn', 1, 0, 0, NULL, 2, NULL, NULL, '500000000.00', '1', 0, '2SI(solutions et strat�gies informatique)', '0.00', '0.00', '0.00', '0.00', '0.00', '500000000.00', 11, 4, 15, 0, '500000000.00', NULL, 0, 0, 1, 2, 170),
(10, 'non', 'oui', 1, 1, 1, 'ati@sentoo.sn', 0, 0, 0, NULL, 2, NULL, NULL, '550000000.00', '2', 0, 'A.T.I multim�dia', '0.00', '0.00', '0.00', '0.00', '0.00', '550000000.00', 11, 4, 16, 0, '550000000.00', NULL, 0, 0, 1, 2, 193),
(11, 'non', 'oui', 1, 1, 1, 'tfc@@yahoo.fr', 0, 0, 0, NULL, 2, NULL, NULL, '600000000.00', '3', 0, 'Entreprise  TFC (Travaux-Fournitures-con)', '0.00', '0.00', '0.00', '0.00', '0.00', '600000000.00', 11, 4, 17, 0, '600000000.00', NULL, 0, 0, 1, 2, 210),
(12, 'non', 'oui', 1, 1, 1, 'ssi@ssi.sn', 1, 0, 0, NULL, 2, NULL, NULL, '400000000.00', '1', 0, '2SI(solutions et strat�gies informatique)', '0.00', '0.00', '0.00', '0.00', '0.00', '400000000.00', 12, 5, 18, 0, '400000000.00', NULL, 0, 0, 1, 2, 170),
(13, 'non', 'oui', 1, 1, 1, 'ati@sentoo.sn', 0, 0, 0, NULL, 2, NULL, NULL, '410000000.00', '2', 0, 'A.T.I multim�dia', '0.00', '0.00', '0.00', '0.00', '0.00', '410000000.00', 12, 5, 19, 0, '410000000.00', NULL, 0, 0, 1, 2, 193),
(14, 'non', 'oui', 1, 1, 1, 'neurotech@yahoo.fr', 0, 0, 0, NULL, 2, NULL, NULL, '450000000.00', '3', 0, 'Neurotech', '0.00', '0.00', '0.00', '0.00', '0.00', '450000000.00', 12, 5, 20, 0, '450000000.00', NULL, 0, 0, 1, 2, 170),
(15, 'non', 'oui', 1, 1, 1, 'ssi@ssi.sn', 0, 0, 0, NULL, 2, NULL, NULL, '10000000.00', '1', 0, '2SI(solutions et strat�gies informatique)', '0.00', '0.00', '0.00', '0.00', '0.00', '10000000.00', 13, 6, 21, 0, '10000000.00', NULL, 0, 0, 1, 2, 170),
(16, 'non', 'oui', 1, 1, 1, 'ssi@ssi.sn', 1, 0, 0, NULL, 2, NULL, NULL, '50000000.00', '1', 0, '2SI(solutions et strat�gies informatique)', '0.00', '0.00', '0.00', '0.00', '0.00', '50000000.00', 13, 7, 21, 0, '50000000.00', NULL, 0, 0, 1, 2, 170),
(17, 'non', 'non', 0, 0, 0, 'ati@sentoo.sn', 0, 0, 0, NULL, 0, NULL, NULL, '0.00', '0', 0, 'A.T.I multim�dia', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 13, 6, 22, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(18, 'non', 'oui', 1, 1, 1, 'ati@sentoo.sn', 0, 0, 0, NULL, 2, NULL, NULL, '53000000.00', '2', 0, 'A.T.I multim�dia', '0.00', '0.00', '0.00', '0.00', '0.00', '53000000.00', 13, 7, 22, 0, '53000000.00', NULL, 0, 0, 1, 2, 193),
(19, 'non', 'oui', 1, 1, 1, 'tfc@@yahoo.fr', 1, 0, 0, NULL, 2, NULL, NULL, '9000000.00', '3', 0, 'Entreprise  TFC (Travaux-Fournitures-con)', '0.00', '0.00', '0.00', '0.00', '0.00', '9000000.00', 13, 6, 23, 0, '9000000.00', NULL, 0, 0, 1, 2, 210),
(20, 'non', 'non', 0, 0, 0, 'tfc@@yahoo.fr', 0, 0, 0, NULL, 0, NULL, NULL, '0.00', '0', 0, 'Entreprise  TFC (Travaux-Fournitures-con)', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 13, 7, 23, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(21, 'non', 'oui', 1, 1, 1, 'ssi@ssi.sn', 1, 0, 0, NULL, 2, NULL, NULL, '10000000.00', '1', 0, '2SI(solutions et strat�gies informatique)', '0.00', '0.00', '0.00', '0.00', '0.00', '10000000.00', 14, 8, 24, 0, '10000000.00', NULL, 0, 0, 1, 2, 170),
(22, 'non', 'oui', 1, 1, 1, 'ati@sentoo.sn', 0, 0, 0, NULL, 2, NULL, NULL, '12000000.00', '2', 0, 'A.T.I multim�dia', '0.00', '0.00', '0.00', '0.00', '0.00', '12000000.00', 14, 8, 25, 0, '12000000.00', NULL, 0, 0, 1, 2, 193),
(23, 'non', 'oui', 1, 1, 1, 'tfc@yahoo.fr', 0, 0, 0, NULL, 2, NULL, NULL, '13000000.00', '3', 0, 'Entreprise  TFC (Travaux-Fournitures-con)', '0.00', '0.00', '0.00', '0.00', '0.00', '13000000.00', 14, 8, 26, 0, '13000000.00', NULL, 0, 0, 1, 2, 210),
(24, 'non', 'oui', 1, 1, 1, 'ssi@ssi.sn', 1, 0, 0, NULL, 0, NULL, NULL, '600000000.00', '1', 0, '2SI(solutions et strat�gies informatique)', '0.00', '0.00', '0.00', '0.00', '0.00', '600000000.00', 15, 9, 27, 0, '600000000.00', NULL, 0, 0, 1, 2, 170),
(25, 'non', 'oui', 1, 1, 1, 'ati@sentoo.sn', 0, 0, 0, NULL, 0, NULL, NULL, '610000000.00', '2', 0, 'A.T.I multim�dia', '0.00', '0.00', '0.00', '0.00', '0.00', '610000000.00', 15, 9, 28, 0, '610000000.00', NULL, 0, 0, 1, 2, 193),
(26, 'non', 'oui', 1, 1, 1, 'ssi@ssi.sn', 1, 0, 0, NULL, 2, NULL, NULL, '200000000.00', '1', 0, '2SI(solutions et strat�gies informatique)', '0.00', '0.00', '0.00', '0.00', '0.00', '200000000.00', 18, 10, 34, 0, '200000000.00', NULL, 0, 0, 1, 2, 170),
(27, 'non', 'oui', 1, 1, 1, 'ssi@ssi.sn', 0, 0, 0, NULL, 2, NULL, NULL, '100000000.00', '1', 0, '2SI(solutions et strat�gies informatique)', '0.00', '0.00', '0.00', '0.00', '0.00', '100000000.00', 18, 11, 34, 0, '100000000.00', NULL, 0, 0, 1, 2, 170),
(28, 'non', 'oui', 1, 1, 1, 'ati@sentoo.sn', 0, 0, 0, NULL, 2, NULL, NULL, '300000000.00', '2', 0, 'A.T.I multim�dia', '0.00', '0.00', '0.00', '0.00', '0.00', '300000000.00', 18, 10, 35, 0, '300000000.00', NULL, 0, 0, 1, 2, 193),
(29, 'non', 'non', 0, 0, 0, 'ati@sentoo.sn', 0, 0, 0, NULL, 2, NULL, NULL, '0.00', '0', 0, 'A.T.I multim�dia', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 18, 11, 35, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(30, 'non', 'non', 0, 0, 0, 'neurotech@yahoo.fr', 0, 0, 0, NULL, 2, NULL, NULL, '0.00', '0', 0, 'Neurotech', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 18, 10, 36, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(31, 'non', 'oui', 1, 1, 1, 'neurotech@yahoo.fr', 1, 0, 0, NULL, 2, NULL, NULL, '95000000.00', '3', 0, 'Neurotech', '0.00', '0.00', '0.00', '0.00', '0.00', '95000000.00', 18, 11, 36, 0, '95000000.00', NULL, 0, 0, 1, 2, 170),
(32, 'non', 'oui', 1, 1, 1, 'tf@yahoo.fr', 0, 0, 0, NULL, 2, NULL, NULL, '350000000.00', '4', 0, 'Entreprise  TFC (Travaux-Fournitures-con)', '0.00', '0.00', '0.00', '0.00', '0.00', '350000000.00', 18, 10, 37, 0, '350000000.00', NULL, 0, 0, 1, 2, 210),
(33, 'non', 'oui', 1, 1, 1, 'tf@yahoo.fr', 0, 0, 0, NULL, 2, NULL, NULL, '110000000.00', '4', 0, 'Entreprise  TFC (Travaux-Fournitures-con)', '0.00', '0.00', '0.00', '0.00', '0.00', '100000000.00', 18, 11, 37, 0, '110000000.00', NULL, 0, 0, 1, 2, 210);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_pliouverture`
--

CREATE TABLE IF NOT EXISTS `pmb_pliouverture` (
  `IDpliouverture` bigint(20) NOT NULL AUTO_INCREMENT,
  `AdresseMail` varchar(100) DEFAULT NULL,
  `Ajustement` bigint(20) DEFAULT NULL,
  `attributaireProvisoire` int(11) DEFAULT NULL,
  `candidatrestreint_ID` int(11) DEFAULT NULL,
  `classementechnique` int(11) DEFAULT NULL,
  `classementgeneral` int(11) DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  `critereQualification` int(11) DEFAULT NULL,
  `dateDepot` date DEFAULT NULL,
  `EtatExamenPreliminaire` int(11) DEFAULT NULL,
  `EtatPreselection` int(11) DEFAULT NULL,
  `lettreSoumission` int(11) DEFAULT NULL,
  `monCode` varchar(10) DEFAULT NULL,
  `montantdefinitif` decimal(19,2) DEFAULT NULL,
  `montantoffert` decimal(19,2) DEFAULT NULL,
  `natCode` varchar(10) DEFAULT NULL,
  `negociation` varchar(3) DEFAULT NULL,
  `Ninea` varchar(50) DEFAULT NULL,
  `Numero` varchar(20) DEFAULT NULL,
  `offreFinanciere` int(11) DEFAULT NULL,
  `offreTechnique` int(11) DEFAULT NULL,
  `Prixevalue` decimal(19,2) DEFAULT NULL,
  `pvoffrefinanciere` varchar(255) DEFAULT NULL,
  `rabais` int(11) DEFAULT NULL,
  `Raisonsociale` varchar(255) DEFAULT NULL,
  `rang` varchar(10) DEFAULT NULL,
  `scorefinal` decimal(19,2) DEFAULT NULL,
  `Scorefinancier` decimal(19,2) DEFAULT NULL,
  `scorefinancierpondere` decimal(19,2) DEFAULT NULL,
  `scoretechnique` int(11) DEFAULT NULL,
  `scoretechniquepondere` decimal(19,2) DEFAULT NULL,
  `seuilatteint` int(11) DEFAULT NULL,
  `Valide` int(11) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `fournisseurs_ID` bigint(20) DEFAULT NULL,
  `retraitregistredao_ID` bigint(20) DEFAULT NULL,
  `pays` varchar(255) DEFAULT NULL,
  `heuredepot` time DEFAULT NULL,
  `modereception` varchar(255) DEFAULT NULL,
  `observationscandidats` varchar(255) DEFAULT NULL,
  `observationsoffres` varchar(255) DEFAULT NULL,
  `notepreselectionne` decimal(19,2) DEFAULT NULL,
  `garantie` int(11) DEFAULT NULL,
  `piecerequise` int(11) DEFAULT NULL,
  `notifie` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IDpliouverture`),
  KEY `FK24A2729E21E224C0` (`Dossiers_ID`),
  KEY `FK24A2729EF35C9F63` (`fournisseurs_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Contenu de la table `pmb_pliouverture`
--

INSERT INTO `pmb_pliouverture` (`IDpliouverture`, `AdresseMail`, `Ajustement`, `attributaireProvisoire`, `candidatrestreint_ID`, `classementechnique`, `classementgeneral`, `commentaire`, `critereQualification`, `dateDepot`, `EtatExamenPreliminaire`, `EtatPreselection`, `lettreSoumission`, `monCode`, `montantdefinitif`, `montantoffert`, `natCode`, `negociation`, `Ninea`, `Numero`, `offreFinanciere`, `offreTechnique`, `Prixevalue`, `pvoffrefinanciere`, `rabais`, `Raisonsociale`, `rang`, `scorefinal`, `Scorefinancier`, `scorefinancierpondere`, `scoretechnique`, `scoretechniquepondere`, `seuilatteint`, `Valide`, `Autorite_ID`, `Dossiers_ID`, `fournisseurs_ID`, `retraitregistredao_ID`, `pays`, `heuredepot`, `modereception`, `observationscandidats`, `observationsoffres`, `notepreselectionne`, `garantie`, `piecerequise`, `notifie`) VALUES
(1, NULL, NULL, 1, 0, 0, 0, NULL, 2, '2013-04-28', 1, 0, 0, NULL, '0.00', NULL, NULL, NULL, NULL, '1', 0, 0, NULL, NULL, 0, '2SI(solutions et strat�gies informatique)', NULL, NULL, '0.00', NULL, 0, NULL, 0, 1, NULL, 1, 1, 1, '42', '12:00:00', 'Courrier', '', '', NULL, 1, 1, NULL),
(2, NULL, NULL, 0, 0, 0, 0, NULL, 2, '2013-04-28', 1, 0, 0, NULL, '0.00', NULL, NULL, NULL, NULL, '2', 0, 0, NULL, NULL, 0, 'A.T.I multim�dia', NULL, NULL, '0.00', NULL, 0, NULL, 0, 1, NULL, 1, 2, 2, '216', '12:00:00', 'Courrier', '', '', NULL, 1, 1, NULL),
(3, NULL, NULL, 0, 0, 0, 0, NULL, 2, '2013-04-28', 1, 0, 0, NULL, '0.00', NULL, NULL, NULL, NULL, '3', 0, 0, NULL, NULL, 0, 'Entreprise  TFC (Travaux-Fournitures-con)', NULL, NULL, '0.00', NULL, 0, NULL, 0, 1, NULL, 1, 3, 3, NULL, '12:00:00', 'Courrier', '', '', NULL, 1, 1, NULL),
(4, NULL, NULL, 1, 0, 0, 0, NULL, 2, '2013-05-04', 1, 0, 0, NULL, '0.00', NULL, NULL, NULL, NULL, '1', 0, 0, NULL, NULL, 0, '2SI(solutions et strat�gies informatique)', NULL, NULL, '0.00', NULL, 0, NULL, 0, 1, NULL, 3, 1, 4, NULL, '12:00:00', 'Courrier', '', '', NULL, 0, 1, NULL),
(5, NULL, NULL, 0, 0, 0, 0, NULL, 2, '2013-05-04', 1, 0, 0, NULL, '0.00', NULL, NULL, NULL, NULL, '2', 0, 0, NULL, NULL, 0, 'A.T.I multim�dia', NULL, NULL, '0.00', NULL, 0, NULL, 0, 1, NULL, 3, 2, 5, NULL, '14:00:00', 'Courrier', '', '', NULL, 0, 1, NULL),
(6, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-04', 0, 1, 0, NULL, '0.00', NULL, NULL, NULL, NULL, '0', 0, 0, NULL, NULL, 0, '2SI(solutions et strat�gies informatique)', NULL, NULL, '0.00', NULL, 0, NULL, 0, 0, 6, 4, 1, 6, 'S�n�gal', NULL, NULL, NULL, NULL, '98.00', 0, 0, 'oui'),
(7, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-04', 0, 1, 0, NULL, '0.00', NULL, NULL, NULL, NULL, '0', 0, 0, NULL, NULL, 0, 'Neurotech', NULL, NULL, '0.00', NULL, 0, NULL, 0, 0, 6, 4, 5, 8, 'S�n�gal', NULL, NULL, NULL, NULL, '78.00', 0, 0, 'oui'),
(8, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-04', 1, 1, 0, 'EUROS', '6000000000.00', '6000000000.00', 'HT', NULL, NULL, '1', 0, 0, '6000000000.00', NULL, 0, '2SI(solutions et strat�gies informatique)', NULL, '93.00', '100.00', NULL, 0, NULL, 0, 1, NULL, 5, 1, 9, 'S�n�gal', '12:00:00', 'Courrier', '', '', '90.00', 0, 0, NULL),
(9, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-04', 1, 1, 0, 'EUROS', '6500000000.00', '6500000000.00', 'HT', NULL, NULL, '2', 0, 0, '6500000000.00', NULL, 0, 'Neurotech', NULL, '82.29', '92.31', NULL, 0, NULL, 0, 1, NULL, 5, 5, 10, 'S�n�gal', '13:00:00', 'Courrier', '', '', '78.00', 0, 0, NULL),
(10, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-04', 1, 1, 0, 'EUROS', '40000000.00', '40000000.00', 'HT', NULL, NULL, '1', 0, 0, '40000000.00', NULL, 0, 'A.T.I multim�dia', NULL, '93.00', '100.00', NULL, 0, NULL, 0, 1, 6, 9, 1, 12, 'S�n�gal', '18:00:00', 'Courrier', '', '', '90.00', 0, 0, NULL),
(11, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-04', 1, 1, 0, 'EUROS', '45000000.00', '45000000.00', 'HT', NULL, NULL, '2', 0, 0, '45000000.00', NULL, 0, '2SI(solutions et strat�gies informatique)', NULL, '81.27', '88.89', NULL, 0, NULL, 0, 1, 6, 9, 5, 11, 'S�n�gal', '18:00:00', 'Courrier', '', '', '78.00', 0, 0, NULL),
(12, NULL, NULL, 1, 0, 0, 0, NULL, 2, '2013-05-05', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '1', 0, 0, '0.00', NULL, 0, '2SI(solutions et strat�gies informatique)', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 6, 10, 1, 13, 'S�n�gal', '12:00:00', 'Courrier', '', '', NULL, 1, 1, NULL),
(13, NULL, NULL, 0, 0, 0, 0, NULL, 2, '2013-05-05', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '2', 0, 0, '0.00', NULL, 0, 'Entreprise  TFC (Travaux-Fournitures-con)', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 6, 10, 3, 14, 'France', '12:00:00', 'Courrier', '', '', NULL, 1, 1, NULL),
(14, NULL, NULL, 0, 0, 0, 0, NULL, 2, '2013-05-05', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '3', 0, 0, '0.00', NULL, 0, 'Neurotech', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 6, 10, 5, 15, 'S�n�gal', '12:00:00', 'Courrier', '', '', NULL, 1, 1, NULL),
(15, NULL, NULL, 1, 0, 0, 0, NULL, 2, '2013-05-05', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '1', 0, 0, '0.00', NULL, 0, '2SI(solutions et strat�gies informatique)', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 6, 11, 1, 16, 'S�n�gal', '12:00:00', 'Courrier', '', '', NULL, 0, 1, NULL),
(16, NULL, NULL, 0, 0, 0, 0, NULL, 2, '2013-05-05', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '2', 0, 0, '0.00', NULL, 0, 'A.T.I multim�dia', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 6, 11, 2, 17, 'Togo', '01:00:00', 'Courrier', '', '', NULL, 0, 1, NULL),
(17, NULL, NULL, 0, 0, 0, 0, NULL, 2, '2013-05-05', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '3', 0, 0, '0.00', NULL, 0, 'Entreprise  TFC (Travaux-Fournitures-con)', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 6, 11, 3, 18, 'France', '13:00:00', 'Courrier', '', '', NULL, 0, 1, NULL),
(18, NULL, NULL, 1, 0, 0, 0, NULL, 2, '2013-05-05', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '1', 0, 0, '0.00', NULL, 0, '2SI(solutions et strat�gies informatique)', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 6, 12, 1, 19, 'S�n�gal', '12:00:00', 'Courrier', '', '', NULL, 1, 1, NULL),
(19, NULL, NULL, 0, 0, 0, 0, NULL, 2, '2013-05-05', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '2', 0, 0, '0.00', NULL, 0, 'A.T.I multim�dia', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 6, 12, 2, 20, 'Togo', '14:00:00', 'Courrier', '', '', NULL, 1, 1, NULL),
(20, NULL, NULL, 0, 0, 0, 0, NULL, 2, '2013-05-05', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '3', 0, 0, '0.00', NULL, 0, 'Neurotech', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 6, 12, 5, 21, 'S�n�gal', '15:00:00', 'Courrier', '', '', NULL, 1, 1, NULL),
(21, NULL, NULL, 1, 0, 0, 0, NULL, 2, '2013-05-05', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '1', 0, 0, '0.00', NULL, 0, '2SI(solutions et strat�gies informatique)', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 6, 13, 1, 22, 'S�n�gal', '12:00:00', 'Courrier', '', '', NULL, 0, 1, NULL),
(22, NULL, NULL, 0, 0, 0, 0, NULL, 2, '2013-05-05', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '2', 0, 0, '0.00', NULL, 0, 'A.T.I multim�dia', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 6, 13, 2, 23, 'Togo', '12:00:00', 'Courrier', '', '', NULL, 0, 1, NULL),
(23, NULL, NULL, 1, 0, 0, 0, NULL, 2, '2013-05-05', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '3', 0, 0, '0.00', NULL, 0, 'Entreprise  TFC (Travaux-Fournitures-con)', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 6, 13, 3, 24, 'France', '14:00:00', 'Courrier', '', '', NULL, 0, 1, NULL),
(24, NULL, NULL, 1, 0, 0, 0, NULL, 2, '2013-05-06', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '1', 0, 0, '0.00', NULL, 0, '2SI(solutions et strat�gies informatique)', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 902, 14, 1, 25, 'S�n�gal', '12:00:00', 'Courrier', '', '', NULL, 1, 1, NULL),
(25, NULL, NULL, 0, 0, 0, 0, NULL, 2, '2013-05-06', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '2', 0, 0, '0.00', NULL, 0, 'A.T.I multim�dia', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 902, 14, 2, 27, 'Togo', '12:00:00', 'Courrier', '', '', NULL, 1, 1, NULL),
(26, NULL, NULL, 0, 0, 0, 0, NULL, 2, '2013-05-06', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '3', 0, 0, '0.00', NULL, 0, 'Entreprise  TFC (Travaux-Fournitures-con)', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 902, 14, 3, 28, 'France', '12:00:00', 'Courrier', '', '', NULL, 1, 1, NULL),
(27, NULL, NULL, 1, 0, 0, 0, NULL, 0, '2013-05-08', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '1', 0, 0, '0.00', NULL, 0, '2SI(solutions et strat�gies informatique)', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 902, 15, 1, 29, 'S�n�gal', '12:00:00', 'Courrier', '', '', NULL, 1, 1, NULL),
(28, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-08', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '2', 0, 0, '0.00', NULL, 0, 'A.T.I multim�dia', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 902, 15, 2, 30, 'Togo', '12:00:00', 'Courrier', '', '', NULL, 1, 1, NULL),
(29, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-08', 0, 1, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '0', 0, 0, '0.00', NULL, 0, '2SI(solutions et strat�gies informatique)', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 902, 16, 1, 31, 'S�n�gal', NULL, NULL, NULL, NULL, '80.00', 0, 0, 'oui'),
(30, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-08', 0, 1, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '0', 0, 0, '0.00', NULL, 0, 'A.T.I multim�dia', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 902, 16, 2, 32, 'Togo', NULL, NULL, NULL, NULL, '90.00', 0, 0, 'oui'),
(31, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-08', 0, -1, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '0', 0, 0, '0.00', NULL, 0, 'Neurotech', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 902, 16, 5, 33, 'S�n�gal', NULL, NULL, NULL, NULL, '60.00', 0, 0, 'non'),
(32, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-01', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1', 0, 0, NULL, NULL, 0, 'A.T.I multim�dia', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 902, 17, NULL, 34, NULL, '12:00:00', 'Courrier', '', '', NULL, 0, 0, NULL),
(33, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-08', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2', 0, 0, NULL, NULL, 0, '2SI(solutions et strat�gies informatique)', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 902, 17, NULL, 35, NULL, '13:00:00', 'Courrier', '', '', NULL, 0, 0, NULL),
(34, NULL, NULL, 1, 0, 0, 0, NULL, 2, '2013-05-08', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '1', 0, 0, '0.00', NULL, 0, '2SI(solutions et strat�gies informatique)', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 9, 18, 1, 36, 'S�n�gal', '12:00:00', 'Courrier', '', '', NULL, 1, 1, NULL),
(35, NULL, NULL, 0, 0, 0, 0, NULL, 2, '2013-05-08', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '2', 0, 0, '0.00', NULL, 0, 'A.T.I multim�dia', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 9, 18, 2, 37, 'Togo', '12:30:00', 'Courrier', '', '', NULL, 1, 1, NULL),
(36, NULL, NULL, 1, 0, 0, 0, NULL, 2, '2013-05-08', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '3', 0, 0, '0.00', NULL, 0, 'Neurotech', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 9, 18, 5, 38, 'S�n�gal', '13:00:00', 'Courrier', '', '', NULL, 1, 1, NULL),
(37, NULL, NULL, 0, 0, 0, 0, NULL, 2, '2013-05-08', 1, 0, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '4', 0, 0, '0.00', NULL, 0, 'Entreprise  TFC (Travaux-Fournitures-con)', NULL, NULL, NULL, NULL, 0, NULL, 0, 1, 9, 18, 3, 39, 'France', '12:00:00', 'Courrier', '', '', NULL, 1, 1, NULL),
(38, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-09', 0, 1, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '0', 0, 0, '0.00', NULL, 0, '2SI(solutions et strat�gies informatique)', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 902, 19, 1, 40, 'S�n�gal', NULL, NULL, NULL, NULL, '75.00', 0, 0, 'oui'),
(39, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-09', 0, 1, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '0', 0, 0, '0.00', NULL, 0, 'Entreprise  TFC (Travaux-Fournitures-con)', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 902, 19, 3, 41, 'France', NULL, NULL, NULL, NULL, '89.00', 0, 0, 'oui'),
(40, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-10', 0, -1, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '0', 0, 0, '0.00', NULL, 0, 'Neurotech', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 902, 19, 5, 42, 'S�n�gal', NULL, NULL, NULL, NULL, '69.00', 0, 0, 'non'),
(42, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-10', 1, 1, 0, 'CFA', '700000000.00', '700000000.00', 'HT', NULL, NULL, '1', 0, 0, '700000000.00', NULL, 0, 'Entreprise  TFC (Travaux-Fournitures-con)', NULL, '77.43', '71.43', NULL, 0, NULL, 0, 1, 902, 20, NULL, 43, 'France', '12:00:00', 'Courrier', '', '', '80.00', 0, 0, NULL),
(43, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-10', 1, 1, 0, 'CFA', '500000000.00', '500000000.00', 'HT', NULL, NULL, '2', 0, 0, '500000000.00', NULL, 0, '2SI(solutions et strat�gies informatique)', NULL, '95.80', '100.00', NULL, 0, NULL, 0, 1, 902, 20, NULL, 44, 'S�n�gal', '12:00:00', 'Courrier', '', '', '94.00', 0, 0, NULL),
(44, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-10', 0, 1, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '0', 0, 0, '0.00', NULL, 0, 'A.T.I multim�dia', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 905, 21, 2, 45, 'Togo', NULL, NULL, NULL, NULL, '85.00', 0, 0, 'oui'),
(45, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-10', 0, 1, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '0', 0, 0, '0.00', NULL, 0, 'Entreprise  TFC (Travaux-Fournitures-con)', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 905, 21, 3, 46, 'France', NULL, NULL, NULL, NULL, '79.00', 0, 0, 'oui'),
(46, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-10', 0, 1, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '0', 0, 0, '0.00', NULL, 0, 'Solid', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 905, 21, 4, 47, 'S�n�gal', NULL, NULL, NULL, NULL, '73.00', 0, 0, 'oui'),
(47, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-10', 0, -1, 0, NULL, '0.00', '0.00', NULL, NULL, NULL, '0', 0, 0, '0.00', NULL, 0, 'Neurotech', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 905, 21, 5, 48, 'S�n�gal', NULL, NULL, NULL, NULL, '45.00', 0, 0, 'non'),
(48, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-10', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '1', 0, 0, NULL, NULL, 0, 'A.T.I multim�dia', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 905, 22, NULL, 49, 'Togo', '13:00:00', 'Courrier', '', '', NULL, 0, 0, NULL),
(49, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-10', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2', 0, 0, NULL, NULL, 0, 'Entreprise  TFC (Travaux-Fournitures-con)', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 905, 22, NULL, 50, 'France', '13:00:00', 'Courrier', '', '', NULL, 0, 0, NULL),
(50, NULL, NULL, 0, 0, 0, 0, NULL, 0, '2013-05-10', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '3', 0, 0, NULL, NULL, 0, 'Solid', NULL, NULL, NULL, NULL, 0, NULL, 0, 0, 905, 22, NULL, 51, 'S�n�gal', '13:00:00', 'Courrier', '', '', NULL, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_presenceouverture`
--

CREATE TABLE IF NOT EXISTS `pmb_presenceouverture` (
  `IDpresenceouverture` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `etapePI` int(11) DEFAULT NULL,
  `nomrepresentant` varchar(255) DEFAULT NULL,
  `nomstructure` varchar(255) DEFAULT NULL,
  `prenomrepresentant` varchar(255) DEFAULT NULL,
  `supplementaire` varchar(255) DEFAULT NULL,
  `telephone` varchar(16) DEFAULT NULL,
  `Appelsoffres_ID` bigint(20) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `Pli_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IDpresenceouverture`),
  KEY `FKADA5339C21E224C0` (`Dossiers_ID`),
  KEY `FKADA5339C2B7B217B` (`Pli_ID`),
  KEY `FKADA5339C25D5960` (`Appelsoffres_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Contenu de la table `pmb_presenceouverture`
--

INSERT INTO `pmb_presenceouverture` (`IDpresenceouverture`, `email`, `etapePI`, `nomrepresentant`, `nomstructure`, `prenomrepresentant`, `supplementaire`, `telephone`, `Appelsoffres_ID`, `Dossiers_ID`, `Pli_ID`) VALUES
(1, 'ibou.diallo@yahoo.fr', 0, 'Diallo', NULL, 'IBou', '', '6666', 1, 1, 1),
(2, 'ss@gmail.com', 0, 'ss', NULL, 'sss', '', 'ss', 3, 3, 4),
(3, 'qq@ssi.sn', 0, 'qq', NULL, 'qq', '', 'qq', 6, 5, 8),
(4, 'dd@yahoo.fr', 1, 'dd', NULL, 'dd', '', 'dd', 6, 5, 8),
(5, 'sss@yahoo.fr', 0, 'ss', NULL, 'ss', '', 'ss', 5, 9, 10),
(6, 'ss@ssi.sn', 1, 'ss', NULL, 'ss', '', 'ss', 5, 9, 10),
(7, 'ss@yahoo.fr', 0, 'ss', NULL, 'ss', '', 'ss', 7, 10, 12),
(8, 'dd@yahoo.fr', 0, 'dd', NULL, 'dd', '', 'dd', 8, 11, 15),
(9, 'ss@yahoo.fr', 0, 'ss', NULL, 'ss', '', 'ss', 9, 12, 18),
(10, 'ss@yahoo.fr', 0, 'ss', NULL, 'ss', '', 'ss', 10, 13, 21),
(11, 'ss@yahoo.fr', 0, 'ss', NULL, 'ss', '', 'ss', 11, 14, 24),
(12, 'ss@yahoo.fr', 0, 'sss', NULL, 'ss', '', 'ss', 13, 15, 27),
(13, 'dd@yahoo.fr', 0, 'dd', NULL, 'dd', '', 'd', 14, 17, 32),
(14, 'asamb@ssi.sn', 0, 'Diop', NULL, 'Awa', '', '4567', 15, 18, 34),
(15, 'asamb@ssi.sn', 0, 'Diallo', NULL, 'Ibou', '', '6788', 15, 18, 36),
(16, 'sss@yahoo.fr', 0, 'sss', NULL, 'sss', '', '', 16, 20, 42),
(17, '', 1, 'dd', NULL, 'ddd', '', '', 16, 20, 42),
(18, '', 0, 'Samb', NULL, 'Adama', '', '', 17, 22, 48);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_prestataire`
--

CREATE TABLE IF NOT EXISTS `pmb_prestataire` (
  `idprestataire` bigint(20) NOT NULL AUTO_INCREMENT,
  `adresse` varchar(255) DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `numeroprest` varchar(50) DEFAULT NULL,
  `raisonsociale` varchar(255) DEFAULT NULL,
  `idpays` bigint(20) DEFAULT NULL,
  `identifiant` varchar(50) DEFAULT NULL,
  `mails` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `natureprestataire` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idprestataire`),
  KEY `FK5ED4FD18AF6AB437` (`idpays`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `pmb_prestataire`
--

INSERT INTO `pmb_prestataire` (`idprestataire`, `adresse`, `commentaire`, `mail`, `numeroprest`, `raisonsociale`, `idpays`, `identifiant`, `mails`, `nom`, `prenom`, `telephone`, `natureprestataire`) VALUES
(1, 'Dakar', '', 'aa@yahoo.fr', NULL, 'Test', 33, NULL, 'ss@yahoo.fr', 'ss', 'ss', 'ss', 'Entreprise'),
(2, 'ss', '', 'sss', NULL, 'ss', 42, NULL, 'ss', 'ss', 'ss', 'ss', 'Entreprise'),
(3, 'Dakar', '', 'asamb', NULL, 'Ablaye diop', 33, NULL, '', '', '', '', 'Consultant Individuel'),
(4, 'Cotonou', '', 'asamb@ssi.sn', NULL, 'FCG', 215, NULL, 'asamb@ssi.sn', 'Sogbossi', 'Jacque', '6789', 'Entreprise');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_prestatairesaudits`
--

CREATE TABLE IF NOT EXISTS `pmb_prestatairesaudits` (
  `idpaac` bigint(20) NOT NULL AUTO_INCREMENT,
  `idaudit` bigint(20) DEFAULT NULL,
  `idprestataire` bigint(20) DEFAULT NULL,
  `ContratsPrestataires` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idpaac`),
  KEY `FKC0B316133A25A6E3` (`idprestataire`),
  KEY `FKC0B316133C56A135` (`idaudit`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `pmb_prestatairesaudits`
--

INSERT INTO `pmb_prestatairesaudits` (`idpaac`, `idaudit`, `idprestataire`, `ContratsPrestataires`) VALUES
(1, 1, 1, NULL),
(2, 1, 2, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_prestataire_greagre`
--

CREATE TABLE IF NOT EXISTS `pmb_prestataire_greagre` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `AdresseMail` varchar(255) DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  `facture` varchar(255) DEFAULT NULL,
  `Ninea` varchar(255) DEFAULT NULL,
  `numero` varchar(255) DEFAULT NULL,
  `Raisonsociale` varchar(255) DEFAULT NULL,
  `Appelsoffres_ID` bigint(20) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  `pays` bigint(20) DEFAULT NULL,
  `Fournisseur_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `pmb_prestataire_greagre`
--

INSERT INTO `pmb_prestataire_greagre` (`id`, `AdresseMail`, `commentaire`, `facture`, `Ninea`, `numero`, `Raisonsociale`, `Appelsoffres_ID`, `Autorite_ID`, `pays`, `Fournisseur_ID`) VALUES
(2, 'ssi@ssi.sn', '', '2308104052013_Pj_adresse.rtf', NULL, NULL, '2SI(solutions et strat�gies informatique)', 2, 6, 170, 1);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_prix`
--

CREATE TABLE IF NOT EXISTS `pmb_prix` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `annee` int(11) DEFAULT NULL,
  `observation` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `prix` decimal(19,2) DEFAULT NULL,
  `prixMax` decimal(19,2) DEFAULT NULL,
  `prixMin` decimal(19,2) DEFAULT NULL,
  `variationVA` bigint(20) DEFAULT NULL,
  `variationVR` bigint(20) DEFAULT NULL,
  `produitID` bigint(20) DEFAULT NULL,
  `commentairePublication` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `datePublication` date DEFAULT NULL,
  `dateValidation` date DEFAULT NULL,
  `statut` varchar(8) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `pmb_prix`
--

INSERT INTO `pmb_prix` (`id`, `annee`, `observation`, `prix`, `prixMax`, `prixMin`, `variationVA`, `variationVR`, `produitID`, `commentairePublication`, `datePublication`, `dateValidation`, `statut`) VALUES
(1, 2013, '', '500.00', '400.00', '300.00', 500, 100, 1, NULL, NULL, '2013-05-08', 'Valid�'),
(2, 2013, '', '100.00', '0.00', '0.00', 0, 0, 2, NULL, NULL, '2013-05-08', 'Valid�'),
(3, 2013, '', '50000000.00', '55000000.00', '40000000.00', 50000000, 100, 3, NULL, NULL, NULL, 'Saisie');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_produits`
--

CREATE TABLE IF NOT EXISTS `pmb_produits` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `familles` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `pmb_produits`
--

INSERT INTO `pmb_produits` (`id`, `code`, `commentaire`, `designation`, `familles`) VALUES
(1, 'ss', 'ss', 'ss', 2),
(2, 'vvv', 'vvv', 'vv', 2),
(3, 'V44', 'V�hicule 4x4 station wagon (puissance 8 cv, diesel 2000 cc ,\n5 palces)', 'V�hicule 4x4 station wagon (puissance 8 cv, diesel 2000 cc ,\n5 palces)', 7);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_proformation`
--

CREATE TABLE IF NOT EXISTS `pmb_proformation` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `effectif` varchar(15) DEFAULT NULL,
  `forCible` varchar(255) DEFAULT NULL,
  `forCommentaire` varchar(255) DEFAULT NULL,
  `forDateDebut` date DEFAULT NULL,
  `forDateFin` date DEFAULT NULL,
  `forLibelle` varchar(255) DEFAULT NULL,
  `forLieu` varchar(255) DEFAULT NULL,
  `forRef` varchar(255) DEFAULT NULL,
  `formateur` varchar(255) DEFAULT NULL,
  `module` varchar(255) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_proformation`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_projetfinancer`
--

CREATE TABLE IF NOT EXISTS `pmb_projetfinancer` (
  `IDPROJETFINANCER` bigint(20) NOT NULL AUTO_INCREMENT,
  `budgetanomalie` varchar(255) DEFAULT NULL,
  `chapitre` varchar(255) DEFAULT NULL,
  `exercice` int(11) DEFAULT NULL,
  `montant` decimal(19,2) DEFAULT NULL,
  `Bailleurs_ID` bigint(20) DEFAULT NULL,
  `Planpassation_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IDPROJETFINANCER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_projetfinancer`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_rapportarmp`
--

CREATE TABLE IF NOT EXISTS `pmb_rapportarmp` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `rapLibelle` tinytext COLLATE latin1_general_ci NOT NULL,
  `rapDescription` text COLLATE latin1_general_ci NOT NULL,
  `rapNomfichier` tinytext COLLATE latin1_general_ci NOT NULL,
  `rapDateRapport` date NOT NULL DEFAULT '0000-00-00',
  `rapPublier` varchar(5) COLLATE latin1_general_ci NOT NULL DEFAULT 'no',
  `rapDatePublication` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `pmb_rapportarmp`
--

INSERT INTO `pmb_rapportarmp` (`ID`, `rapLibelle`, `rapDescription`, `rapNomfichier`, `rapDateRapport`, `rapPublier`, `rapDatePublication`) VALUES
(1, 'qqq', '', '1822424052013_rap_formcourrier.docx', '2013-05-24', 'yes', '2013-05-24');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_rapportaudit`
--

CREATE TABLE IF NOT EXISTS `pmb_rapportaudit` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `libelleRapportAudit` tinytext COLLATE latin1_general_ci NOT NULL,
  `descriptionRapportAudit` text COLLATE latin1_general_ci NOT NULL,
  `nomfichierRapportAudit` tinytext COLLATE latin1_general_ci NOT NULL,
  `dateRapportAudit` date NOT NULL DEFAULT '0000-00-00',
  `publier` varchar(5) COLLATE latin1_general_ci NOT NULL DEFAULT 'no',
  `datePublication` date DEFAULT NULL,
  `datedebut` date DEFAULT NULL,
  `datefin` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_rapportaudit`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_rapportaudit_ac`
--

CREATE TABLE IF NOT EXISTS `pmb_rapportaudit_ac` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `idaudit` int(255) DEFAULT NULL,
  `idautorite` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_rapportaudit_ac`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_rapportdcmp`
--

CREATE TABLE IF NOT EXISTS `pmb_rapportdcmp` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `rapLibelle` tinytext COLLATE latin1_general_ci NOT NULL,
  `rapDescription` text COLLATE latin1_general_ci NOT NULL,
  `rapNomfichier` tinytext COLLATE latin1_general_ci NOT NULL,
  `rapDateRapport` date NOT NULL DEFAULT '0000-00-00',
  `rapPublier` varchar(5) COLLATE latin1_general_ci NOT NULL DEFAULT 'no',
  `rapDatePublication` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_rapportdcmp`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_realisationduplan`
--

CREATE TABLE IF NOT EXISTS `pmb_realisationduplan` (
  `ID_plan` bigint(20) NOT NULL AUTO_INCREMENT,
  `Dateachevement` date DEFAULT NULL,
  `Dateattribution` date DEFAULT NULL,
  `Datedemarrage` date DEFAULT NULL,
  `Datelancement` date DEFAULT NULL,
  `LIB_plan` varchar(255) DEFAULT NULL,
  `montantEstime` decimal(19,2) DEFAULT NULL,
  `natureModePassation` varchar(50) DEFAULT NULL,
  `Reference` varchar(50) DEFAULT NULL,
  `modepassation_ID` bigint(20) DEFAULT NULL,
  `InfoPlan_ID` bigint(20) DEFAULT NULL,
  `Servicemaitreoeuvre` bigint(20) DEFAULT NULL,
  `TypeMarche` bigint(20) DEFAULT NULL,
  `appel` int(11) DEFAULT NULL,
  `examendncmp` int(11) DEFAULT NULL,
  `datefinevaluation` date DEFAULT NULL,
  `dateinvitationsoumission` date DEFAULT NULL,
  `datenonobjectionptf` date DEFAULT NULL,
  `datenonobjectionptfappel` date DEFAULT NULL,
  `dateouvertureplis` date DEFAULT NULL,
  `datepreparationdaodcrbc` date DEFAULT NULL,
  `dateprevisionnellesignaturecontrat` date DEFAULT NULL,
  `datereceptionavisccmpdncmp` date DEFAULT NULL,
  `datereceptionavisccmpdncmpappel` date DEFAULT NULL,
  `delaiexecution` int(11) DEFAULT NULL,
  `examenccmp` int(11) DEFAULT NULL,
  `sourcefinancement` varchar(255) DEFAULT NULL,
  `dateavisccmpdncmp` date DEFAULT NULL,
  `dateavisccmpdncmpami` date DEFAULT NULL,
  `dateavisccmpdncmpdp` date DEFAULT NULL,
  `dateavisccmpdncmpdppt` date DEFAULT NULL,
  `dateavisptfservcontrole` date DEFAULT NULL,
  `datefinevaluationpf` date DEFAULT NULL,
  `datelancementmanifestation` date DEFAULT NULL,
  `datenegociation` date DEFAULT NULL,
  `datenonobjectionptfpt` date DEFAULT NULL,
  `dateouverture` date DEFAULT NULL,
  `dateouverturedp` date DEFAULT NULL,
  `dateouverturemanifestation` date DEFAULT NULL,
  `datepreparationdp` date DEFAULT NULL,
  `datepreparationtdrami` date DEFAULT NULL,
  `dateprevsigncontrat` date DEFAULT NULL,
  `etat` varchar(50) DEFAULT NULL,
  `maj_ID` int(11) DEFAULT NULL,
  `rea_reaID` bigint(20) DEFAULT NULL,
  `supprime` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_plan`),
  KEY `FKBB7B066D51CA5D02` (`InfoPlan_ID`),
  KEY `FKBB7B066D97DC1D4` (`modepassation_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=134 ;

--
-- Contenu de la table `pmb_realisationduplan`
--

INSERT INTO `pmb_realisationduplan` (`ID_plan`, `Dateachevement`, `Dateattribution`, `Datedemarrage`, `Datelancement`, `LIB_plan`, `montantEstime`, `natureModePassation`, `Reference`, `modepassation_ID`, `InfoPlan_ID`, `Servicemaitreoeuvre`, `TypeMarche`, `appel`, `examendncmp`, `datefinevaluation`, `dateinvitationsoumission`, `datenonobjectionptf`, `datenonobjectionptfappel`, `dateouvertureplis`, `datepreparationdaodcrbc`, `dateprevisionnellesignaturecontrat`, `datereceptionavisccmpdncmp`, `datereceptionavisccmpdncmpappel`, `delaiexecution`, `examenccmp`, `sourcefinancement`, `dateavisccmpdncmp`, `dateavisccmpdncmpami`, `dateavisccmpdncmpdp`, `dateavisccmpdncmpdppt`, `dateavisptfservcontrole`, `datefinevaluationpf`, `datelancementmanifestation`, `datenegociation`, `datenonobjectionptfpt`, `dateouverture`, `dateouverturedp`, `dateouverturemanifestation`, `datepreparationdp`, `datepreparationtdrami`, `dateprevsigncontrat`, `etat`, `maj_ID`, `rea_reaID`, `supprime`) VALUES
(1, NULL, NULL, NULL, NULL, 'Construction de route', '500000000.00', NULL, 'T_DAGE_0001', 1, 9, 18, 3, 1, 0, '2013-07-11', '2013-05-12', '2013-04-28', '2013-04-28', '2013-06-11', '2013-04-28', '2013-08-08', '2013-07-18', '2013-05-05', 24, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(2, NULL, NULL, NULL, NULL, 'Demande de Renseignement et de Prix', '500000000.00', NULL, 'T_DAGE_0002', 34, 9, 18, 3, 1, 0, '2013-05-15', '2013-05-01', '2013-04-28', '2013-04-28', '2013-05-08', '2013-04-28', '2013-05-22', '2013-04-28', '2013-04-28', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(3, NULL, NULL, NULL, NULL, 'Appel d''Offres Ouvert avec pr� qualification', '4000000000.00', NULL, 'T_DAGE_0003', 2, 9, 18, 3, 0, 0, '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(4, NULL, NULL, NULL, NULL, 'Appel d''Offres Ouvert en deux etapes', '500000000.00', NULL, 'T_DAGE_0004', 3, 9, 18, 3, 1, 0, '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(5, NULL, NULL, NULL, NULL, 'Appel d''Offres Ouvert avec pr� qualification', '4000000000.00', NULL, 'T_DAGE_0003', 2, 10, 18, 3, 0, 0, '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U2', NULL, 3, 1),
(6, NULL, NULL, NULL, NULL, 'Appel d''Offres Ouvert en deux etapes', '500000000.00', NULL, 'T_DAGE_0004', 3, 10, 18, 3, 0, 0, '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, 1),
(7, NULL, NULL, NULL, NULL, 'Construction de route', '500000000.00', NULL, 'T_DAGE_0001', 1, 10, 18, 3, 0, 0, '2013-07-11', '2013-05-12', '2013-04-28', '2013-04-28', '2013-06-11', '2013-04-28', '2013-08-08', '2013-07-18', '2013-05-05', 24, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U2', NULL, 1, 1),
(8, NULL, NULL, NULL, NULL, 'Demande de Renseignement et de Prix', '500000000.00', NULL, 'T_DAGE_0002', 34, 10, 18, 3, 0, 0, '2013-05-15', '2013-05-01', '2013-04-28', '2013-04-28', '2013-05-08', '2013-04-28', '2013-05-22', '2013-04-28', '2013-04-28', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1),
(9, NULL, NULL, NULL, NULL, 'DSP', '500000000.00', NULL, 'DSP_DAGE_0009', 1, 10, 18, 5, 0, 0, '2013-05-01', '2013-05-01', '2013-05-01', '2013-05-01', '2013-05-01', '2013-05-01', '2013-05-01', '2013-05-01', '2013-05-01', 24, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 0, 1),
(10, NULL, NULL, NULL, NULL, 'Appel d''Offres Ouvert avec pr� qualification', '4000000000.00', NULL, 'T_DAGE_0003', 2, 10, 18, 3, 0, 0, '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', '2013-04-28', 14, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'M2', NULL, 0, 1),
(11, NULL, NULL, NULL, NULL, 'Construction de route', '500000000.00', NULL, 'T_DAGE_0001', 1, 10, 18, 3, 0, 0, '2013-07-11', '2013-05-12', '2013-04-28', '2013-04-28', '2013-06-11', '2013-04-28', '2013-08-08', '2013-07-18', '2013-05-05', 24, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'S2', 10, 0, 0),
(12, NULL, NULL, NULL, NULL, 'Appel d''Offres Ouvert', '40000000000.00', NULL, 'T_DAGE_0012', 5, 11, 18, 3, 1, 0, '2013-07-16', '2013-05-17', '2013-05-18', '2013-05-10', '2013-06-16', '2013-05-03', '2013-08-13', '2013-07-23', '2013-05-10', 24, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(13, NULL, NULL, NULL, NULL, 'Manifestation d''interet', '6000000000.00', NULL, 'PI_DAGE_0013', 42, 11, 18, 4, 1, 1, '2013-10-06', '2013-08-16', '2013-05-04', NULL, NULL, NULL, '2013-12-19', NULL, NULL, 24, 0, NULL, '2013-11-21', '2013-05-19', '2013-08-09', '2013-10-21', '2013-05-04', '2013-11-06', '2013-05-26', '2013-11-28', '2013-05-04', '2013-10-28', '2013-09-15', '2013-06-25', '2013-07-25', '2013-05-04', NULL, NULL, NULL, 0, 1),
(14, NULL, NULL, NULL, NULL, 'Demande renseignement prix', '40000000.00', NULL, 'PI_DAGE_0014', 34, 11, 18, 4, 1, 0, '2013-05-18', '2013-05-07', '2013-05-04', NULL, NULL, NULL, '2013-06-09', NULL, NULL, 12, 0, NULL, '2013-05-04', '2013-05-04', '2013-05-04', '2013-05-04', '2013-05-04', '2013-05-26', '2013-05-04', '2013-06-02', '2013-05-04', '2013-05-23', '2013-05-14', '2013-05-04', '2013-05-04', '2013-05-04', NULL, NULL, NULL, 0, 1),
(15, NULL, NULL, NULL, NULL, 'Appel d''offre ouvert', '5000000000.00', NULL, 'F_DAGE_0015', 1, 9, 18, 1, 1, 0, '2013-07-18', '2013-05-19', '2013-05-05', '2013-05-05', '2013-06-18', '2013-05-05', '2013-08-15', '2013-07-25', '2013-05-12', 12, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(16, NULL, NULL, NULL, NULL, 'DRP', '500000000.00', NULL, 'F_DAGE_0016', 34, 9, 18, 1, 1, 0, '2013-05-22', '2013-05-08', '2013-05-05', '2013-05-05', '2013-05-15', '2013-05-05', '2013-05-29', '2013-05-05', '2013-05-05', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(17, NULL, NULL, NULL, NULL, 'Appel d''offre ouvert', '500000000.00', NULL, 'DSP_DAGE_0017', 1, 9, 18, 5, 0, 1, '2013-07-26', '2013-05-27', '2013-05-05', '2013-05-05', '2013-06-26', '2013-05-05', '2013-08-31', '2013-08-10', '2013-05-20', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(18, NULL, NULL, NULL, NULL, 'DRP', '500000000.00', NULL, 'DSP_DAGE_0018', 34, 9, 18, 5, 0, 0, '2013-05-22', '2013-05-08', '2013-05-05', '2013-05-05', '2013-05-15', '2013-05-05', '2013-05-29', '2013-05-05', '2013-05-05', 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(19, NULL, NULL, NULL, NULL, 'Appel d''offre ouvert', '400000000.00', NULL, 'S_DAGE_0019', 1, 9, 18, 2, 1, 1, '2013-07-26', '2013-05-27', '2013-05-05', '2013-05-05', '2013-06-26', '2013-05-05', '2013-08-31', '2013-08-10', '2013-05-20', 6, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(20, NULL, NULL, NULL, NULL, 'DRP', '50000000.00', NULL, 'S_DAGE_0020', 34, 9, 18, 2, 1, 0, '2013-05-22', '2013-05-08', '2013-05-05', '2013-05-05', '2013-05-15', '2013-05-05', '2013-05-29', '2013-05-05', '2013-05-05', 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(21, NULL, NULL, NULL, NULL, 'Travaux', '500000000.00', NULL, 'T_DAGE_0021', 1, 10, 18, 3, 0, 1, '2013-07-26', '2013-05-27', '2013-05-05', '2013-05-05', '2013-06-26', '2013-05-05', '2013-08-31', '2013-08-10', '2013-05-20', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 0, 1),
(22, NULL, NULL, NULL, NULL, 'Fournitures', '500000000.00', NULL, 'F_DAGE_0022', 1, 10, 18, 1, 0, 1, '2013-07-26', '2013-05-27', '2013-05-05', '2013-05-05', '2013-06-26', '2013-05-05', '2013-08-31', '2013-08-10', '2013-05-20', 4, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 0, 1),
(23, NULL, NULL, NULL, NULL, 'Service', '50000000.00', NULL, 'S_DAGE_0023', 34, 10, 18, 2, 0, 0, '2013-05-22', '2013-05-08', '2013-05-05', '2013-05-05', '2013-05-15', '2013-05-05', '2013-05-29', '2013-05-05', '2013-05-05', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 0, 1),
(24, NULL, NULL, NULL, NULL, 'PI', '70000000.00', NULL, 'PI_DAGE_0024', 42, 10, 18, 4, 0, 1, '2013-10-07', '2013-08-17', '2013-05-05', NULL, NULL, NULL, '2013-12-20', NULL, NULL, 2, 0, NULL, '2013-11-22', '2013-05-20', '2013-08-10', '2013-10-22', '2013-05-05', '2013-11-07', '2013-05-27', '2013-11-29', '2013-05-05', '2013-10-29', '2013-09-16', '2013-06-26', '2013-07-26', '2013-05-05', NULL, 'A2', NULL, 0, 1),
(25, NULL, NULL, NULL, NULL, 'DSP', '80000000.00', NULL, 'DSP_DAGE_0025', 1, 10, 18, 5, 0, 0, '2013-07-18', '2013-05-19', '2013-05-05', '2013-05-05', '2013-06-18', '2013-05-05', '2013-08-15', '2013-07-25', '2013-05-12', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 0, 1),
(26, NULL, NULL, NULL, NULL, 'Travaux', '10000000.00', NULL, 'T_DAGE_0026', 1, 12, 19, 3, 1, 1, '2013-07-27', '2013-05-28', '2013-05-06', '2013-05-06', '2013-06-27', '2013-05-06', '2013-09-01', '2013-08-11', '2013-05-21', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(27, NULL, NULL, NULL, NULL, 'Travaux', '10000000.00', NULL, 'T_DAGE_0026', 1, 13, 19, 3, 0, 1, '2013-07-27', '2013-05-28', '2013-05-06', '2013-05-06', '2013-06-27', '2013-05-06', '2013-09-01', '2013-08-11', '2013-05-21', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U2', 1, 26, 0),
(29, NULL, NULL, NULL, NULL, 'DSP', '12000000.00', NULL, 'T_DAGE_0029', 34, 13, 19, 3, 1, 0, '2013-05-23', '2013-05-09', '2013-05-06', '2013-05-06', '2013-05-16', '2013-05-06', '2013-05-30', '2013-05-06', '2013-05-06', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 0, 1),
(30, NULL, NULL, NULL, NULL, 'PI', '34000000.00', NULL, 'F_DAGE_0030', 34, 13, 19, 1, 0, 0, '2013-05-21', '2013-05-07', '2013-05-06', '2013-05-06', '2013-05-14', '2013-05-04', '2013-05-28', '2013-05-06', '2013-05-06', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 0, 1),
(31, NULL, NULL, NULL, NULL, 'AOO', '200000000.00', NULL, 'F_DAGE_0031', 5, 13, 19, 1, 1, 0, '2013-07-19', '2013-05-20', '2013-05-06', '2013-05-06', '2013-06-19', '2013-05-06', '2013-08-16', '2013-07-26', '2013-05-13', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 0, 1),
(32, NULL, NULL, NULL, NULL, 'AOO', '10000000.00', NULL, 'S_DAGE_0032', 34, 13, 19, 2, 0, 0, '2013-05-23', '2013-05-09', '2013-05-06', '2013-05-06', '2013-05-16', '2013-05-06', '2013-05-30', '2013-05-06', '2013-05-06', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 0, 1),
(33, NULL, NULL, NULL, NULL, 'AOO', '10000000.00', NULL, 'S_DAGE_0033', 1, 13, 19, 2, 0, 1, '2013-07-27', '2013-05-28', '2013-05-06', '2013-05-06', '2013-06-27', '2013-05-06', '2013-09-01', '2013-08-11', '2013-05-21', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 0, 1),
(34, NULL, NULL, NULL, NULL, 'PI', '10000000.00', NULL, 'PI_DAGE_0034', 42, 13, 19, 4, 1, 0, '2013-09-22', '2013-08-02', '2013-05-06', NULL, NULL, NULL, '2013-11-19', NULL, NULL, 2, 1, NULL, '2013-10-22', '2013-05-13', '2013-07-26', '2013-09-29', '2013-05-06', '2013-10-15', '2013-05-20', '2013-10-29', '2013-05-06', '2013-10-06', '2013-09-01', '2013-06-19', '2013-07-19', '2013-05-06', NULL, 'A2', NULL, 0, 1),
(35, NULL, NULL, NULL, NULL, 'DRP', '12000000.00', NULL, 'PI_DAGE_0035', 34, 13, 19, 4, 0, 0, '2013-05-20', '2013-05-09', '2013-05-06', NULL, NULL, NULL, '2013-06-11', NULL, NULL, 4, 0, NULL, '2013-05-06', '2013-05-06', '2013-05-06', '2013-05-06', '2013-05-06', '2013-05-28', '2013-05-06', '2013-06-04', '2013-05-06', '2013-05-25', '2013-05-16', '2013-05-06', '2013-05-06', '2013-05-06', NULL, 'A2', NULL, 0, 1),
(36, NULL, NULL, NULL, NULL, 'DSP', '5000000.00', NULL, 'DSP_DAGE_0036', 34, 13, 19, 5, 0, 0, '2013-05-23', '2013-05-09', '2013-05-06', '2013-05-06', '2013-05-16', '2013-05-06', '2013-05-30', '2013-05-06', '2013-05-06', 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 0, 1),
(37, NULL, NULL, NULL, NULL, 'AOO', '45000000.00', NULL, 'DSP_DAGE_0037', 1, 13, 19, 5, 0, 1, '2013-07-27', '2013-05-28', '2013-05-06', '2013-05-06', '2013-06-27', '2013-05-06', '2013-09-01', '2013-08-11', '2013-05-21', 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 0, 1),
(38, NULL, NULL, NULL, NULL, 'Construction du si�ge de la DNCMP', '500000000.00', NULL, 'T_DNCMP_0038', 1, 14, 20, 3, 1, 1, '2013-07-28', '2013-05-29', '2013-08-12', '2013-05-22', '2013-06-28', '2013-05-07', '2013-09-02', '2013-08-12', '2013-05-22', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(39, NULL, NULL, NULL, NULL, 'Contr�le ', '700000000.00', NULL, 'PI_DNCMP_0039', 42, 14, 20, 4, 0, 1, '2013-10-09', '2013-08-19', '2013-10-13', NULL, NULL, NULL, '2013-12-22', NULL, NULL, 24, 0, NULL, '2013-11-24', '2013-05-22', '2013-08-12', '2013-10-24', '2013-12-03', '2013-11-09', '2013-05-29', '2013-12-01', '2013-10-24', '2013-10-31', '2013-09-18', '2013-06-28', '2013-07-28', '2013-05-07', NULL, NULL, NULL, 0, 1),
(40, NULL, NULL, NULL, NULL, 'Achat de v�hicule', '300000000.00', NULL, 'F_DNCMP_0040', 1, 14, 20, 1, 0, 1, '2013-07-28', '2013-05-29', '2013-10-12', '2013-05-23', '2013-06-28', '2013-05-07', '2013-09-02', '2013-08-12', '2013-05-22', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(41, NULL, NULL, NULL, NULL, 'Achat de v�hicule', '300000000.00', NULL, 'F_DNCMP_0040', 1, 15, 20, 1, 0, 1, '2013-07-28', '2013-05-29', '2013-10-12', '2013-05-23', '2013-06-28', '2013-05-07', '2013-09-02', '2013-08-12', '2013-05-22', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U2', NULL, 40, 1),
(42, NULL, NULL, NULL, NULL, 'Construction du si�ge de la DNCMP', '500000000.00', NULL, 'T_DNCMP_0038', 1, 15, 20, 3, 0, 1, '2013-07-28', '2013-05-29', '2013-08-12', '2013-05-22', '2013-06-28', '2013-05-07', '2013-09-02', '2013-08-12', '2013-05-22', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'S2', NULL, 38, 0),
(43, NULL, NULL, NULL, NULL, 'Contr�le ', '700000000.00', NULL, 'PI_DNCMP_0039', 42, 15, 20, 4, 0, 1, '2013-10-09', '2013-08-19', '2013-10-13', NULL, NULL, NULL, '2013-12-22', NULL, NULL, 24, 0, NULL, '2013-11-24', '2013-05-22', '2013-08-12', '2013-10-24', '2013-12-03', '2013-11-09', '2013-05-29', '2013-12-01', '2013-10-24', '2013-10-31', '2013-09-18', '2013-06-28', '2013-07-28', '2013-05-07', NULL, 'S2', 15, 39, 0),
(45, NULL, NULL, NULL, NULL, 'Achat de v�hicule', '400000000.00', NULL, 'F_DNCMP_0040', 1, 15, 20, 1, 0, 1, '2013-07-28', '2013-05-29', '2013-10-12', '2013-05-23', '2013-06-28', '2013-05-07', '2013-09-02', '2013-08-12', '2013-05-22', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'M2', NULL, 0, 1),
(46, NULL, NULL, NULL, NULL, 'Gardiennage', '300000000.00', NULL, 'S_DNCMP_0046', 1, 15, 20, 2, 0, 1, '2013-07-28', '2013-05-29', '2013-08-07', '2013-06-11', '2013-06-28', '2013-05-07', '2013-09-02', '2013-08-12', '2013-05-22', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 0, 1),
(47, NULL, NULL, NULL, NULL, 'ddd', '600000000.00', NULL, 'T_DAGE_0047', 1, 12, 19, 3, 1, 1, '2013-07-29', '2013-05-30', '2013-05-09', '2013-05-10', '2013-06-29', '2013-05-08', '2013-09-03', '2013-08-13', '2013-05-23', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(48, NULL, NULL, NULL, NULL, 'PI', '700000000.00', NULL, 'PI_DAGE_0048', 42, 12, 19, 4, 1, 1, '2013-10-11', '2013-08-21', '2013-05-09', NULL, NULL, NULL, '2013-12-24', NULL, NULL, 12, 0, NULL, '2013-11-26', '2013-05-24', '2013-08-14', '2013-10-26', '2013-05-09', '2013-11-11', '2013-05-31', '2013-12-03', '2013-05-09', '2013-11-02', '2013-09-20', '2013-06-30', '2013-07-30', '2013-05-09', NULL, NULL, NULL, 0, 1),
(49, NULL, NULL, NULL, NULL, 'Travaux d''entretien de la route', '500000000.00', NULL, 'T_DER_0049', 5, 16, 22, 3, 1, 1, '2013-07-31', '2013-06-01', '2013-05-10', '2013-05-17', '2013-07-01', '2013-05-10', '2013-09-05', '2013-08-15', '2013-05-25', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(50, NULL, NULL, NULL, NULL, 'Fourniture de bureaux', '50000000.00', NULL, 'F_DAF_0050', 1, 16, 21, 1, 0, 1, '2013-07-31', '2013-06-01', '2013-05-10', '2013-05-10', '2013-07-01', '2013-05-10', '2013-09-05', '2013-08-15', '2013-05-25', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(51, NULL, NULL, NULL, NULL, 'Etude d''impact environnemental', '800000000.00', NULL, 'PI_DER_0051', 42, 16, 22, 4, 0, 1, '2013-10-12', '2013-08-22', '2013-05-10', NULL, NULL, NULL, '2013-12-25', NULL, NULL, 12, 0, NULL, '2013-11-27', '2013-05-25', '2013-08-15', '2013-10-27', '2013-05-10', '2013-11-12', '2013-06-01', '2013-12-04', '2013-05-03', '2013-11-03', '2013-09-21', '2013-07-01', '2013-07-31', '2013-05-10', NULL, NULL, NULL, 0, 1),
(52, NULL, NULL, NULL, NULL, 'Etude d''impact environnemental', '800000000.00', NULL, 'PI_DER_0051', 42, 17, 22, 4, 1, 1, '2013-10-12', '2013-08-22', '2013-05-10', NULL, NULL, NULL, '2013-12-25', NULL, NULL, 12, 0, NULL, '2013-11-27', '2013-05-25', '2013-08-15', '2013-10-27', '2013-05-10', '2013-11-12', '2013-06-01', '2013-12-04', '2013-05-03', '2013-11-03', '2013-09-21', '2013-07-01', '2013-07-31', '2013-05-10', NULL, 'U2', NULL, 51, 1),
(53, NULL, NULL, NULL, NULL, 'Fourniture de bureaux', '50000000.00', NULL, 'F_DAF_0050', 1, 17, 21, 1, 0, 1, '2013-07-31', '2013-06-01', '2013-05-10', '2013-05-10', '2013-07-01', '2013-05-10', '2013-09-05', '2013-08-15', '2013-05-25', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'S2', 17, 50, 0),
(54, NULL, NULL, NULL, NULL, 'Travaux d''entretien de la route', '500000000.00', NULL, 'T_DER_0049', 1, 17, 22, 3, 0, 1, '2013-07-31', '2013-06-01', '2013-05-10', '2013-05-17', '2013-07-01', '2013-05-10', '2013-09-05', '2013-08-15', '2013-05-25', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U2', NULL, 49, 1),
(55, NULL, NULL, NULL, NULL, 'Etude d''impact environnemental', '800000000.00', NULL, 'PI_DER_0051', 42, 17, 22, 4, 0, 1, '2013-10-12', '2013-08-22', '2013-05-10', NULL, NULL, NULL, '2013-12-25', NULL, NULL, 12, 0, NULL, '2013-11-27', '2013-05-25', '2013-08-15', '2013-10-27', '2013-05-10', '2013-11-12', '2013-06-01', '2013-12-04', '2013-05-03', '2013-11-03', '2013-09-21', '2013-07-01', '2013-07-31', '2013-05-10', NULL, NULL, NULL, 51, 1),
(56, NULL, NULL, NULL, NULL, 'Gardiennage', '50000000.00', NULL, 'S_DAF_0056', 1, 17, 21, 2, 0, 1, '2013-07-31', '2013-06-01', '2013-05-10', '2013-05-10', '2013-07-01', '2013-05-10', '2013-09-05', '2013-08-15', '2013-05-25', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 0, 1),
(57, NULL, NULL, NULL, NULL, 'Travaux d''entretien de la route', '400000000.00', NULL, 'T_DER_0049', 1, 17, 22, 3, 0, 1, '2013-07-31', '2013-06-01', '2013-05-10', '2013-05-17', '2013-07-01', '2013-05-10', '2013-09-05', '2013-08-15', '2013-05-25', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'M2', NULL, 0, 1),
(58, NULL, NULL, NULL, NULL, 'Travaux', '10000000.00', NULL, 'T_DAGE_0026', 1, 13, 19, 3, 0, 1, '2013-07-27', '2013-05-28', '2013-05-06', '2013-05-06', '2013-06-27', '2013-05-06', '2013-09-01', '2013-08-11', '2013-05-21', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'M2', NULL, 0, 1),
(59, NULL, NULL, NULL, NULL, 'AOO', '200000000.00', NULL, 'F_DAGE_0031', 5, 18, 19, 1, 0, 0, '2013-07-19', '2013-05-20', '2013-05-06', '2013-05-06', '2013-06-19', '2013-05-06', '2013-08-16', '2013-07-26', '2013-05-13', 2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 31, 1),
(60, NULL, NULL, NULL, NULL, 'AOO', '10000000.00', NULL, 'S_DAGE_0032', 34, 18, 19, 2, 0, 0, '2013-05-23', '2013-05-09', '2013-05-06', '2013-05-06', '2013-05-16', '2013-05-06', '2013-05-30', '2013-05-06', '2013-05-06', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 32, 1),
(61, NULL, NULL, NULL, NULL, 'AOO', '10000000.00', NULL, 'S_DAGE_0033', 1, 18, 19, 2, 0, 1, '2013-07-27', '2013-05-28', '2013-05-06', '2013-05-06', '2013-06-27', '2013-05-06', '2013-09-01', '2013-08-11', '2013-05-21', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 33, 1),
(62, NULL, NULL, NULL, NULL, 'AOO', '45000000.00', NULL, 'DSP_DAGE_0037', 1, 18, 19, 5, 0, 1, '2013-07-27', '2013-05-28', '2013-05-06', '2013-05-06', '2013-06-27', '2013-05-06', '2013-09-01', '2013-08-11', '2013-05-21', 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 37, 1),
(63, NULL, NULL, NULL, NULL, 'DRP', '12000000.00', NULL, 'PI_DAGE_0035', 34, 18, 19, 4, 0, 0, '2013-05-20', '2013-05-09', '2013-05-06', NULL, NULL, NULL, '2013-06-11', NULL, NULL, 4, 0, NULL, '2013-05-06', '2013-05-06', '2013-05-06', '2013-05-06', '2013-05-06', '2013-05-28', '2013-05-06', '2013-06-04', '2013-05-06', '2013-05-25', '2013-05-16', '2013-05-06', '2013-05-06', '2013-05-06', NULL, 'A2', NULL, 35, 1),
(64, NULL, NULL, NULL, NULL, 'DSP', '12000000.00', NULL, 'T_DAGE_0029', 34, 18, 19, 3, 0, 0, '2013-05-23', '2013-05-09', '2013-05-06', '2013-05-06', '2013-05-16', '2013-05-06', '2013-05-30', '2013-05-06', '2013-05-06', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 29, 1),
(65, NULL, NULL, NULL, NULL, 'DSP', '5000000.00', NULL, 'DSP_DAGE_0036', 34, 18, 19, 5, 0, 0, '2013-05-23', '2013-05-09', '2013-05-06', '2013-05-06', '2013-05-16', '2013-05-06', '2013-05-30', '2013-05-06', '2013-05-06', 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 36, 1),
(66, NULL, NULL, NULL, NULL, 'PI', '34000000.00', NULL, 'F_DAGE_0030', 34, 18, 19, 1, 0, 0, '2013-05-21', '2013-05-07', '2013-05-06', '2013-05-06', '2013-05-14', '2013-05-04', '2013-05-28', '2013-05-06', '2013-05-06', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A2', NULL, 30, 1),
(67, NULL, NULL, NULL, NULL, 'PI', '10000000.00', NULL, 'PI_DAGE_0034', 42, 18, 19, 4, 0, 0, '2013-09-22', '2013-08-02', '2013-05-06', NULL, NULL, NULL, '2013-11-19', NULL, NULL, 2, 1, NULL, '2013-10-22', '2013-05-13', '2013-07-26', '2013-09-29', '2013-05-06', '2013-10-15', '2013-05-20', '2013-10-29', '2013-05-06', '2013-10-06', '2013-09-01', '2013-06-19', '2013-07-19', '2013-05-06', NULL, 'A2', NULL, 34, 1),
(68, NULL, NULL, NULL, NULL, 'Travaux', '10000000.00', NULL, 'T_DAGE_0026', 1, 18, 19, 3, 0, 1, '2013-07-27', '2013-05-28', '2013-05-06', '2013-05-06', '2013-06-27', '2013-05-06', '2013-09-01', '2013-08-11', '2013-05-21', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U2', NULL, 27, 0),
(69, NULL, NULL, NULL, NULL, 'Travaux', '10000000.00', NULL, 'T_DAGE_0026', 1, 18, 19, 3, 0, 1, '2013-07-27', '2013-05-28', '2013-05-06', '2013-05-06', '2013-06-27', '2013-05-06', '2013-09-01', '2013-08-11', '2013-05-21', 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'M2', NULL, 58, 1),
(80, NULL, NULL, NULL, NULL, 'Achat de v�hicule', '400000000.00', NULL, 'F_DNCMP_0040', 1, 22, 20, 1, 0, 1, '2013-07-28', '2013-05-29', '2013-10-12', '2013-05-23', '2013-06-28', '2013-05-07', '2013-09-02', '2013-08-12', '2013-05-22', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'U3', NULL, 45, 1),
(81, NULL, NULL, NULL, NULL, 'Gardiennage', '300000000.00', NULL, 'S_DNCMP_0046', 1, 22, 20, 2, 0, 1, '2013-07-28', '2013-05-29', '2013-08-07', '2013-06-11', '2013-06-28', '2013-05-07', '2013-09-02', '2013-08-12', '2013-05-22', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 46, 1),
(82, NULL, NULL, NULL, NULL, 'Achat de v�hicule', '400000000.00', NULL, 'F_DNCMP_0040', 1, 22, 20, 1, 0, 1, '2013-07-28', '2013-05-29', '2013-10-12', '2013-05-23', '2013-06-28', '2013-05-07', '2013-09-02', '2013-08-12', '2013-05-22', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'M3', NULL, 0, 1),
(83, NULL, NULL, NULL, NULL, 'PI', '4000000000.00', NULL, 'PI_DNCMP_0083', 42, 22, 20, 4, 0, 0, '2013-10-12', '2013-08-22', '2013-05-26', NULL, NULL, NULL, '2013-12-09', NULL, NULL, 5, 1, NULL, '2013-11-11', '2013-06-02', '2013-08-15', '2013-10-19', '2013-05-26', '2013-11-04', '2013-06-09', '2013-11-18', '2013-05-26', '2013-10-26', '2013-09-21', '2013-07-09', '2013-08-08', '2013-05-26', NULL, 'A3', NULL, 0, 1),
(105, NULL, NULL, NULL, NULL, 'Achat de v�hicule', '400000000.00', NULL, 'F_DNCMP_0040', 1, 26, 20, 1, 0, 1, '2013-07-28', '2013-05-29', '2013-10-12', '2013-05-23', '2013-06-28', '2013-05-07', '2013-09-02', '2013-08-12', '2013-05-22', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82, 1),
(106, NULL, NULL, NULL, NULL, 'Gardiennage', '300000000.00', NULL, 'S_DNCMP_0046', 1, 26, 20, 2, 0, 1, '2013-07-28', '2013-05-29', '2013-08-07', '2013-06-11', '2013-06-28', '2013-05-07', '2013-09-02', '2013-08-12', '2013-05-22', 12, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 81, 1),
(107, NULL, NULL, NULL, NULL, 'PI', '4000000000.00', NULL, 'PI_DNCMP_0083', 42, 26, 20, 4, 0, 0, '2013-10-12', '2013-08-22', '2013-05-26', NULL, NULL, NULL, '2013-12-09', NULL, NULL, 5, 1, NULL, '2013-11-11', '2013-06-02', '2013-08-15', '2013-10-19', '2013-05-26', '2013-11-04', '2013-06-09', '2013-11-18', '2013-05-26', '2013-10-26', '2013-09-21', '2013-07-09', '2013-08-08', '2013-05-26', NULL, 'M4', 0, 83, 1),
(128, '2013-06-19', '2013-06-13', '2013-06-15', '2013-06-11', 'Travaux', '400000000.00', 'Internationale', 'T_DNCMP_0130', 1, 30, 20, 3, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(129, '2013-06-20', '2013-06-12', '2013-06-14', '2013-06-11', 'Fournituree', '400000000.00', 'Communautaire', 'F_DNCMP_0131', 1, 30, 20, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(130, '2013-06-20', '2013-06-12', '2013-06-13', '2013-06-11', 'Services', '40000000.00', 'Communautaire', 'S_DNCMP_0132', 1, 30, 20, 2, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(132, '2013-06-19', '2013-06-13', '2013-06-13', '2013-06-11', 'DS', '400000000.00', 'Communautaire', 'DSP_DNCMP_0134', 1, 30, 20, 5, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(133, '2013-06-19', '2013-06-12', '2013-06-14', '2013-06-11', 'PI', '500000000.00', 'Communautaire', 'PI_DNCMP_0135', 42, 30, 20, 4, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_realisation_bailleurs`
--

CREATE TABLE IF NOT EXISTS `pmb_realisation_bailleurs` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `BAILLEURS` bigint(20) DEFAULT NULL,
  `REALISATION` bigint(20) DEFAULT NULL,
  `chapitre` varchar(255) DEFAULT NULL,
  `montant` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK77F9112F9A3D9319` (`REALISATION`),
  KEY `FK77F9112F8A9EA2B0` (`BAILLEURS`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `pmb_realisation_bailleurs`
--

INSERT INTO `pmb_realisation_bailleurs` (`ID`, `BAILLEURS`, `REALISATION`, `chapitre`, `montant`) VALUES
(1, 2854, 1, 'chapitre 1', '500000000.00'),
(2, 2854, 20, 'dddd', '50000000.00'),
(3, 2854, 15, 'sss', '5000000000.00'),
(4, 2854, 19, 'dd', '400000000.00'),
(5, 2854, 21, 'ss', '500000000.00'),
(6, 2854, 22, 'ssxxxx', '500000000.00'),
(7, 2854, 23, 'vvv', '50000000.00'),
(8, 2854, 24, 'sss', '70000000.00'),
(9, 2854, 25, 'sss', '80000000.00'),
(10, 2854, 26, 'Chapitre 1', '10000000.00'),
(12, 2854, 47, 'sss', '600000000.00'),
(13, 2854, 38, 'test', '500000000.00'),
(14, 2854, 49, 'Chapitre 1', '500000000.00');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_realisation_plans`
--

CREATE TABLE IF NOT EXISTS `pmb_realisation_plans` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `chapitre` varchar(255) DEFAULT NULL,
  `PLAN` bigint(20) DEFAULT NULL,
  `REALISATION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKB2AA5C9E482CA2A8` (`PLAN`),
  KEY `FKB2AA5C9E9A3D9319` (`REALISATION`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_realisation_plans`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_recours`
--

CREATE TABLE IF NOT EXISTS `pmb_recours` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DateBlockdos` date DEFAULT NULL,
  `DateBlockdosopen` date DEFAULT NULL,
  `dateCourrier` date DEFAULT NULL,
  `DateRecous` date DEFAULT NULL,
  `nomSoumissionnaire` varchar(255) DEFAULT NULL,
  `ObjetRecours` varchar(255) DEFAULT NULL,
  `Refrecours` varchar(50) DEFAULT NULL,
  `Appelsoffres_ID` bigint(20) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `CommentBlock` varchar(255) DEFAULT NULL,
  `DateBlock` date DEFAULT NULL,
  `Dateexpiration` date DEFAULT NULL,
  `Datereception` date DEFAULT NULL,
  `refAoffre` varchar(50) DEFAULT NULL,
  `Valider` int(11) DEFAULT NULL,
  `Commentaire` varchar(50) DEFAULT NULL,
  `datecloture` date DEFAULT NULL,
  `fichier` varchar(50) DEFAULT NULL,
  `Issuerecoursgracieux` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKB69CAEFD21E224C0` (`Dossiers_ID`),
  KEY `FKB69CAEFD25D5960` (`Appelsoffres_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_recours`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_recouvrement`
--

CREATE TABLE IF NOT EXISTS `pmb_recouvrement` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `banque` varchar(100) DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  `montantVerse` decimal(19,2) DEFAULT NULL,
  `NatureVersement` varchar(10) DEFAULT NULL,
  `numCheque` varchar(100) DEFAULT NULL,
  `operationID` varchar(50) DEFAULT NULL,
  `appeloffreId` bigint(20) DEFAULT NULL,
  `autoriteId` bigint(20) DEFAULT NULL,
  `banqueID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7D8096CBEF4092C5` (`banqueID`),
  KEY `FK7D8096CB90154997` (`appeloffreId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Contenu de la table `pmb_recouvrement`
--

INSERT INTO `pmb_recouvrement` (`id`, `banque`, `commentaire`, `montantVerse`, `NatureVersement`, `numCheque`, `operationID`, `appeloffreId`, `autoriteId`, `banqueID`) VALUES
(1, NULL, '', NULL, 'Ch�que', 'dd', 'R902201305001', 11, 902, 1),
(2, NULL, '', NULL, 'Ch�que', 'dd', 'R6201305001', 1, 6, 1),
(3, NULL, '', NULL, 'Ch�que', 'dd', 'R6201305001', 5, 6, 1),
(4, NULL, '', NULL, 'Ch�que', 'dd', 'R6201305001', 6, 6, 1),
(5, NULL, '', NULL, 'Ch�que', 'dd', 'R6201305001', 2, 6, 1),
(6, NULL, '', NULL, 'Ch�que', 'dd', 'R6201305001', 3, 6, 1),
(7, NULL, '', NULL, 'Ch�que', 'dd', 'R6201305001', 4, 6, 1),
(8, NULL, '', NULL, 'Ch�que', 'dd', 'R6201305001', 7, 6, 1),
(9, NULL, '', NULL, 'Ch�que', 'dd', 'R6201305001', 8, 6, 1),
(10, NULL, '', NULL, 'Ch�que', 'dd', 'R6201305001', 9, 6, 1),
(11, NULL, '', NULL, 'Ch�que', 'dd', 'R6201305001', 10, 6, 1),
(12, NULL, '', NULL, 'Ch�que', 'fgt', 'R9201305001', 15, 9, 1),
(13, NULL, '', NULL, 'Ch�que', 'ss', 'R905201305001', 17, 905, 1),
(14, NULL, '', NULL, 'Ch�que', 'ss', 'R905201305001', 19, 905, 1),
(15, NULL, '', NULL, 'Ch�que', 'ss', 'R6201305001', 1, 6, 1),
(16, NULL, '', NULL, 'Ch�que', 'ss', 'R6201305001', 5, 6, 1),
(17, NULL, '', NULL, 'Ch�que', 'ss', 'R6201305001', 6, 6, 1),
(18, NULL, '', NULL, 'Ch�que', 'ss', 'R6201305001', 2, 6, 1),
(19, NULL, '', NULL, 'Ch�que', 'ss', 'R6201305001', 3, 6, 1),
(20, NULL, '', NULL, 'Ch�que', 'ss', 'R6201305001', 4, 6, 1),
(21, NULL, '', NULL, 'Ch�que', 'ss', 'R6201305001', 7, 6, 1),
(22, NULL, '', NULL, 'Ch�que', 'ss', 'R6201305001', 8, 6, 1),
(23, NULL, '', NULL, 'Ch�que', 'ss', 'R6201305001', 9, 6, 1),
(24, NULL, '', NULL, 'Ch�que', 'ss', 'R6201305001', 10, 6, 1),
(25, NULL, '', NULL, 'Ch�que', '56789H', 'R9201305001', 15, 9, 1);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_reglementation`
--

CREATE TABLE IF NOT EXISTS `pmb_reglementation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  `Nature` varchar(50) DEFAULT NULL,
  `Type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_reglementation`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_representantsservicestechniques`
--

CREATE TABLE IF NOT EXISTS `pmb_representantsservicestechniques` (
  `IDpresenceouverture` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateconvocation` date DEFAULT NULL,
  `presence` varchar(255) DEFAULT NULL,
  `qualite` varchar(255) DEFAULT NULL,
  `representant` varchar(255) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `etape` int(11) DEFAULT NULL,
  PRIMARY KEY (`IDpresenceouverture`),
  KEY `FK416DA65B21E224C0` (`Dossiers_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Contenu de la table `pmb_representantsservicestechniques`
--

INSERT INTO `pmb_representantsservicestechniques` (`IDpresenceouverture`, `dateconvocation`, `presence`, `qualite`, `representant`, `Dossiers_ID`, `etape`) VALUES
(1, NULL, NULL, 'Qualit�', 'Ibou', 1, 0),
(2, NULL, NULL, 'ddd', 'dd', 3, 0),
(3, NULL, NULL, 'fff', 'ff', 5, 0),
(4, NULL, NULL, 'sss', 'ss', 9, 0),
(5, NULL, NULL, 'sss', 'ss', 10, 0),
(6, NULL, NULL, 'ss', 'ss', 11, 0),
(7, NULL, NULL, 'ss', 'ss', 12, 0),
(8, NULL, NULL, 'dd', 'dd', 13, 0),
(9, NULL, NULL, 'ss', 'ss', 14, 0),
(10, NULL, NULL, 'ddd', 'd', 15, 0),
(11, '2013-05-09', NULL, 'ss', 'ss', 17, 0),
(13, NULL, NULL, 'dd', 'dd', 20, 0),
(14, NULL, NULL, 'Qualit�', 'Samb', 22, 0);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_resultatnegociation`
--

CREATE TABLE IF NOT EXISTS `pmb_resultatnegociation` (
  `idDocument` bigint(20) NOT NULL AUTO_INCREMENT,
  `renDate` date DEFAULT NULL,
  `renResultat` varchar(255) DEFAULT NULL,
  `Appelsoffres_ID` bigint(20) DEFAULT NULL,
  `Autorite_Id` bigint(20) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`idDocument`),
  KEY `FK54080C4621E224C0` (`Dossiers_ID`),
  KEY `FK54080C4625D5960` (`Appelsoffres_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `pmb_resultatnegociation`
--

INSERT INTO `pmb_resultatnegociation` (`idDocument`, `renDate`, `renResultat`, `Appelsoffres_ID`, `Autorite_Id`, `Dossiers_ID`) VALUES
(1, '2013-05-04', 'Concluant', 6, 6, 5),
(2, '2013-05-10', 'Concluant', 16, 902, 20);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_retraitregistredao`
--

CREATE TABLE IF NOT EXISTS `pmb_retraitregistredao` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateRetrait` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `modepaiement` varchar(50) DEFAULT NULL,
  `montantverse` decimal(19,2) DEFAULT NULL,
  `Ninea` varchar(255) DEFAULT NULL,
  `nomSoumissionnaire` varchar(255) DEFAULT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  `Dossiers_ID` bigint(20) DEFAULT NULL,
  `PAYS` bigint(20) DEFAULT NULL,
  `FOURNISSEUR` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK32699CA821E224C0` (`Dossiers_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Contenu de la table `pmb_retraitregistredao`
--

INSERT INTO `pmb_retraitregistredao` (`ID`, `dateRetrait`, `email`, `fax`, `modepaiement`, `montantverse`, `Ninea`, `nomSoumissionnaire`, `telephone`, `Autorite_ID`, `Dossiers_ID`, `PAYS`, `FOURNISSEUR`) VALUES
(1, '2013-04-28', 'ssi@ssi.sn', '', '0', '100000.00', NULL, '2SI(solutions et strat�gies informatique)', '76890', NULL, 1, 42, 1),
(2, '2013-04-28', 'ati@sentoo.sn', '', '0', '100000.00', NULL, 'A.T.I multim�dia', '(221)553-22-22', NULL, 1, 210, 2),
(3, '2013-04-28', 'tfc@yahoo.fr', '', '0', '100000.00', NULL, 'Entreprise  TFC (Travaux-Fournitures-con)', '33333', NULL, 1, 170, 3),
(4, '2013-05-04', 'ssi@ssi.sn', '', NULL, NULL, NULL, '2SI(solutions et strat�gies informatique)', '76890', 6, 3, NULL, 1),
(5, '2013-05-04', 'ati@sentoo.sn', '', NULL, NULL, NULL, 'A.T.I multim�dia', '(221)553-22-22', 6, 3, NULL, 2),
(6, '2013-05-04', 'ssi@ssi.sn', 'fffff', NULL, NULL, NULL, '2SI(solutions et strat�gies informatique)', '76890', 6, 4, 170, 1),
(8, '2013-05-04', 'neurotech@yahoo.fr', '', NULL, NULL, NULL, 'Neurotech', '6789654', 6, 4, 170, 5),
(9, '2013-05-04', 'ssi@ssi.sn', 'fffff', NULL, NULL, NULL, '2SI(solutions et strat�gies informatique)', '76890', 6, 5, 170, 1),
(10, '2013-05-04', 'neurotech@yahoo.fr', '', NULL, NULL, NULL, 'Neurotech', '6789654', 6, 5, 170, 5),
(11, '2013-05-04', 'ssi@ssi.sn', '', NULL, NULL, NULL, '2SI(solutions et strat�gies informatique)', '76890', 6, 9, 170, 1),
(12, '2013-05-04', 'ati@sentoo.sn', '', NULL, NULL, NULL, 'A.T.I multim�dia', '(221)553-22-22', 6, 9, 193, 5),
(13, '2013-05-05', 'ssi@ssi.sn', 'sss', '0', '400000.00', NULL, '2SI(solutions et strat�gies informatique)', '76890', NULL, 10, 170, 1),
(14, '2013-05-05', 'tfc@yahoo.fr', '', '0', '400000.00', NULL, 'Entreprise  TFC (Travaux-Fournitures-con)', '333', NULL, 10, 210, 3),
(15, '2013-05-05', 'neurotech@yahoo.fr', '', '0', '400000.00', NULL, 'Neurotech', '6789654', NULL, 10, 170, 5),
(16, '2013-05-05', 'ssi@ssi.sn', '', NULL, NULL, NULL, '2SI(solutions et strat�gies informatique)', '76890', 6, 11, 170, 1),
(17, '2013-05-05', 'ati@sentoo.sn', '', NULL, NULL, NULL, 'A.T.I multim�dia', '(221)553-22-22', 6, 11, 193, 2),
(18, '2013-05-05', 'tfc@@yahoo.fr', '', NULL, NULL, NULL, 'Entreprise  TFC (Travaux-Fournitures-con)', '', 6, 11, 210, 3),
(19, '2013-05-05', 'ssi@ssi.sn', '', '0', '5000000.00', NULL, '2SI(solutions et strat�gies informatique)', '76890', NULL, 12, 170, 1),
(20, '2013-05-05', 'ati@sentoo.sn', '', '0', '5000000.00', NULL, 'A.T.I multim�dia', '(221)553-22-22', NULL, 12, 193, 2),
(21, '2013-05-05', 'neurotech@yahoo.fr', '', '0', '5000000.00', NULL, 'Neurotech', '6789654', NULL, 12, 170, 5),
(22, '2013-05-04', 'ssi@ssi.sn', '', NULL, NULL, NULL, '2SI(solutions et strat�gies informatique)', '76890', 6, 13, 170, 1),
(23, '2013-05-04', 'ati@sentoo.sn', '', NULL, NULL, NULL, 'A.T.I multim�dia', '(221)553-22-22', 6, 13, 193, 2),
(24, '2013-05-04', 'tfc@@yahoo.fr', '', NULL, NULL, NULL, 'Entreprise  TFC (Travaux-Fournitures-con)', '', 6, 13, 210, 3),
(25, '2013-05-06', 'ssi@ssi.sn', '', '0', '70000.00', NULL, '2SI(solutions et strat�gies informatique)', '76890', NULL, 14, 170, 1),
(27, '2013-05-06', 'ati@sentoo.sn', '', '0', '70000.00', NULL, 'A.T.I multim�dia', '(221)553-22-22', NULL, 14, 193, 2),
(28, '2013-05-06', 'tfc@yahoo.fr', '', '0', '70000.00', NULL, 'Entreprise  TFC (Travaux-Fournitures-con)', '23', NULL, 14, 210, 3),
(29, '2013-05-08', 'ssi@ssi.sn', '', '0', '100000.00', NULL, '2SI(solutions et strat�gies informatique)', '76890', NULL, 15, 170, 1),
(30, '2013-05-08', 'ati@sentoo.sn', '', '0', '100000.00', NULL, 'A.T.I multim�dia', '(221)553-22-22', NULL, 15, 193, 2),
(31, '2013-05-08', 'ssi@ssi.sn', '', NULL, NULL, NULL, '2SI(solutions et strat�gies informatique)', '76890', 902, 16, 170, 1),
(32, '2013-05-08', 'ati@sentoo.sn', 'www', NULL, NULL, NULL, 'A.T.I multim�dia', '(221)553-22-22', 902, 16, 193, 2),
(33, '2013-05-08', 'neurotech@yahoo.fr', '', NULL, NULL, NULL, 'Neurotech', '6789654', 902, 16, 170, 5),
(34, '2013-05-08', 'ati@sentoo.sn', 'www', NULL, NULL, NULL, 'A.T.I multim�dia', '(221)553-22-22', 902, 17, 193, 2),
(35, '2013-05-08', 'ssi@ssi.sn', '', NULL, NULL, NULL, '2SI(solutions et strat�gies informatique)', '76890', 902, 17, 170, 1),
(36, '2013-05-08', 'ssi@ssi.sn', '', '0', '100000.00', NULL, '2SI(solutions et strat�gies informatique)', '76890', NULL, 18, 170, 1),
(37, '2013-05-08', 'ati@sentoo.sn', '', '0', '100000.00', NULL, 'A.T.I multim�dia', '(221)553-22-22', NULL, 18, 193, 2),
(38, '2013-05-08', 'neurotech@yahoo.fr', '', '0', '100000.00', NULL, 'Neurotech', '6789654', NULL, 18, 170, 5),
(39, '2013-05-08', 'tf@yahoo.fr', '', '0', '100000.00', NULL, 'Entreprise  TFC (Travaux-Fournitures-con)', '567', NULL, 18, 210, 3),
(40, '2013-05-09', 'ssi@ssi.sn', '', NULL, NULL, NULL, '2SI(solutions et strat�gies informatique)', '76890', 902, 19, 170, 1),
(41, '2013-05-09', 'tfc@yahoo.fr', '', NULL, NULL, NULL, 'Entreprise  TFC (Travaux-Fournitures-con)', '657983', 902, 19, 210, 3),
(42, '2013-05-10', 'neurotech@yahoo.fr', '', NULL, NULL, NULL, 'Neurotech', '6789654', 902, 19, 170, 5),
(43, '2013-05-09', 'tfc@yahoo.fr', '', NULL, NULL, NULL, 'Entreprise  TFC (Travaux-Fournitures-con)', '657983', 902, 20, 210, 3),
(44, '2013-05-09', 'ssi@ssi.sn', '', NULL, NULL, NULL, '2SI(solutions et strat�gies informatique)', '76890', 902, 20, 170, 1),
(45, '2013-05-10', 'ati@sentoo.sn', '', NULL, NULL, NULL, 'A.T.I multim�dia', '(221)553-22-22', 905, 21, 193, 2),
(46, '2013-05-10', 'tfc@yahoo.fr', '', NULL, NULL, NULL, 'Entreprise  TFC (Travaux-Fournitures-con)', '567', 905, 21, 210, 3),
(47, '2013-05-10', 'asamb@ssi.sn', '', NULL, NULL, NULL, 'Solid', '567', 905, 21, 170, 4),
(48, '2013-05-10', 'neurotech@yahoo.fr', '', NULL, NULL, NULL, 'Neurotech', '6789654', 905, 21, 170, 5),
(49, '2013-05-10', 'ati@sentoo.sn', '', NULL, NULL, NULL, 'A.T.I multim�dia', '(221)553-22-22', 905, 22, 193, 2),
(50, '2013-05-10', 'tfc@yahoo.fr', '', NULL, NULL, NULL, 'Entreprise  TFC (Travaux-Fournitures-con)', '567', 905, 22, 210, 3),
(51, '2013-05-10', 'asamb@ssi.sn', '', NULL, NULL, NULL, 'Solid', '567', 905, 22, 170, 4);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_service`
--

CREATE TABLE IF NOT EXISTS `pmb_service` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODIFICATION` varchar(255) DEFAULT NULL,
  `descriptionService` varchar(255) DEFAULT NULL,
  `libelleService` varchar(255) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  `Typeservice_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKEC594A7B5B8B65F4` (`Typeservice_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Contenu de la table `pmb_service`
--

INSERT INTO `pmb_service` (`ID`, `CODIFICATION`, `descriptionService`, `libelleService`, `Autorite_ID`, `Typeservice_ID`) VALUES
(1, 'DAGE', NULL, 'DAGE', 2, 2),
(2, 'DOP', 'Direction d''étude de projet', 'Direction d''étude de projet', 2, 1),
(3, 'DOP', NULL, NULL, 1, 2),
(4, 'DAGE', NULL, NULL, 1, 1),
(5, 'DOP', NULL, NULL, 4, 1),
(6, 'DAGE', NULL, NULL, 8, 2),
(7, 'DOP', NULL, NULL, 5, 2),
(8, 'RH', '', 'Ressources Humaines', 487, 2),
(9, 'DRH', '', 'Direction  des ressources humaines', 454, 2),
(10, 'DG', '', 'Direction Générale', 454, 2),
(11, 'DAGE', 'DAGE', 'DAGE', 719, NULL),
(12, 'DRH', 'Direction de la construction', 'Direction de la construction', 719, NULL),
(13, 'DOP', 'Direction d''�tude de projet', 'Direction d''�tude de projet', 719, NULL),
(14, 'DAGE', '', 'DAGE', 159, NULL),
(15, 'DAGE', 'Direction de l''administration g�n�rale et des �quipements', 'Direction de l''administration g�n�rale et des �quipements', 901, NULL),
(16, 'DM', 'Direction du mat�riel', 'Direction du mat�riel', 901, NULL),
(17, 'DMM', '', 'Direction des mines', 901, NULL),
(18, 'DAGE', 'DAGE', 'DAGE', 6, NULL),
(19, 'DAGE', 'DAGE', 'DAGE', 902, NULL),
(20, 'DNCMP', 'Direction nationale du contr�le des march�s publics', 'Direction nationale du contr�le des march�s publics', 9, NULL),
(21, 'DAF', 'Direction administrative et financi�re', 'Direction administrative et financi�re', 905, NULL),
(22, 'DER', 'Direction de l''entretien routier', 'Direction de l''entretien routier', 905, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_sigfipbudget`
--

CREATE TABLE IF NOT EXISTS `pmb_sigfipbudget` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `budchapitre` varchar(255) DEFAULT NULL,
  `budexercice` int(11) DEFAULT NULL,
  `budimputation` varchar(255) DEFAULT NULL,
  `budligne` varchar(255) DEFAULT NULL,
  `budmontantinitial` decimal(19,2) DEFAULT NULL,
  `budsolde` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_sigfipbudget`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_sigfipengagements`
--

CREATE TABLE IF NOT EXISTS `pmb_sigfipengagements` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `engchapitre` varchar(255) DEFAULT NULL,
  `engdate` date DEFAULT NULL,
  `engexercice` int(11) DEFAULT NULL,
  `engligne` varchar(255) DEFAULT NULL,
  `engmontant` decimal(19,2) DEFAULT NULL,
  `engnumero` varchar(255) DEFAULT NULL,
  `engobjet` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_sigfipengagements`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_sigfipengagementsannulation`
--

CREATE TABLE IF NOT EXISTS `pmb_sigfipengagementsannulation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `engchapitre` varchar(255) DEFAULT NULL,
  `engdate` date DEFAULT NULL,
  `engevt` varchar(255) DEFAULT NULL,
  `engexercice` bigint(20) DEFAULT NULL,
  `engligne` varchar(255) DEFAULT NULL,
  `engmontant` decimal(19,2) DEFAULT NULL,
  `engnumero` varchar(255) DEFAULT NULL,
  `engnumerobonannule` varchar(255) DEFAULT NULL,
  `engobjet` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_sigfipengagementsannulation`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_sigfipmouvements`
--

CREATE TABLE IF NOT EXISTS `pmb_sigfipmouvements` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mouvchapitre` varchar(255) DEFAULT NULL,
  `mouvexercice` bigint(20) DEFAULT NULL,
  `mouvligne` varchar(255) DEFAULT NULL,
  `mouvmontant` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_sigfipmouvements`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_sigfipmouvementsannulation`
--

CREATE TABLE IF NOT EXISTS `pmb_sigfipmouvementsannulation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mouvchapitre` varchar(255) DEFAULT NULL,
  `mouvexercice` bigint(20) DEFAULT NULL,
  `mouvligne` varchar(255) DEFAULT NULL,
  `mouvmontant` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_sigfipmouvementsannulation`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_statistiqueperiodique`
--

CREATE TABLE IF NOT EXISTS `pmb_statistiqueperiodique` (
  `docId` bigint(20) NOT NULL AUTO_INCREMENT,
  `docDescription` varchar(250) DEFAULT NULL,
  `docFichier` varchar(150) DEFAULT NULL,
  `docDateSaisie` date DEFAULT '0000-00-00',
  `docDatePublication` date DEFAULT '0000-00-00',
  `cat_catId` bigint(20) DEFAULT NULL,
  `eta_etaId` bigint(20) DEFAULT NULL,
  `docStatut` char(3) DEFAULT NULL,
  `docCommentairesPub` text,
  `docDateMiseEnPublication` date DEFAULT '0000-00-00',
  `docDateDemandePublication` date DEFAULT '0000-00-00',
  `docCommentaireDemandePub` text,
  `docDateDemandeMiseEnPublication` date DEFAULT '0000-00-00',
  PRIMARY KEY (`docId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=198 ;

--
-- Contenu de la table `pmb_statistiqueperiodique`
--

INSERT INTO `pmb_statistiqueperiodique` (`docId`, `docDescription`, `docFichier`, `docDateSaisie`, `docDatePublication`, `cat_catId`, `eta_etaId`, `docStatut`, `docCommentairesPub`, `docDateMiseEnPublication`, `docDateDemandePublication`, `docCommentaireDemandePub`, `docDateDemandeMiseEnPublication`) VALUES
(195, '  Arrêté N°144/CAB/PM/ARMP du 29 Juin 2012   portant attributions des Divisions Marchés Publics.', '17839854395035eca8397c0.pdf', '2012-08-23', '2012-08-23', 17, 5, 'PUB', '', '0000-00-00', '0000-00-00', NULL, '0000-00-00'),
(196, ' Arrêté N°145/CAB/PM/ARMP du 29 Juin 2012   portant Création, Attributions , composition-type et fonctionnement de la commission ad''hoc d''ouverture des plis et d''évaluation des offres des Marchés Publics et des délégations de Service Public d', '9430038035035f918158f3.pdf', '2012-08-23', '2012-08-23', 17, 5, 'PUB', '', '0000-00-00', '0000-00-00', NULL, '0000-00-00'),
(197, '  Arrêté N°146/CAB/PM/ARMP du 29 Juin 2012   Fixant les délais dans le cadre de la passation des Marchés Publics et des délégations de Service Publics .', '12856345095035fb5380246.pdf', '2012-08-23', '2012-08-23', 17, 5, 'PUB', '', '0000-00-00', '0000-00-00', NULL, '0000-00-00');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_statperiodique`
--

CREATE TABLE IF NOT EXISTS `pmb_statperiodique` (
  `statId` bigint(20) NOT NULL AUTO_INCREMENT,
  `statCode` varchar(15) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT '',
  `statLibelle` text,
  `statCategorie` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT '',
  PRIMARY KEY (`statId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=84 ;

--
-- Contenu de la table `pmb_statperiodique`
--

INSERT INTO `pmb_statperiodique` (`statId`, `statCode`, `statLibelle`, `statCategorie`) VALUES
(1, 'STAT_N01 ', 'March�s en nombre par type d�autorit� contractante et par type de proc�dure', 'STAT_N'),
(23, 'STAT_M02', 'March�s en montant sur Appel � Concurrence Ouverte par type d�autorit� contractante et par type de march� - ', 'STAT_M'),
(2, 'STAT_N02 ', 'March�s en nombre sur Appel � ConcurrenceOuverte par type d�autorit� contractante et par type de march� ', 'STAT_N'),
(3, 'STAT_N03 ', 'March�s en nombre sur Proc�dures D�rogatoires / GG par type d�autorit� contractante et par type de march� \r\n', 'STAT_N'),
(4, 'STAT_N04 ', 'March�s en nombre sur Proc�dures D�rogatoires / CR (AOR, DP/LR) par type d�autorit� contractante et par type de march�', 'STAT_N'),
(5, 'STAT_N05 ', 'March�s nationaux (dont les montants sont inf�rieurs aux seuils communautaires) en nombre par type d�autorit� contractante et par type de proc�dure ', 'STAT_N'),
(6, 'STAT_N06 ', 'March�s communautaires (dont les montants sont sup�rieurs ou �gal aux seuils communautaires) en nombre par type d�autorit� contractante et par type de proc�dure ', 'STAT_N'),
(7, 'STAT_N07 ', 'March�s nationaux (dont les montants sont inf�rieurs aux seuils communautaires) en nombre par type d�autorit� contractante et par type de march�', 'STAT_N'),
(8, 'STAT_N08 ', 'March�s communautaires (dont les montants sont sup�rieurs ou �gal aux seuils communautaires) en nombre par type d�autorit� contractante et par type de march� ', 'STAT_N'),
(9, 'STAT_N09 ', 'March�s en nombre par origine (nationalit�) du prestataire et par type de march� ', 'STAT_N'),
(10, 'STAT_N10 ', 'March�s de travaux en nombre par origine (nationalit�) du prestataire ', 'STAT_N'),
(11, 'STAT_N11 ', 'March�s de fournitures en nombre par origine (nationalit�) du prestataire ', 'STAT_N'),
(12, 'STAT_N12 ', 'March�s de services courants en nombre par origine (nationalit�) du prestataire ', 'STAT_N'),
(13, 'STAT_N13 ', 'March�s de prestations intellectuelles en nombre par origine (nationalit�) du prestataire ', 'STAT_N'),
(14, 'STAT_N14 ', 'D�l�gations de service public en nombre par origine (nationalit�) du prestataire ', 'STAT_N'),
(15, 'STAT_N15 ', 'March�s en nombre par source de financement et par type de march� ', 'STAT_N'),
(16, 'STAT_N16 ', 'R�partition des recours en nombre par type d�autorit� contractante et par type de march� ', 'STAT_N'),
(17, 'STAT_N17 ', 'R�partition des d�cisions de suspension (recours fond�s) en nombre par type d�autorit� contractante et par type de march� ', 'STAT_N'),
(18, 'STAT_N18 ', 'R�partition des recours en nombre par type d�autorit� contractante et par type de proc�dure ', 'STAT_N'),
(19, 'STAT_N19 ', 'R�partition des recours non recevables en nombre par type d�autorit� contractante et par type de march� ', 'STAT_N'),
(20, 'STAT_N20 ', 'R�partition des recours non recevables en nombre par type d�autorit� contractante et par type de proc�dure', 'STAT_N'),
(21, 'STAT_N21 ', 'R�partition en nombre des personnes form�s par type d�acteur ', 'STAT_N'),
(22, 'STAT_M01', 'March�s en montant par type d�autorit� contractante et par type de proc�dure ', 'STAT_M'),
(24, 'STAT_M03', 'March�s en montant sur Proc�dures D�rogatoires / GG par type d�autorit� contractante et par type de march� -', 'STAT_M'),
(25, 'STAT_M04', 'March�s en montant sur Proc�dures D�rogatoires / CR (AOR, DP/LR) par type d�autorit� contractante et par type de march� ', 'STAT_M'),
(26, 'STAT_M05', 'March�s nationaux (dont les montants sont inf�rieurs aux seuils communautaires) en montant par type d�autorit� contractante et par type de proc�dure ', 'STAT_M'),
(27, 'STAT_M06', 'March�s communautaires (dont les montants sont sup�rieurs ou �gal aux seuils communautaires) en montant par type d�autorit� contractante et par type de proc�dure', 'STAT_M'),
(28, 'STAT_M07', 'March�s nationaux (dont les montants sont inf�rieurs aux seuils communautaires) en montant par type d�autorit� contractante et par type de march� ', 'STAT_M'),
(29, 'STAT_M08', 'March�s communautaires (dont les montants sont sup�rieurs ou �gal aux seuils communautaires) en montant par type d�autorit� contractante et par type de march�', 'STAT_M'),
(30, 'STAT_M09', 'March�s en montant par origine (nationalit�) du prestataire et par type de march� ', 'STAT_M'),
(31, 'STAT_M10', 'March�s de travaux en montant par origine (nationalit�) du prestataire', 'STAT_M'),
(32, 'STAT_M11', 'March�s de fournitures en montant par origine (nationalit�) du prestataire', 'STAT_M'),
(33, 'STAT_M12', 'March�s de services courants en montant par origine (nationalit�) du prestataire ', 'STAT_M'),
(34, 'STAT_M13', 'March�s de prestations intellectuelles en montant par origine (nationalit�) du prestataire', 'STAT_M'),
(35, 'STAT_M14', 'D�l�gations de service public en montant par origine du prestataire ', 'STAT_M'),
(41, 'STAT_M15', 'March�s en montant par source de financement et par type de March�', 'STAT_M'),
(42, 'STAT_M16', 'R�partition des recours en montant par type d�autorit� contractante et par type de march�', 'STAT_M'),
(43, 'STAT_M17', 'R�partition des d�cisions de suspension (recours fond�s) en montant par type d�autorit� contractante et par type de march�', 'STAT_M'),
(44, 'STAT_M18', 'R�partition des recours en montant par type d�autorit� contractante et par type de proc�dure', 'STAT_M'),
(45, 'STAT_M19', 'R�partition des recours non recevables en montant par type d�autorit� contractante et par type de march�', 'STAT_M'),
(46, 'STAT_M20', 'R�partition des recours non recevables en montant par type d�autorit� contractante et par type de proc�dure', 'STAT_M'),
(47, 'LISTE_01', 'D�lai (jours) de r�daction du dossier d�appel � la concurrence ', 'LISTE_'),
(48, 'LISTE_02', 'Respect du PPM (jours) et d�lai de publicit� (jours) des avis d�appel � la concurrence ', 'LISTE_'),
(49, 'LISTE_03', 'Nombre d�offres soumises par processus ', 'LISTE_'),
(50, 'LISTE_04', 'D�lai (jours) d�attribution des march�s ', 'LISTE_'),
(51, 'LISTE_05', 'D�lai (jours) de signature de contrats ', 'LISTE_'),
(52, 'LISTE_06', 'Respect du d�lai (jours) de validit� des offres ', 'LISTE_'),
(53, 'LISTE_07', 'D�lai (jours) de traitement des dossiers par l�organe de contr�le ', 'LISTE_'),
(54, 'LISTE_08', 'Participation des entreprises nationales ', 'LISTE_'),
(55, 'LISTE_09', 'Participation des entreprises communautaires (UEMOA hors nationaux) ', 'LISTE_'),
(56, 'LISTE_10', 'Participation des entreprises internationales (hors UEMOA) ', 'LISTE_'),
(57, 'LISTE_11', 'Modification des contrats en cours d�ex�cution ', 'LISTE_'),
(58, 'LISTE_12', 'Situation d�ach�vement des contrats ', 'LISTE_'),
(59, 'LISTE_13', 'D�lai (jours) de paiement (r�glement) des contrats ', 'LISTE_'),
(60, 'LISTE_14', 'P�nalit�s de retard  ', 'LISTE_'),
(61, 'LISTE_15', 'Nombre de contrats par prestataire ', 'LISTE_'),
(62, 'LISTE_16', 'Proc�dures ayant fait l�objet d�un recours devant le CRD ', 'LISTE_'),
(63, 'LISTE_17', 'R�sultat des recours dans la passation des march�s ', 'LISTE_'),
(64, 'LISTE_18', 'Recours introduits aupr�s des tribunaux ', 'LISTE_'),
(65, 'LISTE_19', 'Recours dans la phase ex�cution ', 'LISTE_'),
(66, 'LISTE_20', 'Conciliation et non conciliation ', 'LISTE_'),
(67, 'LISTE_21', 'D�lai (jours) de traitement des plaintes ', 'LISTE_'),
(68, 'LISTE_22', 'Renforcement des capacit�s (formation des acteurs) ', 'LISTE_'),
(69, 'LISTE_23', 'Renforcement des capacit�s (formation des formateurs)', 'LISTE_'),
(70, 'LISTE_24', 'Audit des march�s publics ', 'LISTE_'),
(71, 'LISTE_25', 'Historiques de la g�n�ration et du tranfert automatis� des statistiques p�riodiques', 'LISTE_'),
(72, 'Graphe_N01 ', 'Evolution des march�s en nombre � sur les cinq (05) derni�res ann�es ', 'GRAPHE_N'),
(73, 'Graphe_N02 ', 'Evolution des march�s en valeur � sur les cinq (05) derni�res ann�es ', 'GRAPHE_N'),
(74, 'Graphe_N03 ', 'Evolution des march�s de gr� � gr� en nombre � sur les cinq (05) derni�res ann�es ', 'GRAPHE_N'),
(75, 'Graphe_N04 ', 'Evolution des march�s de gr� � gr� en valeur � sur les cinq (05) derni�res ann�es ', 'GRAPHE_N'),
(76, 'Graphe_N05 ', 'Evolution des march�s communautaires (dont les montants �teignent les seuils communautaires de publication) en nombre � sur les cinq (05) derni�res ann�es ', 'GRAPHE_N'),
(77, 'Graphe_N06 ', 'Evolution des march�s communautaires (dont les montants �teignent les seuils communautaires de publication) en valeur � sur les cinq (05) derni�res ann�es ', 'GRAPHE_N'),
(78, 'Graphe_N07 ', 'Evolution de la participation communautaire (march�s gagn�s par les ressortissants de l�UEMOA non nationaux) en nombre � sur les cinq (05) derni�res ann�es', 'GRAPHE_N'),
(79, 'Graphe_N08', 'Evolution de la participation communautaire (march�s gagn�s par les ressortissants de l�UEMOA non nationaux) en valeur � sur les cinq (05) derni�res ann�es', 'GRAPHE_N'),
(80, 'Graphe_N09', 'Evolution des recours en nombre � sur les cinq (05) derni�res ann�es ', 'GRAPHE_N'),
(81, 'Graphe_N10', 'Evolution des recours en valeur � sur les cinq (05) derni�res ann�es', 'GRAPHE_N'),
(82, 'Graphe_N11', 'Evolution du nombre de personnes form�es � sur les cinq (05) derni�res ann�es ', 'GRAPHE_N'),
(83, 'Graphe_N12 ', 'Evolution des indicateurs de performances sur les d�lais � sur les cinq (05) derni�res ann�es ', 'GRAPHE_N');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_suivipaiement`
--

CREATE TABLE IF NOT EXISTS `pmb_suivipaiement` (
  `suiviID` bigint(20) NOT NULL AUTO_INCREMENT,
  `datesuivi` date DEFAULT NULL,
  `montant` double DEFAULT NULL,
  `demande` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`suiviID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_suivipaiement`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_tagentcritere`
--

CREATE TABLE IF NOT EXISTS `pmb_tagentcritere` (
  `criId` bigint(20) NOT NULL,
  `cricode` varchar(255) DEFAULT NULL,
  `crilibelle` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`criId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `pmb_tagentcritere`
--

INSERT INTO `pmb_tagentcritere` (`criId`, `cricode`, `crilibelle`) VALUES
(1, 'PAYS', 'pays'),
(2, 'TYPM', 'type de marche'),
(3, 'OBJ', 'objet'),
(4, 'CAT', 'categorie');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_tagentrecherche`
--

CREATE TABLE IF NOT EXISTS `pmb_tagentrecherche` (
  `ageId` bigint(20) NOT NULL AUTO_INCREMENT,
  `agelibelle` varchar(255) DEFAULT NULL,
  `typ_typId` bigint(20) DEFAULT NULL,
  `ageetat` int(11) DEFAULT NULL,
  `usr_usrId` bigint(20) DEFAULT NULL,
  `agedatecreation` date DEFAULT NULL,
  PRIMARY KEY (`ageId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_tagentrecherche`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_tagentvaleurs`
--

CREATE TABLE IF NOT EXISTS `pmb_tagentvaleurs` (
  `valId` bigint(20) NOT NULL AUTO_INCREMENT,
  `age_ageId` bigint(20) DEFAULT NULL,
  `cri_criId` bigint(20) DEFAULT NULL,
  `valValeur` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`valId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_tagentvaleurs`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_talerteavisspecifiques`
--

CREATE TABLE IF NOT EXISTS `pmb_talerteavisspecifiques` (
  `aleId` bigint(20) NOT NULL AUTO_INCREMENT,
  `aleMail` varchar(255) DEFAULT NULL,
  `aleDatelimite` date DEFAULT NULL,
  `aleDatealerte` date DEFAULT NULL,
  `aleNbrjavant` int(11) DEFAULT NULL,
  `aleNbralerte` bigint(20) DEFAULT NULL,
  `aleObjet` varchar(255) DEFAULT NULL,
  `avs_avsId` bigint(20) DEFAULT NULL,
  `usr_usrId` bigint(20) DEFAULT NULL,
  `aleNombrerestant` int(11) NOT NULL,
  PRIMARY KEY (`aleId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_talerteavisspecifiques`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_talertecategorie`
--

CREATE TABLE IF NOT EXISTS `pmb_talertecategorie` (
  `aleId` bigint(20) NOT NULL AUTO_INCREMENT,
  `usr_usrId` bigint(20) DEFAULT NULL,
  `sec_secId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`aleId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_talertecategorie`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_talertemail`
--

CREATE TABLE IF NOT EXISTS `pmb_talertemail` (
  `aleId` bigint(20) NOT NULL AUTO_INCREMENT,
  `aleMail` varchar(255) DEFAULT NULL,
  `aleDatelancement` date DEFAULT NULL,
  `aleDatealerte` date DEFAULT NULL,
  `aleNombre` int(11) DEFAULT NULL,
  `rea_reaId` bigint(20) DEFAULT NULL,
  `usr_usrId` bigint(20) DEFAULT NULL,
  `aleNbrjravant` int(11) DEFAULT NULL,
  `aleNombrerestant` int(11) DEFAULT NULL,
  PRIMARY KEY (`aleId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_talertemail`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_taudit`
--

CREATE TABLE IF NOT EXISTS `pmb_taudit` (
  `idtaudit` bigint(20) NOT NULL AUTO_INCREMENT,
  `datestatut` date DEFAULT NULL,
  `gestion` int(11) DEFAULT NULL,
  `libelleaudit` varchar(255) DEFAULT NULL,
  `statut` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idtaudit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_taudit`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_tcourriers_ac`
--

CREATE TABLE IF NOT EXISTS `pmb_tcourriers_ac` (
  `tid` bigint(20) NOT NULL AUTO_INCREMENT,
  `tidorigine` bigint(20) DEFAULT NULL,
  `tdatesaisie` date DEFAULT NULL,
  `tcourrierinstruction` varchar(50) DEFAULT NULL,
  `tdateimputation` date DEFAULT NULL,
  `tdatemaxtraitement` date DEFAULT NULL,
  `agentdestinataire` bigint(20) DEFAULT NULL,
  `tbureau` bigint(20) DEFAULT NULL,
  `tcourrierac` bigint(20) DEFAULT NULL,
  `taccusereception` bit(1) DEFAULT NULL,
  `tnotifmail` bit(1) DEFAULT NULL,
  `timputationpour` varchar(20) DEFAULT NULL,
  `tcourrierautre` varchar(100) DEFAULT NULL,
  `tdatecourrier` date DEFAULT NULL,
  `tdatereception` date DEFAULT NULL,
  `tobjetcourrier` varchar(200) DEFAULT NULL,
  `tcourrierRef` varchar(50) DEFAULT NULL,
  `treference` varchar(50) DEFAULT NULL,
  `tservice` bigint(20) DEFAULT NULL,
  `tautoritecontractante` bigint(20) DEFAULT NULL,
  `tmodereception` bigint(20) DEFAULT NULL,
  `ttypecourrier` bigint(20) DEFAULT NULL,
  `tnatureCourrier` bigint(20) DEFAULT NULL,
  `imputerpar` varchar(255) DEFAULT NULL,
  `impforgroup` bit(1) DEFAULT NULL,
  `idmodetraitement` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`tid`),
  KEY `FK7C888F558087BA42` (`agentdestinataire`),
  KEY `FK7C888F5517AC5AC8` (`tbureau`),
  KEY `FK7C888F55ADA90538` (`tcourrierac`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `pmb_tcourriers_ac`
--

INSERT INTO `pmb_tcourriers_ac` (`tid`, `tidorigine`, `tdatesaisie`, `tcourrierinstruction`, `tdateimputation`, `tdatemaxtraitement`, `agentdestinataire`, `tbureau`, `tcourrierac`, `taccusereception`, `tnotifmail`, `timputationpour`, `tcourrierautre`, `tdatecourrier`, `tdatereception`, `tobjetcourrier`, `tcourrierRef`, `treference`, `tservice`, `tautoritecontractante`, `tmodereception`, `ttypecourrier`, `tnatureCourrier`, `imputerpar`, `impforgroup`, `idmodetraitement`) VALUES
(4, 4, '2013-05-25', '', '2013-05-25', '2013-05-30', 4, 2, 3, b'0', b'0', 'Traitement', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dcmp', b'0', 2);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_textreglementaire`
--

CREATE TABLE IF NOT EXISTS `pmb_textreglementaire` (
  `idRecueil` bigint(20) NOT NULL AUTO_INCREMENT,
  `text` text,
  `date_modif` date DEFAULT NULL,
  `image_apercu` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idRecueil`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `pmb_textreglementaire`
--

INSERT INTO `pmb_textreglementaire` (`idRecueil`, `text`, `date_modif`, `image_apercu`) VALUES
(1, ' Recueil de textes réglementaires relatifs �  la radioprotection (partie 1 : lois et décrets)\r\n\r\nExtraits du code de la santé publique et du code du marché concernant la protection de la population, des patients et des opérateurs contre les dangers des rayonnements ionisants. ', '2012-11-28', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_typecourrier`
--

CREATE TABLE IF NOT EXISTS `pmb_typecourrier` (
  `idtypecourrier` bigint(20) NOT NULL AUTO_INCREMENT,
  `codetypecourrier` varchar(10) DEFAULT NULL,
  `descriptiontypecourrier` varchar(255) DEFAULT NULL,
  `libelletypecourrier` varchar(255) DEFAULT NULL,
  `duretraitementenjour` int(11) DEFAULT NULL,
  PRIMARY KEY (`idtypecourrier`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `pmb_typecourrier`
--

INSERT INTO `pmb_typecourrier` (`idtypecourrier`, `codetypecourrier`, `descriptiontypecourrier`, `libelletypecourrier`, `duretraitementenjour`) VALUES
(1, 'SAISINE', 'SAISINE', 'SAISINE', 5);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_typedecision`
--

CREATE TABLE IF NOT EXISTS `pmb_typedecision` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `pmb_typedecision`
--

INSERT INTO `pmb_typedecision` (`id`, `libelle`) VALUES
(1, 'Sanction'),
(2, 'Recours irrecevables'),
(3, 'Recours rejetés'),
(4, 'Recours ayant obtenu gain de cause'),
(5, 'décision de suspension');

-- --------------------------------------------------------

--
-- Structure de la table `pmb_typedemande`
--

CREATE TABLE IF NOT EXISTS `pmb_typedemande` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(10) DEFAULT NULL,
  `libelle` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_typedemande`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_typeevenement`
--

CREATE TABLE IF NOT EXISTS `pmb_typeevenement` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_typeevenement`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_typemodeledocument`
--

CREATE TABLE IF NOT EXISTS `pmb_typemodeledocument` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idgrillesanalyses` bigint(20) DEFAULT NULL,
  `ModeleDocument` bigint(20) DEFAULT NULL,
  `TypesDossiers` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKA2CD05ABE4B6F694` (`ModeleDocument`),
  KEY `FKA2CD05AB9D3EDCDC` (`TypesDossiers`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `pmb_typemodeledocument`
--


-- --------------------------------------------------------

--
-- Structure de la table `pmb_typesdossiers`
--

CREATE TABLE IF NOT EXISTS `pmb_typesdossiers` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) DEFAULT NULL,
  `delaitraitement` int(11) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `delaicorrigee` int(11) DEFAULT NULL,
  `delairevue` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `pmb_typesdossiers`
--

INSERT INTO `pmb_typesdossiers` (`Id`, `code`, `delaitraitement`, `description`, `libelle`, `delaicorrigee`, `delairevue`) VALUES
(1, 'RDAO', 4, 'Revue DAO', 'Revue DAO', 1, 1),
(2, 'VAP', 3, 'Validation attribution provisoire', 'Validation attribution provisoire', 1, 1),
(3, 'EJT', 2, 'Examen juridique et technique', 'Examen juridique et technique', 1, 1),
(4, 'VDPD', 5, 'Validation demande de procédures dérogatoires', 'Validation demande de procédures dérogatoires', 1, 1),
(5, 'VAV', 6, 'Validation avenant', 'Validation avenant', 1, 1),
(6, 'DIM', 4, 'Demande d''Immatriculation de marchés', 'Demande d''Immatriculation de marchés', 1, 1),
(7, 'VNPPM', 3, 'Validation et publication nouveaux plans', 'Validation et publication nouveaux plans', 1, 1),
(8, 'VMPPM', 3, 'Validation et publication mises �  jour  plans', 'Validation et publication mises �  jour  plans', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_typesmarches_modespassations`
--

CREATE TABLE IF NOT EXISTS `pmb_typesmarches_modespassations` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `MODE` bigint(20) DEFAULT NULL,
  `TYPE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKAFDF47F285E1049E` (`MODE`),
  KEY `FKAFDF47F2337443D8` (`TYPE`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Contenu de la table `pmb_typesmarches_modespassations`
--

INSERT INTO `pmb_typesmarches_modespassations` (`ID`, `MODE`, `TYPE`) VALUES
(1, 1, 3),
(4, 34, 3),
(5, 1, 2),
(8, 34, 2),
(9, 42, 4),
(10, 34, 4),
(11, 1, 1),
(14, 34, 1),
(15, 1, 5),
(18, 34, 5);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_typesmarches_modesselections`
--

CREATE TABLE IF NOT EXISTS `pmb_typesmarches_modesselections` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `MODE` bigint(20) DEFAULT NULL,
  `TYPE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKBC510DAE581FBCA6` (`MODE`),
  KEY `FKBC510DAE337443D8` (`TYPE`),
  KEY `FK257790EA581FBCA6` (`MODE`),
  KEY `FK257790EA337443D8` (`TYPE`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `pmb_typesmarches_modesselections`
--

INSERT INTO `pmb_typesmarches_modesselections` (`ID`, `MODE`, `TYPE`) VALUES
(1, 2, 3),
(2, 2, 2),
(3, 2, 1),
(4, 2, 5),
(5, 1, 4);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_typeuniteorgarmp`
--

CREATE TABLE IF NOT EXISTS `pmb_typeuniteorgarmp` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `NIVEAU` int(11) DEFAULT NULL,
  `typeuniteorgID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK44661B2B438A352F` (`typeuniteorgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `pmb_typeuniteorgarmp`
--

INSERT INTO `pmb_typeuniteorgarmp` (`ID`, `LIBELLE`, `NIVEAU`, `typeuniteorgID`) VALUES
(1, 'Direction Générale', 0, NULL),
(2, 'Direction', 1, 1),
(3, 'Division', 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_typeuniteorgdcmp`
--

CREATE TABLE IF NOT EXISTS `pmb_typeuniteorgdcmp` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `NIVEAU` int(11) DEFAULT '0',
  `typeuniteorgID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK44673FF9438B59FD` (`typeuniteorgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `pmb_typeuniteorgdcmp`
--

INSERT INTO `pmb_typeuniteorgdcmp` (`ID`, `LIBELLE`, `NIVEAU`, `typeuniteorgID`) VALUES
(1, 'Direction Générale', 0, NULL),
(2, 'Direction', 1, 1),
(3, 'Division', 2, 2),
(4, 'Département', 3, 3),
(5, 'Service', 4, 4);

-- --------------------------------------------------------

--
-- Structure de la table `pmb_uniteorgarmp`
--

CREATE TABLE IF NOT EXISTS `pmb_uniteorgarmp` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `TypeUniteOrgID` bigint(20) DEFAULT NULL,
  `UniteOrgID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK7F671FF1B388FD1B` (`UniteOrgID`),
  KEY `FK7F671FF1438A352F` (`TypeUniteOrgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `pmb_uniteorgarmp`
--

INSERT INTO `pmb_uniteorgarmp` (`ID`, `LIBELLE`, `TypeUniteOrgID`, `UniteOrgID`) VALUES
(1, 'Direction Générale', 1, NULL),
(2, 'Direction ..', 2, 1),
(4, 'Division ....', 3, 2);

-- --------------------------------------------------------

--
-- Structure de la table `profil_role`
--

CREATE TABLE IF NOT EXISTS `profil_role` (
  `ROL_CODE` varchar(200) NOT NULL,
  `PF_CODE` varchar(32) NOT NULL,
  PRIMARY KEY (`ROL_CODE`,`PF_CODE`),
  KEY `FK8C670499C97CE164` (`PF_CODE`),
  KEY `FK8C670499DC88A854` (`ROL_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `profil_role`
--


-- --------------------------------------------------------

--
-- Structure de la table `sygaudit`
--

CREATE TABLE IF NOT EXISTS `sygaudit` (
  `IDAUDIT` bigint(20) NOT NULL AUTO_INCREMENT,
  `datestatut` date DEFAULT NULL,
  `GESTION` int(11) DEFAULT NULL,
  `LIBELLEAUDIT` varchar(255) DEFAULT NULL,
  `STATUT` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IDAUDIT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `sygaudit`
--


-- --------------------------------------------------------

--
-- Structure de la table `sygdossiercourrier`
--

CREATE TABLE IF NOT EXISTS `sygdossiercourrier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `dateStatus` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `state_STA_CODE` varchar(200) DEFAULT NULL,
  `typesDossiers_Id` bigint(20) DEFAULT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `ID_AC` bigint(20) DEFAULT NULL,
  `ID_PLAN` bigint(20) DEFAULT NULL,
  `ID_REALISATION` bigint(20) DEFAULT NULL,
  `ISSUE` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `sygdossiercourrier`
--


-- --------------------------------------------------------

--
-- Structure de la table `sygdossiergrilleanalyse`
--

CREATE TABLE IF NOT EXISTS `sygdossiergrilleanalyse` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `STATUT` int(11) DEFAULT NULL,
  `DOSSIER` bigint(20) DEFAULT NULL,
  `ID_GRILLEANA` bigint(20) DEFAULT NULL,
  `COMMENTAIRE` longtext,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `sygdossiergrilleanalyse`
--


-- --------------------------------------------------------

--
-- Structure de la table `sygdossiermodeldoc`
--

CREATE TABLE IF NOT EXISTS `sygdossiermodeldoc` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `STATUT` int(11) DEFAULT NULL,
  `DOSSIER` bigint(20) DEFAULT NULL,
  `ID_MODELDOC` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `sygdossiermodeldoc`
--


-- --------------------------------------------------------

--
-- Structure de la table `sygdossierpiecesjointes`
--

CREATE TABLE IF NOT EXISTS `sygdossierpiecesjointes` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DATEFILE` date DEFAULT NULL,
  `SRC_FILE` varchar(100) DEFAULT NULL,
  `NAME_FILE` varchar(100) DEFAULT NULL,
  `OBJECTFILE` longtext,
  `ID_DOSSIER` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `sygdossierpiecesjointes`
--


-- --------------------------------------------------------

--
-- Structure de la table `sygfilesac`
--

CREATE TABLE IF NOT EXISTS `sygfilesac` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateFile` datetime DEFAULT NULL,
  `fileSrc` varchar(255) DEFAULT NULL,
  `nameFile` varchar(255) DEFAULT NULL,
  `objectFile` varchar(255) DEFAULT NULL,
  `courrier_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `sygfilesac`
--

INSERT INTO `sygfilesac` (`id`, `dateFile`, `fileSrc`, `nameFile`, `objectFile`, `courrier_id`) VALUES
(1, NULL, '1723825052013_Pj_modele.docx', NULL, 'dd', 3),
(2, '2013-05-25 22:52:33', '17531025052013_Pj_recu.pdf', NULL, 'sss', 4);

-- --------------------------------------------------------

--
-- Structure de la table `sygproformation`
--

CREATE TABLE IF NOT EXISTS `sygproformation` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `effectif` varchar(15) DEFAULT NULL,
  `forCible` varchar(255) DEFAULT NULL,
  `forCommentaire` varchar(255) DEFAULT NULL,
  `forDateDebut` date DEFAULT NULL,
  `forDateFin` date DEFAULT NULL,
  `forLibelle` varchar(255) DEFAULT NULL,
  `forLieu` varchar(255) DEFAULT NULL,
  `forRef` varchar(255) DEFAULT NULL,
  `formateur` varchar(255) DEFAULT NULL,
  `module` varchar(255) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKDE11E5C54B9DC270` (`Autorite_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `sygproformation`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_annee`
--

CREATE TABLE IF NOT EXISTS `syg_annee` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LIBELLE` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `syg_annee`
--

INSERT INTO `syg_annee` (`ID`, `LIBELLE`) VALUES
(3, '2012'),
(4, '2013');

-- --------------------------------------------------------

--
-- Structure de la table `syg_annuaire`
--

CREATE TABLE IF NOT EXISTS `syg_annuaire` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EMAIL` varchar(50) DEFAULT NULL,
  `FONCTION` varchar(255) DEFAULT NULL,
  `NOM` varchar(50) DEFAULT NULL,
  `ORGANE` varchar(50) DEFAULT NULL,
  `PRENOM` varchar(255) DEFAULT NULL,
  `TELEPHONE` varchar(50) DEFAULT NULL,
  `AUTORITE` bigint(20) DEFAULT NULL,
  `SERVICE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_annuaire`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_archive_contenu`
--

CREATE TABLE IF NOT EXISTS `syg_archive_contenu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dateArchive` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `motCle` varchar(255) DEFAULT NULL,
  `NoeudClassement_CODE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK63A34C7F55868AE6` (`NoeudClassement_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_archive_contenu`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_autoritecontractante`
--

CREATE TABLE IF NOT EXISTS `syg_autoritecontractante` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DENOMINATION` varchar(255) DEFAULT NULL,
  `ORDRE` int(11) DEFAULT NULL,
  `RESPONSABLE` varchar(255) DEFAULT NULL,
  `TYPEAUTORITE` bigint(20) DEFAULT NULL,
  `ADRESSE` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(50) DEFAULT NULL,
  `FAX` varchar(50) DEFAULT NULL,
  `TELEPHONE` varchar(50) DEFAULT NULL,
  `LOGO` varchar(50) DEFAULT NULL,
  `SIGLE` varchar(50) DEFAULT NULL,
  `URLSITE` varchar(255) DEFAULT NULL,
  `POLE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK4E3CB8C573B9D2D8` (`TYPEAUTORITE`),
  KEY `FK4E3CB8C5349479D8` (`POLE`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Contenu de la table `syg_autoritecontractante`
--

INSERT INTO `syg_autoritecontractante` (`ID`, `DENOMINATION`, `ORDRE`, `RESPONSABLE`, `TYPEAUTORITE`, `ADRESSE`, `EMAIL`, `FAX`, `TELEPHONE`, `LOGO`, `SIGLE`, `URLSITE`, `POLE`) VALUES
(1, 'Ministère de l’Eau, de l’Assainissement et de l’Hydraulique Villageoise', 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 'MEAHV', NULL, NULL),
(2, 'Ministère de l’économie et des finances', 2, 'Adama Mboup', 1, 'Cellule de passation de marchés  rue Carde - Immeuble Peytavin  (8ème étage ) BP 4017 - Dakar-Sénégal', 'cpmarches@minfinances.sn', '+221338213984', '+221338213985', 'photo_Ministère des Finances.jpeg', 'MF', 'http://www.finances.gouv.sn/', NULL),
(3, 'Ministère de l’Agriculture, de l’Elevage et de la Pêche ', 3, '', 1, 'AA', '', '', '33 7676878', NULL, 'MAT', 'dcmp@minfinances.sn', NULL),
(4, 'Ministère des Arts et Culture', 4, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Ministère de la Fonction Publique et des Réformes Administratives ', 5, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Présidence de la République', 6, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Premier Ministère', 7, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'Ministère délégué auprès du Ministère de l’Agriculture, de l’Elevage et de la Pêche, chargé des Infrastructures Rurales', 8, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'Ministère du Commerce et de la Promotion du Secteur Privé', 9, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'Ministère de l’Enseignement Technique et de la Formation  Professionnelle ', 10, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'Ministère du Développement �  la Base, de l’Artisanat, de la Jeunesse et de l’Emploi des Jeunes', 11, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'Ministère de la Promotion de la Femme ', 12, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'Ministère de l’Action Sociale et de la Solidarité Nationale', 13, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'Ministère des Enseignements Primaire et Secondaire et de l’Alphabétisation', 14, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'Ministère des Droits de l’Homme et de la Consolidation de la Démocratie et de la Formation Civique', 15, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'Ministère des Sports et Loisirs ', 16, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'Ministère de la Sécurité et de la Protection Civile', 17, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 'Ministère du Tourisme', 18, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'Ministère de la Justice chargée des relations avec les Institutions de la République', 19, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'Ministère auprès du Président de la République, chargé de la Planification du Développement et de l’Aménagement du Territoire', 20, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'Ministère de l’Administration Territoriale, de la Décentralisation et des Collectivités Locales ', 21, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'Ministère de Travail, de l’Emploi et de la Sécurité Sociale', 22, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'Ministère de l’enseignement supérieur et de la recherche', 23, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'Ministère de l’Industrie, de la Zone Franche et des Innovation Technologiques', 24, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'Ministère des Affaires Etrangères et de la Coopération ', 25, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'Agence Régionale de Developpement de THIES', 96, '', 2, 'Liberté', '', '', '778569244', NULL, 'ARD', '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `syg_autretype_categori`
--

CREATE TABLE IF NOT EXISTS `syg_autretype_categori` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `NIVEAU` int(11) DEFAULT NULL,
  `autreType` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `syg_autretype_categori`
--

INSERT INTO `syg_autretype_categori` (`ID`, `LIBELLE`, `NIVEAU`, `autreType`) VALUES
(1, 'autre service', 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `syg_bailleuretformation`
--

CREATE TABLE IF NOT EXISTS `syg_bailleuretformation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATEACCORD` date DEFAULT NULL,
  `montant` bigint(20) DEFAULT NULL,
  `NUMEROACCORD` varchar(255) DEFAULT NULL,
  `bailleur_id` bigint(20) DEFAULT NULL,
  `formation_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `syg_bailleuretformation`
--

INSERT INTO `syg_bailleuretformation` (`ID`, `DATEACCORD`, `montant`, `NUMEROACCORD`, `bailleur_id`, `formation_id`) VALUES
(1, '2013-05-02', 1000000, 'ddd', 11, 1),
(2, '2013-05-02', 2000000, 'sss', 15, 1),
(3, '2013-05-11', 500000, 'ZZ', 11, 3),
(4, '2013-05-13', 1000000, '567', 11, 14);

-- --------------------------------------------------------

--
-- Structure de la table `syg_bailleurformation`
--

CREATE TABLE IF NOT EXISTS `syg_bailleurformation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATEACCORD` date DEFAULT NULL,
  `montant` bigint(20) DEFAULT NULL,
  `NUMEROACCORD` varchar(255) DEFAULT NULL,
  `bailleur_id` bigint(20) DEFAULT NULL,
  `formation_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_bailleurformation`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_bureauxdcmp`
--

CREATE TABLE IF NOT EXISTS `syg_bureauxdcmp` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `DIVISION` bigint(20) DEFAULT NULL,
  `UniteOrgID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK7223B622CE72B642` (`DIVISION`),
  KEY `FK7223B62287968BB2` (`DIVISION`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Contenu de la table `syg_bureauxdcmp`
--

INSERT INTO `syg_bureauxdcmp` (`ID`, `LIBELLE`, `DIVISION`, `UniteOrgID`) VALUES
(1, 'Direction Générale', 1, NULL),
(2, 'Direction des affaires juridiques', 2, 1),
(4, 'Direction des affaires financiers', 2, 1),
(5, 'Division des', 3, 2),
(6, 'Département Informatique', 4, 5),
(7, 'Département ....', 4, 5),
(8, 'Direction des affaires maritimes', 2, 1),
(9, 'Direction des', 2, 1),
(10, 'Division des ..', 3, 4),
(11, 'Service ....', 5, 7),
(12, 'Division ', 3, 9),
(14, 'Service ...', 5, 13),
(15, 'Service Informatique', 5, 13),
(16, 'Service f', 5, 13),
(17, 'Service M', 5, 13);

-- --------------------------------------------------------

--
-- Structure de la table `syg_caractere`
--

CREATE TABLE IF NOT EXISTS `syg_caractere` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LIBELLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_caractere`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_categorie`
--

CREATE TABLE IF NOT EXISTS `syg_categorie` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `CATEGORI` bigint(20) DEFAULT NULL,
  `TYPE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKB46CDD39F9B5AB3C` (`TYPE`),
  KEY `FKB46CDD391A0469E2` (`CATEGORI`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Contenu de la table `syg_categorie`
--

INSERT INTO `syg_categorie` (`ID`, `LIBELLE`, `CATEGORI`, `TYPE`) VALUES
(1, 'Categorie', NULL, NULL),
(2, 'Prestation et Services', 1, 1),
(3, 'Agriculture et Produits Alimentaires', 1, 1),
(4, 'Fourniture Courantes', 1, 1),
(5, 'Services de Reparation, d''entretien et d''installation', 2, 2),
(6, 'Services informatique', 2, 2),
(7, 'Services de gardiennage', 2, 2),
(8, 'Services de nettoiement', 2, 2),
(9, 'Autres services', 2, 2),
(10, 'Services d�coration', 9, 3),
(15, 'service carrelage', 9, 3),
(16, 'Service de location', 9, 3),
(20, 'ujjjj', 3, 2),
(21, 'uuuu', 3, 2),
(22, 'gggg', 3, 2),
(23, 'Mat�riel informatique et fournitures de bureau', 4, 2),
(24, 'D�veloppement de logiciels', 4, 2),
(25, 'Mat�riel roulant', 4, 2),
(26, 'Mat�riel de bureau', 4, 2),
(27, 'Denr�es alimentaires', 4, 2),
(28, 'Effets vestimentaires', 4, 2),
(29, 'Fourniture de bureau', 4, 2),
(30, 'Mobilier et mat�riel d''appartement', 4, 2);

-- --------------------------------------------------------

--
-- Structure de la table `syg_catfour`
--

CREATE TABLE IF NOT EXISTS `syg_catfour` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `syg_catfour`
--

INSERT INTO `syg_catfour` (`ID`, `DESCRIPTION`, `LIBELLE`) VALUES
(1, 'Prestataire de service', 'Prestataire de service'),
(2, 'Consommable informatique', 'Consommable informatique');

-- --------------------------------------------------------

--
-- Structure de la table `syg_catindstatgle`
--

CREATE TABLE IF NOT EXISTS `syg_catindstatgle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `libelle` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `syg_catindstatgle`
--

INSERT INTO `syg_catindstatgle` (`id`, `code`, `libelle`) VALUES
(1, 'code', 'Libelle');

-- --------------------------------------------------------

--
-- Structure de la table `syg_catindsurvmulti`
--

CREATE TABLE IF NOT EXISTS `syg_catindsurvmulti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `libelle` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_catindsurvmulti`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_configurationtaux`
--

CREATE TABLE IF NOT EXISTS `syg_configurationtaux` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(20) NOT NULL,
  `TAUX` double DEFAULT NULL,
  `paysID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_configurationtaux`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_coordonateur`
--

CREATE TABLE IF NOT EXISTS `syg_coordonateur` (
  `IDCOOR` int(11) NOT NULL AUTO_INCREMENT,
  `ADRESSECOOR` varchar(255) DEFAULT NULL,
  `COMMENTAIRECOOR` varchar(255) DEFAULT NULL,
  `EMAILCOOR` varchar(255) DEFAULT NULL,
  `NINCOOR` bigint(20) DEFAULT NULL,
  `NOMCOOR` varchar(255) DEFAULT NULL,
  `PRENOMCOOR` varchar(255) DEFAULT NULL,
  `TELEPHONECOOR` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IDCOOR`),
  UNIQUE KEY `IDCOOR` (`IDCOOR`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `syg_coordonateur`
--

INSERT INTO `syg_coordonateur` (`IDCOOR`, `ADRESSECOOR`, `COMMENTAIRECOOR`, `EMAILCOOR`, `NINCOOR`, `NOMCOOR`, `PRENOMCOOR`, `TELEPHONECOOR`) VALUES
(1, 'Dakar', NULL, 'agaye@yahoo.fr', 22, 'Gaye', 'Youssou', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `syg_critere`
--

CREATE TABLE IF NOT EXISTS `syg_critere` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `syg_critere`
--

INSERT INTO `syg_critere` (`ID`, `LIBELLE`, `Autorite_ID`) VALUES
(1, 'Avis de non failite', 6),
(2, 'Maitrise des technologies', 6),
(3, 'Crit�re 1', 902),
(4, '5 march�s simuliares', 9),
(5, 'Chiffre d''affaire au moins �gale � 0,5 fois l''offre financi�re', 9),
(6, 'Crit�re 2', 902),
(7, 'Exp�rience du cabinet', 905),
(8, 'Exp�rience specifique du cabinet', 905),
(9, 'Organisation', 905);

-- --------------------------------------------------------

--
-- Structure de la table `syg_decisiondenonciation`
--

CREATE TABLE IF NOT EXISTS `syg_decisiondenonciation` (
  `idtypedecision` bigint(20) NOT NULL AUTO_INCREMENT,
  `libelletypedecision` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idtypedecision`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `syg_decisiondenonciation`
--

INSERT INTO `syg_decisiondenonciation` (`idtypedecision`, `libelletypedecision`) VALUES
(1, 'Sanction'),
(2, 'Sanction irrecevable'),
(3, 'Sanction rejet�'),
(4, 'D�cision de suspension'),
(5, 'D�cision ayant obtenu gain de cause');

-- --------------------------------------------------------

--
-- Structure de la table `syg_delais`
--

CREATE TABLE IF NOT EXISTS `syg_delais` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(255) DEFAULT NULL,
  `COMMENTAIRE` varchar(255) DEFAULT NULL,
  `DATE` date DEFAULT NULL,
  `DEBUTVALIDITE` date DEFAULT NULL,
  `FINVALIDITE` date DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `UNITE` varchar(50) DEFAULT NULL,
  `VALEUR` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `syg_delais`
--

INSERT INTO `syg_delais` (`ID`, `CODE`, `COMMENTAIRE`, `DATE`, `DEBUTVALIDITE`, `FINVALIDITE`, `LIBELLE`, `UNITE`, `VALEUR`) VALUES
(1, 'DTVP', 'Délais de traitement  des validations de Plans de passations et des avis généraux  par la dcmp', '2012-09-14', '2012-09-14', '2012-10-01', 'Délais de traitement  des validations de Plans de passations et des avis généraux  par la dcmp', 'Jours', 7),
(2, 'RACR', 'L''AC a 5 jours ouvrables pour r''pondre (RECOURS)', '2012-09-15', '2012-09-15', '2012-10-01', 'L’autorité contractante a 5 jours ouvrables pour répondre', 'Jours', 5);

-- --------------------------------------------------------

--
-- Structure de la table `syg_division`
--

CREATE TABLE IF NOT EXISTS `syg_division` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LIBELLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `syg_division`
--

INSERT INTO `syg_division` (`ID`, `LIBELLE`) VALUES
(1, 'Contrôle et Visas'),
(2, 'Statistique et Information'),
(3, 'Formation, Étude, Conseil'),
(4, 'Direction');

-- --------------------------------------------------------

--
-- Structure de la table `syg_dossierpiecesrequises`
--

CREATE TABLE IF NOT EXISTS `syg_dossierpiecesrequises` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `STATUT` int(11) DEFAULT NULL,
  `DOSSIER` bigint(20) DEFAULT NULL,
  `ID_PIECESREQUISE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  KEY `FK8E8636416B247BB5` (`ID_PIECESREQUISE`),
  KEY `FK8E863641132C709D` (`DOSSIER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_dossierpiecesrequises`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_fichpresence`
--

CREATE TABLE IF NOT EXISTS `syg_fichpresence` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DateFiche` date DEFAULT NULL,
  `JOUR` varchar(255) DEFAULT NULL,
  `Formation_id` bigint(20) DEFAULT NULL,
  `Nbre_absen` int(11) DEFAULT NULL,
  `Nbre_present` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Contenu de la table `syg_fichpresence`
--

INSERT INTO `syg_fichpresence` (`ID`, `DateFiche`, `JOUR`, `Formation_id`, `Nbre_absen`, `Nbre_present`) VALUES
(13, '2013-05-03', 'JOUR 2', 2, NULL, NULL),
(14, '2013-05-04', 'JOUR 3', 2, NULL, NULL),
(15, '2013-05-05', 'JOUR 4', 2, NULL, NULL),
(16, '2013-05-02', 'JOUR 1', 1, 0, 4),
(17, '2013-05-03', 'JOUR 2', 1, NULL, NULL),
(18, '2013-05-04', 'JOUR 3', 1, NULL, NULL),
(19, '2013-05-05', 'JOUR 4', 1, NULL, NULL),
(20, '2013-05-06', 'JOUR 5', 1, NULL, NULL),
(21, '2013-05-07', 'JOUR 6', 1, NULL, NULL),
(22, '2013-05-08', 'JOUR 7', 1, NULL, NULL),
(23, '2013-05-09', 'JOUR 8', 1, NULL, NULL),
(24, '2013-05-10', 'JOUR 9', 1, NULL, NULL),
(25, '2013-05-11', 'JOUR 10', 1, NULL, NULL),
(34, '2013-05-11', 'JOUR 1', 3, 0, 2),
(35, '2013-05-12', 'JOUR 2', 3, 0, 2),
(36, '2013-05-13', 'JOUR 3', 3, 1, 1),
(38, '2013-05-13', 'JOUR 1', 13, 1, 2),
(44, '2013-05-21', 'JOUR 1', 14, 0, 1),
(45, '2013-05-22', 'JOUR 2', 14, 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `syg_fichpresence_participant`
--

CREATE TABLE IF NOT EXISTS `syg_fichpresence_participant` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DateFiche` date DEFAULT NULL,
  `JOUR` varchar(255) DEFAULT NULL,
  `NOMBRE_ABSEN` bigint(20) DEFAULT NULL,
  `NOMBRE_PRESENT` bigint(20) DEFAULT NULL,
  `NOM_PARTICIPANT` varchar(255) DEFAULT NULL,
  `PRENOM_PARTICIPANT` varchar(255) DEFAULT NULL,
  `STATUT` bit(1) DEFAULT NULL,
  `Fiche_id` bigint(20) DEFAULT NULL,
  `Participant_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Contenu de la table `syg_fichpresence_participant`
--

INSERT INTO `syg_fichpresence_participant` (`ID`, `DateFiche`, `JOUR`, `NOMBRE_ABSEN`, `NOMBRE_PRESENT`, `NOM_PARTICIPANT`, `PRENOM_PARTICIPANT`, `STATUT`, `Fiche_id`, `Participant_id`) VALUES
(15, NULL, NULL, 0, 4, NULL, NULL, NULL, 16, 2),
(16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, 3),
(17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, 4),
(18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 16, 12),
(19, NULL, NULL, 0, 2, NULL, NULL, NULL, 34, 13),
(20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 34, 14),
(21, NULL, NULL, 0, 2, NULL, NULL, NULL, 35, 13),
(22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 35, 14),
(23, NULL, NULL, 1, 1, NULL, NULL, NULL, 36, 13),
(27, NULL, NULL, 1, 2, NULL, NULL, NULL, 38, 36),
(28, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 38, 34),
(29, NULL, NULL, 0, 1, NULL, NULL, NULL, 44, 37),
(30, NULL, NULL, 0, 1, NULL, NULL, NULL, 45, 37);

-- --------------------------------------------------------

--
-- Structure de la table `syg_fonctions`
--

CREATE TABLE IF NOT EXISTS `syg_fonctions` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `syg_fonctions`
--

INSERT INTO `syg_fonctions` (`ID`, `DESCRIPTION`, `LIBELLE`) VALUES
(1, 'Directeur', 'Directeur'),
(2, 'Division du Contrôle et des Visas', 'Division du Contrôle et des Visas'),
(4, '', 'sds'),
(5, '', 'sds');

-- --------------------------------------------------------

--
-- Structure de la table `syg_formateur`
--

CREATE TABLE IF NOT EXISTS `syg_formateur` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ADDRESS` varchar(255) DEFAULT NULL,
  `COMMENTAIRE` varchar(255) DEFAULT NULL,
  `DATENAISS` datetime DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `LIEUNAISS` varchar(200) DEFAULT NULL,
  `NIN` bigint(20) DEFAULT NULL,
  `NOM` varchar(50) DEFAULT NULL,
  `PRENOM` varchar(100) DEFAULT NULL,
  `SEXE` varchar(255) DEFAULT NULL,
  `TELEPHONE` bigint(20) DEFAULT NULL,
  `SPECIALITE` int(11) NOT NULL,
  `Curi` varchar(255) DEFAULT NULL,
  `TYPEFORM` bigint(20) DEFAULT NULL,
  `ADDRESS_SOCIETE` varchar(255) DEFAULT NULL,
  `MAIL_SOCIETE` varchar(255) DEFAULT NULL,
  `NOM_SOCIETE` varchar(255) DEFAULT NULL,
  `TEL_SOCIETE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  KEY `FKF2B1A64D990BE908` (`SPECIALITE`),
  KEY `FKF2B1A64D46971F09` (`TYPEFORM`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `syg_formateur`
--

INSERT INTO `syg_formateur` (`ID`, `ADDRESS`, `COMMENTAIRE`, `DATENAISS`, `EMAIL`, `LIEUNAISS`, `NIN`, `NOM`, `PRENOM`, `SEXE`, `TELEPHONE`, `SPECIALITE`, `Curi`, `TYPEFORM`, `ADDRESS_SOCIETE`, `MAIL_SOCIETE`, `NOM_SOCIETE`, `TEL_SOCIETE`) VALUES
(1, 'Dakar', 'RAS', '1980-04-30 00:00:00', 'awa.diop@yahoo.fr', NULL, 2, 'Diop', 'Awa', 'Femme', 4567, 1, '', 1, NULL, NULL, NULL, NULL),
(2, 'wwwwwwww', 'wwwwwww', '1973-05-11 00:00:00', 'www@ssi.sn', NULL, 234, 'wwwww', 'wwww', 'Homme', 222222, 1, '11-05-2013_Pj_recu.pdf', 1, NULL, NULL, NULL, NULL),
(3, NULL, '', NULL, 'xx@ssi.sn', NULL, NULL, '', '', NULL, NULL, 1, NULL, 2, 'xxx', 'xx@ssi.sn', 'xxx', 22222222);

-- --------------------------------------------------------

--
-- Structure de la table `syg_formation`
--

CREATE TABLE IF NOT EXISTS `syg_formation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `FORMATEUR` int(11) NOT NULL,
  `MODULE` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  KEY `FK6A23D473317E865E` (`MODULE`),
  KEY `FK6A23D47343D40590` (`FORMATEUR`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_formation`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_formationetautorite`
--

CREATE TABLE IF NOT EXISTS `syg_formationetautorite` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `autorite_id` bigint(20) DEFAULT NULL,
  `formation_id` bigint(20) DEFAULT NULL,
  `typeautorite_id` bigint(20) DEFAULT NULL,
  `groupe_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `syg_formationetautorite`
--

INSERT INTO `syg_formationetautorite` (`ID`, `autorite_id`, `formation_id`, `typeautorite_id`, `groupe_id`) VALUES
(1, 413, 1, 1, NULL),
(2, 922, 1, 3, NULL),
(3, 927, 1, 3, NULL),
(4, 939, 1, 5, NULL),
(5, 916, 2, 2, NULL),
(6, 413, 3, 1, NULL),
(7, 914, 3, 2, NULL),
(8, 932, 3, 3, NULL),
(9, 413, 4, 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `syg_formation_tautorite`
--

CREATE TABLE IF NOT EXISTS `syg_formation_tautorite` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `formation_id` bigint(20) DEFAULT NULL,
  `groupe_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `syg_formation_tautorite`
--

INSERT INTO `syg_formation_tautorite` (`ID`, `formation_id`, `groupe_id`) VALUES
(2, 3, 1),
(3, 3, 2),
(4, 13, 2),
(5, 13, 1),
(6, 14, 2),
(7, 15, 2),
(8, 15, 1);

-- --------------------------------------------------------

--
-- Structure de la table `syg_groupedeformation`
--

CREATE TABLE IF NOT EXISTS `syg_groupedeformation` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(255) DEFAULT NULL,
  `DESCRIPTIONN` varchar(255) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `syg_groupedeformation`
--

INSERT INTO `syg_groupedeformation` (`ID`, `CODE`, `DESCRIPTIONN`, `LIBELLE`) VALUES
(1, 'GA', 'Groupe A', 'Groupe A'),
(2, 'GB', '', 'Groupe B');

-- --------------------------------------------------------

--
-- Structure de la table `syg_indstatgle`
--

CREATE TABLE IF NOT EXISTS `syg_indstatgle` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `libelle` varchar(200) NOT NULL,
  `signacation` varchar(255) NOT NULL,
  `categorie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_indstatgle`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_indsurvmulti`
--

CREATE TABLE IF NOT EXISTS `syg_indsurvmulti` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `libelle` varchar(200) NOT NULL,
  `signacation` varchar(255) NOT NULL,
  `categorie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_indsurvmulti`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_instructions`
--

CREATE TABLE IF NOT EXISTS `syg_instructions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LIBELLE` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_instructions`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_joursferies`
--

CREATE TABLE IF NOT EXISTS `syg_joursferies` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DATE` date DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `syg_joursferies`
--

INSERT INTO `syg_joursferies` (`ID`, `DATE`, `DESCRIPTION`) VALUES
(1, '2013-06-06', 'Jour de l''an'),
(2, '2013-06-10', 'Fête nationale'),
(3, '2013-06-18', 'Fête du travail');

-- --------------------------------------------------------

--
-- Structure de la table `syg_listerougefournisseur`
--

CREATE TABLE IF NOT EXISTS `syg_listerougefournisseur` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `commentaire` varchar(255) DEFAULT NULL,
  `datedebut` date DEFAULT NULL,
  `datefin` date DEFAULT NULL,
  `motif` varchar(255) DEFAULT NULL,
  `ninea` varchar(55) DEFAULT NULL,
  `raisonsociale` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `decision` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_listerougefournisseur`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_logisticien`
--

CREATE TABLE IF NOT EXISTS `syg_logisticien` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ADRESSE` varchar(255) DEFAULT NULL,
  `COMMENTAIRE` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `NIN` bigint(20) DEFAULT NULL,
  `NOM` varchar(255) DEFAULT NULL,
  `PRENOM` varchar(255) DEFAULT NULL,
  `TELEPHONE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `syg_logisticien`
--

INSERT INTO `syg_logisticien` (`ID`, `ADRESSE`, `COMMENTAIRE`, `EMAIL`, `NIN`, `NOM`, `PRENOM`, `TELEPHONE`) VALUES
(1, 'Dakar', NULL, 'adiop@yahoo.fr', 233, 'Diop', 'Amy', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `syg_membres_groupe`
--

CREATE TABLE IF NOT EXISTS `syg_membres_groupe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adresse` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `nom` varchar(100) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `autorite_contra` bigint(20) NOT NULL,
  `groupe_id` int(11) DEFAULT NULL,
  `Typeautorite_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `syg_membres_groupe`
--

INSERT INTO `syg_membres_groupe` (`id`, `adresse`, `email`, `mobile`, `nom`, `prenom`, `telephone`, `autorite_contra`, `groupe_id`, `Typeautorite_id`) VALUES
(1, 'sss dddd', 'sss@hotmail.com', 'ss', 'Diop', 'Awa', 'ss', 96, 1, 1),
(2, 'qqqqqq', 'sss@ssi.sn', 'ss', 'Seck', 'Ibou', 'ss', 413, 1, 1),
(3, 'sss', 'qq@ssi.sn', 'qq', 'Diop', 'Alla', 'qq', 914, 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `syg_modepassation`
--

CREATE TABLE IF NOT EXISTS `syg_modepassation` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(50) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- Contenu de la table `syg_modepassation`
--

INSERT INTO `syg_modepassation` (`ID`, `CODE`, `DESCRIPTION`, `LIBELLE`) VALUES
(1, 'AOO', 'Appel d''Offres Ouvert', 'Appel d''Offres Ouvert'),
(2, 'AOOP', 'Appel d''Offres Ouvert avec pr� qualification', 'Appel d''Offres Ouvert avec pr� qualification'),
(3, 'AOODE', 'Appel d''Offres Ouvert en deux etapes', 'Appel d''Offres Ouvert en deux etapes'),
(4, 'AOR', 'Appel d''Offres Restreint', 'Appel d''Offres Restreint'),
(5, 'MED', 'March� par Entente Directe', 'March� par Entente Directe'),
(34, 'DRP', 'Demande de Renseignement et de Prix', 'Demande de Renseignement et de Prix'),
(35, 'SFQC', '', 'SFQC'),
(36, 'AOI', 'Appel d''Offre Internationale', 'Appel d''Offre Internationale'),
(37, 'AON', 'Appel d''Offre Nationale', 'Appel d''Offre Nationale'),
(38, 'SMC', '', 'SMC'),
(39, 'SMC', '', 'SMC'),
(40, 'SQBC', NULL, 'SQBC'),
(41, 'Avenant', 'Avenant', 'Avenant'),
(42, 'APMI', 'Appel public �  manifestation d''int�ret', 'Appel public �  manifestation d''int�ret'),
(43, 'AOOPU', 'Appel d''offres ouvert en proc�dure d''urgence', 'Appel d''offres ouvert en proc�dure d''urgence'),
(44, 'AC', 'Appel � la concurrence', 'Appel �   la concurrence'),
(45, 'CI', NULL, 'CI'),
(46, 'Cotation', NULL, 'Cotation'),
(47, 'AOUEMOA', NULL, 'Appel d''offres UEMOA'),
(48, 'CCVS', NULL, 'Comparaison d''au moins 5 cvs'),
(50, 'AMI', 'Avis � Manifestation d''int�ret', 'Avis � Manifestation d''int�ret'),
(52, 'xx', NULL, 'Demande de Renseignement de Prix / Appel d''Offre Ouvert'),
(53, 'ss', NULL, 'SQC'),
(54, 'sss', '', 'Consultation restreinte'),
(55, 'AVN', 'Avenant', 'Avenant');

-- --------------------------------------------------------

--
-- Structure de la table `syg_modeselection`
--

CREATE TABLE IF NOT EXISTS `syg_modeselection` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(50) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `syg_modeselection`
--

INSERT INTO `syg_modeselection` (`ID`, `CODE`, `DESCRIPTION`, `LIBELLE`) VALUES
(1, 'QT', 'Qualit� Technique Cout', 'Qualit� Technique Cout'),
(2, 'MC', 'Moindre cout', 'Moindre cout');

-- --------------------------------------------------------

--
-- Structure de la table `syg_module`
--

CREATE TABLE IF NOT EXISTS `syg_module` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(255) DEFAULT NULL,
  `COMMENTAIRE` varchar(255) DEFAULT NULL,
  `LIBELLE` varchar(200) DEFAULT NULL,
  `CIBLE` varchar(255) DEFAULT NULL,
  `OBJECT` varchar(255) DEFAULT NULL,
  `PREREKI` varchar(255) DEFAULT NULL,
  `PROG` varchar(255) DEFAULT NULL,
  `VHORAIRE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `syg_module`
--

INSERT INTO `syg_module` (`ID`, `CODE`, `COMMENTAIRE`, `LIBELLE`, `CIBLE`, `OBJECT`, `PREREKI`, `PROG`, `VHORAIRE`) VALUES
(1, '36598', 'ddddd', 'Textes r�glementaires sur les march�s publics', 'cadre', 'eeee', 'eeee', '09-01-2013_Pj_manuel.pdf', 30),
(2, 'DTMT', 'wwwwwww', 'Dossiers types march�s de travaux', 'Membres commissions des march�s', 'wwwwwww', 'wwwwwwww', '11-05-2013_Pj_recu.pdf', 12);

-- --------------------------------------------------------

--
-- Structure de la table `syg_mois`
--

CREATE TABLE IF NOT EXISTS `syg_mois` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(25) NOT NULL,
  `LIBELLE` varchar(200) NOT NULL,
  `ORD` int(11) DEFAULT NULL,
  `annee` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Contenu de la table `syg_mois`
--

INSERT INTO `syg_mois` (`ID`, `CODE`, `LIBELLE`, `ORD`, `annee`) VALUES
(13, 'J', 'Janvier', 1, 4),
(14, 'F', 'F�vrier', 2, 4),
(15, 'M', 'Mars', 3, 4),
(16, 'A', 'Avril', 4, 4),
(17, 'M', 'Mai', 5, 4),
(18, 'J', 'Juin', 6, 4),
(19, 'Jt', 'Juillet', 7, 4),
(20, 'A', 'Aout', 8, 4),
(21, 'S', 'Septembre', 9, 4),
(22, 'O', 'Octobre', 10, 4),
(23, 'N', 'Novembre', 11, 4),
(24, 'D', 'D�cembre', 12, 4);

-- --------------------------------------------------------

--
-- Structure de la table `syg_noeud_classement`
--

CREATE TABLE IF NOT EXISTS `syg_noeud_classement` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `libelleNoeud` varchar(255) DEFAULT NULL,
  `NoeudClassement_CODE` bigint(20) DEFAULT NULL,
  `TypeElement_CODE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7B7095F5CE805A92` (`TypeElement_CODE`),
  KEY `FK7B7095F555868AE6` (`NoeudClassement_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_noeud_classement`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_participants_formation`
--

CREATE TABLE IF NOT EXISTS `syg_participants_formation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adresse` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `nom` varchar(100) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `formation` bigint(20) NOT NULL,
  `autorite_contra` bigint(20) DEFAULT NULL,
  `groupe_id` int(11) DEFAULT NULL,
  `Typeautorite_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Contenu de la table `syg_participants_formation`
--

INSERT INTO `syg_participants_formation` (`id`, `adresse`, `email`, `mobile`, `nom`, `prenom`, `telephone`, `formation`, `autorite_contra`, `groupe_id`, `Typeautorite_id`) VALUES
(2, 'ss', 'ss@ssi.sn', 'ss', 'ss', 'ss', 'ss', 1, 902, 1, 1),
(3, 'sss', 'fff@yahoo.fr', 'ff', 'f', 'ff', 'fff', 1, 413, 1, 1),
(4, 'ddd', 'ddd@yaoo.fr', 'dd', 'ddd', 'dd', 'dd', 1, 96, 1, 1),
(7, '333', 'ss@yahoo.fr', 'ss', 'ss xxx', 'ss xxx', 'ss', 2, 916, 1, 2),
(8, 'ddd', 'ddd@ssi.sn', 'dd', 'dd', 'ddd', 'dd', 2, 916, 1, 2),
(9, 'wwwwww', 'wwwww@ssi.sn', 'wwwwwwww', 'wwwwwww', 'wwwwww', 'ww', 2, 916, 1, 2),
(10, 'aaaa', 'ss@yahoo.fr', 'ss', 'ss', 'ss', 'sss', 2, 916, 1, 2),
(12, 'sss', 'ss@ssi.sn', 'ss', 'ss', 'sss', 'sss', 1, 922, 1, 3),
(25, 'sss dddd', 'sss@hotmail.com', 'ss', 'ss', 'ss', 'ss', 3, 96, 1, 1),
(26, 'qqqqqq', 'sss@ssi.sn', 'ss', 'ss', 'ss', 'ss', 3, 413, 1, 1),
(27, 'sss', 'qq@ssi.sn', 'qq', 'qq', 'qq', 'qq', 3, 914, 2, 2),
(38, 'sss', 'qq@ssi.sn', 'qq', 'Diop', 'Alla', 'qq', 14, 914, 2, 2),
(39, 'sss', 'qq@ssi.sn', 'qq', 'Diop', 'Alla', 'qq', 13, 914, 2, 2),
(40, 'qqqqqq', 'sss@ssi.sn', 'ss', 'Seck', 'Ibou', 'ss', 13, 413, 1, 1),
(41, 'sss dddd', 'sss@hotmail.com', 'ss', 'Diop', 'Awa', 'ss', 13, 96, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `syg_pays`
--

CREATE TABLE IF NOT EXISTS `syg_pays` (
  `idpays` bigint(20) NOT NULL AUTO_INCREMENT,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `Codepays` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idpays`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=216 ;

--
-- Contenu de la table `syg_pays`
--

INSERT INTO `syg_pays` (`idpays`, `LIBELLE`, `Codepays`) VALUES
(33, 'Burkina Faso', 'BF'),
(42, 'C�te d�Ivoire', 'CI'),
(62, 'Mali', 'ML'),
(98, 'Guin�e Bissau', 'GW'),
(141, 'Niger', 'NE'),
(170, 'S�n�gal', 'SN'),
(193, 'Togo', 'TG'),
(210, 'France', 'FR'),
(215, 'Benin', 'BJ');

-- --------------------------------------------------------

--
-- Structure de la table `syg_piecerecu`
--

CREATE TABLE IF NOT EXISTS `syg_piecerecu` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `GARANTIE` int(11) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `syg_piecerecu`
--

INSERT INTO `syg_piecerecu` (`ID`, `DESCRIPTION`, `GARANTIE`, `LIBELLE`) VALUES
(1, 'Garantie de soumission', 1, 'Garantie de soumission'),
(2, 'Garantie de  bonne fin', 1, 'Garantie de  bonne fin'),
(3, 'Garantie d''avance', 1, 'Garantie d''avance'),
(5, 'PPM ', 0, 'PPM '),
(6, 'AAO', 0, 'AAO');

-- --------------------------------------------------------

--
-- Structure de la table `syg_poledcmp`
--

CREATE TABLE IF NOT EXISTS `syg_poledcmp` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LIBELLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `syg_poledcmp`
--

INSERT INTO `syg_poledcmp` (`ID`, `LIBELLE`) VALUES
(1, 'Test');

-- --------------------------------------------------------

--
-- Structure de la table `syg_proformation`
--

CREATE TABLE IF NOT EXISTS `syg_proformation` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Budget` bigint(20) DEFAULT NULL,
  `effectif` bigint(20) DEFAULT NULL,
  `forCible` varchar(255) DEFAULT NULL,
  `forCommentaire` varchar(255) DEFAULT NULL,
  `forDateDebut` date DEFAULT NULL,
  `forDateFin` date DEFAULT NULL,
  `forLibelle` varchar(255) DEFAULT NULL,
  `forLieu` varchar(255) DEFAULT NULL,
  `forRef` varchar(255) DEFAULT NULL,
  `Autorite_ID` bigint(20) DEFAULT NULL,
  `Formateur` int(11) DEFAULT NULL,
  `Module` int(11) DEFAULT NULL,
  `Autorite_ID_TYPE` bigint(20) DEFAULT NULL,
  `Anne` varchar(255) DEFAULT NULL,
  `mois` varchar(255) DEFAULT NULL,
  `annee_id` int(11) DEFAULT NULL,
  `mois_id` int(11) DEFAULT NULL,
  `DateFin_reel` date DEFAULT NULL,
  `DateDebu_reel` date DEFAULT NULL,
  `Lieu_reel` varchar(255) DEFAULT NULL,
  `Coordonnateur_reel_id` int(11) DEFAULT NULL,
  `coordonnateur_id` int(11) DEFAULT NULL,
  `Formateur_reel_id` int(11) DEFAULT NULL,
  `groupe_id` int(11) DEFAULT NULL,
  `logisticien_id` int(11) DEFAULT NULL,
  `Logiticien_reel_id` int(11) DEFAULT NULL,
  `publie` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Contenu de la table `syg_proformation`
--

INSERT INTO `syg_proformation` (`ID`, `Budget`, `effectif`, `forCible`, `forCommentaire`, `forDateDebut`, `forDateFin`, `forLibelle`, `forLieu`, `forRef`, `Autorite_ID`, `Formateur`, `Module`, `Autorite_ID_TYPE`, `Anne`, `mois`, `annee_id`, `mois_id`, `DateFin_reel`, `DateDebu_reel`, `Lieu_reel`, `Coordonnateur_reel_id`, `coordonnateur_id`, `Formateur_reel_id`, `groupe_id`, `logisticien_id`, `Logiticien_reel_id`, `publie`) VALUES
(1, 50000000, 333, 'sss', 'ddd', '2013-05-02', '2013-05-02', 'sss', 'sss', 'sss', 413, NULL, NULL, 1, NULL, NULL, 4, 13, '2013-05-02', '2013-05-11', 'ddd', 1, NULL, 1, 1, NULL, 1, 1),
(2, 400000, 2, 'ww', 'ddd', '2013-05-02', '2013-05-02', 'ww', 'www', 'www', 902, 1, 1, 1, NULL, NULL, 4, 13, '2013-05-02', '2013-05-05', 'ddd', 1, 1, 1, 1, 1, 1, 1),
(3, 3000000, 5, 'ww', 'wwwww', '2013-05-11', '2013-05-31', 'ww', 'www', 'wwwww', NULL, 1, 1, NULL, NULL, NULL, 4, 13, '2013-05-11', '2013-05-13', 'wwww', NULL, 1, NULL, 1, 1, NULL, 1),
(12, 23, 2, 'sss', 'ddd', '2013-05-13', '2013-05-13', 'sss', 'ss', 's', NULL, 1, 2, NULL, NULL, NULL, 4, 15, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, NULL, 0),
(13, 400000000, 5, 'ss', 'ddd', '2013-05-13', '2013-05-14', 'ss', 'ss', 'ss', NULL, 1, 2, NULL, NULL, NULL, 4, 14, '2013-05-13', '2013-05-13', 'dd', 1, 1, 1, NULL, 1, 1, 1),
(14, 3000000, 30, 'Membres de commission des march�s', '', '2013-05-22', '2013-05-26', 'Pr�paration des dossiers de DP', 'Lom�', '6789', NULL, 1, 2, NULL, NULL, NULL, 4, 17, '2013-05-21', '2013-05-22', 'Lom�', NULL, 1, NULL, NULL, 1, NULL, 1),
(15, 2500000, 20, 'Informaticiens', '', '2013-05-13', '2013-05-17', 'Formation sur le sigmap', 'Lom�', '56T', NULL, 1, 1, NULL, NULL, NULL, 4, 17, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `syg_projet_reponse`
--

CREATE TABLE IF NOT EXISTS `syg_projet_reponse` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amplitaire` varchar(255) DEFAULT NULL,
  `courrierautre` varchar(100) DEFAULT NULL,
  `datecourrier` date DEFAULT NULL,
  `datereception` date DEFAULT NULL,
  `datesaisie` date DEFAULT NULL,
  `objetcourrier` varchar(200) DEFAULT NULL,
  `courrierRef` varchar(255) DEFAULT NULL,
  `file` varchar(50) DEFAULT NULL,
  `isSignedVersion` bit(1) DEFAULT NULL,
  `isLastVersion` bit(1) DEFAULT NULL,
  `autoritecontractante` bigint(20) DEFAULT NULL,
  `dossierCourrier_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_projet_reponse`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_rapport_formation`
--

CREATE TABLE IF NOT EXISTS `syg_rapport_formation` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DateRapport` date DEFAULT NULL,
  `DatePublication` date DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Nomfichier` varchar(255) DEFAULT NULL,
  `Libelle` varchar(255) DEFAULT NULL,
  `Publier` varchar(10) DEFAULT NULL,
  `formation` bigint(20) NOT NULL,
  `type_rapport` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `syg_rapport_formation`
--

INSERT INTO `syg_rapport_formation` (`ID`, `DateRapport`, `DatePublication`, `Description`, `Nomfichier`, `Libelle`, `Publier`, `formation`, `type_rapport`) VALUES
(1, '2013-05-02', NULL, 'ffff', '2847802052013_Pj_adresse.rtf', 'ddd', 'no', 1, 1),
(2, '2013-05-08', NULL, '', '4026508052013_Pj_formcourrier.docx', 'ddd', 'no', 1, 1),
(3, '2013-05-11', NULL, '', '40481011052013_Pj_recu.pdf', 'xxx', 'no', 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `syg_secteuractivites`
--

CREATE TABLE IF NOT EXISTS `syg_secteuractivites` (
  `CODE` varchar(20) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `SECTEUR` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`CODE`),
  KEY `FK67B51CD79B15DAFD` (`SECTEUR`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `syg_secteuractivites`
--

INSERT INTO `syg_secteuractivites` (`CODE`, `DESCRIPTION`, `LIBELLE`, `SECTEUR`) VALUES
('001', 'Agriculture & Produits Alimentaires', 'Agriculture & Produits Alimentaires', NULL),
('002', 'Prestation de services', 'Prestation de services', NULL),
('002001', NULL, 'Services de réparation, d''entretien et d''installation ', '002'),
('002002', NULL, 'Services de réparation, d''entretien et d''installation ', '002'),
('002003', NULL, 'Service informatique', '002'),
('002004', NULL, 'Service de nettoiement', '002'),
('002005', NULL, 'Service de gardiennage', '002'),
('002006', NULL, 'Autres services', '002'),
('002006001', NULL, 'Service décoration', '002006'),
('003', 'Fournitures courantes', 'Fournitures courantes', NULL),
('003001', NULL, 'Matériel informatique et fournitures de bureau', '003'),
('003002', NULL, 'Matériel roulant', '003'),
('003003', NULL, 'Matériel de bureau', '003'),
('003004', NULL, 'Denrées alimentaires', '003'),
('003005', NULL, 'Effets vestimentaires', '003'),
('003006', 'Fourniture de bureau', 'Fourniture de bureau', '003'),
('003007', NULL, 'Développement de logiciels', '003');

-- --------------------------------------------------------

--
-- Structure de la table `syg_service`
--

CREATE TABLE IF NOT EXISTS `syg_service` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODIFICATION` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `AUTORITE` bigint(20) DEFAULT NULL,
  `TYPESERVICE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_service`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_specialite`
--

CREATE TABLE IF NOT EXISTS `syg_specialite` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(50) DEFAULT NULL,
  `DESCIPTION` varchar(50) DEFAULT NULL,
  `LIBELLE` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `syg_specialite`
--

INSERT INTO `syg_specialite` (`ID`, `CODE`, `DESCIPTION`, `LIBELLE`) VALUES
(1, '9856', 'Developeur', 'Developeur'),
(2, '65324', 'administrateur réseau', 'administrateur réseau'),
(3, 's', 'sss', 'ss'),
(4, 'wwwww', 'wwwww', 'wwwww');

-- --------------------------------------------------------

--
-- Structure de la table `syg_tachesafaire`
--

CREATE TABLE IF NOT EXISTS `syg_tachesafaire` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `dateauplutard` date DEFAULT NULL,
  `dateevaluation` date DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `traitepar` varchar(255) DEFAULT NULL,
  `TYPE` bigint(20) DEFAULT NULL,
  `USERS` bigint(20) DEFAULT NULL,
  `fait` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK3606627EF2E5DFB0` (`USERS`),
  KEY `FK3606627E10FC430D` (`TYPE`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `syg_tachesafaire`
--

INSERT INTO `syg_tachesafaire` (`ID`, `date`, `dateauplutard`, `dateevaluation`, `LIBELLE`, `traitepar`, `TYPE`, `USERS`, `fait`) VALUES
(1, '2013-04-29', NULL, NULL, 'Demande Immatriculation du march�: T_DAGE_0001', 'DCMP', NULL, 17, 0),
(2, '2013-05-04', NULL, NULL, 'Demande Immatriculation du march�: T_DAGE_0002', 'DCMP', NULL, 17, 0),
(3, '2013-05-04', NULL, NULL, 'Demande Immatriculation du march�: PI_DAGE_0013', 'DCMP', NULL, 17, 0),
(4, '2013-05-05', NULL, NULL, 'Demande Immatriculation du march�: PI_DAGE_0014', 'DCMP', NULL, 17, 0),
(5, '2013-05-05', NULL, NULL, 'Demande Immatriculation du march�: F_DAGE_0015', 'DCMP', NULL, 17, 0),
(6, '2013-05-05', NULL, NULL, 'Demande Immatriculation du march�: F_DAGE_0016', 'DCMP', NULL, 17, 0),
(7, '2013-05-05', NULL, NULL, 'Demande Immatriculation du march�: S_DAGE_0019', 'DCMP', NULL, 17, 0),
(8, '2013-05-05', NULL, NULL, 'Demande Immatriculation du march�: S_DAGE_0020', 'DCMP', NULL, 17, 0),
(9, '2013-05-08', NULL, NULL, 'Demande Immatriculation du march�: T_DAGE_0047', 'DCMP', NULL, 18, 0),
(10, '2013-05-09', NULL, NULL, 'Demande Immatriculation du march�: T_DNCMP_0038', 'DCMP', NULL, 19, 0);

-- --------------------------------------------------------

--
-- Structure de la table `syg_tdossiercourrier`
--

CREATE TABLE IF NOT EXISTS `syg_tdossiercourrier` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(25) DEFAULT NULL,
  `DATESTATUT` date DEFAULT NULL,
  `DATETARNSMIS` date DEFAULT NULL,
  `DESCRIPT` varchar(255) DEFAULT NULL,
  `DESTINATAIRE` bigint(20) DEFAULT NULL,
  `INSTRUCTIONS` longtext,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `STATUT` varchar(255) DEFAULT NULL,
  `COURRIER` bigint(20) DEFAULT NULL,
  `EXPEDITEUR` bigint(20) DEFAULT NULL,
  `STATE` varchar(200) DEFAULT NULL,
  `taccusereception` bit(1) DEFAULT NULL,
  `tnotifmail` bit(1) DEFAULT NULL,
  `DATEMAXTRAIT` date DEFAULT NULL,
  `tbureau` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`),
  KEY `FK84E3D742CF488463` (`EXPEDITEUR`),
  KEY `FK84E3D742B12707C1` (`STATE`),
  KEY `FK84E3D7428F53DE7F` (`COURRIER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_tdossiercourrier`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_typeautoritecontractante`
--

CREATE TABLE IF NOT EXISTS `syg_typeautoritecontractante` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(10) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `ORDRE` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `syg_typeautoritecontractante`
--

INSERT INTO `syg_typeautoritecontractante` (`ID`, `CODE`, `DESCRIPTION`, `LIBELLE`, `ORDRE`) VALUES
(1, 'MIN', 'Administration publique', 'Administration publique', 1),
(2, 'SCE', 'Soci�t�s d''Etat', 'Soci�t�s d''Etat', 2),
(3, 'EPICL', 'Etablissement publics\n\n', 'Etablissement publics', 3),
(5, 'INS', 'Institutions\n\n', 'Institutions', 4),
(6, 'CL', 'Collectivit�s locales', 'Collectivit�s locales', 5);

-- --------------------------------------------------------

--
-- Structure de la table `syg_typecategori`
--

CREATE TABLE IF NOT EXISTS `syg_typecategori` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `NIVEAU` int(11) DEFAULT NULL,
  `autretypeCategori` bigint(20) DEFAULT NULL,
  `typeCategori` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK72AD4306383A8E2A` (`typeCategori`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `syg_typecategori`
--

INSERT INTO `syg_typecategori` (`ID`, `LIBELLE`, `NIVEAU`, `autretypeCategori`, `typeCategori`) VALUES
(1, 'Categorie', 1, NULL, 1),
(2, 'Services', 2, NULL, 2),
(3, 'Autres Services', 3, NULL, 3);

-- --------------------------------------------------------

--
-- Structure de la table `syg_typeelementarbre`
--

CREATE TABLE IF NOT EXISTS `syg_typeelementarbre` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  `NIVEAU` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_typeelementarbre`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_typerapport`
--

CREATE TABLE IF NOT EXISTS `syg_typerapport` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Code` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Libelle` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `syg_typerapport`
--

INSERT INTO `syg_typerapport` (`ID`, `Code`, `Description`, `Libelle`) VALUES
(1, 'xx', 'xx', 'xx');

-- --------------------------------------------------------

--
-- Structure de la table `syg_typeservice`
--

CREATE TABLE IF NOT EXISTS `syg_typeservice` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LIBELLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `syg_typeservice`
--

INSERT INTO `syg_typeservice` (`ID`, `LIBELLE`) VALUES
(1, 'Service ARMP'),
(2, 'Service DCMP');

-- --------------------------------------------------------

--
-- Structure de la table `syg_typesformateur`
--

CREATE TABLE IF NOT EXISTS `syg_typesformateur` (
  `IDFORM` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODEFORM` varchar(50) DEFAULT NULL,
  `DESCRIPTIONFORM` varchar(255) DEFAULT NULL,
  `LIBELLEFORM` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IDFORM`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `syg_typesformateur`
--

INSERT INTO `syg_typesformateur` (`IDFORM`, `CODEFORM`, `DESCRIPTIONFORM`, `LIBELLEFORM`) VALUES
(1, 'FI', 'Consultant physique', 'Formateur individuel'),
(2, 'FC', 'Formateur Cabinet', 'Formateur Cabinet');

-- --------------------------------------------------------

--
-- Structure de la table `syg_typesmarches`
--

CREATE TABLE IF NOT EXISTS `syg_typesmarches` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(50) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `syg_typesmarches`
--

INSERT INTO `syg_typesmarches` (`ID`, `CODE`, `DESCRIPTION`, `LIBELLE`) VALUES
(1, 'F', 'FOURNITURES', 'FOURNITURES'),
(2, 'S', 'SERVICES', 'SERVICES'),
(3, 'T', 'TRAVAUX', 'TRAVAUX'),
(4, 'PI', 'PRESTATIONS INTELLECTUELLES', 'PRESTATIONS INTELLECTUELLES'),
(5, 'DSP', 'D�l�gation de service publics', 'D�l�gation de service publics');

-- --------------------------------------------------------

--
-- Structure de la table `syg_typesmarches_modespassations`
--

CREATE TABLE IF NOT EXISTS `syg_typesmarches_modespassations` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `MODE` bigint(20) DEFAULT NULL,
  `TYPE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_typesmarches_modespassations`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_typesmarches_modesselections`
--

CREATE TABLE IF NOT EXISTS `syg_typesmarches_modesselections` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `MODE` bigint(20) DEFAULT NULL,
  `TYPE` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_typesmarches_modesselections`
--


-- --------------------------------------------------------

--
-- Structure de la table `syg_typestaches`
--

CREATE TABLE IF NOT EXISTS `syg_typestaches` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `duree` int(11) DEFAULT NULL,
  `LIBELLE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `syg_typestaches`
--


-- --------------------------------------------------------

--
-- Structure de la table `sys_action`
--

CREATE TABLE IF NOT EXISTS `sys_action` (
  `ACT_CODE` varchar(50) NOT NULL,
  `ACT_APPLICATIONSTATE` varchar(200) NOT NULL,
  `ACT_DESC` longtext NOT NULL,
  `ACT_IMAGE` varchar(200) NOT NULL,
  `ACT_JOURNAL` smallint(6) NOT NULL,
  `ACT_LIBELLE` varchar(200) NOT NULL,
  `FEA_CODE` varchar(50) NOT NULL,
  PRIMARY KEY (`ACT_CODE`),
  UNIQUE KEY `ACT_CODE` (`ACT_CODE`),
  KEY `FKC81CEBE8BE8316EA` (`FEA_CODE`),
  KEY `FKC81CEBE8DC54D1E9` (`FEA_CODE`),
  KEY `FKC81CEBE8FDAD492E` (`FEA_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `sys_action`
--

INSERT INTO `sys_action` (`ACT_CODE`, `ACT_APPLICATIONSTATE`, `ACT_DESC`, `ACT_IMAGE`, `ACT_JOURNAL`, `ACT_LIBELLE`, `FEA_CODE`) VALUES
(' MOD_PIECES', '', '', 'disk.png', 1, 'Modifier', 'PIECES'),
('ADD_AC', '', '', 'add.png', 1, 'Ajouter', 'AUTORITEAC'),
('ADD_ANNUAIREARMP', '', '', 'add.png', 1, 'Ajouter', 'ANNUAIREARMP'),
('ADD_ANNUAIREDCMP', '', '', 'add.png', 1, 'Ajouter', 'ANNUAIREDCMP'),
('ADD_ARRETES', '', 'Ajouter', 'add.png', 1, 'Ajouter', 'ARRETES'),
('ADD_AUDIT', '', '', 'add.png', 1, 'Ajouter', 'AUDITS'),
('ADD_AUTORITE', '', '', 'zoom.png', 1, 'GROUPES', 'FEA_FORMATION'),
('ADD_AUTORITEAUTC', '', '', 'add.png', 1, 'Ajouter', 'AUTORITEAUTC'),
('ADD_AVIGENERAL', '  ', 'Ajouter', 'add.png', 1, 'Ajouter', 'AVIGENERAL'),
('ADD_BAILLEUR', '', '', 'add.png', 1, 'Ajouter', 'BAILLEURS'),
('ADD_BANQUE', '', '', 'add.png', 1, 'Ajouter', 'BANQUE'),
('ADD_BREALISATIONS', '', '', 'add.png', 1, 'Ajouter', 'BREALISATIONS'),
('ADD_BUREAUXDCMP', '', '', 'add.png', 1, 'Ajouter', 'BUREAUXDCMP'),
('ADD_CAT', 'Ajouter', '', 'add.png', 1, 'Ajouter', 'CATEGORIE'),
('ADD_CATEGORIE', '', '', 'add.png', 1, 'Ajouter', 'GESTION_CATEGORIE'),
('ADD_CATEGORIES', '', '', 'add.png', 1, 'Ajouter', 'CATEGORIES'),
('ADD_CODE_MARCHE', '', '', 'add.png', 1, 'Ajouter', 'CODE_MARCHE'),
('ADD_COMMISSIONSMARCHES', '', '', 'add.png', 1, 'Ajouter', 'COMMISSIONSMARCHES'),
('ADD_CONFTYPESDOSSIERS', ' ', 'Ajouter', 'add.png', 1, 'Ajouter', 'CONFTYPESDOSSIERS'),
('ADD_CONTENTIEUX', '', '', 'add.png', 1, 'Ajouter', 'CONTENTIEUX'),
('ADD_CONT_DEC', '', 'Ajouter', 'add.png', 1, 'Ajouter', 'REF_CONT_DECISION'),
('ADD_COORDINNATEUR', '', '', 'add.png', 1, 'Ajouter', 'COORDONNATEUR'),
('ADD_COURRIERAC', '', 'Ajouter', 'add.png', 1, 'Ajouter', 'FEA_COURRIERAC'),
('ADD_COURRIERACDOS', 'add', 'Ajouter', 'add.png', 1, 'Ajouter', 'FEA_COURRIERACDOS'),
('ADD_COURRIERDEPART', '', 'Ajouter', 'add.png', 1, 'Ajouter', 'COURRIERDEPART'),
('ADD_CRITERE', '', '', 'add.png', 1, 'Ajouter', 'GESTION_CRITERE'),
('ADD_CRITEREANA', ' ', 'Ajouter', 'add.png', 1, 'Ajouter', 'CRITEREDANALYSE'),
('ADD_DECISION', '', '', 'add.png', 1, 'Ajouter', 'DECISION'),
('ADD_DELAIS', '', '', 'add.png', 1, 'Ajouter', 'DELAIS'),
('ADD_DEMANDE_PAIEMENT', '', '', 'add.png', 1, 'Ajouter', 'DEMANDE_PAIEMENT'),
('ADD_DENONCIATION', 'Denonciation', 'Ajouter', 'add.png', 1, 'Ajouter', 'Denonciation'),
('ADD_DENONCIATIONC', ' ', 'Ajouter', 'add.png', 1, 'Ajouter', 'DENONCIATIONC'),
('ADD_DENONCIATIONN', ' ', 'Ajouter', 'add.png', 1, 'Ajouter', 'DENONCIATIONN'),
('ADD_DIRECTIONSERVICE', '', '', 'add.png', 1, 'Ajouter', 'DIRECTIONSERVICE'),
('ADD_DOCUMENT', ' ', 'Ajouter', 'add.png', 1, 'Ajouter', 'CATEGORIEDOCUMENT'),
('ADD_DOSSIERAC', 'add_dossier', 'Nouveau Dossier', 'add.png', 1, 'Nouveau dossier', 'DOSSIERAC'),
('ADD_DOSSIERTYPE', ' ', 'Ajouter', 'add.png', 1, 'Ajouter', 'DOSSIERSTYPE'),
('ADD_DPROCEDURESDEROGATOIRES', '  ', 'Ajouter', 'add.png', 1, 'Ajouter', 'DPROCEDURESDEROGATOIRES'),
('ADD_EVA', '', '', 'add.png', 1, 'Ajouter', 'GESTION_EVAL'),
('ADD_FAMILLES', '    ', 'Ajouter', 'add.png', 1, 'Ajouter', 'FAMILLES'),
('ADD_FEA_MODELE_ARC', '', '', 'add.png', 1, 'Ajouter', 'FEA_MODELE_ARC'),
('ADD_FICHIER_LEGISLATION', '', '', 'add.png', 1, 'Ajouter', 'FICHIER_LEGISLATION'),
('ADD_FONCTIONS', '', '', 'add.png', 1, 'Ajouter', 'FONCTIONS'),
('ADD_FORM', '', '', 'add.png', 1, 'Ajouter', 'FEA_FORMATION'),
('ADD_FORMATEUR', '', '', 'add.png', 1, 'Ajouter', 'FORMATEUR'),
('ADD_FOURNISSEUR', '', '', 'add.png', 1, 'Ajouter', 'FOURNISSEURS'),
('ADD_GARANTIS', '', '', 'add.png', 1, 'Ajouter', 'GARANTIS'),
('ADD_GESTIONNOEUDARCHIVE', '', 'Ajouter', 'add.png', 1, 'Ajouter', 'GESTIONNOEUDARCHIVE'),
('ADD_GLOSSAIRE', '', '', 'add.png', 1, 'Ajouter', 'REF_GLOSSAIRE'),
('ADD_GRILLESANALYSES', '  ', 'Ajouter', 'add.png', 1, 'Ajouter', 'GRILLESANALYSES'),
('ADD_GROUPE', '', '', 'add.png', 1, 'Ajouter', 'GROUPE'),
('ADD_GROUPEFORM', '', '', 'add.png', 1, 'Ajouter', 'GROUPEFORMATION'),
('ADD_JOURNAL', '', '', 'add.png', 1, 'Ajouter', 'REF_JOURNAL'),
('ADD_JOURSFERIES', '', '', 'add.png', 1, 'Ajouter', 'JOURSFERIES'),
('ADD_LISTE_ROUGE_F', '', 'Ajouter', 'add.png', 1, 'Ajouter', 'FEA_LISTE_ROUGE_FOURNISSEUR'),
('ADD_LOGISTICIEN', '', '', 'add.png', 1, 'Ajouter', 'LOGISTICIEN'),
('ADD_MEMBRESGROUPE', '', '', 'zoom.png', 1, 'Membres', 'GROUPEFORMATION'),
('ADD_MODELEDOCUMENT', '   ', 'Ajouter', 'add.png', 1, 'Ajouter', 'MODELEDOCUMENT'),
('ADD_MODEPASSATION', '', '', 'add.png', 1, 'Ajouter', 'MODEPASSATION'),
('ADD_MODESELECTION', '', '', 'add.png', 1, 'Ajouter', 'MODESELECTION'),
('ADD_MODULE', '', '', 'add.png', 1, 'Ajouter', 'MODULE'),
('ADD_MONNAI', '', '', 'add.png', 1, 'Ajouter', 'MONNAIE_OFFRE'),
('ADD_MONTANTSSEUILS', '', '', 'add.png', 1, 'Ajouter', 'MONTANTSSEUILS'),
('ADD_NAT', '', '', 'add.png', 1, 'Ajouter', 'NAT_PRIX'),
('ADD_NATURE', '  ', 'Ajouter', 'add.png', 1, 'Ajouter', 'NATURECOURRIER'),
('ADD_NPROCEDUREDEROGATOIRE', ' ', 'Ajouter', 'add.png', 0, 'Ajouter', 'NPROCEDUREDEROGATOIRE'),
('ADD_ORGUNITARMP', '', '', '', 1, '', 'ORGUNITARMP'),
('ADD_PAIEMENT', '', '', 'add.png', 1, 'Enregistrer Paiement', 'SUIVIPAIEMENT'),
('ADD_PARTFORMATION', '', '', 'add.png', 1, 'Ajouter', 'REF_PARTFORMATION'),
('ADD_PAYS', '', '', 'add.png', 1, 'Ajouter', 'PAYS'),
('ADD_PIECES', '', '', 'add.png', 1, 'Ajouter', 'PIECES'),
('ADD_PIECESREQUISES', '  ', 'Ajouter', 'add.png', 1, 'Ajouter', 'PIECESREQUISES'),
('ADD_PJCONT', '', '', 'add.png', 1, 'Ajouter', 'PJCONT'),
('ADD_PJDENONCIATION', '', 'Ajouter', 'add.png', 1, 'Ajouter', 'PJDENONCIATION'),
('ADD_PLANPASSATION', '', '', 'add.png', 1, 'Ajouter', 'PLANPASSATION'),
('ADD_POLEDCMP', '', '', 'add.png', 1, 'Ajouter', 'POLEDCMP'),
('ADD_PRESTATAIRE', '', '', 'add.png', 1, 'Ajouter', 'PRESTATAIRES'),
('ADD_PROCEDUREDEROGATOIRE', '  ', 'Ajouter', 'add.png', 1, 'Ajouter', 'PROCEDUREDEROGATOIRE'),
('ADD_PROCEDURESPASSATIONS', '', '', 'add.png', 1, 'Ajouter', 'PROCEDURESPASSATIONS'),
('ADD_PRODUITS', '', '', 'add.png', 1, 'Ajouter', 'PRODUITS'),
('ADD_PROFIL', '', '', 'add.png', 1, 'Ajouter', 'GESTION_PROFIL'),
('ADD_PROJETREP', 'add', 'Ajouter', 'add.png', 1, 'Ajouter', 'FEA_PROJETREPONSE'),
('ADD_RAPARMP', '', '', 'add.png', 1, 'Ajouter', 'RAPPORTARMP'),
('ADD_RAPDCMP', '', '', 'add.png', 1, 'Ajouter', 'RAPPORTDCMP'),
('ADD_RAPFORMATION', '', 'Ajouter', 'add.png', 1, 'Ajouter', 'RAPPORTFORMATION'),
('ADD_REALSATIONS', '', '', 'add.png', 1, 'Ajouter', 'REALSATIONS'),
('ADD_RECEPTION', ' ', 'Ajouter', 'add.png', 1, 'Ajouter', 'MODERECEPTION'),
('ADD_RECOUVREMENT', '', '', 'add.png', 1, 'Enregistrer Recouvrement', 'SUIVIRECOUVREMENT'),
('ADD_REFCATEGORIES', '  ', 'Ajouter', 'add.png', 1, 'Ajouter', 'REFCATEGORIES'),
('ADD_REFERENTIELPRIX', '  ', 'Ajouter', 'add.png', 1, 'Ajouter', 'REFERENTIELPRIX'),
('ADD_REGCOM', '', '', 'add.png', 1, 'Ajouter', 'REGCOMMUNAUTAIRE'),
('ADD_REGNAT', '', '', 'add.png', 1, 'Ajouter', 'REGNATIONALE'),
('ADD_SECTEURSACTIVITES', '', '', 'add.png', 1, 'Ajouter', 'SECTEURSACTIVITES'),
('ADD_SERVICEAC', '', '', 'add.png', 1, 'Ajouter', 'SERVICEAC'),
('ADD_SEUILSCOMMUNAUTAIRE', '', '', 'add.png', 1, 'Ajouter', 'SEUILSCOMMUNAUTAIRE'),
('ADD_SEUILSRAPRIORI', '', '', 'add.png', 1, 'Ajouter', 'SEUILSRAPRIORI'),
('ADD_SPECIALITE', '', '', 'add.png', 1, 'Ajouter', 'SPECIALITE'),
('ADD_SUIVI', '', '', 'commande.png', 1, ' Suivi contrat', 'FEA_SUIVIMARCHE'),
('ADD_SUIVIDENONCIATION', 'Suivi_Denonciation', 'Ajouter', 'add.png', 1, 'Ajouter', 'SUIVIDENONCIATION'),
('ADD_TAUX_PARAFISCALE', '', '', '', 1, '', 'TAUXPARAFIXCALE'),
('ADD_TAUX_VENTEDAO', '', '', '', 1, '', 'TAUXVENTEDAO'),
('ADD_TEXTE_REGLEMENTAIRE', '', '', 'add.png', 1, 'Ajouter', 'TEXTE_REGLEMENTAIRE'),
('ADD_TRAITEMENT', ' ', 'Ajouter', 'add.png', 1, 'Ajouter', 'MODETRAITEMENT'),
('ADD_TYPEAC', '', '', 'add.png', 1, 'Ajouter', 'TYPEAC'),
('ADD_TYPECOURRIER', '  ', 'Ajouter', 'add.png', 1, 'Ajouter', 'TYPECOURRIER'),
('ADD_TYPEDECISION', '', '', 'add.png', 1, 'Ajouter', 'TYPEDECISION'),
('ADD_TYPEDEMANDE', '  ', 'Ajouter', 'add.png', 1, 'Ajouter', 'REF_TYPEDEMANDE'),
('ADD_TYPEMODELEDOCUMENT', '  ', 'Ajouter', 'add.png', 1, 'Ajouter', 'TYPEMODELEDOCUMENT'),
('ADD_TYPERAP', '', '', 'add.png', 1, 'Ajouter', 'TYPERAPPORT'),
('ADD_TYPESDOSSIERS', '  ', 'Ajouter', 'add.png', 1, 'Ajouter', 'TYPESDOSSIERS'),
('ADD_TYPESERVICE', '', '', 'add.png', 1, 'Ajouter', 'TYPESERVICE'),
('ADD_TYPESMARCHESMP', '', '', 'add.png', 1, 'Ajouter', 'TYPESMARCHESMP'),
('ADD_TYPESMARCHESMS', '', '', 'add.png', 1, 'Ajouter', 'TYPESMARCHESMS'),
('ADD_TYPEUNITEORGARMP', '', '', 'add.png', 1, 'Ajouter', 'TYPEUNITEORGARMP'),
('ADD_TYPEUNITEORGDCMP', '', '', 'add.png', 1, 'Ajouter', 'TYPEUNITEORGDCMP'),
('ADD_UNITEORGARMP', '', '', 'add.png', 1, 'Ajouter', 'UNITEORGARMP'),
('ADD_UTILISATEUR', 'add_utilisateur', 'Ajouter un utilisateur', 'add.png', 1, 'Ajouter', 'UTILISATEURARMP'),
('ADD_UTILISATEURAC', '', '', 'add.png', 1, 'Ajouter', 'UTILISATEURAC'),
('ADD_UTILISATEURDNCMP', '', '', 'add.png', 1, 'Ajouter', 'UTILISATEURDCMP'),
('CONFDATESREAL', '', 'Configuration Dates R�alisations', '', 1, 'Configuration Dates R�alisations', 'CONFDATESREAL'),
('DAC_VOIR', '', '', 'zoom.png', 1, 'Voir', 'DOSSIERAC'),
('DEL_ARCHIVAGEMODELE', 'del_archivemodele', 'Supprimer', 'del.png', 1, 'Supprimer', 'ARCHIVAGEMODELE'),
('DEL_ARCHIVECONFIG', 'del_archiveconfig', 'Supprimer', 'delete.png', 1, 'Supprimer', 'ARCHIVECONFIG'),
('DEL_ARCHIVEGESTION', 'del_archivegestion', 'Supprimer', 'delete.png', 1, 'Supprimer', 'ARCHIVEGESTION'),
('DEL_COORDINNATEUR', '', '', 'delete.png', 1, 'Supprimer', 'COORDONNATEUR'),
('DEL_CRITERE', '', '', 'delete.png', 1, 'Supprimer', 'GESTION_CRITERE'),
('DEL_FEA_MODELE_ARC', '', '', 'cancel.png', 1, 'Supprimer', 'FEA_MODELE_ARC'),
('DEL_FORMATEUR', '', '', 'delete.png', 1, 'Supprimer', 'FORMATEUR'),
('DEL_GESTIONNOEUDARCHIVE', '', 'Supprimer', 'cancel.png', 1, 'Supprimer', 'GESTIONNOEUDARCHIVE'),
('DEL_GLOSSAIRE', '', '', 'delete.png', 1, 'Supprimer', 'REF_GLOSSAIRE'),
('DEL_GROUPEFORM', '', '', 'delete.png', 1, 'Supprimer', 'GROUPEFORMATION'),
('DEL_JOURNAL', '', '', 'delete.png', 1, 'Supprimer', 'REF_JOURNAL'),
('DEL_LOGISTICIEN', '', '', 'delete.png', 1, 'Supprimer', 'LOGISTICIEN'),
('DEL_MODULE', '', '', 'delete.png', 1, 'Supprimer', 'MODULE'),
('DEL_MONNAI', '', '', 'delete.png', 1, 'Supprimer', 'MONNAIE_OFFRE'),
('DEL_PROFIL', '', '', 'delete.png', 1, 'Supprimer', 'GESTION_PROFIL'),
('DEL_SPECIALITE', '', '', 'delete.png', 1, 'Supprimer', 'SPECIALITE'),
('DEL_TYPERAP', '', '', 'delete.png', 1, 'Supprimer', 'TYPERAPPORT'),
('DEL_USER', '', '', 'delete.png', 1, 'Supprimer', 'UTILISATEURARMP'),
('DEL_UTILISATEURAC', '', '', 'delete.png', 0, 'Supprimer', 'UTILISATEURAC'),
('DEL_UTILISATEURDNCMP', '', 'Supprimer', 'delete.png', 1, 'Supprimer', 'UTILISATEURDCMP'),
('DETAIL_FORM', '', '', 'zoom.png', 1, 'Detail', 'FEA_FORMATION'),
('DETAIL_FORMATEUR', '', '', 'zoom.png', 1, 'D�tail', 'FORMATEUR'),
('DOSSIERSPP', '', 'Examen Juridique et Technique', '', 1, 'Examen Juridique et Technique', 'DOSSIERSPP'),
('EDITER_OPDAO', '', '', 'gerer.gif', 1, 'Editer Recouvrement', 'OPERATIONDAO'),
('EDITER_OPERATION', '', '', 'gerer.gif', 1, 'Editer', 'OPERATION'),
('EDITER_QUITUS', '', '', 'gerer.gif', 1, 'Editer Quitus', 'QUITUS'),
('EDIT_CONTRAT', '', '', 'gerer.gif', 1, 'Visualiser les contrats', 'SUIVIPAIEMENT'),
('EDIT_DOSSIER', '', '', 'gerer.gif', 1, 'Dossier �  R�gulariser', 'SUIVIRECOUVREMENT'),
('EXAMENJUR', '', '', '', 1, 'Examen Juridique et Technique', 'EXAMENJUR'),
('GERER_ARCHIVECONFIG', '', 'GÃ©rer plan de classement', '', 1, 'Plan de classement', 'ARCHIVECONFIG'),
('GESTION_CONFAUDIT', 'audit', 'Configurer l''audit', 'zoom.png', 1, 'Configurer l''audit', 'GESTION_CONFAUDIT'),
('GESTION_CONSJRNL', 'journal', 'Consulter le journal', 'zoom.png', 1, 'Consulter le journal', 'GESTION_CONSJRNL'),
('HELP_USER', '', '', 'help.png', 1, 'Aide', 'UTILISATEURARMP'),
('LAST_PROJETREP', 'last', 'Marquer Ã�  Signer', 'certif.png', 1, 'Marquer Ã�  Signer', 'FEA_PROJETREPONSE'),
('MAJPLANPASSATION', '', '', '', 1, 'Mise � jour plan', 'MAJPLANPASSATION'),
('MATDOSSIER', '', 'Demande Immatriculation', '', 1, 'Demande Immatriculation', 'MATDOSSIER'),
('MODEPASSATION', '', '', '', 1, 'Mode de passation', 'MODEPASSATION'),
('MOD_AC', '', '', 'disk.png', 1, 'Modifier', 'AUTORITEAC'),
('MOD_ANNUAIREARMP', '', '', 'disk.png', 1, 'Modifier', 'ANNUAIREARMP'),
('MOD_ANNUAIREDCMP', '', '', 'disk.png', 1, 'Modifier', 'ANNUAIREDCMP'),
('MOD_ARCHIVAGEMODELE', 'mod_archivemodele', 'Modifier', 'disk.png', 1, 'Modifier', 'ARCHIVAGEMODELE'),
('MOD_ARCHIVECONFIG', 'mod_archiveconfig', 'Modifier', 'disk.png', 1, 'Modifier', 'ARCHIVECONFIG'),
('MOD_ARCHIVEGESTION', 'mod_archivegestion', 'Modifier', 'disk.png', 1, 'Modifier', 'ARCHIVEGESTION'),
('MOD_ARRETES', '', 'Modifier', 'disk.png', 1, 'Modifier', 'ARRETES'),
('MOD_AUDIT', '', '', 'disk.png', 1, 'Modifier', 'AUDITS'),
('MOD_AUTORITEAUTC', '', '', 'disk.png', 1, 'Modifier', 'AUTORITEAUTC'),
('MOD_AVIGENERAL', '  ', 'Modifier', 'disk.png', 1, 'Modifier', 'AVIGENERAL'),
('MOD_BAILLEUR', '', '', 'disk.png', 1, 'Modifier', 'BAILLEURS'),
('MOD_BANQUE', '', '', 'disk.png', 1, 'Modifier', 'BANQUE'),
('MOD_BUREAUXDCMP', '', '', 'disk.png', 1, 'Modifier', 'BUREAUXDCMP'),
('MOD_CAT', 'Mod', '', 'disk.png', 1, 'Modifier', 'CATEGORIE'),
('MOD_CATEGORIE', '', '', 'disk.png', 1, 'Modifier', 'GESTION_CATEGORIE'),
('MOD_CATEGORIES', '', '', 'disk.png', 1, 'Modifier', 'CATEGORIES'),
('MOD_CODE_MARCHE', '', '', 'disk.png', 1, 'Modifier', 'CODE_MARCHE'),
('MOD_COMMISSIONSMARCHES', '', '', 'disk.png', 1, 'Modifier', 'COMMISSIONSMARCHES'),
('MOD_CONFTYPESDOSSIERS', '  ', 'Modifier', 'disk.png', 1, 'Modifier', 'CONFTYPESDOSSIERS'),
('MOD_CONTENTIEUX', '', '', 'disk.png', 1, 'Modifier', 'CONTENTIEUX'),
('MOD_CONT_DEC', '', '', 'disk.png', 1, 'Modifier', 'REF_CONT_DECISION'),
('MOD_COORDINNATEUR', '', '', 'disk.png', 1, 'Modifier', 'COORDONNATEUR'),
('MOD_COURRIERAC', '', 'Modifier courrier AC', 'disk.png', 1, 'Modifier', 'FEA_COURRIERAC'),
('MOD_COURRIERDEPART', '', 'Modifier', 'disk.png', 1, 'Modifier', 'COURRIERDEPART'),
('MOD_CRITERE', '', '', 'disk.png', 1, 'Modifier', 'GESTION_CRITERE'),
('MOD_CRITEREANA', ' ', 'Modifier', 'disk.png', 1, 'Modifier', 'CRITEREDANALYSE'),
('MOD_DECISION', '', '', 'disk.png', 1, 'Modifier', 'DECISION'),
('MOD_DELAIS', '', '', 'disk.png', 1, 'Modifier', 'DELAIS'),
('MOD_DEMANDE_PAIEMENT', '', '', 'disk.png', 1, 'Modifier', 'DEMANDE_PAIEMENT'),
('MOD_DENONCIATION', 'Denonciation', 'Modifier', 'disk.png', 1, 'Modifier', 'Denonciation'),
('MOD_DENONCIATIONC', ' ', 'Modifier', 'disk.png', 1, 'Modifier', 'DENONCIATIONC'),
('MOD_DENONCIATIONN', ' ', 'Modifier', 'disk.png', 1, 'Modifier', 'DENONCIATIONN'),
('MOD_DIRECTIONSERVICE', '', '', 'disk.png', 1, 'Modifier', 'DIRECTIONSERVICE'),
('MOD_DOCUMENT', ' ', 'Modifier', 'disk.png', 1, 'Modifier', 'CATEGORIEDOCUMENT'),
('MOD_DOSSIERAC', 'mod_dossier', 'Modifier', 'pencil-small.png', 1, 'Modifier', 'DOSSIERAC'),
('MOD_DOSSIERTYPE', ' ', 'Modifier', 'disk.png', 1, 'Modifier', 'DOSSIERSTYPE'),
('MOD_DPROCEDURESDEROGATOIRES', '    ', 'Modifier', 'disk.png', 1, 'Modifier', 'DPROCEDURESDEROGATOIRES'),
('MOD_EVA', '', '', 'disk.png', 1, 'Modifier', 'GESTION_EVAL'),
('MOD_FAMILLES', '   ', 'Modifier', 'disk.png', 1, 'Modifier', 'FAMILLES'),
('MOD_FICHIER_LEGISLATION', '', '', 'disk.png', 1, 'Modifier', 'FICHIER_LEGISLATION'),
('MOD_FONCTIONS', '', '', 'disk.png', 1, 'Modifier', 'FONCTIONS'),
('MOD_FORM', '', '', 'disk.png', 1, 'Modifier', 'FEA_FORMATION'),
('MOD_FORMATEUR', '', '', 'disk.png', 1, 'Modifier', 'FORMATEUR'),
('MOD_FOURNISSEUR', '', '', 'disk.png', 1, 'Modifier', 'FOURNISSEURS'),
('MOD_GARANTIS', '', '', 'disk.png', 1, 'Modifier', 'GARANTIS'),
('MOD_GESTIONNOEUDARCHIVE', '', 'Modifier', 'pencil-small.png', 1, 'Modifier', 'GESTIONNOEUDARCHIVE'),
('MOD_GLOSSAIRE', '', '', 'disk.png', 1, 'Modifier', 'REF_GLOSSAIRE'),
('MOD_GROUPE', '', '', 'disk.png', 1, 'Modifier', 'GROUPE'),
('MOD_GROUPEFORM', '', '', 'disk.png', 1, 'Modifier', 'GROUPEFORMATION'),
('MOD_JOURNAL', '', '', 'disk.png', 1, 'Modifier', 'REF_JOURNAL'),
('MOD_JOURSFERIES', '', '', 'disk.png', 1, 'Modifier', 'JOURSFERIES'),
('MOD_LISTE_ROUGE_F', '', 'Modifier', 'disk.png', 1, 'Modifier', 'FEA_LISTE_ROUGE_FOURNISSEUR'),
('MOD_LOGISTICIEN', '', '', 'disk.png', 1, 'Modifier', 'LOGISTICIEN'),
('MOD_MODELEDOCUMENT', '  ', 'Modifier', 'disk.png', 1, 'Modifier', 'MODELEDOCUMENT'),
('MOD_MODEPASSATION', '', '', 'disk.png', 1, 'Modifier', 'MODEPASSATION'),
('MOD_MODESELECTION', '', '', 'disk.png', 1, 'Modifier', 'MODESELECTION'),
('MOD_MODULE', '', '', 'disk.png', 1, 'Modifier', 'MODULE'),
('MOD_MONNAI', '', '', 'disk.png', 1, 'Modifier', 'MONNAIE_OFFRE'),
('MOD_MONTANTSSEUILS', '', '', 'disk.png', 1, 'Modifier', 'MONTANTSSEUILS'),
('MOD_NAT', '', '', 'disk.png', 1, 'Modifier', 'NAT_PRIX'),
('MOD_NATURE', ' ', 'Modifier', 'disk.png', 1, 'Modifier', 'NATURECOURRIER'),
('MOD_PARTFORMATION', '', '', 'disk.png', 1, 'Modifier', 'REF_PARTFORMATION'),
('MOD_PAYS', '', '', 'disk.png', 1, 'Modifier', 'PAYS'),
('MOD_PJCONT', '', '', 'disk.png', 1, 'Modifier', 'PJCONT'),
('MOD_PJDENONCIATION', '', 'Modifier', 'disk.png', 1, 'Modifier', 'PJDENONCIATION'),
('MOD_POLEDCMP', '', '', 'disk.png', 1, 'Modifier', 'POLEDCMP'),
('MOD_PRESTATAIRE', '', '', 'disk.png', 1, 'Modifier', 'PRESTATAIRES'),
('MOD_PROCEDUREDEROGATOIRE', '   ', 'Modifier', 'disk.png', 1, 'Modifier', 'PROCEDUREDEROGATOIRE'),
('MOD_PROCEDURESPASSATIONS', '', '', 'disk.png', 1, 'Modifier', 'PROCEDURESPASSATIONS'),
('MOD_PRODUITS', '', '', 'disk.png', 1, 'Modifier', 'PRODUITS'),
('MOD_PROFIL', '', '', 'disk.png', 1, 'Modifier', 'GESTION_PROFIL'),
('MOD_PROJETREP', 'mod', 'Modifier', 'pencil-small.png', 1, 'Modifier', 'FEA_PROJETREPONSE'),
('MOD_RAPARMP', '', '', 'disk.png', 1, 'Modifier', 'RAPPORTARMP'),
('MOD_RAPDCMP', '', '', 'disk.png', 1, 'Modifier', 'RAPPORTDCMP'),
('MOD_RAPFORMATION', '', 'Modifier', 'disk.png', 1, 'Modifier', 'RAPPORTFORMATION'),
('MOD_RECEPTION', ' ', 'Modifier', 'disk.png', 1, 'Modifier', 'MODERECEPTION'),
('MOD_REFCATEGORIES', '  ', 'Modifier', 'disk.png', 1, 'Modifier', 'REFCATEGORIES'),
('MOD_REFERENTIELPRIX', '  ', 'Modifier', 'disk.png', 1, 'Modifier', 'REFERENTIELPRIX'),
('MOD_REGCOM', '', '', 'disk.png', 1, 'Modifier', 'REGCOMMUNAUTAIRE'),
('MOD_REGNAT', '', '', 'disk.png', 1, 'Modifier', 'REGNATIONALE'),
('MOD_SECTEURSACTIVITES', '', '', 'disk.png', 1, 'Modifier', 'SECTEURSACTIVITES'),
('MOD_SERVICEAC', '', '', 'disk.png', 1, 'Modifier', 'SERVICEAC'),
('MOD_SEUILSCOMMUNAUTAIRE', '', '', 'disk.png', 1, 'Modifier', 'SEUILSCOMMUNAUTAIRE'),
('MOD_SEUILSRAPRIORI', '', '', 'disk.png', 1, 'Modifier', 'SEUILSRAPRIORI'),
('MOD_SPECIALITE', '', '', 'disk.png', 1, 'Modifier', 'SPECIALITE'),
('MOD_SUIVIDENONCIATION', 'SuiviDenonciation', 'Modifier', 'disk.png', 1, 'Modifier', 'SUIVIDENONCIATION'),
('MOD_TRAITEMENT', ' ', 'Modifier', 'disk.png', 1, 'Modifier', 'MODETRAITEMENT'),
('MOD_TYPEAC', '', '', 'disk.png', 1, 'Modifier', 'TYPEAC'),
('MOD_TYPECOURRIER', '  ', 'Modifier', 'disk.png', 1, 'Modifier', 'TYPECOURRIER'),
('MOD_TYPEDECISION', '', '', 'disk.png', 1, 'Modifier', 'TYPEDECISION'),
('MOD_TYPEDEMANDE', '  ', 'Mdifier', 'disk.png', 1, 'Modifier', 'REF_TYPEDEMANDE'),
('MOD_TYPERAP', '', '', 'disk.png', 1, 'Modifier', 'TYPERAPPORT'),
('MOD_TYPESDOSSIERS', ' ', 'Modifier', 'disk.png', 1, 'Modifier', 'TYPESDOSSIERS'),
('MOD_TYPESERVICE', '', '', 'disk.png', 1, 'Modifier', 'TYPESERVICE'),
('MOD_TYPEUNITEORGARMP', '', '', 'disk.png', 1, 'Modifier', 'TYPEUNITEORGARMP'),
('MOD_TYPEUNITEORGDCMP', '', '', 'disk.png', 1, 'Modifier', 'TYPEUNITEORGDCMP'),
('MOD_UNITEORGARMP', '', '', 'disk.png', 1, 'Modifier', 'UNITEORGARMP'),
('MOD_UTILISATEUR', 'mod_utilisateur', 'Modifier', 'disk.png', 1, 'Modifier', 'UTILISATEURARMP'),
('MOD_UTILISATEURAC', '', '', 'disk.png', 1, 'Modifier', 'UTILISATEURAC'),
('MOD_UTILISATEURDNCMP', '', '', 'disk.png', 1, 'Modifier', 'UTILISATEURDCMP'),
('MWREAL_PLANPASSATIONS', '', '', 'zoom.png', 1, 'R�alisations pr�vues ', 'MAJPLANAVALIDES'),
('MWVALID_PLANPASSATION', '', '', 'zoom.png', 1, 'Valider', 'MAJPLANAVALIDES'),
('MWWPUB_PLANPASSATION', '', '', 'zoom.png', 1, 'Publier', 'MAJPLANAVALIDES'),
('NOUVEAU_CONT', '', '', '', 1, '', 'NOUVEAUX_CONT'),
('NOUV_PLANPASSATION', '', '', '', 1, 'Nouveau Plan de Passation', 'NOUV_PLANPASSATION'),
('PAIEMENT', '', '', '', 1, '', 'PAIEMENT'),
('PASSATIONSMARCHES', '', '', '', 1, 'Passation des march�s', 'PASSATIONSMARCHES'),
('RECOUVREMENT', '', '', '', 1, '', 'RECOUVREMENT'),
('REFERENTIEL', '', 'Référentiel', '', 1, 'R�f�rentiel', 'REFERENTIEL'),
('REVUEDAO', '', '', '', 1, 'Revue DAO', 'REVUEDAO'),
('SAISIPRIX', '', '', '', 1, '', 'SAISIPRIX'),
('STATUEMOA', '', '', '', 1, 'Statistique P�riodique UEMOA', 'STATUEMOA'),
('SUIVI_DOSSIERAC', 'suivi_dossier', 'Suivi', 'imputer.png', 1, 'Suivi', 'DOSSIERAC'),
('SUIVI_FORM', '', '', 'zoom.png', 1, 'Suivi', 'FEA_SUIVIFORMATION'),
('SUI_DEMANDE_PAIEMENT', '', '', 'zoom.png', 1, 'Suivi paiement', 'DEMANDE_PAIEMENT'),
('SUPP_AC', '', '', 'delete.png', 1, 'Supprimer', 'AUTORITEAC'),
('SUPP_ANNUAIREARMP', '', '', 'delete.png', 1, 'Supprimer', 'ANNUAIREARMP'),
('SUPP_ANNUAIREDCMP', '', '', 'delete.png', 1, 'Supprimer', 'ANNUAIREDCMP'),
('SUPP_ARRETES', '', 'Supprimer', 'delete.png', 1, 'Supprimer', 'ARRETES'),
('SUPP_AUDIT', '', '', 'delete.png', 1, 'Supprimer', 'AUDITS'),
('SUPP_AUTORITEAUTC', '', '', 'delete.png', 1, 'Supprimer', 'AUTORITEAUTC'),
('SUPP_AVIGENERAL', '  ', 'Supprimer', 'delete.png', 1, 'Supprimer', 'AVIGENERAL'),
('SUPP_BAILLEUR', '', '', 'delete.png', 1, 'Supprimer', 'BAILLEURS'),
('SUPP_BANQUE', '', '', 'delete.png', 1, 'Supprimer', 'BANQUE'),
('SUPP_BREALISATIONS', '', '', 'delete.png', 1, 'Supprimer', 'BREALISATIONS'),
('SUPP_BUREAUXDCMP', '', '', 'delete.png', 1, 'Supprimer', 'BUREAUXDCMP'),
('SUPP_CAT', 'Sup', '', 'delete.png', 1, 'Supprimer', 'CATEGORIE'),
('SUPP_CATEGORIE', '', '', 'delete.png', 1, 'Supprimer', 'GESTION_CATEGORIE'),
('SUPP_CATEGORIES', '', '', 'delete.png', 1, 'Supprimer', 'CATEGORIES'),
('SUPP_COMMISSIONSMARCHES', '', '', 'delete.png', 1, 'Supprimer', 'COMMISSIONSMARCHES'),
('SUPP_CONFTYPESDOSSIERS', '  ', 'Supprimer', 'delete.png', 1, 'Supprimer', 'CONFTYPESDOSSIERS'),
('SUPP_CONTENTIEUX', '', '', 'delete.png', 1, 'Supprimer', 'CONTENTIEUX'),
('SUPP_CONT_DEC', '', '', 'delete.png', 1, 'Supprimer', 'REF_CONT_DECISION'),
('SUPP_CRITEREANA', ' ', 'Supprimer', 'delete.png', 1, 'Supprimer', 'CRITEREDANALYSE'),
('SUPP_DECISION', '', '', 'delete.png', 1, 'Supprimer', 'DECISION'),
('SUPP_DELAIS', '', '', 'delete.png', 1, 'Supprimer', 'DELAIS'),
('SUPP_DENONCIATION', 'Denonciation', 'Supprimer', 'delete.png', 1, 'Supprimer', 'Denonciation'),
('SUPP_DENONCIATIONC', ' ', 'Spprimer', 'delete.png', 1, 'Supprimer', 'DENONCIATIONC'),
('SUPP_DENONCIATIONN', ' ', 'Supprimer', 'delete.png', 1, 'Supprimer', 'DENONCIATIONN'),
('SUPP_DIRECTIONSERVICE', '', '', 'delete.png', 1, 'Supprimer', 'DIRECTIONSERVICE'),
('SUPP_DOCUMENT', '  ', 'Supprimer', 'delete.png', 1, 'Supprimer', 'CATEGORIEDOCUMENT'),
('SUPP_DOSSIERAC', 'supp_dossier', 'Supprimer', 'delete.png', 1, 'Supprimer', 'DOSSIERAC'),
('SUPP_DOSSIERTYPE', ' ', 'Supprimer', 'delete.png', 1, 'Spprimer', 'DOSSIERSTYPE'),
('SUPP_DPROCEDURESDEROGATOIRES', '  ', 'Supprimer', 'delete.png', 1, 'Supprimer', 'DPROCEDURESDEROGATOIRES'),
('SUPP_FAMILLES', '   ', 'Supprimer', 'delete.png', 1, 'Supprimer', 'FAMILLES'),
('SUPP_FONCTIONS', '', '', 'delete.png', 1, 'Supprimer', 'FONCTIONS'),
('SUPP_FOURNISSEUR', '', '', 'delete.png', 1, 'Supprimer', 'FOURNISSEURS'),
('SUPP_GARANTIS', '', '', 'delete.png', 1, 'Supprimer', 'GARANTIS'),
('SUPP_GRILLESANALYSES', '  ', 'Supprimer', 'delete.png', 1, 'Supprimer', 'GRILLESANALYSES'),
('SUPP_JOURSFERIES', '', '', 'delete.png', 1, 'Supprimer', 'JOURSFERIES'),
('SUPP_MODELEDOCUMENT', '  ', 'Supprimer', 'delete.png', 1, 'Supprimer', 'MODELEDOCUMENT'),
('SUPP_MODEPASSATION', '', '', 'delete.png', 1, 'Supprimer', 'MODEPASSATION'),
('SUPP_MODESELECTION', '', '', 'delete.png', 1, 'Supprimer', 'MODESELECTION'),
('SUPP_MONTANTSSEUILS', '', '', 'delete.png', 1, 'Supprimer', 'MONTANTSSEUILS'),
('SUPP_NATURE', ' ', 'Supprimer', 'delete.png', 1, 'Supprimer', 'NATURECOURRIER'),
('SUPP_PARTFORMATION', '', '', 'delete.png', 1, 'Supprimer', 'REF_PARTFORMATION'),
('SUPP_PAYS', '', '', 'delete.png', 1, 'Supprimer', 'PAYS'),
('SUPP_PIECES', '', '', 'delete.png', 1, 'Supprimer', 'PIECES'),
('SUPP_PIECESREQUISES', '  ', 'Supprimer', 'delete.png', 1, 'Supprimer', 'PIECESREQUISES'),
('SUPP_PJCONT', '', '', 'delete.png', 1, 'Supprimer', 'PJCONT'),
('SUPP_PJDENONCIATION', '', 'Supprimer', 'delete.png', 1, 'Supprimer', 'PJDENONCIATION'),
('SUPP_PLANPASSATION', '', '', 'delete.png', 1, 'Supprimer', 'PLANPASSATION'),
('SUPP_POLEDCMP', '', '', 'delete.png', 0, 'Supprimer', 'POLEDCMP'),
('SUPP_PROCEDUREDEROGATOIRE', '   ', 'Supprimer', 'delete.png', 1, 'Supprimer', 'PROCEDUREDEROGATOIRE'),
('SUPP_PRODUITS', '', '', 'delete.png', 1, 'Supprimer', 'PRODUITS'),
('SUPP_RAPARMP', '', '', 'delete.png', 1, 'Supprimer', 'RAPPORTARMP'),
('SUPP_RAPDCMP', '', '', 'delete.png', 1, 'Supprimer', 'RAPPORTDCMP'),
('SUPP_RAPFORMATION', '', 'Supprimer', 'delete.png', 1, 'Supprimer', 'RAPPORTFORMATION'),
('SUPP_RECEPTION', '  ', 'Supprimer', 'delete.png', 0, 'Supprimer', 'MODERECEPTION'),
('SUPP_REFCATEGORIES', '   ', 'Supprimer', 'delete.png', 1, 'Supprimer', 'REFCATEGORIES'),
('SUPP_REFERENTIELPRIX', '   ', 'Supprimer', 'delete.png', 1, 'Supprimer', 'REFERENTIELPRIX'),
('SUPP_REGCOM', '', '', 'delete.png', 1, 'Supprimer', 'REGCOMMUNAUTAIRE'),
('SUPP_REGNAT', '', '', 'delete.png', 1, 'Supprimer', 'REGNATIONALE'),
('SUPP_SECTEURSACTIVITES', '', '', 'delete.png', 1, 'Supprimer', 'SECTEURSACTIVITES'),
('SUPP_SERVICEAC', '', '', 'delete.png', 1, 'Supprimer', 'SERVICEAC'),
('SUPP_SEUILSCOMMUNAUTAIRE', '', '', 'delete.png', 1, 'Supprimer', 'SEUILSCOMMUNAUTAIRE'),
('SUPP_SEUILSRAPRIORI', '', '', 'delete.png', 1, 'Supprimer', 'SEUILSRAPRIORI'),
('SUPP_SUIVIDENONCIATION', 'Suivi_Denonciation', 'Supprimer', 'delete.png', 1, 'Supprimer', 'SUIVIDENONCIATION'),
('SUPP_TRAITEMENT', ' ', 'Supprimer', 'delete.png', 1, 'Supprimer', 'MODETRAITEMENT'),
('SUPP_TYPEAC', '', '', 'delete.png', 1, 'Supprimer', 'TYPEAC'),
('SUPP_TYPECOURRIER', '  ', 'Supprimer', 'delete.png', 1, 'Supprimer', 'TYPECOURRIER'),
('SUPP_TYPEDECISION', '', '', 'delete.png', 1, 'Supprimer', 'TYPEDECISION'),
('SUPP_TYPEDEMANDE', '  ', 'Supprimer', 'delete.png', 1, 'Supprimer', 'REF_TYPEDEMANDE'),
('SUPP_TYPEMODELEDOCUMENT', '  ', 'Supprimer', 'delete.png', 1, 'Supprimer', 'TYPEMODELEDOCUMENT'),
('SUPP_TYPESDOSSIERS', '  ', 'Supprimer', 'SUPP.png', 1, 'Supprimer', 'TYPESDOSSIERS'),
('SUPP_TYPESERVICE', '', '', 'delete.png', 0, 'Supprimer', 'TYPESERVICE'),
('SUPP_TYPESMARCHESMP', '', '', 'delete.png', 1, 'Supprimer', 'TYPESMARCHESMP'),
('SUPP_TYPESMARCHESMS', '', '', 'delete.png', 1, 'Supprimer', 'TYPESMARCHESMS'),
('SUPP_TYPEUNITEORGARMP', '', '', 'delete.png', 1, 'Supprimer', 'TYPEUNITEORGARMP'),
('SUPP_TYPEUNITEORGDCMP', '', '', 'delete.png', 1, 'Supprimer', 'TYPEUNITEORGDCMP'),
('SUPP_UNITEORGARMP', '', '', 'delete.png', 1, 'Supprimer', 'UNITEORGARMP'),
('SUP_CODE_MARCHE', '', '', 'delete.png', 1, 'Supprimer', 'CODE_MARCHE'),
('SUP_COURRIERAC', '', 'Supprimer', 'delete.png', 1, 'Supprimer', 'FEA_COURRIERAC'),
('SUP_COURRIERACDOS', 'del', 'Ajouter', 'delete.png', 1, 'Supprimer', 'FEA_COURRIERACDOS'),
('SUP_COURRIERDEPART', '', 'Supprimer', 'delete.png', 1, 'Supprimer', 'COURRIERDEPART'),
('SUP_DEMANDE_PAIEMENT', '', '', 'delete.png', 1, 'Supprimer', 'DEMANDE_PAIEMENT'),
('SUP_EVA', '', '', 'delete.png', 1, 'Supprimer', 'GESTION_EVAL'),
('SUP_FICHIER_LEGISLATION', '', '', 'delete.png', 1, 'Supprimer', 'FICHIER_LEGISLATION'),
('SUP_FORM', '', '', 'delete.png', 1, 'Supprimer', 'FEA_FORMATION'),
('SUP_GROUPE', '', '', 'delete.png', 1, 'Supprimer', 'GROUPE'),
('SUP_LISTE_ROUGE_F', '', 'Supprimer', 'delete.png', 1, 'Supprimer', 'FEA_LISTE_ROUGE_FOURNISSEUR'),
('SUP_NAT', '', '', 'delete.png', 1, 'Supprimer', 'NAT_PRIX'),
('SUP_PRESTATAIRE', '', '', 'delete.png', 1, 'Supprimer', 'PRESTATAIRES'),
('SUP_PROJETREP', 'sup', 'Supprimer', 'deletemoins.png', 1, 'Supprimer', 'FEA_PROJETREPONSE'),
('TREE_TYPEUNITEORG', '', '', '', 1, '', 'BUREAUTYPEDCMP'),
('UPDATE_FEA_MODELE_ARC', '', '', 'disk.png', 1, 'Modifier', 'FEA_MODELE_ARC'),
('VALIDAP', '', '', '', 1, 'Validation Attribution provisoire', 'VALIDAP'),
('VAL_PRIX', '', '', 'ok.png', 1, 'Valider', 'VALPUBPRIX'),
('WADD_PAIEMENT', '', '', 'add.png', 1, 'Enregistrer Paiement', 'SUIVIPAIEMENT'),
('WADD_RECOUVREMENT', '', '', 'add.png', 1, 'Enregistrer Recouvrement', 'SUIVIRECOUVREMENT'),
('WCLASSER_SANS_SUITE_DENONCIATIONC', ' ', 'Classer', 'zoom.png', 1, 'Classer sans suite', 'DENONCIATIONC'),
('WCLASSER_SANS_SUITE_DENONCIATIONN', '  ', 'Classer', 'zoom.png', 1, 'Classer sans suite', 'DENONCIATIONN'),
('WCLASSER_SANS_SUITE_DENONCIATIONP', ' ', 'Classer', 'zoom.png', 1, 'Classer sans suite', 'DENONCIATIONP'),
('WCONF_TYPESDOSSIERS', '  ', 'Configuration', 'zoom.png', 1, 'Configuration', 'TYPESDOSSIERS'),
('WCONTRATS', ' ', 'Signer contract', 'zoom.png', 0, 'Contrats', 'AUDITS'),
('WDEPUBLIER_ARRETES', '', 'D�publier', 'zoom.png', 1, 'D�publier', 'ARRETES'),
('WDEPUBLIER_AVIGENERAL', '  ', 'Dépublier', 'publish_g.png', 1, 'D�publier', 'AVIGENERAL'),
('WDETAIL_PRESTATAIRE', '', '', 'gerer.gif', 1, 'D�tails', 'PRESTATAIRES'),
('WDET_AC', '', '', 'zoom.png', 1, 'Service', 'AUTORITEAC'),
('WDET_PROCEDURESPASSATIONS', '', '', 'zoom.png', 1, 'D�tails', 'PROCEDURESPASSATIONS'),
('WDET_TYPEAC', '', '', 'zoom.png', 1, 'Autorit�  contractante', 'TYPEAC'),
('WFER_AC', '', '', 'cancel.png', 1, 'Fermer', 'AUTORITEAC'),
('WFER_BREALISATIONS', '', '', 'cancel.png', 1, 'Fermer', 'BREALISATIONS'),
('WFER_SERVICEAC', '', '', 'cancel.png', 1, 'Fermer', 'SERVICEAC'),
('WFER_TYPESMARCHESMP', '', '', 'cancel.png', 1, 'Fermer', 'TYPESMARCHESMP'),
('WFER_TYPESMARCHESMS', '', '', 'cancel.png', 1, 'Fermer', 'TYPESMARCHESMS'),
('WGER_PROCEDURESPASSATIONS', '', '', 'gerer.gif', 1, 'G�rer la proc�dure', 'PROCEDURESPASSATIONS'),
('WMODP_TYPESMARCHES', '', '', 'zoom.png', 1, 'Mode de passation', 'TYPESMARCHES'),
('WMODS_TYPESMARCHES', '', '', 'zoom.png', 1, 'Mode de s�lection', 'TYPESMARCHES'),
('workflowmanager_ADD', 'workflowmanager', 'Ajouter', 'add.png', 1, 'Ajouter', 'workflowmanager'),
('workflowmanager_MOD', 'workflowmanager', 'Modifier', 'disk.png', 1, 'Modifier', 'workflowmanager'),
('workflowmanager_SUP', 'workflowmanager', 'Supprimer', 'delete.png', 1, 'Supprimer', 'workflowmanager'),
('workflowmanager_WDETAILS', 'workflowmanager', 'Details', 'zoom.png', 1, 'Details', 'workflowmanager'),
('workflowmanager_WVISUALISER', 'workflowmanager', 'Visualiser', 'view.png', 1, 'Visualiser', 'workflowmanager'),
('workflowmanager_WVonReload', 'workflowmanager', 'Recharger les configurations de base des processus', 'view.png', 1, 'Recharger les configurations de base des processus', 'workflowmanager'),
('WPUBLIER_ARRETES', '', 'Publier', 'zoom.png', 1, 'Publier', 'ARRETES'),
('WPUBLIER_AVIGENERAL', '  ', 'Publier', 'zoom.png', 1, 'Publier', 'AVIGENERAL'),
('WPUB_PRIX', '', '', 'zoom.png', 1, 'Publier', 'VALPUBPRIX'),
('WREAL_PLANPASSATION', '', '', 'zoom.png', 1, 'R�alisations pr�vues ', 'PLANPASSATION'),
('WREAL_PLANPASSATIONS', '', '', 'zoom.png', 1, 'R�alisations pr�vues ', 'PLANAVALIDES'),
('WREAL_PLANSPUBLIES', '', '', 'zoom.png', 1, 'R�alisations', 'PLANSPUBLIES'),
('WRESTAURER_DENONCIATIONPO', ' ', 'Restaurer', 'zoom.png', 1, 'Restaurer', 'DENONCIATIONPO'),
('WSAISI_PRIX', '', '', 'gerer.gif', 1, 'Saisi de prix', 'PRODUITS'),
('WSOUMETTRE_PROCEDURE', '  ', 'Soumettre', 'zoom.png', 1, 'Soumettre', 'PASSATIONSMARCHES'),
('WSOUMETTRE_PROCEDUREDEROGATOIRE', '  ', 'Soumettre', 'zoom.png', 1, 'Suivi', 'PROCEDUREDEROGATOIRE'),
('WSOUM_PLAN', '', '', 'zoom.png', 1, 'Soumettre pour validation', 'REALSPLANS'),
('WSOUM_PLANPASSATION', '', '', 'zoom.png', 1, 'Soumettre pour validation', 'PLANPASSATION'),
('WSUIVIDAO', '', '', 'zoom.png', 1, 'Suivi', 'EXECUTIONDAO'),
('WSUIVI_AUDITS', '', '', 'zoom.png', 1, 'Suivi', 'AUDITS'),
('WSUIVI_CONTENTIEUX', '', '', 'gerer.gif', 1, 'Suivi', 'CONTENTIEUX'),
('WSUIVI_COURRIERAC', '', 'Suivi', 'zoom.png', 1, 'Suivi', 'FEA_COURRIERAC'),
('WSUIVI_COURRIERDEPART', '', 'Suivi', 'zoom.png', 1, 'Suivi', 'COURRIERDEPART'),
('WSUIVI_DENONCIATIONC', ' ', 'Suivi', 'zoom.png', 1, 'Suivi', 'DENONCIATIONC'),
('WSUIVI_DENONCIATIONN', ' ', 'Suivi', 'zoom.png', 1, 'Suivi', 'DENONCIATIONN'),
('WSUIVI_DENONCIATIONP', ' ', 'Suivi', 'zoom.png', 1, 'Suivi', 'DENONCIATIONP'),
('WVALIDER_PROCEDUREDEROGATOIRE', '', 'Suivi', 'zoom.png', 1, 'Suivi', 'VALDMDDEROG'),
('WVALID_PLAN', '', '', 'zoom.png', 1, 'Valider', 'REALSPLANS'),
('WVALID_PLANPASSATION', '', '', 'zoom.png', 1, 'Valider', 'PLANAVALIDES'),
('WWPUB_PLAN', '', '', 'zoom.png', 1, 'Publier', 'REALSPLANS'),
('WWPUB_PLANPASSATION', '', '', 'zoom.png', 1, 'Publier', 'PLANAVALIDES'),
('WZREAL_FERMER', '', '', 'cancel.png', 1, 'Fermer', 'REALSPLANS');

-- --------------------------------------------------------

--
-- Structure de la table `sys_alerte`
--

CREATE TABLE IF NOT EXISTS `sys_alerte` (
  `ALT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ALT_CONTENU` longtext,
  `ALT_DELAI` int(11) NOT NULL,
  `ALT_DESC` longtext,
  `ALT_MODE` varchar(200) DEFAULT NULL,
  `ALT_NOM` varchar(200) NOT NULL,
  `ALT_NOTIFICATION` smallint(6) NOT NULL,
  `ALT_PERIODE` int(11) NOT NULL,
  `STA_CODE` varchar(200) NOT NULL,
  PRIMARY KEY (`ALT_ID`),
  UNIQUE KEY `ALT_ID` (`ALT_ID`),
  KEY `FKC895103B27DC46A0` (`STA_CODE`),
  KEY `FKC895103B20D0FEC1` (`STA_CODE`),
  KEY `FKC895103BF805BFDC` (`STA_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `sys_alerte`
--


-- --------------------------------------------------------

--
-- Structure de la table `sys_alerteprofil`
--

CREATE TABLE IF NOT EXISTS `sys_alerteprofil` (
  `APF_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ALT_ID` bigint(20) NOT NULL,
  `PF_CODE` varchar(32) NOT NULL,
  PRIMARY KEY (`APF_ID`),
  UNIQUE KEY `APF_ID` (`APF_ID`),
  KEY `FK9F31F63715A35950` (`ALT_ID`),
  KEY `FK9F31F6375C186C28` (`PF_CODE`),
  KEY `FK9F31F637AB3EC271` (`ALT_ID`),
  KEY `FK9F31F637F1B3D549` (`PF_CODE`),
  KEY `FK9F31F6378307CE8C` (`ALT_ID`),
  KEY `FK9F31F637C97CE164` (`PF_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `sys_alerteprofil`
--


-- --------------------------------------------------------

--
-- Structure de la table `sys_arraw`
--

CREATE TABLE IF NOT EXISTS `sys_arraw` (
  `ARW_CODE` varchar(200) NOT NULL,
  `ARW_DESC` longtext,
  `ARW_LIBELLE` varchar(200) DEFAULT NULL,
  `ARW_TOSTATE` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ARW_CODE`),
  UNIQUE KEY `ARW_CODE` (`ARW_CODE`),
  KEY `FK1F4186654275B011` (`ARW_TOSTATE`),
  KEY `FK1F4186653B6A6832` (`ARW_TOSTATE`),
  KEY `FK1F418665129F294D` (`ARW_TOSTATE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `sys_arraw`
--

INSERT INTO `sys_arraw` (`ARW_CODE`, `ARW_DESC`, `ARW_LIBELLE`, `ARW_TOSTATE`) VALUES
('CLOTDOSSIER', 'Cloturer Dossier', 'Envoyer Dossier', 'CLOTDOSSIER_STATE'),
('ENREGDECCONT', 'Enregistrer Décision Contentieux', 'Enregistrer Décision Contentieux', 'ENREGDECCONT_STATE'),
('ENROBSERVAC', 'Enregistrer Observations AC', 'Enregistrer Observations AC', 'ENROBSERVAC_STATE'),
('ENRRAPPORTFINAL', 'Enregistrer Rapport Final', 'Enregistrer Rapport Final', 'ENRRAPPORTFINAL_STATE'),
('ENRRAPPORTPROV', 'Enregistrer Rapport Provisoire', 'Enregistrer Rapport Provisoire', 'ENRRAPPORTPROV_STATE'),
('INIAUDIT', 'Initialiser Audit', 'Initialiser Audit', 'INIAUDIT_STATE'),
('REJDOSSIER', 'Rejeter Dossier', 'Transmettre pour signature', 'REJDOSSIER_STATE'),
('REJETERCONT', 'Rejeter Contentieux', 'Rejeter Contentieux', 'REJETERCONT_STATE'),
('SAISIDOSSIER', 'Saisir Dossier', 'Saisir Dossier', 'SAISIDOSSIER_STATE'),
('SAISIECONT', 'Saisir Contentieux', 'Saisir Contentieux', 'SAISIECONT_STATE'),
('SAISISPRESTATAIRES', 'Saisir Prestataires', 'Saisir Prestataires', 'SAISISPRESTATAIRES_STATE'),
('TRANSMDOSSIER', 'Transmettre Dossier pour traitement', 'Transmettre Dossier pour traitement', 'TRANSMDOSSIER_STATE'),
('TRANSRAPPORTPROVAC', 'Transmettre Rapport Provisoire aux AC', 'Transmettre Rapport Provisoire aux AC', 'TRANSRAPPORTPROVAC_STATE'),
('VALIDERCONT', 'Valider Contentieux', 'Valider Contentieux', 'VALIDERCONT_STATE'),
('VALIDOSSIER', 'Valider Dossier', 'Envoyer Dossier apr�s signature', 'VALIDOSSIER_STATE');

-- --------------------------------------------------------

--
-- Structure de la table `sys_arrawperm`
--

CREATE TABLE IF NOT EXISTS `sys_arrawperm` (
  `APM_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `APM_ALERTE` int(11) NOT NULL,
  `APM_ALLOW` smallint(6) NOT NULL,
  `ARW_CODE` varchar(200) NOT NULL,
  `ROL_CODE` varchar(200) NOT NULL,
  `FROM_STATE` varchar(200) NOT NULL,
  PRIMARY KEY (`APM_ID`),
  UNIQUE KEY `APM_ID` (`APM_ID`),
  KEY `FKFCD23ED56F8021B0` (`FROM_STATE`),
  KEY `FKFCD23ED520243910` (`ROL_CODE`),
  KEY `FKFCD23ED594C6F592` (`ARW_CODE`),
  KEY `FKFCD23ED56874D9D1` (`FROM_STATE`),
  KEY `FKFCD23ED5D59778CF` (`ROL_CODE`),
  KEY `FKFCD23ED58DBBADB3` (`ARW_CODE`),
  KEY `FKFCD23ED53FA99AEC` (`FROM_STATE`),
  KEY `FKFCD23ED5DC88A854` (`ROL_CODE`),
  KEY `FKFCD23ED564F06ECE` (`ARW_CODE`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Contenu de la table `sys_arrawperm`
--

INSERT INTO `sys_arrawperm` (`APM_ID`, `APM_ALERTE`, `APM_ALLOW`, `ARW_CODE`, `ROL_CODE`, `FROM_STATE`) VALUES
(1, 0, 0, 'INIAUDIT', 'AUDITS', 'INIT_STATE'),
(2, 0, 0, 'SAISISPRESTATAIRES', 'AUDITS', 'INIAUDIT_STATE'),
(3, 0, 0, 'ENRRAPPORTPROV', 'AUDITS', 'SAISISPRESTATAIRES_STATE'),
(4, 0, 0, 'TRANSRAPPORTPROVAC', 'AUDITS', 'ENRRAPPORTPROV_STATE'),
(5, 0, 0, 'ENROBSERVAC', 'AUDITS', 'TRANSRAPPORTPROVAC_STATE'),
(6, 0, 0, 'ENRRAPPORTFINAL', 'AUDITS', 'ENROBSERVAC_STATE'),
(7, 0, 0, 'SAISIECONT', 'SUIVICONTENTIEUX', 'INIT_STATE'),
(8, 0, 0, 'VALIDERCONT', 'SUIVICONTENTIEUX', 'SAISIECONT_STATE'),
(9, 0, 0, 'REJETERCONT', 'SUIVICONTENTIEUX', 'SAISIECONT_STATE'),
(10, 0, 0, 'ENREGDECCONT', 'SUIVICONTENTIEUX', 'VALIDERCONT_STATE'),
(16, 0, 0, 'SAISIDOSSIER', 'TDOSSIER', 'INIT_STATE'),
(17, 0, 0, 'TRANSMDOSSIER', 'TDOSSIER', 'SAISIDOSSIER_STATE'),
(18, 0, 0, 'VALIDOSSIER', 'TDOSSIER', 'TRANSMDOSSIER_STATE'),
(19, 0, 0, 'REJDOSSIER', 'TDOSSIER', 'VALIDOSSIER_STATE'),
(20, 0, 0, 'CLOTDOSSIER', 'TDOSSIER', 'REJDOSSIER_STATE');

-- --------------------------------------------------------

--
-- Structure de la table `sys_feauture`
--

CREATE TABLE IF NOT EXISTS `sys_feauture` (
  `FEA_CODE` varchar(50) NOT NULL,
  `FEA_APPLICATIONSTATE` varchar(200) NOT NULL,
  `FEA_DESC` longtext NOT NULL,
  `FEA_DISPLAYABLE` int(11) NOT NULL,
  `FEA_LIBELLE` varchar(200) NOT NULL,
  `FEA_LIBELLEEN` varchar(200) NOT NULL,
  `FEA_LIBELLEES` varchar(200) NOT NULL,
  `FEA_SEQUENCE` int(11) NOT NULL,
  `MOD_CODE` varchar(32) NOT NULL,
  PRIMARY KEY (`FEA_CODE`),
  UNIQUE KEY `FEA_CODE` (`FEA_CODE`),
  KEY `FK86035D59595B02CC` (`MOD_CODE`),
  KEY `FK86035D59EEF66BED` (`MOD_CODE`),
  KEY `FK86035D59C6BF7808` (`MOD_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `sys_feauture`
--

INSERT INTO `sys_feauture` (`FEA_CODE`, `FEA_APPLICATIONSTATE`, `FEA_DESC`, `FEA_DISPLAYABLE`, `FEA_LIBELLE`, `FEA_LIBELLEEN`, `FEA_LIBELLEES`, `FEA_SEQUENCE`, `MOD_CODE`) VALUES
('ANNUAIREARMP', 'annuaires_armp', 'Annuaire A.R.M.P', 1, 'Annuaire A.R.M.P', '', '', 1, 'ANNUAIRE'),
('ANNUAIREDCMP', 'annuaires_dcmp', 'Annuaire D.N.C.M.P', 1, 'Annuaire D.N.C.M.P', '', '', 2, 'ANNUAIRE'),
('ARCHIVAGEMODELE', 'archives_modele', 'Mod�le de classement ', 1, 'Mod�le de classement', '', '', 1, 'ARCHIVAGE'),
('ARCHIVECONFIG', 'archives_plan_configuration', 'Configuration du plan d''archivage', 1, 'Plan de classement', '', '', 2, 'ARCHIVAGE'),
('ARCHIVEGESTION', 'archives_gestion', 'La gestion des archives', 1, 'Gestion des archives', '', '', 3, 'ARCHIVAGE'),
('ARRETES', 'arretes', 'Arr�t�s de r�siliation', 1, 'Arr�t�s de r�siliation', '', '', 20, 'PUBLICATIONS'),
('AUDITS', 'audits', '', 1, 'Suivit Audits', '', '', 20, 'AUDITS'),
('AUTORITEAC', 'autoritescontractantes', 'Autorité  contractante', 0, 'Autorit� contractante', '', '', 3, 'REFERENTIEL'),
('AUTORITEAUTC', 'liste_ac', 'AutoritÃ©s Contractantes', 1, 'Autorit�s Contractantes', '', '', 50, 'REFERENTIEL'),
('AVIGENERAL', 'avigeneral', 'Avi General', 1, 'Avis G�n�raux', '  ', '   ', 40, 'AVIGENERAL'),
('BAILLEURS', '', '', 0, 'Bailleurs', '', '', 27, 'REFERENTIEL'),
('BANQUE', 'banque', 'Banque', 1, 'Banque', '', '', 22, 'REFERENTIEL'),
('BREALISATIONS', 'realisations_bailleurs', 'Sources de Financement ', 0, 'Sources de Financement ', '', '', 4, 'GESTIONPLANPASSATION'),
('BUREAUTYPEDCMP', 'unite_org_dcmp', 'Organigramme de la D.C.M.P', 1, 'Organigramme de la D.C.M.P', '', '', 2, 'ORGDCMP'),
('BUREAUXDCMP', 'bureauxdcmp', 'Organigramme de la D.C.M.P', 0, 'Organigramme de la D.C.M.P', '', '', 2, 'ORGDCMP'),
('CATEGORIE', 'list_categorie', '', 1, 'Cat�gories d''avis', '', '', 10, 'REFERENTIEL'),
('CATEGORIEDOCUMENT', 'CategorieDocument', 'Catégorie document', 1, 'Cat�gorie Document', '  ', '   ', 34, 'CATEGORIEDOCUMENT'),
('CATEGORIES', '', 'Catégorie', 0, 'Cat�gorie d''avis', '', '', 20, 'REFERENTIEL'),
('CODE_MARCHE', 'list_code_marche', 'code des marche', 1, 'Code des marches', 'code des marhés_EN', 'Code_marche_ES', 1, 'PUBLICATIONS'),
('COMMISSIONSMARCHES', 'membrescommissions', 'Membres Commissions', 1, 'Membres Commissions', '', '', 22, 'REFERENTIEL'),
('CONFDATESREAL', 'delaisdatesrealisations', 'Configuration Dates R�alisations', 1, 'Configuration Dates R�alisations', '', '', 1, 'REFERENTIEL'),
('CONFTYPESDOSSIERS', 'Configuration', 'Configuration Types Dossiers', 0, 'Configuration Types Dossiers', ' ', ' ', 36, 'TRAITEMENTDOSSIER'),
('CONTENTIEUX', 'contentieux', 'Suivi Contentieux', 1, 'Suivi Contentieux', '', '', 2, 'CONTENTIEUX'),
('COORDONNATEUR', 'coordinateurs', '', 1, 'Coordinateurs', '', '', 1, 'FORMATION'),
('COURRIERDEPART', 'courrier_depart', 'R�gistre Courriers d�part', 1, 'R�gistre Courriers d�part', '', '', 2, 'ACCOURRIER'),
('CRITEREDANALYSE', 'ListCritereAnalyse', 'Critére d''analyse', 1, 'Crit�re d''analyse', ' ', ' ', 35, 'TRAITEMENTDOSSIER'),
('DECISION', 'decision', 'Décision', 1, 'D�cision', '', '', 19, 'REFERENTIEL'),
('DELAIS', 'delais', 'Délais', 1, 'D�lais', '', '', 15, 'REFERENTIEL'),
('DEMANDE_PAIEMENT', 'liste_demande_paiement', '', 0, 'Demande paiment', 'Demande paiement_EN', 'Demande paiement_ES', 30, 'SUIVIMARCHE'),
('DENONCIATION', 'denonciations', 'Denonciation', 1, 'Suivi d�nonciations', ' ', '  ', 20, 'DENONCIATION'),
('DENONCIATIONC', 'denonciationc', 'Dénonciation courrier', 0, 'D�nonciation courrier', '  ', ' ', 32, 'DENONCIATION'),
('DENONCIATIONN', 'denonciationn', 'Dénonciation Numéro Vert', 0, 'D�nonciation Num�ro Vert', ' ', ' ', 33, 'DENONCIATION'),
('DENONCIATIONP', 'denonciationp', 'Dénonciation Portail', 0, 'D�nonciation Portail', ' ', ' ', 31, 'DENONCIATION'),
('DENONCIATIONPO', 'Denonciationpo', 'Dénonciation Poubelle', 0, 'D�nonciation Poubelle', ' ', ' ', 31, 'DENONCIATION'),
('DIRECTIONSERVICE', 'directionservice', 'Directions ou services', 1, 'Directions ou services', '', '', 80, 'REFERENTIEL'),
('DOSSIERAC', 'dossier_ac', 'Mes dossiers', 1, 'Mes dossiers', '', '', 1, 'TRAITEMENTDOSSIER'),
('DOSSIERSPP', 'dossiers_pp', 'Consultation DAO', 1, 'Consultation DAO', '', '', 10, 'TDOSSIERSPPM'),
('DOSSIERSTYPE', 'DossiersType', 'Dossiers Type', 1, 'Dossiers Type', ' ', ' ', 31, 'DOSSIERTYPE'),
('DPROCEDURESDEROGATOIRES', 'demandeprocedure', 'Nouvelle Demande de Procédure Dérogatoire', 0, 'Nouvelle Demande de Proc�dure D�rogatoire', '  ', '    ', 50, 'PASSATIONMARCHES'),
('EXAMENJUR', 'examen_juridique', 'Examen Juridique et Technique', 1, 'Examen Juridique et Technique', '', '', 4, 'TDOSSIERSPPM'),
('EXECUTIONDAO', 'dao', 'Execution', 1, 'Execution', '', '', 3, 'PASSATIONMARCHES'),
('FAMILLES', 'Familles', 'Familles', 0, 'Familles', '  ', '  ', 42, 'REFERENTIELPRIX'),
('FEA_COURRIERAC', 'courrier_ac', 'Courrier AC', 1, 'R�gistre des saisines', 'Suivi saisines', 'Suivi saisines', 1, 'ACCOURRIER'),
('FEA_COURRIERACDOS', 'courrier_dossier', 'Courriers', 0, 'Courriers', '', '', 111, 'TRAITEMENTDOSSIER'),
('FEA_FORMATION', 'ListeFormation', '', 1, 'Plan Formation', '', '', 10, 'FORMATION'),
('FEA_LISTE_ROUGE_FOURNISSEUR', 'liste_rouge_fournisseur', 'Liste rouge des fournisseurs ', 1, 'Liste rouge des fournisseurs ', '', '', 22, 'PUBLICATIONS'),
('FEA_MODELE_ARC', 'archives_modele', 'Gestion modÃ¨le archivage', 1, 'Gestion modÃ¨le archivage', '', '', 32, 'ARCHIVAGE_NUMERIQUE'),
('FEA_PROJETREPONSE', 'list_projet_reponse', 'Projet de rÃ©poonse', 0, 'Projet de rÃ©poonse', 'Response project', 'Projet de responsa', 100, 'TRAITEMENTDOSSIER'),
('FEA_SUIVIFORMATION', 'suivi_Formation', 'Suivi Formation', 1, 'Suivi Formation', '', '', 20, 'FORMATION'),
('FEA_SUIVIMARCHE', 'suivi_marche', '', 1, 'Suivi des march�s en cours', 'Suivi', 'Suivi', 1, 'SUIVIMARCHE'),
('FICHIER_LEGISLATION', 'list_fichier_legislation', '', 0, 'Fichier Legislation', '', '', 1, 'REFERENTIEL'),
('FONCTIONS', 'fonctions', 'Fonctions', 1, 'Fonctions', '', '', 9, 'REFERENTIEL'),
('FORMATEUR', 'list_formateur', '', 1, 'Bassin des formateurs', '', '', 7, 'FORMATION'),
('FOURNISSEURS', 'registre_fournisseur', '', 1, 'Registre des founisseurs', '', '', 25, 'FOURNISSEURS'),
('GARANTIS', 'garantie', 'Garanties', 1, 'Garanties', '', '', 11, 'REFERENTIEL'),
('GESTIONNOEUDARCHIVE', 'archives_plan_configuration', 'Configuration Plan de classement', 1, 'Configuration Plan de classement', '', '', 35, 'ARCHIVAGE_NUMERIQUE'),
('GESTION_CATEGORIE', 'categorie_fournisseur', '', 1, 'Cat�gories Fournisseurs', '', '', 21, 'REFERENTIEL'),
('GESTION_CONFAN', 'alertes', 'Configuration alertes et notifications', 1, 'Configuration alertes et notifications', '', '', 60, 'SECURITE'),
('GESTION_CONFAUDIT', 'audit', 'Configurer l''audit', 1, 'Configurer l''audit', '', '', 40, 'SECURITE'),
('GESTION_CONSJRNL', 'journal', 'Consulter le journal', 1, 'Consulter le journal', '', '', 50, 'SECURITE'),
('GESTION_CRITERE', 'criterequalifications', '', 1, 'Crit�res de qualification ', '', '', 20, 'REFERENTIEL'),
('GESTION_EVAL', 'personnes_resources', '', 1, 'Personnes ressources ', '', '', 22, 'REFERENTIEL'),
('GESTION_PROFIL', 'liste_profils', 'Gestion des profils', 1, 'Gestion des profils', '', '', 2, 'SECURITE'),
('GESTION_SECJOUR', 'journal_actions', 'Journal des actions', 1, 'Journal des actions', '', '', 80, 'SECURITE'),
('GRILLESANALYSES', 'GrillesAnalyses', 'Grilles Analyses', 0, 'Grilles Analyses', '  ', '  ', 37, 'TRAITEMENTDOSSIER'),
('GROUPE', 'groupes_imputation', 'Groupe imputation', 1, 'Groupe imputation', '', '', 100, 'REFERENTIEL'),
('GROUPEFORMATION', 'groupedeformation', 'Groupe de formation', 1, 'Groupe de formation', '', '', 1, 'FORMATION'),
('JOURSFERIES', 'joursferies', 'Jours fériés', 1, 'Jours f�ri�s', '', '', 10, 'REFERENTIEL'),
('LOGISTICIEN', 'logisticiens_formation', '', 1, 'Logisticiens', '', '', 2, 'FORMATION'),
('MAJPLANAVALIDES', 'maj_plans_avalider', 'Validation MAJ PPM', 1, 'Validation MAJ PPM', '', '', 7, 'GESTIONPLANPASSATION'),
('MAJPLANPASSATION', 'maj_plan', 'Mise � jour', 1, 'Mise � jour plan', '', '', 4, 'GESTIONPLANPASSATION'),
('MATDOSSIER', 'immatriculation_contrats', 'Demande Immatriculation', 1, 'Demande Immatriculation', '', '', 5, 'TDOSSIERSPPM'),
('MODELEDOCUMENT', 'ModeleDocument', 'Modele Document', 1, 'Modele Document', ' ', '   ', 39, 'TRAITEMENTDOSSIER'),
('MODEPASSATION', 'modepassation', 'Mode de passation', 1, 'Mode de passation', '', '', 13, 'REFERENTIEL'),
('MODERECEPTION', 'ModeReception', 'Mode de Reception', 1, 'Mode de Reception', ' ', ' ', 33, 'REFERENTIEL'),
('MODESELECTION', 'modeselection', 'Mode de sélection', 1, 'Mode de s�lection', '', '', 14, 'REFERENTIEL'),
('MODETRAITEMENT', 'ModeTraitement', 'Mode de Traitement', 1, 'Mode de Traitement', ' ', ' ', 34, 'REFERENTIEL'),
('MODULE', 'list_module', '', 1, 'Modules de formation', '', '', 6, 'FORMATION'),
('MONNAIE_OFFRE', 'monnaie', '', 1, 'Monnaie', '', '', 17, 'REFERENTIEL'),
('MONTANTSSEUILS', 'seuils_passation', '', 1, 'Passation', '', '', 1, 'SEUILS'),
('NATURECOURRIER', 'ListNatureCourrier', 'Nature Courrier', 1, 'Nature Courrier', ' ', ' ', 32, 'REFERENTIEL'),
('NAT_PRIX', 'natureprix', '', 1, 'Nature Prix', '', '', 18, 'REFERENTIEL'),
('NOUVEAUX_CONT', 'nouveaux_contentieux', 'Nouveaux Contentieux', 1, 'Nouveau Contentieux', '', '', 1, 'CONTENTIEUX'),
('NOUV_PLANPASSATION', 'nouv_plan', 'Nouveau Plan de Passation', 1, 'Nouveau Plan de Passation', '', '', 1, 'GESTIONPLANPASSATION'),
('NPROCEDUREDEROGATOIRE', 'demandeencours', 'Demande de Procédure Dérogatoire', 1, 'Demande de Proc�dure D�rogatoire', '   ', '  ', 42, 'PASSATIONMARCHES'),
('OPERATION', 'consultation_paiement', 'Consultation', 1, 'Consultation', '', '', 3, 'TAXEFISCALE'),
('OPERATIONDAO', 'consultation_recouvrement', 'Consultation Recouvrement', 1, 'Consultation Recouvrement', '', '', 3, 'VENTEDAO'),
('ORGUNITARMP', 'org_armp', 'Organigramme ARMP', 1, 'Organigramme ARMP', '', '', 2, 'ORGARMP'),
('PAIEMENT', 'nouveau_paiement', 'Nouveau Paiement', 1, 'Nouveau Paiement', '', '', 1, 'TAXEFISCALE'),
('PASSATIONSMARCHES', 'procedures_passations', 'Procédures de passation', 1, 'Proc�dures de passation', '', '', 1, 'PASSATIONMARCHES'),
('PAYS', 'listpays', '', 1, 'Pays', '', '', 20, 'REFERENTIEL'),
('PIECES', 'ListPieces', '', 1, 'Pi�ces', '', '', 20, 'REFERENTIEL'),
('PIECESREQUISES', 'Piecesrequises', 'Piéce Requises', 0, 'Pi�ce Requises', ' ', ' ', 37, 'TRAITEMENTDOSSIER'),
('PJCONT', 'piecejointe_contentieux', 'Piéces Jointes Contentieux', 0, 'Pi�ces Jointes Contentieux', '', '', 4, 'CONTENTIEUX'),
('PJDENONCIATION', '', 'Pi�ces jointes', 0, 'Pi�ces jointes', '', '', 21, 'DENONCIATION'),
('PLANAVALIDES', 'plans_avalider', 'Validation PPM', 1, 'Validation PPM', '', '', 3, 'GESTIONPLANPASSATION'),
('PLANPASSATION', 'plans', 'Plans de Passation', 1, 'Plans de Passation', '', '', 2, 'GESTIONPLANPASSATION'),
('PLANSPUBLIES', 'plans_publies', 'Plans publi�s', 1, 'Plans publi�s', '', '', 4, 'GESTIONPLANPASSATION'),
('POLEDCMP', 'poledcmp', 'Pôles de rattachement', 1, 'Pôles de rattachement', '', '', 6, 'REFERENTIEL'),
('PRESTATAIRES', 'ListPrestataire', '', 1, 'R�pertoire des auditeurs', '', '', 21, 'AUDITS'),
('PROCEDUREDEROGATOIRE', 'procedurederogatoire', 'Nouvelle Demande de Procédure Dérogatoire', 0, 'Nouvelle Demande de Proc�dure D�rogatoire', '  ', '   ', 42, 'PASSATIONMARCHES'),
('PROCEDURESPASSATIONS', '', 'Procédures de passation ', 0, 'Proc�dures de passation ', '', '', 1, 'PASSATIONMARCHES'),
('PRODUITS', 'saisi_prix', 'Produit/Service', 0, 'Produit/Service', '', '', 3, 'REFERENTIELPRIX'),
('QUITUS', 'editer_quitus', 'Editer Quitus', 1, 'Editer Quitus', '', '', 4, 'TAXEFISCALE'),
('RAPPORTARMP', 'rapport_armp', 'Rapport ARMP', 1, 'Rapport ARMP', '', '', 18, 'PUBLICATIONS'),
('RAPPORTDCMP', 'rapport_dcmp', 'Rapport DCMP', 1, 'Rapport DCMP', '', '', 16, 'PUBLICATIONS'),
('RAPPORTFORMATION', '', '', 0, 'Rapport', '', '', 5, 'FORMATION'),
('REALSATIONS', 'realisations', 'Réalisations', 0, 'R�alisations', '', '', 3, 'GESTIONPLANPASSATION'),
('REALSPLANS', 'realisations', 'R�alisations', 0, 'R�alisations', '', '', 6, 'GESTIONPLANPASSATION'),
('RECOUVREMENT', 'nouveau_recouvrement', 'Nouveau Recouvrement', 1, 'Nouveau Recouvrement', '', '', 1, 'VENTEDAO'),
('REFCATEGORIES', 'refprix_categories', 'Catégories', 1, 'Cat�gories', '   ', '   ', 1, 'REFERENTIELPRIX'),
('REFERENTIEL', 'referentiel', 'Référentiel', 1, 'R�f�rentiel', '', '', 1, 'REFERENTIEL'),
('REFERENTIELPRIX', 'referentielprix', 'Configuration Famille et des Produits/Services', 1, 'Configuration Famille et des Produits/Services', '   ', '   ', 2, 'REFERENTIELPRIX'),
('REF_CONT_DECISION', 'saisi_decision', 'Saisie Décision', 1, 'Saisie D�cision', '', '', 3, 'CONTENTIEUX'),
('REF_GLOSSAIRE', 'liste_glossaire', '', 1, 'Lexiques', 'Lexique', 'lexique', 3, 'PUBLICATIONS'),
('REF_JOURNAL', 'liste_journal_marches', '', 1, 'Journal des march�s publics', 'news of publc mar ', 'journal des marchés', 4, 'PUBLICATIONS'),
('REF_PARTFORMATION', '', '', 0, 'Participants', '', '', 1, 'FORMATION'),
('REF_TYPEDEMANDE', 'typedemande', 'Type Demande', 1, 'Type Demande', '  ', '   ', 40, 'REFERENTIEL'),
('REGCOMMUNAUTAIRE', 'reglementation_communautaire', 'Communautaire', 1, 'Communautaire', '', '', 2, 'REGLEMENTATION'),
('REGNATIONALE', 'reglementation_nationale', 'Nationale', 1, 'Nationale', '', '', 1, 'REGLEMENTATION'),
('REVUEDAO', 'revue_dao', 'Revue DAO', 1, 'Revue DAO', '', '', 1, 'TDOSSIERSPPM'),
('SAISIPRIX', 'saisi_prix', '', 1, 'Saisi Prix', '', '', 3, 'REFERENTIELPRIX'),
('SECTEURSACTIVITES', 'secteursactivites', 'Secteur', 1, 'Secteur', '', '', 15, 'REFERENTIEL'),
('SERVICEAC', 'services', 'Service', 0, 'Service', '', '', 4, 'REFERENTIEL'),
('SEUILSCOMMUNAUTAIRE', 'seuils_communautaire', '', 1, 'Publication', '', '', 3, 'SEUILS'),
('SEUILSRAPRIORI', 'seuils_revueaprori', '', 1, 'Contr�le �  priori', '', '', 2, 'SEUILS'),
('SPECIALITE', 'list_specialite', '', 1, 'Specialite', '', '', 5, 'FORMATION'),
('STATUEMOA', 'stat_periodique', 'Statistique P�riodique UEMOA ', 1, 'Statistique P�riodique UEMOA', '', '', 1, 'STATISTIQUE'),
('SUIVIDENONCIATION', 'nvel_denonciation', 'nouvelle dénonciation', 1, 'Nouvelle d�nonciation', ' ', ' ', 10, 'DENONCIATION'),
('SUIVIPAIEMENT', 'suivi_paiement', 'Suivi Paiement', 1, 'Suivi paiement', '', '', 2, 'TAXEFISCALE'),
('SUIVIRECOUVREMENT', 'suivi_recouvrement', 'Suivi Recouvrement', 1, 'Suivi Recouvrement', '', '', 1, 'VENTEDAO'),
('TAUXPARAFIXCALE', 'congiguration_taux', 'Configuration Taux', 1, 'Configuration Taux', '', '', 3, 'TAXEFISCALE'),
('TAUXVENTEDAO', 'congiguration_taux_ventedao', 'Configuration Taux', 1, 'Configuration Taux', '', '', 3, 'VENTEDAO'),
('TEXTE_REGLEMENTAIRE', 'list_texte_reglementaire', '', 1, 'Texte Reglementaire', 'Texte Reglementaire_EN', 'texte reglementaire_ES', 1, 'PUBLICATIONS'),
('TYPEAC', 'typeautoritecontractante', 'Types autorités contractantes', 1, 'Types autorit�s contractantes', '', '', 2, 'REFERENTIEL'),
('TYPECOURRIER', 'TypeCourrier', 'Type courrier', 1, 'Type Courrier', ' ', ' ', 31, 'REFERENTIEL'),
('TYPEDECISION', 'type_decision', 'Type décision', 1, 'Type d�cision', '', '', 21, 'REFERENTIEL'),
('TYPEMODELEDOCUMENT', 'TypeModeleDocument', 'Type Modele Document', 0, 'Type Modele Document', '  ', '  ', 38, 'TRAITEMENTDOSSIER'),
('TYPERAPPORT', 'type_rapport_formation', '', 1, 'Type rapport', '', '', 3, 'FORMATION'),
('TYPESDOSSIERS', 'TypesDossiers', 'Types Dossiers', 1, 'Types Dossiers', ' ', ' ', 35, 'TRAITEMENTDOSSIER'),
('TYPESERVICE', 'typeservice', 'Types Services', 1, 'Types Services', '', '', 5, 'REFERENTIEL'),
('TYPESMARCHES', 'typesmarches', 'Type de marchés', 1, 'Type de march�s', '', '', 12, 'REFERENTIEL'),
('TYPESMARCHESMP', '', '', 0, 'Modes de passations', '', '', 17, 'REFERENTIEL'),
('TYPESMARCHESMS', '', '', 0, 'Modes de s�lections', '', '', 16, 'REFERENTIEL'),
('TYPEUNITEORGARMP', 'type_unite_org_armp', 'Type Unité Orgonisationnelle ARMP', 1, 'Type Unit� Orgonisationnelle ARMP', '', '', 1, 'ORGARMP'),
('TYPEUNITEORGDCMP', 'type_unite_org', 'Type Unité Orgonisationnelle DCMP', 1, 'Type Unit� Orgonisationnelle DCMP', '', '', 1, 'ORGDCMP'),
('UNITEORGARMP', 'unite_org_armp', 'Organigramme ARMP', 0, 'Organigramme ARMP', '', '', 2, 'ORGARMP'),
('UTILISATEURAC', 'utilisateurs_ac', 'Comptes autorités contractantes ', 1, 'Comptes autorit�s contractantes ', '', '', 4, 'SECURITE'),
('UTILISATEURARMP', 'utilisateurs_armp', 'Comptes A.R.M.P', 1, 'Comptes A.R.M.P', '', '', 2, 'SECURITE'),
('UTILISATEURDCMP', 'utilisateurs_dcmp', 'Comptes D.N.C.MP', 1, 'Comptes D.N.C.M.P', '', '', 3, 'SECURITE'),
('VALDMDDEROG', 'validation_demandederogatoires', 'Validation demandes de proc�dures d�rogatoires', 1, 'Validation demandes de proc�dures d�rogatoires ', '', '', 43, 'PASSATIONMARCHES'),
('VALIDAP', 'validation_attributionprovisoire', 'Validation Attribution provisoire', 1, 'Validation Attribution provisoire', '', '', 2, 'TDOSSIERSPPM'),
('VALPUBPRIX', 'list_prix', 'Valiadation et Publication Prix', 1, 'Validation et Publication Prix', '', '', 4, 'REFERENTIELPRIX'),
('workflowmanager', 'workflowmanager', 'Gestion des processus', 1, 'Gestion des processus', '', '', 30, 'Securite');

-- --------------------------------------------------------

--
-- Structure de la table `sys_journal`
--

CREATE TABLE IF NOT EXISTS `sys_journal` (
  `JOU_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACT_LIBELLE` varchar(200) NOT NULL,
  `JOU_DATE` datetime NOT NULL,
  `JOU_DESC` varchar(255) NOT NULL,
  `USR_LOGIN` varchar(200) NOT NULL,
  PRIMARY KEY (`JOU_ID`),
  UNIQUE KEY `JOU_ID` (`JOU_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=795 ;

--
-- Contenu de la table `sys_journal`
--

INSERT INTO `sys_journal` (`JOU_ID`, `ACT_LIBELLE`, `JOU_DATE`, `JOU_DESC`, `USR_LOGIN`) VALUES
(1, 'Ajouter', '2012-09-22 14:44:02', 'Ajout plan de passation �  :22-09-2012', 'admin'),
(2, 'Ajouter', '2012-09-22 14:47:35', 'Ajout plan de passation �  :22-09-2012', 'admin'),
(3, 'Ajouter', '2012-09-22 23:13:48', 'Ajout réalisation �  :Sat Sep 22 23:13:48 WET 2012', 'admin'),
(4, 'Ajouter', '2012-09-22 23:15:38', 'Ajout réalisation �  :Sat Sep 22 23:15:38 WET 2012', 'admin'),
(5, 'Modifier', '2012-09-22 23:20:55', 'Modification réalisation  �  :Sat Sep 22 23:20:55 WET 2012', 'admin'),
(6, 'Supprimer', '2012-09-22 23:21:06', 'null :Sat Sep 22 23:21:06 WET 2012', 'admin'),
(7, 'Supprimer', '2012-09-22 23:23:51', 'null :Sat Sep 22 23:23:51 WET 2012', 'admin'),
(8, 'Ajouter', '2012-09-22 23:25:27', 'Ajout réalisation �  :Sat Sep 22 23:25:27 WET 2012', 'admin'),
(9, 'Ajouter', '2012-09-23 01:30:09', 'Ajout Source de financement �  :Sun Sep 23 01:30:09 WET 2012', 'admin'),
(10, 'Ajouter', '2012-09-23 01:30:09', 'Ajout Source de financement �  :Sun Sep 23 01:30:09 WET 2012', 'admin'),
(11, 'Ajouter', '2012-09-23 01:30:09', 'Ajout Source de financement �  :Sun Sep 23 01:30:09 WET 2012', 'admin'),
(12, 'Ajouter', '2012-09-23 01:30:17', 'Ajout Source de financement �  :Sun Sep 23 01:30:17 WET 2012', 'admin'),
(13, 'Ajouter', '2012-09-23 01:30:17', 'Ajout Source de financement �  :Sun Sep 23 01:30:17 WET 2012', 'admin'),
(14, 'Ajouter', '2012-09-23 01:30:17', 'Ajout Source de financement �  :Sun Sep 23 01:30:17 WET 2012', 'admin'),
(15, 'Ajouter', '2012-09-23 01:30:17', 'Ajout Source de financement �  :Sun Sep 23 01:30:17 WET 2012', 'admin'),
(16, 'Ajouter', '2012-09-23 01:30:17', 'Ajout Source de financement �  :Sun Sep 23 01:30:17 WET 2012', 'admin'),
(17, 'Ajouter', '2012-09-23 01:30:17', 'Ajout Source de financement �  :Sun Sep 23 01:30:17 WET 2012', 'admin'),
(18, 'Ajouter', '2012-09-23 01:30:17', 'Ajout Source de financement �  :Sun Sep 23 01:30:17 WET 2012', 'admin'),
(19, 'Ajouter', '2012-09-23 01:30:17', 'Ajout Source de financement �  :Sun Sep 23 01:30:17 WET 2012', 'admin'),
(20, 'Supprimer', '2012-09-23 01:30:45', 'Suppression Source de financement  �  :Sun Sep 23 01:30:45 WET 2012', 'admin'),
(21, 'Supprimer', '2012-09-23 01:30:45', 'Suppression Source de financement  �  :Sun Sep 23 01:30:45 WET 2012', 'admin'),
(22, 'Supprimer', '2012-09-23 01:30:45', 'Suppression Source de financement  �  :Sun Sep 23 01:30:45 WET 2012', 'admin'),
(23, 'Ajouter', '2012-09-24 23:02:00', 'Ajout réalisation �  :Mon Sep 24 23:02:00 WET 2012', 'admin'),
(24, 'Supprimer', '2012-09-24 23:02:37', 'Suppression réalisation  �  :Mon Sep 24 23:02:37 WET 2012', 'admin'),
(25, 'Supprimer', '2012-09-24 23:04:30', 'Suppression réalisation  �  :Mon Sep 24 23:04:30 WET 2012', 'admin'),
(26, 'Supprimer', '2012-09-24 23:05:25', 'Suppression réalisation  �  :Mon Sep 24 23:05:25 WET 2012', 'admin'),
(27, 'Modifier', '2012-09-25 08:44:23', 'Modification réalisation  �  :Tue Sep 25 08:44:23 WET 2012', 'admin'),
(28, 'Supprimer', '2012-09-25 08:44:23', 'Suppression Source de financement  �  :Tue Sep 25 08:44:23 WET 2012', 'admin'),
(29, 'Supprimer', '2012-09-25 08:44:23', 'Suppression Source de financement  �  :Tue Sep 25 08:44:23 WET 2012', 'admin'),
(30, 'Supprimer', '2012-09-25 08:44:23', 'Suppression Source de financement  �  :Tue Sep 25 08:44:23 WET 2012', 'admin'),
(31, 'Supprimer', '2012-09-25 08:44:23', 'Suppression Source de financement  �  :Tue Sep 25 08:44:23 WET 2012', 'admin'),
(32, 'Supprimer', '2012-09-25 08:44:23', 'Suppression Source de financement  �  :Tue Sep 25 08:44:23 WET 2012', 'admin'),
(33, 'Supprimer', '2012-09-25 08:44:24', 'Suppression Source de financement  �  :Tue Sep 25 08:44:24 WET 2012', 'admin'),
(34, 'Supprimer', '2012-09-25 08:44:24', 'Suppression Source de financement  �  :Tue Sep 25 08:44:24 WET 2012', 'admin'),
(35, 'Supprimer', '2012-09-25 08:44:24', 'Suppression Source de financement  �  :Tue Sep 25 08:44:24 WET 2012', 'admin'),
(36, 'Ajouter', '2012-09-25 08:44:24', 'Ajout Source de financement �  :Tue Sep 25 08:44:24 WET 2012', 'admin'),
(37, 'Ajouter', '2012-09-25 08:44:24', 'Ajout Source de financement �  :Tue Sep 25 08:44:24 WET 2012', 'admin'),
(38, 'Ajouter', '2012-09-25 08:44:24', 'Ajout Source de financement �  :Tue Sep 25 08:44:24 WET 2012', 'admin'),
(39, 'Ajouter', '2012-09-25 08:44:24', 'Ajout Source de financement �  :Tue Sep 25 08:44:24 WET 2012', 'admin'),
(40, 'Ajouter', '2012-09-25 08:44:24', 'Ajout Source de financement �  :Tue Sep 25 08:44:24 WET 2012', 'admin'),
(41, 'Ajouter', '2012-09-25 08:44:24', 'Ajout Source de financement �  :Tue Sep 25 08:44:24 WET 2012', 'admin'),
(42, 'Soumettre pour validation', '2012-09-25 20:59:28', 'Soumission pour validation du plan de passation n° P_MF_2012_0001 pour la gestion 2012 :25-09-2012', 'admin'),
(43, 'Ajouter', '2012-09-25 21:50:06', 'Ajout plan de passation �  :25-09-2012', 'admin'),
(44, 'Ajouter', '2012-09-25 22:16:41', 'Ajout réalisation �  :Tue Sep 25 22:16:41 WET 2012', 'admin'),
(45, 'Ajouter', '2012-09-25 22:19:31', 'Ajout réalisation �  :Tue Sep 25 22:19:31 WET 2012', 'admin'),
(46, 'Ajouter', '2012-09-25 22:19:47', 'Ajout Source de financement �  :Tue Sep 25 22:19:47 WET 2012', 'admin'),
(47, 'Ajouter', '2012-09-25 22:19:47', 'Ajout Source de financement �  :Tue Sep 25 22:19:47 WET 2012', 'admin'),
(48, 'Ajouter', '2012-09-25 22:19:47', 'Ajout Source de financement �  :Tue Sep 25 22:19:47 WET 2012', 'admin'),
(49, 'Ajouter', '2012-09-25 22:19:47', 'Ajout Source de financement �  :Tue Sep 25 22:19:47 WET 2012', 'admin'),
(50, 'Valider', '2012-09-26 10:19:11', 'Plan de passation des marchés N° P_MF_2012_0001 pour la gestion 2012Validé le :26-09-2012', 'admin'),
(51, 'Soumettre pour validation', '2012-09-26 10:25:22', 'Soumission pour validation du plan de passation n° P_MF_2012_0001 pour la gestion 2012 :26-09-2012', 'admin'),
(52, 'Valider', '2012-09-26 10:25:31', 'Plan de passation des marchés N° P_MF_2012_0001 pour la gestion 2012Validé le :26-09-2012', 'admin'),
(53, 'Publier', '2012-09-26 11:04:23', 'Plan de passation des marchés N° P_MF_2012_0001 pour la gestion 2012Publié le :26-09-2012', 'admin'),
(54, 'Ajouter', '2012-09-26 11:29:47', 'Ajout plan de passation �  :26-09-2012', 'admin'),
(55, 'Ajouter', '2012-09-27 22:43:37', 'Ajout Mode de sélection �  :Thu Sep 27 22:43:37 WET 2012', 'admin'),
(56, 'Ajouter', '2012-09-27 22:43:43', 'Ajout Mode de sélection �  :Thu Sep 27 22:43:43 WET 2012', 'admin'),
(57, 'Supprimer', '2012-09-27 22:43:47', 'Suppression Mode de sélection  �  :Thu Sep 27 22:43:47 WET 2012', 'admin'),
(58, 'Ajouter', '2012-09-27 23:45:40', 'Ajout Mode de sélection �  :Thu Sep 27 23:45:40 WET 2012', 'admin'),
(59, 'Supprimer', '2012-10-05 10:36:26', 'Suppression Fonction  �  :Fri Oct 05 10:36:26 WET 2012', 'admin'),
(60, 'Supprimer', '2012-10-05 10:37:55', 'Suppression Fonction  �  :Fri Oct 05 10:37:55 WET 2012', 'admin'),
(61, 'Modifier utilisateur D.N.C.M.P', '2012-10-09 22:15:46', 'Editer utilisateur D.N.C.M.P � Tue Oct 09 22:15:46 WET 2012', 'admin'),
(62, 'Modifier utilisateur AC', '2012-10-09 22:18:34', 'Editer utilisateur A.C � Tue Oct 09 22:18:34 WET 2012', 'admin'),
(63, 'Ajouter utilisateur AC', '2012-10-09 22:23:21', 'Ajout utilisateur A.C � Tue Oct 09 22:23:21 WET 2012', 'admin'),
(64, 'Modifier utilisateur AC', '2012-10-09 23:34:44', 'Editer utilisateur A.C � Tue Oct 09 23:34:44 WET 2012', 'dcmp'),
(65, 'Ajouter utilisateur AC', '2012-10-09 23:36:34', 'Ajout utilisateur A.C � Tue Oct 09 23:36:34 WET 2012', 'dcmp'),
(66, 'Ajouter', '2012-10-09 23:39:08', 'Ajout plan de passation �  :09-10-2012', 'mat'),
(67, 'Ajouter utilisateur AC', '2012-10-10 08:39:23', 'Ajout utilisateur A.C � Wed Oct 10 08:39:23 WET 2012', 'admin'),
(68, 'Ajouter utilisateur AC', '2012-10-10 08:45:20', 'Ajout utilisateur A.C � Wed Oct 10 08:45:20 WET 2012', 'dcmp'),
(69, 'Modifier utilisateur AC', '2012-10-10 11:14:56', 'Editer utilisateur A.C � Wed Oct 10 11:14:56 WET 2012', 'mef'),
(70, 'Ajouter utilisateur AC', '2012-10-10 12:18:03', 'Ajout utilisateur A.C � Wed Oct 10 12:18:03 WET 2012', 'admin'),
(71, 'Ajouter utilisateur AC', '2012-10-10 12:18:26', 'Ajout utilisateur A.C � Wed Oct 10 12:18:26 WET 2012', 'admin'),
(72, 'Ajouter utilisateur AC', '2012-10-10 12:20:31', 'Ajout utilisateur A.C � Wed Oct 10 12:20:31 WET 2012', 'mef'),
(73, 'Ajouter', '2012-10-10 15:09:18', 'Ajout plan de passation �  :10-10-2012', 'mef'),
(74, 'Ajouter', '2012-10-10 20:39:36', 'Ajout réalisation �  :Wed Oct 10 20:39:36 WET 2012', 'mef'),
(75, 'Ajouter', '2012-10-10 22:48:25', 'Ajout réalisation �  :Wed Oct 10 22:48:25 WET 2012', 'mef'),
(76, 'Ajouter', '2012-10-10 23:02:06', 'Ajout réalisation �  :Wed Oct 10 23:02:06 WET 2012', 'mef'),
(77, 'Ajouter', '2012-10-10 23:12:30', 'Ajout réalisation �  :Wed Oct 10 23:12:30 WET 2012', 'mef'),
(78, 'Ajouter', '2012-10-10 23:17:15', 'Ajout réalisation �  :Wed Oct 10 23:17:14 WET 2012', 'mef'),
(79, 'Ajouter', '2012-10-10 23:19:24', 'Ajout réalisation �  :Wed Oct 10 23:19:24 WET 2012', 'mef'),
(80, 'Ajouter', '2012-10-10 23:21:12', 'Ajout réalisation �  :Wed Oct 10 23:21:03 WET 2012', 'mef'),
(81, 'Soumettre pour validation', '2012-10-11 11:36:45', 'Soumission pour validation du plan de passation n° P_MF_2012_1 pour la gestion 2012 :11-10-2012', 'mef'),
(82, 'Valider', '2012-10-11 11:37:56', 'Plan de passation des marchés N° P_MF_2012_1 pour la gestion 2012Validé le :11-10-2012', 'dcmp'),
(83, 'Publier', '2012-10-11 11:46:12', 'Plan de passation des marchés N° P_MF_2012_1 pour la gestion 2012Publié le :11-10-2012', 'dcmp'),
(84, 'Ajouter', '2012-10-13 11:12:25', 'Ajout plan de passation �  :13-10-2012', 'mef'),
(85, 'Ajouter', '2012-10-13 23:28:20', 'Ajout utilisateur A.C � Sat Oct 13 23:28:20 WET 2012', 'dcmp'),
(86, 'Ajouter', '2012-10-13 23:35:49', 'Ajout utilisateur A.C � Sat Oct 13 23:35:49 WET 2012', 'admin'),
(87, 'Modifier', '2012-10-15 09:33:59', 'Editer utilisateur A.C � Mon Oct 15 09:33:59 WET 2012', 'dcmp'),
(88, 'Ajouter', '2012-10-15 09:38:07', 'Ajout utilisateur A.C � Mon Oct 15 09:38:07 WET 2012', 'dcmp'),
(89, 'Ajouter', '2012-10-15 09:44:35', 'Ajout utilisateur A.C � Mon Oct 15 09:44:35 WET 2012', 'mef'),
(90, 'Ajouter', '2012-10-18 21:34:56', 'Ajout plan de passation �  :18-10-2012', 'mat'),
(91, 'Ajouter', '2012-10-18 21:35:46', 'Ajout réalisation �  :Thu Oct 18 21:35:46 WET 2012', 'mat'),
(92, 'Ajouter', '2012-10-18 21:43:26', 'Ajout réalisation �  :Thu Oct 18 21:43:26 WET 2012', 'mat'),
(93, 'Soumettre pour validation', '2012-10-19 14:32:18', 'Soumission pour validation du plan de passation n° P_MAT_2012_1 pour la gestion 2012 :19-10-2012', 'mat'),
(94, 'Valider', '2012-10-19 14:32:59', 'Plan de passation des marchés N° P_MAT_2012_1 pour la gestion 2012Validé le :19-10-2012', 'dcmp'),
(95, 'Publier', '2012-10-19 14:33:09', 'Plan de passation des marchés N° P_MAT_2012_1 pour la gestion 2012Publié le :19-10-2012', 'dcmp'),
(96, 'Ajouter', '2012-10-23 07:30:21', 'Nouveau type service ajouter �  :Tue Oct 23 07:30:21 WET 2012', 'armp'),
(97, 'Modifier', '2012-10-23 07:30:27', 'Modification type service �  :Tue Oct 23 07:30:27 WET 2012', 'armp'),
(98, 'Supprimer', '2012-10-23 07:30:30', 'Suppression type service �  :Tue Oct 23 07:30:30 WET 2012', 'armp'),
(99, 'Ajouter', '2012-10-23 08:35:31', 'Nouveau type service ajouter �  :Tue Oct 23 08:35:31 WET 2012', 'armp'),
(100, 'Modifier', '2012-10-23 08:35:36', 'Modification type service �  :Tue Oct 23 08:35:36 WET 2012', 'armp'),
(101, 'Supprimer', '2012-10-23 08:35:38', 'Suppression type service �  :Tue Oct 23 08:35:38 WET 2012', 'armp'),
(102, 'Ajouter', '2012-10-31 16:34:04', 'Ajout Mode de passation �  :Wed Oct 31 16:34:04 WET 2012', 'admin'),
(103, 'Ajouter', '2012-10-31 16:34:04', 'Ajout Mode de passation �  :Wed Oct 31 16:34:04 WET 2012', 'admin'),
(104, 'Ajouter', '2012-10-31 16:34:04', 'Ajout Mode de passation �  :Wed Oct 31 16:34:04 WET 2012', 'admin'),
(105, 'Ajouter', '2012-10-31 16:34:04', 'Ajout Mode de passation �  :Wed Oct 31 16:34:04 WET 2012', 'admin'),
(106, 'Ajouter', '2012-10-31 16:36:42', 'Ajout Mode de passation �  :Wed Oct 31 16:36:42 WET 2012', 'admin'),
(107, 'Ajouter', '2012-10-31 16:38:06', 'Ajout Mode de passation �  :Wed Oct 31 16:38:06 WET 2012', 'admin'),
(108, 'Ajouter', '2012-10-31 16:38:57', 'Ajout Mode de passation �  :Wed Oct 31 16:38:57 WET 2012', 'admin'),
(109, 'Ajouter', '2012-10-31 16:38:57', 'Ajout Mode de passation �  :Wed Oct 31 16:38:57 WET 2012', 'admin'),
(110, 'Ajouter', '2012-10-31 16:39:05', 'Ajout Mode de passation �  :Wed Oct 31 16:39:05 WET 2012', 'admin'),
(111, 'Ajouter', '2012-10-31 16:39:41', 'Ajout Mode de passation �  :Wed Oct 31 16:39:41 WET 2012', 'admin'),
(112, 'Ajouter', '2012-10-31 16:39:47', 'Ajout Mode de passation �  :Wed Oct 31 16:39:47 WET 2012', 'admin'),
(113, 'Ajouter', '2012-10-31 16:40:18', 'Ajout Mode de passation �  :Wed Oct 31 16:40:18 WET 2012', 'admin'),
(114, 'Ajouter', '2012-10-31 16:40:18', 'Ajout Mode de passation �  :Wed Oct 31 16:40:18 WET 2012', 'admin'),
(115, 'Ajouter', '2012-10-31 16:40:24', 'Ajout Mode de passation �  :Wed Oct 31 16:40:24 WET 2012', 'admin'),
(116, 'Ajouter', '2012-10-31 17:04:22', 'Ajout Mode de passation �  :Wed Oct 31 17:04:22 WET 2012', 'admin'),
(117, 'Ajouter', '2012-10-31 17:05:43', 'Ajout Mode de passation �  :Wed Oct 31 17:05:43 WET 2012', 'admin'),
(118, 'Modifier', '2012-10-31 17:05:58', 'Modification Mode de passation  �  :Wed Oct 31 17:05:58 WET 2012', 'admin'),
(119, 'Modifier', '2012-10-31 17:07:19', 'Modification Mode de passation  �  :Wed Oct 31 17:07:19 WET 2012', 'admin'),
(120, 'Modifier', '2012-10-31 17:07:36', 'Modification Mode de passation  �  :Wed Oct 31 17:07:36 WET 2012', 'admin'),
(121, 'Modifier', '2012-10-31 17:07:49', 'Modification Mode de passation  �  :Wed Oct 31 17:07:49 WET 2012', 'admin'),
(122, 'Modifier', '2012-10-31 17:08:06', 'Modification Mode de passation  �  :Wed Oct 31 17:08:06 WET 2012', 'admin'),
(123, 'Modifier', '2012-10-31 17:11:04', 'Modification Mode de passation  �  :Wed Oct 31 17:11:04 WET 2012', 'admin'),
(124, 'Ajouter', '2012-10-31 17:23:03', 'Ajout réalisation �  :Wed Oct 31 17:23:03 WET 2012', 'admin'),
(125, 'Supprimer', '2012-11-01 12:13:55', 'Suppression Mode de passation  �  :Thu Nov 01 12:13:55 WET 2012', 'mef'),
(126, 'Ajouter', '2012-11-01 12:14:32', 'Ajout réalisation �  :Thu Nov 01 12:14:32 WET 2012', 'mef'),
(127, 'Ajouter', '2012-11-01 12:28:20', 'Ajout réalisation �  :Thu Nov 01 12:28:20 WET 2012', 'mef'),
(128, 'Ajouter', '2012-11-01 12:33:00', 'Ajout réalisation �  :Thu Nov 01 12:33:00 WET 2012', 'mef'),
(129, 'Soumettre pour validation', '2012-11-01 13:29:50', 'Soumission pour validation du plan de passation n° P_MF_2012_2 pour la gestion 2012 :01-11-2012', 'mef'),
(130, 'Valider', '2012-11-01 13:30:21', 'Plan de passation des marchés N° P_MF_2012_2 pour la gestion 2012Validé le :01-11-2012', 'dcmp'),
(131, 'Publier', '2012-11-01 13:30:31', 'Plan de passation des marchés N° P_MF_2012_2 pour la gestion 2012Publié le :01-11-2012', 'dcmp'),
(132, 'Ajouter', '2012-11-01 14:02:37', 'Ajout Mode de sélection �  :Thu Nov 01 14:02:37 WET 2012', 'mef'),
(133, 'Ajouter', '2012-11-01 14:02:45', 'Ajout Mode de sélection �  :Thu Nov 01 14:02:45 WET 2012', 'mef'),
(134, 'Ajouter', '2012-11-01 14:02:45', 'Ajout Mode de sélection �  :Thu Nov 01 14:02:45 WET 2012', 'mef'),
(135, 'Supprimer', '2012-11-05 15:47:39', 'Suppression contentieux �  :Mon Nov 05 15:47:39 WET 2012', 'armp'),
(136, 'Soumettre pour validation', '2012-11-09 16:34:29', 'Soumission pour validation du plan de passation n\\u00B0 P_MF_2012_1 pour la gestion 2012 :09-11-2012', 'mef'),
(137, 'Valider', '2012-11-09 16:34:58', 'Plan de passation des march\\u00E9s N\\u00B0 P_MF_2012_1 pour la gestion 2012Validé le :09-11-2012', 'dcmp'),
(138, 'Ajouter', '2012-11-20 17:48:25', 'Ajout Banque �  :Tue Nov 20 17:48:25 WET 2012', 'armp'),
(139, 'Modifier', '2012-11-20 17:48:49', 'Modification Banque �  :Tue Nov 20 17:48:49 WET 2012', 'armp'),
(140, 'Modifier', '2012-11-20 17:48:55', 'Modification Banque �  :Tue Nov 20 17:48:55 WET 2012', 'armp'),
(141, 'Ajouter', '2012-11-20 17:51:20', 'Ajout Banque �  :Tue Nov 20 17:51:20 WET 2012', 'armp'),
(142, 'Modifier', '2012-11-20 17:51:29', 'Modification Banque �  :Tue Nov 20 17:51:29 WET 2012', 'armp'),
(143, 'Modifier', '2012-11-20 17:51:36', 'Modification Banque �  :Tue Nov 20 17:51:36 WET 2012', 'armp'),
(144, 'Ajouter', '2012-11-20 17:51:47', 'Ajout Banque �  :Tue Nov 20 17:51:47 WET 2012', 'armp'),
(145, 'Supprimer', '2012-11-20 17:51:50', 'Suppression Banque �  :Tue Nov 20 17:51:50 WET 2012', 'armp'),
(146, 'Supprimer', '2012-11-21 14:34:42', 'Suppression Réglementation �  :Wed Nov 21 14:34:42 WET 2012', 'armp'),
(147, 'Ajouter', '2012-11-26 10:58:28', 'Nouvelle autorité ajouter �  :Mon Nov 26 10:58:28 WET 2012', 'dcmp'),
(148, 'Ajouter', '2012-11-26 16:44:33', 'Ajout utilisateur D.N.C.M.P � Mon Nov 26 16:44:33 WET 2012', 'dcmp'),
(149, 'Ajouter', '2012-11-26 18:24:19', 'Nouveau service ajouter �  :Mon Nov 26 18:24:19 WET 2012', 'dcmp'),
(150, 'Ajouter', '2012-11-26 18:26:21', 'Ajout utilisateur A.C � Mon Nov 26 18:26:21 WET 2012', 'dcmp'),
(151, 'Ajouter', '2012-11-26 18:29:53', 'Ajout plan de passation �  :26-11-2012', 'astaba'),
(152, 'Ajouter', '2012-11-28 10:38:45', 'Nouveau service ajouter �  :Wed Nov 28 10:38:45 WET 2012', 'dcmp'),
(153, 'Ajouter', '2012-11-28 10:39:14', 'Nouveau service ajouter �  :Wed Nov 28 10:39:14 WET 2012', 'dcmp'),
(154, 'Ajouter', '2012-11-28 10:39:27', 'Nouveau service ajouter �  :Wed Nov 28 10:39:27 WET 2012', 'dcmp'),
(155, 'Modifier', '2012-11-28 10:39:37', 'Modification service �  :Wed Nov 28 10:39:37 WET 2012', 'dcmp'),
(156, 'Supprimer', '2012-11-28 10:39:47', 'Suppression service �  :Wed Nov 28 10:39:47 WET 2012', 'dcmp'),
(157, 'Ajouter', '2012-11-28 10:55:02', 'Ajout réalisation �  :Wed Nov 28 10:55:02 WET 2012', 'astaba'),
(158, 'Ajouter', '2012-11-28 11:01:07', 'Ajout réalisation �  :Wed Nov 28 11:01:07 WET 2012', 'astaba'),
(159, 'Soumettre pour validation', '2012-11-28 18:51:00', 'Soumission pour validation du plan de passation n° P_AIBD_2012_1 pour la gestion 2012 :28-11-2012', 'astaba'),
(160, 'Valider', '2012-11-28 18:51:55', 'Plan de passation des marchés N° P_AIBD_2012_1 pour la gestion 2012VAL :28-11-2012', 'dcmp'),
(161, 'Publier', '2012-11-29 13:02:37', 'Plan de passation des marchés N° P_AIBD_2012_1 pour la gestion 2012PUB :29-11-2012', 'dcmp'),
(162, 'Ajouter', '2012-11-29 17:18:38', 'Ajout Rapport �  :Thu Nov 29 17:18:38 WET 2012', 'armp'),
(163, 'Modifier', '2012-11-29 17:19:25', 'Modification Rapport �  :Thu Nov 29 17:19:25 WET 2012', 'armp'),
(164, 'Ajouter', '2012-11-29 17:45:53', 'Ajout Rapport �  :Thu Nov 29 17:45:53 WET 2012', 'armp'),
(165, 'Supprimer', '2012-11-30 09:08:17', 'Suppression Rapport �  :Fri Nov 30 09:08:17 WET 2012', 'armp'),
(166, 'Ajouter', '2012-11-30 09:11:46', 'Ajout Rapport �  :Fri Nov 30 09:11:46 WET 2012', 'armp'),
(167, 'Supprimer', '2012-11-30 10:05:13', 'Suppression Rapport �  :Fri Nov 30 10:05:13 WET 2012', 'armp'),
(168, 'Ajouter', '2012-11-30 10:08:36', 'Ajout Rapport �  :Fri Nov 30 10:08:36 WET 2012', 'armp'),
(169, 'Ajouter', '2012-11-30 15:18:50', 'Ajout Type Unité Orgonisationnelle �  :Fri Nov 30 15:18:49 WET 2012', 'dcmp'),
(170, 'Ajouter', '2012-11-30 15:20:49', 'Ajout Type Unité Orgonisationnelle �  :Fri Nov 30 15:20:49 WET 2012', 'dcmp'),
(171, 'Ajouter', '2012-11-30 15:31:28', 'Ajout Type Unité Orgonisationnelle �  :Fri Nov 30 15:31:28 WET 2012', 'dcmp'),
(172, 'Modifier', '2012-11-30 15:31:40', 'Modification Type Unité Orgonisationnelle �  :Fri Nov 30 15:31:40 WET 2012', 'dcmp'),
(173, 'Modifier', '2012-11-30 15:31:48', 'Modification Type Unité Orgonisationnelle �  :Fri Nov 30 15:31:48 WET 2012', 'dcmp'),
(174, 'Modifier', '2012-11-30 15:32:04', 'Modification Type Unité Orgonisationnelle �  :Fri Nov 30 15:32:04 WET 2012', 'dcmp'),
(175, 'Supprimer', '2012-11-30 15:32:15', 'Suppression Type Unité Orgonisationnelle �  :Fri Nov 30 15:32:15 WET 2012', 'dcmp'),
(176, 'Supprimer', '2012-11-30 15:32:25', 'Suppression Type Unité Orgonisationnelle �  :Fri Nov 30 15:32:25 WET 2012', 'dcmp'),
(177, 'Supprimer', '2012-11-30 15:32:47', 'Suppression Type Unité Orgonisationnelle �  :Fri Nov 30 15:32:47 WET 2012', 'dcmp'),
(178, 'Supprimer', '2012-11-30 15:32:47', 'Suppression Type Unité Orgonisationnelle �  :Fri Nov 30 15:32:47 WET 2012', 'dcmp'),
(179, 'Supprimer', '2012-11-30 15:33:18', 'Suppression Type Unité Orgonisationnelle �  :Fri Nov 30 15:33:18 WET 2012', 'dcmp'),
(180, 'Ajouter', '2012-11-30 15:34:29', 'Ajout Type Unité Orgonisationnelle �  :Fri Nov 30 15:34:29 WET 2012', 'dcmp'),
(181, 'Supprimer', '2012-11-30 15:35:37', 'Suppression Type Unité Orgonisationnelle �  :Fri Nov 30 15:35:37 WET 2012', 'dcmp'),
(182, 'Supprimer', '2012-11-30 15:36:14', 'Suppression Type Unité Orgonisationnelle �  :Fri Nov 30 15:36:14 WET 2012', 'dcmp'),
(183, 'Supprimer', '2012-11-30 15:36:18', 'Suppression Type Unité Orgonisationnelle �  :Fri Nov 30 15:36:18 WET 2012', 'dcmp'),
(184, 'Ajouter', '2012-12-01 14:40:06', 'Ajout Bureau de la D.N.C.M.P  �  :Sat Dec 01 14:40:06 WET 2012', 'dcmp'),
(185, 'Modifier', '2012-12-01 14:41:02', 'Modification Bureau de la D.N.C.M.P  �  :Sat Dec 01 14:41:02 WET 2012', 'dcmp'),
(186, 'Modifier', '2012-12-01 14:41:10', 'Modification Bureau de la D.N.C.M.P  �  :Sat Dec 01 14:41:10 WET 2012', 'dcmp'),
(187, 'Supprimer', '2012-12-01 14:41:26', 'Suppression Bureau de la D.N.C.M.P  �  :Sat Dec 01 14:41:26 WET 2012', 'dcmp'),
(188, 'Ajouter', '2012-12-01 14:42:12', 'Ajout Bureau de la D.N.C.M.P  �  :Sat Dec 01 14:42:12 WET 2012', 'dcmp'),
(189, 'Ajouter', '2012-12-01 14:44:50', 'Ajout Type Unité Orgonisationnelle �  :Sat Dec 01 14:44:50 WET 2012', 'dcmp'),
(190, 'Ajouter', '2012-12-01 14:45:58', 'Ajout Bureau de la D.N.C.M.P  �  :Sat Dec 01 14:45:58 WET 2012', 'dcmp'),
(191, 'Modifier', '2012-12-03 11:55:05', 'Modification Type Unité Orgonisationnelle �  :Mon Dec 03 11:55:05 WET 2012', 'dcmp'),
(192, 'Ajouter', '2012-12-03 11:56:43', 'Ajout Type Unité Orgonisationnelle �  :Mon Dec 03 11:56:43 WET 2012', 'dcmp'),
(193, 'Modifier', '2012-12-03 11:57:01', 'Modification Type Unité Orgonisationnelle �  :Mon Dec 03 11:57:01 WET 2012', 'dcmp'),
(194, 'Ajouter', '2012-12-03 13:29:57', 'Ajout Type Unité Orgonisationnelle �  :Mon Dec 03 13:29:57 WET 2012', 'dcmp'),
(195, 'Supprimer', '2012-12-03 14:43:08', 'Suppression Type Unité Orgonisationnelle �  :Mon Dec 03 14:43:08 WET 2012', 'dcmp'),
(196, 'Ajouter', '2012-12-03 14:43:16', 'Ajout Type Unité Orgonisationnelle �  :Mon Dec 03 14:43:16 WET 2012', 'dcmp'),
(197, 'Ajouter', '2012-12-03 14:45:36', 'Ajout Type Unité Orgonisationnelle �  :Mon Dec 03 14:45:36 WET 2012', 'dcmp'),
(198, 'Ajouter', '2012-12-03 14:46:07', 'Ajout Type Unité Orgonisationnelle �  :Mon Dec 03 14:46:07 WET 2012', 'dcmp'),
(199, 'Ajouter', '2012-12-03 14:53:24', 'Ajout Type Unité Orgonisationnelle �  :Mon Dec 03 14:53:24 WET 2012', 'dcmp'),
(200, 'Ajouter', '2012-12-03 14:53:41', 'Ajout Type Unité Orgonisationnelle �  :Mon Dec 03 14:53:41 WET 2012', 'dcmp'),
(201, 'Ajouter', '2012-12-03 14:54:09', 'Ajout Type Unité Orgonisationnelle �  :Mon Dec 03 14:54:09 WET 2012', 'dcmp'),
(202, 'Ajouter', '2012-12-03 14:55:54', 'Ajout Type Unité Orgonisationnelle �  :Mon Dec 03 14:55:54 WET 2012', 'dcmp'),
(203, 'Modifier', '2012-12-03 15:17:42', 'Modification Bureau de la D.N.C.M.P  �  :Mon Dec 03 15:17:42 WET 2012', 'dcmp'),
(204, 'Ajouter', '2012-12-04 12:14:05', 'Ajout Bureau de la D.N.C.M.P  �  :Tue Dec 04 12:14:05 WET 2012', 'dcmp'),
(205, 'Ajouter', '2012-12-04 12:23:34', 'Ajout Bureau de la D.N.C.M.P  �  :Tue Dec 04 12:23:34 WET 2012', 'dcmp'),
(206, 'Modifier', '2012-12-04 12:24:43', 'Modification Bureau de la D.N.C.M.P  �  :Tue Dec 04 12:24:43 WET 2012', 'dcmp'),
(207, 'Ajouter', '2012-12-04 12:25:41', 'Ajout Bureau de la D.N.C.M.P  �  :Tue Dec 04 12:25:41 WET 2012', 'dcmp'),
(208, 'Ajouter', '2012-12-04 12:54:58', 'Ajout Bureau de la D.N.C.M.P  �  :Tue Dec 04 12:54:58 WET 2012', 'dcmp'),
(209, 'Ajouter', '2012-12-04 12:59:20', 'Ajout Bureau de la D.N.C.M.P  �  :Tue Dec 04 12:59:20 WET 2012', 'dcmp'),
(210, 'Ajouter', '2012-12-04 12:59:59', 'Ajout Bureau de la D.N.C.M.P  �  :Tue Dec 04 12:59:59 WET 2012', 'dcmp'),
(211, 'Ajouter', '2012-12-04 13:50:28', 'Ajout Bureau de la D.N.C.M.P  �  :Tue Dec 04 13:50:28 WET 2012', 'dcmp'),
(212, 'Ajouter', '2012-12-04 13:55:23', 'Ajout Bureau de la D.N.C.M.P  �  :Tue Dec 04 13:55:23 WET 2012', 'dcmp'),
(213, 'Ajouter', '2012-12-04 14:02:25', 'Ajout Bureau de la D.N.C.M.P  �  :Tue Dec 04 14:02:25 WET 2012', 'dcmp'),
(214, 'Ajouter', '2012-12-04 14:39:28', 'Ajout Bureau de la D.N.C.M.P  �  :Tue Dec 04 14:39:28 WET 2012', 'dcmp'),
(215, 'Ajouter', '2012-12-04 15:07:04', 'Ajout Type Unité Orgonisationnelle �  :Tue Dec 04 15:07:04 WET 2012', 'armp'),
(216, 'Ajouter', '2012-12-04 15:07:27', 'Ajout Type Unité Orgonisationnelle �  :Tue Dec 04 15:07:27 WET 2012', 'armp'),
(217, 'Modifier', '2012-12-04 15:13:55', 'Modification Type Unité Orgonisationnelle �  :Tue Dec 04 15:13:55 WET 2012', 'armp'),
(218, 'Ajouter', '2012-12-04 15:14:16', 'Ajout Type Unité Orgonisationnelle �  :Tue Dec 04 15:14:16 WET 2012', 'armp'),
(219, 'Modifier', '2012-12-04 15:14:35', 'Modification Type Unité Orgonisationnelle �  :Tue Dec 04 15:14:35 WET 2012', 'armp'),
(220, 'Modifier', '2012-12-04 15:15:07', 'Modification Type Unité Orgonisationnelle �  :Tue Dec 04 15:15:07 WET 2012', 'armp'),
(221, 'Ajouter', '2012-12-04 16:43:42', 'Ajout Unité Orgonisationnelle ARMP �  :Tue Dec 04 16:43:42 WET 2012', 'armp'),
(222, 'Ajouter', '2012-12-04 16:44:18', 'Ajout Unité Orgonisationnelle ARMP �  :Tue Dec 04 16:44:18 WET 2012', 'armp'),
(223, 'Supprimer', '2012-12-04 16:54:16', 'Suppression Unité Orgonisationnelle ARMP �  :Tue Dec 04 16:54:16 WET 2012', 'armp'),
(224, 'Ajouter', '2012-12-04 16:54:40', 'Ajout Unité Orgonisationnelle ARMP �  :Tue Dec 04 16:54:40 WET 2012', 'armp'),
(225, 'Supprimer', '2012-12-05 09:37:47', 'Suppression contentieux �  :Wed Dec 05 09:37:47 WET 2012', 'armp'),
(226, 'Ajouter', '2012-12-05 10:00:04', 'Ajout Rapport �  :Wed Dec 05 10:00:04 WET 2012', 'armp'),
(227, 'Supprimer', '2012-12-05 10:23:01', 'Suppression Bureau de la D.N.C.M.P  �  :Wed Dec 05 10:23:01 WET 2012', 'dcmp'),
(228, 'Ajouter', '2012-12-05 10:42:53', 'Ajout Rapport �  :Wed Dec 05 10:42:53 WET 2012', 'dcmp'),
(229, 'Modifier', '2012-12-05 10:50:37', 'Modification Rapport �  :Wed Dec 05 10:50:37 WET 2012', 'dcmp'),
(230, 'Supprimer', '2012-12-05 10:51:58', 'Suppression Rapport �  :Wed Dec 05 10:51:58 WET 2012', 'dcmp'),
(231, 'Ajouter', '2012-12-05 10:52:48', 'Ajout Rapport �  :Wed Dec 05 10:52:48 WET 2012', 'dcmp'),
(232, 'Ajouter', '2012-12-08 12:51:22', 'Ajout plan de passation �  :08-12-2012', 'astaba'),
(233, 'Ajouter', '2012-12-21 14:32:22', 'null :Fri Dec 21 14:32:22 WET 2012', 'dcmp'),
(234, 'Supprimer', '2012-12-21 14:34:09', 'null :Fri Dec 21 14:34:09 WET 2012', 'dcmp'),
(235, 'Ajouter', '2013-01-07 09:42:10', 'Ajout Bureau de la D.N.C.M.P  �  :Mon Jan 07 09:42:10 GMT 2013', 'dcmp'),
(236, 'Ajouter', '2013-01-07 09:45:12', 'Ajout Bureau de la D.N.C.M.P  �  :Mon Jan 07 09:45:12 GMT 2013', 'dcmp'),
(237, 'Ajouter', '2013-01-07 09:45:46', 'Ajout Bureau de la D.N.C.M.P  �  :Mon Jan 07 09:45:46 GMT 2013', 'dcmp'),
(238, 'Supprimer', '2013-01-07 09:54:58', 'Suppression Bureau de la D.N.C.M.P  �  :Mon Jan 07 09:54:58 GMT 2013', 'dcmp'),
(239, 'Ajouter', '2013-01-07 10:28:24', 'Ajout Bureau de la D.N.C.M.P  �  :Mon Jan 07 10:28:24 GMT 2013', 'dcmp'),
(240, 'Ajouter', '2013-01-07 10:32:54', 'Ajout Bureau de la D.N.C.M.P  �  :Mon Jan 07 10:32:54 GMT 2013', 'dcmp'),
(241, 'Modifier', '2013-01-07 10:35:05', 'Modification Bureau de la D.N.C.M.P  �  :Mon Jan 07 10:35:05 GMT 2013', 'dcmp'),
(242, 'Modifier', '2013-01-07 10:35:58', 'Modification Bureau de la D.N.C.M.P  �  :Mon Jan 07 10:35:58 GMT 2013', 'dcmp'),
(243, 'Ajouter', '2013-01-07 10:37:27', 'Ajout Bureau de la D.N.C.M.P  �  :Mon Jan 07 10:37:27 GMT 2013', 'dcmp'),
(244, 'Supprimer', '2013-01-07 10:38:03', 'Suppression Bureau de la D.N.C.M.P  �  :Mon Jan 07 10:38:03 GMT 2013', 'dcmp'),
(245, 'Ajouter', '2013-01-07 11:00:58', 'Ajout Bureau de la D.N.C.M.P  �  :Mon Jan 07 11:00:58 GMT 2013', 'dcmp'),
(246, 'Supprimer', '2013-01-07 11:01:02', 'Suppression Bureau de la D.N.C.M.P  �  :Mon Jan 07 11:01:02 GMT 2013', 'dcmp'),
(247, 'Ajouter', '2013-01-08 10:56:35', 'Ajout Rapport �  :Tue Jan 08 10:56:35 WET 2013', 'armp'),
(248, 'Supprimer', '2013-01-09 14:52:24', 'Suppression Catégorie  �  :Wed Jan 09 14:52:24 WET 2013', 'armp'),
(249, 'Ajouter', '2013-01-09 14:52:54', 'Ajout Catégorie  �  :Wed Jan 09 14:52:54 WET 2013', 'armp'),
(250, 'Ajouter', '2013-01-09 14:55:28', 'Ajout Catégorie  �  :Wed Jan 09 14:55:28 WET 2013', 'armp'),
(251, 'Supprimer', '2013-01-09 14:55:39', 'Suppression Catégorie  �  :Wed Jan 09 14:55:39 WET 2013', 'armp'),
(252, 'Ajouter', '2013-01-09 14:56:27', 'Ajout Catégorie  �  :Wed Jan 09 14:56:27 WET 2013', 'armp'),
(253, 'Ajouter', '2013-01-09 15:56:04', 'Ajout Catégorie  �  :Wed Jan 09 15:56:04 WET 2013', 'armp'),
(254, 'Ajouter', '2013-01-09 16:05:42', 'Ajout Catégorie  �  :Wed Jan 09 16:05:42 WET 2013', 'armp'),
(255, 'Ajouter', '2013-01-09 17:13:57', 'Ajout Famille  �  :Wed Jan 09 17:13:57 WET 2013', 'armp'),
(256, 'Ajouter', '2013-01-10 09:37:30', 'Ajout Famille  �  :Thu Jan 10 09:37:30 WET 2013', 'armp'),
(257, 'Ajouter', '2013-01-10 11:00:04', 'Ajout Catégorie  �  :Thu Jan 10 11:00:04 WET 2013', 'armp'),
(258, 'Ajouter', '2013-01-10 11:19:34', 'Ajout Famille  �  :Thu Jan 10 11:19:34 WET 2013', 'armp'),
(259, 'Ajouter', '2013-01-10 11:36:49', 'Ajout Famille  �  :Thu Jan 10 11:36:49 WET 2013', 'armp'),
(260, 'Ajouter', '2013-01-10 11:55:42', 'Ajout Famille  �  :Thu Jan 10 11:55:42 WET 2013', 'armp'),
(261, 'Modifier', '2013-01-10 12:03:54', 'Modification Catégorie  �  :Thu Jan 10 12:03:54 WET 2013', 'armp'),
(262, 'Modifier', '2013-01-10 12:04:18', 'Modification Catégorie  �  :Thu Jan 10 12:04:18 WET 2013', 'armp'),
(263, 'Modifier', '2013-01-10 12:05:21', 'Modification Catégorie  �  :Thu Jan 10 12:05:21 WET 2013', 'armp'),
(264, 'Modifier', '2013-01-10 12:07:58', 'Modification Catégorie  �  :Thu Jan 10 12:07:58 WET 2013', 'armp'),
(265, 'Modifier', '2013-01-10 12:08:31', 'Modification Catégorie  �  :Thu Jan 10 12:08:31 WET 2013', 'armp'),
(266, 'Modifier', '2013-01-10 12:08:48', 'Modification Catégorie  �  :Thu Jan 10 12:08:48 WET 2013', 'armp'),
(267, 'Ajouter', '2013-01-10 13:22:51', 'Ajout Famille  �  :Thu Jan 10 13:22:51 WET 2013', 'armp'),
(268, 'Ajouter', '2013-01-10 13:33:46', 'Ajout Famille  �  :Thu Jan 10 13:33:46 WET 2013', 'armp'),
(269, 'Modifier', '2013-01-10 13:51:06', 'Modification Famille  �  :Thu Jan 10 13:51:06 WET 2013', 'armp'),
(270, 'Ajouter', '2013-01-10 13:51:42', 'Ajout Produit/Service  �  :Thu Jan 10 13:51:42 WET 2013', 'armp'),
(271, 'Ajouter', '2013-01-10 16:14:30', 'Ajout Produit/Service  �  :Thu Jan 10 16:14:30 WET 2013', 'armp'),
(272, 'Ajouter', '2013-01-10 16:15:17', 'Ajout Famille  �  :Thu Jan 10 16:15:17 WET 2013', 'armp'),
(273, 'Ajouter', '2013-01-10 16:33:01', 'Ajout Famille  �  :Thu Jan 10 16:33:01 WET 2013', 'armp'),
(274, 'Ajouter', '2013-01-10 16:33:33', 'Ajout Produit/Service  �  :Thu Jan 10 16:33:33 WET 2013', 'armp'),
(275, 'Modifier', '2013-01-10 16:35:34', 'Modification Famille  �  :Thu Jan 10 16:35:34 WET 2013', 'armp'),
(276, 'Ajouter', '2013-01-11 11:41:05', 'Ajout Famille  �  :Fri Jan 11 11:41:05 WET 2013', 'armp'),
(277, 'Ajouter', '2013-01-11 11:42:12', 'Ajout Produit/Service  �  :Fri Jan 11 11:42:12 WET 2013', 'armp'),
(278, 'Modifier', '2013-01-11 11:42:54', 'Modification Catégorie  �  :Fri Jan 11 11:42:54 WET 2013', 'armp'),
(279, 'Modifier', '2013-01-11 11:43:03', 'Modification Catégorie  �  :Fri Jan 11 11:43:03 WET 2013', 'armp'),
(280, 'Ajouter', '2013-01-17 15:22:12', 'Ajout Catégorie  �  :Thu Jan 17 15:22:12 WET 2013', 'dcmp'),
(281, 'Ajouter', '2013-01-17 15:26:31', 'Ajout Famille  �  :Thu Jan 17 15:26:31 WET 2013', 'dcmp'),
(282, 'Ajouter', '2013-01-17 15:27:14', 'Ajout Produit/Service  �  :Thu Jan 17 15:27:14 WET 2013', 'dcmp'),
(283, 'Ajouter', '2013-01-18 10:40:18', 'Ajout Famille  �  :Fri Jan 18 10:40:18 WET 2013', 'dcmp'),
(284, 'Ajouter', '2013-01-18 10:40:51', 'Ajout Produit/Service  �  :Fri Jan 18 10:40:51 WET 2013', 'dcmp'),
(285, 'Publier', '2013-01-18 10:42:12', 'Plan de passation des marchés N° Miroir de 5mm² pour l''année 2013Publié :18-01-2013', 'dcmp'),
(286, 'Ajouter', '2013-01-18 11:53:26', 'Ajout Bureau de la D.N.C.M.P  �  :Fri Jan 18 11:53:26 WET 2013', 'asadji'),
(287, 'Ajouter', '2013-01-18 12:45:34', 'Ajout Bureau de la D.N.C.M.P  �  :Fri Jan 18 12:45:34 GMT 2013', 'asadji'),
(288, 'Ajouter', '2013-01-18 12:45:57', 'Ajout Bureau de la D.N.C.M.P  �  :Fri Jan 18 12:45:57 GMT 2013', 'asadji'),
(289, 'Supprimer', '2013-01-18 12:46:06', 'Suppression Bureau de la D.N.C.M.P  �  :Fri Jan 18 12:46:06 GMT 2013', 'asadji'),
(290, 'Ajouter', '2013-01-18 12:52:18', 'Ajout Bureau de la D.N.C.M.P  �  :Fri Jan 18 12:52:18 GMT 2013', 'asadji'),
(291, 'Ajouter', '2013-01-18 12:52:20', 'Ajout Bureau de la D.N.C.M.P  �  :Fri Jan 18 12:52:20 GMT 2013', 'asadji'),
(292, 'Ajouter', '2013-01-18 12:52:22', 'Ajout Bureau de la D.N.C.M.P  �  :Fri Jan 18 12:52:22 GMT 2013', 'asadji'),
(293, 'Supprimer', '2013-01-18 12:54:55', 'Suppression Bureau de la D.N.C.M.P  �  :Fri Jan 18 12:54:55 GMT 2013', 'asadji'),
(294, 'Supprimer', '2013-01-18 12:55:17', 'Suppression Bureau de la D.N.C.M.P  �  :Fri Jan 18 12:55:17 GMT 2013', 'asadji'),
(295, 'Ajouter', '2013-01-18 13:43:53', 'Ajout Bureau de la D.N.C.M.P  �  :Fri Jan 18 13:43:53 GMT 2013', 'asadji'),
(296, 'Ajouter', '2013-01-18 16:38:24', 'Ajout plan de passation �  :18-01-2013', 'mef'),
(297, 'Ajouter', '2013-01-18 16:39:14', 'Ajout réalisation �  :Fri Jan 18 16:39:14 WET 2013', 'mef'),
(298, 'Soumettre pour validation', '2013-01-18 16:40:07', 'Soumission pour validation du plan de passation n° MEF_2014_1 pour la gestion 2014 :18-01-2013', 'mef'),
(299, 'Valider', '2013-01-18 16:40:34', 'Plan de passation des marchés N° MEF_2014_1 pour la gestion 2014VAL :18-01-2013', 'dcmp'),
(300, 'Publier', '2013-01-18 16:40:59', 'Plan de passation des marchés N° MEF_2014_1 pour la gestion 2014PUB :18-01-2013', 'dcmp'),
(301, 'Ajouter', '2013-01-18 16:44:03', 'Ajout plan de passation �  :18-01-2013', 'mef'),
(302, 'Soumettre pour validation', '2013-01-18 16:44:18', 'Soumission pour validation du plan de passation n° MEF_2014_1 pour la gestion 2014 :18-01-2013', 'mef'),
(303, 'Ajouter', '2013-01-18 16:44:37', 'Ajout plan de passation �  :18-01-2013', 'mef'),
(304, 'Soumettre pour validation', '2013-01-18 16:45:19', 'Soumission pour validation du plan de passation n° MEF_2014_1 pour la gestion 2014 :18-01-2013', 'mef'),
(305, 'Valider', '2013-01-18 16:45:42', 'Plan de passation des marchés N° MEF_2014_1 pour la gestion 2014VAL :18-01-2013', 'dcmp'),
(306, 'Publier', '2013-01-18 16:45:51', 'Plan de passation des marchés N° MEF_2014_1 pour la gestion 2014PUB :18-01-2013', 'dcmp'),
(307, 'Modifier', '2013-01-19 22:38:36', 'Modification type autorit� � :Sat Jan 19 22:38:36 WET 2013', 'dcmp'),
(308, 'Modifier', '2013-01-19 22:38:47', 'Modification type autorit� � :Sat Jan 19 22:38:47 WET 2013', 'dcmp'),
(309, 'Modifier', '2013-01-19 22:39:07', 'Modification type autorit� � :Sat Jan 19 22:39:07 WET 2013', 'dcmp'),
(310, 'Modifier', '2013-01-19 22:40:07', 'Modification type autorit� � :Sat Jan 19 22:40:07 WET 2013', 'dcmp'),
(311, 'Modifier', '2013-01-19 22:40:19', 'Modification type autorit� � :Sat Jan 19 22:40:19 WET 2013', 'dcmp'),
(312, 'Modifier', '2013-01-19 22:40:40', 'Modification type autorit� � :Sat Jan 19 22:40:40 WET 2013', 'dcmp'),
(313, 'Modifier', '2013-01-19 22:41:04', 'Modification autorit� � :Sat Jan 19 22:41:04 WET 2013', 'dcmp'),
(314, 'Modifier', '2013-01-19 22:41:20', 'Modification autorit� � :Sat Jan 19 22:41:20 WET 2013', 'dcmp'),
(315, 'Modifier', '2013-01-19 22:41:34', 'Modification autorit� � :Sat Jan 19 22:41:34 WET 2013', 'dcmp'),
(316, 'Modifier', '2013-01-19 22:42:07', 'Modification autorit� � :Sat Jan 19 22:42:07 WET 2013', 'dcmp'),
(317, 'Modifier', '2013-01-19 22:43:08', 'Modification autorit� � :Sat Jan 19 22:43:08 WET 2013', 'dcmp'),
(318, 'Modifier', '2013-01-19 22:43:23', 'Modification autorit� � :Sat Jan 19 22:43:23 WET 2013', 'dcmp'),
(319, 'Modifier', '2013-01-19 22:43:35', 'Modification autorit� � :Sat Jan 19 22:43:35 WET 2013', 'dcmp'),
(320, 'Modifier', '2013-01-19 22:44:06', 'Modification autorit� � :Sat Jan 19 22:44:06 WET 2013', 'dcmp'),
(321, 'Modifier', '2013-01-19 22:45:31', 'Modification autorit� � :Sat Jan 19 22:45:31 WET 2013', 'dcmp'),
(322, 'Modifier', '2013-01-19 22:45:48', 'Modification autorit� � :Sat Jan 19 22:45:48 WET 2013', 'dcmp'),
(323, 'Modifier', '2013-01-19 22:48:44', 'Modification autorit� � :Sat Jan 19 22:48:44 WET 2013', 'dcmp'),
(324, 'Modifier', '2013-01-19 22:48:56', 'Modification autorit� � :Sat Jan 19 22:48:56 WET 2013', 'dcmp'),
(325, 'Modifier', '2013-01-19 22:49:11', 'Modification autorit� � :Sat Jan 19 22:49:11 WET 2013', 'dcmp'),
(326, 'Modifier', '2013-01-19 22:49:29', 'Modification autorit� � :Sat Jan 19 22:49:29 WET 2013', 'dcmp'),
(327, 'Modifier', '2013-01-19 22:49:52', 'Modification autorit� � :Sat Jan 19 22:49:52 WET 2013', 'dcmp'),
(328, 'Modifier', '2013-01-19 22:50:29', 'Modification autorit� � :Sat Jan 19 22:50:29 WET 2013', 'dcmp'),
(329, 'Modifier', '2013-01-19 22:51:10', 'Modification autorit� � :Sat Jan 19 22:51:10 WET 2013', 'dcmp'),
(330, 'Modifier', '2013-01-19 22:51:22', 'Modification autorit� � :Sat Jan 19 22:51:22 WET 2013', 'dcmp'),
(331, 'Modifier', '2013-01-19 22:51:32', 'Modification autorit� � :Sat Jan 19 22:51:32 WET 2013', 'dcmp'),
(332, 'Modifier', '2013-01-19 22:51:42', 'Modification autorit� � :Sat Jan 19 22:51:42 WET 2013', 'dcmp'),
(333, 'Modifier', '2013-01-19 22:52:16', 'Modification autorit� � :Sat Jan 19 22:52:16 WET 2013', 'dcmp'),
(334, 'Modifier', '2013-01-19 22:53:05', 'Modification autorit� � :Sat Jan 19 22:53:05 WET 2013', 'dcmp'),
(335, 'Modifier', '2013-01-19 22:53:32', 'Modification autorit� � :Sat Jan 19 22:53:32 WET 2013', 'dcmp'),
(336, 'Modifier', '2013-01-19 22:53:48', 'Modification autorit� � :Sat Jan 19 22:53:48 WET 2013', 'dcmp'),
(337, 'Modifier', '2013-01-19 22:54:33', 'Modification autorit� � :Sat Jan 19 22:54:33 WET 2013', 'dcmp'),
(338, 'Modifier', '2013-01-19 22:54:56', 'Modification autorit� � :Sat Jan 19 22:54:56 WET 2013', 'dcmp'),
(339, 'Modifier', '2013-01-19 22:55:34', 'Modification autorit� � :Sat Jan 19 22:55:34 WET 2013', 'dcmp'),
(340, 'Modifier', '2013-01-19 22:55:46', 'Modification autorit� � :Sat Jan 19 22:55:46 WET 2013', 'dcmp'),
(341, 'Modifier', '2013-01-19 22:55:53', 'Modification autorit� � :Sat Jan 19 22:55:53 WET 2013', 'dcmp'),
(342, 'Modifier', '2013-01-19 22:56:09', 'Modification autorit� � :Sat Jan 19 22:56:09 WET 2013', 'dcmp'),
(343, 'Modifier', '2013-01-19 22:56:19', 'Modification autorit� � :Sat Jan 19 22:56:19 WET 2013', 'dcmp'),
(344, 'Modifier', '2013-01-19 22:56:51', 'Modification autorit� � :Sat Jan 19 22:56:51 WET 2013', 'dcmp'),
(345, 'Modifier', '2013-01-19 22:57:20', 'Modification autorit� � :Sat Jan 19 22:57:20 WET 2013', 'dcmp'),
(346, 'Modifier', '2013-01-19 22:57:39', 'Modification autorit� � :Sat Jan 19 22:57:39 WET 2013', 'dcmp'),
(347, 'Modifier', '2013-01-19 23:03:23', 'Modification autorit� � :Sat Jan 19 23:03:23 WET 2013', 'dcmp'),
(348, 'Modifier', '2013-01-19 23:03:40', 'Modification autorit� � :Sat Jan 19 23:03:40 WET 2013', 'dcmp'),
(349, 'Modifier', '2013-01-19 23:03:59', 'Modification autorit� � :Sat Jan 19 23:03:59 WET 2013', 'dcmp'),
(350, 'Modifier', '2013-01-19 23:05:09', 'Modification autorit� � :Sat Jan 19 23:05:09 WET 2013', 'dcmp'),
(351, 'Soumettre pour validation', '2013-01-20 00:08:56', 'Soumission pour validation du plan de passation n� P_MF_2013_1 pour la gestion 2013 :20-01-2013', 'mef'),
(352, 'Valider', '2013-01-20 00:09:48', 'Plan de passation des march�s N� P_MF_2013_1 pour la gestion 2013VAL :20-01-2013', 'dcmp'),
(353, 'Publier', '2013-01-20 00:09:55', 'Plan de passation des march�s N� P_MF_2013_1 pour la gestion 2013PUB :20-01-2013', 'dcmp'),
(354, 'Publier', '2013-01-20 00:10:11', 'Plan de passation des march�s N� P_MF_2012_1 pour la gestion 2012PUB :20-01-2013', 'dcmp'),
(355, 'Ajouter', '2013-01-20 00:16:04', 'Ajout utilisateur A.C �Sun Jan 20 00:16:04 WET 2013', 'dcmp'),
(356, 'Ajouter', '2013-01-20 00:16:41', 'Ajout plan de passation � :20-01-2013', 'adiop'),
(357, 'Ajouter', '2013-01-20 00:27:12', 'Nouveau service ajouter � :Sun Jan 20 00:27:12 WET 2013', 'adiop'),
(358, 'Ajouter', '2013-01-20 00:28:24', 'Nouveau service ajouter � :Sun Jan 20 00:28:24 WET 2013', 'adiop'),
(359, 'Ajouter', '2013-01-20 00:29:10', 'Nouveau service ajouter � :Sun Jan 20 00:29:10 WET 2013', 'adiop'),
(360, 'Ajouter', '2013-01-20 00:31:30', 'Ajout r�alisation � :Sun Jan 20 00:31:30 WET 2013', 'adiop'),
(361, 'Ajouter', '2013-01-20 00:32:59', 'Ajout r�alisation � :Sun Jan 20 00:32:59 WET 2013', 'adiop'),
(362, 'Ajouter', '2013-01-20 00:34:17', 'Ajout r�alisation � :Sun Jan 20 00:34:17 WET 2013', 'adiop'),
(363, 'Ajouter', '2013-01-20 00:36:02', 'Ajout r�alisation � :Sun Jan 20 00:36:02 WET 2013', 'adiop'),
(364, 'Soumettre pour validation', '2013-01-20 00:36:18', 'Soumission pour validation du plan de passation n� MUA_2013_1 pour la gestion 2013 :20-01-2013', 'adiop'),
(365, 'Valider', '2013-01-20 00:36:47', 'Plan de passation des march�s N� MUA_2013_1 pour la gestion 2013VAL :20-01-2013', 'dcmp'),
(366, 'Publier', '2013-01-20 00:36:53', 'Plan de passation des march�s N� MUA_2013_1 pour la gestion 2013PUB :20-01-2013', 'dcmp'),
(367, 'Ajouter', '2013-01-21 22:34:38', 'Ajout plan de passation � :21-01-2013', 'adiop'),
(368, 'Ajouter', '2013-01-22 22:50:51', 'Ajout r�alisation � :Tue Jan 22 22:50:51 WET 2013', 'adiop'),
(369, 'Soumettre pour validation', '2013-01-26 12:15:43', 'Soumission pour validation du plan de passation n� MUA_2013_2 pour la gestion 2013 :26-01-2013', 'adiop'),
(370, 'Valider', '2013-01-26 12:34:48', 'Plan de passation des march�s N� MUA_2013_2 pour la gestion 2013VAL :26-01-2013', 'dcmp'),
(371, 'Publier', '2013-01-26 12:35:05', 'Plan de passation des march�s N� MUA_2013_2 pour la gestion 2013PUB :26-01-2013', 'dcmp'),
(372, 'Ajouter', '2013-01-26 13:06:40', 'Ajout plan de passation � :26-01-2013', 'adiop'),
(373, 'Ajouter', '2013-01-26 13:08:09', 'Ajout plan de passation � :26-01-2013', 'adiop'),
(374, 'Ajouter', '2013-01-26 13:12:27', 'Ajout plan de passation � :26-01-2013', 'mef'),
(375, 'Modifier', '2013-01-30 09:11:50', 'Modification Mode de s�lection  � :Wed Jan 30 09:11:50 WET 2013', 'adiop'),
(376, 'Soumettre pour validation', '2013-02-02 13:03:27', 'Soumission pour validation du plan de passation n� MUA_2013_3 pour la gestion 2013 :02-02-2013', 'adiop'),
(377, 'Soumettre pour validation', '2013-02-02 13:08:11', 'Soumission pour validation du plan de passation n� MUA_2013_3 pour la gestion 2013 :02-02-2013', 'adiop'),
(378, 'Valider', '2013-02-02 13:49:13', 'Plan de passation des march�s N� MUA_2013_3 pour la gestion 2013VAL :02-02-2013', 'dcmp'),
(379, 'Publier', '2013-02-02 13:54:14', 'Plan de passation des march�s N� MUA_2013_3 pour la gestion 2013PUB :02-02-2013', 'dcmp'),
(380, 'Ajouter', '2013-02-02 15:06:42', 'Ajout plan de passation � :02-02-2013', 'adiop'),
(381, 'Ajouter', '2013-02-04 09:32:08', 'Ajout utilisateur A.C �Mon Feb 04 09:32:08 WET 2013', 'dcmp'),
(382, 'Modifier', '2013-02-04 09:36:01', 'Modification autorit� � :Mon Feb 04 09:36:01 WET 2013', 'dcmp'),
(383, 'Modifier', '2013-02-04 09:36:22', 'Modification autorit� � :Mon Feb 04 09:36:22 WET 2013', 'dcmp'),
(384, 'Modifier', '2013-02-04 09:36:49', 'Modification autorit� � :Mon Feb 04 09:36:49 WET 2013', 'dcmp'),
(385, 'Modifier', '2013-02-04 09:37:03', 'Modification autorit� � :Mon Feb 04 09:37:03 WET 2013', 'dcmp'),
(386, 'Ajouter', '2013-02-04 14:32:58', 'Ajout plan de passation � :04-02-2013', 'dd'),
(387, 'Ajouter', '2013-02-04 14:33:41', 'Nouveau service ajouter � :Mon Feb 04 14:33:41 WET 2013', 'dd'),
(388, 'Ajouter', '2013-02-04 14:35:05', 'Ajout r�alisation � :Mon Feb 04 14:35:05 WET 2013', 'dd'),
(389, 'Ajouter', '2013-02-04 14:39:11', 'Ajout r�alisation � :Mon Feb 04 14:39:11 WET 2013', 'dd'),
(390, 'Ajouter', '2013-02-04 14:40:01', 'Ajout r�alisation � :Mon Feb 04 14:40:01 WET 2013', 'dd'),
(391, 'Ajouter', '2013-02-04 14:43:29', 'Ajout r�alisation � :Mon Feb 04 14:43:29 WET 2013', 'dd'),
(392, 'Soumettre pour validation', '2013-02-04 14:43:37', 'Soumission pour validation du plan de passation n� AGETIP_2013_1 pour la gestion 2013 :04-02-2013', 'dd'),
(393, 'Valider', '2013-02-04 14:44:58', 'Plan de passation des march�s N� AGETIP_2013_1 pour la gestion 2013VAL :04-02-2013', 'dcmp'),
(394, 'Publier', '2013-02-04 14:45:04', 'Plan de passation des march�s N� AGETIP_2013_1 pour la gestion 2013PUB :04-02-2013', 'dcmp'),
(395, 'Ajouter', '2013-02-04 15:15:56', 'Ajout plan de passation � :04-02-2013', 'dd'),
(396, 'Ajouter', '2013-02-04 15:16:41', 'Ajout r�alisation � :Mon Feb 04 15:16:41 WET 2013', 'dd'),
(397, 'Ajouter', '2013-02-04 15:42:38', 'Ajout plan de passation � :04-02-2013', 'dd'),
(398, 'Ajouter', '2013-02-04 16:21:54', 'Nouvelle autorit� ajouter � :Mon Feb 04 16:21:54 WET 2013', 'dcmp'),
(399, 'Ajouter', '2013-02-04 16:27:38', 'Ajout utilisateur A.C �Mon Feb 04 16:27:38 WET 2013', 'dcmp'),
(400, 'Ajouter', '2013-02-04 16:30:36', 'Nouveau service ajouter � :Mon Feb 04 16:30:36 WET 2013', 'afaye'),
(401, 'Ajouter', '2013-02-04 16:31:08', 'Nouveau service ajouter � :Mon Feb 04 16:31:08 WET 2013', 'afaye'),
(402, 'Ajouter', '2013-02-04 16:31:46', 'Nouveau service ajouter � :Mon Feb 04 16:31:46 WET 2013', 'afaye'),
(403, 'Ajouter', '2013-02-04 16:43:02', 'Ajout plan de passation � :04-02-2013', 'afaye'),
(404, 'Ajouter', '2013-02-04 16:48:02', 'Ajout r�alisation � :Mon Feb 04 16:48:02 WET 2013', 'afaye'),
(405, 'Ajouter', '2013-02-04 17:12:44', 'Ajout r�alisation � :Mon Feb 04 17:12:44 WET 2013', 'afaye'),
(406, 'Ajouter', '2013-02-04 17:27:53', 'Ajout r�alisation � :Mon Feb 04 17:27:53 WET 2013', 'afaye'),
(407, 'Ajouter', '2013-02-04 17:31:50', 'Ajout r�alisation � :Mon Feb 04 17:31:50 WET 2013', 'afaye'),
(408, 'Soumettre pour validation', '2013-02-04 17:33:32', 'Soumission pour validation du plan de passation n� MM_2013_1 pour la gestion 2013 :04-02-2013', 'afaye'),
(409, 'Valider', '2013-02-04 17:35:57', 'Plan de passation des march�s N� MM_2013_1 pour la gestion 2013REJ :04-02-2013', 'dcmp'),
(410, 'Soumettre pour validation', '2013-02-04 17:44:16', 'Soumission pour validation du plan de passation n� MM_2013_1 pour la gestion 2013 :04-02-2013', 'afaye'),
(411, 'Valider', '2013-02-04 17:44:37', 'Plan de passation des march�s N� MM_2013_1 pour la gestion 2013VAL :04-02-2013', 'dcmp'),
(412, 'Publier', '2013-02-04 17:45:27', 'Plan de passation des march�s N� MM_2013_1 pour la gestion 2013PUB :04-02-2013', 'dcmp'),
(413, 'Ajouter', '2013-02-04 21:16:51', 'Nouvelle autorit� ajouter � :Mon Feb 04 21:16:51 WET 2013', 'dcmp'),
(414, 'Modifier', '2013-02-04 21:22:58', 'Editer utilisateur A.C �Mon Feb 04 21:22:58 WET 2013', 'dcmp'),
(415, 'Soumettre pour validation', '2013-02-05 13:07:36', 'Soumission pour validation du plan de passation n� AGETIP_2013_2 pour la gestion 2013 :05-02-2013', 'dd'),
(416, 'Valider', '2013-02-05 13:11:48', 'Plan de passation des march�s N� AGETIP_2013_2 pour la gestion 2013VAL :05-02-2013', 'dcmp'),
(417, 'Publier', '2013-02-05 13:15:19', 'Plan de passation des march�s N� AGETIP_2013_2 pour la gestion 2013PUB :05-02-2013', 'dcmp'),
(418, 'Ajouter', '2013-02-05 15:48:53', 'Ajout plan de passation � :05-02-2013', 'afaye'),
(419, 'Soumettre pour validation', '2013-02-05 15:50:32', 'Soumission pour validation du plan de passation n� MM_2013_2 pour la gestion 2013 :05-02-2013', 'afaye'),
(420, 'Valider', '2013-02-05 15:51:13', 'Plan de passation des march�s N� MM_2013_2 pour la gestion 2013VAL :05-02-2013', 'dcmp'),
(421, 'Publier', '2013-02-05 15:51:24', 'Plan de passation des march�s N� MM_2013_2 pour la gestion 2013PUB :05-02-2013', 'dcmp'),
(422, 'Ajouter', '2013-02-25 16:58:09', 'Ajout plan de passation � :25-02-2013', 'adiop'),
(423, 'Ajouter', '2013-02-25 17:29:23', 'Ajout r�alisation � :Mon Feb 25 17:29:23 WET 2013', 'adiop'),
(424, 'Ajouter', '2013-02-25 17:56:47', 'Ajout r�alisation � :Mon Feb 25 17:56:47 WET 2013', 'adiop'),
(425, 'Ajouter', '2013-03-20 20:46:34', 'Ajout plan de passation � :20-03-2013', 'adiop'),
(426, 'Ajouter', '2013-03-20 20:47:21', 'Ajout r�alisation � :Wed Mar 20 20:47:21 WET 2013', 'adiop'),
(427, 'Soumettre pour validation', '2013-03-20 20:47:34', 'Soumission pour validation du plan de passation n� MUA_2013_1 pour la gestion 2013 :20-03-2013', 'adiop'),
(428, 'Valider', '2013-03-20 20:51:32', 'Plan de passation des march�s N� MUA_2013_1 pour la gestion 2013VAL :20-03-2013', 'dcmp'),
(429, 'Publier', '2013-03-20 20:53:34', 'Plan de passation des march�s N� MUA_2013_1 pour la gestion 2013PUB :20-03-2013', 'dcmp'),
(430, 'Ajouter', '2013-04-02 14:22:10', 'Ajout plan de passation � :02-04-2013', 'adiop'),
(431, 'Soumettre pour validation', '2013-04-02 15:16:44', 'Soumission pour validation du plan de passation n� MUA_2013_2 pour la gestion 2013 :02-04-2013', 'adiop'),
(432, 'Valider', '2013-04-02 17:49:54', 'Plan de passation des march�s N� MUA_2013_2 pour la gestion 2013VAL :02-04-2013', 'dcmp'),
(433, 'Publier', '2013-04-02 17:50:08', 'Plan de passation des march�s N� MUA_2013_2 pour la gestion 2013PUB :02-04-2013', 'dcmp'),
(434, 'Ajouter', '2013-04-02 17:53:02', 'Ajout plan de passation � :02-04-2013', 'adiop'),
(435, 'Ajouter', '2013-04-02 17:58:39', 'Ajout plan de passation � :02-04-2013', 'adiop');
INSERT INTO `sys_journal` (`JOU_ID`, `ACT_LIBELLE`, `JOU_DATE`, `JOU_DESC`, `USR_LOGIN`) VALUES
(436, 'Ajouter', '2013-04-03 08:13:45', 'Ajout plan de passation � :03-04-2013', 'adiop'),
(437, 'Soumettre pour validation', '2013-04-03 08:26:53', 'Soumission pour validation du plan de passation n� MUA_2013_2 pour la gestion 2013 :03-04-2013', 'adiop'),
(438, 'Valider', '2013-04-04 08:38:52', 'Plan de passation des march�s N� MUA_2013_2 pour la gestion 2013VAL :04-04-2013', 'dcmp'),
(439, 'Valider', '2013-04-04 08:41:12', 'Plan de passation des march�s N� MUA_2013_2 pour la gestion 2013VAL :04-04-2013', 'dcmp'),
(440, 'Valider', '2013-04-04 09:01:45', 'Plan de passation des march�s N� MUA_2013_2 pour la gestion 2013VAL :04-04-2013', 'dcmp'),
(441, 'Valider', '2013-04-04 09:07:01', 'Plan de passation des march�s N� MUA_2013_2 pour la gestion 2013VAL :04-04-2013', 'dcmp'),
(442, 'Publier', '2013-04-04 09:07:40', 'Plan de passation des march�s N� MUA_2013_2 pour la gestion 2013PUB :04-04-2013', 'dcmp'),
(443, 'Publier', '2013-04-04 09:08:01', 'Plan de passation des march�s N� MUA_2013_2 pour la gestion 2013PUB :04-04-2013', 'dcmp'),
(444, 'Ajouter', '2013-04-05 22:44:09', 'Ajout plan de passation � :05-04-2013', 'adiop'),
(445, 'Soumettre pour validation', '2013-04-05 23:09:03', 'Soumission pour validation du plan de passation n� MUA_2013_3 pour la gestion 2013 :05-04-2013', 'adiop'),
(446, 'Valider', '2013-04-05 23:10:30', 'Plan de passation des march�s N� MUA_2013_3 pour la gestion 2013VAL :05-04-2013', 'dcmp'),
(447, 'Publier', '2013-04-05 23:10:42', 'Plan de passation des march�s N� MUA_2013_3 pour la gestion 2013PUB :05-04-2013', 'dcmp'),
(448, 'Ajouter', '2013-04-06 10:06:12', 'Ajout plan de passation � :06-04-2013', 'adiop'),
(449, 'Ajouter', '2013-04-06 11:58:22', 'Ajout r�alisation � :Sat Apr 06 11:58:22 WET 2013', 'adiop'),
(450, 'Supprimer', '2013-04-06 19:05:56', 'Suppression Mode de passation  � :Sat Apr 06 19:05:56 WET 2013', 'admin'),
(451, 'Supprimer', '2013-04-06 19:05:56', 'Suppression Mode de passation  � :Sat Apr 06 19:05:56 WET 2013', 'admin'),
(452, 'Supprimer', '2013-04-06 19:05:56', 'Suppression Mode de passation  � :Sat Apr 06 19:05:56 WET 2013', 'admin'),
(453, 'Supprimer', '2013-04-06 19:06:08', 'Suppression Mode de passation  � :Sat Apr 06 19:06:08 WET 2013', 'admin'),
(454, 'Supprimer', '2013-04-06 19:06:08', 'Suppression Mode de passation  � :Sat Apr 06 19:06:08 WET 2013', 'admin'),
(455, 'Supprimer', '2013-04-06 19:06:48', 'Suppression Mode de passation  � :Sat Apr 06 19:06:48 WET 2013', 'admin'),
(456, 'Ajouter', '2013-04-06 19:48:31', 'Ajout Mode de passation � :Sat Apr 06 19:48:31 WET 2013', 'admin'),
(457, 'Supprimer', '2013-04-06 19:48:35', 'Suppression Mode de passation  � :Sat Apr 06 19:48:35 WET 2013', 'admin'),
(458, 'Ajouter', '2013-04-06 22:22:34', 'Ajout r�alisation � :Sat Apr 06 22:22:34 WET 2013', 'adiop'),
(459, 'Ajouter', '2013-04-06 22:45:09', 'Ajout r�alisation � :Sat Apr 06 22:45:09 WET 2013', 'adiop'),
(460, 'Ajouter', '2013-04-06 23:10:51', 'Ajout Mode de passation � :Sat Apr 06 23:10:51 WET 2013', 'admin'),
(461, 'Ajouter', '2013-04-06 23:10:51', 'Ajout Mode de passation � :Sat Apr 06 23:10:51 WET 2013', 'admin'),
(462, 'Ajouter', '2013-04-06 23:11:02', 'Ajout Mode de s�lection � :Sat Apr 06 23:11:02 WET 2013', 'admin'),
(463, 'Ajouter', '2013-04-06 23:37:46', 'Ajout r�alisation � :Sat Apr 06 23:37:46 WET 2013', 'adiop'),
(464, 'Ajouter', '2013-04-06 23:39:05', 'Ajout r�alisation � :Sat Apr 06 23:39:05 WET 2013', 'adiop'),
(465, 'Supprimer', '2013-04-06 23:44:38', 'Suppression Mode de passation  � :Sat Apr 06 23:44:38 WET 2013', 'admin'),
(466, 'Ajouter', '2013-04-06 23:44:42', 'Ajout Mode de passation � :Sat Apr 06 23:44:42 WET 2013', 'admin'),
(467, 'Ajouter', '2013-04-06 23:46:18', 'Ajout r�alisation � :Sat Apr 06 23:46:18 WET 2013', 'adiop'),
(468, 'Ajouter', '2013-04-06 23:48:45', 'Ajout r�alisation � :Sat Apr 06 23:48:45 WET 2013', 'adiop'),
(469, 'Ajouter', '2013-04-06 23:51:29', 'Ajout r�alisation � :Sat Apr 06 23:51:29 WET 2013', 'adiop'),
(470, 'Ajouter', '2013-04-06 23:59:37', 'Ajout r�alisation � :Sat Apr 06 23:59:37 WET 2013', 'adiop'),
(471, 'Ajouter', '2013-04-07 00:02:09', 'Ajout r�alisation � :Sun Apr 07 00:02:09 WET 2013', 'adiop'),
(472, 'Soumettre pour validation', '2013-04-07 00:22:18', 'Soumission pour validation du plan de passation n� MUA_2014_1 pour la gestion 2014 :07-04-2013', 'adiop'),
(473, 'Valider', '2013-04-07 00:23:26', 'Plan de passation des march�s N� MUA_2014_1 pour la gestion 2014VAL :07-04-2013', 'dcmp'),
(474, 'Publier', '2013-04-07 00:23:48', 'Plan de passation des march�s N� MUA_2014_1 pour la gestion 2014PUB :07-04-2013', 'dcmp'),
(475, 'Ajouter', '2013-04-07 17:20:37', 'Ajout plan de passation � :07-04-2013', 'mef'),
(476, 'Ajouter', '2013-04-07 17:22:26', 'Ajout r�alisation � :Sun Apr 07 17:22:26 WET 2013', 'mef'),
(477, 'Soumettre pour validation', '2013-04-07 17:24:21', 'Soumission pour validation du plan de passation n� MEF_2013_1 pour la gestion 2013 :07-04-2013', 'mef'),
(478, 'Valider', '2013-04-07 17:35:32', 'Plan de passation des march�s N� MEF_2013_1 pour la gestion 2013REJ :07-04-2013', 'dcmp'),
(479, 'Soumettre pour validation', '2013-04-07 17:36:51', 'Soumission pour validation du plan de passation n� MEF_2013_1 pour la gestion 2013 :07-04-2013', 'mef'),
(480, 'Valider', '2013-04-07 17:37:49', 'Plan de passation des march�s N� MEF_2013_1 pour la gestion 2013VAL :07-04-2013', 'dcmp'),
(481, 'Ajouter', '2013-04-07 18:56:26', 'Ajout plan de passation � :07-04-2013', 'astaba'),
(482, 'Ajouter', '2013-04-07 18:57:35', 'Ajout r�alisation � :Sun Apr 07 18:57:35 WET 2013', 'astaba'),
(483, 'Soumettre pour validation', '2013-04-07 18:57:54', 'Soumission pour validation du plan de passation n� AIBD_2013_1 pour la gestion 2013 :07-04-2013', 'astaba'),
(484, 'Valider', '2013-04-07 18:58:42', 'Plan de passation des march�s N� AIBD_2013_1 pour la gestion 2013REJ :07-04-2013', 'dcmp'),
(485, 'Soumettre pour validation', '2013-04-07 18:59:12', 'Soumission pour validation du plan de passation n� AIBD_2013_1 pour la gestion 2013 :07-04-2013', 'astaba'),
(486, 'Valider', '2013-04-07 18:59:46', 'Plan de passation des march�s N� AIBD_2013_1 pour la gestion 2013REJ :07-04-2013', 'dcmp'),
(487, 'Soumettre pour validation', '2013-04-07 19:00:38', 'Soumission pour validation du plan de passation n� AIBD_2013_1 pour la gestion 2013 :07-04-2013', 'astaba'),
(488, 'Valider', '2013-04-07 19:07:42', 'Plan de passation des march�s N� AIBD_2013_1 pour la gestion 2013VAL :07-04-2013', 'dcmp'),
(489, 'Publier', '2013-04-07 19:08:27', 'Plan de passation des march�s N� AIBD_2013_1 pour la gestion 2013PUB :07-04-2013', 'dcmp'),
(490, 'Ajouter', '2013-04-09 09:19:53', 'Ajout Rapport � :Tue Apr 09 09:19:53 WET 2013', 'dcmp'),
(491, 'Ajouter', '2013-04-09 10:27:16', 'Ajout plan de passation � :09-04-2013', 'astaba'),
(492, 'Supprimer', '2013-04-11 21:11:59', 'Suppression contentieux � :Thu Apr 11 21:11:59 WET 2013', 'armp'),
(493, 'Supprimer', '2013-04-11 21:14:57', 'Suppression contentieux � :Thu Apr 11 21:14:57 WET 2013', 'armp'),
(494, 'Supprimer', '2013-04-11 21:14:57', 'Suppression contentieux � :Thu Apr 11 21:14:57 WET 2013', 'armp'),
(495, 'Supprimer', '2013-04-11 21:18:31', 'Suppression contentieux � :Thu Apr 11 21:18:31 WET 2013', 'armp'),
(496, 'Supprimer', '2013-04-11 21:18:32', 'Suppression contentieux � :Thu Apr 11 21:18:32 WET 2013', 'armp'),
(497, 'Modifier', '2013-04-12 08:10:37', 'Modification autorit� � :Fri Apr 12 08:10:37 WET 2013', 'dcmp'),
(498, 'Modifier', '2013-04-12 08:10:54', 'Modification autorit� � :Fri Apr 12 08:10:54 WET 2013', 'dcmp'),
(499, 'Modifier', '2013-04-12 08:14:36', 'Modification autorit� � :Fri Apr 12 08:14:36 WET 2013', 'dcmp'),
(500, 'Modifier', '2013-04-12 08:16:55', 'Modification autorit� � :Fri Apr 12 08:16:55 WET 2013', 'dcmp'),
(501, 'Modifier', '2013-04-12 08:21:06', 'Modification autorit� � :Fri Apr 12 08:21:06 WET 2013', 'dcmp'),
(502, 'Modifier', '2013-04-12 08:21:32', 'Modification autorit� � :Fri Apr 12 08:21:32 WET 2013', 'dcmp'),
(503, 'Modifier', '2013-04-12 08:21:51', 'Modification autorit� � :Fri Apr 12 08:21:51 WET 2013', 'dcmp'),
(504, 'Modifier', '2013-04-12 08:22:08', 'Modification autorit� � :Fri Apr 12 08:22:08 WET 2013', 'dcmp'),
(505, 'Modifier', '2013-04-12 08:22:18', 'Modification autorit� � :Fri Apr 12 08:22:18 WET 2013', 'dcmp'),
(506, 'Modifier', '2013-04-12 08:22:30', 'Modification autorit� � :Fri Apr 12 08:22:30 WET 2013', 'dcmp'),
(507, 'Modifier', '2013-04-12 08:24:21', 'Modification autorit� � :Fri Apr 12 08:24:21 WET 2013', 'dcmp'),
(508, 'Modifier', '2013-04-12 08:25:22', 'Modification autorit� � :Fri Apr 12 08:25:22 WET 2013', 'dcmp'),
(509, 'Modifier', '2013-04-12 08:25:58', 'Modification autorit� � :Fri Apr 12 08:25:58 WET 2013', 'dcmp'),
(510, 'Modifier', '2013-04-12 08:26:53', 'Modification autorit� � :Fri Apr 12 08:26:53 WET 2013', 'dcmp'),
(511, 'Modifier', '2013-04-12 08:27:29', 'Modification autorit� � :Fri Apr 12 08:27:29 WET 2013', 'dcmp'),
(512, 'Modifier', '2013-04-12 08:27:59', 'Modification autorit� � :Fri Apr 12 08:27:59 WET 2013', 'dcmp'),
(513, 'Modifier', '2013-04-12 08:28:10', 'Modification autorit� � :Fri Apr 12 08:28:10 WET 2013', 'dcmp'),
(514, 'Modifier', '2013-04-12 08:28:24', 'Modification autorit� � :Fri Apr 12 08:28:24 WET 2013', 'dcmp'),
(515, 'Modifier', '2013-04-12 08:28:42', 'Modification autorit� � :Fri Apr 12 08:28:42 WET 2013', 'dcmp'),
(516, 'Modifier', '2013-04-12 08:29:00', 'Modification autorit� � :Fri Apr 12 08:29:00 WET 2013', 'dcmp'),
(517, 'Modifier', '2013-04-12 08:29:18', 'Modification autorit� � :Fri Apr 12 08:29:18 WET 2013', 'dcmp'),
(518, 'Modifier', '2013-04-12 08:29:31', 'Modification autorit� � :Fri Apr 12 08:29:31 WET 2013', 'dcmp'),
(519, 'Modifier', '2013-04-12 08:29:51', 'Modification autorit� � :Fri Apr 12 08:29:51 WET 2013', 'dcmp'),
(520, 'Modifier', '2013-04-12 08:30:28', 'Modification autorit� � :Fri Apr 12 08:30:28 WET 2013', 'dcmp'),
(521, 'Modifier', '2013-04-12 08:30:47', 'Modification autorit� � :Fri Apr 12 08:30:47 WET 2013', 'dcmp'),
(522, 'Modifier', '2013-04-12 08:30:57', 'Modification autorit� � :Fri Apr 12 08:30:57 WET 2013', 'dcmp'),
(523, 'Modifier', '2013-04-12 08:31:24', 'Modification autorit� � :Fri Apr 12 08:31:24 WET 2013', 'dcmp'),
(524, 'Modifier', '2013-04-12 08:31:40', 'Modification autorit� � :Fri Apr 12 08:31:40 WET 2013', 'dcmp'),
(525, 'Modifier', '2013-04-12 08:31:51', 'Modification autorit� � :Fri Apr 12 08:31:51 WET 2013', 'dcmp'),
(526, 'Modifier', '2013-04-12 08:32:00', 'Modification autorit� � :Fri Apr 12 08:32:00 WET 2013', 'dcmp'),
(527, 'Modifier', '2013-04-12 08:32:10', 'Modification autorit� � :Fri Apr 12 08:32:10 WET 2013', 'dcmp'),
(528, 'Modifier', '2013-04-12 08:33:18', 'Modification autorit� � :Fri Apr 12 08:33:18 WET 2013', 'dcmp'),
(529, 'Modifier', '2013-04-12 08:33:32', 'Modification autorit� � :Fri Apr 12 08:33:31 WET 2013', 'dcmp'),
(530, 'Modifier', '2013-04-12 08:33:50', 'Modification autorit� � :Fri Apr 12 08:33:50 WET 2013', 'dcmp'),
(531, 'Modifier', '2013-04-12 08:34:03', 'Modification autorit� � :Fri Apr 12 08:34:03 WET 2013', 'dcmp'),
(532, 'Modifier', '2013-04-12 08:34:13', 'Modification autorit� � :Fri Apr 12 08:34:13 WET 2013', 'dcmp'),
(533, 'Modifier', '2013-04-12 08:34:25', 'Modification autorit� � :Fri Apr 12 08:34:25 WET 2013', 'dcmp'),
(534, 'Modifier', '2013-04-12 20:08:24', 'Modification autorit� � :Fri Apr 12 20:08:24 WET 2013', 'dcmp'),
(535, 'Modifier', '2013-04-12 20:09:30', 'Modification autorit� � :Fri Apr 12 20:09:30 WET 2013', 'dcmp'),
(536, 'Modifier', '2013-04-12 20:12:10', 'Modification autorit� � :Fri Apr 12 20:12:10 WET 2013', 'dcmp'),
(537, 'Modifier', '2013-04-12 20:13:05', 'Modification autorit� � :Fri Apr 12 20:13:05 WET 2013', 'dcmp'),
(538, 'Modifier', '2013-04-12 20:28:20', 'Modification autorit� � :Fri Apr 12 20:28:20 WET 2013', 'dcmp'),
(539, 'Ajouter', '2013-04-12 20:42:56', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:42:55 WET 2013', 'dcmp'),
(540, 'Ajouter', '2013-04-12 20:43:49', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:43:49 WET 2013', 'dcmp'),
(541, 'Ajouter', '2013-04-12 20:46:17', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:46:17 WET 2013', 'dcmp'),
(542, 'Ajouter', '2013-04-12 20:46:48', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:46:48 WET 2013', 'dcmp'),
(543, 'Ajouter', '2013-04-12 20:47:08', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:47:08 WET 2013', 'dcmp'),
(544, 'Ajouter', '2013-04-12 20:47:37', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:47:37 WET 2013', 'dcmp'),
(545, 'Ajouter', '2013-04-12 20:47:54', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:47:54 WET 2013', 'dcmp'),
(546, 'Ajouter', '2013-04-12 20:48:24', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:48:24 WET 2013', 'dcmp'),
(547, 'Ajouter', '2013-04-12 20:48:52', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:48:52 WET 2013', 'dcmp'),
(548, 'Ajouter', '2013-04-12 20:49:20', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:49:20 WET 2013', 'dcmp'),
(549, 'Ajouter', '2013-04-12 20:49:49', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:49:49 WET 2013', 'dcmp'),
(550, 'Ajouter', '2013-04-12 20:50:19', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:50:19 WET 2013', 'dcmp'),
(551, 'Ajouter', '2013-04-12 20:50:39', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:50:39 WET 2013', 'dcmp'),
(552, 'Ajouter', '2013-04-12 20:51:12', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:51:12 WET 2013', 'dcmp'),
(553, 'Ajouter', '2013-04-12 20:51:41', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:51:41 WET 2013', 'dcmp'),
(554, 'Ajouter', '2013-04-12 20:52:15', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:52:15 WET 2013', 'dcmp'),
(555, 'Ajouter', '2013-04-12 20:52:48', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:52:48 WET 2013', 'dcmp'),
(556, 'Ajouter', '2013-04-12 20:53:11', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:53:11 WET 2013', 'dcmp'),
(557, 'Ajouter', '2013-04-12 20:53:42', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:53:42 WET 2013', 'dcmp'),
(558, 'Ajouter', '2013-04-12 20:54:07', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:54:07 WET 2013', 'dcmp'),
(559, 'Ajouter', '2013-04-12 20:54:35', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:54:35 WET 2013', 'dcmp'),
(560, 'Ajouter', '2013-04-12 20:55:08', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:55:08 WET 2013', 'dcmp'),
(561, 'Ajouter', '2013-04-12 20:55:24', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:55:24 WET 2013', 'dcmp'),
(562, 'Ajouter', '2013-04-12 20:55:47', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:55:47 WET 2013', 'dcmp'),
(563, 'Ajouter', '2013-04-12 20:56:17', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:56:17 WET 2013', 'dcmp'),
(564, 'Ajouter', '2013-04-12 20:56:44', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:56:44 WET 2013', 'dcmp'),
(565, 'Ajouter', '2013-04-12 20:57:09', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:57:09 WET 2013', 'dcmp'),
(566, 'Ajouter', '2013-04-12 20:57:40', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:57:40 WET 2013', 'dcmp'),
(567, 'Ajouter', '2013-04-12 20:58:09', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:58:09 WET 2013', 'dcmp'),
(568, 'Ajouter', '2013-04-12 20:58:38', 'Nouvelle autorit� ajouter � :Fri Apr 12 20:58:38 WET 2013', 'dcmp'),
(569, 'Supprimer', '2013-04-13 09:38:34', 'Suppression Rapport � :Sat Apr 13 09:38:34 WET 2013', 'armp'),
(570, 'Modifier', '2013-04-13 09:38:45', 'Modification Rapport � :Sat Apr 13 09:38:45 WET 2013', 'armp'),
(571, 'Modifier', '2013-04-13 09:41:22', 'Modification Rapport � :Sat Apr 13 09:41:22 WET 2013', 'dcmp'),
(572, 'Modifier', '2013-04-13 09:41:34', 'Modification Rapport � :Sat Apr 13 09:41:34 WET 2013', 'dcmp'),
(573, 'Ajouter', '2013-04-14 15:24:59', 'Ajout Mode de passation � :Sun Apr 14 15:24:59 WET 2013', 'admin'),
(574, 'Ajouter', '2013-04-14 15:27:39', 'Ajout plan de passation � :14-04-2013', 'adiop'),
(575, 'Soumettre pour validation', '2013-04-14 15:29:26', 'Soumission pour validation du plan de passation n� MUA_2013_4 pour la gestion 2013 :14-04-2013', 'adiop'),
(576, 'Valider', '2013-04-14 15:29:51', 'Plan de passation des march�s N� MUA_2013_4 pour la gestion 2013VAL :14-04-2013', 'dcmp'),
(577, 'Publier', '2013-04-14 15:29:59', 'Plan de passation des march�s N� MUA_2013_4 pour la gestion 2013PUB :14-04-2013', 'dcmp'),
(578, 'Ajouter', '2013-04-16 21:49:46', 'Ajout Mode de passation � :Tue Apr 16 21:49:46 WET 2013', 'admin'),
(579, 'Ajouter', '2013-04-28 15:08:58', 'Ajout utilisateur A.C \\u00C3\\u00A0Sun Apr 28 15:08:58 WET 2013', 'dcmp'),
(580, 'Ajouter', '2013-04-28 15:10:35', 'Ajout plan de passation � :28-04-2013', 'idiop'),
(581, 'Ajouter', '2013-04-28 15:11:25', 'Nouveau service ajouter � :Sun Apr 28 15:11:25 WET 2013', 'idiop'),
(582, 'Ajouter', '2013-04-28 15:14:14', 'Ajout Mode de passation � :Sun Apr 28 15:14:14 WET 2013', 'admin'),
(583, 'Ajouter', '2013-04-28 15:14:14', 'Ajout Mode de passation � :Sun Apr 28 15:14:14 WET 2013', 'admin'),
(584, 'Ajouter', '2013-04-28 15:14:14', 'Ajout Mode de passation � :Sun Apr 28 15:14:14 WET 2013', 'admin'),
(585, 'Ajouter', '2013-04-28 15:14:14', 'Ajout Mode de passation � :Sun Apr 28 15:14:14 WET 2013', 'admin'),
(586, 'Ajouter', '2013-04-28 15:14:29', 'Ajout Mode de passation � :Sun Apr 28 15:14:29 WET 2013', 'admin'),
(587, 'Ajouter', '2013-04-28 15:14:29', 'Ajout Mode de passation � :Sun Apr 28 15:14:29 WET 2013', 'admin'),
(588, 'Ajouter', '2013-04-28 15:14:29', 'Ajout Mode de passation � :Sun Apr 28 15:14:29 WET 2013', 'admin'),
(589, 'Ajouter', '2013-04-28 15:14:29', 'Ajout Mode de passation � :Sun Apr 28 15:14:29 WET 2013', 'admin'),
(590, 'Ajouter', '2013-04-28 15:15:17', 'Ajout Mode de passation � :Sun Apr 28 15:15:17 WET 2013', 'admin'),
(591, 'Ajouter', '2013-04-28 15:15:23', 'Ajout Mode de passation � :Sun Apr 28 15:15:23 WET 2013', 'admin'),
(592, 'Ajouter', '2013-04-28 15:15:36', 'Ajout Mode de passation � :Sun Apr 28 15:15:36 WET 2013', 'admin'),
(593, 'Ajouter', '2013-04-28 15:15:36', 'Ajout Mode de passation � :Sun Apr 28 15:15:36 WET 2013', 'admin'),
(594, 'Ajouter', '2013-04-28 15:15:36', 'Ajout Mode de passation � :Sun Apr 28 15:15:36 WET 2013', 'admin'),
(595, 'Ajouter', '2013-04-28 15:15:36', 'Ajout Mode de passation � :Sun Apr 28 15:15:36 WET 2013', 'admin'),
(596, 'Ajouter', '2013-04-28 15:15:46', 'Ajout Mode de passation � :Sun Apr 28 15:15:46 WET 2013', 'admin'),
(597, 'Ajouter', '2013-04-28 15:15:47', 'Ajout Mode de passation � :Sun Apr 28 15:15:46 WET 2013', 'admin'),
(598, 'Ajouter', '2013-04-28 15:15:47', 'Ajout Mode de passation � :Sun Apr 28 15:15:47 WET 2013', 'admin'),
(599, 'Ajouter', '2013-04-28 15:15:47', 'Ajout Mode de passation � :Sun Apr 28 15:15:47 WET 2013', 'admin'),
(600, 'Ajouter', '2013-04-28 15:18:05', 'Ajout r�alisation � :Sun Apr 28 15:18:05 WET 2013', 'idiop'),
(601, 'Ajouter', '2013-04-28 17:01:40', 'Ajout r�alisation � :Sun Apr 28 17:01:40 WET 2013', 'idiop'),
(602, 'Ajouter', '2013-04-28 17:02:36', 'Ajout r�alisation � :Sun Apr 28 17:02:36 WET 2013', 'idiop'),
(603, 'Ajouter', '2013-04-28 17:04:03', 'Ajout r�alisation � :Sun Apr 28 17:04:03 WET 2013', 'idiop'),
(604, 'Soumettre pour validation', '2013-04-28 17:05:03', 'Soumission pour validation du plan de passation n� MFPRA_2013_1 pour la gestion 2013 :28-04-2013', 'idiop'),
(605, 'Valider', '2013-04-28 17:05:48', 'Plan de passation des march�s N� MFPRA_2013_1 pour la gestion 2013VAL :28-04-2013', 'dcmp'),
(606, 'Publier', '2013-04-28 17:06:06', 'Plan de passation des march�s N� MFPRA_2013_1 pour la gestion 2013PUB :28-04-2013', 'dcmp'),
(607, 'Ajouter', '2013-04-28 17:56:50', 'Ajout Mode de s�lection � :Sun Apr 28 17:56:50 WET 2013', 'admin'),
(608, 'Ajouter', '2013-04-28 17:57:07', 'Ajout Mode de s�lection � :Sun Apr 28 17:57:07 WET 2013', 'admin'),
(609, 'Ajouter', '2013-04-28 17:57:17', 'Ajout Mode de s�lection � :Sun Apr 28 17:57:17 WET 2013', 'admin'),
(610, 'Ajouter', '2013-04-28 17:57:35', 'Ajout Mode de s�lection � :Sun Apr 28 17:57:35 WET 2013', 'admin'),
(611, 'Ajouter', '2013-04-28 17:59:59', 'Ajout Mode de s�lection � :Sun Apr 28 17:59:59 WET 2013', 'admin'),
(612, 'Ajouter', '2013-05-01 03:04:22', 'Ajout plan de passation � :01-05-2013', 'idiop'),
(613, 'Ajouter', '2013-05-03 23:45:39', 'Ajout plan de passation � :03-05-2013', 'idiop'),
(614, 'Ajouter', '2013-05-03 23:47:09', 'Ajout r�alisation � :Fri May 03 23:47:09 UTC 2013', 'idiop'),
(615, 'Soumettre pour validation', '2013-05-03 23:47:26', 'Soumission pour validation du plan de passation n� MFPRA_2014_1 pour la gestion 2014 :03-05-2013', 'idiop'),
(616, 'Valider', '2013-05-03 23:48:12', 'Plan de passation des march�s N� MFPRA_2014_1 pour la gestion 2014VAL :03-05-2013', 'dcmp'),
(617, 'Publier', '2013-05-03 23:48:19', 'Plan de passation des march�s N� MFPRA_2014_1 pour la gestion 2014PUB :03-05-2013', 'dcmp'),
(618, 'Ajouter', '2013-05-04 12:45:30', 'Ajout r�alisation � :Sat May 04 12:45:30 UTC 2013', 'idiop'),
(619, 'Ajouter', '2013-05-04 12:46:50', 'Ajout r�alisation � :Sat May 04 12:46:50 UTC 2013', 'idiop'),
(620, 'Ajouter', '2013-05-05 09:02:16', 'Ajout r�alisation � :Sun May 05 09:02:16 UTC 2013', 'idiop'),
(621, 'Ajouter', '2013-05-05 09:03:39', 'Ajout r�alisation � :Sun May 05 09:03:39 UTC 2013', 'idiop'),
(622, 'Ajouter', '2013-05-05 09:09:17', 'Ajout r�alisation � :Sun May 05 09:09:17 UTC 2013', 'idiop'),
(623, 'Ajouter', '2013-05-05 09:10:04', 'Ajout r�alisation � :Sun May 05 09:10:04 UTC 2013', 'idiop'),
(624, 'Ajouter', '2013-05-05 09:11:06', 'Ajout r�alisation � :Sun May 05 09:11:06 UTC 2013', 'idiop'),
(625, 'Ajouter', '2013-05-05 09:11:53', 'Ajout r�alisation � :Sun May 05 09:11:53 UTC 2013', 'idiop'),
(626, 'Ajouter', '2013-05-06 17:29:20', 'Ajout utilisateur A.C \\u00C3\\u00A0Mon May 06 17:29:20 UTC 2013', 'dcmp'),
(627, 'Modifier', '2013-05-06 17:30:18', 'Editer utilisateur A.C \\u00C3\\u00A0Mon May 06 17:30:18 UTC 2013', 'dcmp'),
(628, 'Ajouter', '2013-05-06 17:30:52', 'Ajout plan de passation � :06-05-2013', 'asamb'),
(629, 'Supprimer', '2013-05-06 17:32:29', 'Suppression Mode de passation  � :Mon May 06 17:32:29 UTC 2013', 'admin'),
(630, 'Supprimer', '2013-05-06 17:32:29', 'Suppression Mode de passation  � :Mon May 06 17:32:29 UTC 2013', 'admin'),
(631, 'Supprimer', '2013-05-06 17:32:40', 'Suppression Mode de passation  � :Mon May 06 17:32:40 UTC 2013', 'admin'),
(632, 'Supprimer', '2013-05-06 17:32:40', 'Suppression Mode de passation  � :Mon May 06 17:32:40 UTC 2013', 'admin'),
(633, 'Supprimer', '2013-05-06 17:32:51', 'Suppression Mode de passation  � :Mon May 06 17:32:51 UTC 2013', 'admin'),
(634, 'Supprimer', '2013-05-06 17:32:51', 'Suppression Mode de passation  � :Mon May 06 17:32:51 UTC 2013', 'admin'),
(635, 'Supprimer', '2013-05-06 17:33:06', 'Suppression Mode de passation  � :Mon May 06 17:33:06 UTC 2013', 'admin'),
(636, 'Supprimer', '2013-05-06 17:33:06', 'Suppression Mode de passation  � :Mon May 06 17:33:06 UTC 2013', 'admin'),
(637, 'Ajouter', '2013-05-06 17:47:52', 'Nouveau service ajouter � :Mon May 06 17:47:52 UTC 2013', 'asamb'),
(638, 'Ajouter', '2013-05-06 17:49:20', 'Ajout r�alisation � :Mon May 06 17:49:20 UTC 2013', 'asamb'),
(639, 'Soumettre pour validation', '2013-05-06 17:50:14', 'Soumission pour validation du plan de passation n� MC_2013_1 pour la gestion 2013 :06-05-2013', 'asamb'),
(640, 'Valider', '2013-05-06 17:51:31', 'Plan de passation des march�s N� MC_2013_1 pour la gestion 2013REJ :06-05-2013', 'dcmp'),
(641, 'Soumettre pour validation', '2013-05-06 18:11:32', 'Soumission pour validation du plan de passation n� MC_2013_1 pour la gestion 2013 :06-05-2013', 'asamb'),
(642, 'Valider', '2013-05-06 18:13:16', 'Plan de passation des march�s N� MC_2013_1 pour la gestion 2013VAL :06-05-2013', 'dcmp'),
(643, 'Publier', '2013-05-06 18:13:28', 'Plan de passation des march�s N� MC_2013_1 pour la gestion 2013PUB :06-05-2013', 'dcmp'),
(644, 'Publier', '2013-05-06 18:14:30', 'Plan de passation des march�s N� MC_2013_1 pour la gestion 2013PUB :06-05-2013', 'dcmp'),
(645, 'Valider', '2013-05-06 18:26:23', 'Plan de passation des march�s N� MC_2013_1 pour la gestion 2013VAL :06-05-2013', 'dcmp'),
(646, 'Valider', '2013-05-06 18:26:57', 'Plan de passation des march�s N� MC_2013_1 pour la gestion 2013VAL :06-05-2013', 'dcmp'),
(647, 'Publier', '2013-05-06 18:27:11', 'Plan de passation des march�s N� MC_2013_1 pour la gestion 2013PUB :06-05-2013', 'dcmp'),
(648, 'Publier', '2013-05-06 18:28:29', 'Plan de passation des march�s N� MC_2013_1 pour la gestion 2013PUB :06-05-2013', 'dcmp'),
(649, 'Publier', '2013-05-06 18:31:23', 'Plan de passation des march�s N� MC_2013_1 pour la gestion 2013PUB :06-05-2013', 'dcmp'),
(650, 'Publier', '2013-05-06 18:32:05', 'Plan de passation des march�s N� MC_2013_1 pour la gestion 2013PUB :06-05-2013', 'dcmp'),
(651, 'Ajouter', '2013-05-06 18:45:22', 'Ajout plan de passation � :06-05-2013', 'asamb'),
(652, 'Soumettre pour validation', '2013-05-06 18:51:13', 'Soumission pour validation du plan de passation n� MC_2013_2 pour la gestion 2013 :06-05-2013', 'asamb'),
(653, 'Soumettre pour validation', '2013-05-06 18:52:16', 'Soumission pour validation du plan de passation n� MC_2013_2 pour la gestion 2013 :06-05-2013', 'asamb'),
(654, 'Valider', '2013-05-06 18:54:12', 'Plan de passation des march�s N� MC_2013_2 pour la gestion 2013VAL :06-05-2013', 'dcmp'),
(655, 'Valider', '2013-05-06 18:55:26', 'Plan de passation des march�s N� MC_2013_2 pour la gestion 2013VAL :06-05-2013', 'dcmp'),
(656, 'Publier', '2013-05-06 18:55:32', 'Plan de passation des march�s N� MC_2013_2 pour la gestion 2013PUB :06-05-2013', 'dcmp'),
(657, 'Valider', '2013-05-06 18:58:09', 'Plan de passation des march�s N� MC_2013_2 pour la gestion 2013VAL :06-05-2013', 'dcmp'),
(658, 'Valider', '2013-05-06 18:58:52', 'Plan de passation des march�s N� MC_2013_2 pour la gestion 2013REJ :06-05-2013', 'dcmp'),
(659, 'Soumettre pour validation', '2013-05-06 18:59:55', 'Soumission pour validation du plan de passation n� MC_2013_2 pour la gestion 2013 :06-05-2013', 'asamb'),
(660, 'Valider', '2013-05-06 19:00:36', 'Plan de passation des march�s N� MC_2013_2 pour la gestion 2013VAL :06-05-2013', 'dcmp'),
(661, 'Publier', '2013-05-06 19:00:43', 'Plan de passation des march�s N� MC_2013_2 pour la gestion 2013PUB :06-05-2013', 'dcmp'),
(662, 'Soumettre pour validation', '2013-05-06 19:57:34', 'Soumission pour validation du plan de passation n� MC_2013_2 pour la gestion 2013 :06-05-2013', 'asamb'),
(663, 'Soumettre pour validation', '2013-05-06 20:16:13', 'Soumission pour validation du plan de passation n� MC_2013_2 pour la gestion 2013 :06-05-2013', 'asamb'),
(664, 'Valider', '2013-05-06 20:33:15', 'Plan de passation des march�s N� MC_2013_2 pour la gestion 2013REJ :06-05-2013', 'dcmp'),
(665, 'Soumettre pour validation', '2013-05-06 20:33:53', 'Soumission pour validation du plan de passation n� MC_2013_2 pour la gestion 2013 :06-05-2013', 'asamb'),
(666, 'Soumettre pour validation', '2013-05-06 20:34:53', 'Soumission pour validation du plan de passation n� MC_2013_2 pour la gestion 2013 :06-05-2013', 'asamb'),
(667, 'Valider', '2013-05-06 20:36:59', 'Plan de passation des march�s N� MC_2013_2 pour la gestion 2013REJ :06-05-2013', 'dcmp'),
(668, 'Soumettre pour validation', '2013-05-06 20:42:25', 'Soumission pour validation du plan de passation n� MC_2013_2 pour la gestion 2013 :06-05-2013', 'asamb'),
(669, 'Soumettre pour validation', '2013-05-06 20:47:44', 'Soumission pour validation du plan de passation n� MC_2013_2 pour la gestion 2013 :06-05-2013', 'asamb'),
(670, 'Soumettre pour validation', '2013-05-06 20:50:01', 'Soumission pour validation du plan de passation n� MC_2013_2 pour la gestion 2013 :06-05-2013', 'asamb'),
(671, 'Soumettre pour validation', '2013-05-06 20:55:37', 'Soumission pour validation du plan de passation n� MC_2013_2 pour la gestion 2013 :06-05-2013', 'asamb'),
(672, 'Soumettre pour validation', '2013-05-06 20:59:03', 'Soumission pour validation du plan de passation n� MC_2013_2 pour la gestion 2013 :06-05-2013', 'asamb'),
(673, 'Soumettre pour validation', '2013-05-06 21:03:58', 'Soumission pour validation du plan de passation n� MC_2013_2 pour la gestion 2013 :06-05-2013', 'asamb'),
(674, 'Valider', '2013-05-06 21:05:48', 'Plan de passation des march�s N� MC_2013_2 pour la gestion 2013REJ :06-05-2013', 'dcmp'),
(675, 'Soumettre pour validation', '2013-05-06 21:06:58', 'Soumission pour validation du plan de passation n� MC_2013_2 pour la gestion 2013 :06-05-2013', 'asamb'),
(676, 'Valider', '2013-05-06 21:07:51', 'Plan de passation des march�s N� MC_2013_2 pour la gestion 2013VAL :06-05-2013', 'dcmp'),
(677, 'Publier', '2013-05-06 21:07:58', 'Plan de passation des march�s N� MC_2013_2 pour la gestion 2013PUB :06-05-2013', 'dcmp'),
(678, 'Valider', '2013-05-06 21:08:31', 'Plan de passation des march�s N� MC_2013_2 pour la gestion 2013VAL :06-05-2013', 'dcmp'),
(679, 'Publier', '2013-05-06 21:08:37', 'Plan de passation des march�s N� MC_2013_2 pour la gestion 2013PUB :06-05-2013', 'dcmp'),
(680, 'Ajouter', '2013-05-07 15:54:00', 'Nouveau type autorit� ajouter � :Tue May 07 15:54:00 UTC 2013', 'dcmp'),
(681, 'Ajouter', '2013-05-07 15:54:42', 'Nouvelle autorit� ajouter � :Tue May 07 15:54:42 UTC 2013', 'dcmp'),
(682, 'Supprimer', '2013-05-07 15:54:45', 'Suppression autorit� � :Tue May 07 15:54:45 UTC 2013', 'dcmp'),
(683, 'Supprimer', '2013-05-07 15:54:50', 'Suppression type autorit� � :Tue May 07 15:54:50 UTC 2013', 'dcmp'),
(684, 'Modifier', '2013-05-07 16:18:46', 'Modification type autorit� � :Tue May 07 16:18:46 UTC 2013', 'dcmp'),
(685, 'Modifier', '2013-05-07 16:19:15', 'Modification type autorit� � :Tue May 07 16:19:15 UTC 2013', 'dcmp'),
(686, 'Modifier', '2013-05-07 16:20:24', 'Modification type autorit� � :Tue May 07 16:20:24 UTC 2013', 'dcmp'),
(687, 'Ajouter', '2013-05-07 16:21:10', 'Nouveau type autorit� ajouter � :Tue May 07 16:21:10 UTC 2013', 'dcmp'),
(688, 'Ajouter', '2013-05-07 16:21:40', 'Nouveau type autorit� ajouter � :Tue May 07 16:21:40 UTC 2013', 'dcmp'),
(689, 'Ajouter', '2013-05-07 16:25:19', 'Nouvelle autorit� ajouter � :Tue May 07 16:25:19 UTC 2013', 'dcmp'),
(690, 'Ajouter', '2013-05-07 16:27:44', 'Nouveau service ajouter � :Tue May 07 16:27:44 UTC 2013', 'dcmp'),
(691, 'Ajouter', '2013-05-07 16:30:31', 'Ajout utilisateur A.C \\u00C3\\u00A0Tue May 07 16:30:31 UTC 2013', 'dcmp'),
(692, 'Ajouter', '2013-05-07 16:56:30', 'Ajout plan de passation � :07-05-2013', 'mef'),
(693, 'Ajouter', '2013-05-07 17:22:24', 'Ajout r�alisation � :Tue May 07 17:22:24 UTC 2013', 'mef'),
(694, 'Ajouter', '2013-05-07 17:38:37', 'Ajout r�alisation � :Tue May 07 17:38:37 UTC 2013', 'mef'),
(695, 'Ajouter', '2013-05-07 17:40:04', 'Ajout r�alisation � :Tue May 07 17:40:04 UTC 2013', 'mef'),
(696, 'Soumettre pour validation', '2013-05-07 17:40:57', 'Soumission pour validation du plan de passation n� MEF_2013_1 pour la gestion 2013 :07-05-2013', 'mef'),
(697, 'Valider', '2013-05-07 17:42:26', 'Plan de passation des march�s N� MEF_2013_1 pour la gestion 2013REJ :07-05-2013', 'dcmp'),
(698, 'Soumettre pour validation', '2013-05-07 17:43:26', 'Soumission pour validation du plan de passation n� MEF_2013_1 pour la gestion 2013 :07-05-2013', 'mef'),
(699, 'Valider', '2013-05-07 17:44:01', 'Plan de passation des march�s N� MEF_2013_1 pour la gestion 2013VAL :07-05-2013', 'dcmp'),
(700, 'Publier', '2013-05-07 17:45:17', 'Plan de passation des march�s N� MEF_2013_1 pour la gestion 2013PUB :07-05-2013', 'dcmp'),
(701, 'Ajouter', '2013-05-07 18:08:48', 'Ajout plan de passation � :07-05-2013', 'mef'),
(702, 'Soumettre pour validation', '2013-05-07 18:14:04', 'Soumission pour validation du plan de passation n� MEF_2013_2 pour la gestion 2013 :07-05-2013', 'mef'),
(703, 'Valider', '2013-05-07 18:15:54', 'Plan de passation des march�s N� MEF_2013_2 pour la gestion 2013VAL :07-05-2013', 'dcmp'),
(704, 'Publier', '2013-05-07 18:15:59', 'Plan de passation des march�s N� MEF_2013_2 pour la gestion 2013PUB :07-05-2013', 'dcmp'),
(705, 'Ajouter', '2013-05-08 08:25:09', 'Ajout r�alisation � :Wed May 08 08:25:09 UTC 2013', 'asamb'),
(706, 'Ajouter', '2013-05-08 09:46:29', 'Nouvelle autorit� ajouter � :Wed May 08 09:46:29 UTC 2013', 'dcmp'),
(707, 'Ajouter', '2013-05-08 09:47:42', 'Ajout utilisateur A.C \\u00C3\\u00A0Wed May 08 09:47:42 UTC 2013', 'dcmp'),
(708, 'Modifier', '2013-05-08 09:47:56', 'Editer utilisateur A.C \\u00C3\\u00A0Wed May 08 09:47:56 UTC 2013', 'dcmp'),
(709, 'Modifier', '2013-05-08 14:38:24', 'Modification Cat�gorie � :Wed May 08 14:38:24 UTC 2013', 'dcmp'),
(710, 'Modifier', '2013-05-08 14:40:58', 'Modification Cat�gorie � :Wed May 08 14:40:58 UTC 2013', 'dcmp'),
(711, 'Ajouter', '2013-05-08 14:44:04', 'Ajout Produit/Service � :Wed May 08 14:44:04 UTC 2013', 'dcmp'),
(712, 'Ajouter', '2013-05-08 14:48:35', 'Ajout Famille � :Wed May 08 14:48:35 UTC 2013', 'dcmp'),
(713, 'Modifier', '2013-05-08 14:48:40', 'Modification Famille � :Wed May 08 14:48:40 UTC 2013', 'dcmp'),
(714, 'Modifier', '2013-05-08 14:48:47', 'Modification Famille � :Wed May 08 14:48:47 UTC 2013', 'dcmp'),
(715, 'Supprimer', '2013-05-08 14:48:51', 'Suppression Famille  � :Wed May 08 14:48:51 UTC 2013', 'dcmp'),
(716, 'Ajouter', '2013-05-08 14:49:13', 'Ajout Produit/Service � :Wed May 08 14:49:13 UTC 2013', 'dcmp'),
(717, 'Supprimer', '2013-05-08 17:26:35', 'null :Wed May 08 17:26:35 UTC 2013', 'dcmp'),
(718, 'Supprimer', '2013-05-08 17:26:39', 'null :Wed May 08 17:26:39 UTC 2013', 'dcmp'),
(719, 'Ajouter', '2013-05-09 16:51:47', 'Ajout r�alisation � :Thu May 09 16:51:47 UTC 2013', 'asamb'),
(720, 'Ajouter', '2013-05-10 10:43:28', 'Nouveau service ajouter � :Fri May 10 10:43:28 UTC 2013', 'dcmp'),
(721, 'Ajouter', '2013-05-10 10:44:08', 'Nouveau service ajouter � :Fri May 10 10:44:08 UTC 2013', 'dcmp'),
(722, 'Ajouter', '2013-05-10 10:45:59', 'Ajout utilisateur A.C \\u00C3\\u00A0Fri May 10 10:45:59 UTC 2013', 'dcmp'),
(723, 'Ajouter', '2013-05-10 10:57:31', 'Ajout plan de passation � :10-05-2013', 'marc'),
(724, 'Ajouter', '2013-05-10 10:59:32', 'Ajout r�alisation � :Fri May 10 10:59:32 UTC 2013', 'marc'),
(725, 'Ajouter', '2013-05-10 11:00:59', 'Ajout r�alisation � :Fri May 10 11:00:59 UTC 2013', 'marc'),
(726, 'Ajouter', '2013-05-10 11:03:55', 'Ajout r�alisation � :Fri May 10 11:03:55 UTC 2013', 'marc'),
(727, 'Soumettre pour validation', '2013-05-10 11:28:59', 'Soumission pour validation du plan de passation n� MTP_2013_1 pour la gestion 2013 :10-05-2013', 'marc'),
(728, 'Valider', '2013-05-10 11:40:18', 'Plan de passation des march�s N� MTP_2013_1 pour la gestion 2013VAL :10-05-2013', 'dcmp'),
(729, 'Publier', '2013-05-10 11:40:24', 'Plan de passation des march�s N� MTP_2013_1 pour la gestion 2013PUB :10-05-2013', 'dcmp'),
(730, 'Ajouter', '2013-05-10 11:42:20', 'Ajout plan de passation � :10-05-2013', 'marc'),
(731, 'Ajouter', '2013-05-10 11:43:03', 'Ajout r�alisation � :Fri May 10 11:43:03 UTC 2013', 'marc'),
(732, 'Soumettre pour validation', '2013-05-10 11:45:38', 'Soumission pour validation du plan de passation n� MTP_2013_2 pour la gestion 2013 :10-05-2013', 'marc'),
(733, 'Valider', '2013-05-10 11:46:37', 'Plan de passation des march�s N� MTP_2013_2 pour la gestion 2013VAL :10-05-2013', 'dcmp'),
(734, 'Publier', '2013-05-10 11:46:42', 'Plan de passation des march�s N� MTP_2013_2 pour la gestion 2013PUB :10-05-2013', 'dcmp'),
(735, 'Ajouter', '2013-05-10 16:51:31', 'Ajout Cat�gorie � :Fri May 10 16:51:31 UTC 2013', 'dcmp'),
(736, 'Ajouter', '2013-05-10 16:58:44', 'Ajout Famille � :Fri May 10 16:58:44 UTC 2013', 'dcmp'),
(737, 'Ajouter', '2013-05-10 17:01:04', 'Ajout Produit/Service � :Fri May 10 17:01:03 UTC 2013', 'dcmp'),
(738, 'Supprimer', '2013-05-11 13:34:22', 'Suppression d�cision � :Sat May 11 13:34:22 UTC 2013', 'armp'),
(739, 'Supprimer', '2013-05-11 14:05:52', 'Suppression d�cision � :Sat May 11 14:05:52 UTC 2013', 'armp'),
(740, 'Ajouter', '2013-05-11 14:39:58', 'Ajout plan de passation � :11-05-2013', 'asamb'),
(741, 'Ajouter', '2013-05-16 08:54:47', 'Ajout Produit/Service � :Thu May 16 08:54:47 UTC 2013', 'dcmp'),
(742, 'Supprimer', '2013-05-16 08:55:00', 'Suppression Produit/Service � :Thu May 16 08:55:00 UTC 2013', 'dcmp'),
(743, 'Ajouter', '2013-05-24 16:21:45', 'Ajout Rapport � :Fri May 24 16:21:45 UTC 2013', 'armp'),
(744, 'Supprimer', '2013-05-24 23:08:32', 'Suppression type service � :Fri May 24 23:08:32 UTC 2013', 'dcmp'),
(745, 'Ajouter', '2013-05-24 23:08:56', 'Suppression courrier � null :Fri May 24 23:08:56 UTC 2013', 'dcmp'),
(746, 'Modifier', '2013-05-24 23:32:35', 'Modification courrier � null :Fri May 24 23:32:35 UTC 2013', 'dcmp'),
(747, 'Ajouter', '2013-05-25 22:52:09', 'Suppression courrier � null :Sat May 25 22:52:09 UTC 2013', 'dcmp'),
(748, 'Ajouter', '2013-05-26 00:45:03', 'Ajout plan de passation � :26-05-2013', 'mef'),
(749, 'Ajouter', '2013-05-26 01:03:44', 'Ajout plan de passation � :26-05-2013', 'mef'),
(750, 'Ajouter', '2013-05-26 13:09:22', 'Ajout plan de passation � :26-05-2013', 'mef'),
(751, 'Ajouter', '2013-05-26 17:14:21', 'Ajout plan de passation � :26-05-2013', 'mef'),
(752, 'Soumettre pour validation', '2013-05-27 20:57:14', 'Soumission pour validation du plan de passation n� MEF_2013_3 pour la gestion 2013 :27-05-2013', 'mef'),
(753, 'Valider', '2013-05-27 20:58:11', 'Plan de passation des march�s N� MEF_2013_3 pour la gestion 2013VAL :27-05-2013', 'dcmp'),
(754, 'Publier', '2013-05-27 20:58:17', 'Plan de passation des march�s N� MEF_2013_3 pour la gestion 2013PUB :27-05-2013', 'dcmp'),
(755, 'Ajouter', '2013-05-27 21:01:37', 'Ajout plan de passation � :27-05-2013', 'mef'),
(756, 'Ajouter', '2013-05-27 21:06:51', 'Ajout r�alisation � :Mon May 27 21:06:51 UTC 2013', 'mef'),
(757, 'Ajouter', '2013-05-27 21:07:56', 'Ajout r�alisation � :Mon May 27 21:07:56 UTC 2013', 'mef'),
(758, 'Ajouter', '2013-05-27 21:07:58', 'Ajout r�alisation � :Mon May 27 21:07:58 UTC 2013', 'mef'),
(759, 'Ajouter', '2013-05-27 21:08:14', 'Ajout r�alisation � :Mon May 27 21:08:14 UTC 2013', 'mef'),
(760, 'Ajouter', '2013-05-27 21:34:42', 'Ajout r�alisation � :Mon May 27 21:34:42 UTC 2013', 'mef'),
(761, 'Ajouter', '2013-05-27 21:38:42', 'Ajout plan de passation � :27-05-2013', 'mef'),
(762, 'Ajouter', '2013-05-27 21:43:07', 'Ajout r�alisation � :Mon May 27 21:43:07 UTC 2013', 'mef'),
(763, 'Ajouter', '2013-05-27 21:49:34', 'Ajout plan de passation � :27-05-2013', 'mef'),
(764, 'Ajouter', '2013-05-27 21:51:58', 'Ajout r�alisation � :Mon May 27 21:51:58 UTC 2013', 'mef'),
(765, 'Ajouter', '2013-05-27 22:10:22', 'Ajout r�alisation � :Mon May 27 22:10:22 UTC 2013', 'mef'),
(766, 'Ajouter', '2013-05-28 00:05:12', 'Ajout plan de passation � :28-05-2013', 'mef'),
(767, 'Ajouter', '2013-05-29 08:34:40', 'Ajout plan de passation � :29-05-2013', 'mef'),
(768, 'Ajouter', '2013-06-02 20:12:04', 'Ajout r�alisation � :Sun Jun 02 20:12:04 UTC 2013', 'mef'),
(769, 'Ajouter', '2013-06-02 21:23:31', 'Ajout r�alisation � :Sun Jun 02 21:23:31 UTC 2013', 'mef'),
(770, 'Ajouter', '2013-06-02 21:32:07', 'Ajout r�alisation � :Sun Jun 02 21:32:07 UTC 2013', 'mef'),
(771, 'Ajouter', '2013-06-02 21:33:57', 'Ajout r�alisation � :Sun Jun 02 21:33:57 UTC 2013', 'mef'),
(772, 'Ajouter', '2013-06-02 21:40:33', 'Ajout r�alisation � :Sun Jun 02 21:40:33 UTC 2013', 'mef'),
(773, 'Soumettre pour validation', '2013-06-02 21:49:49', 'Soumission pour validation du plan de passation n� MEF_2014_1 pour la gestion 2014 :02-06-2013', 'mef'),
(774, 'Valider', '2013-06-02 21:50:20', 'Plan de passation des march�s N� MEF_2014_1 pour la gestion 2014VAL :02-06-2013', 'dcmp'),
(775, 'Publier', '2013-06-02 21:50:31', 'Plan de passation des march�s N� MEF_2014_1 pour la gestion 2014PUB :02-06-2013', 'dcmp'),
(776, 'Ajouter', '2013-06-02 21:52:19', 'Ajout plan de passation � :02-06-2013', 'mef'),
(777, 'Ajouter', '2013-06-03 22:04:00', 'Ajout plan de passation � :03-06-2013', 'mef'),
(778, 'Modifier', '2013-06-10 08:34:57', 'Modification Bureau de la D.N.C.M.P  � :Mon Jun 10 08:34:57 UTC 2013', 'dcmp'),
(779, 'Ajouter', '2013-06-10 08:38:17', 'Ajout Bureau de la D.N.C.M.P � :Mon Jun 10 08:38:17 UTC 2013', 'dcmp'),
(780, 'Ajouter', '2013-06-10 08:38:28', 'Ajout Bureau de la D.N.C.M.P � :Mon Jun 10 08:38:28 UTC 2013', 'dcmp'),
(781, 'Ajouter', '2013-06-10 08:38:44', 'Ajout Bureau de la D.N.C.M.P � :Mon Jun 10 08:38:44 UTC 2013', 'dcmp'),
(782, 'Ajouter', '2013-06-10 08:39:00', 'Ajout Bureau de la D.N.C.M.P � :Mon Jun 10 08:39:00 UTC 2013', 'dcmp'),
(783, 'Ajouter', '2013-06-10 08:39:16', 'Ajout Bureau de la D.N.C.M.P � :Mon Jun 10 08:39:16 UTC 2013', 'dcmp'),
(784, 'Ajouter', '2013-06-10 08:39:27', 'Ajout Bureau de la D.N.C.M.P � :Mon Jun 10 08:39:27 UTC 2013', 'dcmp'),
(785, 'Ajouter', '2013-06-10 08:39:39', 'Ajout Bureau de la D.N.C.M.P � :Mon Jun 10 08:39:39 UTC 2013', 'dcmp'),
(786, 'Ajouter', '2013-06-10 08:40:03', 'Ajout Bureau de la D.N.C.M.P � :Mon Jun 10 08:40:03 UTC 2013', 'dcmp'),
(787, 'Modifier', '2013-06-10 08:40:53', 'Modification Bureau de la D.N.C.M.P  � :Mon Jun 10 08:40:53 UTC 2013', 'dcmp'),
(788, 'Ajouter', '2013-06-11 21:05:57', 'Ajout plan de passation � :11-06-2013', 'mef'),
(789, 'Ajouter', '2013-06-11 22:17:42', 'Ajout r�alisation � :Tue Jun 11 22:17:42 UTC 2013', 'mef'),
(790, 'Ajouter', '2013-06-11 22:47:02', 'Ajout r�alisation � :Tue Jun 11 22:47:02 UTC 2013', 'mef'),
(791, 'Ajouter', '2013-06-11 22:54:43', 'Ajout r�alisation � :Tue Jun 11 22:54:43 UTC 2013', 'mef'),
(792, 'Ajouter', '2013-06-11 23:11:15', 'Ajout r�alisation � :Tue Jun 11 23:11:15 UTC 2013', 'mef'),
(793, 'Ajouter', '2013-06-11 23:13:25', 'Ajout r�alisation � :Tue Jun 11 23:13:25 UTC 2013', 'mef'),
(794, 'Ajouter', '2013-06-11 23:14:26', 'Ajout r�alisation � :Tue Jun 11 23:14:26 UTC 2013', 'mef');

-- --------------------------------------------------------

--
-- Structure de la table `sys_module`
--

CREATE TABLE IF NOT EXISTS `sys_module` (
  `MOD_CODE` varchar(32) NOT NULL,
  `MOD_DESCRIPTION` longtext,
  `MOD_LIBELLE` varchar(64) DEFAULT NULL,
  `MOD_LIBELLEEN` varchar(64) DEFAULT NULL,
  `MOD_LIBELLEES` varchar(64) DEFAULT NULL,
  `MOD_SEQUENCE` int(11) DEFAULT NULL,
  PRIMARY KEY (`MOD_CODE`),
  UNIQUE KEY `MOD_CODE` (`MOD_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `sys_module`
--

INSERT INTO `sys_module` (`MOD_CODE`, `MOD_DESCRIPTION`, `MOD_LIBELLE`, `MOD_LIBELLEEN`, `MOD_LIBELLEES`, `MOD_SEQUENCE`) VALUES
('ACCOURRIER', 'Gestion courriers', 'Gestion courriers', '', '', 14),
('ANNUAIRE', 'Annuaires', 'Annuaires', '', '', 4),
('ARCHIVAGE', 'Archives', 'Archives', NULL, NULL, 11),
('ARCHIVAGE_NUMERIQUE', 'Archives numériques', 'Archives num�riques', NULL, NULL, 20),
('AUDITS', NULL, 'Audits', NULL, NULL, 110),
('AVIGENERAL', 'Avis Généraux', 'Avis G�n�raux', '  ', '  ', 14),
('CATEGORIEDOCUMENT', 'Catégorie Document', 'Cat�gorie Document', ' ', ' ', 190),
('CONTENTIEUX', 'Contentieux', 'Contentieux', NULL, NULL, 100),
('DENONCIATION', 'Denonciation', 'Denonciations Anonymes', ' ', ' ', 120),
('DOSSIERTYPE', 'Dossiers Type', 'Dossiers Type', ' ', ' ', 170),
('FORMATION', '', 'Formation', '', '', 160),
('FOURNISSEURS', 'Régistre des fournisseurs', 'R�gistre des fournisseurs', NULL, NULL, 12),
('GESTIONPLANPASSATION', 'Plans de passations ', 'Plans de passations ', NULL, NULL, 5),
('ORGARMP', 'Organigramme ARMP', 'Organigramme ARMP', '', '', 220),
('ORGDCMP', 'Organigramme de la DCMP', 'Organigramme de la D.N.C.M.P', '', '', 3),
('PASSATIONMARCHES', 'Passation des marchés ', 'Passation des march�s ', NULL, NULL, 6),
('PUBLICATIONS', 'publication', 'Publications', 'Publication_EN', 'Publication_ES', 200),
('REFERENTIEL', 'Référentiel', 'R�f�rentiel', NULL, NULL, 2),
('REFERENTIELPRIX', 'Référentiel prix', 'R�f�rentiel prix', '   ', '  ', 18),
('REGLEMENTATION', 'Réglementation', 'R�glementation', '', '', 180),
('SECURITE', 'Administration', 'Administration', NULL, NULL, 1),
('SEUILS', 'Seuils', 'Seuils', '', '', 2),
('STATISTIQUE', 'Statistique', 'Statistique', '', '', 300),
('SUIVIMARCHE', '', 'Suivi des march�s', 'Suivi des marchés', 'Suivis des marché', 12),
('TAXEFISCALE', 'Taxe para fiscale', 'Taxe para fiscale', NULL, NULL, 140),
('TDOSSIERSPPM', 'Traitement des dossiers électroniques', 'Traitement des dossiers �lectroniques', '', '', 25),
('TRAITEMENTDOSSIER', 'Traitement du dossier', 'Traitement du dossier', ' ', ' ', 12),
('VENTEDAO', 'Vente DAO', 'Vente DAO', NULL, NULL, 150);

-- --------------------------------------------------------

--
-- Structure de la table `sys_oblect`
--

CREATE TABLE IF NOT EXISTS `sys_oblect` (
  `OBJ_CODE` varchar(200) NOT NULL,
  `OBJ_DESCRIPTION` longtext,
  `OBJ_LIBELLE` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`OBJ_CODE`),
  UNIQUE KEY `OBJ_CODE` (`OBJ_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `sys_oblect`
--

INSERT INTO `sys_oblect` (`OBJ_CODE`, `OBJ_DESCRIPTION`, `OBJ_LIBELLE`) VALUES
('AUDITS', 'Audits', 'Audits'),
('SUIVICONTENTIEUX', 'Suivi Contentieux', 'Suivi Contentieux'),
('TDOSSIER', 'Traitement dossier', 'Traitement dossier');

-- --------------------------------------------------------

--
-- Structure de la table `sys_parametresgeneraux`
--

CREATE TABLE IF NOT EXISTS `sys_parametresgeneraux` (
  `CODE` varchar(20) NOT NULL,
  `LIBELLE` varchar(255) NOT NULL,
  `VALEUR` bigint(20) NOT NULL,
  `TAUX` double DEFAULT NULL,
  PRIMARY KEY (`CODE`),
  UNIQUE KEY `CODE` (`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `sys_parametresgeneraux`
--

INSERT INTO `sys_parametresgeneraux` (`CODE`, `LIBELLE`, `VALEUR`, `TAUX`) VALUES
('NUMCONTENTIEUX', 'Numéro d''enregistrement contentieux', 3, NULL),
('NUMDOSC', 'Numéro dossier courrier', 1, NULL),
('NUMENREGISTREMENT', 'Numero Enregistrement ', 3, 0),
('NUMENREGISTREMENTCD', 'Num�ro Courrier', 2, NULL),
('NUMIMMATRICULATION', 'Demande Imma', 13, 0),
('NUMRECUFISCALE', 'Numéro reçu taxe para fiscale', 6, NULL),
('NUMRECUVENTEDAO', 'Numéro reçu vente DAO', 1, NULL),
('PLANPASSATION', 'Plan de passation', 23, NULL),
('REALISATION', 'Réalisations', 136, NULL),
('SECTEURACTIVITE', 'Secteurs d''activités', 1, NULL),
('TAUXPARAFISCALE', 'Taux para fiscale', 0, 1.5),
('TAUXVENTEDAO', 'Taux vente DAO', 0, 50);

-- --------------------------------------------------------

--
-- Structure de la table `sys_profil`
--

CREATE TABLE IF NOT EXISTS `sys_profil` (
  `PF_CODE` varchar(32) NOT NULL,
  `PF_DESCRIPTION` longtext,
  `PF_LIBELLE` varchar(128) NOT NULL,
  `PF_TYPE` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PF_CODE`),
  UNIQUE KEY `PF_CODE` (`PF_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `sys_profil`
--

INSERT INTO `sys_profil` (`PF_CODE`, `PF_DESCRIPTION`, `PF_LIBELLE`, `PF_TYPE`) VALUES
('AC', 'Autorit� Contractante', 'Autorit� Contractante', 'AC'),
('ADMIN', 'Admin', 'Administrateur', 's'),
('ADMINARMP', 'Administrateur ARMP', 'Administrateur ARMP', 'ARMP'),
('ADMINDCMP', 'Administrateur DCMP', 'Administrateur DCMP', 'DCMP'),
('AGENTAC', 'Agent Autorité contractante', 'Agent Autorité contractante', 's'),
('AGENTDCMP', 'Agent DCMP', 'Agent DCMP', 's'),
('SME', 'Service maitre d''oeuvre', 'Service maitre d''oeuvre', 'AGENTAC');

-- --------------------------------------------------------

--
-- Structure de la table `sys_profilaction`
--

CREATE TABLE IF NOT EXISTS `sys_profilaction` (
  `PAC_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ALLOW` smallint(6) NOT NULL,
  `ACT_CODE` varchar(50) NOT NULL,
  `PF_CODE` varchar(32) NOT NULL,
  PRIMARY KEY (`PAC_ID`),
  KEY `FK8D4FFA46338306` (`ACT_CODE`),
  KEY `FK8D4FFA45C186C28` (`PF_CODE`),
  KEY `FK8D4FFA49BCEEC27` (`ACT_CODE`),
  KEY `FK8D4FFA4F1B3D549` (`PF_CODE`),
  KEY `FK8D4FFA47397F842` (`ACT_CODE`),
  KEY `FK8D4FFA4C97CE164` (`PF_CODE`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1158 ;

--
-- Contenu de la table `sys_profilaction`
--

INSERT INTO `sys_profilaction` (`PAC_ID`, `ALLOW`, `ACT_CODE`, `PF_CODE`) VALUES
(3, 1, 'ADD_PROFIL', 'ADMIN'),
(10, 1, 'DEL_PROFIL', 'ADMIN'),
(11, 1, 'MOD_PROFIL', 'ADMIN'),
(12, 1, 'ADD_UTILISATEUR', 'ADMIN'),
(13, 1, 'DEL_USER', 'ADMIN'),
(14, 1, 'HELP_USER', 'ADMIN'),
(15, 1, 'MOD_UTILISATEUR', 'ADMIN'),
(16, 1, 'REFERENTIEL', 'ADMIN'),
(21, 1, 'ADD_TYPEAC', 'ADMIN'),
(22, 1, 'MOD_TYPEAC', 'ADMIN'),
(23, 1, 'SUPP_TYPEAC', 'ADMIN'),
(24, 1, 'WDET_TYPEAC', 'ADMIN'),
(29, 1, 'ADD_AC', 'ADMIN'),
(30, 1, 'MOD_AC', 'ADMIN'),
(31, 1, 'SUPP_AC', 'ADMIN'),
(32, 1, 'WDET_AC', 'ADMIN'),
(33, 1, 'ADD_SERVICEAC', 'ADMIN'),
(34, 1, 'MOD_SERVICEAC', 'ADMIN'),
(35, 1, 'SUPP_SERVICEAC', 'ADMIN'),
(36, 1, 'WFER_AC', 'ADMIN'),
(37, 1, 'WFER_SERVICEAC', 'ADMIN'),
(38, 1, 'ADD_POLEDCMP', 'ADMIN'),
(39, 1, 'MOD_POLEDCMP', 'ADMIN'),
(40, 1, 'SUPP_POLEDCMP', 'ADMIN'),
(41, 1, 'ADD_TYPESERVICE', 'ADMIN'),
(42, 1, 'MOD_TYPESERVICE', 'ADMIN'),
(43, 1, 'SUPP_TYPESERVICE', 'ADMIN'),
(44, 1, 'ADD_BUREAUXDCMP', 'ADMIN'),
(45, 1, 'MOD_BUREAUXDCMP', 'ADMIN'),
(46, 1, 'SUPP_BUREAUXDCMP', 'ADMIN'),
(50, 1, 'ADD_FONCTIONS', 'ADMIN'),
(51, 1, 'MOD_FONCTIONS', 'ADMIN'),
(52, 1, 'SUPP_FONCTIONS', 'ADMIN'),
(53, 1, 'ADD_GARANTIS', 'ADMIN'),
(54, 1, 'MOD_GARANTIS', 'ADMIN'),
(55, 1, 'SUPP_GARANTIS', 'ADMIN'),
(56, 1, 'ADD_JOURSFERIES', 'ADMIN'),
(57, 1, 'MOD_JOURSFERIES', 'ADMIN'),
(58, 1, 'SUPP_JOURSFERIES', 'ADMIN'),
(59, 1, 'ADD_DELAIS', 'ADMIN'),
(60, 1, 'MOD_DELAIS', 'ADMIN'),
(61, 1, 'SUPP_DELAIS', 'ADMIN'),
(62, 1, 'ADD_MODEPASSATION', 'ADMIN'),
(63, 1, 'MOD_MODEPASSATION', 'ADMIN'),
(64, 1, 'SUPP_MODEPASSATION', 'ADMIN'),
(65, 1, 'ADD_MODESELECTION', 'ADMIN'),
(66, 1, 'MOD_MODESELECTION', 'ADMIN'),
(67, 1, 'SUPP_MODESELECTION', 'ADMIN'),
(68, 1, 'ADD_SECTEURSACTIVITES', 'ADMIN'),
(69, 1, 'MOD_SECTEURSACTIVITES', 'ADMIN'),
(70, 1, 'SUPP_SECTEURSACTIVITES', 'ADMIN'),
(74, 1, 'NOUV_PLANPASSATION', 'ADMIN'),
(75, 1, 'ADD_PLANPASSATION', 'ADMIN'),
(76, 1, 'SUPP_PLANPASSATION', 'ADMIN'),
(77, 1, 'WREAL_PLANPASSATION', 'ADMIN'),
(78, 1, 'ADD_REALSATIONS', 'ADMIN'),
(82, 1, 'ADD_BREALISATIONS', 'ADMIN'),
(83, 1, 'SUPP_BREALISATIONS', 'ADMIN'),
(84, 1, 'WFER_BREALISATIONS', 'ADMIN'),
(86, 1, 'WSOUM_PLANPASSATION', 'ADMIN'),
(87, 1, 'WVALID_PLANPASSATION', 'ADMIN'),
(88, 1, 'WWPUB_PLANPASSATION', 'ADMIN'),
(90, 1, 'PASSATIONSMARCHES', 'ADMIN'),
(91, 1, 'ADD_PROCEDURESPASSATIONS', 'ADMIN'),
(92, 1, 'MOD_PROCEDURESPASSATIONS', 'ADMIN'),
(93, 1, 'WGER_PROCEDURESPASSATIONS', 'ADMIN'),
(94, 1, 'ADD_ANNUAIREARMP', 'ADMIN'),
(95, 1, 'ADD_ANNUAIREDCMP', 'ADMIN'),
(96, 1, 'MOD_ANNUAIREARMP', 'ADMIN'),
(97, 1, 'SUPP_ANNUAIREARMP', 'ADMIN'),
(98, 1, 'MOD_ANNUAIREDCMP', 'ADMIN'),
(99, 1, 'SUPP_ANNUAIREDCMP', 'ADMIN'),
(100, 1, 'WMODS_TYPESMARCHES', 'ADMIN'),
(101, 1, 'ADD_TYPESMARCHESMS', 'ADMIN'),
(102, 1, 'SUPP_TYPESMARCHESMS', 'ADMIN'),
(103, 1, 'WFER_TYPESMARCHESMS', 'ADMIN'),
(104, 1, 'ADD_CATEGORIES', 'ADMIN'),
(105, 1, 'MOD_CATEGORIES', 'ADMIN'),
(106, 1, 'SUPP_CATEGORIES', 'ADMIN'),
(107, 1, 'ADD_MONNAI', 'ADMIN'),
(108, 1, 'DEL_MONNAI', 'ADMIN'),
(109, 1, 'MOD_MONNAI', 'ADMIN'),
(110, 1, 'ADD_NAT', 'ADMIN'),
(111, 1, 'MOD_NAT', 'ADMIN'),
(112, 1, 'SUP_NAT', 'ADMIN'),
(113, 1, 'ADD_CRITERE', 'ADMIN'),
(114, 1, 'DEL_CRITERE', 'ADMIN'),
(115, 1, 'MOD_CRITERE', 'ADMIN'),
(116, 1, 'ADD_CATEGORIE', 'ADMIN'),
(117, 1, 'MOD_CATEGORIE', 'ADMIN'),
(118, 1, 'SUPP_CATEGORIE', 'ADMIN'),
(119, 1, 'ADD_EVA', 'ADMIN'),
(120, 1, 'MOD_EVA', 'ADMIN'),
(121, 1, 'SUP_EVA', 'ADMIN'),
(122, 1, 'ADD_BAILLEUR', 'ADMIN'),
(123, 1, 'MOD_BAILLEUR', 'ADMIN'),
(124, 1, 'SUPP_BAILLEUR', 'ADMIN'),
(125, 1, 'ADD_FOURNISSEUR', 'ADMIN'),
(126, 1, 'MOD_FOURNISSEUR', 'ADMIN'),
(127, 1, 'SUPP_FOURNISSEUR', 'ADMIN'),
(131, 1, 'ADD_UTILISATEURAC', 'ADMIN'),
(132, 1, 'ADD_UTILISATEURDNCMP', 'ADMIN'),
(133, 1, 'MOD_UTILISATEURAC', 'ADMIN'),
(134, 1, 'MOD_UTILISATEURDNCMP', 'ADMIN'),
(135, 0, 'ADD_BREALISATIONS', 'AGENTDCMP'),
(136, 0, 'SUPP_BREALISATIONS', 'AGENTDCMP'),
(137, 0, 'WFER_BREALISATIONS', 'AGENTDCMP'),
(138, 1, 'NOUV_PLANPASSATION', 'AGENTDCMP'),
(139, 1, 'ADD_PLANPASSATION', 'AGENTDCMP'),
(140, 1, 'SUPP_PLANPASSATION', 'AGENTDCMP'),
(141, 1, 'WREAL_PLANPASSATION', 'AGENTDCMP'),
(142, 1, 'WSOUM_PLANPASSATION', 'AGENTDCMP'),
(143, 0, 'WVALID_PLANPASSATION', 'AGENTDCMP'),
(144, 0, 'WWPUB_PLANPASSATION', 'AGENTDCMP'),
(145, 1, 'ADD_REALSATIONS', 'AGENTDCMP'),
(147, 0, 'ADD_BREALISATIONS', 'ADMINDCMP'),
(148, 0, 'SUPP_BREALISATIONS', 'ADMINDCMP'),
(149, 0, 'WFER_BREALISATIONS', 'ADMINDCMP'),
(150, 0, 'NOUV_PLANPASSATION', 'ADMINDCMP'),
(151, 0, 'ADD_PLANPASSATION', 'ADMINDCMP'),
(152, 0, 'SUPP_PLANPASSATION', 'ADMINDCMP'),
(153, 0, 'WREAL_PLANPASSATION', 'ADMINDCMP'),
(154, 0, 'WSOUM_PLANPASSATION', 'ADMINDCMP'),
(155, 1, 'WVALID_PLANPASSATION', 'ADMINDCMP'),
(156, 1, 'WWPUB_PLANPASSATION', 'ADMINDCMP'),
(157, 0, 'ADD_REALSATIONS', 'ADMINDCMP'),
(159, 1, 'ADD_PROFIL', 'ADMINDCMP'),
(160, 1, 'DEL_PROFIL', 'ADMINDCMP'),
(161, 1, 'MOD_PROFIL', 'ADMINDCMP'),
(162, 0, 'ADD_UTILISATEUR', 'ADMINDCMP'),
(163, 1, 'ADD_UTILISATEURAC', 'ADMINDCMP'),
(164, 1, 'ADD_UTILISATEURDNCMP', 'ADMINDCMP'),
(165, 0, 'DEL_USER', 'ADMINDCMP'),
(166, 0, 'HELP_USER', 'ADMINDCMP'),
(167, 0, 'MOD_UTILISATEUR', 'ADMINDCMP'),
(168, 1, 'MOD_UTILISATEURAC', 'ADMINDCMP'),
(169, 1, 'MOD_UTILISATEURDNCMP', 'ADMINDCMP'),
(170, 0, 'ADD_PROFIL', 'AGENTDCMP'),
(171, 0, 'DEL_PROFIL', 'AGENTDCMP'),
(172, 0, 'MOD_PROFIL', 'AGENTDCMP'),
(173, 0, 'ADD_UTILISATEUR', 'AGENTDCMP'),
(174, 1, 'ADD_UTILISATEURAC', 'AGENTDCMP'),
(175, 0, 'ADD_UTILISATEURDNCMP', 'AGENTDCMP'),
(176, 1, 'DEL_USER', 'AGENTDCMP'),
(177, 0, 'HELP_USER', 'AGENTDCMP'),
(178, 0, 'MOD_UTILISATEUR', 'AGENTDCMP'),
(179, 1, 'MOD_UTILISATEURAC', 'AGENTDCMP'),
(180, 0, 'MOD_UTILISATEURDNCMP', 'AGENTDCMP'),
(181, 1, 'ADD_PROFIL', 'AC'),
(182, 1, 'DEL_PROFIL', 'AC'),
(183, 1, 'MOD_PROFIL', 'AC'),
(184, 0, 'ADD_UTILISATEUR', 'AC'),
(185, 1, 'ADD_UTILISATEURAC', 'AC'),
(186, 0, 'ADD_UTILISATEURDNCMP', 'AC'),
(187, 0, 'DEL_USER', 'AC'),
(188, 0, 'HELP_USER', 'AC'),
(189, 0, 'MOD_UTILISATEUR', 'AC'),
(190, 1, 'MOD_UTILISATEURAC', 'AC'),
(191, 0, 'MOD_UTILISATEURDNCMP', 'AC'),
(192, 1, 'PASSATIONSMARCHES', 'AC'),
(193, 1, 'ADD_PROCEDURESPASSATIONS', 'AC'),
(194, 1, 'MOD_PROCEDURESPASSATIONS', 'AC'),
(195, 1, 'WGER_PROCEDURESPASSATIONS', 'AC'),
(196, 0, 'ADD_BREALISATIONS', 'AC'),
(197, 0, 'SUPP_BREALISATIONS', 'AC'),
(198, 0, 'WFER_BREALISATIONS', 'AC'),
(199, 1, 'NOUV_PLANPASSATION', 'AC'),
(200, 1, 'ADD_PLANPASSATION', 'AC'),
(201, 1, 'SUPP_PLANPASSATION', 'AC'),
(202, 1, 'WREAL_PLANPASSATION', 'AC'),
(203, 1, 'WSOUM_PLANPASSATION', 'AC'),
(204, 0, 'WVALID_PLANPASSATION', 'AC'),
(205, 0, 'WWPUB_PLANPASSATION', 'AC'),
(206, 1, 'ADD_REALSATIONS', 'AC'),
(208, 0, 'ADD_BREALISATIONS', 'AGENTAC'),
(209, 0, 'SUPP_BREALISATIONS', 'AGENTAC'),
(210, 0, 'WFER_BREALISATIONS', 'AGENTAC'),
(211, 0, 'NOUV_PLANPASSATION', 'AGENTAC'),
(212, 0, 'ADD_PLANPASSATION', 'AGENTAC'),
(213, 0, 'SUPP_PLANPASSATION', 'AGENTAC'),
(214, 1, 'WREAL_PLANPASSATION', 'AGENTAC'),
(215, 1, 'WSOUM_PLANPASSATION', 'AGENTAC'),
(216, 0, 'WVALID_PLANPASSATION', 'AGENTAC'),
(217, 0, 'WWPUB_PLANPASSATION', 'AGENTAC'),
(218, 1, 'ADD_REALSATIONS', 'AGENTAC'),
(220, 0, 'ADD_AC', 'AC'),
(221, 0, 'MOD_AC', 'AC'),
(222, 0, 'SUPP_AC', 'AC'),
(223, 0, 'WDET_AC', 'AC'),
(224, 0, 'WFER_AC', 'AC'),
(225, 1, 'ADD_BAILLEUR', 'AC'),
(226, 1, 'MOD_BAILLEUR', 'AC'),
(227, 1, 'SUPP_BAILLEUR', 'AC'),
(228, 1, 'ADD_CATEGORIES', 'AC'),
(229, 1, 'MOD_CATEGORIES', 'AC'),
(230, 1, 'SUPP_CATEGORIES', 'AC'),
(231, 0, 'ADD_DELAIS', 'AC'),
(232, 0, 'MOD_DELAIS', 'AC'),
(233, 0, 'SUPP_DELAIS', 'AC'),
(234, 1, 'ADD_FONCTIONS', 'AC'),
(235, 1, 'MOD_FONCTIONS', 'AC'),
(236, 1, 'SUPP_FONCTIONS', 'AC'),
(237, 1, 'ADD_FOURNISSEUR', 'AC'),
(238, 1, 'MOD_FOURNISSEUR', 'AC'),
(239, 1, 'SUPP_FOURNISSEUR', 'AC'),
(240, 1, 'ADD_GARANTIS', 'AC'),
(241, 1, 'MOD_GARANTIS', 'AC'),
(242, 1, 'SUPP_GARANTIS', 'AC'),
(243, 1, 'ADD_CATEGORIE', 'AC'),
(244, 1, 'MOD_CATEGORIE', 'AC'),
(245, 1, 'SUPP_CATEGORIE', 'AC'),
(246, 1, 'ADD_CRITERE', 'AC'),
(247, 1, 'DEL_CRITERE', 'AC'),
(248, 1, 'MOD_CRITERE', 'AC'),
(249, 1, 'ADD_EVA', 'AC'),
(250, 1, 'MOD_EVA', 'AC'),
(251, 1, 'SUP_EVA', 'AC'),
(252, 0, 'ADD_JOURSFERIES', 'AC'),
(253, 0, 'MOD_JOURSFERIES', 'AC'),
(254, 0, 'SUPP_JOURSFERIES', 'AC'),
(258, 0, 'ADD_MODEPASSATION', 'AC'),
(259, 0, 'MOD_MODEPASSATION', 'AC'),
(260, 0, 'SUPP_MODEPASSATION', 'AC'),
(261, 0, 'ADD_MODESELECTION', 'AC'),
(262, 0, 'MOD_MODESELECTION', 'AC'),
(263, 0, 'SUPP_MODESELECTION', 'AC'),
(264, 0, 'ADD_MONNAI', 'AC'),
(265, 0, 'DEL_MONNAI', 'AC'),
(266, 0, 'MOD_MONNAI', 'AC'),
(267, 0, 'ADD_NAT', 'AC'),
(268, 0, 'MOD_NAT', 'AC'),
(269, 0, 'SUP_NAT', 'AC'),
(270, 0, 'ADD_POLEDCMP', 'AC'),
(271, 0, 'MOD_POLEDCMP', 'AC'),
(272, 0, 'SUPP_POLEDCMP', 'AC'),
(273, 1, 'REFERENTIEL', 'AC'),
(274, 0, 'ADD_SECTEURSACTIVITES', 'AC'),
(275, 0, 'MOD_SECTEURSACTIVITES', 'AC'),
(276, 0, 'SUPP_SECTEURSACTIVITES', 'AC'),
(277, 0, 'ADD_SERVICEAC', 'AC'),
(278, 0, 'MOD_SERVICEAC', 'AC'),
(279, 0, 'SUPP_SERVICEAC', 'AC'),
(280, 0, 'WFER_SERVICEAC', 'AC'),
(281, 0, 'ADD_TYPEAC', 'AC'),
(282, 0, 'MOD_TYPEAC', 'AC'),
(283, 0, 'SUPP_TYPEAC', 'AC'),
(284, 0, 'WDET_TYPEAC', 'AC'),
(285, 0, 'ADD_TYPESERVICE', 'AC'),
(286, 0, 'MOD_TYPESERVICE', 'AC'),
(287, 0, 'SUPP_TYPESERVICE', 'AC'),
(291, 0, 'WMODS_TYPESMARCHES', 'AC'),
(292, 0, 'ADD_TYPESMARCHESMS', 'AC'),
(293, 0, 'SUPP_TYPESMARCHESMS', 'AC'),
(294, 0, 'WFER_TYPESMARCHESMS', 'AC'),
(295, 1, 'ADD_BUREAUXDCMP', 'ADMINDCMP'),
(296, 1, 'MOD_BUREAUXDCMP', 'ADMINDCMP'),
(297, 1, 'SUPP_BUREAUXDCMP', 'ADMINDCMP'),
(301, 1, 'ADD_ANNUAIREARMP', 'ADMINDCMP'),
(302, 1, 'MOD_ANNUAIREARMP', 'ADMINDCMP'),
(303, 1, 'SUPP_ANNUAIREARMP', 'ADMINDCMP'),
(304, 1, 'ADD_ANNUAIREDCMP', 'ADMINDCMP'),
(305, 1, 'MOD_ANNUAIREDCMP', 'ADMINDCMP'),
(306, 1, 'SUPP_ANNUAIREDCMP', 'ADMINDCMP'),
(307, 1, 'ADD_AC', 'ADMINDCMP'),
(308, 1, 'MOD_AC', 'ADMINDCMP'),
(309, 1, 'SUPP_AC', 'ADMINDCMP'),
(310, 1, 'WDET_AC', 'ADMINDCMP'),
(311, 1, 'WFER_AC', 'ADMINDCMP'),
(312, 0, 'ADD_BAILLEUR', 'ADMINDCMP'),
(313, 0, 'MOD_BAILLEUR', 'ADMINDCMP'),
(314, 0, 'SUPP_BAILLEUR', 'ADMINDCMP'),
(315, 1, 'ADD_CATEGORIES', 'ADMINDCMP'),
(316, 1, 'MOD_CATEGORIES', 'ADMINDCMP'),
(317, 1, 'SUPP_CATEGORIES', 'ADMINDCMP'),
(318, 1, 'ADD_DELAIS', 'ADMINDCMP'),
(319, 1, 'MOD_DELAIS', 'ADMINDCMP'),
(320, 1, 'SUPP_DELAIS', 'ADMINDCMP'),
(321, 1, 'ADD_FONCTIONS', 'ADMINDCMP'),
(322, 1, 'MOD_FONCTIONS', 'ADMINDCMP'),
(323, 1, 'SUPP_FONCTIONS', 'ADMINDCMP'),
(324, 1, 'ADD_FOURNISSEUR', 'ADMINDCMP'),
(325, 1, 'MOD_FOURNISSEUR', 'ADMINDCMP'),
(326, 1, 'SUPP_FOURNISSEUR', 'ADMINDCMP'),
(327, 1, 'ADD_GARANTIS', 'ADMINDCMP'),
(328, 1, 'MOD_GARANTIS', 'ADMINDCMP'),
(329, 1, 'SUPP_GARANTIS', 'ADMINDCMP'),
(330, 0, 'ADD_CATEGORIE', 'ADMINDCMP'),
(331, 0, 'MOD_CATEGORIE', 'ADMINDCMP'),
(332, 0, 'SUPP_CATEGORIE', 'ADMINDCMP'),
(333, 0, 'ADD_CRITERE', 'ADMINDCMP'),
(334, 0, 'DEL_CRITERE', 'ADMINDCMP'),
(335, 0, 'MOD_CRITERE', 'ADMINDCMP'),
(336, 0, 'ADD_EVA', 'ADMINDCMP'),
(337, 0, 'MOD_EVA', 'ADMINDCMP'),
(338, 0, 'SUP_EVA', 'ADMINDCMP'),
(339, 1, 'ADD_JOURSFERIES', 'ADMINDCMP'),
(340, 1, 'MOD_JOURSFERIES', 'ADMINDCMP'),
(341, 1, 'SUPP_JOURSFERIES', 'ADMINDCMP'),
(345, 0, 'ADD_MODEPASSATION', 'ADMINDCMP'),
(346, 0, 'MOD_MODEPASSATION', 'ADMINDCMP'),
(347, 0, 'SUPP_MODEPASSATION', 'ADMINDCMP'),
(348, 0, 'ADD_MODESELECTION', 'ADMINDCMP'),
(349, 0, 'MOD_MODESELECTION', 'ADMINDCMP'),
(350, 0, 'SUPP_MODESELECTION', 'ADMINDCMP'),
(351, 0, 'ADD_MONNAI', 'ADMINDCMP'),
(352, 0, 'DEL_MONNAI', 'ADMINDCMP'),
(353, 0, 'MOD_MONNAI', 'ADMINDCMP'),
(354, 0, 'ADD_NAT', 'ADMINDCMP'),
(355, 0, 'MOD_NAT', 'ADMINDCMP'),
(356, 0, 'SUP_NAT', 'ADMINDCMP'),
(357, 0, 'ADD_POLEDCMP', 'ADMINDCMP'),
(358, 0, 'MOD_POLEDCMP', 'ADMINDCMP'),
(359, 0, 'SUPP_POLEDCMP', 'ADMINDCMP'),
(360, 1, 'REFERENTIEL', 'ADMINDCMP'),
(361, 0, 'ADD_SECTEURSACTIVITES', 'ADMINDCMP'),
(362, 0, 'MOD_SECTEURSACTIVITES', 'ADMINDCMP'),
(363, 0, 'SUPP_SECTEURSACTIVITES', 'ADMINDCMP'),
(364, 1, 'ADD_SERVICEAC', 'ADMINDCMP'),
(365, 1, 'MOD_SERVICEAC', 'ADMINDCMP'),
(366, 1, 'SUPP_SERVICEAC', 'ADMINDCMP'),
(367, 1, 'WFER_SERVICEAC', 'ADMINDCMP'),
(368, 1, 'ADD_TYPEAC', 'ADMINDCMP'),
(369, 1, 'MOD_TYPEAC', 'ADMINDCMP'),
(370, 1, 'SUPP_TYPEAC', 'ADMINDCMP'),
(371, 1, 'WDET_TYPEAC', 'ADMINDCMP'),
(372, 1, 'ADD_TYPESERVICE', 'ADMINDCMP'),
(373, 1, 'MOD_TYPESERVICE', 'ADMINDCMP'),
(374, 1, 'SUPP_TYPESERVICE', 'ADMINDCMP'),
(378, 0, 'WMODS_TYPESMARCHES', 'ADMINDCMP'),
(379, 0, 'ADD_TYPESMARCHESMS', 'ADMINDCMP'),
(380, 0, 'SUPP_TYPESMARCHESMS', 'ADMINDCMP'),
(381, 0, 'WFER_TYPESMARCHESMS', 'ADMINDCMP'),
(382, 1, 'ADD_PROFIL', 'ADMINARMP'),
(383, 1, 'DEL_PROFIL', 'ADMINARMP'),
(384, 1, 'MOD_PROFIL', 'ADMINARMP'),
(385, 1, 'ADD_UTILISATEUR', 'ADMINARMP'),
(386, 0, 'ADD_UTILISATEURAC', 'ADMINARMP'),
(387, 0, 'ADD_UTILISATEURDNCMP', 'ADMINARMP'),
(388, 1, 'DEL_USER', 'ADMINARMP'),
(389, 1, 'HELP_USER', 'ADMINARMP'),
(390, 1, 'MOD_UTILISATEUR', 'ADMINARMP'),
(391, 0, 'MOD_UTILISATEURAC', 'ADMINARMP'),
(392, 0, 'MOD_UTILISATEURDNCMP', 'ADMINARMP'),
(393, 0, 'DEL_UTILISATEURAC', 'ADMINARMP'),
(394, 0, 'DEL_UTILISATEURDNCMP', 'ADMINARMP'),
(395, 1, 'DEL_UTILISATEURAC', 'ADMIN'),
(396, 1, 'DEL_UTILISATEURDNCMP', 'ADMIN'),
(397, 1, 'DEL_UTILISATEURAC', 'ADMINDCMP'),
(398, 1, 'DEL_UTILISATEURDNCMP', 'ADMINDCMP'),
(399, 1, 'DEL_UTILISATEURAC', 'AC'),
(400, 0, 'DEL_UTILISATEURDNCMP', 'AC'),
(401, 1, 'WDET_PROCEDURESPASSATIONS', 'AC'),
(402, 0, 'ADD_AC', 'ADMINARMP'),
(403, 0, 'MOD_AC', 'ADMINARMP'),
(404, 0, 'SUPP_AC', 'ADMINARMP'),
(405, 0, 'WDET_AC', 'ADMINARMP'),
(406, 0, 'WFER_AC', 'ADMINARMP'),
(407, 0, 'ADD_BAILLEUR', 'ADMINARMP'),
(408, 0, 'MOD_BAILLEUR', 'ADMINARMP'),
(409, 0, 'SUPP_BAILLEUR', 'ADMINARMP'),
(410, 0, 'ADD_CATEGORIES', 'ADMINARMP'),
(411, 0, 'MOD_CATEGORIES', 'ADMINARMP'),
(412, 0, 'SUPP_CATEGORIES', 'ADMINARMP'),
(413, 0, 'ADD_DELAIS', 'ADMINARMP'),
(414, 0, 'MOD_DELAIS', 'ADMINARMP'),
(415, 0, 'SUPP_DELAIS', 'ADMINARMP'),
(416, 0, 'ADD_FONCTIONS', 'ADMINARMP'),
(417, 0, 'MOD_FONCTIONS', 'ADMINARMP'),
(418, 0, 'SUPP_FONCTIONS', 'ADMINARMP'),
(419, 0, 'ADD_FOURNISSEUR', 'ADMINARMP'),
(420, 0, 'MOD_FOURNISSEUR', 'ADMINARMP'),
(421, 0, 'SUPP_FOURNISSEUR', 'ADMINARMP'),
(422, 0, 'ADD_GARANTIS', 'ADMINARMP'),
(423, 0, 'MOD_GARANTIS', 'ADMINARMP'),
(424, 0, 'SUPP_GARANTIS', 'ADMINARMP'),
(425, 0, 'ADD_CATEGORIE', 'ADMINARMP'),
(426, 0, 'MOD_CATEGORIE', 'ADMINARMP'),
(427, 0, 'SUPP_CATEGORIE', 'ADMINARMP'),
(428, 0, 'ADD_CRITERE', 'ADMINARMP'),
(429, 0, 'DEL_CRITERE', 'ADMINARMP'),
(430, 0, 'MOD_CRITERE', 'ADMINARMP'),
(431, 0, 'ADD_EVA', 'ADMINARMP'),
(432, 0, 'MOD_EVA', 'ADMINARMP'),
(433, 0, 'SUP_EVA', 'ADMINARMP'),
(434, 0, 'ADD_JOURSFERIES', 'ADMINARMP'),
(435, 0, 'MOD_JOURSFERIES', 'ADMINARMP'),
(436, 0, 'SUPP_JOURSFERIES', 'ADMINARMP'),
(440, 0, 'ADD_MODEPASSATION', 'ADMINARMP'),
(441, 0, 'MOD_MODEPASSATION', 'ADMINARMP'),
(442, 0, 'SUPP_MODEPASSATION', 'ADMINARMP'),
(443, 0, 'ADD_MODESELECTION', 'ADMINARMP'),
(444, 0, 'MOD_MODESELECTION', 'ADMINARMP'),
(445, 0, 'SUPP_MODESELECTION', 'ADMINARMP'),
(446, 0, 'ADD_MONNAI', 'ADMINARMP'),
(447, 0, 'DEL_MONNAI', 'ADMINARMP'),
(448, 0, 'MOD_MONNAI', 'ADMINARMP'),
(449, 0, 'ADD_NAT', 'ADMINARMP'),
(450, 0, 'MOD_NAT', 'ADMINARMP'),
(451, 0, 'SUP_NAT', 'ADMINARMP'),
(452, 0, 'ADD_POLEDCMP', 'ADMINARMP'),
(453, 0, 'MOD_POLEDCMP', 'ADMINARMP'),
(454, 0, 'SUPP_POLEDCMP', 'ADMINARMP'),
(455, 1, 'REFERENTIEL', 'ADMINARMP'),
(456, 0, 'ADD_SECTEURSACTIVITES', 'ADMINARMP'),
(457, 0, 'MOD_SECTEURSACTIVITES', 'ADMINARMP'),
(458, 0, 'SUPP_SECTEURSACTIVITES', 'ADMINARMP'),
(459, 0, 'ADD_SERVICEAC', 'ADMINARMP'),
(460, 0, 'MOD_SERVICEAC', 'ADMINARMP'),
(461, 0, 'SUPP_SERVICEAC', 'ADMINARMP'),
(462, 0, 'WFER_SERVICEAC', 'ADMINARMP'),
(463, 0, 'ADD_TYPEAC', 'ADMINARMP'),
(464, 0, 'MOD_TYPEAC', 'ADMINARMP'),
(465, 0, 'SUPP_TYPEAC', 'ADMINARMP'),
(466, 0, 'WDET_TYPEAC', 'ADMINARMP'),
(467, 1, 'ADD_TYPEDECISION', 'ADMINARMP'),
(468, 1, 'MOD_TYPEDECISION', 'ADMINARMP'),
(469, 1, 'SUPP_TYPEDECISION', 'ADMINARMP'),
(470, 0, 'ADD_TYPESERVICE', 'ADMINARMP'),
(471, 0, 'MOD_TYPESERVICE', 'ADMINARMP'),
(472, 0, 'SUPP_TYPESERVICE', 'ADMINARMP'),
(476, 0, 'WMODS_TYPESMARCHES', 'ADMINARMP'),
(477, 0, 'ADD_TYPESMARCHESMS', 'ADMINARMP'),
(478, 0, 'SUPP_TYPESMARCHESMS', 'ADMINARMP'),
(479, 0, 'WFER_TYPESMARCHESMS', 'ADMINARMP'),
(480, 1, 'workflowmanager_ADD', 'ADMIN'),
(481, 1, 'workflowmanager_MOD', 'ADMIN'),
(482, 1, 'workflowmanager_SUP', 'ADMIN'),
(483, 1, 'workflowmanager_WDETAILS', 'ADMIN'),
(484, 1, 'workflowmanager_WVISUALISER', 'ADMIN'),
(485, 1, 'workflowmanager_WVonReload', 'ADMIN'),
(486, 1, 'ADD_TYPEDECISION', 'ADMIN'),
(487, 1, 'MOD_TYPEDECISION', 'ADMIN'),
(488, 1, 'SUPP_TYPEDECISION', 'ADMIN'),
(489, 1, 'ADD_TYPESMARCHESMP', 'ADMIN'),
(490, 1, 'SUPP_TYPESMARCHESMP', 'ADMIN'),
(491, 1, 'WFER_TYPESMARCHESMP', 'ADMIN'),
(492, 1, 'WMODP_TYPESMARCHES', 'ADMIN'),
(493, 1, 'ADD_TYPEDECISION', 'AC'),
(494, 1, 'MOD_TYPEDECISION', 'AC'),
(495, 1, 'SUPP_TYPEDECISION', 'AC'),
(496, 0, 'WMODP_TYPESMARCHES', 'AC'),
(497, 0, 'ADD_TYPESMARCHESMP', 'AC'),
(498, 0, 'SUPP_TYPESMARCHESMP', 'AC'),
(499, 0, 'WFER_TYPESMARCHESMP', 'AC'),
(500, 1, 'ADD_COMMISSIONSMARCHES', 'AC'),
(501, 1, 'MOD_COMMISSIONSMARCHES', 'AC'),
(502, 1, 'SUPP_COMMISSIONSMARCHES', 'AC'),
(503, 1, 'ADD_CONTENTIEUX', 'ADMINARMP'),
(504, 1, 'MOD_CONTENTIEUX', 'ADMINARMP'),
(505, 1, 'SUPP_CONTENTIEUX', 'ADMINARMP'),
(506, 1, 'WSUIVI_CONTENTIEUX', 'ADMINARMP'),
(507, 0, 'ADD_COMMISSIONSMARCHES', 'ADMINARMP'),
(508, 0, 'MOD_COMMISSIONSMARCHES', 'ADMINARMP'),
(509, 0, 'SUPP_COMMISSIONSMARCHES', 'ADMINARMP'),
(510, 1, 'ADD_DECISION', 'ADMINARMP'),
(511, 1, 'MOD_DECISION', 'ADMINARMP'),
(512, 1, 'SUPP_DECISION', 'ADMINARMP'),
(513, 0, 'WMODP_TYPESMARCHES', 'ADMINARMP'),
(514, 0, 'ADD_TYPESMARCHESMP', 'ADMINARMP'),
(515, 0, 'SUPP_TYPESMARCHESMP', 'ADMINARMP'),
(516, 0, 'WFER_TYPESMARCHESMP', 'ADMINARMP'),
(517, 0, 'ADD_BUREAUXDCMP', 'AC'),
(518, 0, 'MOD_BUREAUXDCMP', 'AC'),
(519, 0, 'SUPP_BUREAUXDCMP', 'AC'),
(520, 0, 'ADD_AUDIT', 'AC'),
(521, 0, 'MOD_AUDIT', 'AC'),
(522, 0, 'SUPP_AUDIT', 'AC'),
(523, 0, 'WSUIVI_AUDITS', 'AC'),
(524, 0, 'ADD_DECISION', 'AC'),
(525, 0, 'MOD_DECISION', 'AC'),
(526, 0, 'SUPP_DECISION', 'AC'),
(527, 1, 'ADD_PAYS', 'AC'),
(528, 1, 'MOD_PAYS', 'AC'),
(529, 1, 'SUPP_PAYS', 'AC'),
(530, 1, ' MOD_PIECES', 'AC'),
(531, 1, 'ADD_PIECES', 'AC'),
(532, 1, 'SUPP_PIECES', 'AC'),
(533, 0, 'ADD_PRESTATAIRE', 'AC'),
(534, 0, 'MOD_PRESTATAIRE', 'AC'),
(535, 0, 'SUP_PRESTATAIRE', 'AC'),
(536, 1, 'ADD_PJCONT', 'ADMINARMP'),
(537, 1, 'MOD_PJCONT', 'ADMINARMP'),
(538, 1, 'SUPP_PJCONT', 'ADMINARMP'),
(539, 0, 'WDETAIL_PRESTATAIRE', 'AC'),
(540, 0, 'WCONTRATS', 'AC'),
(542, 0, 'ADD_DENONCIATION', 'AC'),
(543, 0, 'MOD_DENONCIATION', 'AC'),
(544, 0, 'SUPP_DENONCIATION', 'AC'),
(545, 1, 'PAIEMENT', 'ADMINARMP'),
(549, 0, 'ADD_SUIVIDENONCIATION', 'AC'),
(550, 0, 'MOD_SUIVIDENONCIATION', 'AC'),
(551, 0, 'SUPP_SUIVIDENONCIATION', 'AC'),
(552, 0, 'WSUIVI_DENONCIATIONC', 'AC'),
(553, 0, 'WSUIVI_DENONCIATIONN', 'AC'),
(554, 0, 'WSUIVI_DENONCIATIONP', 'AC'),
(555, 0, 'ADD_DENONCIATIONC', 'AC'),
(556, 0, 'MOD_DENONCIATIONC', 'AC'),
(557, 0, 'SUPP_DENONCIATIONC', 'AC'),
(558, 0, 'ADD_DENONCIATIONN', 'AC'),
(559, 0, 'MOD_DENONCIATIONN', 'AC'),
(560, 0, 'SUPP_DENONCIATIONN', 'AC'),
(561, 1, 'EDITER_OPERATION', 'ADMINARMP'),
(562, 1, 'RECOUVREMENT', 'ADMINARMP'),
(563, 0, 'WCLASSER_SANS_SUITE_DENONCIATIONC', 'AC'),
(564, 0, 'WCLASSER_SANS_SUITE_DENONCIATIONN', 'AC'),
(565, 0, 'WCLASSER_SANS_SUITE_DENONCIATIONP', 'AC'),
(566, 1, 'ADD_AUDIT', 'ADMINARMP'),
(567, 1, 'MOD_AUDIT', 'ADMINARMP'),
(568, 1, 'SUPP_AUDIT', 'ADMINARMP'),
(569, 0, 'WCONTRATS', 'ADMINARMP'),
(570, 1, 'WSUIVI_AUDITS', 'ADMINARMP'),
(571, 1, 'ADD_PRESTATAIRE', 'ADMINARMP'),
(572, 1, 'MOD_PRESTATAIRE', 'ADMINARMP'),
(573, 1, 'SUP_PRESTATAIRE', 'ADMINARMP'),
(574, 1, 'WDETAIL_PRESTATAIRE', 'ADMINARMP'),
(575, 1, 'ADD_DENONCIATION', 'ADMINARMP'),
(576, 1, 'MOD_DENONCIATION', 'ADMINARMP'),
(577, 1, 'SUPP_DENONCIATION', 'ADMINARMP'),
(578, 1, 'ADD_DENONCIATIONC', 'ADMINARMP'),
(579, 1, 'MOD_DENONCIATIONC', 'ADMINARMP'),
(580, 1, 'SUPP_DENONCIATIONC', 'ADMINARMP'),
(581, 1, 'WCLASSER_SANS_SUITE_DENONCIATIONC', 'ADMINARMP'),
(582, 1, 'WSUIVI_DENONCIATIONC', 'ADMINARMP'),
(583, 1, 'ADD_DENONCIATIONN', 'ADMINARMP'),
(584, 1, 'MOD_DENONCIATIONN', 'ADMINARMP'),
(585, 1, 'SUPP_DENONCIATIONN', 'ADMINARMP'),
(586, 1, 'WCLASSER_SANS_SUITE_DENONCIATIONN', 'ADMINARMP'),
(587, 1, 'WSUIVI_DENONCIATIONN', 'ADMINARMP'),
(588, 1, 'WCLASSER_SANS_SUITE_DENONCIATIONP', 'ADMINARMP'),
(589, 1, 'WSUIVI_DENONCIATIONP', 'ADMINARMP'),
(590, 0, 'ADD_SUIVIDENONCIATION', 'ADMINARMP'),
(591, 0, 'MOD_SUIVIDENONCIATION', 'ADMINARMP'),
(592, 0, 'SUPP_SUIVIDENONCIATION', 'ADMINARMP'),
(593, 1, 'EDITER_OPDAO', 'ADMINARMP'),
(595, 1, 'ADD_BANQUE', 'ADMINARMP'),
(596, 1, 'MOD_BANQUE', 'ADMINARMP'),
(597, 1, 'SUPP_BANQUE', 'ADMINARMP'),
(598, 0, 'ADD_PAYS', 'ADMINARMP'),
(599, 0, 'MOD_PAYS', 'ADMINARMP'),
(600, 0, 'SUPP_PAYS', 'ADMINARMP'),
(601, 0, ' MOD_PIECES', 'ADMINARMP'),
(602, 0, 'ADD_PIECES', 'ADMINARMP'),
(603, 0, 'SUPP_PIECES', 'ADMINARMP'),
(604, 0, 'ADD_REGNAT', 'ADMINARMP'),
(605, 0, 'MOD_REGNAT', 'ADMINARMP'),
(606, 0, 'SUPP_REGNAT', 'ADMINARMP'),
(607, 0, 'ADD_REGCOM', 'ADMINARMP'),
(608, 0, 'MOD_REGCOM', 'ADMINARMP'),
(609, 0, 'SUPP_REGCOM', 'ADMINARMP'),
(610, 1, 'ADD_DOSSIERTYPE', 'ADMINARMP'),
(611, 1, 'MOD_DOSSIERTYPE', 'ADMINARMP'),
(612, 1, 'SUPP_DOSSIERTYPE', 'ADMINARMP'),
(613, 0, 'ADD_DOSSIERTYPE', 'AC'),
(614, 0, 'MOD_DOSSIERTYPE', 'AC'),
(615, 0, 'SUPP_DOSSIERTYPE', 'AC'),
(616, 0, 'workflowmanager_ADD', 'ADMINDCMP'),
(617, 0, 'workflowmanager_MOD', 'ADMINDCMP'),
(618, 0, 'workflowmanager_SUP', 'ADMINDCMP'),
(619, 0, 'workflowmanager_WDETAILS', 'ADMINDCMP'),
(620, 0, 'workflowmanager_WVISUALISER', 'ADMINDCMP'),
(621, 0, 'workflowmanager_WVonReload', 'ADMINDCMP'),
(622, 0, 'workflowmanager_ADD', 'AC'),
(623, 0, 'workflowmanager_MOD', 'AC'),
(624, 0, 'workflowmanager_SUP', 'AC'),
(625, 0, 'workflowmanager_WDETAILS', 'AC'),
(626, 0, 'workflowmanager_WVISUALISER', 'AC'),
(627, 0, 'workflowmanager_WVonReload', 'AC'),
(631, 1, 'WRESTAURER_DENONCIATIONPO', 'ADMINARMP'),
(632, 1, 'ADD_CRITEREANA', 'ADMINDCMP'),
(633, 1, 'SUPP_CRITEREANA', 'ADMINDCMP'),
(634, 1, 'MOD_CRITEREANA', 'ADMINDCMP'),
(635, 1, 'ADD_NATURE', 'ADMINDCMP'),
(636, 1, 'MOD_NATURE', 'ADMINDCMP'),
(637, 1, 'SUPP_NATURE', 'ADMINDCMP'),
(638, 1, 'ADD_TRAITEMENT', 'ADMINDCMP'),
(639, 1, 'MOD_TRAITEMENT', 'ADMINDCMP'),
(640, 1, 'SUPP_TRAITEMENT', 'ADMINDCMP'),
(641, 1, 'ADD_RECEPTION', 'ADMINDCMP'),
(642, 1, 'MOD_RECEPTION', 'ADMINDCMP'),
(643, 1, 'SUPP_RECEPTION', 'ADMINDCMP'),
(644, 1, 'ADD_TYPECOURRIER', 'ADMINDCMP'),
(645, 1, 'MOD_TYPECOURRIER', 'ADMINDCMP'),
(646, 1, 'SUPP_TYPECOURRIER', 'ADMINDCMP'),
(647, 1, 'ADD_DOCUMENT', 'ADMINARMP'),
(648, 1, 'MOD_DOCUMENT', 'ADMINARMP'),
(649, 1, 'SUPP_DOCUMENT', 'ADMINARMP'),
(650, 1, 'ADD_TYPESDOSSIERS', 'ADMINDCMP'),
(651, 1, 'MOD_TYPESDOSSIERS', 'ADMINDCMP'),
(652, 1, 'SUPP_TYPESDOSSIERS', 'ADMINDCMP'),
(653, 1, 'WCONF_TYPESDOSSIERS', 'ADMINDCMP'),
(654, 1, 'ADD_CONFTYPESDOSSIERS', 'ADMINDCMP'),
(655, 1, 'MOD_CONFTYPESDOSSIERS', 'ADMINDCMP'),
(656, 1, 'SUPP_CONFTYPESDOSSIERS', 'ADMINDCMP'),
(657, 1, 'ADD_PIECESREQUISES', 'ADMINDCMP'),
(658, 1, 'SUPP_PIECESREQUISES', 'ADMINDCMP'),
(659, 1, 'ADD_RAPARMP', 'ADMINARMP'),
(660, 1, 'MOD_RAPARMP', 'ADMINARMP'),
(661, 1, 'SUPP_RAPARMP', 'ADMINARMP'),
(662, 1, 'ADD_GRILLESANALYSES', 'ADMINDCMP'),
(663, 1, 'SUPP_GRILLESANALYSES', 'ADMINDCMP'),
(664, 1, 'ADD_TYPEMODELEDOCUMENT', 'ADMINDCMP'),
(665, 1, 'SUPP_TYPEMODELEDOCUMENT', 'ADMINDCMP'),
(666, 1, 'ADD_MODELEDOCUMENT', 'ADMINDCMP'),
(667, 1, 'MOD_MODELEDOCUMENT', 'ADMINDCMP'),
(668, 1, 'SUPP_MODELEDOCUMENT', 'ADMINDCMP'),
(669, 1, 'ADD_TYPEUNITEORGDCMP', 'ADMINDCMP'),
(670, 1, 'MOD_TYPEUNITEORGDCMP', 'ADMINDCMP'),
(671, 1, 'SUPP_TYPEUNITEORGDCMP', 'ADMINDCMP'),
(672, 1, 'ADD_COURRIERAC', 'ADMINDCMP'),
(673, 1, 'MOD_COURRIERAC', 'ADMINDCMP'),
(674, 1, 'SUP_COURRIERAC', 'ADMINDCMP'),
(675, 1, 'WSUIVI_COURRIERAC', 'ADMINDCMP'),
(676, 1, 'TREE_TYPEUNITEORG', 'ADMINDCMP'),
(677, 1, 'ADD_AVIGENERAL', 'AC'),
(678, 1, 'MOD_AVIGENERAL', 'AC'),
(679, 1, 'SUPP_AVIGENERAL', 'AC'),
(680, 1, 'WPUBLIER_AVIGENERAL', 'AC'),
(681, 0, 'ADD_BANQUE', 'ADMINDCMP'),
(682, 0, 'MOD_BANQUE', 'ADMINDCMP'),
(683, 0, 'SUPP_BANQUE', 'ADMINDCMP'),
(684, 0, 'ADD_COMMISSIONSMARCHES', 'ADMINDCMP'),
(685, 0, 'MOD_COMMISSIONSMARCHES', 'ADMINDCMP'),
(686, 0, 'SUPP_COMMISSIONSMARCHES', 'ADMINDCMP'),
(687, 0, 'ADD_DECISION', 'ADMINDCMP'),
(688, 0, 'MOD_DECISION', 'ADMINDCMP'),
(689, 0, 'SUPP_DECISION', 'ADMINDCMP'),
(690, 0, 'ADD_PAYS', 'ADMINDCMP'),
(691, 0, 'MOD_PAYS', 'ADMINDCMP'),
(692, 0, 'SUPP_PAYS', 'ADMINDCMP'),
(693, 0, ' MOD_PIECES', 'ADMINDCMP'),
(694, 0, 'ADD_PIECES', 'ADMINDCMP'),
(695, 0, 'SUPP_PIECES', 'ADMINDCMP'),
(696, 0, 'ADD_RAPARMP', 'ADMINDCMP'),
(697, 0, 'MOD_RAPARMP', 'ADMINDCMP'),
(698, 0, 'SUPP_RAPARMP', 'ADMINDCMP'),
(699, 1, 'ADD_RAPDCMP', 'ADMINDCMP'),
(700, 1, 'MOD_RAPDCMP', 'ADMINDCMP'),
(701, 1, 'SUPP_RAPDCMP', 'ADMINDCMP'),
(702, 1, 'ADD_JOURNAL', 'ADMINDCMP'),
(703, 1, 'DEL_JOURNAL', 'ADMINDCMP'),
(704, 1, 'MOD_JOURNAL', 'ADMINDCMP'),
(705, 0, 'ADD_TYPEDECISION', 'ADMINDCMP'),
(706, 0, 'MOD_TYPEDECISION', 'ADMINDCMP'),
(707, 0, 'SUPP_TYPEDECISION', 'ADMINDCMP'),
(708, 0, 'WMODP_TYPESMARCHES', 'ADMINDCMP'),
(709, 0, 'ADD_TYPESMARCHESMP', 'ADMINDCMP'),
(710, 0, 'SUPP_TYPESMARCHESMP', 'ADMINDCMP'),
(711, 0, 'WFER_TYPESMARCHESMP', 'ADMINDCMP'),
(712, 1, 'ADD_GLOSSAIRE', 'ADMINDCMP'),
(713, 1, 'DEL_GLOSSAIRE', 'ADMINDCMP'),
(714, 1, 'MOD_GLOSSAIRE', 'ADMINDCMP'),
(715, 1, 'WDEPUBLIER_AVIGENERAL', 'AC'),
(716, 1, 'ADD_SUIVI', 'AC'),
(717, 1, 'ADD_TYPEUNITEORGARMP', 'ADMINARMP'),
(718, 1, 'MOD_TYPEUNITEORGARMP', 'ADMINARMP'),
(719, 1, 'SUPP_TYPEUNITEORGARMP', 'ADMINARMP'),
(720, 1, 'ADD_UNITEORGARMP', 'ADMINARMP'),
(721, 1, 'MOD_UNITEORGARMP', 'ADMINARMP'),
(722, 1, 'SUPP_UNITEORGARMP', 'ADMINARMP'),
(723, 1, 'ADD_ORGUNITARMP', 'ADMINARMP'),
(724, 0, 'ADD_BANQUE', 'AC'),
(725, 0, 'MOD_BANQUE', 'AC'),
(726, 0, 'SUPP_BANQUE', 'AC'),
(727, 0, 'ADD_RAPARMP', 'AC'),
(728, 0, 'MOD_RAPARMP', 'AC'),
(729, 0, 'SUPP_RAPARMP', 'AC'),
(730, 0, 'ADD_RAPDCMP', 'AC'),
(731, 0, 'MOD_RAPDCMP', 'AC'),
(732, 0, 'SUPP_RAPDCMP', 'AC'),
(733, 0, 'ADD_GLOSSAIRE', 'AC'),
(734, 0, 'DEL_GLOSSAIRE', 'AC'),
(735, 0, 'MOD_GLOSSAIRE', 'AC'),
(736, 0, 'ADD_JOURNAL', 'AC'),
(737, 0, 'DEL_JOURNAL', 'AC'),
(738, 0, 'MOD_JOURNAL', 'AC'),
(739, 1, 'ADD_TYPEDEMANDE', 'AC'),
(740, 1, 'MOD_TYPEDEMANDE', 'AC'),
(741, 1, 'SUPP_TYPEDEMANDE', 'AC'),
(742, 0, 'ADD_TYPEDEMANDE', 'ADMINDCMP'),
(743, 0, 'MOD_TYPEDEMANDE', 'ADMINDCMP'),
(744, 0, 'SUPP_TYPEDEMANDE', 'ADMINDCMP'),
(745, 0, 'DOSSIERSPP', 'AC'),
(746, 0, 'WSOUMETTRE_PROCEDURE', 'AC'),
(747, 1, 'WSOUMETTRE_PROCEDUREDEROGATOIRE', 'AC'),
(748, 1, 'ADD_NPROCEDUREDEROGATOIRE', 'AC'),
(749, 0, 'DAC_VOIR', 'ADMINDCMP'),
(750, 1, 'ADD_DOSSIERAC', 'ADMINDCMP'),
(751, 1, 'MOD_DOSSIERAC', 'ADMINDCMP'),
(752, 1, 'SUPP_DOSSIERAC', 'ADMINDCMP'),
(753, 1, 'SUIVI_DOSSIERAC', 'ADMINDCMP'),
(754, 1, 'ADD_PROCEDUREDEROGATOIRE', 'AC'),
(755, 0, 'MOD_PROCEDUREDEROGATOIRE', 'AC'),
(756, 0, 'SUPP_PROCEDUREDEROGATOIRE', 'AC'),
(757, 1, 'ADD_DPROCEDURESDEROGATOIRES', 'AC'),
(758, 1, 'MOD_DPROCEDURESDEROGATOIRES', 'AC'),
(759, 1, 'SUPP_DPROCEDURESDEROGATOIRES', 'AC'),
(760, 1, 'ADD_FAMILLES', 'ADMINDCMP'),
(761, 1, 'MOD_FAMILLES', 'ADMINDCMP'),
(762, 1, 'SUPP_FAMILLES', 'ADMINDCMP'),
(763, 1, 'ADD_REFCATEGORIES', 'ADMINDCMP'),
(764, 1, 'MOD_REFCATEGORIES', 'ADMINDCMP'),
(765, 1, 'SUPP_REFCATEGORIES', 'ADMINDCMP'),
(766, 1, 'ADD_REFERENTIELPRIX', 'ADMINDCMP'),
(767, 1, 'MOD_REFERENTIELPRIX', 'ADMINDCMP'),
(768, 1, 'SUPP_REFERENTIELPRIX', 'ADMINDCMP'),
(769, 1, 'NOUVEAU_CONT', 'ADMINARMP'),
(771, 1, 'ADD_CONT_DEC', 'ADMINARMP'),
(772, 1, 'MOD_CONT_DEC', 'ADMINARMP'),
(773, 1, 'SUPP_CONT_DEC', 'ADMINARMP'),
(774, 1, 'ADD_TAUX_PARAFISCALE', 'ADMINARMP'),
(775, 1, 'ADD_TAUX_VENTEDAO', 'ADMINARMP'),
(776, 1, 'EDIT_CONTRAT', 'ADMINARMP'),
(777, 1, 'WADD_PAIEMENT', 'ADMINARMP'),
(778, 1, 'EDITER_QUITUS', 'ADMINARMP'),
(779, 1, 'EDIT_DOSSIER', 'ADMINARMP'),
(780, 1, 'WADD_RECOUVREMENT', 'ADMINARMP'),
(781, 1, 'ADD_CAT', 'ADMINDCMP'),
(782, 1, 'MOD_CAT', 'ADMINDCMP'),
(783, 1, 'SUPP_CAT', 'ADMINDCMP'),
(784, 1, 'ADD_SPECIALITE', 'ADMIN'),
(785, 1, 'DEL_SPECIALITE', 'ADMIN'),
(786, 1, 'MOD_SPECIALITE', 'ADMIN'),
(787, 1, 'ADD_MODULE', 'ADMIN'),
(788, 1, 'DEL_MODULE', 'ADMIN'),
(789, 1, 'MOD_MODULE', 'ADMIN'),
(790, 0, 'ADD_FAMILLES', 'ADMINARMP'),
(791, 0, 'MOD_FAMILLES', 'ADMINARMP'),
(792, 0, 'SUPP_FAMILLES', 'ADMINARMP'),
(793, 0, 'ADD_REFCATEGORIES', 'ADMINARMP'),
(794, 0, 'MOD_REFCATEGORIES', 'ADMINARMP'),
(795, 0, 'SUPP_REFCATEGORIES', 'ADMINARMP'),
(796, 0, 'ADD_REFERENTIELPRIX', 'ADMINARMP'),
(797, 0, 'MOD_REFERENTIELPRIX', 'ADMINARMP'),
(798, 0, 'SUPP_REFERENTIELPRIX', 'ADMINARMP'),
(799, 1, 'ADD_COURRIERACDOS', 'ADMINDCMP'),
(800, 1, 'SUP_COURRIERACDOS', 'ADMINDCMP'),
(801, 0, 'ADD_MODULE', 'ADMINDCMP'),
(802, 0, 'DEL_MODULE', 'ADMINDCMP'),
(803, 0, 'MOD_MODULE', 'ADMINDCMP'),
(804, 0, 'ADD_SPECIALITE', 'ADMINDCMP'),
(805, 0, 'DEL_SPECIALITE', 'ADMINDCMP'),
(806, 0, 'MOD_SPECIALITE', 'ADMINDCMP'),
(807, 0, 'ADD_BANQUE', 'ADMIN'),
(808, 0, 'MOD_BANQUE', 'ADMIN'),
(809, 0, 'SUPP_BANQUE', 'ADMIN'),
(810, 0, 'ADD_CAT', 'ADMIN'),
(811, 0, 'MOD_CAT', 'ADMIN'),
(812, 0, 'SUPP_CAT', 'ADMIN'),
(813, 1, 'ADD_CODE_MARCHE', 'ADMIN'),
(814, 1, 'MOD_CODE_MARCHE', 'ADMIN'),
(815, 1, 'SUP_CODE_MARCHE', 'ADMIN'),
(816, 0, 'ADD_COMMISSIONSMARCHES', 'ADMIN'),
(817, 0, 'MOD_COMMISSIONSMARCHES', 'ADMIN'),
(818, 0, 'SUPP_COMMISSIONSMARCHES', 'ADMIN'),
(819, 0, 'ADD_DECISION', 'ADMIN'),
(820, 0, 'MOD_DECISION', 'ADMIN'),
(821, 0, 'SUPP_DECISION', 'ADMIN'),
(822, 0, 'ADD_PAYS', 'ADMIN'),
(823, 0, 'MOD_PAYS', 'ADMIN'),
(824, 0, 'SUPP_PAYS', 'ADMIN'),
(825, 0, ' MOD_PIECES', 'ADMIN'),
(826, 0, 'ADD_PIECES', 'ADMIN'),
(827, 0, 'SUPP_PIECES', 'ADMIN'),
(828, 0, 'ADD_RAPARMP', 'ADMIN'),
(829, 0, 'MOD_RAPARMP', 'ADMIN'),
(830, 0, 'SUPP_RAPARMP', 'ADMIN'),
(831, 0, 'ADD_RAPDCMP', 'ADMIN'),
(832, 0, 'MOD_RAPDCMP', 'ADMIN'),
(833, 0, 'SUPP_RAPDCMP', 'ADMIN'),
(834, 0, 'ADD_GLOSSAIRE', 'ADMIN'),
(835, 0, 'DEL_GLOSSAIRE', 'ADMIN'),
(836, 0, 'MOD_GLOSSAIRE', 'ADMIN'),
(837, 0, 'ADD_JOURNAL', 'ADMIN'),
(838, 0, 'DEL_JOURNAL', 'ADMIN'),
(839, 0, 'MOD_JOURNAL', 'ADMIN'),
(840, 0, 'ADD_TYPEDEMANDE', 'ADMIN'),
(841, 0, 'MOD_TYPEDEMANDE', 'ADMIN'),
(842, 0, 'SUPP_TYPEDEMANDE', 'ADMIN'),
(843, 0, 'ADD_CAT', 'ADMINARMP'),
(844, 0, 'MOD_CAT', 'ADMINARMP'),
(845, 0, 'SUPP_CAT', 'ADMINARMP'),
(846, 1, 'ADD_CODE_MARCHE', 'ADMINARMP'),
(847, 1, 'MOD_CODE_MARCHE', 'ADMINARMP'),
(848, 1, 'SUP_CODE_MARCHE', 'ADMINARMP'),
(849, 0, 'ADD_RAPDCMP', 'ADMINARMP'),
(850, 0, 'MOD_RAPDCMP', 'ADMINARMP'),
(851, 0, 'SUPP_RAPDCMP', 'ADMINARMP'),
(852, 1, 'ADD_GLOSSAIRE', 'ADMINARMP'),
(853, 1, 'DEL_GLOSSAIRE', 'ADMINARMP'),
(854, 1, 'MOD_GLOSSAIRE', 'ADMINARMP'),
(855, 1, 'ADD_JOURNAL', 'ADMINARMP'),
(856, 1, 'DEL_JOURNAL', 'ADMINARMP'),
(857, 1, 'MOD_JOURNAL', 'ADMINARMP'),
(858, 0, 'ADD_TYPEDEMANDE', 'ADMINARMP'),
(859, 0, 'MOD_TYPEDEMANDE', 'ADMINARMP'),
(860, 0, 'SUPP_TYPEDEMANDE', 'ADMINARMP'),
(861, 0, 'ADD_FORMATEUR', 'ADMINDCMP'),
(862, 0, 'DEL_FORMATEUR', 'ADMINDCMP'),
(863, 0, 'MOD_FORMATEUR', 'ADMINDCMP'),
(864, 1, 'ADD_TEXTE_REGLEMENTAIRE', 'ADMINARMP'),
(865, 1, 'ADD_FICHIER_LEGISLATION', 'ADMINARMP'),
(866, 1, 'MOD_FICHIER_LEGISLATION', 'ADMINARMP'),
(867, 1, 'SUP_FICHIER_LEGISLATION', 'ADMINARMP'),
(868, 0, 'DETAIL_FORMATEUR', 'ADMINDCMP'),
(869, 0, 'ADD_PRODUITS', 'ADMINARMP'),
(870, 0, 'MOD_PRODUITS', 'ADMINARMP'),
(871, 0, 'SUPP_PRODUITS', 'ADMINARMP'),
(872, 0, 'WSAISI_PRIX', 'ADMINARMP'),
(873, 0, 'SAISIPRIX', 'ADMINARMP'),
(874, 1, 'ADD_PRODUITS', 'ADMINDCMP'),
(875, 1, 'MOD_PRODUITS', 'ADMINDCMP'),
(876, 1, 'SUPP_PRODUITS', 'ADMINDCMP'),
(877, 0, 'WSAISI_PRIX', 'ADMINDCMP'),
(878, 1, 'SAISIPRIX', 'ADMINDCMP'),
(879, 1, 'VAL_PRIX', 'ADMINDCMP'),
(880, 1, 'WPUB_PRIX', 'ADMINDCMP'),
(881, 0, 'ADD_FORM', 'ADMINDCMP'),
(882, 0, 'DETAIL_FORM', 'ADMINDCMP'),
(883, 0, 'MOD_FORM', 'ADMINDCMP'),
(884, 0, 'SUP_FORM', 'ADMINDCMP'),
(885, 0, 'ADD_FORM', 'AC'),
(886, 0, 'DETAIL_FORM', 'AC'),
(887, 0, 'MOD_FORM', 'AC'),
(888, 0, 'SUP_FORM', 'AC'),
(889, 0, 'ADD_FORMATEUR', 'AC'),
(890, 0, 'DEL_FORMATEUR', 'AC'),
(891, 0, 'DETAIL_FORMATEUR', 'AC'),
(892, 0, 'MOD_FORMATEUR', 'AC'),
(893, 0, 'ADD_MODULE', 'AC'),
(894, 0, 'DEL_MODULE', 'AC'),
(895, 0, 'MOD_MODULE', 'AC'),
(896, 0, 'ADD_SPECIALITE', 'AC'),
(897, 0, 'DEL_SPECIALITE', 'AC'),
(898, 0, 'MOD_SPECIALITE', 'AC'),
(899, 1, 'DOSSIERSPP', 'ADMINDCMP'),
(900, 1, 'EXAMENJUR', 'ADMINDCMP'),
(901, 1, 'REVUEDAO', 'ADMINDCMP'),
(902, 1, 'VALIDAP', 'ADMINDCMP'),
(903, 0, 'WSOUM_PLAN', 'ADMINDCMP'),
(904, 1, 'WVALID_PLAN', 'ADMINDCMP'),
(905, 1, 'WWPUB_PLAN', 'ADMINDCMP'),
(906, 1, 'WZREAL_FERMER', 'ADMINDCMP'),
(907, 1, 'WREAL_PLANPASSATIONS', 'ADMINDCMP'),
(908, 0, 'WREAL_PLANPASSATIONS', 'AC'),
(909, 1, 'WSOUM_PLAN', 'AC'),
(910, 0, 'WVALID_PLAN', 'AC'),
(911, 0, 'WWPUB_PLAN', 'AC'),
(912, 1, 'WZREAL_FERMER', 'AC'),
(913, 0, 'ADD_CAT', 'AC'),
(914, 0, 'MOD_CAT', 'AC'),
(915, 0, 'SUPP_CAT', 'AC'),
(916, 1, 'ADD_DIRECTIONSERVICE', 'AC'),
(917, 1, 'MOD_DIRECTIONSERVICE', 'AC'),
(918, 1, 'SUPP_DIRECTIONSERVICE', 'AC'),
(919, 0, 'ADD_FICHIER_LEGISLATION', 'AC'),
(920, 0, 'MOD_FICHIER_LEGISLATION', 'AC'),
(921, 0, 'SUP_FICHIER_LEGISLATION', 'AC'),
(922, 1, 'ADD_AUTORITEAUTC', 'ADMINDCMP'),
(923, 1, 'MOD_AUTORITEAUTC', 'ADMINDCMP'),
(924, 1, 'SUPP_AUTORITEAUTC', 'ADMINDCMP'),
(925, 0, 'ADD_DIRECTIONSERVICE', 'ADMINDCMP'),
(926, 0, 'MOD_DIRECTIONSERVICE', 'ADMINDCMP'),
(927, 0, 'SUPP_DIRECTIONSERVICE', 'ADMINDCMP'),
(928, 0, 'ADD_FICHIER_LEGISLATION', 'ADMINDCMP'),
(929, 0, 'MOD_FICHIER_LEGISLATION', 'ADMINDCMP'),
(930, 0, 'SUP_FICHIER_LEGISLATION', 'ADMINDCMP'),
(931, 1, 'ADD_MONTANTSSEUILS', 'ADMINDCMP'),
(932, 1, 'MOD_MONTANTSSEUILS', 'ADMINDCMP'),
(933, 1, 'SUPP_MONTANTSSEUILS', 'ADMINDCMP'),
(934, 1, 'ADD_SEUILSCOMMUNAUTAIRE', 'ADMINDCMP'),
(935, 1, 'MOD_SEUILSCOMMUNAUTAIRE', 'ADMINDCMP'),
(936, 1, 'SUPP_SEUILSCOMMUNAUTAIRE', 'ADMINDCMP'),
(937, 1, 'ADD_SEUILSRAPRIORI', 'ADMINDCMP'),
(938, 1, 'MOD_SEUILSRAPRIORI', 'ADMINDCMP'),
(939, 1, 'SUPP_SEUILSRAPRIORI', 'ADMINDCMP'),
(940, 1, 'ADD_MONTANTSSEUILS', 'AC'),
(941, 1, 'MOD_MONTANTSSEUILS', 'AC'),
(942, 1, 'SUPP_MONTANTSSEUILS', 'AC'),
(943, 1, 'ADD_SEUILSCOMMUNAUTAIRE', 'AC'),
(944, 1, 'MOD_SEUILSCOMMUNAUTAIRE', 'AC'),
(945, 1, 'SUPP_SEUILSCOMMUNAUTAIRE', 'AC'),
(946, 1, 'ADD_SEUILSRAPRIORI', 'AC'),
(947, 1, 'MOD_SEUILSRAPRIORI', 'AC'),
(948, 1, 'SUPP_SEUILSRAPRIORI', 'AC'),
(949, 1, 'MAJPLANPASSATION', 'AC'),
(950, 1, 'MWREAL_PLANPASSATIONS', 'ADMINDCMP'),
(951, 1, 'MWVALID_PLANPASSATION', 'ADMINDCMP'),
(952, 1, 'MWWPUB_PLANPASSATION', 'ADMINDCMP'),
(953, 0, 'MAJPLANPASSATION', 'ADMINDCMP'),
(954, 1, 'WREAL_PLANSPUBLIES', 'ADMINDCMP'),
(955, 1, 'MOD_DEMANDE_PAIEMENT', 'AC'),
(956, 1, 'SUI_DEMANDE_PAIEMENT', 'AC'),
(957, 1, 'SUP_DEMANDE_PAIEMENT', 'AC'),
(958, 1, 'MATDOSSIER', 'ADMINDCMP'),
(959, 1, 'MOD_DEMANDE_PAIEMENT', 'ADMINDCMP'),
(960, 1, 'SUI_DEMANDE_PAIEMENT', 'ADMINDCMP'),
(961, 1, 'SUP_DEMANDE_PAIEMENT', 'ADMINDCMP'),
(962, 1, 'ADD_SUIVI', 'ADMINDCMP'),
(963, 1, 'ADD_DEMANDE_PAIEMENT', 'AC'),
(964, 1, 'ADD_FEA_MODELE_ARC', 'AC'),
(965, 1, 'DEL_FEA_MODELE_ARC', 'AC'),
(966, 1, 'UPDATE_FEA_MODELE_ARC', 'AC'),
(967, 1, 'ADD_GESTIONNOEUDARCHIVE', 'AC'),
(968, 1, 'DEL_GESTIONNOEUDARCHIVE', 'AC'),
(969, 1, 'MOD_GESTIONNOEUDARCHIVE', 'AC'),
(970, 1, 'ADD_FORM', 'ADMINARMP'),
(971, 1, 'DETAIL_FORM', 'ADMINARMP'),
(972, 1, 'MOD_FORM', 'ADMINARMP'),
(973, 1, 'SUP_FORM', 'ADMINARMP'),
(974, 1, 'ADD_FORMATEUR', 'ADMINARMP'),
(975, 1, 'DEL_FORMATEUR', 'ADMINARMP'),
(976, 1, 'DETAIL_FORMATEUR', 'ADMINARMP'),
(977, 1, 'MOD_FORMATEUR', 'ADMINARMP'),
(978, 1, 'ADD_MODULE', 'ADMINARMP'),
(979, 1, 'DEL_MODULE', 'ADMINARMP'),
(980, 1, 'MOD_MODULE', 'ADMINARMP'),
(981, 1, 'ADD_SPECIALITE', 'ADMINARMP'),
(982, 1, 'DEL_SPECIALITE', 'ADMINARMP'),
(983, 1, 'MOD_SPECIALITE', 'ADMINARMP'),
(984, 1, 'ADD_PROJETREP', 'ADMINDCMP'),
(985, 1, 'LAST_PROJETREP', 'ADMINDCMP'),
(986, 1, 'MOD_PROJETREP', 'ADMINDCMP'),
(987, 1, 'SUP_PROJETREP', 'ADMINDCMP'),
(988, 0, 'EDITER_OPERATION', 'ADMINDCMP'),
(989, 0, 'PAIEMENT', 'ADMINDCMP'),
(990, 0, 'EDITER_QUITUS', 'ADMINDCMP'),
(991, 0, 'ADD_PAIEMENT', 'ADMINDCMP'),
(992, 1, 'EDIT_CONTRAT', 'ADMINDCMP'),
(993, 0, 'WADD_PAIEMENT', 'ADMINDCMP'),
(994, 0, 'ADD_TAUX_PARAFISCALE', 'ADMINDCMP'),
(995, 1, 'ADD_DEMANDE_PAIEMENT', 'ADMINDCMP'),
(996, 1, 'ADD_FEA_MODELE_ARC', 'ADMINARMP'),
(997, 1, 'DEL_FEA_MODELE_ARC', 'ADMINARMP'),
(998, 1, 'UPDATE_FEA_MODELE_ARC', 'ADMINARMP'),
(999, 1, 'ADD_GESTIONNOEUDARCHIVE', 'ADMINARMP'),
(1000, 1, 'DEL_GESTIONNOEUDARCHIVE', 'ADMINARMP'),
(1001, 1, 'MOD_GESTIONNOEUDARCHIVE', 'ADMINARMP'),
(1002, 1, 'DEL_ARCHIVAGEMODELE', 'ADMINARMP'),
(1003, 1, 'MOD_ARCHIVAGEMODELE', 'ADMINARMP'),
(1004, 1, 'DEL_ARCHIVECONFIG', 'ADMINARMP'),
(1005, 1, 'GERER_ARCHIVECONFIG', 'ADMINARMP'),
(1006, 1, 'MOD_ARCHIVECONFIG', 'ADMINARMP'),
(1007, 1, 'DEL_ARCHIVEGESTION', 'ADMINARMP'),
(1008, 1, 'MOD_ARCHIVEGESTION', 'ADMINARMP'),
(1009, 1, 'DEL_ARCHIVAGEMODELE', 'AC'),
(1010, 1, 'MOD_ARCHIVAGEMODELE', 'AC'),
(1011, 1, 'DEL_ARCHIVECONFIG', 'AC'),
(1012, 1, 'GERER_ARCHIVECONFIG', 'AC'),
(1013, 1, 'MOD_ARCHIVECONFIG', 'AC'),
(1014, 1, 'DEL_ARCHIVEGESTION', 'AC'),
(1015, 1, 'MOD_ARCHIVEGESTION', 'AC'),
(1016, 0, 'ADD_AVIGENERAL', 'ADMINARMP'),
(1017, 0, 'MOD_AVIGENERAL', 'ADMINARMP'),
(1018, 0, 'SUPP_AVIGENERAL', 'ADMINARMP'),
(1019, 0, 'WDEPUBLIER_AVIGENERAL', 'ADMINARMP'),
(1020, 0, 'WPUBLIER_AVIGENERAL', 'ADMINARMP'),
(1021, 0, 'WSUIVIDAO', 'AC'),
(1022, 1, 'SUIVI_FORM', 'ADMINARMP'),
(1023, 0, 'ADD_DPROCEDURESDEROGATOIRES', 'ADMINDCMP'),
(1024, 0, 'MOD_DPROCEDURESDEROGATOIRES', 'ADMINDCMP'),
(1025, 0, 'SUPP_DPROCEDURESDEROGATOIRES', 'ADMINDCMP'),
(1026, 0, 'WSUIVIDAO', 'ADMINDCMP'),
(1027, 0, 'ADD_NPROCEDUREDEROGATOIRE', 'ADMINDCMP'),
(1028, 0, 'PASSATIONSMARCHES', 'ADMINDCMP'),
(1029, 0, 'WSOUMETTRE_PROCEDURE', 'ADMINDCMP'),
(1030, 0, 'ADD_PROCEDUREDEROGATOIRE', 'ADMINDCMP'),
(1031, 0, 'MOD_PROCEDUREDEROGATOIRE', 'ADMINDCMP'),
(1032, 0, 'SUPP_PROCEDUREDEROGATOIRE', 'ADMINDCMP'),
(1033, 0, 'WSOUMETTRE_PROCEDUREDEROGATOIRE', 'ADMINDCMP'),
(1034, 0, 'ADD_PROCEDURESPASSATIONS', 'ADMINDCMP'),
(1035, 0, 'MOD_PROCEDURESPASSATIONS', 'ADMINDCMP'),
(1036, 0, 'WDET_PROCEDURESPASSATIONS', 'ADMINDCMP'),
(1037, 0, 'WGER_PROCEDURESPASSATIONS', 'ADMINDCMP'),
(1038, 1, 'WVALIDER_PROCEDUREDEROGATOIRE', 'ADMINDCMP'),
(1039, 0, 'WVALIDER_PROCEDUREDEROGATOIRE', 'AC'),
(1040, 1, 'ADD_ARRETES', 'ADMINARMP'),
(1041, 1, 'SUPP_ARRETES', 'ADMINARMP'),
(1042, 1, 'MOD_ARRETES', 'ADMINARMP'),
(1043, 1, 'WDEPUBLIER_ARRETES', 'ADMINARMP'),
(1044, 1, 'WPUBLIER_ARRETES', 'ADMINARMP'),
(1045, 1, 'ADD_LISTE_ROUGE_F', 'ADMINARMP'),
(1046, 1, 'MOD_LISTE_ROUGE_F', 'ADMINARMP'),
(1048, 1, 'SUP_LISTE_ROUGE_F', 'ADMINARMP'),
(1049, 1, 'STATUEMOA', 'ADMINDCMP'),
(1050, 1, 'ADD_RAPFORMATION', 'ADMINARMP'),
(1051, 1, 'MOD_RAPFORMATION', 'ADMINARMP'),
(1052, 1, 'SUPP_RAPFORMATION', 'ADMINARMP'),
(1053, 1, 'ADD_PARTFORMATION', 'ADMINARMP'),
(1054, 1, 'MOD_PARTFORMATION', 'ADMINARMP'),
(1055, 1, 'SUPP_PARTFORMATION', 'ADMINARMP'),
(1056, 0, 'ADD_AUTORITEAUTC', 'ADMINARMP'),
(1057, 0, 'MOD_AUTORITEAUTC', 'ADMINARMP'),
(1058, 0, 'SUPP_AUTORITEAUTC', 'ADMINARMP'),
(1059, 1, 'ADD_COORDINNATEUR', 'ADMINARMP'),
(1060, 1, 'DEL_COORDINNATEUR', 'ADMINARMP'),
(1061, 1, 'MOD_COORDINNATEUR', 'ADMINARMP'),
(1062, 0, 'ADD_DIRECTIONSERVICE', 'ADMINARMP'),
(1063, 0, 'MOD_DIRECTIONSERVICE', 'ADMINARMP'),
(1064, 0, 'SUPP_DIRECTIONSERVICE', 'ADMINARMP'),
(1065, 0, 'ADD_GROUPE', 'ADMINARMP'),
(1066, 0, 'MOD_GROUPE', 'ADMINARMP'),
(1067, 0, 'SUP_GROUPE', 'ADMINARMP'),
(1068, 1, 'ADD_GROUPEFORM', 'ADMINARMP'),
(1069, 1, 'DEL_GROUPEFORM', 'ADMINARMP'),
(1070, 1, 'MOD_GROUPEFORM', 'ADMINARMP'),
(1071, 1, 'ADD_LOGISTICIEN', 'ADMINARMP'),
(1072, 1, 'DEL_LOGISTICIEN', 'ADMINARMP'),
(1073, 1, 'MOD_LOGISTICIEN', 'ADMINARMP'),
(1074, 0, 'MODEPASSATION', 'ADMINARMP'),
(1075, 0, 'ADD_RECEPTION', 'ADMINARMP'),
(1076, 0, 'MOD_RECEPTION', 'ADMINARMP'),
(1077, 0, 'SUPP_RECEPTION', 'ADMINARMP'),
(1078, 0, 'ADD_TRAITEMENT', 'ADMINARMP'),
(1079, 0, 'MOD_TRAITEMENT', 'ADMINARMP'),
(1080, 0, 'SUPP_TRAITEMENT', 'ADMINARMP'),
(1081, 0, 'ADD_NATURE', 'ADMINARMP'),
(1082, 0, 'MOD_NATURE', 'ADMINARMP'),
(1083, 0, 'SUPP_NATURE', 'ADMINARMP'),
(1084, 0, 'ADD_TYPECOURRIER', 'ADMINARMP'),
(1085, 0, 'MOD_TYPECOURRIER', 'ADMINARMP'),
(1086, 0, 'SUPP_TYPECOURRIER', 'ADMINARMP'),
(1087, 1, 'ADD_TYPERAP', 'ADMINARMP'),
(1088, 1, 'DEL_TYPERAP', 'ADMINARMP'),
(1089, 1, 'MOD_TYPERAP', 'ADMINARMP'),
(1090, 1, 'ADD_AUTORITE', 'ADMINARMP'),
(1091, 0, 'ADD_AUTORITEAUTC', 'AC'),
(1092, 0, 'MOD_AUTORITEAUTC', 'AC'),
(1093, 0, 'SUPP_AUTORITEAUTC', 'AC'),
(1094, 0, 'ADD_COORDINNATEUR', 'AC'),
(1095, 0, 'DEL_COORDINNATEUR', 'AC'),
(1096, 0, 'MOD_COORDINNATEUR', 'AC'),
(1097, 0, 'ADD_GROUPE', 'AC'),
(1098, 0, 'MOD_GROUPE', 'AC'),
(1099, 0, 'SUP_GROUPE', 'AC'),
(1100, 0, 'ADD_GROUPEFORM', 'AC'),
(1101, 0, 'DEL_GROUPEFORM', 'AC'),
(1102, 0, 'MOD_GROUPEFORM', 'AC'),
(1103, 0, 'ADD_LOGISTICIEN', 'AC'),
(1104, 0, 'DEL_LOGISTICIEN', 'AC'),
(1105, 0, 'MOD_LOGISTICIEN', 'AC'),
(1106, 0, 'MODEPASSATION', 'AC'),
(1107, 1, 'ADD_RECEPTION', 'AC'),
(1108, 1, 'MOD_RECEPTION', 'AC'),
(1109, 1, 'SUPP_RECEPTION', 'AC'),
(1110, 0, 'ADD_TRAITEMENT', 'AC'),
(1111, 0, 'MOD_TRAITEMENT', 'AC'),
(1112, 0, 'SUPP_TRAITEMENT', 'AC'),
(1113, 0, 'ADD_NATURE', 'AC'),
(1114, 0, 'MOD_NATURE', 'AC'),
(1115, 0, 'SUPP_NATURE', 'AC'),
(1116, 0, 'ADD_TYPECOURRIER', 'AC'),
(1117, 0, 'MOD_TYPECOURRIER', 'AC'),
(1118, 0, 'SUPP_TYPECOURRIER', 'AC'),
(1119, 0, 'ADD_TYPERAP', 'AC'),
(1120, 0, 'DEL_TYPERAP', 'AC'),
(1121, 0, 'MOD_TYPERAP', 'AC'),
(1122, 0, 'ADD_AUTORITE', 'ADMINDCMP'),
(1123, 0, 'SUIVI_FORM', 'ADMINDCMP'),
(1124, 0, 'ADD_RAPFORMATION', 'ADMINDCMP'),
(1125, 0, 'MOD_RAPFORMATION', 'ADMINDCMP'),
(1126, 0, 'SUPP_RAPFORMATION', 'ADMINDCMP'),
(1127, 0, 'ADD_PARTFORMATION', 'ADMINDCMP'),
(1128, 0, 'MOD_PARTFORMATION', 'ADMINDCMP'),
(1129, 0, 'SUPP_PARTFORMATION', 'ADMINDCMP'),
(1130, 1, 'ADD_PJDENONCIATION', 'ADMINARMP'),
(1131, 1, 'MOD_PJDENONCIATION', 'ADMINARMP'),
(1132, 1, 'SUPP_PJDENONCIATION', 'ADMINARMP'),
(1133, 1, 'GESTION_CONFAUDIT', 'ADMINARMP'),
(1134, 1, 'GESTION_CONSJRNL', 'ADMINARMP'),
(1135, 0, 'workflowmanager_ADD', 'ADMINARMP'),
(1136, 0, 'workflowmanager_MOD', 'ADMINARMP'),
(1137, 0, 'workflowmanager_SUP', 'ADMINARMP'),
(1138, 0, 'workflowmanager_WDETAILS', 'ADMINARMP'),
(1139, 0, 'workflowmanager_WVISUALISER', 'ADMINARMP'),
(1140, 0, 'workflowmanager_WVonReload', 'ADMINARMP'),
(1141, 0, 'TREE_TYPEUNITEORG', 'ADMINARMP'),
(1142, 1, 'ADD_BUREAUXDCMP', 'ADMINARMP'),
(1143, 1, 'MOD_BUREAUXDCMP', 'ADMINARMP'),
(1144, 1, 'SUPP_BUREAUXDCMP', 'ADMINARMP'),
(1145, 1, 'ADD_TYPEUNITEORGDCMP', 'ADMINARMP'),
(1146, 1, 'MOD_TYPEUNITEORGDCMP', 'ADMINARMP'),
(1147, 1, 'SUPP_TYPEUNITEORGDCMP', 'ADMINARMP'),
(1148, 1, 'ADD_MEMBRESGROUPE', 'ADMINARMP'),
(1149, 1, 'ADD_GROUPE', 'ADMINDCMP'),
(1150, 1, 'MOD_GROUPE', 'ADMINDCMP'),
(1151, 1, 'SUP_GROUPE', 'ADMINDCMP'),
(1152, 0, 'MODEPASSATION', 'ADMINDCMP'),
(1153, 1, 'ADD_COURRIERDEPART', 'ADMINDCMP'),
(1154, 1, 'MOD_COURRIERDEPART', 'ADMINDCMP'),
(1155, 1, 'SUP_COURRIERDEPART', 'ADMINDCMP'),
(1156, 1, 'WSUIVI_COURRIERDEPART', 'ADMINDCMP'),
(1157, 1, 'CONFDATESREAL', 'ADMINDCMP');

-- --------------------------------------------------------

--
-- Structure de la table `sys_profilarrowperm`
--

CREATE TABLE IF NOT EXISTS `sys_profilarrowperm` (
  `PAM_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ALLOW` smallint(6) NOT NULL,
  `APM_ID` bigint(20) NOT NULL,
  `PF_CODE` varchar(32) NOT NULL,
  PRIMARY KEY (`PAM_ID`),
  UNIQUE KEY `PAM_ID` (`PAM_ID`),
  KEY `FK81F69A4B5C186C28` (`PF_CODE`),
  KEY `FK81F69A4B5A99B138` (`APM_ID`),
  KEY `FK81F69A4BF1B3D549` (`PF_CODE`),
  KEY `FK81F69A4BDC4370D9` (`APM_ID`),
  KEY `FK81F69A4BC97CE164` (`PF_CODE`),
  KEY `FK81F69A4B9AAC2C74` (`APM_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Contenu de la table `sys_profilarrowperm`
--

INSERT INTO `sys_profilarrowperm` (`PAM_ID`, `ALLOW`, `APM_ID`, `PF_CODE`) VALUES
(1, 1, 2, 'ADMINARMP'),
(2, 1, 3, 'ADMINARMP'),
(3, 1, 4, 'ADMINARMP'),
(4, 1, 5, 'ADMINARMP'),
(5, 1, 6, 'ADMINARMP'),
(6, 1, 2, 'ADMIN'),
(7, 1, 3, 'ADMIN'),
(8, 1, 4, 'ADMIN'),
(9, 1, 5, 'ADMIN'),
(10, 1, 6, 'ADMIN'),
(11, 1, 2, 'AC'),
(12, 1, 3, 'AC'),
(13, 1, 4, 'AC'),
(14, 1, 5, 'AC'),
(15, 1, 6, 'AC'),
(16, 1, 2, 'ADMINDCMP'),
(17, 1, 3, 'ADMINDCMP'),
(18, 1, 4, 'ADMINDCMP'),
(19, 1, 5, 'ADMINDCMP'),
(20, 1, 6, 'ADMINDCMP'),
(21, 1, 2, 'AGENTAC'),
(22, 1, 3, 'AGENTAC'),
(23, 1, 4, 'AGENTAC'),
(24, 1, 5, 'AGENTAC'),
(25, 1, 6, 'AGENTAC'),
(26, 1, 2, 'AGENTDCMP'),
(27, 1, 3, 'AGENTDCMP'),
(28, 1, 4, 'AGENTDCMP'),
(29, 1, 5, 'AGENTDCMP'),
(30, 1, 6, 'AGENTDCMP'),
(31, 1, 2, 'SME'),
(32, 1, 3, 'SME'),
(33, 1, 4, 'SME'),
(34, 1, 5, 'SME'),
(35, 1, 6, 'SME'),
(36, 1, 8, 'ADMINARMP'),
(37, 1, 9, 'ADMINARMP'),
(38, 1, 10, 'ADMINARMP'),
(43, 1, 17, 'ADMINARMP'),
(44, 1, 18, 'ADMINARMP'),
(45, 1, 19, 'ADMINARMP'),
(46, 1, 20, 'ADMINARMP'),
(47, 1, 17, 'ADMINDCMP'),
(48, 0, 18, 'ADMINDCMP'),
(49, 0, 19, 'ADMINDCMP'),
(50, 0, 20, 'ADMINDCMP');

-- --------------------------------------------------------

--
-- Structure de la table `sys_role`
--

CREATE TABLE IF NOT EXISTS `sys_role` (
  `ROL_CODE` varchar(200) NOT NULL,
  `ROL_DESC` longtext,
  `ROL_SVG` longtext,
  PRIMARY KEY (`ROL_CODE`),
  UNIQUE KEY `ROL_CODE` (`ROL_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `sys_role`
--

INSERT INTO `sys_role` (`ROL_CODE`, `ROL_DESC`, `ROL_SVG`) VALUES
('AUDITS', 'Audits', '<svg height="550" version="1.1" viewBox="0 0 231 550" width="231" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\n<rect fill="#E8EEF7" height="19" stroke="#000000" stroke-width="1.0" width="230" x="0" y="0"/>\n<text fill="black" font-decoration="none" font-family="Arial,Helvetica" font-size="11" font-weight="normal" text-anchor="middle">\n<tspan x="115" y="13">\n\nInitialiser Audit</tspan></text><rect fill="#E8EEF7" height="19" stroke="#000000" stroke-width="1.0" width="230" x="0" y="100"/>\n<text fill="black" font-decoration="none" font-family="Arial,Helvetica" font-size="11" font-weight="normal" text-anchor="middle">\n<tspan x="115" y="113">\n\nSaisir Prestataires</tspan></text><rect fill="#E8EEF7" height="19" stroke="#000000" stroke-width="1.0" width="230" x="0" y="200"/>\n<text fill="black" font-decoration="none" font-family="Arial,Helvetica" font-size="11" font-weight="normal" text-anchor="middle">\n<tspan x="115" y="213">\n\nEnregistrer Rapport Provisoire</tspan></text><rect fill="#E8EEF7" height="19" stroke="#000000" stroke-width="1.0" width="230" x="0" y="310"/>\n<text fill="black" font-decoration="none" font-family="Arial,Helvetica" font-size="11" font-weight="normal" text-anchor="middle">\n<tspan x="115" y="323">\n\nTransmettre Rapport Provisoire aux AC</tspan></text><rect fill="#E8EEF7" height="19" stroke="#000000" stroke-width="1.0" width="230" x="0" y="420"/>\n<text fill="black" font-decoration="none" font-family="Arial,Helvetica" font-size="11" font-weight="normal" text-anchor="middle">\n<tspan x="115" y="433">\n\nEnregistrer Observations AC</tspan></text><rect fill="#E8EEF7" height="19" stroke="#000000" stroke-width="1.0" width="230" x="0" y="530"/>\n<text fill="black" font-decoration="none" font-family="Arial,Helvetica" font-size="11" font-weight="normal" text-anchor="middle">\n<tspan x="115" y="543">\n\nEnregistrer Rapport Final</tspan></text><g>\n<path d="M 115.0 94.5 L 112.0 88.5 L 115.0 90.0 L 118.0 88.5 z" fill="black" stroke="black" stroke-width="1.0"/>\n<path d="M 115.0 19.5 L 115.0 59.5 L 115.0 95.0" fill="none" stroke="black" stroke-width="1.0"/>\n</g><g>\n<path d="M 115.0 194.5 L 112.0 188.5 L 115.0 190.0 L 118.0 188.5 z" fill="black" stroke="black" stroke-width="1.0"/>\n<path d="M 115.0 119.5 L 115.0 159.5 L 115.0 195.0" fill="none" stroke="black" stroke-width="1.0"/>\n</g><g>\n<path d="M 115.0 304.5 L 112.0 298.5 L 115.0 300.0 L 118.0 298.5 z" fill="black" stroke="black" stroke-width="1.0"/>\n<path d="M 115.0 219.5 L 115.0 264.5 L 115.0 305.0" fill="none" stroke="black" stroke-width="1.0"/>\n</g><g>\n<path d="M 115.0 414.5 L 112.0 408.5 L 115.0 410.0 L 118.0 408.5 z" fill="black" stroke="black" stroke-width="1.0"/>\n<path d="M 115.0 329.5 L 115.0 374.5 L 115.0 415.0" fill="none" stroke="black" stroke-width="1.0"/>\n</g><g>\n<path d="M 115.0 524.5 L 112.0 518.5 L 115.0 520.0 L 118.0 518.5 z" fill="black" stroke="black" stroke-width="1.0"/>\n<path d="M 115.0 439.5 L 115.0 484.5 L 115.0 525.0" fill="none" stroke="black" stroke-width="1.0"/>\n</g></svg>'),
('SUIVICONTENTIEUX', 'Suivi Contentieux', '<svg height="260" version="1.1" viewBox="0 0 531 260" width="531" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\n<rect fill="#E8EEF7" height="19" stroke="#000000" stroke-width="1.0" width="230" x="150" y="0"/>\n<text fill="black" font-decoration="none" font-family="Arial,Helvetica" font-size="11" font-weight="normal" text-anchor="middle">\n<tspan x="265" y="13">\n\nSaisir Contentieux</tspan></text><rect fill="#E8EEF7" height="19" stroke="#000000" stroke-width="1.0" width="230" x="0" y="130"/>\n<text fill="black" font-decoration="none" font-family="Arial,Helvetica" font-size="11" font-weight="normal" text-anchor="middle">\n<tspan x="115" y="143">\n\nValider Contentieux</tspan></text><rect fill="#E8EEF7" height="19" stroke="#000000" stroke-width="1.0" width="230" x="300" y="130"/>\n<text fill="black" font-decoration="none" font-family="Arial,Helvetica" font-size="11" font-weight="normal" text-anchor="middle">\n<tspan x="415" y="143">\n\nRejeter Contentieux</tspan></text><rect fill="#E8EEF7" height="19" stroke="#000000" stroke-width="1.0" width="230" x="20" y="240"/>\n<text fill="black" font-decoration="none" font-family="Arial,Helvetica" font-size="11" font-weight="normal" text-anchor="middle">\n<tspan x="135" y="253">\n\nEnregistrer Decision Contentieux</tspan></text><g>\n<path d="M 190.0 124.5 L 187.0 118.5 L 190.0 120.0 L 193.0 118.5 z" fill="black" stroke="black" stroke-width="1.0"/>\n<path d="M 190.0 19.5 L 190.0 74.5 L 190.0 125.0" fill="none" stroke="black" stroke-width="1.0"/>\n</g><g>\n<path d="M 340.0 124.5 L 337.0 118.5 L 340.0 120.0 L 343.0 118.5 z" fill="black" stroke="black" stroke-width="1.0"/>\n<path d="M 340.0 19.5 L 340.0 74.5 L 340.0 125.0" fill="none" stroke="black" stroke-width="1.0"/>\n</g><g>\n<path d="M 125.0 234.5 L 122.0 228.5 L 125.0 230.0 L 128.0 228.5 z" fill="black" stroke="black" stroke-width="1.0"/>\n<path d="M 125.0 149.5 L 125.0 194.5 L 125.0 235.0" fill="none" stroke="black" stroke-width="1.0"/>\n</g></svg>'),
('TDOSSIER', 'Traitement dossier', '<svg height="500" version="1.1" viewBox="0 0 241 500" width="241" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\n<rect fill="#E8EEF7" height="19" stroke="#000000" stroke-width="1.0" width="230" x="10" y="0"/>\n<text fill="black" font-decoration="none" font-family="Arial,Helvetica" font-size="11" font-weight="normal" text-anchor="middle">\n<tspan x="125" y="13">\n\nSaisir Dossier</tspan></text><rect fill="#E8EEF7" height="19" stroke="#000000" stroke-width="1.0" width="230" x="10" y="130"/>\n<text fill="black" font-decoration="none" font-family="Arial,Helvetica" font-size="11" font-weight="normal" text-anchor="middle">\n<tspan x="125" y="143">\n\nTransmettre Dossier pour traitement</tspan></text><rect fill="#E8EEF7" height="19" stroke="#000000" stroke-width="1.0" width="230" x="0" y="260"/>\n<text fill="black" font-decoration="none" font-family="Arial,Helvetica" font-size="11" font-weight="normal" text-anchor="middle">\n<tspan x="115" y="273">\n\nEnvoyer Dossier apres signature</tspan></text><rect fill="#E8EEF7" height="19" stroke="#000000" stroke-width="1.0" width="230" x="0" y="370"/>\n<text fill="black" font-decoration="none" font-family="Arial,Helvetica" font-size="11" font-weight="normal" text-anchor="middle">\n<tspan x="115" y="383">\n\nSigner signature</tspan></text><rect fill="#E8EEF7" height="19" stroke="#000000" stroke-width="1.0" width="230" x="10" y="480"/>\n<text fill="black" font-decoration="none" font-family="Arial,Helvetica" font-size="11" font-weight="normal" text-anchor="middle">\n<tspan x="125" y="493">\n\nEnvoyer Dossier</tspan></text><g>\n<path d="M 125.0 124.5 L 122.0 118.5 L 125.0 120.0 L 128.0 118.5 z" fill="black" stroke="black" stroke-width="1.0"/>\n<path d="M 125.0 19.5 L 125.0 74.5 L 125.0 125.0" fill="none" stroke="black" stroke-width="1.0"/>\n</g><g>\n<path d="M 120.0 254.5 L 117.0 248.5 L 120.0 250.0 L 123.0 248.5 z" fill="black" stroke="black" stroke-width="1.0"/>\n<path d="M 120.0 149.5 L 120.0 204.5 L 120.0 255.0" fill="none" stroke="black" stroke-width="1.0"/>\n</g><g>\n<path d="M 115.0 364.5 L 112.0 358.5 L 115.0 360.0 L 118.0 358.5 z" fill="black" stroke="black" stroke-width="1.0"/>\n<path d="M 115.0 279.5 L 115.0 324.5 L 115.0 365.0" fill="none" stroke="black" stroke-width="1.0"/>\n</g><g>\n<path d="M 120.0 474.5 L 117.0 468.5 L 120.0 470.0 L 123.0 468.5 z" fill="black" stroke="black" stroke-width="1.0"/>\n<path d="M 120.0 389.5 L 120.0 434.5 L 120.0 475.0" fill="none" stroke="black" stroke-width="1.0"/>\n</g></svg>');

-- --------------------------------------------------------

--
-- Structure de la table `sys_state`
--

CREATE TABLE IF NOT EXISTS `sys_state` (
  `STA_CODE` varchar(200) NOT NULL,
  `STA_DESC` longtext,
  `STA_INIT` smallint(6) DEFAULT NULL,
  `STA_LIBELLE` varchar(200) DEFAULT NULL,
  `OBJ_CODE` varchar(200) NOT NULL,
  PRIMARY KEY (`STA_CODE`),
  UNIQUE KEY `STA_CODE` (`STA_CODE`),
  KEY `FK203FD89F71298671` (`OBJ_CODE`),
  KEY `FK203FD89F96CBD270` (`OBJ_CODE`),
  KEY `FK203FD89FA62F34B5` (`OBJ_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `sys_state`
--

INSERT INTO `sys_state` (`STA_CODE`, `STA_DESC`, `STA_INIT`, `STA_LIBELLE`, `OBJ_CODE`) VALUES
('CLOTDOSSIER_STATE', 'Dossier envoy�', NULL, 'Dossier envoy�', 'TDOSSIER'),
('ENREGDECCONT_STATE', 'Enregistrer Décision Contentieux', NULL, 'Enregistrer Décision Contentieux', 'SUIVICONTENTIEUX'),
('ENROBSERVAC_STATE', 'Observations AC enregistré', 0, 'Observations AC enregistré', 'AUDITS'),
('ENRRAPPORTFINAL_STATE', 'Rapport final enregistré', 0, 'Rapport final enregistré', 'AUDITS'),
('ENRRAPPORTPROV_STATE', 'Rapport provisoire enregistré', 0, 'Rapport provisoire enregistré', 'AUDITS'),
('INIAUDIT_STATE', 'Audit initialisé', 0, 'Audit initialisé', 'AUDITS'),
('INIT_STATE', 'Init', 0, 'Init', 'AUDITS'),
('REJDOSSIER_STATE', 'Dossier encours de signature', NULL, 'Dossier encours de signature', 'TDOSSIER'),
('REJETERCONT_STATE', 'Contentieux Rejeté', NULL, 'Contentieux Rejeté', 'SUIVICONTENTIEUX'),
('SAISIDOSSIER_STATE', 'Dossier saisi', NULL, 'Dossier saisi', 'TDOSSIER'),
('SAISIECONT_STATE', 'Saisir Contentieux ', NULL, 'Saisir Contentieux ', 'SUIVICONTENTIEUX'),
('SAISISPRESTATAIRES_STATE', 'Prestataires saisis', 0, 'Prestataires saisis', 'AUDITS'),
('TRANSMDOSSIER_STATE', 'Dossier encours de traitement', NULL, 'Dossier encours de traitement', 'TDOSSIER'),
('TRANSRAPPORTPROVAC_STATE', 'Rapport provisoire transmis aux AC', 0, 'Rapport provisoire transmis aux AC', 'AUDITS'),
('VALIDERCONT_STATE', 'Valider Contentieux', NULL, 'Valider Contentieux', 'SUIVICONTENTIEUX'),
('VALIDOSSIER_STATE', 'Dossier sign�', NULL, 'Dossier sign�', 'TDOSSIER');

-- --------------------------------------------------------

--
-- Structure de la table `sys_stateperm`
--

CREATE TABLE IF NOT EXISTS `sys_stateperm` (
  `SPM_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `STA_CANADD` smallint(6) NOT NULL,
  `STA_CANDELETE` smallint(6) DEFAULT NULL,
  `STA_CANEDIT` smallint(6) NOT NULL,
  `STA_CANPRINT` smallint(6) NOT NULL,
  `STA_CANVIEW` smallint(6) NOT NULL,
  `PF_CODE` varchar(32) DEFAULT NULL,
  `ROL_CODE` varchar(200) DEFAULT NULL,
  `STA_CODE` varchar(200) NOT NULL,
  PRIMARY KEY (`SPM_ID`),
  UNIQUE KEY `SPM_ID` (`SPM_ID`),
  KEY `FKD588E40F5C186C28` (`PF_CODE`),
  KEY `FKD588E40F27DC46A0` (`STA_CODE`),
  KEY `FKD588E40F20243910` (`ROL_CODE`),
  KEY `FKD588E40FF1B3D549` (`PF_CODE`),
  KEY `FKD588E40F20D0FEC1` (`STA_CODE`),
  KEY `FKD588E40FD59778CF` (`ROL_CODE`),
  KEY `FKD588E40FC97CE164` (`PF_CODE`),
  KEY `FKD588E40FF805BFDC` (`STA_CODE`),
  KEY `FKD588E40FDC88A854` (`ROL_CODE`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62 ;

--
-- Contenu de la table `sys_stateperm`
--

INSERT INTO `sys_stateperm` (`SPM_ID`, `STA_CANADD`, `STA_CANDELETE`, `STA_CANEDIT`, `STA_CANPRINT`, `STA_CANVIEW`, `PF_CODE`, `ROL_CODE`, `STA_CODE`) VALUES
(1, 0, 0, 0, 0, 1, 'ADMINARMP', 'AUDITS', 'INIAUDIT_STATE'),
(2, 0, 0, 0, 0, 1, 'ADMINARMP', 'AUDITS', 'SAISISPRESTATAIRES_STATE'),
(3, 0, 0, 0, 0, 1, 'ADMINARMP', 'AUDITS', 'ENRRAPPORTPROV_STATE'),
(4, 0, 0, 0, 0, 1, 'ADMINARMP', 'AUDITS', 'TRANSRAPPORTPROVAC_STATE'),
(5, 0, 0, 0, 0, 1, 'ADMINARMP', 'AUDITS', 'ENROBSERVAC_STATE'),
(6, 0, 0, 0, 1, 1, 'ADMINARMP', 'AUDITS', 'ENRRAPPORTFINAL_STATE'),
(7, 0, 0, 0, 0, 1, 'ADMIN', 'AUDITS', 'INIAUDIT_STATE'),
(8, 0, 0, 0, 0, 1, 'ADMIN', 'AUDITS', 'SAISISPRESTATAIRES_STATE'),
(9, 0, 0, 0, 0, 1, 'ADMIN', 'AUDITS', 'ENRRAPPORTPROV_STATE'),
(10, 0, 0, 0, 0, 1, 'ADMIN', 'AUDITS', 'TRANSRAPPORTPROVAC_STATE'),
(11, 0, 0, 0, 0, 1, 'ADMIN', 'AUDITS', 'ENROBSERVAC_STATE'),
(12, 0, 0, 0, 1, 1, 'ADMIN', 'AUDITS', 'ENRRAPPORTFINAL_STATE'),
(13, 0, 0, 0, 0, 1, 'AC', 'AUDITS', 'INIAUDIT_STATE'),
(14, 0, 0, 0, 0, 1, 'AC', 'AUDITS', 'SAISISPRESTATAIRES_STATE'),
(15, 0, 0, 0, 0, 1, 'AC', 'AUDITS', 'ENRRAPPORTPROV_STATE'),
(16, 0, 0, 0, 0, 1, 'AC', 'AUDITS', 'TRANSRAPPORTPROVAC_STATE'),
(17, 0, 0, 0, 0, 1, 'AC', 'AUDITS', 'ENROBSERVAC_STATE'),
(18, 0, 0, 0, 1, 1, 'AC', 'AUDITS', 'ENRRAPPORTFINAL_STATE'),
(19, 0, 0, 0, 0, 1, 'ADMINDCMP', 'AUDITS', 'INIAUDIT_STATE'),
(20, 0, 0, 0, 0, 1, 'ADMINDCMP', 'AUDITS', 'SAISISPRESTATAIRES_STATE'),
(21, 0, 0, 0, 0, 1, 'ADMINDCMP', 'AUDITS', 'ENRRAPPORTPROV_STATE'),
(22, 0, 0, 0, 0, 1, 'ADMINDCMP', 'AUDITS', 'TRANSRAPPORTPROVAC_STATE'),
(23, 0, 0, 0, 0, 1, 'ADMINDCMP', 'AUDITS', 'ENROBSERVAC_STATE'),
(24, 0, 0, 0, 1, 1, 'ADMINDCMP', 'AUDITS', 'ENRRAPPORTFINAL_STATE'),
(25, 0, 0, 0, 0, 1, 'AGENTAC', 'AUDITS', 'INIAUDIT_STATE'),
(26, 0, 0, 0, 0, 1, 'AGENTAC', 'AUDITS', 'SAISISPRESTATAIRES_STATE'),
(27, 0, 0, 0, 0, 1, 'AGENTAC', 'AUDITS', 'ENRRAPPORTPROV_STATE'),
(28, 0, 0, 0, 0, 1, 'AGENTAC', 'AUDITS', 'TRANSRAPPORTPROVAC_STATE'),
(29, 0, 0, 0, 0, 1, 'AGENTAC', 'AUDITS', 'ENROBSERVAC_STATE'),
(30, 0, 0, 0, 1, 1, 'AGENTAC', 'AUDITS', 'ENRRAPPORTFINAL_STATE'),
(31, 0, 0, 0, 0, 1, 'AGENTDCMP', 'AUDITS', 'INIAUDIT_STATE'),
(32, 0, 0, 0, 0, 1, 'AGENTDCMP', 'AUDITS', 'SAISISPRESTATAIRES_STATE'),
(33, 0, 0, 0, 0, 1, 'AGENTDCMP', 'AUDITS', 'ENRRAPPORTPROV_STATE'),
(34, 0, 0, 0, 0, 1, 'AGENTDCMP', 'AUDITS', 'TRANSRAPPORTPROVAC_STATE'),
(35, 0, 0, 0, 0, 1, 'AGENTDCMP', 'AUDITS', 'ENROBSERVAC_STATE'),
(36, 0, 0, 0, 1, 1, 'AGENTDCMP', 'AUDITS', 'ENRRAPPORTFINAL_STATE'),
(37, 0, 0, 0, 0, 1, 'SME', 'AUDITS', 'INIAUDIT_STATE'),
(38, 0, 0, 0, 0, 1, 'SME', 'AUDITS', 'SAISISPRESTATAIRES_STATE'),
(39, 0, 0, 0, 0, 1, 'SME', 'AUDITS', 'ENRRAPPORTPROV_STATE'),
(40, 0, 0, 0, 0, 1, 'SME', 'AUDITS', 'TRANSRAPPORTPROVAC_STATE'),
(41, 0, 0, 0, 0, 1, 'SME', 'AUDITS', 'ENROBSERVAC_STATE'),
(42, 0, 0, 0, 1, 1, 'SME', 'AUDITS', 'ENRRAPPORTFINAL_STATE'),
(43, 0, 0, 0, 0, 1, 'ADMINARMP', 'SUIVICONTENTIEUX', 'SAISIECONT_STATE'),
(44, 0, 0, 0, 0, 1, 'ADMINARMP', 'SUIVICONTENTIEUX', 'VALIDERCONT_STATE'),
(45, 0, 0, 0, 0, 1, 'ADMINARMP', 'SUIVICONTENTIEUX', 'REJETERCONT_STATE'),
(46, 0, 0, 0, 0, 1, 'ADMINARMP', 'SUIVICONTENTIEUX', 'ENREGDECCONT_STATE'),
(52, 0, 0, 0, 0, 1, 'ADMINARMP', 'TDOSSIER', 'SAISIDOSSIER_STATE'),
(53, 0, 0, 0, 0, 1, 'ADMINARMP', 'TDOSSIER', 'TRANSMDOSSIER_STATE'),
(54, 0, 0, 0, 0, 1, 'ADMINARMP', 'TDOSSIER', 'VALIDOSSIER_STATE'),
(55, 0, 0, 0, 0, 1, 'ADMINARMP', 'TDOSSIER', 'REJDOSSIER_STATE'),
(56, 0, 0, 0, 1, 1, 'ADMINARMP', 'TDOSSIER', 'CLOTDOSSIER_STATE'),
(57, 0, 0, 0, 0, 1, 'ADMINDCMP', 'TDOSSIER', 'SAISIDOSSIER_STATE'),
(58, 0, 0, 0, 0, 0, 'ADMINDCMP', 'TDOSSIER', 'TRANSMDOSSIER_STATE'),
(59, 0, 0, 0, 0, 0, 'ADMINDCMP', 'TDOSSIER', 'VALIDOSSIER_STATE'),
(60, 0, 0, 0, 0, 0, 'ADMINDCMP', 'TDOSSIER', 'REJDOSSIER_STATE'),
(61, 0, 0, 0, 0, 0, 'ADMINDCMP', 'TDOSSIER', 'CLOTDOSSIER_STATE');

-- --------------------------------------------------------

--
-- Structure de la table `sys_utilisateuralerte`
--

CREATE TABLE IF NOT EXISTS `sys_utilisateuralerte` (
  `USA_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ALT_ID` bigint(20) NOT NULL,
  `USR_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`USA_ID`),
  UNIQUE KEY `USA_ID` (`USA_ID`),
  KEY `FK49BD56DA14540F12` (`USR_ID`),
  KEY `FK49BD56DA15A35950` (`ALT_ID`),
  KEY `FK49BD56DAB0B9B3F3` (`USR_ID`),
  KEY `FK49BD56DAAB3EC271` (`ALT_ID`),
  KEY `FK49BD56DABA70254E` (`USR_ID`),
  KEY `FK49BD56DA8307CE8C` (`ALT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contenu de la table `sys_utilisateuralerte`
--


-- --------------------------------------------------------

--
-- Structure de la table `sys_utilisateurprofil`
--

CREATE TABLE IF NOT EXISTS `sys_utilisateurprofil` (
  `PF_CODE` varchar(32) NOT NULL,
  `USR_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`PF_CODE`,`USR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `sys_utilisateurprofil`
--


-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `USR_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USR_LOGIN` varchar(200) DEFAULT NULL,
  `USR_MAIL` varchar(200) DEFAULT NULL,
  `USR_NOM` varchar(200) DEFAULT NULL,
  `USR_PASSWORD` varchar(200) DEFAULT NULL,
  `USR_PRENOM` varchar(200) DEFAULT NULL,
  `PF_CODE` varchar(32) DEFAULT NULL,
  `USR_ADRESSE` varchar(255) DEFAULT NULL,
  `USR_FAX` varchar(255) DEFAULT NULL,
  `USR_FONCTION` varchar(255) DEFAULT NULL,
  `USR_SERVICE` varchar(255) DEFAULT NULL,
  `USR_TEL` varchar(255) DEFAULT NULL,
  `USR_TYPE` varchar(255) DEFAULT NULL,
  `BR_ID` bigint(20) DEFAULT NULL,
  `USR_ACTIF` bit(1) DEFAULT NULL,
  `AUTORITE` bigint(20) DEFAULT NULL,
  `SERVICE` bigint(20) DEFAULT NULL,
  `USR_CHEF` bit(1) DEFAULT NULL,
  PRIMARY KEY (`USR_ID`),
  UNIQUE KEY `USR_ID` (`USR_ID`),
  KEY `FKDD1633835C186C28` (`PF_CODE`),
  KEY `FKDD163383F1B3D549` (`PF_CODE`),
  KEY `FKDD163383C97CE164` (`PF_CODE`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`USR_ID`, `USR_LOGIN`, `USR_MAIL`, `USR_NOM`, `USR_PASSWORD`, `USR_PRENOM`, `PF_CODE`, `USR_ADRESSE`, `USR_FAX`, `USR_FONCTION`, `USR_SERVICE`, `USR_TEL`, `USR_TYPE`, `BR_ID`, `USR_ACTIF`, `AUTORITE`, `SERVICE`, `USR_CHEF`) VALUES
(1, 'admin', 'abciss@gmail.com', 'admin', 'admin', 'Administrateur', 'ADMIN', '', '', '', '', '', 'ADMIN', NULL, b'1', NULL, NULL, NULL),
(2, 'armp', 'passer', 'Diop', 'passer', 'Modou', 'ADMINARMP', NULL, NULL, NULL, NULL, NULL, 'ARMP', NULL, b'1', NULL, NULL, NULL),
(4, 'dcmp', 'baba.diop@yahoo.fr', 'Diop', 'passer', 'Baba', 'ADMINDCMP', NULL, NULL, NULL, NULL, NULL, 'DCMP', 2, b'1', NULL, NULL, b'1'),
(17, 'idiop', 'asamb@ssi.sn', 'Diop', 'passer', 'Ibrahima', 'AC', 'Dakar', '3677', NULL, NULL, '89765', 'AC', NULL, b'1', 6, NULL, NULL),
(18, 'asamb', 'adama@yahoo.fr', 'Samb', 'passer', 'Adama', 'AC', '', '', NULL, NULL, '', 'AC', NULL, b'1', 902, NULL, NULL),
(19, 'mef', 'asamb@ssi.sn', 'Kogol�', 'passer', 'Jean Baptise', 'AC', '', '', NULL, NULL, '', 'AC', NULL, b'1', 9, NULL, NULL),
(20, 'moussa', 'asamb@ssi.sn', 'Fall', 'passer', 'Moussa', 'AC', 'Lom�', '', NULL, NULL, '67890', 'AC', NULL, b'1', 940, NULL, NULL),
(21, 'marc', 'asamb@ssi.sn', 'Sondou', 'passer', 'Marc', 'AC', '', '', NULL, NULL, '', 'AC', NULL, b'1', 905, NULL, NULL);

--
-- Contraintes pour les tables export�es
--

--
-- Contraintes pour la table `ea_participants`
--
ALTER TABLE `ea_participants`
  ADD CONSTRAINT `FK1539B26356BD84F9` FOREIGN KEY (`groupe_id`) REFERENCES `syg_groupedeformation` (`ID`),
  ADD CONSTRAINT `FK1539B2638AB88E5B` FOREIGN KEY (`formation`) REFERENCES `syg_proformation` (`ID`);

--
-- Contraintes pour la table `eco_groupes`
--
ALTER TABLE `eco_groupes`
  ADD CONSTRAINT `FK1E5A6CBF984D7F38` FOREIGN KEY (`GroupesImputation`) REFERENCES `eco_groupesimputation` (`ID`);

--
-- Contraintes pour la table `eco_groupesimputilisateur`
--
ALTER TABLE `eco_groupesimputilisateur`
  ADD CONSTRAINT `FKF1B6B6984D7F38` FOREIGN KEY (`GroupesImputation`) REFERENCES `eco_groupesimputation` (`ID`),
  ADD CONSTRAINT `FKF1B6B6C955852B` FOREIGN KEY (`Utilisateur`) REFERENCES `utilisateur` (`USR_ID`);

--
-- Contraintes pour la table `pmb_annuaire`
--
ALTER TABLE `pmb_annuaire`
  ADD CONSTRAINT `FK262DEE097B75F0D4` FOREIGN KEY (`Service_ID`) REFERENCES `pmb_service` (`ID`);

--
-- Contraintes pour la table `pmb_attributions`
--
ALTER TABLE `pmb_attributions`
  ADD CONSTRAINT `FKAF2E67CE21E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`),
  ADD CONSTRAINT `FKAF2E67CE57A27964` FOREIGN KEY (`Pliouverture_ID`) REFERENCES `pmb_pliouverture` (`IDpliouverture`),
  ADD CONSTRAINT `FKAF2E67CEB5246B11` FOREIGN KEY (`Lot_ID`) REFERENCES `pmb_lots` (`lotID`);

--
-- Contraintes pour la table `pmb_audit`
--
ALTER TABLE `pmb_audit`
  ADD CONSTRAINT `FK9D5921F805BFDC` FOREIGN KEY (`STA_CODE`) REFERENCES `sys_state` (`STA_CODE`);

--
-- Contraintes pour la table `pmb_auditpiecejointe`
--
ALTER TABLE `pmb_auditpiecejointe`
  ADD CONSTRAINT `FK94B205E8DF43B570` FOREIGN KEY (`audit`) REFERENCES `pmb_audit` (`idaudit`);

--
-- Contraintes pour la table `pmb_auditsprestac`
--
ALTER TABLE `pmb_auditsprestac`
  ADD CONSTRAINT `FK92357FB4A2096A91` FOREIGN KEY (`prestataire`) REFERENCES `pmb_prestatairesaudits` (`idpaac`),
  ADD CONSTRAINT `FK92357FB4DF43B570` FOREIGN KEY (`audit`) REFERENCES `pmb_audit` (`idaudit`);

--
-- Contraintes pour la table `pmb_contractsprestataires`
--
ALTER TABLE `pmb_contractsprestataires`
  ADD CONSTRAINT `FK6531D7C83A25A6E3` FOREIGN KEY (`idprestataire`) REFERENCES `pmb_prestataire` (`idprestataire`),
  ADD CONSTRAINT `FK6531D7C83C56A135` FOREIGN KEY (`idaudit`) REFERENCES `pmb_audit` (`idaudit`);

--
-- Contraintes pour la table `pmb_contrats`
--
ALTER TABLE `pmb_contrats`
  ADD CONSTRAINT `FK2AD0081A21E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`),
  ADD CONSTRAINT `FK2AD0081A2B7B217B` FOREIGN KEY (`Pli_ID`) REFERENCES `pmb_pliouverture` (`IDpliouverture`),
  ADD CONSTRAINT `FK2AD0081AF6884D99` FOREIGN KEY (`LOT`) REFERENCES `pmb_lots` (`lotID`);

--
-- Contraintes pour la table `pmb_courriercontentieux`
--
ALTER TABLE `pmb_courriercontentieux`
  ADD CONSTRAINT `FKB04DF4B1EA7AB2C5` FOREIGN KEY (`contentieuxID`) REFERENCES `pmb_decisionslocaux` (`dlocId`);

--
-- Contraintes pour la table `pmb_courriers_ac`
--
ALTER TABLE `pmb_courriers_ac`
  ADD CONSTRAINT `FKC8346D9512168014` FOREIGN KEY (`dossierCourrier_id`) REFERENCES `sygdossiercourrier` (`id`),
  ADD CONSTRAINT `FKC8346D9539D8E5E7` FOREIGN KEY (`typecourrier`) REFERENCES `pmb_typecourrier` (`idtypecourrier`),
  ADD CONSTRAINT `FKC8346D95578FE0B4` FOREIGN KEY (`natureCourrier_idnaturecourrier`) REFERENCES `pmb_naturecourrier` (`idnaturecourrier`),
  ADD CONSTRAINT `FKC8346D95F7EC5CB2` FOREIGN KEY (`modereception`) REFERENCES `pmb_modereception` (`idmodereception`),
  ADD CONSTRAINT `FKC8346D95FF9E8AB8` FOREIGN KEY (`modetraitement_idmodetraitement`) REFERENCES `pmb_modetraitement` (`idmodetraitement`);

--
-- Contraintes pour la table `pmb_criteresoumissions`
--
ALTER TABLE `pmb_criteresoumissions`
  ADD CONSTRAINT `FK7FC6D7C21E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`),
  ADD CONSTRAINT `FK7FC6D7C2B7B217B` FOREIGN KEY (`Pli_ID`) REFERENCES `pmb_pliouverture` (`IDpliouverture`),
  ADD CONSTRAINT `FK7FC6D7C4E4BAA3B` FOREIGN KEY (`CRITERE`) REFERENCES `pmb_dossiersouscritere` (`ID`);

--
-- Contraintes pour la table `pmb_decisioncontentieux`
--
ALTER TABLE `pmb_decisioncontentieux`
  ADD CONSTRAINT `FKC2662E2211127C33` FOREIGN KEY (`decisionID`) REFERENCES `pmb_typedecision` (`id`),
  ADD CONSTRAINT `FKC2662E229C2E6DA` FOREIGN KEY (`typemarche`) REFERENCES `syg_typesmarches` (`ID`),
  ADD CONSTRAINT `FKC2662E22BB47723C` FOREIGN KEY (`modepassation`) REFERENCES `syg_modepassation` (`ID`),
  ADD CONSTRAINT `FKC2662E22EA7AB2C5` FOREIGN KEY (`contentieuxID`) REFERENCES `pmb_decisionslocaux` (`dlocId`);

--
-- Contraintes pour la table `pmb_denonciation`
--
ALTER TABLE `pmb_denonciation`
  ADD CONSTRAINT `FKBD220A1DF92DAD71` FOREIGN KEY (`statCODE`) REFERENCES `sys_state` (`STA_CODE`);

--
-- Contraintes pour la table `pmb_devise`
--
ALTER TABLE `pmb_devise`
  ADD CONSTRAINT `FK17530BC021E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`);

--
-- Contraintes pour la table `pmb_documents`
--
ALTER TABLE `pmb_documents`
  ADD CONSTRAINT `FK7F00FCFE21E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`),
  ADD CONSTRAINT `FK7F00FCFE25D5960` FOREIGN KEY (`Appelsoffres_ID`) REFERENCES `pmb_appelsoffres` (`apoID`);

--
-- Contraintes pour la table `pmb_dossiercommissioncellules`
--
ALTER TABLE `pmb_dossiercommissioncellules`
  ADD CONSTRAINT `FKFA31C2321E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`),
  ADD CONSTRAINT `FKFA31C2366462C0B` FOREIGN KEY (`commissionmarche_ID`) REFERENCES `pmb_membrecellulepassation` (`celID`);

--
-- Contraintes pour la table `pmb_dossiercommissionmarche`
--
ALTER TABLE `pmb_dossiercommissionmarche`
  ADD CONSTRAINT `FKDD7517BE21E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`),
  ADD CONSTRAINT `FKDD7517BE260F6E03` FOREIGN KEY (`commissionmarche_ID`) REFERENCES `pmb_membrescommissionmarche` (`ID`);

--
-- Contraintes pour la table `pmb_dossierpiece`
--
ALTER TABLE `pmb_dossierpiece`
  ADD CONSTRAINT `FK17227AFD21E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`),
  ADD CONSTRAINT `FK17227AFD4CEAE4D7` FOREIGN KEY (`Piece_ID`) REFERENCES `pmb_piece` (`IDPIECE`);

--
-- Contraintes pour la table `pmb_dossiers`
--
ALTER TABLE `pmb_dossiers`
  ADD CONSTRAINT `FK9B27500225D5960` FOREIGN KEY (`Appelsoffres_ID`) REFERENCES `pmb_appelsoffres` (`apoID`),
  ADD CONSTRAINT `FK9B27500250D62C5D` FOREIGN KEY (`Plan_ID`) REFERENCES `pmb_realisationduplan` (`ID_plan`);

--
-- Contraintes pour la table `pmb_dossiersouscritere`
--
ALTER TABLE `pmb_dossiersouscritere`
  ADD CONSTRAINT `FK4EE1EED33F6CC61D` FOREIGN KEY (`Dossier_ID`) REFERENCES `pmb_dossiers` (`dosID`),
  ADD CONSTRAINT `FK4EE1EED3B74B3DAE` FOREIGN KEY (`Souscritere_ID`) REFERENCES `syg_critere` (`ID`);

--
-- Contraintes pour la table `pmb_dossiersouscritereevaluateur`
--
ALTER TABLE `pmb_dossiersouscritereevaluateur`
  ADD CONSTRAINT `FK737BBC0921E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`),
  ADD CONSTRAINT `FK737BBC092B7B217B` FOREIGN KEY (`Pli_ID`) REFERENCES `pmb_pliouverture` (`IDpliouverture`),
  ADD CONSTRAINT `FK737BBC092D514C80` FOREIGN KEY (`Evaluateur_ID`) REFERENCES `pmb_evaluateurs` (`ID`),
  ADD CONSTRAINT `FK737BBC09B74B3DAE` FOREIGN KEY (`Souscritere_ID`) REFERENCES `syg_critere` (`ID`);

--
-- Contraintes pour la table `pmb_evaluateursdossier`
--
ALTER TABLE `pmb_evaluateursdossier`
  ADD CONSTRAINT `FKEB51CE6821E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`),
  ADD CONSTRAINT `FKEB51CE682D514C80` FOREIGN KEY (`Evaluateur_ID`) REFERENCES `pmb_evaluateurs` (`ID`);

--
-- Contraintes pour la table `pmb_fournisseurs`
--
ALTER TABLE `pmb_fournisseurs`
  ADD CONSTRAINT `FK57B091ECC845D8E2` FOREIGN KEY (`categoriefournisseurs_id`) REFERENCES `syg_catfour` (`ID`);

--
-- Contraintes pour la table `pmb_garantie`
--
ALTER TABLE `pmb_garantie`
  ADD CONSTRAINT `FKEB839665226E1D09` FOREIGN KEY (`garantieID`) REFERENCES `syg_piecerecu` (`ID`),
  ADD CONSTRAINT `FKEB839665D06EC9D9` FOREIGN KEY (`dossier`) REFERENCES `pmb_dossiers` (`dosID`);

--
-- Contraintes pour la table `pmb_garantiesoumissions`
--
ALTER TABLE `pmb_garantiesoumissions`
  ADD CONSTRAINT `FK3E85AD7B21E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`),
  ADD CONSTRAINT `FK3E85AD7B2B7B217B` FOREIGN KEY (`Pli_ID`) REFERENCES `pmb_pliouverture` (`IDpliouverture`),
  ADD CONSTRAINT `FK3E85AD7B9A83BBB5` FOREIGN KEY (`GARANTIE`) REFERENCES `pmb_garantie` (`ID`);

--
-- Contraintes pour la table `pmb_grillesanalyses`
--
ALTER TABLE `pmb_grillesanalyses`
  ADD CONSTRAINT `FKA0AE703270F4102B` FOREIGN KEY (`idcritereanalyse`) REFERENCES `pmb_critereanalyse` (`idcritereanalyse`),
  ADD CONSTRAINT `FKA0AE70327896BF76` FOREIGN KEY (`Id`) REFERENCES `pmb_typesdossiers` (`Id`);

--
-- Contraintes pour la table `pmb_historiqueappeloffres_ac`
--
ALTER TABLE `pmb_historiqueappeloffres_ac`
  ADD CONSTRAINT `FKE6412EBF6840AB71` FOREIGN KEY (`dosID`) REFERENCES `pmb_dossiers` (`dosID`);

--
-- Contraintes pour la table `pmb_historiquedossier`
--
ALTER TABLE `pmb_historiquedossier`
  ADD CONSTRAINT `FKBB3AF0B421E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`),
  ADD CONSTRAINT `FKBB3AF0B425D5960` FOREIGN KEY (`Appelsoffres_ID`) REFERENCES `pmb_appelsoffres` (`apoID`);

--
-- Contraintes pour la table `pmb_lots`
--
ALTER TABLE `pmb_lots`
  ADD CONSTRAINT `FKA533493C1770904D` FOREIGN KEY (`Contrat_ID`) REFERENCES `pmb_contrats` (`conID`),
  ADD CONSTRAINT `FKA533493C21E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`);

--
-- Contraintes pour la table `pmb_marchefournisseurs`
--
ALTER TABLE `pmb_marchefournisseurs`
  ADD CONSTRAINT `FKF2D1F1EE25D5960` FOREIGN KEY (`Appelsoffres_ID`) REFERENCES `pmb_appelsoffres` (`apoID`),
  ADD CONSTRAINT `FKF2D1F1EEF35C9F63` FOREIGN KEY (`Fournisseurs_ID`) REFERENCES `pmb_fournisseurs` (`ID`);

--
-- Contraintes pour la table `pmb_membrecellulepassation`
--
ALTER TABLE `pmb_membrecellulepassation`
  ADD CONSTRAINT `FK23FCE2924B9DC270` FOREIGN KEY (`Autorite_ID`) REFERENCES `syg_autoritecontractante` (`ID`);

--
-- Contraintes pour la table `pmb_montantsseuils`
--
ALTER TABLE `pmb_montantsseuils`
  ADD CONSTRAINT `FK4B52693B73B9D2D8` FOREIGN KEY (`TYPEAUTORITE`) REFERENCES `syg_typeautoritecontractante` (`ID`),
  ADD CONSTRAINT `FK4B52693B9C2E6DA` FOREIGN KEY (`TYPEMARCHE`) REFERENCES `syg_typesmarches` (`ID`),
  ADD CONSTRAINT `FK4B52693BBB47723C` FOREIGN KEY (`MODEPASSATION`) REFERENCES `syg_modepassation` (`ID`);

--
-- Contraintes pour la table `pmb_observateursindependants`
--
ALTER TABLE `pmb_observateursindependants`
  ADD CONSTRAINT `FK7D074F8421E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`);

--
-- Contraintes pour la table `pmb_operationpaiement`
--
ALTER TABLE `pmb_operationpaiement`
  ADD CONSTRAINT `FK47DDCC58BB2F0857` FOREIGN KEY (`fournisseurID`) REFERENCES `pmb_fournisseurs` (`ID`);

--
-- Contraintes pour la table `pmb_paiement`
--
ALTER TABLE `pmb_paiement`
  ADD CONSTRAINT `FK834A3EA51770DB61` FOREIGN KEY (`contratsId`) REFERENCES `pmb_contrats` (`conID`),
  ADD CONSTRAINT `FK834A3EA5BB2F0857` FOREIGN KEY (`fournisseurId`) REFERENCES `pmb_fournisseurs` (`ID`),
  ADD CONSTRAINT `FK834A3EA5EF4092C5` FOREIGN KEY (`banqueID`) REFERENCES `pmb_banque` (`id`);

--
-- Contraintes pour la table `pmb_piecesplisouvertures`
--
ALTER TABLE `pmb_piecesplisouvertures`
  ADD CONSTRAINT `FK849675C721E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`),
  ADD CONSTRAINT `FK849675C74CEAE4D7` FOREIGN KEY (`piece_ID`) REFERENCES `pmb_piece` (`IDPIECE`),
  ADD CONSTRAINT `FK849675C757A27964` FOREIGN KEY (`pliouverture_ID`) REFERENCES `pmb_pliouverture` (`IDpliouverture`);

--
-- Contraintes pour la table `pmb_piecesrequises`
--
ALTER TABLE `pmb_piecesrequises`
  ADD CONSTRAINT `FK30FFBF2E7896BF76` FOREIGN KEY (`Id`) REFERENCES `pmb_typesdossiers` (`Id`),
  ADD CONSTRAINT `FK30FFBF2ED96B837E` FOREIGN KEY (`IDPIECE`) REFERENCES `pmb_piece` (`IDPIECE`);

--
-- Contraintes pour la table `pmb_pjdenonciation`
--
ALTER TABLE `pmb_pjdenonciation`
  ADD CONSTRAINT `FK6F10DA97ECB9AC44` FOREIGN KEY (`id`) REFERENCES `pmb_denonciation` (`ID`);

--
-- Contraintes pour la table `pmb_plansdepassation`
--
ALTER TABLE `pmb_plansdepassation`
  ADD CONSTRAINT `FK8789E873484933B1` FOREIGN KEY (`infoplanID`) REFERENCES `pmb_plansdepassation` (`IDinfoplan`);

--
-- Contraintes pour la table `pmb_plilot`
--
ALTER TABLE `pmb_plilot`
  ADD CONSTRAINT `FK2C29F9BE21E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`),
  ADD CONSTRAINT `FK2C29F9BE2B7B217B` FOREIGN KEY (`Pli_ID`) REFERENCES `pmb_pliouverture` (`IDpliouverture`),
  ADD CONSTRAINT `FK2C29F9BEB5246B11` FOREIGN KEY (`Lot_ID`) REFERENCES `pmb_lots` (`lotID`);

--
-- Contraintes pour la table `pmb_pliouverture`
--
ALTER TABLE `pmb_pliouverture`
  ADD CONSTRAINT `FK24A2729E21E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`),
  ADD CONSTRAINT `FK24A2729EF35C9F63` FOREIGN KEY (`fournisseurs_ID`) REFERENCES `pmb_fournisseurs` (`ID`);

--
-- Contraintes pour la table `pmb_presenceouverture`
--
ALTER TABLE `pmb_presenceouverture`
  ADD CONSTRAINT `FKADA5339C21E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`),
  ADD CONSTRAINT `FKADA5339C25D5960` FOREIGN KEY (`Appelsoffres_ID`) REFERENCES `pmb_appelsoffres` (`apoID`),
  ADD CONSTRAINT `FKADA5339C2B7B217B` FOREIGN KEY (`Pli_ID`) REFERENCES `pmb_pliouverture` (`IDpliouverture`);

--
-- Contraintes pour la table `pmb_prestataire`
--
ALTER TABLE `pmb_prestataire`
  ADD CONSTRAINT `FK5ED4FD18AF6AB437` FOREIGN KEY (`idpays`) REFERENCES `syg_pays` (`idpays`);

--
-- Contraintes pour la table `pmb_prestatairesaudits`
--
ALTER TABLE `pmb_prestatairesaudits`
  ADD CONSTRAINT `FKC0B316133A25A6E3` FOREIGN KEY (`idprestataire`) REFERENCES `pmb_prestataire` (`idprestataire`),
  ADD CONSTRAINT `FKC0B316133C56A135` FOREIGN KEY (`idaudit`) REFERENCES `pmb_audit` (`idaudit`);

--
-- Contraintes pour la table `pmb_realisation_bailleurs`
--
ALTER TABLE `pmb_realisation_bailleurs`
  ADD CONSTRAINT `FK77F9112F9A3D9319` FOREIGN KEY (`REALISATION`) REFERENCES `pmb_realisationduplan` (`ID_plan`);

--
-- Contraintes pour la table `pmb_realisation_plans`
--
ALTER TABLE `pmb_realisation_plans`
  ADD CONSTRAINT `FKB2AA5C9E482CA2A8` FOREIGN KEY (`PLAN`) REFERENCES `pmb_plansdepassation` (`IDinfoplan`),
  ADD CONSTRAINT `FKB2AA5C9E9A3D9319` FOREIGN KEY (`REALISATION`) REFERENCES `pmb_realisationduplan` (`ID_plan`);

--
-- Contraintes pour la table `pmb_recours`
--
ALTER TABLE `pmb_recours`
  ADD CONSTRAINT `FKB69CAEFD21E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`),
  ADD CONSTRAINT `FKB69CAEFD25D5960` FOREIGN KEY (`Appelsoffres_ID`) REFERENCES `pmb_appelsoffres` (`apoID`);

--
-- Contraintes pour la table `pmb_recouvrement`
--
ALTER TABLE `pmb_recouvrement`
  ADD CONSTRAINT `FK7D8096CB90154997` FOREIGN KEY (`appeloffreId`) REFERENCES `pmb_appelsoffres` (`apoID`),
  ADD CONSTRAINT `FK7D8096CBEF4092C5` FOREIGN KEY (`banqueID`) REFERENCES `pmb_banque` (`id`);

--
-- Contraintes pour la table `pmb_representantsservicestechniques`
--
ALTER TABLE `pmb_representantsservicestechniques`
  ADD CONSTRAINT `FK416DA65B21E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`);

--
-- Contraintes pour la table `pmb_resultatnegociation`
--
ALTER TABLE `pmb_resultatnegociation`
  ADD CONSTRAINT `FK54080C4621E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`),
  ADD CONSTRAINT `FK54080C4625D5960` FOREIGN KEY (`Appelsoffres_ID`) REFERENCES `pmb_appelsoffres` (`apoID`);

--
-- Contraintes pour la table `pmb_retraitregistredao`
--
ALTER TABLE `pmb_retraitregistredao`
  ADD CONSTRAINT `FK32699CA821E224C0` FOREIGN KEY (`Dossiers_ID`) REFERENCES `pmb_dossiers` (`dosID`);

--
-- Contraintes pour la table `pmb_service`
--
ALTER TABLE `pmb_service`
  ADD CONSTRAINT `FKEC594A7B5B8B65F4` FOREIGN KEY (`Typeservice_ID`) REFERENCES `syg_typeservice` (`ID`);

--
-- Contraintes pour la table `pmb_tcourriers_ac`
--
ALTER TABLE `pmb_tcourriers_ac`
  ADD CONSTRAINT `FK7C888F5517AC5AC8` FOREIGN KEY (`tbureau`) REFERENCES `syg_bureauxdcmp` (`ID`),
  ADD CONSTRAINT `FK7C888F558087BA42` FOREIGN KEY (`agentdestinataire`) REFERENCES `utilisateur` (`USR_ID`),
  ADD CONSTRAINT `FK7C888F55ADA90538` FOREIGN KEY (`tcourrierac`) REFERENCES `pmb_courriers_ac` (`id`);

--
-- Contraintes pour la table `pmb_typemodeledocument`
--
ALTER TABLE `pmb_typemodeledocument`
  ADD CONSTRAINT `FKA2CD05AB9D3EDCDC` FOREIGN KEY (`TypesDossiers`) REFERENCES `pmb_typesdossiers` (`Id`),
  ADD CONSTRAINT `FKA2CD05ABE4B6F694` FOREIGN KEY (`ModeleDocument`) REFERENCES `pmb_modeledocument` (`Id`);

--
-- Contraintes pour la table `pmb_typesmarches_modespassations`
--
ALTER TABLE `pmb_typesmarches_modespassations`
  ADD CONSTRAINT `FKAFDF47F2337443D8` FOREIGN KEY (`TYPE`) REFERENCES `syg_typesmarches` (`ID`),
  ADD CONSTRAINT `FKAFDF47F285E1049E` FOREIGN KEY (`MODE`) REFERENCES `syg_modepassation` (`ID`);

--
-- Contraintes pour la table `pmb_typesmarches_modesselections`
--
ALTER TABLE `pmb_typesmarches_modesselections`
  ADD CONSTRAINT `FK257790EA337443D8` FOREIGN KEY (`TYPE`) REFERENCES `syg_typesmarches` (`ID`),
  ADD CONSTRAINT `FK257790EA581FBCA6` FOREIGN KEY (`MODE`) REFERENCES `syg_modeselection` (`ID`),
  ADD CONSTRAINT `FKBC510DAE337443D8` FOREIGN KEY (`TYPE`) REFERENCES `syg_typesmarches` (`ID`),
  ADD CONSTRAINT `FKBC510DAE581FBCA6` FOREIGN KEY (`MODE`) REFERENCES `syg_modeselection` (`ID`);

--
-- Contraintes pour la table `pmb_typeuniteorgarmp`
--
ALTER TABLE `pmb_typeuniteorgarmp`
  ADD CONSTRAINT `FK44661B2B438A352F` FOREIGN KEY (`typeuniteorgID`) REFERENCES `pmb_typeuniteorgarmp` (`ID`);

--
-- Contraintes pour la table `pmb_typeuniteorgdcmp`
--
ALTER TABLE `pmb_typeuniteorgdcmp`
  ADD CONSTRAINT `FK44673FF9438B59FD` FOREIGN KEY (`typeuniteorgID`) REFERENCES `pmb_typeuniteorgdcmp` (`ID`);

--
-- Contraintes pour la table `pmb_uniteorgarmp`
--
ALTER TABLE `pmb_uniteorgarmp`
  ADD CONSTRAINT `FK7F671FF1438A352F` FOREIGN KEY (`TypeUniteOrgID`) REFERENCES `pmb_typeuniteorgarmp` (`ID`),
  ADD CONSTRAINT `FK7F671FF1B388FD1B` FOREIGN KEY (`UniteOrgID`) REFERENCES `pmb_uniteorgarmp` (`ID`);

--
-- Contraintes pour la table `profil_role`
--
ALTER TABLE `profil_role`
  ADD CONSTRAINT `FK8C670499C97CE164` FOREIGN KEY (`PF_CODE`) REFERENCES `sys_profil` (`PF_CODE`),
  ADD CONSTRAINT `FK8C670499DC88A854` FOREIGN KEY (`ROL_CODE`) REFERENCES `sys_role` (`ROL_CODE`);

--
-- Contraintes pour la table `sygproformation`
--
ALTER TABLE `sygproformation`
  ADD CONSTRAINT `FKDE11E5C54B9DC270` FOREIGN KEY (`Autorite_ID`) REFERENCES `syg_autoritecontractante` (`ID`);

--
-- Contraintes pour la table `syg_archive_contenu`
--
ALTER TABLE `syg_archive_contenu`
  ADD CONSTRAINT `FK63A34C7F55868AE6` FOREIGN KEY (`NoeudClassement_CODE`) REFERENCES `syg_noeud_classement` (`id`);

--
-- Contraintes pour la table `syg_autoritecontractante`
--
ALTER TABLE `syg_autoritecontractante`
  ADD CONSTRAINT `FK4E3CB8C5349479D8` FOREIGN KEY (`POLE`) REFERENCES `syg_poledcmp` (`ID`),
  ADD CONSTRAINT `FK4E3CB8C573B9D2D8` FOREIGN KEY (`TYPEAUTORITE`) REFERENCES `syg_typeautoritecontractante` (`ID`);

--
-- Contraintes pour la table `syg_bureauxdcmp`
--
ALTER TABLE `syg_bureauxdcmp`
  ADD CONSTRAINT `FK7223B62287968BB2` FOREIGN KEY (`DIVISION`) REFERENCES `pmb_typeuniteorgdcmp` (`ID`);

--
-- Contraintes pour la table `syg_categorie`
--
ALTER TABLE `syg_categorie`
  ADD CONSTRAINT `FKB46CDD391A0469E2` FOREIGN KEY (`CATEGORI`) REFERENCES `syg_categorie` (`ID`),
  ADD CONSTRAINT `FKB46CDD39F9B5AB3C` FOREIGN KEY (`TYPE`) REFERENCES `syg_typecategori` (`ID`);

--
-- Contraintes pour la table `syg_dossierpiecesrequises`
--
ALTER TABLE `syg_dossierpiecesrequises`
  ADD CONSTRAINT `FK8E863641132C709D` FOREIGN KEY (`DOSSIER`) REFERENCES `sygdossiercourrier` (`id`),
  ADD CONSTRAINT `FK8E8636416B247BB5` FOREIGN KEY (`ID_PIECESREQUISE`) REFERENCES `pmb_piecesrequises` (`idpiecesrequises`);

--
-- Contraintes pour la table `syg_formateur`
--
ALTER TABLE `syg_formateur`
  ADD CONSTRAINT `FKF2B1A64D46971F09` FOREIGN KEY (`TYPEFORM`) REFERENCES `syg_typesformateur` (`IDFORM`),
  ADD CONSTRAINT `FKF2B1A64D990BE908` FOREIGN KEY (`SPECIALITE`) REFERENCES `syg_specialite` (`ID`);

--
-- Contraintes pour la table `syg_formation`
--
ALTER TABLE `syg_formation`
  ADD CONSTRAINT `FK6A23D473317E865E` FOREIGN KEY (`MODULE`) REFERENCES `syg_module` (`ID`),
  ADD CONSTRAINT `FK6A23D47343D40590` FOREIGN KEY (`FORMATEUR`) REFERENCES `syg_formateur` (`ID`);

--
-- Contraintes pour la table `syg_noeud_classement`
--
ALTER TABLE `syg_noeud_classement`
  ADD CONSTRAINT `FK7B7095F555868AE6` FOREIGN KEY (`NoeudClassement_CODE`) REFERENCES `syg_noeud_classement` (`id`),
  ADD CONSTRAINT `FK7B7095F5CE805A92` FOREIGN KEY (`TypeElement_CODE`) REFERENCES `syg_typeelementarbre` (`ID`);

--
-- Contraintes pour la table `syg_secteuractivites`
--
ALTER TABLE `syg_secteuractivites`
  ADD CONSTRAINT `FK67B51CD79B15DAFD` FOREIGN KEY (`SECTEUR`) REFERENCES `syg_secteuractivites` (`CODE`);

--
-- Contraintes pour la table `syg_tachesafaire`
--
ALTER TABLE `syg_tachesafaire`
  ADD CONSTRAINT `FK3606627E10FC430D` FOREIGN KEY (`TYPE`) REFERENCES `syg_typestaches` (`ID`),
  ADD CONSTRAINT `FK3606627EF2E5DFB0` FOREIGN KEY (`USERS`) REFERENCES `utilisateur` (`USR_ID`);

--
-- Contraintes pour la table `syg_tdossiercourrier`
--
ALTER TABLE `syg_tdossiercourrier`
  ADD CONSTRAINT `FK84E3D7428F53DE7F` FOREIGN KEY (`COURRIER`) REFERENCES `sygdossiercourrier` (`id`),
  ADD CONSTRAINT `FK84E3D742B12707C1` FOREIGN KEY (`STATE`) REFERENCES `sys_state` (`STA_CODE`),
  ADD CONSTRAINT `FK84E3D742CF488463` FOREIGN KEY (`EXPEDITEUR`) REFERENCES `utilisateur` (`USR_ID`);

--
-- Contraintes pour la table `syg_typecategori`
--
ALTER TABLE `syg_typecategori`
  ADD CONSTRAINT `FK72AD4306383A8E2A` FOREIGN KEY (`typeCategori`) REFERENCES `syg_typecategori` (`ID`);

--
-- Contraintes pour la table `sys_action`
--
ALTER TABLE `sys_action`
  ADD CONSTRAINT `FKC81CEBE8BE8316EA` FOREIGN KEY (`FEA_CODE`) REFERENCES `sys_feauture` (`FEA_CODE`),
  ADD CONSTRAINT `FKC81CEBE8DC54D1E9` FOREIGN KEY (`FEA_CODE`) REFERENCES `sys_feauture` (`FEA_CODE`),
  ADD CONSTRAINT `FKC81CEBE8FDAD492E` FOREIGN KEY (`FEA_CODE`) REFERENCES `sys_feauture` (`FEA_CODE`);

--
-- Contraintes pour la table `sys_alerte`
--
ALTER TABLE `sys_alerte`
  ADD CONSTRAINT `FKC895103B20D0FEC1` FOREIGN KEY (`STA_CODE`) REFERENCES `sys_state` (`STA_CODE`),
  ADD CONSTRAINT `FKC895103B27DC46A0` FOREIGN KEY (`STA_CODE`) REFERENCES `sys_state` (`STA_CODE`),
  ADD CONSTRAINT `FKC895103BF805BFDC` FOREIGN KEY (`STA_CODE`) REFERENCES `sys_state` (`STA_CODE`);

--
-- Contraintes pour la table `sys_alerteprofil`
--
ALTER TABLE `sys_alerteprofil`
  ADD CONSTRAINT `FK9F31F63715A35950` FOREIGN KEY (`ALT_ID`) REFERENCES `sys_alerte` (`ALT_ID`),
  ADD CONSTRAINT `FK9F31F6375C186C28` FOREIGN KEY (`PF_CODE`) REFERENCES `sys_profil` (`PF_CODE`),
  ADD CONSTRAINT `FK9F31F6378307CE8C` FOREIGN KEY (`ALT_ID`) REFERENCES `sys_alerte` (`ALT_ID`),
  ADD CONSTRAINT `FK9F31F637AB3EC271` FOREIGN KEY (`ALT_ID`) REFERENCES `sys_alerte` (`ALT_ID`),
  ADD CONSTRAINT `FK9F31F637C97CE164` FOREIGN KEY (`PF_CODE`) REFERENCES `sys_profil` (`PF_CODE`),
  ADD CONSTRAINT `FK9F31F637F1B3D549` FOREIGN KEY (`PF_CODE`) REFERENCES `sys_profil` (`PF_CODE`);

--
-- Contraintes pour la table `sys_arraw`
--
ALTER TABLE `sys_arraw`
  ADD CONSTRAINT `FK1F418665129F294D` FOREIGN KEY (`ARW_TOSTATE`) REFERENCES `sys_state` (`STA_CODE`),
  ADD CONSTRAINT `FK1F4186653B6A6832` FOREIGN KEY (`ARW_TOSTATE`) REFERENCES `sys_state` (`STA_CODE`),
  ADD CONSTRAINT `FK1F4186654275B011` FOREIGN KEY (`ARW_TOSTATE`) REFERENCES `sys_state` (`STA_CODE`);

--
-- Contraintes pour la table `sys_arrawperm`
--
ALTER TABLE `sys_arrawperm`
  ADD CONSTRAINT `FKFCD23ED520243910` FOREIGN KEY (`ROL_CODE`) REFERENCES `sys_role` (`ROL_CODE`),
  ADD CONSTRAINT `FKFCD23ED53FA99AEC` FOREIGN KEY (`FROM_STATE`) REFERENCES `sys_state` (`STA_CODE`),
  ADD CONSTRAINT `FKFCD23ED564F06ECE` FOREIGN KEY (`ARW_CODE`) REFERENCES `sys_arraw` (`ARW_CODE`),
  ADD CONSTRAINT `FKFCD23ED56874D9D1` FOREIGN KEY (`FROM_STATE`) REFERENCES `sys_state` (`STA_CODE`),
  ADD CONSTRAINT `FKFCD23ED56F8021B0` FOREIGN KEY (`FROM_STATE`) REFERENCES `sys_state` (`STA_CODE`),
  ADD CONSTRAINT `FKFCD23ED58DBBADB3` FOREIGN KEY (`ARW_CODE`) REFERENCES `sys_arraw` (`ARW_CODE`),
  ADD CONSTRAINT `FKFCD23ED594C6F592` FOREIGN KEY (`ARW_CODE`) REFERENCES `sys_arraw` (`ARW_CODE`),
  ADD CONSTRAINT `FKFCD23ED5D59778CF` FOREIGN KEY (`ROL_CODE`) REFERENCES `sys_role` (`ROL_CODE`),
  ADD CONSTRAINT `FKFCD23ED5DC88A854` FOREIGN KEY (`ROL_CODE`) REFERENCES `sys_role` (`ROL_CODE`);

--
-- Contraintes pour la table `sys_feauture`
--
ALTER TABLE `sys_feauture`
  ADD CONSTRAINT `FK86035D59595B02CC` FOREIGN KEY (`MOD_CODE`) REFERENCES `sys_module` (`MOD_CODE`),
  ADD CONSTRAINT `FK86035D59C6BF7808` FOREIGN KEY (`MOD_CODE`) REFERENCES `sys_module` (`MOD_CODE`),
  ADD CONSTRAINT `FK86035D59EEF66BED` FOREIGN KEY (`MOD_CODE`) REFERENCES `sys_module` (`MOD_CODE`);

--
-- Contraintes pour la table `sys_profilaction`
--
ALTER TABLE `sys_profilaction`
  ADD CONSTRAINT `FK8D4FFA45C186C28` FOREIGN KEY (`PF_CODE`) REFERENCES `sys_profil` (`PF_CODE`),
  ADD CONSTRAINT `FK8D4FFA46338306` FOREIGN KEY (`ACT_CODE`) REFERENCES `sys_action` (`ACT_CODE`),
  ADD CONSTRAINT `FK8D4FFA47397F842` FOREIGN KEY (`ACT_CODE`) REFERENCES `sys_action` (`ACT_CODE`),
  ADD CONSTRAINT `FK8D4FFA49BCEEC27` FOREIGN KEY (`ACT_CODE`) REFERENCES `sys_action` (`ACT_CODE`),
  ADD CONSTRAINT `FK8D4FFA4C97CE164` FOREIGN KEY (`PF_CODE`) REFERENCES `sys_profil` (`PF_CODE`),
  ADD CONSTRAINT `FK8D4FFA4F1B3D549` FOREIGN KEY (`PF_CODE`) REFERENCES `sys_profil` (`PF_CODE`);

--
-- Contraintes pour la table `sys_profilarrowperm`
--
ALTER TABLE `sys_profilarrowperm`
  ADD CONSTRAINT `FK81F69A4B5A99B138` FOREIGN KEY (`APM_ID`) REFERENCES `sys_arrawperm` (`APM_ID`),
  ADD CONSTRAINT `FK81F69A4B5C186C28` FOREIGN KEY (`PF_CODE`) REFERENCES `sys_profil` (`PF_CODE`),
  ADD CONSTRAINT `FK81F69A4B9AAC2C74` FOREIGN KEY (`APM_ID`) REFERENCES `sys_arrawperm` (`APM_ID`),
  ADD CONSTRAINT `FK81F69A4BC97CE164` FOREIGN KEY (`PF_CODE`) REFERENCES `sys_profil` (`PF_CODE`),
  ADD CONSTRAINT `FK81F69A4BDC4370D9` FOREIGN KEY (`APM_ID`) REFERENCES `sys_arrawperm` (`APM_ID`),
  ADD CONSTRAINT `FK81F69A4BF1B3D549` FOREIGN KEY (`PF_CODE`) REFERENCES `sys_profil` (`PF_CODE`);

--
-- Contraintes pour la table `sys_state`
--
ALTER TABLE `sys_state`
  ADD CONSTRAINT `FK203FD89F71298671` FOREIGN KEY (`OBJ_CODE`) REFERENCES `sys_oblect` (`OBJ_CODE`),
  ADD CONSTRAINT `FK203FD89F96CBD270` FOREIGN KEY (`OBJ_CODE`) REFERENCES `sys_oblect` (`OBJ_CODE`),
  ADD CONSTRAINT `FK203FD89FA62F34B5` FOREIGN KEY (`OBJ_CODE`) REFERENCES `sys_oblect` (`OBJ_CODE`);

--
-- Contraintes pour la table `sys_stateperm`
--
ALTER TABLE `sys_stateperm`
  ADD CONSTRAINT `FKD588E40F20243910` FOREIGN KEY (`ROL_CODE`) REFERENCES `sys_role` (`ROL_CODE`),
  ADD CONSTRAINT `FKD588E40F20D0FEC1` FOREIGN KEY (`STA_CODE`) REFERENCES `sys_state` (`STA_CODE`),
  ADD CONSTRAINT `FKD588E40F27DC46A0` FOREIGN KEY (`STA_CODE`) REFERENCES `sys_state` (`STA_CODE`),
  ADD CONSTRAINT `FKD588E40F5C186C28` FOREIGN KEY (`PF_CODE`) REFERENCES `sys_profil` (`PF_CODE`),
  ADD CONSTRAINT `FKD588E40FC97CE164` FOREIGN KEY (`PF_CODE`) REFERENCES `sys_profil` (`PF_CODE`),
  ADD CONSTRAINT `FKD588E40FD59778CF` FOREIGN KEY (`ROL_CODE`) REFERENCES `sys_role` (`ROL_CODE`),
  ADD CONSTRAINT `FKD588E40FDC88A854` FOREIGN KEY (`ROL_CODE`) REFERENCES `sys_role` (`ROL_CODE`),
  ADD CONSTRAINT `FKD588E40FF1B3D549` FOREIGN KEY (`PF_CODE`) REFERENCES `sys_profil` (`PF_CODE`),
  ADD CONSTRAINT `FKD588E40FF805BFDC` FOREIGN KEY (`STA_CODE`) REFERENCES `sys_state` (`STA_CODE`);

--
-- Contraintes pour la table `sys_utilisateuralerte`
--
ALTER TABLE `sys_utilisateuralerte`
  ADD CONSTRAINT `FK49BD56DA14540F12` FOREIGN KEY (`USR_ID`) REFERENCES `utilisateur` (`USR_ID`),
  ADD CONSTRAINT `FK49BD56DA15A35950` FOREIGN KEY (`ALT_ID`) REFERENCES `sys_alerte` (`ALT_ID`),
  ADD CONSTRAINT `FK49BD56DA8307CE8C` FOREIGN KEY (`ALT_ID`) REFERENCES `sys_alerte` (`ALT_ID`),
  ADD CONSTRAINT `FK49BD56DAAB3EC271` FOREIGN KEY (`ALT_ID`) REFERENCES `sys_alerte` (`ALT_ID`),
  ADD CONSTRAINT `FK49BD56DAB0B9B3F3` FOREIGN KEY (`USR_ID`) REFERENCES `utilisateur` (`USR_ID`),
  ADD CONSTRAINT `FK49BD56DABA70254E` FOREIGN KEY (`USR_ID`) REFERENCES `utilisateur` (`USR_ID`);

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `FKDD1633835C186C28` FOREIGN KEY (`PF_CODE`) REFERENCES `sys_profil` (`PF_CODE`);