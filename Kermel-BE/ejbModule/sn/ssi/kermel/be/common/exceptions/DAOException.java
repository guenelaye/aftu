package sn.ssi.kermel.be.common.exceptions;

@SuppressWarnings("serial")
public class DAOException extends ErrorCodeException {

	public DAOException(final ErrorCode err) {
		super(err);
	}

}
