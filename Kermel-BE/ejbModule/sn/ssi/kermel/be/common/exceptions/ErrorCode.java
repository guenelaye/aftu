package sn.ssi.kermel.be.common.exceptions;

public interface ErrorCode 
{
	/**
	 * Return the code for this ErrorCode. 
	 * @return the code for this ErrorCode.
	 */
	public String getCode();
	
}
