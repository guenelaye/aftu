package sn.ssi.kermel.be.common.exceptions;

@SuppressWarnings("serial")
public class ErrorCodeException extends Exception {
	/** The key identifying the exception */
	protected ErrorCode err;

	/** Extra info associated to this exception */
	protected String info;

	/**
	 * Creates a new ErrorCodeException
	 * 
	 * @param err
	 */
	public ErrorCodeException(final ErrorCode err) {
		super();
		this.err = err;
	}

	/**
	 * Creates a new ErrorCodeException
	 * 
	 * @param err
	 * @param cause
	 */
	public ErrorCodeException(final ErrorCode err, final Throwable cause) {
		super(cause);
		this.err = err;
	}

	/**
	 * Creates a new ErrorCodeException
	 * 
	 * @param err
	 * @param message
	 * @param cause
	 */
	public ErrorCodeException(final ErrorCode err, final String message,
			final Throwable cause) {
		super(message, cause);
		this.err = err;
	}

	/**
	 * Creates a new ErrorCodeException
	 * 
	 * @param err
	 * @param message
	 */
	public ErrorCodeException(final ErrorCode err, final String message) {
		super(message);
		this.err = err;
	}

	/**
	 * Return the err for this exception (one of the BEErrors constants). To get
	 * the error code, just use e.getErrorCode().getCode().
	 * 
	 * @return the err for this exception
	 */
	public ErrorCode getErrorCode() {
		return err;
	}

	/**
	 * Return the extra info associated to this Exception.
	 * 
	 * @return
	 */
	public String getInfo() {
		return info;
	}

	/**
	 * Set the extra info for this Exception.
	 * 
	 * @param info
	 */
	public void setInfo(final String info) {
		this.info = info;
	}

}
