package sn.ssi.kermel.be.common.utils;

public class ConstKermel {

	public static final int TYPE_STRUCTURE_SERVICE = 1669;

	public static final char affirmativeCode = 'O';
	public static final char negativeCode = 'N';

	public static final char personnePhysiqueCode = 'P';
	public static final char personneMoraleCode = 'M';

	public static final int valideCode = 1;
	public static final int invalideCode = 2;

	public static final String affirmativeLibelle = "Oui";
	public static final String negativeLibelle = "Non";

	public static final String profilAdministrateur = "Administrateur";

	public static final String codeprofilAdministrateur = "ADM";
	public static final String codeprofilCaissier = "CAI";
	public static final String codeprofilAgentCredit = "CRE";
	public static final String codeprofilGerant = "GER";

	public static final String personnePhysiqueLibelle = "Personne physique";
	public static final String personneMoraleLibelle = "Personne morale";

	public static final String valideLibelle = "Valide";
	public static final String invalideLibelle = "Invalide";

	public static final String valider = "Valider";
	public static final String supprimer = "Supprimer";

	public static final String debutCellule = "[";
	public static final String finCellule = "]";
	public static final String separateurCellule = "-";
	public static final String celluleActif = "A";
	public static final String celluleSupprime = "D";

	public static String DATE_DD_MM_YYYY = "dd/MM/yyyy";

	public static int STATUT_LIGNE_ACCEPTE = 0;
	public static int STATUT_LIGNE_REJETE = 1;
	public static String FICHIER_EXTERNE_ENREG_FIN = "99";
	public static String STATUT_OK = "OK";
	public static String STATUT_REJET = "REJETE";

	public static int TYPE_PERSONNEPHYSIQUE = 1;
	public static int TYPE_PERSONNEMORALE = 2;
	public static String PERSONNEPHYSIQUE = "Personne physique";
	public static String PERSONNEMORALE = "Personne morale";
	public static final String INSERT = "Ins";
	public static final String UPDATE = "Mod";
	public static final String DELETE = "Del";

	public static final String prelevementsurcompte = "Prelevement sur compte ";
	public static final String prelevement = "Prelevement";
	public static final String octroicredit = "Octroie de Cr�dit";

	public static final int dossier_saisi = 1;
	public static final int dossier_attente_finance = 2;
	public static final int dossier_valide = 3;
	public static final int dossier_en_souffrace = 4;
	public static final int dossier_annule = 5;
	public static final int dossier_pre_Contencieux = 6;
	public static final int dossier_Contencieux = 7;
	public static final int dossier_Solder = 8;

	public static final int epargne_nanti = 1;
	public static final int epargne_non_nanti = 0;

	public static final int valide = 1;
	public static final int non_valide = 0;

	public static final int oui = 1;
	public static final int non = 0;

	public static final int DUREECREDITCT = 6;
	public static final int DUREECREDITMLT = 12;

	public static final String CREDITCT = "1";
	public static final String CREDITMLT = "2";

	/*
	 * public static final String profil_administrateur ="1"; public static
	 * final String profil_caissier ="2"; public static final String
	 * profil_agent_credit ="3";
	 */

	public static final int statut_credit_saisi = 1;
	public static final int statut_credit_en_attente_finacement = 2;
	public static final int statut_credit_Valide = 3;
	public static final int statut_credit_en_souffrance = 4;
	public static final int statut_credit_anuler = 5;
	public static final int statut_credit_pre_contencieux = 6;
	public static final int statut_credit_contencieux = 7;
	public static final int statut_credit_solder = 8;

	public static final String msgmodif = "Modification effectu�e avec succ�s";

	public static final String codeProfilAdministrateur = "ADM";
	public static final String codeProfilGerant = "GER";
	public static final String codeProfilCredit = "CRE";
	public static final String codeProfilCaissier = "CAI";

	public static final int clientParticulier = 0;
	public static final int clientGroupement = 1;
	public static final int clientEntreprise = 2;

	public static final String libclientParticulier = "Particulier";
	public static final String libclientGroupement = "Groupement";
	public static final String libclientEntreprise = "Entreprise";

	public static final int clientRegulier = 1;
	public static final int clientIrregulier = 2;

	public static final String qualiteClientRegulier = "Regulier";
	public static final String qualiteClientIrregulier = "Irregulier";

	public static final int statutJourneeCaisseOuverte = 1;
	public static final int statutJourneeCaisseFermee = 0;

	public static final int Responsable = 1;
	public static final int non_Responsable = 0;

	public static final int AmortissementLineaire = 1;
	public static final int AmortissementDegressif = 0;

	public static final int sens_debit = 1;
	public static final int sens_credit = 0;

	public static final int statutBatch_Encours = 1;
	public static final int statutBatch_OK = 2;
	public static final int statutBatch_KO = 3;
	public static final String codeBatchCapitalisation = "CAPIN";
	public static final long trois_mois = 90;
	public static final long six_mois = 180;
	public static final long douze_mois = 365;
	public static final long heure_to_ms = 3600000;
	public static final long s_to_ms = 1000;
	public static final long jour_to_heure = 24;

	public static final String comboTypeEnt = "typeEnt";

	public static final int typeclientParticulier = 0;
	public static final int typeclientGroupement = 1;
	public static final int typeclientEntreprise = 2;

	public static final String debit = "debit";
	public static final String credit = "credit";

	public static final String augmenter = "augmenter";
	public static final String diminuer = "diminuer";

	public static final int operation_banque_versement = 1;
	public static final int operation_banque_retrait = 2;

	public static final int etatCompteActif = 1;
	public static final int etatCompteFerme = 2;

	public static final String compteActif = "Actif";
	public static final String compteFerme = "Cl�tur�";

	public static final String annuler = "Annuler";
	public static final String regulariser = "Regulariser";

	public static final String annulation = "Annulation";

}
