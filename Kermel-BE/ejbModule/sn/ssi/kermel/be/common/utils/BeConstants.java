package sn.ssi.kermel.be.common.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;;

/**
 * @author OLA Abdel Rakib (olarakib@gmail.com), Guillaume GB
 *         (guibenissan@yahoo.fr)
 * @since Mardi 23 Fevrier 2010
 * @version 0.0.1
 */
public class BeConstants {

	/**
	 * PARAMETRE GENERAL: Numéro dossier courrier
	 */
	public static final String PARAM_NUMDOSC = "NUMDOSC";
	/**
	 * PARAMETRE GENERAL: Numéro dossier courrier
	 */
	public static final String PARAM_MATINS = "MATINS";

	/**
	 * PARAMETRE GENERAL: Numéro dossier agent
	 */
	public static final String PARAM_NUMDOSA = "NUMDOSA";

	/**
	 * PARAMETRE GENERAL: Matricule�ro dossier agent
	 */
	public static final String PARAM_MATRICULEAGENT = "MATRICULEAGENT";

	public static final String PARAM_CONCOURS = "CONCOURS";
	public static final String PARAM_TYPEACTE = "TYPEACTE";
	public static final String PARAM_DOSSIERCANDIDAT = "DOSSIERCANDIDAT";
	public static final String PARAM_RESULTAT = "RESULTAT";
	public static final String PARAM_CATEGORIE = "CATEGORIE";
	public static final String PARAM_CORPS = "CORPS";
	public static final String PARAM_GRADE = "GRADE";
	public static final String PARAM_AVANCEMENT = "AVANCEMENT";
	public static final String PARAM_ECHELON = "ECHELON";
	public static final String PARAM_PJ = "PJ";
	public static final String PARAM_TYPEDOSSIER = "TYPEDOSSIER";
	public static final String PARAM_PIECESREQUISE = "PIECESREQUISE";
	public static final String PARAM_CONDITIONCONCOURS = "CONDITIONCONCOURS";
	public static final String PARAM_CORPSDIPLOME = "CORPSDIPLOME";
	public static final String PARAM_PROGRAMMECONCOURS = "PROGRAMMECONCOURS";
	public static final String PARAM_EPREUVECONCOURS = "EPREUVECONCOURS";
	public static final String PARAM_NOTECONCOURS = "NOTECONCOURS";
	public static final String PARAM_PJCONCOURS = "PJCONCOURS";
	public static final String PARAM_PJDOSSIERSCANDIDATS = "PJDOSSIERSCANDIDATS";
	public static final String PARAM_CENTREECRITCONCOURS = "CENTREECRITCONCOURS";
	public static final String PARAM_EMPLOICONCOURS = "EMPLOICONCOURS";
	public static final String PARAM_TYPEDISPONIBILITE = "TYPEDISPONIBILITE";
	public static final String PARAM_TYPEDETACHEMENT = "PARAM_TYPEDETACHEMENT";
	public static final String PARAM_STRUCTUREACCUEIL = "PARAM_STRUCTUREACCUEIL";

	public static final String PARAM_ARRETE = "ARRETE";

	public static final String PARAM_SERVICE = "SERVICE";

	public static final String PARAM_SIGNATAIRE = "SIGNATAIRE";

	/**
	 * PARAMETRE GENERAL: code acte
	 */
	public static final String PARAM_CODEACTE = "CODEACTE";
	/**
	 * Nomination (STATES): Dossier créé
	 */
	public static final String CREATION_DOSSIER_STATE = "CREATION_DOSSIER_STATE";
	/**
	 * Nomination (STATES):
	 */
	public static final String IMPUTATION_CHEFDRRS_STATE = "IMPUTATION_CHEFDRRS_STATE";
	/**
	 * Nomination (STATES):
	 */
	public static final String IMPUTATION_CHEFSECTION_STATE = "IMPUTATION_CHEFSECTION_STATE";
	/**
	 * Nomination (STATES):
	 */
	public static final String IMPUTATION_CHARGEDETUDE_STATE = "IMPUTATION_CHARGEDETUDE_STATE";

	/**
	 * Nomination (ACTIONS): Créer Dossier
	 */
	public static final String CREATION_DOSSIER = "CREATION_DOSSIER";

	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_NOMMINATION = "CREATION_PA_NOMMINATION";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String TRANSMISSION_PA_NOMMINATION_CHEFSECTION = "TRANSMISSION_PA_NOMMINATION_CHEFSECTION";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String TRANSMISSION_PA_NOMMINATION_CHEFDRRS = "TRANSMISSION_PA_NOMMINATION_CHEFDRRS";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String TRANSMISSION_PA_NOMMINATION_DG = "TRANSMISSION_PA_NOMMINATION_DG";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String TRANSMISSION_PA_NOMMINATION_MINISTERE = "TRANSMISSION_PA_NOMMINATION_MINISTERE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String ENREGISTREMENT_ACTE_NOMMINATION = "ENREGISTREMENT_ACTE_NOMMINATION";

	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String CREATION_PA_NOMMINATION_STATE = "CREATION_PA_NOMMINATION_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String TRANSMISSION_PA_NOMMINATION_CHEFSECTION_STATE = "TRANSMISSION_PA_NOMMINATION_CHEFSECTION_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String TRANSMISSION_PA_NOMMINATION_CHEFDRRS_STATE = "TRANSMISSION_PA_NOMMINATION_CHEFDRRS_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String TRANSMISSION_PA_NOMMINATION_DG_STATE = "TRANSMISSION_PA_NOMMINATION_DG_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String TRANSMISSION_PA_NOMMINATION_MINISTERE_STATE = "TRANSMISSION_PA_NOMMINATION_MINISTERE_STATE";
	/**
	 * Nomination (Projet d'acte states)
	 */
	public static final String ENREGISTREMENT_ACTE_NOMMINATION_STATE = "ENREGISTREMENT_ACTE_NOMMINATION_STATE";

	/**
	 * 
	 */
	public static final String CANDIDAT_ADMIS = "ADMIS";

	public static final String CANDIDAT_ADMIS_ATTACHE = "ATTACHE";

	public static final String CANDIDAT_ADMIS_NON_ATTACHE = "NON_ATTACHE";

	public static final String CANDIDAT_ADMIS_DOSSIERCREER = "DOSSIERCREER";

	/**
	 * Type dossier et acte
	 * 
	 */
	public static final String TA_NOMINATION = "NOMINATION";
	public static final String TA_IMMATRICULTION = "IMMAT";
	public static final String TA_DISPONIBILITE = "DSP";
	public static final String TA_TITULARISATION = "DPT";
	public static final String TA_AVANCEMENT_ECHELLON = "APAE";
	public static final String TA_AVANCEMENT_GRADE = "APAG";
	public static final String TA_ADMISSION_RETRAITE = "ADRETRAITE";

	public static final String PJ_TYPE_PA = "TYPE_PA";
	public static final String PJ_TYPE_NOTE = "TYPE_NOTE";
	/**
	 * Type dossier et acte
	 * 
	 */

	public static final String PARAM_LOCALITE = "LOCALITE";

	/**
	 * Num�ro type unite administrative
	 */
	public static final String PARAM_TYPUNAD = "TYPUNAD";
	/**
	 * Num�ro unite administrative
	 */
	public static final String PARAM_UAD = "UAD";
	public static final String DEFAULT_VALUE_NUMDOSS = "Num dossier";
	public static final String PARAM_IMMATRICULE = "IMMAT";
	public static final String PARAM_PIECESREQUISE_AGENT = "CODEPRA";
	public static final String PARAM_DOSSIER_COURRIER_AGENT = "CODEDOSAGT";
	public static final int DERNIERECHELON = 1;
	public static final int NONDERNIERECHELON = 0;
	public static final String EN_ACTIVITE = "A0";
	public static final String STA_TITULARISATION = "TITULARISATION";
	public static final String TYPE_AGENT_CANDIDAT = "CANDIDAT";
	public static final String STA_IMMATRICULATION = "IMMATRICULATION";
	public static final String STA_CATEGORIEPOSITION = "POSITION";
	public static final String STA_CATEGORIESANCTION_PREMIER = "SANCTIONPREMIER";
	public static final String STA_CATEGORIESANCTION_SECOND = "SANCTIONSECOND";
	public static final String STA_CATEGORIESORTIEDEF = "SORTIEDEF";
	public static final String STA_CATEGORIESORTIETEMP = "SORTIETEMP";
	public static final String STA_CATEGORIECHANGEMENT = "CHANGEMENT";
	public static final String TYPE_AGT_FONCTIONNAIRE = "FONCTIONNAIRE";
	public static final String TYPE_AGT_NON_FONCTIONNAIRE = "NON_FONCTIONNAIRE";
	public static final String STA_FONCTIONNAIRE = "FONCTIONNAIRE";
	public static final String STA_NON_FONCTIONNAIRE = "NON_FONCTIONNAIRE";

	// Position agent
	public static final String ACTIVITE = "A0";
	public static final String ADMIS_A_LA_RETRAITE = "A";
	public static final String AUTOR_ABS_AVEC_TRAITEMENT = "A1";
	public static final String ABSENCE_IRREGULIERE = "A2";
	public static final String POSITION_DETACHEMENT = "A3";
	public static final String CESSATION_DEFINITIVE_FONCTIONSMEN_MT2MEN8_2MEN = "B";
	public static final String CONGE_ADMINISTRATIF = "B1";
	public static final String AUTOR_ABSENCE_SS_TRAITEMENT = "B2";
	public static final String POSITION_HORS_CADRE = "B3";
	public static final String CESS_DEF_FCTION_COMME_AGENT_TEMPORAIRE = "B4";
	public static final String DEMISSION = "C";
	public static final String CONGE_CONVALESCENCE = "C1";
	public static final String CESSATION_TEMPOR_DE_FONCTIONS = "C2";
	public static final String LICENCIE = "D";
	public static final String CONGE_LONGUE_DUREE = "D1 ";
	public static final String CONGE_AFFAIRES_PERSONNELLES = "D2 ";
	public static final String RADIE = "E";
	public static final String CONGE_MALADIE = "E1";
	public static final String MISE_A_PIED = "E2";
	public static final String REVOQUE = "F";
	public static final String CONGE_MATERNITE = "F1";
	public static final String POSITION_DISPONIBILITE = "F2 ";
	public static final String DECEDE = "G";
	public static final String POSITION_MAINT_PAR_O_SS_AFF = "G1";
	public static final String POSITION_SOUS_LES_DRAPEAUX = "G2";
	public static final String FIN_DE_CONTRAT = "H";
	public static final String POSITION_STAGE_FORM_PROFESS = "H1";
	public static final String SUSPENSION_DE_FONCTIONS = "H2";
	public static final String ABANDON = "I";
	public static final String ABANDON_DE_POSTE = "J2";
	public static final String SANS_POSTE_D_AFFECTATION = "K1";
	public static final String EXCLUSION_TEMPOR_DE_FONCTIONS = "K2";
	public static final String DOUBLON_SUPPRIME = "L0";
	public static final String EN_INSTANCE_DE_REINTEGRATION = "L2";
	public static final String EN_ATTENTE_DE_PRISE_EN_COMPTE = "L3";
	public static final String ABSENT_AU_CONTR_PRESENCE = "M2";
	public static final String EN_DETENTION_PREVENTIVE = "N2";
	public static final String INCARCERATION = "P2";
	public static final String ADMIS_AU_CONCOURS_RECRUTEMENT = "Q";
	public static final String DOIT_PARTIR_A_LA_RETRAITE = "Q0";
	public static final String REFORME = "R";
	public static final String INTEGRE_COM_FONCT_OU_AGT_PERM = "R0";
	public static final String CONGE_SANS_SOLDE = "R2";
	public static final String ANNULE = "ZZ";
	public static final String DETACHEMENT = "DZ";
	public static final String DISPONIBILITE = "DI";
	public static final String MAINTIEN = "M";
	public static final String MOSA = "MO";
	public static final String RETOUR_STAGE = "RS";
	public static final String SPECIALITE = "SSP";

	public static final String TYPE_AGT_CONTRACTUEL = "CONTRACTUEL";
	public static final String TYPE_AGT_ACONTRACTUEL = "ACONTRACTUEL";
	public static final String TYPE_AVCECHELLE = "AVECHELLE";
	public static final String PARAM_NUMBORDEREAU = "NUMBORDEREAU";
	
	public static final String PARAM_NUMSERIE = "NUMSERIE";
	public static final int PARENT=1;
	public static final int NPARENT=0;
	public static final String PARAM_SECTEURACTIVITE = "SECTEURACTIVITE";
	public static final String PARAM_PLANPASSATION = "PLANPASSATION";
	public static final String PARAM_REALISATION = "REALISATION";
	
	public static final Long PARAM_TMTRAVAUX = 3L;
	public static final Long PARAM_TMPI = 4L;
	public static final Long PARAM_TMSERVICES = 2L;
	public static final Long PARAM_TMFOURNITURES = 1L;
	
	public static final String USERS_TYPES_ARMP = "ARMP";
	public static final String USERS_TYPES_DCMP = "DCMP";
	public static final String USERS_TYPES_AC = "AC";
	public static final String USERS_TYPES_AGENTAC = "AGENTAC";
	
	//Add by RAMA 15-11-2012
	public static final String IP_SYGMAP = "localhost";
	public static final String PORT_SYGMAP = "8080";
	public static final String PARAM_NUMRECU = "NUMRECUFISCALE";
	public static final String PARAM_NUMRECUVERSEMENT = "NUMRECUVENTEDAO";
	public static final String PARAM_NUMCONT = "NUMCONTENTIEUX";
	
	public static final String IP_SSI = "localhost";
	public static final String PORT_SSI = "8080";
	public static final String SECURE_IP_SSI = "localhost";
	public static final String SECURE_PORT_SSI = "8443";
	
	public static final String PLANPUB = "PUB";
	public static final String PARAM_NUMIMMATRICULATION = "NUMIMMATRICULATION";
	
	public static final String PARAM_PLANMEV = "MEV";
	public static final String PARAM_PLANVAL = "VAL";
	public static final String  NUMENREGISTREMENT = "NUMENREGISTREMENT";
	
	public static final String path = System.getProperty("jboss.home.dir");
	public static final DateFormat dateFr = DateFormat.getDateInstance(DateFormat.MEDIUM);
	public static final DateFormat formatHeure = new SimpleDateFormat("HH:mm:ss");
	public static final String zipsRepertoire = "C:\\wamp\\www\\kermel\\DepotZip\\";
	public static final String dezipsRepertoire = "C:\\wamp\\www\\kermel\\DepotDezip\\";
//			"/DepotZip/";
	
	
	/*****
	 *      PKERMEL CONSTANTS
	 */
	


}