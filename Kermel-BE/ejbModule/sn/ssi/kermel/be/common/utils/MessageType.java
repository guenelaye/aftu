package sn.ssi.kermel.be.common.utils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class MessageType {
	private String object;
	private String expediteur;
	private String filesPath;
	private String body;
	private String expediteurPassword;
	private String smtp_host;
	private String smtp_port;
	private String smtp_auth;
	private String smtp_starttls;
	private String smtpSender;
	private String smtpSenderPassowrd;
	
	
	public String getSmtpSender() {
		return smtpSender;
	}

	public void setSmtpSender(String smtpSender) {
		this.smtpSender = smtpSender;
	}

	public String getSmtpSenderPassowrd() {
		return smtpSenderPassowrd;
	}

	public void setSmtpSenderPassowrd(String smtpSenderPassowrd) {
		this.smtpSenderPassowrd = smtpSenderPassowrd;
	}

	public MessageType(String object, String expediteur, String filesPath) {
		super();
		this.object = object;
		this.expediteur = expediteur;
		this.filesPath = filesPath;
	}
	
	public MessageType(String object, String expediteur, String filesPath,
			String body, String expediteurPassword, String smtpHost,
			String smtpPort, String smtpAuth, String smtpStarttls) {
		super();
		this.object = object;
		this.expediteur = expediteur;
		this.filesPath = filesPath;
		this.body = body;
		this.expediteurPassword = expediteurPassword;
		smtp_host = smtpHost;
		smtp_port = smtpPort;
		smtp_auth = smtpAuth;
		smtp_starttls = smtpStarttls;
	}

	public String getSmtp_host() {
		return smtp_host;
	}

	public void setSmtp_host(String smtpHost) {
		smtp_host = smtpHost;
	}

	public String getSmtp_port() {
		return smtp_port;
	}

	public void setSmtp_port(String smtpPort) {
		smtp_port = smtpPort;
	}

	public String getSmtp_auth() {
		return smtp_auth;
	}

	public void setSmtp_auth(String smtpAuth) {
		smtp_auth = smtpAuth;
	}

	public String getSmtp_starttls() {
		return smtp_starttls;
	}

	public void setSmtp_starttls(String smtpStarttls) {
		smtp_starttls = smtpStarttls;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getExpediteurPassword() {
		return expediteurPassword;
	}

	public void setExpediteurPassword(String expediteurPassword) {
		this.expediteurPassword = expediteurPassword;
	}

	public MessageType(Node item) {
		expediteur = ((Element)item).getAttribute("Expediteur");
		object = ((Element)item).getAttribute("Objet");
		filesPath = ((Element)item).getAttribute("Chemin_fichiers");
		expediteurPassword = ((Element)item).getAttribute("Mot_de_passe");
	}
	
	public String getObject() {
		return object;
	}
	public void setObject(String object) {
		this.object = object;
	}
	public String getExpediteur() {
		return expediteur;
	}
	public void setExpediteur(String expediteur) {
		this.expediteur = expediteur;
	}
	public String getFilesPath() {
		return filesPath;
	}
	public void setFilesPath(String filesPath) {
		this.filesPath = filesPath;
	}
}
