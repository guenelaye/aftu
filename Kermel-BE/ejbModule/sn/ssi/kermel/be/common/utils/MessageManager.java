package sn.ssi.kermel.be.common.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;



import com.mxgraph.io.mxCodec;
import com.mxgraph.util.mxUtils;

public class MessageManager {
	
	private static String path="";
	
		
	public static MessageType getMessageType(String messageType) 
	{
		String str="";
		//path = MessageManager.class.getResource("mail_config.xml").getPath();
//		path="/opt/jboss-5.1.0/server/default/deploy/Kermel.ear/Kermel-BE.jar/sn/ssi/kermel/be/common/utils/mail_config.xml";
//		
		if(ToolKermel.isUnix())
			path = BeConstants.path+"/mail_config.xml";
		else
			path = BeConstants.path+"\\\\mail_config.xml";
		MessageType message=null;
		
		try {
				mxCodec codec = new mxCodec();
				Document doc = mxUtils.loadDocument(path);
				NodeList l = doc.getElementsByTagName(messageType);

				if (l.getLength()>0)
					message = new MessageType(l.item(0));

				l = doc.getElementsByTagName("Divers");
				Node item = null;
				if (l.getLength()>0)
				{
					item = l.item(0);
					message.setSmtpSender(((Element)item).getAttribute("smtp_utilisateur"));
					message.setSmtpSenderPassowrd((((Element)item).getAttribute("smtp_mot_de_passe")));
					message.setSmtp_host(((Element)item).getAttribute("smtp_hote"));
					message.setSmtp_port(((Element)item).getAttribute("smtp_port"));
					message.setSmtp_starttls(((Element)item).getAttribute("smtp_starttls"));
					message.setSmtp_auth(((Element)item).getAttribute("smtp_auth"));
				}
				
				message.setBody(str);

			}
			catch (Exception e)
			{
				e.printStackTrace();
			}   

			return message;
	}
	
	public String readFile(File file)
	{
		BufferedReader readerBuffer = null;
        try {
			readerBuffer = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        String str="";
        String line = null;
       try {
		while((line=readerBuffer.readLine()) != null) {
			   str+= line;
		   }
       } catch (IOException e) {
		// TODO Auto-generated catch block
    	   e.printStackTrace();
       }
       return str;
	}
	
	public static int sendMail(String subject, String content, String usermail,String termeref,String nomtermeref, String lettreinvitation,String nomlettreinvitation)
	{
		EmailMessage em;
		int sent=0;
        MessageType msType = getMessageType("Message");
		if (usermail!=null)
		{
			em = new EmailMessage(msType);
			em.setRecipient (usermail);
			em.setSubject(subject);
			em.setMessageContent(content);
			em.setTermeref(termeref);
			em.setNomtermeref(nomtermeref);
			em.setLettreinvitation(lettreinvitation);
			em.setNomlettreinvitation(nomlettreinvitation);
			sent = em.sendMail();
		}
		return sent;
	}
	
	public static int sendMail(String subject, String content, List<String> recepients, String termeref,String nomtermeref, String lettreinvitation,String nomlettreinvitation)
	{
		EmailMessage em;
		int sent=0;
        MessageType msType = getMessageType("Message");
		if (recepients !=null && recepients.size() > 0)
		{
			em = new EmailMessage(msType);
			em.setRecipients(recepients);
			em.setSubject(subject);
			em.setMessageContent(content);
			em.setTermeref(termeref);
			em.setNomtermeref(nomtermeref);
			em.setLettreinvitation(lettreinvitation);
			em.setNomlettreinvitation(nomlettreinvitation);
			sent = em.sendMail();
		}
		return sent;
	}
	
	public static int sendMail(String subject, String content, String usermail) throws Exception
	{
		EmailMessage em;
		int sent=0;
        MessageType msType = getMessageType("Message");
        
        if(msType==null){
        	sent=2;
//				throw new Exception("Le fichier de conf n est pas trouv�");
				
        }
        else if(usermail!=null)
		{
			em = new EmailMessage(msType);
			em.setRecipient(usermail);
			em.setSubject(subject);
			em.setMessageContent(content);

			
			
   
			sent = em.sendSimpleMail();
		}
		return sent;
	}

	
	
	
	
}