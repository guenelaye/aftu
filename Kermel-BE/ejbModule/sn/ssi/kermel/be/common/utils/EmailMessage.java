package sn.ssi.kermel.be.common.utils;

import java.io.File;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;



public class EmailMessage {

	private String recipient;
	private List<String> recipients;
	private String subject;
	private String messageContent;
	private String attachmentFilePathList;
	private String sender;
	private String senderPassword;
	private String senderSMTP;
	private String senderPasswordSMTP;
	private String smtp_host;
	private String smtp_port;
	private String smtp_auth;
	private String smtp_starttls;
	private String termeref;
	private String nomtermeref;
	private String lettreinvitation;
	private String nomlettreinvitation;

   
  	public String getSenderSMTP() {
		return senderSMTP;
	}

	public void setSenderSMTP(String senderSMTP) {
		this.senderSMTP = senderSMTP;
	}

	public String getSenderPasswordSMTP() {
		return senderPasswordSMTP;
	}

	public void setSenderPasswordSMTP(String senderPasswordSMTP) {
		this.senderPasswordSMTP = senderPasswordSMTP;
	}

	public String getSenderPassword() {
		return senderPassword;
	}

	public void setSenderPassword(String senderPassword) {
		this.senderPassword = senderPassword;
	}

	public EmailMessage(MessageType msgType)
	{
		this.subject = msgType.getObject();
		this.messageContent = msgType.getBody();
		this.sender = msgType.getExpediteur();
		this.senderPassword = msgType.getExpediteurPassword();
		this.smtp_host = msgType.getSmtp_host();
		this.smtp_port = msgType.getSmtp_port();
		this.smtp_starttls = msgType.getSmtp_starttls();
		this.smtp_auth = msgType.getSmtp_auth();
		this.senderSMTP = msgType.getSmtpSender();
		this.senderPasswordSMTP = msgType.getSmtpSenderPassowrd();
	}
	
	public EmailMessage(String recipient, String subject, String messageContent,
		String attachmentFilePathList, String sender, String senderPassword) {
		this.recipient = recipient;
		this.subject = subject;
		this.setMessageContent(messageContent);
		this.attachmentFilePathList = attachmentFilePathList;
		this.sender = sender;
		this.senderPassword = senderPassword;  	
	}

	public EmailMessage() {
	}

	public int sendMail()
	{
		Authenticator authenticator = new Authenticator();
		Properties props = System.getProperties();
		props.put("mail.smtp.host", this.smtp_host);
		props.put("mail.smtp.port", this.smtp_port);
		props.put("mail.smtp.starttls.enable", this.smtp_starttls);
		props.put("mail.smtp.auth", this.smtp_auth);
		System.out.println("SSI Message Driven Bean [Envoi notification] "+this.getRecipient()+"-Host:"+smtp_host+"-Port:"+smtp_port+"-"+smtp_auth);
		
		// Setup authentication, get session
		Session session = Session.getInstance(props, authenticator);
		
		// Define message
		Message emailMessage = new MimeMessage(session);
		try {
			 
			emailMessage.setFrom(new InternetAddress(this.getSender()));
			
			if(this.getRecipients() != null && this.getRecipients().size() > 0) {
				InternetAddress[] addrs = new InternetAddress[this.getRecipients().size()];
				int counter = 0;
				for(String mail : this.getRecipients() ) {
					addrs[counter] = new InternetAddress(mail);
				 counter++;
				}
				emailMessage.addRecipients(Message.RecipientType.TO, addrs);
				
			}
			else {
			emailMessage.addRecipient(Message.RecipientType.TO, 
					  					new InternetAddress(this.getRecipient()));
			
			}
			
			emailMessage.setSubject(this.getSubject());

			// Create the message part 
			BodyPart messageBodyPart = new MimeBodyPart();
			
			// Fill the message
			//messageBodyPart.setText(this.getMessageContent());
			messageBodyPart.setContent(this.getMessageContent(), "text/html");
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
						
			// Part two is attachment
			messageBodyPart = new MimeBodyPart();
			File attachmentFile = new File(this.getTermeref());
			DataSource source = new FileDataSource(attachmentFile);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(this.getNomtermeref());
			
			
			// Create the message part 
			BodyPart messageBodyPart2 = new MimeBodyPart();
			
			if(this.getLettreinvitation() != null) {
				messageBodyPart2 = new MimeBodyPart();
			// Fill the message
			//messageBodyPart.setText(this.getMessageContent());
			messageBodyPart2.setContent(this.getMessageContent(), "text/html");
			Multipart multipart2 = new MimeMultipart();
			multipart2.addBodyPart(messageBodyPart2);
			
			
			
			File attachmentFile2 = new File(this.getLettreinvitation());
			DataSource source2 = new FileDataSource(attachmentFile2);
			messageBodyPart2.setDataHandler(new DataHandler(source2));
			messageBodyPart2.setFileName(this.getNomlettreinvitation());
			
			multipart.addBodyPart(messageBodyPart2);
			}
			
			multipart.addBodyPart(messageBodyPart);

			// Put parts in message
			emailMessage.setContent(multipart);
			
			// Send the message
			
			Transport.send(emailMessage);
			
		} catch (AddressException e) {
			e.printStackTrace();
			return 0;

		} catch (MessagingException e) {
			e.printStackTrace();
			return 0;
		}
		return 1;
		
	}
	 
	private class Authenticator extends javax.mail.Authenticator {
		private PasswordAuthentication authentication;

		public Authenticator() {
			authentication = new PasswordAuthentication(senderSMTP, senderPasswordSMTP);
			System.out.println(senderSMTP+"-"+senderPasswordSMTP);
		}

		protected PasswordAuthentication getPasswordAuthentication() {
			return authentication;
		}
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getAttachmentFilePathList() {
		return attachmentFilePathList;
	}

	public void setAttachmentFilePathList(String attachmentFilePathList) {
		this.attachmentFilePathList = attachmentFilePathList;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public String getTermeref() {
		return termeref;
	}
	public void setTermeref(String termeref) {
		this.termeref = termeref;
	}

	public String getNomtermeref() {
		return nomtermeref;
	}
	public void setNomtermeref(String nomtermeref) {
		this.nomtermeref = nomtermeref;
	}

	public String getLettreinvitation() {
		return lettreinvitation;
	}
	public void setLettreinvitation(String lettreinvitation) {
		this.lettreinvitation = lettreinvitation;
	}

	public String getNomlettreinvitation() {
		return nomlettreinvitation;
	}
	public void setNomlettreinvitation(String nomlettreinvitation) {
		this.nomlettreinvitation = nomlettreinvitation;
	}

	public List<String> getRecipients() {
		return recipients;
	}

	public void setRecipients(List<String> recipients) {
		this.recipients = recipients;
	}

	public int sendSimpleMail() 
	{
		Authenticator authenticator = new Authenticator();
		Properties props = System.getProperties();
		props.put("mail.smtp.host", this.smtp_host);
		props.put("mail.smtp.port", this.smtp_port);
		props.put("mail.smtp.starttls.enable", this.smtp_starttls);
		props.put("mail.smtp.auth", this.smtp_auth);
		System.out.println("SSI Message Driven Bean [Envoi notification] "+this.getRecipient()+"-Host:"+smtp_host+"-Port:"+smtp_port+"-"+smtp_auth);
		
		// Setup authentication, get session
		Session session = Session.getInstance(props, authenticator);
		
		// Define message
		Message emailMessage = new MimeMessage(session);
		try {
			 
			emailMessage.setFrom(new InternetAddress(this.getSender()));
			emailMessage.addRecipient(Message.RecipientType.TO, 
					  					new InternetAddress(this.getRecipient()));
			emailMessage.setSubject(this.getSubject());

			// Create the message part 
			BodyPart messageBodyPart = new MimeBodyPart();
			// Fill the message
			messageBodyPart.setContent(this.getMessageContent(), "text/html");
			
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			
			emailMessage.setContent(multipart);
			
			
			
						
			// Part two is attachment
			messageBodyPart = new MimeBodyPart();
			
			

			// Put parts in message
			
			
			
			// Send the message
			Transport tr = session.getTransport("smtp");
			tr.connect(smtp_host, sender, senderPasswordSMTP);
			
			emailMessage.saveChanges();
			 
			tr.sendMessage(emailMessage, emailMessage.getAllRecipients());
			tr.close();
			//Transport.send(emailMessage);
			
		} catch (AddressException e) {
			e.printStackTrace();
			return 0;

		} catch (MessagingException e) {
			e.printStackTrace();
			return 0;
		}
		return 1;
		
	}

	
	
	
}