package sn.ssi.kermel.be.common.utils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

public class BeanLocator {
	public static <T> T lookup(final Class<T> clazz, final String jndiName) {
		final Object bean = lookup(jndiName);
		return clazz.cast(PortableRemoteObject.narrow(bean, clazz));
	}

	/* Jboss 5*/
//	public static <T> T defaultLookup(final Class<T> clazz) {
//		final Object bean = lookup("Kermel/" + clazz.getSimpleName()
//				+ "Bean/remote");
//		return clazz.cast(PortableRemoteObject.narrow(bean, clazz));
//	}
	
//	public static <T> T defaultLookup(Class<T> clazz) {
//	Object bean = lookup("ejb/Kermel/Kermel-BE.jar/" + clazz.getSimpleName()
//			+ "Bean" + "#" + clazz.getName());
//	return clazz.cast(PortableRemoteObject.narrow(bean, clazz));
//}
	
	/* Jboss 7*/
	public static <T> T defaultLookup(final Class<T> clazz) {
		 final String appName = "Kermel";
		 final String moduleName = "Kermel-BE";
		 final String distinctName = "";
		 // The EJB name which by default is the simple class name of the bean implementation class
	        final String beanName = clazz.getSimpleName()+"Bean";
	        // the remote view fully qualified class name
	        final String viewClassName = clazz.getName();
	        
//		final Object bean = lookup("ejb:"+appName+"/"+moduleName+"/"+distinctName+"/" + beanName+"!"+viewClassName);
//				+ "Bean!"+clazz.getSimpleName());
	    	final Object bean = lookup("ejb:"+appName+"/"+moduleName+"/"+distinctName+"/"+beanName+"!"+viewClassName);
		return clazz.cast(PortableRemoteObject.narrow(bean, clazz));
		
	}
//
	public static Object lookup(final String jndiName) {
		Context context = null;
		try {
			context = new InitialContext();
			return context.lookup(jndiName);
		} catch (final NamingException ex) {
			throw new IllegalStateException("...");
		} finally {
			try {
				context.close();
			} catch (final NamingException ex) {
				throw new IllegalStateException("...");
			}
		}
	}
}