/*
 * Crï¿½ï¿½ le 3 aoï¿½t 2005
 *
 * Pour changer le modï¿½le de ce fichier gï¿½nï¿½rï¿½, allez ï¿½ :
 * Fenï¿½tre&gt;Prï¿½fï¿½rences&gt;Java&gt;Gï¿½nï¿½ration de code&gt;Code et commentaires
 */
package sn.ssi.kermel.be.common.utils;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ibm.icu.text.RuleBasedNumberFormat;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.RegularExpression;
//import com.sun.org.apache.xerces.internal.impl.xpath.regex.RegularExpression;

import java.util.Locale;

import sn.ssi.kermel.be.entity.SysParametresGeneraux;
import sn.ssi.kermel.be.referentiel.ejb.ParametresGenerauxSession;

//import org.apache.xerces.impl.xpath.regex.RegularExpression;

//import com.sun.org.apache.xerces.internal.impl.xpath.regex.RegularExpression;

/**
 * @author EGTALL
 * 
 *         Pour changer le modï¿½le de ce commentaire de type gï¿½nï¿½rï¿½, allez ï¿½ :
 *         Fenï¿½tre&gt;Prï¿½fï¿½rences&gt;Java&gt;Gï¿½nï¿½ration de code&gt;Code et
 *         commentaires
 */
public class ToolKermel {

	/**
	 * Cette mï¿½thode permet de retourner un code couleur en fonction de la
	 * valeur d'un boolï¿½en
	 * 
	 * @param parite
	 *            <b>boolean</b>
	 * @param classe1
	 *            <b>String</b>
	 * @param classe2
	 *            <b>String</b>
	 * @return
	 */
	public static String getCouleur(final boolean parite, final String classe1,
			final String classe2) {
		if (parite)
			return classe1;
		else
			return classe2;
	}

	/**
	 * Mï¿½thode permettant de transformer une chaine de la forme DD/MM/YYYY en
	 * une date.
	 * 
	 * @param sVal
	 *            <b>String</b>
	 * @return <b>Date</b>
	 */
	public static Date stringToDate(final String sVal) {

		final SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try {
			return df.parse(sVal);
		} catch (final ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Date stringToHeure(final String sVal) {

		final SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
		try {
			return df.parse(sVal);
		} catch (final ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Date stringToDateTiret(final String sVal) {

		final SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		try {
			return df.parse(sVal);
		} catch (final ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Date stringToDateTirets(final String sVal) {

		final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return df.parse(sVal);
		} catch (final ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String formatDecimal(final double nbre) {
		final DecimalFormatSymbols symbole = new DecimalFormatSymbols();
		symbole.setDecimalSeparator(',');
		symbole.setGroupingSeparator(' ');
		final DecimalFormat form = new DecimalFormat("###,###,###,##0.##",
				symbole);
		final String sNombre = form.format(nbre);
		return sNombre;
	}

	public static double formatChaine(final String nbre) throws ParseException {
		final DecimalFormatSymbols symbole = new DecimalFormatSymbols();
		symbole.setDecimalSeparator(',');
		symbole.setGroupingSeparator(' ');
		final DecimalFormat form = new DecimalFormat("###,###,###,##0.##",
				symbole);
		final double sNombre = form.parse(nbre).doubleValue();
		return sNombre;
	}

	public static double doubleToFranc(final double nbre) {
		double franc = round(nbre, 0);
		final String chaine = Double.toString(franc);
		final String c = chaine.substring(chaine.length() - 3,
				chaine.length() - 2);
		final int n = Integer.parseInt(c);
		if (n > 5) {
			franc = franc + (10 - n);
		}
		if ((n > 0) && (n < 5)) {
			franc = franc + (5 - n);
		}

		return franc;
	}

	public static double round(final double what, final int howmuch) {
		return (int) (what * Math.pow(10, howmuch) + .5)
				/ Math.pow(10, howmuch);
	}

	public static String format1Decimal(final double nbre) {
		final DecimalFormatSymbols symbole = new DecimalFormatSymbols();
		symbole.setDecimalSeparator(',');
		symbole.setGroupingSeparator(' ');
		final DecimalFormat form = new DecimalFormat("###,###,###,##0.0",
				symbole);
		final String sNombre = form.format(nbre);
		return sNombre;
	}

	public static String format2Decimal(final double nbre) {
		final DecimalFormatSymbols symbole = new DecimalFormatSymbols();
		symbole.setDecimalSeparator(',');
		symbole.setGroupingSeparator(' ');
		final DecimalFormat form = new DecimalFormat("###,###,###,##0.00",
				symbole);
		final String sNombre = form.format(nbre);
		return sNombre;
	}

	public static String format2Decimal(final BigDecimal nbre) {
		final DecimalFormatSymbols symbole = new DecimalFormatSymbols();
		symbole.setDecimalSeparator(',');
		symbole.setGroupingSeparator(' ');
		final DecimalFormat form = new DecimalFormat("###,###,###,###", symbole);
		final String sNombre = form.format(nbre);
		return sNombre;
	}

	public static String format3Decimal(final BigDecimal nbre) {
		final DecimalFormatSymbols symbole = new DecimalFormatSymbols();
		symbole.setDecimalSeparator(',');
		symbole.setGroupingSeparator(' ');
		final DecimalFormat form = new DecimalFormat("###,###,###,##0.00",
				symbole);
		final String sNombre = form.format(nbre);
		return sNombre;
	}

	public static double formatStringToDouble(String ch) {
		final String[] Tch = ch.split(" ");
		String sch = "";
		for (final String element : Tch) {
			sch = sch + element;
		}
		ch = sch;
		final String chaine = ch.replace(',', '.');
		return Double.parseDouble(chaine);
	}

	@SuppressWarnings("deprecation")
	public static Date dateDuJour() {
		final Date laDate = new Date();
		laDate.setHours(0);
		laDate.setMinutes(0);
		laDate.setSeconds(0);
		return laDate;
	}

	@SuppressWarnings("deprecation")
	public static Date PremierJourDelaSemaine() {
		final Calendar calendar = new GregorianCalendar();
		Date laDate = new Date();
		calendar.set(Integer.parseInt(getAnneeCourante()), laDate.getMonth(),
				laDate.getDay());
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		laDate = calendar.getTime();
		// System.out.println(laDate);
		return laDate;
	}

	@SuppressWarnings("deprecation")
	public static Date PremierJourDuMois() {
		final Calendar calendar = new GregorianCalendar();
		Date laDate = new Date();
		calendar.set(Integer.parseInt(getAnneeCourante()), laDate.getMonth(),
				laDate.getDay());
		calendar.set(Calendar.DAY_OF_MONTH,
				calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		laDate = calendar.getTime();
		System.out.println(laDate);
		return laDate;
	}

	@SuppressWarnings("deprecation")
	public static Date DernierJourDelaSemaine() {
		final Calendar calendar = new GregorianCalendar();
		Date laDate = new Date();
		calendar.set(Integer.parseInt(getAnneeCourante()), laDate.getMonth(),
				laDate.getDay());
		calendar.set(Calendar.DAY_OF_MONTH,
				calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		laDate = calendar.getTime();
		System.out.println(laDate);

		return laDate;
	}

	@SuppressWarnings("deprecation")
	public static Date DernierJourDuMois() {
		final Calendar calendar = new GregorianCalendar();
		Date laDate = new Date();
		calendar.set(Integer.parseInt(getAnneeCourante()), laDate.getMonth(),
				laDate.getDay());
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
		laDate = calendar.getTime();
		// System.out.println(laDate);

		return laDate;
	}

	public static String getAnneeCourante() {
		final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		final String dat = dateFormat.format(new Date());
		return dat.substring(6, 10);
	}

	public static String getMoisCourant(Date date) {
		final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		final String dat = dateFormat.format(date);
		return dat.substring(3, 5);
	}

	public static String dateToStringSql(final Date date) {
		final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String sDate = dateFormat.format(date);
		final String jour = sDate.substring(0, 2);
		final String mois = sDate.substring(3, 5);
		final String annee = sDate.substring(6, 10);
		sDate = annee + "-" + mois + "-" + jour;
		return sDate;
	}

	public static String StringDateToStringSql(String sDate) {
		final String jour = sDate.substring(0, 2);
		final String mois = sDate.substring(3, 5);
		final String annee = sDate.substring(6, 10);
		sDate = annee + "-" + mois + "-" + jour;
		return sDate;
	}

	public static boolean estReel(final String string) {

		/** Tester si string est un rï¿½el **/
		try {
			Float.parseFloat(string);
		} catch (final NumberFormatException e) {
			return false;
		}
		return true;
		/** Fin Tester si string est un rï¿½el **/
	}

	public static boolean estLong(final String string) {

		try {
			Long.parseLong(string);
		} catch (final NumberFormatException e) {
			return false;
		}
		return true;
		/** Fin Tester si string est un rï¿½el **/
	}

	public static String dateToString(final Date laDate) {
		final SimpleDateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
		if (laDate != null)
			return simple.format(laDate);
		else
			return "";
	}

	public static String getStatut(final String statut) {
		String libelle = "";
		int s;
		s = Integer.parseInt(statut);
		switch (s) {
		case 1:
			libelle = "Validï¿½";
			break;
		case 2:
			libelle = "Saisie";
			break;
		case 3:
			libelle = "Financï¿½";
			break;

		}

		return libelle;
	}

	public static Date getCourantEch(final Date datepremiereech,
			final int periode) {
		long dateEche;
		Date courantEche;
		dateEche = datepremiereech.getTime() + periode * 30 * 1000 * 3600 * 24;
		courantEche = new Date(dateEche);
		return courantEche;
	}

	public static String convertIntToString(final int intToString) {
		int valeur = intToString;
		String result = "";

		if (valeur < 0) {
			result = "-";
			valeur = -1 * valeur; // On le met en positif
		}

		// Nous allons commencer par convertir les chiffres supï¿½rieure ï¿½ 1
		// 000
		// 000 000
		if (valeur > 1000000000) {
			final int valeurSupMilliard = valeur / 1000000000;
			result = result + " " + convertIntToString(valeurSupMilliard)
					+ " milliard";
			valeur = valeur - valeurSupMilliard * 1000000000;
		}

		// Nous allons convertir les chifres entre 1 000 000 et 999 999 999
		if (valeur > 1000000) {
			final int valeurMillion = valeur / 1000000;
			result = result + " " + convertIntToString(valeurMillion)
					+ " million";
			valeur = valeur - valeurMillion * 1000000;
		}

		// Nous allons convertir les chiffres entre 1000 et 999 999
		if (valeur > 1000) {
			final int valeurMille = valeur / 1000;

			if (valeurMille > 1) {
				result = result + " " + convertIntToString(valeurMille)
						+ " mille";
			} else {
				result = result + " mille";
			}

			valeur = valeur - valeurMille * 1000;
		}

		// Nous allons convertir les chiffres entre 100 et 999
		if (valeur > 100) {
			final int valeurCent = valeur / 100;

			if (valeurCent > 1) {
				result = result + " " + convertIntToString(valeurCent)
						+ " cent";
			} else {
				result = result + " cent";
			}

			valeur = valeur - valeurCent * 100;
		}

		// Nous allons convertir les chiffres entre 0 et 99
		switch (valeur) {
		case 0:
			if ((result == "") || (result == "-")) {
				result = "zï¿½ro";
			}
			break;
		case 1:
			result = result + " un";
			break;
		case 2:
			result = result + " deux";
			break;
		case 3:
			result = result + " trois";
			break;
		case 4:
			result = result + " quatre";
			break;
		case 5:
			result = result + " cinq";
			break;
		case 6:
			result = result + " six";
			break;
		case 7:
			result = result + " sept";
			break;
		case 8:
			result = result + " huit";
			break;
		case 9:
			result = result + " neuf";
			break;
		case 10:
			result = result + " dix";
			break;
		case 11:
			result = result + " onze";
			break;
		case 12:
			result = result + " douze";
			break;
		case 13:
			result = result + " treize";
			break;
		case 14:
			result = result + " quatorze";
			break;
		case 15:
			result = result + " quinze";
			break;
		case 16:
			result = result + " seize";
			break;
		case 17:
			result = result + " dix-sept";
			break;
		case 18:
			result = result + " dix-huit";
			break;
		case 19:
			result = result + " dix-neuf";
			break;
		case 20:
			result = result + " vingt";
			break;
		case 30:
			result = result + " trente";
			break;
		case 40:
			result = result + " quarante";
			break;
		case 50:
			result = result + " cinquante";
			break;
		case 60:
			result = result + " soixante";
			break;
		case 70:
			result = result + " soixante-dix";
			break;
		case 80:
			result = result + " quatre-vingts";
			break;
		case 81:
			result = result + " quatre-vingts un";
			break;
		case 90:
			result = result + " quatre-vingts-dix";
			break;
		case 91:
			result = result + " quatre-vingts onze";
			break;
		default:
			int valeurDizaine = valeur / 10 * 10;
			int valeurUnite = valeur % 10;

			// Les valeurs entre 16 et 20, 70 et 80, et entre 90 et 100 sont des
			// cas
			// particuliers, nous devons faire un triatment particulier
			if (((valeur > 70) && (valeur < 80))
					|| ((valeur > 90) && (valeur < 100))) {
				valeurDizaine -= 10;
				valeurUnite += 10;
			}

			if (valeurDizaine == 0) {
				result = result + " " + convertIntToString(valeurUnite);
			} else if (valeurUnite == 0) {
				result = result + " " + convertIntToString(valeurDizaine);
			} else if (valeurUnite == 1) {
				result = result + " " + convertIntToString(valeurDizaine)
						+ " et " + convertIntToString(valeurUnite);
			} else {
				result = result + " " + convertIntToString(valeurDizaine) + "-"
						+ convertIntToString(valeurUnite);
			}
		}

		return result.trim();
	}

	public static Date dateDecalageJour(final Date date, final int delai) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, delai);
		return cal.getTime();
	}

	public static Date dateDecalageMois(final Date date, final int delai) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, delai);
		return cal.getTime();
	}

	public static long DifferenceDate(final Date date1, final Date date2,
			final String r) {
		long resultat = 0;
		long msjour = 0;
		long heure = 0;

		if (r == "ss") {
			msjour = date1.getTime() - date2.getTime();
			resultat = Math.abs(msjour) / ConstKermel.s_to_ms;

		}
		if (r == "hh") {
			msjour = date1.getTime() - date2.getTime();
			heure = Math.abs(msjour) / ConstKermel.heure_to_ms;
			resultat = heure;
		}
		if (r == "jj") {
			msjour = date1.getTime() - date2.getTime();
			heure = Math.abs(msjour) / ConstKermel.heure_to_ms;
			resultat = heure / ConstKermel.jour_to_heure;
		}

		return resultat;
	}

	public static String getEncodedPassword(final String key)
			throws IOException {
		final byte[] uniqueKey = key.getBytes();
		byte[] hash = null;
		try {
			hash = MessageDigest.getInstance("MD5").digest(uniqueKey);
		} catch (final NoSuchAlgorithmException e) {
			throw new Error("no MD5 support in this VM");
		}
		final StringBuffer hashString = new StringBuffer();
		for (int i = 0; i < hash.length; ++i) {
			final String hex = Integer.toHexString(hash[i]);
			if (hex.length() == 1) {
				hashString.append('0');
				hashString.append(hex.charAt(hex.length() - 1));
			} else {
				hashString.append(hex.substring(hex.length() - 2));
			}
		}
		return hashString.toString();
	}

	public static boolean ControleValiditeDate(final String date) {
		boolean correct = false;
		// Date au format :dd/MM/yyyy a verifier
		final RegularExpression reg = new RegularExpression(
				"^[0-9]{2}[/]{1}[0-9]{2}[/]{1}[0-9]{4}$");
		if (reg.matches(date)) {
			correct = true;
		}
		return correct;
	}

	public static boolean ControleValiditeHeure(final String date) {
		boolean correct = false;
		// Date au format :dd/MM/yyyy a verifier
		final RegularExpression reg = new RegularExpression(
				"^[0-9]{2}[:]{1}[0-9]{2}$");
		if (reg.matches(date)) {
			correct = true;
		}
		return correct;
	}

	public static boolean ControlValidateEmail(final String mail) {
		boolean juste = false;

		
		final RegularExpression reg = new RegularExpression(
				"^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$");
		if (reg.matches(mail)) {
			juste = true;
		}
		return juste;

	}

	public static boolean checkDate(String date, boolean isEnglish) {
		String monthExpression = "[0-1][1-2]";
		String dayExpression = "(0[1-9]|[12][0-9]|3[01])";
		boolean isValid = false;
		// RegEx to validate date in US format.
		String expression = "^" + monthExpression + "[- / ]?" + dayExpression
				+ "[- /]?(18|19|20|21)\\d{2}";
		if (isEnglish) {
			// RegEx to validate date in Metric format.
			expression = "^" + dayExpression + "[- / ]?" + monthExpression
					+ "[- /]?(18|19|20|21)\\d{2,4}";
		}
		CharSequence inputStr = date;
		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	public static String formatFr(String date) {
		int nbr = date.length();
		if (nbr < 8) {
			date = "01011870";
		}
		String jour = date.substring(0, 2);
		String mois = date.substring(2, 4);
		String an = date.substring(4, 8);
		String dt = jour + "/" + mois + "/" + an;
		return dt;
	}

	public static boolean isWindows() {

		String os = System.getProperty("os.name").toLowerCase();
		// windows
		return (os.indexOf("win") >= 0);

	}

	public static boolean isUnix() {

		String os = System.getProperty("os.name").toLowerCase();
		// linux or unix
		return (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0);

	}

	public static Date stringToDateSQL(final String sVal) {

		final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return df.parse(sVal);
		} catch (final ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static DecimalFormat formatDecimal(final BigDecimal nbre) {
		final DecimalFormatSymbols symbole = new DecimalFormatSymbols();
		symbole.setDecimalSeparator(',');
		symbole.setGroupingSeparator(' ');
		final DecimalFormat form = new DecimalFormat("###,###,###,###", symbole);
		final String sNombre = form.format(nbre);
		return form;
	}

	public static Date getCourantEch3(Date datepremiereech, long periode) {
		long dateEche;
		Date courantEche;
		dateEche = datepremiereech.getTime() + periode * 1000 * 3600 * 24;
		courantEche = new Date(dateEche);
		return courantEche;
	}

	public static String conversionChiffreLettre(BigDecimal valeur) {
		/**
		 * Spellout rules for French. French adds some interesting quirks of its
		 * own: 1) The word "et" is interposed between the tens and ones digits,
		 * but only if the ones digit if 1: 20 is "vingt," and 2 is
		 * "vingt-deux," but 21 is "vingt-et-un." 2) There are no words for 70,
		 * 80, or 90. "quatre-vingts" ("four twenties") is used for 80, and
		 * values proceed by score from 60 to 99 (e.g., 73 is "soixante-treize"
		 * ["sixty-thirteen"]). Numbers from 1,100 to 1,199 are rendered as
		 * hundreds rather than thousands: 1,100 is "onze cents"
		 * ("eleven hundred"), rather than "mille cent"
		 * ("one thousand one hundred")
		 */
		String french =
		// the main rule set
		"%main:\n"
				// negative-number and fraction rules
				+ "    -x: moins >>;\n"
				+ "    x.x: << virgule >>;\n"
				// words for numbers from 0 to 10
				+ "    z\u00e9ro; un; deux; trois; quatre; cinq; six; sept; huit; neuf;\n"
				+ "    dix; onze; douze; treize; quatorze; quinze; seize;\n"
				+ "        dix-sept; dix-huit; dix-neuf;\n"
				// ords for the s of 10: %%alt-ones inserts "et"
				// when needed
				+ "    20: vingt[->%%alt-ones>];\n"
				+ "    30: trente[->%%alt-ones>];\n"
				+ "    40: quarante[->%%alt-ones>];\n"
				+ "    50: cinquante[->%%alt-ones>];\n"
				// rule for 60. The /20 causes this rule's multiplier to be
				// 20 rather than 10, allowinhg us to recurse for all values
				// from 60 to 79...
				+ "    60/20: soixante[->%%alt-ones>];\n"
				// ...except for 71, which must be special-cased
				+ "    71: soixante et onze;\n"
				// at 72, we have to repeat the rule for 60 to get us to 79
				+ "    72/20: soixante->%%alt-ones>;\n"
				// at 80, we state a new rule with the phrase for 80. Since
				// it changes form when there's a ones digit, we need a second
				// rule at 81. This rule also includes "/20," allowing it to
				// be used correctly for all values up to 99
				+ "    80: quatre-vingts; 81/20: quatre-vingt->>;\n"
				// "cent" becomes plural when preceded by a multiplier, and
				// the multiplier is omitted from the singular form
				+ "    100: cent[ >>];\n"
				+ "    200: << cents;\n"
				+ "    201: << cent[ >>];\n"
				+ "    300: << cents;\n"
				+ "    301: << cent[ >>];\n"
				+ "    400: << cents;\n"
				+ "    401: << cent[ >>];\n"
				+ "    500: << cents;\n"
				+ "    501: << cent[ >>];\n"
				+ "    600: << cents;\n"
				+ "    601: << cent[ >>];\n"
				+ "    700: << cents;\n"
				+ "    701: << cent[ >>];\n"
				+ "    800: << cents;\n"
				+ "    801: << cent[ >>];\n"
				+ "    900: << cents;\n"
				+ "    901: << cent[ >>];\n"
				+ "    1000: mille[ >>];\n"
				// values from 1,100 to 1,199 are rendered as "onze cents..."
				// instead of "mille cent..." The > after "1000" decreases
				// the rule's exponent, causing its multiplier to be 100 instead
				// of 1,000. This prevents us from getting "onze cents cent
				// vingt-deux" ("eleven hundred one hundred twenty-two").
				+ "    1100>: onze cents[ >>];\n"
				// at 1,200, we go back to formating in thousands, so we
				// repeat the rule for 1,000
				+ "    1200: mille >>;\n"
				// at 2,000, the multiplier is added
				+ "    2000: << mille[ >>];\n"
				+ "    1,000,000: << million[ >>];\n"
				+ "    2,000,000: << millions[ >>];\n"
				+ "    1,000,000,000: << milliard[ >>];\n"
				+ "    2,000,000,000: << milliards[ >>];\n"
				+ "    1,000,000,000,000: << billion[ >>];\n"
				+ "    2,000,000,000,000: << billions[ >>];\n"
				+ "    1,000,000,000,000,000: =#,##0=;\n"
				// %%alt-ones is used to insert "et" when the ones digit is 1
				+ "%%alt-ones:\n" + "    ; et-un; =%main=;";

		System.out.println(new RuleBasedNumberFormat(french, Locale.FRANCE)
				.format(valeur));

		return new RuleBasedNumberFormat(french, Locale.FRANCE).format(valeur);
	}

	/**
	 * Hash md5
	 * 
	 */

	public static String StringToHash(String text) {
		String hashedText = "";
		try {
			MessageDigest m = MessageDigest.getInstance("MD5");
			m.reset();
			m.update(text.getBytes());
			byte[] digest = m.digest();
			BigInteger bigInt = new BigInteger(1, digest);
			hashedText = bigInt.toString(16);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return hashedText;
	}

	public static String formatPath(String path) {
		String formattedPath = "";
		if (ToolKermel.isWindows())
			formattedPath = path.replaceAll("/", "\\\\");
		else
			formattedPath = path.replaceAll("\\\\", "/");

		return formattedPath;

	}

	/*
	 * RÃ©cuáº¿ration des paramÃ¨tres gÃ©nÃ©raux
	 */
	public static String getParametre(String code) {

		SysParametresGeneraux param = BeanLocator.defaultLookup(
				ParametresGenerauxSession.class).findById(code);

		if (param != null && param.getLibelle() != null)
			return param.getLibelle();

		return null;

	}

	public static String checkDir(String pathAccuse) {
		String cheminFinal = pathAccuse;
		if (isWindows()) {
			cheminFinal = cheminFinal.replace("/", "\\\\");
			cheminFinal = cheminFinal.replace("\\", "\\\\");
		} else {
			cheminFinal = cheminFinal.replace("\\\\", "/");
			cheminFinal = cheminFinal.replace("\\", "/");

		}
		File dir = new File(cheminFinal);
		if(!dir.exists()) 
			dir.mkdirs();
		

		return cheminFinal;
		
	}
	
	
	public static String generateHash() {
		String  hash= "" ;              		
		Random rand = new Random();
		int ran = rand.nextInt();

		String chars = "abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
		StringBuffer pass = new StringBuffer();
		for (int x = 0; x < 8; x++) {
		int t = (int) Math.floor(Math.random() * (chars.length() - 1));
		pass.append(chars.charAt(t));
		}
		// mot = pass;
		//generer++;
		hash =pass.toString();
		
		return hash;
		
	}
	
	public static String format4Decimal(final Double nbre) {
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(3); // arrondi � 2 chiffres apres la
										// virgules
		df.setMinimumFractionDigits(3);
		df.setDecimalSeparatorAlwaysShown(true);

		final String sNombre = df.format(nbre);
		sNombre.replaceAll(" ", ".");

		return sNombre;
	}


}
