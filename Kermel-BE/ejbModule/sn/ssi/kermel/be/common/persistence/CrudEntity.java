package sn.ssi.kermel.be.common.persistence;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;

import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public abstract class CrudEntity<E extends Entity> extends AbstractSessionBean {

	protected Class<E> persistentClass;

	@SuppressWarnings("unchecked")
	public List<E> find(final int start, final int step) {
		Criteria criteria = getHibernateSession().createCriteria(
				persistentClass).setResultTransformer(
				CriteriaSpecification.DISTINCT_ROOT_ENTITY).setFirstResult(
				start);
		if (step > 0)
			criteria.setMaxResults(step);

		return criteria.list();

	}

	public void saveOrUpdate(final Entity entity) {
		getHibernateSession().saveOrUpdate(entity);
	}

	@SuppressWarnings("unchecked")
	public void delete(final int id) {
		Session session = getHibernateSession();
		E entity = (E) session.get(persistentClass, id);
		session.delete(entity);
	}

	@SuppressWarnings("unchecked")
	public List<E> find(final int start, final int step,
			final String orderPropertyName) {
		Criteria criteria = getHibernateSession().createCriteria(
				persistentClass).setResultTransformer(
				CriteriaSpecification.DISTINCT_ROOT_ENTITY).setFirstResult(
				start).addOrder(Order.desc(orderPropertyName));
		if (step > 0)
			criteria.setMaxResults(step);

		return criteria.list();

	}

	public int countAll() {
		Criteria criteria = getHibernateSession().createCriteria(
				persistentClass);
		return (Integer) criteria.setProjection(Projections.rowCount())
				.uniqueResult();
	}
}
