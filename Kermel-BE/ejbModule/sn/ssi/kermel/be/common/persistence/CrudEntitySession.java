package sn.ssi.kermel.be.common.persistence;

import java.util.List;

/**
 * @author alex Il s'agit juste d'un marqueur
 */
public interface CrudEntitySession<E> {

	public E findByCode(final String code);

	public void mergeEntity(Entity entity);

	public List<E> findAll();

	public List<E> find(final int start, final int step);

	public E delete(String code);

	public E saveOrUpdate(final Entity entity);

	public E saveOrUpdate(final Entity entity, final String paramGal);

	List<E> find(final int start, final int step, final String orderPropertyName);

	public int countAll();

	public void setParametresGenerauxSession();

}