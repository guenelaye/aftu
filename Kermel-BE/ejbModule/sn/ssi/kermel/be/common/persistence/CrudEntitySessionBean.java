package sn.ssi.kermel.be.common.persistence;

import java.util.Calendar;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;

import sn.ssi.kermel.be.referentiel.ejb.ParametresGenerauxSession;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public abstract class CrudEntitySessionBean<E extends Entity> extends AbstractSessionBean implements CrudEntitySession<E> {

	protected Class<E> persistentClass;
	protected ParametresGenerauxSession paramGenerauxSession;

	@SuppressWarnings("unchecked")
	public List<E> find(final int start, final int step) {
		Criteria criteria = getHibernateSession().createCriteria(persistentClass).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).setFirstResult(start);
		if (step > 0)
			criteria.setMaxResults(step);

		return criteria.list();

	}

	public void mergeEntity(final Entity entity) {
		getHibernateSession().merge(entity);
	}

	@SuppressWarnings("unchecked")
	public E saveOrUpdate(final Entity entity) {
		getHibernateSession().saveOrUpdate(entity);
		return (E) entity;
	}

	@SuppressWarnings("unchecked")
	public E saveOrUpdate(final Entity entity, final String paramGal) {
		if (paramGal != null && entity.getCode() == null) {
			entity.setCode(getGeneratedCode(paramGal));
			paramGenerauxSession.incrementeCode(paramGal);
			getHibernateSession().saveOrUpdate(entity);
		} else
			getHibernateSession().saveOrUpdate(entity);

		return (E) entity;
	}

	@SuppressWarnings("unchecked")
	public E delete(final String code) {
		Session session = getHibernateSession();
		E entity = (E) session.get(persistentClass, code);
		session.delete(entity);
		return entity;
	}

	@SuppressWarnings("unchecked")
	public E findByCode(final String code) {
		return (E) getHibernateSession().get(persistentClass, code);
	}

	@SuppressWarnings("unchecked")
	public List<E> find(final int start, final int step, final String orderPropertyName) {
		Criteria criteria = getHibernateSession().createCriteria(persistentClass).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).setFirstResult(start).addOrder(
				Order.desc(orderPropertyName));
		if (step > 0)
			criteria.setMaxResults(step);

		return criteria.list();

	}

	public int countAll() {
		Criteria criteria = getHibernateSession().createCriteria(persistentClass);
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	public String getGeneratedCode(final String codeParametreGeneral) {
		String dateCoupee = Calendar.getInstance().getTime().toString();
		dateCoupee = dateCoupee.substring(dateCoupee.length() - 4, dateCoupee.length());
		// System.out.println(codeParametreGeneral + "oooo");
		Long codeParametre = paramGenerauxSession.getValeurParametre(codeParametreGeneral);
		if (codeParametre < 10)
			return dateCoupee + "_00" + codeParametre;
		else if (codeParametre >= 10 && codeParametre < 100)
			return dateCoupee + "_0" + codeParametre;
		else
			return dateCoupee + "_" + codeParametre;
	}

	@SuppressWarnings("unchecked")
	public List<E> findAll() {
		return getHibernateSession().createCriteria(persistentClass).list();
	}

}
