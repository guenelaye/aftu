package sn.ssi.kermel.be.common.persistence;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * @author alex Sert de marqueur même si les attributs ne sont pas utilisés
 */
@SuppressWarnings("serial")
@MappedSuperclass
public abstract class Entity implements java.io.Serializable {

	protected String code;

	@Id
	@Column(name = "CODE", nullable = false, length = 255)
	public String getCode() {
		return code;
	}

	public void setCode(final String code) {
		this.code = code;
	}
}
