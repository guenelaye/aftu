package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;


import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygFormationBail;
import sn.ssi.kermel.be.entity.SygProformation;





@Remote
public interface BailleurFormationSession {
	public void save(SygFormationBail fbail);

	public void delete(Integer id);

	public List<SygFormationBail> find(int indice, int pas);

	public void update(SygFormationBail fbail);

	public List<SygFormationBail> findAll();

	public List<SygFormationBail> find(int indice, int pas,
			 SygProformation formation, SygBailleurs bailleur);
	public int count(SygProformation formation, SygBailleurs bailleur);
	SygFormationBail findByCode(Integer id);
	
	public List<SygFormationBail> finByMontan(Long contBaill);

	public Long getMontantTotal();
}
