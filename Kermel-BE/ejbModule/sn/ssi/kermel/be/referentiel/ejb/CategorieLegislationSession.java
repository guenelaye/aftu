package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCodeMarche;
import sn.ssi.kermel.be.entity.SygRapportARMP;
import sn.ssi.kermel.be.entity.SygCategorieLegislation;

@Remote
public interface CategorieLegislationSession {
	
	public void save(SygCategorieLegislation rapportarmp);
	
	public void delete(Long id);
	
	public List<SygCategorieLegislation> find(int indice, int pas,Long code,String libelle);
	
	public int count(Long code,String libelle);
	
	public void update(SygCategorieLegislation rapportarmp);
	
	public SygCategorieLegislation findById(Long code);
	
}
