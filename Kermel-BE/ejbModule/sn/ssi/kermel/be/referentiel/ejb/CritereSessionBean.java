package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCritere;
import sn.ssi.kermel.be.entity.SygDossierspieces;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;
@Stateless
public class CritereSessionBean  extends AbstractSessionBean implements CritereSession{
	
	
	
	@Override
	public int count(Long id, String libelle,SygAutoriteContractante autorite) {
		Criteria criteria= getHibernateSession().createCriteria(SygCritere.class);
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		if(id!=null){
			criteria.add(Restrictions.ilike("id",id));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(autorite!=null){
			criteria.add(Restrictions.or(Restrictions.eq("autorite", autorite), Restrictions.isNull("autorite")));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygCritere.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygCritere> find(int indice, int pas,Long id,String libelle,SygAutoriteContractante autorite) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCritere.class);
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		if(id!=null){
			criteria.add(Restrictions.ilike("id", id));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(autorite!=null){
			criteria.add(Restrictions.or(Restrictions.eq("autorite", autorite), Restrictions.isNull("autorite")));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	
	@Override
	public SygCritere save(SygCritere critere) {
		getHibernateSession().save(critere);
		getHibernateSession().flush();
		return critere;
	}
	

	@Override
	public void update(SygCritere critere) {
		
		try{
			getHibernateSession().merge(critere);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public SygCritere findById(Long id) {
		// TODO Auto-generated method stub
		return (SygCritere)getHibernateSession().get(SygCritere.class, id );
	}


	@Override
	public List<SygCritere> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCritere.class);
		List<SygCritere> critere = criteria.list();
		return critere;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygCritere > ListeCriteres(int indice, int pas, String libelle,Long dossier,Long autorite, SygLots lot) {
		Criteria criteria = getHibernateSession().createCriteria(SygCritere.class);
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(autorite!=null){
			criteria.add(Restrictions.or(Restrictions.eq("autorite.id", autorite), Restrictions.isNull("autorite")));
		}
		if(ListeCriteres(dossier,lot).size()>0)
		  criteria.add(Restrictions.not(Restrictions.in("id", ListeCriteres(dossier,lot))));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygCritere > ListeCriteres(String libelle,Long dossier,Long autorite, SygLots lot) {
		Criteria criteria = getHibernateSession().createCriteria(SygCritere.class);
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(autorite!=null){
			criteria.add(Restrictions.or(Restrictions.eq("autorite.id", autorite), Restrictions.isNull("autorite")));
		}
		if(ListeCriteres(dossier,lot).size()>0)
		  criteria.add(Restrictions.not(Restrictions.in("id", ListeCriteres(dossier,lot))));

		return criteria.list();
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Long> ListeCriteres(Long dossier, SygLots lot) {
		// TODO Auto-generated method stub
		
		Criteria criteria = getHibernateSession().createCriteria(SygDossierssouscriteres.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("critere", "critere");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier.dosID", dossier));
		}
		if(lot!=null){
			criteria.createAlias("lot", "lot");
			criteria.add(Restrictions.eq("lot", lot));
		}
		criteria.setProjection(Projections.property("critere.id"));
		return criteria.list();
	}



}