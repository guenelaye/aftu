package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class BailleursSessionBean extends AbstractSessionBean implements BailleursSession{

	@Override
	public int count(String libelle,SygAutoriteContractante autorite) {
		Criteria criteria = getHibernateSession().createCriteria(SygBailleurs.class);
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(autorite!=null){
			criteria.add(Restrictions.or(Restrictions.eq("autorite", autorite), Restrictions.isNull("autorite")));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygBailleurs.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygBailleurs> find(int indice, int pas,String libelle,SygAutoriteContractante autorite) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygBailleurs.class);
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(autorite!=null){
			criteria.add(Restrictions.or(Restrictions.eq("autorite", autorite), Restrictions.isNull("autorite")));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygBailleurs bailleur) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(bailleur);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygBailleurs bailleur) {
		
		try{
			getHibernateSession().merge(bailleur);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygBailleurs findById(Long code) {
		return (SygBailleurs)getHibernateSession().get(SygBailleurs.class, code);
	}
}
