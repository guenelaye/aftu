package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygRapportARMP;

@Remote
public interface RapportARMPSession {
	
	public void save(SygRapportARMP rapportarmp);
	
	public void delete(Long id);
	
	public List<SygRapportARMP> find(int indice, int pas,Long code,String libelle);
	
	public int count(Long code,String libelle);
	
	public void update(SygRapportARMP rapportarmp);
	
	public SygRapportARMP findById(Long code);
	
}
