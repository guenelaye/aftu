package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAnnuaire;

@Remote
public interface AnnuaireSession {
	public void save(SygAnnuaire annuaire);
	public void delete(Long id);
	public List<SygAnnuaire> find(int indice, int pas,String prenom,String nom,String fonction,String service,String autorite,String organe);
	public int count(String prenom,String nom,String fonction,String service,String autorite,String organe);
	public void update(SygAnnuaire annuaire);
	public SygAnnuaire findById(Long code);
}
