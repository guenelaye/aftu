package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygEvaluateur;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;



@Stateless
public class EvaluateurSessionBean extends AbstractSessionBean implements EvaluateurSession{

	@Override
	public int count(String nom, String prenom,
		    SygAutoriteContractante autorite,String telephone,String email,String fonction,
			 String commentaire) 
	{
		Criteria criteria = getHibernateSession().createCriteria(SygEvaluateur.class);
		
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		
		if(nom!=null){
			criteria.add(Restrictions.ilike("nom", "%"+nom+"%"));
		}
		if(prenom!=null){
			criteria.add(Restrictions.ilike("prenom", "%"+prenom+"%"));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(telephone!=null){
			criteria.add(Restrictions.ilike("telephone", "%"+telephone+"%"));
		}
		if(email!=null){
			criteria.add(Restrictions.ilike("email", "%"+email+"%"));
		}
		if(fonction!=null){
			criteria.add(Restrictions.ilike("fonction", "%"+fonction+"%"));
		}
		if(commentaire!=null){
			criteria.add(Restrictions.ilike("commentaire", "%"+commentaire+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygEvaluateur.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygEvaluateur> find(int indice, int pas,String nom, String prenom,
		    SygAutoriteContractante autorite,String telephone,String email,String fonction,
			 String commentaire) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygEvaluateur.class);
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.addOrder(Order.asc("nom"));
		
		if(nom!=null){
			criteria.add(Restrictions.ilike("nom", "%"+nom+"%"));
		}
		if(prenom!=null){
			criteria.add(Restrictions.ilike("prenom", "%"+prenom+"%"));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(telephone!=null){
			criteria.add(Restrictions.ilike("telephone", "%"+telephone+"%"));
		}
		if(email!=null){
			criteria.add(Restrictions.ilike("email", "%"+email+"%"));
		}
		if(fonction!=null){
			criteria.add(Restrictions.ilike("fonction", "%"+fonction+"%"));
		}
		if(commentaire!=null){
			criteria.add(Restrictions.ilike("commentaire", "%"+commentaire+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public List<SygEvaluateur> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygEvaluateur.class);
		criteria.createAlias("autorite", "autorite");
		
		List<SygEvaluateur> eval = criteria.list();
		return eval;
	}

	

	@Override
	public void save(SygEvaluateur eval) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(eval);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygEvaluateur eval) {
		
		try{
			getHibernateSession().merge(eval);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygEvaluateur findById(Integer code) {
		return (SygEvaluateur)getHibernateSession().get(SygEvaluateur.class, code);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygEvaluateur > ListeEvaluateurs(int indice, int pas, String libelle,Long dossier,Long autorite) {
		String libellesaisi;
		if(libelle!=null)
		{
		libellesaisi="evaluateurs.nom LIKE '%"+libelle+"%' and ";
		}
		else
		{
		libellesaisi="";	
		}
		
		return getHibernateSession().createQuery( "SELECT evaluateurs FROM  SygEvaluateur evaluateurs where " +libellesaisi
			+ "  evaluateurs.autorite.id='"+autorite+"' and evaluateurs.id NOT IN (SELECT dossiers.evaluateur.id from SygDossiersEvaluateurs dossiers where  dossiers.dossier.dosID='" + dossier + "'  )")
				.setFirstResult(indice).setMaxResults(pas).list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygEvaluateur > ListeEvaluateurs( String libelle,Long dossier,Long autorite) {
		String libellesaisi;
		if(libelle!=null)
		{
		libellesaisi="evaluateurs.nom LIKE '%"+libelle+"%' and ";
		}
		else
		{
		libellesaisi="";	
		}
		
		return getHibernateSession().createQuery( "SELECT evaluateurs FROM  SygEvaluateur evaluateurs where " +libellesaisi
			+ "  evaluateurs.autorite.id='"+autorite+"' and evaluateurs.id NOT IN (SELECT dossiers.evaluateur.id from SygDossiersEvaluateurs dossiers where  dossiers.dossier.dosID='" + dossier + "'  )")
				.list();
	}
}
