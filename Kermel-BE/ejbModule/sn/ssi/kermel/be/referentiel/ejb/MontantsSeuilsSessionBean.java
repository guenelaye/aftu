package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygMontantsSeuils;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class MontantsSeuilsSessionBean extends AbstractSessionBean implements MontantsSeuilsSession {

	@Override
	public void save(SygMontantsSeuils fournisseur) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(fournisseur);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	
	}

	@Override
	public void update(SygMontantsSeuils fournisseur) {
		// TODO Auto-generated method stub
		try{
			getHibernateSession().merge(fournisseur);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygMontantsSeuils.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public List<SygMontantsSeuils> find(int indice, int pas, SygTypeAutoriteContractante typeautorite, SygTypesmarches typemarche,SygModepassation modepassation,String modeengagement,String examen,String type) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygMontantsSeuils.class);
		criteria.createAlias("typeautorite", "typeautorite");
		criteria.createAlias("typemarche", "typemarche");
		criteria.createAlias("modepassation", "modepassation");
		criteria.addOrder(Order.asc("typeautorite.libelle"));
		if(typeautorite!=null)
			criteria.add(Restrictions.eq("typeautorite", typeautorite));
		if(typemarche!=null)
			criteria.add(Restrictions.eq("typemarche", typemarche));
		if(modepassation!=null)
			criteria.add(Restrictions.eq("modepassation", modepassation));
		if(modeengagement!=null)
			criteria.add(Restrictions.ilike("modeengagement","%"+ modeengagement+"%"));
		if(examen!=null)
			criteria.add(Restrictions.eq("examen", examen));	
		if(type!=null)
			criteria.add(Restrictions.eq("type", type));	
		
		criteria.setFirstResult(indice);
		
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public int count(SygTypeAutoriteContractante typeautorite, SygTypesmarches typemarche,SygModepassation modepassation,String modeengagement,String examen,String type) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygMontantsSeuils.class);
		criteria.createAlias("typeautorite", "typeautorite");
		criteria.createAlias("typemarche", "typemarche");
		criteria.createAlias("modepassation", "modepassation");
		if(typeautorite!=null)
			criteria.add(Restrictions.eq("typeautorite", typeautorite));
		if(typemarche!=null)
			criteria.add(Restrictions.eq("typemarche", typemarche));
		if(modepassation!=null)
			criteria.add(Restrictions.eq("modepassation", modepassation));
		if(modeengagement!=null)
			criteria.add(Restrictions.ilike("modeengagement","%"+ modeengagement+"%"));
		if(examen!=null)
			criteria.add(Restrictions.eq("examen", examen));	
		if(type!=null)
			criteria.add(Restrictions.eq("type", type));	
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public SygMontantsSeuils findById(Long code) {
		// TODO Auto-generated method stub
		return (SygMontantsSeuils)getHibernateSession().get(SygMontantsSeuils.class, code);
	}


}
