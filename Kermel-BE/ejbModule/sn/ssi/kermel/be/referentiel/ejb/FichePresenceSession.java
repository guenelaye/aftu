package sn.ssi.kermel.be.referentiel.ejb;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;


import sn.ssi.kermel.be.entity.SygFichePresence;
import sn.ssi.kermel.be.entity.SygProformation;

@Remote
public interface FichePresenceSession {
	public void save(SygFichePresence fiche);
	public List<SygFichePresence> findAll(int indice, int pas);
	public void update(SygFichePresence fiche);
	SygFichePresence findByCode(Long id);
	List<SygFichePresence> findAllby(int indice, int pas, Date datefiche,SygProformation formation);
	public void delete(Long id);
	public int count(Date datefiche,SygProformation formation);
	List<SygFichePresence> findAll();
	

}
