package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygMonnaieoffre;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class MonnaieoffreSessionBean extends AbstractSessionBean implements MonnaieoffreSession{

	@Override
	public int count(String code,String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygMonnaieoffre.class);
		if(code!=null){
			criteria.add(Restrictions.ilike("monCode", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("monLibelle", "%"+libelle+"%"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygMonnaieoffre.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygMonnaieoffre> find(int indice, int pas,String code,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygMonnaieoffre.class);
		criteria.addOrder(Order.desc("monCode"));
		if(code!=null){
			criteria.add(Restrictions.ilike("monCode", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("monLibelle", "%"+libelle+"%"));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygMonnaieoffre monnaie) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(monnaie);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygMonnaieoffre monnaie) {
		
		try{
			getHibernateSession().merge(monnaie);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygMonnaieoffre findById(Long code) {
		return (SygMonnaieoffre)getHibernateSession().get(SygMonnaieoffre.class, code);
	}
	
	
	@Override
	public List<SygMonnaieoffre> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygMonnaieoffre.class);
		if(code!=null)
			criteria.add(Restrictions.eq("monCode", code));
		
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygMonnaieoffre > ListeMonnaies(int indice, int pas, String libelle,Long dossier) {
		String libellesaisi;
		if(libelle!=null)
		{
		libellesaisi="monnaies.monLibelle LIKE '%"+libelle+"%' and ";
		}
		else
		{
		libellesaisi="";	
		}
		
		return getHibernateSession().createQuery( "SELECT monnaies FROM  SygMonnaieoffre monnaies where " +libellesaisi
			+ " monnaies.monId NOT IN (SELECT devises.monnaie.monId from SygDevise devises where  devises.dossier.dosID='" + dossier + "' " +")")
				.setFirstResult(indice).setMaxResults(pas).list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygMonnaieoffre > ListeMonnaies(String libelle,Long dossier) {
		String libellesaisi;
		if(libelle!=null)
		{
			libellesaisi="monnaies.monLibelle LIKE '%"+libelle+"%' and ";
		}
		else
		{
		libellesaisi="";	
		}
		return getHibernateSession().createQuery(
				"SELECT monnaies FROM  SygMonnaieoffre monnaies where " +libellesaisi
			+ " monnaies.monId NOT IN (SELECT devises.monnaie.monId from SygDevise devises where  devises.dossier.dosID='" + dossier + "' " +")")
				.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygMonnaieoffre > ListeMonnaies(int indice, int pas, String libelle,Long dossier,String nature) {
		String libellesaisi;
		if(libelle!=null)
		{
		libellesaisi="monnaies.monLibelle LIKE '%"+libelle+"%' and ";
		}
		else
		{
		libellesaisi="";	
		}
		
		return getHibernateSession().createQuery( "SELECT monnaies FROM  SygMonnaieoffre monnaies where " +libellesaisi+" monnaies.monId "+nature
			+ "  (SELECT devises.monnaie.monId from SygDevise devises where  devises.dossier.dosID='" + dossier + "' " +")")
				.setFirstResult(indice).setMaxResults(pas).list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygMonnaieoffre > ListeMonnaies(String libelle,Long dossier,String nature) {
		String libellesaisi;
		if(libelle!=null)
		{
			libellesaisi="monnaies.monLibelle LIKE '%"+libelle+"%' and ";
		}
		else
		{
		libellesaisi="";	
		}
		return getHibernateSession().createQuery(
				"SELECT monnaies FROM  SygMonnaieoffre monnaies where " +libellesaisi+" monnaies.monId "+nature
			+ "   (SELECT devises.monnaie.monId from SygDevise devises where  devises.dossier.dosID='" + dossier + "' " +")")
				.list();
	}
}
