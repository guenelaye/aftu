package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygService;

@Remote
public interface ContratSession {
	public void save(SygContrats service);
	public void delete(Long id);
	public List<SygContrats> find(int indice, int pas,String code,String libelle,SygAutoriteContractante autorite, Long id);
	public int count(String code,String libelle,SygAutoriteContractante autorite, Long id);
	public void update(SygContrats service);
	public SygContrats findById(Long code);
	List<SygContrats> count1(String code, String libelle, SygAutoriteContractante autorite, Long id);
}
