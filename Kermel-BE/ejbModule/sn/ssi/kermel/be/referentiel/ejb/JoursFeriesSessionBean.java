package sn.ssi.kermel.be.referentiel.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygJoursferies;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class JoursFeriesSessionBean extends AbstractSessionBean implements JoursFeriesSession{

	@Override
	public int count(Long code,Date date) {
		Criteria criteria = getHibernateSession().createCriteria(SygJoursferies.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(date!=null){
			criteria.add(Restrictions.eq("date", date));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygJoursferies.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygJoursferies> find(int indice, int pas,Long code,Date date) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygJoursferies.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(date!=null){
			criteria.add(Restrictions.eq("date", date));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygJoursferies jour) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(jour);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygJoursferies jour) {
		
		try{
			getHibernateSession().merge(jour);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygJoursferies findById(Long code) {
		return (SygJoursferies)getHibernateSession().get(SygJoursferies.class, code);
	}
	
	@Override
	public int NombreJoursFeries(Date datedebut,Date datefin) {
		Criteria criteria = getHibernateSession().createCriteria(SygJoursferies.class);
		if(datedebut!=null){
			criteria.add(Restrictions.ge("date", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("date", datefin));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	@Override
	public List<SygJoursferies> JoursFeries(Date datedebut,Date datefin) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygJoursferies.class);
		if(datedebut!=null){
			criteria.add(Restrictions.ge("date", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("date", datefin));
		}
		
		return criteria.list();
	}

}
