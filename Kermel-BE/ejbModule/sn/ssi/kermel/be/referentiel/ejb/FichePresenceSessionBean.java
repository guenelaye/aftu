package sn.ssi.kermel.be.referentiel.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;



import sn.ssi.kermel.be.entity.SygFichePresence;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class FichePresenceSessionBean extends AbstractSessionBean implements
		FichePresenceSession {

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(SygFichePresence.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygFichePresence> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFichePresence.class);
		List<SygFichePresence> fiche = criteria.list();
		return fiche;
	}


	@Override
	public SygFichePresence findByCode(Long id) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFichePresence.class);

		if (id != null) {
			criteria.add(Restrictions.eq("id", id));

		}
		return (SygFichePresence) criteria.list().get(0);
	}

	
	@Override
	public void save(SygFichePresence fiche) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(fiche);
			//getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void update(SygFichePresence fiche) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().merge(fiche);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygFichePresence> findAllby(int indice, int pas,Date datefiche,SygProformation formation) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFichePresence.class);
		criteria.createAlias("formation","formation");
		
		criteria.addOrder(Order.asc("dateFiche"));
		if (datefiche != null) {
			criteria.add(Restrictions.ge("dateFiche", "%" + datefiche + "%"));
		}
		if (formation != null) {
			criteria.add(Restrictions.eq("formation.id", formation.getId()
					));
		}
		
		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

		
	@Override
	public int count(Date datefiche,SygProformation formation) {
		Criteria criteria = getHibernateSession().createCriteria(
				SygFichePresence.class);
		
		if (datefiche != null) {
			criteria.add(Restrictions.ge("dateFiche", "%" + datefiche + "%"));
		}
		if (formation != null) {
			criteria.add(Restrictions.eq("formation", formation));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public List<SygFichePresence> findAll(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFichePresence.class);

		if (indice > 0)
			criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);
		return criteria.list();
	}

	

}



