package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygFormationAutorite;
import sn.ssi.kermel.be.entity.SygFormationGroupe;
import sn.ssi.kermel.be.entity.SygGroupeFormation;
import sn.ssi.kermel.be.entity.SygProformation;





@Remote
public interface AutoriteFormationSession {
	public void save(SygFormationAutorite fauto);

	public void delete(Integer id);

	public List<SygFormationAutorite> find(int indice, int pas);

	public void update(SygFormationAutorite fauto);

	public List<SygFormationAutorite> findAll();

	public List<SygFormationAutorite> find(int indice, int pas,
			 SygProformation formation, SygAutoriteContractante autorite);
	public int count(SygProformation formation, SygAutoriteContractante autorite);
	SygFormationAutorite findByCode(Integer id);
	
	
	public void saveGroupe(SygFormationGroupe groupe);

	public void deleteGroupe(Integer id);

	public List<SygFormationGroupe> findGroupes(int indice, int pas);

	public void updateGroupe(SygFormationGroupe fauto);

	public List<SygFormationGroupe> findGroupe(int indice, int pas, SygProformation formation,SygGroupeFormation groupe);
	public int countGroupe(SygProformation formation,SygGroupeFormation groupe);
	SygFormationGroupe findGroupe(Integer id);
	
	
}
