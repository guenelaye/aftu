package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygCategorieLegislation;
import sn.ssi.kermel.be.entity.SygCodeMarche;
import sn.ssi.kermel.be.entity.SygFichierLegislation;
import sn.ssi.kermel.be.entity.SygRapportARMP;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class FichierLegislationSessionBean extends AbstractSessionBean implements FichierLegislationSession{

	@Override
	public int count(Long code,String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygFichierLegislation.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygFichierLegislation.class, id));
		//	getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygFichierLegislation> find(int indice, int pas,Long code,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygFichierLegislation.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygFichierLegislation> findAll(int indice, int pas,SygCategorieLegislation categorie,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygFichierLegislation.class);
		if(categorie!=null){
			criteria.add(Restrictions.eq("categorie", categorie));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public int countALL(SygCategorieLegislation categorie,String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygFichierLegislation.class);
		if(categorie!=null){
			criteria.add(Restrictions.eq("categorie", categorie));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	
	

	

	@Override
	public void save(SygFichierLegislation rapportarmp) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(rapportarmp);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygFichierLegislation rapportarmp) {
		
		try{
			getHibernateSession().merge(rapportarmp);
			//getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygFichierLegislation findById(Long code) {
		return (SygFichierLegislation)getHibernateSession().get(SygFichierLegislation.class, code);
	}
}
