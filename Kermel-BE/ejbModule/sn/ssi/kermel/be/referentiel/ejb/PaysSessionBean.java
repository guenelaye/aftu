package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygPays;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;
@Stateless
public class PaysSessionBean  extends AbstractSessionBean implements PaysSession{
	
	
	
	@Override
	public int count(String libelle,String codepays) {
		Criteria criteria= getHibernateSession().createCriteria(SygPays.class);
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(codepays!=null){
			criteria.add(Restrictions.ilike("codepays", "%"+codepays+"%"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	

	@Override
	public void delete(Long idpays) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygPays.class, idpays));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygPays> find(int indice, int pas,String libelle,String codepays) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPays.class);
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		if(codepays!=null){
			criteria.add(Restrictions.ilike("codepays", "%"+codepays+"%"));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygPays pays) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(pays);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygPays pays) {
		
		try{
			getHibernateSession().merge(pays);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public SygPays findById(Long idpays) {
		// TODO Auto-generated method stub
		return (SygPays)getHibernateSession().get(SygPays.class, idpays );
	}



	
	
	
}