package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCatFournisseur;
import sn.ssi.kermel.be.entity.SygFournisseur;

@Remote
public interface FournisseurSession {
	public SygFournisseur save (SygFournisseur fournisseur);
	public void update (SygFournisseur fournisseur);
	public void delete (Long id);
	public List<SygFournisseur> find(int indice, int pas,String nom,SygCatFournisseur categorie, SygAutoriteContractante autorite);
	public int count (String nom,SygCatFournisseur categorie, SygAutoriteContractante autorite);
	public SygFournisseur findById(Long code);
	
	//Add by rama 14-11-2012
	public List<SygFournisseur> findPaiement(int indice, int pas,String statut,String nom);
	public int countPaiement (String statut,String nom);
	public List<Long> Paiement(String status);
	
	public List<SygFournisseur> findEnRegle(int indice, int pas,String statut,String nom);
	public int countEnRegle (String statut,String nom);
	SygFournisseur findExist(String ninea);
	
}
