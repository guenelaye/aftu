package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygPoledcmp;

@Remote
public interface PoledcmpSession {
	public void save(SygPoledcmp pole);
	public void delete(Long id);
	public List<SygPoledcmp> find(int indice, int pas,Long code,String libelle);
	public int count(Long code,String libelle);
	public void update(SygPoledcmp pole);
	public SygPoledcmp findById(Long code);
}
