package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygBureauxdcmp;
import sn.ssi.kermel.be.entity.SygTypeUniteOrgdcmp;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class BureauxdcmpSessionBean extends AbstractSessionBean implements BureauxdcmpSession{

	@Override
	public int count(String libelle,String division) {
		Criteria criteria = getHibernateSession().createCriteria(SygBureauxdcmp.class);
		//criteria.setFetchMode("clients", FetchMode.SELECT);
		criteria.createAlias("division", "division");
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(division!=null){
			criteria.add(Restrictions.ilike("division.libelle", "%"+division+"%"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygBureauxdcmp.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygBureauxdcmp> find(int indice, int pas,String libelle,String division) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygBureauxdcmp.class);
		criteria.createAlias("division", "division");
		criteria.addOrder(Order.asc("libelle"));
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(division!=null){
			criteria.add(Restrictions.ilike("division.libelle", "%"+division+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygBureauxdcmp bureau) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(bureau);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygBureauxdcmp bureau) {
		
		try{
			getHibernateSession().merge(bureau);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygBureauxdcmp findById(Long code) {
		return (SygBureauxdcmp)getHibernateSession().get(SygBureauxdcmp.class, code);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygBureauxdcmp> findRech(int indice, int pas,String libelle,Long division, Long uniteorg) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygBureauxdcmp.class);
		criteria.createAlias("division", "division");
		criteria.setFetchMode("uniteorg", FetchMode.SELECT);
		criteria.addOrder(Order.asc("libelle"));
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(division!=null){
			criteria.add(Restrictions.eq("division.id", division));
		}
		
		if(uniteorg!=null){
			criteria.add(Restrictions.eq("uniteorg.id", uniteorg));
		}else{
			criteria.add(Restrictions.isNull("uniteorg.id"));
		}
		
			
		
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public int countRech(String libelle,Long division,Long uniteorg) {
		Criteria criteria = getHibernateSession().createCriteria(SygBureauxdcmp.class);
		//criteria.setFetchMode("clients", FetchMode.SELECT);
		criteria.createAlias("division", "division");
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(division!=null){
			criteria.add(Restrictions.eq("division.id", division));
		}
		if(uniteorg!=null){
			criteria.add(Restrictions.eq("uniteorg.id", uniteorg));
		}else{
			criteria.add(Restrictions.isNull("uniteorg.id"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygBureauxdcmp> findRech1(int indice, int pas,String libelle, Long uniteorg) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygBureauxdcmp.class);
		criteria.createAlias("division", "division");
		criteria.setFetchMode("uniteorg", FetchMode.SELECT);
		criteria.addOrder(Order.asc("libelle"));
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		
		if(uniteorg!=null){
			criteria.add(Restrictions.eq("uniteorg.id", uniteorg));
		}else{
			criteria.add(Restrictions.isNull("uniteorg.id"));
		}
		
			
		
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
}
