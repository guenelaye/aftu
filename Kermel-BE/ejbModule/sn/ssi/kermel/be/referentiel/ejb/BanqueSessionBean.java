package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygBanque;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class BanqueSessionBean extends AbstractSessionBean implements BanqueSession{

	@Override
	public int count(Long code,String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygBanque.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygBanque.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygBanque> find(int indice, int pas,Long code,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygBanque.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygBanque banque) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(banque);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygBanque banque) {
		
		try{
			getHibernateSession().merge(banque);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygBanque findById(Long code) {
		return (SygBanque)getHibernateSession().get(SygBanque.class, code);
	}
}
