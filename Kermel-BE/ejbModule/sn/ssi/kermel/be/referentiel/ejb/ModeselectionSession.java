package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygModeselection;

@Remote
public interface ModeselectionSession {
	public void save(SygModeselection mode);
	public void delete(Long id);
	public List<SygModeselection> find(int indice, int pas,String code,String libelle);
	public int count(String code,String libelle);
	public void update(SygModeselection mode);
	public SygModeselection findById(Long code);
	public List<SygModeselection> find(String code);
}
