package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygGlossaire;
import sn.ssi.kermel.be.entity.SygJournal;
import sn.ssi.kermel.be.entity.SygService;

@Remote
public interface GlossaireSession {
	public void save(SygGlossaire service);
	public void delete(Long id);
	public List<SygGlossaire> find(int indice, int pas,String code,String libelle,SygAutoriteContractante autorite, Long id);
	public int count(String code,String libelle,SygAutoriteContractante autorite, Long id);
	public void update(SygGlossaire service);
	public SygGlossaire findById(Long code);
}
