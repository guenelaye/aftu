package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygTypeDemande;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;
@Stateless
public class TypeDemandeSessionBean  extends AbstractSessionBean implements TypeDemandeSession{
	
	
	
	@Override
	public int count(String libelle) {
		Criteria criteria= getHibernateSession().createCriteria(SygTypeDemande.class);
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygTypeDemande.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygTypeDemande> find(int indice, int pas,String libelle,String description) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTypeDemande.class);
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		if(description!=null){
			criteria.add(Restrictions.ilike("description", "%"+description+"%"));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygTypeDemande demande) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(demande);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygTypeDemande demande) {
		
		try{
			getHibernateSession().merge(demande);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public SygTypeDemande findById(Long id) {
		// TODO Auto-generated method stub
		return (SygTypeDemande)getHibernateSession().get(SygTypeDemande.class, id );
	}



	
	
	
}