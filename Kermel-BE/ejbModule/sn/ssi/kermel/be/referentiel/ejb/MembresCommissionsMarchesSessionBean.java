package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygArretesMembresCommissionsMarches;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygMembresCommissionsMarches;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class MembresCommissionsMarchesSessionBean extends AbstractSessionBean implements MembresCommissionsMarchesSession{

	@Override
	public int count(String prenom,String nom,SygAutoriteContractante autorite,String gestion, int responsable,int etape) {
		Criteria criteria = getHibernateSession().createCriteria(SygMembresCommissionsMarches.class);
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		if(prenom!=null){
			criteria.add(Restrictions.ilike("prenom", "%"+prenom+"%"));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("nom", "%"+nom+"%"));
		}
		if(autorite!=null){
			criteria.add(Restrictions.or(Restrictions.eq("autorite", autorite), Restrictions.isNull("autorite")));
		}
		if(gestion!=null){
			criteria.add(Restrictions.eq("gestion",gestion));
		}
		if(responsable>-1){
			criteria.add(Restrictions.eq("flagpresident", responsable));
		}
		if(etape>-1){
			criteria.add(Restrictions.eq("etapePI", etape));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}



	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygMembresCommissionsMarches.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygMembresCommissionsMarches> find(int indice, int pas,String prenom,String nom,SygAutoriteContractante autorite,String gestion, int responsable,int etape) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygMembresCommissionsMarches.class);
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.addOrder(Order.desc("nom"));
		if(prenom!=null){
			criteria.add(Restrictions.ilike("prenom", "%"+prenom+"%"));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("nom", "%"+nom+"%"));
		}
		if(autorite!=null){
			criteria.add(Restrictions.or(Restrictions.eq("autorite", autorite), Restrictions.isNull("autorite")));
		}
		if(gestion!=null){
			criteria.add(Restrictions.eq("gestion",gestion));
		}
		
		if(responsable>-1){
			criteria.add(Restrictions.eq("flagpresident", responsable));
		}
		if(etape>-1){
			criteria.add(Restrictions.eq("etapePI", etape));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygMembresCommissionsMarches membre) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(membre);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygMembresCommissionsMarches membre) {
		
		try{
			getHibernateSession().merge(membre);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygMembresCommissionsMarches findById(Long code) {
		return (SygMembresCommissionsMarches)getHibernateSession().get(SygMembresCommissionsMarches.class, code);
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygMembresCommissionsMarches > ListeMembres(int indice, int pas, String nom,Long dossier,Long autorite) {
		String libellesaisi;
		if(nom!=null)
		{
		libellesaisi="membres.nom LIKE '%"+nom+"%' and ";
		}
		else
		{
		libellesaisi="";	
		}
		
		return getHibernateSession().createQuery( "SELECT membres FROM  SygMembresCommissionsMarches membres where " +libellesaisi
			+ " membres.autorite.id='"+autorite+"' and  membres.id NOT IN (SELECT dossiers.membre.id from SygDossierscommissionsmarches dossiers where  dossiers.dossier.dosID='" + dossier + "' " +")")
				.setFirstResult(indice).setMaxResults(pas).list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygMembresCommissionsMarches > ListeMembres(String nom,Long dossier,Long autorite) {
		String libellesaisi;
		if(nom!=null)
		{
		libellesaisi="membres.nom LIKE '%"+nom+"%' and ";
		}
		else
		{
		libellesaisi="";	
		}
		

		return getHibernateSession().createQuery( "SELECT membres FROM  SygMembresCommissionsMarches membres where " +libellesaisi
				+ " membres.autorite.id='"+autorite+"' and  membres.id NOT IN (SELECT dossiers.membre.id from SygDossierscommissionsmarches dossiers where  dossiers.dossier.dosID='" + dossier + "' " +")")
					.list();
	}

	
	////////////Arrete////////
	
	@Override
	public void saveArrete(SygArretesMembresCommissionsMarches arrete) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(arrete);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void updateArrete(SygArretesMembresCommissionsMarches arrete) {
		
		try{
			getHibernateSession().merge(arrete);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygArretesMembresCommissionsMarches findArrete(SygAutoriteContractante autorite,String gestion) {
		Criteria criteria = getHibernateSession().createCriteria(SygArretesMembresCommissionsMarches.class);
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite",autorite));
		}
		if(gestion!=null){
			criteria.add(Restrictions.eq("gestion",gestion));
		}
		
		
		if(criteria.list().size()>0)
			return (SygArretesMembresCommissionsMarches) criteria.list().get(0);
		else
			return null;
	}
}
