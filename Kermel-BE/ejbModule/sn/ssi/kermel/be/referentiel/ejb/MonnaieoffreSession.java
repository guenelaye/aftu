package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygMonnaieoffre;

@Remote
public interface MonnaieoffreSession {
	public void save(SygMonnaieoffre monnaie);
	public void delete(Long id);
	public List<SygMonnaieoffre> find(int indice, int pas,String code,String libelle);
	public int count(String code,String libelle);
	public void update(SygMonnaieoffre monnaie);
	public SygMonnaieoffre findById(Long code);
	public List<SygMonnaieoffre> find(String code);
	
	public List<SygMonnaieoffre> ListeMonnaies(int indice, int pas,String libelle,Long dossier);
	public List<SygMonnaieoffre> ListeMonnaies(String libelle,Long dossier);
	
	public List<SygMonnaieoffre> ListeMonnaies(int indice, int pas,String libelle,Long dossier,String nature);
	public List<SygMonnaieoffre> ListeMonnaies(String libelle,Long dossier,String nature);
}
