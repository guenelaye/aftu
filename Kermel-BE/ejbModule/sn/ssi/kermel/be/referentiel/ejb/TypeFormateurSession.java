package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygTypeFormateur;;

@Remote
public interface TypeFormateurSession {
	public void save(SygTypeFormateur type);
	public void delete(Long id);
	public List<SygTypeFormateur> find(int indice, int pas,String code,String libelle);
	public int count(String code,String libelle);
	public void update(SygTypeFormateur type);
	public SygTypeFormateur findById(Long id);

}
