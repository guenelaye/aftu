package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygFormateur;
import sn.ssi.kermel.be.entity.SygFormationGroupe;
import sn.ssi.kermel.be.entity.SygGroupeFormation;
import sn.ssi.kermel.be.entity.SygMembresGroupes;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;
@Stateless
public class GroupeFormationSessionBean extends AbstractSessionBean implements GroupeFormationSession{

	@Override
	public int count(String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygGroupeFormation.class);
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygGroupeFormation.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygGroupeFormation> find(int indice, int pas, String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygGroupeFormation.class);
		criteria.addOrder(Order.desc("libelle"));
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygGroupeFormation groupe) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(groupe);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygGroupeFormation groupe) {
		
		try{
			getHibernateSession().merge(groupe);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public List<SygGroupeFormation> find(String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygGroupeFormation.class);
		if(libelle!=null)
			criteria.add(Restrictions.eq("libelle", libelle));
		
		return criteria.list();
	}

@Override
public SygGroupeFormation findById(Integer code) {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession().createCriteria(
			SygGroupeFormation.class);
	if (code != null) {
		criteria.add(Restrictions.eq("id", code));

	}
	return (SygGroupeFormation) criteria.list().get(0);
}

/////////////////////////
@Override
public int countMembres(SygGroupeFormation groupe) {
	Criteria criteria = getHibernateSession().createCriteria(SygMembresGroupes.class);
	
	if(groupe!=null){
		criteria.add(Restrictions.eq("groupe", groupe));
	}
	
	criteria.setProjection(Projections.rowCount());
	return Integer.parseInt(criteria.uniqueResult().toString());

}

@Override
public void deleteMembre(Integer id) {
	// TODO Auto-generated method stub
	try {
		getHibernateSession().delete(getHibernateSession().get(SygMembresGroupes.class, id));
		getHibernateSession().flush();
	} catch (Exception e) {
		// TODO: handle exception
	}
}

@Override
public List<SygMembresGroupes> findMembres(int indice, int pas, SygGroupeFormation groupe) {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession().createCriteria(SygMembresGroupes.class);
	criteria.addOrder(Order.desc("nom"));
	if(groupe!=null){
		criteria.add(Restrictions.eq("groupe",groupe));
	}
	
	criteria.setFirstResult(indice);
	if(pas>0)
		criteria.setMaxResults(pas);
	
	return criteria.list();
}



@Override
public void saveMembre(SygMembresGroupes groupe) {
	// TODO Auto-generated method stub
	try {
		getHibernateSession().save(groupe);
		getHibernateSession().flush();
		
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
	}	}

@Override
public void updateMembre(SygMembresGroupes groupe) {
	
	try{
		getHibernateSession().merge(groupe);
		getHibernateSession().flush();
	}catch (Exception e) {
		e.printStackTrace();
	}
}



	@Override
	public SygMembresGroupes findMembre(Integer code) {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession().createCriteria(
			SygMembresGroupes.class);
	if (code != null) {
		criteria.add(Restrictions.eq("id", code));
	
	}
	return (SygMembresGroupes) criteria.list().get(0);
	}
	
	@Override
	public List<SygGroupeFormation> Groupes(int indice, int pas, String libelle,SygProformation formation) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygGroupeFormation.class);
		criteria.addOrder(Order.desc("libelle"));
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(Groupes(formation).size()>0)
			 criteria.add(Restrictions.not(Restrictions.in("id", Groupes(formation))));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public List<SygGroupeFormation> Groupes( String libelle,SygProformation formation) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygGroupeFormation.class);
		criteria.addOrder(Order.desc("libelle"));
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(Groupes(formation).size()>0)
		 criteria.add(Restrictions.not(Restrictions.in("id", Groupes(formation))));
		
		
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> Groupes(SygProformation formation) {
		// TODO Auto-generated method stub
		
		Criteria criteria = getHibernateSession().createCriteria(SygFormationGroupe.class);	
		criteria.createAlias("formation", "formation");
		if(formation!=null){
			criteria.add(Restrictions.eq("formation", formation));
		}
		criteria.setProjection(Projections.property("groupe.id"));
		return criteria.list();
	}
}
