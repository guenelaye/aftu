package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygTypeDemande;


@Remote
public interface TypeDemandeSession {
	public void save(SygTypeDemande demande);
	public void delete(Long id);
	public List<SygTypeDemande> find(int indice, int pas,String libelle,String description);
	public int count(String libelle);
	public void update(SygTypeDemande demande);
	public SygTypeDemande findById(Long id);
	

}
