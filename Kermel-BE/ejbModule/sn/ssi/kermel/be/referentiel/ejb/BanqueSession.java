package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygBanque;

@Remote
public interface BanqueSession {
	
	public void save(SygBanque banque);
	public void delete(Long id);
	public List<SygBanque> find(int indice, int pas,Long code,String libelle);
	public int count(Long code,String libelle);
	public void update(SygBanque banque);
	public SygBanque findById(Long code);
	
}
