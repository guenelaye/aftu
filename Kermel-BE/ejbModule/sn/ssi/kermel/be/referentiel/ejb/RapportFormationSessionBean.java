package sn.ssi.kermel.be.referentiel.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygFichePresence;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.entity.SygRapportFormation;
import sn.ssi.kermel.be.entity.SygTypeRapportFormation;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class RapportFormationSessionBean extends AbstractSessionBean implements RapportFormationSession {

	@Override
	public int count(Date daterapport,Date datepublication,SygProformation formation,SygTypeRapportFormation type) {
		Criteria criteria = getHibernateSession().createCriteria(SygRapportFormation.class);
		if(daterapport!=null){
			criteria.add(Restrictions.ge("date", daterapport));
		}
		if(datepublication!=null){
			criteria.add(Restrictions.ge("datepublication", datepublication));
		}
		if(type!=null){
			criteria.add(Restrictions.eq("type", type));
		}
		if(formation!=null){
			criteria.add(Restrictions.eq("formation", formation));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygRapportFormation.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	@Override
	public void delete(Long idrapport) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygRapportFormation.class, idrapport));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	@Override
	public void save(SygRapportFormation rapport) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(rapport);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygRapportFormation rapport) {
	try{
			getHibernateSession().merge(rapport);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public SygRapportFormation findById(Long code) {
		return (SygRapportFormation)getHibernateSession().get(SygRapportFormation.class, code);
	}
	
	@Override
	public List<SygRapportFormation> find(int indice, int pas,String libelle,Date daterapport,Date datepublication,SygProformation formation,SygTypeRapportFormation type) {
       Criteria criteria = getHibernateSession().createCriteria(
    		                SygRapportFormation.class);
		criteria.createAlias("type","type");
		criteria.createAlias("formation","formation");
		
		criteria.addOrder(Order.asc("libelle"));
		
       if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%" + libelle+ "%"));
		}
		if(daterapport!=null){
			criteria.add(Restrictions.le("date", daterapport));
		}
		if(datepublication!=null){
			criteria.add(Restrictions.le("datepublication", datepublication));
		}
		
		if(type!=null){
			criteria.add(Restrictions.eq("type", type));
		}
		if(formation!=null){
			criteria.add(Restrictions.eq("formation", formation));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);	
		return criteria.list();
		}
	@Override
	public List<SygRapportFormation> find(int indice, int pas) {
	   Criteria criteria = getHibernateSession().createCriteria(SygRapportFormation.class);
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);			
		return criteria.list();
		}
	}
