
package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygModule;

@Remote
public interface ModuleSession {

	public void save(SygModule module);
	public void update(SygModule module);
	public void delete(Integer id);
	
	public List<SygModule> find(int indice, int pas);
	public List<SygModule> find(int indice, int pas,String code, String libelle);
	public List<SygModule> findAll();
	SygModule findByCode(Integer id);
	
	public int count();
	public int count(String code,String libelle);
}
