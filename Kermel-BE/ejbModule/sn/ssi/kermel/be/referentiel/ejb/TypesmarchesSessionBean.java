package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class TypesmarchesSessionBean extends AbstractSessionBean implements TypesmarchesSession{

	@Override
	public int count(String code,String libelle, Long type) {
		Criteria criteria = getHibernateSession().createCriteria(SygTypesmarches.class);
		if(code!=null){
			criteria.add(Restrictions.ilike("code", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(type!=null){
//			if(type.intValue()==BeConstants.PARAM_TMTRAVAUX.intValue())
//			  criteria.add(Restrictions.or(Restrictions.eq("id", BeConstants.PARAM_TMTRAVAUX), Restrictions.eq("id", BeConstants.PARAM_TMFOURNITURES)));
//			else
//			 criteria.add(Restrictions.eq("id", type));
			
			criteria.add(Restrictions.eq("id", type));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygTypesmarches.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygTypesmarches> find(int indice, int pas,String code,String libelle, Long type) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTypesmarches.class);
		criteria.addOrder(Order.desc("code"));
		if(code!=null){
			criteria.add(Restrictions.ilike("code", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(type!=null){
//			if(type.intValue()==BeConstants.PARAM_TMTRAVAUX.intValue())
//			  criteria.add(Restrictions.or(Restrictions.eq("id", BeConstants.PARAM_TMTRAVAUX), Restrictions.eq("id", BeConstants.PARAM_TMFOURNITURES)));
//			else
//			 criteria.add(Restrictions.eq("id", type));
			
			 criteria.add(Restrictions.eq("id", type));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygTypesmarches type) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(type);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygTypesmarches type) {
		
		try{
			getHibernateSession().merge(type);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygTypesmarches findById(Long code) {
		return (SygTypesmarches)getHibernateSession().get(SygTypesmarches.class, code);
	}
	
	
	@Override
	public List<SygTypesmarches> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTypesmarches.class);
		if(code!=null)
			criteria.add(Restrictions.eq("code", code));
		
		return criteria.list();
	}
}
