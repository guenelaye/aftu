package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygPays;


@Remote
public interface PaysSession {
	public void save(SygPays pays);
	public void delete(Long idpays);
	public List<SygPays> find(int indice, int pas,String libelle,String codepays);
	public int count(String libelle,String codepays);
	public void update(SygPays pays);
	public SygPays findById(Long idpays);
	

}
