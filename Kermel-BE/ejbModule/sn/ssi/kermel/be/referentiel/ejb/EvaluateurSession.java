package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygEvaluateur;


@Remote
public interface EvaluateurSession {
	public void save(SygEvaluateur eval);
	public void delete(Integer id);
	public List<SygEvaluateur> find(int indice, int pas,String nom, String prenom,
    SygAutoriteContractante autorite,String telephone,String email,String fonction,
	 String commentaire);
	public List<SygEvaluateur>findAll();
	public int count(String nom, String prenom, SygAutoriteContractante autorite,String telephone,String email,String fonction,  String commentaire);
	public void update(SygEvaluateur eval);
	public SygEvaluateur findById(Integer code);
	
	
	public List<SygEvaluateur> ListeEvaluateurs(int indice, int pas,String libelle,Long dossier,Long autorite);
	public List<SygEvaluateur> ListeEvaluateurs(String libelle,Long dossier,Long autorite);

}
