package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygCoordonateur;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class CoordonnateurSessionBean extends AbstractSessionBean implements
		CoordonnateurSession {

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(SygCoordonateur.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygCoordonateur> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygCoordonateur.class);
		List<SygCoordonateur> fiche = criteria.list();
		return fiche;
	}


	@Override
	public SygCoordonateur findByCode(Integer id) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygCoordonateur.class);

		if (id != null) {
			criteria.add(Restrictions.eq("id", id));

		}
		return (SygCoordonateur) criteria.list().get(0);
	}

	
	@Override
	public void save(SygCoordonateur coord) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(coord);
			//getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void update(SygCoordonateur coord) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().merge(coord);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygCoordonateur> findAllby(int indice, int pas, String nom,String prenom) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygCoordonateur.class);
		criteria.addOrder(Order.asc("nom"));
		if (nom != null) {
			criteria.add(Restrictions.ilike("nom", "%" + nom + "%"));
		}
		if (prenom != null) {
			criteria.add(Restrictions.ilike("prenom", "%" + prenom + "%"));
		}
		
		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

	
	
	@Override
	public int count(String nom,String prenom) {
		Criteria criteria = getHibernateSession().createCriteria(
				SygCoordonateur.class);
		
		
		if (nom != null) {
			criteria.add(Restrictions.ilike("nom", "%" + nom + "%"));
		}
		if (prenom != null) {
			criteria.add(Restrictions.ilike("prenom", "%" + prenom + "%"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public List<SygCoordonateur> findAll(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygCoordonateur.class);

		if (indice > 0)
			criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);
		return criteria.list();
	}

	

	

}


