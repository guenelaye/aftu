package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class AutoriteContractanteSessionBean extends AbstractSessionBean implements AutoriteContractanteSession{

	@Override
	public int count(String libelle,String adresse,SygTypeAutoriteContractante type, String sigle, Long code) {
		Criteria criteria = getHibernateSession().createCriteria(SygAutoriteContractante.class);
		criteria.createAlias("type", "type");
		if(libelle!=null){
			criteria.add(Restrictions.ilike("denomination", "%"+libelle+"%"));
		}
		if(adresse!=null){
			criteria.add(Restrictions.ilike("adresse", "%"+adresse+"%"));
		}
		if(type!=null){
			criteria.add(Restrictions.eq("type", type));
		}
		if(sigle!=null){
			criteria.add(Restrictions.eq("sigle", sigle));
		}
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygAutoriteContractante.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygAutoriteContractante> find(int indice, int pas,String libelle,String adresse,SygTypeAutoriteContractante type, String sigle, Long code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAutoriteContractante.class);
		criteria.createAlias("type", "type");
		criteria.addOrder(Order.asc("denomination"));
		if(libelle!=null){
			criteria.add(Restrictions.ilike("denomination", "%"+libelle+"%"));
		}
		if(adresse!=null){
			criteria.add(Restrictions.ilike("adresse", "%"+adresse+"%"));
		}
		if(type!=null){
			criteria.add(Restrictions.eq("type", type));
		}
		if(sigle!=null){
			criteria.add(Restrictions.eq("sigle", sigle));
		}
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygAutoriteContractante autorite) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(autorite);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygAutoriteContractante autorite) {
		
		try{
			getHibernateSession().merge(autorite);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygAutoriteContractante findById(Long code) {
		return (SygAutoriteContractante)getHibernateSession().get(SygAutoriteContractante.class, code);
	}
	
	@Override
	public List<SygAutoriteContractante> find(int ordre) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAutoriteContractante.class);
		criteria.createAlias("type", "type");
		if(ordre>-1)
			criteria.add(Restrictions.eq("ordre", ordre));
		
		return criteria.list();
	}
	
	//Add by Rama 02-11-2012
	@Override
	public List<SygAutoriteContractante> findRech(int indice, int pas,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAutoriteContractante.class);
		criteria.createAlias("type", "type");
		criteria.addOrder(Order.asc("denomination"));
		if(libelle!=null){
			criteria.add(Restrictions.ilike("denomination", "%"+libelle+"%"));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public int countRech(String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygAutoriteContractante.class);
		criteria.createAlias("type", "type");
		if(libelle!=null){
			criteria.add(Restrictions.ilike("denomination", "%"+libelle+"%"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygAutoriteContractante> findRecouvrement(int indice, int pas, String statut,String denomination) {
		
		String libelle;
		if(denomination!=null)
		{
		libelle="autorite.denomination LIKE '%"+denomination+"%' and ";
		}
		else
		{
		libelle="";	
		}
		
       return getHibernateSession().createQuery("SELECT autorite FROM  SygAutoriteContractante autorite where "+libelle
	      + " autorite.id  IN (SELECT appeloffre.autorite.id from SygAppelsOffres appeloffre where appeloffre.apoStatut='"+statut+"'))")
			.setFirstResult(indice).setMaxResults(pas).list();	
	}
	
	@Override
	public int countRecouvrement (String statut,String denomination) {
		// TODO Auto-generated method stub
		
		String libelle;
		if(denomination!=null)
		{
		libelle="autorite.denomination LIKE '%"+denomination+"%' and ";
		}
		else
		{
		libelle="";	
		}
		  return getHibernateSession().createQuery("SELECT autorite FROM  SygAutoriteContractante autorite where "+libelle
			      + " autorite.id  IN (SELECT appeloffre.autorite.id from SygAppelsOffres appeloffre where appeloffre.apoStatut='"+statut+"'))")
					.list().size();		
	}

}
