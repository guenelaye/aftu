package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygTypeDecision;

@Remote
public interface TypeDecisionSession {
	
	public void save(SygTypeDecision type);
	public void delete(Long id);
	public List<SygTypeDecision> find(int indice, int pas,Long code,String libelle);
	public int count(Long code,String libelle);
	public void update(SygTypeDecision type);
	public SygTypeDecision findById(Long code);
	
}
