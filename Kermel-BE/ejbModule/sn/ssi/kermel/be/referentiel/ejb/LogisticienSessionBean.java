package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygLogisticien;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class LogisticienSessionBean extends AbstractSessionBean implements
		LogisticienSession {

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(SygLogisticien.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygLogisticien> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygLogisticien.class);
		List<SygLogisticien> fiche = criteria.list();
		return fiche;
	}


	@Override
	public SygLogisticien findByCode(Integer id) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygLogisticien.class);

		if (id != null) {
			criteria.add(Restrictions.eq("id", id));

		}
		return (SygLogisticien) criteria.list().get(0);
	}

	
	@Override
	public void save(SygLogisticien logist) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(logist);
			//getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void update(SygLogisticien logist) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().merge(logist);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygLogisticien> findAllby(int indice, int pas, String nom,String prenom) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygLogisticien.class);
		criteria.addOrder(Order.asc("nom"));
		if (nom != null) {
			criteria.add(Restrictions.ilike("nom", "%" + nom + "%"));
		}
		if (prenom != null) {
			criteria.add(Restrictions.ilike("prenom", "%" + prenom + "%"));
		}
		
		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

	
	
	@Override
	public int count(String nom,String prenom) {
		Criteria criteria = getHibernateSession().createCriteria(
				SygLogisticien.class);
		
		
		if (nom != null) {
			criteria.add(Restrictions.ilike("nom", "%" + nom + "%"));
		}
		if (prenom != null) {
			criteria.add(Restrictions.ilike("prenom", "%" + prenom + "%"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public List<SygLogisticien> findAll(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygLogisticien.class);

		if (indice > 0)
			criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);
		return criteria.list();
	}

	

	

}


