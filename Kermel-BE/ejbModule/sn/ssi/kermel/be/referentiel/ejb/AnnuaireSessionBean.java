package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAnnuaire;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class AnnuaireSessionBean extends AbstractSessionBean implements AnnuaireSession{

	@Override
	public int count(String prenom,String nom,String fonction,String service,String autorite,String organe) {
		Criteria criteria = getHibernateSession().createCriteria(SygAnnuaire.class);
		//criteria.setFetchMode("clients", FetchMode.SELECT);
		criteria.createAlias("service", "service");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		if(prenom!=null){
			criteria.add(Restrictions.ilike("prenom", "%"+prenom+"%"));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("nom", "%"+nom+"%"));
		}
		if(fonction!=null){
			criteria.add(Restrictions.ilike("fonction", "%"+fonction+"%"));
		}
		if(service!=null){
			criteria.add(Restrictions.ilike("service.libelle", "%"+service+"%"));
		}
		if(autorite!=null){
			criteria.add(Restrictions.ilike("autorite.denomination", "%"+autorite+"%"));
		}
		if(organe!=null){
			criteria.add(Restrictions.eq("organe", organe));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());


	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygAnnuaire.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygAnnuaire> find(int indice, int pas,String prenom,String nom,String fonction,String service,String autorite,String organe) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAnnuaire.class);
		criteria.createAlias("service", "service");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.addOrder(Order.asc("nom"));
		if(prenom!=null){
			criteria.add(Restrictions.ilike("prenom", "%"+prenom+"%"));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("nom", "%"+nom+"%"));
		}
		if(fonction!=null){
			criteria.add(Restrictions.ilike("fonction", "%"+fonction+"%"));
		}
		if(service!=null){
			criteria.add(Restrictions.ilike("service.libelle", "%"+service+"%"));
		}
		if(autorite!=null){
			criteria.add(Restrictions.ilike("autorite.denomination", "%"+autorite+"%"));
		}
		if(organe!=null){
			criteria.add(Restrictions.eq("organe", organe));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygAnnuaire annuaire) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(annuaire);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygAnnuaire annuaire) {
		
		try{
			getHibernateSession().merge(annuaire);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygAnnuaire findById(Long code) {
		return (SygAnnuaire)getHibernateSession().get(SygAnnuaire.class, code);
	}
}
