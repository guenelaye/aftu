package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;


import sn.ssi.kermel.be.entity.SygSpecialite;


/**
 * @author Coura Diagne, OLA Abdel Rakib, olarakib@gmail.com
 * @since Vendredi 12 F�vrier 2010, 13:27
 * @version 0.0.1
 */

@Remote
public interface SpecialiteSession {

	public void save(SygSpecialite spec);

	public void delete(Integer id);
	public void update(SygSpecialite spec);

	public List<SygSpecialite> findAll();

	public List<SygSpecialite> find(int indice, int pas, String code,
			String libelle);
	
	SygSpecialite findByCode(Integer id);

	int count(String code, String libelle);
	
}
