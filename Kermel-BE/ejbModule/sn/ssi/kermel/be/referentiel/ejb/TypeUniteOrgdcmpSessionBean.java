package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygTypeUniteOrgdcmp;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class TypeUniteOrgdcmpSessionBean extends AbstractSessionBean implements TypeUniteOrgdcmpSession{

	@Override
	public int count(Long code,String libelle, Long direction) {
		Criteria criteria = getHibernateSession().createCriteria(SygTypeUniteOrgdcmp.class);
		criteria.setFetchMode("typeuniteorg", FetchMode.SELECT);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		if(direction!=null){
			criteria.add(Restrictions.eq("typeuniteorg.id", direction));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygTypeUniteOrgdcmp.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygTypeUniteOrgdcmp> find(int indice, int pas,Long code,String libelle, Long direction) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTypeUniteOrgdcmp.class);
		criteria.setFetchMode("typeuniteorg", FetchMode.SELECT);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		if(direction!=null){
			criteria.add(Restrictions.eq("typeuniteorg.id", direction));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygTypeUniteOrgdcmp type) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(type);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygTypeUniteOrgdcmp type) {
		
		try{
			getHibernateSession().merge(type);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygTypeUniteOrgdcmp findById(Long code) {
		return (SygTypeUniteOrgdcmp)getHibernateSession().get(SygTypeUniteOrgdcmp.class, code);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygTypeUniteOrgdcmp> findDirection(int indice, int pas,Long code,String libelle,int niveau) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTypeUniteOrgdcmp.class);
		criteria.addOrder(Order.asc("id"));
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		
			criteria.add(Restrictions.eq("niveau", niveau));
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public int countDirection(Long code,String libelle,int niveau) {
		Criteria criteria = getHibernateSession().createCriteria(SygTypeUniteOrgdcmp.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(niveau==0){
			criteria.add(Restrictions.eq("niveau", niveau));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

}
