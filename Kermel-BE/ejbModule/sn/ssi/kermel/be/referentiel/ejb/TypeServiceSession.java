package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygTypeService;

@Remote
public interface TypeServiceSession {
	public void save(SygTypeService type);
	public void delete(Long id);
	public List<SygTypeService> find(int indice, int pas,Long code,String libelle);
	public int count(Long code,String libelle);
	public void update(SygTypeService type);
	public SygTypeService findById(Long code);
}
