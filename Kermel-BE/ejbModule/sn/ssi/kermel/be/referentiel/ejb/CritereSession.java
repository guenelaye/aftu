package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCritere;
import sn.ssi.kermel.be.entity.SygLots;


@Remote
public interface CritereSession {
	public SygCritere save(SygCritere critere);
	public void delete(Long id);
	public List<SygCritere> find(int indice, int pas,Long id,String libelle,SygAutoriteContractante autorite);
	public List<SygCritere> findAll();
	public int count(Long id,String libelle,SygAutoriteContractante autorite);
	public void update(SygCritere critere);
	public SygCritere findById(Long id);
	
	public List<SygCritere> ListeCriteres(int indice, int pas,String libelle,Long dossier,Long autorite, SygLots lot);
	public List<SygCritere> ListeCriteres(String libelle,Long dossier,Long autorite, SygLots lot);
	public List<Long> ListeCriteres(Long dossier, SygLots lot);
}
