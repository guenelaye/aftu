package sn.ssi.kermel.be.referentiel.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygPays;
import sn.ssi.kermel.be.entity.SygTaudit;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;
@Stateless
public class TauditSessionBean  extends AbstractSessionBean implements TauditSession{
	
	
	
	@Override
	public int count(String libelleaudit,String statut,Integer gestion,Date datestatut) {
		Criteria criteria= getHibernateSession().createCriteria(SygTaudit.class);
		if(libelleaudit!=null){
			criteria.add(Restrictions.ilike("libelleaudit", "%"+libelleaudit+"%"));
		}
		if(statut!=null){
			criteria.add(Restrictions.ilike("statut", "%"+statut+"%"));
		}
		if(gestion!=null){
			criteria.add(Restrictions.ilike("gestion", "%"+gestion+"%"));
		}
		if(datestatut!=null){
			criteria.add(Restrictions.ilike("datestatut", "%"+datestatut+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	

	@Override
	public void delete(Long idtaudit) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygTaudit.class, idtaudit));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygTaudit> find(int indice, int pas,String libelleaudit,String statut,Integer gestion,Date datestatut) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPays.class);
		
		if(libelleaudit!=null){
			criteria.add(Restrictions.ilike("libelleaudit", "%"+libelleaudit+"%"));
		}
		
		if(statut!=null){
			criteria.add(Restrictions.ilike("statut", "%"+statut+"%"));
		}
		if(gestion!=null){
			criteria.add(Restrictions.ilike("gestion", "%"+gestion+"%"));
		}
		if(datestatut!=null){
			criteria.add(Restrictions.ilike("datestatut", "%"+datestatut+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygTaudit taudit) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(taudit);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygTaudit taudit) {
		
		try{
			getHibernateSession().merge(taudit);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public SygTaudit findById(Long idtaudit ) {
		// TODO Auto-generated method stub
		return (SygTaudit)getHibernateSession().get(SygTaudit.class, idtaudit );
	}



	
	
	
}