
package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;


import sn.ssi.kermel.be.entity.SygFormateur;
import sn.ssi.kermel.be.entity.SygSpecialite;
import sn.ssi.kermel.be.entity.SygTypeFormateur;



@Remote
public interface FormateurSession {
	public void save(SygFormateur formateur);
	public List<SygFormateur> findAll(int indice, int pas);
	public void update(SygFormateur formateur);
	SygFormateur findByCode(Integer id);
	List<SygFormateur> findAllby(int indice, int pas, String nom,String prenom,String sexe,SygSpecialite specialite,SygTypeFormateur type);
	List<SygFormateur> findRaison(int indice, int pas, String raison,SygSpecialite specialite,SygTypeFormateur type);
	public void delete(Integer id);
	public int count(String nom,String prenom,String sexe,SygSpecialite specialite,SygTypeFormateur type);
	List<SygFormateur> findAll();
	public int count(Integer code, String prenom);

}
