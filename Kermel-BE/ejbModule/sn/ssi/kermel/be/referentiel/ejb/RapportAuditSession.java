package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygRapportAudit;

@Remote
public interface RapportAuditSession {
	
	public void save(SygRapportAudit rapportaudit);
	
	public void delete(Long id);
	
	public List<SygRapportAudit> find(int indice, int pas,Long code,String libelle);
	
	public int count(Long code,String libelle);
	
	public void update(SygRapportAudit rapportaudit);
	
	public SygRapportAudit findById(Long code);
	
}
