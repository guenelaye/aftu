package sn.ssi.kermel.be.referentiel.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;




import sn.ssi.kermel.be.entity.SygParticipantsFormation;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygFPresenceParcipant;
import sn.ssi.kermel.be.entity.SygFichePresence;
import sn.ssi.kermel.be.entity.SygFormationBail;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class FPresenceParticipantSessionBean extends AbstractSessionBean implements
		FPresenceParticipantSession {

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(SygFPresenceParcipant.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	
	@Override
	public List<SygFPresenceParcipant> find(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFPresenceParcipant.class);

		if (indice > 0)
			criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);
		return criteria.list();
	}

	@Override
	public List<SygFPresenceParcipant> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFPresenceParcipant.class);
		criteria.createAlias("paticipant", "paticipant");
		criteria.createAlias("fiche","fiche");
		
		return criteria.list();
	}

	@Override
	public void save(SygFPresenceParcipant fpart) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(fpart);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void update(SygFPresenceParcipant fpart) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().merge(fpart);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygFPresenceParcipant> find(int indice, int pas,SygParticipantsFormation participant, SygFichePresence fiche) {
	
		Criteria criteria = getHibernateSession().createCriteria(
				SygFPresenceParcipant.class);
		criteria.createAlias("paticipant", "paticipant");
		criteria.createAlias("fiche","fiche");
		
		criteria.addOrder(Order.desc("fiche.dateFiche"));
//		
//		if (dateDebu != null && dateFin != null) {
//			criteria.add(Restrictions.between("date", dateDebu, dateFin));
//		}
			
		if (participant != null) {
			criteria.add(Restrictions.eq("paticipant", participant));
		}
		if (fiche != null) {
			criteria.add(Restrictions.eq("fiche", fiche));
		}

		criteria.setProjection(Projections.groupProperty("fiche"));
		
		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygFPresenceParcipant> findAffListe(int indice, int pas,SygParticipantsFormation participant, SygFichePresence fiche) {
	
		Criteria criteria = getHibernateSession().createCriteria(
				SygFPresenceParcipant.class);
		criteria.createAlias("paticipant", "paticipant");
		criteria.createAlias("fiche","fiche");
		
		criteria.addOrder(Order.desc("paticipant.nom"));
		if (participant != null) {
			criteria.add(Restrictions.eq("paticipant", participant));
		}
		if (fiche != null) {
			criteria.add(Restrictions.eq("fiche", fiche));
		}
	
		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}
	
	
	
	@Override
	public int count(SygParticipantsFormation participant, SygFichePresence fiche) {
		Criteria criteria = getHibernateSession().createCriteria(
				SygFPresenceParcipant.class);
		
		if (participant != null) {
			criteria.add(Restrictions.eq("paticipant", participant));
		}
		if (fiche != null) {
			criteria.add(Restrictions.eq("fiche", fiche));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	
	@Override
	public SygFPresenceParcipant findByCode(Long id) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFPresenceParcipant.class);
		criteria.createAlias("paticipant", "paticipant");
		criteria.createAlias("fiche","fiche");
		if (id != 0) {
			criteria.add(Restrictions.eq("id", id));

		}
		return (SygFPresenceParcipant) criteria.list().get(0);
	}

	
	@Override
	public SygFPresenceParcipant findByFiche(SygFichePresence fiche) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFPresenceParcipant.class);
		criteria.createAlias("paticipant", "paticipant");
		criteria.createAlias("fiche","fiche");
		if (fiche != null) {
			criteria.add(Restrictions.eq("fiche", fiche));

		}
		return (SygFPresenceParcipant) criteria.list().get(0);
	}


}
