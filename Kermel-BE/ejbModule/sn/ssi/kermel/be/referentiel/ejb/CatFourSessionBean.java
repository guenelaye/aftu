package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygCatFournisseur;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class CatFourSessionBean extends AbstractSessionBean implements CatFourSession{

	@Override
	public int count(String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygCatFournisseur.class);
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygCatFournisseur.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygCatFournisseur> find(int indice, int pas, String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCatFournisseur.class);
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygCatFournisseur cat) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(cat);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygCatFournisseur cat) {
		
		try{
			getHibernateSession().merge(cat);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygCatFournisseur findById(Long code) {
		return (SygCatFournisseur)getHibernateSession().get(SygCatFournisseur.class, code);
	}
}
