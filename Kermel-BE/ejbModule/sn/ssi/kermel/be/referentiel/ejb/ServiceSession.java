package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygService;

@Remote
public interface ServiceSession {
	public void save(SygService service);
	public void delete(Long id);
	public List<SygService> find(int indice, int pas,String code,String libelle,SygAutoriteContractante autorite, Long id);
	public int count(String code,String libelle,SygAutoriteContractante autorite, Long id);
	public void update(SygService service);
	public SygService findById(Long code);
}
