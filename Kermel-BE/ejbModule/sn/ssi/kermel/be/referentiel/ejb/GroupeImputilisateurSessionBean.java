package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;


//import sn.ssi.ecourrier.be.entity.EcoCaracteresCourriers;
//import sn.ssi.ecourrier.be.entity.EcoDossiers;
import sn.ssi.kermel.be.entity.EcoGroupesImputation;
import sn.ssi.kermel.be.entity.EcoGroupesImputilisateur;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class GroupeImputilisateurSessionBean extends AbstractSessionBean implements GroupeImpututilisateurSession{

	@Override
	public int count(String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(EcoGroupesImputilisateur.class);
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(EcoGroupesImputilisateur.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<EcoGroupesImputilisateur> find(int indice, int pas, String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(EcoGroupesImputilisateur.class);
		criteria.addOrder(Order.desc("libelle"));
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(EcoGroupesImputilisateur GroupesImputilisateur) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(GroupesImputilisateur);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(EcoGroupesImputilisateur GroupesImputilisateur) {
		
		try{
			getHibernateSession().merge(GroupesImputilisateur);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
//	@Override
//	public EcoGroupesImputation findById(Long code) {
//		return (EcoDossiers)getHibernateSession().get(EcoGroupesImputation.class, code);
//	}
	
	
	@Override
	public List<EcoGroupesImputilisateur> find(String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(EcoGroupesImputilisateur.class);
		if(libelle!=null)
			criteria.add(Restrictions.eq("libelle", libelle));
		
		return criteria.list();
	}

@Override
public EcoGroupesImputilisateur findById(Long code) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public List<EcoGroupesImputilisateur> findByGroup(
		EcoGroupesImputation GroupesImputation) {
	// TODO Auto-generated method stub
	return null;
}
}
