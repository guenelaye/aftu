package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygMembreCellulePassation;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class MembreCellulePassationSessionBean extends AbstractSessionBean implements
	MembreCellulePassationSession {

	@Override
	public void save(SygMembreCellulePassation membre) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(membre);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	
	}

	@Override
	public void update(SygMembreCellulePassation membre) {
		// TODO Auto-generated method stub
		try{
			getHibernateSession().merge(membre);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygMembreCellulePassation.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public List<SygMembreCellulePassation> find(int indice, int pas, String nom,
			String prenom, SygAutoriteContractante autorite) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygMembreCellulePassation.class);
		criteria.addOrder(Order.asc("nom"));
		
		if(nom!=null)
			criteria.add(Restrictions.ilike("nom", "%"+nom+"%"));
		
		if(prenom!=null)
			criteria.add(Restrictions.ilike("prenom", "%"+prenom+"%"));
		
		if(autorite!=null)
			criteria.add(Restrictions.eq("autorite", autorite));
		
		criteria.setFirstResult(indice);
		
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public int count(String nom, String prenom,
			SygAutoriteContractante autorite) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygMembreCellulePassation.class);
		
		if(nom!=null)
			criteria.add(Restrictions.ilike("nom", "%"+nom+"%"));
		
		if(prenom!=null)
			criteria.add(Restrictions.ilike("prenom", "%"+prenom+"%"));
		
		if(autorite!=null)
			criteria.add(Restrictions.eq("autorite", autorite));
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public SygMembreCellulePassation findById(Long code) {
		// TODO Auto-generated method stub
		return (SygMembreCellulePassation)getHibernateSession().get(SygMembreCellulePassation.class, code);
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygMembreCellulePassation > ListeMembres(int indice, int pas, String nom,Long dossier,Long autorite) {
		String libellesaisi;
		if(nom!=null)
		{
		libellesaisi="membres.nom LIKE '%"+nom+"%' and ";
		}
		else
		{
		libellesaisi="";	
		}
		
		return getHibernateSession().createQuery( "SELECT membres FROM  SygMembreCellulePassation membres where " +libellesaisi
			+ " membres.autorite.id='"+autorite+"' and  membres.id NOT IN (SELECT dosiers.membre.id from SygDossierscommissionscellules dosiers where  dosiers.dossier.dosID='" + dossier + "' " +")")
				.setFirstResult(indice).setMaxResults(pas).list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygMembreCellulePassation > ListeMembres(String nom,Long dossier,Long autorite) {
		String libellesaisi;
		if(nom!=null)
		{
		libellesaisi="membres.nom LIKE '%"+nom+"%' and ";
		}
		else
		{
		libellesaisi="";	
		}
		
		return getHibernateSession().createQuery( "SELECT membres FROM  SygMembreCellulePassation membres where " +libellesaisi
				+ " membres.autorite.id='"+autorite+"' and  membres.id NOT IN (SELECT dosiers.membre.id from SygDossierscommissionscellules dosiers where  dosiers.dossier.dosID='" + dossier + "' " +")")
					.list();
	}
}
