package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygNatureprix;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class NatureprixSessionBean extends AbstractSessionBean implements NatureprixSession{

	@Override
	public int count(String code,String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygNatureprix.class);
		if(code!=null){
			criteria.add(Restrictions.ilike("natCode", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("natLibelle", "%"+libelle+"%"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygNatureprix.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygNatureprix> find(int indice, int pas,String code,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygNatureprix.class);
		criteria.addOrder(Order.desc("natCode"));
		if(code!=null){
			criteria.add(Restrictions.ilike("natCode", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("natLibelle", "%"+libelle+"%"));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygNatureprix nature) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(nature);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygNatureprix nature) {
		
		try{
			getHibernateSession().merge(nature);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygNatureprix findById(Long code) {
		return (SygNatureprix)getHibernateSession().get(SygNatureprix.class, code);
	}
	
	
	@Override
	public List<SygNatureprix> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygNatureprix.class);
		if(code!=null)
			criteria.add(Restrictions.eq("natCode", code));
		
		return criteria.list();
	}
}
