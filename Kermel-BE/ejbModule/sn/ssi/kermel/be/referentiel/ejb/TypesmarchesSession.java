package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygTypesmarches;

@Remote
public interface TypesmarchesSession {
	public void save(SygTypesmarches type);
	public void delete(Long id);
	public List<SygTypesmarches> find(int indice, int pas,String code,String libelle, Long type);
	public int count(String code,String libelle, Long type);
	public void update(SygTypesmarches type);
	public SygTypesmarches findById(Long code);
	public List<SygTypesmarches> find(String code);
}
