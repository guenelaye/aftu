package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygTypeUniteOrgarmp;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class TypeUniteOrgarmpSessionBean extends AbstractSessionBean implements TypeUniteOrgarmpSession{

	@Override
	public int count(Long code,String libelle, Long direction) {
		Criteria criteria = getHibernateSession().createCriteria(SygTypeUniteOrgarmp.class);
		criteria.setFetchMode("typeuniteorg", FetchMode.SELECT);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		if(direction!=null){
			criteria.add(Restrictions.eq("typeuniteorg.id", direction));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygTypeUniteOrgarmp.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygTypeUniteOrgarmp> find(int indice, int pas,Long code,String libelle, Long direction) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTypeUniteOrgarmp.class);
		criteria.setFetchMode("typeuniteorg", FetchMode.SELECT);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		if(direction!=null){
			criteria.add(Restrictions.eq("typeuniteorg.id", direction));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygTypeUniteOrgarmp type) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(type);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygTypeUniteOrgarmp type) {
		
		try{
			getHibernateSession().merge(type);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygTypeUniteOrgarmp findById(Long code) {
		return (SygTypeUniteOrgarmp)getHibernateSession().get(SygTypeUniteOrgarmp.class, code);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygTypeUniteOrgarmp> findDirection(int indice, int pas,Long code,String libelle,int niveau) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTypeUniteOrgarmp.class);
		criteria.addOrder(Order.asc("id"));
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		criteria.add(Restrictions.eq("niveau", niveau));
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public int countDirection(Long code,String libelle,int niveau) {
		Criteria criteria = getHibernateSession().createCriteria(SygTypeUniteOrgarmp.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(niveau==0){
			criteria.add(Restrictions.eq("niveau", niveau));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

}
