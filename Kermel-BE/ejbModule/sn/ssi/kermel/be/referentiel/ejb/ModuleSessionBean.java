package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygModule;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class ModuleSessionBean extends AbstractSessionBean implements ModuleSession {

	@Override
	public void save(SygModule module) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(module);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	
	}

	@Override
	public void update(SygModule module) {
		// TODO Auto-generated method stub
		try{
			getHibernateSession().merge(module);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygModule.class, id));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	

	@Override
	public List<SygModule> find(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygModule.class);
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public List<SygModule> find(int indice, int pas,
			String code,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygModule.class);
		criteria.addOrder(Order.asc("libelle"));
		if(code!=null)
			criteria.add(Restrictions.ilike("code","%"+code+"%"));
	
		if(libelle!=null)
			criteria.add(Restrictions.ilike("libelle","%"+libelle+"%"));
						
		criteria.setFirstResult(indice);
		
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public List<SygModule> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygModule.class);
		criteria.addOrder(Order.asc("libelle"));
		
		return criteria.list();
	}

	@Override
	public SygModule findByCode(Integer id) {
		// TODO Auto-generated method stub
		return (SygModule)getHibernateSession().get(SygModule.class, id);
	}

	
	
	@Override
	public int count() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygModule.class);
		criteria.setProjection(Projections.rowCount());
				
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public int count(String code,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygModule.class);
		criteria.setProjection(Projections.rowCount());
		
		if(code!=null)
			criteria.add(Restrictions.ilike("code","%"+code+"%"));
	
		if(libelle!=null)
			criteria.add(Restrictions.ilike("libelle","%"+libelle+"%"));
				
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
}
