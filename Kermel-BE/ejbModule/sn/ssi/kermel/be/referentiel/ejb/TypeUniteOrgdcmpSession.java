package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygTypeUniteOrgdcmp;

@Remote
public interface TypeUniteOrgdcmpSession {
	public void save(SygTypeUniteOrgdcmp type);
	
	public void delete(Long id);
	
	public List<SygTypeUniteOrgdcmp> find(int indice, int pas,Long code,String libelle, Long direction);
	
	public int count(Long code,String libelle, Long direction);
	
	public void update(SygTypeUniteOrgdcmp type);
	
	public SygTypeUniteOrgdcmp findById(Long code);
	
	
	public List<SygTypeUniteOrgdcmp> findDirection(int indice, int pas,Long code,String libelle,int niveau);
	public int countDirection(Long code,String libelle,int niveau);
}
