package sn.ssi.kermel.be.referentiel.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAnnee;
import sn.ssi.kermel.be.entity.SygMois;
import sn.ssi.kermel.be.entity.SygProformation;

@Remote
public interface ProformationSession {
	public void save(SygProformation formation);
	public void delete(Long id);
	public List<SygProformation> find(int indice, int pas,Date forDateDebut,Date forDateFin,String forlibelle,String forRef);
	public int count(Date forDateDebut,SygMois mois,SygAnnee sygannee,int publie);
	public void update(SygProformation formation);
	public SygProformation findById(Long code);
	public List<SygProformation> find(String code);
	List<SygProformation> find(int indice, int pas);
	int count();
	public List<SygProformation> find(int indice, int pas, String mois,String forlibelle,String forRef,String annee);
	List<SygProformation> find(int indice, int pas, Long code, Date forDateDebut, Date forDateFin, String forlibelle, String forRef, String annee, SygAnnee sygannee, SygMois mois,int publie);
	int count(Long code, Date forDateDebut, Date forDateFin, String forlibelle, String forRef, String annee, SygAnnee sygannee, SygMois mois,int publie);
}

