package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygModeselection;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class ModeselectionSessionBean extends AbstractSessionBean implements ModeselectionSession{

	@Override
	public int count(String code,String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygModeselection.class);
		if(code!=null){
			criteria.add(Restrictions.ilike("code", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygModeselection.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygModeselection> find(int indice, int pas,String code,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygModeselection.class);
		criteria.addOrder(Order.desc("code"));
		if(code!=null){
			criteria.add(Restrictions.ilike("code", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygModeselection mode) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(mode);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygModeselection mode) {
		
		try{
			getHibernateSession().merge(mode);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygModeselection findById(Long code) {
		return (SygModeselection)getHibernateSession().get(SygModeselection.class, code);
	}
	
	
	@Override
	public List<SygModeselection> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygModeselection.class);
		if(code!=null)
			criteria.add(Restrictions.eq("code", code));
		
		return criteria.list();
	}
}
