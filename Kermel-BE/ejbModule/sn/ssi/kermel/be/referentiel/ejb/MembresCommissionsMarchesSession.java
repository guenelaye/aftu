package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygArretesMembresCommissionsMarches;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygMembresCommissionsMarches;

@Remote
public interface MembresCommissionsMarchesSession {
	public void save(SygMembresCommissionsMarches membre);
	public void delete(Long id);
	public List<SygMembresCommissionsMarches> find(int indice, int pas,String prenom,String nom,SygAutoriteContractante autorite,String gestion, int responsable,int etape);
	public int count(String prenom,String nom,SygAutoriteContractante autorite,String gestion, int responsable,int etape);
	public void update(SygMembresCommissionsMarches membre);
	public SygMembresCommissionsMarches findById(Long code);
	public List<SygMembresCommissionsMarches> ListeMembres(int indice, int pas,String nom,Long dossier,Long autorite);
	public List<SygMembresCommissionsMarches> ListeMembres(String nom,Long dossier,Long autorite);
	
	
	/////////Arrete///
	public void saveArrete(SygArretesMembresCommissionsMarches arrete);
	public void updateArrete(SygArretesMembresCommissionsMarches arrete);
	public SygArretesMembresCommissionsMarches findArrete(SygAutoriteContractante autorite,String gestion);

}
