package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygPiecesrecus;

@Remote
public interface PiecesrecusSession {
	public void save(SygPiecesrecus piece);
	public void delete(Long id);
	public List<SygPiecesrecus> find(int indice, int pas,Long code,String libelle,int garantie);
	public int count(Long code,String libelle,int garantie);
	public void update(SygPiecesrecus piece);
	public SygPiecesrecus findById(Long code);
}
