package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygConfigurationTaux;
import sn.ssi.kermel.be.entity.SysParametresGeneraux;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

/**
 * @author Ramatoulaye Sylla
 * @since Vendredi 04 Avril 2013, 10:20
 * 
 */
public @Stateless class ConfigurationTauxSessionBean extends AbstractSessionBean implements ConfigurationTauxSession {


	@Override
	public SygConfigurationTaux findById(Long code) {
		return (SygConfigurationTaux)getHibernateSession().get(SygConfigurationTaux.class, code);
	}
	
	@Override
	public void update(SygConfigurationTaux parametre) {
		
		try{
			getHibernateSession().merge(parametre);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygConfigurationTaux> find(int indice, int pas, String code,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygConfigurationTaux.class);
		criteria.createAlias("pays", "pays");
		if(code!=null){
			criteria.add(Restrictions.ilike("code", "%"+code+"%"));
		}
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("pays.libelle", "%"+libelle+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public int count(String code,String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygConfigurationTaux.class);
		
		criteria.createAlias("pays", "pays");
		if(code!=null){
			criteria.add(Restrictions.ilike("code", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("pays.libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygConfigurationTaux.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Override
	public void save(SygConfigurationTaux taux) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(taux);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}
}