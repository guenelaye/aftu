package sn.ssi.kermel.be.referentiel.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;


import sn.ssi.kermel.be.entity.SygTaudit;


@Remote
public interface TauditSession {
	public void save(SygTaudit  taudit);
	public void delete(Long idtaudit);
	public List<SygTaudit> find(int indice, int pas,String libelleaudit,String statut,Integer gestion,Date datestatut);
	public int count(String libelleaudit,String statut,Integer gestion,Date datestatut);
	public void update(SygTaudit  taudit);
	public SygTaudit findById(Long idtaudit);
	

}
