package sn.ssi.kermel.be.referentiel.ejb;

import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class SecteursactivitesSessionBean extends AbstractSessionBean implements SecteursactivitesSession{

	@EJB
	ParametresGenerauxSession ParametresGenerauxSession;
	
	@Override
	public int count(String code,String libelle,SygSecteursactivites secteur) {
		Criteria criteria = getHibernateSession().createCriteria(SygSecteursactivites.class);
		if(code!=null){
			criteria.add(Restrictions.ilike("code", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(secteur!=null){
			criteria.add(Restrictions.eq("secteur", secteur));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygSecteursactivites.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygSecteursactivites> find(int indice, int pas,String code,String libelle,SygSecteursactivites secteur) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygSecteursactivites.class);
		criteria.addOrder(Order.asc("code"));
		if(code!=null){
			criteria.add(Restrictions.ilike("code", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(secteur!=null){
			criteria.add(Restrictions.eq("secteur", secteur));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygSecteursactivites type,int parent) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(type);
			getHibernateSession().flush();
			if(parent==BeConstants.NPARENT)
			  ParametresGenerauxSession.incrementeCode(BeConstants.PARAM_SECTEURACTIVITE);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygSecteursactivites type) {
		
		try{
			getHibernateSession().merge(type);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygSecteursactivites findById(String code) {
		return (SygSecteursactivites)getHibernateSession().get(SygSecteursactivites.class, code);
	}
	
	
	@Override
	public String getGeneratedCode(String code,String codeParametreGeneral,int parent) {
		// TODO Auto-generated method stub
		Long codeParametre;
		String dateCoupee = (Calendar.getInstance().getTime()).toString();
		String mois = String.format("%02d", Calendar.getInstance().get(Calendar.MONTH)+1);
		dateCoupee = dateCoupee.substring(dateCoupee.length() - 4, dateCoupee
				.length());
		if(parent==BeConstants.NPARENT)
		  codeParametre = (long) ParametresGenerauxSession.getValeurParametre(codeParametreGeneral);
		else
			codeParametre=(long) parent;
		if (codeParametre < 10)
			return code+ "00" + codeParametre;
		else if ((codeParametre >= 10) && (codeParametre < 100))
			return code + "0" + codeParametre;
		else
			return code  + codeParametre;
		
//		
//		String dateCoupee = (Calendar.getInstance().getTime()).toString();
//		String mois = String.format("%02d", Calendar.getInstance().get(Calendar.MONTH)+1);
//		dateCoupee = dateCoupee.substring(dateCoupee.length() - 4, dateCoupee
//				.length());
//		Long codeParametre = (long) ParametresGenerauxSession.getValeurParametre(codeParametreGeneral);
//		if (codeParametre < 10)
//			return dateCoupee +mois+ "000" + codeParametre;
//		else if ((codeParametre >= 10) && (codeParametre < 100))
//			return dateCoupee+mois + "00" + codeParametre;
//		else if ((codeParametre >= 100) && (codeParametre < 1000))
//			return dateCoupee+mois + "0" + codeParametre;
//		else
//			return dateCoupee+mois + "" + codeParametre;
	}
}
