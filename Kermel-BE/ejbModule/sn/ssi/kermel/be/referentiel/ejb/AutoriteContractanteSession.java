package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;

@Remote
public interface AutoriteContractanteSession {
	public void save(SygAutoriteContractante type);
	public void delete(Long id);
	public List<SygAutoriteContractante> find(int indice, int pas,String libelle,String adresse,SygTypeAutoriteContractante type, String sigle, Long code);
	public int count(String libelle,String adresse,SygTypeAutoriteContractante type, String sigle, Long code);
	public void update(SygAutoriteContractante type);
	public SygAutoriteContractante findById(Long code);
	public List<SygAutoriteContractante> find(int ordre);
	
  //Add by Rama 02-11-2012
	public List<SygAutoriteContractante> findRech(int indice, int pas,String libelle);
	public int countRech(String libelle);
	
	public List<SygAutoriteContractante> findRecouvrement(int indice, int pas,String statut,String denomination);
	public int countRecouvrement (String statut,String denomination);
}
