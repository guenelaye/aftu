package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.TypeCategorie;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class TypeCategoriSessionBean extends AbstractSessionBean implements TypeCategoriSession{

	@Override
	public int count(Long code,String libelle, Long typecat) {
		Criteria criteria = getHibernateSession().createCriteria(TypeCategorie.class);
		criteria.setFetchMode("typCategorie", FetchMode.SELECT);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		if(typecat!=null){
			criteria.add(Restrictions.eq("typCategorie.id", typecat));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(TypeCategorie.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TypeCategorie> find(int indice, int pas,Long code,String libelle, Long typecat) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(TypeCategorie.class);
		criteria.setFetchMode("typCategorie", FetchMode.SELECT);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		if(typecat!=null){
			criteria.add(Restrictions.eq("typCategorie.id", typecat));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(TypeCategorie type) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(type);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(TypeCategorie type) {
		
		try{
			getHibernateSession().merge(type);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public TypeCategorie findById(Long code) {
		return (TypeCategorie)getHibernateSession().get(TypeCategorie.class, code);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TypeCategorie> findAutreTypeCat(int indice, int pas,Long code,String libelle,int niveau) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(TypeCategorie.class);
		criteria.addOrder(Order.asc("id"));
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		criteria.add(Restrictions.eq("niveau", niveau));
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public int countAutreTypeCat(Long code,String libelle,int niveau) {
		Criteria criteria = getHibernateSession().createCriteria(TypeCategorie.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(niveau==0){
			criteria.add(Restrictions.eq("niveau", niveau));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

}
