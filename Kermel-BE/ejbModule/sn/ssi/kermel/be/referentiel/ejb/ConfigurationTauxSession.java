package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygConfigurationTaux;
/**
 * @author Ramatoulaye Sylla
 * @since Vendredi 04 Avril 2013, 10:20
 * 
 */
@Remote
public interface ConfigurationTauxSession {
	
	SygConfigurationTaux findById(Long code);
	public void update(SygConfigurationTaux conftaux);
	public List<SygConfigurationTaux> find(int indice, int pas,String code,String libelle);
	
	public void save(SygConfigurationTaux type);
	public void delete(Long id);
	public int count(String code,String libelle);
}
