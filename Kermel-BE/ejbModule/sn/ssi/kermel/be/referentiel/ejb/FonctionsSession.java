package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygFonctions;

@Remote
public interface FonctionsSession {
	public void save(SygFonctions fonction);
	public void delete(Long id);
	public List<SygFonctions> find(int indice, int pas,Long code,String libelle);
	public int count(Long code,String libelle);
	public void update(SygFonctions fonction);
	public SygFonctions findById(Long code);
}
