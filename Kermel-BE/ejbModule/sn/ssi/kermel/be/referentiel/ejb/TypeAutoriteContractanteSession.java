package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;

@Remote
public interface TypeAutoriteContractanteSession {
	public void save(SygTypeAutoriteContractante type);
	public void delete(Long id);
	public List<SygTypeAutoriteContractante> find(int indice, int pas,String code,String libelle);
	public int count(String code,String libelle);
	public void update(SygTypeAutoriteContractante type);
	public SygTypeAutoriteContractante findById(Long code);
	public List<SygTypeAutoriteContractante> find(int ordre);
}
