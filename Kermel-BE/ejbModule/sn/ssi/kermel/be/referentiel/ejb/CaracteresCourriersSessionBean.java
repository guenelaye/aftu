package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.EcoCaracteresCourriers;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class CaracteresCourriersSessionBean extends AbstractSessionBean implements CaracteresCourriersSession{

	@Override
	public int count(String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(EcoCaracteresCourriers.class);
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(EcoCaracteresCourriers.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<EcoCaracteresCourriers> find(int indice, int pas, String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(EcoCaracteresCourriers.class);
		criteria.addOrder(Order.desc("libelle"));
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(EcoCaracteresCourriers caractere) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(caractere);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(EcoCaracteresCourriers caractere) {
		
		try{
			getHibernateSession().merge(caractere);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public EcoCaracteresCourriers findById(Long code) {
		return (EcoCaracteresCourriers)getHibernateSession().get(EcoCaracteresCourriers.class, code);
	}
	
	
	@Override
	public List<EcoCaracteresCourriers> find(String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(EcoCaracteresCourriers.class);
		if(libelle!=null)
			criteria.add(Restrictions.eq("libelle", libelle));
		
		return criteria.list();
	}
}
