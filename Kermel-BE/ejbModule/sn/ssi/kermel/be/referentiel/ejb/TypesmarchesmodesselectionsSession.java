package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygModeselection;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.be.entity.SygTypesmarchesmodesselections;

@Remote
public interface TypesmarchesmodesselectionsSession {
	public void save(SygTypesmarchesmodesselections mode);
	public void delete(Long id);
	public List<SygTypesmarchesmodesselections> find(int indice, int pas,String libelle,SygTypesmarches type,SygModeselection mode);
	public int count(String libelle,SygTypesmarches type,SygModeselection mode);
	public void update(SygTypesmarchesmodesselections mode);
	public SygTypesmarchesmodesselections findById(Long code);
	public List<SygModeselection> ListesModes(int indice, int pas,String libelle,Long type);
	public List<SygModeselection> ListesModes(String libelle,Long type);
}
