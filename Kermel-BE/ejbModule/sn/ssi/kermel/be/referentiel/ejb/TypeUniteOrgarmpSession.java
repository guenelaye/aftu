package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygTypeUniteOrgarmp;

@Remote
public interface TypeUniteOrgarmpSession {
	public void save(SygTypeUniteOrgarmp type);
	
	public void delete(Long id);
	
	public List<SygTypeUniteOrgarmp> find(int indice, int pas,Long code,String libelle, Long direction);
	
	public int count(Long code,String libelle, Long direction);
	
	public void update(SygTypeUniteOrgarmp type);
	
	public SygTypeUniteOrgarmp findById(Long code);
	
	
	public List<SygTypeUniteOrgarmp> findDirection(int indice, int pas,Long code,String libelle,int niveau);
	public int countDirection(Long code,String libelle,int niveau);
}
