package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygUniteOrgarmp;

@Remote
public interface UniteOrgarmpSession {
	public void save(SygUniteOrgarmp uniteorg);
	public void delete(Long id);
	public List<SygUniteOrgarmp> find(int indice, int pas,String libelle,String division);
	public int count(String libelle,String division);
	public void update(SygUniteOrgarmp uniteorg);
	public SygUniteOrgarmp findById(Long code);
	
	public List<SygUniteOrgarmp> findRech(int indice, int pas,String libelle,Long division, Long uniteorg);
	public int countRech(String libelle,Long division, Long uniteorg);
}
