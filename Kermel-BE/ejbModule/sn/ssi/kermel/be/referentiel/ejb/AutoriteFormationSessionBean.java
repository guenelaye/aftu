package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;




import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygFormationAutorite;
import sn.ssi.kermel.be.entity.SygFormationGroupe;
import sn.ssi.kermel.be.entity.SygGroupeFormation;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class AutoriteFormationSessionBean extends AbstractSessionBean implements
AutoriteFormationSession {

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(SygFormationAutorite.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	
	@Override
	public List<SygFormationAutorite> find(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormationAutorite.class);

		if (indice > 0)
			criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);
		return criteria.list();
	}

	@Override
	public List<SygFormationAutorite> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormationAutorite.class);
		return criteria.list();
	}

	@Override
	public void save(SygFormationAutorite fauto) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(fauto);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void update(SygFormationAutorite fauto) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().merge(fauto);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygFormationAutorite> find(int indice, int pas,SygProformation formation, SygAutoriteContractante autorite) {
	
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormationAutorite.class);
		criteria.createAlias("formation", "formation");
		criteria.createAlias("autorite","autorite");
		criteria.createAlias("type","type");
	
		criteria.addOrder(Order.asc("id"));
		
		if (formation != null) {
			criteria.add(Restrictions.eq("formation", formation));
		}
		if (autorite != null) {
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

	@Override
	public int count(SygProformation formation, SygAutoriteContractante autorite) {
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormationAutorite.class);
		if (formation != null) {
			criteria.add(Restrictions.eq("formation", formation));
		}
		if (autorite != null) {
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	
	@Override
	public SygFormationAutorite findByCode(Integer id) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormationAutorite.class);
		criteria.createAlias("formation", "formation");
		criteria.createAlias("autorite","autorite");
		if (id != 0) {
			criteria.add(Restrictions.eq("id", id));

		}
		return (SygFormationAutorite) criteria.list().get(0);
	}


	@Override
	public void deleteGroupe(Integer id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(SygFormationGroupe.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Override
	public SygFormationGroupe findGroupe(Integer id) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormationGroupe.class);
		criteria.createAlias("formation", "formation");
		criteria.createAlias("groupe", "groupe");
		if (id != 0) {
			criteria.add(Restrictions.eq("id", id));

		}
		return (SygFormationGroupe) criteria.list().get(0);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygFormationGroupe> findGroupe(int indice, int pas, SygProformation formation,SygGroupeFormation groupe) {
	
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormationGroupe.class);
		criteria.createAlias("formation", "formation");
		criteria.createAlias("groupe", "groupe");
	
		criteria.addOrder(Order.asc("id"));
		
		if (formation != null) {
			criteria.add(Restrictions.eq("formation", formation));
		}
		if (groupe != null) {
			criteria.add(Restrictions.eq("groupe", groupe));
		}
		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}
	
	@Override
	public int countGroupe(SygProformation formation,SygGroupeFormation groupe) {
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormationGroupe.class);
		criteria.createAlias("formation", "formation");
		criteria.createAlias("groupe", "groupe");
	
		if (formation != null) {
			criteria.add(Restrictions.eq("formation", formation));
		}
		if (groupe != null) {
			criteria.add(Restrictions.eq("groupe", groupe));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
	@Override
	public void saveGroupe(SygFormationGroupe groupe) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(groupe);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void updateGroupe(SygFormationGroupe groupe) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().merge(groupe);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygFormationGroupe> findGroupes(int indice, int pas) {
	
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormationGroupe.class);
		criteria.createAlias("formation", "formation");
		criteria.createAlias("groupe", "groupe");
	
		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}
}
