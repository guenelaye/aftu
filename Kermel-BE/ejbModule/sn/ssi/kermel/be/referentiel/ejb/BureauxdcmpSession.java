package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygBureauxdcmp;

@Remote
public interface BureauxdcmpSession {
	public void save(SygBureauxdcmp bureau);
	public void delete(Long id);
	public List<SygBureauxdcmp> find(int indice, int pas,String libelle,String division);
	public int count(String libelle,String division);
	public void update(SygBureauxdcmp bureau);
	public SygBureauxdcmp findById(Long code);
	
	public List<SygBureauxdcmp> findRech(int indice, int pas,String libelle,Long division, Long uniteorg);
	public int countRech(String libelle,Long division, Long uniteorg);
	
	public List<SygBureauxdcmp> findRech1(int indice, int pas,String libelle, Long uniteorg);
}
