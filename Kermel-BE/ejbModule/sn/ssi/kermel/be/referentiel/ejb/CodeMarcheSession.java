package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCodeMarche;
import sn.ssi.kermel.be.entity.SygRapportARMP;

@Remote
public interface CodeMarcheSession {
	
	public void save(SygCodeMarche rapportarmp);
	
	public void delete(Long id);
	
	public List<SygCodeMarche> find(int indice, int pas,Long code,String libelle);
	
	public int count(Long code,String libelle);
	
	public void update(SygCodeMarche rapportarmp);
	
	public SygCodeMarche findById(Long code);
	
}
