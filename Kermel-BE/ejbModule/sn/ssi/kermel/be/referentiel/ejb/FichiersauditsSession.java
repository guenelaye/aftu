package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygFichiersaudits;



@Remote
public interface FichiersauditsSession {
	public void save(SygFichiersaudits fichier);
	public void delete(Long idfichiersaudits);
	public List<SygFichiersaudits> find(int indice, int pas,String nomfichierpdf,String libellefichiersaudits);
	public int count(String nomfichierpdf,String libellefichiersaudits);
	public void update(SygFichiersaudits Fichier);
	public SygFichiersaudits findById(Long idfichiersaudits);
	

}
