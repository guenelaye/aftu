package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygModepassation;

@Remote
public interface ModepassationSession {
	public void save(SygModepassation mode);
	public void delete(Long id);
	public List<SygModepassation> find(int indice, int pas,String code,String libelle);
	public int count(String code,String libelle);
	public void update(SygModepassation mode);
	public SygModepassation findById(Long code);
	public List<SygModepassation> find(String code);
}
