package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;




import sn.ssi.kermel.be.entity.SygCategori;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class CategoriSessionBean extends AbstractSessionBean implements CategoriSession{

	@Override
	public int count(String libelle,String service) {
		Criteria criteria = getHibernateSession().createCriteria(SygCategori.class);
		//criteria.setFetchMode("clients", FetchMode.SELECT);
		criteria.createAlias("type", "type");
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(service!=null){
			criteria.add(Restrictions.ilike("type.libelle", "%"+service+"%"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygCategori.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygCategori> find(int indice, int pas,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCategori.class);
		criteria.createAlias("type", "type");
		criteria.addOrder(Order.asc("libelle"));
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygCategori categori) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(categori);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygCategori categori) {
		
		try{
			getHibernateSession().merge(categori);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygCategori findById(Long code) {
		return (SygCategori)getHibernateSession().get(SygCategori.class, code);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygCategori> findRech(int indice, int pas,String libelle,Long service, Long cat) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCategori.class);
		criteria.createAlias("type", "type");
		criteria.setFetchMode("categori", FetchMode.SELECT);
		criteria.addOrder(Order.asc("libelle"));
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(service!=null){
			criteria.add(Restrictions.eq("type.id", service));
		}
		
		if(cat!=null){
			criteria.add(Restrictions.eq("categori.id", cat));
		}else{
			criteria.add(Restrictions.isNull("categori.id"));
		}
		
			
		
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public int countRech(String libelle,Long service,Long cat) {
		Criteria criteria = getHibernateSession().createCriteria(SygCategori.class);
		//criteria.setFetchMode("clients", FetchMode.SELECT);
		criteria.createAlias("type", "type");
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(service!=null){
			criteria.add(Restrictions.eq("type.id", service));
		}
		if(cat!=null){
			criteria.add(Restrictions.eq("categori.id", cat));
		}else{
			criteria.add(Restrictions.isNull("categori.id"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
}
