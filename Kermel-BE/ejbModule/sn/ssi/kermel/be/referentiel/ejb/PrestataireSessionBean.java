package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygPays;
import sn.ssi.kermel.be.entity.SygPrestataire;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;
@Stateless
public class PrestataireSessionBean  extends AbstractSessionBean implements PrestataireSession{
	
	
	
	@Override
	public int count(String identifiant,String raisonsociale,String adresse,String mail,String commentaire,Long idpays) {
		Criteria criteria= getHibernateSession().createCriteria(SygPrestataire .class);
		if(identifiant!=null){
			criteria.add(Restrictions.ilike("identifiant", "%"+ identifiant+"%"));
		}
		if(raisonsociale!=null){
			criteria.add(Restrictions.ilike("raisonsociale", "%"+raisonsociale+"%"));
		}
		if(adresse!=null){
			criteria.add(Restrictions.ilike("adresse", "%"+adresse+"%"));
		}
		if(mail!=null){
			criteria.add(Restrictions.ilike("mail", "%"+mail+"%"));
		}
		if(commentaire!=null){
			criteria.add(Restrictions.ilike("commentaire", "%"+commentaire+"%"));
		}
		
//		if(idpays!=null){
//			criteria.add(Restrictions.ilike("idpays", "%"+idpays+"%"));
//		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	

	@Override
	public void delete(Long idprestataire) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygPrestataire.class, idprestataire));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygPrestataire> find(int indice, int pas,String identifiant,String raisonsociale,String adresse,String mail,String commentaire,Long idpays) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPrestataire.class);
		
		if(identifiant!=null){
			criteria.add(Restrictions.ilike("identifiant", "%"+identifiant+"%"));
		}
		if(raisonsociale!=null){
			criteria.add(Restrictions.ilike("raisonsociale", "%"+raisonsociale+"%"));
		}
		
		if(adresse!=null){
			criteria.add(Restrictions.ilike("adresse", "%"+adresse+"%"));
		}
		if(mail!=null){
			criteria.add(Restrictions.ilike("mail", "%"+mail+"%"));
		}
		if(commentaire!=null){
			criteria.add(Restrictions.ilike("commentaire", "%"+commentaire+"%"));
		}
		
		
		
//		if(idpays!=null){
//			criteria.add(Restrictions.ilike("idpays", "%"+idpays+"%"));
//		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygPrestataire prestataire) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(prestataire);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygPrestataire prestataire) {
		
		try{
			getHibernateSession().merge(prestataire);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public SygPrestataire findById(Long idprestataire) {
		// TODO Auto-generated method stub
		return (SygPrestataire)getHibernateSession().get(SygPrestataire.class, idprestataire );
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<SygPrestataire> ListesPrestataires(int indice, int pas,Long audit) {
		
		return getHibernateSession().createQuery(
				"SELECT prestataires FROM  SygPrestataire prestataires where " 
			+ " prestataires.idprestataire NOT IN (SELECT audits.prestataire.idprestataire from SygPrestatairesAudits audits where  audits.audit.idaudit='" + audit + "' " +")")
				.setFirstResult(indice).setMaxResults(pas).list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygPrestataire> ListesPrestataires(Long audit) {
		return getHibernateSession().createQuery(
				"SELECT prestataires FROM  SygPrestataire prestataires where " 
			+ " prestataires.idprestataire NOT IN (SELECT audits.prestataire.idprestataire from SygPrestatairesAudits audits where  audits.audit.idaudit='" + audit + "' " +")")
				.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygPrestataire> ListesPrestataire(int indice, int pas,Long audit) {
		
		return getHibernateSession().createQuery(
				"SELECT prestataires FROM  SygPrestataire prestataires where " 
			+ " prestataires.idprestataire NOT IN (SELECT contrats.prestataire.idprestataire from SygContratsPrestataires contrats where  contrats.audit.idaudit='" + audit + "' " +")")
				.setFirstResult(indice).setMaxResults(pas).list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygPrestataire> ListesPrestataire(Long audit) {
		
		return getHibernateSession().createQuery(
				"SELECT prestataires FROM  SygPrestataire prestataires where " 
			+ " prestataires.idprestataire NOT IN (SELECT contrats.prestataire.idprestataire from SygContratsPrestataires contrats where  contrats.audit.idaudit='" + audit + "' " +")")
				.list();
	}
	
}