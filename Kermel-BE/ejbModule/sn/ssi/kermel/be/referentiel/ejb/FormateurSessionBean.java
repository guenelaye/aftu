package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygFormateur;
import sn.ssi.kermel.be.entity.SygSpecialite;
import sn.ssi.kermel.be.entity.SygTypeFormateur;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class FormateurSessionBean extends AbstractSessionBean implements
		FormateurSession {

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(SygFormateur.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygFormateur> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormateur.class);
		List<SygFormateur> fiche = criteria.list();
		return fiche;
	}

//	@Override
//	public SygFormateur findByCode(Integer id) {
//		
//		// TODO Auto-generated method stub
//		return (SygFormateur) getHibernateSession().get(SygFormateur.class,id);
//	}

	@Override
	public SygFormateur findByCode(Integer id) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormateur.class);
		criteria.setFetchMode("specialite", FetchMode.JOIN);
		if (id != null) {
			criteria.add(Restrictions.eq("id", id));

		}
		return (SygFormateur) criteria.list().get(0);
	}

	
	@Override
	public void save(SygFormateur formateur) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(formateur);
			//getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void update(SygFormateur formateur) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().merge(formateur);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygFormateur> findAllby(int indice, int pas, String nom,String prenom,
			String sexe,SygSpecialite specialite,SygTypeFormateur type) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormateur.class);
		criteria.setFetchMode("specialite", FetchMode.JOIN);
		criteria.setFetchMode("type", FetchMode.JOIN);
		criteria.addOrder(Order.asc("nom"));
		if (nom != null) {
			criteria.add(Restrictions.ilike("nom", "%" + nom + "%"));
		}
		if (prenom != null) {
			criteria.add(Restrictions.ilike("prenom", "%" + prenom + "%"));
		}
		if (sexe != null) {
			criteria.add(Restrictions.ilike("sexe", "%" + sexe + "%"));
		}
		
		if (specialite != null) {
			criteria.add(Restrictions.eq("specialite", specialite));
		}
		
		if (type != null) {
			criteria.add(Restrictions.eq("type", type));
		}
		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

	
	@Override
	public List<SygFormateur> findRaison(int indice, int pas, String raison,SygSpecialite specialite,SygTypeFormateur type)
 {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormateur.class);
		criteria.setFetchMode("specialite", FetchMode.JOIN);
		criteria.setFetchMode("type", FetchMode.JOIN);
		criteria.addOrder(Order.asc("societe"));
		if (raison != null) {
			criteria.add(Restrictions.ilike("societe", "%" + raison + "%"));
		}
		
		if (specialite != null) {
			criteria.add(Restrictions.eq("specialite", specialite));
		}
		
		if (type != null) {
			criteria.add(Restrictions.eq("type", type));
		}
		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}
	@Override
	public int count(String nom,String prenom,String sexe,SygSpecialite specialite,SygTypeFormateur type) {
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormateur.class);
		
		criteria.setFetchMode("specialite", FetchMode.JOIN);
		criteria.setFetchMode("type", FetchMode.JOIN);
		if (nom != null) {
			criteria.add(Restrictions.ilike("nom", "%" + nom + "%"));
		}
		if (prenom != null) {
			criteria.add(Restrictions.ilike("prenom", "%" + prenom + "%"));
		}
		if (sexe != null) {
			criteria.add(Restrictions.ilike("sexe", "%" + sexe + "%"));
		}
		
		if (specialite != null) {
			criteria.add(Restrictions.eq("specialite", specialite));
		}
		
		if (type != null) {
			criteria.add(Restrictions.eq("type", type));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public List<SygFormateur> findAll(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormateur.class);

		if (indice > 0)
			criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);
		return criteria.list();
	}

	

	@Override
	public int count(Integer id, String prenom) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormateur.class);
		if (id != null) {
			criteria.add(Restrictions.ilike("id", id ));
		}
		if (prenom != null) {
			criteria.add(Restrictions.ilike("prenom", "%" + prenom + "%"));
		}

		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

}


