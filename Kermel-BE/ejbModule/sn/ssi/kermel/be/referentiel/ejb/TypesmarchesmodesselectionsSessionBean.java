package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygModeselection;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.be.entity.SygTypesmarchesmodesselections;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class TypesmarchesmodesselectionsSessionBean extends AbstractSessionBean implements TypesmarchesmodesselectionsSession{

	
	@Override
	public int count(String libelle,SygTypesmarches type,SygModeselection mode) {
		Criteria criteria = getHibernateSession().createCriteria(SygTypesmarchesmodesselections.class);
		criteria.createAlias("type", "type");
		criteria.createAlias("mode", "mode");
		if(libelle!=null){
			criteria.add(Restrictions.ilike("mode.libelle","%"+ libelle+"%"));
		}
		if(type!=null){
			criteria.add(Restrictions.eq("type", type));
		}
		if(mode!=null){
			criteria.add(Restrictions.eq("mode", mode));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygTypesmarchesmodesselections.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygTypesmarchesmodesselections> find(int indice, int pas,String libelle,SygTypesmarches type,SygModeselection mode) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTypesmarchesmodesselections.class);
		criteria.createAlias("type", "type");
		criteria.createAlias("mode", "mode");
		criteria.addOrder(Order.asc("mode.libelle"));
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("mode.libelle","%"+ libelle+"%"));
		}
		if(type!=null){
			criteria.add(Restrictions.eq("type", type));
		}
		if(mode!=null){
			criteria.add(Restrictions.eq("mode", mode));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygTypesmarchesmodesselections mode) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(mode);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygTypesmarchesmodesselections mode) {
		
		try{
			getHibernateSession().merge(mode);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygTypesmarchesmodesselections findById(Long code) {
		return (SygTypesmarchesmodesselections)getHibernateSession().get(SygTypesmarchesmodesselections.class, code);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<SygModeselection> ListesModes(int indice, int pas, String libelle,Long type) {
		String libellesaisi;
		if(libelle!=null)
		{
		libellesaisi="modes.libelle LIKE '%"+libelle+"%' and ";
		}
		else
		{
		libellesaisi="";	
		}
		return getHibernateSession().createQuery(
				"SELECT modes FROM  SygModeselection modes where " +libellesaisi
			+ " modes.id NOT IN (SELECT typesmodes.mode.id from SygTypesmarchesmodesselections typesmodes where  typesmodes.type.id='" + type + "' " +")")
				.setFirstResult(indice).setMaxResults(pas).list();
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygModeselection> ListesModes(String libelle,Long type) {
		String libellesaisi;
		if(libelle!=null)
		{
		libellesaisi="modes.libelle LIKE '%"+libelle+"%' and ";
		}
		else
		{
		libellesaisi="";	
		}
		return getHibernateSession().createQuery(
				"SELECT modes FROM  SygModeselection modes where " +libellesaisi
			+ " modes.id NOT IN (SELECT typesmodes.mode.id from SygTypesmarchesmodesselections typesmodes where  typesmodes.type.id='" + type + "' " +")")
				.list();
	}
}
