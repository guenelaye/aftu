package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossierspieces;
import sn.ssi.kermel.be.entity.SygPieces;
import sn.ssi.kermel.be.entity.SygPiecesrequises;
import sn.ssi.kermel.be.entity.SygTypesDossiers;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class PiecesSessionBean extends AbstractSessionBean implements PiecesSession{

	@Override
	public int count(String code,String libelle,SygAutoriteContractante autorite) {
		Criteria criteria = getHibernateSession().createCriteria(SygPieces.class);
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		if(code!=null){
			criteria.add(Restrictions.ilike("codepiece", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
//		if(autorite!=null){
//			criteria.add(Restrictions.or(Restrictions.eq("autorite", autorite), Restrictions.isNull("autorite")));
//		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygPieces.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygPieces> find(int indice, int pas,String code,String libelle,SygAutoriteContractante autorite) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPieces.class);
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.addOrder(Order.desc("libelle"));
		if(code!=null){
			criteria.add(Restrictions.ilike("codepiece", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
//		if(autorite!=null){
//			criteria.add(Restrictions.or(Restrictions.eq("autorite", autorite), Restrictions.isNull("autorite")));
//		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public SygPieces save(SygPieces piece) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(piece);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return piece;	}

	@Override
	public void update(SygPieces piece) {
		
		try{
			getHibernateSession().merge(piece);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygPieces findById(Long code) {
		return (SygPieces)getHibernateSession().get(SygPieces.class, code);
	}
	
	
	@Override
	public List<SygPieces> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPieces.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));
		
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygPieces > ListePieces(int indice, int pas, String libelle,Long dossier,Long autorite) {
		Criteria criteria = getHibernateSession().createCriteria(SygPieces.class);
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.addOrder(Order.desc("libelle"));
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
//		if(autorite!=null){
//			criteria.add(Restrictions.or(Restrictions.eq("autorite.id", autorite), Restrictions.isNull("autorite")));
//		}
		if(ListePieces(dossier).size()>0)
		  criteria.add(Restrictions.not(Restrictions.in("id", ListePieces(dossier))));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygPieces > ListePieces(String libelle,Long dossier,Long autorite) {
		Criteria criteria = getHibernateSession().createCriteria(SygPieces.class);
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.addOrder(Order.desc("libelle"));
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
//		if(autorite!=null){
//			criteria.add(Restrictions.or(Restrictions.eq("autorite.id", autorite), Restrictions.isNull("autorite")));
//		}
		if(ListePieces(dossier).size()>0)
			  criteria.add(Restrictions.not(Restrictions.in("id", ListePieces(dossier))));
		
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Long> ListePieces(Long dossier) {
		// TODO Auto-generated method stub
		
		Criteria criteria = getHibernateSession().createCriteria(SygDossierspieces.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("piece", "piece");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier.dosID", dossier));
		}
		criteria.setProjection(Projections.property("piece.id"));
		return criteria.list();
	}

	
		@SuppressWarnings("unchecked")
		@Override
		public List<SygPieces> findNotInDossier(int indice, int pas, SygTypesDossiers code, String libelle) {
		// TODO Auto-generated method stub
		org.hibernate.Query query = null;
		String sql = "SELECT pieces FROM  SygPieces pieces where pieces "
//		+ " NOT IN ( SELECT piecesrequises.piece from SygPiecesrequises piecesrequises where  piecesrequises.dossiers = " + code + " " + " )";
		+ " NOT IN ( SELECT piecesrequises.piece from SygPiecesrequises piecesrequises where  piecesrequises.dossiers = :param  " +  " )";
		
		if (libelle != null)
		sql += " AND pieces.libelle like '%" + libelle + "%'";
		query = getHibernateSession().createQuery(sql);
	
		query.setFirstResult(indice);
	
		return query.setMaxResults(pas).setParameter("param", code).list();
		}
		@Override
		public Integer countNotInDossier( SygTypesDossiers code, String libelle) {
			// TODO Auto-generated method stub
			org.hibernate.Query query = null;
			String sql = "SELECT count (pieces) FROM  SygPieces pieces where  pieces " + 
		 " NOT IN (SELECT piecesrequises.piece from SygPiecesrequises piecesrequises where  piecesrequises.dossiers='" + code + "' " + " )";
		
			if (libelle != null)
			sql += " AND pieces.libelle like '%" + libelle + "%'";
			query = getHibernateSession().createQuery(sql);
			Long val= (Long) query.uniqueResult();
		   return val.intValue();
			}
}
