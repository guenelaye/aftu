package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.EcoGroupesImputation;
@Remote
public interface GroupeImputationSession {
	public void save(EcoGroupesImputation GroupesImputation);
	public void delete(Long id);
	public List<EcoGroupesImputation> find(int indice, int pas, String libelle);
	public int count(String libelle);
	public void update(EcoGroupesImputation GroupesImputation);
	public EcoGroupesImputation findById(Long code);
	public List<EcoGroupesImputation> find(String libelle);
}
