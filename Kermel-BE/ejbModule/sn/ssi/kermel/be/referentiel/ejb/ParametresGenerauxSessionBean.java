package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SysParametresGeneraux;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

/**
 * @author Coura Diagne, OLA Abdel Rakib, olarakib@gmail.com
 * @since Vendredi 12 F�vrier 2010, 13:29
 * @version 0.0.1
 */
public @Stateless class ParametresGenerauxSessionBean extends AbstractSessionBean implements ParametresGenerauxSession {

	
	@Override
	public SysParametresGeneraux findById(String code) {
		return (SysParametresGeneraux)getHibernateSession().get(SysParametresGeneraux.class, code);
	}


	@Override
	public void incrementeCode(String code) {
		Criteria criteria = getHibernateSession().createCriteria(SysParametresGeneraux.class)
		.add(Restrictions.eq("code", code));
		SysParametresGeneraux parametreGeneral = ((SysParametresGeneraux)criteria.list().get(0));
		parametreGeneral.setValeur(parametreGeneral.getValeur()+1);
		getHibernateSession().merge(parametreGeneral);
	}

	@Override
	public Long getValeurParametre(String code) {
		Criteria criteria = getHibernateSession().createCriteria(SysParametresGeneraux.class)
		.add(Restrictions.eq("code", code));
		return ((SysParametresGeneraux)criteria.list().get(0)).getValeur();
	}
	
	//ADD Rama , 04-01-2013
	
	@Override
	public void update(SysParametresGeneraux parametre) {
		
		try{
			getHibernateSession().merge(parametre);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SysParametresGeneraux> find(int indice, int pas, String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SysParametresGeneraux.class);
		
		if(code!=null){
			criteria.add(Restrictions.ilike("code", "%"+code+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
}