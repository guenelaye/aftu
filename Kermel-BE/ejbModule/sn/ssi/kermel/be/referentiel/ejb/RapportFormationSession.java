package sn.ssi.kermel.be.referentiel.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.be.entity.SygRapportFormation;
import sn.ssi.kermel.be.entity.SygTypeRapportFormation;



@Remote
public interface RapportFormationSession {
	public void save(SygRapportFormation rapport);
	public void delete(Long id);
	public List<SygRapportFormation> find(int indice, int pas,String libelle,Date date,Date datepublication,SygProformation formation,SygTypeRapportFormation type);
	public int count(Date daterapport,Date datepublication,SygProformation formation,SygTypeRapportFormation type);
	public void update(SygRapportFormation rapport);
	public SygRapportFormation findById(Long code);
	List<SygRapportFormation> find(int indice, int pas);
	int count();
	
}
