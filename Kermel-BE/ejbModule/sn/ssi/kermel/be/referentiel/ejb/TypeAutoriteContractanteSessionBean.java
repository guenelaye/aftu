package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class TypeAutoriteContractanteSessionBean extends AbstractSessionBean implements TypeAutoriteContractanteSession{

	@Override
	public int count(String code,String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygTypeAutoriteContractante.class);
		if(code!=null){
			criteria.add(Restrictions.ilike("code", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygTypeAutoriteContractante.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygTypeAutoriteContractante> find(int indice, int pas,String code,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTypeAutoriteContractante.class);
		criteria.addOrder(Order.asc("ordre"));
		if(code!=null){
			criteria.add(Restrictions.ilike("code", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygTypeAutoriteContractante type) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(type);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygTypeAutoriteContractante type) {
		
		try{
			getHibernateSession().merge(type);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygTypeAutoriteContractante findById(Long code) {
		return (SygTypeAutoriteContractante)getHibernateSession().get(SygTypeAutoriteContractante.class, code);
	}
	
	
	@Override
	public List<SygTypeAutoriteContractante> find(int ordre) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTypeAutoriteContractante.class);
		if(ordre>-1)
			criteria.add(Restrictions.eq("ordre", ordre));
		
		return criteria.list();
	}
}
