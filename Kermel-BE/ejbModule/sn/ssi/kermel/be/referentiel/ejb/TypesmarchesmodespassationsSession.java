package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.be.entity.SygTypesmarchesmodespassations;

@Remote
public interface TypesmarchesmodespassationsSession {
	public void save(SygTypesmarchesmodespassations mode);
	public void delete(Long id);
	public List<SygTypesmarchesmodespassations> find(int indice, int pas,String libelle,SygTypesmarches type,SygModepassation mode);
	public int count(String libelle,SygTypesmarches type,SygModepassation mode);
	public void update(SygTypesmarchesmodespassations mode);
	public SygTypesmarchesmodespassations findById(Long code);
	public List<SygModepassation> ListesModes(int indice, int pas,String libelle,Long type);
	public List<SygModepassation> ListesModes(String libelle,Long type);
}
