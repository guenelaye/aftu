package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygGroupeFormation;
import sn.ssi.kermel.be.entity.SygMembresGroupes;
import sn.ssi.kermel.be.entity.SygProformation;
@Remote
public interface GroupeFormationSession {
	public void save(SygGroupeFormation groupe);
	public void delete(Integer id);
	public List<SygGroupeFormation> find(int indice, int pas, String libelle);
	public int count(String libelle);
	public void update(SygGroupeFormation groupe);
	public SygGroupeFormation findById(Integer code);
	public List<SygGroupeFormation> find(String libelle);
	
	public List<SygGroupeFormation> Groupes(int indice, int pas, String libelle,SygProformation formation);
	public List<SygGroupeFormation> Groupes( String libelle,SygProformation formation);
	public List<Integer> Groupes(SygProformation formation);
	/////
	
	public void saveMembre(SygMembresGroupes groupe);
	public void deleteMembre(Integer id);
	public List<SygMembresGroupes> findMembres(int indice, int pas, SygGroupeFormation groupe);
	public int countMembres(SygGroupeFormation groupe);
	public void updateMembre(SygMembresGroupes groupe);
	public SygMembresGroupes findMembre(Integer code);
}
