package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.TypeCategorie;

@Remote
public interface TypeCategoriSession {
	public void save(TypeCategorie type);
	
	public void delete(Long id);
	
	public List<TypeCategorie> find(int indice, int pas,Long code,String libelle, Long typecat);
	
	public int count(Long code,String libelle, Long typecat);
	
	public void update(TypeCategorie type);
	
	public TypeCategorie findById(Long code);
	
	
	public List<TypeCategorie> findAutreTypeCat(int indice, int pas,Long code,String libelle,int niveau);
	public int countAutreTypeCat(Long code,String libelle,int niveau);
}
