package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;




import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygFormationBail;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class BailleurFormationSessionBean extends AbstractSessionBean implements
		BailleurFormationSession {

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(SygFormationBail.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	
	@Override
	public List<SygFormationBail> find(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormationBail.class);

		if (indice > 0)
			criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);
		return criteria.list();
	}

	@Override
	public List<SygFormationBail> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormationBail.class);
		return criteria.list();
	}

	@Override
	public void save(SygFormationBail fbail) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(fbail);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void update(SygFormationBail fbail) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().merge(fbail);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygFormationBail> find(int indice, int pas,SygProformation formation, SygBailleurs bailleur) {
	
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormationBail.class);
		criteria.createAlias("formation", "formation");
		criteria.createAlias("bailleur","bailleur");
	
		criteria.addOrder(Order.asc("id"));
		
		if (formation != null) {
			criteria.add(Restrictions.eq("formation", formation));
		}
		if (bailleur != null) {
			criteria.add(Restrictions.eq("bailleur", bailleur));
		}
		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygFormationBail> finByMontan(Long contBaill) {
		
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormationBail.class);
		criteria.createAlias("formation.budg", "formation.budg");
		criteria.createAlias("bailleur","bailleur");
		criteria.addOrder(Order.asc("id"));
		
		if (contBaill != null) {
			criteria.add(Restrictions.eq("formation.budg", contBaill));
		}
		
		return criteria.list();
	}


	
	@Override
	public int count(SygProformation formation, SygBailleurs bailleur) {
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormationBail.class);
		if (formation != null) {
			criteria.add(Restrictions.eq("formation", formation));
		}
		if (bailleur != null) {
			criteria.add(Restrictions.eq("bailleur", bailleur));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	
	@Override
	public SygFormationBail findByCode(Integer id) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygFormationBail.class);
		criteria.createAlias("formation", "formation");
		criteria.createAlias("bailleur","bailleur");
		if (id != 0) {
			criteria.add(Restrictions.eq("id", id));

		}
		return (SygFormationBail) criteria.list().get(0);
	}


	@Override
	public Long getMontantTotal() {
		Criteria criteria = getHibernateSession().createCriteria(SygFormationBail.class);
		criteria.setProjection(Projections.projectionList().add(Projections.sum("montant"))
				);
		
		return (Long) criteria.uniqueResult();
	}


}
