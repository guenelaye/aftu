package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygJournal;
import sn.ssi.kermel.be.entity.SygService;

@Remote
public interface JournalMSession {
	public void save(SygJournal service);
	public void delete(Long id);
	public List<SygJournal> find(int indice, int pas,String code,String libelle,SygAutoriteContractante autorite, Long id);
	public int count(String code,String libelle,SygAutoriteContractante autorite, Long id);
	public void update(SygJournal service);
	public SygJournal findById(Long code);
}
