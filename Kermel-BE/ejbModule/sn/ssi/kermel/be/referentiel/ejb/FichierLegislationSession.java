package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCategorieLegislation;
import sn.ssi.kermel.be.entity.SygCodeMarche;
import sn.ssi.kermel.be.entity.SygFichierLegislation;
import sn.ssi.kermel.be.entity.SygRapportARMP;

@Remote
public interface FichierLegislationSession {
	
	public void save(SygFichierLegislation rapportarmp);
	
	public void delete(Long id);
	
	public List<SygFichierLegislation> find(int indice, int pas,Long code,String libelle);
	
	public int count(Long code,String libelle);
	
	public void update(SygFichierLegislation rapportarmp);
	
	public SygFichierLegislation findById(Long code);

	List<SygFichierLegislation> findAll(int indice, int pas,
			SygCategorieLegislation categorie, String libelle);

	int countALL(SygCategorieLegislation categorie, String libelle);
	
}
