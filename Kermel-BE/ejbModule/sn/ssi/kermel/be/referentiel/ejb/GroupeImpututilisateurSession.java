package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.EcoGroupesImputation;
import sn.ssi.kermel.be.entity.EcoGroupesImputilisateur;
@Remote
public interface GroupeImpututilisateurSession {
	public void save(EcoGroupesImputilisateur GroupesImputilisateur);
	public void delete(Long id);
	public List<EcoGroupesImputilisateur> find(int indice, int pas, String libelle);
	public int count(String libelle);
	public void update(EcoGroupesImputilisateur GroupesImputilisateur);
	public EcoGroupesImputilisateur findById(Long code);
	public List<EcoGroupesImputilisateur> find(String libelle);
	public List<EcoGroupesImputilisateur> findByGroup(EcoGroupesImputation GroupesImputation);
}
