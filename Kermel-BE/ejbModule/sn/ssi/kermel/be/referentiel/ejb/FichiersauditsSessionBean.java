package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygFichiersaudits;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;
@Stateless
public class FichiersauditsSessionBean  extends AbstractSessionBean implements FichiersauditsSession{
	
	
	
	@Override
	public int count(String nomfichierpdf,String libellefichiersaudits) {
		Criteria criteria= getHibernateSession().createCriteria(SygFichiersaudits.class);
		if(nomfichierpdf!=null){
			criteria.add(Restrictions.ilike("nomfichierpdf", "%"+nomfichierpdf+"%"));
		}
		if(libellefichiersaudits!=null){
			criteria.add(Restrictions.ilike("libellefichiersaudits", "%"+libellefichiersaudits+"%"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	

	@Override
	public void delete(Long idfichiersaudits) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygFichiersaudits.class, idfichiersaudits));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygFichiersaudits> find(int indice, int pas,String nomfichierpdf,String libellefichiersaudits) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygFichiersaudits.class);
		
		if(nomfichierpdf!=null){
			criteria.add(Restrictions.ilike("nomfichierpdf", "%"+nomfichierpdf+"%"));
		}
		
		if(libellefichiersaudits!=null){
			criteria.add(Restrictions.ilike("libellefichiersaudits", "%"+libellefichiersaudits+"%"));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygFichiersaudits fichier) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(fichier);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygFichiersaudits fichier) {
		
		try{
			getHibernateSession().merge(fichier);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public SygFichiersaudits findById(Long idfichiersaudits) {
		// TODO Auto-generated method stub
		return (SygFichiersaudits)getHibernateSession().get(SygFichiersaudits.class, idfichiersaudits );
	}



	
	
	
}