package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygDecision;

@Remote
public interface DecisionSession {
	
	public void save(SygDecision decision);
	
	public void delete(Long id);
	
	public List<SygDecision> find(int indice, int pas,Long code,String libelle);
	
	public List<SygDecision> findRech(int indice, int pas,Long code,String libelle,String LibelleType);
	
	public int count(Long code,String libelle);
	
	public int countRech(Long code,String libelle, String LibelleType);
	
	public void update(SygDecision decision);
	
	public SygDecision findById(Long code);

	List<SygDecision> findAll();
	
}
