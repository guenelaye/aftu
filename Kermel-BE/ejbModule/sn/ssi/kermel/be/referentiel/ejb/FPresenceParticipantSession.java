package sn.ssi.kermel.be.referentiel.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;


import sn.ssi.kermel.be.common.utils.BeanLocator;
import sn.ssi.kermel.be.entity.SygParticipantsFormation;
import sn.ssi.kermel.be.entity.SygFPresenceParcipant;
import sn.ssi.kermel.be.entity.SygFichePresence;





@Remote
public interface FPresenceParticipantSession {
	public void save(SygFPresenceParcipant fpart);

	public void delete(Long id);

	public List<SygFPresenceParcipant> find(int indice, int pas);

	public void update(SygFPresenceParcipant fpart);

	public List<SygFPresenceParcipant> findAll();

	public int count(SygParticipantsFormation participant, SygFichePresence fiche);
	SygFPresenceParcipant findByCode(Long id);

	List<SygFPresenceParcipant> find(int indice, int pas, SygParticipantsFormation participant,
			SygFichePresence fiche);
	
	List<SygFPresenceParcipant> findAffListe(int indice, int pas, SygParticipantsFormation participant,
			SygFichePresence fiche);

	SygFPresenceParcipant findByFiche(SygFichePresence fiche);
	
}
