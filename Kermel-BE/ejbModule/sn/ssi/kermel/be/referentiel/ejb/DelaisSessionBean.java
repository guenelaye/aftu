package sn.ssi.kermel.be.referentiel.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygDelais;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class DelaisSessionBean extends AbstractSessionBean implements DelaisSession{

	@Override
	public int count(String code,String libelle,Date datedebut,Date datefin) {
		Criteria criteria = getHibernateSession().createCriteria(SygDelais.class);
		if(code!=null){
			criteria.add(Restrictions.ilike("code", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("datedebutvalidite", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("datefinvalidite", datefin));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygDelais.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygDelais> find(int indice, int pas,String code,String libelle,Date datedebut,Date datefin) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDelais.class);
		criteria.addOrder(Order.desc("date"));
		if(code!=null){
			criteria.add(Restrictions.ilike("code", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("datedebutvalidite", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("datefinvalidite", datefin));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygDelais delai) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(delai);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygDelais delai) {
		
		try{
			getHibernateSession().merge(delai);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygDelais findById(Long code) {
		return (SygDelais)getHibernateSession().get(SygDelais.class, code);
	}
	
	
	@Override
	public List<SygDelais> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDelais.class);
		if(code!=null)
			criteria.add(Restrictions.eq("code", code));
		
		return criteria.list();
	}
}
