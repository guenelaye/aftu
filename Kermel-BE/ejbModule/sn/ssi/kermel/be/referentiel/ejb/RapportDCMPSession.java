package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygRapportDCMP;

@Remote
public interface RapportDCMPSession {
	
	public void save(SygRapportDCMP rapportdcmp);
	
	public void delete(Long id);
	
	public List<SygRapportDCMP> find(int indice, int pas,Long code,String libelle);
	
	public int count(Long code,String libelle);
	
	public void update(SygRapportDCMP rapportdcmp);
	
	public SygRapportDCMP findById(Long code);
	
}
