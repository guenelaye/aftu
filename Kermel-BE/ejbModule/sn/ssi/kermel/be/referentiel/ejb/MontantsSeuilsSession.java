package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygMontantsSeuils;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.entity.SygTypesmarches;

@Remote
public interface MontantsSeuilsSession {
	public void save (SygMontantsSeuils seuil);
	public void update (SygMontantsSeuils seuil);
	public void delete (Long id);
	public List<SygMontantsSeuils> find(int indice, int pas,SygTypeAutoriteContractante typeautorite, SygTypesmarches typemarche,SygModepassation modepassation,String modeengagement,String examen,String type);
	public int count(SygTypeAutoriteContractante typeautorite, SygTypesmarches typemarche,SygModepassation modepassation,String modeengagement,String examen,String type);
	public SygMontantsSeuils findById(Long code);
	   
	
}
