package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygSecteursactivites;

@Remote
public interface SecteursactivitesSession {
	public void save(SygSecteursactivites activite,int parent);
	public void delete(String id);
	public List<SygSecteursactivites> find(int indice, int pas,String code,String libelle,SygSecteursactivites secteur);
	public int count(String code,String libelle,SygSecteursactivites secteur);
	public void update(SygSecteursactivites activite);
	public SygSecteursactivites findById(String code);
	public String getGeneratedCode(String code,String codeParametreGeneral,int parent);
}
