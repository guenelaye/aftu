package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SysParametresGeneraux;
/**
 * @author Coura Diagne, OLA Abdel Rakib, olarakib@gmail.com
 * @since Vendredi 12 F�vrier 2010, 13:27
 * @version 0.0.1
 */
@Remote
public interface ParametresGenerauxSession {
	
	/**
	 * @param code
	 * @return
	 */
	
	/**
	 * @param code
	 * @return la valeur d'un parametre dont le code est pass� en param�tre
	 */
	public Long getValeurParametre(String code);
	
	/**
	 * @param code  Identifiant du paramètre général dont on veut incrémenter la valeur
	 * <li>Incr�mente de 1 la valeur du param�tre g�n�ral dont le code est pass� en param
	 */
	public void incrementeCode(String code);

	SysParametresGeneraux findById(String code);
	
	//ADD Rama , 04-01-2013
	
	public void update(SysParametresGeneraux parametre);
	public List<SysParametresGeneraux> find(int indice, int pas,String code);
}
