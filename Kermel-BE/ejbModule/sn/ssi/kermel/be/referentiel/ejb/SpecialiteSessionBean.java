package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;



import sn.ssi.kermel.be.entity.SygSpecialite;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class SpecialiteSessionBean extends AbstractSessionBean implements
		SpecialiteSession {

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(SygSpecialite.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	
	@Override
	public List<SygSpecialite> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria( SygSpecialite.class);
		List<SygSpecialite> spec = criteria.list();
		return spec;
	}

	@Override
	public void save(SygSpecialite spec) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(spec);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void update(SygSpecialite spec) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().merge(spec);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygSpecialite> find(int indice, int pas, String code,
			String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygSpecialite.class);
		
		criteria.addOrder(Order.asc("libelle"));
		if (code != null) {
			criteria.add(Restrictions.ilike("code", "%" + code + "%"));
		}
		if (libelle != null) {
			criteria.add(Restrictions.ilike("libelle","%" +  libelle + "%"));
		}
		
		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

	

	@Override
	public int count(String code, String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(
				SygSpecialite.class);
		if (code != null) {
			criteria.add(Restrictions.ilike("code", "%" + code + "%"));
		}
		if (libelle != null) {
			criteria.add(Restrictions.ilike("libelle","%" +  libelle + "%"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public SygSpecialite findByCode(Integer id) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygSpecialite.class);
	
		if (id != 0) {
			criteria.add(Restrictions.eq("id", id));

		}
		return (SygSpecialite) criteria.list().get(0);
	}

	
}
