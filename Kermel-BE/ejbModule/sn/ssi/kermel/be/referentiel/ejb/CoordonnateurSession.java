
package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;


import sn.ssi.kermel.be.entity.SygCoordonateur;



@Remote
public interface CoordonnateurSession {
	public void save(SygCoordonateur coord);
	public List<SygCoordonateur> findAll(int indice, int pas);
	public void update(SygCoordonateur coord);
	SygCoordonateur findByCode(Integer id);
	List<SygCoordonateur> findAllby(int indice, int pas, String nom,String prenom);
	public void delete(Integer id);
	public int count(String nom,String prenom);
	List<SygCoordonateur> findAll();


}
