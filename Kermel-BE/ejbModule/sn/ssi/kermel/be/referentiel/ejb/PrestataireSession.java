package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;


import sn.ssi.kermel.be.entity.SygPrestataire;


@Remote
public interface PrestataireSession {
	public void save(SygPrestataire prestataire);
	public void delete(Long prestataire);
	public List<SygPrestataire> find(int indice, int pas,String identifiant,String raisonsociale,String adresse,String mail,String commentaire,Long idpays);
	public int count(String identifiant,String raisonsociale,String adresse,String mail,String commentaire,Long idpays);
	public void update(SygPrestataire prestataire);
	public SygPrestataire findById(Long idprestataire);
	

	public List<SygPrestataire> ListesPrestataires(int indice, int pas,Long audit);
	public List<SygPrestataire> ListesPrestataires(Long audit);
	
	public List<SygPrestataire> ListesPrestataire(int indice, int pas,Long audit);
	public List<SygPrestataire> ListesPrestataire(Long audit);

}
