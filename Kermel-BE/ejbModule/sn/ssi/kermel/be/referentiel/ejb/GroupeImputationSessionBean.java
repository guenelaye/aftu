package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.EcoGroupesImputation;
import sn.ssi.kermel.be.entity.SygDecision;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;
@Stateless
public class GroupeImputationSessionBean extends AbstractSessionBean implements GroupeImputationSession{

	@Override
	public int count(String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(EcoGroupesImputation.class);
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(EcoGroupesImputation.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<EcoGroupesImputation> find(int indice, int pas, String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(EcoGroupesImputation.class);
		criteria.addOrder(Order.desc("libelle"));
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(EcoGroupesImputation GroupesImputation) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(GroupesImputation);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(EcoGroupesImputation GroupesImputation) {
		
		try{
			getHibernateSession().merge(GroupesImputation);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}


	
	@Override
	public List<EcoGroupesImputation> find(String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(EcoGroupesImputation.class);
		if(libelle!=null)
			criteria.add(Restrictions.eq("libelle", libelle));
		
		return criteria.list();
	}

@Override
public EcoGroupesImputation findById(Long code) {
	// TODO Auto-generated method stub
	return (EcoGroupesImputation)getHibernateSession().get(EcoGroupesImputation.class, code);
}
}
