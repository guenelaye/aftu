package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygMembreCellulePassation;

@Remote
public interface MembreCellulePassationSession {
	public void save (SygMembreCellulePassation membre);
	public void update (SygMembreCellulePassation membre);
	public void delete (Long id);
	public List<SygMembreCellulePassation> find(int indice, int pas,String nom, String prenom,
			SygAutoriteContractante autorite);
	public int count (String nom, String prenom,
			SygAutoriteContractante autorite);
	public SygMembreCellulePassation findById(Long code);
	
	public List<SygMembreCellulePassation> ListeMembres(int indice, int pas,String nom,Long dossier,Long autorite);
	public List<SygMembreCellulePassation> ListeMembres(String nom,Long dossier,Long autorite);
}
