package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;
import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygTypeRapportFormation;


@Remote
public interface TypeRapportFormationSession {
	public void save(SygTypeRapportFormation type);
	public void delete(Long id);
	public List<SygTypeRapportFormation> find(int indice, int pas,String code,String libelle);
	public int count(String code,String libelle);
	public void update(SygTypeRapportFormation type);
	public SygTypeRapportFormation findById(Long code);
	public List<SygTypeRapportFormation> find(String code);
}
