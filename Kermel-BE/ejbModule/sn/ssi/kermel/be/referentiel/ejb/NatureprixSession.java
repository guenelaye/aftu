package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygNatureprix;

@Remote
public interface NatureprixSession {
	public void save(SygNatureprix nature);
	public void delete(Long id);
	public List<SygNatureprix> find(int indice, int pas,String code,String libelle);
	public int count(String code,String libelle);
	public void update(SygNatureprix nature);
	public SygNatureprix findById(Long code);
	public List<SygNatureprix> find(String code);
}
