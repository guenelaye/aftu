package sn.ssi.kermel.be.referentiel.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.tool.hbm2x.StringUtils;

import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAnnee;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygFormateur;
import sn.ssi.kermel.be.entity.SygMois;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class ProformationSessionBean extends AbstractSessionBean implements ProformationSession{

	@Override
	public int count(Date forDateDebut,SygMois mois,SygAnnee annee,int publie) {
		Criteria criteria = getHibernateSession().createCriteria(SygProformation.class);
//		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("mois", FetchMode.SELECT);

		if(forDateDebut!=null){
			criteria.add(Restrictions.ge("date", forDateDebut));
		}
//		if(autorite!=null){
//			criteria.add(Restrictions.eq("autorite", autorite));
//		}
		if(mois!=null){
			criteria.add(Restrictions.eq("mois", mois));
		}
		if(annee!=null){
			criteria.add(Restrictions.eq("anne", annee));
		}
		if(publie>0){
			criteria.add(Restrictions.eq("publie",publie));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygProformation.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygProformation.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	

	@Override
	public void save(SygProformation formation) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(formation);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	
		}

	@Override
	public void update(SygProformation formation) {
		
		try{
			getHibernateSession().merge(formation);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public SygProformation findById(Long code)  {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygProformation.class);
//		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("module", FetchMode.SELECT);
		criteria.setFetchMode("formateur", FetchMode.SELECT);
//		criteria.setFetchMode("typeautorite", FetchMode.SELECT);
		criteria.setFetchMode("mois", FetchMode.SELECT);
		criteria.setFetchMode("anne", FetchMode.SELECT);
		if (code != null) {
			criteria.add(Restrictions.eq("id", code));

		}
		return (SygProformation) criteria.list().get(0);
	}

	
//	@Override
//	public SygProformation findById(Long code) {
//		return (SygProformation)getHibernateSession().get(SygProformation.class, code);
//	}
	
	
	@Override
	public List<SygProformation> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygProformation.class);
//		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("module", FetchMode.SELECT);
		criteria.setFetchMode("formateur", FetchMode.SELECT);
//		criteria.setFetchMode("typeautorite", FetchMode.SELECT);
		criteria.setFetchMode("mois", FetchMode.SELECT);
		criteria.setFetchMode("anne", FetchMode.SELECT);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));
		
		return criteria.list();
	}
	

	
	

	@Override
	public List<SygProformation> find(int indice, int pas, String mois,String forlibelle,String forRef,String annee) {
       Criteria criteria = getHibernateSession().createCriteria(SygProformation.class);
      
//		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("module", FetchMode.SELECT);
		criteria.setFetchMode("formateur", FetchMode.SELECT);
//		criteria.setFetchMode("typeautorite", FetchMode.SELECT);
		criteria.setFetchMode("mois", FetchMode.SELECT);
		criteria.setFetchMode("anne", FetchMode.SELECT);
		
		if(mois!=null){
			criteria.add(Restrictions.eq("mois",mois));
			}
		if(forlibelle!=null){
			criteria.add(Restrictions.eq("forLibelle",forlibelle));
			}
		if(forRef!=null){
			criteria.add(Restrictions.eq("forRef",forRef));
			}
			
		
	
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
		if(annee != null)
			criteria.add(Restrictions.sqlRestriction(" SUBSTR(LTRIM(forDateDebut), 1, 4)="+annee ));
		
		ArrayList<SygProformation> avisattributions = (ArrayList<SygProformation>) criteria.list();
		return avisattributions;
//		return criteria.list();
		}
	
	
	@Override
	public List<SygProformation> find(int indice, int pas, Date forDateDebut,
			Date forDateFin,String forlibelle,String forRef) {
       Criteria criteria = getHibernateSession().createCriteria(SygProformation.class);
       String date = ToolKermel.dateToStringSql(forDateDebut);
       Date dat = ToolKermel.stringToDateSQL(date);
//       System.out.println("heiiiiiiiiiiiiiiiiiinnnn!!!!!!   "+dat);
//		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("module", FetchMode.SELECT);
		criteria.setFetchMode("formateur", FetchMode.SELECT);
//		criteria.setFetchMode("typeautorite", FetchMode.SELECT);
		criteria.setFetchMode("mois", FetchMode.SELECT);
		criteria.setFetchMode("anne", FetchMode.SELECT);
		criteria.addOrder(Order.desc("forDateDebut"));
		
		if(forDateDebut!=null){
		criteria.add(Restrictions.eq("forDateDebut",forDateDebut));
		}
		if(forDateFin!=null){
			criteria.add(Restrictions.le("forDateFin",forDateFin));
			}
		if(forlibelle!=null){
			criteria.add(Restrictions.eq("forLibelle",forlibelle));
			}
		if(forRef!=null){
			criteria.add(Restrictions.eq("forRef",forRef));
			}
			
//		if(autorite!=null){
//		criteria.add(Restrictions.eq("autorite", autorite));
//		}
//	
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygProformation> find(int indice, int pas, Long code,Date forDateDebut,
			Date forDateFin,String forlibelle,String forRef,String annee,SygAnnee sygannee,SygMois mois,int publie) {
		Criteria criteria = getHibernateSession().createCriteria(SygProformation.class);
//		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("module", FetchMode.SELECT);
		criteria.setFetchMode("formateur", FetchMode.SELECT);
//		criteria.setFetchMode("typeautorite", FetchMode.SELECT);
		criteria.setFetchMode("mois", FetchMode.SELECT);
		criteria.setFetchMode("anne", FetchMode.SELECT);
		//criteria.setFirstResult(indice);
		criteria.addOrder(Order.desc("forDateDebut"));
		if(code!= null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(forDateDebut!=null){
			criteria.add(Restrictions.ge("forDateDebut",forDateDebut));
			}
		if(forDateFin!=null){
			criteria.add(Restrictions.le("forDateFin",forDateFin));
			}
		if(forlibelle!=null){
			criteria.add(Restrictions.eq("forLibelle",forlibelle));
			}
		if(forRef!=null){
			criteria.add(Restrictions.eq("forRef",forRef));
			}
		
		if(sygannee!=null){
			criteria.add(Restrictions.eq("anne",sygannee));
			}
		if(mois!=null){
			criteria.add(Restrictions.eq("mois",mois));
			}
	
		if(publie>0){
			criteria.add(Restrictions.eq("publie",publie));
		}
		if(pas>0){
			criteria.setMaxResults(pas);
		}
		if(annee != null)
			criteria.add(Restrictions.sqlRestriction(
			" SUBSTR(LTRIM(forDateDebut), 1, 4)="+annee ));
		
			ArrayList<SygProformation> avisattributions = (ArrayList<SygProformation>) criteria.list();
			return avisattributions;
	}


@Override
public List<SygProformation> find(int indice, int pas) {
   Criteria criteria = getHibernateSession().createCriteria(SygProformation.class);
	criteria.setFirstResult(indice);
	if(pas>0)
	criteria.setMaxResults(pas);
		
	return criteria.list();
	}

@Override
public int count(Long code, Date forDateDebut, Date forDateFin, String forlibelle, String forRef, String annee, SygAnnee sygannee, SygMois mois,int publie) {
	Criteria criteria = getHibernateSession().createCriteria(SygProformation.class);
//	criteria.setFetchMode("autorite", FetchMode.SELECT);
	criteria.setFetchMode("mois", FetchMode.SELECT);

	if(code!= null){
		criteria.add(Restrictions.eq("id", code));
	}
	if(forDateDebut!=null){
		criteria.add(Restrictions.ge("forDateDebut",forDateDebut));
		}
	if(forDateFin!=null){
		criteria.add(Restrictions.le("forDateFin",forDateFin));
		}
	if(forlibelle!=null){
		criteria.add(Restrictions.eq("forLibelle",forlibelle));
		}
	if(forRef!=null){
		criteria.add(Restrictions.eq("forRef",forRef));
		}
	
	if(sygannee!=null){
		criteria.add(Restrictions.eq("anne",sygannee));
		}
	if(mois!=null){
		criteria.add(Restrictions.eq("mois",mois));
		}
	if(publie>0){
		criteria.add(Restrictions.eq("publie",publie));
	}
	criteria.setProjection(Projections.rowCount());
	return Integer.parseInt(criteria.uniqueResult().toString());

}

}
