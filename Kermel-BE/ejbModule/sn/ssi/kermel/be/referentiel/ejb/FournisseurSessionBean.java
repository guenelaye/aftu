package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCatFournisseur;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class FournisseurSessionBean extends AbstractSessionBean implements
		FournisseurSession {

	@Override
	public SygFournisseur save(SygFournisseur fournisseur) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(fournisseur);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return fournisseur;	
	}

	@Override
	public void update(SygFournisseur fournisseur) {
		// TODO Auto-generated method stub
		try{
			getHibernateSession().merge(fournisseur);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygFournisseur.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygFournisseur> find(int indice, int pas, String nom,
			SygCatFournisseur categorie, SygAutoriteContractante autorite) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygFournisseur.class);
		criteria.addOrder(Order.asc("nom"));
		
		if(nom!=null)
			criteria.add(Restrictions.ilike("nom", "%"+nom+"%"));
		
		if(categorie!=null)
			criteria.add(Restrictions.eq("categorie", categorie));
		
//		if(autorite!=null)
//			criteria.add(Restrictions.eq("autorite", autorite));
			
		criteria.setFirstResult(indice);
		
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public int count(String nom, SygCatFournisseur categorie, SygAutoriteContractante autorite) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygFournisseur.class);
		
		if(nom!=null)
			criteria.add(Restrictions.ilike("nom", "%"+nom+"%"));
		
		if(categorie!=null)
			criteria.add(Restrictions.eq("categorie", categorie));
		
//		if(autorite!=null)
//			criteria.add(Restrictions.eq("autorite", autorite));
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public SygFournisseur findById(Long code) {
		// TODO Auto-generated method stub
		return (SygFournisseur)getHibernateSession().get(SygFournisseur.class, code);
	}

	//Add by rama 14-11-2012
	@SuppressWarnings("unchecked")
	@Override
	public List<SygFournisseur> findPaiement(int indice, int pas, String statut,String nom) {
		String libelle;
		if(nom!=null)
		{
		libelle="fournisseurs.nom LIKE '%"+nom+"%' and ";
		}
		else
		{
		libelle="";	
		}
		return getHibernateSession().createQuery("SELECT fournisseurs FROM  SygFournisseur fournisseurs where "+libelle
			      + " fournisseurs.id  IN (SELECT plis.fournisseur.id from SygPlisouvertures plis where " +
			   " plis.id IN( select  contrats.plis.id from  SygContrats contrats where contrats.constatus='"+statut+"'))")
					.setFirstResult(indice).setMaxResults(pas).list();		
	}
	
	@Override
	public int countPaiement (String statut,String nom) {
		// TODO Auto-generated method stub
		
		String libelle;
		if(nom!=null)
		{
		libelle="fournisseurs.nom LIKE '%"+nom+"%' and ";
		}
		else
		{
		libelle="";	
		}
		return getHibernateSession().createQuery("SELECT fournisseurs FROM  SygFournisseur fournisseurs where "+libelle
			      + " fournisseurs.id  IN (SELECT plis.fournisseur.id from SygPlisouvertures plis where " +
			   " plis.id IN( select  contrats.plis.id from  SygContrats contrats where contrats.constatus='"+statut+"'))")
					.list().size();		
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<SygFournisseur> findEnRegle(int indice, int pas, String statut,String nom) {
		String libelle;
		if(nom!=null)
		{
		libelle="fournisseurs.nom LIKE '%"+nom+"%' and ";
		}
		else
		{
		libelle="";	
		}
		return getHibernateSession().createQuery("SELECT fournisseurs FROM  SygFournisseur fournisseurs where "+libelle
			      + " fournisseurs.id  Not IN (SELECT distinct(plis.fournisseur.id) from SygPlisouvertures plis where " +
			   " plis.id IN( select  contrats.plis.id from  SygContrats contrats where contrats.constatus='"+statut+"')and (plis.id in (select contrats.plis.id from SygContrats contrats)))")
					.setFirstResult(indice).setMaxResults(pas).list();		
	}
	
	@Override
	public int countEnRegle (String statut,String nom) {
		// TODO Auto-generated method stub
		
		String libelle;
		if(nom!=null)
		{
		libelle="fournisseurs.nom LIKE '%"+nom+"%' and ";
		}
		else
		{
		libelle="";	
		}
		return getHibernateSession().createQuery("SELECT fournisseurs FROM  SygFournisseur fournisseurs where "+libelle
			      + " fournisseurs.id  NOT IN (SELECT distinct(plis.fournisseur.id) from SygPlisouvertures plis where " +
				   " plis.id IN( select  contrats.plis.id from  SygContrats contrats where contrats.constatus='"+statut+"')and (plis.id IN(SELECT contrats.plis.id from SygContrats contrats)))")
					.list().size();		
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Long> Paiement(String status) {
	    // TODO Auto-generated method stub
	   
	    Criteria criteria = getHibernateSession().createCriteria(SygContrats.class);
	    criteria.createAlias("plis", "plis");
	    if(status!=null){
	        criteria.add(Restrictions.eq("constatus", status));
	    }
	    criteria.setProjection(Projections.property("plis.id"));
	    return criteria.list();
	}


	// criteria.add(Restrictions.not(Restrictions.in("plis.id", Paiement(statut))));
	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public SygFournisseur findExist(String ninea ) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygFournisseur.class);
		//criteria.addOrder(Order.asc("nom"));
		
		if(ninea!=null)
			criteria.add(Restrictions.eq("ninea", ninea));
		
        if(criteria.list().size()>0) return  (SygFournisseur) criteria.list().get(0);
             else
            	 return null;
	}
	
	
	
	

}
