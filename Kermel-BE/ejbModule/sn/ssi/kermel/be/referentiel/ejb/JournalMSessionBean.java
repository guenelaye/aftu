package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygJournal;
import sn.ssi.kermel.be.entity.SygService;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class JournalMSessionBean extends AbstractSessionBean implements JournalMSession{

	@Override
	public int count(String code,String libelle,SygAutoriteContractante autorite, Long id) {
		Criteria criteria = getHibernateSession().createCriteria(SygJournal.class);
	//	criteria.createAlias("type", "type");
		/*criteria.createAlias("autorite", "autorite");
		if(code!=null){
			criteria.add(Restrictions.ilike("codification", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(id!=null){
			criteria.add(Restrictions.eq("id", id));
		}
		   */
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygJournal.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygJournal> find(int indice, int pas,String code,String libelle,SygAutoriteContractante autorite, Long id) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygJournal.class);
	//	criteria.createAlias("type", "type");
     /*	
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.asc("libelle"));
		if(code!=null){
			criteria.add(Restrictions.ilike("codification", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(id!=null){
			criteria.add(Restrictions.eq("id", id));
		}
		  */
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygJournal service) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(service);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygJournal service) {
		
		try{
			getHibernateSession().merge(service);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygJournal findById(Long code) {
		return (SygJournal)getHibernateSession().get(SygJournal.class, code);
	}
}
