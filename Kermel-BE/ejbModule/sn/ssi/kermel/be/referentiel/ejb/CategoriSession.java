package sn.ssi.kermel.be.referentiel.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCategori;

@Remote
public interface CategoriSession {
	public void save(SygCategori categori);
	public void delete(Long id);
	public List<SygCategori> find(int indice, int pas,String libelle);
	public int count(String libelle,String service);
	public void update(SygCategori categori);
	public SygCategori findById(Long code);
	
	public List<SygCategori> findRech(int indice, int pas,String libelle,Long service, Long cat);
	public int countRech(String libelle,Long service, Long cat);
}
