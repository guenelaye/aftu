package sn.ssi.kermel.be.referentiel.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygDelais;

@Remote
public interface DelaisSession {
	public void save(SygDelais delai);
	public void delete(Long id);
	public List<SygDelais> find(int indice, int pas,String code,String libelle,Date datedebut,Date datefin);
	public int count(String code,String libelle,Date datedebut,Date datefin);
	public void update(SygDelais delai);
	public SygDelais findById(Long code);
	public List<SygDelais> find(String code);
}
