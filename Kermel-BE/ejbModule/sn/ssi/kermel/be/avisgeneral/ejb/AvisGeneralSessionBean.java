package sn.ssi.kermel.be.avisgeneral.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygAvisGeneral;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class AvisGeneralSessionBean extends AbstractSessionBean implements AvisGeneralSession{

	
	@Override
	public int count(SygAutoriteContractante autorite,String Numero,String annee) {
		Criteria criteria = getHibernateSession().createCriteria(SygAvisGeneral.class);
		//criteria.createAlias("realisation", "realisation");
		//criteria.createAlias("autorite", "autorite");
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(annee!=null){
			criteria.add(Restrictions.eq("annee", annee));
		}
		if(Numero!=null){
			criteria.add(Restrictions.eq("Numero", Numero));
		}
	
		criteria.setProjection(Projections.rowCount());
		
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygAvisGeneral.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygAvisGeneral> find(int indice, int pas,SygAutoriteContractante autorite,String Numero,String annee) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAvisGeneral.class);
		//criteria.createAlias("realisation", "realisation");
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.asc("id"));
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(annee!=null){
			criteria.add(Restrictions.eq("annee", annee));
		}
		if(Numero!=null){
			criteria.add(Restrictions.eq("Numero", Numero));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygAvisGeneral avis) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(avis);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygAvisGeneral avis) {
		
		try{
			getHibernateSession().merge(avis);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygAvisGeneral findById(Long code) {
		return (SygAvisGeneral)getHibernateSession().get(SygAvisGeneral.class, code);
	}
	

	//@Override
//	public SygAvisGeneral getContrat(SygAutoriteContractante autorite){
//		// TODO Auto-generated method stub
//		Criteria criteria = getHibernateSession().createCriteria(SygAvisGeneral.class);
////		criteria.createAlias("realisation", "realisation");
//		criteria.createAlias("autorite", "autorite");
//		if(autorite!=null){
//			criteria.add(Restrictions.eq("autorite", autorite));
//		}
////		if(realisation!=null){
////			criteria.add(Restrictions.eq("realisation", realisation));
////		}
//		if(criteria.list().size()>0)
//		  return (SygAvisGeneral) criteria.list().get(0);
//		else 
//			return null;
//	}


}
