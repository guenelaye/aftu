package sn.ssi.kermel.be.avisgeneral.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygAvisGeneral;
import sn.ssi.kermel.be.entity.SygRealisations;

@Remote
public interface AvisGeneralSession {
	public void save(SygAvisGeneral avis);
	public void delete(Long id);
	public List<SygAvisGeneral> find(int indice, int pas,SygAutoriteContractante autorite,String Numero,String annee);
	public int count(SygAutoriteContractante autorite,String Numero,String annee);
	public void update(SygAvisGeneral avis);
	public SygAvisGeneral findById(Long code);
	
	//public SygAvisGeneral getContrat(SygAutoriteContractante autorite,SygRealisations realisation);
	
}
