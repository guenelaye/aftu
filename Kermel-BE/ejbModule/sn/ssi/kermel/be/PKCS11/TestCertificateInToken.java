package sn.ssi.kermel.be.PKCS11;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.CertSelector;
import java.security.cert.CertStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import org.bouncycastle.cms.CMSProcessable;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.codec.Base64.InputStream;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
import com.itextpdf.text.pdf.security.PdfPKCS7;
import com.itextpdf.text.pdf.security.PrivateKeySignature;

public class TestCertificateInToken {

	/**
	 * @param args
	 */
	
	static String fname  = "C:/CAC/HelloWorld.pdf" ;
	 
	/**
	 * Nom du document PDF g�n�r� sign�
	 */
	static String fnameS = "C:/CAC/HelloWorld_sign.pdf" ;
	public void getCertificats(String pin){

		
			char[] password = null;
			String configName = "C:/gemsafe/pkcs11.cfg";
			//java.security.Provider oProvider = new sun.security.pkcs11.SunPKCS11(configName);
			Security.addProvider(new BouncyCastleProvider());
			
			

			try {
				
				/*Chargement des certificats du token*/
				KeyStore oKeyStoreToken = KeyStore.getInstance("PKCS11");
				password = pin.toCharArray();
				oKeyStoreToken.load(null, password);
				/*Fin chargement*/
				
				/*Recup�ration des certifvcat private Key et public key*/
				X509Certificate cert = null;
				PrivateKey privatekey = null;
				PublicKey publickey = null;
				
				Enumeration en = oKeyStoreToken.aliases();
				//System.out.println(en.);
				String ALIAS = "";
				Vector vectaliases = new Vector();
								while (en.hasMoreElements())
					vectaliases.add(en.nextElement());
				System.out.println("Taille vector:"+vectaliases.size());
				String[] aliases = (String []) (vectaliases.toArray(new String[0]));
				System.out.println("Taille tableau:"+aliases.length);
				for (int i = 0; i < aliases.length; i++)
					if (oKeyStoreToken.isKeyEntry(aliases[i]))
					{
						ALIAS = aliases[i];
						java.security.cert.Certificate[] chain = oKeyStoreToken.getCertificateChain(ALIAS);
						System.out.println("Alias chaine"+chain[i]);
						if(ALIAS.startsWith("CertiNomis")){
							System.out.println("Alias: >"+i+":"+ALIAS);
							break;
						}
					}
				privatekey = (PrivateKey)oKeyStoreToken.getKey(ALIAS, password);
				cert = (X509Certificate)oKeyStoreToken.getCertificate(ALIAS);
				publickey = oKeyStoreToken.getCertificate(ALIAS).getPublicKey();
				 System.out.println("Key: >"+ALIAS +":"+ privatekey.toString() + "<");
				 
				  cert = (X509Certificate)oKeyStoreToken.getCertificate(ALIAS);
				  
				  //Details du certificats
				  String[] infos_emetteur = cert.getIssuerDN().getName().split("(=|, )", -1);
				  String[] infos_titulaire = cert.getSubjectDN().getName().split("(=|, )", -1);

				  System.out.println("CommonName : " + infos_titulaire[3]);
				  System.out.println("EmailAdresse : " + infos_titulaire[1] + "\n");

				  for (int i = 0; i < infos_emetteur.length; i += 2)
				  {
				  	if (infos_emetteur[i].equals("C"))
				  		System.out.println("CountryName : " + infos_emetteur[i + 1]);
				  	if (infos_emetteur[i].equals("O"))
				  		System.out.println("OrganizationName : " + infos_emetteur[i + 1]);
				  	if (infos_emetteur[i].equals("CN"))
				  		System.out.println("CommonName : " + infos_emetteur[i + 1]);
				  }

				  System.out.println("");
				  System.out.println("du : " + cert.getNotBefore());
				  System.out.println("au : " + cert.getNotAfter());
				// System.out.println("Certificat: "+cert.toString());
			} catch (Exception e) {
				e.printStackTrace();
				return ;
			}
			
			
		 
		}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//TestCertificateInToken certificateInToken = new TestCertificateInToken();
		//certificateInToken.getCertificats("20112012");
		
		try {
			buildPDF() ;
			signPdf() ;
		
		}
		catch(Exception e) { }
	}
	
	/**
	 * Cr�ation d'un simple document PDF "Hello World"
	 */
	public static void buildPDF() {
 
		// Creation du document
		Document document = new Document();
 
		try {
			// Creation du "writer" vers le doc
			// directement vers un fichier
			PdfWriter.getInstance(document,
					new FileOutputStream(fname));
			// Ouverture du document
			document.open();
 
			// Ecriture des datas
			document.add(new Paragraph("Hello World"));
 
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
 
		// Fermeture du document
		document.close();
 
	}
	/**
	 * Signature du document
	 */
	public static final boolean signPdf()
			throws IOException, DocumentException, Exception
	{
		// Vous devez preciser ici le chemin d'acces a votre clef pkcs12
		String fileKey          = "C:/CAC/kermel.p12" ;
		// et ici sa "passPhrase"
		String fileKeyPassword  = "ssissi" ;
 
		try {
			// Creation d'un KeyStore
			KeyStore ks = KeyStore.getInstance("pkcs12");
			// Chargement du certificat p12 dans el magasin
			ks.load(new FileInputStream(fileKey), fileKeyPassword.toCharArray());
			String alias = (String)ks.aliases().nextElement();
			// Recup�ration de la clef priv�e
			PrivateKey key = (PrivateKey)ks.getKey(alias, fileKeyPassword.toCharArray());
			// et de la chaine de certificats
			Certificate[] chain = ks.getCertificateChain(alias);
 
			// Lecture du document source
			PdfReader pdfReader = new PdfReader((new File(fname)).getAbsolutePath());
			File outputFile = new File(fnameS);
			// Creation du tampon de signature
			PdfStamper pdfStamper;
			pdfStamper = PdfStamper.createSignature(pdfReader, null, '\0', outputFile);
			PdfSignatureAppearance sap = pdfStamper.getSignatureAppearance();
			
			
			// Position du tampon sur la page (ici en bas a gauche page 1)
			sap.setVisibleSignature(new Rectangle(10, 10, 50, 30), 1, "sign_rbl");
 
			pdfStamper.setFormFlattening(true);
			pdfStamper.close();
 
			return true;
		}
		catch (Exception key) {
			throw new Exception(key);
		}
	}	
	
	public static void checkSignature(){
		try {
			// Chargement du fichier sign�
			File f = new File("fichier_signer.txt.pk7");
			byte[] buffer = new byte[(int)f.length()];
			DataInputStream in = new DataInputStream(new FileInputStream(f));
			in.readFully(buffer);
			in.close();

			
			CMSSignedData signature = new CMSSignedData(buffer);
			SignerInformation signer = (SignerInformation)signature
		                .getSignerInfos().getSigners().iterator().next();
			CertStore cs = null;
//					signature
//		                .getCertificatesAndCRLs("Collection", "BC");
			Iterator iter= cs.getCertificates((CertSelector) signer.getSID()).iterator();
			X509Certificate certificate = (X509Certificate) iter.next();
			CMSProcessable sc = signature.getSignedContent();
			byte[] data = (byte[]) sc.getContent();

			// Verifie la signature
//			System.out.println(signer.verify(certificate, "BC"));

			FileOutputStream envfos = new FileOutputStream("document_non_signer.txt");
			envfos.write(data);
			envfos.close();
		} catch (Exception e) {
			e.printStackTrace();
			return ;
		}
	}
	
	public static void sign(String src, String dest,
			Certificate[] chain, PrivateKey pk, String digestAlgorithm, String provider,
			CryptoStandard subfilter, String reason, String location)
			throws GeneralSecurityException, IOException, DocumentException {
			// Creating the reader and the stamper
			PdfReader reader = new PdfReader(src);
			FileOutputStream os = new FileOutputStream(dest);
			PdfStamper stamper = PdfStamper.createSignature(reader, os, '\0');
			// Creating the appearance
			PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
			appearance.setReason(reason);
			appearance.setLocation(location);
			appearance.setVisibleSignature(new Rectangle(36, 748, 144, 780), 1, "sig");
			// Creating the signature
			ExternalDigest digest = new BouncyCastleDigest();
			ExternalSignature signature =
			new PrivateKeySignature(pk, digestAlgorithm, provider);
			MakeSignature.signDetached(appearance, digest, signature, chain,
			null, null, null, 0, subfilter);
			}
}

