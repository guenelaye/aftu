package sn.ssi.kermel.be.PKCS11;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Vector;

import javax.crypto.Cipher;

import org.bouncycastle.cms.CMSEnvelopedData;
import org.bouncycastle.cms.CMSEnvelopedDataGenerator;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.KeyTransRecipientInformation;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfAnnotation;
import com.itextpdf.text.pdf.PdfAppearance;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.DigestAlgorithms;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
import com.itextpdf.text.pdf.security.PdfPKCS7;
import com.itextpdf.text.pdf.security.PrivateKeySignature;

public class SignFilePDF {

	/**
	 * @param args
	 */
	public static final String KEYSTORE = "C:/CAC/dcmp.p12";
	public static final char[] PASSWORD = "ssissi".toCharArray();
	public static final String SRC = "C:/CAC/hello.pdf";
	public static final String DEST = "C:/CAC/hello_signed.pdf";
	private static final String SIGNAME = "KERMEL";
	public static final String PROPS ="C:/gemsafe/pkcs11.cfg";
	static X509Certificate cert = null;
	static PrivateKey privatekey = null;
	static PublicKey publickey = null;

	public static void main(String[] args) throws GeneralSecurityException,
			IOException, DocumentException {
		//accuse();
		
		if(verifySignatures("C:/DepotPli/S_DAGE_0154/EiyKcesK/pli13.pdf")){
			System.out.println("ok");
			
		}else
			System.out.println("no ok");
		//Properties properties = new Properties();
	//	properties.load(new FileInputStream(PROPS));
		/*BouncyCastleProvider providerBC = new BouncyCastleProvider();
		Security.addProvider(providerBC);
//		FileInputStream fis = new FileInputStream(PROPS);
//		Provider providerPKCS11 = new SunPKCS11(fis);
//		Security.addProvider(providerPKCS11);
		//String configName = 
	//	java.security.Provider provider = new sun.security.pkcs11.SunPKCS11(configName);

	//	BouncyCastleProvider provider = new BouncyCastleProvider();
		//Security.addProvider(provider);
	//	KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		KeyStore ks = KeyStore.getInstance("PKCS12");
		ks.load(new FileInputStream(KEYSTORE), PASSWORD);
		//ks.load(null, PASSWORD);
		Enumeration en = ks.aliases();
		
		String ALIAS = "";
		Vector vectaliases = new Vector();
						while (en.hasMoreElements())
			vectaliases.add(en.nextElement());
		System.out.println("Taille vector:"+vectaliases.size());
		String[] aliases = (String []) (vectaliases.toArray(new String[0]));
		System.out.println("Taille tableau:"+aliases.length);
		for (int i = 0; i < aliases.length; i++)
			if (ks.isKeyEntry(aliases[i]))
			{
				ALIAS = aliases[i];
				java.security.cert.Certificate[] chain = ks.getCertificateChain(ALIAS);
				System.out.println("Alias chaine"+chain[i]);
				if(ALIAS.startsWith("CertiNomis")){
					System.out.println("Alias: >"+i+":"+ALIAS);
					break;
				}
			}
		privatekey = (PrivateKey)ks.getKey(ALIAS, PASSWORD);
		cert = (X509Certificate)ks.getCertificate(ALIAS);
		publickey = ks.getCertificate(ALIAS).getPublicKey();
		String alias = (String) ks.aliases().nextElement();
		PrivateKey pk = (PrivateKey) ks.getKey(alias, PASSWORD);
		Certificate[] chain = ks.getCertificateChain(alias);
	//	privatekey = pk;
	//	publickey = ks.getCertificate(alias).getPublicKey();
	//	cert = (X509Certificate) chain[0];
		cert = (X509Certificate)ks.getCertificate(ALIAS);
		SignFilePDF app = new SignFilePDF();
		String fileToSign = "C:/CAC/caleb.pdf";
		String fileToSigned = "C:/CAC/calebSigne01.pdf";
		app.createPdf(fileToSign);
		app.sign(fileToSign, String.format(fileToSigned, 1), chain, pk,
				DigestAlgorithms.SHA256, providerBC.getName(),
				CryptoStandard.CMS, "Test 1", "Ghent");
//		app.sign(fileToSign, String.format(fileToSigned, 2), chain, pk,
//				DigestAlgorithms.SHA512, providerPKCS11.getName(),
//				CryptoStandard.CMS, "Test 2", "Ghent");
//		app.sign(fileToSign, String.format(fileToSigned, 3), chain, pk,
//				DigestAlgorithms.SHA256, providerPKCS11.getName(),
//				CryptoStandard.CADES, "Test 3", "Ghent");
//		app.sign(fileToSign, String.format(fileToSigned, 4), chain, pk,
//				DigestAlgorithms.RIPEMD160, providerPKCS11.getName(),
//				CryptoStandard.CADES, "Test 4", "Ghent");

		// app.sign(ALICE, PdfSignatureAppearance.CERTIFIED_FORM_FILLING, FORM,
		// "sig1", String.format(DEST, "alice"));
		// app.sign(BOB, PdfSignatureAppearance.NOT_CERTIFIED,
		// String.format(DEST, "alice"),
		// "sig2", String.format(DEST, "bob"));
		// app.sign(KEYSTORE, PdfSignatureAppearance.NOT_CERTIFIED,
		// String.format(DEST, "bob"),
		// "sig3", String.format(DEST, "carol"));
		// app.addAnnotation(fileToSign,fileToSigned);
		// app.addText(fileToSign, fileToSigned);
		app.verifySignatures(fileToSigned);
//		byte[] src = app.encrypt2(fileToSign);
//		app.decrypt2(src, "filederypt");
		System.exit(0);
		
		*/
	}

	public  void  tester(){
		System.out.println(" signature des documents");
		
	}
	
	public  static void  accuse(){
		
		BouncyCastleProvider providerBC = new BouncyCastleProvider();
		Security.addProvider(providerBC);
//		FileInputStream fis = new FileInputStream(PROPS);
//		Provider providerPKCS11 = new SunPKCS11(fis);
//		Security.addProvider(providerPKCS11);
		//String configName = 
	//	java.security.Provider provider = new sun.security.pkcs11.SunPKCS11(configName);

	//	BouncyCastleProvider provider = new BouncyCastleProvider();
		//Security.addProvider(provider);
	//	KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		KeyStore ks=null;
		try {
			ks = KeyStore.getInstance("PKCS12");
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			ks.load(new FileInputStream(KEYSTORE), PASSWORD);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//ks.load(null, PASSWORD);
		Enumeration en=null;
		try {
			en = ks.aliases();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String ALIAS = "";
		Vector vectaliases = new Vector();
						while (en.hasMoreElements())
			vectaliases.add(en.nextElement());
		System.out.println("Taille vector:"+vectaliases.size());
		String[] aliases = (String []) (vectaliases.toArray(new String[0]));
		System.out.println("Taille tableau:"+aliases.length);
		for (int i = 0; i < aliases.length; i++)
			try {
				if (ks.isKeyEntry(aliases[i]))
				{
					ALIAS = aliases[i];
					java.security.cert.Certificate[] chain = ks.getCertificateChain(ALIAS);
					System.out.println("Alias chaine"+chain[i]);
					if(ALIAS.startsWith("CertiNomis")){
						System.out.println("Alias: >"+i+":"+ALIAS);
						break;
					}
				}
			} catch (KeyStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		try {
			privatekey = (PrivateKey)ks.getKey(ALIAS, PASSWORD);
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			cert = (X509Certificate)ks.getCertificate(ALIAS);
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			publickey = ks.getCertificate(ALIAS).getPublicKey();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String alias=null;
		try {
			alias = (String) ks.aliases().nextElement();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PrivateKey pk=null;
		try {
			pk = (PrivateKey) ks.getKey(alias, PASSWORD);
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Certificate[] chain=null;
		try {
			chain = ks.getCertificateChain(alias);
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	//	privatekey = pk;
	//	publickey = ks.getCertificate(alias).getPublicKey();
	//	cert = (X509Certificate) chain[0];
		try {
			cert = (X509Certificate)ks.getCertificate(ALIAS);
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SignFilePDF app = new SignFilePDF();
		String fileToSign = "C:/CAC/caleb.pdf";
		String fileToSigned = "C:/CAC/calebSigne01.pdf";
		try {
			app.createPdf(fileToSign);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			app.sign(fileToSign, String.format(fileToSigned, 1), chain, pk,
					DigestAlgorithms.SHA256, providerBC.getName(),
					CryptoStandard.CMS, "Test 1", "Ghent");
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		app.sign(fileToSign, String.format(fileToSigned, 2), chain, pk,
//				DigestAlgorithms.SHA512, providerPKCS11.getName(),
//				CryptoStandard.CMS, "Test 2", "Ghent");
//		app.sign(fileToSign, String.format(fileToSigned, 3), chain, pk,
//				DigestAlgorithms.SHA256, providerPKCS11.getName(),
//				CryptoStandard.CADES, "Test 3", "Ghent");
//		app.sign(fileToSign, String.format(fileToSigned, 4), chain, pk,
//				DigestAlgorithms.RIPEMD160, providerPKCS11.getName(),
//				CryptoStandard.CADES, "Test 4", "Ghent");

		// app.sign(ALICE, PdfSignatureAppearance.CERTIFIED_FORM_FILLING, FORM,
		// "sig1", String.format(DEST, "alice"));
		// app.sign(BOB, PdfSignatureAppearance.NOT_CERTIFIED,
		// String.format(DEST, "alice"),
		// "sig2", String.format(DEST, "bob"));
		// app.sign(KEYSTORE, PdfSignatureAppearance.NOT_CERTIFIED,
		// String.format(DEST, "bob"),
		// "sig3", String.format(DEST, "carol"));
		// app.addAnnotation(fileToSign,fileToSigned);
		// app.addText(fileToSign, fileToSigned);
		try {
			app.verifySignatures(fileToSigned);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		byte[] src = app.encrypt2(fileToSign);
//		app.decrypt2(src, "filederypt");
		System.exit(0);
		
	}
	
	
	public void sign(String src, String dest, Certificate[] chain,
			PrivateKey pk, String digestAlgorithm, String provider,
			CryptoStandard subfilter, String reason, String location)
			throws GeneralSecurityException, IOException, DocumentException {
		// Creating the reader and the stamper
		PdfReader reader = new PdfReader(src);
		FileOutputStream os = new FileOutputStream(dest);
		PdfStamper stamper = PdfStamper.createSignature(reader, os, '\0');
		// Creating the appearance
		PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
		appearance.setReason(reason);
		appearance.setLocation(location);
		appearance.setVisibleSignature(new Rectangle(36, 748, 144, 780), 1,
				"sig");
		// Creating the signature
		ExternalDigest digest = new BouncyCastleDigest();
		ExternalSignature signature = new PrivateKeySignature(pk,
				digestAlgorithm, provider);
		
		
		MakeSignature.signDetached(appearance, digest, signature, chain, null,
				null, null, 0, subfilter);
	}

	public void createPdf(String filename) throws IOException,
			DocumentException {
		// step 1: Create a Document
		Document document = new Document();
		// step 2: Create a PdfWriter
		PdfWriter writer = PdfWriter.getInstance(document,
				new FileOutputStream(filename));
		// step 3: Open the Document
		document.open();
		// step 4: Add content
		document.add(new Paragraph("Hello World!"));
		// create a signature form field
		PdfFormField field = PdfFormField.createSignature(writer);
		field.setFieldName(SIGNAME);
		// set the widget properties
		field.setPage();
		field.setWidget(new Rectangle(72, 732, 144, 780),
				PdfAnnotation.HIGHLIGHT_INVERT);
		field.setFlags(PdfAnnotation.FLAGS_PRINT);
		// add it as an annotation
		writer.addAnnotation(field);
		// maybe you want to define an appearance
		PdfAppearance tp = PdfAppearance.createAppearance(writer, 72, 48);
		tp.setColorStroke(BaseColor.BLUE);
		tp.setColorFill(BaseColor.LIGHT_GRAY);
		tp.rectangle(0.5f, 0.5f, 71.5f, 47.5f);
		tp.fillStroke();
		tp.setColorFill(BaseColor.BLUE);
		ColumnText.showTextAligned(tp, com.itextpdf.text.Element.ALIGN_CENTER,
				new Phrase("SIGN HERE"), 36, 24, 25);
		field.setAppearance(PdfAnnotation.APPEARANCE_NORMAL, tp);
		// step 5: Close the Document
		document.close();
	}

	public void addAnnotation(String src, String dest) throws IOException,
			DocumentException {
		PdfReader reader = new PdfReader(src);
		PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest),
				'\0', true);
		PdfAnnotation comment = PdfAnnotation.createText(stamper.getWriter(),
				new Rectangle(200, 800, 250, 820), "Finally Signed!",
				"Ousseynou SOW Specimen has finally signed the document", true,
				"Comment");
		stamper.addAnnotation(comment, 1);
		stamper.close();
	}

	public void addText(String src, String dest) throws IOException,
			DocumentException {
		PdfReader reader = new PdfReader(src);
		PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest),
				'\0', true);
		ColumnText.showTextAligned(stamper.getOverContent(1),
				com.itextpdf.text.Element.ALIGN_LEFT, new Phrase("TOP SECRET"),
				36, 820, 0);
		stamper.close();
	}

	public void signAgain(String src, String dest, Certificate[] chain,
			PrivateKey pk, String digestAlgorithm, String provider,
			CryptoStandard subfilter, String reason, String location)
			throws GeneralSecurityException, IOException, DocumentException {
		// Creating the reader and the stamper
		PdfReader reader = new PdfReader(src);
		FileOutputStream os = new FileOutputStream(dest);
		PdfStamper stamper = PdfStamper.createSignature(reader, os, '\0', null,
				true);
		// Creating the appearance
		PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
		appearance.setReason(reason);
		appearance.setLocation(location);
		appearance.setVisibleSignature(new Rectangle(36, 700, 144, 732), 1,
				"Signature2");
		// Creating the signature
		ExternalSignature pks = new PrivateKeySignature(pk, digestAlgorithm,
				provider);
		ExternalDigest digest = new BouncyCastleDigest();
		MakeSignature.signDetached(appearance, digest, pks, chain, null, null,
				null, 0, subfilter);
	}

	public void sign(String keystore, int level, String src, String name,
			String dest) throws GeneralSecurityException, IOException,
			DocumentException {
		//KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		KeyStore	ks = KeyStore.getInstance("PKCS12");
		ks.load(new FileInputStream(keystore), PASSWORD);
		String alias = (String) ks.aliases().nextElement();
		PrivateKey pk = (PrivateKey) ks.getKey(alias, PASSWORD);
		Certificate[] chain = ks.getCertificateChain(alias);
		// Creating the reader and the stamper
		PdfReader reader = new PdfReader(src);
		FileOutputStream os = new FileOutputStream(dest);
		PdfStamper stamper = PdfStamper.createSignature(reader, os, '\0', null,
				true);
		// Creating the appearance
		PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
		appearance.setVisibleSignature(name);
		appearance.setCertificationLevel(level);
		// Creating the signature
		ExternalSignature pks = new PrivateKeySignature(pk, "SHA-256", "BC");
		ExternalDigest digest = new BouncyCastleDigest();
		MakeSignature.signDetached(appearance, digest, pks, chain, null, null,
				null, 0, CryptoStandard.CMS);
	}

	public static boolean verifySignatures(String path) throws IOException,
			GeneralSecurityException {
		boolean signed = false;
		System.out.println(path);
		PdfReader reader = new PdfReader(path);
		AcroFields fields = reader.getAcroFields();
		ArrayList<String> names = fields.getSignatureNames();
		for (String name : names) {
			System.out.println("===== " + name + " =====");
			verifySignature(fields, name);
			signed = true;
			break;
		}
		return signed;
	}

	
	
	public  boolean verifySignaturesDocument(String path) throws IOException,
	GeneralSecurityException {
boolean signed = false;
System.out.println(path);
PdfReader reader = new PdfReader(path);
AcroFields fields = reader.getAcroFields();
ArrayList<String> names = fields.getSignatureNames();
for (String name : names) {
	System.out.println("===== " + name + " =====");
	verifySignature(fields, name);
	signed = true;
	break;
}
return signed;
}
	
	
	public static PdfPKCS7 verifySignature(AcroFields fields, String name)
			throws GeneralSecurityException, IOException {
		System.out.println("Signature covers whole document: "
				+ fields.signatureCoversWholeDocument(name));
		System.out.println("Document revision: " + fields.getRevision(name)
				+ " of " + fields.getTotalRevisions());
		PdfPKCS7 pkcs7 = fields.verifySignature(name);
		System.out.println("Integrity check OK? " + pkcs7.verify());
		return pkcs7;
	}

	public String encryptDecrypt(Key publickey, String src) {
		try {
			// Chargement du fichier � chiffrer
			File f = new File(src);
			byte[] buffer = new byte[(int) f.length()];
			DataInputStream in = new DataInputStream(new FileInputStream(f));
			in.readFully(buffer);
			in.close();

			// Chiffrement du document
			// Seul le mode ECB est possible avec RSA
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC");
			// publickey est la cle publique du destinataire
			cipher.init(Cipher.ENCRYPT_MODE, publickey, new SecureRandom(
					"nyal rand".getBytes()));
			int blockSize = cipher.getBlockSize();
			int outputSize = cipher.getOutputSize(buffer.length);
			int leavedSize = buffer.length % blockSize;
			int blocksSize = leavedSize != 0 ? buffer.length / blockSize + 1
					: buffer.length / blockSize;
			byte[] raw = new byte[outputSize * blocksSize];
			int i = 0;
			while (buffer.length - i * blockSize > 0) {
				if (buffer.length - i * blockSize > blockSize)
					cipher.doFinal(buffer, i * blockSize, blockSize, raw, i
							* outputSize);
				else
					cipher.doFinal(buffer, i * blockSize, buffer.length - i
							* blockSize, raw, i * outputSize);
				i++;
			}

			// Ecriture du fichier chiffr� sur le disque dur
			FileOutputStream envfos = new FileOutputStream("C:/CAC/"
					+ f.getName() + ".p1");
			envfos.write(raw);
			envfos.close();

			return "C:/CAC/" + f.getName() + ".p1";

		} catch (Exception e) {

			e.printStackTrace();
			return null;
		}

	}

	public void decrypt(Key privatekey, String src, String dest) {
		// D�chiffrement du fichier

		try {
			File f = new File(src);
			byte[] buffer = new byte[(int) f.length()];
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC");

			//
			// La variable privatekey correspond � la cl� priv�e associ�e � la
			// cl�
			// publique pr�c�dente. (Voir la section sur le fichier PKCS#12 pour
			// recup�rer la cl� priv�e)
			//
			int blockSize = 0;

			cipher.init(cipher.DECRYPT_MODE, privatekey);
			blockSize = cipher.getBlockSize();
			int outputSize = cipher.getOutputSize(buffer.length);
			int leavedSize = buffer.length % blockSize;
			int blocksSize = leavedSize != 0 ? buffer.length / blockSize + 1
					: buffer.length / blockSize;
			byte[] raw = new byte[outputSize * blocksSize];
			// int i = 0;

			ByteArrayOutputStream bout = new ByteArrayOutputStream(64);
			int j = 0;

			while (raw.length - j * blockSize > 0) {
				bout.write(cipher.doFinal(raw, j * blockSize, blockSize));
				j++;
			}

			FileOutputStream envfos = new FileOutputStream("C:/CAC/" + dest);
			envfos.write(bout.toByteArray());
			envfos.close();
		} catch (Exception e) {

		}
	}

	public byte[] encrypt2(String scr) {
		// Chargement du fichier � chiffrer
		try {
			File f = new File(scr);
			byte[] buffer = new byte[(int) f.length()];

			DataInputStream in = new DataInputStream(new FileInputStream(f));
			in.readFully(buffer);
			in.close();

			// Chiffrement du document

			CMSEnvelopedDataGenerator gen = new CMSEnvelopedDataGenerator();
			// La variable cert correspond au certificat du destinataire
			// La cl� publique de ce certificat servira � chiffrer la cl�
			// sym�trique
//			gen.addKeyTransRecipient((java.security.cert.X509Certificate) cert);

			// Choix de l'algorithme � cl� sym�trique pour chiffrer le document.
			// AES est un standard. Vous pouvez donc l'utiliser sans crainte.
			// Il faut savoir qu'en france la taille maximum autoris�e est de
			// 128
			// bits pour les cl�s sym�triques (ou cl�s secr�tes)
			String algorithm = CMSEnvelopedDataGenerator.AES128_CBC;
			CMSEnvelopedData envData=null ;
//					gen.generate(
//					new CMSProcessableByteArray(buffer), algorithm, "BC");

			byte[] pkcs7envelopedData = envData.getEncoded();

			// Ecriture du document chiffr�
			FileOutputStream envfos = new FileOutputStream("C:/CAC/"
					+ f.getName() + ".pk7");
			envfos.write(pkcs7envelopedData);
			envfos.close();
			return pkcs7envelopedData;
		} catch (Exception exception) {
			exception.printStackTrace();
			return null;
		}
	}

	public void decrypt2(byte[] pkcs7envelopedData, String dest) {
		try {
			// D�chiffrement du fichier

			CMSEnvelopedData ced = new CMSEnvelopedData(pkcs7envelopedData);
			Collection recip = ced.getRecipientInfos().getRecipients();

			KeyTransRecipientInformation rinfo = (KeyTransRecipientInformation) recip
					.iterator().next();
			// privatekey est la cl� priv�e permettant de d�chiffrer la cl�
			// secr�te
			// (sym�trique)
			byte[] contents = null;
//					rinfo.getContent(privatekey, "BC");

			FileOutputStream envfos = new FileOutputStream("C:/CAC/" + dest);
			envfos.write(contents);
			envfos.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
