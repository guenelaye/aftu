package sn.ssi.kermel.be.PKCS11;

import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class DigestBC extends DigestDefault {
	public static final BouncyCastleProvider PROVIDER = new BouncyCastleProvider();
	static {
		Security.addProvider(PROVIDER);
	}

	protected DigestBC(String password, String algorithm)
			throws GeneralSecurityException {
		super(password, algorithm, PROVIDER.getName());
	}

	public static DigestBC getInstance(String password, String algorithm,
			String provider) throws NoSuchAlgorithmException,
			NoSuchProviderException {
		DigestBC dbc = null;;
		try {
			dbc = new DigestBC(password, algorithm);
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dbc;
	}
}