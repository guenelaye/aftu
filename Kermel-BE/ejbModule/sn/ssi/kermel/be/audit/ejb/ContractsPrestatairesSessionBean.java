package sn.ssi.kermel.be.audit.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygContratsPrestataires;
import sn.ssi.kermel.be.entity.SygPrestataire;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class ContractsPrestatairesSessionBean extends AbstractSessionBean implements ContractsPrestatairesSession{

	@Override
	public int count(Date datedebutcontract,Date datefincontract) {
		Criteria criteria = getHibernateSession().createCriteria(SygContratsPrestataires.class);
		if(datedebutcontract!=null){
			criteria.add(Restrictions.ge("date",datedebutcontract));
		}	
		if(datefincontract!=null){
			criteria.add(Restrictions.ge("date",datefincontract));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public int count(String numcontract,Date datedebutcontract,Date datefincontract,SygAudit audit,SygPrestataire prestataire) {
					Criteria criteria = getHibernateSession().createCriteria(SygContratsPrestataires.class);
					criteria.setProjection(Projections.rowCount());
//					criteria.createAlias("audit", "audit");
				    criteria.createAlias("prestataire", "prestataire");
					criteria.addOrder(Order.desc("datedebutcontract"));
					if(numcontract!=null){
						criteria.add(Restrictions.eq("numcontract",numcontract));
						}
					if(datedebutcontract!=null){
						criteria.add(Restrictions.ge("datedebutcontract",datedebutcontract));
						}
					if(datefincontract!=null){
						criteria.add(Restrictions.ge("datefincontract",datefincontract));
						}
					if(prestataire!=null){
						criteria.add(Restrictions.eq("prestataire",prestataire));
						}
					if(audit!=null){
//						criteria.add(Restrictions.eq("audit",audit));
						}
					return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long idcontract) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygContratsPrestataires.class, idcontract));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	

	@Override
	public void save(SygContratsPrestataires contract) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(contract);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygContratsPrestataires contract) {
		
		try{
			getHibernateSession().merge(contract);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygContratsPrestataires findById(Long code) {
		return (SygContratsPrestataires)getHibernateSession().get(SygContratsPrestataires.class, code);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygContratsPrestataires> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygContratsPrestataires.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));
		
		return criteria.list();
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<SygContratsPrestataires> find(int indice, int pas,String numcontract,Date datedebutcontract,Date datefincontract,SygAudit audit,SygPrestataire prestataire) {
	    Criteria criteria = getHibernateSession().createCriteria(SygContratsPrestataires.class);
	    criteria.createAlias("audit", "audit");
	    criteria.createAlias("prestataire", "prestataire");
		criteria.addOrder(Order.desc("datedebutcontract"));
		if(numcontract!=null){
			criteria.add(Restrictions.eq("numcontract",numcontract));
			}
		if(datedebutcontract!=null){
			criteria.add(Restrictions.ge("datedebutcontract",datedebutcontract));
			}
		if(datefincontract!=null){
			criteria.add(Restrictions.ge("datefincontract",datefincontract));
			}
		
		if(audit!=null){
    		criteria.add(Restrictions.eq("audit",audit));
			}
		
		if(prestataire!=null){
			criteria.add(Restrictions.eq("prestataire",prestataire));
			}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}




@Override
public List<SygContratsPrestataires> find(int indice, int pas) {
   Criteria criteria = getHibernateSession().createCriteria(SygContratsPrestataires.class);
	criteria.setFirstResult(indice);
	if(pas>0)
	criteria.setMaxResults(pas);
		
	return criteria.list();
	}


}
