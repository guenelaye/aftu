package sn.ssi.kermel.be.audit.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAudit;


@Remote
public interface AuditSession {
	public void save(SygAudit audit);
	public void delete(Long id);
	public List<SygAudit> find(int indice, int pas,Date datestatut,String libelleaudit,String statut,Integer gestion);
	public int count(Date datestatut);
	public void update(SygAudit audit);
	public SygAudit findById(Long code);
	public List<SygAudit> find(String code);
	List<SygAudit> find(int indice, int pas);
	int count();
	
}

