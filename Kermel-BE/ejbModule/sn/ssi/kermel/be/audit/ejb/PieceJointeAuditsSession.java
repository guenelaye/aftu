package sn.ssi.kermel.be.audit.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDecisionDenonciation;
import sn.ssi.kermel.be.entity.SygDenonciation;
import sn.ssi.kermel.be.entity.SygPieceJointeAudits;
import sn.ssi.kermel.be.entity.SygPieceJointeContentieux;
import sn.ssi.kermel.be.entity.SygPieceJointeDenonciation;


@Remote
public interface PieceJointeAuditsSession {
public void save(SygPieceJointeAudits pjointe);
	
	public void delete(Long id);
	
	public List<SygPieceJointeAudits> find(int indice, int pas,Long code,String libelle,SygAudit audit);
	
	public List<SygPieceJointeAudits> findRech(int indice, int pas,Long code,String libelle,String LibelleType);
	
	public int count(Long code,String libelle,SygAudit audit);
	
	public int countRech(Long code,String libelle, String LibelleType);
	
	public void update(SygPieceJointeAudits pjointe);
	
	public SygPieceJointeAudits findById(Long code);
	
	public SygPieceJointeAudits findden(Long code,String recevable,String decisiondenonciation);
	
}

