package sn.ssi.kermel.be.audit.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygContratsPrestataires;
import sn.ssi.kermel.be.entity.SygPrestatairesAudits;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPrestataire;





@Remote
public interface PrestatairesAuditsSession {
	public void save(SygPrestatairesAudits auditsprest);
	public void delete(Long auditsprest);
	public List<SygPrestatairesAudits> find(int indice, int pas,SygAutoriteContractante autorite,SygPrestataire prestataires, SygAudit audits,SygContratsPrestataires contprest);
	public int count(SygAutoriteContractante autorite,SygPrestataire auditsprest, SygAudit audits,SygContratsPrestataires contprest);
	public void update(SygPrestatairesAudits auditsprest);
	public SygPrestatairesAudits findById(Long idpaac);
	List<SygPrestatairesAudits> find(int indice, int pas, SygAudit audits,SygContratsPrestataires contprest);
	

}
