package sn.ssi.kermel.be.audit.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygAuditsPrestataires;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygContratsPrestataires;
import sn.ssi.kermel.be.entity.SygPrestatairesAudits;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;
@Stateless
public class AuditsPrestatairesSessionBean  extends AbstractSessionBean implements AuditsPrestatairesSession{
	
	
	
	@Override
	public int count(SygAutoriteContractante autorite,SygPrestatairesAudits auditsprest, SygAudit audits) {
		Criteria criteria= getHibernateSession().createCriteria(SygAuditsPrestataires .class);
		criteria.createAlias("autorite", "autorite");
		criteria.createAlias("audit", "audit");
		criteria.createAlias("prestataire", "prestataire");
		criteria.createAlias("prestataire.prestataire", "prestataires");
		//criteria.setFetchMode("prestataire", FetchMode.SELECT);
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite",autorite));
		}
		if(auditsprest!=null){
			criteria.add(Restrictions.eq("prestataire",auditsprest));
		}
		if(audits!=null){
			criteria.add(Restrictions.eq("audit",audits));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygAuditsPrestataires.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
//	@Override
@SuppressWarnings("unchecked")
	//	public List<SygAuditsPrestataires > findby(int indice, int pas,SygTypeAutoriteContractante autorites) {
//		// TODO Auto-generated method stub
//		Criteria criteria = getHibernateSession().createCriteria(SygAuditsPrestataires.class);
//		criteria.createAlias("autorites", "autorite");
//		if(autorites!=null){
//			criteria.add(Restrictions.eq("autorite",autorites));
//		}
//		criteria.setFirstResult(indice);
//		if(pas>0)
//			criteria.setMaxResults(pas);
//		
//		return criteria.list();
//	}
	@Override
	public List<SygAutoriteContractante > findby1(int indice, int pas,SygTypeAutoriteContractante type,SygAudit audits) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAutoriteContractante.class);
		criteria.createAlias("type", "type");
		criteria.createAlias("audits", "audits");
		if(type!=null){
			criteria.add(Restrictions.eq("type",type));
		}
		if(audits!=null){
			criteria.add(Restrictions.eq("audits",audits));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<SygAuditsPrestataires > find(int indice, int pas,SygAutoriteContractante autorite,SygPrestatairesAudits prestataire, SygAudit audit) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAuditsPrestataires.class);
		criteria.createAlias("autorite", "autorite");
		criteria.createAlias("audit", "audit");
		//criteria.createAlias("prestataire", "prestataire");
		//criteria.createAlias("prestataire", "prestataire");
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite",autorite));
		}
		if(prestataire!=null){
			criteria.add(Restrictions.eq("prestataire",prestataire));
		}
		if(audit!=null){
			criteria.add(Restrictions.eq("audit",audit));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
//	@Override
//	public List<SygAuditsPrestataires > find(int indice, int pas,SygAutoriteContractante autorite,SygPrestatairesAudits prestataire, SygAudit audit) {
//		// TODO Auto-generated method stub
//		Criteria criteria = getHibernateSession().createCriteria(SygAuditsPrestataires.class);
//		criteria.createAlias("autorite", "autorite");
//		criteria.createAlias("audit", "audit");
//		//criteria.createAlias("prestataire", "prestataire");
//		criteria.createAlias("prestataire.contprest", "prestataire");
//		if(autorite!=null){
//			criteria.add(Restrictions.eq("autorite",autorite));
//		}
//		if(prestataire!=null){
//			criteria.add(Restrictions.eq("prestataire.idcontract",prestataire.getContprest().getIdcontract()));
//		}
//		if(audit!=null){
//			criteria.add(Restrictions.eq("audit",audit));
//		}
//		criteria.setFirstResult(indice);
//		if(pas>0)
//			criteria.setMaxResults(pas);
//		
//		return criteria.list();
//	}
	@Override
	public SygPrestatairesAudits findById2(SygContratsPrestataires contprest,SygAudit audit ){
		Criteria criteria = getHibernateSession().createCriteria(SygPrestatairesAudits.class);
		//criteria.createAlias("autorite", "autorite");
		criteria.createAlias("audit", "audit");
		criteria.createAlias("contprest", "contprest");
		//criteria.createAlias("prestataire.prestataire", "prestataires");
		if(contprest!=null){
			criteria.add(Restrictions.eq("contprest",contprest));
		}
		if(audit!=null){
			criteria.add(Restrictions.eq("audit",audit));
		}
		return (SygPrestatairesAudits) criteria.uniqueResult();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<SygAuditsPrestataires > find2(int indice, int pas,SygPrestatairesAudits prestataire, SygAudit audit) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAuditsPrestataires.class);
		criteria.createAlias("audit", "audit");
		//criteria.createAlias("prestataire", "prestataire");
		criteria.createAlias("autorite", "autorite");
		
		if(prestataire!=null){
			criteria.add(Restrictions.eq("prestataire",prestataire));
		}
		if(audit!=null){
			criteria.add(Restrictions.eq("audit",audit));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	@Override
	public int count(SygPrestatairesAudits prestataire, SygAudit audit) {
		Criteria criteria= getHibernateSession().createCriteria(SygAuditsPrestataires .class);
		//criteria.createAlias("autorite", "autorite");
		criteria.createAlias("audit", "audit");
		criteria.createAlias("prestataire", "prestataire");
		//criteria.createAlias("prestataire.prestataire", "prestataires");
		//criteria.setFetchMode("prestataire", FetchMode.SELECT);
		if(prestataire!=null){
			criteria.add(Restrictions.eq("prestataire",prestataire));
		}
		if(audit!=null){
			criteria.add(Restrictions.eq("audit",audit));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<SygAuditsPrestataires > find3(int indice, int pas,SygPrestatairesAudits prestataire, SygAudit audit,SygTypeAutoriteContractante type) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAuditsPrestataires.class);
		criteria.createAlias("audit", "audit");
		//criteria.createAlias("contprest", "contprest");
		criteria.createAlias("autorite", "autorite");
		//criteria.createAlias("prestataire.contprest", "prestataire");
		//criteria.createAlias("type", "type");
		//criteria.createAlias("prestataire.prestataire", "prestataires");
		
//		if(prestataire!=null){
//			criteria.add(Restrictions.eq("prestataire.idcontract",prestataire.getContprest().getIdcontract()));
//		}
		if(prestataire!=null){
		criteria.add(Restrictions.eq("prestataire",prestataire));
	}
		if(audit!=null){
			criteria.add(Restrictions.eq("audit",audit));
		}
		if(type!=null){
			criteria.add(Restrictions.eq("autorite.type",type));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	@Override
	public int count(SygPrestatairesAudits prestataire, SygAudit audits,SygTypeAutoriteContractante type) {
		Criteria criteria= getHibernateSession().createCriteria(SygPrestatairesAudits .class);
		//criteria.createAlias("type", "type");
		criteria.createAlias("audit", "audit");
		//criteria.createAlias("autorite", "autorite");
//		criteria.createAlias("prestataire.contprest", "prestataire");
		//criteria.createAlias("prestataire.prestataire", "prestataires");
		//criteria.setFetchMode("prestataire", FetchMode.SELECT);
		if(prestataire!=null){
			criteria.add(Restrictions.eq("prestataire.idcontract",prestataire.getContprest().getIdcontract()));
		}
//		if(type!=null){
//			criteria.add(Restrictions.eq("autorite.type",type));
//		}
		if(audits!=null){
			criteria.add(Restrictions.eq("audit",audits));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	@Override
	public void save(SygAuditsPrestataires  auditsprest) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(auditsprest);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}
	public SygAuditsPrestataires save1(SygAuditsPrestataires auditsprest)
	{
	// TODO Auto-generated method stub
			try {
				SygAuditsPrestataires auditsprest1=(SygAuditsPrestataires) getHibernateSession().save(auditsprest);
				getHibernateSession().flush();
				return auditsprest1;
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				return null;
			}	
			
	}
	@Override
	public void update(SygAuditsPrestataires auditsprest) {
		
		try{
			getHibernateSession().merge(auditsprest);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
//	@SuppressWarnings("unchecked")
//	@Override
//	public List<SygAutoriteContractante> find1(int indice, int pas,SygTypeAutoriteContractante type); {
//		// TODO Auto-generated method stub
//		Criteria criteria = getHibernateSession().createCriteria(SygRealisations.class);
//		criteria.createAlias("SygTypeAutoriteContractante", "type");
//		criteria.addOrder(Order.asc("libelle"));
////		if(autorites!=null){
////			criteria.add(Restrictions.ilike("autorites", "%"+autorites+"%"));
////		}
//		if(SygTypeAutoriteContractante!=null){
//			
//			 criteria.add(Restrictions.eq("type.id", SygTypeAutoriteContractante));
//		} 
//		if(type!=null){
//			criteria.setProjection(Projections.projectionList().add(Projections.groupProperty(type).as("libelle"))
//					.add(Projections.sum("id"))
//					.add(Projections.groupProperty(id)));
//		}
//		criteria.setFirstResult(indice);                    
//		if(pas>0)
//			criteria.setMaxResults(pas);
//		return criteria.list();
//	}
	@Override
	public SygAuditsPrestataires findById(Long idpaac) {
		// TODO Auto-generated method stub
		return (SygAuditsPrestataires)getHibernateSession().get(SygAuditsPrestataires.class, idpaac );
	}	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygAutoriteContractante> ListesAutorites(int indice, int pas,Long audit) {
		
		return getHibernateSession().createQuery(
				"SELECT autorites FROM  SygAutoriteContractante autorites where " 
			+ " autorites.id NOT IN (SELECT audits.autorite.id from SygAuditsPrestataires audits where  audits.audit.idaudit='" + audit + "' " +")")
				.setFirstResult(indice).setMaxResults(pas).list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygAutoriteContractante> ListesAutorites(Long audit) {
		
		return getHibernateSession().createQuery(
				"SELECT autorites FROM  SygAutoriteContractante autorites where " 
			+ " autorites.id NOT IN (SELECT audits.autorite.id from SygAuditsPrestataires audits where  audits.audit.idaudit='" + audit + "' " +")")
				.list();
		
	}
	
	//ADD Rama******************************************************************************************************
	@SuppressWarnings("unchecked")
	@Override
	public List<SygAutoriteContractante> findAuditAutorite(int indice, int pas, Long audit) {
		return getHibernateSession().createQuery(
				"SELECT A FROM SygAutoriteContractante A WHERE  " + "A.id NOT IN (SELECT B.autorite.id FROM SygAuditsPrestataires B where B.audit.idaudit='" + audit + "')")
				.setFirstResult(indice).setMaxResults(pas).list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygAutoriteContractante> countAuditAutorite(Long audit) {
		return getHibernateSession().createQuery(
				"SELECT A FROM SygAutoriteContractante A WHERE  " + "A.id NOT IN (SELECT B.autorite.id FROM SygAuditsPrestataires B where B.audit.idaudit='" + audit + "')")
				.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygAutoriteContractante> findAuditAutoriteTypeAc(int indice, int pas, String libelleaut,Long audit,SygTypeAutoriteContractante type) {
		
		String libellesaisi;
		if(libelleaut!=null)
		{
		libellesaisi="A.denomination LIKE '%"+libelleaut+"%' and ";
		}
		else
		{
		libellesaisi="";	
		}
		
		return getHibernateSession().createQuery(
				"SELECT A FROM SygAutoriteContractante A WHERE  " +libellesaisi + " A.id NOT IN (SELECT B.autorite.id FROM SygAuditsPrestataires B where B.audit.idaudit='" + audit + "')and A.type.id='" +type.getId()+"'")
				.setFirstResult(indice).setMaxResults(pas).list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygAutoriteContractante> countAuditAutoriteTypeAc(String libelleaut,Long audit,SygTypeAutoriteContractante type) {
		String libellesaisi;
		if(libelleaut!=null)
		{
		libellesaisi="A.denomination LIKE '%"+libelleaut+"%' and ";
		}
		else
		{
		libellesaisi="";	
		}
		
		
		return getHibernateSession().createQuery(
				"SELECT A FROM SygAutoriteContractante A WHERE  " +libellesaisi + " A.id NOT IN (SELECT B.autorite.id FROM SygAuditsPrestataires B where B.audit.idaudit='" + audit + "')and A.type.id='" +type.getId()+"'")
				.list();
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygAuditsPrestataires > find(int indice, int pas,Long auditid,Long autoriteid,String libelleaut) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAuditsPrestataires.class);
		criteria.createAlias("audit", "audit");
		criteria.createAlias("autorite", "autorite");
		
		
		
		if(auditid!=null){
			criteria.add(Restrictions.eq("audit.id",auditid));
		}
		
		if(autoriteid!=null){
			criteria.add(Restrictions.eq("autorite.id",autoriteid));
		}
		
		if(libelleaut!=null){
			
			criteria.add(Restrictions.ilike("autorite.denomination", "%"+libelleaut+"%"));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public int count(Long auditid,Long autoriteid,String libelleaut) {
		Criteria criteria= getHibernateSession().createCriteria(SygAuditsPrestataires .class);
		
		criteria.createAlias("audit", "audit");
        criteria.createAlias("autorite", "autorite");
        
		if(auditid!=null){
			criteria.add(Restrictions.eq("audit.id",auditid));
		}
		
		if(autoriteid!=null){
			criteria.add(Restrictions.eq("autorite.id",autoriteid));
		}
		
		if(libelleaut!=null){
			criteria.add(Restrictions.ilike("autorite.denomination", "%"+libelleaut+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygAuditsPrestataires > findauditpresaut(int indice, int pas,Long auditid,Long autoriteid,Long contratpresId,Long typeid) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAuditsPrestataires.class);
		criteria.createAlias("audit", "audit");
		criteria.createAlias("autorite", "autorite");
		criteria.createAlias("contprest", "contprest");
		
		
		
		if(auditid!=null){
			criteria.add(Restrictions.eq("audit.id",auditid));
		}
		
		if(autoriteid!=null){
			criteria.add(Restrictions.eq("autorite.id",autoriteid));
		}
		
		if(contratpresId!=null){
			criteria.add(Restrictions.eq("contprest.idcontract",contratpresId));
		}
		
		if(typeid!=null){
			criteria.add(Restrictions.eq("autorite.type.id",typeid));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public int countauditpresaut(Long auditid,Long autoriteid,Long contratpresId,Long typeid) {
		Criteria criteria= getHibernateSession().createCriteria(SygAuditsPrestataires .class);
		
		criteria.createAlias("audit", "audit");
        criteria.createAlias("autorite", "autorite");
        criteria.createAlias("contprest", "contprest");

		if(auditid!=null){
			criteria.add(Restrictions.eq("audit.id",auditid));
		}
		
		if(autoriteid!=null){
			criteria.add(Restrictions.eq("autorite.id",autoriteid));
		}
		
		if(contratpresId!=null){
			criteria.add(Restrictions.eq("contprest.idcontract",contratpresId));
		}
		
		if(typeid!=null){
			criteria.add(Restrictions.eq("autorite.type.id",typeid));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygAuditsPrestataires > findauditprestypeaut(int indice, int pas,Long auditid,Long typeid,SygContratsPrestataires contrat) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAuditsPrestataires.class);
		criteria.createAlias("audit", "audit");
		criteria.createAlias("autorite", "autorite");
		//criteria.createAlias("contprest", "contprest");
		criteria.setFetchMode("contprest",FetchMode.JOIN);
		
		if(auditid!=null){
			criteria.add(Restrictions.eq("audit.id",auditid));
		}
		
		if(typeid!=null){
			criteria.add(Restrictions.eq("autorite.type.id",typeid));
		}
		
		//String contprest=null;
		//if(contrat!=null){
			//criteria.add(Restrictions.eq("contprest", contrat));
		//}else{
			criteria.add(Restrictions.isNull("contprest"));
		//}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public int countauditprestypeaut(Long auditid,Long typeid,SygContratsPrestataires contrat) {
		Criteria criteria= getHibernateSession().createCriteria(SygAuditsPrestataires .class);
		
		criteria.createAlias("audit", "audit");
		criteria.createAlias("autorite", "autorite");
		criteria.createAlias("contprest", "contprest");

		if(auditid!=null){
			criteria.add(Restrictions.eq("audit.id",auditid));
		}
		
		if(auditid!=null){
			criteria.add(Restrictions.eq("audit.id",auditid));
		}
		
		if(typeid!=null){
			criteria.add(Restrictions.eq("autorite.type.id",typeid));
		}
		
		
		if(contrat!=null){
			criteria.add(Restrictions.eq("contprest", contrat));
		}else{
			criteria.add(Restrictions.eq("contprest",null));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygAuditsPrestataires > findauditprestypeaut2(int indice, int pas,Long auditid,Long typeid) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAuditsPrestataires.class);
		criteria.createAlias("audit", "audit");
		criteria.createAlias("autorite", "autorite");
	
		
		if(auditid!=null){
			criteria.add(Restrictions.eq("audit.id",auditid));
		}
		
		if(typeid!=null){
			criteria.add(Restrictions.eq("autorite.type.id",typeid));
		}
		
		
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	//////Fin add Rama*****************************************************************************
//	@SuppressWarnings("unchecked")   
//	@Override
//	public List<SygAutoriteContractante> list(int indice, int pas,Long audit,Long idpaac,Long type) {
//		
//		return getHibernateSession().createQuery(
//				"SELECT autorites FROM  SygAutoriteContractante autorites where autorites.type.id="+type
//			+ " autorites.id NOT IN (SELECT audits.autorite.id from SygAuditsPrestataires audits where  audits.audit.idaudit='" + audit + "'" +
//					" and      audits.prestataires.idpaac ='" + audit + "'"  +")")
//				.setFirstResult(indice).setMaxResults(pas).list();
//	}
//	@SuppressWarnings("unchecked")
//	@Override
//	public Integer list(Long audit,Long idpaac,Long type) {
//		
//		Long val= (Long) getHibernateSession().createQuery(
//				"SELECT autorites FROM  SygAutoriteContractante autorites where autorites.type.id="+type 
//						+ " autorites.id NOT IN (SELECT audits.autorite.id from SygAuditsPrestataires audits where  audits.audit.idaudit='" + audit + "'" +
//								" and      audits.prestataires.idpaac ='" + audit + "'"  +")")
//				.uniqueResult(); 
//		return val.intValue();
		
		
//	}
//	@SuppressWarnings("unchecked")
//	@Override
//	public List<SygAutoriteContractante> list(int indice, int pas,Long audit,Long type,Long contract) {
//		
//		return getHibernateSession().createQuery(
//				"SELECT autorites FROM  SygAutoriteContractante autorites where autorites.type.id="+type 
//			+ " and autorites.id NOT IN ( SELECT audits.autorite.id from SygAuditsPrestataires audits where  caudit.idaudit = '" + audit + "' and audits.prestataire.contprest.idcontract="+contract+" )")
//				.setFirstResult(indice).setMaxResults(pas).list();
//	}
//	
//	@SuppressWarnings("unchecked")
//	@Override
//	public List<SygAutoriteContractante> list(Long audit,Long type,Long contract) {
//		
//		return getHibernateSession().createQuery(
//				"SELECT autorites FROM  SygAutoriteContractante autorites where autorites.type.id="+type 
//				+ " and autorites.id NOT IN ( SELECT audits.autorite.id from SygAuditsPrestataires audits where  caudit.idaudit = '" + audit + "' and audits.prestataire.contprest.idcontract="+contract+" )")
//				.list();
//		
//	}
//
	@SuppressWarnings("unchecked")
	@Override
	public List<SygAutoriteContractante> list(int indice, int pas,Long audit,Long type) {
		
		return getHibernateSession().createQuery(
				"SELECT autorites FROM  SygAutoriteContractante autorites where autorites.type.id = '"+type 
			+ "' and autorites.id NOT IN ( SELECT audits.autorite.id from SygAuditsPrestataires audits where  audits.audit.idaudit = '" + audit + "')" ).list();
			//	.setFirstResult(indice).setMaxResults(pas).list();
	}
	
	@Override
	public Integer list(Long audit,Long type) {
		
		Long val= (Long) getHibernateSession().createQuery(
				"SELECT count (autorites.id) FROM  SygAutoriteContractante autorites where autorites.type.id= '"+type 
				+ "' and autorites.id NOT IN ( SELECT audits.autorite.id from SygAuditsPrestataires audits where  audits.audit.idaudit = '" + audit + "')")
				.uniqueResult(); 
		return val.intValue();
		
		
	}
	@Override
	public List<SygAutoriteContractante> findby1(int indice, int pas,
			SygTypeAutoriteContractante autorites) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public SygAuditsPrestataires findAll() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAuditsPrestataires.class);
		return (SygAuditsPrestataires) criteria.list();
	}
		
	
}