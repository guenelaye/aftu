package sn.ssi.kermel.be.audit.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygCourrierAudits;
import sn.ssi.kermel.be.entity.SygPieceJointeAudits;
import sn.ssi.kermel.be.entity.SygPrestatairesAudits;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class PierceJointeAuditSessionBean extends AbstractSessionBean implements PieceJointeAuditSession{

	@Override
	public int count(Date date) {
		Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeAudits.class);
		 criteria.createAlias("state", "state");
		if(date!=null){
			criteria.add(Restrictions.ge("date", date));
		}	
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeAudits.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long courrierId) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygPieceJointeAudits.class, courrierId));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	

	@Override
	public SygPieceJointeAudits save(SygPieceJointeAudits  PieceJointeAudits) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(PieceJointeAudits);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return PieceJointeAudits;	}


	@Override
	public void update(SygPieceJointeAudits PieceJointeAudits) {
		
		try{
			getHibernateSession().merge(PieceJointeAudits);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygPieceJointeAudits findById(Long code) {
		return (SygPieceJointeAudits)getHibernateSession().get(SygPieceJointeAudits.class, code);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygPieceJointeAudits> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeAudits.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));
		
		return criteria.list();
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<SygPieceJointeAudits> find(int indice, int pas,String libelle,String fichier,Date date,String reference) {
       Criteria criteria = getHibernateSession().createCriteria(SygCourrierAudits.class);
       criteria.createAlias("state", "state");
		criteria.addOrder(Order.desc("datestatut"));
		
		if(libelle!=null){
		criteria.add(Restrictions.ge("libelle",libelle));
		}
		if(fichier!=null){
			criteria.add(Restrictions.eq("fichier",fichier));
			}
		if(date!=null){
			criteria.add(Restrictions.eq("date",date));
			}
			
		if(reference!=null){
		criteria.add(Restrictions.eq("reference", reference));
		}
//		if(courrierReference!=null){
//			criteria.add(Restrictions.eq("courrierReference", courrierReference));
//			}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}




@SuppressWarnings("unchecked")
@Override
public List<SygPieceJointeAudits> find(int indice, int pas) {
   Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeAudits.class);
	criteria.setFirstResult(indice);
	if(pas>0)
	criteria.setMaxResults(pas);
		
	return criteria.list();
	}

//Add Rama  le 16-04-2013
@Override
public SygPieceJointeAudits findAutoriteAuditPres(Long code,String origine) {
	Criteria criteria = getHibernateSession().createCriteria(SygCourrierAudits.class);
	criteria.createAlias("autoriteauditspres", "autoriteauditspres");
	if(code!=null){
		criteria.add(Restrictions.eq("autoriteauditspres.idpaac", code));
	}
	
	if(origine!=null){
		criteria.add(Restrictions.eq("courrierOrigine", origine));
	}
	
	
	if(criteria.list().size()>0)
        return (SygPieceJointeAudits)criteria.list().get(0);
        else
        return null;
	
	
}

}
