package sn.ssi.kermel.be.audit.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygCourrierAudits;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class CourrierAuditSessionBean extends AbstractSessionBean implements CourrierAuditSession{

	@Override
	public int count(SygAudit audit,Date courrierDateSaisie,Date courrierDateReception,Date courrierDate,String courrierReference,String courrierObjet,String courrierOrigine,String courrierCommentaire) {
		Criteria criteria = getHibernateSession().createCriteria(SygCourrierAudits.class);
		// criteria.createAlias("state", "state");
		if(courrierDateSaisie!=null){
			criteria.add(Restrictions.ge("courrierDateSaisie",courrierDateSaisie));
			}
			if(courrierDateReception!=null){
				criteria.add(Restrictions.eq("courrierDateReception",courrierDateReception));
				}
			if(courrierDate!=null){
				criteria.add(Restrictions.eq("courrierDate",courrierDate));
				}
				
			if(courrierReference!=null){
			criteria.add(Restrictions.eq("courrierReference", courrierReference));
			}
			if(courrierReference!=null){
				criteria.add(Restrictions.eq("courrierReference", courrierReference));
				}
			if(audit!=null){
				criteria.add(Restrictions.eq("audit", audit));
				}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygCourrierAudits.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long courrierId) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygCourrierAudits.class, courrierId));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	

	@Override
	public SygCourrierAudits save(SygCourrierAudits courrier)
	{
	// TODO Auto-generated method stub
			try {
				SygCourrierAudits CourrierAudits1=(SygCourrierAudits) getHibernateSession().save(courrier);
				getHibernateSession().flush();
				return CourrierAudits1;
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				return null;
			}	
			
	}

	@Override
	public void update(SygCourrierAudits courrier) {
		
		try{
			getHibernateSession().merge(courrier);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygCourrierAudits findById(Long code) {
		return (SygCourrierAudits)getHibernateSession().get(SygCourrierAudits.class, code);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygCourrierAudits> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCourrierAudits.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));
		
		return criteria.list();
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<SygCourrierAudits> find(int indice, int pas,SygAudit audit,Date courrierDateSaisie,Date courrierDateReception,Date courrierDate,String courrierReference,String courrierObjet,String courrierOrigine,String courrierCommentaire) {
       Criteria criteria = getHibernateSession().createCriteria(SygCourrierAudits.class);
      //criteria.createAlias("state", "state");
		criteria.addOrder(Order.desc("courrierDateSaisie"));
		if(audit!=null){
			criteria.add(Restrictions.eq("audit", audit));
			}
		if(courrierDateSaisie!=null){
		criteria.add(Restrictions.ge("courrierDateSaisie",courrierDateSaisie));
		}
		if(courrierDateReception!=null){
			criteria.add(Restrictions.eq("courrierDateReception",courrierDateReception));
			}
		if(courrierDate!=null){
			criteria.add(Restrictions.eq("courrierDate",courrierDate));
			}
			
		if(courrierReference!=null){
		criteria.add(Restrictions.eq("courrierReference", courrierReference));
		}
		if(courrierReference!=null){
			criteria.add(Restrictions.eq("courrierReference", courrierReference));
			}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}




@SuppressWarnings("unchecked")
@Override
public List<SygCourrierAudits> find(int indice, int pas) {
   Criteria criteria = getHibernateSession().createCriteria(SygCourrierAudits.class);
	criteria.setFirstResult(indice);
	if(pas>0)
	criteria.setMaxResults(pas);
		
	return criteria.list();
	}

//Add Rama  le 16-04-2013
@Override
public SygCourrierAudits findAutoriteAuditPres(Long code,String origine) {
	Criteria criteria = getHibernateSession().createCriteria(SygCourrierAudits.class);
	criteria.createAlias("autoriteauditspres", "autoriteauditspres");
	if(code!=null){
		criteria.add(Restrictions.eq("autoriteauditspres.idpaac", code));
	}
	
	if(origine!=null){
		criteria.add(Restrictions.eq("courrierOrigine", origine));
	}
	
	
	if(criteria.list().size()>0)
        return (SygCourrierAudits)criteria.list().get(0);
        else
        return null;
	
	
}

@Override
public SygCourrierAudits findAuditCourrier(Long code,String origine) {
	Criteria criteria = getHibernateSession().createCriteria(SygCourrierAudits.class);
	criteria.createAlias("audit", "audit");
	if(code!=null){
		criteria.add(Restrictions.eq("audit.idaudit", code));
	}
	
	if(origine!=null){
		criteria.add(Restrictions.eq("courrierOrigine", origine));
	}
	
	
	if(criteria.list().size()>0)
        return (SygCourrierAudits)criteria.list().get(0);
        else
        return null;
	
	
}

}
