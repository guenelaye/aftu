package sn.ssi.kermel.be.audit.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygContratsPrestataires;
import sn.ssi.kermel.be.entity.SygPrestataire;


@Remote
public interface ContractsPrestatairesSession {
	public void save(SygContratsPrestataires contract);
	public void delete(Long idcontract);
	public List<SygContratsPrestataires> find(int indice, int pas,String numcontract,Date datedebutcontract,Date datefincontract,SygAudit audit,SygPrestataire prestataire);
	public int count(Date datedebutcontract,Date datefincontract);
	public void update(SygContratsPrestataires contract);
	public SygContratsPrestataires findById(Long code);
	public List<SygContratsPrestataires> find(String code);
	List<SygContratsPrestataires> find(int indice, int pas);
    int count(String numcontract,Date datedebutcontract,Date datefincontract,SygAudit audit,SygPrestataire prestataire);
	
}

