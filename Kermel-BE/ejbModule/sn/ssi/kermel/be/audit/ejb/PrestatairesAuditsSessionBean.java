package sn.ssi.kermel.be.audit.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygContratsPrestataires;
import sn.ssi.kermel.be.entity.SygPrestatairesAudits;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygPays;
import sn.ssi.kermel.be.entity.SygPrestataire;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;
@Stateless
public class PrestatairesAuditsSessionBean  extends AbstractSessionBean implements PrestatairesAuditsSession{
	
	
	
	@Override
	public int count(SygAutoriteContractante autorite,SygPrestataire auditsprest, SygAudit audits,SygContratsPrestataires contprest) {
		Criteria criteria= getHibernateSession().createCriteria(SygPrestatairesAudits .class);
		criteria.createAlias("audit", "audit");
		criteria.createAlias("prestataire", "prestataire");
		criteria.createAlias("contprest", "contprest");
		criteria.createAlias("prestataire.pays", "pays");
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite",autorite));
		}
		if(auditsprest!=null){
			criteria.add(Restrictions.eq("prestataire",auditsprest));
		}
		if(contprest!=null){
			criteria.add(Restrictions.eq("prestataire",contprest));
		}
		if(audits!=null){
			criteria.add(Restrictions.eq("audit",audits));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygPrestatairesAudits.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygPrestatairesAudits > find(int indice, int pas,SygAutoriteContractante autorite,SygPrestataire prestataire, SygAudit audits,SygContratsPrestataires contprest) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPrestatairesAudits.class);
		criteria.createAlias("audit", "audit");
		criteria.createAlias("prestataire", "prestataire");
		criteria.createAlias("contprest", "contprest");
		criteria.createAlias("prestataire.pays", "pays");
//		if(autorite!=null){
//			criteria.add(Restrictions.eq("autorite",autorite));
//		}
		if(prestataire!=null){
			criteria.add(Restrictions.eq("prestataire",prestataire));
		}
		if(contprest!=null){
			criteria.add(Restrictions.eq("contprest",contprest));
		}
		if(audits!=null){
			criteria.add(Restrictions.eq("audit",audits));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public void save(SygPrestatairesAudits  auditsprest) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(auditsprest);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygPrestatairesAudits auditsprest) {
		
		try{
			getHibernateSession().merge(auditsprest);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public SygPrestatairesAudits findById(Long idpaac) {
		// TODO Auto-generated method stub
		return (SygPrestatairesAudits)getHibernateSession().get(SygPrestatairesAudits.class, idpaac );
	}	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygPrestatairesAudits > find(int indice, int pas, SygAudit audits,SygContratsPrestataires contprest) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPrestatairesAudits.class);
		criteria.createAlias("audit", "audit");
		
		criteria.createAlias("contprest", "contprest");
		
		if(contprest!=null){
			criteria.add(Restrictions.eq("contprest",contprest));
		}
		if(audits!=null){
			criteria.add(Restrictions.eq("audit",audits));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
}