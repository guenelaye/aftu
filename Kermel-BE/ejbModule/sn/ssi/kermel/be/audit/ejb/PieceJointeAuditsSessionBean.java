package sn.ssi.kermel.be.audit.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygPieceJointeAudits;
import sn.ssi.kermel.be.entity.SygPieceJointeContentieux;
import sn.ssi.kermel.be.entity.SygPieceJointeDenonciation;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class PieceJointeAuditsSessionBean extends AbstractSessionBean implements PieceJointeAuditsSession{

	@Override
	public int count(Long code,String libelle,SygAudit audit) {
		Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeAudits.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(audit!=null){
			criteria.add(Restrictions.eq("audit", audit));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygPieceJointeAudits.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygPieceJointeAudits> find(int indice, int pas,Long code,String libelle,SygAudit audit) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeAudits.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(audit!=null){
			criteria.add(Restrictions.eq("audit", audit));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygPieceJointeAudits pjointe) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(pjointe);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygPieceJointeAudits pjointe) {
		
		try{
			getHibernateSession().merge(pjointe);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygPieceJointeAudits findById(Long code) {
		return (SygPieceJointeAudits)getHibernateSession().get(SygPieceJointeAudits.class, code);
	}
	
	@Override
	public List<SygPieceJointeAudits> findRech(int indice, int pas,Long code,String libelle,String LibelleType) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeDenonciation.class);
		
		criteria.createAlias("denonciation", "denonciation");
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		if(LibelleType!=null){
			criteria.add(Restrictions.ilike("denonciation.libelle", "%"+LibelleType+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public int countRech(Long code,String libelle,String LibelleType) {
		Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeAudits.class);
		criteria.createAlias("denonciation", "denonciation");
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		if(LibelleType!=null){
			criteria.add(Restrictions.ilike("denonciation.libelle", "%"+LibelleType+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	@Override
	public SygPieceJointeAudits findden(Long code,String recevable,String decision) {
		Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeAudits.class);
		criteria.createAlias("denonciation", "denonciation");
		if(code!=null){
			criteria.add(Restrictions.eq("denonciation.id", code));
		}
		
		if(recevable!=null){
			criteria.add(Restrictions.eq("recevable", recevable));
		}
		
		if(decision!=null){
			criteria.add(Restrictions.eq("decision", decision));
		}
		if(criteria.list().size()>0)
	        return (SygPieceJointeAudits)criteria.list().get(0);
	        else
	        return null;
		
		
	}
	
	
}
