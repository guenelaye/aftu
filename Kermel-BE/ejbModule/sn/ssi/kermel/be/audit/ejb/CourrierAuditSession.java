package sn.ssi.kermel.be.audit.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygCourrierAudits;




@Remote
public interface CourrierAuditSession {
	public SygCourrierAudits save(SygCourrierAudits courrier);
	public void delete(Long courrierId);
	public List<SygCourrierAudits> find(int indice, int pas,SygAudit audit,Date courrierDateSaisie,Date courrierDateReception,Date courrierDate,String courrierReference,String courrierObjet,String courrierOrigine,String courrierCommentaire);
	public int count(SygAudit audit,Date courrierDateSaisie,Date courrierDateReception,Date courrierDate,String courrierReference,String courrierObjet,String courrierOrigine,String courrierCommentaire);
	public void update(SygCourrierAudits courrier);
	public SygCourrierAudits findById(Long code);
	public List<SygCourrierAudits> find(String code);
	List<SygCourrierAudits> find(int indice, int pas);
	int count();
	SygCourrierAudits findAutoriteAuditPres(Long code, String origine);
	SygCourrierAudits findAuditCourrier(Long code, String origine);
	
}

