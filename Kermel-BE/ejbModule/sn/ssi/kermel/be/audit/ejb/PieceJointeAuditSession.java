package sn.ssi.kermel.be.audit.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCourrierAudits;
import sn.ssi.kermel.be.entity.SygPieceJointeAudits;




@Remote
public interface PieceJointeAuditSession {
	public SygPieceJointeAudits save(SygPieceJointeAudits PieceJointeAudits);
	public void delete(Long courrierId);
	public List<SygPieceJointeAudits> find(int indice, int pas,String libelle,String fichier,Date date,String reference);
	public int count(Date date);
	public void update(SygPieceJointeAudits PieceJointeAudits);
	public SygPieceJointeAudits findById(Long code);
	public List<SygPieceJointeAudits> find(String code);
	List<SygPieceJointeAudits> find(int indice, int pas);
	int count();
	SygPieceJointeAudits findAutoriteAuditPres(Long code, String origine);
	
}

