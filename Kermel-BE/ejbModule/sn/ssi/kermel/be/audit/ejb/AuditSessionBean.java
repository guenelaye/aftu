package sn.ssi.kermel.be.audit.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class AuditSessionBean extends AbstractSessionBean implements AuditSession{

	@Override
	public int count(Date datestatut) {
		Criteria criteria = getHibernateSession().createCriteria(SygAudit.class);
		 criteria.createAlias("state", "state");
		if(datestatut!=null){
			criteria.add(Restrictions.ge("date", datestatut));
		}	
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygAudit.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long idaudit) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygAudit.class, idaudit));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	

	@Override
	public void save(SygAudit audit) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(audit);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygAudit audit) {
		
		try{
			getHibernateSession().merge(audit);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygAudit findById(Long code) {
		return (SygAudit)getHibernateSession().get(SygAudit.class, code);
	}
	
	
	@Override
	public List<SygAudit> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAudit.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));
		
		return criteria.list();
	}
	

	@Override
	public List<SygAudit> find(int indice, int pas, Date datestatut,
			String libelleaudit,String statut,Integer gestion) {
       Criteria criteria = getHibernateSession().createCriteria(SygAudit.class);
       criteria.createAlias("state", "state");
		criteria.addOrder(Order.desc("datestatut"));
		if(datestatut!=null){
		criteria.add(Restrictions.ge("datestatut",datestatut));
		}
		if(libelleaudit!=null){
			criteria.add(Restrictions.eq("libelleaudit",libelleaudit));
			}
		if(statut!=null){
			criteria.add(Restrictions.eq("statut",statut));
			}
			
		if(gestion!=null){
		criteria.add(Restrictions.eq("gestion", gestion));
		}
			
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}




@Override
public List<SygAudit> find(int indice, int pas) {
   Criteria criteria = getHibernateSession().createCriteria(SygAudit.class);
	criteria.setFirstResult(indice);
	if(pas>0)
	criteria.setMaxResults(pas);
		
	return criteria.list();
	}


}
