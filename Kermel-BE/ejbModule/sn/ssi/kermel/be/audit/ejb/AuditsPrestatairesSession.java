package sn.ssi.kermel.be.audit.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAudit;
import sn.ssi.kermel.be.entity.SygAuditsPrestataires;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygContratsPrestataires;
import sn.ssi.kermel.be.entity.SygPrestatairesAudits;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;





@Remote
public interface AuditsPrestatairesSession {
	public void save(SygAuditsPrestataires auditsprest);
	public SygAuditsPrestataires save1(SygAuditsPrestataires auditsprest);
	public void delete(Long auditsprest);
	public List<SygAuditsPrestataires> find(int indice, int pas,SygAutoriteContractante autorite,SygPrestatairesAudits prestataires, SygAudit audits);
	public int count(SygAutoriteContractante autorite,SygPrestatairesAudits auditsprest, SygAudit audits);
	public void update(SygAuditsPrestataires auditsprest);
	public SygAuditsPrestataires findById(Long idpaac);
//	public List<SygAutoriteContractante> find1(int indice, int pas,SygTypeAutoriteContractante autorites);
//	public int count(SygTypeAutoriteContractante autorites);

	public List<SygAutoriteContractante> ListesAutorites(int indice, int pas,Long audit);
	public List<SygAutoriteContractante> ListesAutorites(Long audit);
//	public List<SygAutoriteContractante> list(int indice, int pas,Long audit,Long type,Long idpaac);
//	public Integer list(Long audit,Long type,Long idpaac);
	public List<SygAutoriteContractante> list(int indice, int pas,Long audit,Long type);
	public Integer list(Long audit,Long type);
	public List<SygAutoriteContractante> findby1(int indice, int pas,SygTypeAutoriteContractante type);
	public List<SygAuditsPrestataires> find2(int indice, int pas,SygPrestatairesAudits prestataire, SygAudit audit);
	public int count(SygPrestatairesAudits prestataire, SygAudit audit);
	public List<SygAuditsPrestataires> find3(int indice, int pas,SygPrestatairesAudits prestataire, SygAudit audit,SygTypeAutoriteContractante type);
	public int count(SygPrestatairesAudits prestataire, SygAudit audit,SygTypeAutoriteContractante type);
	List<SygAutoriteContractante> findby1(int indice, int pas,SygTypeAutoriteContractante type, SygAudit audit);
	
	public SygPrestatairesAudits findById2(SygContratsPrestataires contprest,SygAudit audit );
	
	public SygAuditsPrestataires findAll();
	
	//Add by Rama le 12-04-2013
	List<SygAutoriteContractante> findAuditAutorite(int indice, int pas,Long audit);
	List<SygAutoriteContractante> countAuditAutorite(Long audit);
	List<SygAutoriteContractante> findAuditAutoriteTypeAc(int indice, int pas,String libelleaut,Long audit, SygTypeAutoriteContractante type);
	List<SygAutoriteContractante> countAuditAutoriteTypeAc(String libelleaut,Long audit,SygTypeAutoriteContractante type);
	List<SygAuditsPrestataires> find(int indice, int pas, Long auditid,Long autoriteid,String libelleaut);
	int count(Long auditid,Long autoriteid,String libelleaut);
	
	
	List<SygAuditsPrestataires> findauditpresaut(int indice, int pas,Long auditid, Long autoriteid, Long contratpresId,Long typeid);
	int countauditpresaut(Long auditid, Long autoriteid, Long contratpresId,Long typeid);
	
	List<SygAuditsPrestataires> findauditprestypeaut(int indice, int pas,Long auditid, Long typeid, SygContratsPrestataires contrat);
	int countauditprestypeaut(Long auditid, Long typeid, SygContratsPrestataires contrat);
	List<SygAuditsPrestataires> findauditprestypeaut2(int indice, int pas,Long auditid, Long typeid);
	
	
}
