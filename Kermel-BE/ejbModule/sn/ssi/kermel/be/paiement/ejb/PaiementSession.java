package sn.ssi.kermel.be.paiement.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygPaiement;

@Remote
public interface PaiementSession {
	
	public void save(SygPaiement paiement);
	
	public void delete(Long id);
	
	public List<SygPaiement> find(int indice, int pas,Long code,String libelle);
	
	public List<SygPaiement> findRech(int indice, int pas,Long code,String nom,String prenom);
	
	public int count(Long code,String libelle);
	
	public int countRech(Long code,String nom,String prenom);
	
	public void update(SygPaiement paiement);
	
	public SygPaiement findById(Long code);
	
}
