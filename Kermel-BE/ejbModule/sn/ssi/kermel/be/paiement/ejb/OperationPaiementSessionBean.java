package sn.ssi.kermel.be.paiement.ejb;

import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.entity.SygOperationPaiement;
import sn.ssi.kermel.be.referentiel.ejb.ParametresGenerauxSession;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class OperationPaiementSessionBean extends AbstractSessionBean implements OperationPaiementSession{

	@EJB ParametresGenerauxSession parametresGenerauxSession;
	@Resource
	SessionContext sessionContext;
	
	@Override
	public int count(Long code,String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygOperationPaiement.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygOperationPaiement.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygOperationPaiement> find(int indice, int pas,Long code,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygOperationPaiement.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

//	@Override
//	public void save(SygOperationPaiement operation) {
//		// TODO Auto-generated method stub
//		try {
//			getHibernateSession().save(operation);
//			getHibernateSession().flush();
//			
//		} catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//		}	}
	
	@Override
	public void save(SygOperationPaiement operation) {
		try {
			
			getHibernateSession().save(operation);
			parametresGenerauxSession.incrementeCode(BeConstants.PARAM_NUMRECU);
			getHibernateSession().flush();
		} catch (Exception e) {
			e.printStackTrace();
			sessionContext.setRollbackOnly();
		}
		
		
	}

	@Override
	public void update(SygOperationPaiement operation) {
		
		try{
			getHibernateSession().merge(operation);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygOperationPaiement findById(Long code) {
		return (SygOperationPaiement)getHibernateSession().get(SygOperationPaiement.class, code);
	}
	
	
	@Override
	public String getGeneratedCode(final String codeParametreGeneral) {
		String mois = String.format("%02d", Calendar.getInstance().get(Calendar.MONTH)+1);
		String dateCoupee = Calendar.getInstance().getTime().toString();
		dateCoupee = dateCoupee.substring(dateCoupee.length() - 4, dateCoupee.length());
		Long codeParametre = parametresGenerauxSession.getValeurParametre(codeParametreGeneral);
		if (codeParametre < 10)
			return dateCoupee +mois+ "00" + codeParametre;
		else if (codeParametre >= 10 && codeParametre < 100)
			return dateCoupee +mois+ "0" + codeParametre;
		else
			return dateCoupee +mois+ "" + codeParametre;
	}
}
