package sn.ssi.kermel.be.arrete.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygArretes;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygAvisGeneral;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossiersTypes;
import sn.ssi.kermel.be.entity.SygFichDossierType;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class ArretesSessionBean extends AbstractSessionBean implements ArretesSession{

	
	@Override
	public int count(String arretNum,SygAutoriteContractante autorite) {
		Criteria criteria = getHibernateSession().createCriteria(SygArretes.class);
		//criteria.createAlias("realisation", "realisation");
		//criteria.createAlias("autorite", "autorite");
		if(arretNum!=null){
			criteria.add(Restrictions.eq("arretNum", arretNum));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
	
//		if(Dossiertype_ID!=null){
//			criteria.add(Restrictions.eq("Dossiertype_ID", Dossiertype_ID));
//		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygArretes.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygArretes> find(int indice, int pas,String arretNum,SygAutoriteContractante autorite) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygArretes.class);
		//criteria.createAlias("realisation", "realisation");
		//criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.asc("id"));
		if(arretNum!=null){
			criteria.add(Restrictions.eq("arretNum", arretNum));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
	
//		if(Dossiertype_ID!=null){
//			criteria.add(Restrictions.eq("Dossiertype_ID", Dossiertype_ID));
//		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygArretes Arretes) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(Arretes);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygArretes Arretes) {
		
		try{
			getHibernateSession().merge(Arretes);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygArretes findById(Long code) {
		return (SygArretes)getHibernateSession().get(SygArretes.class, code);
	}
	

	//@Override
//	public SygAvisGeneral getContrat(SygAutoriteContractante autorite){
//		// TODO Auto-generated method stub
//		Criteria criteria = getHibernateSession().createCriteria(SygAvisGeneral.class);
////		criteria.createAlias("realisation", "realisation");
//		criteria.createAlias("autorite", "autorite");
//		if(autorite!=null){
//			criteria.add(Restrictions.eq("autorite", autorite));
//		}
////		if(realisation!=null){
////			criteria.add(Restrictions.eq("realisation", realisation));
////		}
//		if(criteria.list().size()>0)
//		  return (SygAvisGeneral) criteria.list().get(0);
//		else 
//			return null;
//	}


}
