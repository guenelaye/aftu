package sn.ssi.kermel.be.arrete.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygArretes;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygAvisGeneral;
import sn.ssi.kermel.be.entity.SygDossiersTypes;
import sn.ssi.kermel.be.entity.SygFichDossierType;
import sn.ssi.kermel.be.entity.SygRealisations;

@Remote
public interface ArretesSession {
	public void save(SygArretes Arretes);
	public void delete(Long id);
	public List<SygArretes> find(int indice, int pas,String arretNum,SygAutoriteContractante autorite);
	public int count(String arretNum,SygAutoriteContractante autorite);
	public void update(SygArretes Arretes);
	public SygArretes findById(Long code);
	

	
}
