package sn.ssi.kermel.be.fichierdocument.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygFichierDocument;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class FichierDocumentSessionBean extends AbstractSessionBean implements FichierDocumentSession {

	@Override
	public int count(String libelleDoc) {
		Criteria criteria = getHibernateSession().createCriteria(SygFichierDocument.class);
		if(libelleDoc!=null){
			criteria.add(Restrictions.ge("String", libelleDoc));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygFichierDocument.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	@Override
	public void delete(Long IDdoc) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygFichierDocument.class, IDdoc));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	@Override
	public void save(SygFichierDocument fichier) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(fichier);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygFichierDocument fichier) {
	try{
			getHibernateSession().merge(fichier);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public SygFichierDocument findById(Long code) {
		return (SygFichierDocument)getHibernateSession().get(SygFichierDocument.class, code);
	}
	@Override
	public List<SygFichierDocument> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygFichierDocument.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));	
		return criteria.list();
	}
	@Override
	public List<SygFichierDocument> find(int indice, int pas,Long IDdoc,String libelleDoc,String nomFichierDoc,Integer catDoc_ID,String Publier,SygAutoriteContractante autorite) {
       Criteria criteria = getHibernateSession().createCriteria(SygFichierDocument.class);
       criteria.setFetchMode("autorite", FetchMode.SELECT);
		if(libelleDoc!=null){
			criteria.add(Restrictions.ge("String", libelleDoc));
		}
		if(nomFichierDoc!=null){
			criteria.add(Restrictions.ge("String", nomFichierDoc));
		}
		if(Publier!=null){
			criteria.add(Restrictions.ge("String", Publier));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);	
		return criteria.list();
		}
	@Override
	public List<SygFichierDocument> find(int indice, int pas) {
	   Criteria criteria = getHibernateSession().createCriteria(SygFichierDocument.class);
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);			
		return criteria.list();
		}
	}
