package sn.ssi.kermel.be.fichierdocument.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygFichierDocument;



@Remote
public interface FichierDocumentSession {
	public void save(SygFichierDocument fichier);
	public void delete(Long id);
	public List<SygFichierDocument> find(int indice, int pas,Long IDdoc,String libelleDoc,String nomFichierDoc,Integer catDoc_ID,String Publier,SygAutoriteContractante autorite);
	public int count(String libelleDoc);
	public void update(SygFichierDocument fichier);
	public SygFichierDocument findById(Long code);
	public List<SygFichierDocument> find(String code);
	List<SygFichierDocument> find(int indice, int pas);
	int count();
	
}
