package sn.ssi.kermel.be.plansdepassation.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisations;

@Remote
public interface RealisationsSession {
	public SygRealisations save(SygRealisations realisation);
	public void delete(Long id);
	public List<SygRealisations> find(int indice, int pas,String objet,Long typemarche,Long modepassation,SygPlansdepassation plan,int appel, String statut,SygAutoriteContractante autorite, Date datedebut, Date datefin);
	public int count(String objet,Long typemarche,Long modepassation,SygPlansdepassation plan,int appel, String statut,SygAutoriteContractante autorite, Date datedebut, Date datefin);
	public void update(SygRealisations realisation);
	public SygRealisations findById(Long code);
	public String getGeneratedCode(String codeParametreGeneral,String typemarche,String service);
	public List<SygRealisations> find(int indice, int pas,String type,String idtype,SygPlansdepassation plan, Long typemarche);
	public int count(String type,String idtype,SygPlansdepassation plan, Long typemarche);
	public List<SygRealisations> find(String type,Long code,SygPlansdepassation plan, Long typemarche, String objet, Date datedebut, Date datefin, SygModepassation mode);
	public SygRealisations findRealisation(String reference);
	
	public int nombrerealisations(SygPlansdepassation plan, Long typemarche);
	
	public List<SygRealisations> Realisations(int indice, int pas,String objet,Long typemarche,int appel, String statut,SygAutoriteContractante autorite,String mode,Long realisation, int gestion, int supprime);
	public int countRealisations(String objet,Long typemarche,int appel, String statut,SygAutoriteContractante autorite,String mode,Long realisation, int gestion, int supprime);
	public void deletemiseajour(SygRealisations realisation);
	
	public List<SygRealisations> RealisationsMAJ(int indice, int pas,String type,String idtype,SygPlansdepassation plan, Long typemarche,String etat);
	public int countRealisationsMAJ(String type,String idtype,SygPlansdepassation plan, Long typemarche,String etat);
	public List<SygRealisations> findRealisationsMAJ(String type,Long code,SygPlansdepassation plan, Long typemarche, String objet, Date datedebut, Date datefin, SygModepassation mode,String etat);
	public int nombrerealisationsMAJ(SygPlansdepassation plan, Long typemarche,String etat);
	
	public List<SygRealisations> RealisationsMAJ(String type,Long code,SygPlansdepassation plan, Long typemarche, String objet, Date datedebut, Date datefin, SygModepassation mode,String etat);
	public List<SygRealisations> RealisationsMAJ(String type,Long code,SygPlansdepassation plan, Long typemarche, String objet, Date datedebut, Date datefin, SygModepassation mode,String etat,String etatA);

}
