package sn.ssi.kermel.be.plansdepassation.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class RealisationsBailleursSessionBean extends AbstractSessionBean implements RealisationsBailleursSession{

	
	@Override
	public int count(String libelle,SygRealisations realisation,SygBailleurs bailleur) {
		Criteria criteria = getHibernateSession().createCriteria(SygRealisationsBailleurs.class);
		criteria.createAlias("realisations", "realisations");
		criteria.createAlias("bailleurs", "bailleurs");
		if(libelle!=null){
			criteria.add(Restrictions.ilike("bailleurs.libelle","%"+ libelle+"%"));
		}
		if(realisation!=null){
			criteria.add(Restrictions.eq("realisations", realisation));
		}
		if(bailleur!=null){
			criteria.add(Restrictions.eq("bailleurs", bailleur));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygRealisationsBailleurs.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygRealisationsBailleurs> find(int indice, int pas,String libelle,SygRealisations realisation,SygBailleurs bailleur) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygRealisationsBailleurs.class);
		criteria.createAlias("realisations", "realisations");
		criteria.createAlias("bailleurs", "bailleurs");
		criteria.addOrder(Order.asc("bailleurs.libelle"));
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("bailleurs.libelle","%"+ libelle+"%"));
		}
		if(realisation!=null){
			criteria.add(Restrictions.eq("realisations", realisation));
		}
		if(bailleur!=null){
			criteria.add(Restrictions.eq("bailleurs", bailleur));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygRealisationsBailleurs bailleur) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(bailleur);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygRealisationsBailleurs bailleur) {
		
		try{
			getHibernateSession().merge(bailleur);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygRealisationsBailleurs findById(Long code) {
		return (SygRealisationsBailleurs)getHibernateSession().get(SygRealisationsBailleurs.class, code);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<SygBailleurs> ListesBailleurs(int indice, int pas, String libelle,Long demande,Long autorite) {
		String libellesaisi;
		if(libelle!=null)
		{
		libellesaisi="bailleurs.libelle LIKE '%"+libelle+"%' and ";
		}
		else
		{
		libellesaisi="";	
		}
		return getHibernateSession().createQuery(
				"SELECT bailleurs FROM  SygBailleurs bailleurs where " +libellesaisi
			+ " bailleurs.autorite.id='"+autorite+"' and bailleurs.id NOT IN (SELECT realbailleurs.bailleurs.id from SygRealisationsBailleurs realbailleurs where  realbailleurs.realisations.idrealisation='" + demande + "' " +")")
				.setFirstResult(indice).setMaxResults(pas).list();
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygBailleurs> ListesBailleurs(String libelle,Long demande,Long autorite) {
		String libellesaisi;
		if(libelle!=null)
		{
		libellesaisi="bailleurs.libelle LIKE '%"+libelle+"%' and ";
		}
		else
		{
		libellesaisi="";	
		}
		return getHibernateSession().createQuery(
				"SELECT bailleurs FROM  SygBailleurs bailleurs where " +libellesaisi
			+ " bailleurs.autorite.id='"+autorite+"' and bailleurs.id NOT IN (SELECT realbailleurs.bailleurs.id from SygRealisationsBailleurs realbailleurs where  realbailleurs.realisations.idrealisation='" + demande + "' " +")")
				.list();
	}
}
