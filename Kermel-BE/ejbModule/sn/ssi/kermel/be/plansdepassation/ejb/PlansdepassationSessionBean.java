package sn.ssi.kermel.be.plansdepassation.ejb;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygHistoriqueappeloffresAC;
import sn.ssi.kermel.be.entity.SygHistoriqueplan;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.referentiel.ejb.ParametresGenerauxSession;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class PlansdepassationSessionBean extends AbstractSessionBean implements PlansdepassationSession{

	@EJB
	ParametresGenerauxSession ParametresGenerauxSession;
	
	@Override
	public int count(String numero,Date datedebut,Date datefin,SygAutoriteContractante autorite, int version, String etat) {
		Criteria criteria = getHibernateSession().createCriteria(SygPlansdepassation.class);
		//criteria.setFetchMode("clients", FetchMode.SELECT);
		criteria.createAlias("autorite", "autorite");
		if(numero!=null){
			criteria.add(Restrictions.ilike("Numplan", "%"+numero+"%"));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("datecreation", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("datecreation", datefin));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(version>-1){
			criteria.add(Restrictions.eq("version", version));
		}
		if(etat!=null){
			criteria.add(Restrictions.eq("status", etat));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygPlansdepassation.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygPlansdepassation> find(int indice, int pas,String numero,Date datedebut,Date datefin,SygAutoriteContractante autorite, int version, String etat) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlansdepassation.class);
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.asc("annee"));
		if(numero!=null){
			criteria.add(Restrictions.ilike("Numplan", "%"+numero+"%"));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("datecreation", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("datecreation", datefin));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(version>-1){
			criteria.add(Restrictions.eq("version", version));
		}
		if(etat!=null){
			 criteria.add(Restrictions.eq("status", etat));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public SygPlansdepassation save(SygPlansdepassation plan) {
		// TODO Auto-generated method stub
		getHibernateSession().save(plan);
		getHibernateSession().flush();
		 ParametresGenerauxSession.incrementeCode(BeConstants.PARAM_PLANPASSATION);
		return plan;
		}

	@Override
	public void update(SygPlansdepassation plan) {
		
		try{
			getHibernateSession().merge(plan);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygPlansdepassation findById(Long code) {
		return (SygPlansdepassation)getHibernateSession().get(SygPlansdepassation.class, code);
	}
	

	@Override
	public String getGeneratedCode(String codeParametreGeneral,String autorite,String annee) {
		// TODO Auto-generated method stub
	
		
		String dateCoupee = (Calendar.getInstance().getTime()).toString();
		String mois = String.format("%02d", Calendar.getInstance().get(Calendar.MONTH)+1);
		dateCoupee = dateCoupee.substring(dateCoupee.length() - 4, dateCoupee
				.length());
		Long codeParametre = (long) ParametresGenerauxSession.getValeurParametre(codeParametreGeneral);
		if (codeParametre < 10)
			return "P"+"_"+autorite+"_"+annee +"_"+ "000" + codeParametre;
		else if ((codeParametre >= 10) && (codeParametre < 100))
			return "P"+"_"+autorite+"_"+annee+"_" + "00" + codeParametre;
		else if ((codeParametre >= 100) && (codeParametre < 1000))
			return "P"+"_"+autorite+"_"+annee+"_" + "0" + codeParametre;
		else
			return "P"+"_"+autorite+"_"+annee+"_" + "" + codeParametre;
	}
	
	
	@Override
	public SygPlansdepassation Plan(int annee,SygAutoriteContractante autorite,String numero) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlansdepassation.class);
		criteria.createAlias("autorite", "autorite");
		if(annee>-1){
			criteria.add(Restrictions.eq("annee",annee));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(numero!=null){
			criteria.add(Restrictions.eq("numplan", numero));
		}
		if(criteria.list().size()>0)
		   return (SygPlansdepassation) criteria.list().get(0);
		else
			return null;
		
	}
	
	@Override
	public List<SygPlansdepassation> ListesPlans(int indice, int pas,String numero,Date datedebut,Date datefin,SygAutoriteContractante autorite, SygPlansdepassation plan) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlansdepassation.class);
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.desc("version"));
		if(numero!=null){
			criteria.add(Restrictions.ilike("Numplan", "%"+numero+"%"));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("datecreation", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("datecreation", datefin));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(plan!=null){
			criteria.add(Restrictions.eq("plan", plan));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public int countListesPlans(String numero,Date datedebut,Date datefin,SygAutoriteContractante autorite,SygPlansdepassation plan) {
		Criteria criteria = getHibernateSession().createCriteria(SygPlansdepassation.class);
		//criteria.setFetchMode("clients", FetchMode.SELECT);
		criteria.createAlias("autorite", "autorite");
		if(numero!=null){
			criteria.add(Restrictions.ilike("Numplan", "%"+numero+"%"));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("datecreation", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("datecreation", datefin));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(plan!=null){
			criteria.add(Restrictions.eq("plan", plan));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
	
	@Override
	public List<SygPlansdepassation> find(int indice, int pas,String numero,Date datedebut,Date datefin,SygAutoriteContractante autorite, int version) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlansdepassation.class);
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.asc("annee"));
		if(numero!=null){
			criteria.add(Restrictions.ilike("Numplan", "%"+numero+"%"));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("datecreation", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("datecreation", datefin));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(version>-1){
			criteria.add(Restrictions.eq("version", version));
		}
		criteria.add(Restrictions.ne("status", BeConstants.PLANPUB));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public int count(String numero,Date datedebut,Date datefin,SygAutoriteContractante autorite, int version) {
		Criteria criteria = getHibernateSession().createCriteria(SygPlansdepassation.class);
		criteria.createAlias("autorite", "autorite");
		if(numero!=null){
			criteria.add(Restrictions.ilike("Numplan", "%"+numero+"%"));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("datecreation", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("datecreation", datefin));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(version>-1){
			criteria.add(Restrictions.eq("version", version));
		}
		criteria.add(Restrictions.ne("status", BeConstants.PLANPUB));
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
	
	@Override
	public List<SygPlansdepassation> Plans(int indice, int pas,String numero,Date datedebut,Date datefin,SygAutoriteContractante autorite, int version,int annee) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlansdepassation.class);
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.asc("annee"));
		if(numero!=null){
			criteria.add(Restrictions.ilike("Numplan", "%"+numero+"%"));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("datecreation", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("datecreation", datefin));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(version>-1){
			criteria.add(Restrictions.eq("version", version));
		}
		if(annee>-1){
			criteria.add(Restrictions.eq("annee", annee));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	 public int countPlans(String numero,Date datedebut,Date datefin,SygAutoriteContractante autorite, int version,int annee) {
		Criteria criteria = getHibernateSession().createCriteria(SygPlansdepassation.class);
		criteria.createAlias("autorite", "autorite");
		if(numero!=null){
			criteria.add(Restrictions.ilike("Numplan", "%"+numero+"%"));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("datecreation", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("datecreation", datefin));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(version>-1){
			criteria.add(Restrictions.eq("version", version));
		}
		if(annee>-1){
			criteria.add(Restrictions.eq("annee", annee));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
	
	@Override
	public List<SygPlansdepassation> Plans(int indice, int pas,String autorite, int version,int annee) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlansdepassation.class);
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.asc("annee"));
		if(autorite!=null){
			criteria.add(Restrictions.ilike("autorite.denomination", "%"+autorite+"%"));
		}
		if(version>-1){
			criteria.add(Restrictions.eq("version", version));
		}
		if(annee>-1){
			criteria.add(Restrictions.eq("annee", annee));
		}
		criteria.add(Restrictions.or(Restrictions.eq("status", BeConstants.PARAM_PLANMEV), Restrictions.eq("status", BeConstants.PARAM_PLANVAL)));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	

	@Override
	 public int countPlans(String autorite, int version,int annee) {
		Criteria criteria = getHibernateSession().createCriteria(SygPlansdepassation.class);
		criteria.createAlias("autorite", "autorite");
		if(autorite!=null){
			criteria.add(Restrictions.ilike("autorite.denomination", "%"+autorite+"%"));
		}
		if(version>-1){
			criteria.add(Restrictions.eq("version", version));
		}
		if(annee>-1){
			criteria.add(Restrictions.eq("annee", annee));
		}
		criteria.add(Restrictions.or(Restrictions.eq("status", BeConstants.PARAM_PLANMEV), Restrictions.eq("status", BeConstants.PARAM_PLANVAL)));
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
	@Override
	public List<SygPlansdepassation> PlansMAJ(int indice, int pas,String autorite, int version,int annee) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlansdepassation.class);
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.asc("annee"));
		if(autorite!=null){
			criteria.add(Restrictions.ilike("autorite.denomination", "%"+autorite+"%"));
		}
		if(version>-1){
			criteria.add(Restrictions.ne("version", version));
		}
		if(annee>-1){
			criteria.add(Restrictions.eq("annee", annee));
		}
		criteria.add(Restrictions.or(Restrictions.eq("status", BeConstants.PARAM_PLANMEV), Restrictions.eq("status", BeConstants.PARAM_PLANVAL)));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	

	@Override
	 public int countPlansMAJ(String autorite, int version,int annee) {
		Criteria criteria = getHibernateSession().createCriteria(SygPlansdepassation.class);
		criteria.createAlias("autorite", "autorite");
		if(autorite!=null){
			criteria.add(Restrictions.ilike("autorite.denomination", "%"+autorite+"%"));
		}
		if(version>-1){
			criteria.add(Restrictions.ne("version", version));
		}
		if(annee>-1){
			criteria.add(Restrictions.eq("annee", annee));
		}
		criteria.add(Restrictions.or(Restrictions.eq("status", BeConstants.PARAM_PLANMEV), Restrictions.eq("status", BeConstants.PARAM_PLANVAL)));
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
	@Override
	public List<SygPlansdepassation> Plans(int indice, int pas,String numero,Date datedebut,Date datefin,SygAutoriteContractante autorite, int version,int annee, String nomautorite, SygTypeAutoriteContractante type) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlansdepassation.class);
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.desc("datecreation"));
		if(numero!=null){
			criteria.add(Restrictions.ilike("numplan", "%"+numero+"%"));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("datecreation", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("datecreation", datefin));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(version>-1){
			criteria.add(Restrictions.eq("version", version));
		}
		if(annee>-1){
			criteria.add(Restrictions.eq("annee", annee));
		}
		if(nomautorite!=null){
			criteria.add(Restrictions.ilike("autorite.denomination", "%"+nomautorite+"%"));
		}
		if(type!=null){
			criteria.add(Restrictions.eq("autorite.type", type));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public List<SygPlansdepassation> PlansMAJ_AValides(SygAutoriteContractante autorite, int version,int annee) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlansdepassation.class);
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.asc("annee"));
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(version>-1){
			criteria.add(Restrictions.ne("version", version));
		}
		if(annee>-1){
			criteria.add(Restrictions.eq("annee", annee));
		}
		
		return criteria.list();
	}
	
	////////////:Historiques////////
	@Override
	public void saveHistorique(SygHistoriqueplan historique) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(historique);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void updateHistorique(SygHistoriqueplan historique) {
		
		try{
			getHibernateSession().merge(historique);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public int countHistorique(SygPlansdepassation plan) {
		Criteria criteria = getHibernateSession().createCriteria(SygHistoriqueplan.class);
		criteria.createAlias("user", "user");
		if(plan!=null){
			criteria.add(Restrictions.eq("plan", plan));
		}
	
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
	@Override
	public List<SygHistoriqueplan> Historiques(int indice, int pas,SygPlansdepassation plan) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygHistoriqueplan.class);
		criteria.createAlias("user", "user");
		criteria.addOrder(Order.desc("dateMiseEnValidation"));
		if(plan!=null){
			criteria.add(Restrictions.eq("plan", plan));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public SygHistoriqueplan Historique(SygPlansdepassation plan) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygHistoriqueplan.class);
		criteria.addOrder(Order.desc("id"));
	    if(plan!=null){
			criteria.add(Restrictions.eq("plan", plan));
		}
		if(criteria.list().size()>0)
		   return (SygHistoriqueplan) criteria.list().get(0);
		else
			return null;
		
	}
}
