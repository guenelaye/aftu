package sn.ssi.kermel.be.plansdepassation.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygProjetfinancer;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygSigfipbudget;
import sn.ssi.kermel.be.entity.SygSigfipengagements;
import sn.ssi.kermel.be.entity.SygSigfipengagementsannulation;
import sn.ssi.kermel.be.entity.SygSigfipmouvements;
import sn.ssi.kermel.be.entity.SygSigfipmouvementsannulation;

@Remote
public interface SigfipbudgetSession {
	////////////////sigfipbudget///////
	public void savesigfipbudget(SygSigfipbudget budget);
	public void deletesigfipbudget(Long id);
	public List<SygSigfipbudget> findsigfipbudget(int indice, int pas,int exercice,String chapitre);
	public int countsigfipbudget(int exercice,String chapitre);
	public void updatesigfipbudget(SygSigfipbudget budget);
	public SygSigfipbudget findByIdsigfipbudget(Long code);
	
	////////////////sigfipengagements///////
	public void savesigfipengagements(SygSigfipengagements engagement);
	public void deletesigfipengagements(Long id);
	public List<SygSigfipengagements> findsigfipengagements(int indice, int pas,int exercice,String chapitre);
	public int countsigfipengagements(int exercice,String chapitre);
	public void updatesigfipengagements(SygSigfipengagements engagement);
	public SygSigfipengagements findByIdsigfipengagements(Long code);
	
	////////////////sigfipengagementsannulation///////
	public void savesigfipengagementsannulation(SygSigfipengagementsannulation engagement);
	public void deletesigfipengagementsannulation(Long id);
	public List<SygSigfipengagementsannulation> findsigfipengagementsannulation(int indice, int pas,int exercice,String chapitre);
	public int countsigfipengagementsannulation(int exercice,String chapitre);
	public void updatesigfipengagementsannulation(SygSigfipengagementsannulation engagement);
	public SygSigfipengagementsannulation findByIdsigfipengagementsannulation(Long code);
	
	////////////////pmb_sigfipmouvements///////
	public void savesigfipmouvements(SygSigfipmouvements mouvement);
	public void deletesigfipmouvements(Long id);
	public List<SygSigfipmouvements> findsigfipmouvements(int indice, int pas,int exercice,String chapitre);
	public int countsigfipmouvements(int exercice,String chapitre);
	public void updatesigfipmouvements(SygSigfipmouvements mouvement);
	public SygSigfipmouvements findByIdsigfipmouvements(Long code);
	
	////////////////pmb_sigfipmouvementsannulation///////
	public void savesigfipmouvementsannulation(SygSigfipmouvementsannulation mouvement);
	public void deletesigfipmouvementsannulation(Long id);
	public List<SygSigfipmouvementsannulation> findsigfipmouvementsannulation(int indice, int pas,int exercice,String chapitre);
	public int countsigfipmouvementsannulation(int exercice,String chapitre);
	public void updatesigfipmouvementsannulation(SygSigfipmouvementsannulation mouvement);
	public SygSigfipmouvementsannulation findByIdsigfipmouvementsannulation(Long code);
	
    ////////////////pmb_projetfinancer///////
	public void saveprojetfinancer(SygProjetfinancer financement);
	public void deleteprojetfinancer(Long id);
	public List<SygProjetfinancer> findprojetfinancer(int indice, int pas,int exercice,String chapitre,SygRealisations realisation,SygBailleurs bailleur);
	public int countprojetfinancer(int exercice,String chapitre,SygRealisations realisation,SygBailleurs bailleur);
	public void updateprojetfinancer(SygProjetfinancer financement);
	public SygProjetfinancer findByIdprojetfinancer(Long code);
}
