package sn.ssi.kermel.be.plansdepassation.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygDossiersFournisseurs;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class DossiersFournisseursSessionBean extends AbstractSessionBean implements DossiersFournisseursSession{

	
	@Override
	public int count(String libelle,SygAppelsOffres appel,SygFournisseur fournisseur) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossiersFournisseurs.class);
		criteria.createAlias("fournisseur", "fournisseur");
		criteria.createAlias("appel", "appel");
		if(libelle!=null){
			criteria.add(Restrictions.ilike("fournisseur.nom","%"+ libelle+"%"));
		}
		if(appel!=null){
			criteria.add(Restrictions.eq("appel", appel));
		}
		if(fournisseur!=null){
			criteria.add(Restrictions.eq("fournisseur", fournisseur));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygDossiersFournisseurs.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygDossiersFournisseurs> find(int indice, int pas,String libelle,SygAppelsOffres appel,SygFournisseur fournisseur) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiersFournisseurs.class);
		criteria.createAlias("fournisseur", "fournisseur");
		criteria.createAlias("appel", "appel");
		criteria.addOrder(Order.asc("fournisseur.nom"));
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("fournisseur.nom","%"+ libelle+"%"));
		}
		if(appel!=null){
			criteria.add(Restrictions.eq("appel", appel));
		}
		if(fournisseur!=null){
			criteria.add(Restrictions.eq("fournisseur", fournisseur));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygDossiersFournisseurs fournisseur) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(fournisseur);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygDossiersFournisseurs fournisseur) {
		
		try{
			getHibernateSession().merge(fournisseur);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygDossiersFournisseurs findById(Long code) {
		return (SygDossiersFournisseurs)getHibernateSession().get(SygDossiersFournisseurs.class, code);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<SygFournisseur> ListesFournisseurs(int indice, int pas,String libelle,Long appel,Long autorite) {
		String libellesaisi,autorites;
		if(libelle!=null)
		{
		libellesaisi="fournisseur.nom LIKE '%"+libelle+"%' and ";
		}
		else
		{
		libellesaisi="";	
		}
		if(autorite!=null)
		{
			autorites=" fournisseur.autorite.id ="+autorite+" and ";
		}
		else
		{
			autorites="";	
		}
		return getHibernateSession().createQuery(
				"SELECT fournisseur FROM  SygFournisseur fournisseur where " +libellesaisi
			+autorites+"  fournisseur.id NOT IN (SELECT dosfournisseurs.fournisseur.id from SygDossiersFournisseurs dosfournisseurs where  dosfournisseurs.appel.apoid='" + appel + "' " +")")
				.setFirstResult(indice).setMaxResults(pas).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygFournisseur> ListesFournisseurs(String libelle,Long appel,Long autorite) {
		String libellesaisi,autorites;
		if(libelle!=null)
		{
		libellesaisi="fournisseur.nom LIKE '%"+libelle+"%' and ";
		}
		else
		{
		libellesaisi="";	
		}
		if(autorite!=null)
		{
			autorites=" fournisseur.autorite.id ="+autorite+" and ";
		}
		else
		{
			autorites="";	
		}
		return getHibernateSession().createQuery(
				"SELECT fournisseur FROM  SygFournisseur fournisseur where " +libellesaisi
			+autorites+"  fournisseur.id NOT IN (SELECT dosfournisseurs.fournisseur.id from SygDossiersFournisseurs dosfournisseurs where  dosfournisseurs.appel.apoid='" + appel + "' " +")")
				.list();
	}
	
	@Override
	public List<SygDossiersFournisseurs> Invitations(SygAppelsOffres appel) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiersFournisseurs.class);
		criteria.createAlias("fournisseur", "fournisseur");
		criteria.createAlias("appel", "appel");
		criteria.addOrder(Order.desc("dateinvitation"));
		
		if(appel!=null){
			criteria.add(Restrictions.eq("appel", appel));
		}
		
		
		return criteria.list();
	}


}
