package sn.ssi.kermel.be.plansdepassation.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygRealisationsBailleurs;

@Remote
public interface RealisationsBailleursSession {
	public void save(SygRealisationsBailleurs bailleur);
	public void delete(Long id);
	public List<SygRealisationsBailleurs> find(int indice, int pas,String libelle,SygRealisations realisation,SygBailleurs bailleur);
	public int count(String libelle,SygRealisations realisation,SygBailleurs bailleur);
	public void update(SygRealisationsBailleurs bailleur);
	public SygRealisationsBailleurs findById(Long code);
	public List<SygBailleurs> ListesBailleurs(int indice, int pas,String libelle,Long realisation,Long autorite);
	public List<SygBailleurs> ListesBailleurs(String libelle,Long realisation,Long autorite);
}
