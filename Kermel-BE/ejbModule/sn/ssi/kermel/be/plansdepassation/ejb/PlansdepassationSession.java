package sn.ssi.kermel.be.plansdepassation.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygHistoriqueappeloffresAC;
import sn.ssi.kermel.be.entity.SygHistoriqueplan;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;

@Remote
public interface PlansdepassationSession {
	public SygPlansdepassation save(SygPlansdepassation plan);
	public void delete(Long id);
	public List<SygPlansdepassation> find(int indice, int pas,String numero,Date datedebut,Date datefin,SygAutoriteContractante autorite, int version, String etat);
	public int count(String numero,Date datedebut,Date datefin,SygAutoriteContractante autorite, int version, String etat);
	public List<SygPlansdepassation> find(int indice, int pas,String numero,Date datedebut,Date datefin,SygAutoriteContractante autorite, int version);
	public int count(String numero,Date datedebut,Date datefin,SygAutoriteContractante autorite, int version);
	public void update(SygPlansdepassation plan);
	public SygPlansdepassation findById(Long code);
	public String getGeneratedCode(String codeParametreGeneral,String autorite,String annee);
	public SygPlansdepassation Plan(int annee,SygAutoriteContractante autorite,String numero);
	
	public List<SygPlansdepassation> ListesPlans(int indice, int pas,String numero,Date datedebut,Date datefin,SygAutoriteContractante autorite, SygPlansdepassation plan);
	public int countListesPlans(String numero,Date datedebut,Date datefin,SygAutoriteContractante autorite,  SygPlansdepassation plan);
	
	public List<SygPlansdepassation> Plans(int indice, int pas,String numero,Date datedebut,Date datefin,SygAutoriteContractante autorite, int version,int annee);
	public int countPlans(String numero,Date datedebut,Date datefin,SygAutoriteContractante autorite, int version,int annee);
	
	public List<SygPlansdepassation> Plans(int indice, int pas,String autorite, int version,int annee);
	public int countPlans(String autorite, int version,int annee);
	
	public List<SygPlansdepassation> PlansMAJ(int indice, int pas,String autorite, int version,int annee);
	public int countPlansMAJ(String autorite, int version,int annee);
	
	public List<SygPlansdepassation> Plans(int indice, int pas,String numero,Date datedebut,Date datefin,SygAutoriteContractante autorite, int version,int annee, String nomautorite, SygTypeAutoriteContractante type);
	public List<SygPlansdepassation> PlansMAJ_AValides(SygAutoriteContractante autorite, int version,int annee);

	
	//////////::Historiques///////
	public void saveHistorique(SygHistoriqueplan historique);
	public void updateHistorique(SygHistoriqueplan historique);
	public List<SygHistoriqueplan> Historiques(int indice, int pas,SygPlansdepassation plan);
	public int countHistorique(SygPlansdepassation plan);
	public SygHistoriqueplan Historique(SygPlansdepassation plan);
   }
