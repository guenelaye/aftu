package sn.ssi.kermel.be.plansdepassation.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygBailleurs;
import sn.ssi.kermel.be.entity.SygFonctions;
import sn.ssi.kermel.be.entity.SygProjetfinancer;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygSigfipbudget;
import sn.ssi.kermel.be.entity.SygSigfipengagements;
import sn.ssi.kermel.be.entity.SygSigfipengagementsannulation;
import sn.ssi.kermel.be.entity.SygSigfipmouvements;
import sn.ssi.kermel.be.entity.SygSigfipmouvementsannulation;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class SigfipbudgetSessionBean extends AbstractSessionBean implements SigfipbudgetSession{

	
	////////////////sigfipbudget///////
	@Override
	public int countsigfipbudget(int exercice,String chapitre) {
		Criteria criteria = getHibernateSession().createCriteria(SygSigfipbudget.class);
		if(exercice>-1){
			criteria.add(Restrictions.eq("budexercice", exercice));
		}
		if(chapitre!=null){
			criteria.add(Restrictions.eq("budchapitre", chapitre));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void deletesigfipbudget(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygSigfipbudget.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygSigfipbudget> findsigfipbudget(int indice, int pas,int exercice,String chapitre) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygSigfipbudget.class);
		if(exercice>-1){
			criteria.add(Restrictions.eq("budexercice", exercice));
		}
		if(chapitre!=null){
			criteria.add(Restrictions.eq("budchapitre", chapitre));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void savesigfipbudget(SygSigfipbudget budget) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(budget);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void updatesigfipbudget(SygSigfipbudget budget) {
		
		try{
			getHibernateSession().merge(budget);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygSigfipbudget findByIdsigfipbudget(Long code) {
		return (SygSigfipbudget)getHibernateSession().get(SygSigfipbudget.class, code);
	}
	
	////////////////sigfipengagements///////
	@Override
	public int countsigfipengagements(int exercice,String chapitre) {
		Criteria criteria = getHibernateSession().createCriteria(SygSigfipengagements.class);
		if(exercice>-1){
			criteria.add(Restrictions.eq("engexercice", exercice));
		}
		if(chapitre!=null){
			criteria.add(Restrictions.eq("engchapitre", chapitre));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void deletesigfipengagements(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygSigfipengagements.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygSigfipengagements> findsigfipengagements(int indice, int pas,int exercice,String chapitre) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygSigfipengagements.class);
		if(exercice>-1){
			criteria.add(Restrictions.eq("engexercice", exercice));
		}
		if(chapitre!=null){
			criteria.add(Restrictions.eq("engchapitre", chapitre));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void savesigfipengagements(SygSigfipengagements engagement) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(engagement);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void updatesigfipengagements(SygSigfipengagements engagement) {
		
		try{
			getHibernateSession().merge(engagement);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygSigfipengagements findByIdsigfipengagements(Long code) {
		return (SygSigfipengagements)getHibernateSession().get(SygSigfipengagements.class, code);
	}
	
   ////////////////sigfipengagementsannulation///////
	
	@Override
	public int countsigfipengagementsannulation(int exercice,String chapitre) {
		Criteria criteria = getHibernateSession().createCriteria(SygSigfipengagementsannulation.class);
		if(exercice>-1){
			criteria.add(Restrictions.eq("engexercice", exercice));
		}
		if(chapitre!=null){
			criteria.add(Restrictions.eq("engchapitre", chapitre));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void deletesigfipengagementsannulation(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygSigfipengagementsannulation.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygSigfipengagementsannulation> findsigfipengagementsannulation(int indice, int pas,int exercice,String chapitre) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygSigfipengagementsannulation.class);
		if(exercice>-1){
			criteria.add(Restrictions.eq("engexercice", exercice));
		}
		if(chapitre!=null){
			criteria.add(Restrictions.eq("engchapitre", chapitre));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void savesigfipengagementsannulation(SygSigfipengagementsannulation engagement) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(engagement);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void updatesigfipengagementsannulation(SygSigfipengagementsannulation engagement) {
		
		try{
			getHibernateSession().merge(engagement);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygSigfipengagementsannulation findByIdsigfipengagementsannulation(Long code) {
		return (SygSigfipengagementsannulation)getHibernateSession().get(SygSigfipengagementsannulation.class, code);
	}
	
   ////////////////pmb_sigfipmouvements///////
	@Override
	public int countsigfipmouvements(int exercice,String chapitre) {
		Criteria criteria = getHibernateSession().createCriteria(SygSigfipmouvements.class);
		if(exercice>-1){
			criteria.add(Restrictions.eq("mouvexercice", exercice));
		}
		if(chapitre!=null){
			criteria.add(Restrictions.eq("mouvchapitre", chapitre));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void deletesigfipmouvements(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygSigfipmouvements.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygSigfipmouvements> findsigfipmouvements(int indice, int pas,int exercice,String chapitre) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygSigfipmouvements.class);
		if(exercice>-1){
			criteria.add(Restrictions.eq("mouvexercice", exercice));
		}
		if(chapitre!=null){
			criteria.add(Restrictions.eq("mouvchapitre", chapitre));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void savesigfipmouvements(SygSigfipmouvements mouvement) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(mouvement);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void updatesigfipmouvements(SygSigfipmouvements mouvement) {
		
		try{
			getHibernateSession().merge(mouvement);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygSigfipmouvements findByIdsigfipmouvements(Long code) {
		return (SygSigfipmouvements)getHibernateSession().get(SygSigfipmouvements.class, code);
	}
	
	
   ////////////////pmb_igfipmouvementsannulation///////
	@Override
	public int countsigfipmouvementsannulation(int exercice,String chapitre) {
		Criteria criteria = getHibernateSession().createCriteria(SygSigfipmouvementsannulation.class);
		if(exercice>-1){
			criteria.add(Restrictions.eq("mouvexercice", exercice));
		}
		if(chapitre!=null){
			criteria.add(Restrictions.eq("mouvchapitre", chapitre));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void deletesigfipmouvementsannulation(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygSigfipmouvementsannulation.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygSigfipmouvementsannulation> findsigfipmouvementsannulation(int indice, int pas,int exercice,String chapitre) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygSigfipmouvementsannulation.class);
		if(exercice>-1){
			criteria.add(Restrictions.eq("mouvexercice", exercice));
		}
		if(chapitre!=null){
			criteria.add(Restrictions.eq("mouvchapitre", chapitre));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void savesigfipmouvementsannulation(SygSigfipmouvementsannulation mouvement) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(mouvement);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void updatesigfipmouvementsannulation(SygSigfipmouvementsannulation mouvement) {
		
		try{
			getHibernateSession().merge(mouvement);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygSigfipmouvementsannulation findByIdsigfipmouvementsannulation(Long code) {
		return (SygSigfipmouvementsannulation)getHibernateSession().get(SygSigfipmouvementsannulation.class, code);
	}
	
	 ////////////////pmb_projetfinancer///////
	
	@Override
	public int countprojetfinancer(int exercice,String chapitre,SygRealisations realisation,SygBailleurs bailleur) {
		Criteria criteria = getHibernateSession().createCriteria(SygProjetfinancer.class);
		if(exercice>-1){
			criteria.add(Restrictions.eq("exercice", exercice));
		}
		if(chapitre!=null){
			criteria.add(Restrictions.eq("chapitre", chapitre));
		}
		if(realisation!=null){
			criteria.add(Restrictions.eq("realisation", realisation));
		}
		if(bailleur!=null){
			criteria.add(Restrictions.eq("bailleur", bailleur));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void deleteprojetfinancer(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygProjetfinancer.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygProjetfinancer> findprojetfinancer(int indice, int pas,int exercice,String chapitre,SygRealisations realisation,SygBailleurs bailleur) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygProjetfinancer.class);
		if(exercice>-1){
			criteria.add(Restrictions.eq("exercice", exercice));
		}
		if(chapitre!=null){
			criteria.add(Restrictions.eq("chapitre", chapitre));
		}
		if(realisation!=null){
			criteria.add(Restrictions.eq("realisation", realisation));
		}
		if(bailleur!=null){
			criteria.add(Restrictions.eq("bailleur", bailleur));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void saveprojetfinancer(SygProjetfinancer mouvement) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(mouvement);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void updateprojetfinancer(SygProjetfinancer mouvement) {
		
		try{
			getHibernateSession().merge(mouvement);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygProjetfinancer findByIdprojetfinancer(Long code) {
		return (SygProjetfinancer)getHibernateSession().get(SygProjetfinancer.class, code);
	}
}
