package sn.ssi.kermel.be.plansdepassation.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygConfigurationDatesRealisations;
import sn.ssi.kermel.be.entity.SygModepassation;

@Remote
public interface ConfigurationDatesRealisationsSession {
	public void save(SygConfigurationDatesRealisations configuration);
	public List<SygConfigurationDatesRealisations> find(SygModepassation mode,String dncmp,String ccmp,String type);
	public void update(SygConfigurationDatesRealisations configuration);
	public SygConfigurationDatesRealisations findById(Long code);
	
}
