package sn.ssi.kermel.be.plansdepassation.ejb;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygPlansdepassation;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.referentiel.ejb.ParametresGenerauxSession;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class RealisationsSessionBean extends AbstractSessionBean implements RealisationsSession{

	@EJB
	ParametresGenerauxSession ParametresGenerauxSession;
	
	@Override
	public int count(String objet,Long typemarche,Long modepassation,SygPlansdepassation plan,int appel, String statut,SygAutoriteContractante autorite, Date datedebut, Date datefin) {
		Criteria criteria = getHibernateSession().createCriteria(SygRealisations.class);
		criteria.createAlias("plan", "plan");
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("servicemaitreoeuvre", "service");
		criteria.createAlias("typemarche", "type");
		
		if(objet!=null){
			criteria.add(Restrictions.ilike("reference", "%"+objet+"%"));
		}
		if(typemarche!=null){
//			if(typemarche.intValue()==BeConstants.PARAM_TMTRAVAUX.intValue())
//			  criteria.add(Restrictions.or(Restrictions.eq("type.id", BeConstants.PARAM_TMTRAVAUX), Restrictions.eq("type.id", BeConstants.PARAM_TMFOURNITURES)));
//			else
//			 criteria.add(Restrictions.eq("type.id", typemarche));
			
			 criteria.add(Restrictions.eq("type.id", typemarche));
		}
		if(modepassation!=null){
			criteria.add(Restrictions.eq("mode.id", modepassation));
		}
		if(plan!=null){
			criteria.add(Restrictions.eq("plan", plan));
		}
		if(appel>-1){
			criteria.add(Restrictions.eq("appel", appel));
		}
		if(statut!=null){
			criteria.add(Restrictions.eq("plan.status", statut));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("plan.autorite", autorite));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("datelancement", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("datelancement", datefin));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygRealisations.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygRealisations> find(int indice, int pas,String objet,Long typemarche,Long modepassation,SygPlansdepassation plan,int appel, String statut,SygAutoriteContractante autorite, Date datedebut, Date datefin) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygRealisations.class);
		criteria.createAlias("plan", "plan");
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("servicemaitreoeuvre", "service");
		criteria.createAlias("typemarche", "type");
		criteria.addOrder(Order.asc("libelle"));
		if(objet!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+objet+"%"));
		}
		if(typemarche!=null){
//			if(typemarche.intValue()==BeConstants.PARAM_TMTRAVAUX.intValue())
//			  criteria.add(Restrictions.or(Restrictions.eq("type.id", BeConstants.PARAM_TMTRAVAUX), Restrictions.eq("type.id", BeConstants.PARAM_TMFOURNITURES)));
//			else
//			 criteria.add(Restrictions.eq("type.id", typemarche));
			
			 criteria.add(Restrictions.eq("type.id", typemarche));
		}
		if(modepassation!=null){
			criteria.add(Restrictions.eq("mode.id", modepassation));
		}
		if(plan!=null){
			criteria.add(Restrictions.eq("plan", plan));
		}
		if(appel>-1){
			criteria.add(Restrictions.eq("appel", appel));
		}
		if(statut!=null){
			criteria.add(Restrictions.eq("plan.status", statut));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("plan.autorite", autorite));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("datelancement", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("datelancement", datefin));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public SygRealisations save(SygRealisations plan) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(plan);
			getHibernateSession().flush();
			 ParametresGenerauxSession.incrementeCode(BeConstants.PARAM_REALISATION);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return plan;	
	}

	@Override
	public void update(SygRealisations plan) {
		
		try{
			getHibernateSession().merge(plan);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygRealisations findById(Long code) {
		return (SygRealisations)getHibernateSession().get(SygRealisations.class, code);
	}
	

	@Override
	public String getGeneratedCode(String codeParametreGeneral,String typemarche,String service) {
		// TODO Auto-generated method stub
	
		
		String dateCoupee = (Calendar.getInstance().getTime()).toString();
		String mois = String.format("%02d", Calendar.getInstance().get(Calendar.MONTH)+1);
		dateCoupee = dateCoupee.substring(dateCoupee.length() - 4, dateCoupee
				.length());
		Long codeParametre = (long) ParametresGenerauxSession.getValeurParametre(codeParametreGeneral);
		if (codeParametre < 10)
			return typemarche+"_"+service +"_"+ "000" + codeParametre;
		else if ((codeParametre >= 10) && (codeParametre < 100))
			return typemarche+"_"+service+"_" + "00" + codeParametre;
		else if ((codeParametre >= 100) && (codeParametre < 1000))
			return typemarche+"_"+service+"_" + "0" + codeParametre;
		else
			return typemarche+"_"+service+"_" + "" + codeParametre;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygRealisations> find(int indice, int pas,String type,String idtype,SygPlansdepassation plan, Long typemarche) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygRealisations.class);
		criteria.createAlias("plan", "plan");
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("servicemaitreoeuvre", "service");
		criteria.createAlias("typemarche", "type");
		criteria.addOrder(Order.asc("libelle"));
	
		if(plan!=null){
			criteria.add(Restrictions.eq("plan", plan));
		}
		if(typemarche!=null){
//			if(typemarche.intValue()==BeConstants.PARAM_TMTRAVAUX.intValue())
//			  criteria.add(Restrictions.or(Restrictions.eq("type.id", BeConstants.PARAM_TMTRAVAUX), Restrictions.eq("type.id", BeConstants.PARAM_TMFOURNITURES)));
//			else
//			 criteria.add(Restrictions.eq("type.id", typemarche));
			
			 criteria.add(Restrictions.eq("type.id", typemarche));
		} 
		if(type!=null){
			criteria.setProjection(Projections.projectionList().add(Projections.groupProperty(type).as("libelle"))
					.add(Projections.sum("idrealisation"))
					.add(Projections.groupProperty(idtype)));
		}
		criteria.setFirstResult(indice);                    
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public int count(String type,String idtype,SygPlansdepassation plan, Long typemarche) {
		Criteria criteria = getHibernateSession().createCriteria(SygRealisations.class);
		criteria.createAlias("plan", "plan");
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("servicemaitreoeuvre", "service");
		criteria.createAlias("typemarche", "type");
		if(plan!=null){
			criteria.add(Restrictions.eq("plan", plan));
		}
		if(typemarche!=null){
//			if(typemarche.intValue()==BeConstants.PARAM_TMTRAVAUX.intValue())
//			  criteria.add(Restrictions.or(Restrictions.eq("type.id", BeConstants.PARAM_TMTRAVAUX), Restrictions.eq("type.id", BeConstants.PARAM_TMFOURNITURES)));
//			else
//			 criteria.add(Restrictions.eq("type.id", typemarche));
			
			criteria.add(Restrictions.eq("type.id", typemarche));
		} 
		if(type!=null){
			criteria.setProjection(Projections.projectionList().add(Projections.groupProperty(type).as("libelle"))
					.add(Projections.sum("idrealisation"))
					.add(Projections.groupProperty(idtype)));
		}
		criteria.setProjection(Projections.count(type));
		
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
	@Override
	public List<SygRealisations> find(String type,Long code,SygPlansdepassation plan, Long typemarche, String objet, Date datedebut, Date datefin, SygModepassation mode) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygRealisations.class);
		criteria.createAlias("plan", "plan");
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("servicemaitreoeuvre", "service");
		criteria.createAlias("typemarche", "type");
		criteria.addOrder(Order.asc("libelle"));
		if(type!=null){
			criteria.add(Restrictions.eq(type, code));
		}
		if(plan!=null){
			criteria.add(Restrictions.eq("plan", plan));
		}
		if(typemarche!=null){
//			if(typemarche.intValue()==BeConstants.PARAM_TMTRAVAUX.intValue())
//			  criteria.add(Restrictions.or(Restrictions.eq("type.id", BeConstants.PARAM_TMTRAVAUX), Restrictions.eq("type.id", BeConstants.PARAM_TMFOURNITURES)));
//			else
//			 criteria.add(Restrictions.eq("type.id", typemarche));
			
			 criteria.add(Restrictions.eq("type.id", typemarche));
		} 
		if(objet!=null){
			criteria.add(Restrictions.ilike("libelle","%" +objet+"%"));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("datelancement", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("datelancement", datefin));
		}
		if(mode!=null){
			criteria.add(Restrictions.eq("modepassation", mode));
		}
		return criteria.list();
	}

	@Override
	public SygRealisations findRealisation(String reference) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygRealisations.class);
		criteria.createAlias("plan", "plan");
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("servicemaitreoeuvre", "service");
		criteria.createAlias("typemarche", "type");
			criteria.add(Restrictions.eq("reference", reference));
		
		
		return (SygRealisations) criteria.uniqueResult();
	}
	
	@Override
	public int nombrerealisations(SygPlansdepassation plan, Long typemarche) {
		Criteria criteria = getHibernateSession().createCriteria(SygRealisations.class);
		criteria.createAlias("plan", "plan");
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("servicemaitreoeuvre", "service");
		criteria.createAlias("typemarche", "type");
		
		if(plan!=null){
			criteria.add(Restrictions.eq("plan", plan));
		}
		if(typemarche!=null){
//			if(typemarche.intValue()==BeConstants.PARAM_TMTRAVAUX.intValue())
//			  criteria.add(Restrictions.or(Restrictions.eq("type.id", BeConstants.PARAM_TMTRAVAUX), Restrictions.eq("type.id", BeConstants.PARAM_TMFOURNITURES)));
//			else
//			 criteria.add(Restrictions.eq("type.id", typemarche));
			criteria.add(Restrictions.eq("type.id", typemarche));
		}
		criteria.setProjection(Projections.count("type.id"));
		
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	@Override
	public List<SygRealisations> Realisations(int indice, int pas,String objet,Long typemarche,int appel, String statut,SygAutoriteContractante autorite,String mode,Long realisation, int gestion, int supprime) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygRealisations.class);
		criteria.createAlias("plan", "plan");
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("servicemaitreoeuvre", "service");
		criteria.createAlias("typemarche", "type");
		criteria.addOrder(Order.asc("libelle"));
		if(objet!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+objet+"%"));
		}
		if(typemarche!=null){
			 criteria.add(Restrictions.eq("type.id", typemarche));
		}
		if(appel>-1){
			if(mode.equals("NEW"))
			   criteria.add(Restrictions.eq("appel", appel));
			else
			  criteria.add(Restrictions.eq("idrealisation", realisation));
				//criteria.add(Restrictions.or(Restrictions.eq("appel", appel), Restrictions.eq("idrealisation", realisation)));
		}
		if(statut!=null){
			criteria.add(Restrictions.eq("plan.status", statut));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("plan.autorite", autorite));
		}
		if(gestion>-1){
			criteria.add(Restrictions.eq("plan.annee", gestion));
		}
		if(supprime>-1){
			criteria.add(Restrictions.ne("supprime", supprime));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public int countRealisations(String objet,Long typemarche,int appel, String statut,SygAutoriteContractante autorite,String mode,Long realisation, int gestion, int supprime) {
		Criteria criteria = getHibernateSession().createCriteria(SygRealisations.class);
		criteria.createAlias("plan", "plan");
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("servicemaitreoeuvre", "service");
		criteria.createAlias("typemarche", "type");
		
		if(objet!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+objet+"%"));
		}
		if(typemarche!=null){
			 criteria.add(Restrictions.eq("type.id", typemarche));
		}
		if(appel>-1){
			if(mode.equals("NEW"))
			   criteria.add(Restrictions.eq("appel", appel));
			else
				 criteria.add(Restrictions.eq("idrealisation", realisation));
			//	criteria.add(Restrictions.or(Restrictions.eq("appel", appel), Restrictions.eq("idrealisation", realisation)));
		}
		if(statut!=null){
			criteria.add(Restrictions.eq("plan.status", statut));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("plan.autorite", autorite));
		}
		if(gestion>-1){
			criteria.add(Restrictions.eq("plan.annee", gestion));
		}
		if(supprime>-1){
			criteria.add(Restrictions.ne("supprime", supprime));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
	@Override
	public void deletemiseajour(SygRealisations plan) {
		
		try{
			getHibernateSession().merge(plan);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygRealisations> RealisationsMAJ(int indice, int pas,String type,String idtype,SygPlansdepassation plan, Long typemarche,String etat) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygRealisations.class);
		criteria.createAlias("plan", "plan");
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("servicemaitreoeuvre", "service");
		criteria.createAlias("typemarche", "type");
		criteria.addOrder(Order.asc("libelle"));
	
		if(plan!=null){
			criteria.add(Restrictions.eq("plan", plan));
		}
		if(typemarche!=null){
//			if(typemarche.intValue()==BeConstants.PARAM_TMTRAVAUX.intValue())
//			  criteria.add(Restrictions.or(Restrictions.eq("type.id", BeConstants.PARAM_TMTRAVAUX), Restrictions.eq("type.id", BeConstants.PARAM_TMFOURNITURES)));
//			else
//			 criteria.add(Restrictions.eq("type.id", typemarche));
			
			 criteria.add(Restrictions.eq("type.id", typemarche));
		} 
		if(type!=null){
			criteria.setProjection(Projections.projectionList().add(Projections.groupProperty(type).as("libelle"))
					.add(Projections.sum("idrealisation"))
					.add(Projections.groupProperty(idtype)));
		}
		if(etat!=null){
			criteria.add(Restrictions.or(Restrictions.ne("etat", etat), Restrictions.isNull("etat")));
		}
		criteria.setFirstResult(indice);                    
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public int countRealisationsMAJ(String type,String idtype,SygPlansdepassation plan, Long typemarche,String etat) {
		Criteria criteria = getHibernateSession().createCriteria(SygRealisations.class);
		criteria.createAlias("plan", "plan");
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("servicemaitreoeuvre", "service");
		criteria.createAlias("typemarche", "type");
		if(plan!=null){
			criteria.add(Restrictions.eq("plan", plan));
		}
		if(typemarche!=null){
			
			criteria.add(Restrictions.eq("type.id", typemarche));
		} 
		if(type!=null){
			criteria.setProjection(Projections.projectionList().add(Projections.groupProperty(type).as("libelle"))
					.add(Projections.sum("idrealisation"))
					.add(Projections.groupProperty(idtype)));
		}
		if(etat!=null){
			criteria.add(Restrictions.or(Restrictions.ne("etat", etat), Restrictions.isNull("etat")));
		}
		criteria.setProjection(Projections.count(type));
		
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
	@Override
	public List<SygRealisations> findRealisationsMAJ(String type,Long code,SygPlansdepassation plan, Long typemarche, String objet, Date datedebut, Date datefin, SygModepassation mode,String etat) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygRealisations.class);
		criteria.createAlias("plan", "plan");
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("servicemaitreoeuvre", "service");
		criteria.createAlias("typemarche", "type");
		criteria.addOrder(Order.asc("libelle"));
		if(type!=null){
			criteria.add(Restrictions.eq(type, code));
		}
		if(plan!=null){
			criteria.add(Restrictions.eq("plan", plan));
		}
		if(typemarche!=null){
			
			 criteria.add(Restrictions.eq("type.id", typemarche));
		} 
		if(objet!=null){
			criteria.add(Restrictions.ilike("libelle","%" +objet+"%"));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("datelancement", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("datelancement", datefin));
		}
		if(mode!=null){
			criteria.add(Restrictions.eq("modepassation", mode));
		}
		if(etat!=null){
			criteria.add(Restrictions.or(Restrictions.ne("etat", etat), Restrictions.isNull("etat")));
		}
		return criteria.list();
	}
	
	@Override
	public int nombrerealisationsMAJ(SygPlansdepassation plan, Long typemarche,String etat) {
		Criteria criteria = getHibernateSession().createCriteria(SygRealisations.class);
		criteria.createAlias("plan", "plan");
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("servicemaitreoeuvre", "service");
		criteria.createAlias("typemarche", "type");
		
		if(plan!=null){
			criteria.add(Restrictions.eq("plan", plan));
		}
		if(typemarche!=null){
			criteria.add(Restrictions.eq("type.id", typemarche));
		}
		if(etat!=null){
			criteria.add(Restrictions.or(Restrictions.ne("etat", etat), Restrictions.isNull("etat")));
		}
		criteria.setProjection(Projections.count("type.id"));
		
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	@Override
	public List<SygRealisations> RealisationsMAJ(String type,Long code,SygPlansdepassation plan, Long typemarche, String objet, Date datedebut, Date datefin, SygModepassation mode,String etat) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygRealisations.class);
		criteria.createAlias("plan", "plan");
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("servicemaitreoeuvre", "service");
		criteria.createAlias("typemarche", "type");
		criteria.addOrder(Order.asc("libelle"));
		if(type!=null){
			criteria.add(Restrictions.eq(type, code));
		}
		if(plan!=null){
			criteria.add(Restrictions.eq("plan", plan));
		}
		if(typemarche!=null){
//			if(typemarche.intValue()==BeConstants.PARAM_TMTRAVAUX.intValue())
//			  criteria.add(Restrictions.or(Restrictions.eq("type.id", BeConstants.PARAM_TMTRAVAUX), Restrictions.eq("type.id", BeConstants.PARAM_TMFOURNITURES)));
//			else
//			 criteria.add(Restrictions.eq("type.id", typemarche));
			
			 criteria.add(Restrictions.eq("type.id", typemarche));
		} 
		if(objet!=null){
			criteria.add(Restrictions.ilike("libelle","%" +objet+"%"));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("datelancement", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("datelancement", datefin));
		}
		if(mode!=null){
			criteria.add(Restrictions.eq("modepassation", mode));
		}
		if(etat!=null){
			criteria.add(Restrictions.or(Restrictions.ne("etat", etat), Restrictions.isNull("etat")));
		}
		return criteria.list();
	}
	
	@Override
	public List<SygRealisations> RealisationsMAJ(String type,Long code,SygPlansdepassation plan, Long typemarche, String objet, Date datedebut, Date datefin, SygModepassation mode,String etat,String etatA) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygRealisations.class);
		criteria.createAlias("plan", "plan");
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("servicemaitreoeuvre", "service");
		criteria.createAlias("typemarche", "type");
		criteria.addOrder(Order.asc("libelle"));
		if(type!=null){
			criteria.add(Restrictions.eq(type, code));
		}
		if(plan!=null){
			criteria.add(Restrictions.eq("plan", plan));
		}
		if(typemarche!=null){
//			if(typemarche.intValue()==BeConstants.PARAM_TMTRAVAUX.intValue())
//			  criteria.add(Restrictions.or(Restrictions.eq("type.id", BeConstants.PARAM_TMTRAVAUX), Restrictions.eq("type.id", BeConstants.PARAM_TMFOURNITURES)));
//			else
//			 criteria.add(Restrictions.eq("type.id", typemarche));
			
			 criteria.add(Restrictions.eq("type.id", typemarche));
		} 
		if(objet!=null){
			criteria.add(Restrictions.ilike("libelle","%" +objet+"%"));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("datelancement", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("datelancement", datefin));
		}
		if(mode!=null){
			criteria.add(Restrictions.eq("modepassation", mode));
		}
		if(etat!=null&&etatA!=null){
			criteria.add(Restrictions.or(Restrictions.and(Restrictions.ne("etat", etat), Restrictions.ne("etat", etatA)), Restrictions.isNull("etat")));
			//criteria.add(Restrictions.or(Restrictions.ne("etat", etat), Restrictions.isNull("etat")));
		}
		return criteria.list();
	}
}
