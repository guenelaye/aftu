package sn.ssi.kermel.be.plansdepassation.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygDossiersFournisseurs;
import sn.ssi.kermel.be.entity.SygFournisseur;

@Remote
public interface DossiersFournisseursSession {
	public void save(SygDossiersFournisseurs fournisseur);
	public void delete(Long id);
	public List<SygDossiersFournisseurs> find(int indice, int pas,String libelle,SygAppelsOffres appel,SygFournisseur fournisseur);
	public int count(String libelle,SygAppelsOffres appel,SygFournisseur fournisseur);
	public void update(SygDossiersFournisseurs fournisseur);
	public SygDossiersFournisseurs findById(Long code);
	public List<SygFournisseur> ListesFournisseurs(int indice, int pas,String libelle,Long appel,Long autorite);
	public List<SygFournisseur> ListesFournisseurs(String libelle,Long appel,Long autorite);
	
	public List<SygDossiersFournisseurs> Invitations(SygAppelsOffres appel);
}
