package sn.ssi.kermel.be.plansdepassation.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygConfigurationDatesRealisations;
import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class ConfigurationDatesRealisationsSessionBean extends AbstractSessionBean implements ConfigurationDatesRealisationsSession{

	
	
	

	@Override
	public List<SygConfigurationDatesRealisations> find(SygModepassation mode,String dncmp,String ccmp,String type) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygConfigurationDatesRealisations.class);
		criteria.createAlias("mode", "mode");
		criteria.addOrder(Order.asc("mode.libelle"));
		
		if(mode!=null){
			criteria.add(Restrictions.eq("mode",mode));
		}
		if(dncmp!=null){
			criteria.add(Restrictions.eq("examendncmp",dncmp));
		}
		if(ccmp!=null){
			criteria.add(Restrictions.eq("examenccmp", ccmp));
		}
		if(type!=null){
			criteria.add(Restrictions.eq("type", type));
		}
		
		return criteria.list();
	}

	

	@Override
	public void save(SygConfigurationDatesRealisations configuration) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(configuration);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygConfigurationDatesRealisations configuration) {
		
		try{
			getHibernateSession().merge(configuration);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygConfigurationDatesRealisations findById(Long code) {
		return (SygConfigurationDatesRealisations)getHibernateSession().get(SygConfigurationDatesRealisations.class, code);
	}
	


}
