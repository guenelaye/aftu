package sn.ssi.kermel.be.contentieux.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygPieceJointeContentieux;

@Remote
public interface PieceJointeContentieuxSession {
	
	public void save(SygPieceJointeContentieux pjointe);
	
	public void delete(Long id);
	
	public List<SygPieceJointeContentieux> find(int indice, int pas,Long code,String libelle);
	
	public List<SygPieceJointeContentieux> findRech(int indice, int pas,Long code,String libelle,String LibelleType);
	
	public int count(Long code,String libelle);
	
	public int countRech(Long code,String libelle, String LibelleType);
	
	public void update(SygPieceJointeContentieux pjointe);
	
	public SygPieceJointeContentieux findById(Long code);
	
	public SygPieceJointeContentieux findCont(Long code,String recevable,String decision);
	
	
}
