package sn.ssi.kermel.be.contentieux.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygContentieux;

@Remote
public interface ContentieuxSession {
	
	//public void save(SygContentieux contentieux);
	
	public void delete(Long id);
	
	public List<SygContentieux> find(int indice, int pas,Long code,String libelle);
	
	public List<SygContentieux> findRech(int indice, int pas,Long code,String nom,String prenom);
	
	public int count(Long code,String libelle);
	
	public int countRech(Long code,String nom,String prenom);
	
	public void update(SygContentieux contentieux);
	
	public SygContentieux findById(Long code);
	String getGeneratedCode(String codeParametreGeneral);

	SygContentieux save(SygContentieux contentieux);
	
}
