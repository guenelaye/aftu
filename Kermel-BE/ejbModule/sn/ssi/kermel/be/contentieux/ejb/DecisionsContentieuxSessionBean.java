package sn.ssi.kermel.be.contentieux.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygDecisionsContentieux;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class DecisionsContentieuxSessionBean extends AbstractSessionBean implements DecisionsContentieuxSession{

	@Override
	public int count(Long code,String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygDecisionsContentieux.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("decAuteurrecour", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygDecisionsContentieux.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygDecisionsContentieux> find(int indice, int pas,Long code,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDecisionsContentieux.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("decAuteurrecour", "%"+libelle+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygDecisionsContentieux decisioncont) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(decisioncont);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygDecisionsContentieux decisioncont) {
		
		try{
			getHibernateSession().merge(decisioncont);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygDecisionsContentieux findById(Long code) {
		return (SygDecisionsContentieux)getHibernateSession().get(SygDecisionsContentieux.class, code);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygDecisionsContentieux> findRech(int indice, int pas,Long code,String libelle,String Objet) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDecisionsContentieux.class);
		
		criteria.createAlias("contentieux", "contentieux");
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		if(Objet!=null){
			criteria.add(Restrictions.ilike("contentieux.objet", "%"+Objet+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public int countRech(Long code,String libelle,String Objet) {
		Criteria criteria = getHibernateSession().createCriteria(SygDecisionsContentieux.class);
		criteria.createAlias("contentieux", "contentieux");
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		if(Objet!=null){
			criteria.add(Restrictions.ilike("contentieux.objet", "%"+Objet+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	@Override
	public SygDecisionsContentieux findCont(Long code) {
		Criteria criteria = getHibernateSession().createCriteria(SygDecisionsContentieux.class);
		criteria.createAlias("contentieux", "contentieux");
		if(code!=null){
			criteria.add(Restrictions.eq("contentieux.id", code));
		}
		
		if(criteria.list().size()>0)
	        return (SygDecisionsContentieux)criteria.list().get(0);
	        else
	        return null;
		
		
	}
	
	
	
}
