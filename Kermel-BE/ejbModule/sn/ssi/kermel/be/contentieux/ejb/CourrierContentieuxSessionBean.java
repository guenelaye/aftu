package sn.ssi.kermel.be.contentieux.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygCourrierContentieux;
import sn.ssi.kermel.be.entity.SygPieceJointeContentieux;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class CourrierContentieuxSessionBean extends AbstractSessionBean implements CourrierContentieuxSession{

	@Override
	public int count(Long code,String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygCourrierContentieux.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygCourrierContentieux.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygCourrierContentieux> find(int indice, int pas,Long code,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCourrierContentieux.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygCourrierContentieux courrier) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(courrier);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygCourrierContentieux courrier) {
		
		try{
			getHibernateSession().merge(courrier);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygCourrierContentieux findById(Long code) {
		return (SygCourrierContentieux)getHibernateSession().get(SygCourrierContentieux.class, code);
	}
	
	@Override
	public List<SygCourrierContentieux> findRech(int indice, int pas,Long code,String libelle,String LibelleType) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCourrierContentieux.class);
		
		criteria.createAlias("contentieux", "contentieux");
		if(code!=null){
			criteria.add(Restrictions.eq("contentieux.id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		if(LibelleType!=null){
			criteria.add(Restrictions.ilike("contentieux.libelle", "%"+LibelleType+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public int countRech(Long code,String libelle,String LibelleType) {
		Criteria criteria = getHibernateSession().createCriteria(SygCourrierContentieux.class);
		criteria.createAlias("contentieux", "contentieux");
		if(code!=null){
			criteria.add(Restrictions.eq("contentieux.id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		if(LibelleType!=null){
			criteria.add(Restrictions.ilike("contentieux.libelle", "%"+LibelleType+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	@Override
	public SygCourrierContentieux findCont(Long code,String recevable,String decision) {
		Criteria criteria = getHibernateSession().createCriteria(SygCourrierContentieux.class);
		criteria.createAlias("contentieux", "contentieux");
		if(code!=null){
			criteria.add(Restrictions.eq("contentieux.id", code));
		}
		
		if(recevable!=null){
			criteria.add(Restrictions.eq("recevable", recevable));
		}
		
		if(decision!=null){
			criteria.add(Restrictions.eq("decision", decision));
		}
		if(criteria.list().size()>0)
	        return (SygCourrierContentieux)criteria.list().get(0);
	        else
	        return null;
		
		
	}
	
	
}
