package sn.ssi.kermel.be.contentieux.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCourrierContentieux;

@Remote
public interface CourrierContentieuxSession {
	
	public void save(SygCourrierContentieux courrier);
	
	public void delete(Long id);
	
	public List<SygCourrierContentieux> find(int indice, int pas,Long code,String libelle);
	
	public List<SygCourrierContentieux> findRech(int indice, int pas,Long code,String libelle,String LibelleType);
	
	public int count(Long code,String libelle);
	
	public int countRech(Long code,String libelle, String LibelleType);
	
	public void update(SygCourrierContentieux courrier);
	
	public SygCourrierContentieux findById(Long code);
	
	public SygCourrierContentieux findCont(Long code,String recevable,String decision);
	
	
}
