package sn.ssi.kermel.be.contentieux.ejb;

import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.entity.SygContentieux;
import sn.ssi.kermel.be.referentiel.ejb.ParametresGenerauxSession;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class ContentieuxSessionBean extends AbstractSessionBean implements ContentieuxSession{

	@EJB ParametresGenerauxSession parametresGenerauxSession;
	@Resource
	SessionContext sessionContext;
	
	@Override
	public int count(Long code,String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygContentieux.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygContentieux.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygContentieux> find(int indice, int pas,Long code,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygContentieux.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

//	@Override
//	public  void save(SygContentieux contentieux) {
//		try {
//			
//			getHibernateSession().save(contentieux);
//			parametresGenerauxSession.incrementeCode(BeConstants.PARAM_NUMCONT);
//			getHibernateSession().flush();
//		} catch (Exception e) {
//			e.printStackTrace();
//			sessionContext.setRollbackOnly();
//		}
//		
//		
//	}
	
	@Override
	public  SygContentieux save(SygContentieux contentieux) {
		try {
			
			getHibernateSession().save(contentieux);
			parametresGenerauxSession.incrementeCode(BeConstants.PARAM_NUMCONT);
			getHibernateSession().flush();
		} catch (Exception e) {
			e.printStackTrace();
			sessionContext.setRollbackOnly();
		}
		
		return contentieux ;
	}

	@Override
	public void update(SygContentieux type) {
		
		try{
			getHibernateSession().merge(type);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygContentieux findById(Long code) {
		return (SygContentieux)getHibernateSession().get(SygContentieux.class, code);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygContentieux> findRech(int indice, int pas,Long code,String nom,String prenom) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygContentieux.class);
		
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("nom", "%"+nom+"%"));
		}
		
		if(prenom!=null){
			criteria.add(Restrictions.ilike("prenom", "%"+prenom+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public int countRech(Long code,String nom,String prenom) {
		Criteria criteria = getHibernateSession().createCriteria(SygContentieux.class);
		//criteria.createAlias("decision", "decision");
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("nom", "%"+nom+"%"));
		}
		
		if(prenom!=null){
			criteria.add(Restrictions.ilike("prenom", "%"+prenom+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	
	@Override
	public String getGeneratedCode(final String codeParametreGeneral) {
		//String mois = String.format("%02d", Calendar.getInstance().get(Calendar.MONTH)+1);
		String dateCoupee = Calendar.getInstance().getTime().toString();
		dateCoupee = dateCoupee.substring(dateCoupee.length() - 4, dateCoupee.length());
		Long codeParametre = parametresGenerauxSession.getValeurParametre(codeParametreGeneral);
		if (codeParametre < 10)
			return dateCoupee + "00" + codeParametre;
		else if (codeParametre >= 10 && codeParametre < 100)
			return dateCoupee + "0" + codeParametre;
		else
			return dateCoupee + "" + codeParametre;
	}
}
