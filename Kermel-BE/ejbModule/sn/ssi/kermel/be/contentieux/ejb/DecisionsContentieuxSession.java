package sn.ssi.kermel.be.contentieux.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygDecisionsContentieux;

@Remote
public interface DecisionsContentieuxSession {
	
	public void save(SygDecisionsContentieux decisioncont);
	
	public void delete(Long id);
	
	public List<SygDecisionsContentieux> find(int indice, int pas,Long code,String libelle);
	
	public List<SygDecisionsContentieux> findRech(int indice, int pas,Long code,String libelle,String LibelleType);
	
	public int count(Long code,String libelle);
	
	public int countRech(Long code,String libelle, String LibelleType);
	
	public void update(SygDecisionsContentieux decisioncont);
	
	public SygDecisionsContentieux findById(Long code);
	public SygDecisionsContentieux findCont(Long code);
	
}
