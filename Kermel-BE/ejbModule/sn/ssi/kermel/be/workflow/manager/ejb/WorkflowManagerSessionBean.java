package sn.ssi.kermel.be.workflow.manager.ejb;

import java.io.File;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sn.ssi.kermel.be.common.utils.StringOperation;
import sn.ssi.kermel.be.entity.SysModule;
import sn.ssi.kermel.be.workflow.entity.SysArrow;
import sn.ssi.kermel.be.workflow.entity.SysArrowperm;
import sn.ssi.kermel.be.workflow.entity.SysObject;
import sn.ssi.kermel.be.workflow.entity.SysProfilarrowperm;
import sn.ssi.kermel.be.workflow.entity.SysRole;
import sn.ssi.kermel.be.workflow.entity.SysState;
import sn.ssi.kermel.be.workflow.entity.SysStateperm;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

import com.mxgraph.io.mxCodec;
import com.mxgraph.util.mxUtils;
/**
 * Session Bean implementation class WorkflowSessionBean
 */
@Stateless
public class WorkflowManagerSessionBean extends AbstractSessionBean implements WorkflowManagerSession {

	@Override
	public List<SysRole> findRoles(int start, int step) {
		List<SysRole> roles = null;
		Criteria criteria = getHibernateSession().createCriteria(SysRole.class);
		criteria.addOrder(Order.asc("code"));
		criteria.setFirstResult(start);
		if(step>0)
			criteria.setMaxResults(step);
		roles = criteria.list();
		return roles;
	}
	
	@Override
	public List<SysRole> findRoles() {
		List<SysRole> roles = null;
		Criteria criteria = getHibernateSession().createCriteria(SysRole.class);
		criteria.addOrder(Order.asc("code"));
		roles = criteria.list();
		return roles;
	}

	@Override
	public List<SysRole> findRoles(String str) {
		List<SysRole> roles = null;
		Query q = getHibernateSession().createQuery(
				"FROM SysRole rol WHERE rol.desc LIKE :s"
				);
		
		return q.setParameter("s", str + "%").list();
	}

	@Override
	public int countAllRoles() {
		Criteria criteria = getHibernateSession().createCriteria(SysModule.class);
		criteria.setProjection(Projections.count("code"));
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void createRole(SysRole role) {
		getHibernateSession().saveOrUpdate(role);
	}

	@Override
	public SysRole findByCode(String roleCode) {
		// TODO Auto-generated method stub
		return (SysRole) getHibernateSession().get(SysRole.class, roleCode);
	}

	@Override
	public void deleteRole(String roleCode) {
		SysRole role = (SysRole) getHibernateSession().get(SysRole.class, roleCode);
		cleanWorkflow(role);
		getHibernateSession().delete(role);
	}

	@Override
	public void deleteProfilarrowperm(long id) {
		SysProfilarrowperm apm = (SysProfilarrowperm) getHibernateSession().get(SysProfilarrowperm.class, id);
		getHibernateSession().delete(apm);
	}

   	@Override
	public List<SysRole> findObjectRoles(String objCode) {
   		List<SysRole> roles = null;
		Query q = getHibernateSession().createQuery(
				" FROM SysRole rol " + 
				" WHERE rol.sysObject.name LIKE="+objCode
				);
		roles = q.list();
		return roles;
	}

	@Override
	public List<SysRole> findUserObjectRoles(long actId, String objCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SysRole> findUserRoles(long actId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int reloadAllRoleConfig() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int setUserRole(long rolId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int uploadFile(File f) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int loadRoleConfig(String xml) {
		mxCodec codec = new mxCodec();
		return 0;
	}

	@Override
	public List<SysState> findStates() {
		List<SysState> states = null;
		Criteria criteria = getHibernateSession().createCriteria(SysState.class);
		criteria.addOrder(Order.asc("code"));
		states = criteria.list();
		return states;
	}

	@Override
	public List<SysState> findRoleStates(String rolCode) {
		List<SysState> states = null;
		Query q = getHibernateSession().createQuery(
				"SELECT apm.sysArrow.sysState FROM SysArrowperm apm WHERE" +
										" apm.sysRole.code='"+rolCode+"' " +
										" ORDER BY apm.id ASC"
				);
		states = q.list();
		return states;
	}
	
	@Override
	public SysState findInitState() {
		Query q = getHibernateSession().createQuery(
				"FROM SysState WHERE init=1"
				);
		List<SysState> states = q.list();
		if (states.isEmpty())
			return (SysState)states.get(0);
		else
			return null;
	}
	
	@Override
	public SysState findState (String staCode) {
		return (SysState) getHibernateSession().get(SysState.class, staCode);
	}
	
	@Override
	public SysArrow findArrow (String arwCode) {
		return (SysArrow) getHibernateSession().get(SysArrow.class, arwCode);
	}
	
	@Override
	public SysObject findObject (String objCode) {
		return (SysObject) getHibernateSession().get(SysObject.class, objCode);
	}
	
	@Override
	public List<SysArrow> findArrows() {
		List<SysArrow> arrows = null;
		Criteria criteria = getHibernateSession().createCriteria(SysArrow.class);
		criteria.addOrder(Order.asc("code"));
		arrows = criteria.list();
		return arrows;
	}
	
	public void cleanWorkflow(SysRole role) {
		//getHibernateSession().createQuery("DELETE FROM SysProfilarrowperm papm WHERE papm.sysArrowperm.sysRole.code='"+role.getCode()+"'").executeUpdate();
		List<SysProfilarrowperm> l =  getHibernateSession().createQuery("FROM " +
										"SysProfilarrowperm WHERE sysArrowperm.sysRole.code='"+role.getCode()+"'").list();
		for (int i=0;i<l.size();i++)
			deleteProfilarrowperm(l.get(i).getId());
		getHibernateSession().flush();	
		getHibernateSession().createQuery("DELETE FROM SysStateperm WHERE sysRole.code='"+role.getCode()+"'").executeUpdate();
		getHibernateSession().createQuery("DELETE FROM SysArrowperm WHERE sysRole.code='"+role.getCode()+"'").executeUpdate();
		
	}

	
	
	@Override
	public void loadRole(SysRole role, String xml) {
				
		try {
			Document doc = mxUtils.parse(xml);
			NodeList l = doc.getElementsByTagName("role");
			
			if (l.getLength()>0)
			{
				cleanWorkflow(role);
				NodeList arrows = doc.getElementsByTagName("arrow");
				NodeList previewsvgs = doc.getElementsByTagName("svg");
				getHibernateSession().saveOrUpdate(role);
			
				if (previewsvgs.getLength()>0)
				{
					role.setSvg(StringOperation.elementToString((Node) previewsvgs.item(0)));
				}
				
				for (int i =0;i<arrows.getLength();i++)
				{
					Node arrowNode = arrows.item(i);
					SysArrow arrow = findArrow(((Element) arrowNode).getAttribute("name"));
					boolean b= true;
					Node child  = arrowNode.getFirstChild();
					while( child!=null ) {
						if (child.getNodeName().equalsIgnoreCase("fromstate")) 
						{
							b = false;
							SysState st = findState(((Element)child).getAttribute("name"));
							SysArrowperm arrowperm = new SysArrowperm();						
							arrowperm.setSysArrow(arrow);
							arrowperm.setSysState(st);
							arrowperm.setSysRole(role);
							arrowperm.setAlerte(Integer.parseInt(((Element)child).getAttribute("alerte")));
							getHibernateSession().merge(arrowperm);
							getHibernateSession().evict(arrowperm);
						}
						child = child.getNextSibling();
					}
					if (b)
					{
						SysArrowperm arrowperm = new SysArrowperm();						
						arrowperm.setSysArrow(arrow);
						arrowperm.setSysState(findState("INIT_STATE"));
						arrowperm.setSysRole(role);
						getHibernateSession().merge(arrowperm);
						getHibernateSession().evict(arrowperm);
					}
					
				}
			}			
			
			
			
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}


	@Override
	public int loadArrowsAndStates() {
				
		try {
			Document doc = mxUtils.loadDocument(WorkflowManagerSessionBean.class.getResource("/sn/ssi/kermel/be/workflow/manager/ejb/config.xml").toString());
			NodeList l = doc.getElementsByTagName("ssipalette");
			
			for (int k=0;k<l.getLength();k++)
			{
				Node paletteNode = l.item(k);
				Node child  = paletteNode.getFirstChild();
				String objectCode=((Element) paletteNode).getAttribute("object");
				SysObject obj = findObject(objectCode);
				if (obj==null)
				{
					obj = new SysObject();
					obj.setCode(objectCode);
					obj.setDescription(objectCode);
					obj.setLibelle(objectCode);
				}
				while( child!=null ) {
					if (child.getNodeName().equalsIgnoreCase("ssicell")) 
					{
						SysArrow arrow = findArrow(((Element) child).getAttribute("arrow"));
						if (arrow==null)
						{
							arrow = new SysArrow();
							arrow.setCode(((Element) child).getAttribute("arrow"));
						}
					
						arrow.setDesc(((Element) child).getAttribute("title"));
						arrow.setLibelle(((Element) child).getAttribute("title"));
					
						SysState state = findState(((Element) child).getAttribute("tostate"));
						if (state==null)
						{
							state = new SysState();
							state.setCode(((Element) child).getAttribute("tostate"));
						}
						state.setSysObject(obj);
						state.setDesc(((Element) child).getAttribute("statetitle"));
						state.setLibelle(((Element) child).getAttribute("statetitle"));
					
						arrow.setSysState(state);
						getHibernateSession().save(obj);
						getHibernateSession().save(state);
						getHibernateSession().save(arrow);
					
					}
					child = child.getNextSibling();
				}			
			}
			return 1;
			
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
			return 0;
		}		
	}

	
	@Override
	public SysState findState(long staId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SysRole findRole(long roId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveRole(SysRole role, String xml) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean canSomething(String profilCode, String staCode, String thing)
	{
		Query q = getHibernateSession().createQuery(
				"SELECT spm.sysState FROM SysStateperm spm " +
										"WHERE spm.sysProfil.pfCode='"+profilCode+"'"+
										" AND spm."+thing+"=1 AND spm.sysState.code='"+staCode+"'"
				);
		
		if (q.list().isEmpty())
			return false;
		else
			return true;
	}

	@Override
	public void saveStateperm(SysStateperm spm) {	
		getHibernateSession().saveOrUpdate(spm);
	}
	
	@Override
	public void saveArrowperm(SysArrowperm apm) {	
		getHibernateSession().saveOrUpdate(apm);
	}
	
	@Override
	public void saveProfilArrowperm(SysProfilarrowperm papm) {	
		getHibernateSession().saveOrUpdate(papm);
	}
		
	@Override
	public SysStateperm findStateperm(String profilCode, String roleCode, String staCode) {
		Query q = getHibernateSession().createQuery(
				"SELECT spm FROM SysStateperm spm " +
										" WHERE spm.sysRole.code='"+roleCode+"'"+
										" AND spm.sysProfil.pfCode='"+profilCode+"'"+
										" AND spm.sysState.code='"+staCode+"'"
				);
		if (q.list().isEmpty())
			return null;
		else
			return (SysStateperm)q.list().get(0);
	}
	
	@Override
	public List<SysArrowperm> findStateArrows(String roleCode, String staCode) {
		List<SysArrowperm> arrows = null;
		Query q = getHibernateSession().createQuery(
				"SELECT apm FROM SysArrowperm apm " +
										"LEFT JOIN FETCH apm.sysArrow " +
										"WHERE apm.sysRole.code='"+roleCode+
										"' AND apm.sysState.code='"+staCode+
										"'"
				);
		arrows = q.list();
		return arrows;
	}

	@Override
	public SysState findToState(String arwCode) {
		Query q = getHibernateSession().createQuery(
				"SELECT arw.sysState FROM SysArrow arw "+
										"WHERE arw.code='"+arwCode+"'"
				);
		return (SysState)q.uniqueResult();
	}
	
	@Override
	public SysArrowperm findArrowperm(int apmId) {
		Query q = getHibernateSession().createQuery(
				"FROM SysArrowperm apm LEFT JOIN FETCH apm.sysArrow WHERE apm.id="+apmId
				);
		if (q.list().isEmpty())
			return null;
		else
			return (SysArrowperm)q.list().get(0);
	}
	
	
	@Override
	public SysProfilarrowperm findProfilarrowperm(long apm_id, String profilCode) {
		List<SysArrowperm> arrows = null;
		Query q = getHibernateSession().createQuery(
				"SELECT papm FROM SysProfilarrowperm papm " +
										" WHERE papm.sysProfil.pfCode='"+profilCode+
										"'  AND papm.sysArrowperm.id="+apm_id
				);
		if (q.list().isEmpty())
			return null;
		else
			return (SysProfilarrowperm) q.list().get(0) ;
	}
	
	@Override
	public SysProfilarrowperm findProfilarrowperm(long apm_id, String profilCode, String stateCode) {
		List<SysArrowperm> arrows = null;
		Query q = getHibernateSession().createQuery(
				"SELECT papm FROM SysProfilarrowperm papm " +
										" WHERE papm.sysProfil.pfCode='"+profilCode+
										"'  AND papm.sysArrowperm.id="+apm_id+
										"  AND papm.sysArrowperm.sysState.code='"+stateCode+"'"
				);
		if (q.list().isEmpty())
			return null;
		else
			return (SysProfilarrowperm) q.list().get(0) ;
	}
	
	@Override
	public SysObject findObjectByState(String statecode) {

		SysObject mylistacte = null;
		Query q = getHibernateSession().createQuery("SELECT sysObject FROM SysState state WHERE state.code = :param").setParameter("param", statecode);

		if (q.list().size() == 1) {
			return (SysObject) q.uniqueResult();
		} else if (q.list().size() > 0) {
			return (SysObject) q.list().get(0);
		} else
			return new SysObject();

	}

	@Override
	public List<SysArrowperm> findByObject(String code) {

		Criteria criteria = getHibernateSession().createCriteria(SysArrowperm.class);
		criteria.createAlias("sysRole", "role");
		criteria.createAlias("sysState", "state");
		criteria.createAlias("sysState.sysObject", "objet");
		criteria.addOrder(Order.asc("id"));
		if (code != null) {
			
			criteria.add(Restrictions.eq("objet.code", code));
		}
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		return criteria.list();

	}
	
	@Override
	public List<SysArrowperm> findarrowpermBystate(String code) {

		Criteria criteria = getHibernateSession().createCriteria(SysArrowperm.class);
		criteria.createAlias("sysRole", "role");
		criteria.createAlias("sysState", "state");
		
		criteria.addOrder(Order.asc("id"));
		if (code != null) {
			criteria.add(Restrictions.eq("state.code", code));
		}
		
		return criteria.list();

	}
	
	@Override
	public SysStateperm findStateperm(String prof, String inistate) {
		// TODO Auto-generated method stub
		Criteria criteria=getHibernateSession().createCriteria(SysStateperm.class);
		criteria.createAlias("sysProfil", "profil");
		criteria.createAlias("sysState", "state");
		
		criteria.add(Restrictions.like("profil.pfCode", prof));
		criteria.add(Restrictions.like("state.code", inistate));
		
		return (SysStateperm) criteria.uniqueResult();
		
	}
}