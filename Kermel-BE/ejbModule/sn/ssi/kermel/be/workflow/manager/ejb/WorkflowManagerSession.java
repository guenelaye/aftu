package sn.ssi.kermel.be.workflow.manager.ejb;

import java.io.File;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.workflow.entity.SysArrow;
import sn.ssi.kermel.be.workflow.entity.SysArrowperm;
import sn.ssi.kermel.be.workflow.entity.SysObject;
import sn.ssi.kermel.be.workflow.entity.SysProfilarrowperm;
import sn.ssi.kermel.be.workflow.entity.SysRole;
import sn.ssi.kermel.be.workflow.entity.SysState;
import sn.ssi.kermel.be.workflow.entity.SysStateperm;

@Remote
public interface WorkflowManagerSession {

	// Liste des r�les m�tiers
	public SysRole findRole(long roId);

	// Liste des r�les m�tiers d'un user (actId)
	public List<SysRole> findUserRoles(long actId);

	// Liste des r�les m�tiers d'un user (actId) sur un objet (objCode)
	public List<SysRole> findUserObjectRoles(long actId, String objCode);

	// Liste des r�les m�tiers d'un objet (objCode)
	public List<SysRole> findObjectRoles(String objCode);

	public int setUserRole(long rolId);

	public int uploadFile(File f);

	public int loadRoleConfig(String xml);

	public int reloadAllRoleConfig();

	public void deleteRole(String roleCode);

	void saveRole(SysRole role, String xml);

	List<SysRole> findRoles(int start, int step);

	// Liste des r�les m�tiers
	public List<SysRole> findRoles(String str);

	SysState findState(long staId);

	SysState findState(String staCode);

	SysArrow findArrow(String arwCode);

	int countAllRoles();

	void createRole(SysRole role);

	SysRole findByCode(String roleCode);

	void loadRole(SysRole role, String xml);

	List<SysState> findStates();

	List<SysArrow> findArrows();

	List<SysState> findRoleStates(String rolCode);

	boolean canSomething(String profilCode, String staCode, String thing);

	public void saveStateperm(SysStateperm spm);

	SysStateperm findStateperm(String profilCode, String roleCode,
			String staCode);

	List<SysArrowperm> findStateArrows(String roleCode, String staCode);

	SysState findToState(String arwCode);

	SysArrowperm findArrowperm(int spmId);

	void saveArrowperm(SysArrowperm apm);

	void saveProfilArrowperm(SysProfilarrowperm papm);

	SysProfilarrowperm findProfilarrowperm(long apmId, String profilCode);

	SysProfilarrowperm findProfilarrowperm(long apmId, String profilCode,
			String stateCode);

	List<SysRole> findRoles();

	void deleteProfilarrowperm(long id);

	SysState findInitState();

	int loadArrowsAndStates();

	SysObject findObject(String objCode);

	SysObject findObjectByState(String statecode);

	List<SysArrowperm> findByObject(String code);

	List<SysArrowperm> findarrowpermBystate(String code);

	SysStateperm findStateperm(String prof, String inistate);

}