package sn.ssi.kermel.be.workflow.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.workflow.entity.SysArrow;
import sn.ssi.kermel.be.workflow.entity.SysArrowperm;
import sn.ssi.kermel.be.workflow.entity.SysState;

@Remote
public interface WorkflowSession {

	// Les actions possibles (arrows)
	// pour un utilisateur (usrId) sur un objet (objCode) dans un �tat donn�
	// (staId)
	public List<SysArrow> findStateArrows(long usrId, String staCode);

	public List<SysState> findArrowFromStates(long usrId, String arwCode);

	public boolean isAllowFromState(long usrId, String arwCode, String staCode);

	// L'�tat cible d'une action (arwId)
	public SysState findToState(String arwCode);

	// Les �tats d'un objet ou l'utilisateur peut le voir
	public List<SysState> findCanViewStates(String objCode, long usrId);

	// L'utilisateur (actId) peut-il ajouter un objet (objCode) dans un �tat
	// donn� (staId)
	public boolean canAdd(long usrId, String staCode);

	// L'utilisateur (actId) peut-il modifier un objet (objCode) dans un �tat
	// donn� (staId)
	public boolean canEdit(long usrId, String staCode);

	// L'utilisateur (actId) peut-il supprimer un objet (objCode) dans un �tat
	// donn� (staId)
	public boolean canDelete(long usrId, String staCode);

	// L'utilisateur (actId) peut-il voir un objet (objCode) dans un �tat donn�
	// (staId)
	public boolean canView(long usrId, String staCode);

	// L'utilisateur (actId) peut-il imprimer un objet (objCode) dans un �tat
	// donn� (staId)
	public boolean canPrint(long usrId, String staCode);

	List<Utilisateur> findAssignableUsersAfterTransition(String arwCode);

	List<Utilisateur> findAssignableUsersAfterTransition(int start, int step,
			String arwCode);

	int countAssignableUsersAfterTransition(String arwCode);

	List<SysArrowperm> findArrowPermFromStates(String staCode);
	public SysState findState(String state);

}