package sn.ssi.kermel.be.workflow.ejb;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;

import org.hibernate.Query;

import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.workflow.entity.SysArrow;
import sn.ssi.kermel.be.workflow.entity.SysArrowperm;
import sn.ssi.kermel.be.workflow.entity.SysState;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

/**
 * Session Bean implementation class WorkflowSessionBean
 */
@Stateless
public class WorkflowSessionBean extends AbstractSessionBean implements
		WorkflowSession {

	public boolean canSomething(long usrId, String staCode, String thing) {
		Query q = getHibernateSession().createQuery(
				"SELECT spm.sysState FROM SysStateperm spm "
						+ "LEFT JOIN spm.sysProfil.utilisateurs usr "
						+ "WHERE usr.id=" + usrId + " AND spm." + thing
						+ "=1 AND spm.sysState.code=" + staCode);

		if (q.list().isEmpty())
			return false;
		else
			return true;
	}

	@Override
	public boolean canAdd(long usrId, String staCode) {
		return canSomething(usrId, staCode, "canadd");
	}

	@Override
	public boolean canDelete(long usrId, String staCode) {
		return canSomething(usrId, staCode, "candelete");
	}

	@Override
	public boolean canEdit(long usrId, String staCode) {
		return canSomething(usrId, staCode, "canedit");
	}

	@Override
	public boolean canView(long usrId, String staCode) {
		return canSomething(usrId, staCode, "canview");
	}

	public boolean canPrint(long usrId, String staCode) {
		return canSomething(usrId, staCode, "canprint");
	}

	@Override
	public List<SysState> findCanViewStates(String objCode, long usrId) {
		List<SysState> states = null;
		Query q = getHibernateSession().createQuery(
				"SELECT sta FROM SysState sta "
						+ "LEFT JOIN sta.sysStateperms spm "
						+ "LEFT JOIN spm.sysProfil.utilisateurs usr "
						+ "WHERE usr.id=" + usrId
						+ " AND sta.sysObject.code = '" + objCode + "' "
						+ " AND spm.canview=1");
		states = q.list();
		return states;
	}

	@Override
	public List<SysArrow> findStateArrows(long usrId, String staCode) {
		List<SysArrow> arrows = null;
		Query q = getHibernateSession().createQuery(
				"SELECT arw FROM SysArrow arw "
						+ "LEFT JOIN arw.sysArrowperms apm "
						+ "LEFT JOIN apm.sysProfilarrowperms pap "
						+ "LEFT JOIN pap.sysProfil.utilisateurs usr "
						+ "WHERE usr.id=" + usrId + " AND apm.sysState.code='"
						+ staCode + "' AND pap.allow = 1");
		arrows = q.list();
		return arrows;
	}

	@Override
	public List<SysState> findArrowFromStates(long usrId, String arwCode) {
		List<SysState> states = null;
		Query q = getHibernateSession().createQuery(
				"SELECT sta FROM SysState sta "
						+ "LEFT JOIN sta.sysArrowperms apm "
						+ "LEFT JOIN apm.sysProfilarrowperms pap "
						+ "LEFT JOIN pap.sysProfil.utilisateurs usr "
						+ "WHERE usr.id=" + usrId + " AND apm.sysArrow.code='"
						+ arwCode + "' AND pap.allow = 1");
		states = q.list();
		return states;
	}
	
	@Override
	public List<SysArrowperm> findArrowPermFromStates(String staCode) {
		List<SysArrowperm> arrowperm = null;
		Query q = getHibernateSession().createQuery(
				"SELECT apm FROM SysArrowperm apm " +
				"LEFT JOIN  apm.sysState sta " +
				"Where sta.code ='"+staCode+"'"
				);
		arrowperm = q.list();
		return arrowperm;
	}

	@Override
	public SysState findToState(String arwCode) {
		Query q = getHibernateSession().createQuery(
				"SELECT arw.sysState FROM SysArrow arw " + "WHERE arw.code='"
						+ arwCode + "'");
		return (SysState) q.uniqueResult();
	}

	@Override
	public boolean isAllowFromState(long usrId, String arwCode, String staCode) {
		boolean b = false;
		Query q = getHibernateSession().createQuery(
				"SELECT sta FROM SysState sta "
						+ " LEFT JOIN sta.sysArrowperms apm "
						+ " LEFT JOIN apm.sysProfilarrowperms pap "
						+ " LEFT JOIN pap.sysProfil.utilisateurs usr "
						+ " WHERE usr.id=" + usrId + " AND apm.sysArrow.code='"
						+ arwCode + "' AND pap.allow = 1" + " AND sta.code='"
						+ staCode + "'");
		if (q.list().isEmpty())
			return false;
		else
			return true;
	}

	// liste des utilisateurs pouvant ex�cuter des actions � partir de l'�tat
	// cible
	@SuppressWarnings( { "unchecked", "null" })
	@Override
	public List<Utilisateur> findAssignableUsersAfterTransition(String arwCode) {
		List<Utilisateur> users = new ArrayList<Utilisateur>();
		String staCode = findToState(arwCode).getCode();
		Query q = getHibernateSession().createQuery(
				" SELECT usr FROM Utilisateur usr "
						+ " LEFT JOIN usr.sysProfil pf "
						+ " LEFT JOIN pf.sysProfilarrowperm pap "
						+ " LEFT JOIN pap.sysArrowperm apm "
						+ " LEFT JOIN apm.sysState sta " + " WHERE sta.code='"
						+ staCode + "'" + " AND pap.allow = 1");
		Set setItems = new LinkedHashSet(q.list());
		users.addAll(setItems);
		return users;
	}
	
	@Override
	public int countAssignableUsersAfterTransition(String arwCode) {
		List<Utilisateur> users = new ArrayList<Utilisateur>();
		String staCode = findToState(arwCode).getCode();
		Query q = getHibernateSession().createQuery(
				" SELECT count(usr) FROM Utilisateur usr "
						+ " LEFT JOIN usr.sysProfil pf "
						+ " LEFT JOIN pf.sysProfilarrowperm pap "
						+ " LEFT JOIN pap.sysArrowperm apm "
						+ " LEFT JOIN apm.sysState sta " + " WHERE sta.code='"
						+ staCode + "'" + " AND pap.allow = 1");
//		Set setItems = new LinkedHashSet(q.list());
//		users.addAll(setItems);
//		return users;
		
		return ((Long) q.uniqueResult()).intValue();
 
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Utilisateur> findAssignableUsersAfterTransition(int start, int step, String arwCode) {
		List<Utilisateur> users = new ArrayList<Utilisateur>();
		String staCode = findToState(arwCode).getCode();
		Query q = getHibernateSession().createQuery(
				" SELECT usr FROM Utilisateur usr "
						+ " LEFT JOIN usr.sysProfil pf "
						+ " LEFT JOIN pf.sysProfilarrowperm pap "
						+ " LEFT JOIN pap.sysArrowperm apm "
						+ " LEFT JOIN apm.sysState sta " + " WHERE sta.code='"
						+ staCode + "'" + " AND pap.allow = 1");
		q.setFirstResult(start);
		q.setMaxResults(step);
		Set setItems = new LinkedHashSet(q.list());
		users.addAll(setItems);
		return users;
//		return q.list();
	}
	
	@Override
	public SysState findState(String state) {
		Query q = getHibernateSession().createQuery(
				"SELECT state FROM SysState state "+
										"WHERE state.code='"+state+"'"
				);
		return (SysState)q.uniqueResult();
	}
}