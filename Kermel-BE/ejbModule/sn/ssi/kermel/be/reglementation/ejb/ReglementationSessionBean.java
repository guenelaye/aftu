package sn.ssi.kermel.be.reglementation.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygReglementation;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class ReglementationSessionBean extends AbstractSessionBean implements ReglementationSession{

	@Override
	public int count(Long code,String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygReglementation.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygReglementation.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygReglementation> find(int indice, int pas,Long code,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygReglementation.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygReglementation reglementation) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(reglementation);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygReglementation reglementation) {
		
		try{
			getHibernateSession().merge(reglementation);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygReglementation findById(Long code) {
		return (SygReglementation)getHibernateSession().get(SygReglementation.class, code);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygReglementation> findRech(int indice, int pas,String nature,String type,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygReglementation.class);
		
		if(nature!=null){
			criteria.add(Restrictions.eq("nature", nature));
		}
		
		if(type!=null){
			criteria.add(Restrictions.eq("type", type));
		}
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public int countRech(String nature,String type,String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygReglementation.class);
		
		if(nature!=null){
			criteria.add(Restrictions.eq("nature", nature));
		}
		
		if(type!=null){
			criteria.add(Restrictions.eq("type", type));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

}
