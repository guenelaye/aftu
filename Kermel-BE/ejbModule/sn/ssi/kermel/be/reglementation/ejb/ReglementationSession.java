package sn.ssi.kermel.be.reglementation.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygReglementation;

@Remote
public interface ReglementationSession {
	
	public void save(SygReglementation reglementation);
	public void delete(Long id);
	public List<SygReglementation> find(int indice, int pas,Long code,String libelle);
	public int count(Long code,String libelle);
	public void update(SygReglementation reglementation);
	public SygReglementation findById(Long code);
	
	public List<SygReglementation> findRech(int indice, int pas,String nature,String type,String libelle);
	public int countRech(String nature,String type,String libelle);
}
