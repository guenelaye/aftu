package sn.ssi.kermel.be.proceduresderogatoires.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygAvisAttribution;
import sn.ssi.kermel.be.entity.SygDerogatoire;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygPays;
import sn.ssi.kermel.be.entity.SygPrestatairegreagre;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class ProcedurederogatoireSessionBean extends AbstractSessionBean implements ProcedurederogatoireSession{




	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygDerogatoire.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	

	@Override
	public void save(SygDerogatoire derogatoire) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(derogatoire);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygDerogatoire derogatoire) {
		
		try{
			getHibernateSession().merge(derogatoire);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}	
	@Override
	public SygDerogatoire findById(Long code) {
		return (SygDerogatoire)getHibernateSession().get(SygDerogatoire.class, code);
	}
	@Override
	public List<SygDerogatoire> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDerogatoire.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));
		
		return criteria.list();
	}
	

	@Override
	public List<SygDerogatoire> find(int indice, int pas,String motif,String piecejointe,SygRealisations realisation,String typedemande1,String typedemande2,Date dateStatut,String statut,String reference) {
       Criteria criteria = getHibernateSession().createCriteria(SygDerogatoire.class);
       //criteria.createAlias("state", "state");
		//criteria.addOrder(Order.desc("datestatut"));
		if(motif!=null){
			criteria.add(Restrictions.eq("motif", motif));
			}
		if(piecejointe!=null){
			criteria.add(Restrictions.eq("piecejointe", piecejointe));
			}
		if(typedemande1!=null){
			criteria.add(Restrictions.eq("typedemande1", typedemande1));
			}
		if(typedemande2!=null){
			criteria.add(Restrictions.eq("typedemande2", typedemande2));
			}
		if(realisation!=null){
			criteria.add(Restrictions.eq("realisation", realisation));
			}
		if(dateStatut!=null){
			criteria.add(Restrictions.eq("dateStatut", dateStatut));
			}
		if(statut!=null){
			criteria.add(Restrictions.eq("statut", statut));
			}
		if(reference!=null){
			criteria.add(Restrictions.eq("reference", reference));
			}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}

	public List<SygDerogatoire> find(int indice, int pas,String motif,String piecejointe,SygRealisations realisation,Long typemarche,String typedemande1,String typedemande2,Date dateStatut,String statut,String reference) {
	       Criteria criteria = getHibernateSession().createCriteria(SygDerogatoire.class);
	       //criteria.createAlias("state", "state");
			//criteria.addOrder(Order.desc("datestatut"));
	       criteria.createAlias("realisation", "realisation");
	       criteria.createAlias("realisation.typemarche", "typemarche");
			if(motif!=null){
				criteria.add(Restrictions.eq("motif", motif));
				}
			if(piecejointe!=null){
				criteria.add(Restrictions.eq("piecejointe", piecejointe));
				}
			if(typedemande1!=null){
				criteria.add(Restrictions.eq("typedemande1", typedemande1));
				}
			if(typedemande2!=null){
				criteria.add(Restrictions.eq("typedemande2", typedemande2));
				}
			if(realisation!=null){
				criteria.add(Restrictions.eq("realisation", realisation));
				}
			if(typemarche!=null){
				criteria.add(Restrictions.eq("typemarche.id", typemarche));
				}
			if(dateStatut!=null){
				criteria.add(Restrictions.eq("dateStatut", dateStatut));
				}
			if(statut!=null){
				criteria.add(Restrictions.eq("statut", statut));
				}
			if(reference!=null){
				criteria.add(Restrictions.eq("reference", reference));
				}
			criteria.setFirstResult(indice);
			if(pas>0)
			criteria.setMaxResults(pas);
				
			return criteria.list();
			}




@Override
public List<SygDerogatoire> find(int indice, int pas,SygTypesmarches type,SygModepassation mode,SygRealisations realisation,int validationED,int validationAOR,int validationPU) {
   Criteria criteria = getHibernateSession().createCriteria(SygDerogatoire.class);
   criteria.createAlias("realisation", "realisation");
   criteria.createAlias("user", "user");
   criteria.addOrder(Order.desc("id"));
	if(type!=null){
		criteria.add(Restrictions.eq("realisation.typemarche", type));
		}
	if(mode!=null){
		criteria.add(Restrictions.eq("modepassation", mode));
		}
	if(realisation!=null){
		criteria.add(Restrictions.eq("realisation", realisation));
		}
	if(validationED>-1){
		criteria.add(Restrictions.eq("validationED", validationED));
		}
	if(validationAOR>-1){
		criteria.add(Restrictions.eq("validationAOR", validationAOR));
		}
	if(validationPU>-1){
		criteria.add(Restrictions.eq("validationPU", validationPU));
		}
	
	criteria.setFirstResult(indice);
	if(pas>0)
	criteria.setMaxResults(pas);
		
	return criteria.list();
	}


@Override
public int count(SygTypesmarches type,SygModepassation mode,SygRealisations realisation,int validationED,int validationAOR,int validationPU) {
	Criteria criteria = getHibernateSession().createCriteria(SygDerogatoire.class);
	criteria.createAlias("realisation", "realisation");
	if(type!=null){
		criteria.add(Restrictions.eq("realisation.typemarche", type));
		}
	if(mode!=null){
		criteria.add(Restrictions.eq("modepassation", mode));
		}
	if(realisation!=null){
		criteria.add(Restrictions.eq("realisation", realisation));
		}
	if(validationED>-1){
		criteria.add(Restrictions.eq("validationED", validationED));
		}
	if(validationAOR>-1){
		criteria.add(Restrictions.eq("validationAOR", validationAOR));
		}
	if(validationPU>-1){
		criteria.add(Restrictions.eq("validationPU", validationPU));
		}
	criteria.setProjection(Projections.rowCount());
	return Integer.parseInt(criteria.uniqueResult().toString());

}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygDerogatoire> findDemandes(int indice, int pas,Long type) {
		// TODO Auto-generated method stub
		return getHibernateSession().createQuery("SELECT demandes FROM  SygDerogatoire demandes  where realisation.typemarche.id='"+type+"' group by demandes.realisation.idrealisation ")
					.setFirstResult(indice).setMaxResults(pas).list();	
	}
	
	@Override
	public int countDemandes(Long type) {
		return getHibernateSession().createQuery("SELECT demandes FROM  SygDerogatoire demandes  where realisation.typemarche.id='"+type+"' group by demandes.realisation.idrealisation ")
				.list().size();	
	}
	
/////////:::::Prestataire Gr�agre///////////
	
	@Override
	public void deletePrestataire(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygPrestatairegreagre.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	

	@Override
	public void savePrestataire(SygPrestatairegreagre prestataire) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(prestataire);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void updatePrestataire(SygPrestatairegreagre derogatoire) {
		
		try{
			getHibernateSession().merge(derogatoire);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}	
	@Override
	public SygPrestatairegreagre findPrestataire(Long code) {
		return (SygPrestatairegreagre)getHibernateSession().get(SygPrestatairegreagre.class, code);
	}
	
	@Override
	public List<SygPrestatairegreagre> findPrestataires(int indice, int pas,SygAutoriteContractante autorite,SygAppelsOffres appel,SygPays pays) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPrestatairegreagre.class);
		if(autorite!=null)
			criteria.add(Restrictions.eq("autorite", autorite));
		if(appel!=null)
			criteria.add(Restrictions.eq("appel", appel));
		if(pays!=null)
			criteria.add(Restrictions.eq("pays", pays));
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
		return criteria.list();
	}
	
	@Override
	public int countPrestataires(SygAutoriteContractante autorite,SygAppelsOffres appel,SygPays pays) {
		Criteria criteria = getHibernateSession().createCriteria(SygPrestatairegreagre.class);
		criteria.createAlias("realisation", "realisation");
		if(autorite!=null)
			criteria.add(Restrictions.eq("autorite", autorite));
		if(appel!=null)
			criteria.add(Restrictions.eq("appel", appel));
		if(pays!=null)
			criteria.add(Restrictions.eq("pays", pays));
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
	@Override
	public SygPrestatairegreagre getPrestataire(SygAutoriteContractante autorite,SygAppelsOffres appel){
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPrestatairegreagre.class);
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(appel!=null)
			criteria.add(Restrictions.eq("appel", appel));
		if(criteria.list().size()>0)
		  return (SygPrestatairegreagre) criteria.list().get(0);
		else 
			return null;
	}

}
