package sn.ssi.kermel.be.proceduresderogatoires.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;


import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygAvisAttribution;
import sn.ssi.kermel.be.entity.SygDerogatoire;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygModepassation;
import sn.ssi.kermel.be.entity.SygPays;
import sn.ssi.kermel.be.entity.SygPrestatairegreagre;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygTypeDemande;
import sn.ssi.kermel.be.entity.SygTypesmarches;


@Remote
public interface ProcedurederogatoireSession {
	public void save(SygDerogatoire derogatoire);
	public void delete(Long id);
	public List<SygDerogatoire> find(int indice, int pas,String motif,String piecejointe,SygRealisations realisation,String typedemande1,String typedemande2,Date dateStatut,String statut,String reference);
	public List<SygDerogatoire> find(int indice, int pas,String motif,String piecejointe,SygRealisations realisation,Long typemarche,String typedemande1,String typedemande2,Date dateStatut,String statut,String reference);
	//public int count(Date DateValidation);
	public void update(SygDerogatoire derogatoire);
	public SygDerogatoire findById(Long code);
	public List<SygDerogatoire> find(String code);
	
	public List<SygDerogatoire> find(int indice, int pas,SygTypesmarches type,SygModepassation mode,SygRealisations realisation,int validationED,int validationAOR,int validationPU);
	int count(SygTypesmarches type,SygModepassation mode,SygRealisations realisation,int validationED,int validationAOR,int validationPU);
	
	public List<SygDerogatoire> findDemandes(int indice, int pas,Long type);
	int countDemandes(Long type);
	

	/////////:::::Prestataire Gr�agre///////////
	public void savePrestataire(SygPrestatairegreagre prestataire);
	public void deletePrestataire(Long id);
	public void updatePrestataire(SygPrestatairegreagre prestataire);
	public SygPrestatairegreagre findPrestataire(Long code);
	public List<SygPrestatairegreagre> findPrestataires(int indice, int pas,SygAutoriteContractante autorite,SygAppelsOffres appel,SygPays pays);
	int countPrestataires(SygAutoriteContractante autorite,SygAppelsOffres appel,SygPays pays);
	public SygPrestatairegreagre getPrestataire(SygAutoriteContractante autorite,SygAppelsOffres appel);

}

