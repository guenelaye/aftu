package sn.ssi.kermel.be.recouvrement.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygRecouvrement;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class RecouvrementSessionBean extends AbstractSessionBean implements RecouvrementSession{

	@Override
	public int count(Long code,String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygRecouvrement.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygRecouvrement.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygRecouvrement> find(int indice, int pas,Long code,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygRecouvrement.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygRecouvrement recouvrement) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(recouvrement);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygRecouvrement recouvrement) {
		
		try{
			getHibernateSession().merge(recouvrement);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygRecouvrement findById(Long code) {
		return (SygRecouvrement)getHibernateSession().get(SygRecouvrement.class, code);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygRecouvrement> findRech(int indice, int pas,Long code,String nom,String prenom) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygRecouvrement.class);
		
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("nom", "%"+nom+"%"));
		}
		
		if(prenom!=null){
			criteria.add(Restrictions.ilike("prenom", "%"+prenom+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public int countRech(Long code,String nom,String prenom) {
		Criteria criteria = getHibernateSession().createCriteria(SygRecouvrement.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("nom", "%"+nom+"%"));
		}
		
		if(prenom!=null){
			criteria.add(Restrictions.ilike("prenom", "%"+prenom+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
}
