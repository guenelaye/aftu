package sn.ssi.kermel.be.recouvrement.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygOperationRecouvrement;

@Remote
public interface OperationRecouvrementSession {
	
	public void save(SygOperationRecouvrement operation);
	public void delete(Long id);
	public List<SygOperationRecouvrement> find(int indice, int pas,Long code,String libelle);
	public int count(Long code,String libelle);
	public void update(SygOperationRecouvrement operation);
	public SygOperationRecouvrement findById(Long code);
	
	String getGeneratedCode(String codeParametreGeneral);
	
}
