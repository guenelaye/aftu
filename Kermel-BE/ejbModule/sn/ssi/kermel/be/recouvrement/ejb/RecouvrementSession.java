package sn.ssi.kermel.be.recouvrement.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygRecouvrement;

@Remote
public interface RecouvrementSession {
	
	public void save(SygRecouvrement recouvrement);
	
	public void delete(Long id);
	
	public List<SygRecouvrement> find(int indice, int pas,Long code,String libelle);
	
	public List<SygRecouvrement> findRech(int indice, int pas,Long code,String nom,String prenom);
	
	public int count(Long code,String libelle);
	
	public int countRech(Long code,String nom,String prenom);
	
	public void update(SygRecouvrement recouvrement);
	
	public SygRecouvrement findById(Long code);
	
}
