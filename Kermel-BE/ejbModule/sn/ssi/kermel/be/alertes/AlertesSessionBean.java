package sn.ssi.kermel.be.alertes;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;

import sn.ssi.kermel.be.entity.SysAlerte;
import sn.ssi.kermel.be.entity.SysAlerteprofil;
import sn.ssi.kermel.be.entity.SysMessage;
import sn.ssi.kermel.be.entity.SysUtilisateuralerte;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.workflow.entity.SysState;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless
class AlertesSessionBean extends AbstractSessionBean implements AlertesSession {

	@Override
	public int countAllAlertes() {
		Criteria criteria = getHibernateSession().createCriteria(
				SysAlerte.class);
		criteria.setProjection(Projections.count("id"));
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void createAlerte(SysAlerte a) {
		getHibernateSession().saveOrUpdate(a);

	}

	@Override
	public void createAlerteprofil(SysAlerteprofil apf) {
		getHibernateSession().saveOrUpdate(apf);

	}

	@Override
	public void createAlerteutilisateur(SysUtilisateuralerte usa) {
		getHibernateSession().saveOrUpdate(usa);

	}

	@Override
	public void deleteAlerte(long id) {
		SysAlerte a = (SysAlerte) getHibernateSession()
				.get(SysAlerte.class, id);
		getHibernateSession().delete(a);

	}

	@Override
	public SysAlerte findById(long id) {
		return (SysAlerte) getHibernateSession().get(SysAlerte.class, id);
	}

	@Override
	public Utilisateur findUserByLogin(String login) {
		Query q = getHibernateSession().createQuery(
				"FROM Utilisateur  " + "WHERE login='" + login + "'");
		return (Utilisateur) q.uniqueResult();
	}

	@Override
	public Utilisateur findUserById(long id) {
		Query q = getHibernateSession().createQuery(
				"FROM Utilisateur  " + "WHERE id=" + id);
		return (Utilisateur) q.uniqueResult();
	}

	@Override
	public List<Utilisateur> findAlerteUsers(long id) {
		Query q = getHibernateSession().createQuery(
				"SELECT usa.utilisateur FROM "
						+ "SysUtilisateuralerte usa WHERE usa.sysAlerte.id="
						+ id);
		return q.list();
	}

	@Override
	public List<SysAlerte> findAlertes(int start, int step) {
		List<SysAlerte> alertes = null;
		Criteria criteria = getHibernateSession().createCriteria(
				SysAlerte.class);
		criteria.addOrder(Order.asc("id"));
		criteria.setFirstResult(start);
		if (step > 0) {
			criteria.setMaxResults(step);
		}
		alertes = criteria.list();
		return alertes;
	}

	@Override
	public boolean isProfilAlerted(String pfCode, long altId) {

		Query q = getHibernateSession().createQuery(
				"FROM SysAlerteprofil  " + "WHERE sysAlerte.id=" + altId
						+ " AND sysProfil.pfCode='" + pfCode + "' ");
		if (q.list().isEmpty())
			return false;
		else
			return true;
	}

	@Override
	public SysUtilisateuralerte findUtilisateurAlerte(long usrId, long altId) {

		Query q = getHibernateSession().createQuery(
				"FROM SysUtilisateuralerte  " + "WHERE sysAlerte.id=" + altId
						+ " AND utilisateur.id='" + usrId + "' ");
		if (q.list().isEmpty())
			return null;
		else
			return (SysUtilisateuralerte) q.list().get(0);
	}

	@Override
	public boolean deleteAlerteprofil(long altId) {

		Query q = getHibernateSession().createQuery(
				"DELETE FROM SysAlerteprofil apf " + "WHERE apf.sysAlerte.id="
						+ altId);
		q.executeUpdate();
		return true;
	}

	@Override
	public boolean deleteAlerteuser(long altId) {

		Query q = getHibernateSession().createQuery(
				"DELETE FROM SysUtilisateuralerte usa "
						+ "WHERE usa.sysAlerte.id=" + altId);
		q.executeUpdate();
		return true;
	}

	@Override
	public List<Utilisateur> findUsers(int start, int step) {
		List<Utilisateur> users = null;
		Criteria criteria = getHibernateSession().createCriteria(
				Utilisateur.class);
		criteria.addOrder(Order.asc("id"));
		criteria.setFirstResult(start);
		if (step > 0) {
			criteria.setMaxResults(step);
		}
		users = criteria.list();
		return users;
	}

	@Override
	public String getAlerteContent(SysAlerte alt, String[] params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SysAlerte> getDateAlerte(Date day) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SysAlerte> getDateAlerte(Date day, long usrId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getFrequenceAlerte(SysAlerte alt) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<SysAlerte> getStateDateAlerte(SysState state, Date day) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SysAlerte> getStateDateAlerte(SysState state, Date day,
			long usrId) {
		return null;
	}

	@Override
	public List<SysAlerte> getTodaysAlerte() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SysAlerte> getTodaysAlerte(long usrId) {
		return null;
	}

	@Override
	public List<SysMessage> getSysMessages(String staCode) {
		List<SysMessage> messages = new ArrayList<SysMessage>();
		Query q = getHibernateSession().createQuery(
				"FROM SysAlerte alt LEFT JOIN FETCH alt.sysUtilisateuralertes usa "
						+ " LEFT JOIN FETCH usa.utilisateur usr "
						+ " LEFT JOIN FETCH alt.sysAlerteprofils pfa "
						+ " LEFT JOIN FETCH pfa.sysProfil.utilisateurs pfu "
						+ " WHERE alt.sysState.code='" + staCode + "' AND "
						+ " alt.notification=1");

		List<SysAlerte> alertes = q.list();
		for (SysAlerte alerte : alertes) {
			for (SysUtilisateuralerte usa : alerte.getSysUtilisateuralertes()) {
				SysMessage message = new SysMessage();
				message.setContent(alerte.getContenu());
				message.setUsermail(usa.getUtilisateur().getMail());
				messages.add(message);
			}

			for (SysAlerteprofil pf : alerte.getSysAlerteprofils()) {
				for (Utilisateur user : pf.getSysProfil().getUtilisateurs()) {
					SysMessage message = new SysMessage();
					message.setContent(alerte.getContenu());
					message.setUsermail(user.getMail());
					messages.add(message);
				}
			}
		}
		return messages;
	}

	public int sendSomething(String type, Object thing) {
		HashMap hm = new HashMap();
		hm.put("TYPE", type);
		hm.put("OBJECT", thing);
		Queue queue = null;
		QueueConnectionFactory factory = null;
		QueueConnection connection = null;
		QueueSession session = null;
		MessageProducer messageProducer = null;
		try {
			InitialContext ctx = new InitialContext();
			queue = (Queue) ctx.lookup("queue/AlertesQueue");
			factory = (QueueConnectionFactory) ctx.lookup("ConnectionFactory");
			connection = factory.createQueueConnection();
			session = connection.createQueueSession(false,
					QueueSession.AUTO_ACKNOWLEDGE);
			messageProducer = session.createProducer(queue);
			ObjectMessage om = session.createObjectMessage(hm);
			messageProducer.send(om);
			messageProducer.close();
			connection.close();
		} catch (JMSException ex) {
			ex.printStackTrace();
		} catch (NamingException ex) {
			ex.printStackTrace();
		}
		return 0;
	}

	@Override
	public int sendNotifications(String staCode) {
		this.sendSomething("STATE", staCode);
		return 0;
	}

	@Override
	public int sendMails(List<SysMessage> mails) {
		this.sendSomething("MAILS", mails);
		return 0;
	}

	@Override
	public int sendAlertes() {
		// this.sendSomething("","");
		return 0;
	}

}