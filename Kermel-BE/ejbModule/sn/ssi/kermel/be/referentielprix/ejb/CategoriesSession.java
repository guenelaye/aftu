package sn.ssi.kermel.be.referentielprix.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCategories;




@Remote
public interface CategoriesSession {
	public void save(SygCategories categories);
	public void delete(Long id);
	public List<SygCategories> find(int indice, int pas,String code,String designation);
	public int count(String code,String designation);
	public void update(SygCategories categories);
	public SygCategories findById(Long code);
	public List<SygCategories> find(String code);
	List<SygCategories> find(int indice, int pas);
	int count();
	public List<SygCategories> findRech(int indice, int pas,String code,String categoriesref,String designation);
	public int countRech(String libelle, String categoriesref);
	
	public List<SygCategories> findRech1(int indice, int pas,String code, String categoriesref);
	
}

