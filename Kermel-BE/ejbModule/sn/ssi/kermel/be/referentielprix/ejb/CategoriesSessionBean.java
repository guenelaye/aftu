package sn.ssi.kermel.be.referentielprix.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygCategories;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class CategoriesSessionBean extends AbstractSessionBean implements CategoriesSession{

	@Override
	public int count(String code,String designation) {
		Criteria criteria = getHibernateSession().createCriteria(SygCategories.class);
		// criteria.createAlias("state", "state");
		if(code!=null){
			criteria.add(Restrictions.ilike("String", "%"+code+"%"));
		}	
		
		
		if(designation!=null){
			criteria.add(Restrictions.ilike("designation", "%"+designation+"%"));
			}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygCategories.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygCategories.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	

	@Override
	public void save(SygCategories categories) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(categories);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygCategories categories) {
		
		try{
			getHibernateSession().merge(categories);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}	
	@Override
	public SygCategories findById(Long code) {
		return (SygCategories)getHibernateSession().get(SygCategories.class, code);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<SygCategories> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCategories.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));
		
		return criteria.list();
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<SygCategories> find(int indice, int pas,String code,String designation) {
       Criteria criteria = getHibernateSession().createCriteria(SygCategories.class);
       //criteria.createAlias("state", "state");
		//criteria.addOrder(Order.desc("datestatut"));
		if(code!=null){
			criteria.add(Restrictions.ilike("code", "%"+code+"%"));
			}
		if(designation!=null){
			criteria.add(Restrictions.ilike("designation", "%"+designation+"%"));
			}
		
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}
	@SuppressWarnings("unchecked")
	@Override
	public List<SygCategories> findRech(int indice, int pas,String code,String categoriesref,String designation) {
       Criteria criteria = getHibernateSession().createCriteria(SygCategories.class);

		if(code!=null){
			criteria.add(Restrictions.eq("code", code));
			}
		if(categoriesref!=null){
			criteria.add(Restrictions.eq("categoriesref", categoriesref));
			}
		if(designation!=null){
			criteria.add(Restrictions.ilike("designation", "%"+designation+"%"));
			}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}
	public int countRech(String code, String categoriesref) {
	       Criteria criteria = getHibernateSession().createCriteria(SygCategories.class);
	       //criteria.createAlias("state", "state");
			//criteria.addOrder(Order.desc("datestatut"));
			if(code!=null){
				criteria.add(Restrictions.eq("code", code));
				}
			if(categoriesref!=null){
				criteria.add(Restrictions.eq("categoriesref", categoriesref));
				}
				
			criteria.setProjection(Projections.rowCount());
			return Integer.parseInt(criteria.uniqueResult().toString());
			}
	@SuppressWarnings("unchecked")
	@Override
	public List<SygCategories> findRech1(int indice, int pas,String code, String categoriesref) {
       Criteria criteria = getHibernateSession().createCriteria(SygCategories.class);
       //criteria.createAlias("state", "state");
		//criteria.addOrder(Order.desc("datestatut"));
		if(code!=null){
			criteria.add(Restrictions.eq("code", code));
			}
		if(categoriesref!=null){
			criteria.add(Restrictions.eq("categoriesref", categoriesref));
			}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}

@SuppressWarnings("unchecked")
@Override
public List<SygCategories> find(int indice, int pas) {
   Criteria criteria = getHibernateSession().createCriteria(SygCategories.class);
	criteria.setFirstResult(indice);
	if(pas>0)
	criteria.setMaxResults(pas);
		
	return criteria.list();
	}


}
