package sn.ssi.kermel.be.referentielprix.ejb;

import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygFamilles;
import sn.ssi.kermel.be.entity.SygPrix;
import sn.ssi.kermel.be.entity.SygProduits;

@Remote
public interface PrixSession {
	public void save(SygPrix prix);
	
	public void delete(Long id);
	
	public List<SygPrix> find(int indice, int pas,Long code,String libelle, Long produit);
	
	public int count(Long code,String libelle, Long produit);
	
	public void update(SygPrix prix);
	
	public SygPrix findById(Long code);
	
	
	public List<SygPrix> findRech(int indice, int pas,String libelle,SygProduits produit,int anne);
	public int countRech(String libelle,SygProduits produit,int anne);
	
	
	

	public SygPrix findByFamillAndProduit(SygFamilles famille, Long produit,int annee);

	public Map<String, Integer> findf1(Long distID);

	public SygPrix savePrix(SygPrix prix);

	public List<SygPrix> findProduits(int indice, int pas, String libelle,
			Long famille, Long produit, int annee);

	public int countProduits(String libelle, Long famille, Long produit, int annee);

	
}
