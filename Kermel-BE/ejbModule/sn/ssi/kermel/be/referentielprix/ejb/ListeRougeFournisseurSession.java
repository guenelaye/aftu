package sn.ssi.kermel.be.referentielprix.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCategories;
import sn.ssi.kermel.be.entity.SygListeRougeF;




@Remote
public interface ListeRougeFournisseurSession {
	public void save(SygListeRougeF categories);
	public void delete(Long id);
	public List<SygListeRougeF> find(int indice, int pas,String code,String designation);
	public int count(String code,String designation);
	public void update(SygListeRougeF categories);
	public SygListeRougeF findById(Long code);
	public List<SygListeRougeF> find(String code);
	List<SygListeRougeF> find(int indice, int pas);
	int count();
	public List<SygListeRougeF> findRech(int indice, int pas,String code,String categoriesref,String designation);
	public int countRech(String libelle, String categoriesref);
	
	public List<SygListeRougeF> findRech1(int indice, int pas,String code, String categoriesref);
	
}

