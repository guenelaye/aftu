package sn.ssi.kermel.be.referentielprix.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygFamilles;
import sn.ssi.kermel.be.entity.SygProduits;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class ProduitsSessionBean extends AbstractSessionBean implements ProduitsSession{

	@Override
	public int count(String code) {
		Criteria criteria = getHibernateSession().createCriteria(SygProduits.class);
		// criteria.createAlias("state", "state");
		if(code!=null){
			criteria.add(Restrictions.ge("String", code));
		}	
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygProduits.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());


	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygProduits.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	

	@Override
	public void save(SygProduits produits) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(produits);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygProduits produits) {
		
		try{
			getHibernateSession().merge(produits);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}	
	@Override
	public SygProduits findById(Long code) {
		return (SygProduits)getHibernateSession().get(SygProduits.class, code);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<SygProduits> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygProduits.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));
		
		return criteria.list();
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<SygProduits> find(int indice, int pas,String code,String designation,String commentaire,SygFamilles familles) {
       Criteria criteria = getHibernateSession().createCriteria(SygProduits.class);
       //criteria.createAlias("state", "state");
		//criteria.addOrder(Order.desc("datestatut"));
		if(code!=null){
			criteria.add(Restrictions.eq("code", code));
			}
		if(designation!=null){
			criteria.add(Restrictions.eq("designation", designation));
			}
		if(commentaire!=null){
			criteria.add(Restrictions.eq("commentaire", commentaire));
			}
		if(familles!=null){
			criteria.add(Restrictions.eq("familles", familles));
			}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygProduits> findRech(int indice, int pas,String code,String designation,Long famillesref) {
       Criteria criteria = getHibernateSession().createCriteria(SygProduits.class);
       criteria.createAlias("familles", "famillesref" );
       
       if(code!=null){
			criteria.add(Restrictions.ilike("code", "%"+code+"%"));
			}
		
		if(designation!=null){
			criteria.add(Restrictions.ilike("designation", "%"+designation+"%"));
			}
		
		if(famillesref!=null){
			criteria.add(Restrictions.eq("famillesref.id", famillesref));
			}
		
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}
	public int countRech(String code,String designation,Long famillesref) {
	       Criteria criteria = getHibernateSession().createCriteria(SygProduits.class);
	       criteria.createAlias("familles", "famillesref" );
			if(code!=null){
				criteria.add(Restrictions.ilike("code", "%"+code+"%"));
				}
		
			if(designation!=null){
				criteria.add(Restrictions.ilike("designation", "%"+designation+"%"));
				}
			
			if(famillesref!=null){
				criteria.add(Restrictions.eq("famillesref.id", famillesref));
				}
			criteria.setProjection(Projections.rowCount());
			return Integer.parseInt(criteria.uniqueResult().toString());
			}
	@SuppressWarnings("unchecked")
	@Override
	public List<SygProduits> findRech1(int indice, int pas,String code, String produitsref) {
       Criteria criteria = getHibernateSession().createCriteria(SygProduits.class);
       //criteria.createAlias("state", "state");
		//criteria.addOrder(Order.desc("datestatut"));
		if(code!=null){
			criteria.add(Restrictions.eq("code", code));
			}
		if(produitsref!=null){
			criteria.add(Restrictions.eq("produitsref", produitsref));
			}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}
@SuppressWarnings("unchecked")
@Override
public List<SygProduits> find(int indice, int pas) {
   Criteria criteria = getHibernateSession().createCriteria(SygProduits.class);
	criteria.setFirstResult(indice);
	if(pas>0)
	criteria.setMaxResults(pas);
		
	return criteria.list();
	}


@SuppressWarnings("unchecked")
@Override
public List<SygProduits> findByFamilly(int indice, int pas,Long famillesref) {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession().createCriteria( SygProduits.class);
	criteria.createAlias("familles","familles");
	
	
	criteria.add(Restrictions.eq("familles.id", famillesref));
	
	
	criteria.setFirstResult(indice);
	if(pas>0)
	criteria.setMaxResults(pas);
		
	return criteria.list();
}

}
