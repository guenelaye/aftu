package sn.ssi.kermel.be.referentielprix.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCategories;
import sn.ssi.kermel.be.entity.SygFamilles;





@Remote
public interface FamillesSession {
	public void save(SygFamilles familles);
	public void delete(Long id);
	public List<SygFamilles> find(int indice, int pas,String code,String designation,String commentaire,SygCategories categories);
	public int count(String code);
	public void update(SygFamilles familles);
	public SygFamilles findById(Long code);
	public List<SygFamilles> find(String code);
	List<SygFamilles> find(int indice, int pas);
	int count();
	public List<SygFamilles> findRech(int indice, int pas,String code,String designation,Long categoriesref);
	public int countRech(String libelle,String designation,Long categoriesref);
	
	public List<SygFamilles> findRech1(int indice, int pas,String code, String famillesref);
}

