package sn.ssi.kermel.be.referentielprix.ejb;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygFamilles;
import sn.ssi.kermel.be.entity.SygPrix;
import sn.ssi.kermel.be.entity.SygProduits;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class PrixSessionBean extends AbstractSessionBean implements PrixSession{

	@Override
	public int count(Long code,String libelle, Long produit) {
		Criteria criteria = getHibernateSession().createCriteria(SygPrix.class);
		//criteria.setFetchMode("typeuniteorg", FetchMode.SELECT);
		 criteria.createAlias("produitservice", "produitservice" );
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		if(produit!=null){
			criteria.add(Restrictions.eq("produitservice.id", produit));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygPrix.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygPrix> find(int indice, int pas,Long code,String libelle, Long produit) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPrix.class);
		//criteria.setFetchMode("typeuniteorg", FetchMode.SELECT);
		 criteria.createAlias("produitservice", "produitservice" );
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		if(produit!=null){
			criteria.add(Restrictions.eq("produitservice.id", produit));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygPrix prix) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(prix);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}
	
	
	

	@Override
	public void update(SygPrix prix) {
		
		try{
			getHibernateSession().merge(prix);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygPrix findById(Long code) {
		return (SygPrix)getHibernateSession().get(SygPrix.class, code);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygPrix> findRech(int indice, int pas,String libelle,SygProduits produit,int anne) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPrix.class);
		criteria.addOrder(Order.asc("id"));
		criteria.createAlias("produitservice", "produitservice" );
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		
		if(produit!=null){
			criteria.add(Restrictions.eq("produitservice", produit));
		}
		
		
		if(anne!=0){
			criteria.add(Restrictions.eq("annee", anne));
		}
			
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public int countRech(String libelle,SygProduits produit,int anne) {
		Criteria criteria = getHibernateSession().createCriteria(SygPrix.class);
         criteria.createAlias("produitservice", "produitservice" );
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		
		if(produit!=null){
			criteria.add(Restrictions.eq("produitservice", produit));
		}
		
		if(anne!=0){
			criteria.add(Restrictions.eq("annee", anne));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygPrix> findProduits(int indice, int pas,String libelle,Long famille,Long produit,int annee) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPrix.class);
		criteria.addOrder(Order.asc("id"));
		criteria.createAlias("produitservice", "produitservice" );
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		
		if(famille!=null){
			criteria.add(Restrictions.eq("produitservice.familles.id", famille));
		}
		
		if(produit!=null){
			criteria.add(Restrictions.eq("produitservice.id", produit));
		}
		
		if(annee!=0){
			criteria.add(Restrictions.eq("annee", annee));
		}
			
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public int countProduits(String libelle,Long famille,Long produit,int annee) {
		Criteria criteria = getHibernateSession().createCriteria(SygPrix.class);
         criteria.createAlias("produitservice", "produitservice" );
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		
		if(famille!=null){
			criteria.add(Restrictions.eq("produitservice.familles.id", famille));
		}
		
		
		if(produit!=null){
			criteria.add(Restrictions.eq("produitservice.id", produit));
		}
		
		if(annee!=0){
			criteria.add(Restrictions.eq("annee", annee));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
	//ADD 14-01-2012
	@Override
	public SygPrix findByFamillAndProduit(SygFamilles famille, Long produit,int annee) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria( SygPrix.class);
		
		criteria.createAlias("produitservice", "produitservice" );
		criteria.add(Restrictions.eq("produitservice.familles", famille));
		criteria.add(Restrictions.eq("produitservice.id", produit));
		
		if(annee!=0){
			criteria.add(Restrictions.eq("annee", annee));
		}
		if(criteria.list().size()>0)
			return (SygPrix) criteria.list().get(0);
		else return null;
	}
	
	@Override
	public Map<String, Integer> findf1(Long distID) {
		// TODO Auto-generated method stub
		Map<String, Integer> values = new HashMap<String, Integer>();
		values.put("un", 0);
		values.put("deux", 0);
		values.put("trois", 0);
		values.put("quatre", 0);
		values.put("cinq", 0);
		values.put("six", null);
		values.put("sept", 0);
		values.put("huit", 0);
		values.put("neuf", 0);
		values.put("dix", 0);
		values.put("onze", 0);
		values.put("douze", 0);
		values.put("treize", 0);
		values.put("quatorze", 0);
		values.put("quinze", 0);
		values.put("seize", 0);

		return values;
	}
	
	
	@Override
	public SygPrix savePrix(SygPrix prix) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(prix);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return prix;
	}

}
