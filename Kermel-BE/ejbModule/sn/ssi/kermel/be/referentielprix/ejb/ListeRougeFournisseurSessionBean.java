package sn.ssi.kermel.be.referentielprix.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygCategories;
import sn.ssi.kermel.be.entity.SygListeRougeF;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class ListeRougeFournisseurSessionBean extends AbstractSessionBean implements ListeRougeFournisseurSession{

	@Override
	public int count(String code,String designation) {
		Criteria criteria = getHibernateSession().createCriteria(SygListeRougeF.class);
		// criteria.createAlias("state", "state");
		if(code!=null){
			//criteria.add(Restrictions.ilike("String", "%"+code+"%"));
		}	
		
		
		if(designation!=null){
			//criteria.add(Restrictions.ilike("designation", "%"+designation+"%"));
			}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygListeRougeF.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygListeRougeF.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	

	@Override
	public void save(SygListeRougeF categories) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(categories);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygListeRougeF categories) {
		
		try{
			getHibernateSession().merge(categories);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}	
	@Override
	public SygListeRougeF findById(Long code) {
		return (SygListeRougeF)getHibernateSession().get(SygListeRougeF.class, code);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<SygListeRougeF> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygListeRougeF.class);
		//if(code!=null)
		//	criteria.add(Restrictions.eq("codepiece", code));
		
		return criteria.list();
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<SygListeRougeF> find(int indice, int pas,String code,String designation) {
       Criteria criteria = getHibernateSession().createCriteria(SygListeRougeF.class);
       //criteria.createAlias("state", "state");
		//criteria.addOrder(Order.desc("datestatut"));
		if(code!=null){
			//criteria.add(Restrictions.ilike("code", "%"+code+"%"));
			}
		if(designation!=null){
			//criteria.add(Restrictions.ilike("designation", "%"+designation+"%"));
			}
		
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}
	@SuppressWarnings("unchecked")
	@Override
	public List<SygListeRougeF> findRech(int indice, int pas,String code,String categoriesref,String designation) {
       Criteria criteria = getHibernateSession().createCriteria(SygListeRougeF.class);

		if(code!=null){
			//criteria.add(Restrictions.eq("code", code));
			}
		if(categoriesref!=null){
			//criteria.add(Restrictions.eq("categoriesref", categoriesref));
			}
		if(designation!=null){
			//criteria.add(Restrictions.ilike("designation", "%"+designation+"%"));
			}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}
	public int countRech(String code, String categoriesref) {
	       Criteria criteria = getHibernateSession().createCriteria(SygListeRougeF.class);
	       //criteria.createAlias("state", "state");
			//criteria.addOrder(Order.desc("datestatut"));
			if(code!=null){
			//	criteria.add(Restrictions.eq("code", code));
				}
			if(categoriesref!=null){
			//	criteria.add(Restrictions.eq("categoriesref", categoriesref));
				}
				
			criteria.setProjection(Projections.rowCount());
			return Integer.parseInt(criteria.uniqueResult().toString());
			}
	@SuppressWarnings("unchecked")
	@Override
	public List<SygListeRougeF> findRech1(int indice, int pas,String code, String categoriesref) {
       Criteria criteria = getHibernateSession().createCriteria(SygListeRougeF.class);
       //criteria.createAlias("state", "state");
		//criteria.addOrder(Order.desc("datestatut"));
		if(code!=null){
			//criteria.add(Restrictions.eq("code", code));
			}
		if(categoriesref!=null){
			//criteria.add(Restrictions.eq("categoriesref", categoriesref));
			}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}

@SuppressWarnings("unchecked")
@Override
public List<SygListeRougeF> find(int indice, int pas) {
   Criteria criteria = getHibernateSession().createCriteria(SygListeRougeF.class);
	criteria.setFirstResult(indice);
	if(pas>0)
	criteria.setMaxResults(pas);
		
	return criteria.list();
	}


}
