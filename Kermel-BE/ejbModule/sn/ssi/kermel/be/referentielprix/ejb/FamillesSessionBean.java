package sn.ssi.kermel.be.referentielprix.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygCategories;
import sn.ssi.kermel.be.entity.SygFamilles;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class FamillesSessionBean extends AbstractSessionBean implements FamillesSession{

	@Override
	public int count(String code) {
		Criteria criteria = getHibernateSession().createCriteria(SygFamilles.class);
		// criteria.createAlias("state", "state");
		if(code!=null){
			criteria.add(Restrictions.ge("String", code));
		}	
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygFamilles.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygFamilles.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	

	@Override
	public void save(SygFamilles familles) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(familles);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygFamilles familles) {
		
		try{
			getHibernateSession().merge(familles);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}	
	@Override
	public SygFamilles findById(Long code) {
		return (SygFamilles)getHibernateSession().get(SygFamilles.class, code);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<SygFamilles> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygFamilles.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));
		
		return criteria.list();
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<SygFamilles> find(int indice, int pas,String code,String designation,String commentaire,SygCategories categories) {
       Criteria criteria = getHibernateSession().createCriteria(SygFamilles.class);
       //criteria.createAlias("state", "state");
		//criteria.addOrder(Order.desc("datestatut"));
		if(code!=null){
			criteria.add(Restrictions.eq("code", code));
			}
		if(designation!=null){
			criteria.add(Restrictions.eq("designation", designation));
			}
		if(commentaire!=null){
			criteria.add(Restrictions.eq("commentaire", commentaire));
			}
		if(categories!=null){
			criteria.add(Restrictions.eq("categories", categories));
			}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}
	@SuppressWarnings("unchecked")
	@Override
	public List<SygFamilles> findRech(int indice, int pas,String code,String designation,Long categoriesref) {
       Criteria criteria = getHibernateSession().createCriteria(SygFamilles.class);
       criteria.createAlias("categories", "categoriesref" );
		if(code!=null){
			criteria.add(Restrictions.ilike("code","%"+code+"%"));
			}
		
		if(designation!=null){
			criteria.add(Restrictions.ilike("designation", "%"+designation+"%"));
			}
		
		if(categoriesref!=null){
			criteria.add(Restrictions.eq("categoriesref.id", categoriesref));
			}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}
	public int countRech(String code, String designation,Long categoriesref) {
	       Criteria criteria = getHibernateSession().createCriteria(SygFamilles.class);
	       criteria.createAlias("categories", "categoriesref" );
	       if(code!=null){
				criteria.add(Restrictions.ilike("code","%"+code+"%"));
				}
		
			if(designation!=null){
				criteria.add(Restrictions.ilike("designation", "%"+designation+"%"));
				}
			
			if(categoriesref!=null){
				criteria.add(Restrictions.eq("categoriesref.id", categoriesref));
				}
			
				
			criteria.setProjection(Projections.rowCount());
			return Integer.parseInt(criteria.uniqueResult().toString());

			}
	@SuppressWarnings("unchecked")
	@Override
	public List<SygFamilles> findRech1(int indice, int pas,String code, String famillesref) {
       Criteria criteria = getHibernateSession().createCriteria(SygCategories.class);
       //criteria.createAlias("state", "state");
		//criteria.addOrder(Order.desc("datestatut"));
		if(code!=null){
			criteria.add(Restrictions.eq("code", code));
			}
		if(famillesref!=null){
			criteria.add(Restrictions.eq("famillesref", famillesref));
			}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}


@SuppressWarnings("unchecked")
@Override
public List<SygFamilles> find(int indice, int pas) {
   Criteria criteria = getHibernateSession().createCriteria(SygFamilles.class);
	criteria.setFirstResult(indice);
	if(pas>0)
	criteria.setMaxResults(pas);
		
	return criteria.list();
	}


}
