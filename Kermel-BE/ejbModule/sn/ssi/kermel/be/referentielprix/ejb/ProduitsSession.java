package sn.ssi.kermel.be.referentielprix.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygFamilles;
import sn.ssi.kermel.be.entity.SygProduits;





@Remote
public interface ProduitsSession {
	public void save(SygProduits produits);
	public void delete(Long id);
	public List<SygProduits> find(int indice, int pas,String code,String designation,String commentaire,SygFamilles familles);
	public int count(String code);
	public void update(SygProduits produits);
	public SygProduits findById(Long code);
	public List<SygProduits> find(String code);
	List<SygProduits> find(int indice, int pas);
	int count();
	public List<SygProduits> findRech(int indice, int pas,String code,String designation,Long famillesref);
	public int countRech(String code,String designation,Long famillesref);
	
	public List<SygProduits> findRech1(int indice, int pas,String code, String produitsref);
	List<SygProduits> findByFamilly(int indice, int pas, Long famillesref);
	
	
}

