package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygFilesAc;

@Remote
public interface FilesAcSession {

    public SygFilesAc findById(long id);
    public List<SygFilesAc> find(int start, int step);
    public List<SygFilesAc> findByCourrier(int start, int step, Long  courrierId);
    public int count(Long  courrierId);
    public SygFilesAc save(SygFilesAc filesAc);
    public SygFilesAc update(SygFilesAc filesAc);
    public void delete(long id);
    int count();
}
