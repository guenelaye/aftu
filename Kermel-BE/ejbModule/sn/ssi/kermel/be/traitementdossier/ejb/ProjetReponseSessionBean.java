package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygProjeReponse;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class ProjetReponseSessionBean extends AbstractSessionBean implements ProjetReponseSession {

	@Override
	public List<SygProjeReponse> findByDossier(int start, int step, String ref, String dossierCourrier){

		Criteria criteria = getHibernateSession().createCriteria(SygProjeReponse.class);
		criteria.setFirstResult(start);
		criteria.createAlias("dossierCourrier", "dossier");
		//criteria.createAlias("courrierAutoriteContractante", "ac");

		if(ref!=null){
			criteria.add(Restrictions.eq("courrierRef",ref ));
		}

		if(dossierCourrier!=null){
			criteria.add(Restrictions.eq("dossier.code",dossierCourrier ));
		}
		if(step>0){
			criteria.setMaxResults(step);
		}

		return criteria.list();
	}

	@Override
	public Integer count(String ref, String dossierCourrier){
		Criteria criteria = getHibernateSession().createCriteria(SygProjeReponse.class);
		criteria.createAlias("dossierCourrier", "dossier");
		criteria.createAlias("courrierAutoriteContractante", "ac");

		if(ref!=null){
			criteria.add(Restrictions.eq("courrierRef",ref ));
		}

		if(dossierCourrier!=null){
			criteria.add(Restrictions.eq("dossier.code",dossierCourrier ));
		}

		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public SygProjeReponse save(SygProjeReponse projeReponse){

		try {
			getHibernateSession().save(projeReponse);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return projeReponse;
	}

	@Override
	public SygProjeReponse update(SygProjeReponse projeReponse){

		try {
			getHibernateSession().update(projeReponse);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return projeReponse;
	}

	@Override
	public void delete(Long id){
		try {
			getHibernateSession().delete(getHibernateSession().load(SygProjeReponse.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public SygProjeReponse findById(Long id){
		return (SygProjeReponse) getHibernateSession().get(SygProjeReponse.class, id);
	}


	@Override
	public SygProjeReponse findLastVersion(String dossierCourrier){
		Criteria criteria = getHibernateSession().createCriteria(SygProjeReponse.class);
		criteria.createAlias("dossierCourrier", "dossier");
		if(dossierCourrier!=null){
			criteria.add(Restrictions.eq("dossier.code",dossierCourrier ));
		}

		criteria.add(Restrictions.eq("isLastVersion", 0));
		return (SygProjeReponse) criteria.uniqueResult();
	}

	@Override
	public void updateAll() {
		getHibernateSession().createQuery("UPDATE SygProjeReponse set isLastVersion = false").executeUpdate();

	}



}
