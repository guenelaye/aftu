package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygFilesAc;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class FilesAcSessionBean extends AbstractSessionBean implements FilesAcSession {

    @Override
    public SygFilesAc findById(long id) {
	// TODO Auto-generated method stub
	return (SygFilesAc) getHibernateSession().get(SygFilesAc.class, id);
    }

    @Override
    public List<SygFilesAc> find(int start, int step) {
	Criteria criteria= getHibernateSession().createCriteria(SygFilesAc.class);
	criteria.setFirstResult(start);
	if(step>0){
	    criteria.setMaxResults(step); 
	}
	criteria.createAlias("courrier", "courrier");
	return criteria.list();
    }

    @Override
    public int count() {
	Criteria criteria= getHibernateSession().createCriteria(SygFilesAc.class);
	criteria.createAlias("courrier", "courrier");
	criteria.setProjection(Projections.rowCount());
	return Integer.parseInt(criteria.uniqueResult().toString());

    }

    @Override
    public SygFilesAc save(SygFilesAc filesAc) {
	try {
	    getHibernateSession().saveOrUpdate(filesAc);
	    return filesAc;
	} catch (Exception e) {
	    return null;
	}

    }

    @Override
    public SygFilesAc update(SygFilesAc filesAc) {
	try {
	    getHibernateSession().update(filesAc);
	    return filesAc;
	} catch (Exception e) {
	    return null;
	}
    }

    @Override
    public void delete(long id) {
	try {
	    getHibernateSession().delete(getHibernateSession().load(SygFilesAc.class, id));

	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

    @Override
    public List<SygFilesAc> findByCourrier(int start, int step, Long courrierId) {
	Criteria criteria= getHibernateSession().createCriteria(SygFilesAc.class);
	criteria.setFirstResult(start);
	if(step>0){
	    criteria.setMaxResults(step); 
	}
	criteria.createAlias("courrier", "courrier");
	if(courrierId!=null){
	    criteria.add(Restrictions.eq("courrier.courrierId", courrierId));
	}
	return criteria.list();
    }

    @Override
    public int count(Long courrierId) {
	Criteria criteria= getHibernateSession().createCriteria(SygFilesAc.class);
	criteria.createAlias("courrier", "courrier");
	if(courrierId!=null){
	    criteria.add(Restrictions.eq("courrier.courrierId", courrierId));
	}
	criteria.setProjection(Projections.rowCount());
	return Integer.parseInt(criteria.uniqueResult().toString());

    }

}
