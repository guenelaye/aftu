package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygBureauxdcmp;
import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygTDossierCourrier;
import sn.ssi.kermel.be.entity.Utilisateur;

@Remote
public interface DossierAcSession {

	public List<SygDossierCourrier> find(int start, int step, String code, String libelle);
	public Integer count(String code, String libelle);
	public List<SygDossierCourrier> findByType(int start, int step, String typeDos, String status);
	public List<SygDossierCourrier> findByType(int start, int step, String code, String typeDos, String status);
	public Integer countByType(String typeDos, String code);
	public SygDossierCourrier findById(Long id);
	public SygDossierCourrier findByCode(String code);
	public SygDossierCourrier save(SygDossierCourrier dossierCourrier, String state);
	public SygDossierCourrier update(SygDossierCourrier dossierCourrier);
	public void delete(Long id);
	List<Object[]> groupByType(int start, int step, Long userId);
	List<Object[]> groupByStateAndType(int start, int step, String typeDos);
	public void saveTdossier(SygTDossierCourrier tDossierCourrier);
	public List<SygTDossierCourrier> findTdossier(int start, int step, String codeDossier);
	public int countTDossier(String codeDossier);
	int countByServiceAndAgent(SygBureauxdcmp bureau, Utilisateur agent);
	List<SygDossierCourrier> getDossierInMyStates(int start, int step,
			long userId, String typeDos);

}
