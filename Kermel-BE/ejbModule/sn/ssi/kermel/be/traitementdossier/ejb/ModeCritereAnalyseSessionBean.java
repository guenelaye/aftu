package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygCritereAnalyse;
import sn.ssi.kermel.be.entity.SygPieces;
import sn.ssi.kermel.be.entity.SygTypesDossiers;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class ModeCritereAnalyseSessionBean extends AbstractSessionBean implements ModeCritereAnalyseSession {

	@Override
	public int count(String libellecritereanalyse) {
		Criteria criteria = getHibernateSession().createCriteria(SygCritereAnalyse.class);
		if(libellecritereanalyse!=null){
			criteria.add(Restrictions.ge("libellecritereanalyse", libellecritereanalyse));
		}	
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygCritereAnalyse.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	@Override
	public void delete(Long idcritereanalyse) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygCritereAnalyse.class, idcritereanalyse));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	@Override
	public void save(SygCritereAnalyse nature) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(nature);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygCritereAnalyse nature) {
	try{
			getHibernateSession().merge(nature);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public SygCritereAnalyse findById(Long code) {
		return (SygCritereAnalyse)getHibernateSession().get(SygCritereAnalyse.class, code);
	}
	@Override
	public List<SygCritereAnalyse> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCritereAnalyse.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));	
		return criteria.list();
	}
	@Override
	public List<SygCritereAnalyse> find(int indice, int pas,String libellecritereanalyse,String codecritereanalyse,String descriptionanalyse) {
       Criteria criteria = getHibernateSession().createCriteria(SygCritereAnalyse.class);
		if(libellecritereanalyse!=null){
			criteria.add(Restrictions.ge("libellecritereanalyse", libellecritereanalyse));
		}
		if(codecritereanalyse!=null){
			criteria.add(Restrictions.ge("codecritereanalyse", codecritereanalyse));
		}
		if(descriptionanalyse!=null){
			criteria.add(Restrictions.ge("descriptionanalyse", descriptionanalyse));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);	
		return criteria.list();
		}
	@Override
	public List<SygCritereAnalyse> find(int indice, int pas) {
	   Criteria criteria = getHibernateSession().createCriteria(SygCritereAnalyse.class);
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);			
		return criteria.list();
		}
	public List<SygCritereAnalyse> findNotInDossier(int indice, int pas, SygTypesDossiers code, String libelle) {
		// TODO Auto-generated method stub
		org.hibernate.Query query = null;
		String sql = "SELECT analyse FROM  SygCritereAnalyse analyse where analyse "
//		+ " NOT IN ( SELECT piecesrequises.piece from SygPiecesrequises piecesrequises where  piecesrequises.dossiers = " + code + " " + " )";
		+ " NOT IN ( SELECT GrillesAnalyses.analyse from SygGrillesAnalyses GrillesAnalyses where  GrillesAnalyses.dossiers = :param  " +  " )";
		
		if (libelle != null)
		sql += " AND analyse.libelle like '%" + libelle + "%'";
		query = getHibernateSession().createQuery(sql);
	
		query.setFirstResult(indice);
	
		return query.setMaxResults(pas).setParameter("param", code).list();
		}
		@Override
		public Integer countNotInDossier( SygTypesDossiers code, String libelle) {
			// TODO Auto-generated method stub
			org.hibernate.Query query = null;
			String sql = "SELECT count (analyse) FROM  SygCritereAnalyse analyse where  analyse " + 
		 " NOT IN (SELECT GrillesAnalyses.analyse from SygGrillesAnalyses GrillesAnalyses where  GrillesAnalyses.dossiers='" + code + "' " + " )";
		
			if (libelle != null)
			sql += " AND analyse.libelle like '%" + libelle + "%'";
			query = getHibernateSession().createQuery(sql);
			Long val= (Long) query.uniqueResult();
		   return val.intValue();
			}
	}
