package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygDossierPieceRequises;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class DossierPiecesRequisesBean extends AbstractSessionBean implements DossierPiecesRequises {

    @Override
    public SygDossierPieceRequises save(
	    SygDossierPieceRequises dossierPieceRequises) {
	try {
	    getHibernateSession().save(dossierPieceRequises);
	    return dossierPieceRequises;
	} catch (Exception e) {
	    return null;
	}

    }

    @Override
    public SygDossierPieceRequises update(
	    SygDossierPieceRequises dossierPieceRequises) {
	try {
	    getHibernateSession().update(dossierPieceRequises);
	    return dossierPieceRequises;
	} catch (Exception e) {
	    return null;
	}
    }

    @Override
    public List<SygDossierPieceRequises> find(int start, int step) {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public List<SygDossierPieceRequises> findByDossier(int start, int step,
	    SygDossierCourrier dossierCourrier) {
	Criteria criteria = getHibernateSession().createCriteria(SygDossierPieceRequises.class).setFirstResult(start);
	criteria.createAlias("dossierCourrier", "dossierCourrier");
	criteria.createAlias("piecesrequises", "piecesrequises");
	criteria.createAlias("piecesrequises.piece", "piece");
	if(dossierCourrier!=null){
	    criteria.add(Restrictions.eq("dossierCourrier", dossierCourrier));
	}
	if(step>0){
	    criteria.setMaxResults(step);
	}
	//	List<SygPiecesrequises> mylistGrilleana = getHibernateSession().createQuery("SELECT SygPiecesrequises FROM SygDossierPieceRequises WHERE dossierCourrier = :param")
	//		.setParameter("param", dossierCourrier).list();
	return criteria.list();
    }

}
