package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;




import sn.ssi.kermel.be.entity.SygModeTraitement;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class ModeTraitementSessionBean extends AbstractSessionBean implements ModeTraitementSession {

	@Override
	public int count(String libellemodetraitement) {
		Criteria criteria = getHibernateSession().createCriteria(SygModeTraitement.class);
		if(libellemodetraitement!=null){
			criteria.add(Restrictions.ge("libellemodetraitement", libellemodetraitement));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygModeTraitement.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	@Override
	public void delete(Long idmodetraitement) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygModeTraitement.class, idmodetraitement));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	@Override
	public void save(SygModeTraitement traitement) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(traitement);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygModeTraitement traitement) {
	try{
			getHibernateSession().merge(traitement);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public SygModeTraitement findById(Long code) {
		return (SygModeTraitement)getHibernateSession().get(SygModeTraitement.class, code);
	}
	@Override
	public List<SygModeTraitement> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygModeTraitement.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));	
		return criteria.list();
	}
	@Override
	public List<SygModeTraitement> find(int indice, int pas,String libellemodetraitement,String codemodetraitement,String descriptionmodetraitement) {
       Criteria criteria = getHibernateSession().createCriteria(SygModeTraitement.class);
		if(libellemodetraitement!=null){
			criteria.add(Restrictions.ge("libellemodetraitement", libellemodetraitement));
		}
		if(codemodetraitement!=null){
			criteria.add(Restrictions.ge("codemodetraitement", codemodetraitement));
		}
		if(descriptionmodetraitement!=null){
			criteria.add(Restrictions.ge("descriptionmodetraitement", descriptionmodetraitement));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);	
		return criteria.list();
		}
	@Override
	public List<SygModeTraitement> find(int indice, int pas) {
	   Criteria criteria = getHibernateSession().createCriteria(SygModeTraitement.class);
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);			
		return criteria.list();
		}
	}
