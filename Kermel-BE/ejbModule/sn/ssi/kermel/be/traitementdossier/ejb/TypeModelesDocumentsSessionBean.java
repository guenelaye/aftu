package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygModeleDocument;
import sn.ssi.kermel.be.entity.SygPiecesrequises;
import sn.ssi.kermel.be.entity.SygTypeModeleDocument;
import sn.ssi.kermel.be.entity.SygTypesDossiers;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class TypeModelesDocumentsSessionBean extends AbstractSessionBean implements TypeModelesDocumentsSession {

//	@Override
//	public int count(String libellecritereanalyse) {
//		Criteria criteria = getHibernateSession().createCriteria(SygCritereAnalyse.class);
//		if(libellecritereanalyse!=null){
//			criteria.add(Restrictions.ge("String", libellecritereanalyse));
//		}	
//		criteria.setProjection(Projections.rowCount());
//		return Integer.parseInt(criteria.uniqueResult().toString());
//	}
	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygTypeModeleDocument.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygTypeModeleDocument.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	@Override
	public void save(SygTypeModeleDocument  modeledocument) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save( modeledocument);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygTypeModeleDocument modeledocument) {
	try{
			getHibernateSession().merge(modeledocument);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public SygTypeModeleDocument findById(Long code) {
		return (SygTypeModeleDocument)getHibernateSession().get(SygTypeModeleDocument.class, code);
	}
	@Override
	public List<SygTypeModeleDocument> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTypeModeleDocument.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));	
		return criteria.list();
	}
	@Override
	public List<SygTypeModeleDocument> find(int indice, int pas,SygModeleDocument document,SygTypesDossiers dossiers) {
       Criteria criteria = getHibernateSession().createCriteria(SygTypeModeleDocument.class);
		if(document!=null){
			criteria.add(Restrictions.ge("document", document));
		}
		if(dossiers!=null){
			criteria.add(Restrictions.ge("dossiers", dossiers));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);	
		return criteria.list();
		}
	@Override
	public List<SygTypeModeleDocument> find(int indice, int pas) {
	   Criteria criteria = getHibernateSession().createCriteria(SygTypeModeleDocument.class);
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);			
		return criteria.list();
		}
	@Override
	public List<SygTypeModeleDocument> find1(int indice, int pas, String code, String libelleDoc) {
	Criteria criteria = getHibernateSession().createCriteria(SygTypeModeleDocument.class);	
	criteria.createAlias("dossiers", "dossiers");
//    criteria.createAlias("document", "document");
	if(code!=null){
		criteria.add(Restrictions.ge("dossiers.code", code));
	}
	if(libelleDoc!=null){
		criteria.add(Restrictions.ilike("document.libelle", libelleDoc,MatchMode.ANYWHERE));
	}
	criteria.setFirstResult(indice);
	if(pas>0)
	criteria.setMaxResults(pas);	
	return criteria.list();
	}
	@Override
	public int count(String code, String libelleDoc) {
		// TODO Auto-generated method stub
		 Criteria criteria = getHibernateSession().createCriteria(SygPiecesrequises.class);
		 criteria.createAlias("dossiers", "dossiers"); 
//	    criteria.createAlias("document", "document");
		if(code!=null){
			criteria.add(Restrictions.ge("dossiers.code", code));
		}
		if(libelleDoc!=null){
			criteria.add(Restrictions.ilike("document.libelle", libelleDoc,MatchMode.ANYWHERE));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());  
	}
	}
