package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygDossierPiecesJointes;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class DossierPiecesJointesSessionBean extends AbstractSessionBean implements DossierPiecesJointesSession {

    @Override
    public SygDossierPiecesJointes findById(Long id) {
	Criteria criteria = getHibernateSession().createCriteria(SygDossierPiecesJointes.class);
	criteria.createAlias("dossierCourrier", "dossierCourrier");
	criteria.add(Restrictions.eq("id", id));
	return (SygDossierPiecesJointes) criteria.uniqueResult();
    }

    @Override
    public List<SygDossierPiecesJointes> findByDossier(
	    SygDossierCourrier dossierCourrier) {
	Criteria criteria = getHibernateSession().createCriteria(SygDossierPiecesJointes.class);
	criteria.createAlias("dossierCourrier", "dossierCourrier");
	if(dossierCourrier!=null){
	    criteria.add(Restrictions.eq("dossierCourrier", dossierCourrier));
	}
	return criteria.list();
    }

    @Override
    public SygDossierPiecesJointes save(
	    SygDossierPiecesJointes dossierPiecesJointes) {
	try {
	    getHibernateSession().saveOrUpdate(dossierPiecesJointes);
	    return dossierPiecesJointes;
	} catch (Exception e) {
	    return null;
	}

    }

    @Override
    public SygDossierPiecesJointes update(
	    SygDossierPiecesJointes dossierPiecesJointes) {
	try {
	    getHibernateSession().update(dossierPiecesJointes);
	    return dossierPiecesJointes;
	} catch (Exception e) {
	    return null;
	}
    }

    @Override
    public void delete(Long id) {
	try {
	    getHibernateSession().delete(getHibernateSession().load(SygDossierPiecesJointes.class, id));
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

}
