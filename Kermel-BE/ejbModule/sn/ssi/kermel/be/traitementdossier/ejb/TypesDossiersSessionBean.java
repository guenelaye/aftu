package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygCritereAnalyse;
import sn.ssi.kermel.be.entity.SygTypesDossiers;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class TypesDossiersSessionBean extends AbstractSessionBean implements TypesDossiersSession {

	@Override
	public int count(String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygTypesDossiers.class);
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", libelle,MatchMode.ANYWHERE));
		}	
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygTypesDossiers.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	@Override
	public void delete(Long Id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygTypesDossiers.class, Id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	@Override
	public void save(SygTypesDossiers nature) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(nature);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygTypesDossiers nature) {
		try{
			getHibernateSession().merge(nature);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public SygTypesDossiers findById(Long code) {
		return (SygTypesDossiers)getHibernateSession().get(SygTypesDossiers.class, code);
	}
	@Override
	public List<SygTypesDossiers> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCritereAnalyse.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));	
		return criteria.list();
	}
	@Override
	public List<SygTypesDossiers> find(int indice, int pas,String code,String libelle,Integer delaitraitement,String description) {
       Criteria criteria = getHibernateSession().createCriteria(SygTypesDossiers.class);
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", libelle,MatchMode.ANYWHERE));
		}
		if(code!=null){
			criteria.add(Restrictions.ilike("code", code,MatchMode.ANYWHERE));
		}
		if(delaitraitement!=null){
			criteria.add(Restrictions.eq("delaitraitement", delaitraitement));
		}
		if(description!=null){
			criteria.add(Restrictions.ilike("description", description,MatchMode.ANYWHERE));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);	
		return criteria.list();
		}
	@Override
	public List<SygTypesDossiers> find(int indice, int pas) {
	   Criteria criteria = getHibernateSession().createCriteria(SygCritereAnalyse.class);
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);			
		return criteria.list();
		}
	}
