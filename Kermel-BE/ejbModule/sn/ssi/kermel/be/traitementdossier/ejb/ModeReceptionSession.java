package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygModeReception;



@Remote
public interface ModeReceptionSession {
	public void save(SygModeReception reception);
	public void delete(Long idmodereception);
	public List<SygModeReception> find(int indice, int pas,String libellemodereception,String codemodereception,String descriptionmodereception);
	public int count(String libellemodereception);
	public void update(SygModeReception reception);
	public SygModeReception findById(Long code);
	public List<SygModeReception> find(String code);
	List<SygModeReception> find(int indice, int pas);
	int count();
	
}
