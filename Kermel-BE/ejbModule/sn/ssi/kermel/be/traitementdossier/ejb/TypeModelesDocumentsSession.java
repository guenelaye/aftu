package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;


import sn.ssi.kermel.be.entity.SygModeleDocument;
import sn.ssi.kermel.be.entity.SygTypeModeleDocument;
import sn.ssi.kermel.be.entity.SygTypesDossiers;


@Remote
public interface TypeModelesDocumentsSession {
	public void save(SygTypeModeleDocument modeledocument);
	public void delete(Long id);
	public List<SygTypeModeleDocument> find(int indice, int pas,SygModeleDocument document,SygTypesDossiers dossiers);
	//public int count(String libellecritereanalyse);
	public void update(SygTypeModeleDocument modeledocument);
	public List<SygTypeModeleDocument> find1(int indice, int pas,String code, String libelleDoc);
	public int count(String code, String libelleDoc);
	public SygTypeModeleDocument findById(Long code);
	public List<SygTypeModeleDocument> find(String code);
	List<SygTypeModeleDocument> find(int indice, int pas);
	int count();
	
}
