package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygPieces;
import sn.ssi.kermel.be.entity.SygPiecesrequises;
import sn.ssi.kermel.be.entity.SygTypesDossiers;




@Remote
public interface PiecesRequisesSession {
	public void save(SygPiecesrequises requises);
	public void delete(Long idpiecesrequises);
	public List<SygPiecesrequises> find(int indice, int pas,SygPieces piece,SygTypesDossiers dossiers);
	//public int count(String libellecritereanalyse);
	public void update(SygPiecesrequises requises);
	public SygPiecesrequises findById(Long code);
	public List<SygPiecesrequises> find1(int indice, int pas,String code,String libellePiece);
	public int count(String code,String libellePiece);
	public List<SygPiecesrequises> find(String code);
	List<SygPiecesrequises> find(int indice, int pas);
	int count();

	
}
