package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygProjeReponse;

@Remote
public interface ProjetReponseSession {

	List<SygProjeReponse> findByDossier(int start, int step, String ref,
			String dossierCourrier);

	Integer count(String ref, String dossierCourrier);

	SygProjeReponse save(SygProjeReponse projeReponse);

	SygProjeReponse update(SygProjeReponse projeReponse);

	void delete(Long id);

	SygProjeReponse findById(Long id);
	public void updateAll();



	SygProjeReponse findLastVersion(String dossierCourrier);

}
