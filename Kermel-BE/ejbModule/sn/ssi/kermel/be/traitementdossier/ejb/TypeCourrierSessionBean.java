package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;


import sn.ssi.kermel.be.entity.SygTypesCourrier;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class TypeCourrierSessionBean extends AbstractSessionBean implements TypeCourrierSession {

	@Override
	public int count(String libelletypecourrier) {
		Criteria criteria = getHibernateSession().createCriteria(SygTypesCourrier.class);
		if(libelletypecourrier!=null){
			criteria.add(Restrictions.ilike("libelletypecourrier", "%" +  libelletypecourrier + "%" ));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygTypesCourrier.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	@Override
	public void delete(Long idtypecourrier) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygTypesCourrier.class, idtypecourrier));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	@Override
	public void save(SygTypesCourrier courrier) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(courrier);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygTypesCourrier courrier) {
	try{
			getHibernateSession().merge(courrier);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public SygTypesCourrier findById(Long code) {
		return (SygTypesCourrier)getHibernateSession().get(SygTypesCourrier.class, code);
	}
	@Override
	public List<SygTypesCourrier> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTypesCourrier.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));	
		return criteria.list();
	}
	@Override
	public List<SygTypesCourrier> find(int indice, int pas,String libelletypecourrier,String codetypecourrier,String descriptiontypecourrier) {
       Criteria criteria = getHibernateSession().createCriteria(SygTypesCourrier.class);
		if(libelletypecourrier!=null){
			criteria.add(Restrictions.ilike("libelletypecourrier", libelletypecourrier, MatchMode.ANYWHERE));
		}
		if(codetypecourrier!=null){
			criteria.add(Restrictions.ilike("codetypecourrier", codetypecourrier, MatchMode.ANYWHERE));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);	
		return criteria.list();
		}
	@Override
	public List<SygTypesCourrier> find(int indice, int pas) {
	   Criteria criteria = getHibernateSession().createCriteria(SygTypesCourrier.class);
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);			
		return criteria.list();
		}
	}
