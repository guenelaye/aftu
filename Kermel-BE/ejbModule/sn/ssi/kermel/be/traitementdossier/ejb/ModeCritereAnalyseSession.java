package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCritereAnalyse;
import sn.ssi.kermel.be.entity.SygPieces;
import sn.ssi.kermel.be.entity.SygTypesDossiers;


@Remote
public interface ModeCritereAnalyseSession {
	public void save(SygCritereAnalyse analyse);
	public void delete(Long idcritereanalyse);
	public List<SygCritereAnalyse> find(int indice, int pas,String libellecritereanalyse,String codecritereanalyse,String descriptionanalyse);
	public int count(String libellecritereanalyse);
	public void update(SygCritereAnalyse analyse);
	public SygCritereAnalyse findById(Long code);
	public List<SygCritereAnalyse> find(String code);
	List<SygCritereAnalyse> find(int indice, int pas);
	int count();
	public List<SygCritereAnalyse> findNotInDossier(int indice, int pas, SygTypesDossiers code, String libelle);
	Integer countNotInDossier(SygTypesDossiers code, String libelle);
	
}
