package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygPieces;
import sn.ssi.kermel.be.entity.SygPiecesrequises;
import sn.ssi.kermel.be.entity.SygTypesDossiers;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class PiecesRequisesSessionBean extends AbstractSessionBean implements PiecesRequisesSession {

//	@Override
//	public int count(String libellecritereanalyse) {
//		Criteria criteria = getHibernateSession().createCriteria(SygCritereAnalyse.class);
//		if(libellecritereanalyse!=null){
//			criteria.add(Restrictions.ge("String", libellecritereanalyse));
//		}	
//		criteria.setProjection(Projections.rowCount());
//		return Integer.parseInt(criteria.uniqueResult().toString());
//	}
	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygPiecesrequises.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	@Override
	public void delete(Long idpiecesrequises) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygPiecesrequises.class, idpiecesrequises));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	@Override
	public void save(SygPiecesrequises requises) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(requises);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygPiecesrequises requises) {
	try{
			getHibernateSession().merge(requises);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public SygPiecesrequises findById(Long code) {
		return (SygPiecesrequises)getHibernateSession().get(SygPiecesrequises.class, code);
	}
	@Override
	public List<SygPiecesrequises> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPiecesrequises.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));	
		return criteria.list();
	}
	@Override
	public List<SygPiecesrequises> find(int indice, int pas,SygPieces piece,SygTypesDossiers dossiers) {
       Criteria criteria = getHibernateSession().createCriteria(SygPiecesrequises.class);
		if(piece!=null){
			criteria.add(Restrictions.ge("piece", piece));
		}
		if(dossiers!=null){
			criteria.add(Restrictions.ge("dossiers", dossiers));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);	
		return criteria.list();
		}
	
	@Override
	public List<SygPiecesrequises> find1(int indice, int pas,String code,String libellePiece) {
       Criteria criteria = getHibernateSession().createCriteria(SygPiecesrequises.class);
       criteria.createAlias("dossiers", "dossiers");
       criteria.createAlias("piece", "piece");
		if(code!=null){
			criteria.add(Restrictions.ilike("dossiers.code", code,MatchMode.ANYWHERE));
		}
		if(libellePiece!=null){
			criteria.add(Restrictions.ilike("piece.libelle", libellePiece,MatchMode.ANYWHERE));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);	
		return criteria.list();
		}
	@Override
	public List<SygPiecesrequises> find(int indice, int pas) {
	   Criteria criteria = getHibernateSession().createCriteria(SygPiecesrequises.class);
	   criteria.createAlias("dossiers", "dossiers"); 
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);			
		return criteria.list();
		}
	@Override
	public int count(String code,String libellePiece) {
		 Criteria criteria = getHibernateSession().createCriteria(SygPiecesrequises.class);
		 criteria.createAlias("dossiers", "dossiers"); 
	       criteria.createAlias("piece", "piece");
			if(code!=null){
				criteria.add(Restrictions.ilike("dossiers.code", code,MatchMode.ANYWHERE));
			}
			if(libellePiece!=null){
				criteria.add(Restrictions.ilike("piece.libelle", libellePiece,MatchMode.ANYWHERE));
			}
			
			criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());    
	}
	}
