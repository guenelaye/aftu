package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygTypesCourrier;

@Remote
public interface TypeCourrierSession {
	public void save(SygTypesCourrier courrier);
	public void delete(Long idtypecourrier);
	public List<SygTypesCourrier> find(int indice, int pas,String libelletypecourrier,String codetypecourrier,String descriptiontypecourrier);
	public int count(String libelletypecourrier);
	public void update(SygTypesCourrier courrier);
	public SygTypesCourrier findById(Long code);
	public List<SygTypesCourrier> find(String code);
	List<SygTypesCourrier> find(int indice, int pas);
	int count();
	
}
