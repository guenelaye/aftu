package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygModeleDocument;
import sn.ssi.kermel.be.entity.SygPieces;
import sn.ssi.kermel.be.entity.SygTypesDossiers;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class ModeleDocumentSessionBean extends AbstractSessionBean implements ModeleDocumentSession {

	@Override
	public int count(String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygModeleDocument.class);
		if(libelle!=null){
			criteria.add(Restrictions.ge("libelle", libelle));
		}	
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygModeleDocument.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	@Override
	public void delete(Long Id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygModeleDocument.class, Id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	@Override
	public void save(SygModeleDocument document) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(document);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygModeleDocument document) {
	try{
			getHibernateSession().merge(document);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public SygModeleDocument findById(Long code) {
		return (SygModeleDocument)getHibernateSession().get(SygModeleDocument.class, code);
	}
	@Override
	public List<SygModeleDocument> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygModeleDocument.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));	
		return criteria.list();
	}
	@Override
	public List<SygModeleDocument> find(int indice, int pas,String libelle,String code,String description,String fichier) {
       Criteria criteria = getHibernateSession().createCriteria(SygModeleDocument.class);
		if(libelle!=null){
			criteria.add(Restrictions.ge("String", libelle));
		}
		if(code!=null){
			criteria.add(Restrictions.ge("code", code));
		}
		if(description!=null){
			criteria.add(Restrictions.ge("description", description));
		}
		if(fichier!=null){
			criteria.add(Restrictions.ge("fichier", fichier));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);	
		return criteria.list();
		}
	@Override
	public List<SygModeleDocument> find(int indice, int pas) {
	   Criteria criteria = getHibernateSession().createCriteria(SygModeleDocument.class);
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);			
		return criteria.list();
		}
	public List<SygModeleDocument> findNotInDossier(int indice, int pas, SygTypesDossiers code, String libelle) {
		// TODO Auto-generated method stub
		org.hibernate.Query query = null;
		String sql = "SELECT document FROM  SygModeleDocument document where document "
//		+ " NOT IN ( SELECT piecesrequises.piece from SygPiecesrequises piecesrequises where  piecesrequises.dossiers = " + code + " " + " )";
		+ " NOT IN ( SELECT modeledocument.document from SygTypeModeleDocument modeledocument where  modeledocument.dossiers = :param  " +  " )";
		
		if (libelle != null)
		sql += " AND document.libelle like '%" + libelle + "%'";
		query = getHibernateSession().createQuery(sql);
	
		query.setFirstResult(indice);
	
		return query.setMaxResults(pas).setParameter("param", code).list();
		}
		@Override
		public Integer countNotInDossier( SygTypesDossiers code, String libelle) {
			// TODO Auto-generated method stub
			org.hibernate.Query query = null;
			String sql = "SELECT count (document) FROM  SygModeleDocument document where  document " + 
		 " NOT IN (SELECT modeledocument.document from SygTypeModeleDocument modeledocument where  modeledocument.dossiers='" + code + "' " + " )";
		
			if (libelle != null)
			sql += " AND document.libelle like '%" + libelle + "%'";
			query = getHibernateSession().createQuery(sql);
			Long val= (Long) query.uniqueResult();
		   return val.intValue();
			}
	}
