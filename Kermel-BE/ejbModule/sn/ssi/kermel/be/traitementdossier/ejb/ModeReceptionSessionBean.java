package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygModeReception;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class ModeReceptionSessionBean extends AbstractSessionBean implements ModeReceptionSession {

	@Override
	public int count(String libellemodereception) {
		Criteria criteria = getHibernateSession().createCriteria(SygModeReception.class);
		if(libellemodereception!=null){
			criteria.add(Restrictions.ge("libellemodereception", libellemodereception));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygModeReception.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	@Override
	public void delete(Long idmodereception) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygModeReception.class, idmodereception));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	@Override
	public void save(SygModeReception reception) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(reception);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygModeReception reception) {
	try{
			getHibernateSession().merge(reception);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public SygModeReception findById(Long code) {
		return (SygModeReception)getHibernateSession().get(SygModeReception.class, code);
	}
	@Override
	public List<SygModeReception> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygModeReception.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));	
		return criteria.list();
	}
	@Override
	public List<SygModeReception> find(int indice, int pas,String libellemodereception,String codemodereception,String descriptionmodereception) {
       Criteria criteria = getHibernateSession().createCriteria(SygModeReception.class);
		if(libellemodereception!=null){
			criteria.add(Restrictions.ge("libellemodereception", libellemodereception));
		}
		if(codemodereception!=null){
			criteria.add(Restrictions.ge("codemodereception", codemodereception));
		}
		if(descriptionmodereception!=null){
			criteria.add(Restrictions.ge("descriptionmodereception", descriptionmodereception));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);	
		return criteria.list();
		}
	@Override
	public List<SygModeReception> find(int indice, int pas) {
	   Criteria criteria = getHibernateSession().createCriteria(SygModeReception.class);
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);			
		return criteria.list();
		}
	}
