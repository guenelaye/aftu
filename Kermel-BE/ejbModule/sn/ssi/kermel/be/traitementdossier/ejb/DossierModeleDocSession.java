package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygDossierModeleDoc;

@Remote
public interface DossierModeleDocSession {
    public SygDossierModeleDoc save(SygDossierModeleDoc modeleDoc);
    public SygDossierModeleDoc update(SygDossierModeleDoc modeleDoc);
    public List<SygDossierModeleDoc> findByDossier(SygDossierCourrier dossierCourrier);

}
