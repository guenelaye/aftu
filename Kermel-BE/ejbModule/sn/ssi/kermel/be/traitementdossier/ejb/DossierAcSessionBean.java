package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygBureauxdcmp;
import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygTDossierCourrier;
import sn.ssi.kermel.be.entity.SysParametresGeneraux;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.be.referentiel.ejb.ParametresGenerauxSession;
import sn.ssi.kermel.be.workflow.entity.SysStateperm;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class DossierAcSessionBean extends AbstractSessionBean implements DossierAcSession {

	@EJB
	ParametresGenerauxSession ParametresGenerauxSession;
	@Override
	public List<SygDossierCourrier> find(int start, int step, String code,
			String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossierCourrier.class);
		criteria.setFirstResult(start);
		criteria.createAlias("typesDossiers", "type");
		if(step>0) {
			criteria.setMaxResults(step);
		}

		if(code!=null){
			criteria.add(Restrictions.eq("code", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.eq("libelle", libelle));
		}
		return criteria.list();
	}

	@Override
	public Integer count(String code, String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossierCourrier.class);
		criteria.createAlias("typesDossiers", "type");

		if(code!=null){
			criteria.add(Restrictions.eq("code", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.eq("libelle", libelle));
		}
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public List<SygDossierCourrier> findByType(int start, int step, String code,
			String typeDos,String status) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossierCourrier.class);
		criteria.setFirstResult(start);
		criteria.createAlias("typesDossiers", "type");
		if(step>0) {
			criteria.setMaxResults(step);
		}
		if(code!=null){
			criteria.add(Restrictions.ilike("code", "%" + code + "%"));
		}
		if(typeDos!=null){
			criteria.add(Restrictions.eq("type.code", typeDos));
		}
		if(status!=null){
			criteria.add(Restrictions.eq("status", status));
		}
		return criteria.list();
	}

	@Override
	public List<SygDossierCourrier> findByType(int start, int step,
			String typeDos,String status) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossierCourrier.class);
		criteria.setFirstResult(start);
		criteria.createAlias("typesDossiers", "type");
		if(step>0) {
			criteria.setMaxResults(step);
		}
		if(typeDos!=null){
			criteria.add(Restrictions.eq("type.code", typeDos));
		}
		if(status!=null){
			criteria.add(Restrictions.eq("status", status));
		}
		return criteria.list();
	}

	@Override
	public List<Object[]> groupByType(int start, int step, Long userId){
		Criteria criteria = getHibernateSession().createCriteria(SygDossierCourrier.class);
		criteria.setFirstResult(start);
		criteria.createAlias("typesDossiers", "type");

		if(step>0) {
			criteria.setMaxResults(step);
		}

		if(userId!=null) {
			criteria.add(Restrictions.eq("userId", userId));
		}
		criteria.setProjection(Projections.projectionList().add(Projections.rowCount(), "NBRDOSSIER"
				).add(Projections.property("type.libelle"),"LIBELLE")
				.add(Projections.property("type.code"),"CODE")
				.add(Projections.property("type.id"),"ID")
				.add(Projections.groupProperty("type.id")));

		//criteria.setResultTransformer(new AliasToBeanResultTransformer(SygTypesDossiers.class));

		return criteria.list();
	}

	@Override
	public List<Object[]> groupByStateAndType(int start, int step, String typeDos){
		Criteria criteria = getHibernateSession().createCriteria(SygDossierCourrier.class);
		criteria.setFirstResult(start);
		criteria.createAlias("typesDossiers", "type");

		if(step>0) {
			criteria.setMaxResults(step);
		}

		if(typeDos!=null){
			criteria.add(Restrictions.eq("type.code", typeDos));
		}
		criteria.setProjection(Projections.projectionList().add(Projections.rowCount(), "NBRDOSSIER"
				).add(Projections.property("status"),"LIBELLE")
				.add(Projections.property("type.code"),"CODE")
				.add(Projections.property("type.id"),"ID")
				.add(Projections.groupProperty("type.id"))
				.add(Projections.groupProperty("status"), "status"));

		//criteria.setResultTransformer(new AliasToBeanResultTransformer(SygTypesDossiers.class));

		return criteria.list();
	}
	@Override
	public Integer countByType(String typeDos, String code) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossierCourrier.class);
		criteria.createAlias("typesDossiers", "type");
		if(typeDos!=null){
			criteria.add(Restrictions.eq("type.code", typeDos));
		}

		if(code!=null){
			criteria.add(Restrictions.ilike("code", "%" + code + "%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public SygDossierCourrier findById(Long id) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossierCourrier.class);
		criteria.createAlias("typesDossiers", "type");
		criteria.createAlias("state", "state");
		criteria.add(Restrictions.eq("id", id));

		return (SygDossierCourrier) criteria.uniqueResult();
	}

	@Override
	public SygDossierCourrier findByCode(String code) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossierCourrier.class);
		criteria.createAlias("typesDossiers", "type");
		criteria.createAlias("state", "state");
		criteria.add(Restrictions.eq("code", code));

		return (SygDossierCourrier) criteria.uniqueResult();
	}

	@Override
	public SygDossierCourrier save(SygDossierCourrier dossierCourrier, String state) {
		try {

			Integer increment = countByType(dossierCourrier.getTypesDossiers().getCode(),null);
			SysParametresGeneraux parametreGeneral = ParametresGenerauxSession.findById(BeConstants.PARAM_NUMDOSC);
			if(increment==null) {
				increment = 0;
			}
			if(dossierCourrier.getCode()==null){
				dossierCourrier.setCode(getGeneratedCode(BeConstants.PARAM_NUMDOSC, dossierCourrier.getTypesDossiers().getCode(),ToolKermel.getAnneeCourante(),increment+1 ));
			}

			dossierCourrier.setStatus("SAISIDOSSIER_STATE");
			parametreGeneral.setValeur(increment+1);
			getHibernateSession().merge(parametreGeneral);
			getHibernateSession().save(dossierCourrier);

			return dossierCourrier;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public SygDossierCourrier update(SygDossierCourrier dossierCourrier) {
		try {
			getHibernateSession().merge(dossierCourrier);
			return dossierCourrier;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void delete(Long id) {
		try {
			getHibernateSession().delete(getHibernateSession().load(SygDossierCourrier.class, id));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getGeneratedCode(String codeParametreGeneral,String typeDos,String annee, Integer increment) {
		// TODO Auto-generated method stub


		String dateCoupee = Calendar.getInstance().getTime().toString();
		String mois = String.format("%02d", Calendar.getInstance().get(Calendar.MONTH)+1);
		dateCoupee = dateCoupee.substring(dateCoupee.length() - 4, dateCoupee
				.length());

		if (increment < 10) {
			return "D"+"_"+typeDos+"_"+annee +"_"+ "000" + increment;
		} else if (increment >= 10 && increment < 100) {
			return "D"+"_"+typeDos+"_"+annee+"_" + "00" + increment;
		} else if (increment >= 100 && increment < 1000) {
			return "D"+"_"+typeDos+"_"+annee+"_" + "0" + increment;
		} else {
			return "D"+"_"+typeDos+"_"+annee+"_" + "" + increment;
		}
	}

	@Override
	public void saveTdossier(SygTDossierCourrier tDossierCourrier) {
		try {
			getHibernateSession().save(tDossierCourrier);

		} catch (Exception e) {
			e.printStackTrace();

		}

	}


	public String getNextState(String state){
		String result = "ENCOURS";
		if(state!=null &&state.equals("ENCOURS")){
			result = "TRAITE";
		}else if(state!=null &&state.equals("TRAITE")){
			result = "TRANSMIS";
		}else if(state!=null &&state.equals("TRANSMIS")){
			result = "RETOURNE";
		}else if(state!=null &&state.equals("RETOURNE")){
			result = "CLOTURE";
		}else if(state!=null &&state.equals("CLOTURE")){
			result = "ENCOURS";
		}
		return result;

	}

	@Override
	public List<SygTDossierCourrier> findTdossier(int start, int step,
			String codeDossier) {

		Criteria criteria = getHibernateSession().createCriteria(SygTDossierCourrier.class);
		criteria.setFirstResult(start);
		criteria.createAlias("courrier", "courrier");
		criteria.createAlias("expediteur", "user");
		if(step>0) {
			criteria.setMaxResults(step);
		}

		if(codeDossier!=null){
			criteria.add(Restrictions.eq("courrier.code", codeDossier));
		}

		return criteria.list();
	}

	@Override
	public int countTDossier(String codeDossier) {
		Criteria criteria = getHibernateSession().createCriteria(SygTDossierCourrier.class);
		criteria.createAlias("courrier", "courrier");
		criteria.createAlias("expediteur", "user");


		if(codeDossier!=null){
			criteria.add(Restrictions.eq("courrier.code", codeDossier));
		}

		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public int countByServiceAndAgent(SygBureauxdcmp bureau, Utilisateur agent) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTDossierCourrier.class);	
		criteria.createAlias("expediteur", "expediteur");
		criteria.createAlias("bureau", "bureau");
		criteria.createAlias("courrier", "courrier");


		if(bureau!=null){
			criteria.add(Restrictions.eq("bureau", bureau));
		}

		if(agent!=null){
			criteria.add(Restrictions.eq("expediteur", agent));
		}

		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public List<SygDossierCourrier> getDossierInMyStates(int start, int step, long userId, String typeDos) {

		DetachedCriteria statesperm = DetachedCriteria.forClass(SysStateperm.class, "spm");
		statesperm.createAlias("spm.sysState", "state");
		statesperm.createAlias("spm.sysProfil", "profil");
		statesperm.createAlias("profil.utilisateurs", "users").add(Restrictions.eq("users.id", userId));
		statesperm.add(Restrictions.eq("spm.canview", (short) 1));
		statesperm.setProjection(Projections.property("state.code"));

		Criteria criteria = getHibernateSession().createCriteria(SygDossierCourrier.class).setFirstResult(start);
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		criteria.createAlias("typesDossiers", "type");
		criteria.createAlias("state", "state");
		// criteria.setFetchMode("state.sysStateperms",
		// FetchMode.JOIN).createAlias("state.sysStateperms", "spms");
//		criteria.createAlias("state.sysArrowperms", "apm");
//		criteria.createAlias("apm.sysProfilarrowperms", "pap").add(
//				Restrictions.or(Restrictions.eq("pap.allow", (short) 1), Restrictions.in("state.code", statesperm.getExecutableCriteria(getHibernateSession()).list().toArray())));
//		criteria.createAlias("pap.sysProfil", "prf");
//		criteria.createAlias("prf.utilisateurs", "user").add(Restrictions.eq("user.id", userId));

		if(typeDos!=null){
			criteria.add(Restrictions.eq("type.code", typeDos));
		}
		criteria.addOrder(Order.desc("code"));
		if (step > 0) {
			criteria.setMaxResults(step);
		}
		return criteria.list();
	}
}
