package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygModeTraitement;


@Remote
public interface ModeTraitementSession {
	public void save(SygModeTraitement traitement);
	public void delete(Long idmodetraitement);
	public List<SygModeTraitement > find(int indice, int pas,String libellemodetraitement,String codemodetraitement,String descriptionmodetraitement);
	public int count(String libellemodetraitement);
	public void update(SygModeTraitement  traitement);
	public SygModeTraitement  findById(Long code);
	public List<SygModeTraitement > find(String code);
	List<SygModeTraitement > find(int indice, int pas);
	int count();
	
}
