package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygDossierGrilleAnalyse;
import sn.ssi.kermel.be.entity.SygGrillesAnalyses;

@Remote
public interface DossierGrilleAnalyseSession {

    public SygDossierGrilleAnalyse save (SygDossierGrilleAnalyse dossierGrilleAnalyse);
    public SygDossierGrilleAnalyse update(SygDossierGrilleAnalyse dossierGrilleAnalyse);  
    public List<SygDossierGrilleAnalyse> findByDossier(SygDossierCourrier dossierCourrier);
    public List<SygGrillesAnalyses> find(SygDossierCourrier dossierCourrier);

}
