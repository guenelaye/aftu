package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygDossierGrilleAnalyse;
import sn.ssi.kermel.be.entity.SygGrillesAnalyses;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class DossierGrilleAnalyseSessionBean extends AbstractSessionBean implements DossierGrilleAnalyseSession {

    @Override
    public SygDossierGrilleAnalyse save(
	    SygDossierGrilleAnalyse dossierGrilleAnalyse) {
	try {
	    getHibernateSession().save(dossierGrilleAnalyse);
	    return dossierGrilleAnalyse;
	} catch (Exception e) {
	    return null;
	}

    }

    @Override
    public SygDossierGrilleAnalyse update(
	    SygDossierGrilleAnalyse dossierGrilleAnalyse) {
	try {
	    getHibernateSession().update(dossierGrilleAnalyse);
	    return dossierGrilleAnalyse;
	} catch (Exception e) {
	    return null;
	}
    }

    @Override
    public List<SygDossierGrilleAnalyse> findByDossier(
	    SygDossierCourrier dossierCourrier) {
	Criteria criteria = getHibernateSession().createCriteria(SygDossierGrilleAnalyse.class);
	criteria.createAlias("dossierCourrier", "dossierCourrier");
	criteria.createAlias("grillesAnalyse", "grillesAnalyse");
	//criteria.createAlias("grillesAnalyse.analyses", "analyses");
	if(dossierCourrier!=null){
	    criteria.add(Restrictions.eq("dossierCourrier", dossierCourrier));
	}
	return criteria.list();
    }

    @Override
    public List<SygGrillesAnalyses> find(SygDossierCourrier dossierCourrier) {
	List<SygGrillesAnalyses> mylistGrilleana = getHibernateSession().createQuery("SELECT grillesAnalyse FROM SygDossierGrilleAnalyse WHERE dossierCourrier = :param")
		.setParameter("param", dossierCourrier).list();
	return mylistGrilleana;

    }

}
