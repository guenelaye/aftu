package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygNatureCourrier;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class NatureCourrierSessionBean extends AbstractSessionBean implements NatureCourrierSession {

	@Override
	public int count(String libellenaturecourrier) {
		Criteria criteria = getHibernateSession().createCriteria(SygNatureCourrier.class);
		if(libellenaturecourrier!=null){
			criteria.add(Restrictions.ilike("libellenaturecourrier", libellenaturecourrier,MatchMode.ANYWHERE));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygNatureCourrier.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	@Override
	public void delete(Long idnaturecourrier) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygNatureCourrier.class, idnaturecourrier));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	@Override
	public void save(SygNatureCourrier courrier) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(courrier);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygNatureCourrier courrier) {
	try{
			getHibernateSession().merge(courrier);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public SygNatureCourrier findById(Long code) {
		return (SygNatureCourrier)getHibernateSession().get(SygNatureCourrier.class, code);
	}
	@Override
	public List<SygNatureCourrier> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygNatureCourrier.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));	
		return criteria.list();
	}
	@Override
	public List<SygNatureCourrier> find(int indice, int pas,String libellenaturecourrier,String codenaturecourrier,String descriptionnaturecourrier) {
       Criteria criteria = getHibernateSession().createCriteria(SygNatureCourrier.class);
		if(libellenaturecourrier!=null){
			criteria.add(Restrictions.ilike("libellenaturecourrier", libellenaturecourrier,MatchMode.ANYWHERE));
		}
		if(codenaturecourrier!=null){
			criteria.add(Restrictions.ilike("codenaturecourrier", codenaturecourrier,MatchMode.ANYWHERE));
		}
		if(descriptionnaturecourrier!=null){
			criteria.add(Restrictions.ilike("descriptionnaturecourrier", descriptionnaturecourrier,MatchMode.ANYWHERE));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);	
		return criteria.list();
		}
	@Override
	public List<SygNatureCourrier> find(int indice, int pas) {
	   Criteria criteria = getHibernateSession().createCriteria(SygNatureCourrier.class);
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);			
		return criteria.list();
		}
	}
