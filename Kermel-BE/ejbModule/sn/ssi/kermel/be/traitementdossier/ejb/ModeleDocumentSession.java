package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygModeleDocument;
import sn.ssi.kermel.be.entity.SygPieces;
import sn.ssi.kermel.be.entity.SygTypesDossiers;



@Remote
public interface ModeleDocumentSession {
	public void save(SygModeleDocument document);
	public void delete(Long Id);
	public List<SygModeleDocument> find(int indice, int pas,String libelle,String code,String description,String fichier);
	public int count(String libelle);
	public void update(SygModeleDocument document);
	public SygModeleDocument findById(Long code);
	public List<SygModeleDocument> find(String code);
	List<SygModeleDocument> find(int indice, int pas);
	int count();
	public List<SygModeleDocument> findNotInDossier(int indice, int pas, SygTypesDossiers code, String libelle);
	Integer countNotInDossier(SygTypesDossiers code, String libelle);
	
}
