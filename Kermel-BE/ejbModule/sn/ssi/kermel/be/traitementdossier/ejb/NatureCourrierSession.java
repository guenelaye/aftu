package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygNatureCourrier;


@Remote
public interface NatureCourrierSession {
	public void save(SygNatureCourrier courrier);
	public void delete(Long idnaturecourrier);
	public List<SygNatureCourrier> find(int indice, int pas,String libellenaturecourrier,String codenaturecourrier,String descriptionnaturecourrier);
	public int count(String libellenaturecourrier);
	public void update(SygNatureCourrier courrier);
	public SygNatureCourrier findById(Long code);
	public List<SygNatureCourrier> find(String code);
	List<SygNatureCourrier> find(int indice, int pas);
	int count();
	
}
