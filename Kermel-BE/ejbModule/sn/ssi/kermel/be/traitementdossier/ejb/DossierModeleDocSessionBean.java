package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygDossierModeleDoc;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class DossierModeleDocSessionBean extends AbstractSessionBean implements DossierModeleDocSession {

    @Override
    public SygDossierModeleDoc save(SygDossierModeleDoc modeleDoc) {
	try {
	    getHibernateSession().save(modeleDoc);
	    return modeleDoc;
	} catch (Exception e) {
	    return null;
	}

    }

    @Override
    public SygDossierModeleDoc update(SygDossierModeleDoc modeleDoc) {
	try {
	    getHibernateSession().update(modeleDoc);
	    return modeleDoc;
	} catch (Exception e) {
	    return null;
	}
    }

    @Override
    public List<SygDossierModeleDoc> findByDossier(
	    SygDossierCourrier dossierCourrier) {
	Criteria criteria = getHibernateSession().createCriteria(SygDossierModeleDoc.class);
	criteria.createAlias("dossierCourrier", "dossierCourrier");
	criteria.createAlias("modeleDocument", "modeleDocument");
	if(dossierCourrier!=null) {
	    criteria.add(Restrictions.eq("dossierCourrier", dossierCourrier));
	}
	return criteria.list();
    }

}
