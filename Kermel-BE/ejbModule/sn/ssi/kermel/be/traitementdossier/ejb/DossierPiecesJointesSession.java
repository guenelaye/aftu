package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygDossierPiecesJointes;

@Remote
public interface DossierPiecesJointesSession {

    public SygDossierPiecesJointes findById(Long id);
    public List<SygDossierPiecesJointes> findByDossier(SygDossierCourrier  dossierCourrier);
    public SygDossierPiecesJointes save(SygDossierPiecesJointes dossierPiecesJointes);
    public SygDossierPiecesJointes update(SygDossierPiecesJointes dossierPiecesJointes);
    public void delete(Long id);
}
