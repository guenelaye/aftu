package sn.ssi.kermel.be.traitementdossier.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygTypesDossiers;



@Remote
public interface TypesDossiersSession {
	public void save(SygTypesDossiers dossiers);
	public void delete(Long idcritereanalyse);
	public List<SygTypesDossiers> find(int indice, int pas,String code,String libelle,Integer delaitraitement,String description);
	public int count(String libelle);
	public void update(SygTypesDossiers dossiers);
	public SygTypesDossiers findById(Long code);
	public List<SygTypesDossiers> find(String code);
	List<SygTypesDossiers> find(int indice, int pas);
	int count();
	
}
