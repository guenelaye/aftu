package sn.ssi.kermel.be.dossiertype.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygAvisGeneral;
import sn.ssi.kermel.be.entity.SygDossiersTypes;
import sn.ssi.kermel.be.entity.SygFichDossierType;
import sn.ssi.kermel.be.entity.SygRealisations;

@Remote
public interface FichDossierTypeSession {
	public void save(SygFichDossierType fichdossiertype);
	public void delete(Integer ID);
	public List<SygFichDossierType> find(int indice, int pas,String libellefichierdossiertype,String nomfichierdossiertype,String publier,SygDossiersTypes Dossiertype_ID);
	public int count(String libellefichierdossiertype,String nomfichierdossiertype,String publier,SygDossiersTypes Dossiertype_ID);
	public void update(SygFichDossierType fichdossiertype);
	public SygFichDossierType findById(Long code);
	

	
}
