package sn.ssi.kermel.be.dossiertype.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygAvisGeneral;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossiersTypes;
import sn.ssi.kermel.be.entity.SygFichDossierType;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class FichDossierTypeSessionBean extends AbstractSessionBean implements FichDossierTypeSession{

	
	@Override
	public int count(String libellefichierdossiertype,String nomfichierdossiertype,String publier,SygDossiersTypes Dossiertype_ID) {
		Criteria criteria = getHibernateSession().createCriteria(SygAvisGeneral.class);
		//criteria.createAlias("realisation", "realisation");
		//criteria.createAlias("autorite", "autorite");
		if(libellefichierdossiertype!=null){
			criteria.add(Restrictions.eq("libellefichierdossiertype", libellefichierdossiertype));
		}
		if(nomfichierdossiertype!=null){
			criteria.add(Restrictions.eq("nomfichierdossiertype", nomfichierdossiertype));
		}
		if(publier!=null){
			criteria.add(Restrictions.eq("publier", publier));
		}
		if(Dossiertype_ID!=null){
			criteria.add(Restrictions.eq("Dossiertype_ID", Dossiertype_ID));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Integer ID) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygFichDossierType.class, ID));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygFichDossierType> find(int indice, int pas,String libellefichierdossiertype,String nomfichierdossiertype,String publier,SygDossiersTypes Dossiertype_ID) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygFichDossierType.class);
		//criteria.createAlias("realisation", "realisation");
		//criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.asc("id"));
		if(libellefichierdossiertype!=null){
			criteria.add(Restrictions.eq("libellefichierdossiertype", libellefichierdossiertype));
		}
		if(nomfichierdossiertype!=null){
			criteria.add(Restrictions.eq("nomfichierdossiertype", nomfichierdossiertype));
		}
		if(publier!=null){
			criteria.add(Restrictions.eq("publier", publier));
		}
		if(Dossiertype_ID!=null){
			criteria.add(Restrictions.eq("Dossiertype_ID", Dossiertype_ID));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygFichDossierType fichdossiertype) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(fichdossiertype);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygFichDossierType fichdossiertype) {
		
		try{
			getHibernateSession().merge(fichdossiertype);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygFichDossierType findById(Long code) {
		return (SygFichDossierType)getHibernateSession().get(SygFichDossierType.class, code);
	}
	

	//@Override
//	public SygAvisGeneral getContrat(SygAutoriteContractante autorite){
//		// TODO Auto-generated method stub
//		Criteria criteria = getHibernateSession().createCriteria(SygAvisGeneral.class);
////		criteria.createAlias("realisation", "realisation");
//		criteria.createAlias("autorite", "autorite");
//		if(autorite!=null){
//			criteria.add(Restrictions.eq("autorite", autorite));
//		}
////		if(realisation!=null){
////			criteria.add(Restrictions.eq("realisation", realisation));
////		}
//		if(criteria.list().size()>0)
//		  return (SygAvisGeneral) criteria.list().get(0);
//		else 
//			return null;
//	}


}
