package sn.ssi.kermel.be.dossiers.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygDossiersTypes;

@Remote
public interface DossierTypeSessions {

	public void save(SygDossiersTypes dossier);
	public void delete(Long id);
	public List<SygDossiersTypes> find(int indice, int pas,String libelledossiers,String Typedossier);
	public int count(String libelledossiers,String Typedossier);
	public void update(SygDossiersTypes dossier);
	public SygDossiersTypes findById(Long code);
	public List<SygDossiersTypes> find(String code);
	List<SygDossiersTypes> find(int indice, int pas);
	int count();
}
