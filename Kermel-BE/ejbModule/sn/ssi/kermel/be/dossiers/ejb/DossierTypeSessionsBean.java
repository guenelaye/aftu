package sn.ssi.kermel.be.dossiers.ejb;

import java.util.List;

import sn.ssi.kermel.common.ejb.AbstractSessionBean;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.dossiers.ejb.DossierTypeSessions;
import sn.ssi.kermel.be.entity.SygDossiersTypes;

public @Stateless class DossierTypeSessionsBean extends AbstractSessionBean implements DossierTypeSessions {

	@Override
	public int count(String libelledossiers,String Typedossier) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossiersTypes.class);
		if(libelledossiers!=null){
			criteria.add(Restrictions.ge("String", libelledossiers));
		}
		if(Typedossier!=null){
			criteria.add(Restrictions.ge("String", Typedossier));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygDossiersTypes.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long iddossiers) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygDossiersTypes.class, iddossiers));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	@Override
	public void save(SygDossiersTypes dossier) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(dossier);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygDossiersTypes dossier) {
	try{
			getHibernateSession().merge(dossier);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public SygDossiersTypes findById(Long code) {
		return (SygDossiersTypes)getHibernateSession().get(SygDossiersTypes.class, code);
	}
	@Override
	public List<SygDossiersTypes> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiersTypes.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));	
		return criteria.list();
	}
	@Override
	public List<SygDossiersTypes> find(int indice, int pas,String  libelledossiers,String Typedossier) {
       Criteria criteria = getHibernateSession().createCriteria(SygDossiersTypes.class);
		if(libelledossiers!=null){
			criteria.add(Restrictions.ge("String", libelledossiers));
		}
		if(Typedossier!=null){
			criteria.add(Restrictions.ge("String", Typedossier));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);	
		return criteria.list();
		}
	@Override
	public List<SygDossiersTypes> find(int indice, int pas) {
	   Criteria criteria = getHibernateSession().createCriteria(SygDossiersTypes.class);
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);			
		return criteria.list();
		}
}
