package sn.ssi.kermel.be.security;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBureauxdcmp;
import sn.ssi.kermel.be.entity.Utilisateur;

@Remote
public interface UtilisateurSession {

	List<Utilisateur> findUtilisateurs(int start, int step, String type, SygAutoriteContractante autorite,String user);

	int countAllUtilisateurs(String type, SygAutoriteContractante autorite,String user);

	void createUtilisateur(Utilisateur user);

	void deleteUtilisateur(long id);

	void updateUtilisateur(Utilisateur utilisateur);

	Utilisateur findByCode(long usrId);


	List<Utilisateur> findUtilisateur(String login, String password);

	List<Utilisateur> findUtilisateur(String login);

	List<Utilisateur> findUser(String login, String User);
	List<Utilisateur> findUserByProfil(String prf);

	Utilisateur findMaxUserByProfil(String prf);

	List<Utilisateur> findMinUserByProfil(String prf);
	
	Utilisateur InfosCompte(String login);
	
	List<Utilisateur> findByBureau(int start, int step, String type, SygBureauxdcmp bureau);
	
	int countUtilByBureau(String type, SygBureauxdcmp bureau);
	public Utilisateur findUserChefService(Long UniteOrg,Boolean etat);
	
	public Utilisateur save(Utilisateur utilisateur) ;
}
