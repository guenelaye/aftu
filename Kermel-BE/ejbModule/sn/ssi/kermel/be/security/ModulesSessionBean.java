package sn.ssi.kermel.be.security;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SysAction;
import sn.ssi.kermel.be.entity.SysFeature;
import sn.ssi.kermel.be.entity.SysModule;
import sn.ssi.kermel.be.entity.SysProfilAction;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless
class ModulesSessionBean extends AbstractSessionBean implements ModulesSession {

	@Override
	public List<SysModule> findModules(int start, int step) {
		List<SysModule> modules = null;
		Criteria criteria = getHibernateSession().createCriteria(
				SysModule.class);
		criteria.addOrder(Order.asc("modCode"));
		criteria.setFirstResult(start);
		if (step > 0) {
			criteria.setMaxResults(step);
		}
		modules = criteria.list();
		return modules;
	}

	@Override
	public int countAllModules() {
		Criteria criteria = getHibernateSession().createCriteria(
				SysModule.class);
		criteria.setProjection(Projections.count("modCode"));
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void createModule(SysModule module) {
		getHibernateSession().saveOrUpdate(module);

	}

	@Override
	public SysModule findByCode(String moduleCode) {
		// TODO Auto-generated method stub
		return (SysModule) getHibernateSession().get(SysModule.class,
				moduleCode);
	}

	@Override
	public void deleteModule(String moduleCode) {
		SysModule module = (SysModule) getHibernateSession().get(
				SysModule.class, moduleCode);
		getHibernateSession().delete(module);
	}

	@Override
	public int countAllFeatureActions(String feaCode) {
		Criteria criteria = getHibernateSession().createCriteria(
				SysAction.class);
		criteria.add(Restrictions.eq("sysFeature.feaCode", feaCode));
		criteria.setProjection(Projections.count("feaCode"));
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public int countAllModuleFeatures(String moduleCode) {
		Criteria criteria = getHibernateSession().createCriteria(
				SysFeature.class);
		criteria.add(Restrictions.eq("sysModule.modCode", moduleCode));
		criteria.setProjection(Projections.count("feaCode"));
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public List<SysAction> findActions(String featureCode, int start, int step) {
		List<SysAction> actions = null;
		Criteria criteria = getHibernateSession().createCriteria(
				SysAction.class);
		criteria.add(Restrictions.eq("sysFeature.feaCode", featureCode));
		criteria.addOrder(Order.asc("actCode"));
		criteria.setFirstResult(start);
		if (step > 0) {
			criteria.setMaxResults(step);
		}
		actions = criteria.list();
		return actions;
	}

	@Override
	public List<SysFeature> findFeatures(String moduleCode, int start, int step) {
		List<SysFeature> features = null;
		Criteria criteria = getHibernateSession().createCriteria(
				SysFeature.class);
		criteria.add(Restrictions.eq("sysModule.modCode", moduleCode));
		criteria.addOrder(Order.asc("feaCode"));
		criteria.setFirstResult(start);
		if (step > 0) {
			criteria.setMaxResults(step);
		}
		features = criteria.list();
		return features;
	}

	@Override
	public SysFeature findFeatureByCode(String featureCode) {
		return (SysFeature) getHibernateSession().get(SysFeature.class,
				featureCode);
	}

	@Override
	public SysAction findActionByCode(String actionCode) {
		return (SysAction) getHibernateSession().get(SysAction.class,
				actionCode);
	}

	@Override
	public SysProfilAction findProfilAction(String actCode, String profilCode) {

		Query q = getHibernateSession().createQuery(
				"SELECT pac FROM SysProfilAction pac "
						+ " WHERE pac.sysProfil.pfCode='" + profilCode
						+ "'  AND pac.sysAction.actCode='" + actCode + "'");
		if (q.list().isEmpty())
			return null;
		else
			return (SysProfilAction) q.list().get(0);
	}

	@Override
	public void saveProfilAction(SysProfilAction pac) {
		getHibernateSession().saveOrUpdate(pac);
	}

	@Override
	public void saveAction(SysAction act) {
		getHibernateSession().saveOrUpdate(act);
	}
}