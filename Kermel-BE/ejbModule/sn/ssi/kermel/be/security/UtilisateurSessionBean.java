package sn.ssi.kermel.be.security;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBureauxdcmp;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless
class UtilisateurSessionBean extends AbstractSessionBean implements UtilisateurSession {

	@Override
	public int countAllUtilisateurs(String type, SygAutoriteContractante autorite,String user) {
		Criteria criteria = getHibernateSession().createCriteria(Utilisateur.class);
		criteria.createAlias("sysProfil", "sysProfil");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("bureaux", FetchMode.SELECT);
		criteria.setFetchMode("services", FetchMode.SELECT);
		if (type != null) {
			if(user!=null)
			{
				if((type.equals(BeConstants.USERS_TYPES_DCMP))&&(user.equals(BeConstants.USERS_TYPES_AC)))
					 criteria.add(Restrictions.eq("type", BeConstants.USERS_TYPES_AC));
			}
			else
			{
				if(type.equals(BeConstants.USERS_TYPES_AC))
					  criteria.add(Restrictions.or(Restrictions.eq("type", type), Restrictions.eq("type", BeConstants.USERS_TYPES_AGENTAC)));
					else
					  criteria.add(Restrictions.eq("type", type));
			}
			
		}
		if (autorite != null) {
			 criteria.add(Restrictions.eq("autorite", autorite));
		}
		criteria.setProjection(Projections.count("id"));
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void createUtilisateur(Utilisateur user) {
		getHibernateSession().saveOrUpdate(user);
	}

	@Override
	public void deleteUtilisateur(long id) {
		getHibernateSession().delete(getHibernateSession().load(Utilisateur.class, id));
	}

	@Override
	public Utilisateur findByCode(long id) {
		return (Utilisateur) getHibernateSession().get(Utilisateur.class, id);
	}

	@Override
	public List<Utilisateur> findUtilisateurs(int start, int step, String type, SygAutoriteContractante autorite,String user) {
		List<Utilisateur> utilisateurs = null;
		Criteria criteria = getHibernateSession().createCriteria(Utilisateur.class);
		criteria.createAlias("sysProfil", "sysProfil");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("bureaux", FetchMode.SELECT);
		criteria.setFetchMode("services", FetchMode.SELECT);
		criteria.addOrder(Order.asc("id"));
//		if (type != null) {
//			if(type.equals(BeConstants.USERS_TYPES_DCMP))
//			  criteria.add(Restrictions.or(Restrictions.eq("type", type), Restrictions.eq("type", BeConstants.USERS_TYPES_AC)));
//			else
//				if(type.equals(BeConstants.USERS_TYPES_AC))
//				  criteria.add(Restrictions.or(Restrictions.eq("type", type), Restrictions.eq("type", BeConstants.USERS_TYPES_AGENTAC)));
//				else
//			      criteria.add(Restrictions.eq("type", type));
//		}
		if (type != null) {
			if(user!=null)
			{
				if((type.equals(BeConstants.USERS_TYPES_DCMP))&&(user.equals(BeConstants.USERS_TYPES_AC)))
					 criteria.add(Restrictions.eq("type", BeConstants.USERS_TYPES_AC));
			}
			else
			{
				if(type.equals(BeConstants.USERS_TYPES_AC))
					  criteria.add(Restrictions.or(Restrictions.eq("type", type), Restrictions.eq("type", BeConstants.USERS_TYPES_AGENTAC)));
					else
					  criteria.add(Restrictions.eq("type", type));
			}
			
		}
		if (autorite != null) {
			 criteria.add(Restrictions.eq("autorite", autorite));
		}
		criteria.setFirstResult(start);
		if (step > 0) {
			criteria.setMaxResults(step);
		}
		utilisateurs = criteria.list();
		return utilisateurs;
	}

	@Override
	public List<Utilisateur> findUtilisateur(String login, String password) {
		List<Utilisateur> utilisateurs = null;
		Query q = getHibernateSession().createQuery("FROM Utilisateur usr LEFT JOIN FETCH usr.sysProfil WHERE login LIKE '" + login + "' AND password LIKE '" + password + "'");
		utilisateurs = q.list();
		return utilisateurs;
	}

	@Override
	public List<Utilisateur> findUtilisateur(String login) {
		List<Utilisateur> utilisateurs = null;
		Query q = getHibernateSession().createQuery("FROM Utilisateur WHERE login LIKE '" + login + "' ");
		utilisateurs = q.list();
		return utilisateurs;
	}

	@Override
	public void updateUtilisateur(Utilisateur utilisateur) {
		// TODO Auto-generated method stub
		getHibernateSession().merge(utilisateur);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Utilisateur> findUser(String login, String User) {
		Criteria criteria = getHibernateSession().createCriteria(Utilisateur.class);
		if (login != null) {
			criteria.add(Restrictions.eq("login", login));
		}
		if (User != null) {
			criteria.add(Restrictions.eq("password", User));
		}
		return criteria.list();
	}

	@Override
	public List<Utilisateur> findUserByProfil(String prf) {
		Criteria criteria = getHibernateSession().createCriteria(Utilisateur.class);
		criteria.createAlias("sysProfil", "profil");
		if (prf != null) {
			criteria.add(Restrictions.eq("profil.pfCode", prf));
		}
		return criteria.list();
	}
	
	@Override
	public Utilisateur findMaxUserByProfil(String prf) {
		Criteria criteria = getHibernateSession().createCriteria(Utilisateur.class);
		criteria.createAlias("sysProfil", "profil");
		if (prf != null) {
			criteria.add(Restrictions.eq("profil.pfCode", prf));
		}
		criteria.setProjection(Projections.min("nbdossier"));
		return (Utilisateur) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Utilisateur> findMinUserByProfil(String prf) {
		Criteria criteria = getHibernateSession().createCriteria(Utilisateur.class);
		criteria.createAlias("sysProfil", "profil");
		if (prf != null) {
			criteria.add(Restrictions.eq("profil.pfCode", prf));
		}
//		criteria.setProjection(Projections.min("nbdossier"));
//		criteria.setProjection(Projections.property("id"));
		
		return  criteria.list();
	}
	
	@Override
	public Utilisateur InfosCompte(String login) {
		Criteria criteria = getHibernateSession().createCriteria(Utilisateur.class);
		criteria.createAlias("sysProfil", "profil");
		criteria.setFetchMode("bureaux", FetchMode.SELECT);
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		if (login != null) {
			criteria.add(Restrictions.eq("login", login));
		}
		return (Utilisateur) criteria.list().get(0);                                
	}

	@Override
	public int countUtilByBureau(String type, SygBureauxdcmp bureau) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Utilisateur> findByBureau(int start, int step, String type,
			SygBureauxdcmp bureau) {
		// TODO Auto-generated method stub
		List<Utilisateur> utilisateurs = null;
		Criteria criteria = getHibernateSession().createCriteria(Utilisateur.class);
		criteria.createAlias("sysProfil", "sysProfil");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("bureaux", FetchMode.SELECT);
		criteria.setFetchMode("services", FetchMode.SELECT);
		criteria.addOrder(Order.asc("id"));
		if (type != null) {
			if(type.equals(BeConstants.USERS_TYPES_DCMP))
			  criteria.add(Restrictions.eq("type", type));			
		}
		
		if (bureau != null) {
			 criteria.add(Restrictions.eq("bureaux", bureau));
		}
		criteria.setFirstResult(start);
		if (step > 0) {
			criteria.setMaxResults(step);
		}
		utilisateurs = criteria.list();
		return utilisateurs;
	}

	@Override
	public Utilisateur findUserChefService(Long UniteOrg, Boolean etat) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(Utilisateur.class);
		criteria.createAlias("bureaux", "bureaux");
				
		if(etat!=null){
			criteria.add(Restrictions.eq("chefService", etat));
		}
		
		if(UniteOrg!=null){
			criteria.add(Restrictions.eq("bureaux.id", UniteOrg));
		}
		
		
		return (Utilisateur)criteria.uniqueResult();
	}
	

	@Override
	public Utilisateur save(Utilisateur utilisateur) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(utilisateur);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	
		return utilisateur;
	}
	
}
