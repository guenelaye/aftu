package sn.ssi.kermel.be.security;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SysAction;
import sn.ssi.kermel.be.entity.SysFeature;
import sn.ssi.kermel.be.entity.SysModule;
import sn.ssi.kermel.be.entity.SysProfil;
import sn.ssi.kermel.be.entity.SysProfilAction;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class ProfilsSessionBean extends AbstractSessionBean
		implements ProfilsSession {

	@Override
	public int countAllProfils(String code, String libelle, String type,
			SygAutoriteContractante autorite) {
		Criteria criteria = getHibernateSession().createCriteria(
				SysProfil.class);
		criteria.setProjection(Projections.count("pfCode"));
		if (code != null) {
			criteria.add(Restrictions.ilike("pfCode", "%" + code + "%"));
		}
		if (libelle != null) {
			criteria.add(Restrictions.ilike("pfLibelle", "%" + libelle + "%"));
		}
		// if(type!=null){
		// criteria.add(Restrictions.eq("type", type));
		// }
		if (type != null) {
			if (autorite == null)
				criteria.add(Restrictions.eq("type", type));
			else
				criteria.add(Restrictions.or(Restrictions.eq("type", type),
						Restrictions
								.eq("type", BeConstants.USERS_TYPES_AGENTAC)));
		}
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void createProfil(SysProfil profil) {
		getHibernateSession().saveOrUpdate(profil);
	}

	@Override
	public void deleteProfil(String profilCode) {
		SysProfil profil = (SysProfil) getHibernateSession().get(
				SysProfil.class, profilCode);
		getHibernateSession().delete(profil);
	}

	@Override
	public SysProfil findByCode(String profilCode) {
		return (SysProfil) getHibernateSession().get(SysProfil.class,
				profilCode);
	}

	@Override
	public List<SysProfil> findProfils(int start, int step, String code,
			String libelle, String type, SygAutoriteContractante autorite) {
		List<SysProfil> profils = null;
		Criteria criteria = getHibernateSession().createCriteria(
				SysProfil.class);
		criteria.addOrder(Order.asc("pfCode"));
		if (code != null) {
			criteria.add(Restrictions.ilike("pfCode", "%" + code + "%"));
		}
		if (libelle != null) {
			criteria.add(Restrictions.ilike("pfLibelle", "%" + libelle + "%"));
		}
		// if (type != null) {
		// if(type.equals(BeConstants.USERS_TYPES_DCMP))
		// criteria.add(Restrictions.or(Restrictions.eq("type", type),
		// Restrictions.eq("type", BeConstants.USERS_TYPES_AC)));
		// else
		// if(type.equals(BeConstants.USERS_TYPES_AC))
		// criteria.add(Restrictions.or(Restrictions.eq("type", type),
		// Restrictions.eq("type", BeConstants.USERS_TYPES_AGENTAC)));
		// else
		// criteria.add(Restrictions.eq("type", type));
		// }

		if (type != null) {
			if (autorite == null)
				criteria.add(Restrictions.eq("type", type));
			else
				criteria.add(Restrictions.or(Restrictions.eq("type", type),
						Restrictions
								.eq("type", BeConstants.USERS_TYPES_AGENTAC)));
		}
		criteria.setFirstResult(start);
		if (step > 0) {
			criteria.setMaxResults(step);
		}
		profils = criteria.list();
		return profils;
	}

	@Override
	public List<SysFeature> findDisplayableFeaturesForProfilAndModule(
			String profilCode, String moduleCode, int start, int step) {
		List<SysFeature> features = null;
		Query q = getHibernateSession().createQuery(
				"SELECT fea FROM  SysFeature fea LEFT JOIN fea.sysActions act  "
						+ " LEFT JOIN act.sysProfilActions pac "
						+ " WHERE pac.sysProfil.pfCode='" + profilCode + "'"
						+ " AND fea.sysModule.modCode='" + moduleCode + "'"
						+ " AND fea.feaDisplayable=1" + " AND pac.allow=1"
						+ " ORDER BY fea.feaSequence asc");
		features = q.list();
		return features;
	}

	@Override
	public List<SysModule> findDisplayableModulesForProfil(String profilCode,
			int start, int step) {
		List<SysModule> modules = null;
		Query q = getHibernateSession().createQuery(
				"SELECT mod FROM SysModule mod LEFT JOIN mod.sysFeatures fea "
						+ " LEFT JOIN fea.sysActions act "
						+ " LEFT JOIN act.sysProfilActions pac "
						+ " WHERE pac.sysProfil.pfCode='" + profilCode
						+ "' AND pac.allow=1" + "ORDER BY mod.modSequence");
		modules = q.list();
		return modules;
	}

	@Override
	public List<SysAction> findDisplayableActionsForProfil(String profilCode,
			String feaCode) {
		List<SysAction> actions = null;
		Query q = getHibernateSession().createQuery(
				"SELECT act FROM SysAction act "
						+ " LEFT JOIN act.sysProfilActions pac "
						+ " WHERE act.sysFeature.feaCode='" + feaCode
						+ "' AND pac.sysProfil.pfCode='" + profilCode
						+ "' AND pac.allow=1  order by act.actCode asc");
		actions = q.list();
		return actions;
	}

	@Override
	public List<Utilisateur> findUtilisateur(String login, String password) {
		List<Utilisateur> utilisateurs = null;
		Query q = getHibernateSession()
				.createQuery(
						"SELECT usr FROM Utilisateur usr LEFT JOIN FETCH usr.sysProfil WHERE usr.login ='"
								+ login
								+ "' AND usr.password ='"
								+ password
								+ "'");
		utilisateurs = q.list();
		return utilisateurs;
	}

	@Override
	public List<Utilisateur> findUtilisateur(String login) {
		List<Utilisateur> utilisateurs = null;
		Query q = getHibernateSession()
				.createQuery(
						"SELECT usr FROM Utilisateur usr LEFT JOIN FETCH usr.sysProfil WHERE usr.login ='"
								+ login + "' ");
		utilisateurs = q.list();
		return utilisateurs;
	}

	@Override
	public List<SysProfilAction> ListeDroits(String profil) {

		List<SysProfilAction> actions = null;
		Query q = getHibernateSession().createQuery(
				"SELECT act FROM SysProfilAction act  WHERE act.sysProfil.pfCode='"
						+ profil + "' AND act.allow=1 ");
		actions = q.list();
		return actions;

		// List<SysProfil> profils = null;
		// Criteria criteria =
		// getHibernateSession().createCriteria(SysProfilAction.class);
		// criteria.createAlias("sysProfil", "profil");
		// criteria.createAlias("sysAction", "action");
		// if(profil!=null){
		// criteria.add(Restrictions.eq("profil.pfCode", profil));
		// }
		// criteria.add(Restrictions.eq("allow",allow));
		// return criteria.list();
	}

	@Override
	public List<Utilisateur> findUtilisateur(String login, String password,
			String profil) {
		List<Utilisateur> utilisateurs = null;
		Query q = getHibernateSession()
				.createQuery(
						"SELECT usr FROM Utilisateur usr LEFT JOIN FETCH usr.sysProfil profil WHERE usr.login ='"
								+ login
								+ "' AND usr.password ='"
								+ password
								+ "'" + " and profil.pfCode='" + profil + "'");
		utilisateurs = q.list();
		return utilisateurs;
	}

	@Override
	public List<Utilisateur> findUtilisateur(String login, String password,
			String profil, SygAutoriteContractante autorite) {
		Criteria criteria = getHibernateSession().createCriteria(
				Utilisateur.class);
		criteria.createAlias("sysProfil", "profil");
		criteria.createAlias("autorite", "autorite");
		if (login != null) {
			criteria.add(Restrictions.eq("login", login));
		}
		if (password != null) {
			criteria.add(Restrictions.eq("password", password));
		}
		if (profil != null) {
			criteria.add(Restrictions.eq("profil.pfCode", profil));
		}
		if (autorite != null) {
			criteria.add(Restrictions.eq("autorite", autorite));
		}

		return criteria.list();
	}

	@Override
	public Utilisateur findSecretaire(SygAutoriteContractante autorite) {
		Criteria criteria = getHibernateSession().createCriteria(
				Utilisateur.class);
		criteria.createAlias("sysProfil", "profil");
		criteria.createAlias("autorite", "autorite");

		criteria.add(Restrictions.eq("profil.pfCode", "SOPLIS"));

		criteria.add(Restrictions.eq("autorite", autorite));

		ArrayList<Utilisateur> lst = (ArrayList<Utilisateur>) criteria.list();

		if (lst.size() > 0)
			return lst.get(0);
		else
			return new Utilisateur();
	}

	@Override
	public List<SysProfilAction> Liste_Droits(String profil) {

		Criteria criteria = getHibernateSession().createCriteria(
				SysProfilAction.class);
		criteria.createAlias("sysProfil", "profil");
		criteria.createAlias("sysAction", "action");
		criteria.createAlias("action.sysFeature", "feature");
		criteria.createAlias("feature.sysModule", "module");
		if (profil != null) {
			criteria.add(Restrictions.eq("profil.pfCode", profil));
		}

		// criteria.add(Restrictions.or(Restrictions.eq("module.modCode",
		// "TDOSSIERSPPM"),
		// Restrictions.or(Restrictions.eq("module.modCode",
		// "GESTIONPLANPASSATION"), Restrictions.eq("module.modCode",
		// "PASSATIONMARCHES"))));
		//
		// criteria.add(Restrictions.or(Restrictions.eq("module.modCode",
		// "TDOSSIERSPPM"), Restrictions.eq("module.modCode",
		// "GESTIONPLANPASSATION")));
		criteria.add(Restrictions.eq("allow", (short) 1));
		return criteria.list();
	}

	@Override
	public Utilisateur findByLogin(String login) {
		Criteria criteria = getHibernateSession().createCriteria(
				Utilisateur.class);
		if (login != null) {
			criteria.add(Restrictions.eq("login", login));
		}

		List lst = criteria.list();

		if (lst.size() > 0)
			return (Utilisateur) lst.get(0);

		else
			return null;

	}
	
	@Override
	public Utilisateur findByCodeSMS(String codeSMS) {
		Criteria criteria = getHibernateSession().createCriteria(
				Utilisateur.class);
		if (codeSMS != null) {
			criteria.add(Restrictions.eq("codesms", codeSMS));
		}

		List lst = criteria.list();

		if (lst.size() > 0)
			return (Utilisateur) lst.get(0);

		else
			return null;

	}

}
