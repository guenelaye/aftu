package sn.ssi.kermel.be.security;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SysJournal;

/**
 * @author Guillaume GB (guibenissan@gmail.com)
 */
@Remote
public interface JournalSession {

	public List<SysJournal> find(Date periodStart, Date periodEnd, int start,
			int step);

	public int count(Date periodStart, Date periodEnd);

	void logAction(String actCode, String desc, String login);

	void logArrow(String arwCode, String desc, String login);

}
