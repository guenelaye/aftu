package sn.ssi.kermel.be.security;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SysAction;
import sn.ssi.kermel.be.entity.SysFeature;
import sn.ssi.kermel.be.entity.SysModule;
import sn.ssi.kermel.be.entity.SysProfil;
import sn.ssi.kermel.be.entity.SysProfilAction;
import sn.ssi.kermel.be.entity.Utilisateur;

@Remote
public interface ProfilsSession {
	public List<SysProfil> findProfils(int start, int step, String code, String libelle,String type,SygAutoriteContractante autorite);

	public int countAllProfils(String code, String libelle,String type,SygAutoriteContractante autorite);

	public void createProfil(SysProfil profil);

	public SysProfil findByCode(String profilCode);

	public void deleteProfil(String profilCode);

	public List<SysModule> findDisplayableModulesForProfil(String profilCode, int start, int step);

	List<SysFeature> findDisplayableFeaturesForProfilAndModule(String profilCode, String moduleCode, int start, int step);

	List<Utilisateur> findUtilisateur(String login, String password);

	List<SysAction> findDisplayableActionsForProfil(String profilCode, String feaCode);

	List<Utilisateur> findUtilisateur(String login);
	
	List<SysProfilAction> ListeDroits(String profil);
	List<Utilisateur> findUtilisateur(String login, String password,String profil);
	List<Utilisateur> findUtilisateur(String login, String password,String profil,SygAutoriteContractante autorite);
	
	List<SysProfilAction> Liste_Droits(String profil);

	Utilisateur findSecretaire(SygAutoriteContractante autorite);

	Utilisateur findByLogin(String login);
	
	Utilisateur findByCodeSMS(String codeSMS);
}
