package sn.ssi.kermel.be.security;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SysAction;
import sn.ssi.kermel.be.entity.SysFeature;
import sn.ssi.kermel.be.entity.SysModule;
import sn.ssi.kermel.be.entity.SysProfilAction;

@Remote
public interface ModulesSession {

	public List<SysModule> findModules(int start, int step);

	public int countAllModules();

	public void createModule(SysModule module);

	public SysModule findByCode(String moduleCode);

	public void deleteModule(String moduleCode);

	public int countAllModuleFeatures(String moduleCode);

	public List<SysFeature> findFeatures(String moduleCode, int start, int step);

	public SysAction findActionByCode(String ActionCode);

	public SysFeature findFeatureByCode(String featureCode);

	public List<SysAction> findActions(String featureCode, int start, int step);

	public int countAllFeatureActions(String feaCode);

	public SysProfilAction findProfilAction(String profilCode,
			String featureCode);

	public void saveProfilAction(SysProfilAction pac);

	public void saveAction(SysAction act);
}
