package sn.ssi.kermel.be.security;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SysAction;
import sn.ssi.kermel.be.entity.SysJournal;
import sn.ssi.kermel.be.workflow.entity.SysArrow;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

/**
 * @author Guillaume GB (guibenissan@gmail.com)
 */
public @Stateless
class JournalSessionBean extends AbstractSessionBean implements JournalSession {

	@Override
	public int count(Date periodStart, Date periodEnd) {
		Criteria criteria = getHibernateSession().createCriteria(
				SysJournal.class).setProjection(Projections.rowCount());

		if (periodStart != null) {
			criteria.add(Restrictions.ge("jouDate", periodStart));
		}
		if (periodEnd != null) {
			criteria.add(Restrictions.le("jouDate", periodEnd));
		}

		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@SuppressWarnings("unchecked")
	public List<SysJournal> find(Date periodStart, Date periodEnd, int start,
			int step) {

		Criteria criteria = getHibernateSession().createCriteria(
				SysJournal.class).addOrder(Order.desc("jouDate")).addOrder(
				Order.desc("jouId"));

		if (periodStart != null) {
			criteria.add(Restrictions.ge("jouDate", periodStart));
		}
		if (periodEnd != null) {
			criteria.add(Restrictions.le("jouDate", periodEnd));
		}

		criteria.setFirstResult(start);
		if (step > 0) {
			criteria.setMaxResults(step);
		}

		return criteria.list();

	}

	@Override
	public void logAction(String actCode, String desc, String login) {
		SysAction action = (SysAction) getHibernateSession().get(
				SysAction.class, actCode);
		if (action != null) {
			if (action.getActJournal() != 0) {
				SysJournal journal = new SysJournal();
				journal.setActLibelle(action.getActLibelle());
				journal.setJouDesc(desc);
				journal.setJouDate(new Date());
				journal.setUsrLogin(login);
				getHibernateSession().saveOrUpdate(journal);
			}
		}
	}

	@Override
	public void logArrow(String arwCode, String desc, String login) {
		SysArrow arrow = (SysArrow) getHibernateSession().get(SysArrow.class,
				arwCode);
		if (arrow != null) {
			SysJournal journal = new SysJournal();
			journal.setActLibelle(arrow.getLibelle());
			journal.setJouDesc(desc);
			journal.setJouDate(new Date());
			getHibernateSession().saveOrUpdate(journal);
		}
	}
}