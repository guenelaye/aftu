package sn.ssi.kermel.be.courriers.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygBureauxdcmp;
import sn.ssi.kermel.be.entity.SygCourrierAc;
import sn.ssi.kermel.be.entity.SygDossierCourrier;
import sn.ssi.kermel.be.entity.SygModeReception;
import sn.ssi.kermel.be.entity.SygTCourrierAC;
import sn.ssi.kermel.be.entity.SygTypesCourrier;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class TCourriersAcSessionBean extends AbstractSessionBean implements
		TCourriersAcSession {

	@Override
	public int count(Long code, String courrierReference,
			Date courrierDateReception, Date courrierDate,
			SygModeReception modeReception, SygAutoriteContractante ac,
			SygTypesCourrier typecourrier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygTCourrierAC.class);
		criteria.createAlias("tcourrierModeReception", "tcourrierModeReception");
		criteria.createAlias("tcourrierAutoriteContractante",
				"tcourrierAutoriteContractante");
		criteria.createAlias("tcourrierType", "courrierType");

		if (code != null) {
			criteria.add(Restrictions.eq("tcourrierId", code));
		}
		if (courrierReference != null) {
			criteria.add(Restrictions.ilike("tcourrierReference", "%"
					+ courrierReference + "%"));
		}
		if (courrierDateReception != null) {
			criteria.add(Restrictions.eq("tcourrierDateReception",
					courrierDateReception));
		}
		if (courrierDate != null) {
			criteria.add(Restrictions.eq("tcourrierDate", courrierDate));
		}
		if (modeReception != null) {
			criteria.add(Restrictions.eq("tcourrierModeReception",
					modeReception));
		}
		if (ac != null) {
			criteria.add(Restrictions.eq("tcourrierAutoriteContractante", ac));
		}
		if (typecourrier != null) {
			criteria.add(Restrictions.eq("tcourrierType", typecourrier));
		}

		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(SygTCourrierAC.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygTCourrierAC> find(int indice, int pas, Long code,
			String courrierReference, Date courrierDateReception,
			Date courrierDate, SygModeReception modeReception,
			SygAutoriteContractante ac, SygTypesCourrier typecourrier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygTCourrierAC.class);
		criteria.createAlias("agent", "agent");
		criteria.createAlias("bureau", "bureau");
		criteria.createAlias("courrierac", "courrierac");
		criteria.createAlias("courrierac.tcourrierModeReception",
				"tcourrierModeReception");
		criteria.createAlias("courrierac.tcourrierAutoriteContractante",
				"tcourrierAutoriteContractante");

		if (code != null) {
			criteria.add(Restrictions.eq("tcourrierId", code));
		}
		if (courrierReference != null) {
			criteria.add(Restrictions.ilike("tcourrierReference", "%"
					+ courrierReference + "%"));
		}
		if (courrierDateReception != null) {
			criteria.add(Restrictions.eq("courrierac.courrierDateReception",
					courrierDateReception));
		}
		if (courrierDate != null) {
			criteria.add(Restrictions.eq("tcourrierDate", courrierDate));
		}
		if (modeReception != null) {
			criteria.add(Restrictions.eq("courrierac.courrierModeReception",
					modeReception));
		}
		if (ac != null) {
			criteria.add(Restrictions.eq(
					"courrierac.courrierAutoriteContractante", ac));
		}
		if (typecourrier != null) {
			criteria.add(Restrictions.eq("courrierac.tcourrierType",
					typecourrier));
		}
		criteria.setFirstResult(indice);
		if (pas > 0) {
			criteria.setMaxResults(pas);
		}

		return criteria.list();
	}

	@Override
	public SygTCourrierAC findById(Long code) {
		// TODO Auto-generated method stub
		return (SygTCourrierAC) getHibernateSession().get(SygTCourrierAC.class,
				code);
	}

	@Override
	public SygTCourrierAC save(SygTCourrierAC courrier) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(courrier);
			return courrier;

		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
			return null;
		}
	}

	@Override
	public SygTCourrierAC update(SygTCourrierAC courrier) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().merge(courrier);
			return courrier;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int countByServiceAndAgent(SygBureauxdcmp bureau, Utilisateur agent,
			SygCourrierAc courrier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygTCourrierAC.class);
		criteria.createAlias("agent", "agent");
		criteria.createAlias("bureau", "bureau");
		criteria.createAlias("courrierac", "courrierac");
		criteria.createAlias("courrierac.courrierModeReception",
				"courrierModeReception");
		criteria.createAlias("courrierac.courrierAutoriteContractante",
				"courrierAutoriteContractante");
		criteria.setFetchMode("modetraitement", FetchMode.SELECT);
		if (courrier != null) {
			criteria.add(Restrictions.eq("courrierac", courrier));
		}
		if (bureau != null) {
			criteria.add(Restrictions.eq("bureau", bureau));
		}

		if (agent != null) {
			criteria.add(Restrictions.eq("agent", agent));
		}

		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygTCourrierAC> findByServiceAndAgent(int indice, int pas,
			SygBureauxdcmp bureau, Utilisateur agent, SygCourrierAc courrier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygTCourrierAC.class);

		criteria.createAlias("agent", "agent");
		criteria.createAlias("bureau", "bureau");
		criteria.createAlias("courrierac", "courrierac");
		criteria.createAlias("courrierac.courrierModeReception",
				"courrierModeReception");
		criteria.createAlias("courrierac.courrierAutoriteContractante",
				"courrierAutoriteContractante");
		criteria.setFetchMode("modetraitement", FetchMode.SELECT);
		if (courrier != null) {
			criteria.add(Restrictions.eq("courrierac", courrier));
		}

		if (bureau != null) {
			criteria.add(Restrictions.eq("bureau", bureau));
		}

		if (agent != null) {
			criteria.add(Restrictions.eq("agent", agent));
		}

		criteria.setFirstResult(indice);
		if (pas > 0) {
			criteria.setMaxResults(pas);
		}

		return criteria.list();
	}

	@Override
	public SygTCourrierAC findByCourrierAndAgent(SygCourrierAc courrier,
			Utilisateur agent) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygTCourrierAC.class);
		criteria.createAlias("agent", "agent");
		criteria.createAlias("bureau", "bureau");
		criteria.createAlias("courrierac", "courrierac");
		criteria.createAlias("courrierac.courrierModeReception",
				"courrierModeReception");
		criteria.createAlias("courrierac.courrierAutoriteContractante",
				"courrierAutoriteContractante");

		if (courrier != null) {
			criteria.add(Restrictions.eq("courrierac", courrier));
		}

		if (agent != null) {
			criteria.add(Restrictions.eq("agent", agent));
		}

		if (criteria.list().size() > 0) {
			return (SygTCourrierAC) criteria.uniqueResult();
		} else {
			return null;
		}
	}

	@Override
	public List<SygTCourrierAC> findByCourrierAndAgent(int start, int step,
			SygCourrierAc courrier, Utilisateur agent,
			String courrierReference, Date courrierDateReception,
			Date courrierDate, SygTypesCourrier typecourrier) {
		Criteria criteria = getHibernateSession().createCriteria(
				SygTCourrierAC.class).setFirstResult(start);
		criteria.createAlias("agent", "agent");
		criteria.createAlias("courrierac", "courrierac");
		criteria.createAlias("courrierac.courrierType", "type");
		if (courrier != null) {
			criteria.add(Restrictions.eq("courrierac", courrier));
		}

		if (agent != null) {
			criteria.add(Restrictions.eq("agent.id", agent.getId()));
		}
		if (courrierReference != null) {
			criteria.add(Restrictions.ilike("tcourrierReference", "%"
					+ courrierReference + "%"));
		}
		if (courrierDateReception != null) {
			criteria.add(Restrictions.eq("courrierac.courrierDateReception",
					courrierDateReception));
		}
		if (courrierDate != null) {
			criteria.add(Restrictions.eq("tcourrierDate", courrierDate));
		}
		if (typecourrier != null) {
			criteria.add(Restrictions.eq("courrierac.tcourrierType",
					typecourrier));
		}

		if (step > 0) {
			criteria.setMaxResults(step);
		}

		criteria.addOrder(Order.desc("tdateImputation"));

		criteria.add(Restrictions.isNull("courrierac.dossierCourrier"));

		return criteria.list();

	}

	@Override
	public int countByCourrierAndAgent(SygCourrierAc courrier,
			Utilisateur agent, String courrierReference,
			Date courrierDateReception, Date courrierDate,
			SygTypesCourrier typecourrier) {
		Criteria criteria = getHibernateSession().createCriteria(
				SygTCourrierAC.class);
		criteria.createAlias("agent", "agent");
		criteria.createAlias("courrierac", "courrierac");
		criteria.createAlias("courrierac.courrierType", "type");

		if (courrier != null) {
			criteria.add(Restrictions.eq("courrierac", courrier));
		}

		if (agent != null) {
			criteria.add(Restrictions.eq("agent", agent));
		}
		if (courrierReference != null) {
			criteria.add(Restrictions.ilike("tcourrierReference", "%"
					+ courrierReference + "%"));
		}
		if (courrierDateReception != null) {
			criteria.add(Restrictions.eq("courrierac.courrierDateReception",
					courrierDateReception));
		}
		if (courrierDate != null) {
			criteria.add(Restrictions.eq("tcourrierDate", courrierDate));
		}
		if (typecourrier != null) {
			criteria.add(Restrictions.eq("courrierac.tcourrierType",
					typecourrier));
		}

		criteria.setProjection(Projections.rowCount());

		// criteria.add(Restrictions.isNotNull("courrierac.dossierCourrier"));

		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public SygTCourrierAC findByDossier(SygDossierCourrier dossierCourrier) {
		Criteria criteria = getHibernateSession().createCriteria(
				SygTCourrierAC.class);
		criteria.createAlias("courrierac", "courrierac");
		criteria.createAlias("courrierac.courrierType", "type");
		criteria.createAlias("courrierac.courrierAutoriteContractante",
				"origine");

		if (dossierCourrier != null) {
			criteria.add(Restrictions.eq("courrierac.dossierCourrier",
					dossierCourrier));
		}
		criteria.addOrder(Order.desc("id"));

		return (SygTCourrierAC) criteria.list().get(0);
	}

}
