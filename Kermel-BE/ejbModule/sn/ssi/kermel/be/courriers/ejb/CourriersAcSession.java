package sn.ssi.kermel.be.courriers.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCourrierAc;
import sn.ssi.kermel.be.entity.SygModeReception;
import sn.ssi.kermel.be.entity.SygTypesCourrier;

@Remote
public interface CourriersAcSession {

    public SygCourrierAc save(SygCourrierAc courrier,String numero);

    public void delete(Long id);

    public List<SygCourrierAc> find(int indice, int pas,Long code,String courrierReference,Date courrierDateReception,Date courrierDate,SygModeReception modeReception, SygAutoriteContractante ac,SygTypesCourrier typecourrier,String nature);

    public int count(Long code,String courrierReference,Date courrierDateReception,Date courrierDate,SygModeReception modeReception, SygAutoriteContractante ac,SygTypesCourrier typecourrier,String nature);

    public SygCourrierAc update(SygCourrierAc courrier);

    public SygCourrierAc findById(Long code);	
    public List<SygCourrierAc> findByDossier(int indice, int pas,Long dossier);

    int countByDossier(Long dossier);
    String getGeneratedCode(String codeParametreGeneral);
}
