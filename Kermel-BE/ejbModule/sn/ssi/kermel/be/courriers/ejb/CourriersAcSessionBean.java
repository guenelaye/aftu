/**
 * 
 */
package sn.ssi.kermel.be.courriers.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCourrierAc;
import sn.ssi.kermel.be.entity.SygModeReception;
import sn.ssi.kermel.be.entity.SygTypesCourrier;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.util.Calendar;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.referentiel.ejb.ParametresGenerauxSession;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

/**
 * @author db2admin
 *
 */
@Stateless
public class CourriersAcSessionBean extends AbstractSessionBean implements CourriersAcSession {

	@EJB
	ParametresGenerauxSession parametresGenerauxSession;
	
    @Override
    public int count(Long code, String courrierReference,
	    Date courrierDateReception, Date courrierDate,
	    SygModeReception modeReception, SygAutoriteContractante ac,
	    SygTypesCourrier typecourrier,String nature) {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession().createCriteria(SygCourrierAc.class);
	criteria.createAlias("courrierModeReception", "courrierModeReception");
	criteria.createAlias("courrierAutoriteContractante", "courrierAutoriteContractante");
	criteria.createAlias("courrierType", "courrierType");
	criteria.setFetchMode("typesDossiers",FetchMode.JOIN );
	//criteria.createAlias("modetraitement", "modetraitement");
	criteria.createAlias("natureCourrier", "natureCourrier");
	if(code!=null){
	    criteria.add(Restrictions.eq("courrierId", code));
	}
	if(courrierReference!=null){
	    criteria.add(Restrictions.ilike("courrierReference", "%"+courrierReference+"%"));
	}
	if(courrierDateReception!=null){
	    criteria.add(Restrictions.eq("courrierDateReception", courrierDateReception));
	}
	if(courrierDate!=null){
	    criteria.add(Restrictions.eq("courrierDate",courrierDate));
	}
	if(modeReception!=null){
	    criteria.add(Restrictions.eq("courrierModeReception", modeReception));
	}
	if(ac!=null){
	    criteria.add(Restrictions.eq("courrierAutoriteContractante", ac));
	}
	if(typecourrier!=null){
	    criteria.add(Restrictions.eq("courrierType", typecourrier));
	}
	if(nature!=null){
	    criteria.add(Restrictions.eq("nature", nature));
	}
	criteria.setProjection(Projections.rowCount());
	return Integer.parseInt(criteria.uniqueResult().toString());
    }

    @Override
    public void delete(Long id) {
	// TODO Auto-generated method stub
	try {
	    getHibernateSession().delete(getHibernateSession().get(SygCourrierAc.class, id));
	    getHibernateSession().flush();
	} catch (Exception e) {
	    // TODO: handle exception
	}
    }

    @Override
    public List<SygCourrierAc> find(int indice, int pas, Long code,
	    String courrierReference, Date courrierDateReception,
	    Date courrierDate, SygModeReception modeReception,
	    SygAutoriteContractante ac, SygTypesCourrier typecourrier,String nature) {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession().createCriteria(SygCourrierAc.class);
	criteria.createAlias("courrierModeReception", "courrierModeReception");
	criteria.createAlias("courrierAutoriteContractante", "courrierAutoriteContractante");
	criteria.createAlias("courrierType", "courrierType");
	criteria.setFetchMode("typesDossiers",FetchMode.JOIN );
	//criteria.createAlias("modetraitement", "modetraitement");
	criteria.createAlias("natureCourrier", "natureCourrier");
	criteria.addOrder(Order.desc("courrierDate"));
	if(code!=null){
	    criteria.add(Restrictions.eq("courrierId", code));
	}
	if(courrierReference!=null){
	    criteria.add(Restrictions.ilike("courrierReference", "%"+courrierReference+"%"));
	}
	if(courrierDateReception!=null){
	    criteria.add(Restrictions.eq("courrierDateReception", courrierDateReception));
	}
	if(courrierDate!=null){
	    criteria.add(Restrictions.eq("courrierDate",courrierDate));
	}
	if(modeReception!=null){
	    criteria.add(Restrictions.eq("courrierModeReception", modeReception));
	}
	if(ac!=null){
	    criteria.add(Restrictions.eq("courrierAutoriteContractante", ac));
	}
	if(typecourrier!=null){
	    criteria.add(Restrictions.eq("courrierType", typecourrier));
	}
	if(nature!=null){
	    criteria.add(Restrictions.eq("nature", nature));
	}
	criteria.setFirstResult(indice);
	if(pas>0) {
	    criteria.setMaxResults(pas);
	}

	return criteria.list();
    }

    @Override
    public SygCourrierAc findById(Long code) {
	// TODO Auto-generated method stub
	return (SygCourrierAc)getHibernateSession().get(SygCourrierAc.class, code);
    }

    @Override
    public SygCourrierAc save(SygCourrierAc courrier,String numero) {
	// TODO Auto-generated method stub
	try {
	    getHibernateSession().save(courrier);
	    parametresGenerauxSession.incrementeCode(numero);
	    return courrier;

	} catch (Exception e) {
	    // TODO: handle exception

	    e.printStackTrace();
	    return null;
	}
    }

    @Override
    public SygCourrierAc update(SygCourrierAc courrier) {
	// TODO Auto-generated method stub
	try{
	    getHibernateSession().merge(courrier);
	    return courrier;
	}catch (Exception e) {
	    e.printStackTrace();
	    return null;
	}
    }

    @Override
    public List<SygCourrierAc> findByDossier(int indice, int pas, Long dossier) {
	Criteria criteria = getHibernateSession().createCriteria(SygCourrierAc.class).setFirstResult(indice);
	criteria.createAlias("courrierModeReception", "courrierModeReception");
	criteria.createAlias("courrierAutoriteContractante", "courrierAutoriteContractante");
	criteria.createAlias("courrierType", "courrierType");
	criteria.createAlias("modetraitement", "modetraitement");
	criteria.createAlias("natureCourrier", "natureCourrier");

	if(dossier!=null) {
	    criteria.createAlias("dossierCourrier", "dossierCourrier");
	    criteria.add(Restrictions.eq("dossierCourrier.id",dossier));
	}
	if(pas>0) {
	    criteria.setMaxResults(pas);
	}
	return criteria.list();
    }

    @Override
    public int countByDossier(Long dossier) {
	Criteria criteria = getHibernateSession().createCriteria(SygCourrierAc.class);
	criteria.createAlias("courrierModeReception", "courrierModeReception");
	criteria.createAlias("courrierAutoriteContractante", "courrierAutoriteContractante");
	criteria.createAlias("courrierType", "courrierType");
	criteria.createAlias("modetraitement", "modetraitement");
	criteria.createAlias("natureCourrier", "natureCourrier");

	if(dossier!=null) {
	    criteria.createAlias("dossierCourrier", "dossierCourrier");
	    criteria.add(Restrictions.eq("dossierCourrier.id",dossier));
	}
	criteria.setProjection(Projections.rowCount());
	return Integer.parseInt(criteria.uniqueResult().toString());
    }
    
    @Override
   	public String getGeneratedCode(String codeParametreGeneral) {
   		String dateCoupee = (Calendar.getInstance().getTime()).toString();
   		dateCoupee = dateCoupee.substring(dateCoupee.length() - 2, dateCoupee.length());
   		Long codeParametre = parametresGenerauxSession.getValeurParametre(codeParametreGeneral);
   		if (codeParametre < 10)
   			return "20"+dateCoupee + "/00" + codeParametre;
   		else if ((codeParametre >= 10) && (codeParametre < 100))
   			return "20"+dateCoupee + "/0" + codeParametre;
   		else
   			return dateCoupee + "/" + codeParametre;
   	}

}
