package sn.ssi.kermel.be.courriers.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygInstruction;

@Remote
public interface InstructionSession {
	
	public void saveInstruction(SygInstruction instruction);
	
	public void updateInstruction(SygInstruction instruction);
	
	public List<SygInstruction> find(int start, int step, String libelle);
	
	public int count(String libelle);

}
