package sn.ssi.kermel.be.courriers.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygInstruction;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class InstructionSessionBean extends AbstractSessionBean implements InstructionSession{

	@Override
	public int count(String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygInstruction.class);
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygInstruction> find(int start, int step, String libelle) {
		// TODO Auto-generated method stub
		
		Criteria criteria = getHibernateSession().createCriteria(SygInstruction.class);
		criteria.setFirstResult(start);
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setMaxResults(step);
		
		return criteria.list();
	}

	@Override
	public void saveInstruction(SygInstruction instruction) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateInstruction(SygInstruction instruction) {
		// TODO Auto-generated method stub
		
	}

}
