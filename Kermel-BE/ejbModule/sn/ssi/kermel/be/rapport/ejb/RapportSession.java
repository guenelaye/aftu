package sn.ssi.kermel.be.rapport.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygRapportARMP;

@Remote
public interface RapportSession {
	public void save(SygRapportARMP rapport);
	public void delete(Long id);
	public List<SygRapportARMP> find(int indice, int pas,String libellerapport,Date Daterapport,Date Datepublication);
	public int count(Date Daterapport,Date Datepublication);
	public void update(SygRapportARMP rapport);
	public SygRapportARMP findById(Long code);
	public List<SygRapportARMP> find(String code);
	List<SygRapportARMP> find(int indice, int pas);
	int count();
	
}
