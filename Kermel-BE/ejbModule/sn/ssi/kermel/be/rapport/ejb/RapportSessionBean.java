package sn.ssi.kermel.be.rapport.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygRapportARMP;
import sn.ssi.kermel.be.rapport.ejb.RapportSession;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class RapportSessionBean extends AbstractSessionBean implements RapportSession {

	@Override
	public int count(Date Daterapport,Date Datepublication) {
		Criteria criteria = getHibernateSession().createCriteria(SygRapportARMP.class);
		if(Daterapport!=null){
			criteria.add(Restrictions.ge("Date", Daterapport));
		}
		if(Datepublication!=null){
			criteria.add(Restrictions.ge("Date", Datepublication));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygRapportARMP.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	@Override
	public void delete(Long idrapport) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygRapportARMP.class, idrapport));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	@Override
	public void save(SygRapportARMP rapport) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(rapport);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygRapportARMP rapport) {
	try{
			getHibernateSession().merge(rapport);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public SygRapportARMP findById(Long code) {
		return (SygRapportARMP)getHibernateSession().get(SygRapportARMP.class, code);
	}
	@Override
	public List<SygRapportARMP> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygRapportARMP.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));	
		return criteria.list();
	}
	@Override
	public List<SygRapportARMP> find(int indice, int pas,String libellerapport,Date Daterapport,Date Datepublication) {
       Criteria criteria = getHibernateSession().createCriteria(SygRapportARMP.class);
		if(libellerapport!=null){
			criteria.add(Restrictions.ge("String", libellerapport));
		}
		if(Daterapport!=null){
			criteria.add(Restrictions.ge("Date", Daterapport));
		}
		if(Datepublication!=null){
			criteria.add(Restrictions.ge("Date", Datepublication));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);	
		return criteria.list();
		}
	@Override
	public List<SygRapportARMP> find(int indice, int pas) {
	   Criteria criteria = getHibernateSession().createCriteria(SygRapportARMP.class);
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);			
		return criteria.list();
		}
	}
