package sn.ssi.kermel.be.tachesafaire.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygTachesafaire;
import sn.ssi.kermel.be.entity.Utilisateur;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class TachesAFaireSessionBean extends AbstractSessionBean implements TachesAFaireSession{

	
	@Override
	public int count(Utilisateur user) {
		Criteria criteria = getHibernateSession().createCriteria(SygTachesafaire.class);
		criteria.createAlias("type", "type");
		criteria.createAlias("users", "users");
		if(user!=null){
			criteria.add(Restrictions.eq("users", user));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygTachesafaire.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygTachesafaire> find(int indice, int pas,Utilisateur user) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTachesafaire.class);
		criteria.createAlias("type", "type");
		criteria.createAlias("users", "users");
		criteria.addOrder(Order.asc("id"));
		if(user!=null){
			criteria.add(Restrictions.eq("users", user));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public List<SygTachesafaire> findByDate(int indice, int pas,Utilisateur user, Date date, Long typeId) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTachesafaire.class);
		criteria.createAlias("type", "type");
		criteria.createAlias("users", "users");
		criteria.addOrder(Order.asc("id"));
		if(user!=null){
			criteria.add(Restrictions.eq("users", user));
		}
		if(typeId!=null){
			criteria.add(Restrictions.eq("type.id", typeId));
		}
		criteria.add(Restrictions.eq("dateauplutard", date));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	

	@Override
	public void save(SygTachesafaire tache) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(tache);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygTachesafaire tache) {
		
		try{
			getHibernateSession().merge(tache);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygTachesafaire findById(Long code) {
		return (SygTachesafaire)getHibernateSession().get(SygTachesafaire.class, code);
	}
	

	@Override
	public SygTachesafaire getContrat(Utilisateur user){
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTachesafaire.class);
		criteria.createAlias("type", "type");
		criteria.createAlias("users", "users");
		
		
		if(user!=null){
			criteria.add(Restrictions.eq("users", user));
		}
		
		if(criteria.list().size()>0)
		  return (SygTachesafaire) criteria.list().get(0);
		else 
			return null;
	}

	@Override
	public List<Object[]> getTacheToDoByType(int start, int step, Utilisateur user){
		Criteria criteria = getHibernateSession().createCriteria(SygTachesafaire.class);
		criteria.createAlias("type", "type");
		criteria.add(Restrictions.eq("fait", 0));
		if(user!=null){
			criteria.add(Restrictions.eq("users", user));
		}
		
		criteria.setProjection(Projections.projectionList()
				.add(Projections.rowCount(),"NBTACHE")
				.add(Projections.property("type.libelle"))
				.add(Projections.property("type.id"))
				.add(Projections.property("libelle"))
				.add(Projections.property("fait"))
				.add(Projections.property("dateauplutard"))
				.add(Projections.property("dateevaluation"))
				.add(Projections.property("date")));
		criteria.addOrder(Order.desc("date"));
				
		

		return criteria.list();
		
	}

	

}
