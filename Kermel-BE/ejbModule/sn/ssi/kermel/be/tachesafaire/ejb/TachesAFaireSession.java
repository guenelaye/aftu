package sn.ssi.kermel.be.tachesafaire.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygTachesafaire;
import sn.ssi.kermel.be.entity.Utilisateur;

@Remote
public interface TachesAFaireSession {
	public void save(SygTachesafaire tache);
	public void delete(Long id);
	public List<SygTachesafaire> find(int indice, int pas,Utilisateur user);
	public int count(Utilisateur user);
	public void update(SygTachesafaire tache);
	public SygTachesafaire findById(Long code);
	
	public SygTachesafaire getContrat(Utilisateur user);
	List<Object[]> getTacheToDoByType(int start, int step, Utilisateur user);
	List<SygTachesafaire> findByDate(int indice, int pas, Utilisateur user,
			Date date, Long typeId);
}
