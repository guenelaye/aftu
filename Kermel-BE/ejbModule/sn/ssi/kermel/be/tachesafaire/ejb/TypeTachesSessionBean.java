package sn.ssi.kermel.be.tachesafaire.ejb;

import java.util.List;

import sn.ssi.kermel.common.ejb.AbstractSessionBean;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygTypesTaches;
import sn.ssi.kermel.be.tachesafaire.ejb.TypeTachesSession;

public @Stateless class TypeTachesSessionBean extends AbstractSessionBean implements TypeTachesSession {

	@Override
	public List<SygTypesTaches> find(int start, int step, String code,
			String libelle) {
		Criteria  criteria = getHibernateSession().createCriteria(SygTypesTaches.class);
		criteria.setFirstResult(start);
		if(code!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		if(step>0){
			criteria.setMaxResults(step);
		}
		return criteria.list();
	}

	@Override
	public int count(String code, String libelle) {
		Criteria  criteria = getHibernateSession().createCriteria(SygTypesTaches.class);
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

}
