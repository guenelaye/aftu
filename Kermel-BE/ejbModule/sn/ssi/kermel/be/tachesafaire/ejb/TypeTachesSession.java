package sn.ssi.kermel.be.tachesafaire.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygTypesTaches;

@Remote
public interface TypeTachesSession {

	public List<SygTypesTaches> find(int start, int step, String code, String libelle);
	public int count(String code, String libelle);
}
