package sn.ssi.kermel.be.archivage.ejb;

import java.util.List;

import javax.ejb.Remote;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygNoeudClassement;
import sn.ssi.kermel.be.entity.SygTypeElementArbre;




@Remote
public interface SygTypeElementArbreSession {
	public void save(SygTypeElementArbre type);
	public void delete(Long id);
	public int count();
	public void update(SygTypeElementArbre type);
	public SygTypeElementArbre findById(Long id);
	public List<SygTypeElementArbre> find(int indice, int pas);
	public List<SygTypeElementArbre> findAll();
	//public List<SygTypeElementArbre> findByLevel(int level);
}
