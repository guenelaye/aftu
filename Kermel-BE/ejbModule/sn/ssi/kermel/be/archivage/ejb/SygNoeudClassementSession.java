package sn.ssi.kermel.be.archivage.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygNoeudClassement;

@Remote
public interface SygNoeudClassementSession {
	public void save(SygNoeudClassement type);
	public void delete(Long id);
	public int count();
	public int count(Long id);
	public void update(SygNoeudClassement type);
	public SygNoeudClassement findById(Long id);
	public List<SygNoeudClassement> find(int indice, int pas);
	public List<SygNoeudClassement> findByLevel(Long level);
	public List<SygNoeudClassement> findByNode(Long codeNodeParent);
	public List<SygNoeudClassement> findAll();
	
}
