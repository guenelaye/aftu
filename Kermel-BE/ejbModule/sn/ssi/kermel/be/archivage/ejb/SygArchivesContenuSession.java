package sn.ssi.kermel.be.archivage.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygArchivesContenu;



@Remote
public interface SygArchivesContenuSession {
	public void save(SygArchivesContenu contenu);
	public void delete(Long id);
	public int count();
	public void update(SygArchivesContenu contenu);
	public SygArchivesContenu findById(Long id);
	public List<SygArchivesContenu> find(int indice, int pas);
	List<SygArchivesContenu> findAllArchived(Long id);
	List<SygArchivesContenu> findAllToArchive();
}
