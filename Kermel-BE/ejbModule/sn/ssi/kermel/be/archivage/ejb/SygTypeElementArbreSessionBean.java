package sn.ssi.kermel.be.archivage.ejb;



import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.denonciation.ejb.DecisionDenonciationSession;
import sn.ssi.kermel.be.entity.SygNoeudClassement;
import sn.ssi.kermel.be.entity.SygTypeElementArbre;
import sn.ssi.kermel.be.entity.SygTypesmarches;
import sn.ssi.kermel.be.archivage.ejb.SygTypeElementArbreSession;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;



@Stateless
public class SygTypeElementArbreSessionBean extends AbstractSessionBean implements SygTypeElementArbreSession{

	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygTypeElementArbre.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygTypeElementArbre.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void save(SygTypeElementArbre type) {
		// TODO Auto-generated method stub
				
		try {
			getHibernateSession().save(type);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygTypeElementArbre type) {
		
		try{
			getHibernateSession().merge(type);
			getHibernateSession().flush();
		}catch (Exception e) {
			System.out.println("--------------SAVE----------------------");
			e.printStackTrace();
			
		}
	}
	
	@Override
	public SygTypeElementArbre findById(Long id) {
		return (SygTypeElementArbre)getHibernateSession().get(SygTypeElementArbre.class, id);
	}
	
	@Override
	public List<SygTypeElementArbre> find(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTypeElementArbre.class);
		//criteria.addOrder(Order.desc("id"));
				
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public List<SygTypeElementArbre> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTypeElementArbre.class);
		criteria.addOrder(Order.asc("niveau"));
		return criteria.list();
	}
	
	
	
	/*@Override
	public List<SygNoeudClassement> findByNode(Long codeNodeParent){
		;
	}*/
}
