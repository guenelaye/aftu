package sn.ssi.kermel.be.archivage.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygArchivesContenu;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class SygArchivesContenuSessionBean extends AbstractSessionBean
		implements SygArchivesContenuSession {

	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(
				SygArchivesContenu.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(SygArchivesContenu.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void save(SygArchivesContenu type) {
		// TODO Auto-generated method stub

		try {
			getHibernateSession().save(type);
			// getHibernateSession().flush();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void update(SygArchivesContenu type) {

		try {
			getHibernateSession().merge(type);
			getHibernateSession().flush();
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Override
	public SygArchivesContenu findById(Long id) {
		return (SygArchivesContenu) getHibernateSession().get(
				SygArchivesContenu.class, id);
	}

	@Override
	public List<SygArchivesContenu> find(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygArchivesContenu.class);
		// criteria.addOrder(Order.desc("id"));

		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

	@Override
	public List<SygArchivesContenu> findAllArchived(Long id) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygArchivesContenu.class);

		criteria.setFetchMode("conteneur", FetchMode.JOIN);
		criteria.createAlias("conteneur", "cnt");
		criteria.add(Restrictions.eq("cnt.id", id));

		return criteria.list();
	}
	@Override
	public List<SygArchivesContenu> findAllToArchive() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygArchivesContenu.class);

		criteria.add(Restrictions.isNull("conteneur"));

		return criteria.list();
	}

}
