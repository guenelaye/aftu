package sn.ssi.kermel.be.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pkl_typealerte")
public class PklTypeAlerte implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer Id;
    private String description;
    private String Libelle;
    

	public PklTypeAlerte() {
    }

    public PklTypeAlerte(Integer Id, String description, String Libelle) {
	super();
	this.Id = Id;
	this.description = description;
	this.Libelle = Libelle;

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false, length = 10)
    public Integer getId() {
	return this.Id;
    }

    public void setId(Integer id) {
	Id = id;
    }

    @Column(name = "description", length = 200)
    public String getCode() {
	return this.description;
    }

    public void setCode(String Code) {
	this.description = Code;
    }

    @Column(name = "LIBELLE", nullable = false, length = 50)
    public String getLibelle() {
	return this.Libelle;
    }

    public void setLibelle(String Libelle) {
	this.Libelle = Libelle;
    }

}
