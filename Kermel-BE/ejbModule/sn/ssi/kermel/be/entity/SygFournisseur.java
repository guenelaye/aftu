package sn.ssi.kermel.be.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "pmb_fournisseurs")
public class SygFournisseur implements java.io.Serializable {

	private Long id;
	private String nom, adresse, email, tel, fax;
	private SygCatFournisseur categorie;
	private SygAutoriteContractante autorite;
	private SygPays pays;
	private String ninea;
	private String logo;
	private String codePostal;
	private String siteWeb;
	private String registreCommerce;
	
	/* pour le responsable moral */
	private String rmNom;
	private String rmPrenom;
	private String rmTel;
	private String rmEmail;
	private String rmFonction;
	private String rmAdresse;

	public SygFournisseur() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "raisonSociale", length = 2)
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Column(name = "adresse")
	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	@Column(name = "email", length = 20)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "fax", length = 15)
	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Column(name = "telephone", length = 15)
	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "categoriefournisseurs_id")
	public SygCatFournisseur getCategorie() {
		return categorie;
	}

	public void setCategorie(SygCatFournisseur categorie) {
		this.categorie = categorie;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Autorite_ID")
	public SygAutoriteContractante getAutorite() {
		return autorite;
	}

	public void setAutorite(SygAutoriteContractante autorite) {
		this.autorite = autorite;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ORIGINE")
	public SygPays getPays() {
		return pays;
	}

	public void setPays(SygPays pays) {
		this.pays = pays;
	}

	public void setNinea(String ninea) {
		this.ninea = ninea;
	}

	@Column(name = "ninea")
	public String getNinea() {
		return ninea;
	}

	@Column(name = "logo")
	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	@Column(name = "codepostal", length=10)
	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	@Column(name = "siteweb", length=100)
	public String getSiteWeb() {
		return siteWeb;
	}

	public void setSiteWeb(String siteWeb) {
		this.siteWeb = siteWeb;
	}

	@Column(name = "registrecommerce", length=100)
	public String getRegistreCommerce() {
		return registreCommerce;
	}

	public void setRegistreCommerce(String registreCommerce) {
		this.registreCommerce = registreCommerce;
	}

	@Column(name = "rmnom", length=100)
	public String getRmNom() {
		return rmNom;
	}

	public void setRmNom(String rmNom) {
		this.rmNom = rmNom;
	}

	@Column(name = "rmprenom", length=100)
	public String getRmPrenom() {
		return rmPrenom;
	}

	public void setRmPrenom(String rmPrenom) {
		this.rmPrenom = rmPrenom;
	}
	
	

	@Column(name = "rmtel", length=20)
	public String getRmTel() {
		return rmTel;
	}

	public void setRmTel(String rmTel) {
		this.rmTel = rmTel;
	}

	@Column(name = "rmemail", length=100)
	public String getRmEmail() {
		return rmEmail;
	}

	public void setRmEmail(String rmEmail) {
		this.rmEmail = rmEmail;
	}

	@Column(name = "rmfonction")
	public String getRmFonction() {
		return rmFonction;
	}

	public void setRmFonction(String rmFonction) {
		this.rmFonction = rmFonction;
	}

	@Column(name = "rmadresse")
	public String getRmAdresse() {
		return rmAdresse;
	}

	public void setRmAdresse(String rmAdresse) {
		this.rmAdresse = rmAdresse;
	}
	
	
	
	
	
	
}
