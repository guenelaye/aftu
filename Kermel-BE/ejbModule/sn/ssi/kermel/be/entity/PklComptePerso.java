package sn.ssi.kermel.be.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "pkl_compteperso")
public class PklComptePerso implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String login;
	private String password;
	private String prenom;
	private String nom;
	private String mail, fonction, telephone, fax, adresse, Idu;
	private SygFournisseur fournisseur;
	private PklComptePerso user;
	private Profil profil;
	private StatutCompte statut = StatutCompte.INACTIVE;
	private Date dateInscription;
	private Date dateActivation;
	private String hash;

	public enum Profil {
		ADMIN, INTERNE
	}
	
	public enum StatutCompte {
		ACTIVE, INACTIVE, ATTENTE
	}
	
	

	public PklComptePerso() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "login", length = 200)
	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Column(name = "password", length = 200)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "prenom", length = 200)
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@Column(name = "nom", length = 200)
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Column(name = "mail", length = 200)
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@Column(name = "fonction")
	public String getFonction() {
		return fonction;
	}

	public void setFonction(String fonction) {
		this.fonction = fonction;
	}

	@Column(name = "telephone")
	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Column(name = "fax")
	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Column(name = "adresse")
	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	@Column(name = "Idu")
	public String getIdu() {
		return Idu;
	}

	public void setIdu(String Idu) {
		this.Idu = Idu;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fournisseur")
	public SygFournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(SygFournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public void setUser(PklComptePerso user) {
		this.user = user;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user")
	public PklComptePerso getUser() {
		return user;
	}

	@Column(name="profil")
	@Enumerated(EnumType.STRING)
	public Profil getProfil() {
		return profil;
	}

	public void setProfil(Profil profil) {
		this.profil = profil;
	}

	@Column(name="statut")
	@Enumerated(EnumType.STRING)
	public StatutCompte getStatut() {
		return statut;
	}

	public void setStatut(StatutCompte statut) {
		this.statut = statut;
	}

	@Column(name="dateinscription")
	@Temporal(TemporalType.DATE)
	public Date getDateInscription() {
		return dateInscription;
	}

	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}

	@Column(name="dateactivation")
	@Temporal(TemporalType.DATE)
	public Date getDateActivation() {
		return dateActivation;
	}

	public void setDateActivation(Date dateActivation) {
		this.dateActivation = dateActivation;
	}

	@Column(name="hash")
	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}
	
	

}
