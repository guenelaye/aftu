package sn.ssi.kermel.be.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
	
	@SuppressWarnings("serial")
	@Entity
	@Table(name = "syg_secteuractivites")
	public class SygSecteursactivites  implements java.io.Serializable {
		
  		private String code;
		private String libelle,description;
		private SygSecteursactivites secteur;

	



		public SygSecteursactivites() {
		}

	

		@Id
		@Column(name = "CODE", nullable = false)
        public String getCode() {
			return this.code;
		}
		public void setCode(String code) {
			this.code = code;
		}

	

		@Column(name = "LIBELLE",  length = 255)
		public String getLibelle() {
			return this.libelle;
		}

		public void setLibelle(String libelle) {
			this.libelle = libelle;
		}


		@Column(name = "DESCRIPTION",  length = 255)
		public String getDescription() {
			return description;
		}
    	public void setDescription(String description) {
			this.description = description;
		}



    	@ManyToOne(fetch = FetchType.EAGER)
    	@JoinColumn(name = "SECTEUR")
    	public SygSecteursactivites getSecteur() {
			return secteur;
		}

		public void setSecteur(SygSecteursactivites secteur) {
			this.secteur = secteur;
		}
        
    	
    	
	}

