package sn.ssi.kermel.be.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "sys_utilisateurprofil")
public class UtilisateurProfil implements java.io.Serializable {

	private UtilisateurProfilId id;

	public UtilisateurProfil() {
	}

	public UtilisateurProfil(final UtilisateurProfilId id) {
		this.id = id;
	}

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "usrId", column = @Column(name = "USR_ID", nullable = false)),
			@AttributeOverride(name = "pfCode", column = @Column(name = "PF_CODE", nullable = false, length = 32)) })
	public UtilisateurProfilId getId() {
		return id;
	}

	public void setId(final UtilisateurProfilId id) {
		this.id = id;
	}

}
