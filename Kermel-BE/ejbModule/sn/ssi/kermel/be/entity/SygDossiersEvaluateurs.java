package sn.ssi.kermel.be.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
	
	@SuppressWarnings("serial")
	@Entity
	@Table(name = "pmb_evaluateursdossier")
public class SygDossiersEvaluateurs implements java.io.Serializable {
     
		private Long id;
		private String commission;
		private SygDossiers dossier;
		private SygEvaluateur evaluateur;
		private Date Dateremise,Datelimite;	
		
		

		

		public SygDossiersEvaluateurs(){
		}


		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name = "ID",  length = 10)
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

     
      


		@ManyToOne(fetch = FetchType.EAGER)
    	@JoinColumn(name = "Dossiers_ID")
		public SygDossiers getDossier() {
			return dossier;
		}
    	public void setDossier(SygDossiers dossier) {
			this.dossier = dossier;
		}



		
		@Column(name = "commission",  length = 50)
		public String getCommission() {
			return commission;
		}

		public void setCommission(String commission) {
			this.commission = commission;
		}

		
		@ManyToOne(fetch = FetchType.EAGER)
    	@JoinColumn(name = "Evaluateur_ID")
		public SygEvaluateur getEvaluateur() {
			return evaluateur;
		}
    	public void setEvaluateur(SygEvaluateur evaluateur) {
			this.evaluateur = evaluateur;
		}

		
    	@Temporal(TemporalType.DATE)
    	@Column(name = "Dateremise", length = 7)
    	public Date getDateremise() {
			return Dateremise;
		}
		public void setDateremise(Date dateremise) {
			Dateremise = dateremise;
		}

		@Temporal(TemporalType.DATE)
    	@Column(name = "Datelimite", length = 7)
    	public Date getDatelimite() {
			return Datelimite;
		}
    	public void setDatelimite(Date datelimite) {
			Datelimite = datelimite;
		}

}
