package sn.ssi.kermel.be.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "pkl_userenveloppe")
public class UserEnveloppe implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
    private Date dateEntree;
    private PklComptePerso   user;
    private SygPresentationsOffres  enveloppe;
  
    

   

	public UserEnveloppe() {
    }

   

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false, length = 10)
    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

   

    

	public void setDateEntree(Date dateEntree) {
		this.dateEntree = dateEntree;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "datesaisie" )
	public Date getDateEntree() {
		return dateEntree;
	}

	
	



	public void setEnveloppe(SygPresentationsOffres enveloppe) {
		this.enveloppe = enveloppe;
	}


	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "enveloppe")
	public SygPresentationsOffres getEnveloppe() {
		return enveloppe;
	}



	public void setUser(PklComptePerso user) {
		this.user = user;
	}


	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user")
	public PklComptePerso getUser() {
		return user;
	}

}
