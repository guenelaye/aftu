package sn.ssi.kermel.be.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "eco_groupesimputilisateur")
public class EcoGroupesImputilisateur  implements java.io.Serializable {
	
		private Long id;
	private String libelle;
	private String description;

	private EcoGroupesImputation groupesImputation;
	private Utilisateur utilisateur;

	public EcoGroupesImputilisateur() {
	}



	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID",  length = 10)
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}



	@Column(name = "LIBELLE",  length = 255)
	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@Column(name = "description",  length = 255)

	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}


 	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "GroupesImputation")

	public EcoGroupesImputation getGroupesImputation() {
		return groupesImputation;
	}
	public void setGroupesImputation(EcoGroupesImputation groupesImputation) {
		this.groupesImputation = groupesImputation;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Utilisateur")
	
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
}