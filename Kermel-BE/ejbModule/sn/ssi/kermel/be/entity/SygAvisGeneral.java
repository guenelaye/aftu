package sn.ssi.kermel.be.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
	
	@SuppressWarnings("serial")
	@Entity
	@Table(name = "pmb_avisgeneral")
	public class SygAvisGeneral  implements java.io.Serializable {
		
  		private Long id;  
		//private SygRealisations realisation;
		private SygAutoriteContractante autorite;
		private String Numero,fichier_Avis,annee;
//		private String NomService,NomAgentResponsable,PrenomResponsable,Adressecomplete,NumeroTelephone,NumeroTelecopieur,
//	     commentaireValidation,commentaireMiseEnvalidation,motif,fichier_validation,etatValidAvis,BorderauAvis,texteAvisGeneral;
//		private Date Datepublication,dateValidation,dateCreation,dateMiseEnvalidation,dateRejet,DateAuPlusTard;
//		private int Etatmarches,version,lastVersionValid;
		private int Etat,soumissionelectronique;
		private Date Datepublication;
		
    	public SygAvisGeneral() {
    		super();
		}

	

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name = "ID",  length = 255)
        public Long getId() {
			return this.id;
		}
		public void setId(Long id) {
			this.id = id;
		}

	

		@Column(name = "Numero",  length = 50)
		public String getNumero() {
			return this.Numero;
		}

		public void setNumero(String Numero) {
			this.Numero = Numero;
		}


       
//		
//    	@ManyToOne(fetch = FetchType.EAGER)
//    	@JoinColumn(name = "InfoPlan_ID")
//    	public SygRealisations getRealisation() {
//			return realisation;
//		}
//    	public void setRealisation(SygRealisations realisation) {
//			this.realisation = realisation;
//		}


    	@ManyToOne(fetch = FetchType.EAGER)
    	@JoinColumn(name = "Autorite_ID")
		public SygAutoriteContractante getAutorite() {
			return autorite;
		}
    	public void setAutorite(SygAutoriteContractante autorite) {
			this.autorite = autorite;
		}


//    	@Column(name = "Commentaires")
//		public String getCommentaires() {
//			return Commentaires;
//		}
//    	public void setCommentaires(String Commentaires) {
//			this.Commentaires = Commentaires;
//		}


    	@Temporal(TemporalType.DATE)
    	@Column(name = "Datepublication", length = 7)
		public Date getDatepublication() {
			return Datepublication;
		}
    	public void setDatepublication(Date datepublication) {
			Datepublication = datepublication;
		}





    	@Column(name = "fichier_Avis")
		public String getFichier_Avis() {
			return fichier_Avis;
		}
    	public void setFichier_Avis(String fichier_Avis) {
			this.fichier_Avis = fichier_Avis;
		}




    	@Column(name = "annee")
		public String getAnnee() {
			return annee;
		}
    	public void setAnnee(String annee) {
			this.annee = annee;
		}


    	@Column(name = "Etat")
		public int getEtat() {
			return Etat;
		}

		public void setEtat(int etat) {
			Etat = etat;
		}


		@Column(name = "soumelectron")
		public int getSoumissionelectronique() {
			return soumissionelectronique;
		}

		public void setSoumissionelectronique(int soumissionelectronique) {
			this.soumissionelectronique = soumissionelectronique;
		}




    	
    	
    	
	}

