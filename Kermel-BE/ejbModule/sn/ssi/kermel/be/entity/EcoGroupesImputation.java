package sn.ssi.kermel.be.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "eco_groupesimputation")
public class EcoGroupesImputation  implements java.io.Serializable {
	
		private Long id;
	private String libelle;
	private String description;

	


	public EcoGroupesImputation() {
	}



	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID",  length = 10)
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}



	@Column(name = "LIBELLE",  length = 255)
	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@Column(name = "description",  length = 255)

	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	

	
	
	
}

