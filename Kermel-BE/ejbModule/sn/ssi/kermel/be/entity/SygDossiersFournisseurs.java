package sn.ssi.kermel.be.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
	
	@SuppressWarnings("serial")
	@Entity
	@Table(name = "pmb_marchefournisseurs")
	public class SygDossiersFournisseurs  implements java.io.Serializable {
		
  		private Long id;
	    private SygFournisseur fournisseur;
		private SygAppelsOffres appel;
		private String commentaire,lettreInvitation,termeRef;
		private Date dateinvitation;
		private SygDossiers dossier;
		
	
     	public SygDossiersFournisseurs() {
		}

	

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name = "ID",  length = 255)
        public Long getId() {
			return this.id;
		}
		public void setId(Long id) {
			this.id = id;
		}

	
		@ManyToOne(fetch = FetchType.EAGER)
    	@JoinColumn(name = "Fournisseurs_ID")
        public SygFournisseur getFournisseur() {
			return fournisseur;
		}
    	public void setFournisseur(SygFournisseur fournisseur) {
			this.fournisseur = fournisseur;
		}


    	@ManyToOne(fetch = FetchType.EAGER)
    	@JoinColumn(name = "Appelsoffres_ID")
		public SygAppelsOffres getAppel() {
			return appel;
		}
    	public void setAppel(SygAppelsOffres appel) {
			this.appel = appel;
		}
    	
    	@Temporal(TemporalType.DATE  )
    	@Column(name = "dateinvitation", length = 7)
		public Date getDateinvitation() {
			return dateinvitation;
		}
     	public void setDateinvitation(Date dateinvitation) {
			this.dateinvitation = dateinvitation;
		}


     	@Column(name = "commentaire")
		public String getCommentaire() {
			return commentaire;
		}
    	public void setCommentaire(String commentaire) {
			this.commentaire = commentaire;
		}


    	@Column(name = "lettreInvitation")
		public String getLettreInvitation() {
			return lettreInvitation;
		}
    	public void setLettreInvitation(String lettreInvitation) {
			this.lettreInvitation = lettreInvitation;
		}


    	@Column(name = "termeRef")
		public String getTermeRef() {
			return termeRef;
		}
    	public void setTermeRef(String termeRef) {
			this.termeRef = termeRef;
		}


    	@ManyToOne(fetch = FetchType.LAZY)
    	@JoinColumn(name = "DOSSIER_ID")
		public SygDossiers getDossier() {
			return dossier;
		}
    	public void setDossier(SygDossiers dossier) {
			this.dossier = dossier;
		}
    	
    	

	}

