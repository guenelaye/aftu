package sn.ssi.kermel.be.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "syg_contenus_enveloppesoffres")
public class SygContenusEnveloppesOffres  implements java.io.Serializable {
	
		private Long id;
	    private String libelle;
	    private SygPresentationsOffres enveloppe;
	    private int affichage,signature;


	public SygContenusEnveloppesOffres() {
	}



	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID",  length = 10)
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}



	@Column(name = "LIBELLE",  length = 255)
	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}



	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "enveloppe")
	public SygPresentationsOffres getEnveloppe() {
		return enveloppe;
	}
	public void setEnveloppe(SygPresentationsOffres enveloppe) {
		this.enveloppe = enveloppe;
	}
	
	@Column(name = "affichage",  length = 1)
	public int getAffichage() {
		return affichage;
	}
 	public void setAffichage(int affichage) {
		this.affichage = affichage;
	}


 	@Column(name = "signature",  length = 1)
	public int getSignature() {
		return signature;
	}
 	public void setSignature(int signature) {
		this.signature = signature;
	}


	
}

