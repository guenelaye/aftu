package sn.ssi.kermel.be.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
	
	@SuppressWarnings("serial")
	@Entity
	@Table(name = "pmb_typeuniteorgarmp")
	public class SygTypeUniteOrgarmp  implements java.io.Serializable {
		
  		private Long id;
		private String libelle;
		private int niveau;
		private SygTypeUniteOrgarmp typeuniteorg;
	
		public SygTypeUniteOrgarmp() {
		}

	

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name = "ID",  length = 10)
		public Long getId() {
			return this.id;
		}
		public void setId(Long id) {
			this.id = id;
		}

	

		@Column(name = "LIBELLE",  length = 255)
		public String getLibelle() {
			return this.libelle;
		}

		public void setLibelle(String libelle) {
			this.libelle = libelle;
		}


		@Column(name = "NIVEAU")
		public int getNiveau() {
			return niveau;
		}



		public void setNiveau(int niveau) {
			this.niveau = niveau;
		}


		@ManyToOne(fetch = FetchType.EAGER)
		@JoinColumn(name = "typeuniteorgID")
		public SygTypeUniteOrgarmp getTypeuniteorg() {
			return typeuniteorg;
		}



		public void setTypeuniteorg(SygTypeUniteOrgarmp typeuniteorg) {
			this.typeuniteorg = typeuniteorg;
		}


		
		
	}

