package sn.ssi.kermel.be.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
	
	@SuppressWarnings("serial")
	@Entity
	@Table(name = "pmb_contrats")
	public class SygContrats  implements java.io.Serializable {
		
  		private Long conID;
		private SygPlisouvertures plis;
		private SygAutoriteContractante autorite;
		private SygDossiers dossier;
		private BigDecimal montant,montantverse;
		private Date conDateSignature,conDateApprobation,conDateNotification,conDateRecepProvisoire,conDateRecepDefinitive,conDateOrdreDemarrage
		,condateAttributionprovisoire,condateAttributiondefinitive,condatepaiement,datedemandeimmatriculation,dateimmatriculation;
		private String conCommentSignature,conRefSignature,conCommentApprobation,conRefApprobation,conCommentNotification,conRefNotification,
		conFichierRecepProvisoire,conCommentRecepProvisoire,conFichierRecepDefinitive,conCommentRecepDefinitive,conFichierOrdreDemarrage,
		conCommentOrdreDemarrage,conrefAttributionprovisoire,concommentaireAttributionprovisoire,conrefAttributiondefinitive,concommentaireAttributiondefinitive,
		constatus,nummatriculation,concommentairesdmdmat,concommentairesmatriculation,concsituation;
		private int immatriculation;
		private SygLots lot;
		private SygService service;
		private SygFournisseur fournisseur;
		private SygModepassation modepassation;
		private SygTypesmarches typemarche;
    
	
    	public SygContrats() {
		}

	

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name = "conID",  length = 255)
        public Long getConID() {
			return this.conID;
		}
		public void setConID(Long conID) {
			this.conID = conID;
		}

	

	
		
    	@ManyToOne(fetch = FetchType.EAGER)
    	@JoinColumn(name = "Pli_ID")
    	public SygPlisouvertures getPlis() {
			return plis;
		}
    	public void setPlis(SygPlisouvertures plis) {
			this.plis = plis;
		}




    	@ManyToOne(fetch = FetchType.EAGER)
    	@JoinColumn(name = "Autorite_ID")
		public SygAutoriteContractante getAutorite() {
			return autorite;
		}
    	public void setAutorite(SygAutoriteContractante autorite) {
			this.autorite = autorite;
		}


    	@ManyToOne(fetch = FetchType.EAGER)
    	@JoinColumn(name = "Dossiers_ID")
		public SygDossiers getDossier() {
			return dossier;
		}
    	public void setDossier(SygDossiers dossier) {
			this.dossier = dossier;
		}


    	@Column(name = "montant")
		public BigDecimal getMontant() {
			return montant;
		}
		public void setMontant(BigDecimal montant) {
			this.montant = montant;
		}



		@Temporal(TemporalType.DATE)
		@Column(name = "conDateSignature", length = 7)
		public Date getConDateSignature() {
			return conDateSignature;
		}
		public void setConDateSignature(Date conDateSignature) {
			this.conDateSignature = conDateSignature;
		}


		@Temporal(TemporalType.DATE)
		@Column(name = "conDateApprobation", length = 7)
		public Date getConDateApprobation() {
			return conDateApprobation;
		}
    	public void setConDateApprobation(Date conDateApprobation) {
			this.conDateApprobation = conDateApprobation;
		}


    	@Temporal(TemporalType.DATE)
		@Column(name = "conDateNotification", length = 7)
		public Date getConDateNotification() {
			return conDateNotification;
		}
    	public void setConDateNotification(Date conDateNotification) {
			this.conDateNotification = conDateNotification;
		}


    	@Column(name = "conCommentSignature", length = 255)
		public String getConCommentSignature() {
			return conCommentSignature;
		}
    	public void setConCommentSignature(String conCommentSignature) {
			this.conCommentSignature = conCommentSignature;
		}


    	@Column(name = "conRefSignature", length = 255)
		public String getConRefSignature() {
			return conRefSignature;
		}
    	public void setConRefSignature(String conRefSignature) {
			this.conRefSignature = conRefSignature;
		}


    	@Column(name = "conCommentApprobation", length = 255)
		public String getConCommentApprobation() {
			return conCommentApprobation;
		}
 		public void setConCommentApprobation(String conCommentApprobation) {
			this.conCommentApprobation = conCommentApprobation;
		}


 		@Column(name = "conRefApprobation", length = 255)
		public String getConRefApprobation() {
			return conRefApprobation;
		}
    	public void setConRefApprobation(String conRefApprobation) {
			this.conRefApprobation = conRefApprobation;
		}


    	@Column(name = "conCommentNotification", length = 255)
		public String getConCommentNotification() {
			return conCommentNotification;
		}
    	public void setConCommentNotification(String conCommentNotification) {
			this.conCommentNotification = conCommentNotification;
		}


    	@Column(name = "conRefNotification", length = 255)
		public String getConRefNotification() {
			return conRefNotification;
		}
    	public void setConRefNotification(String conRefNotification) {
			this.conRefNotification = conRefNotification;
		}


    	@Temporal(TemporalType.DATE)
		@Column(name = "conDateRecepProvisoire", length = 7)
		public Date getConDateRecepProvisoire() {
			return conDateRecepProvisoire;
		}
    	public void setConDateRecepProvisoire(Date conDateRecepProvisoire) {
			this.conDateRecepProvisoire = conDateRecepProvisoire;
		}


		@Temporal(TemporalType.DATE)
		@Column(name = "conDateRecepDefinitive", length = 7)
		public Date getConDateRecepDefinitive() {
			return conDateRecepDefinitive;
		}
    	public void setConDateRecepDefinitive(Date conDateRecepDefinitive) {
			this.conDateRecepDefinitive = conDateRecepDefinitive;
		}


		@Temporal(TemporalType.DATE)
		@Column(name = "conDateOrdreDemarrage", length = 7)
		public Date getConDateOrdreDemarrage() {
			return conDateOrdreDemarrage;
		}
    	public void setConDateOrdreDemarrage(Date conDateOrdreDemarrage) {
			this.conDateOrdreDemarrage = conDateOrdreDemarrage;
		}


		@Temporal(TemporalType.DATE)
		@Column(name = "condateAttributionprovisoire", length = 7)
		public Date getCondateAttributionprovisoire() {
			return condateAttributionprovisoire;
		}
		public void setCondateAttributionprovisoire(Date condateAttributionprovisoire) {
			this.condateAttributionprovisoire = condateAttributionprovisoire;
		}

		
		@Column(name = "conFichierRecepProvisoire", length = 255)
    	public String getConFichierRecepProvisoire() {
			return conFichierRecepProvisoire;
		}
    	public void setConFichierRecepProvisoire(String conFichierRecepProvisoire) {
			this.conFichierRecepProvisoire = conFichierRecepProvisoire;
		}


    	@Column(name = "conCommentRecepProvisoire", length = 255)
		public String getConCommentRecepProvisoire() {
			return conCommentRecepProvisoire;
		}
    	public void setConCommentRecepProvisoire(String conCommentRecepProvisoire) {
			this.conCommentRecepProvisoire = conCommentRecepProvisoire;
		}


    	@Column(name = "conFichierRecepDefinitive", length = 255)
		public String getConFichierRecepDefinitive() {
			return conFichierRecepDefinitive;
		}
    	public void setConFichierRecepDefinitive(String conFichierRecepDefinitive) {
			this.conFichierRecepDefinitive = conFichierRecepDefinitive;
		}


    	@Column(name = "conCommentRecepDefinitive", length = 255)
		public String getConCommentRecepDefinitive() {
			return conCommentRecepDefinitive;
		}
    	public void setConCommentRecepDefinitive(String conCommentRecepDefinitive) {
			this.conCommentRecepDefinitive = conCommentRecepDefinitive;
		}


    	@Column(name = "conFichierOrdreDemarrage", length = 255)
		public String getConFichierOrdreDemarrage() {
			return conFichierOrdreDemarrage;
		}
    	public void setConFichierOrdreDemarrage(String conFichierOrdreDemarrage) {
			this.conFichierOrdreDemarrage = conFichierOrdreDemarrage;
		}


    	@Column(name = "conCommentOrdreDemarrage", length = 255)
		public String getConCommentOrdreDemarrage() {
			return conCommentOrdreDemarrage;
		}
    	public void setConCommentOrdreDemarrage(String conCommentOrdreDemarrage) {
			this.conCommentOrdreDemarrage = conCommentOrdreDemarrage;
		}


    	@Column(name = "conrefAttributionprovisoire", length = 255)
		public String getConrefAttributionprovisoire() {
			return conrefAttributionprovisoire;
		}
    	public void setConrefAttributionprovisoire(String conrefAttributionprovisoire) {
			this.conrefAttributionprovisoire = conrefAttributionprovisoire;
		}


    	@Column(name = "concommentaireAttributionprovisoire", length = 255)
		public String getConcommentaireAttributionprovisoire() {
			return concommentaireAttributionprovisoire;
		}
    	public void setConcommentaireAttributionprovisoire(
				String concommentaireAttributionprovisoire) {
			this.concommentaireAttributionprovisoire = concommentaireAttributionprovisoire;
		}


    	@Temporal(TemporalType.DATE)
		@Column(name = "condateAttributiondefinitive", length = 7)
		public Date getCondateAttributiondefinitive() {
			return condateAttributiondefinitive;
		}
    	public void setCondateAttributiondefinitive(Date condateAttributiondefinitive) {
			this.condateAttributiondefinitive = condateAttributiondefinitive;
		}


    	@Column(name = "conrefAttributiondefinitive", length = 255)
		public String getConrefAttributiondefinitive() {
			return conrefAttributiondefinitive;
		}
    	public void setConrefAttributiondefinitive(String conrefAttributiondefinitive) {
			this.conrefAttributiondefinitive = conrefAttributiondefinitive;
		}


    	@Column(name = "concommentaireAttributiondefinitive", length = 255)
		public String getConcommentaireAttributiondefinitive() {
			return concommentaireAttributiondefinitive;
		}
		public void setConcommentaireAttributiondefinitive(
				String concommentaireAttributiondefinitive) {
			this.concommentaireAttributiondefinitive = concommentaireAttributiondefinitive;
		}


		@Temporal(TemporalType.DATE)
		@Column(name = "condatepaiement", length = 7)
		public Date getCondatepaiement() {
			return condatepaiement;
		}
    	public void setCondatepaiement(Date condatepaiement) {
			this.condatepaiement = condatepaiement;
		}


    	@Column(name = "constatus", length = 10)
		public String getConstatus() {
			return constatus;
		}
    	public void setConstatus(String constatus) {
			this.constatus = constatus;
		}


    	@Column(name = "montantverse")
		public BigDecimal getMontantverse() {
			return montantverse;
		}
    	public void setMontantverse(BigDecimal montantverse) {
			this.montantverse = montantverse;
		}


    	@Temporal(TemporalType.DATE)
		@Column(name = "datedemandeimmatriculation", length = 7)
		public Date getDatedemandeimmatriculation() {
			return datedemandeimmatriculation;
		}
    	public void setDatedemandeimmatriculation(Date datedemandeimmatriculation) {
			this.datedemandeimmatriculation = datedemandeimmatriculation;
		}


    	@Temporal(TemporalType.DATE)
		@Column(name = "dateimmatriculation", length = 7)
		public Date getDateimmatriculation() {
			return dateimmatriculation;
		}
    	public void setDateimmatriculation(Date dateimmatriculation) {
			this.dateimmatriculation = dateimmatriculation;
		}


    	
		@Column(name = "nummatriculation", length = 50)
		public String getNummatriculation() {
			return nummatriculation;
		}
    	public void setNummatriculation(String nummatriculation) {
			this.nummatriculation = nummatriculation;
		}


    	
		@Column(name = "concommentairesdmdmat", length = 255)
		public String getConcommentairesdmdmat() {
			return concommentairesdmdmat;
		}
    	public void setConcommentairesdmdmat(String concommentairesdmdmat) {
			this.concommentairesdmdmat = concommentairesdmdmat;
		}


    	
		@Column(name = "concommentairesmatriculation", length =255)
		public String getConcommentairesmatriculation() {
			return concommentairesmatriculation;
		}
    	public void setConcommentairesmatriculation(String concommentairesmatriculation) {
			this.concommentairesmatriculation = concommentairesmatriculation;
		}


    	@Column(name = "immatriculation", length =1)
		public int getImmatriculation() {
			return immatriculation;
		}
    	public void setImmatriculation(int immatriculation) {
			this.immatriculation = immatriculation;
		}


    	@ManyToOne(fetch = FetchType.LAZY)
    	@JoinColumn(name = "LOT")
		public SygLots getLot() {
			return lot;
		}
    	public void setLot(SygLots lot) {
			this.lot = lot;
		}


    	@ManyToOne(fetch = FetchType.LAZY)
    	@JoinColumn(name = "Service_ID")
    	public SygService getService() {
			return service;
		}
    	public void setService(SygService service) {
			this.service = service;
		}


    	@ManyToOne(fetch = FetchType.LAZY)
    	@JoinColumn(name = "fournisseurs_ID")
		public SygFournisseur getFournisseur() {
			return fournisseur;
		}
		public void setFournisseur(SygFournisseur fournisseur) {
			this.fournisseur = fournisseur;
		}


		@ManyToOne(fetch = FetchType.LAZY)
    	@JoinColumn(name = "Modepassation_ID")
		public SygModepassation getModepassation() {
			return modepassation;
		}
		public void setModepassation(SygModepassation modepassation) {
			this.modepassation = modepassation;
		}


		@ManyToOne(fetch = FetchType.LAZY)
    	@JoinColumn(name = "Typemarche_ID")
		public SygTypesmarches getTypemarche() {
			return typemarche;
		}
    	public void setTypemarche(SygTypesmarches typemarche) {
			this.typemarche = typemarche;
		}


    	@Column(name = "concsituation", length =50)
		public String getConcsituation() {
			return concsituation;
		}
    	public void setConcsituation(String concsituation) {
			this.concsituation = concsituation;
		}
    	
    	    	
    	
	}

