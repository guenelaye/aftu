package sn.ssi.kermel.be.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
	
	@SuppressWarnings("serial")
	@Entity
	@Table(name = "pmb_attributions")
	public class SygAttributions  implements java.io.Serializable {
		
  		private Long id;
  		private SygDossiers dossier;
  		private SygPlisouvertures plis;
  		private String ReferencePlandePassation,ReferenceAvisGeneral,AttributaireProvisoire,Commentaire,commentairedefinitif,nomFichierDef;
  		private BigDecimal MontantMarche,montantdefinitif;
  		private int MoisExecution,SemaineExecution,JoursExecution;
  		private Date DatePublicationProvisoire,DatePublicationDefinitive,dateattribution;
  		private SygLots lot;
		
    	public SygAttributions() {
		}

	

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name = "ID",  length = 255)
        public Long getId() {
			return this.id;
		}
		public void setId(Long id) {
			this.id = id;
		}

	

		@Column(name = "ReferencePlandePassation",  length = 255)
		public String getReferencePlandePassation() {
			return this.ReferencePlandePassation;
		}

		public void setReferencePlandePassation(String ReferencePlandePassation) {
			this.ReferencePlandePassation = ReferencePlandePassation;
		}


		@Column(name = "ReferenceAvisGeneral",  length = 50)
    	public String getReferenceAvisGeneral() {
			return ReferenceAvisGeneral;
		}
    	public void setReferenceAvisGeneral(String ReferenceAvisGeneral) {
			this.ReferenceAvisGeneral = ReferenceAvisGeneral;
		}


    	@Column(name = "AttributaireProvisoire",  length = 255)
    	public String getAttributaireProvisoire() {
			return AttributaireProvisoire;
		}
        public void setAttributaireProvisoire(String AttributaireProvisoire) {
			this.AttributaireProvisoire = AttributaireProvisoire;
		}


        @Column(name = "Commentaire",  length = 255)
     	public String getCommentaire() {
			return Commentaire;
		}
        public void setCommentaire(String Commentaire) {
			this.Commentaire = Commentaire;
		}


        @Column(name = "commentairedefinitif",  length = 255)
		public String getCommentairedefinitif() {
			return commentairedefinitif;
		}
    	public void setCommentairedefinitif(String commentairedefinitif) {
			this.commentairedefinitif = commentairedefinitif;
		}

		
    	@ManyToOne(fetch = FetchType.EAGER)
    	@JoinColumn(name = "Dossiers_ID")
    	public SygDossiers getDossier() {
			return dossier;
		}
    	public void setDossier(SygDossiers dossier) {
			this.dossier = dossier;
		}



    	@ManyToOne(fetch = FetchType.EAGER)
    	@JoinColumn(name = "Pliouverture_ID")
    	public SygPlisouvertures getPlis() {
			return plis;
		}
    	public void setPlis(SygPlisouvertures plis) {
			this.plis = plis;
		}


    	@Column(name = "nomFichierDef",  length = 255)
		public String getNomFichierDef() {
			return nomFichierDef;
		}
    	public void setNomFichierDef(String nomFichierDef) {
			this.nomFichierDef = nomFichierDef;
		}


    	@Column(name = "MontantMarche",  length = 255)
		public BigDecimal getMontantMarche() {
			return MontantMarche;
		}
    	public void setMontantMarche(BigDecimal montantMarche) {
			MontantMarche = montantMarche;
		}


    	@Column(name = "montantdefinitif",  length = 255)
		public BigDecimal getMontantdefinitif() {
			return montantdefinitif;
		}
    	public void setMontantdefinitif(BigDecimal montantdefinitif) {
			this.montantdefinitif = montantdefinitif;
		}


    	@Column(name = "MoisExecution",  length = 10)
		public int getMoisExecution() {
			return MoisExecution;
		}
    	public void setMoisExecution(int moisExecution) {
			MoisExecution = moisExecution;
		}


    	@Column(name = "SemaineExecution",  length = 10)
		public int getSemaineExecution() {
			return SemaineExecution;
		}
    	public void setSemaineExecution(int semaineExecution) {
			SemaineExecution = semaineExecution;
		}


    	@Column(name = "JoursExecution",  length = 10)
		public int getJoursExecution() {
			return JoursExecution;
		}
    	public void setJoursExecution(int joursExecution) {
			JoursExecution = joursExecution;
		}


    	@Temporal(TemporalType.DATE)
    	@Column(name = "DatePublicationProvisoire", length = 7)
		public Date getDatePublicationProvisoire() {
			return DatePublicationProvisoire;
		}
    	public void setDatePublicationProvisoire(Date datePublicationProvisoire) {
			DatePublicationProvisoire = datePublicationProvisoire;
		}


    	@Temporal(TemporalType.DATE)
    	@Column(name = "DatePublicationDefinitive", length = 7)
		public Date getDatePublicationDefinitive() {
			return DatePublicationDefinitive;
		}
		public void setDatePublicationDefinitive(Date datePublicationDefinitive) {
			DatePublicationDefinitive = datePublicationDefinitive;
		}


		
		@ManyToOne(fetch = FetchType.LAZY)
    	@JoinColumn(name = "Lot_ID")
		public SygLots getLot() {
			return lot;
		}
    	public void setLot(SygLots lot) {
			this.lot = lot;
		}


    	@Temporal(TemporalType.DATE)
    	@Column(name = "dateattribution", length = 7)
		public Date getDateattribution() {
			return dateattribution;
		}
    	public void setDateattribution(Date dateattribution) {
			this.dateattribution = dateattribution;
		}
        
    	
    	
    	
	}

