package sn.ssi.kermel.be.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pkl_plis_enveloppes")
public class PklPlisEnveloppes implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String libelle;
	private PklPlis plis;

	public PklPlisEnveloppes() {
	}

	public PklPlisEnveloppes(Long id, String libelle) {
		super();
		this.libelle = libelle;
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false, length = 10)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "LIBELLE", nullable = false, length = 200)
	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "plis", nullable = false)
	public PklPlis getPlis() {
		return plis;
	}
	public void setPlis(PklPlis plis) {
		this.plis = plis;
	}

	



	
}
