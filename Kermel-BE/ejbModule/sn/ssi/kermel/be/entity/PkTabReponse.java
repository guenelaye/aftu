package sn.ssi.kermel.be.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "pkl_tabreponse")
public class PkTabReponse implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer Id;
    private Date dateEntree;
    private String nomfichier;
    private String reponse;
    

   

	public PkTabReponse() {
    }

   

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false, length = 10)
    public Integer getId() {
	return this.Id;
    }

    public void setId(Integer id) {
	Id = id;
    }

   

    

	public void setDateEntree(Date dateEntree) {
		this.dateEntree = dateEntree;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "datesaisie" )
	public Date getDateEntree() {
		return dateEntree;
	}

	
	public void setNomfichier(String nomfichier) {
		this.nomfichier = nomfichier;
	}

	 @Column(name = "fichier", length = 50)
	public String getNomfichier() {
		return nomfichier;
	}

	

	

	
	public void setReponse(String reponse) {
		this.reponse = reponse;
	}


	 @Column(name = "reponse")
	public String getReponse() {
		return reponse;
	}

}
