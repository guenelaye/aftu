package sn.ssi.kermel.be.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pkl_formejuridique")
public class PklFormeJuridique implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
    private String code;
    private String libelle;
    

	public PklFormeJuridique() {
    }

    public PklFormeJuridique(Integer Id, String Code, String Libelle) {
	super();
	this.id = Id;
	this.code = Code;
	this.libelle = Libelle;

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false, length = 10)
    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
    	this.id = id;
    }

    @Column(name = "CODE", nullable = false, length = 50)
    public String getCode() {
	return this.code;
    }

    public void setCode(String Code) {
	this.code = Code;
    }

    @Column(name = "LIBELLE", nullable = false, length = 200)
    public String getLibelle() {
	return this.libelle;
    }

    public void setLibelle(String Libelle) {
	this.libelle = Libelle;
    }

}
