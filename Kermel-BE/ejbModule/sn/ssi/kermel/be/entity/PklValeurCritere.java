package sn.ssi.kermel.be.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pkl_typealerte")
public class PklValeurCritere implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer Id;
    private String valeur;
    

	public PklValeurCritere() {
    }

    public PklValeurCritere(Integer Id, String valeur) {
		super();
		this.Id = Id;
		this.valeur = valeur;

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false, length = 10)
    public Integer getId() {
    	return this.Id;
    }
    public void setId(Integer id) {
    	Id = id;
    }

    @Column(name = "valeur", nullable = false, length = 50)
    public String getValeur() {
    	return this.valeur;
    }
    public void setValeur(String valeur) {
    	this.valeur = valeur;
    }

}
