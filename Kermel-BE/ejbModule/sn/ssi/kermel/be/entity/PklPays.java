package sn.ssi.kermel.be.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pkl_pays")
public class PklPays implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer Id;
    private String code;
    private String Libelle;
    private String drapeau;
    

    @Column(name = "drapeau", length = 50)
    public String getDrapeau() {
		return drapeau;
	}

	public void setDrapeau(String drapeau) {
		this.drapeau = drapeau;
	}

	public PklPays() {
    }

    public PklPays(Integer Id, String Code, String Libelle) {
	super();
	this.Id = Id;
	this.code = Code;
	this.Libelle = Libelle;

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false, length = 10)
    public Integer getId() {
	return this.Id;
    }

    public void setId(Integer id) {
	Id = id;
    }

    @Column(name = "CODE", nullable = false, length = 50)
    public String getCode() {
	return this.code;
    }

    public void setCode(String Code) {
	this.code = Code;
    }

    @Column(name = "LIBELLE", nullable = false, length = 200)
    public String getLibelle() {
	return this.Libelle;
    }

    public void setLibelle(String Libelle) {
	this.Libelle = Libelle;
    }

}
