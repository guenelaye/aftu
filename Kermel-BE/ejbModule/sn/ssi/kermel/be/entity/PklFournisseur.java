package sn.ssi.kermel.be.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "pkl_founisseur")
public class PklFournisseur implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private PklFormeJuridique forme;
	private PklVille ville;
	private String nom, tel, fax, mail, url, adresse;
	private String IdN, codeP;
	
	
	

	public PklFournisseur() {
		super();
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="Id", nullable=false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="nom" , length=50)
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}


	@Column(name="tel" , length=50)
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}

	@Column(name="fax" , length=50)
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}

	@Column(name="mail" , length=50)
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}

	@Column(name="url" , length=50)
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name="adresse" , length=100)
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	@Column(name="IdNational" , length=50)
	public String getIdN() {
		return IdN;
	}
	public void setIdN(String IdN) {
		this.IdN = IdN;
	}

	@Column(name="codePostal" , length=50)
	public String getCodeP() {
		return codeP;
	}
	public void setCodeP(String codeP) {
		this.codeP = codeP;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "forme")
	public PklFormeJuridique getForme() {
		return forme;
	}
	public void setForme(PklFormeJuridique forme) {
		this.forme = forme;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ville")
	public PklVille getVille() {
		return ville;
	}

	public void setVille(PklVille departement) {
		this.ville = departement;
	}
	
	
}
