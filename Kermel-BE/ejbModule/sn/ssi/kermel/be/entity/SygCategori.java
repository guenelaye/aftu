package sn.ssi.kermel.be.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
	
	@SuppressWarnings("serial")
	@Entity
	@Table(name = "syg_categorie")
public class SygCategori implements java.io.Serializable {
	
	private Long id;
	private String libelle;
	private TypeCategorie type;
	private SygCategori categori;
	
	public SygCategori() {
	}



	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID",  length = 255)
    public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}



	@Column(name = "LIBELLE",  length = 255)
	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}


	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "TYPE")
	public TypeCategorie getType() {
		return type;
	}
	public void setType(TypeCategorie type) {
		this.type = type;
	}


	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CATEGORI")
	public SygCategori getCategori() {
		return categori;
	}

	public void setCategori(SygCategori categori) {
		this.categori = categori;
	}

	}
