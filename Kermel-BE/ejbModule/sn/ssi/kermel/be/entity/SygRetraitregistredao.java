package sn.ssi.kermel.be.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@SuppressWarnings("serial")
@Entity
@Table(name = "pmb_retraitregistredao")
public class SygRetraitregistredao  implements java.io.Serializable{
	private Long id;
	private SygDossiers dossier;
	private SygAutoriteContractante autorite;
	private String nomSoumissionnaire,fax,telephone,email,modepaiement,Ninea;
	private BigDecimal montantverse; 
	private Date dateRetrait;
	private SygPays pays;
	private SygFournisseur fournisseur;
	private  String origine;
	private PklComptePerso personne;
	
	


	public SygRetraitregistredao() {
		// TODO Auto-generated constructor stub
	}
	

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID",  length = 255)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	


	@ManyToOne(fetch = FetchType.EAGER)
 	@JoinColumn(name = "Dossiers_ID")
	public SygDossiers getDossier() {
		return dossier;
	}
	public void setDossier(SygDossiers dossier) {
		this.dossier = dossier;
	}


	@ManyToOne(fetch = FetchType.EAGER)
 	@JoinColumn(name = "Autorite_ID")
	public SygAutoriteContractante getAutorite() {
		return autorite;
	}
	public void setAutorite(SygAutoriteContractante autorite) {
		this.autorite = autorite;
	}


	@Column(name = "nomSoumissionnaire")
	public String getNomSoumissionnaire() {
		return nomSoumissionnaire;
	}
	public void setNomSoumissionnaire(String nomSoumissionnaire) {
		this.nomSoumissionnaire = nomSoumissionnaire;
	}


	@Column(name = "fax",  length = 50)
	public String getFax() {
		return fax;
	}
    public void setFax(String fax) {
		this.fax = fax;
	}


	@Column(name = "telephone",  length = 50)
	public String getTelephone() {
		return telephone;
	}
    public void setTelephone(String telephone) {
		this.telephone = telephone;
	}


	@Column(name = "email",  length = 255)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}


	@Column(name = "modepaiement",  length = 50)
	public String getModepaiement() {
		return modepaiement;
	}
	public void setModepaiement(String modepaiement) {
		this.modepaiement = modepaiement;
	}


	@Column(name = "Ninea",  length = 255)
	public String getNinea() {
		return Ninea;
	}
	public void setNinea(String ninea) {
		Ninea = ninea;
	}


	@Column(name = "montantverse")
	public BigDecimal getMontantverse() {
		return montantverse;
	}
	public void setMontantverse(BigDecimal montantverse) {
		this.montantverse = montantverse;
	}


	@Temporal(TemporalType.DATE)
	@Column(name = "dateRetrait", length = 7)
	public Date getDateRetrait() {
		return dateRetrait;
	}
	public void setDateRetrait(Date dateRetrait) {
		this.dateRetrait = dateRetrait;
	}


	@ManyToOne(fetch = FetchType.EAGER)
 	@JoinColumn(name = "PAYS")
	public SygPays getPays() {
		return pays;
	}
	public void setPays(SygPays pays) {
		this.pays = pays;
	}


	@ManyToOne(fetch = FetchType.EAGER)
 	@JoinColumn(name = "FOURNISSEUR")
	public SygFournisseur getFournisseur() {
		return fournisseur;
	}
	public void setFournisseur(SygFournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}



	public void setOrigine(String origine) {
		this.origine = origine;
	}


	@Column(name = "origine")
	public String getOrigine() {
		return origine;
	}



	public void setPersonne(PklComptePerso personne) {
		this.personne = personne;
	}



	@ManyToOne(fetch = FetchType.EAGER)
 	@JoinColumn(name = "personne")
	public PklComptePerso getPersonne() {
		return personne;
	}
	
	
	
}
