package sn.ssi.kermel.be.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
	
	@SuppressWarnings("serial")
	@Entity
	@Table(name = "pmb_realisationduplan")
	public class SygRealisations  implements java.io.Serializable {
		
  		private Long idrealisation,realisationid,miseajourid;
		private String libelle,reference,naturemodepassation,sourcefinancement,etat;
		private Date datelancement,dateattribution,datedemarrage,dateachevement,datepreparationdaodcrbc,datereceptionavisccmpdncmpappel,
		datenonobjectionptfappel,dateinvitationsoumission,dateouvertureplis,datefinevaluation,datereceptionavisccmpdncmp,datenonobjectionptf,
		dateprevisionnellesignaturecontrat,datepreparationtdrami,dateavisccmpdncmpami,datelancementmanifestation,dateouverturemanifestation,
		datepreparationdp,dateavisccmpdncmpdp,dateouverturedp,dateavisccmpdncmpdppt,datenonobjectionptfpt,dateouverture,datefinevaluationpf,
		dateavisccmpdncmp,datenegociation,dateavisptfservcontrole,dateprevsigncontrat;
    	private SygModepassation modepassation;
		private SygService servicemaitreoeuvre;
		private SygTypesmarches typemarche;
		private SygPlansdepassation plan;
		private BigDecimal montant;
		private int appel,examendncmp,examenccmp,delaiexecution,supprime;
		
     	public SygRealisations() {
		}

	

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name = "ID_plan",  length = 255)
        public Long getIdrealisation() {
			return this.idrealisation;
		}
		public void setIdrealisation(Long idrealisation) {
			this.idrealisation = idrealisation;
		}

		
	    @Column(name = "LIB_plan",  length = 255)
		public String getLibelle() {
			return libelle;
		}
    	public void setLibelle(String libelle) {
			this.libelle = libelle;
		}
    	
    	 @Column(name = "Reference",  length = 50)
 		public String getReference() {
 			return reference;
 		}
     	public void setReference(String reference) {
 			this.reference = reference;
 		}


      	@Temporal(TemporalType.DATE)
    	@Column(name = "Datelancement", length = 7)
		public Date getDatelancement() {
			return datelancement;
		}
		public void setDatelancement(Date datelancement) {
			this.datelancement = datelancement;
		}
    	
		
		
		@Temporal(TemporalType.DATE)
    	@Column(name = "Dateattribution", length = 7)
		public Date getDateattribution() {
			return dateattribution;
		}
		public void setDateattribution(Date dateattribution) {
			this.dateattribution = dateattribution;
		}
		
		@Temporal(TemporalType.DATE)
    	@Column(name = "Datedemarrage", length = 7)
		public Date getDatedemarrage() {
			return datedemarrage;
		}
		public void setDatedemarrage(Date datedemarrage) {
			this.datedemarrage = datedemarrage;
		}
		
		@Temporal(TemporalType.DATE)
    	@Column(name = "Dateachevement", length = 7)
		public Date getDateachevement() {
			return dateachevement;
		}
		public void setDateachevement(Date dateachevement) {
			this.dateachevement = dateachevement;
		}
		
		@ManyToOne(fetch = FetchType.EAGER)
    	@JoinColumn(name = "modepassation_ID")
		public SygModepassation getModepassation() {
			return modepassation;
		}
     	public void setModepassation(SygModepassation modepassation) {
			this.modepassation = modepassation;
		}


    	@ManyToOne(fetch = FetchType.EAGER)
    	@JoinColumn(name = "Servicemaitreoeuvre")
		public SygService getServicemaitreoeuvre() {
			return servicemaitreoeuvre;
		}
    	public void setServicemaitreoeuvre(SygService servicemaitreoeuvre) {
			this.servicemaitreoeuvre = servicemaitreoeuvre;
		}
     	
    	@ManyToOne(fetch = FetchType.EAGER)
    	@JoinColumn(name = "TypeMarche")
    	public SygTypesmarches getTypemarche() {
			return typemarche;
		}
		public void setTypemarche(SygTypesmarches typemarche) {
			this.typemarche = typemarche;
		}


		@Column(name = "natureModePassation", length = 50)
		public String getNaturemodepassation() {
			return naturemodepassation;
		}
    	public void setNaturemodepassation(String naturemodepassation) {
			this.naturemodepassation = naturemodepassation;
		}
     	
		
    	@Column(name = "montantEstime")
    	public BigDecimal getMontant() {
			return montant;
		}
    	public void setMontant(BigDecimal montant) {
			this.montant = montant;
		}


    	@ManyToOne(fetch = FetchType.EAGER)
    	@JoinColumn(name = "InfoPlan_ID")
		public SygPlansdepassation getPlan() {
			return plan;
		}
		public void setPlan(SygPlansdepassation plan) {
			this.plan = plan;
		}


		@Column(name = "appel",length=1)
		public int getAppel() {
			return appel;
		}
    	public void setAppel(int appel) {
			this.appel = appel;
		}
    	
		
    	@Column(name = "examendncmp",length=1)
		public int getExamendncmp() {
			return examendncmp;
		}
    	public void setExamendncmp(int examendncmp) {
			this.examendncmp = examendncmp;
		}


    	@Column(name = "sourcefinancement",length=255)
		public String getSourcefinancement() {
			return sourcefinancement;
		}
    	public void setSourcefinancement(String sourcefinancement) {
			this.sourcefinancement = sourcefinancement;
		}


    	@Column(name = "examenccmp",length=1)
		public int getExamenccmp() {
			return examenccmp;
		}
		public void setExamenccmp(int examenccmp) {
			this.examenccmp = examenccmp;
		}


		@Column(name = "delaiexecution",length=50)
		public int getDelaiexecution() {
			return delaiexecution;
		}
    	public void setDelaiexecution(int delaiexecution) {
			this.delaiexecution = delaiexecution;
		}


    	@Temporal(TemporalType.DATE)
    	@Column(name = "datenonobjectionptfappel", length = 7)
		public Date getDatenonobjectionptfappel() {
			return datenonobjectionptfappel;
		}
    	public void setDatenonobjectionptfappel(Date datenonobjectionptfappel) {
			this.datenonobjectionptfappel = datenonobjectionptfappel;
		}


    	@Temporal(TemporalType.DATE)
    	@Column(name = "dateinvitationsoumission", length = 7)
		public Date getDateinvitationsoumission() {
			return dateinvitationsoumission;
		}
    	public void setDateinvitationsoumission(Date dateinvitationsoumission) {
			this.dateinvitationsoumission = dateinvitationsoumission;
		}


    	@Temporal(TemporalType.DATE)
    	@Column(name = "datefinevaluation", length = 7)
		public Date getDatefinevaluation() {
			return datefinevaluation;
		}
    	public void setDatefinevaluation(Date datefinevaluation) {
			this.datefinevaluation = datefinevaluation;
		}


    	@Temporal(TemporalType.DATE)
    	@Column(name = "datenonobjectionptf", length = 7)
		public Date getDatenonobjectionptf() {
			return datenonobjectionptf;
		}
    	public void setDatenonobjectionptf(Date datenonobjectionptf) {
			this.datenonobjectionptf = datenonobjectionptf;
		}


    	@Temporal(TemporalType.DATE)
    	@Column(name = "datepreparationdaodcrbc", length = 7)
		public Date getDatepreparationdaodcrbc() {
			return datepreparationdaodcrbc;
		}
 		public void setDatepreparationdaodcrbc(Date datepreparationdaodcrbc) {
			this.datepreparationdaodcrbc = datepreparationdaodcrbc;
		}


 		@Temporal(TemporalType.DATE)
    	@Column(name = "datereceptionavisccmpdncmpappel", length = 7)
		public Date getDatereceptionavisccmpdncmpappel() {
			return datereceptionavisccmpdncmpappel;
		}
    	public void setDatereceptionavisccmpdncmpappel( Date datereceptionavisccmpdncmpappel) {
			this.datereceptionavisccmpdncmpappel = datereceptionavisccmpdncmpappel;
		}


    	@Temporal(TemporalType.DATE)
    	@Column(name = "dateouvertureplis", length = 7)
		public Date getDateouvertureplis() {
			return dateouvertureplis;
		}
    	public void setDateouvertureplis(Date dateouvertureplis) {
			this.dateouvertureplis = dateouvertureplis;
		}


    	@Temporal(TemporalType.DATE)
    	@Column(name = "datereceptionavisccmpdncmp", length = 7)
		public Date getDatereceptionavisccmpdncmp() {
			return datereceptionavisccmpdncmp;
		}
    	public void setDatereceptionavisccmpdncmp(Date datereceptionavisccmpdncmp) {
			this.datereceptionavisccmpdncmp = datereceptionavisccmpdncmp;
		}


    	@Temporal(TemporalType.DATE)
    	@Column(name = "dateprevisionnellesignaturecontrat", length = 7)
		public Date getDateprevisionnellesignaturecontrat() {
			return dateprevisionnellesignaturecontrat;
		}
    	public void setDateprevisionnellesignaturecontrat(
				Date dateprevisionnellesignaturecontrat) {
			this.dateprevisionnellesignaturecontrat = dateprevisionnellesignaturecontrat;
		}


    	@Temporal(TemporalType.DATE)
    	@Column(name = "dateavisccmpdncmpami", length = 7)
		public Date getDateavisccmpdncmpami() {
			return dateavisccmpdncmpami;
		}
    	public void setDateavisccmpdncmpami(Date dateavisccmpdncmpami) {
			this.dateavisccmpdncmpami = dateavisccmpdncmpami;
		}


    	@Temporal(TemporalType.DATE)
    	@Column(name = "dateavisccmpdncmpdp", length = 7)
		public Date getDateavisccmpdncmpdp() {
			return dateavisccmpdncmpdp;
		}
    	public void setDateavisccmpdncmpdp(Date dateavisccmpdncmpdp) {
			this.dateavisccmpdncmpdp = dateavisccmpdncmpdp;
		}


    	@Temporal(TemporalType.DATE)
    	@Column(name = "dateavisccmpdncmpdppt", length = 7)
		public Date getDateavisccmpdncmpdppt() {
			return dateavisccmpdncmpdppt;
		}
    	public void setDateavisccmpdncmpdppt(Date dateavisccmpdncmpdppt) {
			this.dateavisccmpdncmpdppt = dateavisccmpdncmpdppt;
		}


    	@Temporal(TemporalType.DATE)
    	@Column(name = "dateavisccmpdncmp", length = 7)
		public Date getDateavisccmpdncmp() {
			return dateavisccmpdncmp;
		}
        public void setDateavisccmpdncmp(Date dateavisccmpdncmp) {
			this.dateavisccmpdncmp = dateavisccmpdncmp;
		}


        @Temporal(TemporalType.DATE)
    	@Column(name = "dateavisptfservcontrole", length = 7)
		public Date getDateavisptfservcontrole() {
			return dateavisptfservcontrole;
		}
    	public void setDateavisptfservcontrole(Date dateavisptfservcontrole) {
			this.dateavisptfservcontrole = dateavisptfservcontrole;
		}


    	@Temporal(TemporalType.DATE)
     	@Column(name = "datelancementmanifestation", length = 7)
		public Date getDatelancementmanifestation() {
			return datelancementmanifestation;
		}
    	public void setDatelancementmanifestation(Date datelancementmanifestation) {
			this.datelancementmanifestation = datelancementmanifestation;
		}


        @Temporal(TemporalType.DATE)
     	@Column(name = "datenonobjectionptfpt", length = 7)
		public Date getDatenonobjectionptfpt() {
			return datenonobjectionptfpt;
		}
    	public void setDatenonobjectionptfpt(Date datenonobjectionptfpt) {
			this.datenonobjectionptfpt = datenonobjectionptfpt;
		}


    	 @Temporal(TemporalType.DATE)
     	@Column(name = "dateouverture", length = 7)
		public Date getDateouverture() {
			return dateouverture;
		}
    	public void setDateouverture(Date dateouverture) {
			this.dateouverture = dateouverture;
		}


    	 @Temporal(TemporalType.DATE)
     	@Column(name = "datefinevaluationpf", length = 7)
		public Date getDatefinevaluationpf() {
			return datefinevaluationpf;
		}
    	public void setDatefinevaluationpf(Date datefinevaluationpf) {
			this.datefinevaluationpf = datefinevaluationpf;
		}


    	 @Temporal(TemporalType.DATE)
     	@Column(name = "datenegociation", length = 7)
		public Date getDatenegociation() {
			return datenegociation;
		}
    	public void setDatenegociation(Date datenegociation) {
			this.datenegociation = datenegociation;
		}


        @Temporal(TemporalType.DATE)
      	@Column(name = "datepreparationtdrami", length = 7)
		public Date getDatepreparationtdrami() {
			return datepreparationtdrami;
		}
		public void setDatepreparationtdrami(Date datepreparationtdrami) {
			this.datepreparationtdrami = datepreparationtdrami;
		}

		
		@Temporal(TemporalType.DATE)
	    @Column(name = "dateouverturemanifestation", length = 7)
    	public Date getDateouverturemanifestation() {
			return dateouverturemanifestation;
		}
    	public void setDateouverturemanifestation(Date dateouverturemanifestation) {
			this.dateouverturemanifestation = dateouverturemanifestation;
		}


    	@Temporal(TemporalType.DATE)
	    @Column(name = "datepreparationdp", length = 7)
		public Date getDatepreparationdp() {
			return datepreparationdp;
		}
		public void setDatepreparationdp(Date datepreparationdp) {
			this.datepreparationdp = datepreparationdp;
		}


		@Temporal(TemporalType.DATE)
	    @Column(name = "dateouverturedp", length = 7)
		public Date getDateouverturedp() {
			return dateouverturedp;
		}
		public void setDateouverturedp(Date dateouverturedp) {
			this.dateouverturedp = dateouverturedp;
		}


		@Temporal(TemporalType.DATE)
	    @Column(name = "dateprevsigncontrat", length = 7)
		public Date getDateprevsigncontrat() {
			return dateprevsigncontrat;
		}
    	public void setDateprevsigncontrat(Date dateprevsigncontrat) {
			this.dateprevsigncontrat = dateprevsigncontrat;
		}



    	@Column(name = "etat",length=50)
		public String getEtat() {
			return etat;
		}
    	public void setEtat(String etat) {
			this.etat = etat;
		}


    	@Column(name = "rea_reaID")
		public Long getRealisationid() {
			return realisationid;
		}
    	public void setRealisationid(Long realisationid) {
			this.realisationid = realisationid;
		}


    	@Column(name = "maj_ID")
		public Long getMiseajourid() {
			return miseajourid;
		}
    	public void setMiseajourid(Long miseajourid) {
			this.miseajourid = miseajourid;
		}


    	@Column(name = "supprime")
		public int getSupprime() {
			return supprime;
		}
    	public void setSupprime(int supprime) {
			this.supprime = supprime;
		}
    	
    	
    	
    	
	}

