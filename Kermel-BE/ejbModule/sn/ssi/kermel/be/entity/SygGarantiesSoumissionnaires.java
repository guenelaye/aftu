package sn.ssi.kermel.be.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
	
	@SuppressWarnings("serial")
	@Entity
	@Table(name = "pmb_garantiesoumissions")
	public class SygGarantiesSoumissionnaires  implements java.io.Serializable {
		
  		private Long id;
		private SygPlisouvertures plis;
		private SygGarantiesDossiers garantie;
		private SygDossiers dossier;
		private String fournie,libelle;
			
		
    	public SygGarantiesSoumissionnaires() {
		}

	

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name = "ID",  length = 255)
        public Long getId() {
			return this.id;
		}
		public void setId(Long id) {
			this.id = id;
		}

	

		
    	@ManyToOne(fetch = FetchType.EAGER)
    	@JoinColumn(name = "Pli_ID")
    	public SygPlisouvertures getPlis() {
			return plis;
		}
    	public void setPlis(SygPlisouvertures plis) {
			this.plis = plis;
		}



    	@ManyToOne(fetch = FetchType.EAGER)
    	@JoinColumn(name = "GARANTIE")
    	public SygGarantiesDossiers getGarantie() {
			return garantie;
		}
		public void setGarantie(SygGarantiesDossiers garantie) {
			this.garantie = garantie;
		}



    	
    	@ManyToOne(fetch = FetchType.EAGER)
    	@JoinColumn(name = "Dossiers_ID")
    	public SygDossiers getDossier() {
			return dossier;
		}
    	public void setDossier(SygDossiers dossier) {
			this.dossier = dossier;
		}


    	@Column(name = "fournie",length=3)
		public String getFournie() {
			return fournie;
		}
    	public void setFournie(String fournie) {
			this.fournie = fournie;
		}


    	@Column(name = "libelle",length=255)
		public String getLibelle() {
			return libelle;
		}
    	public void setLibelle(String libelle) {
			this.libelle = libelle;
		}



    	
	}

