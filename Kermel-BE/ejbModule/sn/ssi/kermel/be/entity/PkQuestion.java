package sn.ssi.kermel.be.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "pkl_question")
public class PkQuestion implements java.io.Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer Id;
   
    private Date dateEntree;
    private String objet ;
    private String nomfichier;
    private String  description;
    private SygDossiers  dossier;
    private PklComptePerso   user;
    private String reponse;
    private PkTabReponse   rep;
    

   

	public PkQuestion() {
    }

   

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false, length = 10)
    public Integer getId() {
	return this.Id;
    }

    public void setId(Integer id) {
	Id = id;
    }

   

    

	public void setDateEntree(Date dateEntree) {
		this.dateEntree = dateEntree;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "datesaisie" )
	public Date getDateEntree() {
		return dateEntree;
	}

	
	public void setObjet(String objet) {
		this.objet = objet;
	}
	 @Column(name = "objet", length = 50)
	public String getObjet() {
		return objet;
	}

	public void setNomfichier(String nomfichier) {
		this.nomfichier = nomfichier;
	}

	 @Column(name = "fichier", length = 50)
	public String getNomfichier() {
		return nomfichier;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	 @Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDossier(SygDossiers dossier) {
		this.dossier = dossier;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "dossier")
	public SygDossiers getDossier() {
		return dossier;
	}

	public void setUser(PklComptePerso user) {
		this.user = user;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user")
	public PklComptePerso getUser() {
		return user;
	}



	public void setReponse(String reponse) {
		this.reponse = reponse;
	}


	 @Column(name = "reponse")
	public String getReponse() {
		return reponse;
	}



	public void setRep(PkTabReponse rep) {
		this.rep = rep;
	}


	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "rep")
	public PkTabReponse getRep() {
		return rep;
	}

}
