package sn.ssi.kermel.be.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "pkl_alerte")
public class PklAlerte implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String libelle,statut;
	private String description;
	private Date dateCreation,dateActivation;
	private PklComptePerso compte;
	private PklTypeAlerte type;

	public PklAlerte() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "libelle", length = 200)
	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String empreinte) {
		this.libelle = empreinte;
	}

	@Column(name = "description", length = 200)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "statut", length = 50)
	public String getStatut() {
		return statut;
	}
	public void setStatut(String statut) {
		this.statut = statut;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "dateCreation")
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "dateActivation")
	public Date getDateActivation() {
		return dateActivation;
	}
	public void setDateActivation(Date dateActivation) {
		this.dateActivation = dateActivation;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "compte")
	public PklComptePerso getCompte() {
		return compte;
	}
	public void setCompte(PklComptePerso fournisseur) {
		this.compte = fournisseur;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "type")
	public PklTypeAlerte getType() {
		return type;
	}
	public void setType(PklTypeAlerte type) {
		this.type = type;
	}
	
	
}
