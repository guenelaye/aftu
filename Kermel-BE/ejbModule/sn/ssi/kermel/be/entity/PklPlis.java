package sn.ssi.kermel.be.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pkl_plis")
public class PklPlis implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String libelle;
	private SygFournisseur fournisseur;
	private SygDossiers dossier;

	public PklPlis() {
	}

	public PklPlis(Long id, String libelle) {
		super();
		this.libelle = libelle;
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false, length = 10)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "LIBELLE", nullable = false, length = 200)
	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fournisseur", nullable = false)
	public SygFournisseur getFournisseur() {
		return fournisseur;
	}
	public void setFournisseur(SygFournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "dossier", nullable = false)
	public SygDossiers getDossier() {
		return dossier;
	}
	public void setDossier(SygDossiers dossier) {
		this.dossier = dossier;
	}


	
}
