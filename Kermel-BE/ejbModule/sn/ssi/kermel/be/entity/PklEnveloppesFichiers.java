package sn.ssi.kermel.be.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pkl_enveloppes_fichiers")
public class PklEnveloppesFichiers implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String libelle,chemin;
	
	private PklPlisEnveloppes enveloppe;

	public PklEnveloppesFichiers() {
	}

	public PklEnveloppesFichiers(Long id, String libelle) {
		super();
		this.libelle = libelle;
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false, length = 10)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "LIBELLE", nullable = false, length = 200)
	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "enveloppe", nullable = false)
	public PklPlisEnveloppes getEnveloppe() {
		return enveloppe;
	}
	public void setEnveloppe(PklPlisEnveloppes enveloppe) {
		this.enveloppe = enveloppe;
	}


	
	@Column(name = "chemin",  length = 200)
	public String getChemin() {
		return chemin;
	}

	public void setChemin(String chemin) {
		this.chemin = chemin;
	}

	
}
