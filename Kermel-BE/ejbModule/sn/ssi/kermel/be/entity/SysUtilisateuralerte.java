package sn.ssi.kermel.be.entity;

// Generated 23 janv. 2010 23:31:59 by Hibernate Tools 3.2.5.Beta

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * PadUtilisateuralerte generated by hbm2java
 */
@Entity
@Table(name = "sys_utilisateuralerte")
public class SysUtilisateuralerte implements java.io.Serializable {

	private long id;
	private Utilisateur utilisateur;
	private SysAlerte sysAlerte;

	public SysUtilisateuralerte() {
	}

	public SysUtilisateuralerte(long id, Utilisateur utilisateur,
			SysAlerte sysAlerte) {
		this.id = id;
		this.utilisateur = utilisateur;
		this.sysAlerte = sysAlerte;
	}

	@Id
	@Column(name = "USA_ID", unique = true, nullable = false)
	@GeneratedValue(strategy = IDENTITY)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USR_ID", nullable = false)
	public Utilisateur getUtilisateur() {
		return this.utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ALT_ID", nullable = false)
	public SysAlerte getSysAlerte() {
		return this.sysAlerte;
	}

	public void setSysAlerte(SysAlerte sysAlerte) {
		this.sysAlerte = sysAlerte;
	}
}