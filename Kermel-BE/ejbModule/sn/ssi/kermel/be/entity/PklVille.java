package sn.ssi.kermel.be.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pkl_ville")
public class PklVille implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String libelle;
	private PklPays siap_pays;

	public PklVille() {
	}

	public PklVille(Integer id, String libelle) {
		super();
		this.libelle = libelle;
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false, length = 10)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "LIBELLE", nullable = false, length = 200)
	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PAYS_CODE", nullable = false)
	public PklPays getSiap_pays() {
		return siap_pays;
	}

	public void setSiap_pays(PklPays pklPays) {
		siap_pays = pklPays;
	}

}
