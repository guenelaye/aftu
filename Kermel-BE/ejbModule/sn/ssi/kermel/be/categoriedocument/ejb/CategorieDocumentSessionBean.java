package sn.ssi.kermel.be.categoriedocument.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCategorieDocument;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

public @Stateless class CategorieDocumentSessionBean extends AbstractSessionBean implements CategorieDocumentSession {

	@Override
	public int count(String libelleCatDoc) {
		Criteria criteria = getHibernateSession().createCriteria(SygCategorieDocument.class);
		if(libelleCatDoc!=null){
			criteria.add(Restrictions.ge("String", libelleCatDoc));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygCategorieDocument.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	@Override
	public void delete(Long IDcatDoc) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygCategorieDocument.class, IDcatDoc));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	@Override
	public void save(SygCategorieDocument document) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(document);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygCategorieDocument document) {
	try{
			getHibernateSession().merge(document);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public SygCategorieDocument findById(Long code) {
		return (SygCategorieDocument)getHibernateSession().get(SygCategorieDocument.class, code);
	}
	@Override
	public List<SygCategorieDocument> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCategorieDocument.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));	
		return criteria.list();
	}
	@Override
	public List<SygCategorieDocument> find(int indice, int pas,Long IDcatDoc,String niveauDoc,String libelleCatDoc,Integer nbTotalFichiersDoc,Integer flagdernierniveauDoc,String descriptionDocument,SygAutoriteContractante autorite) {
       Criteria criteria = getHibernateSession().createCriteria(SygCategorieDocument.class);
       criteria.setFetchMode("autorite", FetchMode.SELECT);
		if(niveauDoc!=null){
			criteria.add(Restrictions.ge("String", niveauDoc));
		}
		if(libelleCatDoc!=null){
			criteria.add(Restrictions.ge("String", libelleCatDoc));
		}
		if(flagdernierniveauDoc!=null){
			criteria.add(Restrictions.ge("Integer", flagdernierniveauDoc));
		}
		if(descriptionDocument!=null){
			criteria.add(Restrictions.ge("String", descriptionDocument));
		}
		if(flagdernierniveauDoc!=null){
			criteria.add(Restrictions.ge("Integer", flagdernierniveauDoc));
		}
		if(flagdernierniveauDoc!=null){
			criteria.add(Restrictions.ge("Integer", flagdernierniveauDoc));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);	
		return criteria.list();
		}
	@Override
	public List<SygCategorieDocument> find(int indice, int pas) {
	   Criteria criteria = getHibernateSession().createCriteria(SygCategorieDocument.class);
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);			
		return criteria.list();
		}
	}
