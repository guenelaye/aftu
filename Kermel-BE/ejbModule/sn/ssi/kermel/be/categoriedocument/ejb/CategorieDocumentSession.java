package sn.ssi.kermel.be.categoriedocument.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCategorieDocument;



@Remote
public interface CategorieDocumentSession {
	public void save(SygCategorieDocument document);
	public void delete(Long id);
	public List<SygCategorieDocument> find(int indice, int pas,Long IDcatDoc,String niveauDoc,String libelleCatDoc,Integer nbTotalFichiersDoc,Integer flagdernierniveauDoc,String descriptionDocument,SygAutoriteContractante autorite);
	public int count(String libelleCatDoc);
	public void update(SygCategorieDocument document);
	public SygCategorieDocument findById(Long code);
	public List<SygCategorieDocument> find(String code);
	List<SygCategorieDocument> find(int indice, int pas);
	int count();
	
}
