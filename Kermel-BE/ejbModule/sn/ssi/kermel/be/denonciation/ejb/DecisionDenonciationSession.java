package sn.ssi.kermel.be.denonciation.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDecisionDenonciation;
import sn.ssi.kermel.be.entity.SygDecisionsContentieux;
import sn.ssi.kermel.be.entity.SygDenonciation;


@Remote
public interface DecisionDenonciationSession {
	public SygDecisionDenonciation save(SygDecisionDenonciation decisiondenon);
	public void delete(Long id);
	public List<SygDecisionDenonciation> find(int indice, int pas,Long code,String libelle);
	public List<SygDecisionDenonciation> findRech(int indice, int pas,Long code,String libelle,String Objet);
	public int count(Long code,String libelle);
	public int countRech(Long code,String libelle,String Objet);
	public void update(SygDecisionDenonciation decisiondenon);
	public SygDecisionDenonciation findById(Long code);
	public List<SygDecisionDenonciation> find(String code);
	//List<SygDecisionDenonciation> find(int indice, int pas);
	int count();
	SygDecisionDenonciation findden(Long code);

	public SygDecisionDenonciation findCont(Long code);
}

