package sn.ssi.kermel.be.denonciation.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDenonciation;
import sn.ssi.kermel.be.entity.SygTypeDecisionDenonciation;


@Remote
public interface TypeDecisionDenonciationSession {
	public void save(SygTypeDecisionDenonciation  typedecision);
	public void delete(Long id);
	public List<SygTypeDecisionDenonciation> find(int indice, int pas,Long code,String libelletypedecision);
	public int count(Long code,String libelletypedecision);
	public void update(SygTypeDecisionDenonciation  typedecision);
	public SygTypeDecisionDenonciation findById(Long code);
	public List<SygTypeDecisionDenonciation> find(String code);
	List<SygTypeDecisionDenonciation> find(int indice, int pas);
	int count();
	
}

