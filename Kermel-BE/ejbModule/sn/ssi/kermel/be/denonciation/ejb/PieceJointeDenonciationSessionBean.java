package sn.ssi.kermel.be.denonciation.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;



import sn.ssi.kermel.be.entity.SygPieceJointeDenonciation;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class PieceJointeDenonciationSessionBean extends AbstractSessionBean implements PieceJointeDenonciationSession{

	@Override
	public int count(Long code,String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeDenonciation.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygPieceJointeDenonciation.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygPieceJointeDenonciation> find(int indice, int pas,Long code,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeDenonciation.class);
		if(code!=null){
			criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public SygPieceJointeDenonciation save(SygPieceJointeDenonciation pjointe) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(pjointe);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return pjointe;	}

	@Override
	public void update(SygPieceJointeDenonciation pjointe) {
		
		try{
			getHibernateSession().merge(pjointe);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygPieceJointeDenonciation findById(Long code) {
		return (SygPieceJointeDenonciation)getHibernateSession().get(SygPieceJointeDenonciation.class, code);
	}
	
	@Override
	public List<SygPieceJointeDenonciation> findRech(int indice, int pas,Long code,String libelle,String LibelleType) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeDenonciation.class);
		
		criteria.createAlias("denonciation", "denonciation");
		if(code!=null){
			criteria.add(Restrictions.eq("denonciation.id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		if(LibelleType!=null){
			criteria.add(Restrictions.ilike("denonciation.libelle", "%"+LibelleType+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public int countRech(Long code,String libelle,String LibelleType) {
		Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeDenonciation.class);
		criteria.createAlias("denonciation", "denonciation");
		if(code!=null){
			criteria.add(Restrictions.eq("denonciation.id", code));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		
		if(LibelleType!=null){
			criteria.add(Restrictions.ilike("denonciation.libelle", "%"+LibelleType+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	@Override
	public SygPieceJointeDenonciation findden(Long code,String recevable,String decision) {
		Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeDenonciation.class);
		criteria.createAlias("denonciation", "denonciation");
		if(code!=null){
			criteria.add(Restrictions.eq("denonciation.id", code));
		}
		
		if(recevable!=null){
			criteria.add(Restrictions.eq("recevable", recevable));
		}
		
		if(decision!=null){
			criteria.add(Restrictions.eq("decision", decision));
		}
		if(criteria.list().size()>0)
	        return (SygPieceJointeDenonciation)criteria.list().get(0);
	        else
	        return null;
		
		
	}
	
	
}
