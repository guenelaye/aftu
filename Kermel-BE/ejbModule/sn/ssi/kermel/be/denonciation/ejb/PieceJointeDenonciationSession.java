package sn.ssi.kermel.be.denonciation.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygPieceJointeDenonciation;


@Remote
public interface PieceJointeDenonciationSession {
public SygPieceJointeDenonciation save(SygPieceJointeDenonciation pjointe);
	
	public void delete(Long id);
	
	public List<SygPieceJointeDenonciation> find(int indice, int pas,Long code,String libelle);
	
	public List<SygPieceJointeDenonciation> findRech(int indice, int pas,Long code,String libelle,String LibelleType);
	
	public int count(Long code,String libelle);
	
	public int countRech(Long code,String libelle, String LibelleType);
	
	public void update(SygPieceJointeDenonciation pjointe);
	
	public SygPieceJointeDenonciation findById(Long code);
	
	public SygPieceJointeDenonciation findden(Long code,String recevable,String decisiondenonciation);
	
}

