package sn.ssi.kermel.be.denonciation.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDecisionDenonciation;
import sn.ssi.kermel.be.entity.SygDenonciation;


@Remote
public interface DenonciationSession {
	public void save(SygDenonciation denonciation);
	public void delete(Long id);
	public List<SygDenonciation> find(int indice, int pas,Date datedenonciation,Integer traiter,Integer publier,Integer poubelle,String origine);
	public int count(Date datedenonciation,Integer traiter,Integer publier,Integer poubelle,String origine);
	public void update(SygDenonciation denonciation);
	public SygDenonciation findById(Long code);
	public List<SygDenonciation> find(String code);
	List<SygDenonciation> find(int indice, int pas);
	int count();
	SygDecisionDenonciation findden(Long code);
	
}

