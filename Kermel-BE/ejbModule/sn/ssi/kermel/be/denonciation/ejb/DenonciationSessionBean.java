package sn.ssi.kermel.be.denonciation.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDecisionDenonciation;
import sn.ssi.kermel.be.entity.SygDenonciation;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class DenonciationSessionBean extends AbstractSessionBean implements DenonciationSession{

	@Override
	public int count(Date datedenonciation,Integer traiter,Integer publier,Integer poubelle,String origine) {
		Criteria criteria = getHibernateSession().createCriteria(SygDenonciation.class);
		if(datedenonciation!=null){
			criteria.add(Restrictions.ge("date", datedenonciation));
		}
		if(traiter!=null){
			criteria.add(Restrictions.eq("traiter",traiter));
			}
		if(publier!=null){
			criteria.add(Restrictions.eq("publier",publier));
			}
		if(poubelle!=null){
			criteria.add(Restrictions.eq("poubelle",poubelle));
			}
		if(origine!=null){
			criteria.add(Restrictions.eq("origine",origine));
			}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygDenonciation.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygDenonciation.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	

	@Override
	public void save(SygDenonciation denonciation) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(denonciation);
			//getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygDenonciation denonciation) {
		
		try{
			getHibernateSession().merge(denonciation);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygDenonciation findById(Long code) {
		return (SygDenonciation)getHibernateSession().get(SygDenonciation.class, code);
	}
	@Override
	public SygDecisionDenonciation findden(Long code) {
		Criteria criteria = getHibernateSession().createCriteria(SygDecisionDenonciation.class);
		criteria.createAlias("denonciation", "denonciation");
		if(code!=null){
			criteria.add(Restrictions.eq("denonciation.id", code));
		}
		
		if(criteria.list().size()>0)
	        return (SygDecisionDenonciation)criteria.list().get(0);
	        else
	        return null;
		
		
	}
	
	@Override
	public List<SygDenonciation> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDenonciation.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));
		
		return criteria.list();
	}
	

	@Override
	public List<SygDenonciation> find(int indice, int pas,Date datedenonciation,Integer traiter,Integer publier,Integer poubelle,String origine) {
       Criteria criteria = getHibernateSession().createCriteria(SygDenonciation.class);
		criteria.addOrder(Order.desc("datedenonciation"));
		if(datedenonciation!=null){
		criteria.add(Restrictions.ge("datedenonciation",datedenonciation));
		}
		if(traiter!=null){
			criteria.add(Restrictions.eq("traiter",traiter));
			}
		if(publier!=null){
			criteria.add(Restrictions.eq("publier",publier));
			}
		if(poubelle!=null){
			criteria.add(Restrictions.eq("poubelle",poubelle));
			}
		if(origine!=null){
			criteria.add(Restrictions.eq("origine",origine));
			}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}




@Override
public List<SygDenonciation> find(int indice, int pas) {
   Criteria criteria = getHibernateSession().createCriteria(SygDenonciation.class);
	criteria.setFirstResult(indice);
	if(pas>0)
	criteria.setMaxResults(pas);
		
	return criteria.list();
	}


}
