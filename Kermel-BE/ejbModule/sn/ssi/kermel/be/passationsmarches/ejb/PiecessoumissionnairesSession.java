package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPieces;
import sn.ssi.kermel.be.entity.SygPiecesplisouvertures;
import sn.ssi.kermel.be.entity.SygPlisouvertures;

@Remote
public interface PiecessoumissionnairesSession {
	public void save(SygPiecesplisouvertures piece);
	public void delete(Long id);
	public List<SygPiecesplisouvertures> find(int indice, int pas,SygDossiers dossier,SygPieces piece,SygPlisouvertures plis,String etat);
	public int count(SygDossiers dossier,SygPieces piece,SygPlisouvertures plis,String etat);
	public void update(SygPiecesplisouvertures piece);
	public SygPiecesplisouvertures findById(Long code);
	
	
}
