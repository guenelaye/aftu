package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygPieceJointeEvenement;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class PieceJointeEvenementSessionBean extends AbstractSessionBean implements PieceJointeEvenementSession{

	@Override
	public int count(Date date) {
		Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeEvenement.class);
		 //criteria.createAlias("state", "state");
		if(date!=null){
			criteria.add(Restrictions.ge("date", date));
		}	
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeEvenement.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long Id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygPieceJointeEvenement.class, Id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	

	@Override
	public SygPieceJointeEvenement save(SygPieceJointeEvenement piecejointeevenement)
	{
	// TODO Auto-generated method stub
			try {
				SygPieceJointeEvenement CourrierAudits1=(SygPieceJointeEvenement) getHibernateSession().save(piecejointeevenement);
				getHibernateSession().flush();
				return CourrierAudits1;
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				return null;
			}	
			
	}

	@Override
	public void update(SygPieceJointeEvenement piecejointeevenement) {
		
		try{
			getHibernateSession().merge(piecejointeevenement);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygPieceJointeEvenement findById(Long code) {
		return (SygPieceJointeEvenement)getHibernateSession().get(SygPieceJointeEvenement.class, code);
	}
	
	
	@Override
	public List<SygPieceJointeEvenement> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeEvenement.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));
		
		return criteria.list();
	}
	

	@Override
	public List<SygPieceJointeEvenement> find(int indice, int pas,String libelle,String fichier,Date date,Date datecourrier,String reference) {
       Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeEvenement.class);
//       criteria.createAlias("state", "state");
//		criteria.addOrder(Order.desc("datestatut"));
		
		if(libelle!=null){
		criteria.add(Restrictions.ge("libelle",libelle));
		}
		if(fichier!=null){
			criteria.add(Restrictions.eq("fichier",fichier));
			}
		if(date!=null){
			criteria.add(Restrictions.eq("date",date));
			}
		if(datecourrier!=null){
			criteria.add(Restrictions.eq("datecourrier",datecourrier));
			}
		if(reference!=null){
			criteria.add(Restrictions.eq("reference",reference));
			}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}




@Override
public List<SygPieceJointeEvenement> find(int indice, int pas) {
   Criteria criteria = getHibernateSession().createCriteria(SygPieceJointeEvenement.class);
	criteria.setFirstResult(indice);
	if(pas>0)
	criteria.setMaxResults(pas);
		
	return criteria.list();
	}


}
