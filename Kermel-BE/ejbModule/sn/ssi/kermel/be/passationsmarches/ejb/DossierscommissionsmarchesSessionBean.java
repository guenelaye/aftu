package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierscommissionsmarches;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class DossierscommissionsmarchesSessionBean extends AbstractSessionBean implements DossierscommissionsmarchesSession{


	
	@Override
	public int count(SygDossiers dossier,int presence,int etape) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossierscommissionsmarches.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("membre", "membre");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(presence>-1)
			criteria.add(Restrictions.eq("flagpresenceevaluation", presence));
		if(etape>-1)
			criteria.add(Restrictions.eq("etapePI", etape));
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygDossierscommissionsmarches.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygDossierscommissionsmarches> find(int indice, int pas,SygDossiers dossier,int presence,int etape) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossierscommissionsmarches.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("membre", "membre");
		criteria.addOrder(Order.asc("membre.nom"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(presence>-1)
			criteria.add(Restrictions.eq("flagpresenceevaluation", presence));
		if(etape>-1)
			criteria.add(Restrictions.eq("etapePI", etape));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygDossierscommissionsmarches membre) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(membre);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygDossierscommissionsmarches membre) {
		
		try{
			getHibernateSession().merge(membre);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygDossierscommissionsmarches findById(Long code) {
		return (SygDossierscommissionsmarches)getHibernateSession().get(SygDossierscommissionsmarches.class, code);
	}
	


}
