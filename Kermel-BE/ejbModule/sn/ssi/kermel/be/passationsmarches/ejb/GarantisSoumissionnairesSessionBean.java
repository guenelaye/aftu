package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygGarantiesDossiers;
import sn.ssi.kermel.be.entity.SygGarantiesSoumissionnaires;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class GarantisSoumissionnairesSessionBean extends AbstractSessionBean implements GarantisSoumissionnairesSession{

	
	
	@Override
	public int count(SygDossiers dossier,SygPlisouvertures plis,SygGarantiesDossiers garantie,String fournie) {
		Criteria criteria = getHibernateSession().createCriteria(SygGarantiesSoumissionnaires.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("garantie", "garantie");
		criteria.createAlias("plis", "plis");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(garantie!=null){
			criteria.add(Restrictions.eq("garantie", garantie));
		}
		if(fournie!=null)
			criteria.add(Restrictions.eq("fournie", fournie));
	
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygGarantiesSoumissionnaires.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygGarantiesSoumissionnaires> find(int indice, int pas,SygDossiers dossier,SygPlisouvertures plis,SygGarantiesDossiers garantie,String fournie) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygGarantiesSoumissionnaires.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("garantie", "garantie");
		criteria.createAlias("plis", "plis");
		criteria.addOrder(Order.asc("id"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(garantie!=null){
			criteria.add(Restrictions.eq("garantie", garantie));
		}
		if(fournie!=null)
			criteria.add(Restrictions.eq("fournie", fournie));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygGarantiesSoumissionnaires critere) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(critere);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygGarantiesSoumissionnaires critere) {
		
		try{
			getHibernateSession().merge(critere);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygGarantiesSoumissionnaires findById(Long code) {
		return (SygGarantiesSoumissionnaires)getHibernateSession().get(SygGarantiesSoumissionnaires.class, code);
	}
	

	
	@Override
	public SygGarantiesSoumissionnaires getLot(Long code,SygDossiers dossier,SygPlisouvertures plis,SygGarantiesDossiers garantie){
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygGarantiesSoumissionnaires.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("garantie", "garantie");
		criteria.createAlias("plis", "plis");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(garantie!=null){
			criteria.add(Restrictions.eq("garantie", garantie));
		}
		if(criteria.list().size()>0)
		  return (SygGarantiesSoumissionnaires) criteria.list().get(0);
		else 
			return null;
	}

		

}
