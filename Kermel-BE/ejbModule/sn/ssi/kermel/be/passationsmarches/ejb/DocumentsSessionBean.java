package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class DocumentsSessionBean extends AbstractSessionBean implements DocumentsSession{

	
	@Override
	public int count(SygDossiers dossier,SygAppelsOffres appel,String type,Long lot, SygPlisouvertures plis) {
		Criteria criteria = getHibernateSession().createCriteria(SygDocuments.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("appel", "appel");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(appel!=null){
			criteria.add(Restrictions.eq("appel", appel));
		}
		if(type!=null){
			criteria.add(Restrictions.eq("typeDocument", type));
		}
		if(lot!=null)
			criteria.add(Restrictions.eq("idlot", lot));
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
	
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygDocuments.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygDocuments> find(int indice, int pas,SygDossiers dossier,SygAppelsOffres appel,String type,Long lot, SygPlisouvertures plis) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDocuments.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("appel", "appel");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(appel!=null){
			criteria.add(Restrictions.eq("appel", appel));
		}
		if(type!=null){
			criteria.add(Restrictions.eq("typeDocument", type));
		}
		if(lot!=null)
			criteria.add(Restrictions.eq("idlot", lot));
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygDocuments documents) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(documents);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygDocuments document) {
		
		try{
			getHibernateSession().merge(document);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygDocuments findById(Long code) {
		return (SygDocuments)getHibernateSession().get(SygDocuments.class, code);
	}
	


}
