package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierscommissionscellules;

@Remote
public interface DossierscommissionscellulesSession {
	public void save(SygDossierscommissionscellules membre);
	public void delete(Long id);
	public List<SygDossierscommissionscellules> find(int indice, int pas,SygDossiers dossier);
	public int count(SygDossiers dossier);
	public void update(SygDossierscommissionscellules membre);
	public SygDossierscommissionscellules findById(Long code);
	
	
}
