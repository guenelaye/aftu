package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygAvisAttribution;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class AvisAttributionSessionBean extends AbstractSessionBean implements AvisAttributionSession{

	
	@Override
	public int count(SygDossiers dossier,SygAutoriteContractante autorite) {
		Criteria criteria = getHibernateSession().createCriteria(SygAvisAttribution.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("autorite", "autorite");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
	
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygAvisAttribution.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygAvisAttribution> find(int indice, int pas,SygDossiers dossier,SygAutoriteContractante autorite) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAvisAttribution.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.asc("id"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
	
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygAvisAttribution avis) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(avis);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygAvisAttribution avis) {
		
		try{
			getHibernateSession().merge(avis);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygAvisAttribution findById(Long code) {
		return (SygAvisAttribution)getHibernateSession().get(SygAvisAttribution.class, code);
	}
	

	@Override
	public SygAvisAttribution getAvis(SygDossiers dossier,SygAutoriteContractante autorite,String type){
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAvisAttribution.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("autorite", "autorite");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(type!=null){
			criteria.add(Restrictions.eq("attriType", type));
		}
		if(criteria.list().size()>0)
		  return (SygAvisAttribution) criteria.list().get(0);
		else 
			return null;
	}


}
