package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCritere;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygDossierssouscriteresevaluateurs;
import sn.ssi.kermel.be.entity.SygEvaluateur;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygPlisouvertures;

@Remote
public interface DossiersSouscriteresSession {
	public void save(SygDossierssouscriteres critere);
	public void delete(Long id);
	public List<SygDossierssouscriteres> find(int indice, int pas,SygDossiers dossier, Long critere, SygLots lot);
	public int count(SygDossiers dossier, Long critere, SygLots lot);
	public void update(SygDossierssouscriteres critere);
	public SygDossierssouscriteres findById(Long code);
	
	///////////:::::Notes//////:::
	public List<SygDossierssouscriteresevaluateurs> ListesNotes(int indice, int pas,SygDossiers dossier, SygCritere critere,SygPlisouvertures plis,SygEvaluateur evaluateur);
	public int countListesNotes(SygDossiers dossier, SygCritere critere,SygPlisouvertures plis,SygEvaluateur evaluateur);
	public void saveNote(SygDossierssouscriteresevaluateurs note);
	public void deleteNote(Long id);
	public void updateNote(SygDossierssouscriteresevaluateurs note);
	public SygDossierssouscriteresevaluateurs findNote(Long code);
}
