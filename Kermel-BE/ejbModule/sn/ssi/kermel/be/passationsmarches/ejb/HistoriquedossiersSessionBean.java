package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygHistoriquedossiers;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class HistoriquedossiersSessionBean extends AbstractSessionBean implements HistoriquedossiersSession{


	
	@Override
	public int count(SygDossiers dossier,SygAppelsOffres appel) {
		Criteria criteria = getHibernateSession().createCriteria(SygHistoriquedossiers.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("appel", "appel");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(appel!=null){
			criteria.add(Restrictions.eq("appel", appel));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygHistoriquedossiers.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygHistoriquedossiers> find(int indice, int pas,SygDossiers dossier,SygAppelsOffres appel) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygHistoriquedossiers.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("appel", "appel");
		criteria.addOrder(Order.desc("hisDateRejet"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(appel!=null){
			criteria.add(Restrictions.eq("appel", appel));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygHistoriquedossiers historique) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(historique);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygHistoriquedossiers historique) {
		
		try{
			getHibernateSession().merge(historique);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygHistoriquedossiers findById(Long code) {
		return (SygHistoriquedossiers)getHibernateSession().get(SygHistoriquedossiers.class, code);
	}
	


}
