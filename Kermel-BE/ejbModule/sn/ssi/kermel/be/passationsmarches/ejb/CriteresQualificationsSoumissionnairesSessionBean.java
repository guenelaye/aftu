package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygCriteresQualificationsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygLotsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class CriteresQualificationsSoumissionnairesSessionBean extends AbstractSessionBean implements CriteresQualificationsSoumissionnairesSession{

	
	
	@Override
	public int count(SygDossiers dossier,SygPlisouvertures plis,SygDossierssouscriteres critere,int conforme, SygLots lot, String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygCriteresQualificationsSoumissionnaires.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("critere", "critere");
		criteria.createAlias("plis", "plis");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(critere!=null){
			criteria.add(Restrictions.eq("critere", critere));
		}
		if(lot!=null){
			criteria.add(Restrictions.eq("lot", lot));
		}
		if(conforme>-1)
			criteria.add(Restrictions.eq("conforme", conforme));
		if(libelle!=null){
			criteria.add(Restrictions.eq("critere.critere.libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygLotsSoumissionnaires.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygCriteresQualificationsSoumissionnaires> find(int indice, int pas,SygDossiers dossier,SygPlisouvertures plis,SygDossierssouscriteres critere,int conforme, SygLots lot, String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCriteresQualificationsSoumissionnaires.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("critere", "critere");
		criteria.createAlias("plis", "plis");
		criteria.addOrder(Order.asc("id"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(critere!=null){
			criteria.add(Restrictions.eq("critere", critere));
		}
		if(lot!=null){
			criteria.add(Restrictions.eq("lot", lot));
		}
		if(conforme>-1)
			criteria.add(Restrictions.eq("conforme", conforme));
		if(libelle!=null){
			criteria.add(Restrictions.eq("critere.critere.libelle", "%"+libelle+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygCriteresQualificationsSoumissionnaires critere) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(critere);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygCriteresQualificationsSoumissionnaires critere) {
		
		try{
			getHibernateSession().merge(critere);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygCriteresQualificationsSoumissionnaires findById(Long code) {
		return (SygCriteresQualificationsSoumissionnaires)getHibernateSession().get(SygCriteresQualificationsSoumissionnaires.class, code);
	}
	

	
	@Override
	public SygCriteresQualificationsSoumissionnaires getLot(Long code,SygDossiers dossier,SygPlisouvertures plis,SygDossierssouscriteres critere){
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCriteresQualificationsSoumissionnaires.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("critere", "critere");
		criteria.createAlias("plis", "plis");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(critere!=null){
			criteria.add(Restrictions.eq("critere", critere));
		}
		
		if(criteria.list().size()>0)
		  return (SygCriteresQualificationsSoumissionnaires) criteria.list().get(0);
		else 
			return null;
	}

		

}
