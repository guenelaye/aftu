package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygResultatNegociation;

@Remote
public interface ResultatNegociationSession {
	public void save(SygResultatNegociation resultat);
	public void delete(Long id);
	public List<SygResultatNegociation> find(int indice, int pas,SygDossiers dossier,SygAutoriteContractante autorite);
	public int count(SygDossiers dossier,SygAutoriteContractante autorite);
	public void update(SygResultatNegociation resultat);
	public SygResultatNegociation findById(Long code);
	public SygResultatNegociation getResultat(SygDossiers dossier,SygAutoriteContractante autorite);
	
}
