package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;

@Remote
public interface RegistrededepotSession {
	
	public void save(SygPlisouvertures plis);
	public void delete(Long id);
	public List<SygPlisouvertures> find(int indice, int pas,SygDossiers dossier,SygRetraitregistredao retrait,SygAutoriteContractante autorite,SygFournisseur fournisseur,int EtatExamenPreliminaire,int valide,int garantie, int critere, int piece, int attributaire, String nom, int preselectionne, String garanti, String ouvert);
	public List<SygPlisouvertures> find(int indice, int pas,SygDossiers dossier,SygRetraitregistredao retrait,SygAutoriteContractante autorite,SygFournisseur fournisseur,int EtatExamenPreliminaire,int valide,int garantie, int critere, int piece, int attributaire, String nom, int preselectionne, String signatureoffre, String exhaustivite, String garantiesoumission, String conformite);

	
	public int count(SygDossiers dossier,SygRetraitregistredao retrait,SygAutoriteContractante autorite,SygFournisseur fournisseur,int EtatExamenPreliminaire,int valide,int garantie, int critere, int piece, int attributaire, String nom, int preselectionne, String garanti, String ouvert);
	public SygPlisouvertures update(SygPlisouvertures plis);
	public SygPlisouvertures findById(Long code);
	public List<SygPlisouvertures> find(SygDossiers dossier,SygRetraitregistredao retrait,int numero, String nom);

	
	public SygPlisouvertures findSoumissionnaire(SygDossiers dossier,int etatexamen,int critere);
	
	public List<SygPlisouvertures> find(int indice, int pas,SygAutoriteContractante autorite,SygDossiers dossier,int etat);
	public int count(SygAutoriteContractante autorite,SygDossiers dossier,int etat);
	
	public List<SygPlisouvertures> ListeClassementFinancieres(int indice, int pas,SygDossiers dossier,int selectionne);
	public int countClassementFinancieres(SygDossiers dossier,int selectionne);
	public List<SygPlisouvertures> ClassementFinanciere(SygDossiers dossier,int selectionne);
	
	public List<SygPlisouvertures> ListeClassementFinal(int indice, int pas,SygDossiers dossier,int selectionne,Long soumissionnaire);
	public int countClassementFinal(SygDossiers dossier,int selectionne,Long soumissionnaire);
	SygPlisouvertures findLignePli(SygDossiers dossier,
			SygFournisseur founisseur);
	SygPlisouvertures savePli(SygPlisouvertures retrait);
	List<SygPlisouvertures> find2(int indice, int pas, SygDossiers dossier,
			SygRetraitregistredao retrait, SygAutoriteContractante autorite,
			SygFournisseur fournisseur, int EtatExamenPreliminaire, int valide,
			int garantie, int critere, int piece, int attributaire, String nom,
			int preselectionne, String garanti, String ouvert);


}
