package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygGarantiesDossiers;
import sn.ssi.kermel.be.entity.SygGarantiesSoumissionnaires;
import sn.ssi.kermel.be.entity.SygPlisouvertures;

@Remote
public interface GarantisSoumissionnairesSession {
	public void save(SygGarantiesSoumissionnaires garantie);
	public void delete(Long id);
	public List<SygGarantiesSoumissionnaires> find(int indice, int pas,SygDossiers dossier,SygPlisouvertures plis,SygGarantiesDossiers garantie,String fournie);
	public int count(SygDossiers dossier,SygPlisouvertures plis,SygGarantiesDossiers garantie,String fournie);
	public void update(SygGarantiesSoumissionnaires lot);
	public SygGarantiesSoumissionnaires findById(Long code);
	
	public SygGarantiesSoumissionnaires getLot(Long code,SygDossiers dossier,SygPlisouvertures plis,SygGarantiesDossiers garantie);
	
}
