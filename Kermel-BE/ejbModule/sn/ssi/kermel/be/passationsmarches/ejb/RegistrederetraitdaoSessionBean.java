package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.PklComptePerso;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierspieces;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;
import sn.ssi.kermel.be.referentiel.ejb.ParametresGenerauxSession;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class RegistrederetraitdaoSessionBean extends AbstractSessionBean implements RegistrederetraitdaoSession{

	@EJB
	ParametresGenerauxSession ParametresGenerauxSession;
	
	@Override
	public int count(SygDossiers dossier) {
		Criteria criteria = getHibernateSession().createCriteria(SygRetraitregistredao.class);
		criteria.createAlias("dossier", "dossier");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygRetraitregistredao.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygRetraitregistredao> find(int indice, int pas,SygDossiers dossier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygRetraitregistredao.class);
		criteria.createAlias("dossier", "dossier");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.addOrder(Order.asc("dateRetrait"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	
	@Override
	public SygRetraitregistredao findExiste(SygDossiers dossier,PklComptePerso user) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygRetraitregistredao.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("personne", "personne");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.addOrder(Order.asc("dateRetrait"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		
		if(dossier!=null){
			criteria.add(Restrictions.eq("personne", user));
		}
		
		  if( criteria.list().size()>0) return (SygRetraitregistredao) criteria.list().get(0) ;
		  else
		return  null;
	}

	

	@Override
	public SygRetraitregistredao save(SygRetraitregistredao retrait) {
		// TODO Auto-generated method stub
		getHibernateSession().save(retrait);
		getHibernateSession().flush();
		return retrait;
			}

	@Override
	public void update(SygRetraitregistredao retrait) {
		
		try{
			getHibernateSession().merge(retrait);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygRetraitregistredao findById(Long code) {
		return (SygRetraitregistredao)getHibernateSession().get(SygRetraitregistredao.class, code);
	}
	
	@Override
	public List<SygRetraitregistredao> Candidats(int indice, int pas,SygDossiers dossier,String nom) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygRetraitregistredao.class);
		criteria.createAlias("dossier", "dossier");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.addOrder(Order.asc("dateRetrait"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("nomSoumissionnaire", "%"+nom+"%"));
		}
		if(Candidats(dossier).size()>0)
			  criteria.add(Restrictions.not(Restrictions.in("id", Candidats(dossier))));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public int countCandidats(SygDossiers dossier,String nom) {
		Criteria criteria = getHibernateSession().createCriteria(SygRetraitregistredao.class);
		criteria.createAlias("dossier", "dossier");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("nomSoumissionnaire", "%"+nom+"%"));
		}
		if(Candidats(dossier).size()>0)
			  criteria.add(Restrictions.not(Restrictions.in("id", Candidats(dossier))));
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Long> Candidats(SygDossiers dossier) {
		// TODO Auto-generated method stub
		
		Criteria criteria = getHibernateSession().createCriteria(SygPlisouvertures.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("retrait", "retrait");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		criteria.setProjection(Projections.property("retrait.id"));
		return criteria.list();
	}

}
