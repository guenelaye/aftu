package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCategori;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;

@Remote
public interface AppelsOffresSession {
	public void save(SygAppelsOffres appel);
	public void delete(Long id);
	public List<SygAppelsOffres> find(int indice, int pas,Long type,Long mode,int annee,SygAutoriteContractante autorite, String reference, Date datedebut, Date datefin);
	public int count(Long type,Long mode,int annee,SygAutoriteContractante autorite, String reference, Date datedebut, Date datefin);
	
	
	public void update(SygAppelsOffres appel);
	public SygAppelsOffres findById(Long code);
	
	 //Add by Rama 02-11-2012
	public List<SygAppelsOffres> findRech(int indice, int pas,String Statut,SygAutoriteContractante autorite,String Objet);
	public int countRech(String Statut,SygAutoriteContractante autorite,String Objet);
	public List<SygAppelsOffres> findRech(int indice, int pas,SygAutoriteContractante autorite,String Objet);
	public int countRech(SygAutoriteContractante autorite,String Objet);
	public List<Long> findRech();
	List<SygAppelsOffres> findAll(String objet, String organisme);
	List<SygCategori> findAllSecteurActivite();
	int countCategorie(Long categorie);
	List<SygAppelsOffres> findCategorie(int indice, int pas, Long categorie);
	List<SygTypeAutoriteContractante> findAllTypeAutoriteContactante();
	int countTypeAutorite(SygTypeAutoriteContractante typeAutorite);
	List<SygAutoriteContractante> findAutoriteContactante(Long type);
	int countAutorite(SygAutoriteContractante autorite);
	List<SygAppelsOffres> findAppelOffreAutorite(int indice, int pas,
			Long autorite);
	int countAppelOffreAutorite(Long autorite);
	
}
