package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCourrierEvenement;



@Remote
public interface CourrierEvenementSession {
	public SygCourrierEvenement save(SygCourrierEvenement courrierevenement);
	public void delete(Long courrierId);
	public List<SygCourrierEvenement> find(int indice, int pas,Date courrierDateSaisie,Date courrierDateReception,Date courrierDate,String courrierReference,String courrierObjet,String courrierOrigine,String courrierCommentaire);
	public int count(Date courrierDateReception);
	public void update(SygCourrierEvenement courrierevenement);
	public SygCourrierEvenement findById(Long code);
	public List<SygCourrierEvenement> find(String code);
	List<SygCourrierEvenement> find(int indice, int pas);
	int count();
	
}

