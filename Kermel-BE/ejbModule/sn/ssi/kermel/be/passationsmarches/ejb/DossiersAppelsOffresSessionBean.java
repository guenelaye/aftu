package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Date;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.ibm.icu.util.Calendar;

import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossiersFournisseurs;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygTachesEffectues;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers.EtatOuverture;
import sn.ssi.kermel.be.referentiel.ejb.ParametresGenerauxSession;
import sn.ssi.kermel.be.workflow.entity.SysStateperm;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class DossiersAppelsOffresSessionBean extends AbstractSessionBean implements DossiersAppelsOffresSession{

	@EJB
	ParametresGenerauxSession ParametresGenerauxSession;
	Lock lock = new ReentrantLock();
	@Resource SessionContext context;
	
	@Override
	public int count(SygAutoriteContractante autorite,SygAppelsOffres appel,SygRealisations realisation, int etat, int gestion) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("realisation", "realisation");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(appel!=null){
			criteria.add(Restrictions.eq("appel", appel));
		}
		if(realisation!=null){
			criteria.add(Restrictions.eq("realisation", realisation));
		}
		if(etat>-1)
			criteria.add(Restrictions.eq("dosEtatValidation", etat));
		if(gestion>-1){
			criteria.add(Restrictions.eq("realisation.plan.annee", gestion));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygDossiers.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygDossiers> find(int indice, int pas,SygAutoriteContractante autorite,SygAppelsOffres appel,SygRealisations realisation, int etat, int gestion) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("natureprix", FetchMode.SELECT);
		criteria.addOrder(Order.asc("dosID"));
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(appel!=null){
			criteria.add(Restrictions.eq("appel", appel));
		}
		if(realisation!=null){
			criteria.add(Restrictions.eq("realisation", realisation));
		}
		if(etat>-1)
			criteria.add(Restrictions.eq("dosEtatValidation", etat));
		if(gestion>-1){
			criteria.add(Restrictions.eq("realisation.plan.annee", gestion));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	
	@Override
	public  SygDossiers findDos( SygAppelsOffres appel) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("natureprix", FetchMode.SELECT);
		//criteria.addOrder(Order.asc("dosID"));
		
		if(appel!=null){
			criteria.add(Restrictions.eq("appel", appel));
		}
		
		if(criteria.list().size()!=0)
			return (SygDossiers) criteria.list().get(0) ;
		else 
			return null;
	}

	
	
	

	@Override
	public SygDossiers save(SygDossiers dossier) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(dossier);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dossier;	}

	@Override
	public void update(SygDossiers dossier) {
		
		try{
			getHibernateSession().merge(dossier);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygDossiers findById(Long code) {
		return (SygDossiers)getHibernateSession().get(SygDossiers.class, code);
	}
	

	@Override
	public SygDossiers Dossier(Long code,SygAppelsOffres appel,int etatprequalif) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.setFetchMode("natureprix", FetchMode.SELECT);
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		if(code!=null)
			criteria.add(Restrictions.eq("dosID", code));
		if(appel!=null)
			criteria.add(Restrictions.eq("appel", appel));
		if(etatprequalif>-1){
			criteria.add(Restrictions.eq("etatPrequalif", etatprequalif));
		}
		if(criteria.list().size()>0)
		  return (SygDossiers) criteria.list().get(0);
		else
			return null;
	}
	
	@Override
	public List<SygDossiers> find(int indice, int pas,SygAutoriteContractante autorite,SygAppelsOffres appel,SygRealisations realisation) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("natureprix", FetchMode.SELECT);
		criteria.addOrder(Order.asc("dosID"));
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(appel!=null){
			criteria.add(Restrictions.eq("appel", appel));
		}
		if(realisation!=null){
			criteria.add(Restrictions.eq("realisation", realisation));
		}
		criteria.add(Restrictions.isNotNull("dosDatePublicationDefinitive"));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public List<SygDossiers> findAll( int offset,int noOfRecords, String objet, String organisme) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("natureprix", FetchMode.SELECT);
		criteria.add(Restrictions.isNotNull("dosDateLimiteDepot"));
//		criteria.add(Restrictions.isNotNull("dosDatePublication"));
		/* a gerer */
//		criteria.add(Restrictions.ge("dosDateLimiteDepot",new Date()));
		criteria.addOrder(Order.asc("dosID"));
		
		if(objet!=null){
			criteria.add(Restrictions.ilike("appel.apoobjet","%" +objet+"%"));
		}
		
		criteria.setFirstResult(offset);
		if(noOfRecords>0)
			criteria.setMaxResults(noOfRecords);
		return criteria.list();
	}
	
	@Override
	public List<SygDossiers> findAllPublie( int offset,int noOfRecords, String objet, String organisme) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("natureprix", FetchMode.SELECT);
		criteria.add(Restrictions.isNotNull("dosDateLimiteDepot"));
		criteria.add(Restrictions.isNotNull("dosDatePublication"));
		/* a gerer */
//		criteria.add(Restrictions.ge("dosDateLimiteDepot",new Date()));
		criteria.addOrder(Order.asc("dosID"));
		
		if(objet!=null){
			criteria.add(Restrictions.ilike("appel.apoobjet","%" +objet+"%"));
		}
		
		criteria.setFirstResult(offset);
		if(noOfRecords>0)
			criteria.setMaxResults(noOfRecords);
		return criteria.list();
	}
	
	
	@Override
	public List<SygDossiers> findAllType( int offset,int noOfRecords, Long type ,String objet, String organisme) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("natureprix", FetchMode.SELECT);
		criteria.add(Restrictions.isNotNull("dosDateLimiteDepot"));
		criteria.add(Restrictions.isNotNull("dosDatePublication"));
		criteria.add(Restrictions.ge("dosDateLimiteDepot",new Date()));
		
		criteria.addOrder(Order.asc("dosID"));
		
		if(type!=null){
			criteria.add(Restrictions.eq("appel.typemarche.id", type));
		}
		
		if(objet!=null){
			criteria.add(Restrictions.ilike("appel.apoobjet","%" +objet+"%"));
		}
		
		criteria.setFirstResult(offset);
		if(noOfRecords>0)
			criteria.setMaxResults(noOfRecords);
		return criteria.list();
	}
	
	
	@Override
	public  List<SygDossiers> findAppelOffreAutorite(int indice, int pas,Long autorite) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.autorite", "autorite");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("natureprix", FetchMode.SELECT);
		criteria.add(Restrictions.isNotNull("dosDateLimiteDepot"));
		criteria.add(Restrictions.isNotNull("dosDatePublication"));
		
		criteria.addOrder(Order.asc("dosID"));
		
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite.id", autorite));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public int countAppelOffreAutorite(Long autorite) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.autorite", "autorite");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("natureprix", FetchMode.SELECT);
		criteria.add(Restrictions.isNotNull("dosDateLimiteDepot"));
		criteria.add(Restrictions.isNotNull("dosDatePublication"));
		
		criteria.addOrder(Order.asc("dosID"));
		
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite.id", autorite));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	
	
	@Override
	public List<SygDossiers> findCountAll(String objet, String organisme) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("natureprix", FetchMode.SELECT);
		criteria.add(Restrictions.isNotNull("dosDateLimiteDepot"));
		criteria.add(Restrictions.isNotNull("dosDatePublication"));
		criteria.add(Restrictions.ge("dosDateLimiteDepot",new Date()));
		
		criteria.addOrder(Order.asc("dosID"));
		
		if(objet!=null){
			criteria.add(Restrictions.ilike("appel.apoobjet","%" +objet+"%"));
		}
	
		
		return criteria.list();
	}
	
	
	@Override
	public List<SygDossiers> findCountAllType(Long type,String objet, String organisme) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("natureprix", FetchMode.SELECT);
		criteria.add(Restrictions.isNotNull("dosDateLimiteDepot"));
		criteria.add(Restrictions.isNotNull("dosDatePublication"));
		criteria.add(Restrictions.ge("dosDateLimiteDepot",new Date()));
		criteria.addOrder(Order.asc("dosID"));
		
		if(type!=null){
			criteria.add(Restrictions.eq("appel.typemarche.id", type));
		}
		
		if(objet!=null){
			criteria.add(Restrictions.ilike("appel.apoobjet","%" +objet+"%"));
		}
	
		
		return criteria.list();
	}
	
	
	@Override
	public int countCategorie(Long categorie) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("natureprix", FetchMode.SELECT);
		criteria.add(Restrictions.isNotNull("dosDateLimiteDepot"));
		criteria.add(Restrictions.isNotNull("dosDatePublication"));
		criteria.add(Restrictions.ge("dosDateLimiteDepot",new Date()));
		
		if(categorie!=null){
			criteria.add(Restrictions.eq("appel.categorie.id", categorie));
		}
	
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	
	@Override
	public int countTypeAutorite(SygTypeAutoriteContractante typeAutorite) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.autorite", "autorite");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("natureprix", FetchMode.SELECT);
		criteria.add(Restrictions.isNotNull("dosDateLimiteDepot"));
		criteria.add(Restrictions.isNotNull("dosDatePublication"));
		criteria.add(Restrictions.ge("dosDateLimiteDepot",new Date()));
		
		if(typeAutorite!=null){
			criteria.add(Restrictions.eq("autorite.type", typeAutorite));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	
	@Override
	public int countAutorite(SygAutoriteContractante autorite) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.autorite", "autorite");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("natureprix", FetchMode.SELECT);
		criteria.add(Restrictions.isNotNull("dosDateLimiteDepot"));
		criteria.add(Restrictions.isNotNull("dosDatePublication"));
		
			
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	
	@Override
	public  List<SygDossiers> findCategorie(int indice, int pas,Long categorie) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("natureprix", FetchMode.SELECT);
		criteria.add(Restrictions.isNotNull("dosDateLimiteDepot"));
		criteria.add(Restrictions.isNotNull("dosDatePublication"));
		
		if(categorie!=null){
			criteria.add(Restrictions.eq("appel.categorie.id", categorie));
		}

		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	
	@Override
	public int count(SygAutoriteContractante autorite,SygAppelsOffres appel,SygRealisations realisation) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("realisation", "realisation");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(appel!=null){
			criteria.add(Restrictions.eq("appel", appel));
		}
		if(realisation!=null){
			criteria.add(Restrictions.eq("realisation", realisation));
		}
		criteria.add(Restrictions.isNotNull("dosDatePublicationDefinitive"));
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	@Override
	public List<SygDossiers> RevueDAO(int indice, int pas,String autorite, int etat, int gestion) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.asc("dosID"));
		if(autorite!=null){
			criteria.add(Restrictions.ilike("autorite.denomination","%" +autorite+"%"));
		}
		
		if(etat>-1)
			criteria.add(Restrictions.eq("dosEtatValidation", etat));
		if(gestion>-1){
			criteria.add(Restrictions.eq("realisation.plan.annee", gestion));
		}
		criteria.add(Restrictions.and(Restrictions.isNotNull("dosDateMiseValidation"), Restrictions.isNull("dosDateValidation")));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public int countRevueDAO(String autorite, int etat, int gestion) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("autorite", "autorite");
		if(autorite!=null){
			criteria.add(Restrictions.ilike("autorite.denomination","%" +autorite+"%"));
		}
		
		if(etat>-1)
			criteria.add(Restrictions.eq("dosEtatValidation", etat));
		if(gestion>-1){
			criteria.add(Restrictions.eq("realisation.plan.annee", gestion));
		}
		criteria.add(Restrictions.and(Restrictions.isNotNull("dosDateMiseValidation"), Restrictions.isNull("dosDateValidation")));
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	@Override
	public List<SygDossiers> ValidationAttributionprovisoire(int indice, int pas,String autorite, int etat, int gestion) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.asc("dosID"));
		if(autorite!=null){
			criteria.add(Restrictions.ilike("autorite.denomination","%" +autorite+"%"));
		}
		
		if(etat>-1)
			criteria.add(Restrictions.eq("dosEtatValidation", etat));
		if(gestion>-1){
			criteria.add(Restrictions.eq("realisation.plan.annee", gestion));
		}
		criteria.add(Restrictions.and(Restrictions.isNotNull("dosDateMiseValidationattribution"), Restrictions.isNull("dosDateValidationPrequalif")));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public int countValidationAttributionprovisoire(String autorite, int etat, int gestion) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("autorite", "autorite");
		if(autorite!=null){
			criteria.add(Restrictions.ilike("autorite.denomination","%" +autorite+"%"));
		}
		
		if(etat>-1)
			criteria.add(Restrictions.eq("dosEtatValidation", etat));
		if(gestion>-1){
			criteria.add(Restrictions.eq("realisation.plan.annee", gestion));
		}
		criteria.add(Restrictions.and(Restrictions.isNotNull("dosDateMiseValidationattribution"), Restrictions.isNull("dosDateValidationPrequalif")));
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	@Override
	public List<SygDossiers> ExamenJurique(int indice, int pas,String autorite, int etat, int gestion) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.asc("dosID"));
		if(autorite!=null){
			criteria.add(Restrictions.ilike("autorite.denomination","%" +autorite+"%"));
		}
		
		if(etat>-1)
			criteria.add(Restrictions.eq("dosEtatValidation", etat));
		if(gestion>-1){
			criteria.add(Restrictions.eq("realisation.plan.annee", gestion));
		}
		criteria.add(Restrictions.and(Restrictions.isNotNull("dosDateMiseValidationSignature"), Restrictions.isNull("dosDateValidationSignature")));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public int countExamenJurique(String autorite, int etat, int gestion) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("autorite", "autorite");
		if(autorite!=null){
			criteria.add(Restrictions.ilike("autorite.denomination","%" +autorite+"%"));
		}
		
		if(etat>-1)
			criteria.add(Restrictions.eq("dosEtatValidation", etat));
		if(gestion>-1){
			criteria.add(Restrictions.eq("realisation.plan.annee", gestion));
		}
		criteria.add(Restrictions.and(Restrictions.isNotNull("dosDateMiseValidationSignature"), Restrictions.isNull("dosDateValidationSignature")));
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	
	@Override
	public List<SygDossiers> DossiersEnAttenteOP(int indice, int pas,SygAutoriteContractante autorite,String reference,String objet,String modepassation,String typemarche,Date date) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("appel.typemarche", "type");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.asc("dosID"));
		if(autorite!=null)
			criteria.add(Restrictions.eq("autorite", autorite));
		if(reference!=null){
			criteria.add(Restrictions.ilike("dosReference","%" +reference+"%"));
		}
		if(objet!=null){
			criteria.add(Restrictions.ilike("appel.apoobjet","%" +objet+"%"));
		}
		if(modepassation!=null){
			criteria.add(Restrictions.ilike("mode.libelle","%" +modepassation+"%"));
		}	
		if(typemarche!=null){
			criteria.add(Restrictions.ilike("type.libelle","%" +typemarche+"%"));
		}
		
		
		if(date!=null)
			criteria.add(Restrictions.ge("dosDateOuvertueDesplis", date));
		
//		criteria.add(Restrictions.le ("dosDateOuvertueDesplis", date));
		criteria.add(Restrictions.isNull("appel.apoDatepvouverturepli"));
		criteria.add(Restrictions.or(Restrictions.eq("appel.modeSoumission", 2), Restrictions.eq("appel.modeSoumission", 3)));
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public int countDossiersEnAttenteOP(SygAutoriteContractante autorite,String reference,String objet,String modepassation,String typemarche,Date date) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("appel.typemarche", "type");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("autorite", "autorite");
		if(autorite!=null)
			criteria.add(Restrictions.eq("autorite", autorite));
		if(reference!=null){
			criteria.add(Restrictions.ilike("dosReference","%" +reference+"%"));
		}
		if(objet!=null){
			criteria.add(Restrictions.ilike("appel.apoobjet","%" +objet+"%"));
		}
		if(modepassation!=null){
			criteria.add(Restrictions.ilike("mode.libelle","%" +modepassation+"%"));
		}	
		if(typemarche!=null){
			criteria.add(Restrictions.ilike("type.libelle","%" +typemarche+"%"));
		}
		if(date!=null)
			criteria.add(Restrictions.ge("dosDateOuvertueDesplis", date));
		criteria.add(Restrictions.isNull("appel.apoDatepvouverturepli"));
		criteria.add(Restrictions.or(Restrictions.eq("appel.modeSoumission", 2), Restrictions.eq("appel.modeSoumission", 3)));
		
		criteria.add(Restrictions.eq("etatOuverture", EtatOuverture.ENATTENTE));
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	//21/01/2016
    ////:::::::Taches Effectues//////////
	@Override
	public void deleteTache(Long id) {
		// TODO Auto-generated method stub
		lock.lock();
		try {
			getHibernateSession().delete(getHibernateSession().get(SygTachesEffectues.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
		finally{
			lock.unlock();
		}
	}
	
	@Override
	public SygTachesEffectues saveTache(SygTachesEffectues tache) {
		// TODO Auto-generated method stub
		lock.lock();
		try {
			getHibernateSession().save(tache);
			getHibernateSession().flush();
			return tache;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
		
		finally{
			lock.unlock();
		}
	}

	@Override
	public void updateTache(SygTachesEffectues tache) {
		lock.lock();
		try{
			getHibernateSession().merge(tache);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			lock.unlock();
		}
	}

	
	@Override
	public SygTachesEffectues Tache(Long code) {
		return (SygTachesEffectues)getHibernateSession().get(SygTachesEffectues.class, code);
	}
	
	@Override
	public SygTachesEffectues Taches(Long dossier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTachesEffectues.class);
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier",dossier));
		}
	  if(criteria.list().size()>0)
		 return (SygTachesEffectues) criteria.list().get(0);
	  else return null;
	}
	
	///26/01/2016
	
	@Override
	public List<SygDossiersFournisseurs> DossiersFournisseurs(Long Fournisseur,Date date) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiersFournisseurs.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "modepassation");
		criteria.addOrder(Order.asc("dateinvitation"));
		if(Fournisseur!=null)
			criteria.add(Restrictions.eq("fournisseur.id", Fournisseur));
		
		if(date!=null)
			criteria.add(Restrictions.le("dossier.dosDateLimiteDepot", date));
		criteria.add(Restrictions.isNull("appel.apoDatepvouverturepli"));
//		criteria.add(Restrictions.eq("appel.modeSoumission", 3));		
		Criterion modesm1 = Restrictions.eq("appel.modeSoumission", 2);
		Criterion modesm2 = Restrictions.eq("appel.modeSoumission", 3);
		LogicalExpression modeSoumission = Restrictions.or(modesm1, modesm2);
		criteria.add(modeSoumission);
		criteria.add(Restrictions.eq("modepassation.id", 34L));
			
		
		return criteria.list();
	}
	
	
	@Override
	public List<SygDossiersFournisseurs> findDossiersFournisseursNonRepondus(Long fournisseur,Date date) {
		
		DetachedCriteria reponses = DetachedCriteria.forClass(SygPlisouvertures.class);
		reponses.createAlias("dossier", "dossier");
		reponses.createAlias("fournisseur", "fournisseur");
		
		if(fournisseur!=null)
			reponses.add(Restrictions.eq("fournisseur.id", fournisseur));
		
		reponses.setProjection(Projections.property("dossier.id"));
		
//		reponses.getExecutableCriteria(getHibernateSession()).list().toArray()
		
		List lstReponses = reponses.getExecutableCriteria(getHibernateSession()).list();
		
		
		Criteria criteria = getHibernateSession().createCriteria(SygDossiersFournisseurs.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "modepassation");
		criteria.addOrder(Order.asc("dateinvitation"));
		if(lstReponses.size() > 0)
		criteria.add(Restrictions.not(Restrictions.in("dossier.id", lstReponses)));
		if(fournisseur!=null)
			criteria.add(Restrictions.eq("fournisseur.id", fournisseur));
		
		if(date!=null)
			criteria.add(Restrictions.le("dossier.dosDateLimiteDepot", date));
		criteria.add(Restrictions.isNull("appel.apoDatepvouverturepli"));
//		criteria.add(Restrictions.eq("appel.modeSoumission", 3));		
		Criterion modesm1 = Restrictions.eq("appel.modeSoumission", 2);
		Criterion modesm2 = Restrictions.eq("appel.modeSoumission", 3);
		LogicalExpression modeSoumission = Restrictions.or(modesm1, modesm2);
		criteria.add(modeSoumission);
		criteria.add(Restrictions.eq("modepassation.id", 34L));
			
		
		return criteria.list();
	}
	
	@Override
	public int countDossiersEnCoursOP(SygAutoriteContractante autorite,String reference,String objet,String modepassation,String typemarche,Date date) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.createAlias("appel", "appel");
		criteria.createAlias("appel.modepassation", "mode");
		criteria.createAlias("appel.typemarche", "type");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("autorite", "autorite");
		if(autorite!=null)
			criteria.add(Restrictions.eq("autorite", autorite));
		if(reference!=null){
			criteria.add(Restrictions.ilike("dosReference","%" +reference+"%"));
		}
		if(objet!=null){
			criteria.add(Restrictions.ilike("appel.apoobjet","%" +objet+"%"));
		}
		if(modepassation!=null){
			criteria.add(Restrictions.ilike("mode.libelle","%" +modepassation+"%"));
		}	
		if(typemarche!=null){
			criteria.add(Restrictions.ilike("type.libelle","%" +typemarche+"%"));
		}
		if(date!=null)
			criteria.add(Restrictions.ge("dosDateOuvertueDesplis", date));
		criteria.add(Restrictions.isNull("appel.apoDatepvouverturepli"));
		criteria.add(Restrictions.or(Restrictions.eq("appel.modeSoumission", 2), Restrictions.eq("appel.modeSoumission", 3)));
		
		criteria.add(Restrictions.eq("etatOuverture", EtatOuverture.ENCOURS));
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
}
