package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.referentiel.ejb.ParametresGenerauxSession;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class ContratsSessionBean extends AbstractSessionBean implements ContratsSession{

	@EJB
	ParametresGenerauxSession ParametresGenerauxSession;
	
	@Override
	public int count(SygDossiers dossier,SygAutoriteContractante autorite,SygPlisouvertures plis,String statut,SygFournisseur fournisseur,int immatriculation) {
		Criteria criteria = getHibernateSession().createCriteria(SygContrats.class);
		criteria.createAlias("plis", "plis");
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("autorite", "autorite");
//		criteria.setFetchMode("dossier", FetchMode.SELECT);
//		criteria.setFetchMode("autorite", FetchMode.SELECT);
//		criteria.setFetchMode("plis", FetchMode.SELECT);
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		
		if(statut!=null){
			criteria.add(Restrictions.eq("constatus", statut));
		}
		
		if(fournisseur!=null){
			criteria.add(Restrictions.eq("plis.fournisseur", fournisseur));
		}
		if(immatriculation>-1)
			criteria.add(Restrictions.eq("immatriculation", immatriculation));
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	@Override
	public int countTypeMarche(Long type,String statut) {
		Criteria criteria = getHibernateSession().createCriteria(SygContrats.class);
		//criteria.createAlias("plis", "plis");
		//criteria.createAlias("dossier", "dossier");
		//criteria.createAlias("autorite", "autorite");
		/*if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		
		if(statut!=null){
			criteria.add(Restrictions.eq("constatus", statut));
		}
		
		if(fournisseur!=null){
			criteria.add(Restrictions.eq("plis.fournisseur", fournisseur));
		}
		   */
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygContrats.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygContrats> find(int indice, int pas,SygDossiers dossier,SygAutoriteContractante autorite,SygPlisouvertures plis,String statut,SygFournisseur fournisseur,int immatriculation) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygContrats.class);
		criteria.createAlias("plis", "plis");
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.asc("conID"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		
		if(statut!=null){
			criteria.add(Restrictions.eq("constatus", statut));
		}
		
		if(fournisseur!=null){
			criteria.add(Restrictions.eq("plis.fournisseur", fournisseur));
		}
		if(immatriculation>-1)
			criteria.add(Restrictions.eq("immatriculation", immatriculation));
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygContrats> findbyTypeMarche(int indice, int pas,Long type,String statut) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygContrats.class);
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}


	@Override
	public void save(SygContrats contrat) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(contrat);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygContrats contrat) {
		
		try{
			getHibernateSession().merge(contrat);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public void updateContrat(SygContrats contrat) {
		
		try{
			getHibernateSession().merge(contrat);
			ParametresGenerauxSession.incrementeCode(BeConstants.PARAM_NUMIMMATRICULATION);

			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

		@Override
	public SygContrats findById(Long code) {
		return (SygContrats)getHibernateSession().get(SygContrats.class, code);
	}
	

	@Override
	public SygContrats getContrat(SygDossiers dossier,SygAutoriteContractante autorite,SygPlisouvertures plis,Long code,SygLots lot){
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygContrats.class);
		criteria.createAlias("plis", "plis");
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("dossier.appel", "appel");
		criteria.createAlias("autorite", "autorite");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(code!=null){
			criteria.add(Restrictions.eq("conID", code));
		}
		if(lot!=null){
			criteria.add(Restrictions.eq("lot", lot));
		}
		if(criteria.list().size()>0)
		  return (SygContrats) criteria.list().get(0);
		else 
			return null;
	}


	@Override
	public String getGeneratedCode(String code) {
	
		String dateCoupee = (Calendar.getInstance().getTime()).toString();
		String mois = String.format("%02d", Calendar.getInstance().get(Calendar.MONTH)+1);
		dateCoupee = dateCoupee.substring(dateCoupee.length() - 4, dateCoupee
				.length());
		Long codeParametre = (long) ParametresGenerauxSession.getValeurParametre(code);
		if (codeParametre < 10)
			return dateCoupee +mois+ "000" + codeParametre;
		else if ((codeParametre >= 10) && (codeParametre < 100))
			return dateCoupee+mois + "00" + codeParametre;
		else if ((codeParametre >= 100) && (codeParametre < 1000))
			return dateCoupee+mois + "0" + codeParametre;
		else
			return dateCoupee+mois + "" + codeParametre;
	}
	
	
	
	//
		@Override
	public int count(SygDossiers dossier,SygAutoriteContractante autorite,SygPlisouvertures plis,String statut,SygFournisseur fournisseur) {
		Criteria criteria = getHibernateSession().createCriteria(SygContrats.class);
		criteria.createAlias("plis", "plis");
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("autorite", "autorite");
//		criteria.setFetchMode("dossier", FetchMode.SELECT);
//		criteria.setFetchMode("autorite", FetchMode.SELECT);
//		criteria.setFetchMode("plis", FetchMode.SELECT);
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		
		if(statut!=null){
			criteria.add(Restrictions.eq("constatus", statut));
		}
		
		if(fournisseur!=null){
			criteria.add(Restrictions.eq("plis.fournisseur", fournisseur));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
		@SuppressWarnings("unchecked")
		@Override
	public List<SygContrats> find(int indice, int pas,SygDossiers dossier,SygAutoriteContractante autorite,SygPlisouvertures plis,String statut,SygFournisseur fournisseur) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygContrats.class);
		criteria.createAlias("plis", "plis");
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.asc("conID"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		
		if(statut!=null){
			criteria.add(Restrictions.eq("constatus", statut));
		}
		
		if(fournisseur!=null){
			criteria.add(Restrictions.eq("plis.fournisseur", fournisseur));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
}
