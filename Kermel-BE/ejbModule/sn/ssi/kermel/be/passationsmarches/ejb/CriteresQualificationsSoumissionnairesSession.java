package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCriteresQualificationsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygPlisouvertures;

@Remote
public interface CriteresQualificationsSoumissionnairesSession {
	public void save(SygCriteresQualificationsSoumissionnaires critere);
	public void delete(Long id);
	public List<SygCriteresQualificationsSoumissionnaires> find(int indice, int pas,SygDossiers dossier,SygPlisouvertures plis,SygDossierssouscriteres critere,int conforme, SygLots lot, String libelle);
	public int count(SygDossiers dossier,SygPlisouvertures plis,SygDossierssouscriteres critere,int conforme, SygLots lot, String libelle);
	public void update(SygCriteresQualificationsSoumissionnaires lot);
	public SygCriteresQualificationsSoumissionnaires findById(Long code);
	
	public SygCriteresQualificationsSoumissionnaires getLot(Long code,SygDossiers dossier,SygPlisouvertures plis,SygDossierssouscriteres critere);
	
}
