package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygObservateursIndependants;

@Remote
public interface ObservateursIndependantsSession {
	public void save(SygObservateursIndependants presence);
	public void delete(Long id);
	public List<SygObservateursIndependants> find(int indice, int pas,SygDossiers dossier,int etape);
	public int count(SygDossiers dossier,int etape);
	public void update(SygObservateursIndependants presence);
	public SygObservateursIndependants findById(Long code);
	
	
}
