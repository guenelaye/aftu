package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierscommissionsmarches;

@Remote
public interface DossierscommissionsmarchesSession {
	public void save(SygDossierscommissionsmarches membre);
	public void delete(Long id);
	public List<SygDossierscommissionsmarches> find(int indice, int pas,SygDossiers dossier,int presence,int etape);
	public int count(SygDossiers dossier,int presence,int etape);
	public void update(SygDossierscommissionsmarches membre);
	public SygDossierscommissionsmarches findById(Long code);
	
	
}
