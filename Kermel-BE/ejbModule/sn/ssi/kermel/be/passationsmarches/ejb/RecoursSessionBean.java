package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygRecours;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class RecoursSessionBean extends AbstractSessionBean implements RecoursSession{

	
	@Override
	public int count(SygDossiers dossier,SygAutoriteContractante autorite,SygAppelsOffres appel,Date dateCourrier) {
		Criteria criteria = getHibernateSession().createCriteria(SygRecours.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("autorite", "autorite");
		criteria.createAlias("appel", "appel");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(appel!=null){
			criteria.add(Restrictions.eq("appel", appel));
		}
		if(dateCourrier!=null){
			criteria.add(Restrictions.eq("dateCourrier", dateCourrier));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygRecours.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygRecours> find(int indice, int pas,SygDossiers dossier,SygAutoriteContractante autorite,SygAppelsOffres appel,String Refrecours,String ObjetRecours,Date DateRecous,Date dateCourrier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygRecours.class);
		
		
		
		//criteria.addOrder(Order.asc("dateRecous"));
		if(dossier!=null){
			criteria.createAlias("dossier", "dossier");
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(autorite!=null){
			criteria.createAlias("autorite", "autorite");
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(appel!=null){
			criteria.createAlias("appel", "appel");
			criteria.add(Restrictions.eq("appel", appel));
		}
		if(Refrecours!=null){
			criteria.add(Restrictions.eq("Refrecours", Refrecours));
		}
		if(ObjetRecours!=null){
			criteria.add(Restrictions.eq("ObjetRecours", ObjetRecours));
		}
		if(DateRecous!=null){
			criteria.add(Restrictions.eq("DateRecous", DateRecous));
		}
		if(dateCourrier!=null){
			criteria.add(Restrictions.eq("dateCourrier", dateCourrier));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygRecours recours) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(recours);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygRecours recours) {
		
		try{
			getHibernateSession().merge(recours);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygRecours findById(Long code) {
		return (SygRecours)getHibernateSession().get(SygRecours.class, code);
	}
	




}
