package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDevise;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.referentiel.ejb.ParametresGenerauxSession;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class LotsSessionBean extends AbstractSessionBean implements LotsSession{

	@EJB
	ParametresGenerauxSession ParametresGenerauxSession;
	
	@Override
	public int count(SygDossiers dossier,SygContrats contrat) {
		Criteria criteria = getHibernateSession().createCriteria(SygLots.class);
		criteria.createAlias("dossier", "dossier");
		criteria.setFetchMode("contrat", FetchMode.SELECT);
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(contrat!=null){
			criteria.add(Restrictions.eq("contrat", contrat));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygLots.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygLots> find(int indice, int pas,SygDossiers dossier,SygContrats contrat) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygLots.class);
		criteria.createAlias("dossier", "dossier");
		criteria.setFetchMode("contrat", FetchMode.SELECT);
		criteria.addOrder(Order.asc("numero"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(contrat!=null){
			criteria.add(Restrictions.eq("contrat", contrat));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public List<SygLots> findLotsByIdDossier(SygDossiers dossier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygLots.class);
		criteria.createAlias("dossier", "dossier");
		criteria.setFetchMode("contrat", FetchMode.SELECT);
		criteria.addOrder(Order.asc("numero"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		
		return criteria.list();
	}

	

	@Override
	public void save(SygLots lot) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(lot);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygLots lot) {
		
		try{
			getHibernateSession().merge(lot);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygLots findById(Long code) {
		return (SygLots)getHibernateSession().get(SygLots.class, code);
	}
	

	

}
