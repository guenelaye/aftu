package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.PklEnveloppesFichiers;
import sn.ssi.kermel.be.entity.PklPlis;
import sn.ssi.kermel.be.entity.PklPlisEnveloppes;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class SoumissionElectroniqueSessionBean extends AbstractSessionBean
		implements SoumissionElectroniqueSession {

	// ///////////Plis///////////

	@Override
	public int countPlis(SygDossiers dossier, SygFournisseur fournisseur) {
		Criteria criteria = getHibernateSession().createCriteria(PklPlis.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("fournisseur", "fournisseur");
		if (dossier != null) {
			criteria.add(Restrictions.eq("dossier", dossier));
		}

		if (fournisseur != null) {
			criteria.add(Restrictions.eq("fournisseur", fournisseur));
		}

		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void deletePlis(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(PklPlis.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<PklPlis> Plis(int indice, int pas, SygDossiers dossier,
			SygFournisseur fournisseur) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(PklPlis.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("fournisseur", "fournisseur");
		criteria.addOrder(Order.asc("libelle"));
		if (dossier != null) {
			criteria.add(Restrictions.eq("dossier", dossier));
		}

		if (fournisseur != null) {
			criteria.add(Restrictions.eq("fournisseur", fournisseur));
		}
		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

	@Override
	public PklPlis findPlisExiste(SygDossiers dossier,
			SygFournisseur fournisseur) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(PklPlis.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("fournisseur", "fournisseur");
		criteria.addOrder(Order.asc("libelle"));
		if (dossier != null) {
			criteria.add(Restrictions.eq("dossier", dossier));
		}

		if (fournisseur != null) {
			criteria.add(Restrictions.eq("fournisseur", fournisseur));
		}
		if (criteria.list().size() > 0)
			return (PklPlis) criteria.list().get(0);
		else
			return null;

		// return criteria.list();
	}

	@Override
	public PklPlis savePlis(PklPlis plis) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(plis);
			getHibernateSession().flush();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return plis;
	}

	@Override
	public void updatePlis(PklPlis plis) {

		try {
			getHibernateSession().merge(plis);
			getHibernateSession().flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public PklPlis findPlis(Long code) {
		return (PklPlis) getHibernateSession().get(PklPlis.class, code);
	}

	// ///////////Enveloppes///////////

	@Override
	public PklPlisEnveloppes saveEnveloppe(PklPlisEnveloppes enveloppe) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(enveloppe);
			getHibernateSession().flush();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return enveloppe;
	}

	@Override
	public void updateEnveloppe(PklPlisEnveloppes enveloppe) {

		try {
			getHibernateSession().merge(enveloppe);
			getHibernateSession().flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteEnveloppe(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(PklPlisEnveloppes.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public PklPlisEnveloppes findEnveloppe(Long code) {
		return (PklPlisEnveloppes) getHibernateSession().get(
				PklPlisEnveloppes.class, code);
	}

	@Override
	public List<PklPlisEnveloppes> Enveloppes(int indice, int pas, PklPlis plis) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PklPlisEnveloppes.class);
		criteria.createAlias("plis", "plis");
		criteria.addOrder(Order.asc("libelle"));
		if (plis != null) {
			criteria.add(Restrictions.eq("plis", plis));
		}

		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

	@Override
	public int countEnveloppes(PklPlis plis) {
		Criteria criteria = getHibernateSession().createCriteria(
				PklPlisEnveloppes.class);
		criteria.createAlias("plis", "plis");
		if (plis != null) {
			criteria.add(Restrictions.eq("plis", plis));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	// ///////////Fichiers///////////

	@Override
	public void saveFichier(PklEnveloppesFichiers enveloppe) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(enveloppe);
			getHibernateSession().flush();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void updateFichier(PklEnveloppesFichiers enveloppe) {

		try {
			getHibernateSession().merge(enveloppe);
			getHibernateSession().flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteFichier(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(PklEnveloppesFichiers.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public PklEnveloppesFichiers findFichier(Long code) {
		return (PklEnveloppesFichiers) getHibernateSession().get(
				PklEnveloppesFichiers.class, code);
	}

	@Override
	public List<PklEnveloppesFichiers> Fichiers(int indice, int pas,
			PklPlisEnveloppes enveloppe) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PklEnveloppesFichiers.class);
		criteria.createAlias("enveloppe", "enveloppe");
		criteria.addOrder(Order.asc("libelle"));
		if (enveloppe != null) {
			criteria.add(Restrictions.eq("enveloppe", enveloppe));
		}

		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

	@Override
	public int countFichier(PklPlisEnveloppes enveloppe) {
		Criteria criteria = getHibernateSession().createCriteria(
				PklEnveloppesFichiers.class);
		criteria.createAlias("enveloppe", "enveloppe");
		if (enveloppe != null) {
			criteria.add(Restrictions.eq("enveloppe", enveloppe));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public PklEnveloppesFichiers getOffreFinanciere(Long idFournisseur,
			Long idDossier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PklEnveloppesFichiers.class);
		criteria.createAlias("enveloppe", "enveloppe");
		criteria.createAlias("enveloppe.plis", "pli");
		criteria.createAlias("pli.fournisseur", "fournisseur");
		criteria.createAlias("pli.dossier", "dossier");
		
		criteria.add(Restrictions.ilike("libelle", "financiere", MatchMode.ANYWHERE));


		if (idFournisseur != null) {
			criteria.add(Restrictions.eq("fournisseur.id", idFournisseur));
		}
		if (idDossier != null) {
			criteria.add(Restrictions.eq("dossier.dosID", idDossier));
		}

		List lst = criteria.list();

		if (lst.size() > 0)
			return (PklEnveloppesFichiers) lst.get(0);
		else
			return null;
	}
}
