package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossiersEvaluateurs;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class DossiersEvaluateursSessionBean extends AbstractSessionBean implements DossiersEvaluateursSession{


	
	@Override
	public int count(SygDossiers dossier) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossiersEvaluateurs.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("evaluateur", "evaluateur");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygDossiersEvaluateurs.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygDossiersEvaluateurs> find(int indice, int pas,SygDossiers dossier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossiersEvaluateurs.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("evaluateur", "evaluateur");
		criteria.addOrder(Order.asc("evaluateur.nom"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygDossiersEvaluateurs evaluateur) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(evaluateur);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygDossiersEvaluateurs evaluateur) {
		
		try{
			getHibernateSession().merge(evaluateur);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygDossiersEvaluateurs findById(Long code) {
		return (SygDossiersEvaluateurs)getHibernateSession().get(SygDossiersEvaluateurs.class, code);
	}
	


}
