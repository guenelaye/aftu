package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierspieces;

@Remote
public interface DossiersPiecesSession {
	public void save(SygDossierspieces piece);
	public void delete(Long id);
	public List<SygDossierspieces> find(int indice, int pas,SygDossiers dossier);
	public int count(SygDossiers dossier);
	public void update(SygDossierspieces piece);
	public SygDossierspieces findById(Long code);
	
	
}
