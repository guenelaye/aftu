package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygGarantiesDossiers;
import sn.ssi.kermel.be.entity.SygPiecesrecus;

@Remote
public interface GarantiesSession {
	public void save(SygGarantiesDossiers garantie);
	public void delete(Long id);
	public List<SygGarantiesDossiers> find(int indice, int pas,SygDossiers dossier,SygPiecesrecus piece,SygAutoriteContractante autorite);
	public int count(SygDossiers dossier,SygPiecesrecus piece,SygAutoriteContractante autorite);
	public void update(SygGarantiesDossiers garantie);
	public SygGarantiesDossiers findById(Long code);
	
	
}
