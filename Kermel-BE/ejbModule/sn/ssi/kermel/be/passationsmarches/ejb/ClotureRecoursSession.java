package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygClotureRecours;






@Remote
public interface ClotureRecoursSession {
	public SygClotureRecours save(SygClotureRecours cloturerecours);
	public void delete(Long Id);
	public List<SygClotureRecours> find(int indice, int pas,Date Datecloture,String Commentaire,String Issuerecoursgracieux);
	public int count(Date Datecloture);
	public void update(SygClotureRecours cloturerecours);
	public SygClotureRecours findById(Long code);
	public List<SygClotureRecours> find(String code);
	List<SygClotureRecours> find(int indice, int pas);
	int count();
	
}

