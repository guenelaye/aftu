package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAttributions;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygPlisouvertures;

@Remote
public interface AttributionsSession {
	public void save(SygAttributions attributaire);
	public void delete(Long id);
	public List<SygAttributions> find(int indice, int pas,SygDossiers dossier,SygPlisouvertures plis);
	public int count(SygDossiers dossier,SygPlisouvertures plis);
	public void update(SygAttributions attributaire);
	public SygAttributions findById(Long code);
	
	public SygAttributions findAttributaire(SygDossiers dossier,SygPlisouvertures plis,SygLots lot);
}
