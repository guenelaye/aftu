package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAttributions;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.referentiel.ejb.ParametresGenerauxSession;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class AttributionsSessionBean extends AbstractSessionBean implements AttributionsSession{

	@EJB
	ParametresGenerauxSession ParametresGenerauxSession;
	
	@Override
	public int count(SygDossiers dossier,SygPlisouvertures plis) {
		Criteria criteria = getHibernateSession().createCriteria(SygAttributions.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("plis", "plis");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygAttributions.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygAttributions> find(int indice, int pas,SygDossiers dossier,SygPlisouvertures plis) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAttributions.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("plis", "plis");
		criteria.addOrder(Order.asc("plis.raisonsociale"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygAttributions attributaire) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(attributaire);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygAttributions attributaire) {
		
		try{
			getHibernateSession().merge(attributaire);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygAttributions findById(Long code) {
		return (SygAttributions)getHibernateSession().get(SygAttributions.class, code);
	}
	

	@Override
	public SygAttributions findAttributaire(SygDossiers dossier,SygPlisouvertures plis,SygLots lot) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAttributions.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("plis", "plis");
		criteria.addOrder(Order.asc("plis.raisonsociale"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(lot!=null)
		{
			criteria.setFetchMode("lot", FetchMode.SELECT);
			criteria.add(Restrictions.eq("lot", lot));
		}
		if(criteria.list().size()>0)
		   return (SygAttributions) criteria.list().get(0);
		else
			return null;
	}


}
