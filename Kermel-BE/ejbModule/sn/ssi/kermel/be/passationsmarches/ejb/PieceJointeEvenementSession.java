package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygPieceJointeEvenement;



@Remote
public interface PieceJointeEvenementSession {
	public SygPieceJointeEvenement save(SygPieceJointeEvenement piecejointeevenement);
	public void delete(Long Id);
	public List<SygPieceJointeEvenement> find(int indice, int pas,String libelle,String fichier,Date date,Date datecourrier,String reference);
	public int count(Date date);
	public void update(SygPieceJointeEvenement piecejointeevenement);
	public SygPieceJointeEvenement findById(Long code);
	public List<SygPieceJointeEvenement> find(String code);
	List<SygPieceJointeEvenement> find(int indice, int pas);
	int count();
	
}

