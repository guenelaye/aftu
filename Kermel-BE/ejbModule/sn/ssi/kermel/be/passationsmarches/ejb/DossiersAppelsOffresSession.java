package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossiersFournisseurs;
import sn.ssi.kermel.be.entity.SygRealisations;
import sn.ssi.kermel.be.entity.SygTachesEffectues;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;

@Remote
public interface DossiersAppelsOffresSession {
	public SygDossiers save(SygDossiers dossier);
	public void delete(Long id);
	public List<SygDossiers> find(int indice, int pas,SygAutoriteContractante autorite,SygAppelsOffres appel,SygRealisations realisation, int etat, int gestion);
	public int count(SygAutoriteContractante autorite,SygAppelsOffres appel,SygRealisations realisation, int etat, int gestion);
	public void update(SygDossiers dossier);
	public SygDossiers findById(Long code);
	public SygDossiers Dossier(Long code,SygAppelsOffres appel,int etatprequalif);
	
	public List<SygDossiers> find(int indice, int pas,SygAutoriteContractante autorite,SygAppelsOffres appel,SygRealisations realisation);
	public int count(SygAutoriteContractante autorite,SygAppelsOffres appel,SygRealisations realisation);
	
	
	public List<SygDossiers> RevueDAO(int indice, int pas,String autorite, int etat, int gestion);
	public int countRevueDAO(String autorite, int etat, int gestion);
	
	
	public List<SygDossiers> ValidationAttributionprovisoire(int indice, int pas,String autorite, int etat, int gestion);
	public int countValidationAttributionprovisoire(String autorite, int etat, int gestion);
	
	public List<SygDossiers> ExamenJurique(int indice, int pas,String autorite, int etat, int gestion);
	public int countExamenJurique(String autorite, int etat, int gestion);
	SygDossiers findDos(SygAppelsOffres appel);
	List<SygDossiers> findAll(int offset,int noOfRecords,String objet, String organisme);
	List<SygDossiers> findCountAll(String objet, String organisme);
	List<SygDossiers> findAllType(int offset, int noOfRecords, Long type,
			String objet, String organisme);
	List<SygDossiers> findCountAllType(Long type, String objet, String organisme);
	int countCategorie(Long categorie);
	List<SygDossiers> findCategorie(int indice, int pas, Long categorie);
	int countTypeAutorite(SygTypeAutoriteContractante typeAutorite);
	int countAutorite(SygAutoriteContractante autorite);
	List<SygDossiers> findAppelOffreAutorite(int indice, int pas, Long autorite);
	int countAppelOffreAutorite(Long autorite);
	
	public List<SygDossiers> DossiersEnAttenteOP(int indice, int pas,SygAutoriteContractante autorite,String reference,String objet,String modepassation,String typemarche,Date date);
	public int countDossiersEnAttenteOP(SygAutoriteContractante autorite,String reference,String objet,String modepassation,String typemarche,Date date);
	
	//21/01/2016
    ////:::::::Taches Effectues//////////
	public SygTachesEffectues saveTache(SygTachesEffectues tache);
	public void deleteTache(Long id);
	public void updateTache(SygTachesEffectues tache);
	public SygTachesEffectues Tache(Long code);
	public SygTachesEffectues Taches(Long dossier);
	
	///26/01/2016
	public List<SygDossiersFournisseurs> DossiersFournisseurs(Long Fournisseur,Date date);
	List<SygDossiers> findAllPublie(int offset, int noOfRecords, String objet,
			String organisme);
	List<SygDossiersFournisseurs> findDossiersFournisseursNonRepondus(
			Long fournisseur, Date date);
	int countDossiersEnCoursOP(SygAutoriteContractante autorite,
			String reference, String objet, String modepassation,
			String typemarche, Date date);

	
}
