package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class RegistrededepotSessionBean extends AbstractSessionBean implements RegistrededepotSession{

	
	@Override
	public int count(SygDossiers dossier,SygRetraitregistredao retrait,SygAutoriteContractante autorite,SygFournisseur fournisseur,int EtatExamenPreliminaire,int valide,int garantie, int critere, int piece, int attributaire, String nom, int preselectionne, String garanti, String ouvert) {
		Criteria criteria = getHibernateSession().createCriteria(SygPlisouvertures.class);
		criteria.createAlias("dossier", "dossier");
		//criteria.createAlias("retrait", "retrait");
		criteria.setFetchMode("retrait", FetchMode.SELECT);
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("fournisseur", FetchMode.SELECT);
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(retrait!=null){
			criteria.add(Restrictions.eq("retrait", retrait));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(fournisseur!=null){
			criteria.add(Restrictions.eq("fournisseur", fournisseur));
		}
		if(EtatExamenPreliminaire>-1){
			criteria.add(Restrictions.eq("etatExamenPreliminaire", EtatExamenPreliminaire));
		}
		if(valide>-1){
			criteria.add(Restrictions.eq("valide", valide));
		}
		if(garantie>-1){
			criteria.add(Restrictions.eq("garantie", garantie));
		}
		if(critere>-1){
			criteria.add(Restrictions.eq("critereQualification", critere));
		}
		if(piece>-1){
			criteria.add(Restrictions.eq("piecerequise", piece));
		}
		if(attributaire>-1){
			criteria.add(Restrictions.eq("attributaireProvisoire", attributaire));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("retrait.nomSoumissionnaire","%" +nom+"%"));
		}
		if(preselectionne>-1){
			criteria.add(Restrictions.eq("etatPreselection", preselectionne));
		}
		if(garanti!=null){
			criteria.add(Restrictions.eq("garantiesoumission",garanti));
		}
		if(ouvert!=null){
			criteria.add(Restrictions.eq("ouvert",ouvert));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygPlisouvertures.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygPlisouvertures> find(int indice, int pas,SygDossiers dossier,SygRetraitregistredao retrait,SygAutoriteContractante autorite,SygFournisseur fournisseur,int EtatExamenPreliminaire,int valide,int garantie, int critere, int piece,int attributaire, String nom, int preselectionne, String garanti, String ouvert) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlisouvertures.class);
		criteria.createAlias("dossier", "dossier");
		//criteria.createAlias("retrait", "retrait");
		criteria.setFetchMode("retrait", FetchMode.SELECT);
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("fournisseur", FetchMode.SELECT);
		criteria.addOrder(Order.asc("dateDepot"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(retrait!=null){
			criteria.add(Restrictions.eq("retrait", retrait));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(fournisseur!=null){
			criteria.add(Restrictions.eq("fournisseur", fournisseur));
		}
		if(EtatExamenPreliminaire>-1){
			criteria.add(Restrictions.eq("etatExamenPreliminaire", EtatExamenPreliminaire));
		}
		if(valide>-1){
			criteria.add(Restrictions.eq("valide", valide));
		}
		if(garantie>-1){
			criteria.add(Restrictions.eq("garantie", garantie));
		}
		if(critere>-1){
			criteria.add(Restrictions.eq("critereQualification", critere));
		}
		if(piece>-1){
			criteria.add(Restrictions.eq("piecerequise", piece));
		}
		if(attributaire>-1){
			criteria.add(Restrictions.eq("attributaireProvisoire", attributaire));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("retrait.nomSoumissionnaire","%" +nom+"%"));
		}
		if(preselectionne>-1){
			criteria.add(Restrictions.eq("etatPreselection", preselectionne));
		}
		if(garanti!=null){
			criteria.add(Restrictions.eq("garantiesoumission",garanti));
		}
		if(ouvert!=null){
			criteria.add(Restrictions.eq("ouvert",ouvert));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public List<SygPlisouvertures> find(int indice, int pas,SygDossiers dossier,SygRetraitregistredao retrait,SygAutoriteContractante autorite,SygFournisseur fournisseur,int EtatExamenPreliminaire,int valide,int garantie, int critere, int piece,int attributaire, String nom, int preselectionne, String signatureoffre, String exhaustivite, String garantiesoumission, String conformite) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlisouvertures.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("retrait", "retrait");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("fournisseur", FetchMode.SELECT);
		criteria.addOrder(Order.asc("dateDepot"));		
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(retrait!=null){
			criteria.add(Restrictions.eq("retrait", retrait));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(fournisseur!=null){
			criteria.add(Restrictions.eq("fournisseur", fournisseur));
		}
		if(EtatExamenPreliminaire>-1){
			criteria.add(Restrictions.eq("etatExamenPreliminaire", EtatExamenPreliminaire));
		}
		if(valide>-1){
			criteria.add(Restrictions.eq("valide", valide));
		}
		if(garantie>-1){
			criteria.add(Restrictions.eq("garantie", garantie));
		}
		if(critere>-1){
			criteria.add(Restrictions.eq("critereQualification", critere));
		}
		if(piece>-1){
			criteria.add(Restrictions.eq("piecerequise", piece));
		}
		if(attributaire>-1){
			criteria.add(Restrictions.eq("attributaireProvisoire", attributaire));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("retrait.nomSoumissionnaire","%" +nom+"%"));
		}
		if(preselectionne>-1){
			criteria.add(Restrictions.eq("etatPreselection", preselectionne));
		}
		if(signatureoffre!=null){
			criteria.add(Restrictions.eq("signatureoffre", signatureoffre));
		}
		if(exhaustivite!=null){
			criteria.add(Restrictions.eq("exhaustivite", exhaustivite));
		}
		if(garantiesoumission!=null){
			criteria.add(Restrictions.eq("garantiesoumission", garantiesoumission));
		}
		if(conformite!=null){
			criteria.add(Restrictions.eq("conformite", conformite));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	


	@Override
	public void save(SygPlisouvertures retrait) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(retrait);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	
	@Override
	public SygPlisouvertures savePli(SygPlisouvertures retrait) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(retrait);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	
		
	   return  retrait;
	}

	
	@Override
	public SygPlisouvertures update(SygPlisouvertures retrait) {
		
		try{
			getHibernateSession().merge(retrait);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return retrait;
	}

	
	@Override
	public SygPlisouvertures findById(Long code) {
		return (SygPlisouvertures)getHibernateSession().get(SygPlisouvertures.class, code);
	}
	

	@Override
	public List<SygPlisouvertures> find(SygDossiers dossier,SygRetraitregistredao retrait,int numero, String nom) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlisouvertures.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("retrait", "retrait");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("fournisseur", FetchMode.SELECT);
		criteria.addOrder(Order.asc("numero"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(retrait!=null){
			criteria.add(Restrictions.eq("retrait", retrait));
		}
		
		if(numero>-1){
			criteria.add(Restrictions.eq("numero", numero));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("retrait.nomSoumissionnaire", "%"+nom+"%"));
		}
		
		return criteria.list();
	}
	
	
	
	
	@Override
	public SygPlisouvertures findLignePli(SygDossiers dossier,SygFournisseur founisseur) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlisouvertures.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("fournisseur", "fournisseur");
		
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(founisseur!=null){
			criteria.add(Restrictions.eq("fournisseur", founisseur));
		}
		
		
		if(criteria.list().size()>0)
		return (SygPlisouvertures) criteria.list().get(0);
		else
			return null;
	}
	
	
	
	@Override
	public SygPlisouvertures findSoumissionnaire(SygDossiers dossier,int etatexamen,int critere){
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlisouvertures.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("retrait", "retrait");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("fournisseur", FetchMode.SELECT);
		criteria.addOrder(Order.asc("montantdefinitif"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(etatexamen>-1){
			criteria.add(Restrictions.eq("etatExamenPreliminaire", etatexamen));
		}
		if(critere>-1){
			criteria.add(Restrictions.or(Restrictions.eq("critereQualification", critere), Restrictions.eq("critereQualification", BeConstants.NPARENT)));
		}
		
		if(criteria.list().size()>0)
		  return (SygPlisouvertures) criteria.list().get(0);
		else 
			return null;
	}
	
	
	@Override
	public List<SygPlisouvertures> find(int indice, int pas, SygAutoriteContractante autorite,SygDossiers dossier,int etat) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlisouvertures.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("retrait", "retrait");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("fournisseur", FetchMode.SELECT);
		criteria.addOrder(Order.desc("notepreselectionne"));
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
	
		if(etat!=0){
			criteria.add(Restrictions.eq("etatPreselection", etat));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public int count(SygAutoriteContractante autorite,SygDossiers dossier,int etat) {
		Criteria criteria = getHibernateSession().createCriteria(SygPlisouvertures.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("retrait", "retrait");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("fournisseur", FetchMode.SELECT);
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
	
		if(etat!=0){
			criteria.add(Restrictions.eq("etatPreselection", etat));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	
	@Override
	public List<SygPlisouvertures> ListeClassementFinancieres(int indice, int pas,SygDossiers dossier,int selectionne) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlisouvertures.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("retrait", "retrait");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("fournisseur", FetchMode.SELECT);
		criteria.addOrder(Order.desc("scorefinancier"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		
		if(selectionne>-1){
			criteria.add(Restrictions.eq("etatPreselection", selectionne));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public int countClassementFinancieres(SygDossiers dossier,int selectionne) {
		Criteria criteria = getHibernateSession().createCriteria(SygPlisouvertures.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("retrait", "retrait");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("fournisseur", FetchMode.SELECT);
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		
		if(selectionne>-1){
			criteria.add(Restrictions.eq("etatPreselection", selectionne));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
	@Override
	public List<SygPlisouvertures> ClassementFinanciere(SygDossiers dossier,int selectionne) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlisouvertures.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("retrait", "retrait");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("fournisseur", FetchMode.SELECT);
		criteria.addOrder(Order.asc("montantdefinitif"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		
		if(selectionne>-1){
			criteria.add(Restrictions.eq("etatPreselection", selectionne));
		}
		
		
		return criteria.list();
	}
	
	@Override
	public List<SygPlisouvertures> ListeClassementFinal(int indice, int pas,SygDossiers dossier,int selectionne,Long soumissionnaire) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlisouvertures.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("retrait", "retrait");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("fournisseur", FetchMode.SELECT);
		criteria.addOrder(Order.desc("scorefinal"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		
		if(selectionne>-1){
			criteria.add(Restrictions.eq("etatPreselection", selectionne));
		}
		if(soumissionnaire!= null){
			criteria.add(Restrictions.ne("id", soumissionnaire));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public int countClassementFinal(SygDossiers dossier,int selectionne,Long soumissionnaire) {
		Criteria criteria = getHibernateSession().createCriteria(SygPlisouvertures.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("retrait", "retrait");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("fournisseur", FetchMode.SELECT);
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		
		if(selectionne>-1){
			criteria.add(Restrictions.eq("etatPreselection", selectionne));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
	
	
	@Override
	public List<SygPlisouvertures> find2(int indice, int pas,SygDossiers dossier,SygRetraitregistredao retrait,SygAutoriteContractante autorite,SygFournisseur fournisseur,int EtatExamenPreliminaire,int valide,int garantie, int critere, int piece,int attributaire, String nom, int preselectionne, String garanti, String ouvert) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPlisouvertures.class);
		criteria.createAlias("dossier", "dossier");
		//criteria.createAlias("retrait", "retrait");
		criteria.setFetchMode("retrait", FetchMode.SELECT);
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.setFetchMode("fournisseur", FetchMode.SELECT);
		//criteria.addOrder(Order.desc("dateDepot"));
		criteria.addOrder(Order.desc("numero"));
		
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(retrait!=null){
			criteria.add(Restrictions.eq("retrait", retrait));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(fournisseur!=null){
			criteria.add(Restrictions.eq("fournisseur", fournisseur));
		}
		if(EtatExamenPreliminaire>-1){
			criteria.add(Restrictions.eq("etatExamenPreliminaire", EtatExamenPreliminaire));
		}
		if(valide>-1){
			criteria.add(Restrictions.eq("valide", valide));
		}
		if(garantie>-1){
			criteria.add(Restrictions.eq("garantie", garantie));
		}
		if(critere>-1){
			criteria.add(Restrictions.eq("critereQualification", critere));
		}
		if(piece>-1){
			criteria.add(Restrictions.eq("piecerequise", piece));
		}
		if(attributaire>-1){
			criteria.add(Restrictions.eq("attributaireProvisoire", attributaire));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("retrait.nomSoumissionnaire","%" +nom+"%"));
		}
		if(preselectionne>-1){
			criteria.add(Restrictions.eq("etatPreselection", preselectionne));
		}
		if(garanti!=null){
			criteria.add(Restrictions.eq("garantiesoumission",garanti));
		}
		if(ouvert!=null){
			criteria.add(Restrictions.eq("ouvert",ouvert));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

}
