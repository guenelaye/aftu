package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygCritere;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierssouscriteres;
import sn.ssi.kermel.be.entity.SygDossierssouscriteresevaluateurs;
import sn.ssi.kermel.be.entity.SygEvaluateur;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class DossiersSouscriteresSessionBean extends AbstractSessionBean implements DossiersSouscriteresSession{


	
	@Override
	public int count(SygDossiers dossier, Long critere, SygLots lot) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossierssouscriteres.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("critere", "critere");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(critere!=null){
			criteria.add(Restrictions.eq("critere.id", critere));
		}
		if(lot!=null){
			criteria.createAlias("lot", "lot");
			criteria.add(Restrictions.eq("lot", lot));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygDossierssouscriteres.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygDossierssouscriteres> find(int indice, int pas,SygDossiers dossier, Long critere, SygLots lot) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossierssouscriteres.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("critere", "critere");
		criteria.addOrder(Order.asc("critere.libelle"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(critere!=null){
			criteria.add(Restrictions.eq("critere.id", critere));
		}
		if(lot!=null){
			criteria.createAlias("lot", "lot");
			criteria.add(Restrictions.eq("lot", lot));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygDossierssouscriteres critere) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(critere);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygDossierssouscriteres critere) {
		
		try{
			getHibernateSession().merge(critere);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygDossierssouscriteres findById(Long code) {
		return (SygDossierssouscriteres)getHibernateSession().get(SygDossierssouscriteres.class, code);
	}
	
////////////////////////NOTE//////:::::
	@Override
	public int countListesNotes(SygDossiers dossier, SygCritere critere,SygPlisouvertures plis,SygEvaluateur evaluateur) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossierssouscriteresevaluateurs.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("critere", "critere");
		criteria.createAlias("plis", "plis");
		criteria.setFetchMode("evaluateur", FetchMode.SELECT);
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(critere!=null){
			criteria.add(Restrictions.eq("critere", critere));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(evaluateur!=null){
			criteria.add(Restrictions.eq("evaluateur", evaluateur));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	

	@Override
	public List<SygDossierssouscriteresevaluateurs> ListesNotes(int indice, int pas,SygDossiers dossier, SygCritere critere,SygPlisouvertures plis,SygEvaluateur evaluateur) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossierssouscriteresevaluateurs.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("critere", "critere");
		criteria.createAlias("plis", "plis");
		criteria.setFetchMode("evaluateur", FetchMode.SELECT);
		criteria.addOrder(Order.asc("critere.libelle"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(critere!=null){
			criteria.add(Restrictions.eq("critere", critere));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(evaluateur!=null){
			criteria.add(Restrictions.eq("evaluateur", evaluateur));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public void deleteNote(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygDossierssouscriteresevaluateurs.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Override
	public void saveNote(SygDossierssouscriteresevaluateurs note) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(note);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void updateNote(SygDossierssouscriteresevaluateurs note) {
		
		try{
			getHibernateSession().merge(note);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygDossierssouscriteresevaluateurs findNote(Long code) {
		return (SygDossierssouscriteresevaluateurs)getHibernateSession().get(SygDossierssouscriteresevaluateurs.class, code);
	}
	
}
