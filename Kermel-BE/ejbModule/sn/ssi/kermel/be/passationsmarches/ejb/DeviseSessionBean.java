package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygDevise;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.referentiel.ejb.ParametresGenerauxSession;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class DeviseSessionBean extends AbstractSessionBean implements DeviseSession{

	@EJB
	ParametresGenerauxSession ParametresGenerauxSession;
	
	@Override
	public int count(SygDossiers dossier) {
		Criteria criteria = getHibernateSession().createCriteria(SygDevise.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("monnaie", "monnaie");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygDevise.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygDevise> find(int indice, int pas,SygDossiers dossier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDevise.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("monnaie", "monnaie");
		criteria.addOrder(Order.asc("devId"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygDevise devise) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(devise);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygDevise devise) {
		
		try{
			getHibernateSession().merge(devise);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygDevise findById(Long code) {
		return (SygDevise)getHibernateSession().get(SygDevise.class, code);
	}
	

	@Override
	public SygDevise getMonnaieCFA(SygDossiers dossier,String monnaie){
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDevise.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("monnaie", "monnaie");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(monnaie!=null){
			criteria.add(Restrictions.eq("monnaie.monCode", monnaie));
		}
		if(criteria.list().size()>0)
		  return (SygDevise) criteria.list().get(0);
		else 
			return null;
	}


}
