package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;


import sn.ssi.kermel.be.entity.SygCourrierEvenement;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygEvenement;
import sn.ssi.kermel.be.entity.SygPieceJointeEvenement;
import sn.ssi.kermel.be.entity.SygTypeEvenement;






@Remote
public interface EvenementSession {
	public SygEvenement save(SygEvenement evenement);
	public void delete(Long Id);
	public List<SygEvenement> find(int indice, int pas,Date Date,String intitule,String lieu,String participant,String description,SygTypeEvenement typeevenement);
	public int count(Date Date);
	public void update(SygEvenement evenement);
	public SygEvenement findById(Long code);
	public List<SygEvenement> find(String code);
	List<SygEvenement> find(int indice, int pas);
	int count();
	
}

