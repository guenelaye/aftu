package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygIssuerecours;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class IssueRecoursSessionBean extends AbstractSessionBean implements IssueRecoursSession{

	@Override
	public int count(String issLibelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygIssuerecours.class);
		if(issLibelle!=null){
			criteria.add(Restrictions.ge("issLibelle", issLibelle));
		}	
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygIssuerecours.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long issID) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygIssuerecours.class, issID));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	

	@Override
	public SygIssuerecours save(SygIssuerecours issuerecours)
	{
	// TODO Auto-generated method stub
			try {
				SygIssuerecours CourrierAudits1=(SygIssuerecours) getHibernateSession().save(issuerecours);
				getHibernateSession().flush();
				return issuerecours;
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				return null;
			}	
			
	}

	@Override
	public void update(SygIssuerecours issuerecours) {
		
		try{
			getHibernateSession().merge(issuerecours);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygIssuerecours findById(Long code) {
		return (SygIssuerecours)getHibernateSession().get(SygIssuerecours.class, code);
	}
	
	
	@Override
	public List<SygIssuerecours> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygIssuerecours.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));
		
		return criteria.list();
	}
	

	@Override
	public List<SygIssuerecours> find(int indice, int pas,String issLibelle) {
       Criteria criteria = getHibernateSession().createCriteria(SygIssuerecours.class);
       //criteria.createAlias("state", "state");
		
		if(issLibelle!=null){
		criteria.add(Restrictions.ge("issLibelle",issLibelle));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}




	@Override
	public List<SygIssuerecours> find(int indice, int pas) {
	   Criteria criteria = getHibernateSession().createCriteria(SygIssuerecours.class);
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}


}
