package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygLotsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygPlisouvertures;

@Remote
public interface LotsSoumissionnairesSession {
	public void save(SygLotsSoumissionnaires lot);
	public void delete(Long id);
	public List<SygLotsSoumissionnaires> find(int indice, int pas,SygDossiers dossier,SygPlisouvertures plis,SygLots lot,int etatexamenpreleminaire,String soumis,int plilEtatPreselection,int attributaire, int critere, String libelle);
	public int count(SygDossiers dossier,SygPlisouvertures plis,SygLots lot,int etatexamenpreleminaire,String soumis,int plilEtatPreselection,int attributaire, int critere, String libelle);
	public void update(SygLotsSoumissionnaires lot);
	public SygLotsSoumissionnaires findById(Long code);
	
	public SygLotsSoumissionnaires getLot(Long code,SygDossiers dossier,SygPlisouvertures plis,SygLots lot);
	public SygLotsSoumissionnaires getAttributaire(SygDossiers dossier,SygPlisouvertures plis,SygLots lot,int etatexamenpreleminaire,String soumis,int plilEtatPreselection,int critere);
	
	public List<SygLotsSoumissionnaires> getAttributaires(SygDossiers dossier,SygPlisouvertures plis,SygLots lot,int etatexamenpreleminaire,String soumis,int plilEtatPreselection,int critere);
	SygLotsSoumissionnaires findByIDS(SygDossiers dossier,
			SygPlisouvertures plis, SygLots lot);

}
