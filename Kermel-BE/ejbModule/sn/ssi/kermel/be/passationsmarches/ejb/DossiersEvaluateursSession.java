package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossiersEvaluateurs;

@Remote
public interface DossiersEvaluateursSession {
	public void save(SygDossiersEvaluateurs evaluateur);
	public void delete(Long id);
	public List<SygDossiersEvaluateurs> find(int indice, int pas,SygDossiers dossier);
	public int count(SygDossiers dossier);
	public void update(SygDossiersEvaluateurs evaluateur);
	public SygDossiersEvaluateurs findById(Long code);
	
	
}
