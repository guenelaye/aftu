package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygHistoriquedossiers;

@Remote
public interface HistoriquedossiersSession {
	public void save(SygHistoriquedossiers historique);
	public void delete(Long id);
	public List<SygHistoriquedossiers> find(int indice, int pas,SygDossiers dossier,SygAppelsOffres appel);
	public int count(SygDossiers dossier,SygAppelsOffres appel);
	public void update(SygHistoriquedossiers historique);
	public SygHistoriquedossiers findById(Long code);
	
	
}
