package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.PklEnveloppesFichiers;
import sn.ssi.kermel.be.entity.PklPlis;
import sn.ssi.kermel.be.entity.PklPlisEnveloppes;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygFournisseur;

@Remote
public interface SoumissionElectroniqueSession {
	
	/////////////Plis///////////
	public PklPlis savePlis(PklPlis plis);
	public void deletePlis(Long id);
	public List<PklPlis> Plis(int indice, int pas,SygDossiers dossier,SygFournisseur fournisseur);
	public int countPlis(SygDossiers dossier,SygFournisseur fournisseur);
	public void updatePlis(PklPlis plis);
	public PklPlis findPlis(Long code);
	PklPlis findPlisExiste(SygDossiers dossier, SygFournisseur fournisseur);
	
    /////////////Enveloppes///////////
	public PklPlisEnveloppes saveEnveloppe(PklPlisEnveloppes enveloppe);
	public void updateEnveloppe(PklPlisEnveloppes enveloppe);
	public void deleteEnveloppe(Long id);
	public PklPlisEnveloppes findEnveloppe(Long code);
	public List<PklPlisEnveloppes> Enveloppes(int indice, int pas,PklPlis plis);
	public int countEnveloppes(PklPlis plis);
	
	 /////////////Fichiers///////////
	public void saveFichier(PklEnveloppesFichiers fichier);
	public void updateFichier(PklEnveloppesFichiers fichier);
	public void deleteFichier(Long id);
	public PklEnveloppesFichiers findFichier(Long code);
	public List<PklEnveloppesFichiers> Fichiers(int indice, int pas,PklPlisEnveloppes enveloppe);
	public int countFichier(PklPlisEnveloppes enveloppe);
	PklEnveloppesFichiers getOffreFinanciere(Long idFournisseur, Long idDossier);
	
}
