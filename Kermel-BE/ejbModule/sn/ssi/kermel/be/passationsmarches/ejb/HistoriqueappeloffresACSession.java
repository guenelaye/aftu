package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygHistoriqueappeloffresAC;

@Remote
public interface HistoriqueappeloffresACSession {
	public void save(SygHistoriqueappeloffresAC historique);
	public void delete(Long id);
	public List<SygHistoriqueappeloffresAC> find(int indice, int pas,SygDossiers dossier,SygAutoriteContractante autorite,int attribution);
	public int count(SygDossiers dossier,SygAutoriteContractante autorite,int attribution);
	public void update(SygHistoriqueappeloffresAC historique);
	public SygHistoriqueappeloffresAC findById(Long code);
	
	public SygHistoriqueappeloffresAC getHistorique(SygDossiers dossier,int attribution);
}
