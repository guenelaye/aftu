package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygAvisAttribution;
import sn.ssi.kermel.be.entity.SygDossiers;

@Remote
public interface AvisAttributionSession {
	public void save(SygAvisAttribution avis);
	public void delete(Long id);
	public List<SygAvisAttribution> find(int indice, int pas,SygDossiers dossier,SygAutoriteContractante autorite);
	public int count(SygDossiers dossier,SygAutoriteContractante autorite);
	public void update(SygAvisAttribution avis);
	public SygAvisAttribution findById(Long code);
	
	public SygAvisAttribution getAvis(SygDossiers dossier,SygAutoriteContractante autorite,String type);
	
}
