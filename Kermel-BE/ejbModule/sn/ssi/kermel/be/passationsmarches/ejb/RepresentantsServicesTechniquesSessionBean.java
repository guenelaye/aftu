package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygRepresentantsServicesTechniques;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class RepresentantsServicesTechniquesSessionBean extends AbstractSessionBean implements RepresentantsServicesTechniquesSession{

	
	
	@Override
	public int count(SygDossiers dossier,int etape) {
		Criteria criteria = getHibernateSession().createCriteria(SygRepresentantsServicesTechniques.class);
		criteria.createAlias("dossier", "dossier");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(etape>-1)
			criteria.add(Restrictions.eq("etape", etape));
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygRepresentantsServicesTechniques.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygRepresentantsServicesTechniques> find(int indice, int pas,SygDossiers dossier,int etape) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygRepresentantsServicesTechniques.class);
		criteria.createAlias("dossier", "dossier");
		criteria.addOrder(Order.asc("representant"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(etape>-1)
			criteria.add(Restrictions.eq("etape", etape));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygRepresentantsServicesTechniques presence) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(presence);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygRepresentantsServicesTechniques presence) {
		
		try{
			getHibernateSession().merge(presence);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygRepresentantsServicesTechniques findById(Long code) {
		return (SygRepresentantsServicesTechniques)getHibernateSession().get(SygRepresentantsServicesTechniques.class, code);
	}
	


}
