package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;




import sn.ssi.kermel.be.entity.SygPays;
import sn.ssi.kermel.be.entity.SygTypeEvenement;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class TypeEvenementSessionBean extends AbstractSessionBean implements TypeEvenementSession{

	@Override
	public int count(String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygTypeEvenement.class);
		 //criteria.createAlias("state", "state");
		if(libelle!=null){
			criteria.add(Restrictions.ge("libelle", libelle));
		}	
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygTypeEvenement.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long Id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygTypeEvenement.class, Id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	

	@Override
	public SygTypeEvenement save(SygTypeEvenement typeevenement)
	{
	// TODO Auto-generated method stub
			try {
				SygTypeEvenement CourrierAudits1=(SygTypeEvenement) getHibernateSession().save(typeevenement);
				getHibernateSession().flush();
				return typeevenement;
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				return null;
			}	
			
	}

	@Override
	public void update(SygTypeEvenement typeevenement) {
		
		try{
			getHibernateSession().merge(typeevenement);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

//	
//	@Override
//	public SygTypeEvenement findById(Long code) {
//		return (SygTypeEvenement)getHibernateSession().get(SygTypeEvenement.class, code);
//	}
	
	
	@Override
	public List<SygTypeEvenement> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygTypeEvenement.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));
		
		return criteria.list();
	}
	

	@Override
	public List<SygTypeEvenement> find(int indice, int pas,String libelle) {
       Criteria criteria = getHibernateSession().createCriteria(SygTypeEvenement.class);
//       criteria.createAlias("state", "state");
//		criteria.addOrder(Order.desc("datestatut"));
		
		if(libelle!=null){
		criteria.add(Restrictions.ge("libelle",libelle));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}




@Override
public List<SygTypeEvenement> find(int indice, int pas) {
   Criteria criteria = getHibernateSession().createCriteria(SygTypeEvenement.class);
	criteria.setFirstResult(indice);
	if(pas>0)
	criteria.setMaxResults(pas);
		
	return criteria.list();
	}


@Override
public SygTypeEvenement findById(Long Id) {
	// TODO Auto-generated method stub
	return (SygTypeEvenement)getHibernateSession().get(SygTypeEvenement.class, Id );
}
}
