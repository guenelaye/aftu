package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygLots;

@Remote
public interface LotsSession {
	public void save(SygLots lot);
	public void delete(Long id);
	public List<SygLots> find(int indice, int pas,SygDossiers dossier,SygContrats contrat);
	public int count(SygDossiers dossier,SygContrats contrat);
	public void update(SygLots lot);
	public SygLots findById(Long code);
	List<SygLots> findLotsByIdDossier(SygDossiers dossier);
	
}
