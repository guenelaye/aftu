package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.common.utils.BeConstants;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygLotsSoumissionnaires;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.referentiel.ejb.ParametresGenerauxSession;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class LotsSoumissionnairesSessionBean extends AbstractSessionBean implements LotsSoumissionnairesSession{

	@EJB
	ParametresGenerauxSession ParametresGenerauxSession;
	
	@Override
	public int count(SygDossiers dossier,SygPlisouvertures plis,SygLots lot,int etatexamenpreleminaire,String soumis,int plilEtatPreselection,int attributaire, int critere, String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygLotsSoumissionnaires.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("lot", "lot");
		criteria.createAlias("plis", "plis");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(lot!=null){
			criteria.add(Restrictions.eq("lot", lot));
		}
		if(etatexamenpreleminaire>-1)
			criteria.add(Restrictions.eq("plilEtatExamenPreliminaire", etatexamenpreleminaire));
		if(soumis!=null){
			criteria.add(Restrictions.eq("lotsoumis", soumis));
		}
		if(plilEtatPreselection>-1)
			criteria.add(Restrictions.eq("plilEtatPreselection", plilEtatPreselection));
		if(attributaire>-1)
			criteria.add(Restrictions.eq("plilattributaireProvisoire", attributaire));
		if(critere>-1)
			criteria.add(Restrictions.eq("plilcritereQualification", critere));
		if(libelle!=null)
			  criteria.add(Restrictions.ilike("lot.libelle","%" +libelle+"%"));
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygLotsSoumissionnaires.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygLotsSoumissionnaires> find(int indice, int pas,SygDossiers dossier,SygPlisouvertures plis,SygLots lot,int etatexamenpreleminaire,String soumis,int plilEtatPreselection,int attributaire, int critere, String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygLotsSoumissionnaires.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("lot", "lot");
		criteria.createAlias("plis", "plis");
		criteria.addOrder(Order.asc("id"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(lot!=null){
			criteria.add(Restrictions.eq("lot", lot));
		}
		if(etatexamenpreleminaire>-1)
			criteria.add(Restrictions.eq("plilEtatExamenPreliminaire", etatexamenpreleminaire));
		if(soumis!=null){
			criteria.add(Restrictions.eq("lotsoumis", soumis));
		}
		if(plilEtatPreselection>-1)
			criteria.add(Restrictions.eq("plilEtatPreselection", plilEtatPreselection));
		if(attributaire>-1)
			criteria.add(Restrictions.eq("plilattributaireProvisoire", attributaire));
		if(critere>-1)
			criteria.add(Restrictions.eq("plilcritereQualification", critere));
		if(libelle!=null)
		  criteria.add(Restrictions.ilike("lot.libelle","%" +libelle+"%"));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public SygLotsSoumissionnaires findByIDS(SygDossiers dossier,SygPlisouvertures plis,SygLots lot) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygLotsSoumissionnaires.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("lot", "lot");
		criteria.createAlias("plis", "plis");
		criteria.addOrder(Order.asc("id"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(lot!=null){
			criteria.add(Restrictions.eq("lot", lot));
		}
		
		
		 if ( criteria.list().size()>0 )  return (SygLotsSoumissionnaires) criteria.list().get(0) ;
		 else
			 return null;
	}

	

	@Override
	public void save(SygLotsSoumissionnaires lot) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(lot);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygLotsSoumissionnaires lot) {
		
		try{
			getHibernateSession().merge(lot);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygLotsSoumissionnaires findById(Long code) {
		return (SygLotsSoumissionnaires)getHibernateSession().get(SygLotsSoumissionnaires.class, code);
	}
	

	
	@Override
	public SygLotsSoumissionnaires getLot(Long code,SygDossiers dossier,SygPlisouvertures plis,SygLots lot){
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygLotsSoumissionnaires.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("lot", "lot");
		criteria.createAlias("plis", "plis");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(lot!=null){
			criteria.add(Restrictions.eq("lot", lot));
		}
		if(criteria.list().size()>0)
		  return (SygLotsSoumissionnaires) criteria.list().get(0);
		else 
			return null;
	}

	@Override
	public SygLotsSoumissionnaires getAttributaire(SygDossiers dossier,SygPlisouvertures plis,SygLots lot,int etatexamenpreleminaire,String soumis,int plilEtatPreselection,int critere){
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygLotsSoumissionnaires.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("lot", "lot");
		criteria.createAlias("plis", "plis");
		criteria.addOrder(Order.asc("plilsrixevalue"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(lot!=null){
			criteria.add(Restrictions.eq("lot", lot));
		}
		if(soumis!=null){
			criteria.add(Restrictions.eq("lotsoumis", soumis));
		}
		if(etatexamenpreleminaire>-1)
			criteria.add(Restrictions.eq("plilEtatExamenPreliminaire", etatexamenpreleminaire));
		if(plilEtatPreselection>-1)
			criteria.add(Restrictions.eq("plilEtatPreselection", plilEtatPreselection));
		if(critere>-1){
			criteria.add(Restrictions.or(Restrictions.eq("plis.critereQualification", critere), Restrictions.eq("plis.critereQualification", BeConstants.NPARENT)));
		}
		if(criteria.list().size()>0)
		  return (SygLotsSoumissionnaires) criteria.list().get(0);
		else 
			return null;
	}

	
	@Override
	public List<SygLotsSoumissionnaires> getAttributaires(SygDossiers dossier,SygPlisouvertures plis,SygLots lot,int etatexamenpreleminaire,String soumis,int plilEtatPreselection,int critere){
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygLotsSoumissionnaires.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("lot", "lot");
		criteria.createAlias("plis", "plis");
		criteria.addOrder(Order.asc("plilsrixevalue"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(lot!=null){
			criteria.add(Restrictions.eq("lot", lot));
		}
		if(soumis!=null){
			criteria.add(Restrictions.eq("lotsoumis", soumis));
		}
		if(etatexamenpreleminaire>-1)
			criteria.add(Restrictions.eq("plilEtatExamenPreliminaire", etatexamenpreleminaire));
		if(plilEtatPreselection>-1)
			criteria.add(Restrictions.eq("plilEtatPreselection", plilEtatPreselection));
		if(critere>-1){
			criteria.add(Restrictions.or(Restrictions.eq("plis.critereQualification", critere), Restrictions.eq("plis.critereQualification", BeConstants.NPARENT)));
		}
		
		  return criteria.list();
		
	}
}
