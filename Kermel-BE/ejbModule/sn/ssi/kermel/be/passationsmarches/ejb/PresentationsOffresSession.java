package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygContenusEnveloppesOffres;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygPresentationsOffres;
import sn.ssi.kermel.be.entity.UserEnveloppe;

@Remote
public interface PresentationsOffresSession {
	
	///////////Presentation Offres////////
	public void save(SygPresentationsOffres enveloppe);
	public void delete(Long id);
	public List<SygPresentationsOffres> find(int indice, int pas,SygDossiers dossier,String libelle, SygLots lot);
	public int count(SygDossiers dossier,String libelle, SygLots lot);
	public void update(SygPresentationsOffres enveloppe);
	public SygPresentationsOffres findById(Long code);
	List<SygPresentationsOffres> find(Long dossier);

	
	/////////::Contenues Enveloppes////////
	
	public void saveContenu(SygContenusEnveloppesOffres contenu);
	public void deleteContenu(Long id);
	public List<SygContenusEnveloppesOffres> Contenus(int indice, int pas,SygPresentationsOffres enveloppe);
	public int countContenus(SygPresentationsOffres enveloppe);
	public void updateContenu(SygContenusEnveloppesOffres contenu);
	public SygContenusEnveloppesOffres findContenu(Long code);
	List<SygContenusEnveloppesOffres> Contenus(Long enveloppe);

	
	////   traitement sur la cration des enveloppe
	List<SygPresentationsOffres> findEnveloppeNotCreat(Long user, Long dossier);
	List<UserEnveloppe> Contenus(Long user, Long dossier);
	void saveUserEnveloppe(UserEnveloppe userEnveloppe);
	void deleteUserEnveloppe(Long id);
	UserEnveloppe findByIdUserEnveloppe(Long code);
	List<SygContenusEnveloppesOffres> findByDossier(Long dossier);
	
	
	///  fin du traitement  ///
	
	
}
