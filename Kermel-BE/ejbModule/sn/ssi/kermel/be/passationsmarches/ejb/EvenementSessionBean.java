package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;




import sn.ssi.kermel.be.entity.SygClotureRecours;
import sn.ssi.kermel.be.entity.SygCourrierEvenement;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygEvenement;
import sn.ssi.kermel.be.entity.SygPieceJointeEvenement;
import sn.ssi.kermel.be.entity.SygTypeEvenement;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class EvenementSessionBean extends AbstractSessionBean implements EvenementSession{

	@Override
	public int count(Date Date) {
		Criteria criteria = getHibernateSession().createCriteria(SygEvenement.class);
		// criteria.createAlias("state", "state");
		if(Date!=null){
			criteria.add(Restrictions.ge("Date", Date));
		}	
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygEvenement.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long Id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygEvenement.class, Id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	

	@Override
	public SygEvenement save(SygEvenement evenement)
	{
	// TODO Auto-generated method stub
			try {
				SygEvenement CourrierAudits1=(SygEvenement) getHibernateSession().save(evenement);
				getHibernateSession().flush();
				return evenement;
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				return null;
			}	
			
	}

	@Override
	public void update(SygEvenement evenement) {
		
		try{
			getHibernateSession().merge(evenement);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygEvenement findById(Long code) {
		return (SygEvenement)getHibernateSession().get(SygEvenement.class, code);
	}
	
	
	@Override
	public List<SygEvenement> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygEvenement.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));
		
		return criteria.list();
	}
	

	@Override
	public List<SygEvenement> find(int indice, int pas,Date Date,String intitule,String lieu,String participant,String description,SygTypeEvenement typeevenement) {
       Criteria criteria = getHibernateSession().createCriteria(SygEvenement.class);
       //criteria.createAlias("state", "state");
		//criteria.addOrder(Order.desc("datestatut"));
		
		if(Date!=null){
		criteria.add(Restrictions.ge("Date",Date));
		}
		if(intitule!=null){
			criteria.add(Restrictions.eq("intitule",intitule));
			}
		if(lieu!=null){
			criteria.add(Restrictions.eq("lieu",lieu));
			}
		if(participant!=null){
			criteria.add(Restrictions.eq("participant",participant));
			}
		if(description!=null){
			criteria.add(Restrictions.eq("description",description));
			}
		criteria.createAlias("typeevenement", "typeevenement");
		if(typeevenement!=null){
			criteria.add(Restrictions.eq("typeevenement",typeevenement));
			}
//		criteria.createAlias("piecejointeevenement", "piecejointeevenement");
//		if(piecejointeevenement!=null){
//			criteria.add(Restrictions.eq("piecejointeevenement",piecejointeevenement));
//			}
//		criteria.createAlias("courrierevenement", "courrierevenement");
//		if(courrierevenement!=null){
//			criteria.add(Restrictions.eq("courrierevenement",courrierevenement));
//			}
//		criteria.createAlias("dossiers", "dossiers");
//		if(dossiers!=null){
//			criteria.add(Restrictions.eq("dossiers",dossiers));
//			}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}

  


@Override
public List<SygEvenement> find(int indice, int pas) {
   Criteria criteria = getHibernateSession().createCriteria(SygEvenement.class);
	criteria.setFirstResult(indice);
	if(pas>0)
	criteria.setMaxResults(pas);
		
	return criteria.list();
	}


}
