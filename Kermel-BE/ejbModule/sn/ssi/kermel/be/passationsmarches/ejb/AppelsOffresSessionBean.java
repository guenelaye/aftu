package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.common.utils.ToolKermel;
import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygCategori;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygSecteursactivites;
import sn.ssi.kermel.be.entity.SygTypeAutoriteContractante;
import sn.ssi.kermel.be.referentiel.ejb.ParametresGenerauxSession;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class AppelsOffresSessionBean extends AbstractSessionBean implements AppelsOffresSession{

	@EJB
	ParametresGenerauxSession ParametresGenerauxSession;
	
	@Override
	public int count(Long type,Long mode,int annee,SygAutoriteContractante autorite, String reference, Date datedebut, Date datefin) {
		Criteria criteria = getHibernateSession().createCriteria(SygAppelsOffres.class);
		//criteria.setFetchMode("clients", FetchMode.SELECT);
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("typemarche", "type");
		criteria.createAlias("categorie", "categorie");
		criteria.createAlias("modeselection", "selection");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("realisation.plan", "plan");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		if(type!=null){
			criteria.add(Restrictions.eq("type.id", type));
		}
		if(mode!=null){
			criteria.add(Restrictions.eq("mode.id", mode));
		}
		if(annee>0){
			criteria.add(Restrictions.eq("plan.annee", annee));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(reference!=null){
			criteria.add(Restrictions.ilike("apoobjet","%" +reference+"%"));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("realisation.datelancement", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("realisation.datelancement", datefin));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
		
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygAppelsOffres.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygAppelsOffres> find(int indice, int pas,Long type,Long mode,int annee,SygAutoriteContractante autorite, String reference, Date datedebut, Date datefin) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAppelsOffres.class);
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("typemarche", "type");
		criteria.createAlias("categorie", "categorie");
		criteria.createAlias("modeselection", "selection");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("realisation.plan", "plan");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.addOrder(Order.desc("apodatecreation"));
		if(type!=null){
			criteria.add(Restrictions.eq("type.id", type));
		}
		if(mode!=null){
			criteria.add(Restrictions.eq("mode.id", mode));
		}
		if(annee>0){
			criteria.add(Restrictions.eq("plan.annee", annee));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(reference!=null){
			criteria.add(Restrictions.ilike("apoobjet","%" +reference+"%"));
		}
		if(datedebut!=null){
			criteria.add(Restrictions.ge("realisation.datelancement", datedebut));
		}
		if(datefin!=null){
			criteria.add(Restrictions.le("realisation.datelancement", datefin));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygAppelsOffres> findAll(String objet,String organisme) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAppelsOffres.class);
		
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("typemarche", "type");
		criteria.createAlias("categorie", "categorie");
		criteria.createAlias("modeselection", "selection");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("realisation.plan", "plan");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.addOrder(Order.desc("apodatecreation"));  
		criteria.setMaxResults(5);
		return criteria.list();
	}
	

	@Override
	public void save(SygAppelsOffres plan) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(plan);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygAppelsOffres appel) {
		
		try{
			getHibernateSession().merge(appel);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygAppelsOffres findById(Long code) {
		return (SygAppelsOffres)getHibernateSession().get(SygAppelsOffres.class, code);
	}
	

	 //Add by Rama 02-11-2012
	@SuppressWarnings("unchecked")
	@Override
	public List<SygAppelsOffres> findRech(int indice, int pas,String Statut, SygAutoriteContractante autorite,String Objet) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAppelsOffres.class);
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("typemarche", "type");
		criteria.createAlias("categorie", "categorie");
		criteria.createAlias("modeselection", "selection");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("realisation.plan", "plan");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.addOrder(Order.asc("apodatecreation"));
		
		if(Statut!=null){
			criteria.add(Restrictions.ilike("apoStatut", Statut));
		}
		
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		
		if(Objet!=null){
			criteria.add(Restrictions.ilike("apoobjet","%" +Objet+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public int countRech(String Statut,SygAutoriteContractante autorite,String Objet) {
		Criteria criteria = getHibernateSession().createCriteria(SygAppelsOffres.class);
		//criteria.setFetchMode("clients", FetchMode.SELECT);
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("typemarche", "type");
		criteria.createAlias("categorie", "categorie");
		criteria.createAlias("modeselection", "selection");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("realisation.plan", "plan");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		
		
		if(Statut!=null){
			criteria.add(Restrictions.ilike("apoStatut", Statut));
		}
		
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		
		if(Objet!=null){
			criteria.add(Restrictions.ilike("apoobjet","%" +Objet+"%"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygAppelsOffres> findRech(int indice, int pas, SygAutoriteContractante autorite,String Objet) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygAppelsOffres.class);
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("typemarche", "type");
		criteria.createAlias("categorie", "categorie");
		criteria.createAlias("modeselection", "selection");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("realisation.plan", "plan");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		criteria.addOrder(Order.asc("apodatecreation"));
		
	
		
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		
		if(Objet!=null){
			criteria.add(Restrictions.ilike("apoobjet","%" +Objet+"%"));
		}
		criteria.add(Restrictions.in("apoid", findRech()));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public int countRech(SygAutoriteContractante autorite,String Objet) {
		Criteria criteria = getHibernateSession().createCriteria(SygAppelsOffres.class);
		//criteria.setFetchMode("clients", FetchMode.SELECT);
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("typemarche", "type");
		criteria.createAlias("categorie", "categorie");
		criteria.createAlias("modeselection", "selection");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("realisation.plan", "plan");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		
		if(Objet!=null){
			criteria.add(Restrictions.ilike("apoobjet","%" +Objet+"%"));
		}
		criteria.add(Restrictions.in("apoid", findRech()));
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Long> findRech() {
		Criteria criteria = getHibernateSession().createCriteria(SygDossiers.class);
		criteria.setProjection(Projections.property("appel.apoid"));
		criteria.add(Restrictions.isNotNull("dosDatePublicationDefinitive"));
		return criteria.list();
	}
	
	@Override
	public  List<SygCategori>  findAllSecteurActivite() {
		
		List<SygCategori> mylistacte = null;
		
		Query q = getHibernateSession().createQuery("SELECT distinct categorie FROM SygAppelsOffres aa where aa in ( SELECT appel from SygDossiers bb where  dosDateLimiteDepot < '"+ new Date()+"' or dosDateLimiteDepot='"+ new Date()+"' )");
        
		return q.list();
		
	}
	
	
	@Override
	public int countCategorie(Long categorie) {
		Criteria criteria = getHibernateSession().createCriteria(SygAppelsOffres.class);
		//criteria.setFetchMode("clients", FetchMode.SELECT);
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("typemarche", "type");
		criteria.createAlias("categorie", "categorie");
		criteria.createAlias("modeselection", "selection");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("realisation.plan", "plan");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		
		
		if(categorie!=null){
			criteria.add(Restrictions.eq("categorie.id", categorie));
		}
		
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public int countAppelOffreAutorite(Long autorite) {
		Criteria criteria = getHibernateSession().createCriteria(SygAppelsOffres.class);
		//criteria.setFetchMode("clients", FetchMode.SELECT);
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("typemarche", "type");
		criteria.createAlias("categorie", "categorie");
		criteria.createAlias("modeselection", "selection");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("realisation.plan", "plan");
		criteria.createAlias("autorite", "autorite");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		
		
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite.id", autorite));
		}
		
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	
	
	
	@Override
	public  List<SygAppelsOffres> findCategorie(int indice, int pas,Long categorie) {
		Criteria criteria = getHibernateSession().createCriteria(SygAppelsOffres.class);
		//criteria.setFetchMode("clients", FetchMode.SELECT);
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("typemarche", "type");
		criteria.createAlias("categorie", "categorie");
		criteria.createAlias("modeselection", "selection");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("realisation.plan", "plan");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		
		
		if(categorie!=null){
			criteria.add(Restrictions.eq("categorie.id", categorie));
		}
		
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	
	
	@Override
	public  List<SygAppelsOffres> findAppelOffreAutorite(int indice, int pas,Long autorite) {
		Criteria criteria = getHibernateSession().createCriteria(SygAppelsOffres.class);
		//criteria.setFetchMode("clients", FetchMode.SELECT);
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("typemarche", "type");
		criteria.createAlias("categorie", "categorie");
		criteria.createAlias("modeselection", "selection");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("realisation.plan", "plan");
		criteria.createAlias("autorite", "autorite");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		
		
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite.id", autorite));
		}
		
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	
	
	
	
	@Override
	public  List<SygTypeAutoriteContractante>  findAllTypeAutoriteContactante() {
		
		List<SygTypeAutoriteContractante> mylistacte = null;
		Query q = getHibernateSession().createQuery("SELECT distinct autorite.type FROM SygAppelsOffres aa where aa in ( SELECT appel from SygDossiers bb where bb.dosDateLimiteDepot < '"+new Date()+"' or bb.dosDateLimiteDepot = '"+ new Date() +"' ) ");

		
		return q.list();
		
	}
	
	
	@Override
	public  List<SygAutoriteContractante>  findAutoriteContactante(Long type) {
		
		List<SygAutoriteContractante> mylistacte = null;
		Query q = getHibernateSession().createQuery("SELECT distinct autorite  FROM SygAppelsOffres aa  WHERE   aa in ( SELECT appel from SygDossiers bb ) AND  aa.autorite.type.id = :param").setParameter("param",
				type);

		
		return q.list();
		
	}
	
	
	@Override
	public int countTypeAutorite(SygTypeAutoriteContractante typeAutorite) {
		Criteria criteria = getHibernateSession().createCriteria(SygAppelsOffres.class);
		//criteria.setFetchMode("clients", FetchMode.SELECT);
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("typemarche", "type");
		criteria.createAlias("categorie", "categorie");
		criteria.createAlias("modeselection", "selection");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("realisation.plan", "plan");
		criteria.createAlias("autorite", "autorite");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		
		
		if(typeAutorite!=null){
			criteria.add(Restrictions.eq("autorite.type", typeAutorite));
		}
		
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	
	
	@Override
	public int countAutorite(SygAutoriteContractante autorite) {
		Criteria criteria = getHibernateSession().createCriteria(SygAppelsOffres.class);
		//criteria.setFetchMode("clients", FetchMode.SELECT);
		criteria.createAlias("modepassation", "mode");
		criteria.createAlias("typemarche", "type");
		criteria.createAlias("categorie", "categorie");
		criteria.createAlias("modeselection", "selection");
		criteria.createAlias("realisation", "realisation");
		criteria.createAlias("realisation.plan", "plan");
		criteria.createAlias("autorite", "autorite");
		criteria.setFetchMode("autorite", FetchMode.SELECT);
		
		
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}
	
	
}
