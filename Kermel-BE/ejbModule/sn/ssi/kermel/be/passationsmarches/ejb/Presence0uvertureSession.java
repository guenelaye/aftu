package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygPresenceouverture;

@Remote
public interface Presence0uvertureSession {
	public void save(SygPresenceouverture presence);
	public void delete(Long id);
	public List<SygPresenceouverture> find(int indice, int pas,SygDossiers dossier,SygAppelsOffres appel,SygPlisouvertures plis,int etape);
	public int count(SygDossiers dossier,SygAppelsOffres appel,SygPlisouvertures plis,int etape);
	public void update(SygPresenceouverture presence);
	public SygPresenceouverture findById(Long code);
	
	
}
