package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.PklComptePerso;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygRetraitregistredao;

@Remote
public interface RegistrederetraitdaoSession {
	public SygRetraitregistredao save(SygRetraitregistredao retrait);
	public void delete(Long id);
	public List<SygRetraitregistredao> find(int indice, int pas,SygDossiers dossier);
	public int count(SygDossiers dossier);
	public void update(SygRetraitregistredao retrait);
	public SygRetraitregistredao findById(Long code);
	
	public List<SygRetraitregistredao> Candidats(int indice, int pas,SygDossiers dossier,String nom);
	public int countCandidats(SygDossiers dossier,String nom);
	public List<Long> Candidats(SygDossiers dossier);
	SygRetraitregistredao findExiste(SygDossiers dossier, PklComptePerso user);
}
