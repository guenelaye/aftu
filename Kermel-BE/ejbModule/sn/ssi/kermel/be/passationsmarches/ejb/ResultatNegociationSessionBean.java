package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygResultatNegociation;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class ResultatNegociationSessionBean extends AbstractSessionBean implements ResultatNegociationSession{

	
	@Override
	public int count(SygDossiers dossier,SygAutoriteContractante autorite) {
		Criteria criteria = getHibernateSession().createCriteria(SygResultatNegociation.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("autorite", "autorite");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
	
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygResultatNegociation.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygResultatNegociation> find(int indice, int pas,SygDossiers dossier,SygAutoriteContractante autorite) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygResultatNegociation.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.asc("id"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
	
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygResultatNegociation avis) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(avis);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygResultatNegociation avis) {
		
		try{
			getHibernateSession().merge(avis);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygResultatNegociation findById(Long code) {
		return (SygResultatNegociation)getHibernateSession().get(SygResultatNegociation.class, code);
	}
	

	@Override
	public SygResultatNegociation getResultat(SygDossiers dossier,SygAutoriteContractante autorite){
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygResultatNegociation.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("autorite", "autorite");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		
		if(criteria.list().size()>0)
		  return (SygResultatNegociation) criteria.list().get(0);
		else 
			return null;
	}


}
