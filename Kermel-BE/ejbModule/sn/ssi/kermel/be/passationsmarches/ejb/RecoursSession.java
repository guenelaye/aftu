package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygRecours;

@Remote
public interface RecoursSession {
	public void save(SygRecours recours);
	public void delete(Long id);
	public List<SygRecours> find(int indice, int pas,SygDossiers dossier,SygAutoriteContractante autorite,SygAppelsOffres appel,String Refrecours,String ObjetRecours,Date DateRecous,Date dateCourrier);
	public int count(SygDossiers dossier,SygAutoriteContractante autorite,SygAppelsOffres appel,Date dateCourrier);
	public void update(SygRecours recours);
	public SygRecours findById(Long code);
	
	
}
