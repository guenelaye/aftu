package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierspieces;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class DossiersPiecesSessionBean extends AbstractSessionBean implements DossiersPiecesSession{


	
	@Override
	public int count(SygDossiers dossier) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossierspieces.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("piece", "piece");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygDossierspieces.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygDossierspieces> find(int indice, int pas,SygDossiers dossier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossierspieces.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("piece", "piece");
		criteria.addOrder(Order.asc("piece.libelle"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygDossierspieces piece) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(piece);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygDossierspieces piece) {
		
		try{
			getHibernateSession().merge(piece);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygDossierspieces findById(Long code) {
		return (SygDossierspieces)getHibernateSession().get(SygDossierspieces.class, code);
	}
	


}
