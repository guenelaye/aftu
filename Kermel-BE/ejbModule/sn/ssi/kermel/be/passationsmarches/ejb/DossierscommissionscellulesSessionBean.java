package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossierscommissionscellules;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class DossierscommissionscellulesSessionBean extends AbstractSessionBean implements DossierscommissionscellulesSession{


	
	@Override
	public int count(SygDossiers dossier) {
		Criteria criteria = getHibernateSession().createCriteria(SygDossierscommissionscellules.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("membre", "membre");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygDossierscommissionscellules.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygDossierscommissionscellules> find(int indice, int pas,SygDossiers dossier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDossierscommissionscellules.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("membre", "membre");
		criteria.addOrder(Order.asc("membre.nom"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygDossierscommissionscellules membre) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(membre);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygDossierscommissionscellules membre) {
		
		try{
			getHibernateSession().merge(membre);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygDossierscommissionscellules findById(Long code) {
		return (SygDossierscommissionscellules)getHibernateSession().get(SygDossierscommissionscellules.class, code);
	}
	


}
