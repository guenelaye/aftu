package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.be.entity.SygPresenceouverture;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class Presence0uvertureSessionBean extends AbstractSessionBean implements Presence0uvertureSession{

	
	
	@Override
	public int count(SygDossiers dossier,SygAppelsOffres appel,SygPlisouvertures plis,int etape) {
		Criteria criteria = getHibernateSession().createCriteria(SygPresenceouverture.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("appel", "appel");
		criteria.createAlias("plis", "plis");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(appel!=null){
			criteria.add(Restrictions.eq("appel", appel));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(etape>-1)
			criteria.add(Restrictions.eq("etapePI", etape));
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygPresenceouverture.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygPresenceouverture> find(int indice, int pas,SygDossiers dossier,SygAppelsOffres appel,SygPlisouvertures plis,int etape) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPresenceouverture.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("appel", "appel");
		criteria.createAlias("plis", "plis");
		criteria.addOrder(Order.asc("plis.raisonsociale"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(appel!=null){
			criteria.add(Restrictions.eq("appel", appel));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis.id", plis.getId()));
		}
		if(etape>-1)
			criteria.add(Restrictions.eq("etapePI", etape));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygPresenceouverture presence) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(presence);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygPresenceouverture presence) {
		
		try{
			getHibernateSession().merge(presence);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygPresenceouverture findById(Long code) {
		return (SygPresenceouverture)getHibernateSession().get(SygPresenceouverture.class, code);
	}
	


}
