package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygContrats;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygPlisouvertures;

@Remote
public interface ContratsSession {
	public void save(SygContrats contrat);
	public void delete(Long id);
	public List<SygContrats> find(int indice, int pas,SygDossiers dossier,SygAutoriteContractante autorite,SygPlisouvertures plis,String statut,SygFournisseur fournisseur,int immatriculation);
	public int count(SygDossiers dossier,SygAutoriteContractante autorite,SygPlisouvertures plis,String statut,SygFournisseur fournisseur,int immatriculation);
	public void update(SygContrats contrat);
	public SygContrats findById(Long code);
	
	public SygContrats getContrat(SygDossiers dossier,SygAutoriteContractante autorite,SygPlisouvertures plis,Long code,SygLots lot);
	List<SygContrats> findbyTypeMarche(int indice, int pas, Long type,
			String statut);
	int countTypeMarche(Long type, String statut);
	public String getGeneratedCode(String code);
	public void updateContrat(SygContrats contrat);
	
	
	public List<SygContrats> find(int indice, int pas,SygDossiers dossier,SygAutoriteContractante autorite,SygPlisouvertures plis,String statut,SygFournisseur fournisseur);
	public int count(SygDossiers dossier,SygAutoriteContractante autorite,SygPlisouvertures plis,String statut,SygFournisseur fournisseur);

	
}
