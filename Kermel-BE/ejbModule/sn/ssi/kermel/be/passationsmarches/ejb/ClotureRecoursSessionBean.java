package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;




import sn.ssi.kermel.be.entity.SygClotureRecours;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class ClotureRecoursSessionBean extends AbstractSessionBean implements ClotureRecoursSession{

	@Override
	public int count(Date Datecloture) {
		Criteria criteria = getHibernateSession().createCriteria(SygClotureRecours.class);
		 criteria.createAlias("state", "state");
		if(Datecloture!=null){
			criteria.add(Restrictions.ge("Datecloture", Datecloture));
		}	
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygClotureRecours.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long Id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygClotureRecours.class, Id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	

	@Override
	public SygClotureRecours save(SygClotureRecours cloturerecours)
	{
	// TODO Auto-generated method stub
			try {
				SygClotureRecours CourrierAudits1=(SygClotureRecours) getHibernateSession().save(cloturerecours);
				getHibernateSession().flush();
				return CourrierAudits1;
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				return null;
			}	
			
	}

	@Override
	public void update(SygClotureRecours cloturerecours) {
		
		try{
			getHibernateSession().merge(cloturerecours);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygClotureRecours findById(Long code) {
		return (SygClotureRecours)getHibernateSession().get(SygClotureRecours.class, code);
	}
	
	
	@Override
	public List<SygClotureRecours> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygClotureRecours.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));
		
		return criteria.list();
	}
	

	@Override
	public List<SygClotureRecours> find(int indice, int pas,Date Datecloture,String Commentaire,String Issuerecoursgracieux) {
       Criteria criteria = getHibernateSession().createCriteria(SygClotureRecours.class);
       criteria.createAlias("state", "state");
		criteria.addOrder(Order.desc("datestatut"));
		
		if(Datecloture!=null){
		criteria.add(Restrictions.ge("Datecloture",Datecloture));
		}
		if(Commentaire!=null){
			criteria.add(Restrictions.eq("Commentaire",Commentaire));
			}
		if(Issuerecoursgracieux!=null){
			criteria.add(Restrictions.eq("Issuerecoursgracieux",Issuerecoursgracieux));
			}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}




@Override
public List<SygClotureRecours> find(int indice, int pas) {
   Criteria criteria = getHibernateSession().createCriteria(SygClotureRecours.class);
	criteria.setFirstResult(indice);
	if(pas>0)
	criteria.setMaxResults(pas);
		
	return criteria.list();
	}


}
