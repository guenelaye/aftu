package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygIssuerecours;








@Remote
public interface IssueRecoursSession {
	public SygIssuerecours save(SygIssuerecours issuerecours);
	public void delete(Long issID);
	public List<SygIssuerecours> find(int indice, int pas,String issLibelle);
	public int count(String issLibelle);
	public void update(SygIssuerecours issuerecours);
	public SygIssuerecours findById(Long code);
	public List<SygIssuerecours> find(String code);
	List<SygIssuerecours> find(int indice, int pas);
	int count();
	
}

