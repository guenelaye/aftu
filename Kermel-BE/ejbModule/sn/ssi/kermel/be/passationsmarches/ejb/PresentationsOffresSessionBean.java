package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygContenusEnveloppesOffres;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygLots;
import sn.ssi.kermel.be.entity.SygPresentationsOffres;
import sn.ssi.kermel.be.entity.SygRecours;
import sn.ssi.kermel.be.entity.UserEnveloppe;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class PresentationsOffresSessionBean extends AbstractSessionBean implements PresentationsOffresSession{

	
	@Override
	public int count(SygDossiers dossier,String libelle, SygLots lot) {
		Criteria criteria = getHibernateSession().createCriteria(SygPresentationsOffres.class);
		criteria.createAlias("dossier", "dossier");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		  if(libelle!=null){
				
				criteria.add(Restrictions.ilike("intituleenveloppe", "%"+libelle+"%"));
			}
		  if(lot!=null){
				
				criteria.add(Restrictions.eq("lot", lot));
			}  
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygPresentationsOffres.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygPresentationsOffres> find(int indice, int pas,SygDossiers dossier,String libelle, SygLots lot) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPresentationsOffres.class);
		criteria.createAlias("dossier", "dossier");
		if(dossier!=null){
			
			criteria.add(Restrictions.eq("dossier", dossier));
		}
        if(libelle!=null){
			
			criteria.add(Restrictions.ilike("intituleenveloppe", "%"+libelle+"%"));
		}
       if(lot!=null){
			
			criteria.add(Restrictions.eq("lot", lot));
		}  
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygPresentationsOffres enveloppe) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(enveloppe);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygPresentationsOffres enveloppe) {
		
		try{
			getHibernateSession().merge(enveloppe);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygPresentationsOffres findById(Long code) {
		return (SygPresentationsOffres)getHibernateSession().get(SygPresentationsOffres.class, code);
	}
	
	@Override
	public List<SygPresentationsOffres> find(Long dossier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPresentationsOffres.class);
		criteria.createAlias("dossier", "dossier");
		if(dossier!=null){
			
			criteria.add(Restrictions.eq("dossier.dosID", dossier));
		}
       
		return criteria.list();
	}

     /////////::Contenues Enveloppes////////

	
	
	@Override
	public int countContenus(SygPresentationsOffres enveloppe) {
		Criteria criteria = getHibernateSession().createCriteria(SygContenusEnveloppesOffres.class);
		criteria.createAlias("enveloppe", "enveloppe");
		if(enveloppe!=null){
			criteria.add(Restrictions.eq("enveloppe", enveloppe));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void deleteContenu(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygContenusEnveloppesOffres.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygContenusEnveloppesOffres> Contenus(int indice, int pas,SygPresentationsOffres enveloppe) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygContenusEnveloppesOffres.class);
		criteria.createAlias("enveloppe", "enveloppe");
		if(enveloppe!=null){
			
			criteria.add(Restrictions.eq("enveloppe", enveloppe));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void saveContenu(SygContenusEnveloppesOffres contenu) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(contenu);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void updateContenu(SygContenusEnveloppesOffres contenu) {
		
		try{
			getHibernateSession().merge(contenu);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygContenusEnveloppesOffres findContenu(Long code) {
		return (SygContenusEnveloppesOffres)getHibernateSession().get(SygContenusEnveloppesOffres.class, code);
	}
	
	@Override
	public List<SygContenusEnveloppesOffres> Contenus(Long enveloppe) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygContenusEnveloppesOffres.class);
		criteria.createAlias("enveloppe", "enveloppe");
		if(enveloppe!=null){
			
			criteria.add(Restrictions.eq("enveloppe.id", enveloppe));
		}
		
		
		return criteria.list();
	}

//  creation des dossier 
	
	

	@SuppressWarnings("unchecked")
	@Override
	public List<SygPresentationsOffres> findEnveloppeNotCreat(Long user, Long dossier) {
		
		return getHibernateSession().createQuery(
				"SELECT enveloppes FROM  SygPresentationsOffres enveloppes where enveloppes.dossier.dosID='"+dossier+"'" 
			+ " and enveloppes.id NOT IN (SELECT userEnveloppe.enveloppe.id from UserEnveloppe userEnveloppe where  userEnveloppe.user.id='" + user + "' and userEnveloppe.enveloppe.dossier.dosID="+dossier+")")
				.list();
	}
	
	
	
	@Override
	public List<UserEnveloppe> Contenus(Long user, Long dossier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(UserEnveloppe.class);
		criteria.createAlias("enveloppe", "enveloppe");
		if(user!=null){
			
			criteria.add(Restrictions.eq("user.id", user));
		}
		
		
        if(dossier!=null){
			
			criteria.add(Restrictions.eq("enveloppe.dossier.dosID", dossier));
		}
		
		return criteria.list();
	}
	
	
	@Override
	public void saveUserEnveloppe(UserEnveloppe userEnveloppe) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(userEnveloppe);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	
	
	@Override
	public void deleteUserEnveloppe(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(UserEnveloppe.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	@Override
	public UserEnveloppe findByIdUserEnveloppe(Long code) {
		return (UserEnveloppe)getHibernateSession().get(UserEnveloppe.class, code);
	}
	// fin du taitement 
	
	@Override
	public List<SygContenusEnveloppesOffres> findByDossier(Long dossier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygContenusEnveloppesOffres.class);
		criteria.createAlias("enveloppe", "enveloppe");
		criteria.createAlias("enveloppe.dossier", "dossier");
		if(dossier!=null){
			
			criteria.add(Restrictions.eq("dossier.dosID", dossier));
		} else 
			return new ArrayList<SygContenusEnveloppesOffres>();
		
		
		return criteria.list();
	}

}
