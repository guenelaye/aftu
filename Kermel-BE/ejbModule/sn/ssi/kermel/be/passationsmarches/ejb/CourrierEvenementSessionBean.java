package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;





import sn.ssi.kermel.be.entity.SygCourrierEvenement;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class CourrierEvenementSessionBean extends AbstractSessionBean implements CourrierEvenementSession{

	@Override
	public int count(Date courrierDateReception) {
		Criteria criteria = getHibernateSession().createCriteria(SygCourrierEvenement.class);
		 //criteria.createAlias("state", "state");
		if(courrierDateReception!=null){
			criteria.add(Restrictions.ge("courrierDateReception", courrierDateReception));
		}	
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygCourrierEvenement.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long Id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygCourrierEvenement.class, Id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	

	@Override
	public SygCourrierEvenement save(SygCourrierEvenement courrierevenement)
	{
	// TODO Auto-generated method stub
			try {
				SygCourrierEvenement CourrierAudits1=(SygCourrierEvenement) getHibernateSession().save(courrierevenement);
				getHibernateSession().flush();
				return CourrierAudits1;
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				return null;
			}	
			
	}

	@Override
	public void update(SygCourrierEvenement courrierevenement) {
		
		try{
			getHibernateSession().merge(courrierevenement);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygCourrierEvenement findById(Long code) {
		return (SygCourrierEvenement)getHibernateSession().get(SygCourrierEvenement.class, code);
	}
	
	
	@Override
	public List<SygCourrierEvenement> find(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCourrierEvenement.class);
		if(code!=null)
			criteria.add(Restrictions.eq("codepiece", code));
		
		return criteria.list();
	}
	

	@Override
	public List<SygCourrierEvenement> find(int indice, int pas,Date courrierDateSaisie,Date courrierDateReception,Date courrierDate,String courrierReference,String courrierObjet,String courrierOrigine,String courrierCommentaire) {
       Criteria criteria = getHibernateSession().createCriteria(SygCourrierEvenement.class);
       //criteria.createAlias("state", "state");
		//criteria.addOrder(Order.desc("datestatut"));
		
		if(courrierDateSaisie!=null){
		criteria.add(Restrictions.ge("courrierDateSaisie",courrierDateSaisie));
		}
		if(courrierDateReception!=null){
			criteria.add(Restrictions.eq("courrierDateReception",courrierDateReception));
			}
		if(courrierDate!=null){
			criteria.add(Restrictions.eq("courrierDate",courrierDate));
			}
		if(courrierObjet!=null){
			criteria.add(Restrictions.eq("courrierObjet",courrierObjet));
			}
		if(courrierOrigine!=null){
			criteria.add(Restrictions.eq("courrierOrigine",courrierOrigine));
			}
		if(courrierCommentaire!=null){
			criteria.add(Restrictions.eq("courrierCommentaire",courrierCommentaire));
			}
		criteria.setFirstResult(indice);
		if(pas>0)
		criteria.setMaxResults(pas);
			
		return criteria.list();
		}




@Override
public List<SygCourrierEvenement> find(int indice, int pas) {
   Criteria criteria = getHibernateSession().createCriteria(SygCourrierEvenement.class);
	criteria.setFirstResult(indice);
	if(pas>0)
	criteria.setMaxResults(pas);
		
	return criteria.list();
	}


}
