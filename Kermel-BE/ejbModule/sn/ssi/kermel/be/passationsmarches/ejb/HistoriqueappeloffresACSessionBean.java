package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygDossiersEvaluateurs;
import sn.ssi.kermel.be.entity.SygHistoriqueappeloffresAC;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class HistoriqueappeloffresACSessionBean extends AbstractSessionBean implements HistoriqueappeloffresACSession{


	
	@Override
	public int count(SygDossiers dossier,SygAutoriteContractante autorite,int attribution) {
		Criteria criteria = getHibernateSession().createCriteria(SygHistoriqueappeloffresAC.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("autorite", "autorite");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(attribution>-1)
			criteria.add(Restrictions.eq("histoac_attribution", attribution));
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygDossiersEvaluateurs.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygHistoriqueappeloffresAC> find(int indice, int pas,SygDossiers dossier,SygAutoriteContractante autorite,int attribution) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygHistoriqueappeloffresAC.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("autorite", "autorite");
		criteria.createAlias("user", "user");
		criteria.addOrder(Order.asc("id"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(autorite!=null){
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if(attribution>-1)
			criteria.add(Restrictions.eq("histoac_attribution", attribution));
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygHistoriqueappeloffresAC historique) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(historique);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygHistoriqueappeloffresAC historique) {
		
		try{
			getHibernateSession().merge(historique);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygHistoriqueappeloffresAC findById(Long code) {
		return (SygHistoriqueappeloffresAC)getHibernateSession().get(SygHistoriqueappeloffresAC.class, code);
	}
	
	@Override
	public SygHistoriqueappeloffresAC getHistorique(SygDossiers dossier,int attribution) {
		Criteria criteria = getHibernateSession().createCriteria(SygHistoriqueappeloffresAC.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("autorite", "autorite");
		criteria.addOrder(Order.desc("id"));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		
		if(attribution>-1)
			criteria.add(Restrictions.eq("histoac_attribution", attribution));
		if(criteria.list().size()>0)
			return (SygHistoriqueappeloffresAC) criteria.list().get(0);
		else
			return null;
	}

}
