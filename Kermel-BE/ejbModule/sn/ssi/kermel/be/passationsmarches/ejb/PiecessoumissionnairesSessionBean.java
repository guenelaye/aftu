package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.DistinctResultTransformer;
import org.hibernate.transform.DistinctRootEntityResultTransformer;
import org.hibernate.transform.ResultTransformer;

import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPieces;
import sn.ssi.kermel.be.entity.SygPiecesplisouvertures;
import sn.ssi.kermel.be.entity.SygPlisouvertures;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class PiecessoumissionnairesSessionBean extends AbstractSessionBean implements PiecessoumissionnairesSession{


	
	@Override
	public int count(SygDossiers dossier,SygPieces piece,SygPlisouvertures plis,String etat) {
		Criteria criteria = getHibernateSession().createCriteria(SygPiecesplisouvertures.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("piece", "piece");
		criteria.createAlias("plis", "plis");
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(piece!=null){
			criteria.add(Restrictions.eq("piece", piece));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(etat!=null){
			criteria.add(Restrictions.eq("etat", etat));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygPiecesplisouvertures.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygPiecesplisouvertures> find(int indice, int pas,SygDossiers dossier,SygPieces piece,SygPlisouvertures plis,String etat) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPiecesplisouvertures.class);
		criteria.createAlias("dossier", "dossier");
		criteria.createAlias("piece", "piece");
		criteria.createAlias("plis", "plis");
		//criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		//scriteria.createAlias("plis.retrait", "retrait");
		//scriteria.addOrder(Order.asc("retrait.nomSoumissionnaire"));
		  // criteria.addOrder(Order.asc("plis.fournisseur"));
		   //criteria.add(Restrictions.)
		   //
		   //criteria.add(P);
		
		// criteria.setProjection(Projections.distinct(Projections.projectionList()));
		if(dossier!=null){
			criteria.add(Restrictions.eq("dossier", dossier));
		}
		if(piece!=null){
			criteria.add(Restrictions.eq("piece", piece));
		}
		if(plis!=null){
			criteria.add(Restrictions.eq("plis", plis));
		}
		if(etat!=null){
			criteria.add(Restrictions.eq("etat", etat));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	

	@Override
	public void save(SygPiecesplisouvertures piece) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(piece);
			getHibernateSession().flush();
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygPiecesplisouvertures piece) {
		
		try{
			getHibernateSession().merge(piece);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygPiecesplisouvertures findById(Long code) {
		return (SygPiecesplisouvertures)getHibernateSession().get(SygPiecesplisouvertures.class, code);
	}
	


}
