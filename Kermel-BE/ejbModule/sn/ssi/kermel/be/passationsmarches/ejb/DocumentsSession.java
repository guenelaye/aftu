package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygAppelsOffres;
import sn.ssi.kermel.be.entity.SygDocuments;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.be.entity.SygPlisouvertures;

@Remote
public interface DocumentsSession {
	public void save(SygDocuments documents);
	public void delete(Long id);
	public List<SygDocuments> find(int indice, int pas,SygDossiers dossier,SygAppelsOffres appel,String type,Long lot, SygPlisouvertures plis);
	public int count(SygDossiers documents,SygAppelsOffres appel,String type,Long lot, SygPlisouvertures plis);
	public void update(SygDocuments documents);
	public SygDocuments findById(Long code);
	
	
}
