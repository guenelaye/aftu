package sn.ssi.kermel.be.passationsmarches.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygPays;
import sn.ssi.kermel.be.entity.SygTypeEvenement;







@Remote
public interface TypeEvenementSession {
	public SygTypeEvenement save(SygTypeEvenement typeevenement);
	public void delete(Long Id);
	public List<SygTypeEvenement> find(int indice, int pas,String libelle);
	public int count(String libelle);
	public void update(SygTypeEvenement typeevenement);
	public SygTypeEvenement findById(Long Id);
	public List<SygTypeEvenement> find(String code);
	List<SygTypeEvenement> find(int indice, int pas);
	int count();
	
}

