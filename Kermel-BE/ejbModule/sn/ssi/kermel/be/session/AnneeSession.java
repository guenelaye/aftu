package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;


import sn.ssi.kermel.be.entity.SygAnnee;


@Remote
public interface AnneeSession {
	public void save(SygAnnee mois);

	public List<SygAnnee> findAll(int indice, int pas);

	public void update(SygAnnee mois);

	SygAnnee findByCode(Integer code);

	public void delete(Integer id);

	List<SygAnnee> findAll();

	public int count(Integer code, String libelle);

	List<SygAnnee> find(int start, int step, String libelle);

	public int count();

//	//
//	public int sumpop(int population);
//
//	public int sumsup(double superficie);

}
