package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.EaPiecesJointes;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class PiecesJointesSessionBean extends AbstractSessionBean implements
		PiecesJointesSession {

	@Override
	public int count(String libelle, String nomfichier, SygProformation formation) {
		Criteria criteria = getHibernateSession().createCriteria(
				EaPiecesJointes.class);

		criteria.createAlias("formation", "formation");
		if (formation != null) {
			criteria.add(Restrictions.eq("formation", formation));
		}
		if (libelle != null) {
			criteria.add(Restrictions.ilike("libelle", "%" + libelle + "%"));
		}
		if (nomfichier != null) {
			criteria.add(Restrictions.ilike("nomfichier", "%" + nomfichier
					+ "%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Integer id) {
		try {
			getHibernateSession().delete(
					getHibernateSession().get(EaPiecesJointes.class, id));
		} catch (Exception e) {
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EaPiecesJointes> find(int indice, int pas,
			SygProformation formation) {
		Criteria criteria = getHibernateSession().createCriteria(
				EaPiecesJointes.class);
		criteria.createAlias("formation", "formation");
		if (formation != null) {
			criteria.add(Restrictions.eq("formation", formation));
		}
		if (indice > 0)
			criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EaPiecesJointes> find(int indice, int pas, String libelle,
			String nomfichier, SygProformation formation) {
		Criteria criteria = getHibernateSession().createCriteria(
				EaPiecesJointes.class);
		criteria.createAlias("formation", "formation");
		if (formation != null) {
			criteria.add(Restrictions.eq("formation", formation));
		}
		if (libelle != null) {
			criteria.add(Restrictions.ilike("libelle", "%" + libelle + "%"));
		}
		if (nomfichier != null) {
			criteria.add(Restrictions.ilike("nomfichier", "%" + nomfichier
					+ "%"));
		}
		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EaPiecesJointes> findAll() {
		Criteria criteria = getHibernateSession().createCriteria(
				EaPiecesJointes.class);
		List<EaPiecesJointes> piecejointe = criteria.list();
		return piecejointe;
	}

	@Override
	public EaPiecesJointes findByCode(Integer id) {
		return (EaPiecesJointes) getHibernateSession().get(
				EaPiecesJointes.class, id);
	}

	@Override
	public void save(EaPiecesJointes piecejointe) {
		try {
			getHibernateSession().save(piecejointe);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void update(EaPiecesJointes piecejointe) {
		try {
			getHibernateSession().merge(piecejointe);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public int count() {
		return 0;
	}

	@Override
	public EaPiecesJointes find(SygProformation formation) {
		Criteria criteria = getHibernateSession().createCriteria(
				EaPiecesJointes.class);
		criteria.createAlias("formation", "formation");
		if (formation != null) {
			criteria.add(Restrictions.eq("formation", formation));
		}
		criteria.add(Restrictions.eq("rapport", true));

		return (EaPiecesJointes) criteria.uniqueResult();
	}
}
