package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.PkQuestion;
import sn.ssi.kermel.be.entity.PklComptePerso;
import sn.ssi.kermel.be.entity.PklPays;
import sn.ssi.kermel.be.entity.SygDossiers;



@Remote
public interface QuestionSession {

    public void save(PkQuestion pays);

    public void delete(Integer Id);

    public List<PkQuestion> find(int indice, int pas);

    public void update(PkQuestion pays);

    public List<PkQuestion> findAll();

    public List<PkQuestion> find(int indice, int pas, String code, String libelle);

    public PkQuestion findByCode(Integer id);

    public PkQuestion find();

    public  int count();

    public PkQuestion findByCode(String code);
	public int count(String code, String libelle);

	List<PkQuestion> find(SygDossiers dossier);

	List<PkQuestion> findRepond(SygDossiers dossier);

	List<PkQuestion> findRep(SygDossiers dossier);

	List<PkQuestion> find(SygDossiers dossier, PklComptePerso user);

	List<PkQuestion> findAllQr(SygDossiers dossier, PklComptePerso user);

	List<PkQuestion> findAllReponses(SygDossiers dossier, PklComptePerso user);
}
