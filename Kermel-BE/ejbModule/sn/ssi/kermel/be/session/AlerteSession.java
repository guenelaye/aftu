package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.PklAlerte;
import sn.ssi.kermel.be.entity.PklComptePerso;
import sn.ssi.kermel.be.entity.PklTypeAlerte;


@Remote
public interface AlerteSession {
	public void save(PklAlerte culture);
	public void delete(Long id);
	public void update(PklAlerte culture);
	int count();
	public int count(String nom,String empreinte, PklComptePerso fournisseur, PklTypeAlerte type);
	public List<PklAlerte> findAll();
	PklAlerte findByCode(Long id);
	public List<PklAlerte> find(int indice, int pas);
	public List<PklAlerte> find(String nom,String empreinte);
	public List<PklAlerte> find(int indice, int pas,String nom,String empreinte, PklComptePerso fournisseur, PklTypeAlerte type);
	PklAlerte saveEcole(PklAlerte culture);
	PklAlerte updateEcole(PklAlerte culture);
	public List<PklAlerte> find(String nom,String empreinte, PklComptePerso fournisseur);
	
}
