package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.PklVille;
import sn.ssi.kermel.be.entity.PklPays;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class VilleSessionBean extends AbstractSessionBean implements VilleSession {

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(PklVille.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

    @SuppressWarnings("unchecked")
	@Override
	public List<PklVille> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PklVille.class);
		criteria.addOrder(Order.asc("libelle"));
		List<PklVille> decoupage1 = criteria.list();

		return decoupage1;
	}

	@Override
	public PklVille findByCode(Integer id) {
		// TODO Auto-generated method stub
		return (PklVille) getHibernateSession().get(PklVille.class,
				id);
	}

	@Override
	public void save(PklVille decoupage1) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(decoupage1);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void update(PklVille decoupage1) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().merge(decoupage1);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public int count(String code, String libelle, PklPays pays) {
		Criteria criteria = getHibernateSession().createCriteria(
				PklVille.class);
		if (code != null) {
			criteria.add(Restrictions.ilike("code", "%" + code + "%"));
		}
		if (libelle != null) {
			criteria.add(Restrictions.ilike("libelle", "%" + libelle + "%"));
		}
		if (pays != null) {
			criteria.add(Restrictions.eq("siap_pays", pays));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

    @SuppressWarnings("unchecked")
	@Override
	public List<PklVille> findAll(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PklVille.class);

		if (indice > 0)
			criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);
		return criteria.list();
	}

	@Override
	public List<PklVille> findAllby(int indice, int pas,String libelle) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int count(Integer code, String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PklVille.class);
		if (code != null) {
			criteria.add(Restrictions.ilike("id", "%" + code + "%"));
		}
		if (libelle != null) {
			criteria.add(Restrictions.ilike("libelle", "%" + libelle + "%"));
		}

		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PklVille> find(int start, int step, String libelle, PklPays pays) {
		Criteria criteria = getHibernateSession().createCriteria(
				PklVille.class);
		criteria.setFirstResult(start);

		if (libelle != null) {
			criteria.add(Restrictions.ilike("libelle", libelle,MatchMode.ANYWHERE));
		}
		criteria.add(Restrictions.eq("siap_pays", pays));
		if(step>0)
			criteria.setMaxResults(step);

		return criteria.list();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PklVille> find(int indice, int pas,
			String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(PklVille.class);

		criteria.addOrder(Order.asc("libelle"));
		
		if (libelle != null) {
			criteria.add(Restrictions.ilike("libelle", "%" + libelle + "%"));
		}

		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

	@Override
	public PklVille findByCodePays(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PklVille.class);
		criteria.setFetchMode("siap_pays", FetchMode.SELECT);
		criteria.addOrder(Order.asc("libelle"));
		if (code != null) {
			criteria.add(Restrictions.eq("code",code));
		}
		return (PklVille)criteria.uniqueResult();
	}




}
