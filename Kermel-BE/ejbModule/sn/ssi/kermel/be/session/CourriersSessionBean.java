package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.EaCourriers;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class CourriersSessionBean extends AbstractSessionBean implements CourriersSession {

	@Override
	public int count( String objet, String referencecourrier, SygProformation formation) {
		Criteria criteria = getHibernateSession().createCriteria(
				EaCourriers.class);
		criteria.createAlias("formation", "formation");
		if (objet != null) {
			criteria.add(Restrictions.ilike("objet", "%"
					+ objet + "%"));
		}
		if (referencecourrier != null) {
			criteria.add(Restrictions.ilike("referencecourrier", "%"
					+ referencecourrier + "%"));
		}
		if (formation != null) {
			criteria.add(Restrictions.eq("formation", formation));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Integer id) {
		try {
			getHibernateSession().delete(
					getHibernateSession().get(EaCourriers.class, id));
		} catch (Exception e) {
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EaCourriers> find(int indice, int pas, SygProformation formation) {
		Criteria criteria = getHibernateSession().createCriteria(
				EaCourriers.class);
		criteria.createAlias("formation", "formation");
		if (formation != null) {
			criteria.add(Restrictions.eq("formation", formation));
		}

		if (indice > 0)
			criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EaCourriers> find(int indice, int pas, String objet,
			String referencecourrier, SygProformation formation) {
		Criteria criteria = getHibernateSession().createCriteria(
				EaCourriers.class);
		criteria.createAlias("formation", "formation");
		if (objet != null) {
			criteria.add(Restrictions.ilike("objet", "%"
					+ objet + "%"));
		}
		if (referencecourrier != null) {
			criteria.add(Restrictions.ilike("referencecourrier", "%"
					+ referencecourrier + "%"));
		}
		if (formation != null) {
			criteria.add(Restrictions.eq("formation", formation));
		}

		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EaCourriers> findAll() {
		Criteria criteria = getHibernateSession().createCriteria(
				EaCourriers.class);
		List<EaCourriers> courrier = criteria.list();
		return courrier;
	}

	@Override
	public EaCourriers findByCode(Integer id) {
		return (EaCourriers) getHibernateSession().get(EaCourriers.class, id);
	}

	@Override
	public void save(EaCourriers courrier) {
		try {
			getHibernateSession().save(courrier);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(EaCourriers courrier) {
		try {
			getHibernateSession().merge(courrier);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public int count() {
		return 0;
	}

}
