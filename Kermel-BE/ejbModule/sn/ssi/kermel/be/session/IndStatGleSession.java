package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCatIndStatGle;
import sn.ssi.kermel.be.entity.SygIndStatGle;

@Remote
public interface IndStatGleSession {

	public void save(SygIndStatGle indicator);
	
	public void delete(Long id);
	
	public List<SygIndStatGle> find(int indice, int pas,String code,String libelle, SygCatIndStatGle categorie);
	
	public int count(String code,String libelle, SygCatIndStatGle categorie);
	
	public void update(SygIndStatGle indicator);
	
	public List<SygIndStatGle> findAll();
	
	SygIndStatGle findByCode(Long id);
	
	public List<SygIndStatGle> find(int indice, int pas);
	
	int count();
}
