package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.PklComptePerso;
import sn.ssi.kermel.be.entity.PklComptePerso.StatutCompte;
import sn.ssi.kermel.be.entity.PklFournisseur;
import sn.ssi.kermel.be.entity.SygFournisseur;


@Remote
public interface ComptePersoSession {
	public void save(PklComptePerso culture);
	public void delete(Long id);
	public void update(PklComptePerso culture);
	int count();
	public int count(String login,String mdp, SygFournisseur fournisseur);
	public List<PklComptePerso> findAll();
	PklComptePerso findByCode(Long id);
	public List<PklComptePerso> find(int indice, int pas);
	public List<PklComptePerso> find(String login,String mdp);
	public List<PklComptePerso> find(int indice, int pas,String login,String mdp, SygFournisseur fournisseur);
	PklComptePerso saveEcole(PklComptePerso culture);
	PklComptePerso updateEcole(PklComptePerso culture);
	public List<PklComptePerso> find(String login,String mdp, SygFournisseur fournisseur);
	PklComptePerso saveP(PklComptePerso culture);
	PklComptePerso findbyLoginPasseword(String login, String password);
	List<PklComptePerso> findUser(int indice, int pas, PklComptePerso user);
	int count(StatutCompte statut, String nomFournisseur);
	List<PklComptePerso> find(int indice, int pas, StatutCompte statut,
			String nomFournisseur);
	PklComptePerso findByHash(String hash);
	
}
