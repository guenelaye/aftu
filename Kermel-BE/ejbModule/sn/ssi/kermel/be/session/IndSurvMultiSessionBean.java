package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygCatIndSurvMulti;
import sn.ssi.kermel.be.entity.SygIndSurvMulti;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class IndSurvMultiSessionBean extends AbstractSessionBean implements IndSurvMultiSession{

	@Override
	public void save(SygIndSurvMulti indicator) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(indicator);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygIndSurvMulti.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygIndSurvMulti> find(int indice, int pas, String code,
			String libelle, SygCatIndSurvMulti categorie) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygIndSurvMulti.class);
		criteria.addOrder(Order.asc("code"));
		if(code!=null){
			criteria.add(Restrictions.ilike("code","%"+ code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle","%"+ libelle+"%"));
		}
		if(categorie!=null){
			criteria.add(Restrictions.eq("categorie", categorie));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public int count(String code, String libelle, SygCatIndSurvMulti categorie) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygIndSurvMulti.class);
		if(code!=null){
			criteria.add(Restrictions.ilike("code","%"+ code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle","%"+ libelle+"%"));
		}
		if(categorie!=null){
			criteria.add(Restrictions.eq("categorie", categorie));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void update(SygIndSurvMulti indicator) {
		// TODO Auto-generated method stub
		try{
			getHibernateSession().merge(indicator);
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygIndSurvMulti> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria=getHibernateSession().createCriteria(SygIndSurvMulti.class);
		List <SygIndSurvMulti> unites=criteria.list();
		return unites;	
	}

	@Override
	public SygIndSurvMulti findByCode(Long id) {
		// TODO Auto-generated method stub
		return (SygIndSurvMulti)getHibernateSession().get(SygIndSurvMulti.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygIndSurvMulti> find(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygIndSurvMulti.class);
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public int count() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygIndSurvMulti.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

}
