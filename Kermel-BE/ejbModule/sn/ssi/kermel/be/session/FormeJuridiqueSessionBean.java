package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.PklFormeJuridique;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class FormeJuridiqueSessionBean extends AbstractSessionBean implements FormeJuridiqueSession {

    @Override
    public void delete(Integer id) {
	// TODO Auto-generated method stub
	try {
	    getHibernateSession().delete(
		    getHibernateSession().get(PklFormeJuridique.class, id));
	} catch (Exception e) {
	    // TODO: handle exception
	}
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PklFormeJuridique> find(int indice, int pas) {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession()
		.createCriteria(PklFormeJuridique.class);

	if (indice > 0) {
	    criteria.setFirstResult(indice);
	}
	if (pas > 0) {
	    criteria.setMaxResults(pas);
	}
	return criteria.list();
    }

    @Override
    public PklFormeJuridique find() {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession()
		.createCriteria(PklFormeJuridique.class);

	return (PklFormeJuridique) criteria.list().get(0);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PklFormeJuridique> findAll() {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession()
		.createCriteria(PklFormeJuridique.class);
	List<PklFormeJuridique> pays = criteria.list();
	return pays;
    }

    @Override
    public void save(PklFormeJuridique pays) {
	// TODO Auto-generated method stub
	try {
	    getHibernateSession().save(pays);

	} catch (Exception e) {
	    // TODO: handle exception
	    e.printStackTrace();
	}
    }

    @Override
    public int count() {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession().createCriteria(PklFormeJuridique.class);
	criteria.setProjection(Projections.rowCount());
	return Integer.parseInt(criteria.uniqueResult().toString());

    }
    @Override
    public void update(PklFormeJuridique pays) {
	// TODO Auto-generated method stub
	try {
	    getHibernateSession().merge(pays);
	} catch (Exception e) {
	    // TODO: handle exception
	}
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PklFormeJuridique> find(int indice, int pas, String code, String libelle) {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession()
		.createCriteria(PklFormeJuridique.class);
	criteria.addOrder(Order.asc("libelle"));
	if (code != null) {
	    criteria.add(Restrictions.ilike("code", "%" + code + "%"));
	}
	if (libelle != null) {
	    criteria.add(Restrictions.ilike("libelle", "%" + libelle + "%"));
	}

	criteria.setFirstResult(indice);
	if (pas > 0) {
	    criteria.setMaxResults(pas);
	}

	return criteria.list();
    }

    @Override
    public PklFormeJuridique findByCode(Integer id) {
	// TODO Auto-generated method stub
	return (PklFormeJuridique) getHibernateSession().get(PklFormeJuridique.class, id);
    }

    @Override
    public PklFormeJuridique findByCode(String code) {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession()
		.createCriteria(PklFormeJuridique.class);
	if(code!=null){
	    criteria.add(Restrictions.eq("code", code));
	}

	PklFormeJuridique pays = null;

	if(criteria.list().size()==1){
	    pays =  (PklFormeJuridique)criteria.uniqueResult();
	}else if(criteria.list().size()>0){
	    pays = (PklFormeJuridique) criteria.list().get(0);
	}
	return pays;
    }

	@Override
	public int count(String code, String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(PklFormeJuridique.class);

		if(code!=null){
			criteria.add(Restrictions.ilike("code", "%"+code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.eq("libelle", libelle));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
}
