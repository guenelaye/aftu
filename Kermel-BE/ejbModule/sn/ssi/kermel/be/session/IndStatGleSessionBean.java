package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygCatIndStatGle;
import sn.ssi.kermel.be.entity.SygIndStatGle;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class IndStatGleSessionBean extends AbstractSessionBean implements IndStatGleSession{

	@Override
	public void save(SygIndStatGle indicator) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(indicator);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygIndStatGle.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygIndStatGle> find(int indice, int pas, String code,
			String libelle, SygCatIndStatGle categorie) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygIndStatGle.class);
		criteria.addOrder(Order.asc("code"));
		if(code!=null){
			criteria.add(Restrictions.ilike("code","%"+ code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle","%"+ libelle+"%"));
		}
		if(categorie!=null){
			criteria.add(Restrictions.eq("categorie", categorie));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public int count(String code, String libelle, SygCatIndStatGle categorie) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygIndStatGle.class);
		if(code!=null){
			criteria.add(Restrictions.ilike("code","%"+ code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle","%"+ libelle+"%"));
		}
		if(categorie!=null){
			criteria.add(Restrictions.eq("categorie", categorie));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void update(SygIndStatGle indicator) {
		// TODO Auto-generated method stub
		try{
			getHibernateSession().merge(indicator);
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygIndStatGle> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria=getHibernateSession().createCriteria(SygIndStatGle.class);
		List <SygIndStatGle> unites=criteria.list();
		return unites;	
	}

	@Override
	public SygIndStatGle findByCode(Long id) {
		// TODO Auto-generated method stub
		return (SygIndStatGle)getHibernateSession().get(SygIndStatGle.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygIndStatGle> find(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygIndStatGle.class);
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public int count() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygIndStatGle.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

}
