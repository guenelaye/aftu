package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCatIndSurvMulti;

@Remote
public interface CatIndSurvMultiSession {

	public void save(SygCatIndSurvMulti categ);

	public void update(SygCatIndSurvMulti categ);
	
	public void delete(Integer id);
	
	int count();

	public int count(String code,String libelle);

	public List<SygCatIndSurvMulti> findAll();

	SygCatIndSurvMulti findByCode(Integer id);

	public List<SygCatIndSurvMulti> find(int indice, int pas);
	
	public List<SygCatIndSurvMulti> find(int indice, int pas,String code,String libelle);
	
	
}
