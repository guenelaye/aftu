package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.PkQuestion;
import sn.ssi.kermel.be.entity.PklComptePerso;
import sn.ssi.kermel.be.entity.PklPays;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class QuestionSessionBean extends AbstractSessionBean implements
		QuestionSession {

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(PkQuestion.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PkQuestion> find(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PkQuestion.class);

		if (indice > 0) {
			criteria.setFirstResult(indice);
		}
		if (pas > 0) {
			criteria.setMaxResults(pas);
		}
		return criteria.list();
	}

	@Override
	public PkQuestion find() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PkQuestion.class);

		return (PkQuestion) criteria.list().get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PkQuestion> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PkQuestion.class);
		List<PkQuestion> pays = criteria.list();
		return pays;
	}

	@Override
	public void save(PkQuestion pays) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(pays);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public int count() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PkQuestion.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@Override
	public void update(PkQuestion pays) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().merge(pays);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PkQuestion> find(int indice, int pas, String code,
			String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PkQuestion.class);
		// criteria.addOrder(Order.asc("libelle"));
		if (code != null) {
			// criteria.add(Restrictions.ilike("code", "%" + code + "%"));
		}
		if (libelle != null) {
			// criteria.add(Restrictions.ilike("libelle", "%" + libelle + "%"));
		}

		criteria.setFirstResult(indice);
		if (pas > 0) {
			criteria.setMaxResults(pas);
		}

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PkQuestion> find(SygDossiers dossier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PkQuestion.class);
		criteria.add(Restrictions.isNull("rep"));

		if (dossier != null) {
			criteria.add(Restrictions.eq("dossier", dossier));
		}

		return criteria.list();
	}

	@Override
	public List<PkQuestion> find(SygDossiers dossier, PklComptePerso user) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PkQuestion.class);
		criteria.add(Restrictions.isNull("rep"));

		if (dossier != null) {
			criteria.add(Restrictions.eq("dossier", dossier));
		}

		if (user != null) {
			criteria.add(Restrictions.eq("user", user));
		}

		return criteria.list();
	}
	
	@Override
	public List<PkQuestion> findAllQr(SygDossiers dossier, PklComptePerso user) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PkQuestion.class);
		
		

		if (dossier != null) {
			criteria.add(Restrictions.eq("dossier", dossier));
		}

		if (user != null) {
			criteria.add(Restrictions.eq("user", user));
		}

		return criteria.list();
	}
	
	
	@Override
	public List<PkQuestion> findAllReponses(SygDossiers dossier, PklComptePerso user) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PkQuestion.class);
		criteria.add(Restrictions.isNotNull("rep"));

		if (dossier != null) {
			criteria.add(Restrictions.eq("dossier", dossier));
		}

		if (user != null) {
			criteria.add(Restrictions.eq("user", user));
		}

		return criteria.list();
	}
	
	

	@SuppressWarnings("unchecked")
	@Override
	public List<PkQuestion> findRep(SygDossiers dossier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PkQuestion.class);
		criteria.add(Restrictions.isNotNull("rep"));

		if (dossier != null) {
			criteria.add(Restrictions.eq("dossier", dossier));
		}

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PkQuestion> findRepond(SygDossiers dossier) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PkQuestion.class);
		// criteria.add(Restrictions.isNotNull("reponse"));
		if (dossier != null) {
			criteria.add(Restrictions.eq("dossier", dossier));
		}

		return criteria.list();
	}

	@Override
	public PkQuestion findByCode(Integer id) {
		// TODO Auto-generated method stub
		return (PkQuestion) getHibernateSession().get(PkQuestion.class, id);

	}

	@Override
	public PkQuestion findByCode(String code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PkQuestion.class);
		if (code != null) {
			// criteria.add(Restrictions.eq("code", code));
		}

		PkQuestion pays = null;

		if (criteria.list().size() == 1) {
			pays = (PkQuestion) criteria.uniqueResult();
		} else if (criteria.list().size() > 0) {
			pays = (PkQuestion) criteria.list().get(0);
		}
		return pays;
	}

	@Override
	public int count(String code, String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(
				PkQuestion.class);

		if (code != null) {
			// criteria.add(Restrictions.ilike("code", "%"+code+"%"));
		}
		if (libelle != null) {
			// criteria.add(Restrictions.eq("libelle", libelle));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
}
