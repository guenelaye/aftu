package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;


import sn.ssi.kermel.be.entity.SygMois;
import sn.ssi.kermel.be.entity.SygAnnee;


@Remote
public interface MoisSession {
	public void save(SygMois mois);

	public List<SygMois> findAll(int indice, int pas);

	public void update(SygMois mois);

	SygMois findByCode(Integer code);

	List<SygMois> findAllby(int indice, int pas, String code,
			String libelle);

	public void delete(Integer id);

	public List<SygMois> find(int indice, int pas, String code,
			String libelle, SygAnnee pays);

	public int count(String code, String libelle, SygAnnee pays);

	List<SygMois> findAll();

	public int count(Integer code, String libelle);

	List<SygMois> find(int start, int step, String libelle);

	List<SygMois> find(int indice, int pas, String code, String libelle);
	
	public List<SygMois> find(int indice, int pas,Integer coderegion);

//	//
//	public int sumpop(int population);
//
//	public int sumsup(double superficie);

}
