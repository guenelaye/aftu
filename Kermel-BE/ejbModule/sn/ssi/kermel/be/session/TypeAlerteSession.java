package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.PklTypeAlerte;



@Remote
public interface TypeAlerteSession {

    public void save(PklTypeAlerte pays);

    public void delete(Integer Id);

    public List<PklTypeAlerte> find(int indice, int pas);

    public void update(PklTypeAlerte pays);

    public List<PklTypeAlerte> findAll();

    public List<PklTypeAlerte> find(int indice, int pas, String libelle);

    PklTypeAlerte findByCode(Integer id);

    PklTypeAlerte find();

    int count();

	public int count(String libelle);
}
