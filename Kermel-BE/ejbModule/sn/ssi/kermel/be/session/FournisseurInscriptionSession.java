package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.PklFormeJuridique;
import sn.ssi.kermel.be.entity.PklFournisseur;
import sn.ssi.kermel.be.entity.PklVille;


@Remote
public interface FournisseurInscriptionSession {
	public void save(PklFournisseur culture);
	public void delete(Long id);
	public void update(PklFournisseur culture);
	int count();
	public int count(String IdN,String nom, PklFormeJuridique departement);
	public List<PklFournisseur> findAll();
	PklFournisseur findByCode(Long id);
	public List<PklFournisseur> find(int indice, int pas);
	public List<PklFournisseur> find(String IdN,String nom);
	public List<PklFournisseur> find(int indice, int pas,String IdN,String nom, PklFormeJuridique departement);
	PklFournisseur saveEcole(PklFournisseur culture);
	PklFournisseur updateEcole(PklFournisseur culture);
	List<PklFournisseur> find(String IdN, String nom, PklVille region);
	PklFournisseur saveF(PklFournisseur culture);
	
}
