package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.PklComptePerso;
import sn.ssi.kermel.be.entity.PklComptePerso.StatutCompte;
import sn.ssi.kermel.be.entity.PklFournisseur;
import sn.ssi.kermel.be.entity.SygFournisseur;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class ComptePersoSessionBean extends AbstractSessionBean implements
		ComptePersoSession {

	@Override
	public void save(PklComptePerso culture) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(culture);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public PklComptePerso saveP(PklComptePerso culture) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(culture);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return culture;
	}

	@Override
	public PklComptePerso saveEcole(PklComptePerso culture) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(culture);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return culture;
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(PklComptePerso.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void update(PklComptePerso culture) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().merge(culture);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public PklComptePerso updateEcole(PklComptePerso culture) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().merge(culture);
		} catch (Exception e) {
			// TODO: handle exception
		}

		return culture;
	}

	@Override
	public int count() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PklComptePerso.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public int count(String login, String mdp, SygFournisseur fournisseur) {
		Criteria criteria = getHibernateSession().createCriteria(
				PklComptePerso.class);
		if (login != null) {
			criteria.add(Restrictions.ilike("login", login, MatchMode.ANYWHERE));
		}
		if (mdp != null) {
			criteria.add(Restrictions.eq("mdp", mdp));
		}
		if (fournisseur != null) {
			criteria.add(Restrictions.eq("fournisseur.id", fournisseur.getId()));
		}

		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PklComptePerso> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PklComptePerso.class);
		List<PklComptePerso> unites = criteria.list();
		return unites;
	}

	@Override
	public PklComptePerso findByCode(Long id) {
		// TODO Auto-generated method stub
		return (PklComptePerso) getHibernateSession().get(PklComptePerso.class,
				id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PklComptePerso> find(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PklComptePerso.class);

		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PklComptePerso> findUser(int indice, int pas,
			PklComptePerso user) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				PklComptePerso.class);

		if (user != null) {
			criteria.add(Restrictions.eq("user", user));
		}

		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PklComptePerso> find(String login, String mdp) {
		// Criteria criteria =
		// getHibernateSession().createCriteria(PklComptePerso.class);
		//
		// if(login!=null){
		// criteria.add(Restrictions.ilike("login", login, MatchMode.ANYWHERE));
		// }
		// if(mdp!=null){
		// criteria.add(Restrictions.eq("mdp", mdp));
		// }
		//
		List<PklComptePerso> utilisateurs = null;
		Query q = getHibernateSession().createQuery(
				"FROM pkl_compteperso WHERE login LIKE '" + login
						+ "' AND password LIKE '" + mdp + "'");
		utilisateurs = q.list();
		return utilisateurs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public PklComptePerso findbyLoginPasseword(String login, String password) {
		Criteria criteria = getHibernateSession().createCriteria(
				PklComptePerso.class);

		if (login != null) {
			criteria.add(Restrictions.eq("login", login));
		}

		if (password != null) {
			criteria.add(Restrictions.eq("password", password));
		}

		if (criteria.list().size() > 0)
			return (PklComptePerso) criteria.list().get(0);
		else
			return null;

	}

	@Override
	public PklComptePerso findByHash(String hash) {
		Criteria criteria = getHibernateSession().createCriteria(
				PklComptePerso.class);

		if (hash != null) {
			criteria.add(Restrictions.eq("hash", hash));
		}

//		criteria.add(Restrictions.eq("statut", StatutCompte.ATTENTE));

		if (criteria.list().size() > 0)
			return (PklComptePerso) criteria.list().get(0);
		else
			return null;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PklComptePerso> find(int indice, int pas, String login,
			String mdp, SygFournisseur fournisseur) {
		Criteria criteria = getHibernateSession().createCriteria(
				PklComptePerso.class);
		criteria.addOrder(Order.asc("login"));
		if (login != null) {
			criteria.add(Restrictions.ilike("login", login, MatchMode.ANYWHERE));
		}
		if (mdp != null) {
			criteria.add(Restrictions.eq("mdp", mdp));
		}
		if (fournisseur != null) {
			criteria.add(Restrictions.eq("fournisseur.id", fournisseur.getId()));
		}

		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PklComptePerso> find(String login, String mdp,
			SygFournisseur fournisseur) {
		Criteria criteria = getHibernateSession().createCriteria(
				PklComptePerso.class);

		if (login != null) {
			criteria.add(Restrictions.eq("login", login));
		}
		if (mdp != null) {
			criteria.add(Restrictions.eq("mdp", mdp));
		}
		if (fournisseur != null) {
			criteria.add(Restrictions.eq("fournisseur.id", fournisseur.getId()));
		}

		return criteria.list();
	}

	@Override
	public List<PklComptePerso> find(int indice, int pas, StatutCompte statut,
			String nomFournisseur) {
		Criteria criteria = getHibernateSession().createCriteria(
				PklComptePerso.class);
		// criteria.addOrder(Order.asc("login"));
		if (statut != null) {
			criteria.add(Restrictions.eq("statut", statut));
		}

		if (nomFournisseur != null) {
			criteria.add(Restrictions.eq("fournisseur.nom", nomFournisseur));
		}

		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

	@Override
	public int count(StatutCompte statut, String nomFournisseur) {
		Criteria criteria = getHibernateSession().createCriteria(
				PklComptePerso.class);

		if (statut != null) {
			criteria.add(Restrictions.eq("statut", statut));
		}

		if (nomFournisseur != null) {
			criteria.add(Restrictions.eq("fournisseur.nom", nomFournisseur));
		}

		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

}
