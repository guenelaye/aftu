package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.EaCourriers;
import sn.ssi.kermel.be.entity.SygProformation;

@Remote
public interface CourriersSession {

	public void save(EaCourriers courrier);

	public void delete(Integer id);

	public List<EaCourriers> find(int indice, int pas, SygProformation formation);

	public void update(EaCourriers courrier);

	public List<EaCourriers> findAll();

	public List<EaCourriers> find(int indice, int pas, String objet,
			String referencecourrier, SygProformation formation);

	public int count(String objet, String referencecourrier,
			SygProformation formation);

	public EaCourriers findByCode(Integer id);

	public int count();

}
