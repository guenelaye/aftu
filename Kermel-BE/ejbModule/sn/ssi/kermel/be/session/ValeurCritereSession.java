package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.PklValeurCritere;



@Remote
public interface ValeurCritereSession {

    public void save(PklValeurCritere pays);

    public void delete(Integer Id);

    public List<PklValeurCritere> find(int indice, int pas);

    public void update(PklValeurCritere pays);

    public List<PklValeurCritere> findAll();

    public List<PklValeurCritere> find(int indice, int pas, String valeur);

    PklValeurCritere findByCode(Integer id);

    PklValeurCritere find();

    int count();

	public int count(String valeur);
}
