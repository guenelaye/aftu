package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygCatIndSurvMulti;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class CatIndSurvMultiSessionBean extends AbstractSessionBean implements CatIndSurvMultiSession{

	@Override
	public void save(SygCatIndSurvMulti categ) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(categ);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	
	}

	@Override
	public void update(SygCatIndSurvMulti categ) {
		// TODO Auto-generated method stub
		try{
			getHibernateSession().merge(categ);
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygCatIndSurvMulti.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public int count() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCatIndSurvMulti.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public int count(String code, String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCatIndSurvMulti.class);
		if(code!=null){
			criteria.add(Restrictions.ilike("code","%"+ code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle","%"+ libelle+"%"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygCatIndSurvMulti> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria=getHibernateSession().createCriteria(SygCatIndSurvMulti.class);
		List <SygCatIndSurvMulti> categ=criteria.list();
		return categ;	
	}

	@Override
	public SygCatIndSurvMulti findByCode(Integer id) {
		// TODO Auto-generated method stub
		return (SygCatIndSurvMulti)getHibernateSession().get(SygCatIndSurvMulti.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygCatIndSurvMulti> find(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCatIndSurvMulti.class);
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygCatIndSurvMulti> find(int indice, int pas, String code,
			String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCatIndSurvMulti.class);
		criteria.addOrder(Order.asc("code"));
		if(code!=null){
			criteria.add(Restrictions.ilike("code","%"+ code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle","%"+ libelle+"%"));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

}
