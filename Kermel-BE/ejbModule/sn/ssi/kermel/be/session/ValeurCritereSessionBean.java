package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.PklValeurCritere;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class ValeurCritereSessionBean extends AbstractSessionBean implements ValeurCritereSession {

    @Override
    public void delete(Integer id) {
	// TODO Auto-generated method stub
		try {
		    getHibernateSession().delete(
			    getHibernateSession().get(PklValeurCritere.class, id));
		} catch (Exception e) {
		    // TODO: handle exception
		}
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PklValeurCritere> find(int indice, int pas) {
	// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(PklValeurCritere.class);
	
		if (indice > 0) {
		    criteria.setFirstResult(indice);
		}
		if (pas > 0) {
		    criteria.setMaxResults(pas);
		}
		return criteria.list();
    }

    @Override
    public PklValeurCritere find() {
	// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(PklValeurCritere.class);
	
		return (PklValeurCritere) criteria.list().get(0);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PklValeurCritere> findAll() {
	// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(PklValeurCritere.class);
		List<PklValeurCritere> pays = criteria.list();
		return pays;
    }

    @Override
    public void save(PklValeurCritere pays) {
	// TODO Auto-generated method stub
	try {
	    getHibernateSession().save(pays);

	} catch (Exception e) {
	    // TODO: handle exception
	    e.printStackTrace();
	}
    }

    @Override
    public int count() {
	// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(PklValeurCritere.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

    }
    @Override
    public void update(PklValeurCritere pays) {
	// TODO Auto-generated method stub
		try {
		    getHibernateSession().merge(pays);
		} catch (Exception e) {
		    // TODO: handle exception
		}
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PklValeurCritere> find(int indice, int pas, String valeur) {
	// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession()
			.createCriteria(PklValeurCritere.class);
		criteria.addOrder(Order.asc("valeur"));
		if (valeur != null) {
		    criteria.add(Restrictions.ilike("valeur", valeur, MatchMode.ANYWHERE));
		}
	
		criteria.setFirstResult(indice);
		if (pas > 0) {
		    criteria.setMaxResults(pas);
		}
	
		return criteria.list();
    }

    @Override
    public PklValeurCritere findByCode(Integer id) {
	// TODO Auto-generated method stub
    	return (PklValeurCritere) getHibernateSession().get(PklValeurCritere.class, id);
    }

	@Override
	public int count(String valeur) {
		Criteria criteria = getHibernateSession().createCriteria(PklValeurCritere.class);

		if(valeur!=null){
		    criteria.add(Restrictions.ilike("valeur", valeur, MatchMode.ANYWHERE));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
}
