package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.PklTypeAlerte;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class TypeAlerteSessionBean extends AbstractSessionBean implements TypeAlerteSession {

    @Override
    public void delete(Integer id) {
	// TODO Auto-generated method stub
		try {
		    getHibernateSession().delete(
			    getHibernateSession().get(PklTypeAlerte.class, id));
		} catch (Exception e) {
		    // TODO: handle exception
		}
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PklTypeAlerte> find(int indice, int pas) {
	// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(PklTypeAlerte.class);
	
		if (indice > 0) {
		    criteria.setFirstResult(indice);
		}
		if (pas > 0) {
		    criteria.setMaxResults(pas);
		}
		return criteria.list();
    }

    @Override
    public PklTypeAlerte find() {
	// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(PklTypeAlerte.class);
	
		return (PklTypeAlerte) criteria.list().get(0);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PklTypeAlerte> findAll() {
	// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(PklTypeAlerte.class);
		List<PklTypeAlerte> pays = criteria.list();
		return pays;
    }

    @Override
    public void save(PklTypeAlerte pays) {
	// TODO Auto-generated method stub
	try {
	    getHibernateSession().save(pays);

	} catch (Exception e) {
	    // TODO: handle exception
	    e.printStackTrace();
	}
    }

    @Override
    public int count() {
	// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(PklTypeAlerte.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

    }
    @Override
    public void update(PklTypeAlerte pays) {
	// TODO Auto-generated method stub
		try {
		    getHibernateSession().merge(pays);
		} catch (Exception e) {
		    // TODO: handle exception
		}
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PklTypeAlerte> find(int indice, int pas, String libelle) {
	// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession()
			.createCriteria(PklTypeAlerte.class);
		criteria.addOrder(Order.asc("libelle"));
		if (libelle != null) {
		    criteria.add(Restrictions.ilike("libelle", "%" + libelle + "%"));
		}
	
		criteria.setFirstResult(indice);
		if (pas > 0) {
		    criteria.setMaxResults(pas);
		}
	
		return criteria.list();
    }

    @Override
    public PklTypeAlerte findByCode(Integer id) {
	// TODO Auto-generated method stub
    	return (PklTypeAlerte) getHibernateSession().get(PklTypeAlerte.class, id);
    }

	@Override
	public int count(String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(PklTypeAlerte.class);

		if(libelle!=null){
		    criteria.add(Restrictions.ilike("libelle", "%" + libelle + "%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
}
