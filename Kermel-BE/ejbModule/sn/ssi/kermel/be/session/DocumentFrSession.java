package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.PklDocumentFr;
import sn.ssi.kermel.be.entity.PklFournisseur;


@Remote
public interface DocumentFrSession {
	public void save(PklDocumentFr culture);
	public void delete(Long id);
	public void update(PklDocumentFr culture);
	int count();
	public int count(String nom,String empreinte, PklFournisseur fournisseur);
	public List<PklDocumentFr> findAll();
	PklDocumentFr findByCode(Long id);
	public List<PklDocumentFr> find(int indice, int pas);
	public List<PklDocumentFr> find(String nom,String empreinte);
	public List<PklDocumentFr> find(int indice, int pas,String nom,String empreinte, PklFournisseur fournisseur);
	PklDocumentFr saveEcole(PklDocumentFr culture);
	PklDocumentFr updateEcole(PklDocumentFr culture);
	public List<PklDocumentFr> find(String nom,String empreinte, PklFournisseur fournisseur);
	
}
