package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygParticipantsFormation;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygGroupeFormation;
import sn.ssi.kermel.be.entity.SygProformation;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class ParticipantsSessionBean extends AbstractSessionBean implements
		ParticipantsSession {

	@Override
	public int count(String prenom, String nom, SygProformation formation,SygGroupeFormation groupe) {
		Criteria criteria = getHibernateSession().createCriteria(
				SygParticipantsFormation.class);
		criteria.createAlias("formation", "formation");
		//criteria.createAlias("groupe", "groupe");
		if (formation != null) {
			criteria.add(Restrictions.eq("formation", formation));
		}
		if (groupe != null) {
			criteria.add(Restrictions.eq("groupe", groupe));
		}
		if (prenom != null) {
			criteria.add(Restrictions.ilike("prenom", "%" + prenom + "%"));
		}
		if (nom != null) {
			criteria.add(Restrictions.ilike("nom", "%" + nom + "%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Integer id) {
		try {
			getHibernateSession().delete(
					getHibernateSession().get(SygParticipantsFormation.class, id));
		} catch (Exception e) {
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygParticipantsFormation> find(int indice, int pas, SygProformation formation) {
		Criteria criteria = getHibernateSession().createCriteria(
				SygParticipantsFormation.class);
		criteria.createAlias("formation", "formation");
		if (formation != null) {
			criteria.add(Restrictions.eq("formation", formation));
		}
		if (indice > 0)
			criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygParticipantsFormation> find(int indice, int pas, String prenom,
			String nom,SygProformation formation,SygAutoriteContractante autorite,SygGroupeFormation groupe) {
		Criteria criteria = getHibernateSession().createCriteria(
				SygParticipantsFormation.class);
		criteria.createAlias("formation", "formation");
		criteria.createAlias("autorite", "autorite");
		criteria.createAlias("typeautorite", "typeautorite");
		//criteria.createAlias("groupe", "groupe");
		if (formation != null) {
			criteria.add(Restrictions.eq("formation", formation));
		}
		if (autorite != null) {
			criteria.add(Restrictions.eq("autorite", autorite));
		}
		if (groupe != null) {
			criteria.add(Restrictions.eq("groupe", groupe));
		}
		if (prenom != null) {
			criteria.add(Restrictions.ilike("prenom", "%" + prenom + "%"));
		}
		if (nom != null) {
			criteria.add(Restrictions.ilike("nom", "%" + nom + "%"));
		}
		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygParticipantsFormation> findAll() {
		Criteria criteria = getHibernateSession().createCriteria(
				SygParticipantsFormation.class);
		criteria.createAlias("formation", "formation");
		criteria.createAlias("autorite", "autorite");
		criteria.createAlias("typeautorite", "typeautorite");
		criteria.createAlias("groupe", "groupe");
		List<SygParticipantsFormation> participant = criteria.list();
		return participant;
	}

	@Override
	public SygParticipantsFormation findByCode(Integer id) {
		return (SygParticipantsFormation) getHibernateSession().get(SygParticipantsFormation.class,
				id);
	}

	@Override
	public void save(SygParticipantsFormation participant) {
		try {
			getHibernateSession().save(participant);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(SygParticipantsFormation participant) {
		try {
			getHibernateSession().merge(participant);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public int count() {
		return 0;
	}

}
