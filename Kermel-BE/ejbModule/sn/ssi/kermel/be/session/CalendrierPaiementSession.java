package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCalendrierPaiement;
import sn.ssi.kermel.be.entity.SygPaiement;

@Remote
public interface CalendrierPaiementSession {
	
	public void save(SygCalendrierPaiement paiement);
	
	public void delete(Long id);
	
	public List<SygCalendrierPaiement> find(int indice, int pas,Long code,String libelle);
	
	public List<SygCalendrierPaiement> findRech(int indice, int pas,Long code,String nom,String prenom);
	
	public int count(Long code,String libelle);
	
	public int countRech(Long code,String nom,String prenom);
	
	public void update(SygCalendrierPaiement paiement);
	
	public SygCalendrierPaiement findById(Long code);

	List<SygCalendrierPaiement> findCalNonPayer(int indice, int pas, Long code,
			String libelle);

	List<SygCalendrierPaiement> findCalen(Long code);
	
}
