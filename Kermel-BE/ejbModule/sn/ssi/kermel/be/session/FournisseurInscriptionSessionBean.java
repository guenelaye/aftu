package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.PklFormeJuridique;
import sn.ssi.kermel.be.entity.PklFournisseur;
import sn.ssi.kermel.be.entity.PklVille;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class FournisseurInscriptionSessionBean extends AbstractSessionBean implements FournisseurInscriptionSession {

	@Override
	public void save(PklFournisseur culture) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(culture);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	
	}
	@Override
	public PklFournisseur saveF(PklFournisseur culture) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(culture);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return culture;
	}

	@Override
	public PklFournisseur saveEcole(PklFournisseur culture) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(culture);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return culture;
	}
	
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(PklFournisseur.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void update(PklFournisseur culture) {
		// TODO Auto-generated method stub
		try{
			getHibernateSession().merge(culture);
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public PklFournisseur updateEcole(PklFournisseur culture) {
		// TODO Auto-generated method stub
		try{
			getHibernateSession().merge(culture);
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		return culture;
	}
	
	@Override
	public int count() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(PklFournisseur.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public int count(String IdN,String nom, PklFormeJuridique forme) {
		Criteria criteria = getHibernateSession().createCriteria(PklFournisseur.class);
		if(IdN!=null){
			criteria.add(Restrictions.ilike("IdN", IdN, MatchMode.ANYWHERE));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("nom", nom, MatchMode.ANYWHERE));
		}
		if(forme!=null){
			criteria.add(Restrictions.eq("forme.id", forme.getId()));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PklFournisseur> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria=getHibernateSession().createCriteria(PklFournisseur.class);
		List <PklFournisseur> unites=criteria.list();
		return unites;	
	}

	@Override
	public PklFournisseur findByCode(Long id) {
		// TODO Auto-generated method stub
		return (PklFournisseur)getHibernateSession().get(PklFournisseur.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PklFournisseur> find(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(PklFournisseur.class);
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PklFournisseur> find(String IdN,String nom) {
		Criteria criteria = getHibernateSession().createCriteria(PklFournisseur.class);
		
		if(IdN!=null){
			criteria.add(Restrictions.ilike("IdN", IdN, MatchMode.ANYWHERE));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("nom", nom, MatchMode.ANYWHERE));
		}
				
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PklFournisseur> find(int indice, int pas,String IdN,String nom, PklFormeJuridique forme) {
		Criteria criteria = getHibernateSession().createCriteria(PklFournisseur.class);
		criteria.addOrder(Order.asc("IdN"));
		if(IdN!=null){
			criteria.add(Restrictions.ilike("IdN", IdN, MatchMode.ANYWHERE));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("nom", nom, MatchMode.ANYWHERE));
		}
		if(forme!=null){
			criteria.add(Restrictions.eq("forme.id", forme.getId()));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PklFournisseur> find(String IdN,String nom, PklVille region) {
		Criteria criteria = getHibernateSession().createCriteria(PklFournisseur.class);
		criteria.addOrder(Order.asc("IdN"));
		
		if(IdN!=null){
			criteria.add(Restrictions.ilike("IdN", IdN, MatchMode.ANYWHERE));
		}
		if(nom!=null){
			criteria.add(Restrictions.ilike("nom", nom, MatchMode.ANYWHERE));
		}
		if(region!=null){
			criteria.add(Restrictions.eq("ville", region.getId()));
		}
		
		
		return criteria.list();
	}

	
}
