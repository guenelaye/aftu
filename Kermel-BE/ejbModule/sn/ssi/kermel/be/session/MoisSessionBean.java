package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAnnee;
import sn.ssi.kermel.be.entity.SygMois;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class MoisSessionBean extends AbstractSessionBean implements
		MoisSession {

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(SygMois.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygMois> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygMois.class);
		criteria.addOrder(Order.asc("ord"));
		List<SygMois> decoupage1 = criteria.list();
		
		return decoupage1;
	}

	@Override
	public SygMois findByCode(Integer id) {
		// TODO Auto-generated method stub
		return (SygMois) getHibernateSession().get(SygMois.class,
				id);
	}

	@Override
	public void save(SygMois mois) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(mois);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void update(SygMois mois) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().merge(mois);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygMois> find(int indice, int pas, String code,
			String libelle, SygAnnee pays) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygMois.class);
//		criteria.addOrder(Order.asc("libelle"));
		if (code != null) {
			criteria.add(Restrictions.ilike("code", "%" + code + "%"));
		}
		if (libelle != null) {
			criteria.add(Restrictions.ilike("libelle", "%" + libelle + "%"));
		}
		if (pays != null) {
			criteria.add(Restrictions.eq("annee", pays));
		}
		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}

	@Override
	public int count(String code, String libelle, SygAnnee pays) {
		Criteria criteria = getHibernateSession().createCriteria(
				SygMois.class);
		if (code != null) {
			criteria.add(Restrictions.ilike("code", "%" + code + "%"));
		}
		if (libelle != null) {
			criteria.add(Restrictions.ilike("libelle", "%" + libelle + "%"));
		}
		if (pays != null) {
			criteria.add(Restrictions.eq("annee", pays));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygMois> findAll(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygMois.class);

		if (indice > 0)
			criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);
		return criteria.list();
	}

	@Override
	public List<SygMois> findAllby(int indice, int pas, String code,
			String libelle) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int count(Integer code, String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygMois.class);
		if (code != null) {
			criteria.add(Restrictions.ilike("id", "%" + code + "%"));
		}
		if (libelle != null) {
			criteria.add(Restrictions.ilike("libelle", "%" + libelle + "%"));
		}

		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygMois> find(int start, int step, String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygMois.class);
		criteria.setFirstResult(start);
		
		if (libelle != null) {
			criteria.add(Restrictions.ilike("libelle", libelle,MatchMode.ANYWHERE));
		}
		if(step>0)
			criteria.setMaxResults(step);
		
		return criteria.list(); 

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SygMois> find(int indice, int pas, String code,
			String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygMois.class);
		
		criteria.addOrder(Order.asc("libelle"));
		
		if (code != null) {
			criteria.add(Restrictions.ilike("code", "%" + code + "%"));
		}
		if (libelle != null) {
			criteria.add(Restrictions.ilike("libelle", "%" + libelle + "%"));
		}

		criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<SygMois> find(int indice, int pas,Integer coderegion) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygMois.class);
		if (coderegion != null) {
			criteria.add(Restrictions.like("annee.id", coderegion));
		}
		if (indice > 0)
			criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);

		return criteria.list();
	}
}
