package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.PklDocumentFr;
import sn.ssi.kermel.be.entity.PklFournisseur;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class DocumentFrSessionBean extends AbstractSessionBean implements DocumentFrSession {

	@Override
	public void save(PklDocumentFr culture) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(culture);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	
	}

	@Override
	public PklDocumentFr saveEcole(PklDocumentFr culture) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(culture);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return culture;
	}
	
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(PklDocumentFr.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void update(PklDocumentFr culture) {
		// TODO Auto-generated method stub
		try{
			getHibernateSession().merge(culture);
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public PklDocumentFr updateEcole(PklDocumentFr culture) {
		// TODO Auto-generated method stub
		try{
			getHibernateSession().merge(culture);
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		return culture;
	}
	
	@Override
	public int count() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(PklDocumentFr.class);
		criteria.setProjection(Projections.rowCount());
		return (Integer)criteria.uniqueResult();
	}

	@Override
	public int count(String nom,String empreinte, PklFournisseur fournisseur) {
		Criteria criteria = getHibernateSession().createCriteria(PklDocumentFr.class);
		if(nom!=null){
			criteria.add(Restrictions.ilike("nom", nom, MatchMode.ANYWHERE));
		}
		if(empreinte!=null){
			criteria.add(Restrictions.eq("empreinte", empreinte));
		}
		if(fournisseur!=null){
			criteria.add(Restrictions.eq("fournisseur.id", fournisseur.getId()));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PklDocumentFr> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria=getHibernateSession().createCriteria(PklDocumentFr.class);
		List <PklDocumentFr> unites=criteria.list();
		return unites;	
	}

	@Override
	public PklDocumentFr findByCode(Long id) {
		// TODO Auto-generated method stub
		return (PklDocumentFr)getHibernateSession().get(PklDocumentFr.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PklDocumentFr> find(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(PklDocumentFr.class);
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PklDocumentFr> find(String nom,String empreinte) {
		Criteria criteria = getHibernateSession().createCriteria(PklDocumentFr.class);
		
		if(nom!=null){
			criteria.add(Restrictions.ilike("nom", nom, MatchMode.ANYWHERE));
		}
		if(empreinte!=null){
			criteria.add(Restrictions.eq("empreinte", empreinte));
		}
		
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PklDocumentFr> find(int indice, int pas,String nom,String empreinte, PklFournisseur fournisseur) {
		Criteria criteria = getHibernateSession().createCriteria(PklDocumentFr.class);
		criteria.addOrder(Order.asc("nom"));
		if(nom!=null){
			criteria.add(Restrictions.ilike("nom", nom, MatchMode.ANYWHERE));
		}
		if(empreinte!=null){
			criteria.add(Restrictions.eq("empreinte", empreinte));
		}
		if(fournisseur!=null){
			criteria.add(Restrictions.eq("fournisseur.id", fournisseur.getId()));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PklDocumentFr> find(String nom,String empreinte, PklFournisseur fournisseur) {
		Criteria criteria = getHibernateSession().createCriteria(PklDocumentFr.class);
		
		if(nom!=null){
			criteria.add(Restrictions.eq("nom", nom));
		}
		if(empreinte!=null){
			criteria.add(Restrictions.eq("empreinte", empreinte));
		}
		if(fournisseur!=null){
			criteria.add(Restrictions.eq("fournisseur.id", fournisseur.getId()));
		}

		return criteria.list();
	}

	
}
