package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.PklCritereAlerte;



@Remote
public interface CritereAlerteSession {

    public void save(PklCritereAlerte pays);

    public void delete(Integer Id);

    public List<PklCritereAlerte> find(int indice, int pas);

    public void update(PklCritereAlerte pays);

    public List<PklCritereAlerte> findAll();

    public List<PklCritereAlerte> find(int indice, int pas, String libelle);

    PklCritereAlerte findByCode(Integer id);

    PklCritereAlerte find();

    int count();

	public int count(String libelle);
}
