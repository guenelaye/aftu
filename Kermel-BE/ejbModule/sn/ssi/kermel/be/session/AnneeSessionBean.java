package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygAnnee;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class AnneeSessionBean extends AbstractSessionBean implements
		AnneeSession {

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(
					getHibernateSession().get(SygAnnee.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygAnnee> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygAnnee.class);
		List<SygAnnee> decoupage1 = criteria.list();
		
		return decoupage1;
	}

	@Override
	public SygAnnee findByCode(Integer id) {
		// TODO Auto-generated method stub
		return (SygAnnee) getHibernateSession().get(SygAnnee.class,
				id);
	}

	@Override
	public void save(SygAnnee mois) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(mois);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public void update(SygAnnee mois) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().merge(mois);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public int count() {
		Criteria criteria = getHibernateSession().createCriteria(SygAnnee.class);
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygAnnee> findAll(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygAnnee.class);

		if (indice > 0)
			criteria.setFirstResult(indice);
		if (pas > 0)
			criteria.setMaxResults(pas);
		return criteria.list();
	}


	@Override
	public int count(Integer code, String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygAnnee.class);
		if (code != null) {
			criteria.add(Restrictions.ilike("id", "%" + code + "%"));
		}
		if (libelle != null) {
			criteria.add(Restrictions.ilike("libelle", "%" + libelle + "%"));
		}

		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygAnnee> find(int start, int step, String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(
				SygAnnee.class);
		criteria.setFirstResult(start);
		
		if (libelle != null) {
			criteria.add(Restrictions.ilike("libelle", libelle,MatchMode.ANYWHERE));
		}
		if(step>0)
			criteria.setMaxResults(step);
		
		return criteria.list(); 

	}

}
