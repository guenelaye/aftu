package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.PklFormeJuridique;



@Remote
public interface FormeJuridiqueSession {

    public void save(PklFormeJuridique pays);

    public void delete(Integer Id);

    public List<PklFormeJuridique> find(int indice, int pas);

    public void update(PklFormeJuridique pays);

    public List<PklFormeJuridique> findAll();

    public List<PklFormeJuridique> find(int indice, int pas, String code, String libelle);

    PklFormeJuridique findByCode(Integer id);

    PklFormeJuridique find();

    int count();

    PklFormeJuridique findByCode(String code);
	public int count(String code, String libelle);
}
