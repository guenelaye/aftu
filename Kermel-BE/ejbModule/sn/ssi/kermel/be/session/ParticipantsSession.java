package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygParticipantsFormation;
import sn.ssi.kermel.be.entity.SygAutoriteContractante;
import sn.ssi.kermel.be.entity.SygGroupeFormation;
import sn.ssi.kermel.be.entity.SygProformation;

@Remote
public interface ParticipantsSession {

	public void save(SygParticipantsFormation participant);

	public void delete(Integer id);

	public List<SygParticipantsFormation> find(int indice, int pas, SygProformation formation);

	public void update(SygParticipantsFormation participant);

	public List<SygParticipantsFormation> findAll();

	public List<SygParticipantsFormation> find(int indice, int pas, String prenom,
			String nom,SygProformation formation,SygAutoriteContractante autorite,SygGroupeFormation groupe);

	public int count(String prenom, String nom, SygProformation formation,SygGroupeFormation groupe);

	public SygParticipantsFormation findByCode(Integer id);

	public int count();

}
