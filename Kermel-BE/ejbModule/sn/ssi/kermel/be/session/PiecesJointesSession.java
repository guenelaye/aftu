package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.EaPiecesJointes;
import sn.ssi.kermel.be.entity.SygProformation;

@Remote
public interface PiecesJointesSession {

	public void save(EaPiecesJointes piecejointe);

	public void delete(Integer id);

	public List<EaPiecesJointes> find(int indice, int pas, SygProformation formation);

	public void update(EaPiecesJointes piecejointe);

	public List<EaPiecesJointes> findAll();

	public List<EaPiecesJointes> find(int indice, int pas, String libelle,String nomfichier, SygProformation formation);

	public int count(String libelle, String nomfichier, SygProformation formation);

	public EaPiecesJointes findByCode(Integer id);

	public int count();

	public EaPiecesJointes find(SygProformation formation);
}
