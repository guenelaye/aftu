package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.PkQuestion;
import sn.ssi.kermel.be.entity.PkTabReponse;
import sn.ssi.kermel.be.entity.PklPays;
import sn.ssi.kermel.be.entity.SygDossiers;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class TabReponseSessionBean extends AbstractSessionBean implements TabReponseSession {

    @Override
    public void delete(Integer id) {
	// TODO Auto-generated method stub
	try {
	    getHibernateSession().delete(
		    getHibernateSession().get(PkTabReponse.class, id));
	} catch (Exception e) {
	    // TODO: handle exception
	}
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PkTabReponse> find(int indice, int pas) {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession()
		.createCriteria(PkTabReponse.class);

	if (indice > 0) {
	    criteria.setFirstResult(indice);
	}
	if (pas > 0) {
	    criteria.setMaxResults(pas);
	}
	return criteria.list();
    }

    @Override
    public PkTabReponse find() {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession()
		.createCriteria(PkTabReponse.class);

	return (PkTabReponse) criteria.list().get(0);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PkTabReponse> findAll() {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession()
		.createCriteria(PkTabReponse.class);
	List<PkTabReponse> pays = criteria.list();
	return pays;
    }

    @Override
    public void save(PkTabReponse pays) {
	// TODO Auto-generated method stub
	try {
	    getHibernateSession().save(pays);

	} catch (Exception e) {
	    // TODO: handle exception
	    e.printStackTrace();
	}
    }

    @Override
    public PkTabReponse saveTabReponse(PkTabReponse reponse) {
	// TODO Auto-generated method stub
	try {
	    getHibernateSession().save(reponse);

	} catch (Exception e) {
	    // TODO: handle exception
	    e.printStackTrace();
	}
	return reponse;
    }
    
    
    @Override
    public int count() {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession().createCriteria(PkTabReponse.class);
	criteria.setProjection(Projections.rowCount());
	return Integer.parseInt(criteria.uniqueResult().toString());

    }
    @Override
    public void update(PkTabReponse pays) {
	// TODO Auto-generated method stub
	try {
	    getHibernateSession().merge(pays);
	} catch (Exception e) {
	    // TODO: handle exception
	}
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PkTabReponse> find(int indice, int pas, String code, String libelle) {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession()
		.createCriteria(PkTabReponse.class);
	//criteria.addOrder(Order.asc("libelle"));
	if (code != null) {
	  //  criteria.add(Restrictions.ilike("code", "%" + code + "%"));
	}
	if (libelle != null) {
	   // criteria.add(Restrictions.ilike("libelle", "%" + libelle + "%"));
	}

	criteria.setFirstResult(indice);
	if (pas > 0) {
	    criteria.setMaxResults(pas);
	}

	return criteria.list();
    }

    
    @SuppressWarnings("unchecked")
    @Override
    public List<PkTabReponse> find(SygDossiers dossier) {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession()
		.createCriteria(PkTabReponse.class);
	if (dossier != null) {
	  // criteria.add(Restrictions.eq("dossier" ,dossier));
	}
	

	return criteria.list();
    }

    
    @SuppressWarnings("unchecked")
    @Override
    public List<PkTabReponse> findRepond(SygDossiers dossier) {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession()
		.createCriteria(PkTabReponse.class);
	  // criteria.add(Restrictions.isNotNull("reponse"));
	if (dossier != null) {
	  // criteria.add(Restrictions.eq("dossier" ,dossier));
	}
	

	return criteria.list();
    }

    
    @Override
    public PkTabReponse findByCode(Integer id) {
	// TODO Auto-generated method stub
	return (PkTabReponse) getHibernateSession().get(PkTabReponse.class, id);
	
    }

    @Override
    public PkTabReponse findByCode(String code) {
	// TODO Auto-generated method stub
	Criteria criteria = getHibernateSession()
		.createCriteria(PkTabReponse.class);
	if(code!=null){
	    //criteria.add(Restrictions.eq("code", code));
	}

	PkTabReponse pays = null;

	if(criteria.list().size()==1){
	    pays =  (PkTabReponse)criteria.uniqueResult();
	}else if(criteria.list().size()>0){
	    pays = (PkTabReponse) criteria.list().get(0);
	}
	return pays;
    }

	@Override
	public int count(String code, String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(PkTabReponse.class);

		if(code!=null){
			//criteria.add(Restrictions.ilike("code", "%"+code+"%"));
		}
		if(libelle!=null){
			//criteria.add(Restrictions.eq("libelle", libelle));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
}
