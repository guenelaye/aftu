package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.PklAlerte;
import sn.ssi.kermel.be.entity.PklComptePerso;
import sn.ssi.kermel.be.entity.PklTypeAlerte;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class AlerteSessionBean extends AbstractSessionBean implements AlerteSession {

	@Override
	public void save(PklAlerte culture) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(culture);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	
	}

	@Override
	public PklAlerte saveEcole(PklAlerte culture) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(culture);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return culture;
	}
	
	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(PklAlerte.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void update(PklAlerte culture) {
		// TODO Auto-generated method stub
		try{
			getHibernateSession().merge(culture);
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public PklAlerte updateEcole(PklAlerte culture) {
		// TODO Auto-generated method stub
		try{
			getHibernateSession().merge(culture);
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		return culture;
	}
	
	@Override
	public int count() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(PklAlerte.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public int count(String libelle,String statut, PklComptePerso compte, PklTypeAlerte type) {
		Criteria criteria = getHibernateSession().createCriteria(PklAlerte.class);
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", libelle, MatchMode.ANYWHERE));
		}
		if(statut!=null){
			criteria.add(Restrictions.eq("statut", statut));
		}
		if(compte!=null){
			criteria.add(Restrictions.eq("compte.id", compte.getId()));
		}
		if(type!=null){
			criteria.add(Restrictions.eq("type.id", type.getId()));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PklAlerte> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria=getHibernateSession().createCriteria(PklAlerte.class);
		List <PklAlerte> unites=criteria.list();
		return unites;	
	}

	@Override
	public PklAlerte findByCode(Long id) {
		// TODO Auto-generated method stub
		return (PklAlerte)getHibernateSession().get(PklAlerte.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PklAlerte> find(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(PklAlerte.class);
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PklAlerte> find(String libelle,String statut) {
		Criteria criteria = getHibernateSession().createCriteria(PklAlerte.class);
		
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", libelle, MatchMode.ANYWHERE));
		}
		if(statut!=null){
			criteria.add(Restrictions.eq("statut", statut));
		}
		
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PklAlerte> find(int indice, int pas,String libelle,String statut, PklComptePerso compte, PklTypeAlerte type) {
		Criteria criteria = getHibernateSession().createCriteria(PklAlerte.class);
		criteria.addOrder(Order.asc("libelle"));
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle", libelle, MatchMode.ANYWHERE));
		}
		if(statut!=null){
			criteria.add(Restrictions.eq("statut", statut));
		}
		if(compte!=null){
			criteria.add(Restrictions.eq("compte.id", compte.getId()));
		}
		if(type!=null){
			criteria.add(Restrictions.eq("type.id", type.getId()));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PklAlerte> find(String libelle,String statut, PklComptePerso compte) {
		Criteria criteria = getHibernateSession().createCriteria(PklAlerte.class);
		
		if(libelle!=null){
			criteria.add(Restrictions.eq("libelle", libelle));
		}
		if(statut!=null){
			criteria.add(Restrictions.eq("statut", statut));
		}
		if(compte!=null){
			criteria.add(Restrictions.eq("compte.id", compte.getId()));
		}

		return criteria.list();
	}

	
}
