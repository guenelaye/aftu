package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygCalendrierPaiement;
import sn.ssi.kermel.be.entity.SygDemandePaiement;
import sn.ssi.kermel.be.entity.SygPaiement;
import sn.ssi.kermel.be.entity.SygPenaliteRetard;
import sn.ssi.kermel.be.entity.SygSuiviPaiement;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class PenaliteRetardSessionBean extends AbstractSessionBean implements PenaliteRetardSession{

	@Override
	public int count(Long code,String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygPenaliteRetard.class);
		if(code!=null){
			//criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
		//	criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygPenaliteRetard.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygPenaliteRetard> find(int indice, int pas,Long code,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPenaliteRetard.class);
		
		if(code!=null){
		 	criteria.createAlias("demande", "demande");
		    criteria.createAlias("demande.contrat", "contrat");
	        criteria.add(Restrictions.eq("contrat.conID", code));
		}
		if(libelle!=null){
			//criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	@Override
	public  SygPenaliteRetard findPenalite(Long code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPenaliteRetard.class);
		
		if(code!=null){
		 	criteria.createAlias("demande", "demande");
		        criteria.add(Restrictions.eq("demande.demPaId", code));
		}
		if(criteria.list().size()!=0)
			return  (SygPenaliteRetard) criteria.list().get(0);
		else
		
		return  null;
	}

	

	@Override
	public void save(SygPenaliteRetard paiement) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(paiement);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygPenaliteRetard paiement) {
		
		try{
			getHibernateSession().merge(paiement);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygPenaliteRetard findById(Long code) {
		return (SygPenaliteRetard)getHibernateSession().get(SygPenaliteRetard.class, code);
	}
	
	@Override
	public List<SygPenaliteRetard> findRech(int indice, int pas,Long code,String nom,String prenom) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygPenaliteRetard.class);
		
		if(code!=null){
		//	criteria.add(Restrictions.eq("id", code));
		}
		if(nom!=null){
			//criteria.add(Restrictions.ilike("nom", "%"+nom+"%"));
		}
		
		if(prenom!=null){
			//criteria.add(Restrictions.ilike("prenom", "%"+prenom+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public int countRech(Long code,String nom,String prenom) {
		Criteria criteria = getHibernateSession().createCriteria(SygPenaliteRetard.class);
		//criteria.createAlias("decision", "decision");
		if(code!=null){
			//criteria.add(Restrictions.eq("id", code));
		}
		if(nom!=null){
			//criteria.add(Restrictions.ilike("nom", "%"+nom+"%"));
		}
		
		if(prenom!=null){
			//criteria.add(Restrictions.ilike("prenom", "%"+prenom+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
}
