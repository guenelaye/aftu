package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.PklPays;



@Remote
public interface PaySession {

    public void save(PklPays pays);

    public void delete(Integer Id);

    public List<PklPays> find(int indice, int pas);

    public void update(PklPays pays);

    public List<PklPays> findAll();

    public List<PklPays> find(int indice, int pas, String code, String libelle);

    PklPays findByCode(Integer id);

    PklPays find();

    int count();

    PklPays findByCode(String code);
	public int count(String code, String libelle);
}
