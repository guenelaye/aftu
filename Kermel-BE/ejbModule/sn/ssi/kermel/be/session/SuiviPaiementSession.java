package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCalendrierPaiement;
import sn.ssi.kermel.be.entity.SygDemandePaiement;
import sn.ssi.kermel.be.entity.SygPaiement;
import sn.ssi.kermel.be.entity.SygSuiviPaiement;

@Remote
public interface SuiviPaiementSession {
	
	public void save(SygSuiviPaiement paiement);
	
	public void delete(Long id);
	
	public List<SygSuiviPaiement> find(int indice, int pas,Long code,String libelle);
	
	public List<SygSuiviPaiement> findRech(int indice, int pas,Long code,String nom,String prenom);
	
	public int count(Long code,String libelle);
	
	public int countRech(Long code,String nom,String prenom);
	
	public void update(SygSuiviPaiement paiement);
	
	public SygSuiviPaiement findById(Long code);

	SygSuiviPaiement saveSuivi(SygSuiviPaiement paiement);
	
}
