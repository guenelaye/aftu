package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCalendrierPaiement;
import sn.ssi.kermel.be.entity.SygDemandePaiement;
import sn.ssi.kermel.be.entity.SygPaiement;
import sn.ssi.kermel.be.entity.SygPenaliteRetard;
import sn.ssi.kermel.be.entity.SygSuiviPaiement;

@Remote
public interface PenaliteRetardSession {
	
	public void save(SygPenaliteRetard paiement);
	
	public void delete(Long id);
	
	public List<SygPenaliteRetard> find(int indice, int pas,Long code,String libelle);
	
	public List<SygPenaliteRetard> findRech(int indice, int pas,Long code,String nom,String prenom);
	
	public int count(Long code,String libelle);
	
	public int countRech(Long code,String nom,String prenom);
	
	public void update(SygPenaliteRetard paiement);
	
	public SygPenaliteRetard findById(Long code);

	SygPenaliteRetard findPenalite(Long code);

	
}
