package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCalendrierPaiement;
import sn.ssi.kermel.be.entity.SygDemandePaiement;
import sn.ssi.kermel.be.entity.SygPaiement;

@Remote
public interface DemandePaiementSession {
	
	public void save(SygDemandePaiement paiement);
	
	public void delete(Long id);
	
	public List<SygDemandePaiement> find(int indice, int pas,Long code,String libelle);
	
	public List<SygDemandePaiement> findRech(int indice, int pas,Long code,String nom,String prenom);
	
	public int count(Long code,String libelle);
	
	public int countRech(Long code,String nom,String prenom);
	
	public void update(SygDemandePaiement paiement);
	
	public SygDemandePaiement findById(Long code);

	SygDemandePaiement findDemande(Long code);
	
}
