package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCatIndStatGle;

@Remote
public interface CatIndStatGleSession {

	public void save(SygCatIndStatGle categ);

	public void update(SygCatIndStatGle categ);
	
	public void delete(Integer id);
	
	int count();

	public int count(String code,String libelle);

	public List<SygCatIndStatGle> findAll();

	SygCatIndStatGle findByCode(Integer id);

	public List<SygCatIndStatGle> find(int indice, int pas);
	
	public List<SygCatIndStatGle> find(int indice, int pas,String code,String libelle);
	
	
}
