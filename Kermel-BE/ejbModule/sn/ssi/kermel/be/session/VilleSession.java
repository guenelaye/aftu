package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.PklVille;
import sn.ssi.kermel.be.entity.PklPays;

/**
 * @author Coura Diagne, OLA Abdel Rakib, olarakib@gmail.com
 * @since Vendredi 12 F�vrier 2010, 13:27
 * @version 0.0.1
 */
@Remote
public interface VilleSession {
	public void save(PklVille decoupage1);

	public List<PklVille> findAll(int indice, int pas);

	public void update(PklVille decoupage1);

	PklVille findByCode(Integer code);

	List<PklVille> findAllby(int indice, int pas,
			String libelle);

	public void delete(Integer id);

	public int count(String code, String libelle, PklPays pays);

	List<PklVille> findAll();

	public int count(Integer code, String libelle);

	List<PklVille> find(int start, int step, String libelle, PklPays pays);

	List<PklVille> find(int indice, int pas, String libelle);

	PklVille findByCodePays(String code);

	//	//
	//	public int sumpop(int population);
	//
	//	public int sumsup(double superficie);

}
