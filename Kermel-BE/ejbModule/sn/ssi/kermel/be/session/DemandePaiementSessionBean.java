package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygCalendrierPaiement;
import sn.ssi.kermel.be.entity.SygDemandePaiement;
import sn.ssi.kermel.be.entity.SygPaiement;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;


@Stateless
public class DemandePaiementSessionBean extends AbstractSessionBean implements DemandePaiementSession{

	@Override
	public int count(Long code,String libelle) {
		Criteria criteria = getHibernateSession().createCriteria(SygDemandePaiement.class);
		if(code!=null){
			//criteria.add(Restrictions.eq("id", code));
		}
		if(libelle!=null){
		//	criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());


	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygDemandePaiement.class, id));
			getHibernateSession().flush();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public List<SygDemandePaiement> find(int indice, int pas,Long code,String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDemandePaiement.class);
		if(code!=null){
			criteria.add(Restrictions.eq("contrat.conID", code));
		}
		if(libelle!=null){
			//criteria.add(Restrictions.ilike("libelle", "%"+libelle+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@Override
	public SygDemandePaiement findDemande(Long code) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDemandePaiement.class);
		if(code!=null){
			criteria.add(Restrictions.eq("calendrier.calenpaId", code));
		}
		
		if( criteria.list().size()!=0)
		return (SygDemandePaiement) criteria.list().get(0);
		else
			return null;
	}
	

	@Override
	public void save(SygDemandePaiement paiement) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(paiement);
			getHibernateSession().flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	}

	@Override
	public void update(SygDemandePaiement paiement) {
		
		try{
			getHibernateSession().merge(paiement);
			getHibernateSession().flush();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public SygDemandePaiement findById(Long code) {
		return (SygDemandePaiement)getHibernateSession().get(SygDemandePaiement.class, code);
	}
	
	@Override
	public List<SygDemandePaiement> findRech(int indice, int pas,Long code,String nom,String prenom) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygDemandePaiement.class);
		
		if(code!=null){
		//	criteria.add(Restrictions.eq("id", code));
		}
		if(nom!=null){
			//criteria.add(Restrictions.ilike("nom", "%"+nom+"%"));
		}
		
		if(prenom!=null){
			//criteria.add(Restrictions.ilike("prenom", "%"+prenom+"%"));
		}
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}
	
	
	@Override
	public int countRech(Long code,String nom,String prenom) {
		Criteria criteria = getHibernateSession().createCriteria(SygDemandePaiement.class);
		//criteria.createAlias("decision", "decision");
		if(code!=null){
			//criteria.add(Restrictions.eq("id", code));
		}
		if(nom!=null){
			//criteria.add(Restrictions.ilike("nom", "%"+nom+"%"));
		}
		
		if(prenom!=null){
			//criteria.add(Restrictions.ilike("prenom", "%"+prenom+"%"));
		}
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}
}
