package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Stateless;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import sn.ssi.kermel.be.entity.SygCatIndStatGle;
import sn.ssi.kermel.common.ejb.AbstractSessionBean;

@Stateless
public class CatIndStatGleSessionBean extends AbstractSessionBean implements CatIndStatGleSession{

	@Override
	public void save(SygCatIndStatGle categ) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().save(categ);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}	
	}

	@Override
	public void update(SygCatIndStatGle categ) {
		// TODO Auto-generated method stub
		try{
			getHibernateSession().merge(categ);
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		try {
			getHibernateSession().delete(getHibernateSession().get(SygCatIndStatGle.class, id));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public int count() {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCatIndStatGle.class);
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@Override
	public int count(String code, String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCatIndStatGle.class);
		if(code!=null){
			criteria.add(Restrictions.ilike("code","%"+ code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle","%"+ libelle+"%"));
		}
		
		criteria.setProjection(Projections.rowCount());
		return Integer.parseInt(criteria.uniqueResult().toString());

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygCatIndStatGle> findAll() {
		// TODO Auto-generated method stub
		Criteria criteria=getHibernateSession().createCriteria(SygCatIndStatGle.class);
		List <SygCatIndStatGle> categ=criteria.list();
		return categ;	
	}

	@Override
	public SygCatIndStatGle findByCode(Integer id) {
		// TODO Auto-generated method stub
		return (SygCatIndStatGle)getHibernateSession().get(SygCatIndStatGle.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygCatIndStatGle> find(int indice, int pas) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCatIndStatGle.class);
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SygCatIndStatGle> find(int indice, int pas, String code,
			String libelle) {
		// TODO Auto-generated method stub
		Criteria criteria = getHibernateSession().createCriteria(SygCatIndStatGle.class);
		criteria.addOrder(Order.asc("code"));
		if(code!=null){
			criteria.add(Restrictions.ilike("code","%"+ code+"%"));
		}
		if(libelle!=null){
			criteria.add(Restrictions.ilike("libelle","%"+ libelle+"%"));
		}
		
		criteria.setFirstResult(indice);
		if(pas>0)
			criteria.setMaxResults(pas);
		
		return criteria.list();
	}

}
