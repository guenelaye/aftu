package sn.ssi.kermel.be.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ssi.kermel.be.entity.SygCatIndSurvMulti;
import sn.ssi.kermel.be.entity.SygIndSurvMulti;

@Remote
public interface IndSurvMultiSession {

	public void save(SygIndSurvMulti indicator);
	
	public void delete(Long id);
	
	public List<SygIndSurvMulti> find(int indice, int pas,String code,String libelle, SygCatIndSurvMulti categorie);
	
	public int count(String code,String libelle, SygCatIndSurvMulti categorie);
	
	public void update(SygIndSurvMulti indicator);
	
	public List<SygIndSurvMulti> findAll();
	
	SygIndSurvMulti findByCode(Long id);
	
	public List<SygIndSurvMulti> find(int indice, int pas);
	
	int count();
}
