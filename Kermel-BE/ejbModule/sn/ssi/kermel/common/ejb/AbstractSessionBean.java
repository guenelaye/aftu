package sn.ssi.kermel.common.ejb;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;

public class AbstractSessionBean {
	@PersistenceContext(unitName = "kermel")
	EntityManager entityManager;

	// Session factory de hibernate
	public Session getHibernateSession() {
		return (Session) entityManager.getDelegate();
	}
}